using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace ParticleEditor
{
    public partial class EmitterWindow : UserControl
    {
        public String m_CurrentEmitterName = "";

        public EmitterWindow()
        {
            InitializeComponent();
            m_DataGridKeyframeList.ColumnHeadersVisible = false;
            m_DataGridEmitKeyframes.ColumnHeadersVisible = false;
			m_DataGridVelKeyframes.ColumnHeadersVisible = false;
			m_DataGridAttrKeyframes.ColumnHeadersVisible = false;

//            m_Duration.OnValueChanged += new ptxSlider.ptxSliderEventDelegate(m_Timeline_OnValueChanged);
            m_CheckOneShot.Click += new EventHandler(m_OneShot_Click);
        }

        public void BuildDomainTypeList()
        {
            m_ComboEmitShape.SuspendLayout();
			m_ComboVelShape.SuspendLayout();
			m_ComboAttrShape.SuspendLayout();

            m_ComboEmitShape.Items.Clear();
			m_ComboVelShape.Items.Clear();
			m_ComboAttrShape.Items.Clear();
            
            //Emitter Domains
            for(int i =0;i<DataMod.m_SDataMod.m_EmitterDomainTypes.Count;i++)
				m_ComboEmitShape.Items.Add(DataMod.m_SDataMod.m_EmitterDomainTypes[i]);

            //Velocity Domains
            for (int i = 0; i < DataMod.m_SDataMod.m_VelocityDomainTypes.Count; i++)
				m_ComboVelShape.Items.Add(DataMod.m_SDataMod.m_VelocityDomainTypes[i]);

			//Attractor Domains
			for (int i = 0; i < DataMod.m_SDataMod.m_AttractorDomainTypes.Count; i++)
				m_ComboAttrShape.Items.Add(DataMod.m_SDataMod.m_AttractorDomainTypes[i]);

            m_ComboEmitShape.ResumeLayout();
			m_ComboVelShape.ResumeLayout();
			m_ComboAttrShape.ResumeLayout();
        }
        public void DisplayEmitter(String name)
        {
            DataMod.dtaStdEmitRule doe = DataMod.GetEmitRule(name) as DataMod.dtaStdEmitRule;
            if (doe == null)
            {
                //DataCon.m_SDataCon.RequestEmitterInfo(name);
                EnableSubSections(false);
                main.m_MainForm.ClearEvolutionData(m_DataGridEmitKeyframes);
				main.m_MainForm.ClearEvolutionData(m_DataGridVelKeyframes);
				main.m_MainForm.ClearEvolutionData(m_DataGridAttrKeyframes);
                main.m_MainForm.ClearEvolutionData(m_DataGridKeyframeList);
                return;
            }
            EnableSubSections(true);

            m_CurrentEmitterName = doe.m_Name;
            m_LabelHeader.Text = "Emitter: " + doe.m_Name;
            
            //Setup
            m_CheckOneShot.Checked = doe.m_OneShot;
//            m_Duration.SetValue = doe.m_Duration;

            //Emitter Domain
            m_CheckEmitShow.Checked = doe.m_ShowEmitterDomain;
            m_CheckEmitWorld.Checked = doe.m_EmitterDomain.m_WorldSpace;
			m_ComboEmitShape.SelectedIndex = DataMod.GetEmitterDomainTypeFromName(DataMod.GetDomainNameFromType((byte)doe.m_EmitterDomain.m_Type));
            main.m_MainForm.BuildKeyframeGrid(m_DataGridEmitKeyframes, doe.m_EmitterDomain.m_KeyframeList);

            //Velocity Domain
            m_CheckVelShow.Checked = doe.m_ShowVelocityDomain;
            m_CheckVelWorld.Checked = doe.m_VelocityDomain.m_WorldSpace;
            m_CheckVelPointRel.Checked = doe.m_VelocityDomain.m_PointRelative;
            m_CheckVelCreationRel.Checked = doe.m_VelocityDomain.m_CreationRelative;
			m_ComboVelShape.SelectedIndex = DataMod.GetVelocityDomainTypeFromName(DataMod.GetDomainNameFromType((byte)doe.m_VelocityDomain.m_Type));
            main.m_MainForm.BuildKeyframeGrid(m_DataGridVelKeyframes, doe.m_VelocityDomain.m_KeyframeList);

			//Attractor Domain
			if (doe.m_AttractorDomain.m_Exists)
			{
				m_CheckAttrShow.Enabled = true;
				m_CheckAttrWorld.Enabled = true;
				m_ComboAttrShape.Enabled = true;
				m_LabelAttrShape.Enabled = true;
				m_ButtonAttrEnableToggle.Text = "Disable";

				m_CheckAttrShow.Checked = doe.m_ShowAttractorDomain;
                m_CheckAttrWorld.Checked = doe.m_AttractorDomain.m_WorldSpace;
                m_CheckAttrCreationRel.Checked = doe.m_AttractorDomain.m_CreationRelative;
                m_CheckAttrTargetRel.Checked = doe.m_AttractorDomain.m_TargetRelative;
				m_ComboAttrShape.SelectedIndex = DataMod.GetAttractorDomainTypeFromName(DataMod.GetDomainNameFromType((byte)doe.m_AttractorDomain.m_Type));
				main.m_MainForm.BuildKeyframeGrid(m_DataGridAttrKeyframes, doe.m_AttractorDomain.m_KeyframeList);
			}
			else
			{
				m_CheckAttrShow.Checked = false;
				m_CheckAttrWorld.Checked = false;
				m_ComboAttrShape.SelectedIndex = 0;
				DataMod.dtaPtxKeyframePropList emptyKeyframePropList = new DataMod.dtaPtxKeyframePropList();
				main.m_MainForm.BuildKeyframeGrid(m_DataGridAttrKeyframes, emptyKeyframePropList);

				m_CheckAttrShow.Enabled = false;
				m_CheckAttrWorld.Enabled = false;
				m_ComboAttrShape.Enabled = false;
				m_LabelAttrShape.Enabled = false;
				m_ButtonAttrEnableToggle.Text = "Enable";
			}

            //Emitter Keyframes
            //Keyframes
            main.m_MainForm.BuildKeyframeGrid(m_DataGridKeyframeList, doe.m_KeyframeList);

            DisplayEvolutionData(main.m_MainForm.m_EffectWindow.m_CurrentEvent);
        }

        protected void DisplayEvolutionData(DataMod.dtaEvent doev)
        {
            if (doev == null) return;
            if (doev is DataMod.dtaEventEmitter)
                if ((doev as DataMod.dtaEventEmitter).m_EmitRuleName == m_CurrentEmitterName)
                {
                    main.m_MainForm.DisplayEvolutionData(m_DataGridKeyframeList,doev.m_EvoList);
                    main.m_MainForm.DisplayEvolutionData(m_DataGridEmitKeyframes,doev.m_EvoList);
					main.m_MainForm.DisplayEvolutionData(m_DataGridVelKeyframes, doev.m_EvoList);
					main.m_MainForm.DisplayEvolutionData(m_DataGridAttrKeyframes, doev.m_EvoList);
                }
        }

        protected void SetDataFromGui()
        {
            DataMod.dtaStdEmitRule doe = DataMod.GetEmitRule(m_CurrentEmitterName);
            if (doe == null) return;

            //Setup
            doe.m_OneShot = m_CheckOneShot.Checked;
 //           doe.m_Duration = m_Duration.Value;

            //Emitter Domain
            doe.m_ShowEmitterDomain = m_CheckEmitShow.Checked;
            doe.m_EmitterDomain.m_WorldSpace = m_CheckEmitWorld.Checked;
            doe.m_EmitterDomain.m_Type = DataMod.GetDomainTypeFromName(m_ComboEmitShape.Items[m_ComboEmitShape.SelectedIndex].ToString());

            //Velocity Domain
            doe.m_ShowVelocityDomain = m_CheckVelShow.Checked;
            doe.m_VelocityDomain.m_WorldSpace = m_CheckVelWorld.Checked;
            doe.m_VelocityDomain.m_PointRelative = m_CheckVelPointRel.Checked;
            doe.m_VelocityDomain.m_CreationRelative = m_CheckVelCreationRel.Checked;
            doe.m_VelocityDomain.m_Type = DataMod.GetDomainTypeFromName(m_ComboVelShape.Items[m_ComboVelShape.SelectedIndex].ToString());
	
			//Attractor Domain
			doe.m_ShowAttractorDomain = m_CheckAttrShow.Checked;
            doe.m_AttractorDomain.m_WorldSpace = m_CheckAttrWorld.Checked;
            doe.m_AttractorDomain.m_CreationRelative = m_CheckAttrCreationRel.Checked;
            doe.m_AttractorDomain.m_TargetRelative = m_CheckAttrTargetRel.Checked;
			doe.m_AttractorDomain.m_Type = DataMod.GetDomainTypeFromName(m_ComboAttrShape.Items[m_ComboAttrShape.SelectedIndex].ToString());
  
		}

        public void ChangedEmitter(String name)
        {
            if (name == "") return;
            DataCon.m_SDataCon.SendEmitterRule(name);
        }

        public void EnableSubSections(bool b)
        {
            foreach (ptxSubSection sub in this.m_PanelSection.Controls)
                sub.Enabled = b;
        }

        public DataMod.KeyframeSpec GetSelectedKeyframeFromDataGrid(DataGridView grid)
        {
            if (grid.CurrentRow == null) return null;
            DataMod.KeyframeSpec key = grid.CurrentRow.Cells[0].Tag as DataMod.KeyframeSpec;
            return key;

        }
        public DataMod.KeyframeSpec GetSelectedKeyframeCellFromDataGrid(DataGridView grid)
        {
            if (grid.CurrentCell == null) return null;
            DataMod.KeyframeSpec key = grid.CurrentCell.Tag as DataMod.KeyframeSpec;
            return key;

        }
        public void SelectPropertyFromDataGrid(DataGridView grid, String propName)
        {
            if (propName == "")
            {
                grid.CurrentCell = null;
                return;
            }

            foreach (DataGridViewRow row in grid.Rows)
            {
                if (row.Cells[0].Value.ToString() == propName)
                {
                    grid.CurrentCell = row.Cells[0];
                    return;
                }
            }
            grid.CurrentCell = null;
        }
        public void SelectOnRightMouseButton_CellMouseDown(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                DataGridView grid = sender as DataGridView;
                grid.CurrentCell = grid.Rows[e.RowIndex].Cells[e.ColumnIndex];
            }

        }
        public void DataGridAutoSelectValidCell(object sender, EventArgs e)
        {
            DataGridView grid = sender as DataGridView;
            if (grid.CurrentCell == null) return;
            if (grid.CurrentCell.Value == null)
                grid.CurrentCell = grid.Rows[grid.CurrentCell.RowIndex].Cells[0];
        }


        private void m_CheckBox_Click(object sender, EventArgs e)
        {
            SetDataFromGui();
            ChangedEmitter(m_CurrentEmitterName);
        }


        private void m_Slider_OnValueChanged(ptxSlider slider)
        {
            SetDataFromGui();
            ChangedEmitter(m_CurrentEmitterName);
        }

        private void m_Combo_SelectionChangeCommitted(object sender, EventArgs e)
        {
            SetDataFromGui();
            ChangedEmitter(m_CurrentEmitterName);
            //DataCon.m_SDataCon.SendEmitRuleDomainTypeChanged(m_CurrentEmitterName);
        }

        private void ShowKeyframe(String description,object sender, EventArgs e)
        {
            DataGridView grid = sender as DataGridView;
            if (grid.CurrentCell == null) return;
            int cdx = grid.CurrentCell.ColumnIndex;
            
            String desc = "EmitRule: " + m_CurrentEmitterName;
            if(description.Length>0)
                desc +=": "+description;

            if ((cdx > 0) && (cdx <= main.m_MainForm.m_EffectWindow.m_CurrentEvent.m_EvoList.m_Evolutions.Count))
            {
                String evoname = (main.m_MainForm.m_EffectWindow.m_CurrentEvent.m_EvoList.m_Evolutions[cdx - 1] as DataMod.dtaEvolution).m_Name;
                desc += ": Evolution (" + evoname + ")";
            }
            KeyframeEditorFormBase.ShowKeyframe(GetSelectedKeyframeCellFromDataGrid(sender as DataGridView), desc + ":");
            //TrackViewerFormBase.ShowKeyframe(GetSelectedKeyframeCellFromDataGrid(sender as DataGridView), desc + ":");
        }

        private void KeyProp_Menu_AddEvo_Click(object sender, EventArgs e)
        {
            String effectName = main.m_MainForm.m_EffectWindow.m_CurrentEffectName;
            DataMod.dtaEvent doe = main.m_MainForm.m_EffectWindow.m_CurrentEvent;

            ToolStripMenuItem item = sender as ToolStripMenuItem;
            if (item == null) return;
            String evoname = item.Tag as String;

            DataMod.dtaPtxKeyframeProp keyprop = m_ContextMenuKeyProp.Tag as DataMod.dtaPtxKeyframeProp;
            if (keyprop == null) return;
            DataCon.m_SDataCon.SendAddPropEvolutionEvent(effectName, evoname,doe.m_Index, keyprop);


        }
        private void KeyProp_Menu_DelEvo_Click(object sender, EventArgs e)
        {
            String effectName = main.m_MainForm.m_EffectWindow.m_CurrentEffectName;
            DataMod.dtaEvent doe = main.m_MainForm.m_EffectWindow.m_CurrentEvent;

            ToolStripMenuItem item = sender as ToolStripMenuItem;
            if (item == null) return;
            String evoname = item.Tag as String;
            DataMod.dtaPtxKeyframeProp keyprop = m_ContextMenuKeyProp.Tag as DataMod.dtaPtxKeyframeProp;
            if (keyprop == null) return;
            DataCon.m_SDataCon.SendDelPropEvolutionEvent(effectName, evoname,doe.m_Index, keyprop);
        }
        private void KeyProp_Menu_AddEvo_ED_Click(object sender, EventArgs e)
        {
            String effectName = main.m_MainForm.m_EffectWindow.m_CurrentEffectName;
            DataMod.dtaEvent doe = main.m_MainForm.m_EffectWindow.m_CurrentEvent;

            ToolStripMenuItem item = sender as ToolStripMenuItem;
            if (item == null) return;
            String evoname = item.Tag as String;
            DataMod.dtaPtxKeyframeProp keyprop = m_ContextMenuEDKeyProp.Tag as DataMod.dtaPtxKeyframeProp;
            if (keyprop == null) return;
            DataCon.m_SDataCon.SendAddPropEvolutionEvent(effectName, evoname,doe.m_Index, keyprop);
        }
        private void KeyProp_Menu_DelEvo_ED_Click(object sender, EventArgs e)
        {
            String effectName = main.m_MainForm.m_EffectWindow.m_CurrentEffectName;
            DataMod.dtaEvent doe = main.m_MainForm.m_EffectWindow.m_CurrentEvent;

            ToolStripMenuItem item = sender as ToolStripMenuItem;
            if (item == null) return;
            String evoname = item.Tag as String;
            DataMod.dtaPtxKeyframeProp keyprop = m_ContextMenuEDKeyProp.Tag as DataMod.dtaPtxKeyframeProp;
            if (keyprop == null) return;
            DataCon.m_SDataCon.SendDelPropEvolutionEvent(effectName, evoname,doe.m_Index, keyprop);
        }
        private void KeyProp_Menu_AddEvo_VD_Click(object sender, EventArgs e)
        {
            String effectName = main.m_MainForm.m_EffectWindow.m_CurrentEffectName;
            DataMod.dtaEvent doe = main.m_MainForm.m_EffectWindow.m_CurrentEvent;

            ToolStripMenuItem item = sender as ToolStripMenuItem;
            if (item == null) return;
            String evoname = item.Tag as String;
            DataMod.dtaPtxKeyframeProp keyprop = m_ContextMenuVDKeyProp.Tag as DataMod.dtaPtxKeyframeProp;
            if (keyprop == null) return;
            DataCon.m_SDataCon.SendAddPropEvolutionEvent(effectName, evoname,doe.m_Index, keyprop);
        }
        private void KeyProp_Menu_DelEvo_VD_Click(object sender, EventArgs e)
        {
            String effectName = main.m_MainForm.m_EffectWindow.m_CurrentEffectName;
            DataMod.dtaEvent doe = main.m_MainForm.m_EffectWindow.m_CurrentEvent;

            ToolStripMenuItem item = sender as ToolStripMenuItem;
            if (item == null) return;
            String evoname = item.Tag as String;
            DataMod.dtaPtxKeyframeProp keyprop = m_ContextMenuVDKeyProp.Tag as DataMod.dtaPtxKeyframeProp;
            if (keyprop == null) return;
            DataCon.m_SDataCon.SendDelPropEvolutionEvent(effectName, evoname,doe.m_Index, keyprop);
		}
		private void KeyProp_Menu_AddEvo_AD_Click(object sender, EventArgs e)
		{
			String effectName = main.m_MainForm.m_EffectWindow.m_CurrentEffectName;
			DataMod.dtaEvent doe = main.m_MainForm.m_EffectWindow.m_CurrentEvent;

			ToolStripMenuItem item = sender as ToolStripMenuItem;
			if (item == null) return;
			String evoname = item.Tag as String;
			DataMod.dtaPtxKeyframeProp keyprop = m_ContextMenuADKeyProp.Tag as DataMod.dtaPtxKeyframeProp;
			if (keyprop == null) return;
			DataCon.m_SDataCon.SendAddPropEvolutionEvent(effectName, evoname, doe.m_Index, keyprop);
		}
		private void KeyProp_Menu_DelEvo_AD_Click(object sender, EventArgs e)
		{
			String effectName = main.m_MainForm.m_EffectWindow.m_CurrentEffectName;
			DataMod.dtaEvent doe = main.m_MainForm.m_EffectWindow.m_CurrentEvent;

			ToolStripMenuItem item = sender as ToolStripMenuItem;
			if (item == null) return;
			String evoname = item.Tag as String;
			DataMod.dtaPtxKeyframeProp keyprop = m_ContextMenuADKeyProp.Tag as DataMod.dtaPtxKeyframeProp;
			if (keyprop == null) return;
			DataCon.m_SDataCon.SendDelPropEvolutionEvent(effectName, evoname, doe.m_Index, keyprop);
		}

        private void m_ContextMenuEmitKeyProps_Opening(object sender, CancelEventArgs e)
        {
            DataMod.dtaEvent doe = main.m_MainForm.m_EffectWindow.m_CurrentEvent;
            if (doe == null) { e.Cancel = true; return; }
            DataMod.dtaPtxKeyframeProp keyProp = main.m_MainForm.GetSelectedKeyframePropFromDataGrid(m_DataGridKeyframeList);
            if (keyProp == null) { e.Cancel = true; return; }

            if (doe is DataMod.dtaEventEmitter)
            {
                if ((doe as DataMod.dtaEventEmitter).m_EmitRuleName == m_CurrentEmitterName)
                    main.m_MainForm.GenerateKeyPropEvoMenu("Emitter", m_ContextMenuKeyProp, doe.m_EvoList, keyProp, new EventHandler(KeyProp_Menu_AddEvo_Click), new EventHandler(KeyProp_Menu_DelEvo_Click));
                else
                    main.m_MainForm.GenerateKeyPropMenu("Emitter", m_ContextMenuKeyProp, keyProp);
            }
            e.Cancel = false;
        }
        private void m_ContextMenuEDKeyProp_Opening(object sender, CancelEventArgs e)
        {
            DataMod.dtaEvent doe = main.m_MainForm.m_EffectWindow.m_CurrentEvent;
            if (doe == null) { e.Cancel = true; return; }

            DataMod.dtaPtxKeyframeProp keyProp = main.m_MainForm.GetSelectedKeyframePropFromDataGrid(m_DataGridEmitKeyframes);
            if (keyProp == null) { e.Cancel = true; return; }


            if (doe is DataMod.dtaEventEmitter)
            {
                if ((doe as DataMod.dtaEventEmitter).m_EmitRuleName == m_CurrentEmitterName)
                    main.m_MainForm.GenerateKeyPropEvoMenu("Emit Domain", m_ContextMenuEDKeyProp, doe.m_EvoList, keyProp, new EventHandler(KeyProp_Menu_AddEvo_ED_Click), new EventHandler(KeyProp_Menu_DelEvo_ED_Click));
                else
                    main.m_MainForm.GenerateKeyPropMenu("Emit Domain", m_ContextMenuEDKeyProp, keyProp);
            }
            e.Cancel = false;
        }
        private void m_ContextMenuVDKeyProp_Opening(object sender, CancelEventArgs e)
        {
            DataMod.dtaEvent doe = main.m_MainForm.m_EffectWindow.m_CurrentEvent;
            if (doe == null) { e.Cancel = true; return; }

            DataMod.dtaPtxKeyframeProp keyProp = main.m_MainForm.GetSelectedKeyframePropFromDataGrid(m_DataGridVelKeyframes);
            if (keyProp == null) { e.Cancel = true; return; }

            if (doe is DataMod.dtaEventEmitter)
            {
                if ((doe as DataMod.dtaEventEmitter).m_EmitRuleName == m_CurrentEmitterName)
                    main.m_MainForm.GenerateKeyPropEvoMenu("Vel Domain", m_ContextMenuVDKeyProp, doe.m_EvoList,keyProp, new EventHandler(KeyProp_Menu_AddEvo_VD_Click), new EventHandler(KeyProp_Menu_DelEvo_VD_Click));
                else
                    main.m_MainForm.GenerateKeyPropMenu("Vel Domain", m_ContextMenuVDKeyProp, keyProp);
            }
            e.Cancel = false;
        }
		private void m_ContextMenuADKeyProp_Opening(object sender, CancelEventArgs e)
		{
			DataMod.dtaEvent doe = main.m_MainForm.m_EffectWindow.m_CurrentEvent;
			if (doe == null) { e.Cancel = true; return; }

			DataMod.dtaPtxKeyframeProp keyProp = main.m_MainForm.GetSelectedKeyframePropFromDataGrid(m_DataGridAttrKeyframes);
			if (keyProp == null) { e.Cancel = true; return; }

			if (doe is DataMod.dtaEventEmitter)
			{
				if ((doe as DataMod.dtaEventEmitter).m_EmitRuleName == m_CurrentEmitterName)
					main.m_MainForm.GenerateKeyPropEvoMenu("Attr Domain", m_ContextMenuADKeyProp, doe.m_EvoList, keyProp, new EventHandler(KeyProp_Menu_AddEvo_AD_Click), new EventHandler(KeyProp_Menu_DelEvo_AD_Click));
				else
					main.m_MainForm.GenerateKeyPropMenu("Attr Domain", m_ContextMenuADKeyProp, keyProp);
			}
			e.Cancel = false;
		}

        private void m_DataGridEmitKeyframes_DoubleClick(object sender, EventArgs e)
        {
            ShowKeyframe("Emitter Domain", sender, e);
        }

        private void m_DataGridVelKeyframes_DoubleClick(object sender, EventArgs e)
        {
            ShowKeyframe("Velocity Domain", sender, e);
        }

		private void m_DataGridAttrKeyframes_DoubleClick(object sender, EventArgs e)
		{
			ShowKeyframe("Attractor Domain", sender, e);
		}

        private void m_DataGridKeyframeList_DoubleClick(object sender, EventArgs e)
        {
            ShowKeyframe("", sender, e);
        }
        public void m_Timeline_OnValueChanged(ptxSlider slider)
        {
            main.m_MainForm.m_EffectWindow.UpdateTimeline();
        }
        private void m_OneShot_Click(object sender, EventArgs e)
        {
            main.m_MainForm.m_EffectWindow.UpdateTimeline();
        }

        private void m_DataGridEmitKeyframes_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            main.m_MainForm.PropertyDataGrid_CurrentCellDirtyStateChanged(sender, e);
        }

        private void m_DataGridVelKeyframes_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            main.m_MainForm.PropertyDataGrid_CurrentCellDirtyStateChanged(sender, e);
        }

		private void m_DataGridAttrKeyframes_CurrentCellDirtyStateChanged(object sender, EventArgs e)
		{
			main.m_MainForm.PropertyDataGrid_CurrentCellDirtyStateChanged(sender, e);
		}

        private void m_DataGridKeyframeList_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            main.m_MainForm.PropertyDataGrid_CurrentCellDirtyStateChanged(sender, e);
        }

        private void m_DataGridEmitKeyframes_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            DataMod.guiPropEventData guievt = new DataMod.guiPropEventData();
            if(main.m_MainForm != null)
                main.m_MainForm.PropertyDataGrid_CellValueChanged(sender,guievt, e);
            if(guievt.InfoChanged)
            {
                if(main.m_MainForm != null)
                    main.m_MainForm.m_EffectWindow.ChangedEffect(main.m_MainForm.m_EffectWindow.m_CurrentEffectName);
            }
        }

        private void m_DataGridVelKeyframes_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            DataMod.guiPropEventData guievt = new DataMod.guiPropEventData();
            if(main.m_MainForm != null)
                main.m_MainForm.PropertyDataGrid_CellValueChanged(sender,guievt, e);
            if(guievt.InfoChanged)
            {
                if(main.m_MainForm != null)
                    main.m_MainForm.m_EffectWindow.ChangedEffect(main.m_MainForm.m_EffectWindow.m_CurrentEffectName);
            }
        }

		private void m_DataGridAttrKeyframes_CellValueChanged(object sender, DataGridViewCellEventArgs e)
		{
            DataMod.guiPropEventData guievt = new DataMod.guiPropEventData();
            if(main.m_MainForm != null)
                main.m_MainForm.PropertyDataGrid_CellValueChanged(sender,guievt, e);
            if(guievt.InfoChanged)
            {
                if(main.m_MainForm != null)
                    main.m_MainForm.m_EffectWindow.ChangedEffect(main.m_MainForm.m_EffectWindow.m_CurrentEffectName);
            }
		}

        private void m_DataGridKeyframeList_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            DataMod.guiPropEventData guievt = new DataMod.guiPropEventData();
            if(main.m_MainForm != null)
                main.m_MainForm.PropertyDataGrid_CellValueChanged(sender,guievt, e);
            if (guievt.InfoChanged)
            {
                if (main.m_MainForm != null)
                    main.m_MainForm.m_EffectWindow.ChangedEffect(main.m_MainForm.m_EffectWindow.m_CurrentEffectName);
            }
		}

		private void m_ButtonAttrEnableToggle_Click(object sender, EventArgs e)
		{
			DataMod.dtaStdEmitRule doe = DataMod.GetEmitRule(m_CurrentEmitterName) as DataMod.dtaStdEmitRule;
			DataCon.m_SDataCon.SendToggleAttractorDomain(m_CurrentEmitterName);
		}

        private void m_PanelSection_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
