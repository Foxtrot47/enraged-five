using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace ParticleEditor
{
    public partial class EffectOverrides : UserControl
    {
        private DataMod.dtaEffectOverridable m_CurrentOverridable;
        public event EventHandler ValueChanged;

        public EffectOverrides()
        {
            InitializeComponent();
        }

        [Browsable(true)]
        public bool UseInheritDirection
        {
            get {
				return false;
            }
            set {
            }
        }

        public DataMod.dtaEffectOverridable Overridable
        {
            get {
                return m_CurrentOverridable;
            }
            set {
                m_CurrentOverridable = value;
                if (m_CurrentOverridable == null)
                {
                    Enabled = false;
                }
                else
                {
                    Enabled = true;
                    UpdateUI();
                }
            }
        }

        private bool m_UIUpdating = false;
        void UpdateUI()
        {
            m_UIUpdating = true;

            m_DurationCheck.Checked = m_CurrentOverridable.m_Duration;
            m_DurationMinValue.SetValue = m_CurrentOverridable.m_DurationMin;
            m_DurationMaxValue.SetValue = m_CurrentOverridable.m_DurationMax;
            m_PlaybackSpeedCheck.Checked = m_CurrentOverridable.m_PlaybackSpeed;
            m_PlaybackSpeedMinValue.SetValue = m_CurrentOverridable.m_PlaybackSpeedMin;
            m_PlaybackSpeedMaxValue.SetValue = m_CurrentOverridable.m_PlaybackSpeedMax;
            m_ColorTintCheck.Checked = m_CurrentOverridable.m_ColorTint;
            m_ColorMinButton.BackColor = Color.FromArgb(
                (int)(m_CurrentOverridable.m_ColorTintMin >> 24) & 0xFF,
                (int)(m_CurrentOverridable.m_ColorTintMin >> 16) & 0xFF,
                (int)(m_CurrentOverridable.m_ColorTintMin >> 8) & 0xFF,
                (int)(m_CurrentOverridable.m_ColorTintMin >> 0) & 0xFF
                );
            m_ColorMaxButton.BackColor = Color.FromArgb(
                (int)(m_CurrentOverridable.m_ColorTintMax >> 24) & 0xFF,
                (int)(m_CurrentOverridable.m_ColorTintMax >> 16) & 0xFF,
                (int)(m_CurrentOverridable.m_ColorTintMax >> 8) & 0xFF,
                (int)(m_CurrentOverridable.m_ColorTintMax >> 0) & 0xFF
                );
            m_ZoomCheck.Checked = m_CurrentOverridable.m_Zoom;
            m_ZoomMinValue.SetValue = m_CurrentOverridable.m_ZoomMin;
            m_ZoomMaxValue.SetValue = m_CurrentOverridable.m_ZoomMax;
//            m_SizeScaleCheck.Checked = m_CurrentOverridable.m_SizeScale;
//            m_SizeXMinValue.SetValue = m_CurrentOverridable.m_SizeScaleXMin;
//            m_SizeYMinValue.SetValue = m_CurrentOverridable.m_SizeScaleYMin;
//            m_SizeZMinValue.SetValue = m_CurrentOverridable.m_SizeScaleZMin;
//            m_SizeXMaxValue.SetValue = m_CurrentOverridable.m_SizeScaleXMax;
//            m_SizeYMaxValue.SetValue = m_CurrentOverridable.m_SizeScaleYMax;
//            m_SizeZMaxValue.SetValue = m_CurrentOverridable.m_SizeScaleZMax;

//            m_InheritDirectionCheck.Checked = m_CurrentOverridable.m_InheritDirection;
//            m_FlipDirectionCheck.Enabled = m_CurrentOverridable.m_InheritDirection;
//            m_FlipDirectionCheck.Checked = m_CurrentOverridable.m_FlipDirection;

            m_UIUpdating = false;
        }

        private void m_DurationCheck_CheckedChanged(object sender, EventArgs e)
        {
            m_DurationMinValue.Enabled = m_DurationCheck.Checked;
            m_DurationMaxValue.Enabled = m_DurationCheck.Checked;

            m_CurrentOverridable.m_Duration = m_DurationCheck.Checked;

            OnValueChanged();
        }

        private void m_PlaybackSpeedCheck_CheckedChanged(object sender, EventArgs e)
        {
            m_PlaybackSpeedMinValue.Enabled = m_PlaybackSpeedCheck.Checked;
            m_PlaybackSpeedMaxValue.Enabled = m_PlaybackSpeedCheck.Checked;

            m_CurrentOverridable.m_PlaybackSpeed = m_PlaybackSpeedCheck.Checked;

            OnValueChanged();
        }

        private void m_ColorTintCheck_CheckedChanged(object sender, EventArgs e)
        {
            m_ColorMinButton.Enabled = m_ColorTintCheck.Checked;
            m_ColorMaxButton.Enabled = m_ColorTintCheck.Checked;

            m_CurrentOverridable.m_ColorTint = m_ColorTintCheck.Checked;

            OnValueChanged();
        }

        private void m_ZoomCheck_CheckedChanged(object sender, EventArgs e)
        {
            m_ZoomMinValue.Enabled = m_ZoomCheck.Checked;
            m_ZoomMaxValue.Enabled = m_ZoomCheck.Checked;

            m_CurrentOverridable.m_Zoom = m_ZoomCheck.Checked;

            OnValueChanged();
        }

/*        private void m_SizeScaleCheck_CheckedChanged(object sender, EventArgs e)
        {
            m_SizeXMinValue.Enabled = m_SizeScaleCheck.Checked;
            m_SizeYMinValue.Enabled = m_SizeScaleCheck.Checked;
            m_SizeZMinValue.Enabled = m_SizeScaleCheck.Checked;
            m_SizeXMaxValue.Enabled = m_SizeScaleCheck.Checked;
            m_SizeYMaxValue.Enabled = m_SizeScaleCheck.Checked;
            m_SizeZMaxValue.Enabled = m_SizeScaleCheck.Checked;

            m_CurrentOverridable.m_SizeScale = m_SizeScaleCheck.Checked;

            OnValueChanged();
        }
*/
        private void OnSliderValueChanged(ptxSlider slider)
        {

            m_CurrentOverridable.m_DurationMin = m_DurationMinValue.Value;
            m_CurrentOverridable.m_DurationMax = m_DurationMaxValue.Value;
            m_CurrentOverridable.m_PlaybackSpeedMin = m_PlaybackSpeedMinValue.Value;
            m_CurrentOverridable.m_PlaybackSpeedMax = m_PlaybackSpeedMaxValue.Value;
            m_CurrentOverridable.m_ZoomMin = m_ZoomMinValue.Value;
            m_CurrentOverridable.m_ZoomMax = m_ZoomMaxValue.Value;
/*            m_CurrentOverridable.m_SizeScaleXMin = m_SizeXMinValue.Value;
            m_CurrentOverridable.m_SizeScaleYMin = m_SizeYMinValue.Value;
            m_CurrentOverridable.m_SizeScaleZMin = m_SizeZMinValue.Value;
            m_CurrentOverridable.m_SizeScaleXMax = m_SizeXMaxValue.Value;
            m_CurrentOverridable.m_SizeScaleYMax = m_SizeYMaxValue.Value;
            m_CurrentOverridable.m_SizeScaleZMax = m_SizeZMaxValue.Value;
*/
            OnValueChanged();
        }

        private void OnValueChanged()
        {
            if (m_UIUpdating)
            {
                return;
            }

            if (ValueChanged != null)
            {
                ValueChanged.Invoke(this, EventArgs.Empty);
            }
        }

        private void m_ColorMinButton_Click(object sender, EventArgs e)
        {
            Sano.PersonalProjects.ColorPicker.Controls.ColorPickerDialog dlg = new Sano.PersonalProjects.ColorPicker.Controls.ColorPickerDialog();
            dlg.CurrentColor = m_ColorMinButton.BackColor;
            dlg.ColorChanged += delegate
            {
                m_ColorMinButton.BackColor = dlg.CurrentColor;
            };

            dlg.ShowDialog();

            m_CurrentOverridable.m_ColorTintMin = (
                ((uint)dlg.CurrentColor.A << 24) |
                ((uint)dlg.CurrentColor.R << 16) |
                ((uint)dlg.CurrentColor.G << 8) |
                ((uint)dlg.CurrentColor.B)
                );

            OnValueChanged();
        }

        private void m_ColorMaxButton_Click(object sender, EventArgs e)
        {
            Sano.PersonalProjects.ColorPicker.Controls.ColorPickerDialog dlg = new Sano.PersonalProjects.ColorPicker.Controls.ColorPickerDialog();
            dlg.CurrentColor = m_ColorMaxButton.BackColor;
            dlg.ColorChanged += delegate
            {
                m_ColorMaxButton.BackColor = dlg.CurrentColor;
            };

            dlg.ShowDialog();

            m_CurrentOverridable.m_ColorTintMax = (
                ((uint)dlg.CurrentColor.A << 24) |
                ((uint)dlg.CurrentColor.R << 16) |
                ((uint)dlg.CurrentColor.G << 8) |
                ((uint)dlg.CurrentColor.B)
                );

            OnValueChanged();
        }

/*        private void CheckChanged(object sender, EventArgs e)
        {
            if (m_UIUpdating)
            {
                return;
            }

            m_CurrentOverridable.m_InheritDirection = m_InheritDirectionCheck.Checked;

            m_FlipDirectionCheck.Enabled = m_CurrentOverridable.m_InheritDirection;
            m_CurrentOverridable.m_FlipDirection = m_FlipDirectionCheck.Checked;

            OnValueChanged();
        }
*/

    }
}
