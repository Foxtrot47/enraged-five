using System;
using System.Collections.Generic;
using System.Text;

namespace ParticleEditor
{
    public class ptxMessageIDs
    {

        public enum eCommunicationID
        {
            kInvalid =-1,
            kEffect_Profile = 0,
            kEffect_Evolution,
            kEffect_BlahBlah,
            kNum_IDs
        }
    };
}
