using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;


namespace ParticleEditor
{
    //This class will interface between the GUI and the data model
    public class DataCon
    {
        public static DataCon m_SDataCon = null;

        static int BUFFER_SIZE = 49152; // 48 * 1024 bytes buffer size

        //Perhaps put these in a shared include file?  [c++/c#]

		// fx lists
		static uint kMSG_REQUEST_LOAD_EFFECTLIST			= 0x00000010;
		static uint kMSG_REQUEST_SAVE_EFFECTLIST			= 0x00000011;

		// rule lists
		static uint kMSG_EFFECTLIST							= 0x00000020;
		static uint kMSG_EMITTERLIST						= 0x00000021;
		static uint kMSG_PTXTLIST							= 0x00000022;

		// effect rules
		static uint kMSG_EFFECTINFO							= 0x00000030;
		static uint kMSG_SEND_EFFECTINFO					= 0x00000031;
		static uint kMSG_LOADEFFECTRULE						= 0x00000032;
		static uint kMSG_BATCHLOADEFFECTRULE				= 0x00000033;
		static uint kMSG_SEND_CREATE_NEW_EFFECTRULE			= 0x00000034;
		static uint kMSG_REQUEST_REMOVE_EFFECTRULE			= 0x00000035;
		static uint kMSG_SEND_RENAME_EFFECTRULE_EVENT		= 0x00000036;

		// emitter rules
		static uint kMSG_EMITINFO							= 0x00000040;
		static uint kMSG_SEND_EMITINFO						= 0x00000041;
		static uint kMSG_LOAD_EMITRULE						= 0x00000042;
		static uint kMSG_SEND_CREATE_NEW_EMITRULE			= 0x00000043;
		static uint kMSG_REQUEST_REMOVE_EMITRULE			= 0x00000044;
		static uint kMSG_SEND_RENAME_EMITRULE_EVENT			= 0x00000045;

		// particle rules
		static uint kMSG_PTXRULEINFO						= 0x00000050;
		static uint kMSG_SEND_PTXRULE						= 0x00000051;
		static uint kMSG_LOAD_PTXRULE						= 0x00000052;
		static uint kMSG_SEND_CREATE_NEW_PTXRULE			= 0x00000053;
		static uint kMSG_REQUEST_REMOVE_PTXRULE				= 0x00000054;
		static uint kMSG_SEND_RENAME_PTXRULE_EVENT			= 0x00000055;
		static uint kMSG_REQUEST_CLONE_PTXRULE				= 0x00000056;

		// timeline events
		static uint kMSG_SEND_CREATE_NEW_TIMELINE_EVENT		= 0x00000060;
		static uint kMSG_SEND_DEL_TIMELINE_EVENT			= 0x00000061;
		static uint kMSG_SEND_RESORT_TIMELINE_EVENT			= 0x00000062;

		// domains
		static uint kMSG_DOMAIN_LIST						= 0x00000070;
		static uint kMSG_TOGGLE_ATTRACTOR_DOMAIN			= 0x00000071;

		// behaviours
		static uint kMSG_SEND_BEHAVIOR_LIST					= 0x00000080;
		static uint kMSG_REQUEST_ADD_BEHAVIOR_PTXRULE		= 0x00000081;
		static uint kMSG_REQUEST_DEL_BEHAVIOR_PTXRULE		= 0x00000082;
		static uint kMSG_REQUEST_VALIDATE_BEHAVIOR_PTXRULE	= 0x00000083;

		// keyframes
		static uint kMSG_REQUEST_UPDATE_KEYFRAME_UI			= 0x00000090;
		static uint kMSG_REQUEST_KEYFRAME_DATA				= 0x00000091;
		static uint kMSG_SET_KEYFRAME_DATA					= 0x00000092;
		static uint kMSG_REQUEST_KEYFRAME_EXISTS			= 0x00000093;

		// evolutions
		static uint kMSG_REQUEST_EVOLUTION_CREATE			= 0x000000A0;
		static uint kMSG_REQUEST_EVOLUTION_DELETE			= 0x000000A1;
		static uint kMSG_PROPERTY_ADD_EVOLUTION				= 0x000000A2;
		static uint kMSG_PROPERTY_DEL_EVOLUTION				= 0x000000A3;
		static uint kMSG_SAVE_EVOLUTION						= 0x000000A4;
		static uint kMSG_LOAD_EVOLUTION						= 0x000000A5;

		// graphics
		static uint kMSG_REQUEST_CLIP_REGION_DATA			= 0x000000B0;
		static uint kMSG_BLENDSET_LIST						= 0x000000B1;
		static uint kMSG_SHADER_LIST						= 0x000000B2;
		static uint kMSG_CHANGE_SHADER_EVENT				= 0x000000B3;

		// models
		static uint kMSG_PTXRULE_ADD_DEL_MODEL_EVENT		= 0x000000C0;

		// saving
		static uint kMSG_SAVE_ALL_COMPLETE					= 0x000000D0;
		static uint kMSG_SAVE_ALL_PROGRESS					= 0x000000D1;
		static uint kMSG_SEND_REQUEST_SAVE					= 0x000000D2;
		static uint kMSG_REQUEST_SAVE_ALL					= 0x000000D3;
		static uint kMSG_CANCEL_SAVE_ALL					= 0x000000D4;

		// misc
		static uint kMSG_SYSINFO							= 0x000000E0;
		static uint kMSG_REQUEST_DRAW_LISTS					= 0x000000E1;
		static uint kMSG_SEND_GAME_SPECIFIC_DATA			= 0x000000E2;
		static uint kMSG_SEND_FREEZESCALE					= 0x000000E3;

		// profile
		static uint kMSG_REQUEST_PROFILING_GLOBAL			= 0x000000F0;
		static uint kMSG_REQUEST_PROFILING_EFFECT			= 0x000000F1;
		static uint kMSG_REQUEST_PROFILING_EMITTER			= 0x000000F2;
		static uint kMSG_REQUEST_PROFILING_PTX				= 0x000000F3;

		// debug
		static uint kMSG_SEND_PLAY_CONTROL_INFO				= 0x00000100;
		static uint kMSG_SEND_STARTEFFECT					= 0x00000101;
		static uint kMSG_SEND_SHOW_EFFECT_NAMES				= 0x00000102;
		static uint kMSG_SEND_RESET_EVENT					= 0x00000103;

		// custom
        static uint kCUSTOM_MSG_HANDLER_START				= 0x10000000;



        ArrayList m_ComObjs = new ArrayList();
        main m_GUI = null;
        tcpComClient m_Connection;
        public DataMod m_DataModel = new DataMod();
        static uint sm_CustomMsgId = kCUSTOM_MSG_HANDLER_START;    
            
        //Messages
        tcpComObj m_SysInfoObj = new tcpComObj(kMSG_SYSINFO);
        tcpComObj m_EffectListObj = new tcpComObj(kMSG_EFFECTLIST);
        tcpComObj m_EmitterListObj = new tcpComObj(kMSG_EMITTERLIST);
        tcpComObj m_PtxListObj = new tcpComObj(kMSG_PTXTLIST);
        tcpComObj m_DomainTypeListObj = new tcpComObj(kMSG_DOMAIN_LIST);
        tcpComObj m_ShaderListObj = new tcpComObj(kMSG_SHADER_LIST);
        tcpComObj m_EffectInfoObj = new tcpComObj(kMSG_EFFECTINFO);
        tcpComObj m_EmitInfoObj = new tcpComObj(kMSG_EMITINFO);
        tcpComObj m_PtxRuleInfoObj = new tcpComObj(kMSG_PTXRULEINFO);
        tcpComObj m_BlendSetListObj = new tcpComObj(kMSG_BLENDSET_LIST);
        tcpComObj m_ProfileGlobalObj = new tcpComObj(kMSG_REQUEST_PROFILING_GLOBAL);
        tcpComObj m_ProfileEffectObj = new tcpComObj(kMSG_REQUEST_PROFILING_EFFECT);
        tcpComObj m_ProfileEmitterObj = new tcpComObj(kMSG_REQUEST_PROFILING_EMITTER);
        tcpComObj m_ProfilePtxObj = new tcpComObj(kMSG_REQUEST_PROFILING_PTX);
        tcpComObj m_GameSpecificDataObj = new tcpComObj(kMSG_SEND_GAME_SPECIFIC_DATA);
        tcpComObj m_BehaviorListObj= new tcpComObj(kMSG_SEND_BEHAVIOR_LIST);
        tcpComObj m_UpdateKeyframeUIObj = new tcpComObj(kMSG_REQUEST_UPDATE_KEYFRAME_UI);
        tcpComObj m_DrawListsObj = new tcpComObj(kMSG_REQUEST_DRAW_LISTS);
        tcpComObj m_SaveAllCompleteObj = new tcpComObj(kMSG_SAVE_ALL_COMPLETE);
        tcpComObj m_SaveAllProgressObj = new tcpComObj(kMSG_SAVE_ALL_PROGRESS);
		tcpComObj m_SaveCancelProgressObj = new tcpComObj(kMSG_CANCEL_SAVE_ALL);
		tcpComObj m_ClipRegionDataObj = new tcpComObj(kMSG_REQUEST_CLIP_REGION_DATA);


        public DataCon(main gui, tcpComClient connection)
        {
            m_SDataCon = this;
            m_GUI = gui;
            m_Connection = connection;

            //Processor
            m_Connection.OnCompleteBuffer += new tcpComClient.tcpComClientBufferEventDelegate(this.OnCompleteBuffer);

            //Set message callback
            m_SysInfoObj.OnRecieveData += new tcpComObj.ComObjEventDelegate(this.OnReceiveSysInfo);
            m_EffectListObj.OnRecieveData += new tcpComObj.ComObjEventDelegate(this.OnReceiveEffectList);
            m_EmitterListObj.OnRecieveData += new tcpComObj.ComObjEventDelegate(this.OnReceiveEmitterList);
            m_PtxListObj.OnRecieveData += new tcpComObj.ComObjEventDelegate(this.OnReceivePtxList);
            m_ShaderListObj.OnRecieveData += new tcpComObj.ComObjEventDelegate(this.OnReceiveShaderList);
            m_EffectInfoObj.OnRecieveData += new tcpComObj.ComObjEventDelegate(this.OnReceiveEffectInfo);
            m_EmitInfoObj.OnRecieveData += new tcpComObj.ComObjEventDelegate(this.OnReceiveEmitInfo);
            m_PtxRuleInfoObj.OnRecieveData += new tcpComObj.ComObjEventDelegate(this.OnReceivePtxRuleInfo);
            m_DomainTypeListObj.OnRecieveData += new tcpComObj.ComObjEventDelegate(this.OnReceiveDomainTypeList);
            m_BlendSetListObj.OnRecieveData += new tcpComObj.ComObjEventDelegate(this.OnReceiveBlendSetList);
            m_ProfileGlobalObj.OnRecieveData += new tcpComObj.ComObjEventDelegate(this.OnReceiveProfileGlobal);
            m_ProfileEffectObj.OnRecieveData += new tcpComObj.ComObjEventDelegate(this.OnReceiveProfileEffect);
            m_ProfileEmitterObj.OnRecieveData += new tcpComObj.ComObjEventDelegate(this.OnReceiveProfileEmitter);
            m_ProfilePtxObj.OnRecieveData += new tcpComObj.ComObjEventDelegate(this.OnReceiveProfilePtx);
            m_GameSpecificDataObj.OnRecieveData += new tcpComObj.ComObjEventDelegate(this.OnReceiveGameSpecificData);
            m_BehaviorListObj.OnRecieveData += new tcpComObj.ComObjEventDelegate(this.OnReceiveBehaviorList);
            m_UpdateKeyframeUIObj.OnRecieveData += new tcpComObj.ComObjEventDelegate(this.OnReceiveUpdateKeyframeUIData);
            m_DrawListsObj.OnRecieveData += new tcpComObj.ComObjEventDelegate(this.OnReceiveDrawLists);
            m_SaveAllCompleteObj.OnRecieveData += new tcpComObj.ComObjEventDelegate(this.OnFinishSaveAll);
            m_SaveAllProgressObj.OnRecieveData += new tcpComObj.ComObjEventDelegate(this.OnUpdateSaveProgress);
            m_SaveCancelProgressObj.OnRecieveData += new tcpComObj.ComObjEventDelegate(this.OnCancelSaveAll);
			m_ClipRegionDataObj.OnRecieveData += new tcpComObj.ComObjEventDelegate(this.OnRecieveClipRegionData);
            //Register Messages
            AddComObj(m_SysInfoObj);
            AddComObj(m_EffectListObj);
            AddComObj(m_EmitterListObj);
            AddComObj(m_PtxListObj);
            AddComObj(m_ShaderListObj);
            AddComObj(m_DomainTypeListObj);
            AddComObj(m_EffectInfoObj);
            AddComObj(m_EmitInfoObj);
            AddComObj(m_PtxRuleInfoObj);
            AddComObj(m_BlendSetListObj);
            AddComObj(m_ProfileGlobalObj);
            AddComObj(m_ProfileEffectObj);
            AddComObj(m_ProfileEmitterObj);
            AddComObj(m_ProfilePtxObj);
            AddComObj(m_GameSpecificDataObj);
            AddComObj(m_BehaviorListObj);
            AddComObj(m_UpdateKeyframeUIObj);
            AddComObj(m_DrawListsObj);
            AddComObj(m_SaveAllCompleteObj);
            AddComObj(m_SaveAllProgressObj);
            AddComObj(m_SaveCancelProgressObj);
			AddComObj(m_ClipRegionDataObj);
        }
        
        public void AddComObj(tcpComObj obj)
        {
            m_ComObjs.Add(obj);
        }

        void OnCompleteBuffer(tcpByteBuffer buff)
        {
            buff.SetReadPos(4);
            int id = (int)buff.Read_u32();
            //Ignore Ping
            if (id == 0)
                return; 
            //Here we figure out which message id it is destined to 
            foreach (tcpComObj obj in m_ComObjs)
            {
                if (obj.m_MessageID == id)
                {
                    obj.RecieveData(buff);
                    return;
                }
            }
        }

        public bool IsConnected()
        {
            return m_Connection.IsConnected(); 
        }
        void Tokenize(tcpByteBuffer msg,ArrayList data)
        {
            //here we create an array of data
        }

        #region Receive
        private void OnReceiveSysInfo(tcpComObj obj)
        {
            tcpByteBuffer msg = obj.m_Data;
            msg.SetReadPos(8);
            //Read Version
            float version = msg.Read_float();
            if (version != DataMod.m_SDataMod.m_InterfaceVersion)
            {
                m_GUI.Disconnect();
                String message = "RMPTFX System Version (" + version.ToString("0.000") + ") does not match Interface Version (" + DataMod.m_SDataMod.m_InterfaceVersion.ToString("0.000") + ").\nVersions must be Synchronized!\n The Interface will now Disconnect.";
                System.Windows.Forms.MessageBox.Show(message, "RMPTFX Error: Version Conflict", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void OnReceiveDrawLists(tcpComObj obj)
        {
            tcpByteBuffer msg = obj.m_Data;
            msg.SetReadPos(8);

            UInt32 cnt = msg.Read_u32();
            m_DataModel.m_DrawListNames.Clear();
            for(int i=0;i<cnt;i++)
            {
                String name = msg.Read_const_char();
                m_DataModel.m_DrawListNames.Add(name);
            }
            m_GUI.UpdateDrawListNames();
        }

		private void OnRecieveClipRegionData(tcpComObj obj)
        {
            tcpByteBuffer msg = obj.m_Data;
            msg.SetReadPos(8);

            UInt32 cnt = msg.Read_u32();
			PtxClipRegions.m_clipRegionData.Clear();
            for(int i=0;i<cnt;i++)
            {
				PtxClipRegionData clipRegionData = new PtxClipRegionData();
				clipRegionData.m_textureName = msg.Read_const_char();
				clipRegionData.m_numTexCols = msg.Read_s32();
				clipRegionData.m_numTexRows = msg.Read_s32();

				PtxClipRegions.m_clipRegionData.Add(clipRegionData);
            }
        }

        private void OnReceiveGameSpecificData(tcpComObj obj)
        {
            tcpByteBuffer msg = obj.m_Data;
            msg.SetReadPos(8);
            m_DataModel.m_GameSpecificDataEffectRuleBoolList.Clear();
            
            //Read in the EffectRule bool strings
            int cnt = msg.Read_s32();
            for(int i=0;i<cnt;i++)
            {
                String name = msg.Read_const_char();
                m_DataModel.m_GameSpecificDataEffectRuleBoolList.Add(name);
            }

			// read in the data volume type names
			cnt = msg.Read_s32();
			m_DataModel.m_DataVolumeTypeNames.Clear();
			for (int i = 0; i < cnt; i++)
			{
				String name = msg.Read_const_char();
				m_DataModel.m_DataVolumeTypeNames.Add(name);
			}
			m_GUI.UpdateDataVolumeTypeNames();
        }

        private void OnReceiveBehaviorList(tcpComObj obj)
        {
            //ArrayList clist = m_DataModel.m_EffectRuleList.Clone() as ArrayList;
            m_DataModel.m_UpdateBehaviorDefList.Clear();
            m_DataModel.m_DrawTypeList.Clear();
            tcpByteBuffer msg = obj.m_Data;
            msg.SetReadPos(8);

            //Read draw types
            uint cnt = msg.Read_u32();
            for (uint i = 0; i < cnt; i++)
            {
                String drawTypeString = msg.Read_const_char();
                m_DataModel.m_DrawTypeList.Add(drawTypeString);
            }
                        
            //Read behavior definitions
            cnt = msg.Read_u32();
            for (uint i = 0; i < cnt; i++ )
            {
                DataMod.dtaBehaviorDefinition bobj = new DataMod.dtaBehaviorDefinition();
                bobj.LoadDefinitionFromByteBuffer(msg);
                //bobj.LoadFromByteBuffer(msg);
                //This should check for dups first
                m_DataModel.m_UpdateBehaviorDefList.Add(bobj);
            }
            m_GUI.BuildDrawTypeList();
        }

        private void OnReceiveEffectList(tcpComObj obj)
        {
            //ArrayList clist = m_DataModel.m_EffectRuleList.Clone() as ArrayList;
            m_DataModel.m_EffectRuleList.Clear();
            tcpByteBuffer msg = obj.m_Data;
            msg.SetReadPos(8);
            uint cnt = msg.Read_u32();
            for (uint i = 0; i < cnt;i++ )
            {
                //Gather first pass info on the effectrule
                String s = msg.Read_const_char();
                int hash = (int)msg.Read_u32();
                //byte type = msg.Read_u8();
                DataMod.dtaObject dobj = m_DataModel.GetObject(m_DataModel.m_EffectRuleList,s);
                if (dobj == null)
                {
                    dobj = new DataMod.dtaObject();
                    m_DataModel.m_EffectRuleList.Add(dobj);
                }
                dobj.m_Name = s;
                dobj.m_Hash = hash;
                //dobj.m_Type = type;
                dobj.m_ClassName = "ptxEffectRule";
                dobj.m_RefCount++;
                
                //Remap the DataObject
                DataMod.dtaObject dob = m_DataModel.GetObject(m_DataModel.m_EffectRuleList, s);
                if(dob != null)
                    dobj.m_Object = dob.m_Object;
            }
            m_GUI.BuildEffectList();
        }
        
        private void OnReceiveEmitterList(tcpComObj obj)
        {
            //ArrayList clist = m_DataModel.m_EmitRuleList.Clone() as ArrayList;
            m_DataModel.m_EmitRuleList.Clear();
            tcpByteBuffer msg = obj.m_Data;
            msg.SetReadPos(8);
            uint cnt = msg.Read_u32();
            for (uint i = 0; i < cnt; i++)
            {
                //Gather first pass info on the emitrule
                String s = msg.Read_const_char();
                int hash = (int)msg.Read_u32();
                //byte type = msg.Read_u8();
                DataMod.dtaObject dobj = m_DataModel.GetObject(m_DataModel.m_EmitRuleList, s);
                if (dobj == null)
                {
                    dobj = new DataMod.dtaObject();
                    m_DataModel.m_EmitRuleList.Add(dobj);
                }
                dobj.m_Name = s;
                dobj.m_Hash = hash;
                //dobj.m_Type = type;
				dobj.m_ClassName = "ptxEmitterRule";
                dobj.m_RefCount++;

                //Remap the DataObject
                DataMod.dtaObject dob = m_DataModel.GetObject(m_DataModel.m_EmitRuleList, s);
                if (dob != null)
                    dobj.m_Object = dob.m_Object;

            }
            m_GUI.BuildEmitterList();
        }

        private void OnReceivePtxList(tcpComObj obj)
        {
            //ArrayList clist = m_DataModel.m_PtxRuleList.Clone() as ArrayList;
            m_DataModel.m_PtxRuleList.Clear();
            tcpByteBuffer msg = obj.m_Data;
            msg.SetReadPos(8);
            uint cnt = msg.Read_u32();
            for (uint i = 0; i < cnt; i++)
            {
                //Gather first pass info on the ptxrule
                String s = msg.Read_const_char();
                int hash = (int)msg.Read_u32();
                DataMod.dtaObject dobj = m_DataModel.GetObject(m_DataModel.m_PtxRuleList, s);
                if (dobj == null)
                {
                    dobj = new DataMod.dtaObject();
                    m_DataModel.m_PtxRuleList.Add(dobj);
                }
                dobj.m_Name = s;
                dobj.m_Hash = hash;
				dobj.m_ClassName = "ptxparticlerule";
                //dobj.m_Type = 2;
                dobj.m_RefCount++;

                //Remap the DataObject
                DataMod.dtaObject dob = m_DataModel.GetObject(m_DataModel.m_PtxRuleList, s);
                if (dob != null)
                    dobj.m_Object = dob.m_Object;

            }
            m_GUI.BuildPtxList();
        }

        private void OnReceiveDomainTypeList(tcpComObj obj)
        {
            DataMod.m_SDataMod.m_EmitterDomainTypes.Clear();
			DataMod.m_SDataMod.m_VelocityDomainTypes.Clear();
			DataMod.m_SDataMod.m_AttractorDomainTypes.Clear();
            
            tcpByteBuffer msg = obj.m_Data;
            msg.SetReadPos(8);
            uint cnt = msg.Read_u32();
            for (uint i = 0; i < cnt; i++)
            {
                string name = msg.Read_const_char();
                byte typefilter = msg.Read_u8();
                DataMod.m_SDataMod.m_DomainTypes.Add(name);

				if ((typefilter & 0x1) > 0)
				{
					DataMod.m_SDataMod.m_EmitterDomainTypes.Add(name);
				}
				if ((typefilter & 0x2) > 0)
				{
					DataMod.m_SDataMod.m_VelocityDomainTypes.Add(name);
				}
				if ((typefilter & 0x4) > 0)
				{
					DataMod.m_SDataMod.m_AttractorDomainTypes.Add(name);
				}
            }
            m_GUI.BuildDomainTypeList();
        }

        private void OnReceiveBlendSetList(tcpComObj obj)
        {
            DataMod.m_SDataMod.m_BlendSetList.Clear();
            tcpByteBuffer msg = obj.m_Data;
            msg.SetReadPos(8);
            uint cnt = msg.Read_u8();
            for (uint i = 0; i < cnt; i++)
            {
                DataMod.m_SDataMod.m_BlendSetList.Add(msg.Read_const_char());
            }
            m_GUI.BuildBlendSetList();
        }

        private void OnReceiveShaderList(tcpComObj obj)
        {
            DataMod.m_SDataMod.m_ShaderList.Clear();
            tcpByteBuffer msg = obj.m_Data;
            msg.SetReadPos(8);
            uint cnt = msg.Read_u8();
            for (uint i = 0; i < cnt; i++)
            {
                DataMod.m_SDataMod.m_ShaderList.Add(msg.Read_const_char());
            }
            m_GUI.BuildShaderList();
        }

        private void OnReceiveEffectInfo(tcpComObj obj)
        {
            tcpByteBuffer msg = obj.m_Data;
            msg.SetReadPos(8);
            String name = msg.Read_const_char();

            //Find the dtaObject within the effect list
            DataMod.dtaObject dobj = m_DataModel.GetObject(m_DataModel.m_EffectRuleList,name);

            //If not exist then ignore
            if (dobj == null)
                return;

            //Fill out the data
            if (dobj.m_Object == null)
            {
                dobj.m_Object = new DataMod.dtaEffectRuleStd();
            }

            DataMod.dtaEffectRuleStd dta = dobj.m_Object as DataMod.dtaEffectRuleStd;
            dta.LoadFromByteBuffer(msg);
            m_GUI.DisplayEffect(name);
        }

        private void OnReceiveEmitInfo(tcpComObj obj)
        {
            tcpByteBuffer msg = obj.m_Data;
            msg.SetReadPos(8);
            String name = msg.Read_const_char();

            //Find the dtaObject within the emitter list
            DataMod.dtaObject dobj = m_DataModel.GetObject(m_DataModel.m_EmitRuleList, name);

            //If not exist then ignore
            if (dobj == null)
                return;

            //Fill out the data
            if (dobj.m_Object == null)
            {
                dobj.m_Object = new DataMod.dtaStdEmitRule();
            }

            DataMod.dtaStdEmitRule dta = dobj.m_Object as DataMod.dtaStdEmitRule;
            dta.LoadFromByteBuffer(msg);
            m_GUI.DisplayEmitter(name);
        }

        private void OnReceivePtxRuleInfo(tcpComObj obj)
        {
            tcpByteBuffer msg = obj.m_Data;
            msg.SetReadPos(8);
            String name = msg.Read_const_char();
            String classname = msg.Read_const_char();

            //Find the dtaObject within the ptxrule list
            DataMod.dtaObject dobj = m_DataModel.GetObject(m_DataModel.m_PtxRuleList, name);

            //If not exist then ignore
            if (dobj == null)
                return;

            //Create the proper object
            if (dobj.m_Object == null)
            {
//                if (classname == "ptxsprite")
//                    dobj.m_Object = new DataMod.dtaPtxSprite();
//                if (classname == "ptxmodel")
//                    dobj.m_Object = new DataMod.dtaPtxModel();
                if (classname == "ptxparticlerule")
                    dobj.m_Object = new DataMod.dtaPtxRuleBehavior();
            }

            DataMod.dtaPtxRule dor = dobj.m_Object as DataMod.dtaPtxRule;
            if (dor == null)
                return;
            
            dor.LoadFromByteBuffer(msg);
            m_GUI.DisplayPtxRule(name);
        }

        private void OnReceiveProfileGlobal(tcpComObj obj)
        {
            tcpByteBuffer msg = obj.m_Data;
            msg.SetReadPos(8);
            DataMod.m_SDataMod.m_ProfileGlobal.LoadFromByteBuffer(msg);
            m_GUI.UpdateProfile();
        }
        private void OnReceiveProfileEffect(tcpComObj obj)
        {
            tcpByteBuffer msg = obj.m_Data;
            msg.SetReadPos(8);
            DataMod.m_SDataMod.m_ProfileEffect.LoadFromByteBuffer(msg);
            m_GUI.UpdateProfile();
        }
        private void OnReceiveProfileEmitter(tcpComObj obj)
        {
            tcpByteBuffer msg = obj.m_Data;
            msg.SetReadPos(8);
            DataMod.m_SDataMod.m_ProfileEmitter.LoadFromByteBuffer(msg);
            m_GUI.UpdateProfile();
        }
        private void OnReceiveProfilePtx(tcpComObj obj)
        {
            tcpByteBuffer msg = obj.m_Data;
            msg.SetReadPos(8);
            DataMod.m_SDataMod.m_ProfilePtx.LoadFromByteBuffer(msg);
            m_GUI.UpdateProfile();
            m_GUI.UpdateProfilePtx();
        }
        private void OnReceiveUpdateKeyframeUIData(tcpComObj obj)
        {
            tcpByteBuffer msg = obj.m_Data;
            KeyframeEditorFormBase.UpdateKeyframeUI(msg);
        }
        private void OnUpdateSaveProgress(tcpComObj obj)
        {
            tcpByteBuffer msg = obj.m_Data;
            msg.SetReadPos(8);
            m_GUI.m_SavingPopup.LoadFromByteBuffer(msg);
            m_GUI.UpdateProfile();
        }
        private void OnFinishSaveAll(tcpComObj obj)
        {
            m_GUI.m_SavingPopup.Finish(); 
        }
        private void OnCancelSaveAll(tcpComObj obj)
        {
            m_GUI.m_SavingPopup.Stop();
        }
        #endregion

        #region Request
        public void RequestLists()
        {
            RequestEffectList();
            RequestEmitterList();
            RequestPtxList();
            RequestDomainTypeList();
            RequestBlendSetList();
            RequestShaderList();
            RequestGameSpecificData();
            RequestBehaviorList();
            RequestDrawLists();
			RequestClipRegionData();
        }

        public void RequestDrawLists()
        {
            tcpByteBuffer buffer = new tcpByteBuffer(BUFFER_SIZE);
            buffer.BeginTCPBuffer();
            buffer.Write_u32(kMSG_REQUEST_DRAW_LISTS);
            buffer.EndTCPBuffer();
            m_Connection.SendByteBuffer(buffer);
        }

		public void RequestClipRegionData()
		{
			tcpByteBuffer buffer = new tcpByteBuffer(BUFFER_SIZE);
			buffer.BeginTCPBuffer();
			buffer.Write_u32(kMSG_REQUEST_CLIP_REGION_DATA);
			buffer.EndTCPBuffer();
			m_Connection.SendByteBuffer(buffer);
		}
        
        public void RequestEffectList()
        {
            tcpByteBuffer buffer = new tcpByteBuffer(BUFFER_SIZE);
            buffer.BeginTCPBuffer();
            buffer.Write_u32(kMSG_EFFECTLIST);
            buffer.EndTCPBuffer();
            m_Connection.SendByteBuffer(buffer);
        }

        public void RequestEmitterList()
        {
            tcpByteBuffer buffer = new tcpByteBuffer(BUFFER_SIZE);
            buffer.BeginTCPBuffer();
            buffer.Write_u32(kMSG_EMITTERLIST);
            buffer.EndTCPBuffer();
            m_Connection.SendByteBuffer(buffer);
        }

        public void RequestPtxList()
        {
            tcpByteBuffer buffer = new tcpByteBuffer(BUFFER_SIZE);
            buffer.BeginTCPBuffer();
            buffer.Write_u32(kMSG_PTXTLIST);
            buffer.EndTCPBuffer();
            m_Connection.SendByteBuffer(buffer);
        }

        public void RequestDomainTypeList()
        {
            tcpByteBuffer buffer = new tcpByteBuffer(BUFFER_SIZE);
            buffer.BeginTCPBuffer();
            buffer.Write_u32(kMSG_DOMAIN_LIST);
            buffer.EndTCPBuffer();
            m_Connection.SendByteBuffer(buffer);
        }

        public void RequestBlendSetList()
        {
            tcpByteBuffer buffer = new tcpByteBuffer(BUFFER_SIZE);
            buffer.BeginTCPBuffer();
            buffer.Write_u32(kMSG_BLENDSET_LIST);
            buffer.EndTCPBuffer();
            m_Connection.SendByteBuffer(buffer);
        }

        public void RequestShaderList()
        {
            tcpByteBuffer buffer = new tcpByteBuffer(BUFFER_SIZE);
            buffer.BeginTCPBuffer();
            buffer.Write_u32(kMSG_SHADER_LIST);
            buffer.EndTCPBuffer();
            m_Connection.SendByteBuffer(buffer);
        }

        public void RequestGameSpecificData()
        {
            tcpByteBuffer buffer = new tcpByteBuffer(BUFFER_SIZE);
            buffer.BeginTCPBuffer();
            buffer.Write_u32(kMSG_SEND_GAME_SPECIFIC_DATA);
            buffer.EndTCPBuffer();
            m_Connection.SendByteBuffer(buffer);
        }
        
        public void RequestBehaviorList()
        {
            tcpByteBuffer buffer = new tcpByteBuffer(BUFFER_SIZE);
            buffer.BeginTCPBuffer();
            buffer.Write_u32(kMSG_SEND_BEHAVIOR_LIST);
            buffer.EndTCPBuffer();
            m_Connection.SendByteBuffer(buffer);
        }

        public void RequestEffectInfo(String name)
        {
            if (name == "") return;
            tcpByteBuffer buffer = new tcpByteBuffer(BUFFER_SIZE);
            buffer.BeginTCPBuffer();
            buffer.Write_u32(kMSG_EFFECTINFO);
            buffer.Write_const_char(name);
            buffer.EndTCPBuffer();
            m_Connection.SendByteBuffer(buffer);
        }
        
        public void RequestEmitterInfo(String name)
        {
            if (name == "") return;
            tcpByteBuffer buffer = new tcpByteBuffer(BUFFER_SIZE);
            buffer.BeginTCPBuffer();
            buffer.Write_u32(kMSG_EMITINFO);
            buffer.Write_const_char(name);
            buffer.EndTCPBuffer();
            m_Connection.SendByteBuffer(buffer);
        }
        
        public void RequestPtxRuleInfo(String name)
        {
            if (name == "") return;
            tcpByteBuffer buffer = new tcpByteBuffer(BUFFER_SIZE);
            buffer.BeginTCPBuffer();
            buffer.Write_u32(kMSG_PTXRULEINFO);
            buffer.Write_const_char(name);
            buffer.EndTCPBuffer();
            m_Connection.SendByteBuffer(buffer);
        }

        public void RequestLoadEffectRule(String path,String name)
        {
            tcpByteBuffer buffer = new tcpByteBuffer(BUFFER_SIZE);
            buffer.BeginTCPBuffer();
            buffer.Write_u32(kMSG_LOADEFFECTRULE);
            buffer.Write_const_char(name);
            buffer.Write_const_char(path);
            buffer.EndTCPBuffer();
            m_Connection.SendByteBuffer(buffer);

        }
        public void RequestBatchLoadEffectRule(String path, List<String> name)
        {
            tcpByteBuffer buffer = new tcpByteBuffer(BUFFER_SIZE);
            buffer.BeginTCPBuffer();
            buffer.Write_u32(kMSG_BATCHLOADEFFECTRULE);
            buffer.Write_const_char(path);
            buffer.Write_s32(name.Count);
            for (int i = 0; i < name.Count;i++ )
                buffer.Write_const_char(name[i]);
            
            buffer.EndTCPBuffer();
            m_Connection.SendByteBuffer(buffer);

        }

        public void RequestLoadEmitRule(String path, String name)
        {
            tcpByteBuffer buffer = new tcpByteBuffer(BUFFER_SIZE);
            buffer.BeginTCPBuffer();
            buffer.Write_u32(kMSG_LOAD_EMITRULE);
            buffer.Write_const_char(name);
            buffer.Write_const_char(path);
            buffer.EndTCPBuffer();
            m_Connection.SendByteBuffer(buffer);
        }

        public void RequestLoadPtxRule(String path, String name)
        {
            tcpByteBuffer buffer = new tcpByteBuffer(BUFFER_SIZE);
            buffer.BeginTCPBuffer();
            buffer.Write_u32(kMSG_LOAD_PTXRULE);
            buffer.Write_const_char(name);
            buffer.Write_const_char(path);
            buffer.EndTCPBuffer();
            m_Connection.SendByteBuffer(buffer);
        }

        public delegate void RequestResponse(tcpByteBuffer data);

        class Responder
        {
            public RequestResponse ResponseFn;

            public Responder(RequestResponse fn)
            {
                ResponseFn = fn;
            }

            public void OnReceiveResponse(tcpComObj obj)
            {
                DataCon.m_SDataCon.m_ComObjs.Remove(obj);
                ResponseFn(obj.m_Data);    
            }
        };

		public void RequestKeyframeData(string ruleName, int ruleType, uint propertyId, int eventIdx, int evoIdx, RequestResponse resp)
        {

            tcpByteBuffer buffer = new tcpByteBuffer(BUFFER_SIZE);
            buffer.BeginTCPBuffer();
            buffer.Write_u32(kMSG_REQUEST_KEYFRAME_DATA); // Message type
			buffer.Write_const_char(ruleName);
			buffer.Write_s32(ruleType);
			buffer.Write_u32(propertyId);
			buffer.Write_s32(eventIdx);
			buffer.Write_s32(evoIdx);
            buffer.Write_u32(AddResponder(resp)); // reply-to address
            
            buffer.EndTCPBuffer();
            m_Connection.SendByteBuffer(buffer);
        }

        public uint AddResponder(RequestResponse resp)
        {
            // set up a message handler for the reply
            uint newMsgId = sm_CustomMsgId++;

            tcpComObj com = new tcpComObj(newMsgId);
            Responder respWrapper = new Responder(resp);
            com.OnRecieveData += respWrapper.OnReceiveResponse;
            DataCon.m_SDataCon.AddComObj(com);

            return newMsgId;
        }

        public void RequestRemoveEffect(string name, RequestResponse resp)
        {
            tcpByteBuffer buffer = new tcpByteBuffer(BUFFER_SIZE);
            buffer.BeginTCPBuffer();
            buffer.Write_u32(kMSG_REQUEST_REMOVE_EFFECTRULE);
            buffer.Write_u32(AddResponder(resp)); // reply-to function
            buffer.Write_const_char(name);
            buffer.EndTCPBuffer();
            m_Connection.SendByteBuffer(buffer);
        }

		public void RequestKeyframeExists(string ruleName, int ruleType, uint propertyId, int eventIdx, int evoIdx, RequestResponse resp)
        {
            tcpByteBuffer buffer = new tcpByteBuffer(BUFFER_SIZE);
            buffer.BeginTCPBuffer();
			buffer.Write_u32(kMSG_REQUEST_KEYFRAME_EXISTS); // Message type

			buffer.Write_const_char(ruleName);
			buffer.Write_s32(ruleType);
			buffer.Write_u32(propertyId);
			buffer.Write_s32(eventIdx);
			buffer.Write_s32(evoIdx);
            
			buffer.Write_u32(AddResponder(resp)); // reply-to address

            buffer.EndTCPBuffer();
            m_Connection.SendByteBuffer(buffer);
        }

        public void RequestRemoveEmitter(string name, RequestResponse resp)
        {
            tcpByteBuffer buffer = new tcpByteBuffer(BUFFER_SIZE);
            buffer.BeginTCPBuffer();
            buffer.Write_u32(kMSG_REQUEST_REMOVE_EMITRULE);
            buffer.Write_u32(AddResponder(resp)); // reply-to function
            buffer.Write_const_char(name);
            buffer.EndTCPBuffer();
            m_Connection.SendByteBuffer(buffer);
        }

        public void RequestRemovePtxRule(string name, RequestResponse resp)
        {
            tcpByteBuffer buffer = new tcpByteBuffer(BUFFER_SIZE);
            buffer.BeginTCPBuffer();
            buffer.Write_u32(kMSG_REQUEST_REMOVE_PTXRULE);
            buffer.Write_u32(AddResponder(resp)); // reply-to function
            buffer.Write_const_char(name);
            buffer.EndTCPBuffer();
            m_Connection.SendByteBuffer(buffer);
        }

        public void RequestProfilingGlobal()
        {
            tcpByteBuffer buffer = new tcpByteBuffer(BUFFER_SIZE);
            buffer.BeginTCPBuffer();
            buffer.Write_u32(kMSG_REQUEST_PROFILING_GLOBAL);
            buffer.EndTCPBuffer();
            m_Connection.SendByteBuffer(buffer);
        }

        public void RequestProfilingEffect(String name)
        {
            tcpByteBuffer buffer = new tcpByteBuffer(BUFFER_SIZE);
            buffer.BeginTCPBuffer();
            buffer.Write_u32(kMSG_REQUEST_PROFILING_EFFECT);
            buffer.Write_const_char(name);
            buffer.EndTCPBuffer();
            m_Connection.SendByteBuffer(buffer);
        }
        public void RequestProfilingEmitter(String name)
        {
            tcpByteBuffer buffer = new tcpByteBuffer(BUFFER_SIZE);
            buffer.BeginTCPBuffer();
            buffer.Write_u32(kMSG_REQUEST_PROFILING_EMITTER);
            buffer.Write_const_char(name);
            buffer.EndTCPBuffer();
            m_Connection.SendByteBuffer(buffer);
        }
        public void RequestProfilingPtx(String name)
        {
            tcpByteBuffer buffer = new tcpByteBuffer(BUFFER_SIZE);
            buffer.BeginTCPBuffer();
            buffer.Write_u32(kMSG_REQUEST_PROFILING_PTX);
            buffer.Write_const_char(name);
            buffer.EndTCPBuffer();
            m_Connection.SendByteBuffer(buffer);
        }

        public void RequestLoadEffectList(String path, String name)
        {
            tcpByteBuffer buffer = new tcpByteBuffer(BUFFER_SIZE);
            buffer.BeginTCPBuffer();
            buffer.Write_u32(kMSG_REQUEST_LOAD_EFFECTLIST);
            buffer.Write_const_char(path);
            buffer.Write_const_char(name);
            buffer.EndTCPBuffer();
            m_Connection.SendByteBuffer(buffer);
        }

        public void RequestSaveEffectList(String path, String name)
        {
            tcpByteBuffer buffer = new tcpByteBuffer(BUFFER_SIZE);
            buffer.BeginTCPBuffer();
            buffer.Write_u32(kMSG_REQUEST_SAVE_EFFECTLIST);
            buffer.Write_const_char(path);
            buffer.Write_const_char(name);
            buffer.EndTCPBuffer();
            m_Connection.SendByteBuffer(buffer);
        }

        public void RequestSaveAll()
        {
            tcpByteBuffer buffer = new tcpByteBuffer(BUFFER_SIZE);
            buffer.BeginTCPBuffer();
            buffer.Write_u32(kMSG_REQUEST_SAVE_ALL);
            buffer.EndTCPBuffer();
            m_Connection.SendByteBuffer(buffer);
        }

        public void CancelSaveAll() 
        {
            tcpByteBuffer buffer = new tcpByteBuffer(BUFFER_SIZE);
            buffer.BeginTCPBuffer();
            buffer.Write_u32(kMSG_CANCEL_SAVE_ALL);
            buffer.EndTCPBuffer();
            m_Connection.SendByteBuffer(buffer);
        }

        public void RequestSave(int t, string name, bool saveDependents)
        {
            tcpByteBuffer buffer = new tcpByteBuffer(BUFFER_SIZE);
            buffer.BeginTCPBuffer();
            buffer.Write_u32(kMSG_SEND_REQUEST_SAVE);
            buffer.Write_u8((byte)t);
            buffer.Write_const_char(name);
            buffer.Write_bool(saveDependents);
            buffer.EndTCPBuffer();
            m_Connection.SendByteBuffer(buffer);
        }

        public void RequestSaveEvolution(string effectName, int index, string evoName, string filename)
        {
            tcpByteBuffer buffer = new tcpByteBuffer(BUFFER_SIZE);
            buffer.BeginTCPBuffer();
            buffer.Write_u32(kMSG_SAVE_EVOLUTION);
            buffer.Write_const_char(effectName);
            buffer.Write_s32(index);
            buffer.Write_const_char(evoName);
            buffer.Write_const_char(filename);
            buffer.EndTCPBuffer();
            m_Connection.SendByteBuffer(buffer);
        }

        public void RequestLoadEvolution(string effectName, int index, string filename)
        {
            tcpByteBuffer buffer = new tcpByteBuffer(BUFFER_SIZE);
            buffer.BeginTCPBuffer();
            buffer.Write_u32(kMSG_LOAD_EVOLUTION);
            buffer.Write_const_char(effectName);
            buffer.Write_s32(index);
            buffer.Write_const_char(filename);
            buffer.EndTCPBuffer();
            m_Connection.SendByteBuffer(buffer);
        }
        #endregion

        #region Send
        public void SendEffectRule(String name)
        {
            DataMod.dtaEffectRule dte = DataMod.GetEffectRule(name);
            if (dte == null) return;

            tcpByteBuffer buffer = new tcpByteBuffer(BUFFER_SIZE);
            buffer.BeginTCPBuffer();
            buffer.Write_u32(kMSG_SEND_EFFECTINFO);
            dte.WriteToByteBuffer(buffer);
            buffer.EndTCPBuffer();
            m_Connection.SendByteBuffer(buffer);
        }

        public void SendEmitterRule(String name)
        {
            DataMod.dtaEmitRule dte = DataMod.GetEmitRule(name);
            if (dte == null) return;

            tcpByteBuffer buffer = new tcpByteBuffer(BUFFER_SIZE);
            buffer.BeginTCPBuffer();
            buffer.Write_u32(kMSG_SEND_EMITINFO);
            dte.WriteToByteBuffer(buffer);
            buffer.EndTCPBuffer();
            m_Connection.SendByteBuffer(buffer);

        }

        public void SendPtxRule(String name)
        {
            DataMod.dtaPtxRule dte = DataMod.GetPtxRule(name);
            if (dte == null) return;

            tcpByteBuffer buffer = new tcpByteBuffer(BUFFER_SIZE);
            buffer.BeginTCPBuffer();
            buffer.Write_u32(kMSG_SEND_PTXRULE);
            dte.WriteToByteBuffer(buffer);
            buffer.EndTCPBuffer();
            m_Connection.SendByteBuffer(buffer);
        }

        public void SendRenameEmitterRule(String name, String newname)
        {
            if ((name.Length == 0) || (newname.Length == 0))
                return;
            tcpByteBuffer buffer = new tcpByteBuffer(BUFFER_SIZE);
            buffer.BeginTCPBuffer();
            buffer.Write_u32(kMSG_SEND_RENAME_EMITRULE_EVENT);
            buffer.Write_const_char(name);
            buffer.Write_const_char(newname);
            buffer.EndTCPBuffer();
            m_Connection.SendByteBuffer(buffer);
        }
        public void SendRenamePtxRule(String name, String newname)
        {
            if ((name.Length == 0) || (newname.Length == 0))
                return;
            tcpByteBuffer buffer = new tcpByteBuffer(BUFFER_SIZE);
            buffer.BeginTCPBuffer();
            buffer.Write_u32(kMSG_SEND_RENAME_PTXRULE_EVENT);
            buffer.Write_const_char(name);
            buffer.Write_const_char(newname);
            buffer.EndTCPBuffer();
            m_Connection.SendByteBuffer(buffer);
        }
        public void SendRenameEffectRule(String name, String newname)
        {
            if ((name.Length == 0) || (newname.Length == 0))
                return;
            tcpByteBuffer buffer = new tcpByteBuffer(BUFFER_SIZE);
            buffer.BeginTCPBuffer();
            buffer.Write_u32(kMSG_SEND_RENAME_EFFECTRULE_EVENT);
            buffer.Write_const_char(name);
            buffer.Write_const_char(newname);
            buffer.EndTCPBuffer();
            m_Connection.SendByteBuffer(buffer);
        }

        public void SendPlayControl()
        {
            tcpByteBuffer buffer = new tcpByteBuffer(BUFFER_SIZE);
            buffer.BeginTCPBuffer();
            buffer.Write_u32(kMSG_SEND_PLAY_CONTROL_INFO);
            buffer.Write_const_char(m_GUI.m_PlayControlForm.m_CurEffectName);
            buffer.Write_float(m_GUI.m_PlayControlForm.m_TimeScalar.Value);
            buffer.Write_bool(m_GUI.m_PlayControlForm.m_CheckRepeat.Checked);
            buffer.Write_float(m_GUI.m_PlayControlForm.m_RepeatTime.Value);
            buffer.Write_bool(m_GUI.m_PlayControlForm.m_RadioSCam.Checked);
            buffer.Write_float(m_GUI.m_PlayControlForm.m_CamStartDist.Value);
            buffer.Write_float(m_GUI.m_PlayControlForm.m_WPosX.Value);
            buffer.Write_float(m_GUI.m_PlayControlForm.m_WPosY.Value);
            buffer.Write_float(m_GUI.m_PlayControlForm.m_WPosZ.Value);
            buffer.Write_bool(m_GUI.m_PlayControlForm.m_CheckAnimateEffect.Checked);
            buffer.Write_float(m_GUI.m_PlayControlForm.m_SliderAnimateSpeed.Value);
            buffer.Write_float(m_GUI.m_PlayControlForm.m_SliderAnimateDist.Value);
            buffer.Write_u8((byte)m_GUI.m_PlayControlForm.m_AnimateOrientation);

            buffer.EndTCPBuffer();
            m_Connection.SendByteBuffer(buffer);
        }

        public void SendResetEvent()
        {
            SendResetEvent("");
        }

        public void SendResetEvent(String effectname)
        {
            tcpByteBuffer buffer = new tcpByteBuffer(BUFFER_SIZE);
            buffer.BeginTCPBuffer();
            buffer.Write_u32(kMSG_SEND_RESET_EVENT);
            if (effectname.Length > 0)
            {
                buffer.Write_bool(true);
                buffer.Write_const_char(effectname);
            }
            else
                buffer.Write_bool(false);
            buffer.EndTCPBuffer();
            m_Connection.SendByteBuffer(buffer);
        }

        public void SendStartEffectEvent(String name)
        {
            if (name == "") return;
            tcpByteBuffer buffer = new tcpByteBuffer(BUFFER_SIZE);
            buffer.BeginTCPBuffer();
            buffer.Write_u32(kMSG_SEND_STARTEFFECT);
            buffer.Write_const_char(name);
            buffer.EndTCPBuffer();
            m_Connection.SendByteBuffer(buffer);
        }

        public void SendFreezeScaleEvent(String name)
        {
            if (name == "") return;
            tcpByteBuffer buffer = new tcpByteBuffer(BUFFER_SIZE);
            buffer.BeginTCPBuffer();
            buffer.Write_u32(kMSG_SEND_FREEZESCALE);
            buffer.Write_const_char(name);
            buffer.EndTCPBuffer();
            m_Connection.SendByteBuffer(buffer);
        }

        public void SendDeleteTimeLineEvent(String name,int index)
        {
            if (name == "") return;
            tcpByteBuffer buffer = new tcpByteBuffer(BUFFER_SIZE);
            buffer.BeginTCPBuffer();
            buffer.Write_u32(kMSG_SEND_DEL_TIMELINE_EVENT);
            buffer.Write_const_char(name);
            buffer.Write_s32(index);
            buffer.EndTCPBuffer();
            m_Connection.SendByteBuffer(buffer);
        }

        public void SendReorderTimeLineEvents(String name)
        {
            if (name == "") return;
            tcpByteBuffer buffer = new tcpByteBuffer(BUFFER_SIZE);
            buffer.BeginTCPBuffer();
            buffer.Write_u32(kMSG_SEND_RESORT_TIMELINE_EVENT);
            buffer.Write_const_char(name);
            buffer.EndTCPBuffer();
            m_Connection.SendByteBuffer(buffer);
        }

        public void SendPtxRuleAddModelEvent(String rulename,String path,String modelname)
        {
            DataMod.dtaPtxRule dtm = DataMod.GetPtxRule(rulename);
            if (dtm == null) return;

            tcpByteBuffer buffer = new tcpByteBuffer(BUFFER_SIZE);
            buffer.BeginTCPBuffer();
            buffer.Write_u32(kMSG_PTXRULE_ADD_DEL_MODEL_EVENT);
            buffer.Write_const_char(rulename);
            buffer.Write_bool(true);
            buffer.Write_const_char(modelname);
            buffer.Write_const_char(path);
            buffer.EndTCPBuffer();
            m_Connection.SendByteBuffer(buffer);
        }

        public void SendPtxRuleRemoveModelEvent(String rulename, String modelname)
        {
            DataMod.dtaPtxRule dtm = DataMod.GetPtxRule(rulename);
            if (dtm == null) return;

            tcpByteBuffer buffer = new tcpByteBuffer(BUFFER_SIZE);
            buffer.BeginTCPBuffer();
            buffer.Write_u32(kMSG_PTXRULE_ADD_DEL_MODEL_EVENT);
            buffer.Write_const_char(rulename);
            buffer.Write_bool(false);
            buffer.Write_const_char(modelname);
            buffer.EndTCPBuffer();
            m_Connection.SendByteBuffer(buffer);
        }

        public void SendShaderChangedEvent(String rulename, String shadername)
        {
            DataMod.dtaPtxSprite dtm = DataMod.GetPtxRule(rulename) as DataMod.dtaPtxSprite;
            DataMod.dtaPtxRuleBehavior dtb = DataMod.GetPtxRule(rulename) as DataMod.dtaPtxRuleBehavior;
            if (dtm == null && dtb==null) return;

            tcpByteBuffer buffer = new tcpByteBuffer(BUFFER_SIZE);
            buffer.BeginTCPBuffer();
            buffer.Write_u32(kMSG_CHANGE_SHADER_EVENT);
            buffer.Write_const_char(rulename);
            buffer.Write_const_char(shadername);
			dtb.m_Shader.WriteToByteBuffer(buffer);
            buffer.EndTCPBuffer();
            m_Connection.SendByteBuffer(buffer);
        }

		public void SendKeyframeData(string ruleName, int ruleType, uint propertyId, int eventIdx, int evoIdx, DataMod.KeyframeData data)
        {
            tcpByteBuffer buffer = new tcpByteBuffer(BUFFER_SIZE);
            buffer.BeginTCPBuffer();
			buffer.Write_u32(kMSG_SET_KEYFRAME_DATA);
			buffer.Write_const_char(ruleName);
			buffer.Write_s32(ruleType);
			buffer.Write_u32(propertyId);
			buffer.Write_s32(eventIdx);
			buffer.Write_s32(evoIdx);
            DataMod.SaveXmlStruct(buffer, "rage__ptxKeyframe", data);
            buffer.EndTCPBuffer();
            m_Connection.SendByteBuffer(buffer);
        }

        public void SendCreateEvolutionEvent(String rulename, String evoname)
        {
            tcpByteBuffer buffer = new tcpByteBuffer(BUFFER_SIZE);
            buffer.BeginTCPBuffer();
            buffer.Write_u32(kMSG_REQUEST_EVOLUTION_CREATE);
            buffer.Write_const_char(rulename);
            buffer.Write_const_char(evoname);
            buffer.EndTCPBuffer();
            m_Connection.SendByteBuffer(buffer);
        }

		public void SendToggleAttractorDomain(String rulename)
		{
			tcpByteBuffer buffer = new tcpByteBuffer(BUFFER_SIZE);
			buffer.BeginTCPBuffer();
			buffer.Write_u32(kMSG_TOGGLE_ATTRACTOR_DOMAIN);
			buffer.Write_const_char(rulename);
			buffer.EndTCPBuffer();
			m_Connection.SendByteBuffer(buffer);
		}

        public void SendDelBehaviorPtxRuleEvent(String rulename, String behaviorname)
        {
            DataMod.dtaBehaviorDefinition def = DataMod.GetPtxRuleBehaviorDefinitionFromName(behaviorname);
            if (def == null) return;

            tcpByteBuffer buffer = new tcpByteBuffer(BUFFER_SIZE);
            buffer.BeginTCPBuffer();
            buffer.Write_u32(kMSG_REQUEST_DEL_BEHAVIOR_PTXRULE);
            buffer.Write_const_char(rulename);
            buffer.Write_const_char(def.m_BehaviorId);
            buffer.EndTCPBuffer();
            m_Connection.SendByteBuffer(buffer);
        }

        public void SendAddBehaviorPtxRuleEvent(String rulename, String behaviorname, RequestResponse resp)
        {
            DataMod.dtaBehaviorDefinition def = DataMod.GetPtxRuleBehaviorDefinitionFromName(behaviorname);
            if (def == null) return;

            tcpByteBuffer buffer = new tcpByteBuffer(BUFFER_SIZE);
            buffer.BeginTCPBuffer();
            buffer.Write_u32(kMSG_REQUEST_ADD_BEHAVIOR_PTXRULE);
            buffer.Write_const_char(rulename);
            buffer.Write_const_char(def.m_BehaviorId);
            buffer.Write_u32(AddResponder(resp)); // reply-to function
            buffer.EndTCPBuffer();
            m_Connection.SendByteBuffer(buffer);
        }
        public void SendValidateBehaviorEvent(DataMod.dtaPtxBehavior beh, string effectRuleName)
        {
            tcpByteBuffer buffer = new tcpByteBuffer(BUFFER_SIZE);
            buffer.BeginTCPBuffer();
            buffer.Write_u32(kMSG_REQUEST_VALIDATE_BEHAVIOR_PTXRULE);
            buffer.Write_const_char(beh.m_PtxRule.m_Name);
            buffer.Write_const_char(beh.m_BehaviorId);
            buffer.EndTCPBuffer();
            m_Connection.SendByteBuffer(buffer);
        }

        public void SendDeleteEvolutionEvent(String rulename, String evoname)
        {
            tcpByteBuffer buffer = new tcpByteBuffer(BUFFER_SIZE);
            buffer.BeginTCPBuffer();
            buffer.Write_u32(kMSG_REQUEST_EVOLUTION_DELETE);
            buffer.Write_const_char(rulename);
            buffer.Write_const_char(evoname);
            buffer.EndTCPBuffer();
            m_Connection.SendByteBuffer(buffer);
        }

        public void SendAddPropEvolutionEvent(String effectName, String evoName,int eventId, DataMod.dtaPtxKeyframeProp keyProp)
        {
            tcpByteBuffer buffer = new tcpByteBuffer(BUFFER_SIZE);
            buffer.BeginTCPBuffer();
            buffer.Write_u32(kMSG_PROPERTY_ADD_EVOLUTION);
            buffer.Write_const_char(effectName);
            buffer.Write_const_char(evoName);
            buffer.Write_s32(eventId);
            buffer.Write_u32((uint)keyProp.m_PropID);
            buffer.EndTCPBuffer();
            m_Connection.SendByteBuffer(buffer);
        }

        public void SendDelPropEvolutionEvent(String effectName, String evoName,int eventId, DataMod.dtaPtxKeyframeProp keyProp)
        {
            tcpByteBuffer buffer = new tcpByteBuffer(BUFFER_SIZE);
            buffer.BeginTCPBuffer();
            buffer.Write_u32(kMSG_PROPERTY_DEL_EVOLUTION);
            buffer.Write_const_char(effectName);
            buffer.Write_const_char(evoName);
            buffer.Write_s32(eventId);
            buffer.Write_u32((uint)keyProp.m_PropID);
            buffer.EndTCPBuffer();
            m_Connection.SendByteBuffer(buffer);
        }

        public void SendCreateNewPtxRuleEvent(int type,bool autoselect)
        {
            tcpByteBuffer buffer = new tcpByteBuffer(BUFFER_SIZE);
            buffer.BeginTCPBuffer();
            buffer.Write_u32(kMSG_SEND_CREATE_NEW_PTXRULE);
            buffer.Write_s32(type);
            buffer.Write_bool(autoselect);
            buffer.EndTCPBuffer();
            m_Connection.SendByteBuffer(buffer);
        }
        public void SendClonePtxRuleEvent(string name)
        {
            tcpByteBuffer buffer = new tcpByteBuffer(BUFFER_SIZE);
            buffer.BeginTCPBuffer();
            buffer.Write_u32(kMSG_REQUEST_CLONE_PTXRULE);
            buffer.Write_const_char(name);
            buffer.EndTCPBuffer();
            m_Connection.SendByteBuffer(buffer);
        }
        public void SendCreateNewEmitRuleEvent(bool autoselect)
        {
            tcpByteBuffer buffer = new tcpByteBuffer(BUFFER_SIZE);
            buffer.BeginTCPBuffer();
            buffer.Write_u32(kMSG_SEND_CREATE_NEW_EMITRULE);
            buffer.Write_bool(autoselect);
            buffer.EndTCPBuffer();
            m_Connection.SendByteBuffer(buffer);
        }
        public void SendCreateNewEffectRuleEvent(bool autoselect)
        {
            tcpByteBuffer buffer = new tcpByteBuffer(BUFFER_SIZE);
            buffer.BeginTCPBuffer();
            buffer.Write_u32(kMSG_SEND_CREATE_NEW_EFFECTRULE);
            buffer.Write_bool(autoselect);
            buffer.EndTCPBuffer();
            m_Connection.SendByteBuffer(buffer);
        }
        public void SendCreateNewTimelineEvent(String effectname,int type,bool autoselect)
        {
            tcpByteBuffer buffer = new tcpByteBuffer(BUFFER_SIZE);
            buffer.BeginTCPBuffer();
            buffer.Write_u32(kMSG_SEND_CREATE_NEW_TIMELINE_EVENT);
            buffer.Write_const_char(effectname);
            buffer.Write_s32(type);
            buffer.Write_bool(autoselect);
            buffer.EndTCPBuffer();
            m_Connection.SendByteBuffer(buffer);
        }

        public void SendShowEffectNames(bool show)
        {
            tcpByteBuffer buffer = new tcpByteBuffer(BUFFER_SIZE);
            buffer.BeginTCPBuffer();
            buffer.Write_u32(kMSG_SEND_SHOW_EFFECT_NAMES);
            buffer.Write_bool(show);
            buffer.EndTCPBuffer();
            m_Connection.SendByteBuffer(buffer);
        }

        #endregion

    }
}

