using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Ini;

namespace ParticleEditor
{
    public partial class ptxForm : Form
    {
        public static IniFile m_ConfigFile = new IniFile("rmptfx.ini");
        public string m_WindowID = "";
        public string m_Name = "";
        bool m_ShowOnLoad = true;
        bool m_Loaded = false;
        bool m_Persistant = true;

#region Properties
        public bool ShowOnLoad
        {
            get { return m_ShowOnLoad; }
            set { m_ShowOnLoad = value; }
        }
        public bool Persistant
        {
            get { return m_Persistant; }
            set { m_Persistant = value; }
        }
#endregion

        public ptxForm()
        {
            InitializeComponent();
        }

        protected virtual void LoadConfigData()
        {
            Top = int.Parse(m_ConfigFile.IniReadValueDefault(m_WindowID, "TOP",Top.ToString()));
            Left = int.Parse(m_ConfigFile.IniReadValueDefault(m_WindowID, "LEFT",Left.ToString()));
            if (this.FormBorderStyle != FormBorderStyle.FixedToolWindow)
            {
                Width = int.Parse(m_ConfigFile.IniReadValueDefault(m_WindowID, "WIDTH", Width.ToString()));
                Height = int.Parse(m_ConfigFile.IniReadValueDefault(m_WindowID, "HEIGHT", Height.ToString()));
            }
            for (int i = 0; i < Controls.Count; i++)
                LoadControl(Controls[i]);
            m_Loaded = true;
        }


        protected virtual void SaveConfigData()
        {
            m_ConfigFile.IniWriteValue(m_WindowID, "TOP", Top.ToString());
            m_ConfigFile.IniWriteValue(m_WindowID, "Left", Left.ToString());
            if (this.FormBorderStyle != FormBorderStyle.FixedToolWindow)
            {
                m_ConfigFile.IniWriteValue(m_WindowID, "WIDTH", Width.ToString());
                m_ConfigFile.IniWriteValue(m_WindowID, "HEIGHT", Height.ToString());
            }
            for (int i = 0; i < Controls.Count; i++)
                SaveControl(Controls[i]);
        }

        protected void SaveControl(Control c)
        {
            if (c is ptxSubSection)
            {
                ptxSubSection sub = c as ptxSubSection;
                sub.SaveConfigData(m_ConfigFile);
                for (int i = 0; i < c.Controls.Count; i++)
                    SaveControl(c.Controls[i]);
                return;
            }
            for (int i = 0; i < c.Controls.Count; i++)
                SaveControl(c.Controls[i]);

        }
        protected void LoadControl(Control c)
        {
            if (c is ptxSubSection)
            {
                ptxSubSection sub = c as ptxSubSection;
                sub.LoadConfigData(m_ConfigFile);
                for (int i = 0; i < c.Controls.Count; i++)
                    LoadControl(c.Controls[i]);
                return;
            }
            for (int i = 0; i < c.Controls.Count; i++)
                LoadControl(c.Controls[i]);
        }

        private void ptxForm_Load(object sender, EventArgs e)
        {
            LoadConfigData();
        }

        private void ptxForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!m_Persistant) return;
            e.Cancel = true;
            if (e.CloseReason == CloseReason.UserClosing)
                m_ShowOnLoad = false;
            Hide();
        }

        private void ptxForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            if(m_Loaded)
                SaveConfigData();
        }
    }
}