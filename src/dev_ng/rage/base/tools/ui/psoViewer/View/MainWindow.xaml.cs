﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using Microsoft.Win32;
using RSG.Base;
using System.ComponentModel;

namespace PsoViewer.View
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private const String HELP_URL = "https://devstar.rockstargames.com/wiki/index.php/PsoViewer";
        private String lastPath = "";
        ViewModel.PsoViewModel vm;

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public MainWindow()
        {

            InitializeComponent();
            this.vm = new ViewModel.PsoViewModel();
            this.DataContext = vm;
        }
        #endregion // Constructor(s)

        public void LoadFile(String filename)
        {
            this.vm = new ViewModel.PsoViewModel();
            this.DataContext = vm;
            this.vm.Load(filename);
        }

        #region Command Handlers
        #region CanExecute Handlers
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Command_CanExecuteAlways(Object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Command_CanExecuteNever(Object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = false;
        }
        #endregion // CanExecute Handlers

        private void CommandOpen_Executed(Object sender, ExecutedRoutedEventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Filter = Properties.Settings.Default.PsoOpenFilter;
            dlg.FilterIndex = Properties.Settings.Default.PsoOpenFilterIndex;
            if (dlg.ShowDialog() == true)
            {
                vm.Load(dlg.FileName);
            }
        }

        /// <summary>
        /// Exit action.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CommandExit_Executed(Object sender, ExecutedRoutedEventArgs e)
        {
            Close();
        }

        /// <summary>
        /// Help Command Execute
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CommandHelp_Executed(Object sender, ExecutedRoutedEventArgs e)
        {
            System.Diagnostics.Process.Start(HELP_URL);
        }

        /// <summary>
        /// About Command Execute
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CommandAbout_Executed(Object sender, ExecutedRoutedEventArgs e)
        {

        }
        #endregion // Command Handlers

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_Loaded(Object sender, RoutedEventArgs e)
        {
            CommandLineParser parser = new CommandLineParser(Environment.GetCommandLineArgs());
            if (parser.TrailingArguments.Count > 1)
            {
                vm.Load(parser.TrailingArguments[1]);
            }        
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_Closing(Object sender, System.ComponentModel.CancelEventArgs e)
        {
            Properties.Settings.Default.Save();
        }
    }

} // PsoViewer.View namespace
