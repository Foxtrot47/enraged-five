﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using RSG.Base.Editor;
using RSG.Base.Editor.Command;
using RSG.ManagedRage;
using RSG.Base.Collections;

namespace PsoViewer.ViewModel
{
    
    /// <summary>
    /// Pso File ViewModel class.
    /// </summary>
    public class PsoViewModel : ViewModelBase
    {  
        #region Properties and Associated Member Data

        public ObservableCollection<MPsoStructureSchema> StructureSchemas
        {
            get { return m_StructureSchemas; }
            private set
            {
                SetPropertyValue(value, () => this.StructureSchemas, new PropertySetDelegate(delegate(Object newValue) { m_StructureSchemas = (ObservableCollection<MPsoStructureSchema>)newValue; }));
            }
        }
        private ObservableCollection<MPsoStructureSchema> m_StructureSchemas;

        public ObservableCollection<MPsoEnumSchema> EnumSchemas
        {
            get { return m_EnumSchemas; }
            private set
            {
                SetPropertyValue(value, () => this.EnumSchemas, new PropertySetDelegate(delegate(Object newValue) { m_EnumSchemas = (ObservableCollection<MPsoEnumSchema>)newValue; }));
            }
        }
        private ObservableCollection<MPsoEnumSchema> m_EnumSchemas;

        public ObservableCollection<MPsoStructArray> StructArrays
        {
            get { return m_StructArrays; }
            private set
            {
                SetPropertyValue(value, () => this.StructArrays, new PropertySetDelegate(delegate(Object newValue) { m_StructArrays = (ObservableCollection<MPsoStructArray>)newValue; }));
            }
        }
        private ObservableCollection<MPsoStructArray> m_StructArrays;



        /// <summary>
        /// 
        /// </summary>
        public String Title
        {
            get { return m_sTitle; }
            private set
            {
                SetPropertyValue(value, () => this.Title,
                    new PropertySetDelegate(delegate(Object newValue) { m_sTitle = (String)newValue; }));
            }
        }
        private String m_sTitle;

        /// <summary>
        /// summary for status bar display.
        /// </summary>
        public String Summary
        {
            get { return m_sSummary; }
            private set
            {
                SetPropertyValue(value, () => this.Summary,
                    new PropertySetDelegate(delegate(Object newValue) { m_sSummary = (String)newValue; }));
            }
        }
        private String m_sSummary;

        /// <summary>
        /// Recent files.
        /// </summary>
        public ObservableCollection<RecentFileViewModel> RecentFiles
        {
            get { return m_RecentFiles; }
            private set
            {
                SetPropertyValue(value, () => this.RecentFiles,
                    new PropertySetDelegate(delegate(Object newValue) { m_RecentFiles = (ObservableCollection<RecentFileViewModel>)newValue; }));
            }
        }
        private ObservableCollection<RecentFileViewModel> m_RecentFiles;


        #endregion // Properties and Associated Member Data

        public RSG.ManagedRage.MPsoFile PsoFile
        {
            get { return m_PsoFile; }
        }

        private RSG.ManagedRage.MPsoFile m_PsoFile;

        #region Member Data
        /// <summary>
        /// 
        /// </summary>
		
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public PsoViewModel()
        {
            this.StructArrays = new ObservableCollection<MPsoStructArray>();
            this.StructureSchemas = new ObservableCollection<MPsoStructureSchema>();
            this.EnumSchemas = new ObservableCollection<MPsoEnumSchema>();

            UpdateTitle(String.Empty);
            UpdateRecentFileVM();
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Load an RPF file by filename.
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public bool Load(String filename)
        {
            m_PsoFile = MPsoFile.Load(filename);

            // Build the viewmodels
            this.StructArrays.Clear();
            this.StructArrays = new ObservableCollection<MPsoStructArray>(PsoFile.StructureMap.StructArrays);

            this.StructureSchemas.Clear();
            this.StructureSchemas = new ObservableCollection<MPsoStructureSchema>(PsoFile.SchemaCatalog.StructureSchemas.Values);

            this.EnumSchemas.Clear();
            this.EnumSchemas = new ObservableCollection<MPsoEnumSchema>(PsoFile.SchemaCatalog.EnumSchemas.Values);

            //StringBuilder sb = new StringBuilder();
            //DescribeData(m_PsoFile.RootInstance, sb, "");
            //Console.Write(sb.ToString());

            return (m_PsoFile != null);
        }

        #endregion // Controller Methods

        #region Private Methods

        void DescribeData(MPsoStruct str, StringBuilder sb, String indent)
        {
            sb.Append(indent);
            sb.AppendLine(str.ToString());
            String memIndent = indent + "\t";
            foreach (var mem in str)
            {
                DescribeData(mem, sb, memIndent);
            }
        }

        void DescribeData(MPsoMember mem, StringBuilder sb, String indent)
        {
            sb.Append(indent);
            sb.AppendLine(mem.ToString());

            MPsoStructMember structMem = mem as MPsoStructMember;
            if (structMem != null)
            {
                DescribeData(structMem.SubStructure, sb, indent + "\t");
            }

            MPsoArrayMember arrayMem = mem as MPsoArrayMember;
            if (arrayMem != null)
            {
                int idx = 0;
                foreach (var elt in arrayMem)
                {
                    DescribeData(elt, sb, indent + String.Format("[{0}] ", idx));
                    idx++;
                }
            }

            MPsoMapMember mapMem = mem as MPsoMapMember;
            if (mapMem != null)
            {
                int idx = 0;
                foreach (var elt in mapMem)
                {
                    DescribeData(elt.Key, sb, indent + String.Format("[{0}] Key ", idx));
                    DescribeData(elt.Value, sb, indent + String.Format("[{0}] Value ", idx));
                }
            }
        }

        /// <summary>
        /// Update Window title property.
        /// </summary>
        /// <param name="filename"></param>
        private void UpdateTitle(String filename)
        {
            System.Reflection.Assembly assembly = 
                System.Reflection.Assembly.GetEntryAssembly();
            filename = Path.GetFileName(filename);
#if DEBUG
            if (String.IsNullOrWhiteSpace(filename))
                this.Title = String.Format("Rockstar PSO Viewer - {0} DEBUG",
                        assembly.GetName().Version);
            else
                this.Title = String.Format("Rockstar PSO Viewer - {0} DEBUG - {1}",
                        assembly.GetName().Version, filename);
#else
            if (String.IsNullOrWhiteSpace(filename))
                this.Title = String.Format("Rockstar PSO Viewer - {0}",
                        assembly.GetName().Version);
            else
                this.Title = String.Format("Rockstar PSO Viewer - {0} - {1}",
                        assembly.GetName().Version, filename);
#endif
        }

        /// <summary>
        /// Add new recent file.
        /// </summary>
        /// <param name="filename"></param>
        private void AddRecentFile(String filename)
        {
            String[] recentFiles = Properties.Settings.Default.PsoRecentFiles.Split(
                new char[]{';'}, StringSplitOptions.RemoveEmptyEntries);
            List<String> recentFileList = new List<String>(recentFiles);
            if (recentFileList.Contains(filename))
                recentFileList.Remove(filename);
            recentFileList.Add(filename);
            while (recentFileList.Count > Properties.Settings.Default.PsoRecentFileCount)
                recentFileList.RemoveAt(0);
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            foreach (String file in recentFileList)
            {
                sb.Append(file);
                sb.Append(';');
            }
            Properties.Settings.Default.PsoRecentFiles = sb.ToString();
            UpdateRecentFileVM();
        }

        /// <summary>
        /// Update recent files VM.
        /// </summary>
        private void UpdateRecentFileVM()
        {
            this.RecentFiles = new ObservableCollection<RecentFileViewModel>();
            int index = 1;
            String[] recentFiles = Properties.Settings.Default.PsoRecentFiles.Split(
                new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            List<String> recentFileList = new List<String>(recentFiles);
            recentFileList.Reverse();
            foreach (String filename in recentFileList)
            {
                RecentFileViewModel vm = new RecentFileViewModel(index++, filename, this);
                this.RecentFiles.Add(vm);
            }
        }
        #endregion // Private Methods
    }
    

    /// <summary>
    /// PSO recent file view model.
    /// </summary>
    public class RecentFileViewModel : ViewModelBase
    {
        #region Properties
        /// <summary>
        /// Menuitem header.
        /// </summary>
        public String Header
        {
            get { return m_sHeader; }
            private set
            {
                SetPropertyValue(value, () => this.Header,
                    new PropertySetDelegate(delegate(Object newValue) { m_sHeader = (String)newValue; }));
            }
        }
        private String m_sHeader;

        /// <summary>
        /// Associated filename string.
        /// </summary>
        public String Filename
        {
            get { return m_sFilename; }
            private set
            {
                SetPropertyValue(value, () => this.Filename,
                    new PropertySetDelegate(delegate(Object newValue) { m_sFilename = (String)newValue; }));
            }
        }
        private String m_sFilename;

        /// <summary>
        /// 
        /// </summary>
        public System.Windows.Input.ICommand OpenCommand
        {
            get { return m_OpenCommand; }
            private set
            {
                SetPropertyValue(value, () => this.OpenCommand,
                    new PropertySetDelegate(delegate(Object newValue) { m_OpenCommand = (System.Windows.Input.ICommand)newValue; }));
            }
        }
        private System.Windows.Input.ICommand m_OpenCommand;
        #endregion // Properties

        #region Member Data
        PsoViewModel vm;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="filename"></param>
        public RecentFileViewModel(int index, String filename, PsoViewModel vm)
        {        
            this.Filename = (filename.Clone() as String);
            this.Header = String.Format("_{0} {1}", index, Filename);
            this.vm = vm;
            this.OpenCommand = new OpenFileCommand(filename, vm);
        }
        #endregion // Constructor(s)
    }

    public class PsoDataViewModel : ViewModelBase
    {

    }

    internal class OpenFileCommand : System.Windows.Input.ICommand
    {
        private String m_sFilename;
        private PsoViewModel m_vm;
        public event EventHandler CanExecuteChanged;

        public OpenFileCommand(String filename, PsoViewModel vm)
        {
            this.m_sFilename = filename;
            this.m_vm = vm;
        }

        public bool CanExecute(Object parameter)
        {
            return (true);
        }

        public void Execute(Object parameter)
        {
            this.m_vm.Load(this.m_sFilename);
        }
    }

} // RpfViewer.ViewModel namespace
