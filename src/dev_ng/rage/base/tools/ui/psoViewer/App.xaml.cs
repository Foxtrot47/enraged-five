﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Windows;
using System.Windows.Threading;
using PsoViewer.View;
using RSG.Base;
using RSG.Base.Windows;
using RSG.ManagedRage;

namespace PsoViewer
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        #region Constants
        private const String AUTHOR = "Russ Schaaf";
        private const String EMAIL = "russ.schaaf@rockstarsandiego.com";
        #endregion // Constants

        #region Event Handlers
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Application_DispatcherUnhandledException(Object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            ExceptionStackTraceDlg dlg = new ExceptionStackTraceDlg(e.Exception, AUTHOR, EMAIL);
            dlg.ShowDialog();
        }
        #endregion // Event Handlers

        private void Application_Startup(object sender, StartupEventArgs e)
        {
            cRage.Init();

            System.IO.StreamReader reader = new System.IO.StreamReader("X:\\debug\\strings.txt");
            while (!reader.EndOfStream)
            {
                string s = reader.ReadLine();
                MAtLiteralHashString.Add(s);
            }
        }
    }

} // PsoViewer namespace
