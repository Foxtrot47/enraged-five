﻿using System;
using System.Windows.Input;

namespace PsoViewer
{

    /// <summary>
    /// PsoViewer Command Class.
    /// </summary>
    internal static class Commands
    {
        public static RoutedUICommand FileOpen =
            new RoutedUICommand("_Open", "FileOpen", typeof(Commands));
        public static RoutedUICommand FileExit =
            new RoutedUICommand("E_xit", "FileExit", typeof(Commands));
        public static RoutedUICommand Help =
            new RoutedUICommand("_Help", "Help", typeof(Commands));
        public static RoutedUICommand About =
            new RoutedUICommand("_About...", "About", typeof(Commands));
    }

} // PsoViewer namespace
