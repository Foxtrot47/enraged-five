using System;
using ragWidgets;
using RSG.Base.Logging;

namespace ragMultilineTextPlugin
{
	/// <summary>
	/// Summary description for Class1.
	/// </summary>
	public class PlugIn : ragWidgetPage.WidgetPagePlugIn
	{
		public PlugIn()
		{
			//
			// TODO: Add constructor logic here
			//
        }

        #region WidgetPagePlugIn overrides
        // PURPOSE: This is the name that will appear in the Plug Ins menu.
		// Choosing this menu item will call ActivatePlugInMenuItem
		public override string Name
		{
			get
			{
				return "Multiline Text View";
			}
		}

        public override void InitPlugIn( PlugInHostData hostData )
        {
            base.InitPlugIn( hostData );

            sm_Instance = this;

            base.m_pageNameToCreateDelMap[MultilineControl.MyName] = new ragWidgetPage.WidgetPage.CreatePageDel( MultilineControl.CreatePage );
        }

        public override void RemovePlugIn()
        {
            base.RemovePlugIn();
            sm_Instance = null;
        }

        // PURPOSE: Override so that we can have one CreagePageDel create multiple instances of MultilineControl
        public override TD.SandDock.DockControl ResolveDockControl(String guid, ILog log)
        {
            if ( base.DllInfo.GuidToNameHash.ContainsKey( guid ) )
            {
                String pageName = base.DllInfo.GuidToNameHash[guid];
                if ( pageName.StartsWith( MultilineControl.MyName ) )
                {
                    // a bit of hack, but this will specify which InstanceNumber to use in the Tab Text and subsequently our hash tables
                    String num = pageName.Substring( MultilineControl.MyName.Length ).Trim();
                    MultilineControl.UseInstanceNumber = int.Parse( num );

                    IDockableView view = CreateView( m_pageNameToCreateDelMap[MultilineControl.MyName], log );

                    // we can go ahead and call PostOpenView because SandDockManager is going to do the opening for us
                    PostOpenView( view.DockControl, view );
                    return view.DockControl;
                }
            }

            return null;
        }

        // PURPOSE: Override to call the Bind function on each of our MultilineControls that we created at startup
        protected override void MainBankManager_InitFinished()
        {
            if ( CheckPluginStatus() )
            {
                foreach ( ragWidgetPage.WidgetPage page in base.m_WidgetPages )
                {
                    MultilineControl ctrl = page as MultilineControl;
                    ctrl.Bind();
                }
            }
            else
            {
                base.MainBankManager_InitFinished();
            }
        }

		// PURPOSE: This function gets called when the plug in menu item is activated.
		// For most plug ins this creates dockable windows, though the plugin can do whatever
		// it wants here.
		public override void ActivatePlugInMenuItem(Object sender, System.EventArgs e)
		{
			// In this case, we call the base class CreateView function. This function creates a new
			// dockable view. You have to pass it a delegate that creates the control that it
			// puts into the view. In this case it calls the MultilineControl.CreatePage static function.
            // We can have multiple instances of our MultilineControl, so no need to check if it is already created.
            base.CreateAndOpenView(m_pageNameToCreateDelMap[MultilineControl.MyName], LogFactory.ApplicationLog);
		}

		// PURPOSE: Whenever the Plug Ins menu is displayed, these functions are called to determine
		// whether or not to show this menu item as activated. 
		// If your plug-in looks for particular widgets, it could check that those widgets
		// exist here and return false if they don't.
		public override bool CheckPluginStatus()
		{
			return true;
		}

		// PURPOSE: If this returns true, newly created dockable views will be put on top
		// If false, they will be placed underneath existing views.
		public override bool ShowOnActivation
		{
			get
			{
				return true;
			}
        }
        #endregion

        // PURPOSE: We typically only have one instance of each ragWidgetPage.WidgetPagePlugIn subclass.
        public static PlugIn Instance
        {
            get
            {
                return sm_Instance;
            }
        }
        protected static PlugIn sm_Instance;
    }
}
