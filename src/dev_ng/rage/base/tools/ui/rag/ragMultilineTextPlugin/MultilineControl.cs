using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using ragWidgets;
using RSG.Base.Logging;

namespace ragMultilineTextPlugin
{
	/// <summary>
	/// Summary description for MultilineControl.
	/// </summary>
	public class MultilineControl : ragWidgetPage.WidgetPage
	{
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox WidgetNameBox;
		private System.Windows.Forms.Button BindButton;
		private System.Windows.Forms.TextBox WidgetContents;
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		// PURPOSE: This constructor is required to be able to design this control
		public MultilineControl()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			// TODO: Add any initialization after the InitializeComponent call
		}

		// PURPOSE: This is the constructor that will be called when creating a new dockable view.
		// We need to pass the appName, bankMgr and dockControl along to the base class c'tor
		// so that the new control gets hooked up correctly.
        public MultilineControl(String appName, ragCore.IBankManager bankMgr, IKeyUpProcessor keyProcessor, TD.SandDock.DockControl dockControl, ILog log, int instanceNumber)
			: base(appName, bankMgr,keyProcessor, dockControl, log)
		{
			InitializeComponent();

            m_instanceNumber = instanceNumber;

            // dispose of this control when its DockControl is closed
            dockControl.CloseAction = TD.SandDock.DockControlCloseAction.Dispose;
		}

        private int m_instanceNumber;
        private string m_currentWidgetName = string.Empty;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.WidgetNameBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.BindButton = new System.Windows.Forms.Button();
            this.WidgetContents = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // WidgetNameBox
            // 
            this.WidgetNameBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.WidgetNameBox.Location = new System.Drawing.Point( 96, 8 );
            this.WidgetNameBox.Name = "WidgetNameBox";
            this.WidgetNameBox.Size = new System.Drawing.Size( 504, 20 );
            this.WidgetNameBox.TabIndex = 0;
            this.WidgetNameBox.Text = "Widget name";
            this.WidgetNameBox.TextChanged += new System.EventHandler( this.WidgetNameBox_TextChanged );
            this.WidgetNameBox.KeyDown += new System.Windows.Forms.KeyEventHandler( this.WidgetNameBox_KeyDown );
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point( 12, 12 );
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size( 80, 16 );
            this.label1.TabIndex = 1;
            this.label1.Text = "Widget Name";
            // 
            // BindButton
            // 
            this.BindButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.BindButton.Location = new System.Drawing.Point( 619, 7 );
            this.BindButton.Name = "BindButton";
            this.BindButton.Size = new System.Drawing.Size( 75, 23 );
            this.BindButton.TabIndex = 2;
            this.BindButton.Text = "View";
            this.BindButton.Click += new System.EventHandler( this.BindButton_Click );
            // 
            // WidgetContents
            // 
            this.WidgetContents.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.WidgetContents.Enabled = false;
            this.WidgetContents.Location = new System.Drawing.Point( 8, 40 );
            this.WidgetContents.Multiline = true;
            this.WidgetContents.Name = "WidgetContents";
            this.WidgetContents.Size = new System.Drawing.Size( 688, 160 );
            this.WidgetContents.TabIndex = 3;
            // 
            // MultilineControl
            // 
            this.Controls.Add( this.WidgetContents );
            this.Controls.Add( this.BindButton );
            this.Controls.Add( this.label1 );
            this.Controls.Add( this.WidgetNameBox );
            this.Name = "MultilineControl";
            this.Size = new System.Drawing.Size( 704, 208 );
            this.ResumeLayout( false );
            this.PerformLayout();

		}
		#endregion

		// PURPOSE: GetTabText returns the string name to use on the tab for the newly
		// created dockable form. This will be used as the initial value. It can be changed
		// at any time in the future by setting the m_DockControl.TabText property
		public override string GetTabText()
		{
			return MyName + " " + m_instanceNumber.ToString();
		}

        // PURPOSE: Identifier for SandDock related operations.  Recognize that this name is 
        // dynamic and that PlugIn has been written specifically to handle this.
        public override string GetMyName()
        {
            return GetTabText();
        }

        // PURPOSE: When the text changes, we're going to check if the Bind button should be enabled.
        // If the text is different from the name we're bound to, we'll enable it.
        private void WidgetNameBox_TextChanged( object sender, EventArgs e )
        {
            if ( this.WidgetNameBox.Text != m_currentWidgetName )
            {
                this.BindButton.Enabled = true;
            }
            else
            {
                this.BindButton.Enabled = false;
            }
        }

        // PURPOSE: For convenience, we'll capture the Enter key and simulate clicking the Bind button
        private void WidgetNameBox_KeyDown( object sender, KeyEventArgs e )
        {
            if ( e.KeyCode == Keys.Enter )
            {
                BindButton_Click( sender, EventArgs.Empty );
                e.Handled = true;
            }
        }

		// PURPOSE: When the user clicks on the "View" button, we want to search for the 
		// widget in the widget hierarchy and if one is found, bind the contents of that
		// widget to the contents of the multiline text control.
		private void BindButton_Click(object sender, System.EventArgs e)
		{
			// First remove any old data bindings. This is important because
			// old bindings can keep handles to unused objects.
			RemoveBindings();

			// Now bind the widget whose path name is WidgetNameBox.Text to
			// the WidgetContents.Text property. The property has to be specified
			// in two parts: Control, "Property". There are a number of other overloads
			// of Bind for other kinds of binding. For example when binding one ragUi
			// control to another, you can just pass the control, you don't need to 
			// specify which property.
            if ( Bind( WidgetNameBox.Text, WidgetContents, "Text" ) )
            {
                m_currentWidgetName = WidgetNameBox.Text;
                BindButton.Enabled = false;
            }
		}

		// PURPOSE: Plug-ins should have a way to preserve their current state and restore
		// it later. This is currently used when the widget tree is rebuilt and the plugin
		// has to update its bindings. It also may be used in the future for persistant state
		// for the plugins.
		protected class StateData
		{
			public string m_WidgetName;
		}

		// PURPOSE: This function is called whenever the ragWidgetPage.PlugIn needs to 
		// save the state for this widget. In this case the state is just the name of the 
		// widget we're viewing.
		public override object GetStateData()
		{
			StateData data = new StateData();
			data.m_WidgetName = WidgetNameBox.Text;
			return data;
		}

		// PURPOSE: This function is called whenever the ragWidgetPage.PlugIn needs to
		// restore a previously saved state. The object that is passed in matches one that
		// was returned from GetStateData().
		public override void SetStateData(object o)
		{
			StateData data = o as StateData;
			if (data != null)
			{
				// In this case, restoring the state is just a matter of setting the name box
				// to its old value and simulating a click on the "View" button.
				WidgetNameBox.Text = data.m_WidgetName;
				BindButton_Click(this, System.EventArgs.Empty);
			}
		}

        // PURPOSE: Save the binding information
        public override void SaveWidgetPageData( System.Collections.Generic.Dictionary<string, string> data )
        {
            base.SaveWidgetPageData( data );

            // save the last widget name we were was bound to
            data[this.GetTabText() + ":WidgetName"] = m_currentWidgetName;
        }

        // PURPOSE: Load the binding information
        public override void LoadWidgetPageData( System.Collections.Generic.Dictionary<string, string> data )
        {
            base.LoadWidgetPageData( data );

            String value;
            if ( data.TryGetValue( this.GetTabText() + ":WidgetName", out value ) )
            {
                // We can't call SetStateData here because the widget this is bound to has yet to be created.
                this.WidgetNameBox.Text = value;
            }
        }

        // PURPOSE: Called from PlugIn.MainBankManager_InitFinished, attempts to bind to the widget name that was loaded
        //   by LoadWidgetPageData.
        public void Bind()
        {
            BindButton_Click( this, EventArgs.Empty );
        }

        private static int sm_InstanceCount = 0;

        public static string MyName = "Multiline Text";

        public static int UseInstanceNumber = -1;

		// PURPOSE: This is the delegate that the WidgetPagePlugIn uses to create an instance
		// of the view. Here we create a new multiline control every time this is called.
		// You could also keep a static instance pointer and return that if you just want one
		// instance of your plug in control.
        public static ragWidgetPage.WidgetPage CreatePage(String appName, ragCore.IBankManager bankMgr, IKeyUpProcessor keyProcessor, TD.SandDock.DockControl dockControl, ILog log)
		{
            int instanceNumber;
            if ( UseInstanceNumber > -1 )
            {
                instanceNumber = UseInstanceNumber;
                UseInstanceNumber = -1;

                if ( instanceNumber >= sm_InstanceCount )
                {
                    sm_InstanceCount = instanceNumber + 1;
                }
            }
            else
            {
                instanceNumber = sm_InstanceCount;
                ++sm_InstanceCount;
            }

            return new MultilineControl( appName, bankMgr, keyProcessor, dockControl, log, instanceNumber );
		}
	}
}
