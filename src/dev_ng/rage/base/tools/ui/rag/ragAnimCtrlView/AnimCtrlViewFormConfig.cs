using System;
using System.Configuration;
using System.Xml;

namespace ragAnimCtrlView
{
	public class AnimCtrlViewFormConfigSectionHandler : IConfigurationSectionHandler
	{
		public AnimCtrlViewFormConfigSectionHandler()
		{
		}

		public object Create(object parent,object context,XmlNode section)
		{
			AnimCtrlViewFormSettings acs = new AnimCtrlViewFormSettings();
			
			if(section == null)
				return acs;

			XmlNode configNode = section.SelectSingleNode("BankRoot");
			if(configNode != null)
				acs.BankRootPath = configNode.InnerText;

            configNode = section.SelectSingleNode("Play Forwards");
			if(configNode != null)
				acs.PlayForwardLabel = configNode.InnerText;

			configNode = section.SelectSingleNode("Pause");
			if(configNode != null)
				acs.PauseLabel = configNode.InnerText;

            configNode = section.SelectSingleNode("Play Backwards");
			if(configNode != null)
				acs.PlayBackwardLabel = configNode.InnerText;

			configNode = section.SelectSingleNode("Step Backward");
			if(configNode != null)
				acs.StepBackwardLabel = configNode.InnerText;

            configNode = section.SelectSingleNode("Step Forward");
			if(configNode != null)
				acs.StepForwardLabel = configNode.InnerText;

			configNode = section.SelectSingleNode("Goto End");
			if(configNode != null)
				acs.GotoEndLabel = configNode.InnerText;

			configNode = section.SelectSingleNode("Goto Start");
			if(configNode != null)
				acs.GotoStartLabel = configNode.InnerText;

			return acs;	
		}
	}

	public class AnimCtrlViewFormSettings
	{
		public AnimCtrlViewFormSettings()
		{
            BankRootPath = "rage - GlobalAnimation";
			PlayForwardLabel = "Play Forwards";
			PlayBackwardLabel = "Play Backwards";
			PauseLabel = "Pause";
			StepForwardLabel = "Step Forward";
			StepBackwardLabel = "Step Back";
			GotoEndLabel = "Goto End";
			GotoStartLabel = "Goto Start";
		}
		public String	BankRootPath;
		public String	PlayForwardLabel;
		public String	PlayBackwardLabel;
		public String	PauseLabel;
		public String	StepForwardLabel;
		public String	StepBackwardLabel;
		public String	GotoEndLabel;
		public String	GotoStartLabel;
	}
}
