namespace ragAnimCtrlView
{
    partial class AnimCtrlViewForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && (components != null) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.playbackControl = new PlaybackControls.PlaybackControl();
            this.SuspendLayout();
            // 
            // playbackControl
            // 
            this.playbackControl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.playbackControl.EndFrame = new decimal( new int[] {
            100,
            0,
            0,
            0} );
            this.playbackControl.Location = new System.Drawing.Point( 0, 0 );
            this.playbackControl.MaximumFrame = new decimal( new int[] {
            10000,
            0,
            0,
            0} );
            this.playbackControl.MinimumFrame = new decimal( new int[] {
            0,
            0,
            0,
            0} );
            this.playbackControl.Name = "playbackControl";
            this.playbackControl.PlaybackEnd = new decimal( new int[] {
            100,
            0,
            0,
            0} );
            this.playbackControl.PlaybackStart = new decimal( new int[] {
            0,
            0,
            0,
            0} );
            this.playbackControl.Size = new System.Drawing.Size( 920, 70 );
            this.playbackControl.StartFrame = new decimal( new int[] {
            0,
            0,
            0,
            0} );
            this.playbackControl.TabIndex = 0;
            this.playbackControl.Value = new decimal( new int[] {
            0,
            0,
            0,
            65536} );
            this.playbackControl.VCRButtonStyle = PlaybackControls.VCRControl.ButtonStyle.Animation;
            this.playbackControl.StartFrameChanged += new System.EventHandler( this.playbackControl_StartFrameChanged );
            this.playbackControl.RewindOrGoToStartButtonClick += new System.EventHandler( this.playbackControl_RewindOrGoToStartButtonClick );
            this.playbackControl.PlaybackStartChanged += new System.EventHandler( this.playbackControl_PlaybackStartChanged );
            this.playbackControl.PlaybackEndChanged += new System.EventHandler( this.playbackControl_PlaybackEndChanged );
            this.playbackControl.EndFrameChanged += new System.EventHandler( this.playbackControl_EndFrameChanged );
            this.playbackControl.GoToPreviousOrStepBackwardButtonClick += new System.EventHandler( this.playbackControl_GoToPreviousOrStepBackwardButtonClick );
            this.playbackControl.ValueChanged += new System.EventHandler( this.playbackControl_ValueChanged );
            this.playbackControl.PlayForwardsButtonClick += new System.EventHandler( this.playbackControl_PlayForwardsButtonClick );
            this.playbackControl.GoToNextOrStepForwardButtonClick += new System.EventHandler( this.playbackControl_GoToNextOrStepForwardButtonClick );
            this.playbackControl.PlayBackwardsButtonClick += new System.EventHandler( this.playbackControl_PlayBackwardsButtonClick );
            this.playbackControl.FastForwardOrGoToEndButtonClick += new System.EventHandler( this.playbackControl_FastForwardOrGoToEndButtonClick );
            this.playbackControl.ValueChanging += new System.EventHandler( this.playbackControl_ValueChanging );
            this.playbackControl.PauseButtonClick += new System.EventHandler( this.playbackControl_PauseButtonClick );
            // 
            // AnimCtrlViewForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add( this.playbackControl );
            this.Name = "AnimCtrlViewForm";
            this.Size = new System.Drawing.Size( 920, 70 );
            this.ResumeLayout( false );

        }

        #endregion

        private PlaybackControls.PlaybackControl playbackControl;

    }
}