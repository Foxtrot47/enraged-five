using System;
using System.Collections.Generic;
using System.Text;

using ragCore;
using ragWidgetPage;
using ragWidgets;
using RSG.Base.Logging;

namespace ragAnimCtrlView
{
    public class ragAnimCtrlViewPlugin : WidgetPagePlugIn
    {
        #region WidgetPagePlugIn overrides
        public override string Name
        {
            get
            {
                return "Show Anim Ctrls";
            }
        }

        public override void InitPlugIn( PlugInHostData data )
        {
            base.InitPlugIn( data );
            sm_Instance = this;

            m_MenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift | System.Windows.Forms.Keys.A)));

            // register our WidgetPage
            base.m_pageNameToCreateDelMap[AnimCtrlViewForm.MyName] = new ragWidgetPage.WidgetPage.CreatePageDel( AnimCtrlViewForm.CreatePage );
        }

        public override void RemovePlugIn()
        {
            base.RemovePlugIn();
            AnimCtrlViewForm.Shutdown();
            sm_Instance = null;
        }

        protected override void MainBankManager_InitFinished()
        {
            if ( CheckPluginStatus() )
            {
                AnimCtrlViewForm.Init();
            }
            else
            {
                base.MainBankManager_InitFinished();
            }
        }

        public override void ActivatePlugInMenuItem(object sender, EventArgs args)
        {
            if ( AnimCtrlViewForm.sm_Instance == null || AnimCtrlViewForm.sm_Instance.DockControl == null)
            {
                base.CreateAndOpenView(base.m_pageNameToCreateDelMap[AnimCtrlViewForm.MyName], LogFactory.ApplicationLog);
            }
            else if ( !AnimCtrlViewForm.sm_Instance.DockControl.IsOpen )
            {
                AnimCtrlViewForm.sm_Instance.DockControl.Open( TD.SandDock.WindowOpenMethod.OnScreenActivate );
            }
        }

        public override bool CheckPluginStatus()
        {
            return sm_HostData.MainRageApplication.BankMgr.FindFirstWidgetFromPath( "rage - GlobalAnimation" ) != null;
        }
        #endregion

        private static ragAnimCtrlViewPlugin sm_Instance;

        public static ragAnimCtrlViewPlugin Instance
        {
            get
            {
                return sm_Instance;
            }
        }
    }
}
