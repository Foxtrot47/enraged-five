using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using ragCore;
using ragWidgets;
using ragWidgetPage;
using RSG.Base.Logging;

namespace ragAnimCtrlView
{
    public partial class AnimCtrlViewForm : WidgetPage
    {
        public AnimCtrlViewForm()
        {
            InitializeComponent();
        }

        public AnimCtrlViewForm( String appName, IBankManager bankMgr, IKeyUpProcessor keyProcessor, TD.SandDock.DockControl dockControl, ILog log )
            : base( appName, bankMgr, keyProcessor, dockControl, log )
        {
            InitializeComponent();
        }

        #region Variables
        private WidgetButton m_WidgetPlayForwardBtn;
        private WidgetButton m_WidgetPlayBackwardBtn;
        private WidgetButton m_WidgetPauseBtn;
        private WidgetButton m_WidgetStepBackBtn;
        private WidgetButton m_WidgetStepForwardBtn;
        private WidgetButton m_WidgetGotoEndBtn;
        private WidgetButton m_WidgetGotoStartBtn;

        private WidgetSliderFloat m_WidgetStartFrame;
        private WidgetSliderFloat m_WidgetEndFrame;
        private WidgetSliderFloat m_WidgetPlaybackStartFrame;
        private WidgetSliderFloat m_WidgetPlaybackEndFrame;
        private WidgetSliderFloat m_WidgetCurrentTime;

        public static AnimCtrlViewForm sm_Instance = null;
        #endregion

        #region Properties
        public static string MyName = "Animation Controls";
        #endregion

        #region Overrides
        public override string GetMyName()
        {
            return MyName;
        }
        
        public override string GetTabText()
        {
            return MyName;
        }

        public override bool IsSingleInstance
        {
            get
            {
                return true;
            }
        }
        #endregion

        #region Public Static Functions
        public static ragWidgetPage.WidgetPage CreatePage( String appName, IBankManager bankMgr, IKeyUpProcessor keyProcessor, TD.SandDock.DockControl dockControl, ILog log )
        {
            if ( sm_Instance == null )
            {
                sm_Instance = new AnimCtrlViewForm( appName, bankMgr, keyProcessor, dockControl, log );

                if ( ragAnimCtrlViewPlugin.Instance.CheckPluginStatus() )
                {
                    Init();
                }

                return sm_Instance;
            }
            else
            {
                if (sm_Instance.DockControl == null)
                {
                    sm_Instance.DockControl = dockControl;
                }

                return sm_Instance;
            }
        }

        public static void Init()
        {
            if ( sm_Instance != null )
            {
                sm_Instance.FindWidgetHandles();
            }
        }

        public static void Shutdown()
        {
            if ( sm_Instance != null )
            {
                sm_Instance.ClearView();
                sm_Instance = null;
            }
        }
        #endregion

        #region Private Functions
        private void FindWidgetHandles()
        {
            AnimCtrlViewFormSettings acs = new AnimCtrlViewFormSettings();

            ReferenceGroup( acs.BankRootPath );
            m_WidgetRoot = m_BankManager.FindFirstWidgetFromPath( acs.BankRootPath ) as WidgetGroupBase;
            if ( m_WidgetRoot == null )
                throw new ApplicationException( String.Format( "Failed to bind to bank root using supplied path '{0}'", acs.BankRootPath ) );

            m_WidgetPlayForwardBtn = m_WidgetRoot.FindFirstWidgetFromPath( acs.PlayForwardLabel ) as WidgetButton;
            if ( m_WidgetPlayForwardBtn == null )
                throw new ApplicationException( String.Format( "Failed to bind to 'PlayForward' widget using the supplied label '{0}'", acs.PlayForwardLabel ) );

            m_WidgetPlayBackwardBtn = m_WidgetRoot.FindFirstWidgetFromPath( acs.PlayBackwardLabel ) as WidgetButton;
            if ( m_WidgetPlayBackwardBtn == null )
                throw new ApplicationException( String.Format( "Failed to bind to 'PlayBackward' widget using the supplied label '{0}'", acs.PlayBackwardLabel ) );

            m_WidgetPauseBtn = m_WidgetRoot.FindFirstWidgetFromPath( acs.PauseLabel ) as WidgetButton;
            if ( m_WidgetPauseBtn == null )
                throw new ApplicationException( String.Format( "Failed to bind to 'Pause' widget using the supplied label '{0}'", acs.PauseLabel ) );

            m_WidgetStepBackBtn = m_WidgetRoot.FindFirstWidgetFromPath( acs.StepBackwardLabel ) as WidgetButton;
            if ( m_WidgetStepBackBtn == null )
                throw new ApplicationException( String.Format( "Failed to bind to 'StepBackward' widget using the supplied label '{0}'", acs.StepBackwardLabel ) );

            m_WidgetStepForwardBtn = m_WidgetRoot.FindFirstWidgetFromPath( acs.StepForwardLabel ) as WidgetButton;
            if ( m_WidgetStepForwardBtn == null )
                throw new ApplicationException( String.Format( "Failed to bind to 'StepForward' widget using the supplied label '{0}'", acs.StepForwardLabel ) );

            m_WidgetGotoEndBtn = m_WidgetRoot.FindFirstWidgetFromPath( acs.GotoEndLabel ) as WidgetButton;
            if ( m_WidgetGotoEndBtn == null )
                throw new ApplicationException( String.Format( "Failed to bind to 'GotoEnd' widget using the supplied label '{0}'", acs.GotoEndLabel ) );

            m_WidgetGotoStartBtn = m_WidgetRoot.FindFirstWidgetFromPath( acs.GotoStartLabel ) as WidgetButton;
            if ( m_WidgetGotoStartBtn == null )
                throw new ApplicationException( String.Format( "Failed to bind to 'GotoStart' widget using the supplied label '{0}'", acs.GotoStartLabel ) );

            m_WidgetStartFrame = m_WidgetRoot.FindFirstWidgetFromPath( "Start Frame" ) as WidgetSliderFloat;
            if ( m_WidgetStartFrame == null )
            {
                throw new ApplicationException( "Failed to bind to 'Start Frame' widget." );
            }
            else
            {
                m_WidgetStartFrame.ValueChanged += new System.EventHandler( WidgetStartFrame_ValueChanged );
            }

            m_WidgetEndFrame = m_WidgetRoot.FindFirstWidgetFromPath( "End Frame" ) as WidgetSliderFloat;
            if ( m_WidgetEndFrame == null )
            {
                throw new ApplicationException( "Failed to bind to 'End Frame' widget." );
            }
            else
            {
                m_WidgetEndFrame.ValueChanged += new System.EventHandler( WidgetEndFrame_ValueChanged );
            }

            m_WidgetPlaybackStartFrame = m_WidgetRoot.FindFirstWidgetFromPath( "Playback Start Frame" ) as WidgetSliderFloat;
            if ( m_WidgetPlaybackStartFrame == null )
            {
                throw new ApplicationException( "Failed to bind to 'Playback Start Frame' widget." );
            }
            else
            {
                m_WidgetPlaybackStartFrame.ValueChanged += new System.EventHandler( WidgetPlaybackStartFrame_ValueChanged );
            }

            m_WidgetPlaybackEndFrame = m_WidgetRoot.FindFirstWidgetFromPath( "Playback End Frame" ) as WidgetSliderFloat;
            if ( m_WidgetPlaybackEndFrame == null )
            {
                throw new ApplicationException( "Failed to bind to 'Playback End Frame' widget." );
            }
            else
            {
                m_WidgetPlaybackEndFrame.ValueChanged += new System.EventHandler( WidgetPlaybackEndFrame_ValueChanged );
            }

            m_WidgetCurrentTime = m_WidgetRoot.FindFirstWidgetFromPath( "Current Frame" ) as WidgetSliderFloat;
            if ( m_WidgetCurrentTime == null )
            {
                throw new ApplicationException( "Failed to bind to 'Current Frame' widget." );
            }
            else
            {
                m_WidgetCurrentTime.ValueChanged += new System.EventHandler( WidgetCurrentTime_ValueChanged );
            }
        }
        #endregion

        #region Event Handlers
        private void playbackControl_EndFrameChanged( object sender, EventArgs e )
        {
            if ( m_WidgetEndFrame != null )
            {
                try
                {
                    m_WidgetEndFrame.Value = (float)Convert.ToDouble( this.playbackControl.EndFrame );
                }
                catch ( Exception ex )
                {
                    Log.ToolExceptionCtx("AnimCtrlViewForm", ex, "Could not convert End Frame to float.", ex);
                }
            }
        }

        private void playbackControl_FastForwardOrGoToEndButtonClick( object sender, EventArgs e )
        {
            if ( m_WidgetGotoEndBtn != null )
            {
                m_WidgetGotoEndBtn.Activate();
            }
        }

        private void playbackControl_PauseButtonClick( object sender, EventArgs e )
        {
            if ( m_WidgetPauseBtn != null )
            {
                m_WidgetPauseBtn.Activate();
            }
        }

        private void playbackControl_PlaybackEndChanged( object sender, EventArgs e )
        {
            if ( m_WidgetPlaybackEndFrame != null )
            {
                try
                {
                    m_WidgetPlaybackEndFrame.Value = (float)Convert.ToDouble( this.playbackControl.PlaybackEnd );
                }
                catch ( Exception ex )
                {
                    Log.ToolExceptionCtx("AnimCtrlViewForm", ex, "Could not convert Playback End to float.");
                }
            }
        }

        private void playbackControl_PlaybackStartChanged( object sender, EventArgs e )
        {
            if ( m_WidgetPlaybackStartFrame != null )
            {
                try
                {
                    m_WidgetPlaybackStartFrame.Value = (float)Convert.ToDouble( this.playbackControl.PlaybackStart );
                }
                catch ( Exception ex )
                {
                    Log.ToolExceptionCtx("AnimCtrlViewForm", ex, "Could not convert Playback Start to float.");
                }
            }
        }

        private void playbackControl_PlayBackwardsButtonClick( object sender, EventArgs e )
        {
            if ( m_WidgetPlayBackwardBtn != null )
            {
                m_WidgetPlayBackwardBtn.Activate();
            }
        }

        private void playbackControl_PlayForwardsButtonClick( object sender, EventArgs e )
        {
            if ( m_WidgetPlayForwardBtn != null )
            {
                m_WidgetPlayForwardBtn.Activate();
            }
        }

        private void playbackControl_RewindOrGoToStartButtonClick( object sender, EventArgs e )
        {
            if ( m_WidgetGotoStartBtn != null )
            {
                m_WidgetGotoStartBtn.Activate();
            }
        }

        private void playbackControl_StartFrameChanged( object sender, EventArgs e )
        {
            if ( m_WidgetStartFrame != null )
            {
                try
                {
                    m_WidgetStartFrame.Value = (float)Convert.ToDouble( this.playbackControl.StartFrame );
                }
                catch ( Exception ex )
                {
                    Log.ToolExceptionCtx("AnimCtrlViewForm", ex, "Could not convert Start Frame to float.");
                }
            }
        }

        private void playbackControl_GoToPreviousOrStepBackwardButtonClick( object sender, EventArgs e )
        {
            if ( m_WidgetStepBackBtn != null )
            {
                m_WidgetStepBackBtn.Activate();
            }
        }

        private void playbackControl_GoToNextOrStepForwardButtonClick( object sender, EventArgs e )
        {
            if ( m_WidgetStepForwardBtn != null )
            {
                m_WidgetStepForwardBtn.Activate();
            }
        }

        private void playbackControl_ValueChanged( object sender, EventArgs e )
        {
            if ( m_WidgetCurrentTime != null )
            {
                try
                {
                    m_WidgetCurrentTime.Value = (float)Convert.ToDouble( this.playbackControl.Value );
                }
                catch ( Exception ex )
                {
                    Log.ToolExceptionCtx("AnimCtrlViewForm", ex, "Could not convert Value to float.");
                }
            }
        }

        private void WidgetStartFrame_ValueChanged( object sender, System.EventArgs e )
        {
            if ( m_WidgetStartFrame != null )
            {
                try
                {
                    this.playbackControl.StartFrame = Convert.ToDecimal( m_WidgetStartFrame.Value );
                }
                catch ( Exception ex )
                {
                    Log.ToolExceptionCtx("AnimCtrlViewForm", ex, "Could not convert Start Frame to decimal.");
                }
            }
        }

        private void WidgetEndFrame_ValueChanged( object sender, System.EventArgs e )
        {
            if ( m_WidgetEndFrame != null )
            {
                try
                {
                    this.playbackControl.EndFrame = Convert.ToDecimal( m_WidgetEndFrame.Value );
                }
                catch ( Exception ex )
                {
                    Log.ToolExceptionCtx("AnimCtrlViewForm", ex, "Could not convert End Frame to decimal.");
                }
            }
        }

        private void WidgetPlaybackStartFrame_ValueChanged( object sender, System.EventArgs e )
        {
            if ( m_WidgetPlaybackStartFrame != null )
            {
                try
                {
                    this.playbackControl.PlaybackStart = Convert.ToDecimal( m_WidgetPlaybackStartFrame.Value );
                }
                catch ( Exception ex )
                {
                    Log.ToolExceptionCtx("AnimCtrlViewForm", ex, "Could not convert Playback Start Frame to decimal.");
                }
            }
        }

        private void WidgetPlaybackEndFrame_ValueChanged( object sender, System.EventArgs e )
        {
            if ( m_WidgetPlaybackEndFrame != null )
            {
                try
                {
                    this.playbackControl.PlaybackEnd = Convert.ToDecimal( m_WidgetPlaybackEndFrame.Value );
                }
                catch ( Exception ex )
                {
                    Log.ToolExceptionCtx("AnimCtrlViewForm", ex, "Could not convert Playback End Frame to decimal.");
                }
            }
        }

        private void playbackControl_ValueChanging( object sender, EventArgs e )
        {
            if ( m_WidgetCurrentTime != null )
            {
                try
                {
                    m_WidgetCurrentTime.Value = (float)Convert.ToDouble(this.playbackControl.Value);
                }
                catch ( Exception ex )
                {
                    Log.ToolExceptionCtx("AnimCtrlViewForm", ex, "Could not convert Current Time to decimal.");
                }
            }
        }

        private void WidgetCurrentTime_ValueChanged( object sender, System.EventArgs e )
        {
            if ( m_WidgetCurrentTime != null )
            {
                try
                {
                    this.playbackControl.Value = Convert.ToDecimal( m_WidgetCurrentTime.Value );
                }
                catch ( Exception ex )
                {
                    Log.ToolExceptionCtx("AnimCtrlViewForm", ex, "Could not convert Current Time to decimal.");
                }
            }
        }
        #endregion
    }
}