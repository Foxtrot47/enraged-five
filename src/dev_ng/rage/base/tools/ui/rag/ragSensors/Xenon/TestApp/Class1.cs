using System;
using ragSensorXenon;
using System.Windows.Forms;

using ragCore;

namespace TestApp
{
	/// <summary>
	/// Summary description for Class1.
	/// </summary>
	class Class1
	{
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main(string[] args)
		{            
            Sensor.OutputMessageHandler = new Sensor.OutputMessageDel( OutputMessage );
			ragSensorXenon.SensorXenon sensor = new ragSensorXenon.SensorXenon();

            Console.WriteLine( "Press Q to terminate the program." );

            while ( !sensor.TestAppExit )
            {
                if ( Console.KeyAvailable )
                {
                    ConsoleKeyInfo cki;
                    cki = Console.ReadKey( true );
                    if ( cki.Key == ConsoleKey.Q )
                    {
                        sensor.Quit();

                        while ( !sensor.TestAppExit )
                        {
                            // do nothing while we wait
                        }
                        break;
                    }
                }
            }
            
            sensor.Dispose();
		}

        static void OutputMessage( string msg )
        {
            Console.Write( msg );
        }
	}
}
