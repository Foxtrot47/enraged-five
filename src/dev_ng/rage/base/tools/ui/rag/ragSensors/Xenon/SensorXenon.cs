using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Security.Permissions;
using System.Threading;

using ragCore;
using RSG.Base.Forms;
using XDevkit;

namespace ragSensorXenon
{
	/// <summary>
	/// Summary description for Class1.
	/// </summary>
	public class SensorXenon : Sensor
    {
        public SensorXenon()
        {
            m_XboxConsole = null;

            try
            {
                m_XboxManager = new XboxManagerClass();

                if ( (Sensor.MainApplicationIpAddress != null) && (Sensor.MainApplicationIpAddress != string.Empty) )
                {
                    // search for the console on which the main application is running
                    foreach ( string consoleName in m_XboxManager.Consoles )
                    {
                        XboxConsole console = m_XboxManager.OpenConsole( consoleName );
                        uint ip = console.IPAddressTitle;
                        ip = SWAPL( ip );
                        IPAddress addr = new IPAddress( ip );

                        if ( addr.ToString() == Sensor.MainApplicationIpAddress )
                        {
                            m_XboxConsole = console;
                            break;
                        }
                    }
                }
                else
                {
                    m_XboxConsole = m_XboxManager.OpenConsole( m_XboxManager.DefaultConsole );
                }

                if ( m_XboxConsole != null )
                {
                    Sensor.OutputMessage( "[RAG SENSOR XBOX 360] - Connected\n" );

                    try
                    {
                        m_XboxConsole.OnStdNotify += new XboxEvents_OnStdNotifyEventHandler(xboxConsole_OnStdNotify);
                    }
                    catch (System.Runtime.InteropServices.COMException)
                    {
                        rageMessageBox.ShowExclamation("Unable to link to the Xbox 360 standard notification event.  Important Xbox events will not be outputted to RAG but the program can continue.\nCold reboot your console to restore functionality.\n", "Xbox 360 Connection Error");
                    }

                    m_XboxConsole.DebugTarget.ConnectAsDebugger( null, XboxDebugConnectFlags.Force );
                }
                else
                {
                    throw (new Exception( "Could not find a console to connect to." ));
                }

                m_thread = new Thread( new ThreadStart( ThreadProc ) );
                m_thread.Name = "Rag Sensor XBox 360";
                m_thread.Start();

                Console.WriteLine( "Started {0} Thread", m_thread.Name );
            }
            catch ( Exception e )
            {
                Console.WriteLine(
                    String.Format( "{0}\n\nYou may continue normally but when you are finished, the game and Rag may not automatically\nterminate the other like they do under normal conditions.  You will have to do that manually.", e.ToString() ), 
                    "Could Not Connect to XBox 360" );
                
                // Display an error message if connecting fails. Translating
                // error codes to text is not currently supported.
                if ( m_XboxConsole != null )
                {
                    m_XboxConsole.OnStdNotify -= new XboxEvents_OnStdNotifyEventHandler( xboxConsole_OnStdNotify );
                    m_XboxConsole.DebugTarget.DisconnectAsDebugger();
                }                
            }
        }

        #region Overrides
        public override bool HasStopped
		{
			get
			{
				return m_HasStopped;
			}
		}

		public override bool HasQuit
		{
			get
			{
				return m_HasQuit;
			}
		}

		public override string Platform 
		{
			get
			{
				return "Xbox360";
			}
		}

        public override bool CanTakeScreenShot
        {
            get
            {
                return true;
            }
        }

        public override void Quit()
        {
            ResetConsole();
        }

        public override bool SaveScreenShot( string filename )
        {
            try
            {
                if ( m_XboxConsole != null )
                {
                    m_XboxConsole.ScreenShot( filename );
                    Sensor.OutputMessage( String.Format( "[RAG SENSOR XBOX 360] - Saved Screen Shot '{0}'.\n", filename ) );
                }
            }
            catch ( Exception e )
            {
                Sensor.OutputMessage( String.Format( "[RAG SENSOR XBOX 360] - Screen Shot Error\n{0}\n{1}\n", e.Message, e.StackTrace ) );
                return false;
            }
            
            return true;
        }

        [SocketPermission( SecurityAction.Demand, Unrestricted = true )]
        public override TcpClient GetDebugIpAddress( int port )
        {
            if ( m_XboxConsole == null )
            {
                return base.GetDebugIpAddress( port );
            }

            uint ipAddress = m_XboxConsole.IPAddressTitle;
            ipAddress = SWAPL( ipAddress );
            IPAddress addressStruct = new IPAddress( ipAddress );
            TcpClient client = new TcpClient();
            client.Connect( addressStruct, port );
            return client;

        }
		#endregion        

        #region IDisposable interface
        public override void Dispose()
        {
            Dispose( true );
            GC.SuppressFinalize( this );
        }
        #endregion

        #region Constants
        private IntPtr MENUITEM_RESET = (IntPtr)4005;
        #endregion

		#region Variables
        private bool m_disposed = false;
        private bool m_HasQuit = false;
        private bool m_HasStopped = false;
        private XDevkit.XboxConsole m_XboxConsole = null;
        private XDevkit.XboxManager m_XboxManager = null;
        private bool m_AlreadyReset = false;
        private Thread m_thread;
		#endregion

        public bool TestAppExit = false;

        #region Static Variables
        public static Mutex sm_SysTrayRfsMutex = new Mutex();
        #endregion

        #region Static Functions
        public static Sensor CreateSensorXenon()
		{
			return new SensorXenon();
        }
        #endregion

        #region Private Functions
        private void Dispose( bool disposing )
        {
            if ( !m_disposed )
            {
                if ( disposing )
                {
                    try
                    {
                        if ( m_XboxConsole != null )
                        {
                            m_XboxConsole.DebugTarget.DisconnectAsDebugger();
                        }
                    }
                    catch
                    {

                    }

                    if ( m_thread != null )
                    {
                        m_thread.Abort();
                    }
                }

                m_HasStopped = true;
                m_HasQuit = true;
            }

            m_disposed = true;
        }

        private uint SWAPL( uint s )
        {
            return (s >> 24) | ((s >> 8) & 0xFF00) | ((s << 8) & 0xFF0000) | (s << 24);
        }

        private void OutputStackFrame( IXboxEventInfo eventInformation, ref String theMsg )
        {
            theMsg += "\n\nStack Frame: \n";

            // walk the stack:
            XDevkit.IXboxStackFrame context = eventInformation.Info.Thread.TopOfStack;
            while ( context != null )
            {
                theMsg += "\t0x" + context.FunctionInfo.BeginAddress.ToString( "x" ) + "\n";
                context = context.NextStackFrame;
            }
        }

        private void ResetConsole()
        {
            if ( !m_AlreadyReset )
            {
                m_AlreadyReset = true;

                try
                {
                    if ( m_XboxConsole != null )
                    {
                        Sensor.OutputMessage( "[RAG SENSOR XBOX 360] - Rebooting...\n" );

                        m_XboxConsole.Reboot( null, null, null, XboxRebootFlags.Title );
                    }
                }
                catch ( Exception e )
                {
                    Sensor.OutputMessage( String.Format( "Error: [RAG SENSOR XBOX 360] - Could not reboot.\n{0}\n{1}\n", e.Message, e.StackTrace ) );
                }
            }

            this.TestAppExit = true;
        }

        private void ThreadProc()
        {
            Process[] sysTrayRfsProcesses = null;
            try
            {
                sysTrayRfsProcesses = Process.GetProcessesByName( "SysTrayRfs" );
            }
            catch 
            {

            }

            while ( true )
            {
                try
                {
                    if ( sysTrayRfsProcesses != null )
                    {
                        foreach ( Process p in sysTrayRfsProcesses )
                        {
                            if ( p.HasExited )
                            {
                                m_HasQuit = true;
                                ResetConsole();
                            }
                        }
                    }

                    Thread.Sleep( 100 );
                }
                catch ( ThreadAbortException )
                {
                    Console.WriteLine( "Ended {0} Thread", m_thread.Name );
                    break;
                }
            }
        }
        #endregion

        #region XboxConsole Event Handlers
        private void xboxConsole_OnStdNotify( XboxDebugEventType eventCode, IXboxEventInfo eventInformation )
        {
            switch ( eventCode )
            {
                case XDevkit.XboxDebugEventType.RIP:
                    {
                        string theMsg = String.Format( "Fatal Error: [RAG SENSOR XBOX 360] - RIP - {0}\n", eventInformation.Info.Message );

                        OutputStackFrame( eventInformation, ref theMsg );
                        Sensor.OutputMessage( theMsg );
                        Thread.Sleep( 1000 );
                        m_HasStopped = true;
                        m_HasQuit = true;
                        ResetConsole();
                    }
                    break;

                case XDevkit.XboxDebugEventType.Exception:
                    {
                        String theMsg = "[RAG SENSOR XBOX 360] - ";

                        // special handling for starting a thread:
                        if ( eventInformation.Info.Code == 0x406d1388 )
                        {
                            theMsg += "New thread started\n";
                        }
                        else
                        {
                            Thread.Sleep( 5000 );

                            theMsg = "Error: " + theMsg;
                            theMsg = "The instruction at address 0x" + eventInformation.Info.Address.ToString( "x" ) + " referenced memory\n";
                            if ( eventInformation.Info.Parameters[0] != 0 )
                                theMsg += " at address 0x" + eventInformation.Info.Parameters[1].ToString( "x" ) + ". The memory could not be written.\n";
                            else
                                theMsg += " at address 0x" + eventInformation.Info.Parameters[1].ToString( "x" ) + ". The memory could not be read.\n";
                        }

                        //OutputStackFrame(eventInformation,ref theMsg);
                        Sensor.OutputMessage( theMsg );
                        // don't quit yet - give the app the opportunity to continue
                        //m_HasStopped=true;
                        //m_HasQuit=true;		
                    }
                    break;

                case XDevkit.XboxDebugEventType.ExecStateChange:
                    switch ( eventInformation.Info.ExecState )
                    {
                        case XboxExecutionState.Stopped:
                            m_HasStopped = true;
                            break;

                        case XboxExecutionState.Running:
                            m_HasStopped = false;
                            break;

                        case XboxExecutionState.Rebooting:
                        case XboxExecutionState.RebootingTitle:
                            m_HasStopped = true;
                            m_HasQuit = true;
                            ResetConsole();
                            break;
                    }
                    break;
            }
        }
        #endregion
    }

	public class External : ragCore.IWidgetPlugIn
	{
        public void AddHandlers( BankRemotePacketProcessor packetProcessor )
		{
			Sensor.Map["Xbox360"]=new Sensor.CreateSensorDel(SensorXenon.CreateSensorXenon);
		}

        public void RemoveHandlers( BankRemotePacketProcessor packetProcessor )
		{
			Sensor.Map.Remove("Xbox360");
		}
	}
}
