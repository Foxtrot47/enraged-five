using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Windows.Forms;

using ragCore;
using ragSensorPS3;
using ps3tmapiInterface;

namespace TestApp
{
    class Program
    {
        [STAThread]
        static void Main( string[] args )
        {
            Console.WriteLine( "[TEST APP] Press Q to terminate the program." );

            Sensor.OutputMessageHandler = new Sensor.OutputMessageDel( OutputMessage );
            SensorPS3 sensor = new SensorPS3();

            while ( !sensor.TestAppExit )
            {
                if ( Console.KeyAvailable )
                {
                    ConsoleKeyInfo cki;
                    cki = Console.ReadKey( true );
                    if ( cki.Key == ConsoleKey.Q )
                    {
                        Console.WriteLine( "[TEST APP] QUITTING" );

                        sensor.Quit();
                        
                        while ( !sensor.TestAppExit )
                        {
                            // do nothing while we wait
                        }
                        break;
                    }
                }
            }
            
            sensor.Dispose();
        }

        static void OutputMessage( string msg )
        {
            Console.Write( msg );
        }
    }
}
