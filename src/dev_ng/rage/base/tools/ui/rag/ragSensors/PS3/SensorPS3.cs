using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using System.Text;
using System.Threading;

using ps3tmapiInterface;
using ragCore;
using RSG.Base.Forms;

namespace ragSensorPS3
{
    public class SensorPS3 : Sensor
    {
        public SensorPS3()
        {
            m_readThread = new Thread( new ThreadStart( ReadThreadProc ) );
            m_readThread.Name = "Rag Sensor Playstation3";
            m_readThread.Priority = ThreadPriority.Lowest;
            m_readThread.Start();

            Console.WriteLine( "Started {0} Thread", m_readThread.Name );
        }

        #region Overrides
        public override bool HasStopped
        {
            get
            {
                return m_HasStopped;
            }
        }

        public override bool HasQuit
        {
            get
            {
                return m_HasQuit;
            }
        }

        public override string Platform
        {
            get
            {
                return "PlayStation3";
            }
        }

        public override void Quit()
        {
            ResetSystrayRfs();

            m_HasQuit = true;
        }

        [SocketPermission( SecurityAction.Demand, Unrestricted = true )]
        public override TcpClient GetDebugIpAddress( int port )
        {
            if ( (Sensor.MainApplicationIpAddress != null) && (Sensor.MainApplicationIpAddress != string.Empty) )
            {
                TcpClient client = new TcpClient();
                client.ExclusiveAddressUse = false;
                client.Connect( Sensor.MainApplicationIpAddress, port );
                return client;
            }

            throw (new Exception( "[RAG SENSOR Playstation3] - Unable to determine Debug IP Address of PS3." ));
        }
        #endregion

        #region IDisposable interface
        public override void Dispose()
        {
            Dispose( true );
            GC.SuppressFinalize( this );
        }
        #endregion

        #region Constants
        private const string c_messagePrefix = "[RAG SENSOR Playstation3] - ";
        #endregion

        #region Variables
        private bool m_disposed = false;
        private bool m_HasStopped = false;
        private bool m_HasQuit = false;
        private bool m_AlreadyReset = false;
        private bool m_performReset = false;
        private bool m_usingDebugger = false;
        private bool m_testAppExit = false;

        private Thread m_readThread;
        private PS3Target m_target;
        #endregion

        #region Properties
        public bool TestAppExit
        {
            get
            {
                return m_testAppExit;
            }
            set
            {
                m_testAppExit = value;
            }
        }
        #endregion

        #region Static Variables
        public static Mutex sm_SysTrayRfsMutex = new Mutex();
        #endregion

        #region Static Functions
        public static Sensor CreateSensorPS3()
        {
            return new SensorPS3();
        }
        #endregion

        #region Private Functions
        private void Dispose( bool disposing )
        {
            if ( !m_disposed )
            {
                if ( disposing )
                {
                    // make sure we allow the thread to do this if it has been requested.
                    while ( m_readThread.IsAlive && m_performReset )
                    {
                        Thread.Sleep( 100 );
                    }

                    m_readThread.Abort();
                }

                m_HasStopped = true;
                m_HasQuit = true;
            }

            m_disposed = true;
        }

        private void ResetSystrayRfs()
        {
            if ( m_AlreadyReset == false )
            {
                m_AlreadyReset = true;

                m_performReset = true;
            }

            this.TestAppExit = true;
        }

        private void ReadThreadProc()
        {
            try
            {
                m_target = new PS3Target();

                m_target.ConnectError += new ps3tmapiInterface.MessageEventHandler( target_ConnectError );
                m_target.ConnectInfo += new ps3tmapiInterface.MessageEventHandler( target_ConnectInfo );
                m_target.ConnectStatus += new ConnectStatusEventHandler(target_ConnectStatus);
                m_target.DebugEvent += new DebugEventEventHandler(target_DebugEvent);
                m_target.OutputTTY = false;
                m_target.TargetEvent += new TargetEventEventHandler(target_TargetEvent);
                m_target.UnhandledCrash += new ps3tmapiInterface.MessageEventHandler( target_UnhandledCrash );
                m_target.UnitStatus += new UnitStatusEventHandler(target_UnitStatus);

                m_target.Connect((SensorPS3.PS3TargetIpAddress == null) ? string.Empty : SensorPS3.PS3TargetIpAddress);
                if (m_target.IsConnected)
                {
                    Sensor.OutputMessage("[RAG SENSOR Playstation3] - Connected\n");
                }
                else
                {
                    rageMessageBox.ShowExclamation(
                        "You may continue normally but when you are finished, the game and Rag may not automatically\nterminate the other like they do under normal conditions.  You will have to do that manually.",
                        "Could Not Connect to Playstation3");
                }

                Process[] ps3DebuggerProcesses = Process.GetProcessesByName("ps3debugger");
                Process[] sysTrayRfsProcesses = Process.GetProcessesByName("SysTrayRfs");

                m_usingDebugger = ps3DebuggerProcesses.Length > 0;

                while (m_target.IsConnected)
                {
                    m_target.Update();

                    if (m_performReset)
                    {
                        Sensor.OutputMessage("[RAG SENSOR Playstation3] - Rebooting...\n");

                        m_performReset = false;
                        m_target.Reset();
                    }
                    else if (m_usingDebugger)
                    {
                        foreach (Process p in ps3DebuggerProcesses)
                        {
                            if (p.HasExited)
                            {
                                ResetSystrayRfs();
                            }
                        }
                    }

                    foreach (Process p in sysTrayRfsProcesses)
                    {
                        if (p.HasExited)
                        {
                            m_HasQuit = true;
                            ResetSystrayRfs();
                        }
                    }

                    Thread.Sleep(20);
                }
            }
            finally
            {
                if (m_target != null)
                {
                    m_target.Disconnect();
                    m_target.Dispose();
                    m_target = null;
                }

                m_HasQuit = true;
                this.TestAppExit = true;
                
                Console.WriteLine( "Ended {0} Thread", m_readThread.Name );
            }
        }
        #endregion

        #region Event Handlers
        private void target_ConnectError( object sender, MessageEventArgs e )
        {
            Sensor.OutputMessage( c_messagePrefix + e.Message + "\n");
        }

        private void target_ConnectInfo( object sender, MessageEventArgs e )
        {
            Sensor.OutputMessage( c_messagePrefix + e.Message + "\n");
        }

        private void target_ConnectStatus( object sender, ConnectStatusEventArgs e )
        {
            if ( e.Status == ConnectStatus.NOT_CONNECTED )
            {
                if ( !m_AlreadyReset )
                {
                    Sensor.OutputMessage( "Error: " + c_messagePrefix + e.Status.ToString() + "\n");
                }
                else
                {
                    Sensor.OutputMessage( c_messagePrefix + e.Status.ToString() + "\n");
                }
            }
            else
            {
                Sensor.OutputMessage( c_messagePrefix + e.Status.ToString() + "\n");
            }
        }

        private void target_DebugEvent( object sender, DebugEventEventArgs e )
        {
            switch ( e.Event )
            {
                case DebugEvent.PPU_THREAD_CREATE:
                case DebugEvent.PPU_THREAD_EXIT:
                    Sensor.OutputMessage( c_messagePrefix + e.AsString() + "\n");
                    break;
                case DebugEvent.PPU_EXP_ALIGNMENT:
                case DebugEvent.PPU_EXP_DABR_MATCH:
                case DebugEvent.PPU_EXP_DATA_HTAB_MISS:
                case DebugEvent.PPU_EXP_DATA_SLB_MISS:
                case DebugEvent.PPU_EXP_FLOAT:
                case DebugEvent.PPU_EXP_ILL_INST:
                case DebugEvent.PPU_EXP_PREV_INT:
                case DebugEvent.PPU_EXP_TEXT_HTAB_MISS:
                case DebugEvent.PPU_EXP_TEXT_SLB_MISS:
                case DebugEvent.PPU_EXP_TRAP:
                    Sensor.OutputMessage( "Error: " + c_messagePrefix + e.AsString() + "\n");
                    break;
                default:
                    break;
            }
        }

        private void target_TargetEvent( object sender, TargetEventEventArgs e )
        {
            Sensor.OutputMessage( c_messagePrefix + e.AsString() + "\n");
        }

        private void target_UnhandledCrash( object sender, MessageEventArgs e )
        {
            Sensor.OutputMessage( "Error: " + c_messagePrefix + e.Message + "\n");

            ResetSystrayRfs();
        }

        private void target_UnitStatus( object sender, UnitStatusEventArgs e )
        {
            if ( e.Status == UnitStatus.NOT_CONNECTED )
            {
                if ( !m_AlreadyReset )
                {
                    Sensor.OutputMessage( "Error: " + c_messagePrefix + e.Status.ToString() + "\n");
                    Thread.Sleep( 1000 );

                    m_HasStopped = true;
                    m_HasQuit = true;

                    m_AlreadyReset = true;
                }
                else
                {
                    Sensor.OutputMessage( c_messagePrefix + e.Status.ToString() + "\n");
                }
            }
            else if ( (e.Status == UnitStatus.RESET) || (e.Status == UnitStatus.RESETTING) )
            {
                if ( !m_AlreadyReset )
                {
                    m_HasStopped = true;
                    m_HasQuit = true;

                    m_AlreadyReset = true;
                }
            }
        }
        #endregion
    }

    public class External : ragCore.IWidgetPlugIn
    {
        public void AddHandlers()
        {
            Sensor.Map["PlayStation3"] = new Sensor.CreateSensorDel( SensorPS3.CreateSensorPS3 );
        }

        public void RemoveHandlers()
        {
            Sensor.Map.Remove( "PlayStation3" );
        }
    }
}
