using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

using ragCore;
using System.Net.Sockets;
using RSG.Base.Logging;

namespace rag
{
	/// <summary>
	/// Summary description for Input.
	/// </summary>
	public class Input
    {
        #region Enums
        public enum CommandKeyboard
		{
			// keyboard commands:
			QUEUE_CHAR			=0x0102,	// WM_CHAR
			QUEUE_SYSCHAR		=0x0106,	// WM_SYSCHAR

			QUEUE_KEYDOWN		=0x0100,	// WM_KEYDOWN
			QUEUE_SYSKEYDOWN	=0x0104,	// WM_SYSKEYDOWN

			QUEUE_KEYUP			=0x0101,	// WM_KEYDOWN
			QUEUE_SYSKEYUP		=0x0105,	// WM_SYSKEYDOWN

			QUEUE_SHIFT			=0x0004,	// MK_SHIFT
			QUEUE_CONTROL		=0x0008,	// MK_CONTROL
		};

		public enum CommandMouse
		{
			// mouse commands:
			QUEUE_MOUSEMOVE		=0x0200,	// WM_MOUSEMOVE

			QUEUE_LBUTTONDOWN	=0x0201,	// WM_LBUTTONDOWN
			QUEUE_LBUTTONUP		=0x0202,	// WM_LBUTTONUP
	
			QUEUE_RBUTTONDOWN	=0x0204,	// WM_RBUTTONDOWN
			QUEUE_RBUTTONUP		=0x0205,	// WM_RBUTTONUP

			QUEUE_MBUTTONDOWN	=0x0207,	// WM_MBUTTONDOWN
			QUEUE_MBUTTONUP		=0x0208,	// WM_MBUTTONUP

			QUEUE_LBUTTON		=0x0001,	// MK_LBUTTON
			QUEUE_RBUTTON		=0x0002,	// MK_RBUTTON
			QUEUE_MBUTTON       =0x0010		// MK_MBUTTON
		};
        #endregion

        public Input(ILog log)
		{
            _log = log;

			m_Thread = new Thread(new ThreadStart(WritePipeProc));
			m_Thread.Name = "Input Thread";
            m_Thread.Priority = ThreadPriority.Lowest;

			for (int i=0;i<m_CommandArrays.Length;i++)
			{
                m_CommandArrays[i] = new List<byte[]>();
			}

            for ( int i = 0; i < m_Mutex.Length; i++ )
            {
                m_Mutex[i] = new Mutex();
            }
        }

        #region Variables
        private readonly ILog _log;
        private const String LogCtx = "Input";
        private Thread m_Thread = null;
        private bool m_abortThread = false;
        private const int NUM_COMMANDS = 5;
        private const int COMMAND_SIZE = 4 * NUM_COMMANDS;
        private List<byte[]>[] m_CommandArrays = new List<byte[]>[2];
        private int m_WhichBuffer = 0;
        private Mutex[] m_Mutex = new Mutex[2];
        private INamedPipe m_Pipe = null;
        #endregion

        #region Public Functions
        public bool IsValid()
        {
            if ( m_Pipe != null )
            {
                return m_Pipe.IsValid();
            }

            return false;
        }

        public bool IsConnected()
        {
            if ( m_Pipe != null )
            {
                return m_Pipe.IsConnected();
            }

            return false;
        }

        public bool Connect(PipeID pipeId)
		{
            if ( m_Pipe == null )
            {
                m_Pipe = PipeID.CreateNamedPipe();
                m_Thread.Start();
            }

            if ( !m_Pipe.IsConnected() )
            {
                _log.MessageCtx(LogCtx, "Waiting for client to connect to pipe '{0}' on port {1}...", pipeId.ToString(), pipeId.Port);

                if ( m_Pipe.Create( pipeId, false ) )
                {
                    _log.MessageCtx(LogCtx, "Connected to pipe '{0}'.", pipeId.ToString());
                    m_Pipe.NoDelay = true;
                    return true;
                }

                _log.MessageCtx(LogCtx, "Input could not connect to pipe '{0}'!", pipeId.ToString());
                return false;
            }

            return true;
		}

        public void Close()
        {
            m_Thread.Abort();
            m_abortThread = true;

            if ( m_Pipe != null )
            {
                m_Pipe.Close();
                _log.MessageCtx(LogCtx, "Disconnected from pipe '{0}'.", m_Pipe.Name);
                
                m_Pipe = null;
            }
        }

        public void SendKeyboardEvent( CommandKeyboard command, System.Windows.Forms.Keys key )
        {
            byte[] commandBuff = new byte[COMMAND_SIZE];

            // command:
            uint commandInt = (uint)command;
            int i = 0;
            commandBuff[i++] = (byte)(commandInt);
            commandBuff[i++] = (byte)(commandInt >> 8);
            commandBuff[i++] = (byte)(commandInt >> 16);
            commandBuff[i++] = (byte)(commandInt >> 24);

            // key:
            commandInt = (uint)key;
            commandBuff[i++] = (byte)(commandInt);
            commandBuff[i++] = (byte)(commandInt >> 8);
            commandBuff[i++] = (byte)(commandInt >> 16);
            commandBuff[i++] = (byte)(commandInt >> 24);

            // null out rest of command
            for ( ; i < COMMAND_SIZE; i++ )
            {
                commandBuff[i++] = 0;
            }

            WritePipe( commandBuff );
        }

        public void SendMouseEvent( int x, int y, int z, int width, int height )
        {
            SendMouseEvent( CommandMouse.QUEUE_MOUSEMOVE, false, x, y, z, width, height );
        }

        public void SendMouseEvent( System.Windows.Forms.MouseButtons buttons, bool down, int x, int y, int z, int width, int height )
        {
            CommandMouse mouseCommand = CommandMouse.QUEUE_MOUSEMOVE;

            switch ( buttons )
            {
                case System.Windows.Forms.MouseButtons.Left:
                    mouseCommand = down ? CommandMouse.QUEUE_LBUTTONDOWN : CommandMouse.QUEUE_LBUTTONUP;
                    break;

                case System.Windows.Forms.MouseButtons.Middle:
                    mouseCommand = down ? CommandMouse.QUEUE_MBUTTONDOWN : CommandMouse.QUEUE_MBUTTONUP;
                    break;

                case System.Windows.Forms.MouseButtons.Right:
                    mouseCommand = down ? CommandMouse.QUEUE_RBUTTONDOWN : CommandMouse.QUEUE_RBUTTONUP;
                    break;

                default:
                    break;
            }

            SendMouseEvent( mouseCommand, down, x, y, z, width, height );
        }

        public void SendMouseEvent( CommandMouse mouseCommand, bool down, int x, int y, int z, int width, int height )
        {
            byte[] command = new byte[COMMAND_SIZE];

            // command:
            uint commandInt = (uint)mouseCommand;
            int i = 0;
            command[i++] = (byte)(commandInt);
            command[i++] = (byte)(commandInt >> 8);
            command[i++] = (byte)(commandInt >> 16);
            command[i++] = (byte)(commandInt >> 24);

            // mouse:
            command[i++] = (byte)(commandInt);
            command[i++] = (byte)(commandInt >> 8);
            command[i++] = (byte)(commandInt >> 16);
            command[i++] = (byte)(commandInt >> 24);

            // x position:
            RemotePacket.FloatToInt floatToInt;
            floatToInt.u = 0; // appease the compiler
            floatToInt.f = ((float)x) / ((float)width);
            command[i++] = (byte)(floatToInt.u);
            command[i++] = (byte)(floatToInt.u >> 8);
            command[i++] = (byte)(floatToInt.u >> 16);
            command[i++] = (byte)(floatToInt.u >> 24);

            // y position:
            floatToInt.f = ((float)y) / ((float)height);
            command[i++] = (byte)(floatToInt.u);
            command[i++] = (byte)(floatToInt.u >> 8);
            command[i++] = (byte)(floatToInt.u >> 16);
            command[i++] = (byte)(floatToInt.u >> 24);

            // mouse wheel:
            ushort posZ = (ushort)z;
            command[i++] = (byte)(posZ);
            command[i++] = (byte)(posZ >> 8);

            // null out rest of command
            for ( ; i < COMMAND_SIZE; i++ )
            {
                command[i++] = 0;
            }

            WritePipe( command );
        }
        #endregion

        #region Private Functions
        private void WritePipeProc()
        {
            _log.MessageCtx(LogCtx, "Started {0}", m_Thread.Name);

            while ( true )
            {
                try
                {
                    int readBuffer = m_WhichBuffer;
                    if ( (m_Pipe != null) && m_Pipe.IsValid() && (m_CommandArrays[readBuffer].Count > 0) )
                    {
                        m_WhichBuffer = readBuffer == 0 ? 1 : 0;
                        m_Mutex[readBuffer].WaitOne();

                        int allDataSize = m_CommandArrays[readBuffer].Count * COMMAND_SIZE;
                        List<byte> allData = new List<byte>( allDataSize );

                        foreach ( byte[] data in m_CommandArrays[readBuffer] )
                        {
                            allData.AddRange( data );
                        }

                        m_Pipe.WriteData( allData.ToArray(), allDataSize );
                        
                        m_CommandArrays[readBuffer].Clear();
                        m_Mutex[readBuffer].ReleaseMutex();
                    }
                    else if ( m_abortThread )
                    {
                        break;
                    }
                    else
                    {
                        Thread.Sleep( 10 );
                    }
                }
                catch ( SocketException e )
                {
                	if ( e.Message.Contains( "An existing connection was forcibly closed by the remote host" ) )
                    {                        
                        break;
                    }
                }
                catch ( ThreadAbortException )
                {
                    break;
                }
            }

            if ( m_Thread != null )
            {
                _log.MessageCtx(LogCtx, "Ended {0}", m_Thread.Name);
            }
        }

		private void WritePipe(byte[] commandBuff)
		{
			int writeBuffer=m_WhichBuffer;
			m_Mutex[writeBuffer].WaitOne();
			m_CommandArrays[writeBuffer].Add(commandBuff);
			m_Mutex[writeBuffer].ReleaseMutex();
        }
        #endregion
	}
}
