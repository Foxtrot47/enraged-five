using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Xml;

using ragCore;
using ragWidgets;

namespace rag
{
    public partial class FindView : rag.WidgetViewPlaceholder
    {
        protected FindView()
        {
            InitializeComponent();
        }

        public FindView( String appName, IBankManager bankMgr, TD.SandDock.DockControl dockControl )
            : base( appName, bankMgr, dockControl )
        {
            // This call is required by the Windows.Forms Form Designer.
            InitializeComponent();

            InitializeXpanderList( dockControl );

            if ( sm_TabIcon == null )
            {
                sm_TabIcon = global::rag.Properties.Resources.find;
            }

            m_ViewNum = ++sm_ViewNum;

            dockControl.Closed += new EventHandler( dockControl_Closed );
        }

        #region Variables
        private static Bitmap sm_TabIcon;
        private static int sm_ViewNum = 0;
        private static List<string> sm_FoundStrings = new List<string>();
        #endregion

        #region Overrides
        public override Bitmap GetBitmap()
        {
            return sm_TabIcon;
        }

        public override string GetTabText()
        {
            return "Find View " + m_ViewNum;
        }

        public override void DeserializeView( XmlTextReader xmlReader, IBankManager bankMgr )
        {
            base.DeserializeView( xmlReader, bankMgr );

            // read check boxes:
            int index = -1;
            try
            {
                index = Convert.ToInt32( xmlReader["SelectedIndex"] );                
                this.matchCaseCheckBox.Checked = xmlReader["MatchCase"] == "1" ? true : false;                
                this.matchWholeWordCheckBox.Checked = xmlReader["MatchWholeWord"] == "1" ? true : false;                
                this.exactMatchCheckBox.Checked = xmlReader["ExactMatch"] == "1" ? true : false;
                this.searchInHelpDescriptionsCheckBox.Checked = xmlReader["SearchInHelpDesc"] == "1" ? true : false;                
                this.useRegularExpressionsCheckBox.Checked = xmlReader["UseRegExp"] == "1" ? true : false;
                this.includeGroupsCheckBox.Checked = xmlReader["IncludeGroups"] == "1" ? true : false;
                this.displayPathCheckBox.Checked = xmlReader["Display"] == "1" ? true : false;

                xmlReader.Read();

                // read search strings:
                while ( xmlReader.NodeType != XmlNodeType.EndElement && xmlReader.Name == "SearchString" )
                {
                    String theString = xmlReader["string"];                    
                    if ( this.findWhatComboBox.Items.Contains( theString ) == false )
                    {
                        this.findWhatComboBox.Items.Add( theString );
                        if ( sm_FoundStrings.Contains( theString ) == false )
                        {
                            sm_FoundStrings.Add( theString );
                        }
                    }
                    xmlReader.Read();
                }

                // need to do this after the search strings are loaded:
                this.findWhatComboBox.SelectedIndex = index;
                bankMgr.InitFinished += new ragCore.InitFinishedHandler( InitFinished );
            }
            catch
            {
            }
        }

        protected override void SerializeView( XmlTextWriter xmlWriter, String exePathName )
        {
            base.SerializeView( xmlWriter, exePathName );

            // write check boxes:
            xmlWriter.WriteAttributeString( "SelectedIndex", this.findWhatComboBox.SelectedIndex.ToString() );
            xmlWriter.WriteAttributeString( "MatchCase", this.matchCaseCheckBox.Checked ? "1" : "0" );
            xmlWriter.WriteAttributeString( "MatchWholeWord", this.matchWholeWordCheckBox.Checked ? "1" : "0" );
            xmlWriter.WriteAttributeString( "ExactMatch", this.exactMatchCheckBox.Checked ? "1" : "0" );
            xmlWriter.WriteAttributeString( "SearchInHelpDesc", this.searchInHelpDescriptionsCheckBox.Checked ? "1" : "0" );
            xmlWriter.WriteAttributeString( "UseRegExp", this.useRegularExpressionsCheckBox.Checked ? "1" : "0" );
            xmlWriter.WriteAttributeString( "IncludeGroups", this.includeGroupsCheckBox.Checked ? "1" : "0" );
            xmlWriter.WriteAttributeString( "DisplayPath", m_displayPath ? "1" : "0" );

            // write search strings:
            foreach ( String item in sm_FoundStrings )
            {
                xmlWriter.WriteWhitespace( "\n  " );
                xmlWriter.WriteStartElement( "SearchString" );
                xmlWriter.WriteAttributeString( "string", item );
                xmlWriter.WriteEndElement();
            }
        }
        #endregion

        #region Event Handlers
        private void dockControl_Closed( object sender, EventArgs e )
        {
            TD.SandDock.DockControl control = sender as TD.SandDock.DockControl;
            if (control.CloseAction == TD.SandDock.DockControlCloseAction.Dispose)
            {
                m_BankManager.InitFinished -= new ragCore.InitFinishedHandler( InitFinished );
            }
        }

        private void findWhatComboBox_KeyPress( object sender, KeyPressEventArgs e )
        {
            if ( e.KeyChar == '\r' )
            {
                FindWidgets( this.findWhatComboBox.Text );
            }
        }

        private void findButton_Click( object sender, EventArgs e )
        {
            FindWidgets( this.findWhatComboBox.Text );
        }

        private void findWhatComboBox_SelectedValueChanged( object sender, EventArgs e )
        {
            FindWidgets( this.findWhatComboBox.Text );
        }

        private void displayPathCheckBox_CheckedChanged( object sender, EventArgs e )
        {
            SetDisplayPath( this.displayPathCheckBox.Checked );
        }
        #endregion

        #region Private Functions
        private void InitFinished()
        {
            if ( this.findWhatComboBox.Text != null )
            {
                FindWidgets( this.findWhatComboBox.Text );
            }
        }

        private void FindWidgets( string findString )
        {
            this.findWhatComboBox.Focus();
            this.findWhatComboBox.SelectAll();

            if ( String.IsNullOrEmpty( findString ) )
            {
                return;
            }

            // wrapped with a mutex to prevent destroying a widget while it's being viewed
            BankRemotePacketProcessor.MakingVisibleMutex.WaitOne();

            List<Widget> foundWidgets = new List<Widget>();
            WidgetBank.FindWidgetsWithMatchingString( m_BankManager, this.includeGroupsCheckBox.Checked, this.matchCaseCheckBox.Checked,
                this.matchWholeWordCheckBox.Checked, this.exactMatchCheckBox.Checked, this.searchInHelpDescriptionsCheckBox.Checked,
                findString, ref foundWidgets, -1 );

            // this will remove all widgets
            base.ClearView();

            foreach ( Widget widget in foundWidgets )
            {
                AddWidget( widget );
            }

            // store the string so we can easy select it from the combo box later:
            if ( m_widgetsAdded.Count > 0 )
            {
                if ( !sm_FoundStrings.Contains( findString ) )
                {
                    sm_FoundStrings.Add( findString );
                }

                if ( !this.findWhatComboBox.Items.Contains( findString ) )
                {
                    this.findWhatComboBox.Items.Add( findString );
                }
            }

            BankRemotePacketProcessor.MakingVisibleMutex.ReleaseMutex();
        }
        #endregion

        #region Public Static Functions
        public static WidgetViewBase CreateFindView( String appName, IBankManager bankMgr, TD.SandDock.DockControl dockControl )
        {
            FindView findView = new FindView( appName, bankMgr, dockControl );
            dockControl.CloseAction = TD.SandDock.DockControlCloseAction.Dispose;
            return findView;
        }

        public static void AddContextMenuItems( IWidgetView currentWidgetView, ContextMenuStrip menu, Widget widget, WidgetVisible widgetVisible )
        {
            if ( widget == null )
            {
                return;
            }

            FindView findView = currentWidgetView as FindView;
            if ( findView != null )
            {
                // Add menu items for finding and showing widgets in each of the bank tree views
                bool addedSeparator = false;
                foreach ( IDockableView view in DockableViewManager.Instance.DockableViews )
                {
                    if ( view is BankTreeView )
                    {
                        BankTreeView bankView = (BankTreeView)view;

                        if ( !addedSeparator && menu.Items.Count > 0 )
                        {
                            menu.Items.Add( new ToolStripSeparator() );
                            addedSeparator = true;
                        }

                        ToolStripMenuItem findItem = new ToolStripMenuItem( "Show In " + bankView.GetTabText() );
                        findItem.Enabled = false;
                        menu.Items.Add( findItem );

                        WidgetTreeNode treeNode = bankView.FindDeepestMatchingTreeNode( widget.Path );
                        if ( treeNode != null )
                        {
                            WidgetFinder finder = new WidgetFinder( bankView, treeNode );
                            findItem.Enabled = true;
                            findItem.Click += new EventHandler( finder.FindWidgetHandler );
                        }
                    }
                }
            }
        }

        public static void ClearStatics()
        {
            sm_ViewNum = 0;
            sm_FoundStrings.Clear();
        }
        #endregion
    }
}

