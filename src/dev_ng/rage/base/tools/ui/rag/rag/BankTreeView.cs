using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Xml;

using ragCore;
using RSG.Base.Forms;
using ragWidgets;

namespace rag
{

    public partial class BankTreeView : rag.WidgetView
    {
        protected BankTreeView()
        {
            InitializeComponent();

            InitializeAdditionalComponents();
        }

        public BankTreeView( String appName, IBankManager bankMgr, TD.SandDock.DockControl dockControl )
            : base( appName, bankMgr, dockControl )
        {
            // This call is required by the Windows.Forms Form Designer.
            InitializeComponent();

            InitializeAdditionalComponents();

            bankMgr.InitFinished += new ragCore.InitFinishedHandler(InitFinished);
        }

        private void InitializeAdditionalComponents()
        {
            this.DockControl.Closed += new EventHandler( dockControl_Closed );

            base.leftPanelContextMenuStrip.Items.Insert( 0, this.sortBanksToolStripMenuItem );
            base.leftPanelContextMenuStrip.Items.Insert( 1, this.unsortBanksToolStripMenuItem );
            base.leftPanelContextMenuStrip.Items.Insert( 2, this.toolStripSeparator1 );

            base.mainPaneContextMenuStrip.Items.Insert( 0, this.sortAllWidgetsToolStripMenuItem );
            base.mainPaneContextMenuStrip.Items.Insert( 1, this.unsortAllWidgetsToolStripMenuItem );
            base.mainPaneContextMenuStrip.Items.Insert( 2, this.toolStripSeparator2 );

            InitializeXpanderList( this.DockControl );
            if ( sm_ImageList == null )
            {
                sm_ImageList = new ImageList();
                sm_ImageList.Images.Add( global::rag.Properties.Resources.bank );
                sm_ImageList.Images.Add( global::rag.Properties.Resources.widget_group );
            }
            this.treeView.ImageList = sm_ImageList;

            BeginUpdate();
            m_TreeviewManager = new WidgetTreeViewManager(this.treeView, this);
            m_TreeviewManager.PopulateTreeViewForAllBanks( m_BankManager );
            EndUpdate();


            this.treeView.AfterExpand += new TreeViewEventHandler( treeView_AfterExpand );

            m_ViewNum = ++sm_ViewNum;
        }


        void treeView_AfterExpand( object sender, TreeViewEventArgs e )
        {
            if ( e.Action != TreeViewAction.Expand )
                return;

            if ( (Control.ModifierKeys & Keys.Shift) != Keys.Shift )
                return;

            e.Node.ExpandAll();
        }

        #region Variables
        private static int sm_ViewNum = 0;
        private static System.Windows.Forms.ImageList sm_ImageList;

        private int m_ViewNum;
        private String m_SelectedNodePath;
        private WidgetTreeNode m_LastTreeNodeSelected = null;
        
        private bool m_banksSorted = true;
        private bool m_widgetsSorted = false;

        WidgetTreeViewManager m_TreeviewManager;
        #endregion

        #region Overrides
        public override void AddContextMenuOptions( ContextMenuStrip menu, Widget widget, WidgetVisible visible )
        {
            if ( menu.Items.Count >= 1 )
            {
                menu.Items.Add( new ToolStripSeparator() );
            }


            ToolStripMenuItem item = new ToolStripMenuItem( "Sort All Widgets" );
            item.Click += new EventHandler( sortAllWidgetsToolStripMenuItem_Click );
            menu.Items.Add( item );

            item = new ToolStripMenuItem( "Unsort All Widgets" );
            item.Click += new EventHandler( unsortAllWidgetsToolStripMenuItem_Click );
            menu.Items.Add( item );

            if ( visible is WidgetGroupVisible )
            {
                WidgetGroupVisible groupVisible = visible as WidgetGroupVisible;

                menu.Items.Add( new ToolStripSeparator() );

                item = new ToolStripMenuItem( "Sort Group" );
                item.Click += new EventHandler( groupVisible.sortGroupMenuItem_Click );
                menu.Items.Add( item );

                item = new ToolStripMenuItem( "Unsort Group" );
                item.Click += new EventHandler( groupVisible.unsortGroupMenuItem_Click );
                menu.Items.Add( item );
            }

            base.AddContextMenuOptions( menu, widget, visible );
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing  )
            {
                this.treeView.SuspendLayout();
                m_TreeviewManager.UnpopulateTreeViewForAllBanks(m_BankManager );

                this.treeView.ResumeLayout();
                if ( components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose( disposing );
        }

        public override string GetTabText()
        { 
            return "Bank View " + m_ViewNum; 
        }

        public override bool DisplayPath
        {
            get 
            { 
                return false; 
            }
        }

        public override void BeginUpdate()
        {
            base.BeginUpdate();
            this.treeView.SuspendLayout();
        }

        public override void EndUpdate()
        {
            this.treeView.ResumeLayout();
            base.EndUpdate();
        }

        public override void DeserializeView( XmlTextReader xmlReader, IBankManager bankMgr )
        {
            base.DeserializeView( xmlReader, bankMgr );

            m_SelectedNodePath = xmlReader["selectednode"];

            string sorted = xmlReader["BanksSorted"];
            if ( !String.IsNullOrEmpty( sorted ) )
            {
                m_banksSorted = bool.Parse( sorted );
            }

            sorted = xmlReader["WidgetsSorted"];
            if ( !String.IsNullOrEmpty( sorted ) )
            {
                m_widgetsSorted = bool.Parse( sorted );
            }
        }

        protected override void SerializeView( XmlTextWriter xmlWriter, String exePathName )
        {
            base.SerializeView( xmlWriter, exePathName );
            if ( this.treeView.SelectedNode != null )
            {
                xmlWriter.WriteAttributeString( "selectednode", this.treeView.SelectedNode.FullPath );
            }

            xmlWriter.WriteAttributeString( "BanksSorted", m_banksSorted.ToString() );
            xmlWriter.WriteAttributeString( "WidgetsSorted", m_widgetsSorted.ToString() );
        }

        public override void ClearView()
        {            
            this.xPanderList.BeginUpdate();
            HideLastSelected();
            this.xPanderList.EndUpdate();

            this.treeView.SelectedNode = null;
            this.treeView.Nodes.Clear();
        }
        #endregion

        #region Event Handlers
        private void dockControl_Closed( object sender, EventArgs e )
        {
            TD.SandDock.DockControl dockControl = sender as TD.SandDock.DockControl;
            if (dockControl.CloseAction == TD.SandDock.DockControlCloseAction.Dispose)
            {
                m_BankManager.InitFinished -= new ragCore.InitFinishedHandler( InitFinished );
            }
        }

        private void sortBanksToolStripMenuItem_Click( object sender, EventArgs e )
        {
            SortBanks();
        }

        private void unsortBanksToolStripMenuItem_Click( object sender, EventArgs e )
        {
            UnsortBanks();
        }

        private void sortAllWidgetsToolStripMenuItem_Click( object sender, EventArgs e )
        {
            SortWidgets();
        }

        private void unsortAllWidgetsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UnsortWidgets();
        }


        private void treeView_AfterSelect( object sender, TreeViewEventArgs e )
        {
            if ( this.treeView == null )
                return;

            WidgetTreeNode node = (WidgetTreeNode)(this.treeView.SelectedNode);
            Debug.Assert( node != null, "Node is not a WidgetTreeNode" );
            SelectTreeNode( node );
        }

        private void treeView_MouseUp(object sender, MouseEventArgs e)
        {
            if ( e.Button == MouseButtons.Right )
            {
                ContextMenuStrip menu = new ContextMenuStrip();

                // If we clicked on a tree node
                WidgetTreeNode selectedTreeNode = (WidgetTreeNode)( this.treeView.GetNodeAt(e.X, e.Y) );
                if ( selectedTreeNode != null )
                {
                    // Force this node to be the selected node
                    this.treeView.SelectedNode = selectedTreeNode;

                    // Add context menu items for the FavoritesView
                    FavoritesView.AddContextMenuItems( this, menu, selectedTreeNode.TheWidget, null );

                    // Add context menu items for the FindView
                    FindView.AddContextMenuItems( this, menu, selectedTreeNode.TheWidget, null );

                    // Reset to default item
                    if ( menu.Items.Count > 0 )
                    {
                        menu.Items.Add( new ToolStripSeparator() );
                    }

                    WidgetResetter resetter = new WidgetResetter( selectedTreeNode.TheWidget );
                    ToolStripMenuItem resetItem = new ToolStripMenuItem( "Reset to Default Values" );
                    if ( selectedTreeNode.TheWidget == null || !selectedTreeNode.TheWidget.IsModified() )
                    {
                        resetItem.Enabled = false;
                    }

                    resetItem.Click += new EventHandler( resetter.ResetWidgetHandler );
                    menu.Items.Add( resetItem );

                    // Copy/pasting values
                    WidgetClipboardHelper clipboard = new WidgetClipboardHelper( selectedTreeNode.TheWidget );
                    ToolStripMenuItem copyItem = new ToolStripMenuItem( "Copy All Values (JSON)" );
                    copyItem.Click += new EventHandler( clipboard.WidgetCopyHandler );
                    menu.Items.Add( copyItem );

                    ToolStripMenuItem pasteItem = new ToolStripMenuItem( "Paste All Values (JSON)" );
                    pasteItem.Click += new EventHandler( clipboard.WidgetPasteHandler );
                    menu.Items.Add( pasteItem );

                    if ( selectedTreeNode.TheWidget == null )
                    {
                        copyItem.Enabled = false;
                        pasteItem.Enabled = false;
                    }

                    if ( !Clipboard.ContainsText() )
                    {
                        pasteItem.Enabled = false;
                    }

                    // For WidgetBank nodes, add a context menu item for hiding the bank
                    if ( selectedTreeNode.TheWidget is WidgetBank )
                    {
                        menu.Items.Add( new ToolStripSeparator() );
                        ToolStripMenuItem hideItem = new ToolStripMenuItem( "Hide Bank" );
                        hideItem.Tag = selectedTreeNode;
                        hideItem.Click += new EventHandler( hideBankMenuItem_Click );
                        menu.Items.Add( hideItem );
                    }
                }

                // Add context menu item for showing individual hidden banks all hidden banks
                if ( menu.Items.Count > 0 )
                {
                    menu.Items.Add( new ToolStripSeparator() );
                }

                ToolStripMenuItem showHiddenBankItem = new ToolStripMenuItem( "Show Hidden Bank" );
                List<WidgetTreeNode> treeNodes = m_TreeviewManager.GetOriginalTreeViewNodeOrder();
                if ( treeNodes != null && treeNodes.Count > 0 )
                {
                    foreach ( WidgetTreeNode treeNode in treeNodes )
                    {
                        if ( treeNode.TheWidget != null && treeNode.TheWidget is WidgetBank )
                        {
                            WidgetBank widgetBank = (WidgetBank)treeNode.TheWidget;
                            if ( ViewUtils.IsBankHidden( this, widgetBank ) )
                            {
                                ToolStripMenuItem hiddenBankItem = new ToolStripMenuItem( treeNode.Text );
                                hiddenBankItem.Tag = treeNode;
                                hiddenBankItem.Click += new EventHandler( this.toggleBankMenuItem_Click );
                                showHiddenBankItem.DropDownItems.Add( hiddenBankItem );
                            }
                        }
                    }
                }
                showHiddenBankItem.Enabled = ( showHiddenBankItem.DropDownItems.Count > 0 );
                menu.Items.Add( showHiddenBankItem );

                ToolStripMenuItem showAllBanksItem = new ToolStripMenuItem( "Show All Banks" );
                showAllBanksItem.Click += new EventHandler( this.showAllBanksMenuItem_Click );
                menu.Items.Add( showAllBanksItem );

                // Add context menu items for sorting/unsorting the banks
                menu.Items.Add( new ToolStripSeparator() );

                ToolStripMenuItem sortBanksItem = new ToolStripMenuItem( "Sort Banks" );
                sortBanksItem.Click += new EventHandler( this.sortBanksToolStripMenuItem_Click );
                menu.Items.Add( sortBanksItem );

                ToolStripMenuItem unsortBanksItem = new ToolStripMenuItem("Unsort Banks");
                unsortBanksItem.Click += new EventHandler(this.unsortBanksToolStripMenuItem_Click);
                menu.Items.Add(unsortBanksItem);



                // Add the base's context menu items
                Widget widget = selectedTreeNode == null ? null : selectedTreeNode.TheWidget;
                base.AddContextMenuOptions(menu, widget, null);

                if ( menu.Items.Count > 0 )
                {
                    menu.Show( (Control)sender, new Point(e.X, e.Y) );
                }
            }
        }

        private void hideBankMenuItem_Click( object sender, EventArgs e )
        {
            if ( sender is ToolStripMenuItem )
            {
                ToolStripMenuItem menuItem = (ToolStripMenuItem)sender;
                if ( menuItem.Tag is WidgetTreeNode )
                {
                    WidgetTreeNode treeNode = (WidgetTreeNode)menuItem.Tag;
                    SetBankHidden( treeNode, true );
                }
            }
        }

        private void toggleBankMenuItem_Click( object sender, EventArgs e )
        {
            if ( sender is ToolStripMenuItem )
            {
                ToolStripMenuItem menuItem = (ToolStripMenuItem)sender;
                if ( menuItem.Tag is WidgetTreeNode )
                {
                    WidgetTreeNode treeNode = (WidgetTreeNode)menuItem.Tag;
                    SetBankHidden( treeNode, menuItem.Checked );
                }
            }
        }

        private void showAllBanksMenuItem_Click( object sender, EventArgs e )
        {
            List<WidgetTreeNode> treeNodes = m_TreeviewManager.GetOriginalTreeViewNodeOrder();
            if ( treeNodes != null )
            {
                foreach ( WidgetTreeNode treeNode in treeNodes )
                {
                    SetBankHidden( treeNode, false );
                }
            }
        }

        private void hideAllBanksMenuItem_Click( object sender, EventArgs e )
        {
            List<WidgetTreeNode> treeNodes = m_TreeviewManager.GetOriginalTreeViewNodeOrder();
            if ( treeNodes != null )
            {
                foreach ( WidgetTreeNode treeNode in treeNodes )
                {
                    SetBankHidden( treeNode, true );
                }
            }
        }

        private void displayToolStripMenuItem_DropDownOpening( object sender, EventArgs e )
        {
            ToolStripMenuItem displayMenuItem = (ToolStripMenuItem)sender;

            displayMenuItem.DropDownItems.Clear();

            ToolStripMenuItem showAllBanksMenuItem = new ToolStripMenuItem( "Show All" );
            showAllBanksMenuItem.Click += new EventHandler( showAllBanksMenuItem_Click );
            displayMenuItem.DropDownItems.Add( showAllBanksMenuItem );

            ToolStripMenuItem hideAllBanksMenuItem = new ToolStripMenuItem( "Hide All" );
            hideAllBanksMenuItem.Click += new EventHandler( hideAllBanksMenuItem_Click );
            displayMenuItem.DropDownItems.Add( hideAllBanksMenuItem );

            List<WidgetTreeNode> treeNodes = m_TreeviewManager.GetOriginalTreeViewNodeOrder();
            if ( treeNodes != null && treeNodes.Count > 0 )
            {
                displayMenuItem.DropDownItems.Add( new ToolStripSeparator() );

                foreach ( WidgetTreeNode treeNode in treeNodes )
                {
                    if ( treeNode.TheWidget != null && treeNode.TheWidget is WidgetBank )
                    {
                        WidgetBank widgetBank = (WidgetBank)treeNode.TheWidget;

                        ToolStripMenuItem menuItem = new ToolStripMenuItem( treeNode.Text );
                        menuItem.Checked = !ViewUtils.IsBankHidden( this, widgetBank );
                        menuItem.Tag = treeNode;
                        menuItem.Click += new EventHandler( toggleBankMenuItem_Click );
                        displayMenuItem.DropDownItems.Add( menuItem );
                    }
                }
            }
        }
        #endregion

        #region Public Functions
        public WidgetTreeNode FindDeepestMatchingTreeNode( String nodePath )
        {
            if (this.treeView.Nodes.Count == 0)
            {
                return null;
            }

            return FindDeepestMatchingTreeNode( (WidgetTreeNode)this.treeView.Nodes[0], nodePath );
        }

        public WidgetTreeNode FindDeepestMatchingTreeNode( WidgetTreeNode nodeToCheckFrom, String nodePath )
        {
            if ( nodeToCheckFrom == null || nodePath == null )
            {
                return null;
            }

            String treeNodePath = nodeToCheckFrom.FullPath.ToLower();
            String searchNodePath = nodePath.ToLower();

            if ( treeNodePath == searchNodePath )
            {
                return nodeToCheckFrom;
            }
            else if ( searchNodePath.StartsWith( treeNodePath ) )
            {
                foreach ( TreeNode childNode in nodeToCheckFrom.Nodes )
                {
                    WidgetTreeNode foundNode = FindDeepestMatchingTreeNode( (WidgetTreeNode)childNode, nodePath );
                    if ( foundNode != null )
                    {
                        return foundNode;
                    }
                }
                
                return nodeToCheckFrom;
            }
            
            return FindDeepestMatchingTreeNode( (WidgetTreeNode)nodeToCheckFrom.NextNode, nodePath );
        }

        public WidgetTreeNode FindTreeNode( WidgetTreeNode nodeToCheckFrom, String nodePath )
        {
            while ( nodeToCheckFrom != null )
            {
                // see if we match:
                if ( nodeToCheckFrom.FullPath.ToLower() == nodePath )
                    return nodeToCheckFrom;
                // otherwise we check children:
                else
                {
                    WidgetTreeNode foundNode = FindTreeNode( nodeToCheckFrom.NextNode as WidgetTreeNode, nodePath );
                    if ( foundNode != null )
                        return foundNode;
                }

                // get sibling:
                nodeToCheckFrom = nodeToCheckFrom.FirstNode as WidgetTreeNode;
            }

            return null;
        }

        public WidgetTreeNode FindTreeNode( WidgetTreeNode nodeToCheckFrom, WidgetTreeNode nodeToFind )
        {
            while ( nodeToCheckFrom != null )
            {
                // see if we match:
                if ( nodeToCheckFrom == nodeToFind )
                    return nodeToCheckFrom;
                // otherwise we check children:
                else
                {
                    WidgetTreeNode foundNode = FindTreeNode( nodeToCheckFrom.NextNode as WidgetTreeNode, nodeToFind );
                    if ( foundNode != null )
                        return foundNode;
                }

                // get sibling:
                nodeToCheckFrom = nodeToCheckFrom.FirstNode as WidgetTreeNode;
            }

            return null;
        }

        public void SelectTreeNode( WidgetTreeNode node )
        {
            // if the last tree node was destroyed, don't use it:			
            if ( FindTreeNode( this.treeView.Nodes[0] as WidgetTreeNode, m_LastTreeNodeSelected ) == null )
            {
                m_LastTreeNodeSelected = null;
            }

            // if the node isn't the same as the last node, then we update the widget display:
            bool needEndUpdate = false;
            if ( (m_LastTreeNodeSelected == null) || (m_LastTreeNodeSelected.TheWidget != node.TheWidget) )
            {
                BeginUpdate();

                this.xPanderList.BeginUpdate();
                HideLastSelected();
                this.xPanderList.EndUpdate();

                BankRemotePacketProcessor.MakingVisibleMutex.WaitOne();

                try
                {
                    string hashKey = GetType().ToString() + m_ViewNum;
                    bool forceOpen = node.TheWidget.GetType() != Type.GetType( "WidgetBank" );
                    this.xPanderList.BeginUpdate();
                    WidgetVisible widgetVisible = WidgetVisualiser.ShowVisible( node.TheWidget, this, this.xPanderList, hashKey, forceOpen, toolTip1 );
                    this.xPanderList.EndUpdate();
                }
                catch ( Exception e )
                {
                    DialogResult result = rageMessageBox.ShowExclamation( this.DockControl,
                        String.Format( "There was an error creating a Widget Visible for the Widget {0}:\n\n{1}\nWould you like to disable the widget?",
                        node.TheWidget.Title, e.ToString() ), "Widget Error", MessageBoxButtons.YesNo );
                    if ( result == DialogResult.Yes )
                    {
                        node.TheWidget.Enabled = false;

                        if ( !WidgetVisualiser.PacketProcessor.DisabledWidgets.Contains( node.TheWidget ) )
                        {
                            WidgetVisualiser.PacketProcessor.DisabledWidgets.Add( node.TheWidget );
                        }
                    }
                }

                needEndUpdate = true;
                BankRemotePacketProcessor.MakingVisibleMutex.ReleaseMutex();
            }

            // keep track of last node selected:
            m_LastTreeNodeSelected = node;

            if ( needEndUpdate )
            {
                EndUpdate();

                // NOTE: When we select a new tree node, the vertical scroll bar would sometimes
                // fail to register the new height of the XPanderList. So we let the update finish,
                // then set the scroll bar to the top, which forces it to re-evaluate the height of
                // the XPanderList.
                this.xPanderList.VerticalScroll.Value = 0;
            }

            this.treeView.SelectedNode = node;
        }
        #endregion

        #region Private Functions
        private void InitFinished()
        {
            if ( !String.IsNullOrEmpty( m_SelectedNodePath ) )
            {
                WidgetTreeNode node = FindTreeNode( this.treeView.TopNode as WidgetTreeNode, m_SelectedNodePath.ToLower() );
                if ( node != null )
                {
                    SelectTreeNode( node );
                }

                m_SelectedNodePath = null;
            }

            if ( m_banksSorted )
            {
                SortBanks();
            }

            if ( m_widgetsSorted )
            {
                SortWidgets();
            }

            // Hide any bank tree nodes that are supposed to be hidden
            HideHiddenBanks();

            // HACK: The TreeView sometimes had issues determining the height
            // of its contents, which caused the last TreeNode to be displayed
            // below the scrollable area. As a workaround, if you expand a
            // TreeNode in the TreeView, the problem is fixed. So we do that
            // programmatically here as a hack to fix the bug.
            TreeNode tempNode = this.treeView.Nodes.Add( "THIS IS" );
            tempNode.Nodes.Add( "SUPER LAME" );
            tempNode.Expand();
            tempNode.Collapse();
            this.treeView.Nodes.Remove( tempNode );
        }

        private void HideLastSelected()
        {
            // hide the last selected group:
            if ( m_LastTreeNodeSelected != null && m_LastTreeNodeSelected.TheWidget != null && !m_LastTreeNodeSelected.TheWidget.IsDestroyed)
            {
                WidgetVisualiser.HideVisible( m_LastTreeNodeSelected.TheWidget, GetType().ToString() + m_ViewNum );
            }
        }

        private void SortBanks()
        {
            // wrapped with a mutex to prevent destroying a widget while it's being viewed:
            BankRemotePacketProcessor.MakingVisibleMutex.WaitOne();

            // save off the path of the selected node, if any
            string selectedNodePath = string.Empty;
            if ( this.treeView.SelectedNode != null )
            {
                selectedNodePath = this.treeView.SelectedNode.FullPath;
            }

            this.treeView.Sort();

            // make sure the old node gets re-selected
            if ( !String.IsNullOrEmpty( selectedNodePath ) )
            {
                WidgetTreeNode node = FindTreeNode( this.treeView.Nodes[0] as WidgetTreeNode, selectedNodePath.ToLower() );
                if ( node != null )
                {
                    SelectTreeNode( node );
                }
            }

            m_banksSorted = true;

            BankRemotePacketProcessor.MakingVisibleMutex.ReleaseMutex();
        }

        private void UnsortBanks()
        {
            // wrapped with a mutex to prevent destroying a widget while it's being viewed:
            BankRemotePacketProcessor.MakingVisibleMutex.WaitOne();

            // save off the path of the selected node, if any
            string selectedNodePath = string.Empty;
            if ( this.treeView.SelectedNode != null )
            {
                selectedNodePath = this.treeView.SelectedNode.FullPath;
            }

            this.treeView.BeginUpdate();

            this.treeView.Sorted = false;

            m_TreeviewManager.UnsortTreeView();

            // Unsorting the tree view will cause all tree nodes to be shown, so we
            // must re-hide any bank tree nodes that are supposed to be hidden
            HideHiddenBanks();
            
            this.treeView.EndUpdate();

            // make sure the old node gets re-selected
            if ( !String.IsNullOrEmpty( selectedNodePath ) )
            {
                WidgetTreeNode node = FindTreeNode( this.treeView.Nodes[0] as WidgetTreeNode, selectedNodePath.ToLower() );
                if ( node != null )
                {
                    SelectTreeNode( node );
                }
            }

            m_banksSorted = false;

            BankRemotePacketProcessor.MakingVisibleMutex.ReleaseMutex();
        }

        private void SortWidgets()
        {
            // wrapped with a mutex to prevent destroying a widget while it's being viewed:
            BankRemotePacketProcessor.MakingVisibleMutex.WaitOne();

            m_widgetsSorted = true;

            foreach ( TreeNode node in this.treeView.Nodes )
            {
                if ( node is WidgetTreeNode )
                {
                    WidgetTreeNode widgetNode = node as WidgetTreeNode;
                    if ( widgetNode.TheWidget is WidgetGroupBase )
                    {
                        SortWidgets( widgetNode.TheWidget as WidgetGroupBase, true );
                    }
                }
            }

            this.xPanderList.Sort();

            BankRemotePacketProcessor.MakingVisibleMutex.ReleaseMutex();
        }

        private void UnsortWidgets()
        {
            // wrapped with a mutex to prevent destroying a widget while it's being viewed:
            BankRemotePacketProcessor.MakingVisibleMutex.WaitOne();

            m_widgetsSorted = false;

            foreach ( TreeNode node in this.treeView.Nodes )
            {
                if ( node is WidgetTreeNode )
                {
                    WidgetTreeNode widgetNode = node as WidgetTreeNode;
                    if ( widgetNode.TheWidget is WidgetGroupBase )
                    {
                        SortWidgets( widgetNode.TheWidget as WidgetGroupBase, false );
                    }
                }
            }

            this.xPanderList.Unsort();

            BankRemotePacketProcessor.MakingVisibleMutex.ReleaseMutex();
        }

        private void SortWidgets( WidgetGroupBase group, bool sort )
        {
            WidgetGroupBase.SetSortedGroup( this.Guid.ToString(), group.FindPath(), sort );

            foreach ( Widget subWidget in group.List )
            {
                if ( subWidget is WidgetGroupBase )
                {
                    SortWidgets( subWidget as WidgetGroupBase, sort );
                }
            }
        }

        private void SetBankHidden( WidgetTreeNode treeNode, bool hidden )
        {
            if ( treeNode != null && treeNode.TheWidget != null && treeNode.TheWidget is WidgetBank )
            {
                // wrapped with a mutex to prevent destroying a widget while it's being viewed:
                BankRemotePacketProcessor.MakingVisibleMutex.WaitOne();
                this.treeView.BeginUpdate();

                // Mark the WidgetBank as hidden or not
                WidgetBank widgetBank = (WidgetBank)treeNode.TheWidget;
                ViewUtils.SetBankHidden( this, widgetBank, hidden );

                // Update our TreeView to properly show/hide the tree node
                if ( hidden && this.treeView.Nodes.Contains( treeNode ) )
                {
                    if ( this.treeView.Nodes.Count <= 1 )
                    {
                        HideLastSelected();
                        this.treeView.SelectedNode = null;
                        m_LastTreeNodeSelected = null;
                    }
                    else
                    {
                        this.treeView.SelectedNode = ( ( treeNode.PrevNode != null ) ? treeNode.PrevNode : treeNode.NextNode );
                    }

                    this.treeView.Nodes.Remove( treeNode );
                }
                else if ( !hidden && !this.treeView.Nodes.Contains( treeNode ) )
                {
                    List<WidgetTreeNode> treeNodes = m_TreeviewManager.GetOriginalTreeViewNodeOrder();
                    if ( treeNodes != null )
                    {
                        int index = treeNodes.IndexOf( treeNode );
                        this.treeView.Nodes.Insert( index, treeNode );
                        this.treeView.SelectedNode = treeNode;
                    }
                }

                this.treeView.EndUpdate();
                BankRemotePacketProcessor.MakingVisibleMutex.ReleaseMutex();
            }
        }

        private void HideHiddenBanks()
        {
            List<WidgetTreeNode> treeNodes = m_TreeviewManager.GetOriginalTreeViewNodeOrder();
            if ( treeNodes != null )
            {
                foreach ( WidgetTreeNode treeNode in treeNodes )
                {
                    // If this tree node represents a WidgetBank, and that widget bank
                    // is marked hidden, then the tree node should be hidden too
                    if ( treeNode.TheWidget != null && treeNode.TheWidget is WidgetBank )
                    {
                        WidgetBank widgetBank = (WidgetBank)treeNode.TheWidget;
                        if ( ViewUtils.IsBankHidden( this, widgetBank ) )
                        {
                            SetBankHidden( treeNode, true );
                        }
                    }
                }
            }
        }
        #endregion

        #region Public Static Functions
        public static WidgetViewBase CreateBankTreeView( String appName, IBankManager bankMgr, TD.SandDock.DockControl dockControl )
        {
            BankTreeView view = new BankTreeView( appName, bankMgr, dockControl );
            dockControl.CloseAction = (view.m_ViewNum == 1) ? TD.SandDock.DockControlCloseAction.HideOnly : TD.SandDock.DockControlCloseAction.Dispose;
            return view;
        }

        public static void ClearStatics()
        {
            sm_ViewNum = 0;
            sm_ImageList = null;
        }
        #endregion
    }

    public class WidgetFinder
    {
        public WidgetFinder( BankTreeView bankView, WidgetTreeNode treeNode )
        {
            m_BankView = bankView;
            m_TreeNode = treeNode;
        }

        public void FindWidgetHandler( Object obj, System.EventArgs e )
        {
            m_BankView.DockControl.Open();
            m_BankView.SelectTreeNode( m_TreeNode );
        }

        private BankTreeView m_BankView;
        private WidgetTreeNode m_TreeNode;
    }
}

