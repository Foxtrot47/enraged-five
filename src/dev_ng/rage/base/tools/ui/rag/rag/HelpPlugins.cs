using System;
using System.Drawing;
using System.ComponentModel;
using System.Windows.Forms;

namespace rag
{
	/// <summary>
	/// Summary description for HelpPlugins.
	/// </summary>
	public class HelpPlugins : System.Windows.Forms.Form
    {
		private System.Windows.Forms.Button m_ButtonOK;
		private System.Windows.Forms.Button m_ButtonCancel;
		private System.Windows.Forms.Label m_WarningLabel;
		private System.Windows.Forms.CheckedListBox m_ListBoxPlugins;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public HelpPlugins()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}


		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.m_ListBoxPlugins = new System.Windows.Forms.CheckedListBox();
            this.m_ButtonOK = new System.Windows.Forms.Button();
            this.m_ButtonCancel = new System.Windows.Forms.Button();
            this.m_WarningLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // m_ListBoxPlugins
            // 
            this.m_ListBoxPlugins.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.m_ListBoxPlugins.Location = new System.Drawing.Point( 11, 17 );
            this.m_ListBoxPlugins.Name = "m_ListBoxPlugins";
            this.m_ListBoxPlugins.Size = new System.Drawing.Size( 619, 214 );
            this.m_ListBoxPlugins.TabIndex = 1;
            this.m_ListBoxPlugins.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler( this.m_ListBox_ItemCheck );
            // 
            // m_ButtonOK
            // 
            this.m_ButtonOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.m_ButtonOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.m_ButtonOK.Location = new System.Drawing.Point( 464, 240 );
            this.m_ButtonOK.Name = "m_ButtonOK";
            this.m_ButtonOK.Size = new System.Drawing.Size( 75, 23 );
            this.m_ButtonOK.TabIndex = 2;
            this.m_ButtonOK.Text = "OK";
            // 
            // m_ButtonCancel
            // 
            this.m_ButtonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.m_ButtonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.m_ButtonCancel.Location = new System.Drawing.Point( 552, 240 );
            this.m_ButtonCancel.Name = "m_ButtonCancel";
            this.m_ButtonCancel.Size = new System.Drawing.Size( 75, 23 );
            this.m_ButtonCancel.TabIndex = 3;
            this.m_ButtonCancel.Text = "Cancel";
            // 
            // m_WarningLabel
            // 
            this.m_WarningLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.m_WarningLabel.AutoSize = true;
            this.m_WarningLabel.Location = new System.Drawing.Point( 12, 245 );
            this.m_WarningLabel.Name = "m_WarningLabel";
            this.m_WarningLabel.Size = new System.Drawing.Size( 277, 13 );
            this.m_WarningLabel.TabIndex = 4;
            this.m_WarningLabel.Text = "Warning: Changes won\'t take effect until you restart RAG";
            // 
            // HelpPlugins
            // 
            this.AcceptButton = this.m_ButtonOK;
            this.AutoScaleBaseSize = new System.Drawing.Size( 5, 13 );
            this.CancelButton = this.m_ButtonCancel;
            this.ClientSize = new System.Drawing.Size( 642, 274 );
            this.Controls.Add( this.m_WarningLabel );
            this.Controls.Add( this.m_ButtonCancel );
            this.Controls.Add( this.m_ButtonOK );
            this.Controls.Add( this.m_ListBoxPlugins );
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.MinimumSize = new System.Drawing.Size( 481, 212 );
            this.Name = "HelpPlugins";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Loaded Plugins";
            this.Load += new System.EventHandler( this.HelpPlugins_Load );
            this.ResumeLayout( false );
            this.PerformLayout();

		}
		#endregion

		public class DllInfoWrapper
		{
			public DllInfoWrapper(ragCore.DllInfo info)
			{
				DllInfo = info;
			}

			public ragCore.DllInfo DllInfo;

			public override string ToString()
			{
				if (DllInfo.FullName != null)
				{
					return DllInfo.FullName;
				}
				else
				{
					return DllInfo.Name + " (not loaded)";
				}
			}
		}

		public void Init(GlobalPrefs prefs)
		{
			m_ListBoxPlugins.BeginUpdate();
			m_ListBoxPlugins.Items.Clear();

            foreach ( ragCore.DllInfo dllInfo in prefs.KnownDllHash.Values )
            {
                DllInfoWrapper info = new DllInfoWrapper( dllInfo );
                int itemNum = m_ListBoxPlugins.Items.Add( info );

                m_ListBoxPlugins.SetItemChecked( m_ListBoxPlugins.Items.Count - 1, info.DllInfo.ShouldLoad );
            }
		}

		private void HelpPlugins_Load(object sender, System.EventArgs e)
		{
			m_WarningLabel.Visible = false;
		}

		private void m_ListBox_ItemCheck(object sender, System.Windows.Forms.ItemCheckEventArgs e)
		{
			m_WarningLabel.Visible = true;
		}

		public void UpdateShouldLoadFlags()
		{
			for(int i = 0; i < m_ListBoxPlugins.Items.Count; i++) {
				ragCore.DllInfo info = (m_ListBoxPlugins.Items[i] as DllInfoWrapper).DllInfo;
				info.ShouldLoad = m_ListBoxPlugins.GetItemChecked(i);
			}
		}

	}
}
