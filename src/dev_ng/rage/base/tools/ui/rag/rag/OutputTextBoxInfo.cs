﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TD.SandDock;
using rag.Extensions;
using System.Runtime.InteropServices;
using System.Drawing;
using rag.Controls;
using System.Text.RegularExpressions;

namespace rag
{
    /// <summary>
    /// 
    /// </summary>
    internal class OutputTextBoxInfo
    {
        #region Constants
        /// <summary>
        /// Maximum limit to the number of queued messages.
        /// </summary>
        private const int _maxQueuedMessages = 50000;
        #endregion

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="channelName"></param>
        /// <param name="mcInfo"></param>
        /// <param name="textBox"></param>
        /// <param name="window"></param>
        public OutputTextBoxInfo(string channelName, MultiColorInfo mcInfo, SuspendableRichTextBox textBox, DockableWindow window)
        {
            _channelName = channelName;
            _multiColorInfo = mcInfo;
            _visibleTextBox = textBox;
            _window = window;
        }
        #endregion // Constructor(s)

        #region Private Classes
        /// <summary>
        /// 
        /// </summary>
        public class CachedMessage
        {
            public CachedMessage(String message, Color foreground, Color background)
            {
                Message = message;
                Foreground = foreground;
                Background = background;
            }

            public String Message { get; private set; }
            public Color Foreground { get; private set; }
            public Color Background { get; private set; }
        }
        #endregion // Private Classes

        #region Fields
        /// <summary>
        /// 
        /// </summary>
        private bool _addedText = false;

        /// <summary>
        /// 
        /// </summary>
        private Queue<CachedMessage> _pauseMessages = new Queue<CachedMessage>();
        #endregion // Fields

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public String ChannelName
        {
            get { return _channelName; }
        }
        private String _channelName;

        /// <summary>
        /// 
        /// </summary>
        public ISet<String> SubChannelNames
        {
            get { return _subChannelNames; }
            set { _subChannelNames = value; }
        }
        private ISet<String> _subChannelNames = new HashSet<string>();

        /// <summary>
        /// 
        /// </summary>
        public MultiColorInfo ColorInfo
        {
            get { return _multiColorInfo; }
        }
        private MultiColorInfo _multiColorInfo;

        /// <summary>
        /// 
        /// </summary>
        public DockableWindow Window
        {
            get { return _window; }
        }
        private DockableWindow _window;

        /// <summary>
        /// 
        /// </summary>
        public SuspendableRichTextBox VisibleTextBox
        {
            get { return _visibleTextBox; }
        }
        private SuspendableRichTextBox _visibleTextBox;

        /// <summary>
        /// 
        /// </summary>
        public bool IsPaused
        {
            get { return _isPaused; }
            set
            {
                if (_isPaused != value)
                {
                    _isPaused = value;

                    // Update some stuff based on whether we paused the output or not.
                    if (_isPaused)
                    {
                        _window.Text += " (Paused)";
                        _window.TabText = _window.Text;
                    }
                    else
                    {
                        _window.Text = _window.Text.Replace(" (Paused)", "");
                        _window.TabText = _window.Text;
                        ProcessPausedMessages();
                        ScrollToBottom(_visibleTextBox);
                    }
                }
            }
        }
        private bool _isPaused;

        /// <summary>
        /// 
        /// </summary>
        public Queue<CachedMessage> PauseMessages
        {
            get { return _pauseMessages; }
        }
        #endregion // Properties

        #region Public Methods
        /// <summary>
        /// 
        /// </summary>
        public void BeginAddText()
        {
            _visibleTextBox.SuspendLayout();
            _visibleTextBox.SuspendPaint();
        }

        /// <summary>
        /// 
        /// </summary>
        public void EndAddText()
        {
            if (_addedText)
            {
                _visibleTextBox.LimitTextLength();
                ScrollToBottom(_visibleTextBox);
                _addedText = false;
            }

            _visibleTextBox.ResumeLayout();
            _visibleTextBox.ResumePaint();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="text"></param>
        public void AppendText(String text)
        {
            if (_isPaused)
            {
                if (_pauseMessages.Count == _maxQueuedMessages)
                {
                    _pauseMessages.Dequeue();
                }
                _pauseMessages.Enqueue(new CachedMessage(text, _visibleTextBox.ForeColor, _visibleTextBox.BackColor));
            }
            else
            {
                _visibleTextBox.AppendText(text);
                _addedText = true;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="rtfText"></param>
        /// <param name="foreground"></param>
        /// <param name="background"></param>
        public void AppendRTFText(String text, Color foreground, Color background)
        {
            if (_isPaused)
            {
                if (_pauseMessages.Count == _maxQueuedMessages)
                {
                    _pauseMessages.Dequeue();
                }
                _pauseMessages.Enqueue(new CachedMessage(text, foreground, background));
            }
            else
            {
                if (_multiColorInfo.MultiColor && (foreground != _visibleTextBox.ForeColor || background != _visibleTextBox.BackColor))
                {
                    _visibleTextBox.AppendText(text, foreground, background);
                }
                else
                {
                    _visibleTextBox.AppendText(text);
                }
                _addedText = true;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void ProcessPausedMessages()
        {
            _visibleTextBox.SuspendPaint();

            String colorTableString = ExtractColorTableString(_visibleTextBox.Rtf);

            // Argb color to index mapping
            IList<Color> colorTable;
            if (colorTableString != null)
            {
                colorTable = ConvertColorTable(colorTableString);
            }
            else
            {
                colorTable = new List<Color>();
            }

            // Get the forecolor/backcolor the output window is currently using.
            Color previousForeColor = default(Color);
            Match cfMatch = Regex.Match(_visibleTextBox.Rtf, @"\\cf(\d+)", RegexOptions.RightToLeft);
            if (cfMatch.Success)
            {
                int idx = Int32.Parse(cfMatch.Groups[1].Value) - 1;
                previousForeColor = (idx >= 0 ? colorTable[idx] : _visibleTextBox.ForeColor);
            }

            Color previousBackColor = default(Color);
            Match highlightMatch = Regex.Match(_visibleTextBox.Rtf, @"\\highlight(\d+)", RegexOptions.RightToLeft);
            if (highlightMatch.Success)
            {
                int idx = Int32.Parse(highlightMatch.Groups[1].Value) - 1;
                previousBackColor = (idx >= 0 ? colorTable[idx] : _visibleTextBox.BackColor);
            }

            // Get the string that we will be working with.
            int lastBracket = _visibleTextBox.Rtf.LastIndexOf('}');

            StringBuilder sb = new StringBuilder(_visibleTextBox.Rtf.Substring(0, lastBracket));
            String remainder = _visibleTextBox.Rtf.Substring(lastBracket);

            while (_pauseMessages.Any())
            {
                CachedMessage msg = _pauseMessages.Dequeue();

                if (_multiColorInfo.MultiColor && (msg.Foreground != previousForeColor || msg.Background != previousBackColor))
                {
                    int foreColorIdx = GetColorTableIndex(msg.Foreground, ref colorTable);
                    int backColorIdx = GetColorTableIndex(msg.Background, ref colorTable);

                    sb.AppendFormat("\\cf{0}\\highlight{1} {2}", foreColorIdx, backColorIdx, Regex.Replace(msg.Message, @"\r?\n", "\\par\r\n"));

                    previousForeColor = msg.Foreground;
                    previousBackColor = msg.Background;
                }
                else
                {
                    sb.Append(Regex.Replace(msg.Message, @"\r?\n", "\\par\r\n"));
                }
            }

            if (colorTableString != null)
            {
                String newColorTableString = CreateColorTableString(colorTable);
                sb.Replace(colorTableString, newColorTableString);
            }
            sb.Append(remainder);

            _visibleTextBox.Rtf = sb.ToString();
            _visibleTextBox.LimitTextLength();
            _visibleTextBox.ResumePaint();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="rtf"></param>
        /// <returns></returns>
        private String ExtractColorTableString(String rtf)
        {
            Regex tableRegex = new Regex(@"\{\\colortbl.+?\}");
            Match tableMatch = tableRegex.Match(rtf);
            if (tableMatch.Success)
            {
                return tableMatch.Value;
            }
            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sb"></param>
        /// <returns></returns>
        private IList<Color> ConvertColorTable(String colorTable)
        {
            Regex colorRegex = new Regex(@"\\red(\d+)\\green(\d+)\\blue(\d+)");
            IList<Color> results = new List<Color>();

            colorTable = colorTable.Trim(new char[] { '{', '}' });
            colorTable = colorTable.Replace("\\colortbl ;", "");

            int idx = 0;
            foreach (String color in colorTable.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries))
            {
                Match colorMatch = colorRegex.Match(color);
                if (colorMatch.Success)
                {
                    int r = Int32.Parse(colorMatch.Groups[1].Value);
                    int g = Int32.Parse(colorMatch.Groups[2].Value);
                    int b = Int32.Parse(colorMatch.Groups[3].Value);
                    results.Insert(idx, Color.FromArgb(255, r, g, b));
                }
                else
                {
                    throw new ArgumentException();
                }

                ++idx;
            }

            return results;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="color"></param>
        /// <param name="colorTable"></param>
        /// <returns></returns>
        private int GetColorTableIndex(Color color, ref IList<Color> colorTable)
        {
            int idx;
            for (idx = 0; idx < colorTable.Count; ++idx)
            {
                if (colorTable[idx].ToArgb() == color.ToArgb())
                {
                    return idx + 1;
                }
            }

            // If we fall through to here, the colour wasn't found so add it.
            colorTable.Add(color);
            return colorTable.Count;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="colorTable"></param>
        /// <returns></returns>
        private String CreateColorTableString(IList<Color> colorTable)
        {
            return String.Format("{{\\colortbl ;{0};}}",
                String.Join(";", colorTable.Select(item =>
                    String.Format("\\red{0}\\green{1}\\blue{2}", item.R, item.G, item.B))));
        }
        #endregion // Public Methods

        #region Microsoft Interop Methods
        /// <summary>
        /// Vertical scroll.
        /// </summary>
        private const int WM_VSCROLL = 277;

        /// <summary>
        /// Scroll to bottom.
        /// </summary>
        private const int SB_BOTTOM = 7;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="hWnd"></param>
        /// <param name="wMsg"></param>
        /// <param name="wParam"></param>
        /// <param name="lParam"></param>
        /// <returns></returns>
        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        private static extern int SendMessage(IntPtr hWnd, int wMsg, IntPtr wParam, IntPtr lParam);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="controlToScroll"></param>
        private void ScrollToBottom(Control controlToScroll)
        {
            SendMessage(controlToScroll.Handle, WM_VSCROLL, (IntPtr)SB_BOTTOM, IntPtr.Zero);
        }
        #endregion // Microsoft Interop Methods
    } // OutputTextBoxInfo
}
