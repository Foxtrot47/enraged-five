namespace rag
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && (components != null) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            TD.SandDock.DockContainer dockContainer1;
            TD.SandDock.DockingRules dockingRules1 = new TD.SandDock.DockingRules();
            TD.SandDock.DockingRules dockingRules2 = new TD.SandDock.DockingRules();
            TD.SandDock.DockContainer dockContainer3;
            TD.SandDock.DockingRules dockingRules3 = new TD.SandDock.DockingRules();
            TD.SandDock.DockingRules dockingRules4 = new TD.SandDock.DockingRules();
            TD.SandDock.DockingRules dockingRules5 = new TD.SandDock.DockingRules();
            TD.SandDock.DockingRules dockingRules6 = new TD.SandDock.DockingRules();
            TD.SandDock.DockingRules dockingRules7 = new TD.SandDock.DockingRules();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainWindow));
            this.bankViewDockableWindow = new TD.SandDock.DockableWindow();
            this.sandDockManager1 = new TD.SandDock.SandDockManager();
            this.toolStripContainer1 = new System.Windows.Forms.ToolStripContainer();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.framerateToolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.lastErrorToolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.gameViewDocumentContainer = new TD.SandDock.DocumentContainer();
            this.gameViewTabbedDocument = new TD.SandDock.TabbedDocument();
            this.outputDockableWindow = new TD.SandDock.DockableWindow();
            this.richTextBoxesContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.pauseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.copyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.clearAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.selectAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.multiColorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator12 = new System.Windows.Forms.ToolStripSeparator();
            this.emailOutputToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator13 = new System.Windows.Forms.ToolStripSeparator();
            this.findToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.displayDockableWindow = new TD.SandDock.DockableWindow();
            this.warningsDockableWindow = new TD.SandDock.DockableWindow();
            this.errorsDockableWindow = new TD.SandDock.DockableWindow();
            this.scriptsDockableWindow = new TD.SandDock.DockableWindow();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openLayoutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openSharedLayoutsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.saveLayoutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveLayoutAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.recentLayoutsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.preferencesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.userPreferencesDirectoryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator9 = new System.Windows.Forms.ToolStripSeparator();
            this.dumpOutputInRealTimeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dumpOutputOnExitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.appendTimestampToOutputFileNameToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.zipOutputOnExitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.suppressAssertCrashQuitDialogOnExitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.enableColouredModificationsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.enableNumPadOverrideToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.exitApplicationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitRAGToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.disabledWidgetsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator11 = new System.Windows.Forms.ToolStripSeparator();
            this.addWidgetViewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addFindViewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addFavoritesViewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.sharedFavoritesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.windowsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pluginsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pluginsToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.commandLineToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.throwExceptionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator10 = new System.Windows.Forms.ToolStripSeparator();
            this.submitFeedbackToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.layoutSaveAsFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.updateTimer = new System.Windows.Forms.Timer(this.components);
            this.dockControlContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.disposeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.hideToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.folderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.tabDocContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.OutputRichTextBox = new rag.Controls.SuspendableRichTextBox();
            this.DisplayRichTextBox = new rag.Controls.SuspendableRichTextBox();
            this.WarningRichTextBox = new rag.Controls.SuspendableRichTextBox();
            this.ErrorRichTextBox = new rag.Controls.SuspendableRichTextBox();
            this.ScriptRichTextBox = new rag.Controls.SuspendableRichTextBox();
            dockContainer1 = new TD.SandDock.DockContainer();
            dockContainer3 = new TD.SandDock.DockContainer();
            dockContainer1.SuspendLayout();
            this.toolStripContainer1.BottomToolStripPanel.SuspendLayout();
            this.toolStripContainer1.ContentPanel.SuspendLayout();
            this.toolStripContainer1.TopToolStripPanel.SuspendLayout();
            this.toolStripContainer1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.gameViewDocumentContainer.SuspendLayout();
            dockContainer3.SuspendLayout();
            this.outputDockableWindow.SuspendLayout();
            this.richTextBoxesContextMenuStrip.SuspendLayout();
            this.displayDockableWindow.SuspendLayout();
            this.warningsDockableWindow.SuspendLayout();
            this.errorsDockableWindow.SuspendLayout();
            this.scriptsDockableWindow.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.dockControlContextMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // dockContainer1
            // 
            dockContainer1.ContentSize = 596;
            dockContainer1.Controls.Add(this.bankViewDockableWindow);
            dockContainer1.Dock = System.Windows.Forms.DockStyle.Left;
            dockContainer1.LayoutSystem = new TD.SandDock.SplitLayoutSystem(new System.Drawing.SizeF(596F, 413F), System.Windows.Forms.Orientation.Vertical, new TD.SandDock.LayoutSystemBase[] {
            ((TD.SandDock.LayoutSystemBase)(new TD.SandDock.ControlLayoutSystem(new System.Drawing.SizeF(596F, 413F), new TD.SandDock.DockControl[] {
                        ((TD.SandDock.DockControl)(this.bankViewDockableWindow))}, this.bankViewDockableWindow)))});
            dockContainer1.Location = new System.Drawing.Point(0, 0);
            dockContainer1.Manager = this.sandDockManager1;
            dockContainer1.Name = "dockContainer1";
            dockContainer1.Size = new System.Drawing.Size(600, 413);
            dockContainer1.TabIndex = 3;
            // 
            // bankViewDockableWindow
            // 
            this.bankViewDockableWindow.AutoScroll = true;
            this.bankViewDockableWindow.CloseAction = TD.SandDock.DockControlCloseAction.Dispose;
            dockingRules1.AllowDockBottom = true;
            dockingRules1.AllowDockLeft = true;
            dockingRules1.AllowDockRight = true;
            dockingRules1.AllowDockTop = true;
            dockingRules1.AllowFloat = true;
            dockingRules1.AllowTab = true;
            this.bankViewDockableWindow.DockingRules = dockingRules1;
            this.bankViewDockableWindow.Guid = new System.Guid("b69612f0-40f7-415f-a96c-163dbcd3c66f");
            this.bankViewDockableWindow.Location = new System.Drawing.Point(0, 23);
            this.bankViewDockableWindow.Name = "bankViewDockableWindow";
            this.bankViewDockableWindow.PersistState = false;
            this.bankViewDockableWindow.ShowOptions = false;
            this.bankViewDockableWindow.Size = new System.Drawing.Size(596, 366);
            this.bankViewDockableWindow.TabIndex = 0;
            this.bankViewDockableWindow.Text = "Bank View";
            // 
            // sandDockManager1
            // 
            this.sandDockManager1.AllowMiddleButtonClosure = false;
            this.sandDockManager1.DockSystemContainer = this.toolStripContainer1.ContentPanel;
            this.sandDockManager1.MaximumDockContainerSize = 100000;
            this.sandDockManager1.OwnerForm = this;
            this.sandDockManager1.SerializeTabbedDocuments = true;
            this.sandDockManager1.ShowControlContextMenu += new TD.SandDock.ShowControlContextMenuEventHandler(this.sandDockManager1_ShowControlContextMenu);
            this.sandDockManager1.ResolveDockControl += new TD.SandDock.ResolveDockControlEventHandler(this.sandDockManager1_ResolveDockControl);
            // 
            // toolStripContainer1
            // 
            // 
            // toolStripContainer1.BottomToolStripPanel
            // 
            this.toolStripContainer1.BottomToolStripPanel.Controls.Add(this.statusStrip1);
            // 
            // toolStripContainer1.ContentPanel
            // 
            this.toolStripContainer1.ContentPanel.Controls.Add(this.gameViewDocumentContainer);
            this.toolStripContainer1.ContentPanel.Controls.Add(dockContainer1);
            this.toolStripContainer1.ContentPanel.Controls.Add(dockContainer3);
            this.toolStripContainer1.ContentPanel.Size = new System.Drawing.Size(1228, 667);
            this.toolStripContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.toolStripContainer1.Location = new System.Drawing.Point(0, 0);
            this.toolStripContainer1.Name = "toolStripContainer1";
            this.toolStripContainer1.Size = new System.Drawing.Size(1228, 716);
            this.toolStripContainer1.TabIndex = 0;
            this.toolStripContainer1.Text = "toolStripContainer1";
            // 
            // toolStripContainer1.TopToolStripPanel
            // 
            this.toolStripContainer1.TopToolStripPanel.Controls.Add(this.menuStrip1);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.framerateToolStripStatusLabel,
            this.lastErrorToolStripStatusLabel});
            this.statusStrip1.Location = new System.Drawing.Point(0, 0);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1228, 25);
            this.statusStrip1.TabIndex = 0;
            this.statusStrip1.Click += new System.EventHandler(this.statusStrip1_Click);
            // 
            // framerateToolStripStatusLabel
            // 
            this.framerateToolStripStatusLabel.AutoSize = false;
            this.framerateToolStripStatusLabel.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.framerateToolStripStatusLabel.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter;
            this.framerateToolStripStatusLabel.Image = global::rag.Properties.Resources.framerate;
            this.framerateToolStripStatusLabel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.framerateToolStripStatusLabel.Name = "framerateToolStripStatusLabel";
            this.framerateToolStripStatusLabel.Size = new System.Drawing.Size(300, 20);
            this.framerateToolStripStatusLabel.Text = "FrameRate";
            this.framerateToolStripStatusLabel.ToolTipText = "Shows Frame Rate";
            // 
            // lastErrorToolStripStatusLabel
            // 
            this.lastErrorToolStripStatusLabel.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.lastErrorToolStripStatusLabel.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter;
            this.lastErrorToolStripStatusLabel.Image = global::rag.Properties.Resources.error;
            this.lastErrorToolStripStatusLabel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lastErrorToolStripStatusLabel.Name = "lastErrorToolStripStatusLabel";
            this.lastErrorToolStripStatusLabel.Size = new System.Drawing.Size(913, 20);
            this.lastErrorToolStripStatusLabel.Spring = true;
            this.lastErrorToolStripStatusLabel.Text = "Error";
            // 
            // gameViewDocumentContainer
            // 
            this.gameViewDocumentContainer.ContentSize = 411;
            this.gameViewDocumentContainer.Controls.Add(this.gameViewTabbedDocument);
            this.gameViewDocumentContainer.LayoutSystem = new TD.SandDock.SplitLayoutSystem(new System.Drawing.SizeF(626F, 411F), System.Windows.Forms.Orientation.Horizontal, new TD.SandDock.LayoutSystemBase[] {
            ((TD.SandDock.LayoutSystemBase)(new TD.SandDock.DocumentLayoutSystem(new System.Drawing.SizeF(626F, 411F), new TD.SandDock.DockControl[] {
                        ((TD.SandDock.DockControl)(this.gameViewTabbedDocument))}, this.gameViewTabbedDocument)))});
            this.gameViewDocumentContainer.Location = new System.Drawing.Point(600, 0);
            this.gameViewDocumentContainer.Manager = this.sandDockManager1;
            this.gameViewDocumentContainer.Name = "gameViewDocumentContainer";
            this.gameViewDocumentContainer.Size = new System.Drawing.Size(628, 413);
            this.gameViewDocumentContainer.TabIndex = 4;
            this.gameViewDocumentContainer.SizeChanged += new System.EventHandler(this.documentContainer1_SizeChanged);
            // 
            // gameViewTabbedDocument
            // 
            this.gameViewTabbedDocument.AllowClose = false;
            dockingRules2.AllowDockBottom = false;
            dockingRules2.AllowDockLeft = false;
            dockingRules2.AllowDockRight = false;
            dockingRules2.AllowDockTop = false;
            dockingRules2.AllowFloat = true;
            dockingRules2.AllowTab = true;
            this.gameViewTabbedDocument.DockingRules = dockingRules2;
            this.gameViewTabbedDocument.FloatingSize = new System.Drawing.Size(550, 400);
            this.gameViewTabbedDocument.Guid = new System.Guid("c1834e12-2608-4016-9679-99d8aedabc6f");
            this.gameViewTabbedDocument.Location = new System.Drawing.Point(1, 21);
            this.gameViewTabbedDocument.Name = "gameViewTabbedDocument";
            this.gameViewTabbedDocument.Size = new System.Drawing.Size(626, 391);
            this.gameViewTabbedDocument.TabIndex = 0;
            this.gameViewTabbedDocument.Text = "Game View 1";
            // 
            // dockContainer3
            // 
            dockContainer3.ContentSize = 250;
            dockContainer3.Controls.Add(this.outputDockableWindow);
            dockContainer3.Controls.Add(this.displayDockableWindow);
            dockContainer3.Controls.Add(this.warningsDockableWindow);
            dockContainer3.Controls.Add(this.errorsDockableWindow);
            dockContainer3.Controls.Add(this.scriptsDockableWindow);
            dockContainer3.Dock = System.Windows.Forms.DockStyle.Bottom;
            dockContainer3.LayoutSystem = new TD.SandDock.SplitLayoutSystem(new System.Drawing.SizeF(1228F, 250F), System.Windows.Forms.Orientation.Vertical, new TD.SandDock.LayoutSystemBase[] {
            ((TD.SandDock.LayoutSystemBase)(new TD.SandDock.ControlLayoutSystem(new System.Drawing.SizeF(1228F, 250F), new TD.SandDock.DockControl[] {
                        ((TD.SandDock.DockControl)(this.outputDockableWindow)),
                        ((TD.SandDock.DockControl)(this.displayDockableWindow)),
                        ((TD.SandDock.DockControl)(this.warningsDockableWindow)),
                        ((TD.SandDock.DockControl)(this.errorsDockableWindow)),
                        ((TD.SandDock.DockControl)(this.scriptsDockableWindow))}, this.outputDockableWindow)))});
            dockContainer3.Location = new System.Drawing.Point(0, 413);
            dockContainer3.Manager = this.sandDockManager1;
            dockContainer3.Name = "dockContainer3";
            dockContainer3.Size = new System.Drawing.Size(1228, 254);
            dockContainer3.TabIndex = 2;
            // 
            // outputDockableWindow
            // 
            this.outputDockableWindow.Controls.Add(this.OutputRichTextBox);
            dockingRules3.AllowDockBottom = true;
            dockingRules3.AllowDockLeft = true;
            dockingRules3.AllowDockRight = true;
            dockingRules3.AllowDockTop = true;
            dockingRules3.AllowFloat = true;
            dockingRules3.AllowTab = true;
            this.outputDockableWindow.DockingRules = dockingRules3;
            this.outputDockableWindow.Guid = new System.Guid("85097a65-4b28-4f23-a6ce-03f18ed4143c");
            this.outputDockableWindow.Location = new System.Drawing.Point(0, 27);
            this.outputDockableWindow.Name = "outputDockableWindow";
            this.outputDockableWindow.ShowOptions = false;
            this.outputDockableWindow.Size = new System.Drawing.Size(1228, 203);
            this.outputDockableWindow.TabImage = global::rag.Properties.Resources.info;
            this.outputDockableWindow.TabIndex = 0;
            this.outputDockableWindow.Text = "Output";
            // 
            // richTextBoxesContextMenuStrip
            // 
            this.richTextBoxesContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pauseToolStripMenuItem,
            this.copyToolStripMenuItem,
            this.toolStripSeparator4,
            this.clearAllToolStripMenuItem,
            this.selectAllToolStripMenuItem,
            this.toolStripSeparator6,
            this.multiColorToolStripMenuItem,
            this.toolStripSeparator12,
            this.emailOutputToolStripMenuItem,
            this.toolStripSeparator13,
            this.findToolStripMenuItem,
            this.toolStripMenuItem2,
            this.toolStripMenuItem3,
            this.toolStripMenuItem4,
            this.toolStripMenuItem5});
            this.richTextBoxesContextMenuStrip.Name = "richTextBoxesContextMenuStrip";
            this.richTextBoxesContextMenuStrip.ShowCheckMargin = true;
            this.richTextBoxesContextMenuStrip.ShowImageMargin = false;
            this.richTextBoxesContextMenuStrip.Size = new System.Drawing.Size(272, 270);
            this.richTextBoxesContextMenuStrip.Opening += new System.ComponentModel.CancelEventHandler(this.richTextBoxesContextMenuStrip_Opening);
            // 
            // pauseToolStripMenuItem
            // 
            this.pauseToolStripMenuItem.Name = "pauseToolStripMenuItem";
            this.pauseToolStripMenuItem.Size = new System.Drawing.Size(271, 22);
            this.pauseToolStripMenuItem.Text = "&Pause";
            this.pauseToolStripMenuItem.Click += new System.EventHandler(this.pauseToolStripMenuItem_Click);
            // 
            // copyToolStripMenuItem
            // 
            this.copyToolStripMenuItem.Name = "copyToolStripMenuItem";
            this.copyToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.C)));
            this.copyToolStripMenuItem.Size = new System.Drawing.Size(271, 22);
            this.copyToolStripMenuItem.Text = "Cop&y";
            this.copyToolStripMenuItem.Click += new System.EventHandler(this.copyToolStripMenuItem_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(268, 6);
            // 
            // clearAllToolStripMenuItem
            // 
            this.clearAllToolStripMenuItem.Name = "clearAllToolStripMenuItem";
            this.clearAllToolStripMenuItem.Size = new System.Drawing.Size(271, 22);
            this.clearAllToolStripMenuItem.Text = "Clear Al&l";
            this.clearAllToolStripMenuItem.Click += new System.EventHandler(this.clearAllToolStripMenuItem_Click);
            // 
            // selectAllToolStripMenuItem
            // 
            this.selectAllToolStripMenuItem.Name = "selectAllToolStripMenuItem";
            this.selectAllToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.A)));
            this.selectAllToolStripMenuItem.Size = new System.Drawing.Size(271, 22);
            this.selectAllToolStripMenuItem.Text = "Select &All";
            this.selectAllToolStripMenuItem.Visible = false;
            this.selectAllToolStripMenuItem.Click += new System.EventHandler(this.selectAllToolStripMenuItem_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(268, 6);
            // 
            // multiColorToolStripMenuItem
            // 
            this.multiColorToolStripMenuItem.Checked = true;
            this.multiColorToolStripMenuItem.CheckOnClick = true;
            this.multiColorToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.multiColorToolStripMenuItem.Name = "multiColorToolStripMenuItem";
            this.multiColorToolStripMenuItem.Size = new System.Drawing.Size(271, 22);
            this.multiColorToolStripMenuItem.Text = "Multi-&Color";
            this.multiColorToolStripMenuItem.Click += new System.EventHandler(this.multiColorToolStripMenuItem_Click);
            // 
            // toolStripSeparator12
            // 
            this.toolStripSeparator12.Name = "toolStripSeparator12";
            this.toolStripSeparator12.Size = new System.Drawing.Size(268, 6);
            // 
            // emailOutputToolStripMenuItem
            // 
            this.emailOutputToolStripMenuItem.Name = "emailOutputToolStripMenuItem";
            this.emailOutputToolStripMenuItem.Size = new System.Drawing.Size(271, 22);
            this.emailOutputToolStripMenuItem.Text = "&Email Output...";
            this.emailOutputToolStripMenuItem.Click += new System.EventHandler(this.emailOutputToolStripMenuItem_Click);
            // 
            // toolStripSeparator13
            // 
            this.toolStripSeparator13.Name = "toolStripSeparator13";
            this.toolStripSeparator13.Size = new System.Drawing.Size(268, 6);
            // 
            // findToolStripMenuItem
            // 
            this.findToolStripMenuItem.Name = "findToolStripMenuItem";
            this.findToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F)));
            this.findToolStripMenuItem.Size = new System.Drawing.Size(271, 22);
            this.findToolStripMenuItem.Text = "&Find";
            this.findToolStripMenuItem.Click += new System.EventHandler(this.findToolStripMenuItem_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.ShortcutKeys = System.Windows.Forms.Keys.F3;
            this.toolStripMenuItem2.Size = new System.Drawing.Size(271, 22);
            this.toolStripMenuItem2.Text = "Search Again Down";
            this.toolStripMenuItem2.Visible = false;
            this.toolStripMenuItem2.Click += new System.EventHandler(this.searchAgainDownToolStripMenuItem);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Shift | System.Windows.Forms.Keys.F3)));
            this.toolStripMenuItem3.Size = new System.Drawing.Size(271, 22);
            this.toolStripMenuItem3.Text = "Search Again Up";
            this.toolStripMenuItem3.Visible = false;
            this.toolStripMenuItem3.Click += new System.EventHandler(this.searchAgainUpToolStripMenuItem);
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F3)));
            this.toolStripMenuItem4.Size = new System.Drawing.Size(271, 22);
            this.toolStripMenuItem4.Text = "Search Highlighted Down";
            this.toolStripMenuItem4.Visible = false;
            this.toolStripMenuItem4.Click += new System.EventHandler(this.searchHighlightedDownToolStripMenuItem);
            // 
            // toolStripMenuItem5
            // 
            this.toolStripMenuItem5.Name = "toolStripMenuItem5";
            this.toolStripMenuItem5.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.F3)));
            this.toolStripMenuItem5.Size = new System.Drawing.Size(271, 22);
            this.toolStripMenuItem5.Text = "Search Highlighted Up";
            this.toolStripMenuItem5.Visible = false;
            this.toolStripMenuItem5.Click += new System.EventHandler(this.searchHighlightedUpToolStripMenuItem);
            // 
            // displayDockableWindow
            // 
            this.displayDockableWindow.Controls.Add(this.DisplayRichTextBox);
            dockingRules4.AllowDockBottom = true;
            dockingRules4.AllowDockLeft = true;
            dockingRules4.AllowDockRight = true;
            dockingRules4.AllowDockTop = true;
            dockingRules4.AllowFloat = true;
            dockingRules4.AllowTab = true;
            this.displayDockableWindow.DockingRules = dockingRules4;
            this.displayDockableWindow.Guid = new System.Guid("c172273c-f01e-4782-b915-b0130a116e1c");
            this.displayDockableWindow.Location = new System.Drawing.Point(1228, 254);
            this.displayDockableWindow.Name = "displayDockableWindow";
            this.displayDockableWindow.ShowOptions = false;
            this.displayDockableWindow.Size = new System.Drawing.Size(1228, 203);
            this.displayDockableWindow.TabImage = global::rag.Properties.Resources.display;
            this.displayDockableWindow.TabIndex = 0;
            this.displayDockableWindow.Text = "Display";
            // 
            // warningsDockableWindow
            // 
            this.warningsDockableWindow.Controls.Add(this.WarningRichTextBox);
            dockingRules5.AllowDockBottom = true;
            dockingRules5.AllowDockLeft = true;
            dockingRules5.AllowDockRight = true;
            dockingRules5.AllowDockTop = true;
            dockingRules5.AllowFloat = true;
            dockingRules5.AllowTab = true;
            this.warningsDockableWindow.DockingRules = dockingRules5;
            this.warningsDockableWindow.Guid = new System.Guid("d41da0d5-781e-487c-b3a7-0d080fe67f47");
            this.warningsDockableWindow.Location = new System.Drawing.Point(1228, 254);
            this.warningsDockableWindow.Name = "warningsDockableWindow";
            this.warningsDockableWindow.ShowOptions = false;
            this.warningsDockableWindow.Size = new System.Drawing.Size(1228, 203);
            this.warningsDockableWindow.TabImage = global::rag.Properties.Resources.warning;
            this.warningsDockableWindow.TabIndex = 1;
            this.warningsDockableWindow.Text = "Warnings";
            // 
            // errorsDockableWindow
            // 
            this.errorsDockableWindow.Controls.Add(this.ErrorRichTextBox);
            dockingRules6.AllowDockBottom = true;
            dockingRules6.AllowDockLeft = true;
            dockingRules6.AllowDockRight = true;
            dockingRules6.AllowDockTop = true;
            dockingRules6.AllowFloat = true;
            dockingRules6.AllowTab = true;
            this.errorsDockableWindow.DockingRules = dockingRules6;
            this.errorsDockableWindow.Guid = new System.Guid("5d787407-1e3c-4840-ab49-dd8788d51443");
            this.errorsDockableWindow.Location = new System.Drawing.Point(1228, 254);
            this.errorsDockableWindow.Name = "errorsDockableWindow";
            this.errorsDockableWindow.ShowOptions = false;
            this.errorsDockableWindow.Size = new System.Drawing.Size(1228, 203);
            this.errorsDockableWindow.TabImage = global::rag.Properties.Resources.error;
            this.errorsDockableWindow.TabIndex = 2;
            this.errorsDockableWindow.Text = "Errors";
            // 
            // scriptsDockableWindow
            // 
            this.scriptsDockableWindow.Controls.Add(this.ScriptRichTextBox);
            dockingRules7.AllowDockBottom = true;
            dockingRules7.AllowDockLeft = true;
            dockingRules7.AllowDockRight = true;
            dockingRules7.AllowDockTop = true;
            dockingRules7.AllowFloat = true;
            dockingRules7.AllowTab = true;
            this.scriptsDockableWindow.DockingRules = dockingRules7;
            this.scriptsDockableWindow.Guid = new System.Guid("9be9f55a-009c-48a1-b521-18f008f70d0b");
            this.scriptsDockableWindow.Location = new System.Drawing.Point(1228, 254);
            this.scriptsDockableWindow.Name = "scriptsDockableWindow";
            this.scriptsDockableWindow.ShowOptions = false;
            this.scriptsDockableWindow.Size = new System.Drawing.Size(1228, 203);
            this.scriptsDockableWindow.TabImage = global::rag.Properties.Resources.Scripts;
            this.scriptsDockableWindow.TabIndex = 3;
            this.scriptsDockableWindow.Text = "Scripts";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.viewsToolStripMenuItem,
            this.viewersToolStripMenuItem,
            this.windowsToolStripMenuItem,
            this.pluginsToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1228, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openLayoutToolStripMenuItem,
            this.openSharedLayoutsToolStripMenuItem,
            this.toolStripSeparator1,
            this.saveLayoutToolStripMenuItem,
            this.saveLayoutAsToolStripMenuItem,
            this.toolStripSeparator2,
            this.recentLayoutsToolStripMenuItem,
            this.toolStripSeparator3,
            this.preferencesToolStripMenuItem,
            this.toolStripSeparator5,
            this.exitApplicationToolStripMenuItem,
            this.exitRAGToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // openLayoutToolStripMenuItem
            // 
            this.openLayoutToolStripMenuItem.Name = "openLayoutToolStripMenuItem";
            this.openLayoutToolStripMenuItem.Size = new System.Drawing.Size(181, 22);
            this.openLayoutToolStripMenuItem.Text = "&Open Layout...";
            this.openLayoutToolStripMenuItem.Click += new System.EventHandler(this.openLayoutToolStripMenuItem_Click);
            // 
            // openSharedLayoutsToolStripMenuItem
            // 
            this.openSharedLayoutsToolStripMenuItem.Name = "openSharedLayoutsToolStripMenuItem";
            this.openSharedLayoutsToolStripMenuItem.Size = new System.Drawing.Size(181, 22);
            this.openSharedLayoutsToolStripMenuItem.Text = "Open Shared Layout";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(178, 6);
            // 
            // saveLayoutToolStripMenuItem
            // 
            this.saveLayoutToolStripMenuItem.Name = "saveLayoutToolStripMenuItem";
            this.saveLayoutToolStripMenuItem.Size = new System.Drawing.Size(181, 22);
            this.saveLayoutToolStripMenuItem.Text = "&Save Layout";
            this.saveLayoutToolStripMenuItem.Click += new System.EventHandler(this.saveLayoutToolStripMenuItem_Click);
            // 
            // saveLayoutAsToolStripMenuItem
            // 
            this.saveLayoutAsToolStripMenuItem.Name = "saveLayoutAsToolStripMenuItem";
            this.saveLayoutAsToolStripMenuItem.Size = new System.Drawing.Size(181, 22);
            this.saveLayoutAsToolStripMenuItem.Text = "Save Layout &As...";
            this.saveLayoutAsToolStripMenuItem.Click += new System.EventHandler(this.saveLayoutAsToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(178, 6);
            // 
            // recentLayoutsToolStripMenuItem
            // 
            this.recentLayoutsToolStripMenuItem.Enabled = false;
            this.recentLayoutsToolStripMenuItem.Name = "recentLayoutsToolStripMenuItem";
            this.recentLayoutsToolStripMenuItem.Size = new System.Drawing.Size(181, 22);
            this.recentLayoutsToolStripMenuItem.Text = "&Recent Layouts";
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(178, 6);
            // 
            // preferencesToolStripMenuItem
            // 
            this.preferencesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.userPreferencesDirectoryToolStripMenuItem,
            this.toolStripSeparator9,
            this.dumpOutputInRealTimeToolStripMenuItem,
            this.dumpOutputOnExitToolStripMenuItem,
            this.appendTimestampToOutputFileNameToolStripMenuItem,
            this.zipOutputOnExitToolStripMenuItem,
            this.toolStripSeparator8,
            this.suppressAssertCrashQuitDialogOnExitToolStripMenuItem,
            this.enableColouredModificationsToolStripMenuItem,
            this.enableNumPadOverrideToolStripMenuItem});
            this.preferencesToolStripMenuItem.Name = "preferencesToolStripMenuItem";
            this.preferencesToolStripMenuItem.Size = new System.Drawing.Size(181, 22);
            this.preferencesToolStripMenuItem.Text = "&Preferences";
            // 
            // userPreferencesDirectoryToolStripMenuItem
            // 
            this.userPreferencesDirectoryToolStripMenuItem.Name = "userPreferencesDirectoryToolStripMenuItem";
            this.userPreferencesDirectoryToolStripMenuItem.Size = new System.Drawing.Size(297, 22);
            this.userPreferencesDirectoryToolStripMenuItem.Text = "&User Preferences Directory";
            this.userPreferencesDirectoryToolStripMenuItem.Click += new System.EventHandler(this.userPreferencesDirectoryToolStripMenuItem_Click);
            // 
            // toolStripSeparator9
            // 
            this.toolStripSeparator9.Name = "toolStripSeparator9";
            this.toolStripSeparator9.Size = new System.Drawing.Size(294, 6);
            // 
            // dumpOutputInRealTimeToolStripMenuItem
            // 
            this.dumpOutputInRealTimeToolStripMenuItem.CheckOnClick = true;
            this.dumpOutputInRealTimeToolStripMenuItem.Name = "dumpOutputInRealTimeToolStripMenuItem";
            this.dumpOutputInRealTimeToolStripMenuItem.Size = new System.Drawing.Size(297, 22);
            this.dumpOutputInRealTimeToolStripMenuItem.Text = "&Dump Output in Real Time";
            this.dumpOutputInRealTimeToolStripMenuItem.Click += new System.EventHandler(this.dumpOutputInRealTimeToolStripMenuItem_Click);
            // 
            // dumpOutputOnExitToolStripMenuItem
            // 
            this.dumpOutputOnExitToolStripMenuItem.CheckOnClick = true;
            this.dumpOutputOnExitToolStripMenuItem.Name = "dumpOutputOnExitToolStripMenuItem";
            this.dumpOutputOnExitToolStripMenuItem.Size = new System.Drawing.Size(297, 22);
            this.dumpOutputOnExitToolStripMenuItem.Text = "Dump Output on &Exit";
            this.dumpOutputOnExitToolStripMenuItem.Click += new System.EventHandler(this.dumpOutputOnExitToolStripMenuItem_Click);
            // 
            // appendTimestampToOutputFileNameToolStripMenuItem
            // 
            this.appendTimestampToOutputFileNameToolStripMenuItem.CheckOnClick = true;
            this.appendTimestampToOutputFileNameToolStripMenuItem.Name = "appendTimestampToOutputFileNameToolStripMenuItem";
            this.appendTimestampToOutputFileNameToolStripMenuItem.Size = new System.Drawing.Size(297, 22);
            this.appendTimestampToOutputFileNameToolStripMenuItem.Text = "&Append Timestamp To Output File Name";
            this.appendTimestampToOutputFileNameToolStripMenuItem.Click += new System.EventHandler(this.appendTimestampToOutputFileNameToolStripMenuItem_Click);
            // 
            // zipOutputOnExitToolStripMenuItem
            // 
            this.zipOutputOnExitToolStripMenuItem.CheckOnClick = true;
            this.zipOutputOnExitToolStripMenuItem.Name = "zipOutputOnExitToolStripMenuItem";
            this.zipOutputOnExitToolStripMenuItem.Size = new System.Drawing.Size(297, 22);
            this.zipOutputOnExitToolStripMenuItem.Text = "&Zip Output on Exit";
            this.zipOutputOnExitToolStripMenuItem.Click += new System.EventHandler(this.zipOutputOnExitToolStripMenuItem_Click);
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(294, 6);
            // 
            // suppressAssertCrashQuitDialogOnExitToolStripMenuItem
            // 
            this.suppressAssertCrashQuitDialogOnExitToolStripMenuItem.CheckOnClick = true;
            this.suppressAssertCrashQuitDialogOnExitToolStripMenuItem.Name = "suppressAssertCrashQuitDialogOnExitToolStripMenuItem";
            this.suppressAssertCrashQuitDialogOnExitToolStripMenuItem.Size = new System.Drawing.Size(297, 22);
            this.suppressAssertCrashQuitDialogOnExitToolStripMenuItem.Text = "Su&ppress Assert/Crash/Quit Dialog On Exit";
            this.suppressAssertCrashQuitDialogOnExitToolStripMenuItem.Click += new System.EventHandler(this.suppressAssertCrashQuitDialogOnExitToolStripMenuItem_Click);
            // 
            // enableColouredModificationsToolStripMenuItem
            // 
            this.enableColouredModificationsToolStripMenuItem.Checked = true;
            this.enableColouredModificationsToolStripMenuItem.CheckOnClick = true;
            this.enableColouredModificationsToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.enableColouredModificationsToolStripMenuItem.Name = "enableColouredModificationsToolStripMenuItem";
            this.enableColouredModificationsToolStripMenuItem.Size = new System.Drawing.Size(297, 22);
            this.enableColouredModificationsToolStripMenuItem.Text = "Enable Coloured Modifications";
            this.enableColouredModificationsToolStripMenuItem.Click += new System.EventHandler(this.enableColouredModificationsToolStripMenuItem_Click);
            // 
            // enableNumPadOverrideToolStripMenuItem
            // 
            this.enableNumPadOverrideToolStripMenuItem.Name = "enableNumPadOverrideToolStripMenuItem";
            this.enableNumPadOverrideToolStripMenuItem.Size = new System.Drawing.Size(297, 22);
            this.enableNumPadOverrideToolStripMenuItem.Text = "Enable Number Pad Override";
            this.enableNumPadOverrideToolStripMenuItem.ToolTipText = "This will force the number lock on, even when" +
    " shift tries to disable it.";
            this.enableNumPadOverrideToolStripMenuItem.Click += new System.EventHandler(this.enableShiftNumPadOverrideToolStripMenuItem_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(178, 6);
            // 
            // exitApplicationToolStripMenuItem
            // 
            this.exitApplicationToolStripMenuItem.Enabled = false;
            this.exitApplicationToolStripMenuItem.Name = "exitApplicationToolStripMenuItem";
            this.exitApplicationToolStripMenuItem.Size = new System.Drawing.Size(181, 22);
            this.exitApplicationToolStripMenuItem.Text = "E&xit Application";
            this.exitApplicationToolStripMenuItem.Click += new System.EventHandler(this.exitApplicationToolStripMenuItem_Click);
            // 
            // exitRAGToolStripMenuItem
            // 
            this.exitRAGToolStripMenuItem.Image = global::rag.Properties.Resources.rs;
            this.exitRAGToolStripMenuItem.Name = "exitRAGToolStripMenuItem";
            this.exitRAGToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F4)));
            this.exitRAGToolStripMenuItem.Size = new System.Drawing.Size(181, 22);
            this.exitRAGToolStripMenuItem.Text = "&Exit RAG";
            this.exitRAGToolStripMenuItem.Click += new System.EventHandler(this.exitRAGToolStripMenuItem_Click);
            // 
            // viewsToolStripMenuItem
            // 
            this.viewsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.disabledWidgetsToolStripMenuItem,
            this.toolStripSeparator11,
            this.addWidgetViewToolStripMenuItem,
            this.addFindViewToolStripMenuItem,
            this.addFavoritesViewToolStripMenuItem,
            this.toolStripSeparator7,
            this.sharedFavoritesToolStripMenuItem});
            this.viewsToolStripMenuItem.Name = "viewsToolStripMenuItem";
            this.viewsToolStripMenuItem.Size = new System.Drawing.Size(49, 20);
            this.viewsToolStripMenuItem.Text = "&Views";
            // 
            // disabledWidgetsToolStripMenuItem
            // 
            this.disabledWidgetsToolStripMenuItem.Name = "disabledWidgetsToolStripMenuItem";
            this.disabledWidgetsToolStripMenuItem.Size = new System.Drawing.Size(213, 22);
            this.disabledWidgetsToolStripMenuItem.Text = "Disabled Widgets";
            this.disabledWidgetsToolStripMenuItem.Click += new System.EventHandler(this.disabledWidgetsToolStripMenuItem_Click);
            // 
            // toolStripSeparator11
            // 
            this.toolStripSeparator11.Name = "toolStripSeparator11";
            this.toolStripSeparator11.Size = new System.Drawing.Size(210, 6);
            // 
            // addWidgetViewToolStripMenuItem
            // 
            this.addWidgetViewToolStripMenuItem.Name = "addWidgetViewToolStripMenuItem";
            this.addWidgetViewToolStripMenuItem.Size = new System.Drawing.Size(213, 22);
            this.addWidgetViewToolStripMenuItem.Text = "&Add Widget View";
            this.addWidgetViewToolStripMenuItem.Click += new System.EventHandler(this.addWidgetViewToolStripMenuItem_Click);
            // 
            // addFindViewToolStripMenuItem
            // 
            this.addFindViewToolStripMenuItem.Image = global::rag.Properties.Resources.find;
            this.addFindViewToolStripMenuItem.Name = "addFindViewToolStripMenuItem";
            this.addFindViewToolStripMenuItem.Size = new System.Drawing.Size(213, 22);
            this.addFindViewToolStripMenuItem.Text = "Add &Find View";
            this.addFindViewToolStripMenuItem.Click += new System.EventHandler(this.addFindViewToolStripMenuItem_Click);
            // 
            // addFavoritesViewToolStripMenuItem
            // 
            this.addFavoritesViewToolStripMenuItem.Image = global::rag.Properties.Resources.favorites;
            this.addFavoritesViewToolStripMenuItem.Name = "addFavoritesViewToolStripMenuItem";
            this.addFavoritesViewToolStripMenuItem.Size = new System.Drawing.Size(213, 22);
            this.addFavoritesViewToolStripMenuItem.Text = "Add Fa&vorites View";
            this.addFavoritesViewToolStripMenuItem.Click += new System.EventHandler(this.addFavoritesViewToolStripMenuItem_Click);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(210, 6);
            // 
            // sharedFavoritesToolStripMenuItem
            // 
            this.sharedFavoritesToolStripMenuItem.Name = "sharedFavoritesToolStripMenuItem";
            this.sharedFavoritesToolStripMenuItem.Size = new System.Drawing.Size(213, 22);
            this.sharedFavoritesToolStripMenuItem.Text = "Add Shared Favorites View";
            // 
            // viewersToolStripMenuItem
            // 
            this.viewersToolStripMenuItem.Enabled = false;
            this.viewersToolStripMenuItem.Name = "viewersToolStripMenuItem";
            this.viewersToolStripMenuItem.Size = new System.Drawing.Size(59, 20);
            this.viewersToolStripMenuItem.Text = "&Viewers";
            // 
            // windowsToolStripMenuItem
            // 
            this.windowsToolStripMenuItem.Name = "windowsToolStripMenuItem";
            this.windowsToolStripMenuItem.Size = new System.Drawing.Size(68, 20);
            this.windowsToolStripMenuItem.Text = "&Windows";
            this.windowsToolStripMenuItem.DropDownOpening += new System.EventHandler(this.windowsToolStripMenuItem_DropDownOpening);
            // 
            // pluginsToolStripMenuItem
            // 
            this.pluginsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pluginsToolStripMenuItem1});
            this.pluginsToolStripMenuItem.Name = "pluginsToolStripMenuItem";
            this.pluginsToolStripMenuItem.Size = new System.Drawing.Size(58, 20);
            this.pluginsToolStripMenuItem.Text = "&Plugins";
            // 
            // pluginsToolStripMenuItem1
            // 
            this.pluginsToolStripMenuItem1.Name = "pluginsToolStripMenuItem1";
            this.pluginsToolStripMenuItem1.Size = new System.Drawing.Size(122, 22);
            this.pluginsToolStripMenuItem1.Text = "&Plugins...";
            this.pluginsToolStripMenuItem1.Click += new System.EventHandler(this.pluginsToolStripMenuItem1_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.helpToolStripMenuItem1,
            this.commandLineToolStripMenuItem,
            this.throwExceptionToolStripMenuItem,
            this.toolStripSeparator10,
            this.submitFeedbackToolStripMenuItem,
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "&Help";
            // 
            // helpToolStripMenuItem1
            // 
            this.helpToolStripMenuItem1.Name = "helpToolStripMenuItem1";
            this.helpToolStripMenuItem1.ShortcutKeys = System.Windows.Forms.Keys.F1;
            this.helpToolStripMenuItem1.Size = new System.Drawing.Size(174, 22);
            this.helpToolStripMenuItem1.Text = "&Help";
            this.helpToolStripMenuItem1.Click += new System.EventHandler(this.helpToolStripMenuItem1_Click);
            // 
            // commandLineToolStripMenuItem
            // 
            this.commandLineToolStripMenuItem.Name = "commandLineToolStripMenuItem";
            this.commandLineToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
            this.commandLineToolStripMenuItem.Text = "&Command Line...";
            this.commandLineToolStripMenuItem.Click += new System.EventHandler(this.commandLineToolStripMenuItem_Click);
            // 
            // throwExceptionToolStripMenuItem
            // 
            this.throwExceptionToolStripMenuItem.Enabled = false;
            this.throwExceptionToolStripMenuItem.Name = "throwExceptionToolStripMenuItem";
            this.throwExceptionToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
            this.throwExceptionToolStripMenuItem.Text = "Throw Exception";
            this.throwExceptionToolStripMenuItem.Click += new System.EventHandler(this.throwExceptionToolStripMenuItem_Click);
            // 
            // toolStripSeparator10
            // 
            this.toolStripSeparator10.Name = "toolStripSeparator10";
            this.toolStripSeparator10.Size = new System.Drawing.Size(171, 6);
            // 
            // submitFeedbackToolStripMenuItem
            // 
            this.submitFeedbackToolStripMenuItem.Name = "submitFeedbackToolStripMenuItem";
            this.submitFeedbackToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
            this.submitFeedbackToolStripMenuItem.Text = "&Submit Feedback...";
            this.submitFeedbackToolStripMenuItem.Click += new System.EventHandler(this.submitFeedbackToolStripMenuItem_Click);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
            this.aboutToolStripMenuItem.Text = "&About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // layoutSaveAsFileDialog
            // 
            this.layoutSaveAsFileDialog.DefaultExt = "xml";
            this.layoutSaveAsFileDialog.Filter = "RAG layout files (.raglayout.*.xml)|.raglayout.*.xml|All files (*.*)|*.*";
            this.layoutSaveAsFileDialog.SupportMultiDottedExtensions = true;
            this.layoutSaveAsFileDialog.Title = "Save Layout As";
            // 
            // updateTimer
            // 
            this.updateTimer.Interval = 16;
            this.updateTimer.Tick += new System.EventHandler(this.updateTimer_Tick);
            // 
            // dockControlContextMenuStrip
            // 
            this.dockControlContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.disposeToolStripMenuItem,
            this.hideToolStripMenuItem});
            this.dockControlContextMenuStrip.Name = "dockControlContextMenuStrip";
            this.dockControlContextMenuStrip.Size = new System.Drawing.Size(185, 48);
            // 
            // disposeToolStripMenuItem
            // 
            this.disposeToolStripMenuItem.CheckOnClick = true;
            this.disposeToolStripMenuItem.Name = "disposeToolStripMenuItem";
            this.disposeToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.disposeToolStripMenuItem.Text = "&Dispose when closed";
            // 
            // hideToolStripMenuItem
            // 
            this.hideToolStripMenuItem.CheckOnClick = true;
            this.hideToolStripMenuItem.Name = "hideToolStripMenuItem";
            this.hideToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.hideToolStripMenuItem.Text = "&Hide when closed";
            // 
            // tabDocContextMenuStrip
            // 
            this.tabDocContextMenuStrip.Name = "tabDocContextMenuStrip";
            this.tabDocContextMenuStrip.Size = new System.Drawing.Size(61, 4);
            // 
            // OutputRichTextBox
            // 
            this.OutputRichTextBox.BackColor = System.Drawing.SystemColors.Window;
            this.OutputRichTextBox.ContextMenuStrip = this.richTextBoxesContextMenuStrip;
            this.OutputRichTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.OutputRichTextBox.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OutputRichTextBox.HideSelection = false;
            this.OutputRichTextBox.Location = new System.Drawing.Point(0, 0);
            this.OutputRichTextBox.Name = "OutputRichTextBox";
            this.OutputRichTextBox.ReadOnly = true;
            this.OutputRichTextBox.Size = new System.Drawing.Size(1228, 203);
            this.OutputRichTextBox.TabIndex = 0;
            this.OutputRichTextBox.Text = "";
            this.OutputRichTextBox.LinkClicked += new System.Windows.Forms.LinkClickedEventHandler(this.OutputRichTextBox_LinkClicked);
            this.OutputRichTextBox.MouseEnter += new System.EventHandler(this.richTextBoxes_MouseEnter);
            // 
            // DisplayRichTextBox
            // 
            this.DisplayRichTextBox.BackColor = System.Drawing.SystemColors.Window;
            this.DisplayRichTextBox.ContextMenuStrip = this.richTextBoxesContextMenuStrip;
            this.DisplayRichTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DisplayRichTextBox.Font = new System.Drawing.Font("Courier New", 8.25F);
            this.DisplayRichTextBox.HideSelection = false;
            this.DisplayRichTextBox.Location = new System.Drawing.Point(0, 0);
            this.DisplayRichTextBox.Name = "DisplayRichTextBox";
            this.DisplayRichTextBox.ReadOnly = true;
            this.DisplayRichTextBox.Size = new System.Drawing.Size(1228, 203);
            this.DisplayRichTextBox.TabIndex = 0;
            this.DisplayRichTextBox.Text = "";
            this.DisplayRichTextBox.MouseEnter += new System.EventHandler(this.richTextBoxes_MouseEnter);
            // 
            // WarningRichTextBox
            // 
            this.WarningRichTextBox.BackColor = System.Drawing.SystemColors.Window;
            this.WarningRichTextBox.ContextMenuStrip = this.richTextBoxesContextMenuStrip;
            this.WarningRichTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.WarningRichTextBox.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.WarningRichTextBox.ForeColor = System.Drawing.Color.Goldenrod;
            this.WarningRichTextBox.HideSelection = false;
            this.WarningRichTextBox.Location = new System.Drawing.Point(0, 0);
            this.WarningRichTextBox.Name = "WarningRichTextBox";
            this.WarningRichTextBox.ReadOnly = true;
            this.WarningRichTextBox.Size = new System.Drawing.Size(1228, 203);
            this.WarningRichTextBox.TabIndex = 0;
            this.WarningRichTextBox.Text = "";
            this.WarningRichTextBox.MouseEnter += new System.EventHandler(this.richTextBoxes_MouseEnter);
            // 
            // ErrorRichTextBox
            // 
            this.ErrorRichTextBox.BackColor = System.Drawing.SystemColors.Window;
            this.ErrorRichTextBox.ContextMenuStrip = this.richTextBoxesContextMenuStrip;
            this.ErrorRichTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ErrorRichTextBox.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ErrorRichTextBox.ForeColor = System.Drawing.Color.Red;
            this.ErrorRichTextBox.HideSelection = false;
            this.ErrorRichTextBox.Location = new System.Drawing.Point(0, 0);
            this.ErrorRichTextBox.Name = "ErrorRichTextBox";
            this.ErrorRichTextBox.ReadOnly = true;
            this.ErrorRichTextBox.Size = new System.Drawing.Size(1228, 203);
            this.ErrorRichTextBox.TabIndex = 0;
            this.ErrorRichTextBox.Text = "";
            this.ErrorRichTextBox.MouseEnter += new System.EventHandler(this.richTextBoxes_MouseEnter);
            // 
            // ScriptRichTextBox
            // 
            this.ScriptRichTextBox.BackColor = System.Drawing.SystemColors.Window;
            this.ScriptRichTextBox.ContextMenuStrip = this.richTextBoxesContextMenuStrip;
            this.ScriptRichTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ScriptRichTextBox.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ScriptRichTextBox.ForeColor = System.Drawing.Color.Chocolate;
            this.ScriptRichTextBox.HideSelection = false;
            this.ScriptRichTextBox.Location = new System.Drawing.Point(0, 0);
            this.ScriptRichTextBox.Name = "ScriptRichTextBox";
            this.ScriptRichTextBox.ReadOnly = true;
            this.ScriptRichTextBox.Size = new System.Drawing.Size(1228, 203);
            this.ScriptRichTextBox.TabIndex = 0;
            this.ScriptRichTextBox.Text = "";
            this.ScriptRichTextBox.MouseEnter += new System.EventHandler(this.richTextBoxes_MouseEnter);
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1228, 716);
            this.Controls.Add(this.toolStripContainer1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainWindow";
            this.Text = "RAG";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainWindow_FormClosing);
            this.Load += new System.EventHandler(this.MainWindow_Load);
            this.Shown += new System.EventHandler(this.MainWindow_Shown);
            dockContainer1.ResumeLayout(false);
            this.toolStripContainer1.BottomToolStripPanel.ResumeLayout(false);
            this.toolStripContainer1.BottomToolStripPanel.PerformLayout();
            this.toolStripContainer1.ContentPanel.ResumeLayout(false);
            this.toolStripContainer1.TopToolStripPanel.ResumeLayout(false);
            this.toolStripContainer1.TopToolStripPanel.PerformLayout();
            this.toolStripContainer1.ResumeLayout(false);
            this.toolStripContainer1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.gameViewDocumentContainer.ResumeLayout(false);
            dockContainer3.ResumeLayout(false);
            this.outputDockableWindow.ResumeLayout(false);
            this.richTextBoxesContextMenuStrip.ResumeLayout(false);
            this.displayDockableWindow.ResumeLayout(false);
            this.warningsDockableWindow.ResumeLayout(false);
            this.errorsDockableWindow.ResumeLayout(false);
            this.scriptsDockableWindow.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.dockControlContextMenuStrip.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ToolStripContainer toolStripContainer1;
        private TD.SandDock.DockableWindow bankViewDockableWindow;
        private TD.SandDock.SandDockManager sandDockManager1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openLayoutToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem saveLayoutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveLayoutAsToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem recentLayoutsToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem exitApplicationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitRAGToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pluginsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pluginsToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem commandLineToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem throwExceptionToolStripMenuItem;
        private TD.SandDock.DockableWindow outputDockableWindow;
        private TD.SandDock.DockableWindow warningsDockableWindow;
        private TD.SandDock.DockableWindow errorsDockableWindow;
        private TD.SandDock.DockableWindow scriptsDockableWindow;
        private TD.SandDock.TabbedDocument gameViewTabbedDocument;
        private rag.Controls.SuspendableRichTextBox OutputRichTextBox;
        private rag.Controls.SuspendableRichTextBox WarningRichTextBox;
        private rag.Controls.SuspendableRichTextBox ErrorRichTextBox;
        private rag.Controls.SuspendableRichTextBox ScriptRichTextBox;
        private System.Windows.Forms.ContextMenuStrip richTextBoxesContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem pauseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem copyToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem clearAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem selectAllToolStripMenuItem;
        private System.Windows.Forms.SaveFileDialog layoutSaveAsFileDialog;
        private System.Windows.Forms.Timer updateTimer;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel framerateToolStripStatusLabel;
        private System.Windows.Forms.ToolStripStatusLabel lastErrorToolStripStatusLabel;
        private TD.SandDock.DocumentContainer gameViewDocumentContainer;
        private System.Windows.Forms.ToolStripMenuItem preferencesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dumpOutputOnExitToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripMenuItem multiColorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem suppressAssertCrashQuitDialogOnExitToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip dockControlContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem disposeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem hideToolStripMenuItem;
        private TD.SandDock.DockableWindow displayDockableWindow;
        private rag.Controls.SuspendableRichTextBox DisplayRichTextBox;
        private System.Windows.Forms.ToolStripMenuItem dumpOutputInRealTimeToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripMenuItem userPreferencesDirectoryToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator9;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog;
        private System.Windows.Forms.ContextMenuStrip tabDocContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator10;
        private System.Windows.Forms.ToolStripMenuItem submitFeedbackToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem appendTimestampToOutputFileNameToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem windowsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addWidgetViewToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addFindViewToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addFavoritesViewToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem disabledWidgetsToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator11;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripMenuItem sharedFavoritesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openSharedLayoutsToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator12;
        private System.Windows.Forms.ToolStripMenuItem findToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem emailOutputToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator13;
        private System.Windows.Forms.ToolStripMenuItem zipOutputOnExitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem enableColouredModificationsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem enableNumPadOverrideToolStripMenuItem;
    }
}

