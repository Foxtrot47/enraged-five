using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Xml;

using ragCore;
using RSG.Base.Forms;
using TD.SandDock;
using ragWidgets;

namespace rag
{
    public partial class WidgetView : rag.WidgetViewBase
    {
        protected WidgetView()
        {
            InitializeComponent();
        }

        public WidgetView( string appName, IBankManager bankMgr, TD.SandDock.DockControl dockControl )
            : base( appName, bankMgr, dockControl )
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

            dockControl.Load += new EventHandler( dockControl_Load );
		}

        #region Variables
        private bool m_hideLeftPane = false;
        private bool m_hideHelpPane = false;

        private float m_leftPaneSplitterDistanceRatio = 0.25f;
        private float m_helpPaneSplitterDistanceRatio = 0.85f;

        private int m_leftPaneSplitterDistance = 0;
        private int m_helpPaneSplitterDistance = 0;
        #endregion

        #region Overrides
        public override string LabelHelp
        {
            set
            {
                this.helpLabel.Text = "     " + value; // put some whitespace in to account for help icon
            }
        }

        public override string LabelDescription
        {
            set
            {
                this.helpDescriptionLabel.Text = value;
            }
        }

        public override Bitmap GetBitmap() 
        { 
            return null; 
        }
        
        public override string GetTabText()
        { 
            return "Widget View"; 
        }

        public override void SetDirty()
        { 

        }
        
        public override void SetIndexForWidget( WidgetVisible visible, int newIndex ) 
        {

        }

        public override void AddContextMenuOptions( ContextMenuStrip menu, Widget widget, WidgetVisible visible )
        {
            if ( menu.Items.Count >= 1 )
            {
                menu.Items.Add( new ToolStripSeparator() );
            }

            ToolStripMenuItem item = new ToolStripMenuItem( (m_hideLeftPane ? "Show" : "Hide") + " Left Pane" );
            item.Click += new EventHandler( hideShowLeftPaneToolStripMenuItem_Click );
            menu.Items.Add( item );

            item = new ToolStripMenuItem( (m_hideHelpPane ? "Show" : "Hide") + " Help Pane" );
            item.Click += new EventHandler( hideShowHelpPaneToolStripMenuItem_Click );
            menu.Items.Add( item );
        }

        public override void DeserializeView( XmlTextReader xmlReader, IBankManager bankMgr )
        {
            this.BeginUpdate();

            base.DeserializeView( xmlReader, bankMgr );

            string leftPanelWidth = xmlReader["leftpanelwidth"];
            if ( !String.IsNullOrEmpty( leftPanelWidth ) )
            {
                if ( leftPanelWidth.Contains( "." ) )
                {
                    m_leftPaneSplitterDistanceRatio = float.Parse( leftPanelWidth );
                }
                else
                {
                    m_leftPaneSplitterDistance = Math.Max( int.Parse( leftPanelWidth ), 0 );
                }
            }

            string helpHeight = xmlReader["helpheight"];
            if ( !String.IsNullOrEmpty( helpHeight ) )
            {
                if ( helpHeight.Contains( "." ) )
                {
                    m_helpPaneSplitterDistanceRatio = float.Parse( helpHeight );
                }
                else
                {
                    m_helpPaneSplitterDistance = Math.Max( int.Parse( helpHeight ), 0 );
                }
            }

            // see if we hide panes:
            string hideTreeAndHelpPanes = xmlReader["hidetreeandhelppanes"];
            if ( !String.IsNullOrEmpty( hideTreeAndHelpPanes ) )
            {
                bool hide = bool.Parse( hideTreeAndHelpPanes );
                m_hideHelpPane = hide;
                m_hideLeftPane = hide;
            }

            string hideTreePane = xmlReader["HideTreePane"];
            if ( !String.IsNullOrEmpty( hideTreePane ) )
            {
                m_hideLeftPane = bool.Parse( hideTreePane );
            }

            string hideHelpPane = xmlReader["HideHelpPane"];
            if ( !String.IsNullOrEmpty( hideHelpPane ) )
            {
                m_hideHelpPane = bool.Parse( hideHelpPane );
            }

            this.EndUpdate();
        }

        protected override void SerializeView( XmlTextWriter xmlWriter, String exePathName )
        {
            base.SerializeView( xmlWriter, exePathName );

            xmlWriter.WriteAttributeString( "leftpanelwidth", this.splitContainer1.SplitterDistance.ToString() );
            xmlWriter.WriteAttributeString( "helpheight", this.splitContainer2.SplitterDistance.ToString() );

            xmlWriter.WriteAttributeString( "HideTreePane", m_hideLeftPane.ToString() );
            xmlWriter.WriteAttributeString( "HideHelpPane", m_hideHelpPane.ToString() );
        }

        public override Control GetControlAtCursorPoint()
        {
            Point point = this.splitContainer2.Panel1.PointToClient( Cursor.Position );

            Control ctrl = this.xPanderList.GetChildAtPoint( point );
            if ( ctrl is XPander )
            {
                return (ctrl as XPander).GetChildAtPoint( point );
            }

            return ctrl;
        }
        #endregion

        #region Public Virtual Functions
        public virtual void BeginUpdate()
        {
            this.xPanderList.BeginUpdate();
        }

        public virtual void EndUpdate()
        {
            this.xPanderList.EndUpdate();
        }
        #endregion

        #region Protected Functions
        protected void InitializeXpanderList( TD.SandDock.DockControl dockControl )
        {
            this.xPanderList.Name = "m_XpanderListWidgets" + dockControl.Guid.ToString();

            this.xPanderList.NestingBottomPad = Int32.Parse( MainWindow.GlobalPreferences.GetUiDataItem( "XPanderBottomPad", "10" ) );
            this.xPanderList.NestingIndent = Int32.Parse( MainWindow.GlobalPreferences.GetUiDataItem( "XPanderIndent", "10" ) );
        }
        #endregion

        #region Event Handlers
        private void dockControl_Load( object sender, EventArgs e )
        {
            try
            {
                if ( m_leftPaneSplitterDistance == 0 )
                {
                    this.splitContainer1.SplitterDistance = (int)(m_leftPaneSplitterDistanceRatio * this.splitContainer1.Width);
                }
                else
                {
                    this.splitContainer1.SplitterDistance = m_leftPaneSplitterDistance;
                }
            }
            catch ( Exception ex )
            {
                rageMessageBox.ShowWarning( this.DockControl, "There was an error loading the settings for " + this.DockControl.TabText + ":\n\n" + ex.Message + "\n\nLoading will continue.", "WidgetView Settings Error" );
            }
            
            this.splitContainer1.Panel1Collapsed = m_hideLeftPane;

            try
            {
                if ( m_helpPaneSplitterDistance == 0 )
                {
                    this.splitContainer2.SplitterDistance = (int)(m_helpPaneSplitterDistanceRatio * this.splitContainer2.Height);
                }
                else
                {
                    this.splitContainer2.SplitterDistance = m_helpPaneSplitterDistance;
                }

                this.splitContainer2.Panel2Collapsed = m_hideHelpPane;
            }
            catch ( Exception ex )
            {
                rageMessageBox.ShowWarning( this.DockControl, "There was an error loading the settings for " + this.DockControl.TabText + ":\n\n" + ex.Message + "\n\nLoading will continue.", "WidgetView Settings Error" );
            }
        }

        private void helpPaneContextMenuStrip_Opening( object sender, CancelEventArgs e )
        {
            string leftPaneText = (m_hideLeftPane ? "Show" : "Hide") + " Left Pane";
            string helpPaneText = (m_hideHelpPane ? "Show" : "Hide") + " Help Pane";
            
            this.leftPaneHideShowLeftPaneToolStripMenuItem.Text = leftPaneText;
            this.leftPaneHideShowHelpPaneToolStripMenuItem.Text = helpPaneText;

            this.mainPaneHideShowLeftPaneToolStripMenuItem.Text = leftPaneText;
            this.mainPaneHideShowHelpPaneToolStripMenuItem.Text = helpPaneText;

            this.helpPaneHideShowLeftPaneToolStripMenuItem.Text = leftPaneText;
            this.helpPaneHideShowHelpPaneToolStripMenuItem.Text = helpPaneText;
        }

        private void hideShowLeftPaneToolStripMenuItem_Click( object sender, EventArgs e )
        {
            m_hideLeftPane = !m_hideLeftPane;
            this.splitContainer1.Panel1Collapsed = m_hideLeftPane;
        }

        private void hideShowHelpPaneToolStripMenuItem_Click( object sender, EventArgs e )
        {
            m_hideHelpPane = !m_hideHelpPane;
            this.splitContainer2.Panel2Collapsed = m_hideHelpPane;
        }
        #endregion

		private void helpDescriptionLabel_LinkClicked(object sender, LinkClickedEventArgs e)
		{
			string strLinkText = e.LinkText;
			System.Diagnostics.Process.Start(strLinkText);
		}
    }
}

