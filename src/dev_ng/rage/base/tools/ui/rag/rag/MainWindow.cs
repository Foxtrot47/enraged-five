using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;

using rag.Properties;
using rag.Extensions;
using ragCore;
using TD.SandDock;
using RSG.Base.Command;
using RSG.Base.Drawing;
using RSG.Base.Extensions;
using RSG.Base.Forms;
using RSG.Base.IO;
using ragWidgets;
using rag.Controls;
using RSG.Base.Logging;

namespace rag
{
    public partial class MainWindow : Form
    {
        private readonly ILog _log;

        private String LogCtx
        {
            get { return "Main Window"; }
        }

        public MainWindow(ILog log)
        {
            _log = log;
            m_layoutFileInfos = new LayoutFileInfos(log);
            InitializeComponent();

            InitializeControls();
        }

        private void InitializeControls()
        {
            // set up delegates for dock control creation:
            RageApplication.DockControlCreatorRenderWindow = new RageApplication.DockControlCreatorDel(DockControlManager.Instance.CreateViewDockControl);
            RageApplication.DockControlCreatorWidgetView = new RageApplication.DockControlCreatorDel(DockControlManager.Instance.CreateWidgetDockControl);

            WidgetBank.AddBankEvent += new WidgetBank.BankDelegate(RageApplication.AddBank);
            WidgetBank.RemoveBankEvent += new WidgetBank.BankDelegate(RageApplication.RemoveBank);

            BankRemotePacketProcessor.DisplayMessage += new BankRemotePacketProcessor.DisplayMessageEventHandler(BankRemotePacket_OnDisplayMessage);
            NamedPipeSocket.DisplayMessage += new BankRemotePacketProcessor.DisplayMessageEventHandler(BankRemotePacket_OnDisplayMessage);


            BankManager.TimeStringHandler += new BankManager.TimeStringDelegate(SetTimeString);

            Sensor.OnOutputMessage += new Sensor.MessageEventHandler(Sensor_OnOutputMessage);

            DockControlManager.Instance.Initialize();

            // let the dock manager now about the master dock controls:
            DockControlManager.Instance.MasterDockControlWidgets = this.bankViewDockableWindow;
            DockControlManager.Instance.MasterDockControlViews = this.gameViewTabbedDocument;

            // let widget views know of docks, so they can search for the layout system:
            DockableViewManager.Instance.SandDock = this.sandDockManager1;
            RenderWindow.DocumentContainer = this.gameViewDocumentContainer;

            // add the favorites right click view:
            WidgetVisible.ContextMenuItemsDelegate = new WidgetVisible.AddContextMenuItemsDel(WidgetVisible_AddContextMenuItems);
            WidgetVisible.CreateWidgetViewDelegate = new WidgetVisible.CreateWidgetViewSingleDel(WidgetVisible_CreateWidgetSingle);

            RagWidgetVisualiser.InitialiseHandlers();

            Dictionary<string, MultiColorInfo> mcInfoDict = MainWindow.MultiColorOutputInfo;
            m_outputTextBoxDictionary.Add(this.OutputRichTextBox.Name, new OutputTextBoxInfo("Output", mcInfoDict["Output"], this.OutputRichTextBox, this.outputDockableWindow));
            m_outputTextBoxDictionary.Add(this.DisplayRichTextBox.Name, new OutputTextBoxInfo("Display", mcInfoDict["Display"], this.DisplayRichTextBox, this.displayDockableWindow));
            m_outputTextBoxDictionary.Add(this.WarningRichTextBox.Name, new OutputTextBoxInfo("Warning", mcInfoDict["Warning"], this.WarningRichTextBox, this.warningsDockableWindow));
            m_outputTextBoxDictionary.Add(this.ErrorRichTextBox.Name, new OutputTextBoxInfo("Error", mcInfoDict["Error"], this.ErrorRichTextBox, this.errorsDockableWindow));
            m_outputTextBoxDictionary.Add(this.ScriptRichTextBox.Name, new OutputTextBoxInfo("Script", mcInfoDict["Script"], this.ScriptRichTextBox, this.scriptsDockableWindow));

            // These event handlers are static, so we must initialize it ourselves
            this.disposeToolStripMenuItem.Click += new EventHandler(disposeToolStripMenuItem_Click);
            this.hideToolStripMenuItem.Click += new EventHandler(hideToolStripMenuItem_Click);

            // Rich Text Box Search Replace Dialog
            m_richTextBoxSearchReplaceDialog = new RichTextBoxSearchReplaceDialog();
            m_richTextBoxSearchReplaceDialog.CanReplace = false;
            m_richTextBoxSearchReplaceDialog.CanFindAll = false;
            m_richTextBoxSearchReplaceDialog.CanReplaceAll = false;
            m_richTextBoxSearchReplaceDialog.FindCurrentRichTextBox += new rageSearchReplaceRichTextBoxEventHandler(richTextBoxSearchReplaceDialog_FindCurrentRichTextBox);
            m_richTextBoxSearchReplaceDialog.SearchBegin += new rageSearchBeginEventHandler(richTextBoxSearchReplaceDialog_SearchBegin);
            m_richTextBoxSearchReplaceDialog.SearchComplete += new rageSearchCompleteEventHandler(richTextBoxSearchReplaceDialog_SearchComplete);

            enableColouredModificationsToolStripMenuItem.Checked = Settings.Default.ShowColouredModifications;
            ragCore.ViewSettings.ShowModifiedColours = Settings.Default.ShowColouredModifications;

            enableNumPadOverrideToolStripMenuItem.Checked = Settings.Default.NumPadOverride;
        }

        void BankRemotePacket_OnDisplayMessage(object sender, DisplayMessageEventArgs e)
        {
            if (e.Error)
            {
                rageMessageBox.ShowError(this, e.Message, e.Caption);
            }
            else
            {
                DialogResult result = rageMessageBox.ShowExclamation(this, e.Message, e.Caption, (e.OkOnly ? MessageBoxButtons.OK : MessageBoxButtons.YesNo));
                e.ResultYesOrOK = (result == DialogResult.Yes || result == DialogResult.OK);
            }
        }

        #region Creation
        public static MainWindow Create(INamedPipe pipe, RestartHandler handler, GameConnection connection, ILog log)
        {
            try
            {
                log.Message("MainWindow::Create");
                MainWindow newWindow = new MainWindow(log);
                Application.ThreadException += new ThreadExceptionEventHandler(newWindow.HandleException_EH);
                sm_RestartHandler = handler;
                if (newWindow.ReadApps(pipe, connection))
                {
                    newWindow.Show();
                    return newWindow;
                }
            }
            catch (System.Exception ex)
            {
                log.ToolException(ex, "MainWindow::Create failed");
                throw ex;
            }

            return null;
        }

        private bool ReadApps(INamedPipe pipe, GameConnection connection)
        {
            _log.MessageCtx(LogCtx, "ReadApps");
            bool result = false;

            try
            {
                result = RageApplication.ReadMainAppInfo(pipe, _log, LogCtx);
                if (result)
                {
                    RageApplication mainApp = RageApplication.MainApp;

                    String filePath = Path.Combine(MainWindow.UserPrefDir, ".ragappinfo." + mainApp.ExeName + ".xml");
                    RageApplication.ReadAppInfo(filePath, pipe.GetLocalIP(), _log);

                    pipe.Close();
                    _log.MessageCtx(LogCtx, "Disconnected from pipe '{0}'.", pipe.Name);


                    RageApplication.PacketProcessor = new BankRemotePacketProcessor();
                    WidgetVisualiser.PacketProcessor = RageApplication.PacketProcessor;

                    RageApplication.PacketProcessor.DestroyHandler = new BankRemotePacketProcessor.DestroyDelegate(Widget.DestroyHandler);
                    RageApplication.PacketProcessor.ResetTypes();
                    BankManager.AddHandlers(RageApplication.PacketProcessor);
                    WidgetAngle.AddHandlers(RageApplication.PacketProcessor);
                    WidgetBank.AddHandlers(RageApplication.PacketProcessor);
                    WidgetButton.AddHandlers(RageApplication.PacketProcessor);
                    WidgetColor.AddHandlers(RageApplication.PacketProcessor);
                    WidgetCombo.AddHandlers(RageApplication.PacketProcessor);
                    WidgetData.AddHandlers(RageApplication.PacketProcessor);
                    WidgetGroup.AddHandlers(RageApplication.PacketProcessor);
                    WidgetImageViewer.AddHandlers(RageApplication.PacketProcessor);
                    WidgetList.AddHandlers(RageApplication.PacketProcessor);
                    WidgetMatrix.AddHandlers(RageApplication.PacketProcessor);
                    WidgetSeparator.AddHandlers(RageApplication.PacketProcessor);
                    WidgetSliderFloat.AddHandlers(RageApplication.PacketProcessor);
                    WidgetSliderInt.AddHandlers(RageApplication.PacketProcessor);
                    WidgetText.AddHandlers(RageApplication.PacketProcessor);
                    WidgetTitle.AddHandlers(RageApplication.PacketProcessor);
                    WidgetToggle.AddHandlers(RageApplication.PacketProcessor);
                    WidgetTreeList.AddHandlers(RageApplication.PacketProcessor);
                    WidgetVector.AddHandlers(RageApplication.PacketProcessor);
                    WidgetVCR.AddHandlers(RageApplication.PacketProcessor);

                    m_LoadedApps = result;
                    LoadApps(connection);

                    // start the bank update timer:
                    this.updateTimer.Enabled = true;
                }
            }
            catch (Exception e)
            {
                _log.ToolExceptionCtx(LogCtx, e, "READ APPS Exception");
                //throw e;
            }

            return result;
        }
        #endregion

        #region Delegates
        public delegate void RestartHandler(Exception e, bool showMessage);
        #endregion

        #region Properties
        public static string DefaultUserPrefDir
        {
            get
            {
                return Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "rag");
            }
        }

        public static string UserPrefDir
        {
            get
            {
                string prefDir = Settings.Default.PreferencesDirectory;
                if (!String.IsNullOrEmpty(prefDir) && !Directory.Exists(prefDir))
                {
                    rageMessageBox.ShowWarning(
                        String.Format("Your user preferences path '{0}' doesn't exist. Defaulting to '{1}'. ", prefDir, DefaultUserPrefDir),
                        "User Preferences Directory");

                    prefDir = null;
                    Settings.Default.PreferencesDirectory = null;
                }

                // Check if we need to use the default directory
                if (String.IsNullOrEmpty(prefDir))
                {
                    prefDir = DefaultUserPrefDir;
                }

                // Create a sub directory for the project and ensure the directory exists
                String projectName = Environment.GetEnvironmentVariable("RS_PROJECT");
                if (projectName == null)
                {
                    rageMessageBox.ShowWarning(
                        "The RS_PROJECT environment variable isn't currently set.  Have you run the tools installer?",
                        "RS_PROJECT Environment Variable Missing");
                }
                else
                {
                    prefDir = Path.Combine(prefDir, projectName);
                }

                if (!Directory.Exists(prefDir))
                {
                    Directory.CreateDirectory(prefDir);
                }

                return prefDir;
            }
        }

        public static GlobalPrefs GlobalPreferences
        {
            get
            {
                return sm_GlobalPrefs;
            }
        }

        public static string PluginPath
        {
            set
            {
                sm_PluginPath = value;
            }
        }

        public static bool AdminMode
        {
            get
            {
                return sm_adminMode;
            }
            set
            {
                sm_adminMode = value;
            }
        }

        public static string IpAddress
        {
            set
            {
                sm_ipAddress = value;
            }
        }

        public static bool DumpOutputInRealTime
        {
            set
            {
                sm_DumpOutputInRealTime = value;
            }
        }

        public static bool DumpOutputOnExit
        {
            set
            {
                sm_DumpOutputOnExit = value;
            }
        }

        public static bool ZipOutputFileOnExit
        {
            set
            {
                sm_zipOutputFileOnExit = value;
            }
        }

        public static bool SuppressAssertQuitCrashDialog
        {
            set
            {
                // use this set toggle so that we respect the value of the command line, if it was given
                if (!sm_suppressAssertQuitCrashDetectedDialogSet)
                {
                    sm_suppressAssertQuitCrashDetectedDialog = value;
                    sm_suppressAssertQuitCrashDetectedDialogSet = true;
                }
            }
        }

        public static bool AppendTimestampToOutputFilename
        {
            set
            {
                sm_appendTimestampToOutputFilename = value;
            }
        }

        private string OutputLogPath
        {
            get
            {
                StringBuilder filename = new StringBuilder();
                filename.Append(m_ExeFileName);

                if (!String.IsNullOrEmpty(sm_ipAddress))
                {
                    filename.Append("_ip");
                    filename.Append(sm_ipAddress);
                }

                filename.Append(".txt");

                return Path.Combine(MainWindow.UserPrefDir, filename.ToString());
            }
        }

        private string OutputLogPathWithTimestamp
        {
            get
            {
                StringBuilder filename = new StringBuilder();
                filename.Append(Path.GetFileNameWithoutExtension(this.OutputLogPath));
                filename.Append("_time");
                filename.Append(rageFileUtilities.GenerateTimeBasedFilename());
                filename.Append(".txt");

                return Path.Combine(MainWindow.UserPrefDir, filename.ToString());
            }
        }

        private RichTextBox RichTextBoxThatHasFocus
        {
            get
            {
                // return whichever one has focus
                foreach (KeyValuePair<string, OutputTextBoxInfo> pair in m_outputTextBoxDictionary)
                {
                    if (pair.Value.VisibleTextBox.Focused)
                    {
                        return pair.Value.VisibleTextBox;
                    }
                }

                // it's possible we don't have focus, so use the cursor position
                foreach (KeyValuePair<string, OutputTextBoxInfo> pair in m_outputTextBoxDictionary)
                {
                    if ((pair.Value.VisibleTextBox == this.m_richTextBoxMouseEntered) && pair.Value.VisibleTextBox.Visible)
                    {
                        return pair.Value.VisibleTextBox;
                    }
                }

                return null;
            }
        }

        public SandDockManager SandDockManager
        {
            get
            {
                return this.sandDockManager1;
            }
        }

        internal static Dictionary<string, MultiColorInfo> MultiColorOutputInfo
        {
            get
            {
                if (sm_multiColorOutputDictionary.Count == 0)
                {
                    sm_multiColorOutputDictionary.Add("Output", new MultiColorInfo("multiColorOutput", Color.Empty));
                    sm_multiColorOutputDictionary.Add("Display", new MultiColorInfo("multiColorDisplay", Color.Empty));
                    sm_multiColorOutputDictionary.Add("Warning", new MultiColorInfo("multiColorWarnings", Color.Goldenrod));
                    sm_multiColorOutputDictionary.Add("Error", new MultiColorInfo("multiColorErrors", Color.Red));
                    sm_multiColorOutputDictionary.Add("Script", new MultiColorInfo("multiColorScripts", Color.Chocolate));
                }

                return sm_multiColorOutputDictionary;
            }
        }

        public static List<string> DeferredLoadOutputWindows
        {
            get
            {
                return sm_deferredLoadOutputWindows;
            }
        }

        public bool PauseRichTextBoxAllOutput
        {
            get
            {
                return m_PauseRichTextBoxAllOutput;
            }
        }

        public string CurrentLayoutFilename
        {
            get
            {
                return m_currentLayoutFilename;
            }
            set
            {
                m_currentLayoutFilename = value;

                if (!String.IsNullOrEmpty(m_currentLayoutFilename)
                    && File.Exists(m_currentLayoutFilename))
                {
                    this.saveLayoutToolStripMenuItem.Text = "&Save Layout '" + Path.GetFileName(m_currentLayoutFilename) + "'";
                    this.saveLayoutAsToolStripMenuItem.Text = "Save Layout '" + Path.GetFileName(m_currentLayoutFilename) + "' &As...";

                    this.layoutSaveAsFileDialog.InitialDirectory = Path.GetDirectoryName(m_currentLayoutFilename);
                }
            }

        }
        #endregion

        #region Constants
        private const string c_helpFile = @"chm\overview_rag.chm";
        private const string c_RagSettingsFile = "ragSettings.xml";
        #endregion

        #region Variables
        private static Dictionary<string, Assembly> sm_DllModules = new Dictionary<string, Assembly>();

        private static List<IViewPlugIn> sm_ViewPlugIns = new List<IViewPlugIn>();
        private static Dictionary<string, IViewPlugIn> sm_InitializedViewPlugInsMap = new Dictionary<string, IViewPlugIn>();

        private static List<IWidgetPlugIn> sm_WidgetPlugIns = new List<IWidgetPlugIn>();
        private static Dictionary<string, IWidgetPlugIn> sm_InitializedWidgetPlugInsMap = new Dictionary<string, IWidgetPlugIn>();

        private Dictionary<string, string> m_HelpNameToCommandLine = new Dictionary<string, string>();

        private string m_ExePathName;
        private string m_ExeFileName;
        private string m_defaultLayoutFilename;
        private bool m_LoadedApps;

        private static GlobalPrefs sm_GlobalPrefs;
        private LayoutFileInfos m_layoutFileInfos;
        private static string sm_PluginPath;
        private static bool sm_adminMode = false;
        private static string sm_ipAddress = string.Empty;

        private static RagSettings sm_RagSettings = new RagSettings();

        private static TextWriter sm_dumpWriter;
        private static bool sm_DumpOutputInRealTime = false;
        private static bool sm_DumpOutputOnExit = true;
        private static bool sm_appendTimestampToOutputFilename = true;
        private static bool sm_zipOutputFileOnExit = false;

        private static bool sm_suppressAssertQuitCrashDetectedDialogSet = false;
        private static bool sm_suppressAssertQuitCrashDetectedDialog = false;

        private static Dictionary<string, MultiColorInfo> sm_multiColorOutputDictionary = new Dictionary<string, MultiColorInfo>();
        private static List<string> sm_deferredLoadOutputWindows = new List<string>();

        private bool m_RagExit = false;

        const int MAX_NUM_RECENT_LAYOUTS = 10;
        private string m_currentLayoutFilename;
        private static RestartHandler sm_RestartHandler;
        private bool m_PauseRichTextBoxAllOutput = false;

        private RichTextBoxSearchReplaceDialog m_richTextBoxSearchReplaceDialog = null;

        private RichTextBox m_richTextBoxMouseEntered = null;
        private Dictionary<string, OutputTextBoxInfo> m_outputTextBoxDictionary = new Dictionary<string, OutputTextBoxInfo>();
        private IDictionary<string, ISet<string>> m_subChannelMappings = new Dictionary<string, ISet<string>>();
        private RichTextBox m_cachedTextBox = new RichTextBox();   //an extra text box used to acquire RTF-formatted strings.
        private List<DockControl> m_userCreatedOutputWindows = new List<DockControl>();
        private FeedbackForm m_feedbackForm = null;

        private Size m_lastScreenSize = new Size(0, 0);

        private Point m_location = new Point(0, 0);
        private FormWindowState m_windowState = FormWindowState.Normal;
        private Mutex m_outputTextBoxMutex = new Mutex();
        #endregion

        #region Structs and Classes
        [StructLayout(LayoutKind.Sequential)]
        private struct POINTAPI
        {
            public int x;
            public int y;
        }

        [StructLayout(LayoutKind.Sequential)]
        private struct RECT
        {
            public int left;
            public int top;
            public int right;
            public int bottom;
        }

        [StructLayout(LayoutKind.Sequential)]
        private struct WINDOWPLACEMENT
        {
            public int length;
            public int flags;
            public int showCmd;
            public POINTAPI ptMinPosition;
            public POINTAPI ptMaxPosition;
            public RECT rcNormalPosition;
        }
        #endregion

        #region Event Handlers
        private void MainWindow_Load(object sender, EventArgs e)
        {
            // NOTE: We stored the location and window state of the MainWindow when we loaded
            // the layout file. We wait to apply them until the MainWindow is loaded because
            // SandDock doesn't play nice with multiple monitors, and tends to put it on the
            // primary monitor no matter what.
            this.Location = m_location;
            this.WindowState = m_windowState;

            this.throwExceptionToolStripMenuItem.Enabled = sm_adminMode;

            // create deferred output windows, if any
            foreach (string outputWindow in sm_deferredLoadOutputWindows)
            {
                CreateOutputWindow(outputWindow);
            }

            sm_deferredLoadOutputWindows.Clear();
        }

        private void MainWindow_Shown(object sender, EventArgs e)
        {
            // The Shown event is sent slightly after the Load event so hopefully
            // it'll be fine to create the sensor here.
            Sensor.CreateSensor(RenderWindow.PlatformString, _log);
        }


        private object m_OutputMessageLock = new object();
        void Sensor_OnOutputMessage(object sender, MessageEventArgs e)
        {
            lock (m_OutputMessageLock)
            {
                if (InvokeRequired)
                {
                    Invoke((MethodInvoker)delegate
                    {
                        RageApplication.OutputTextEventArgs ev = new RageApplication.OutputTextEventArgs(new RageApplication.TTYLine(e.Message));

                        BeginAddStringToOutput(this, EventArgs.Empty);
                        AddStringToOutput(RageApplication.MainApp, ev);
                        EndAddStringToOutput(this, EventArgs.Empty);

                        Console.Write(ev.Output.Text);
                    });
                }
                else
                {
                    RageApplication.OutputTextEventArgs ev = new RageApplication.OutputTextEventArgs(new RageApplication.TTYLine(e.Message));

                    BeginAddStringToOutput(this, EventArgs.Empty);
                    AddStringToOutput(RageApplication.MainApp, ev);
                    EndAddStringToOutput(this, EventArgs.Empty);

                    Console.Write(ev.Output.Text);
                }
            }
        }

        private void MainWindow_FormClosing(object sender, FormClosingEventArgs e)
        {
            string outputFilename;
            if (sm_appendTimestampToOutputFilename)
            {
                outputFilename = this.OutputLogPathWithTimestamp;
            }
            else
            {
                outputFilename = this.OutputLogPath;
            }

            bool savedOutputFile = false;

            // disable it so we don't hit it!
            this.updateTimer.Enabled = false;

            RagTrayIcon.Instance.PrepareToQuit();
            RageApplication.PrepareToStopAppsFinal();

            if (e.CloseReason != CloseReason.WindowsShutDown)
            {
                // NOTE: Some of the shutdown steps that are preformed in this function
                // do not play nice with the fact that Windows is shutting down. For
                // example, writing to the OutputRichTextBox will sometimes cause a
                // System.OutOfMemoryException to be thrown. So, since we're shutting
                // down Windows anyway, let's just skip it :)

                // if this is set, we need to close the file.
                if (sm_DumpOutputInRealTime && (sm_dumpWriter != null))
                {
                    sm_dumpWriter.Close();
                    sm_dumpWriter = null;

                    // change the filename to give it a timestamp
                    if (sm_appendTimestampToOutputFilename)
                    {
                        rageStatus obStatus;
                        rageFileUtilities.MoveFile(this.OutputLogPath, this.OutputLogPathWithTimestamp, out obStatus);
                        if (!obStatus.Success())
                        {
                            _log.ErrorCtx(LogCtx, "Error saving log file: " + obStatus.GetErrorString());
                        }
                    }
                }

                // if this is set, we always dump the output.
                if (sm_DumpOutputOnExit)
                {
                    SaveOutputLog(outputFilename, sm_zipOutputFileOnExit, false);

                    //this will prevent us from dumping 2x.
                    sm_DumpOutputOnExit = false;
                    savedOutputFile = true;
                }
            }

            Sensor.EmptyOutputMessageHandler();

            if (Sensor.CurrentSensor != null)
            {
                Sensor.CurrentSensor.Quit();
                Sensor.CurrentSensor.Dispose();
                // Don't null out the sensor just yet.  We need to check some of its status properties.
            }

            if (e.CloseReason != CloseReason.WindowsShutDown)
            {
                bool saveSettings = m_LoadedApps && !String.IsNullOrEmpty(m_ExePathName);

                if (saveSettings)
                {
                    // save the last loaded layout and recent layout filenames
                    string lowercaseAppName = Path.Combine(m_ExePathName, m_ExeFileName).ToLower();

                    LayoutFileInfo info;
                    if (!m_layoutFileInfos.LayoutInfos.TryGetValue(lowercaseAppName, out info))
                    {
                        info = new LayoutFileInfo(lowercaseAppName);
                        m_layoutFileInfos.LayoutInfos.Add(lowercaseAppName, info);
                    }

                    info.LastLayoutFilename = m_currentLayoutFilename;

                    info.RecentLayoutFilenames.Clear();
                    foreach (ToolStripMenuItem item in this.recentLayoutsToolStripMenuItem.DropDownItems)
                    {
                        info.RecentLayoutFilenames.Add(item.Text);
                    }

                    Settings.Default.LayoutFiles = m_layoutFileInfos.Serialize();

                    // RichTextBoxFindReplace
                    rageRichTextBoxSearchReplaceSettings rtbSettings = new rageRichTextBoxSearchReplaceSettings();
                    m_richTextBoxSearchReplaceDialog.SetSettingsFromUI(rtbSettings);
                    Settings.Default.RichTextBoxFindReplace = rtbSettings.Serialize();
                }

                // now save it
                int retryCount = 10;
                do
                {
                    try
                    {
                        Settings.Default.Save();
                        break;
                    }
                    catch
                    {
                        --retryCount;
                        Thread.Sleep(100);
                    }
                }
                while (retryCount > 0);

                RageApplication.SerializeBanks();

                if (saveSettings)
                {
                    // save the layout file itself
                    SaveLayout(m_defaultLayoutFilename, m_ExePathName);
                }
            }

            // stop the applications:
            _log.MessageCtx(LogCtx, "MainWindow_FormClosing calling StopAppsFinal");
            RageApplication.StopAppsFinal(_log);

            if (e.CloseReason != CloseReason.WindowsShutDown)
            {
                // Search the last 1000 characters to see if a crash or assert occured
                int len = this.OutputRichTextBox.TextLength;
                int start = Math.Max(len - 1000, 0);

                bool bAssertDetected = (this.OutputRichTextBox.Find("Fatal Error", start, RichTextBoxFinds.Reverse) != -1)
                    || ((Sensor.CurrentSensor != null) && (Sensor.CurrentSensor.Platform == "Xbox360")
                    && (this.OutputRichTextBox.Find("Error: Assert(", start, RichTextBoxFinds.Reverse) != -1));

                bool bCrashDetected = this.OutputRichTextBox.Find("ExceptMain: Abnormal exit.", start, RichTextBoxFinds.Reverse) != -1;

                if (bAssertDetected || bCrashDetected)
                {
                    if (!sm_suppressAssertQuitCrashDetectedDialog)
                    {
                        if (bAssertDetected)
                        {
                            rageMessageBox.ShowError(this, "Assert or Quitf() detected:\n\n" +
                                                   "Log will be saved as '" + outputFilename + "'.", "Assert or Quitf() Detected!");
                        }
                        else
                        {
                            rageMessageBox.ShowError(this, "Crash detected.  Log will be saved as '" + outputFilename + "'.", "Crash Detected!");
                        }
                    }

                    if (!savedOutputFile)
                    {
                        SaveOutputLog(outputFilename, sm_zipOutputFileOnExit, false);
                    }
                }
            }

            // Now it's ok to nullify the sensor
            Sensor.CurrentSensor = null;

            m_LoadedApps = false;


            if (e.CloseReason != CloseReason.WindowsShutDown)
            {
                string globalPrefsFilename = Path.Combine(MainWindow.UserPrefDir, ".ragpreferences." + Environment.UserName + ".xml");
                SaveGlobalPrefs(globalPrefsFilename);
            }


            // clear all widget views:
            DockableViewManager.Instance.CloseAllViews();
            CloseAllUserCreatedOutputWindows();

            ShutdownPlugIns();
            ClearPlugIns();

            ClearAllOutputWindows();

            RagTrayIcon.Instance.AppExiting = true;

            // exit everything:
            RagTrayIcon.Instance.Close();
            if (!m_RagExit && (e.CloseReason != CloseReason.WindowsShutDown))
            {
                RagTrayIcon.Instance.Restart();
            }
        }

        private void BankMgr_InitFinished()
        {
            List<OutputTextBoxInfo> remove = new List<OutputTextBoxInfo>();

            foreach (KeyValuePair<string, OutputTextBoxInfo> pair in m_outputTextBoxDictionary)
            {
                if (!pair.Value.Window.PersistState)
                {
                    remove.Add(pair.Value);
                }
            }

            foreach (OutputTextBoxInfo otbInfo in remove)
            {
                otbInfo.Window.CloseAction = DockControlCloseAction.Dispose;
                otbInfo.Window.Close();
            }
        }

        private void openLayoutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string layoutFile = ShowOpenDialog(Path.GetDirectoryName(this.CurrentLayoutFilename));
            if (!String.IsNullOrEmpty(layoutFile))
            {
                ReloadALayout(layoutFile);
            }
        }

        private void saveLayoutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (m_currentLayoutFilename == null)
            {
                saveLayoutAsToolStripMenuItem_Click(sender, e);
            }
            else
            {
                SaveLayout(m_currentLayoutFilename, m_ExePathName);
            }
        }

        private void saveLayoutAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //this.layoutSaveAsFileDialog.FileName = m_currentLayoutFilename;
            DialogResult dialogResult = this.layoutSaveAsFileDialog.ShowDialog(this);
            if (dialogResult == DialogResult.OK)
            {
                if (SaveLayout(this.layoutSaveAsFileDialog.FileName, m_ExePathName))
                {
                    if (this.CurrentLayoutFilename != null)
                    {
                        // remember the one we had loaded
                        AddToRecentLayoutList(this.CurrentLayoutFilename);
                    }

                    this.CurrentLayoutFilename = this.layoutSaveAsFileDialog.FileName.ToLower();
                }
            }
        }

        private void userPreferencesDirectoryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.folderBrowserDialog.Description = "Select User Preferences Directory";

            if (Directory.Exists(MainWindow.UserPrefDir))
            {
                this.folderBrowserDialog.SelectedPath = MainWindow.UserPrefDir;
            }

            DialogResult result = this.folderBrowserDialog.ShowDialog(this);
            if (result == DialogResult.OK)
            {
                bool bReinitDumpWriter = false;

                if (MainWindow.UserPrefDir.ToUpper() != this.folderBrowserDialog.SelectedPath.ToUpper())
                {
                    result = rageMessageBox.ShowQuestion(this, "Would you like to move your current preferences files to the new location?",
                        "User Preferences Directory Changed");
                    if (result == DialogResult.Yes)
                    {
                        string[] files = Directory.GetFiles(MainWindow.UserPrefDir, "*" + Environment.UserName + "*.xml");

                        List<string> filenames = new List<string>();
                        foreach (string file in files)
                        {
                            filenames.Add(file);

                            string filename = Path.GetFileNameWithoutExtension(file);
                            if (filename.ToLower().StartsWith(".raglayout"))
                            {
                                string[] split = filename.Split(new char[] { '.' }, StringSplitOptions.RemoveEmptyEntries);
                                if (split.Length >= 2)
                                {
                                    string[] outputFiles = Directory.GetFiles(MainWindow.UserPrefDir, split[1] + "*");
                                    foreach (string outputFile in outputFiles)
                                    {
                                        if (!filenames.Contains(outputFile))
                                        {
                                            if ((sm_dumpWriter != null) && (outputFile.ToUpper() == this.OutputLogPath.ToUpper()))
                                            {
                                                sm_dumpWriter.Close();
                                                sm_dumpWriter = null;
                                                bReinitDumpWriter = true;
                                            }

                                            filenames.Add(outputFile);
                                        }
                                    }
                                }
                            }
                        }

                        for (int i = 0; i < filenames.Count; )
                        {
                            rageStatus obStatus;
                            rageFileUtilities.MoveLocalFile(filenames[i],
                                Path.Combine(this.folderBrowserDialog.SelectedPath, Path.GetFileName(filenames[i])), out obStatus);
                            if (!obStatus.Success())
                            {
                                result = rageMessageBox.ShowError(this, obStatus.ErrorString, "Move File Error", MessageBoxButtons.AbortRetryIgnore);
                                if (result == DialogResult.Abort)
                                {
                                    break;
                                }
                                else if (result == DialogResult.Ignore)
                                {
                                    ++i;
                                }
                            }
                            else
                            {
                                ++i;
                            }
                        }
                    }

                    // change the current filename's path if it used to live in the old dir
                    if (!String.IsNullOrEmpty(this.CurrentLayoutFilename))
                    {
                        string currLayoutFilename = this.CurrentLayoutFilename.ToUpper();
                        if (currLayoutFilename.StartsWith(Settings.Default.PreferencesDirectory.ToUpper()))
                        {
                            currLayoutFilename = this.CurrentLayoutFilename.Substring(Settings.Default.PreferencesDirectory.Length);
                            currLayoutFilename = this.folderBrowserDialog.SelectedPath + currLayoutFilename;
                            this.CurrentLayoutFilename = currLayoutFilename;
                        }
                    }
                }

                string prevUserPrefDir = UserPrefDir;
                Settings.Default.PreferencesDirectory = this.folderBrowserDialog.SelectedPath;
                this.userPreferencesDirectoryToolStripMenuItem.Text = "&User Preferences Directory: " + Settings.Default.PreferencesDirectory;

                if (bReinitDumpWriter)
                {
                    sm_dumpWriter = new StreamWriter(this.OutputLogPath, true);
                }

                DockableViewManager.Instance.OnUserPrefDirChanged(prevUserPrefDir, UserPrefDir);
            }
        }

        private void dumpOutputInRealTimeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem menuItem = sender as ToolStripMenuItem;

            sm_DumpOutputInRealTime = menuItem.Checked;
            sm_GlobalPrefs.SetUiDataItem("DumpOutputInRealTime", sm_DumpOutputInRealTime.ToString());

            if (sm_DumpOutputInRealTime)
            {
                sm_dumpWriter = new StreamWriter(this.OutputLogPath);

                this.dumpOutputOnExitToolStripMenuItem.Checked = false;
                dumpOutputOnExitToolStripMenuItem_Click(this.dumpOutputOnExitToolStripMenuItem, EventArgs.Empty);
            }
            else
            {
                if (sm_dumpWriter != null)
                {
                    sm_dumpWriter.Close();
                    sm_dumpWriter = null;
                }
            }
        }

        private void dumpOutputOnExitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem menuItem = sender as ToolStripMenuItem;

            sm_DumpOutputOnExit = menuItem.Checked;
            sm_GlobalPrefs.SetUiDataItem("DumpOutputOnExit", sm_DumpOutputOnExit.ToString());

            if (sm_DumpOutputOnExit)
            {
                this.dumpOutputInRealTimeToolStripMenuItem.Checked = false;
                dumpOutputInRealTimeToolStripMenuItem_Click(this.dumpOutputInRealTimeToolStripMenuItem, EventArgs.Empty);
            }
        }

        private void zipOutputOnExitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem menuItem = sender as ToolStripMenuItem;

            sm_zipOutputFileOnExit = menuItem.Checked;
            sm_GlobalPrefs.SetUiDataItem("ZipOutputFileOnExit", sm_zipOutputFileOnExit.ToString());
        }

        private void appendTimestampToOutputFileNameToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem menuItem = sender as ToolStripMenuItem;

            sm_appendTimestampToOutputFilename = menuItem.Checked;
            sm_GlobalPrefs.SetUiDataItem("AppendTimestampToOutputFilename", sm_appendTimestampToOutputFilename.ToString());
        }

        private void suppressAssertCrashQuitDialogOnExitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem menuItem = sender as ToolStripMenuItem;

            sm_suppressAssertQuitCrashDetectedDialog = menuItem.Checked;
            sm_GlobalPrefs.SetUiDataItem("SuppressAssertQuitCrashDialog", sm_suppressAssertQuitCrashDetectedDialog.ToString());
        }

        private void exitApplicationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void exitRAGToolStripMenuItem_Click(object sender, EventArgs e)
        {
            m_RagExit = true;
            this.Close();
            // RagTrayIcon.Instance.Close(); redundant with MainWindow_FormClosing does
            // Application.Exit(); redundant with RagTrayIcon.Instance.Close()
        }

        private void disabledWidgetsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DisabledWidgetsForm form = new DisabledWidgetsForm(WidgetVisualiser.PacketProcessor.DisabledWidgets);
            form.ShowDialog(this);
        }

        private void pluginsToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Plugins_Activate();
        }

        private void helpToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            string helpFile = Path.Combine(Path.GetDirectoryName(Application.ExecutablePath), c_helpFile);
            if (File.Exists(helpFile))
            {
                Process.Start(helpFile);
            }
        }

        private void commandLineToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CommandLineForm cmdForm = new CommandLineForm();
            cmdForm.ShowDialog(this);
        }

        private void throwExceptionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            HandleException(new Exception("Test Exception"));
        }

        private void submitFeedbackToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (m_feedbackForm == null)
            {
                m_feedbackForm = new FeedbackForm("Rag", Settings.Default.FeedbackFromEmailAddress, Settings.Default.FeedbackToEmailAddress,
                    this.Icon);
                m_feedbackForm.FromEmailAddressChanged += new FeedbackForm.FromEmailAddressChangedEventHandler(feedbackForm_FromEmailAddressChanged);

                Rectangle rect = this.Bounds;
                m_feedbackForm.StartPosition = FormStartPosition.Manual;

                int x = (rect.Width / 2) + rect.Left - (m_feedbackForm.Width / 2);
                int y = (rect.Height / 2) + rect.Top - (m_feedbackForm.Height / 2);

                m_feedbackForm.Location = new Point(x, y);
            }

            if (m_feedbackForm.Visible)
            {
                if (m_feedbackForm.WindowState == FormWindowState.Minimized)
                {
                    m_feedbackForm.WindowState = FormWindowState.Normal;
                }

                m_feedbackForm.Focus();
            }
            else
            {
                m_feedbackForm.Show();
            }
        }

        private void feedbackForm_FromEmailAddressChanged(object sender, FromEmailAddressChangedEventArgs e)
        {
            Settings.Default.FeedbackFromEmailAddress = e.FromEmailAddress;
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            rageMessageBox.ShowInformation(this, "Please direct questions, comments, and problems to the 'RSGSAN RAGE Tools' mailing list.",
                "About...");
        }

        private void documentContainer1_SizeChanged(object sender, EventArgs e)
        {
            //documentContainer1.Width=640;
            //documentContainer1.Height=480;
        }

        private void ActivateHelpMenuOption(object sender, EventArgs e)
        {
            try
            {
                ToolStripMenuItem item = sender as ToolStripMenuItem;
                if (item != null)
                {
                    if (m_HelpNameToCommandLine.ContainsKey(item.Text))
                    {
                        Process.Start(m_HelpNameToCommandLine[item.Text]);
                    }
                }
            }
            catch (System.Exception)
            {

            }
        }

        private static bool sm_TimerIsRecursing = false;

        private void updateTimer_Tick(object sender, EventArgs e)
        {
            if (sm_TimerIsRecursing)
            {
                return;
            }

            // update all pipes...
            if (m_LoadedApps)
            {
                try
                {
                    sm_TimerIsRecursing = true;

                    RageApplication.ProcessPipes();

                    if (RageApplication.MainApp.PipesDisconnected)
                    {
                        this.OutputRichTextBox.AppendText(Environment.NewLine + "Application Disconnected." + Environment.NewLine);

                        Invoke(new MethodInvoker(AppExit));
                    }
                }
                catch (Exception except)
                {
                    HandleException(except);
                }
                finally
                {
                    sm_TimerIsRecursing = false;
                }
            }
        }

        private void RecentLayoutSelect(object sender, System.EventArgs e)
        {
            ToolStripMenuItem item = sender as ToolStripMenuItem;

            if (!ReloadALayout(item.Text))
            {
                RemoveFromRecentLayoutList(item.Text);
            }
        }

        private void statusStrip1_Click(object sender, EventArgs e)
        {

        }

        private void sandDockManager1_ShowControlContextMenu(object sender, ShowControlContextMenuEventArgs e)
        {
            if ((e.Context == ContextMenuContext.OptionsButton)
                || (e.DockControl.ShowOptions && (e.DockControl.DockSituation == DockSituation.Floating) && (e.Context == ContextMenuContext.RightClick) && (e.DockControl is DockableWindow)))
            {
                this.disposeToolStripMenuItem.Tag = e.DockControl;
                this.hideToolStripMenuItem.Tag = e.DockControl;

                this.disposeToolStripMenuItem.Checked = e.DockControl.CloseAction == DockControlCloseAction.Dispose;
                this.hideToolStripMenuItem.Checked = e.DockControl.CloseAction == DockControlCloseAction.HideOnly;

                this.dockControlContextMenuStrip.Show(e.DockControl.PointToScreen(e.Position));
            }
            else if ((e.Context == ContextMenuContext.RightClick) && (e.DockControl is TabbedDocument))
            {
                RenderWindow window = RenderWindow.DockControlToRenderWindow(e.DockControl);
                if (window != null)
                {
                    window.PopulateContextMenu(this.tabDocContextMenuStrip);

                    this.tabDocContextMenuStrip.Show(this, this.PointToClient(Control.MousePosition));
                }
            }
        }

        private void sandDockManager1_ResolveDockControl(object sender, ResolveDockControlEventArgs e)
        {
            foreach (IViewPlugIn viewPlugin in sm_ViewPlugIns)
            {
                if (viewPlugin is IDockableViewManager)
                {
                    IDockableViewManager viewMgr = viewPlugin as IDockableViewManager;
                    DockControl dockControl = viewMgr.ResolveDockControl(e.Guid.ToString(), LogFactory.ApplicationLog);
                    if (dockControl != null)
                    {
                        e.DockControl = dockControl;
                        break;
                    }
                }
            }
        }

        private void addWidgetViewToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (RageApplication.Applications.Length > 0)
            {
                RageApplication rageApp = RageApplication.Applications[0]; // We only support a single running app
                WidgetViewBase view = rageApp.AddWidgetView(rageApp.ExeName, new WidgetViewBase.CreateDel(BankTreeView.CreateBankTreeView), false);
                view.DockControl.Activate();
            }
        }

        private void addFindViewToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (RageApplication.Applications.Length > 0)
            {
                RageApplication rageApp = RageApplication.Applications[0]; // We only support a single running app
                WidgetViewBase view = rageApp.AddWidgetView(rageApp.ExeName, new WidgetViewBase.CreateDel(FindView.CreateFindView), false);
                view.DockControl.Activate();
            }
        }

        private void addFavoritesViewToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (RageApplication.Applications.Length > 0)
            {
                RageApplication rageApp = RageApplication.Applications[0]; // We only support a single running app
                WidgetViewBase view = rageApp.AddWidgetView(rageApp.ExeName, new WidgetViewBase.CreateDel(FavoritesView.CreateFavoritesView), false);
                view.DockControl.Activate();
            }
        }


        private void enableColouredModificationsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ragCore.ViewSettings.ShowModifiedColours = !ragCore.ViewSettings.ShowModifiedColours;
            Settings.Default.ShowColouredModifications = ragCore.ViewSettings.ShowModifiedColours;
        }

        private void windowsToolStripMenuItem_DropDownOpening(object sender, EventArgs e)
        {
            this.windowsToolStripMenuItem.DropDownItems.Clear();

            // Output window menu
            ToolStripMenuItem menuItem = MainWindow.CreateMenuItemForDockControl(this.outputDockableWindow);
            menuItem.Image = global::rag.Properties.Resources.info;
            if (menuItem.DropDownItems.Count > 0)
            {
                ToolStripMenuItem subItem = menuItem.DropDownItems[0] as ToolStripMenuItem;
                subItem.ShortcutKeys = ((Keys)((Keys.Control | Keys.O)));
            }
            this.windowsToolStripMenuItem.DropDownItems.Add(menuItem);

            // Display window menu
            menuItem = MainWindow.CreateMenuItemForDockControl(this.displayDockableWindow);
            menuItem.Image = global::rag.Properties.Resources.display;
            if (menuItem.DropDownItems.Count > 0)
            {
                ToolStripMenuItem subItem = menuItem.DropDownItems[0] as ToolStripMenuItem;
                subItem.ShortcutKeys = ((Keys)((Keys.Control | Keys.D)));
            }
            this.windowsToolStripMenuItem.DropDownItems.Add(menuItem);

            // Errors window menu
            menuItem = MainWindow.CreateMenuItemForDockControl(this.errorsDockableWindow);
            menuItem.Image = global::rag.Properties.Resources.error;
            if (menuItem.DropDownItems.Count > 0)
            {
                ToolStripMenuItem subItem = menuItem.DropDownItems[0] as ToolStripMenuItem;
                subItem.ShortcutKeys = ((Keys)((Keys.Control | Keys.E)));
            }
            this.windowsToolStripMenuItem.DropDownItems.Add(menuItem);

            // Warnings window menu
            menuItem = MainWindow.CreateMenuItemForDockControl(this.warningsDockableWindow);
            menuItem.Image = global::rag.Properties.Resources.warning;
            if (menuItem.DropDownItems.Count > 0)
            {
                ToolStripMenuItem subItem = menuItem.DropDownItems[0] as ToolStripMenuItem;
                subItem.ShortcutKeys = ((Keys)((Keys.Control | Keys.W)));
            }
            this.windowsToolStripMenuItem.DropDownItems.Add(menuItem);

            // Script window menu
            menuItem = MainWindow.CreateMenuItemForDockControl(this.scriptsDockableWindow);
            menuItem.Image = global::rag.Properties.Resources.Scripts;
            this.windowsToolStripMenuItem.DropDownItems.Add(menuItem);

            // Menus for each custom window
            foreach (DockControl dockControl in m_userCreatedOutputWindows)
            {
                menuItem = MainWindow.CreateMenuItemForDockControl(dockControl);
                this.windowsToolStripMenuItem.DropDownItems.Add(menuItem);
            }

            if (DockControlManager.Instance.RenderWindowDockControls.Count > 0)
            {
                this.windowsToolStripMenuItem.DropDownItems.Add(new ToolStripSeparator());
            }

            // Menus for each render window dock control
            foreach (DockControl dockControl in DockControlManager.Instance.RenderWindowDockControls)
            {
                menuItem = RenderWindow.CreateMenuItemForDockControl(dockControl);
                this.windowsToolStripMenuItem.DropDownItems.Add(menuItem);
            }

            if (DockableViewManager.Instance.DockControls.Count > 0)
            {
                this.windowsToolStripMenuItem.DropDownItems.Add(new ToolStripSeparator());
            }

            // Menus for each dock control
            int windowIndex = 1;
            foreach (DockControl dockControl in DockableViewManager.Instance.DockControls)
            {
                menuItem = MainWindow.CreateMenuItemForDockControl(dockControl);
                if (windowIndex < 10)
                {
                    menuItem.Text = "&" + (windowIndex++) + ". " + menuItem.Text;
                }
                this.windowsToolStripMenuItem.DropDownItems.Add(menuItem);
            }
        }

        private void addSharedFavoritesViewMenuItem_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem menuItem = (ToolStripMenuItem)sender;
            string favoritesFile = (string)menuItem.Tag;

            if (File.Exists(favoritesFile))
            {
                if (RageApplication.Applications.Length > 0)
                {
                    RageApplication rageApp = RageApplication.Applications[0]; // We only support a single running app
                    WidgetViewBase view = rageApp.AddWidgetView(rageApp.ExeName, new WidgetViewBase.CreateDel(FavoritesView.CreateFavoritesView), false);

                    FavoritesView favoritesView = (FavoritesView)view;
                    favoritesView.OpenFavoritesFile(favoritesFile);
                    favoritesView.DockControl.Activate();
                }
            }
            else
            {
                rageMessageBox.ShowError(this, "Could not find favorites file '" + favoritesFile + "'.", "Problem loading favorites file!");
            }
        }

        private void openSharedLayoutMenuItem_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem menuItem = (ToolStripMenuItem)sender;
            string layoutFile = (string)menuItem.Tag;

            if (File.Exists(layoutFile))
            {
                try
                {
                    // Make a local copy, since we don't allow editing shared layouts
                    string copyFile = MainWindow.UserPrefDir + "\\" + Path.GetFileName(layoutFile);
                    File.Copy(layoutFile, copyFile, true);

                    ReloadALayout(copyFile);
                }
                catch (System.Exception ex)
                {
                    rageMessageBox.ShowError(this,
                        "Failed to create local copy of the shared layout file: " + ex.Message,
                        "Load Layout File");
                }
            }
            else
            {
                rageMessageBox.ShowError(this, "Could not find layout file '" + layoutFile + "'.", "Problem loading layout file!");
            }
        }
        #endregion

        #region Rich Text Boxes Context Menu Event Handlers
        private void richTextBoxes_MouseEnter(object sender, EventArgs e)
        {
            m_richTextBoxMouseEntered = sender as RichTextBox;

            if (m_richTextBoxMouseEntered != null)
            {
                OutputTextBoxInfo otbInfo;
                if (m_outputTextBoxDictionary.TryGetValue(m_richTextBoxMouseEntered.Name, out otbInfo))
                {
                    m_richTextBoxSearchReplaceDialog.Text = String.Format("Find in {0}", otbInfo.ChannelName);
                }
            }
        }

        private void richTextBoxesContextMenuStrip_Opening(object sender, CancelEventArgs e)
        {
            System.Windows.Forms.RichTextBox textBox = this.RichTextBoxThatHasFocus;
            if (textBox != null)
            {
                OutputTextBoxInfo otbInfo;
                if (m_outputTextBoxDictionary.TryGetValue(textBox.Name, out otbInfo))
                {
                    this.multiColorToolStripMenuItem.Checked = otbInfo.ColorInfo.MultiColor;
                }
            }
        }

        private void pauseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            m_PauseRichTextBoxAllOutput = !m_PauseRichTextBoxAllOutput;
            this.pauseToolStripMenuItem.Text = m_PauseRichTextBoxAllOutput ? "&Continue" : "&Pause";

            BeginAddStringToOutput(this, EventArgs.Empty);

            foreach (OutputTextBoxInfo info in m_outputTextBoxDictionary.Values)
            {
                info.IsPaused = m_PauseRichTextBoxAllOutput;
            }

            EndAddStringToOutput(this, EventArgs.Empty);
        }

        private void copyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.RichTextBox textBox = this.RichTextBoxThatHasFocus;
            if (textBox != null)
            {
                textBox.Copy();
            }
        }

        private void clearAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.RichTextBox textBox = this.RichTextBoxThatHasFocus;
            if (textBox != null)
            {
                textBox.Text = "";
            }
        }

        private void selectAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.RichTextBox textBox = this.RichTextBoxThatHasFocus;
            if (textBox != null)
            {
                textBox.SelectAll();
            }
        }

        private void multiColorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.RichTextBox textBox = this.RichTextBoxThatHasFocus;
            if (textBox != null)
            {
                OutputTextBoxInfo otbInfo;
                if (m_outputTextBoxDictionary.TryGetValue(textBox.Name, out otbInfo))
                {
                    otbInfo.ColorInfo.MultiColor = this.multiColorToolStripMenuItem.Checked;
                    if (otbInfo.ColorInfo.MultiColor)
                    {
                        textBox.ForeColor = otbInfo.ColorInfo.DefaultColor == Color.Empty ? SystemColors.WindowText : otbInfo.ColorInfo.DefaultColor;
                    }
                    else
                    {
                        textBox.ForeColor = SystemColors.WindowText;
                    }
                }
            }
        }

        private void emailOutputToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.RichTextBox textBox = this.RichTextBoxThatHasFocus;
            if (textBox != null)
            {
                string outputFilename = this.OutputLogPathWithTimestamp;
                try
                {
                    textBox.SaveFile(outputFilename, RichTextBoxStreamType.PlainText);
                }
                catch (Exception ex)
                {
                    rageMessageBox.ShowExclamation(ex.ToString(), "Unable to save output file");

                    if (File.Exists(outputFilename))
                    {
                        File.Delete(outputFilename);
                    }

                    return;
                }

                FeedbackForm dlg = new FeedbackForm("Rag", Settings.Default.EmailOutputFromEmailAddress,
                    Settings.Default.EmailOutputToEmailAddress, this.Icon);

                List<string> attachments = new List<string>();
                attachments.Add(outputFilename);

                dlg.Attachments = attachments;
                dlg.CanEditAttachments = false;
                dlg.CanEditCategory = false;
                dlg.CanEditToEmailAddress = true;
                dlg.Text = "Email Output File";
                dlg.StartPosition = FormStartPosition.CenterParent;

                DialogResult result = dlg.ShowDialog(this);
                if (result == DialogResult.Cancel)
                {
                    try
                    {
                        File.Delete(outputFilename);
                    }
                    catch (Exception ex)
                    {
                        _log.ToolExceptionCtx(LogCtx, ex, "Couldn't delete output filename '{0}'.", outputFilename);
                    }
                }

                Settings.Default.EmailOutputFromEmailAddress = dlg.FromEmailAddress;
                Settings.Default.EmailOutputToEmailAddress = dlg.ToEmailAddress;
            }
        }

        private void findToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!m_richTextBoxSearchReplaceDialog.LocationSet)
            {
                Rectangle rect = this.Bounds;
                m_richTextBoxSearchReplaceDialog.StartPosition = FormStartPosition.Manual;

                int x = (rect.Width / 2) + rect.Left - (m_richTextBoxSearchReplaceDialog.Width / 2);
                int y = (rect.Height / 2) + rect.Top - (m_richTextBoxSearchReplaceDialog.Height / 2);

                m_richTextBoxSearchReplaceDialog.Location = new Point(x, y);

                m_richTextBoxSearchReplaceDialog.LocationSet = true;
            }

            if (!m_richTextBoxSearchReplaceDialog.Visible)
            {
                m_richTextBoxSearchReplaceDialog.Show(this);
            }

            m_richTextBoxSearchReplaceDialog.Focus();

            m_richTextBoxSearchReplaceDialog.InitSearchText(m_richTextBoxMouseEntered);
        }

        private void searchAgainDownToolStripMenuItem(object sender, EventArgs e)
        {
            if (m_richTextBoxMouseEntered != null)
            {
                m_richTextBoxSearchReplaceDialog.SearchAgainDown(m_richTextBoxMouseEntered);
            }
        }

        private void searchAgainUpToolStripMenuItem(object sender, EventArgs e)
        {
            if (m_richTextBoxMouseEntered != null)
            {
                m_richTextBoxSearchReplaceDialog.SearchAgainUp(m_richTextBoxMouseEntered);
            }
        }

        private void searchHighlightedDownToolStripMenuItem(object sender, EventArgs e)
        {
            if (m_richTextBoxMouseEntered != null)
            {
                m_richTextBoxSearchReplaceDialog.SearchHighlightedDown(m_richTextBoxMouseEntered);
            }
        }

        private void searchHighlightedUpToolStripMenuItem(object sender, EventArgs e)
        {
            if (m_richTextBoxMouseEntered != null)
            {
                m_richTextBoxSearchReplaceDialog.SearchHighlightedUp(m_richTextBoxMouseEntered);
            }
        }

        private void richTextBoxSearchReplaceDialog_FindCurrentRichTextBox(object sender, rageSearchReplaceRichTextBoxEventArgs e)
        {
            e.RichTextBox = m_richTextBoxMouseEntered;
        }

        private void richTextBoxSearchReplaceDialog_SearchBegin(object sender, rageSearchReplaceEventArgs e)
        {
            // grab the mutex so we don't try to add text while we are searching it.
            if (m_richTextBoxMouseEntered != null)
            {
                m_outputTextBoxMutex.WaitOne();
            }
        }

        private void richTextBoxSearchReplaceDialog_SearchComplete(object sender, rageSearchCompleteEventArgs e)
        {
            // release the mutex so we can continue adding text.
            if (m_richTextBoxMouseEntered != null)
            {
                m_outputTextBoxMutex.ReleaseMutex();
            }
        }

        #endregion

        #region Save & Load Layouts and Preferences
        private void Deserialize(XmlTextReader xmlReader)
        {
            // main window position:

            int left, top, width, height;

            left = Convert.ToInt32(xmlReader["left"]);
            top = Convert.ToInt32(xmlReader["top"]);
            width = Convert.ToInt32(xmlReader["width"]);
            height = Convert.ToInt32(xmlReader["height"]);

            string strScreenWidth = xmlReader["screenWidth"];
            string strScreenHeight = xmlReader["screenHeight"];
            if (!String.IsNullOrEmpty(strScreenWidth) && !String.IsNullOrEmpty(strScreenHeight))
            {
                m_lastScreenSize = new Size(Convert.ToInt32(strScreenWidth), Convert.ToInt32(strScreenHeight));
            }
            else
            {
                m_lastScreenSize = new Size(0, 0);
            }

            Point loc = new Point(left, top);
            Size size = new Size(width, height);
            rageImage.AdjustForResolution(m_lastScreenSize, rageImage.GetScreenBounds(),
                ref loc, ref size);

            m_location = loc;
            this.Size = size;

            if (xmlReader["windowstate"] == FormWindowState.Maximized.ToString())
            {
                m_windowState = FormWindowState.Maximized;
            }
            else if (xmlReader["windowstate"] == FormWindowState.Minimized.ToString())
            {
                m_windowState = FormWindowState.Minimized;
            }
            else if (xmlReader["windowstate"] == FormWindowState.Normal.ToString())
            {
                m_windowState = FormWindowState.Normal;
            }

            // old format
            Dictionary<string, MultiColorInfo> mcInfoDict = MainWindow.MultiColorOutputInfo;
            foreach (KeyValuePair<string, MultiColorInfo> pair in mcInfoDict)
            {
                if (!pair.Value.SetByCommandLine)
                {
                    string multiColor = xmlReader[pair.Value.CommandLine];
                    if (multiColor != null)
                    {
                        MultiColorInfo mcInfo = mcInfoDict[pair.Key];
                        mcInfo.MultiColor = Boolean.Parse(multiColor);
                    }
                }
            }

            // new format
            string parentName = xmlReader.Name;
            while (xmlReader.Read())
            {
                if (xmlReader.NodeType == XmlNodeType.Element && xmlReader.Name == "MultiColorOutput" && xmlReader.IsEmptyElement == false)
                {
                    while (xmlReader.NodeType != XmlNodeType.EndElement || xmlReader.Name != "MultiColorOutput")
                    {
                        xmlReader.Read();

                        if (xmlReader.Name == "Item")
                        {
                            string name = xmlReader["name"];
                            if (name != null)
                            {
                                MultiColorInfo mcInfo;
                                if (!MainWindow.MultiColorOutputInfo.TryGetValue(name, out mcInfo))
                                {
                                    string strColor = xmlReader["defaultColor"];
                                    string strGuid = xmlReader["guid"];
                                    string strCloseAction = xmlReader["CloseAction"];

                                    if ((strColor != null) && (strGuid != null))
                                    {
                                        CreateOutputWindow(name, strColor, new Guid(strGuid), strCloseAction);

                                        RageApplication.TTYLine.ChannelColors[name] = Widget.DeserializeColor(strColor);
                                    }

                                    mcInfo = MainWindow.MultiColorOutputInfo[name];
                                }

                                if ((mcInfo != null) && !mcInfo.SetByCommandLine)
                                {
                                    string multiColor = xmlReader["multiColor"];
                                    if (multiColor != null)
                                    {
                                        mcInfo.MultiColor = bool.Parse(multiColor);
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    break;
                }
            }

            // now set all of the RichTextBox ForeColors
            foreach (KeyValuePair<string, OutputTextBoxInfo> pair in m_outputTextBoxDictionary)
            {
                if (pair.Value.ColorInfo.MultiColor)
                {
                    pair.Value.VisibleTextBox.ForeColor = (pair.Value.ColorInfo.DefaultColor == Color.Empty)
                        ? SystemColors.WindowText : pair.Value.ColorInfo.DefaultColor;
                }
                else
                {
                    pair.Value.VisibleTextBox.ForeColor = SystemColors.WindowText;
                }
            }
        }

        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        private static extern int GetWindowPlacement(int hWnd, ref WINDOWPLACEMENT lpPlacement);

        private void Serialize(XmlTextWriter xmlWriter)
        {
            WINDOWPLACEMENT placement = new WINDOWPLACEMENT();
            GetWindowPlacement((int)this.Handle, ref placement);

            xmlWriter.WriteWhitespace("\n");

            xmlWriter.WriteStartElement("MainWindow");
            {
                // layout stuff:
                xmlWriter.WriteAttributeString("left", placement.rcNormalPosition.left.ToString());
                xmlWriter.WriteAttributeString("top", placement.rcNormalPosition.top.ToString());
                long width = placement.rcNormalPosition.right - placement.rcNormalPosition.left;
                long height = placement.rcNormalPosition.bottom - placement.rcNormalPosition.top;
                xmlWriter.WriteAttributeString("width", width.ToString());
                xmlWriter.WriteAttributeString("height", height.ToString());

                Rectangle screenRect = rageImage.GetScreenBounds();
                xmlWriter.WriteAttributeString("screenWidth", screenRect.Width.ToString());
                xmlWriter.WriteAttributeString("screenHeight", screenRect.Height.ToString());

                xmlWriter.WriteAttributeString("windowstate", this.WindowState.ToString());

                xmlWriter.WriteStartElement("MultiColorOutput");
                {
                    foreach (KeyValuePair<string, OutputTextBoxInfo> pair in m_outputTextBoxDictionary)
                    {
                        xmlWriter.WriteStartElement("Item");
                        {
                            xmlWriter.WriteAttributeString("name", pair.Value.ChannelName);
                            xmlWriter.WriteAttributeString("multiColor", pair.Value.ColorInfo.MultiColor.ToString());
                            xmlWriter.WriteAttributeString("defaultColor", Widget.SerializeColor(pair.Value.ColorInfo.DefaultColor));
                            xmlWriter.WriteAttributeString("guid", pair.Value.Window.Guid.ToString());

                            if (pair.Value.Window.ShowOptions)
                            {
                                xmlWriter.WriteAttributeString("CloseAction", pair.Value.Window.CloseAction.ToString());
                            }
                        }
                        xmlWriter.WriteEndElement();
                    }
                }
                xmlWriter.WriteEndElement();
            }
            xmlWriter.WriteEndElement();
        }

        private static string GetTempFileName()
        {
            return Path.Combine(Path.GetTempPath(), string.Format("{0}.tmp", Guid.NewGuid()));
        }

        private bool SaveLayout(string filename, String exePathName)
        {
            string errorsFilename = filename + ".LayoutErrors.txt";

            string tempFilename = GetTempFileName();
            string tempErrorsFilename = tempFilename + ".LayoutErrors.txt";

            StreamWriter errorsWriter = null;

            XmlTextWriter xmlWriter = null;
            try
            {
                xmlWriter = new XmlTextWriter(tempFilename, System.Text.Encoding.Unicode);
            }
            catch (Exception e)
            {
                _log.ToolExceptionCtx(LogCtx, e, "Unable to create temporary Layout save file, '{0}'.", tempFilename);
                return false;
            }

            try
            {
                if (xmlWriter != null)
                {
                    xmlWriter.Formatting = Formatting.Indented;

                    xmlWriter.WriteStartDocument();
                    {
                        xmlWriter.WriteWhitespace("\n");
                        xmlWriter.WriteStartElement("rag");
                        xmlWriter.WriteAttributeString("version", System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString());
                        {
                            // serialize widget views:
                            xmlWriter.WriteWhitespace("\n");
                            WidgetViewBase.SerializeViews(xmlWriter, exePathName);
                            xmlWriter.WriteWhitespace("\n");

                            // serialize render windows:
                            xmlWriter.WriteWhitespace("\n");
                            RenderWindow.SerializeViews(xmlWriter, exePathName);
                            xmlWriter.WriteWhitespace("\n");

                            // serialize widget groups
                            xmlWriter.WriteWhitespace("\n");
                            WidgetGroupBase.SerializeGroups(xmlWriter);
                            xmlWriter.WriteWhitespace("\n");

                            xmlWriter.WriteWhitespace("\n");
                            Serialize(xmlWriter);
                            xmlWriter.WriteWhitespace("\n");

                            xmlWriter.WriteWhitespace("\n");
                            xmlWriter.WriteStartElement("Plugins");
                            {
                                foreach (DllInfo info in sm_GlobalPrefs.KnownDlls)
                                {
                                    if (info.NameToGuidHash.Count == 0)
                                    {
                                        continue;
                                    }

                                    xmlWriter.WriteStartElement(info.Name);
                                    {
                                        foreach (KeyValuePair<String, String> pair in info.NameToGuidHash)
                                        {
                                            xmlWriter.WriteStartElement("WidgetPage");
                                            {
                                                xmlWriter.WriteAttributeString("name", pair.Key);
                                                xmlWriter.WriteAttributeString("guid", pair.Value);
                                            }
                                            xmlWriter.WriteEndElement();
                                        }
                                    }
                                    xmlWriter.WriteEndElement();
                                }
                            }
                            xmlWriter.WriteEndElement();
                            xmlWriter.WriteWhitespace("\n\n");

                            // serialize docking layout managers:
                            string layoutString = this.sandDockManager1.GetLayout();

                            // verification
                            string errors = ValidateLayoutSave(layoutString);
                            if (errors.Length > 0)
                            {
                                try
                                {
                                    errorsWriter = new StreamWriter(tempErrorsFilename);
                                }
                                catch (Exception e)
                                {
                                    _log.ToolExceptionCtx(LogCtx, e, "Unable to create temporary Layout errors file, '{0}'.", tempErrorsFilename);
                                }

                                if (errorsWriter != null)
                                {
                                    errorsWriter.Write(errors);
                                    errorsWriter.Close();
                                }

                                bool moved = RageApplication.MoveFileSafe(tempErrorsFilename, errorsFilename, _log);

                                throw (new Exception(String.Format("Invalid SandDock layout.  See '{0}' for more information.",
                                    moved ? errorsFilename : tempErrorsFilename)));
                            }
                            else
                            {
                                const string xmlHeaderString = "<?xml version=\"1.0\" encoding=\"utf-16\"?>";
                                xmlWriter.WriteStartElement("SandDockManager");
                                {
                                    // argghh - stupid header:
                                    layoutString = layoutString.Replace(xmlHeaderString, "");
                                    xmlWriter.WriteRaw(layoutString);
                                    xmlWriter.WriteWhitespace("\n");
                                }
                                xmlWriter.WriteEndElement();
                                xmlWriter.WriteWhitespace("\n");
                            }
                        }
                        xmlWriter.WriteEndElement();
                    }
                    xmlWriter.WriteEndDocument();
                    xmlWriter.Close();
                }
            }
            catch (Exception e)
            {
                if (xmlWriter != null)
                {
                    xmlWriter.Close();
                }

                if (errorsWriter != null)
                {
                    errorsWriter.Close();
                }

                rageMessageBox.ShowWarning(this, "Problem serializing layout file: " + e.Message, "Problem serializing layout file!");
                return false;
            }

            return RageApplication.MoveFileSafe(tempFilename, filename, _log);
        }

        private bool LoadForced(string fileName)
        {
            if (File.Exists(fileName) == false)
            {
                return false;
            }

            // make sure our file exists:
            if (File.Exists(fileName) == false)
            {
                RemoveFromRecentLayoutList(fileName);
                return false;
            }

            // parse the layout file:
            XmlTextReader xmlReader = null;
            try
            {
                xmlReader = new XmlTextReader(fileName);
                xmlReader.WhitespaceHandling = WhitespaceHandling.None;

                while (xmlReader.Read())
                {
                    if (xmlReader.NodeType == XmlNodeType.Element && xmlReader.Name == "WidgetViews" && xmlReader.IsEmptyElement == false)
                    {
                        while (xmlReader.Read())
                        {
                            if (xmlReader.NodeType == XmlNodeType.Element && xmlReader.Name == "rag.FavoritesView")
                            {
                                bool exists = false;
                                String widgetFileName = Environment.GetEnvironmentVariable("RS_TOOLSROOT") + "\\" + xmlReader["WidgetsManifestFile"];

                                foreach (DockControl dockControl in DockableViewManager.Instance.DockControls)
                                {
                                    if ((dockControl.Controls.Count > 0) && (dockControl.Controls[0] is FavoritesView))
                                    {
                                        FavoritesView faveView = dockControl.Controls[0] as FavoritesView;
                                        if (faveView.SameManifest(widgetFileName) == true)
                                        {
                                            exists = true;
                                            break;
                                        }
                                    }
                                }

                                if (exists == false)
                                {
                                    WidgetViewBase.CreateDel del = null;
                                    WidgetViewBase widgetView = null;
                                    del = new WidgetViewBase.CreateDel(FavoritesView.CreateFavoritesView);
                                    widgetView = RageApplication.MainApp.AddWidgetView(RageApplication.MainApp.ExeName.ToLower(), del, true);
                                    widgetView.DeserializeView(xmlReader, RageApplication.MainApp.BankMgr);
                                }
                            }
                        }
                    }
                }

                xmlReader.Close();
            }
            catch (Exception)
            {
                if (xmlReader != null)
                {
                    xmlReader.Close();
                }
            }

            return true;
        }

        private bool LoadLayout(string fileName, bool reloading)
        {
            // make sure our file exists:
            if (File.Exists(fileName) == false)
            {
                RemoveFromRecentLayoutList(fileName);
                return false;
            }

            // parse the layout file:
            XmlTextReader xmlReader = null;
            StreamReader sr = null;
            try
            {
                xmlReader = new XmlTextReader(fileName);
                xmlReader.WhitespaceHandling = WhitespaceHandling.None;

                while (xmlReader.Read())
                {
                    // 1. parse xml stuff:
                    while (xmlReader.Read())
                    {
                        // read and create render views:
                        if (xmlReader.NodeType == XmlNodeType.Element && xmlReader.Name == "RenderWindows" && xmlReader.IsEmptyElement == false)
                        {
                            while (xmlReader.NodeType != XmlNodeType.EndElement || xmlReader.Name != "RenderWindows")
                            {
                                xmlReader.Read();

                                if (xmlReader.NodeType == XmlNodeType.Element && xmlReader.Name == "rag.RenderWindow")
                                {
                                    string guid = xmlReader["guid"];
                                    string appName = xmlReader["appname"];
                                    RageApplication app = RageApplication.FindApplication(appName);

                                    if (app != null)
                                    {
                                        if (app != RageApplication.MainApp)
                                        {
                                            app.ShouldStart = xmlReader["start"] == "true" ? true : false;
                                        }

                                        RenderWindow window = app.AddRenderWindow(new System.Guid(guid));
                                        if (window != null)
                                        {
                                            window.DeserializeView(xmlReader, appName);
                                        }
                                    }
                                }
                            }
                        }
                        // read and create widget views:
                        else if (xmlReader.NodeType == XmlNodeType.Element && xmlReader.Name == "WidgetViews" && xmlReader.IsEmptyElement == false)
                        {
                            while (xmlReader.NodeType != XmlNodeType.EndElement || xmlReader.Name != "WidgetViews")
                            {
                                xmlReader.Read();
                                WidgetViewBase.CreateDel del = null;
                                if (xmlReader.NodeType == XmlNodeType.Element && xmlReader.Name == System.Type.GetType("rag.BankTreeView").ToString())
                                {
                                    del = new WidgetViewBase.CreateDel(BankTreeView.CreateBankTreeView);
                                }
                                else if (xmlReader.NodeType == XmlNodeType.Element && xmlReader.Name == System.Type.GetType("rag.FindView").ToString())
                                {
                                    del = new WidgetViewBase.CreateDel(FindView.CreateFindView);
                                }
                                else if (xmlReader.NodeType == XmlNodeType.Element && xmlReader.Name == System.Type.GetType("rag.FavoritesView").ToString())
                                {
                                    del = new WidgetViewBase.CreateDel(FavoritesView.CreateFavoritesView);
                                }
                                else if (xmlReader.NodeType == XmlNodeType.Element && xmlReader.Name == System.Type.GetType("rag.WidgetViewSingle").ToString())
                                {
                                    del = new WidgetViewBase.CreateDel(WidgetViewSingle.CreateWidgetViewSingle);
                                }

                                WidgetViewBase widgetView = null;
                                RageApplication app = null;

                                // create the view:
                                if (del != null)
                                {
                                    string guid = xmlReader["guid"];
                                    string appName = xmlReader["appname"];
                                    app = RageApplication.FindApplication(appName);
                                    if (app != null)
                                        widgetView = app.AddWidgetView(appName, new System.Guid(guid), del, true);
                                }

                                // deserialize it:
                                if (widgetView != null)
                                {
                                    try
                                    {
                                        widgetView.DeserializeView(xmlReader, app.BankMgr);
                                    }
                                    catch (Exception e)
                                    {
                                        String message = String.Format("An error occurred while loading '{0}':\r\n\t{1}\r\n\r\n" +
                                            "Do you wish to remove this view from the UI?",
                                            widgetView.GetTabText(), e.Message);
                                        String caption = String.Format("Unable to load widget view.");

                                        DialogResult result = rageMessageBox.ShowError(this, message, caption, MessageBoxButtons.YesNo);
                                        if (result == DialogResult.Yes)
                                        {
                                            widgetView.DockControl.Close();
                                        }
                                    }
                                }
                            }
                        }
                        else if (xmlReader.NodeType == XmlNodeType.Element && xmlReader.Name == "WidgetGroups" && xmlReader.IsEmptyElement == false)
                        {
                            WidgetGroupBase.DeserializeGroups(xmlReader);
                        }
                        else if (xmlReader.NodeType == XmlNodeType.Element && xmlReader.Name == "MainWindow")
                        {
                            Deserialize(xmlReader);
                        }
                        else if (xmlReader.NodeType == XmlNodeType.Element && xmlReader.Name == "Plugins" && xmlReader.IsEmptyElement == false)
                        {
                            while (xmlReader.NodeType != XmlNodeType.EndElement || xmlReader.Name != "Plugins")
                            {
                                xmlReader.Read();

                                if (sm_GlobalPrefs.KnownDllHash.ContainsKey(xmlReader.Name))
                                {
                                    ragCore.DllInfo info = sm_GlobalPrefs.KnownDllHash[xmlReader.Name];
                                    if (info != null)
                                    {
                                        while (xmlReader.NodeType != XmlNodeType.EndElement || xmlReader.Name != info.Name)
                                        {
                                            xmlReader.Read();

                                            if (xmlReader.NodeType == XmlNodeType.Element && xmlReader.Name == "WidgetPage")
                                            {
                                                String name = xmlReader["name"];
                                                String guid = xmlReader["guid"];

                                                if (name != null && guid != null)
                                                {
                                                    info.NameToGuidHash[name] = guid;
                                                    info.GuidToNameHash[guid] = name;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else if (xmlReader.NodeType == XmlNodeType.Element && xmlReader.Name == "SandBarManager")
                        {
                            throw (new Exception("Unable to parse old layout file " + fileName));
                        }
                    }
                }
                xmlReader.Close();

                // see if the file exists first:
                try
                {
                    sr = new StreamReader(fileName);
                }
                catch
                {
                    RemoveFromRecentLayoutList(fileName);
                    return false;
                }

                // 2. parse layout manager stuff.
                while (sr.Peek() != -1)
                {
                    // find the begin tag:
                    string line = sr.ReadLine();
                    string stopTag = "";
                    TD.SandDock.SandDockManager manager = null;

                    if (line.Trim() == "<SandDockManager>")
                    {
                        stopTag = "</SandDockManager>";
                        manager = this.sandDockManager1;
                    }

                    // parse the layout info for the manager:
                    if (manager != null)
                    {
                        line = sr.ReadLine();
                        string sandDockLayoutInfo = "";
                        while (line.Trim() != stopTag)
                        {
                            sandDockLayoutInfo += line;
                            line = sr.ReadLine();
                        }

                        bool validWidgetViewGuid = true;
                        bool validRenderWindowGuid = true;
                        string loadErrors = reloading ? string.Empty
                            : ValidateLayoutLoad(sandDockLayoutInfo, out validWidgetViewGuid, out validRenderWindowGuid);

                        if (validRenderWindowGuid)
                        {
                            if (!validWidgetViewGuid)
                            {
                                rageMessageBox.ShowWarning(this, "Could not locate WidgetView guid in SandDock layout.\n"
                                    + "Open it manually from the Window menu or create a new one from the Widget menu.",
                                    "WidgetView Guid Not Found");
                            }

                            manager.SetLayout(sandDockLayoutInfo);

                            // ensure all windows are at least partially visible on screen
                            DockControl[] dockControls = manager.GetDockControls();
                            foreach (DockControl dockControl in dockControls)
                            {
                                Point loc = dockControl.FloatingLocation;
                                Size size = dockControl.FloatingSize;
                                rageImage.AdjustForResolution(m_lastScreenSize, rageImage.GetScreenBounds(),
                                    ref loc, ref size);

                                dockControl.FloatingLocation = loc;
                                dockControl.FloatingSize = size;
                            }
                        }
                        else
                        {
                            throw (new Exception(loadErrors + "\nPlease email "
                                + fileName + " to greg@rockstarnorth.com."));
                        }
                    }
                }
                sr.Close();
            }
            catch (Exception e)
            {
                if (xmlReader != null)
                {
                    xmlReader.Close();
                }

                if (sr != null)
                {
                    sr.Close();
                }

                rageMessageBox.ShowWarning(this, "Problem parsing layout file " + fileName + ":\n" + e.Message +
                    "\n\n RAG will continue but it will not load the last saved layout.", "Problem parsing layout file!");

                // clear all widget views:
                DockableViewManager.Instance.CloseAllViews();
                CloseAllUserCreatedOutputWindows();

                RemoveFromRecentLayoutList(fileName);

                try
                {
                    // Move the file so that it doesn't get loaded next time
                    File.Move(fileName, fileName + "." + DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss") + ".old");
                }
                catch (System.Exception ex)
                {
                    _log.ToolExceptionCtx(LogCtx, ex, "Couldn't move layout file");
                }

                return false;
            }

            this.CurrentLayoutFilename = fileName;
            return true;
        }

        private string ValidateLayoutGuids(String layout, out List<string> guidStrings, out List<string> ctrlStrings)
        {
            guidStrings = new List<string>();
            Regex regex = new Regex("Window Guid\\x3d\\x22(?<guid>[a-zA-Z0-9\\x2d]+)\\x22");
            Match match = regex.Match(layout);
            while (match.Success)
            {
                string guidString = match.Result("${guid}");
                if ((guidString != null) && (guidString != string.Empty))
                {
                    guidStrings.Add(guidString);
                }

                match = match.NextMatch();
            }

            regex = new Regex("LastFixedLayoutGuid\\x3d\\x22(?<guid>[a-zA-Z0-9\\x2d]+)\\x22");
            match = regex.Match(layout);
            while (match.Success)
            {
                string guidString = match.Result("${guid}");
                if ((guidString != null) && (guidString != string.Empty))
                {
                    guidStrings.Add(guidString);
                }

                match = match.NextMatch();
            }

            regex = new Regex("LastLayoutGuid\\x3d\\x22(?<guid>[a-zA-Z0-9\\x2d]+)\\x22");
            match = regex.Match(layout);
            while (match.Success)
            {
                string guidString = match.Result("${guid}");
                if ((guidString != null) && (guidString != string.Empty))
                {
                    guidStrings.Add(guidString);
                }

                match = match.NextMatch();
            }

            ctrlStrings = new List<string>();
            regex = new Regex("Control Guid\\x3d\\x22(?<guid>[a-zA-Z0-9\\x2d]+)\\x22");
            match = regex.Match(layout);
            while (match.Success)
            {
                string ctrlString = match.Result("${guid}");
                if ((ctrlString != null) && (ctrlString != string.Empty))
                {
                    ctrlStrings.Add(ctrlString);
                }

                match = match.NextMatch();
            }

            StringBuilder errors = new StringBuilder();

            /* This is actually ok.  We have a persistant window that has been closed
            foreach ( String guidString in guidStrings )
            {
                if ( !ctrlStrings.Contains( guidString ) )
                {
                    errors.Append( "Window Guid " );
                    errors.Append( guidString );
                    errors.Append( " not found in a Control.\n" );
                }
            }
            */

            foreach (String ctrlString in ctrlStrings)
            {
                if (!guidStrings.Contains(ctrlString))
                {
                    errors.Append("Control Guid ");
                    errors.Append(ctrlString);
                    errors.Append(" not found in a Window.\n");
                }
            }

            return errors.ToString();
        }

        private string ValidateLayoutSave(String layout)
        {
            List<string> guidStrings;
            List<string> ctrlStrings;
            StringBuilder saveErrors = new StringBuilder(ValidateLayoutGuids(layout, out guidStrings, out ctrlStrings));

            foreach (TD.SandDock.DockControl dockControl in DockControlManager.Instance.WidgetViewDockControls)
            {
                if (dockControl.PersistState && !guidStrings.Contains(dockControl.Guid.ToString()))
                {
                    saveErrors.Append("SandDock Layout did not contain WidgetView Guid ");
                    saveErrors.Append(dockControl.Guid.ToString());
                    saveErrors.Append(" for ");
                    saveErrors.Append(dockControl.TabText);

                    if (dockControl.Manager != this.sandDockManager1)
                    {
                        saveErrors.Append(".  SandDockManager mismatch");
                    }

                    saveErrors.Append(".\n");
                }
            }

            foreach (TD.SandDock.DockControl dockControl in DockControlManager.Instance.RenderWindowDockControls)
            {
                if (dockControl.PersistState && !guidStrings.Contains(dockControl.Guid.ToString()))
                {
                    saveErrors.Append("SandDock Layout did not contain RenderWindow Guid ");
                    saveErrors.Append(dockControl.Guid.ToString());
                    saveErrors.Append(" for ");
                    saveErrors.Append(dockControl.TabText);

                    if (dockControl.Manager != this.sandDockManager1)
                    {
                        saveErrors.Append(".  SandDockManager mismatch");
                    }

                    saveErrors.Append(".\n");
                }
            }

            return saveErrors.ToString();
        }

        private string ValidateLayoutLoad(String layout, out bool validWidgetViewGuid, out bool validRenderWindowGuid)
        {
            List<string> guidStrings;
            List<string> ctrlStrings;
            StringBuilder loadErrors = new StringBuilder(ValidateLayoutGuids(layout, out guidStrings, out ctrlStrings));

            validWidgetViewGuid = false;
            foreach (TD.SandDock.DockControl dockControl in DockControlManager.Instance.WidgetViewDockControls)
            {
                if (guidStrings.Contains(dockControl.Guid.ToString()))
                {
                    validWidgetViewGuid = true;
                    break;
                }
            }

            validRenderWindowGuid = false;
            foreach (TD.SandDock.DockControl dockControl in DockControlManager.Instance.RenderWindowDockControls)
            {
                if (guidStrings.Contains(dockControl.Guid.ToString()))
                {
                    validRenderWindowGuid = true;
                    break;
                }
            }

            if (!validWidgetViewGuid)
            {
                loadErrors.Append("Could not locate a valid WidgetView Guid.\n");
            }

            if (!validRenderWindowGuid)
            {
                loadErrors.Append("Could not locate a valid RenderWindow Guid.\n");
            }

            return loadErrors.ToString();
        }

        protected static void LoadGlobalPrefs(string filename)
        {
            try
            {
                if (!InSafeMode() && File.Exists(filename))
                {
                    XmlSerializer loader = new XmlSerializer(typeof(GlobalPrefs));
                    FileStream inStream = new FileStream(filename, FileMode.Open);
                    sm_GlobalPrefs = loader.Deserialize(inStream) as GlobalPrefs;
                    inStream.Close();
                }

                if (sm_GlobalPrefs == null)
                {
                    sm_GlobalPrefs = new GlobalPrefs();
                }
            }
            catch (Exception e)
            {
                rageMessageBox.ShowWarning("Problem parsing global preferences file" + filename + ":\n " + e.Message +
                    "\n\n RAG will continue but it will not load the global preferences.", "Problem parsing global preferences!");
                sm_GlobalPrefs = new GlobalPrefs();
            }

            // Apply preferences
            ragUi.ControlColor.DefaultToExtended = Boolean.Parse(sm_GlobalPrefs.GetUiDataItem("ExtendedColorWidget", "false"));

            MainWindow.DumpOutputInRealTime = Boolean.Parse(sm_GlobalPrefs.GetUiDataItem("DumpOutputInRealTime", "false"));
            MainWindow.DumpOutputOnExit = Boolean.Parse(sm_GlobalPrefs.GetUiDataItem("DumpOutputOnExit", "true"));
            MainWindow.AppendTimestampToOutputFilename = Boolean.Parse(sm_GlobalPrefs.GetUiDataItem("AppendTimestampToOutputFilename", "false"));
            MainWindow.ZipOutputFileOnExit = Boolean.Parse(sm_GlobalPrefs.GetUiDataItem("ZipOutputFileOnExit", "false"));
            MainWindow.SuppressAssertQuitCrashDialog = Boolean.Parse(sm_GlobalPrefs.GetUiDataItem("SuppressAssertQuitCrashDialog", "false"));
        }

        protected void SaveGlobalPrefs(string filename)
        {
            if (InSafeMode())
            {
                return;
            }

            string tempFilename = GetTempFileName();

            StreamWriter writer = null;
            try
            {
                writer = new StreamWriter(tempFilename);
            }
            catch (Exception e)
            {
                _log.ToolExceptionCtx(LogCtx, e, "Unable to create temporary GlobalPrefs save file, '{0}'.", tempFilename);
                return;
            }

            try
            {
                if (writer != null)
                {
                    XmlSerializer saver = new XmlSerializer(typeof(GlobalPrefs));
                    saver.Serialize(writer, sm_GlobalPrefs);
                    writer.Close();
                }
            }
            catch (Exception e)
            {
                if (writer != null)
                {
                    writer.Close();
                }

                rageMessageBox.ShowWarning(String.Format("There was an error saving '{0}':\n{1}", tempFilename, e.Message),
                    "Problem saving Global Preferences file!");
                return;
            }

            RageApplication.MoveFileSafe(tempFilename, filename, _log);
        }

        protected static void LoadRagSettings(string filename)
        {
            if (File.Exists(filename))
            {
                try
                {
                    XmlSerializer loader = new XmlSerializer(typeof(RagSettings));
                    FileStream inStream = new FileStream(filename, FileMode.Open);
                    sm_RagSettings = loader.Deserialize(inStream) as RagSettings;
                    inStream.Close();
                }
                catch (Exception e)
                {
                    rageMessageBox.ShowWarning("Problem parsing RAG settings file " + filename + ":\n " + e.Message +
                        "\n\n RAG will continue but it will not load the RAG settings.", "Problem parsing RAG settings!");
                }
            }

            // Validate the Rag Settings
            if (!String.IsNullOrEmpty(sm_RagSettings.SharedLayoutsDir) &&
                 !Directory.Exists(sm_RagSettings.SharedLayoutsDir))
            {
                rageMessageBox.ShowWarning(
                    "Could not find shared layouts directory '" + sm_RagSettings.SharedLayoutsDir + "' specified in the RAG settings file.\n" +
                    "Shared layout functionality will be disabled.",
                    "RAG Settings");

                sm_RagSettings.SharedLayoutsDir = String.Empty;
            }

            if (!String.IsNullOrEmpty(sm_RagSettings.SharedFavoritesDir) &&
                 !Directory.Exists(sm_RagSettings.SharedFavoritesDir))
            {
                rageMessageBox.ShowWarning(
                    "Could not find shared favorites directory '" + sm_RagSettings.SharedFavoritesDir + "' specified in the RAG settings file.\n" +
                    "Shared favorites functionality will be disabled.",
                    "RAG Settings");

                sm_RagSettings.SharedFavoritesDir = String.Empty;
            }
        }

        private bool ReloadALayout(string fileName)
        {
            if (!File.Exists(fileName))
            {
                rageMessageBox.ShowWarning(this, String.Format("The layout file '{0}' doesn't exist.", fileName), "Load Layout Problem");
                return false;
            }

            DockableViewManager.Instance.CloseAllViews();
            CloseAllUserCreatedOutputWindows();

            // To stop the gameViewDocumentContainer from being disposed...
            RageApplication.MainApp.RemoveRenderWindow();

            string previousLayoutFilename = this.CurrentLayoutFilename;
            if (LoadLayout(fileName, true))
            {
                if (previousLayoutFilename != fileName)
                {
                    // add the old one to the recent layout list
                    AddToRecentLayoutList(previousLayoutFilename);
                }

                RageApplication.CallInitFinished();
                return true;
            }

            // we'll automatically add a bank view if the layout load failed
            if (DockControlManager.Instance.WidgetViewDockControls.Count == 0)
            {
                RageApplication.MainApp.AddWidgetView(RageApplication.MainApp.ExeName, new WidgetViewBase.CreateDel(BankTreeView.CreateBankTreeView), false);
            }

            return false;
        }

        private void CreateSharedLayoutsMenu(string directory, ToolStripMenuItem topMenuItem)
        {
            topMenuItem.DropDownItems.Clear();

            if (!String.IsNullOrEmpty(directory) && Directory.Exists(directory))
            {
                string[] subDirectories = Directory.GetDirectories(directory);
                foreach (string subDirectory in subDirectories)
                {
                    ToolStripMenuItem menuItem = new ToolStripMenuItem();
                    menuItem.Text = Path.GetFileName(subDirectory);
                    CreateSharedLayoutsMenu(subDirectory, menuItem);

                    if (menuItem.DropDownItems.Count > 0)
                    {
                        topMenuItem.DropDownItems.Add(menuItem);
                    }
                }

                string[] layoutFiles = Directory.GetFiles(directory, "*.xml", SearchOption.TopDirectoryOnly);
                foreach (string layoutFile in layoutFiles)
                {
                    string filename = Path.GetFileName(layoutFile);
                    if (filename.StartsWith(".ragwidgets.") || filename.StartsWith("FavoritesView")
                        || filename.StartsWith(".ragpreferences."))
                    {
                        continue;
                    }

                    ToolStripMenuItem menuItem = new ToolStripMenuItem();
                    menuItem.Text = filename;
                    menuItem.Click += new EventHandler(openSharedLayoutMenuItem_Click);
                    menuItem.Tag = layoutFile;
                    topMenuItem.DropDownItems.Add(menuItem);
                }
            }

            topMenuItem.Enabled = (topMenuItem.DropDownItems.Count > 0);
        }

        private void CreateSharedFavoritesMenu(string directory, ToolStripMenuItem topMenuItem)
        {
            topMenuItem.DropDownItems.Clear();

            if (!String.IsNullOrEmpty(directory) && Directory.Exists(directory))
            {
                string[] subDirectories = Directory.GetDirectories(directory);
                foreach (string subDirectory in subDirectories)
                {
                    ToolStripMenuItem menuItem = new ToolStripMenuItem();
                    menuItem.Text = Path.GetFileName(subDirectory);
                    CreateSharedFavoritesMenu(subDirectory, menuItem);

                    if (menuItem.DropDownItems.Count > 0)
                    {
                        topMenuItem.DropDownItems.Add(menuItem);
                    }
                }

                string[] favoritesFiles = Directory.GetFiles(directory, "*.xml", SearchOption.TopDirectoryOnly);
                foreach (string favoritesFile in favoritesFiles)
                {
                    string filename = Path.GetFileName(favoritesFile);
                    if (filename.StartsWith(".raglayout.") || filename.StartsWith(".ragwidgets.")
                        || filename.StartsWith(".ragpreferences."))
                    {
                        continue;
                    }

                    ToolStripMenuItem menuItem = new ToolStripMenuItem();
                    menuItem.Text = filename;
                    menuItem.Click += new EventHandler(addSharedFavoritesViewMenuItem_Click);
                    menuItem.Tag = favoritesFile;
                    topMenuItem.DropDownItems.Add(menuItem);
                }
            }

            topMenuItem.Enabled = (topMenuItem.DropDownItems.Count > 0);
        }
        #endregion

        #region Misc
        private void AppExit()
        {
            this.Close();
        }

        private TD.SandDock.DockControl FindDockControl(System.Guid guid)
        {
            TD.SandDock.DockControl[] dockControls = this.sandDockManager1.GetDockControls();
            foreach (TD.SandDock.DockControl dockControl in dockControls)
            {
                if (dockControl.Guid == guid)
                    return dockControl;
            }

            // not found:
            return null;
        }

        public static bool InSafeMode()
        {
            string[] args = Environment.GetCommandLineArgs();
            foreach (string str in args)
            {
                if (str == "-safe")
                {
                    return true;
                }
            }
            return false;
        }

        private void LoadApps(GameConnection connection)
        {
            int retryCount = 10;
            while (retryCount > 0)
            {
                try
                {
                    this.Cursor = Cursors.AppStarting;

                    // show application, the ip (if specified) and its command line arguments in the title bar:
                    this.Text = GenerateTitleString(connection);

                    RageApplication mainApp = RageApplication.MainApp;
                    m_ExePathName = Path.GetDirectoryName(mainApp.ExePath);
                    m_ExeFileName = Path.GetFileName(mainApp.ExePath);

                    // load the last layout and recent layout filenames
                    String strLayoutFiles = Settings.Default.LayoutFiles;
                    if (!String.IsNullOrEmpty(strLayoutFiles))
                    {
                        if (!m_layoutFileInfos.Deserialize(strLayoutFiles))
                        {
                            _log.ErrorCtx(LogCtx, "Could not deserialized LayoutFileInfos");
                        }
                    }

                    // RichTextBoxFindReplace
                    string richTextBoxFindReplaceSerializedText = Settings.Default.RichTextBoxFindReplace;
                    if (!String.IsNullOrEmpty(richTextBoxFindReplaceSerializedText))
                    {
                        rageRichTextBoxSearchReplaceSettings s = new rageRichTextBoxSearchReplaceSettings();
                        if (s.Deserialize(richTextBoxFindReplaceSerializedText))
                        {
                            m_richTextBoxSearchReplaceDialog.SetUIFromSettings(s);
                        }
                    }

                    // delete the log so people don't report old crashes:
                    DeleteOutputLog(this.OutputLogPath);

                    string globalPrefsFilename = Path.Combine(MainWindow.UserPrefDir, ".ragpreferences." + Environment.UserName + ".xml");
                    LoadGlobalPrefs(globalPrefsFilename);

                    // Load the RAG settings file
                    string settingsFile = Path.GetDirectoryName(Application.ExecutablePath) + "\\" + c_RagSettingsFile;
                    LoadRagSettings(settingsFile);

                    CreateSharedLayoutsMenu(sm_RagSettings.SharedLayoutsDir, this.openSharedLayoutsToolStripMenuItem);
                    CreateSharedFavoritesMenu(sm_RagSettings.SharedFavoritesDir, this.sharedFavoritesToolStripMenuItem);

                    // setup the preferences submenu
                    this.userPreferencesDirectoryToolStripMenuItem.Text = "&User Preferences Directory: " + Settings.Default.PreferencesDirectory;
                    this.dumpOutputInRealTimeToolStripMenuItem.Checked = sm_DumpOutputInRealTime;
                    this.dumpOutputOnExitToolStripMenuItem.Checked = sm_DumpOutputOnExit;
                    this.appendTimestampToOutputFileNameToolStripMenuItem.Checked = sm_appendTimestampToOutputFilename;
                    this.zipOutputOnExitToolStripMenuItem.Checked = sm_zipOutputFileOnExit;
                    this.suppressAssertCrashQuitDialogOnExitToolStripMenuItem.Checked = sm_suppressAssertQuitCrashDetectedDialog;

                    if (sm_DumpOutputInRealTime)
                    {
                        if (sm_dumpWriter != null)
                        {
                            sm_dumpWriter.Close();
                        }

                        sm_dumpWriter = new StreamWriter(this.OutputLogPath);
                    }

                    // load .dlls from paths specified by user:
                    if (String.IsNullOrEmpty(sm_PluginPath) == false)
                    {
                        string[] paths = sm_PluginPath.Split(';');
                        foreach (string path in paths)
                        {
                            LoadDlls(path);
                        }
                    }

                    // load .dlls from executable path:
                    if (Debugger.IsAttached)
                    {
                        LoadDlls(Path.Combine(Environment.CurrentDirectory, "Plugins"));
                    }
                    else
                    {
                        LoadDlls(Path.Combine(Path.GetDirectoryName(Application.ExecutablePath), "Plugins"));
                    }


                    // load the saved out layout, or use the default if it exists:
                    string layoutFilename = string.Empty;
                    LayoutFileInfo info;
                    if (m_layoutFileInfos.LayoutInfos.TryGetValue(mainApp.ExePath.ToLower(), out info))
                    {
                        if (!String.IsNullOrEmpty(info.LastLayoutFilename))
                        {
                            layoutFilename = info.LastLayoutFilename;
                        }

                        foreach (string filename in info.RecentLayoutFilenames)
                        {
                            AddToRecentLayoutList(filename);
                        }
                    }

                    m_defaultLayoutFilename = Path.Combine(MainWindow.UserPrefDir, ".raglayout." + m_ExeFileName + "." + Environment.UserName + ".xml");
                    string mainAppDefaultLayoutFilename = Path.GetDirectoryName(mainApp.ExePath) + @"\.raglayout." + m_ExeFileName + ".xml";

                    if (String.IsNullOrEmpty(layoutFilename) || !File.Exists(layoutFilename))
                    {
                        if (File.Exists(m_defaultLayoutFilename))
                        {
                            layoutFilename = m_defaultLayoutFilename;
                        }
                        else if (File.Exists(mainAppDefaultLayoutFilename))
                        {
                            layoutFilename = mainAppDefaultLayoutFilename;
                        }
                    }

                    InitPlugIns();

                    if (!String.IsNullOrEmpty(layoutFilename))
                    {
                        LoadLayout(layoutFilename, false);
                    }

                    //String usertype = m_View.UserType;

                    String forcedFavsFilename = Environment.GetEnvironmentVariable("RS_TOOLSROOT") + "\\etc\\rag\\default_widgetviews.xml";

                    if (!String.IsNullOrEmpty(forcedFavsFilename))
                    {
                        LoadForced(forcedFavsFilename);
                    }

                    mainApp.BankMgr.InitFinished += new InitFinishedHandler(BankMgr_InitFinished);


                    RageApplication.LoadApps(this, this.viewersToolStripMenuItem, _log, new EventHandler(BeginAddStringToOutput),
                        new RageApplication.OutputTextEventHandler(AddStringToOutput), new EventHandler(EndAddStringToOutput),
                        new RageApplication.CreateOutputWindowDel(CreateOutputWindow),
                        new RageApplication.AddSubChannelInfoDel(AddSubChannelInfo));

                    // enable the menus:
                    this.exitApplicationToolStripMenuItem.Enabled = true;
                    this.exitApplicationToolStripMenuItem.Visible = true;
                    this.exitApplicationToolStripMenuItem.Text = "E&xit " + mainApp.VisibleName;

                    // we'll automatically add a bank view if the layout wasn't loaded:
                    //if ( loadedLayout == false )
                    if (DockControlManager.Instance.WidgetViewDockControls.Count == 0)
                    {
                        RageApplication.MainApp.AddWidgetView(RageApplication.MainApp.ExeName, new WidgetViewBase.CreateDel(BankTreeView.CreateBankTreeView), true);
                    }

                    this.Cursor = Cursors.Default;

                    //Sensor.CreateSensor( RenderWindow.PlatformString, this );

                    // make sure the game has focus on startup:
                    RageApplication.MainApp.FocusRenderWindow();

                    //RagTrayIcon.Instance.AddCommandLine(RageApplication.MainApp.ExePath + " " + RageApplication.MainApp.Args);

                    // added help options:
                    string[] args = Args.SplitArgs(RageApplication.MainApp.Args);
                    if (args != null)
                    {
                        foreach (string arg in args)
                        {
                            string helpFile = null;
                            if (Args.CheckArg(arg, "-ragHelpFile", ref helpFile))
                            {
                                LoadHelpFileMenu(helpFile);
                                break;
                            }
                        }
                    }

                    break;
                }
                catch (Exception e)
                {
                    --retryCount;

                    if (retryCount == 0)
                    {
                        throw (new Exception("There was an unrecoverable error in LoadApps().", e));
                    }
                    else
                    {
                        _log.ErrorCtx(LogCtx, "****************************************************************");
                        _log.ErrorCtx(LogCtx, "Error:  In LoadApps()");
                        _log.ErrorCtx(LogCtx, "****************************************************************");
                        _log.ErrorCtx(LogCtx, e.ToString());
                        _log.ErrorCtx(LogCtx, "Retrying ({0})", retryCount);
                        _log.ErrorCtx(LogCtx, "****************************************************************");
                    }

                    Thread.Sleep(10);
                }
            }
        }

        /// <summary>
        /// Creates the title string based on the application's arguments.
        /// </summary>
        private String GenerateTitleString(GameConnection connection)
        {
            StringBuilder text = new StringBuilder();
            text.Append("RAG - ");
            text.Append(connection.Platform);
            text.Append(" @ ");
            text.Append(connection.IPAddress);
            if (!String.IsNullOrEmpty(sm_ipAddress))
            {
                text.Append(" (");
                text.Append(sm_ipAddress);
                text.Append(")");
            }
            text.Append(": ");
            text.Append(RageApplication.MainApp.ExePath);
            text.Append(' ');
            text.Append(RageApplication.MainApp.Args);
            return text.ToString();
        }

        private void LoadHelpFileMenu(string fileName)
        {
            if (m_HelpNameToCommandLine.Count > 0)
            {
                int numStandardHelpItems = this.helpToolStripMenuItem.DropDownItems.Count - (m_HelpNameToCommandLine.Count + 1);
                while (this.helpToolStripMenuItem.DropDownItems.Count > numStandardHelpItems)
                {
                    this.helpToolStripMenuItem.DropDownItems.RemoveAt(this.helpToolStripMenuItem.DropDownItems.Count - 1);
                }

                m_HelpNameToCommandLine.Clear();
            }

            List<ToolStripItem> helpItems = new List<ToolStripItem>();
            bool beginGroup = true;

            try
            {
                // open and read help file:
                XmlTextReader xmlReader = new XmlTextReader(fileName);
                xmlReader.WhitespaceHandling = WhitespaceHandling.None;

                while (xmlReader.Read())
                {
                    // 1. parse xml stuff:
                    while (xmlReader.Read())
                    {
                        // read and create render views:
                        if (xmlReader.NodeType == XmlNodeType.Element && xmlReader.Name == "MenuOptions" && xmlReader.IsEmptyElement == false)
                        {
                            xmlReader.Read();
                            while (xmlReader.NodeType != XmlNodeType.EndElement && xmlReader.Name != "MenuOptions")
                            {
                                string visibleName = xmlReader["VisibleName"];
                                string commandLine = xmlReader["CommandLine"];
                                if (visibleName != "" && commandLine != "")
                                {
                                    if (!m_HelpNameToCommandLine.ContainsKey(visibleName))
                                    {
                                        if (beginGroup)
                                        {
                                            ToolStripSeparator separator = new ToolStripSeparator();
                                            helpItems.Add(separator);
                                            beginGroup = false;
                                        }

                                        ToolStripMenuItem item = new ToolStripMenuItem(visibleName, null, new EventHandler(ActivateHelpMenuOption));
                                        helpItems.Add(item);

                                        m_HelpNameToCommandLine.Add(visibleName, commandLine);
                                    }
                                }
                                xmlReader.Read();
                            }
                        }
                    }
                }

                // make sure we actually have help items:
                if (helpItems.Count > 0)
                {
                    this.helpToolStripMenuItem.DropDownItems.AddRange(helpItems.ToArray());
                }

            }
            catch (System.Exception)
            {

            }
        }

        private void DeleteOutputLog(string filename)
        {
            try
            {
                if (File.Exists(filename))
                {
                    File.Delete(filename);
                }
            }
            catch (Exception e)
            {
                rageMessageBox.ShowWarning(this, String.Format("Couldn't delete previous output log file '{0}' because: {1}", filename, e.Message),
                    "Delete Output Log Failed!");
            }
        }

        private void SaveOutputLog(string filename, bool zip, bool silent)
        {
            try
            {
                if (sm_dumpWriter != null)
                {
                    sm_dumpWriter.Close();
                    sm_dumpWriter = null;
                }

                OutputTextBoxInfo otbInfo;
                if (m_outputTextBoxDictionary.TryGetValue(this.OutputRichTextBox.Name, out otbInfo))
                {
                    BeginAddStringToOutput(this, EventArgs.Empty);
                    otbInfo.ProcessPausedMessages();
                    EndAddStringToOutput(this, EventArgs.Empty);
                }

                // now save it
                int retryCount = 10;
                bool success = false;
                do
                {
                    try
                    {
                        this.OutputRichTextBox.SaveFile(filename, RichTextBoxStreamType.PlainText);
                        success = true;
                        break;
                    }
                    catch
                    {
                        --retryCount;
                        Thread.Sleep(100);
                    }
                }
                while (retryCount > 0);

                if (!success && !silent)
                {
                    rageMessageBox.ShowWarning(this, String.Format("Couldn't save output log file '{0}'.", filename),
                        "Save Output Log Failed!");
                }
            }
            catch (Exception e)
            {
                if (silent)
                {
                    _log.ToolExceptionCtx(LogCtx, e, "Couldn't save output log file '{0}'.", filename);
                }
                else
                {
                    string msg = String.Format("Couldn't save output log file '{0}' because {1}", filename, e.Message);
                    rageMessageBox.ShowExclamation(this, msg, "Save Output Log Failed!");
                }

                return;
            }

            if (zip)
            {
                // try to zip it up
                string zipExecutable = Path.GetFullPath(Path.Combine(Path.GetDirectoryName(Application.ExecutablePath), @"..\zip.exe"));
                try
                {
                    if (!File.Exists(zipExecutable))
                    {
                        zipExecutable = Path.GetFullPath(Path.Combine(Path.GetDirectoryName(Application.ExecutablePath), @"zip.exe"));
                        if (!File.Exists(zipExecutable))
                        {
                            if (silent)
                            {
                                _log.ErrorCtx(LogCtx, "Unable to locate zip.exe.");
                            }
                            else
                            {
                                rageMessageBox.ShowExclamation("Unable to locate zip.exe.", "Zip Output File on Exit Failed");
                            }

                            zipExecutable = null;
                        }
                    }
                }
                catch (Exception ex)
                {
                    if (silent)
                    {
                        _log.ToolExceptionCtx(LogCtx, ex, "Unable to locate zip.exe.");
                    }
                    else
                    {
                        string msg = String.Format("Unable to locate zip.exe.\n\n{0}", ex.ToString());
                        rageMessageBox.ShowExclamation(msg, "Zip Output File on Exit Failed");
                    }

                    zipExecutable = null;
                }

                if (zipExecutable != null)
                {
                    string zipFilename = Path.ChangeExtension(filename, ".zip");

                    rageExecuteCommand command = new rageExecuteCommand();

                    command.Arguments = string.Format("\"{0}\" \"{1}\"", zipFilename, filename);
                    command.Command = zipExecutable;
                    command.EchoToConsole = true;
                    command.LogToHtmlFile = false;
                    command.TimeOutInSeconds = 60.0f;
                    command.TimeOutWithoutOutputInSeconds = 60.0f;
                    command.UseBusySpinner = false;

                    _log.ErrorCtx(LogCtx, command.GetDosCommand());

                    rageStatus status;
                    command.Execute(out status);
                    if (!status.Success())
                    {
                        string msg = String.Format("Unable to create zip.\n\n{0}", status.ErrorString);
                        if (silent)
                        {
                            _log.ErrorCtx(LogCtx, msg);
                        }
                        else
                        {
                            rageMessageBox.ShowExclamation(msg, "Zip Output File on Exit Failed");
                        }
                    }
                }
            }
        }

        public static void ClearStatics()
        {
        }

        public void Plugins_Activate()
        {
            HelpPlugins form = new HelpPlugins();
            form.Init(sm_GlobalPrefs);

            if (form.ShowDialog(this) == DialogResult.OK)
            {
                // Save list of disabled plugins
                form.UpdateShouldLoadFlags();
            }
        }

        private void SetTimeString(String str)
        {
            this.framerateToolStripStatusLabel.Text = str;
        }

        private void AddToRecentLayoutList(string layoutFilename)
        {
            this.recentLayoutsToolStripMenuItem.Enabled = true;

            string fullPathName = null;
            try
            {
                fullPathName = Path.GetFullPath(layoutFilename).ToLower();
            }
            catch
            {
                return;
            }

            bool found = false;
            foreach (ToolStripMenuItem item in this.recentLayoutsToolStripMenuItem.DropDownItems)
            {
                if (item.Text == fullPathName)
                {
                    found = true;
                    break;
                }
            }

            if (!found)
            {
                if (this.recentLayoutsToolStripMenuItem.DropDownItems.Count >= MAX_NUM_RECENT_LAYOUTS)
                {
                    this.recentLayoutsToolStripMenuItem.DropDownItems.RemoveAt(0);
                }

                this.recentLayoutsToolStripMenuItem.DropDownItems.Add(fullPathName, null, new EventHandler(RecentLayoutSelect));
            }
        }

        private void RemoveFromRecentLayoutList(string layoutFilename)
        {
            string lowercaseName = layoutFilename.ToLower();
            for (int i = 0; i < this.recentLayoutsToolStripMenuItem.DropDownItems.Count; ++i)
            {
                if (this.recentLayoutsToolStripMenuItem.DropDownItems[i].Text == lowercaseName)
                {
                    this.recentLayoutsToolStripMenuItem.DropDownItems.RemoveAt(i);
                    break;
                }
            }
        }

        public void Quit()
        {
            this.exitRAGToolStripMenuItem_Click(null, null);
        }

        private string ShowOpenDialog(string initialDir)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Title = "Open Layout";
            dialog.Filter = "RAG layout files (.raglayout.*.xml)|.raglayout.*.xml|All files (*.*)|*.*";
            dialog.RestoreDirectory = true;
            dialog.SupportMultiDottedExtensions = true;
            dialog.AddExtension = false;
            dialog.DefaultExt = "xml";
            dialog.InitialDirectory = initialDir;

            DialogResult result = dialog.ShowDialog(this);
            if (result == DialogResult.OK)
            {
                return dialog.FileName;
            }

            return String.Empty;
        }

        static public bool IsSharedFavoritesFile(string searchFavoritesFile)
        {
            if (!String.IsNullOrEmpty(searchFavoritesFile))
            {
                string sharedFavoritesDir = sm_RagSettings.SharedFavoritesDir;
                if (!String.IsNullOrEmpty(sharedFavoritesDir) && Directory.Exists(sharedFavoritesDir))
                {
                    string[] favoritesFiles = Directory.GetFiles(sharedFavoritesDir, "*.xml", SearchOption.AllDirectories);
                    foreach (string favoritesFile in favoritesFiles)
                    {
                        if (favoritesFile.ToLower() == searchFavoritesFile.ToLower())
                            return true;
                    }
                }
            }

            return false;
        }
        #endregion

        #region Overrides
        public const int MSG_REALLY_QUIT = 0xA123;
        protected override void WndProc(ref Message message)
        {
            //filter the RF_TESTMESSAGE
            if (message.Msg == MSG_REALLY_QUIT)
            {
                this.exitRAGToolStripMenuItem_Click(null, null);
            }
            //be sure to pass along all messages to the base also
            base.WndProc(ref message);
        }
        #endregion

        #region External DLL Handling
        private void LoadDlls(string path)
        {
            // If we're in safe mode, don't load any dlls
            if (InSafeMode())
            {
                return;
            }

            // load rag dlls:
            String[] dllNames = null;
            try
            {
                dllNames = Directory.GetFiles(path, "*.dll");
            }
            catch (System.IO.DirectoryNotFoundException)
            {
                return;
            }

            // Build a list of the loaded assemblies so that we don't load them again from the Plugins folder
            Assembly[] loadedAssemblies = AppDomain.CurrentDomain.GetAssemblies();
            List<string> ragDlls = new List<string>();
            foreach (Assembly loadedAssembly in loadedAssemblies.Where(ass => !ass.IsDynamic))
            {
                ragDlls.Add(Path.GetFileNameWithoutExtension(loadedAssembly.Location).ToUpper());
            }

            foreach (String dll in dllNames)
            {
                string dllFile = Path.GetFileNameWithoutExtension(dll);
                if (ragDlls.Contains(dllFile.ToUpper()))
                {
                    _log.MessageCtx(LogCtx, "Skipping {0} because it is already loaded.", dllFile);
                    continue;
                }

                bool alreadyLoaded = false;
                Assembly assembly = LoadDll(dll, out alreadyLoaded);
                if ((assembly != null) && !alreadyLoaded)
                {
                    // Check for special types in the DLL.

                    foreach (Type t in assembly.GetTypes())
                    {
                        if (t.IsAbstract)
                        {
                            continue; // skip abstract classes
                        }
                        // register any IWidgetPlugIn types
                        if (t.GetInterface("ragCore.IWidgetPlugIn") != null)
                        {
                            ragCore.IWidgetPlugIn plugIn = t.GetConstructor(System.Type.EmptyTypes).Invoke(null) as ragCore.IWidgetPlugIn;
                            sm_WidgetPlugIns.Add(plugIn);
                        }

                        if (t.GetInterface("ragWidgets.IViewPlugIn") != null)
                        {
                            IViewPlugIn plugIn = t.GetConstructor(System.Type.EmptyTypes).Invoke(null) as IViewPlugIn;
                            sm_ViewPlugIns.Add(plugIn);
                        }
                    }
                }
            }
        }

        private Assembly LoadDll(string path, out bool alreadyLoaded)
        {
            // check to see if it's already loaded:
            if (sm_DllModules.ContainsKey(path))
            {
                alreadyLoaded = true;
                return sm_DllModules[path];
            }

            alreadyLoaded = false;

            if (!sm_GlobalPrefs.ShouldLoad(path))
            {
                return null;
            }

            _log.MessageCtx(LogCtx, "Loading assembly {0}.", path);

            Assembly assembly = null;
            try
            {
                assembly = Assembly.LoadFrom(path);
            }
            catch (System.Exception)
            {
            }

            if (assembly != null)
            {
                bool hasPluginType = false;
                foreach (Type t in assembly.GetTypes())
                {
                    if ((t.GetInterface("ragCore.IWidgetPlugIn") != null ||
                         t.GetInterface("ragWidgets.IViewPlugIn") != null) &&
                         !t.IsAbstract)
                    {
                        hasPluginType = true;
                        break;
                    }
                }

                if (!hasPluginType)
                {
                    return null;
                }

                // make sure it's a plugin DLL, otherwise unload it
                sm_DllModules.Add(path, assembly);
                sm_GlobalPrefs.LoadedDll(path);

                return assembly;
            }

            return null;
        }

        private void InitPlugIns()
        {
            foreach (IWidgetPlugIn plugin in sm_WidgetPlugIns)
            {
                string pluginName = (plugin.GetType().Assembly.GetName().Name) + ".dll";
                if (!sm_InitializedWidgetPlugInsMap.ContainsKey(pluginName))
                {
                    _log.MessageCtx(LogCtx, "Initializing plugin {0}", pluginName);

                    plugin.AddHandlers(RageApplication.PacketProcessor);

                    sm_InitializedWidgetPlugInsMap.Add(pluginName, plugin);
                }
            }

            foreach (IViewPlugIn plugin in sm_ViewPlugIns)
            {
                string pluginName = (plugin.GetType().Assembly.GetName().Name) + ".dll";
                if (!sm_InitializedViewPlugInsMap.ContainsKey(pluginName))
                {


                    // build the plugin host data
                    PlugInHostData data = new PlugInHostData(RageApplication.MainApp, RageApplication.Applications,
                        DockableViewManager.Instance, this.pluginsToolStripMenuItem, this.menuStrip1, sm_GlobalPrefs.FindInfo(pluginName),
                        RenderWindow.PlatformString, RagTrayIcon.Instance.ConnectedToProxy, RageApplication.MainApp.KeyProcessor,
                        _log);

                    _log.MessageCtx(LogCtx, "Initializing plugin {0}", pluginName);

                    plugin.InitPlugIn(data);

                    sm_InitializedViewPlugInsMap.Add(pluginName, plugin);
                }
            }
        }

        private void ShutdownPlugIns()
        {
            foreach (IViewPlugIn plugin in sm_ViewPlugIns)
            {
                try
                {
                    string pluginName = (plugin.GetType().Assembly.GetName().Name) + ".dll";
                    _log.MessageCtx(LogCtx, "Shutting down plugin {0}", pluginName);

                    plugin.RemovePlugIn();

                    sm_InitializedViewPlugInsMap.Remove(pluginName);
                }
                catch (Exception)
                {
                    // do nothing?
                }
            }

            foreach (IWidgetPlugIn plugin in sm_WidgetPlugIns)
            {
                try
                {
                    string pluginName = (plugin.GetType().Assembly.GetName().Name) + ".dll";
                    _log.MessageCtx(LogCtx, "Shutting down plugin {0}", pluginName);

                    plugin.RemoveHandlers(RageApplication.PacketProcessor);

                    sm_InitializedWidgetPlugInsMap.Remove(pluginName);
                }
                catch (Exception)
                {
                    // do nothing?
                }
            }
        }

        private void ClearPlugIns()
        {
            sm_ViewPlugIns.Clear();
            sm_InitializedViewPlugInsMap.Clear();

            sm_WidgetPlugIns.Clear();
            sm_InitializedWidgetPlugInsMap.Clear();
        }
        #endregion

        #region Widgets
        protected void WidgetVisible_AddContextMenuItems(IWidgetView currentWidgetView, ContextMenuStrip menu, Widget widget, WidgetVisible widgetVisible)
        {
            FavoritesView.AddContextMenuItems(currentWidgetView, menu, widget, widgetVisible);
            FindView.AddContextMenuItems(currentWidgetView, menu, widget, widgetVisible);

        }

        protected void WidgetVisible_CreateWidgetSingle(Widget widget)
        {
            CreateWidgetSingle(widget, System.Guid.NewGuid());
        }

        protected void CreateWidgetSingle(Widget widget, System.Guid guid)
        {
            WidgetViewBase.CreateDel del = new WidgetViewBase.CreateDel(WidgetViewSingle.CreateWidgetViewSingle);
            WidgetViewSingle view = RageApplication.MainApp.AddWidgetView(RageApplication.MainApp.ExeName, guid, del, false) as WidgetViewSingle;
            view.AddWidget(widget);
        }
        #endregion

        #region Output
        /// <summary>
        /// Called before text is added to any of the output windows.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BeginAddStringToOutput(object sender, EventArgs e)
        {
            m_outputTextBoxMutex.WaitOne();

            foreach (OutputTextBoxInfo info in m_outputTextBoxDictionary.Values)
            {
                info.BeginAddText();
            }
        }

        /// <summary>
        /// Gets called whenever any output is received from the game
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AddStringToOutput(object sender, RageApplication.OutputTextEventArgs e)
        {
            // Early out in case the string is empty
            if (e.Output.Text.Length == 0)
            {
                return;
            }

            // Add the output to the various output windows
            AddStringToAllOutput(e.Output);
            AddStringToSeverityOutput(e.Output);
            AddStringToUserOutputs(e.Output);

            // Check whether we should dump out the output.
            if (sm_DumpOutputInRealTime && (sm_dumpWriter != null))
            {
                sm_dumpWriter.Write(e.Output.Text.Replace("\n", Environment.NewLine));
                sm_dumpWriter.Flush();
            }
        }

        /// <summary>
        /// Adds a single line of text to one of the severity based output windows.
        /// </summary>
        /// <param name="output"></param>
        private void AddStringToSeverityOutput(RageApplication.TTYLine output)
        {
            // Determine the name of the rich text box that this information should be output to.
            string severityRichTextBoxName = this.DisplayRichTextBox.Name;
            if (output.TextSeverity == RageApplication.TTYLine.Severity.Error ||
                output.TextSeverity == RageApplication.TTYLine.Severity.FatalError)
            {
                severityRichTextBoxName = this.ErrorRichTextBox.Name;
            }
            else if (output.TextSeverity == RageApplication.TTYLine.Severity.Warning)
            {
                severityRichTextBoxName = this.WarningRichTextBox.Name;
            }

            // Retrieve the appropriate output info from our dictionary and append the text.
            OutputTextBoxInfo otbInfo = m_outputTextBoxDictionary[severityRichTextBoxName];
            otbInfo.AppendText(output.Text);
        }

        /// <summary>
        /// Adds a single line of TTY output to the output window that contains all TTY spew.
        /// </summary>
        /// <param name="output"></param>
        private void AddStringToAllOutput(RageApplication.TTYLine output)
        {
            OutputTextBoxInfo otbInfo = m_outputTextBoxDictionary[this.OutputRichTextBox.Name];
            otbInfo.AppendRTFText(output.Text, output.ForeColor, output.BackColor);
        }

        /// <summary>
        /// Adds a single line of TTY output to all user outputs that the channel is a part of.
        /// </summary>
        /// <param name="output"></param>
        private void AddStringToUserOutputs(RageApplication.TTYLine output)
        {
            if (!String.IsNullOrEmpty(output.ChannelName))
            {
                // Add the text to all the output windows that need it.
                foreach (OutputTextBoxInfo otbInfo in GetOutputInfosForChannel(output.ChannelName))
                {
                    otbInfo.AppendText(output.Text);
                }
            }
        }

        /// <summary>
        /// Goes over all the output windows checking whether this one should be included as part of it (based on the channels and sub-channels that are registered).
        /// </summary>
        /// <param name="channelName"></param>
        /// <returns></returns>
        private IEnumerable<OutputTextBoxInfo> GetOutputInfosForChannel(string channelName)
        {
            foreach (OutputTextBoxInfo info in m_outputTextBoxDictionary.Values)
            {
                if (info.ChannelName == channelName || info.SubChannelNames.Contains(channelName))
                {
                    yield return info;
                }
            }
        }

        /// <summary>
        /// Called after text has been added to output windows.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EndAddStringToOutput(object sender, EventArgs e)
        {
            foreach (OutputTextBoxInfo info in m_outputTextBoxDictionary.Values)
            {
                info.EndAddText();
            }

            m_outputTextBoxMutex.ReleaseMutex();
        }

        /// <summary>
        /// Called to create a new output window.
        /// </summary>
        /// <param name="channelName"></param>
        /// <param name="color"></param>
        /// <param name="subChannelNames"></param>
        /// <param name="guid"></param>
        /// <param name="strCloseAction"></param>
        /// <returns></returns>
        public DockControl CreateOutputWindow(string channelName, string color, Guid guid, string strCloseAction)
        {
            OutputTextBoxInfo otbInfo;
            if (m_outputTextBoxDictionary.TryGetValue(channelName + "RichTextBox", out otbInfo))
            {
                if (!otbInfo.Window.IsOpen)
                {
                    otbInfo.Window.Open(WindowOpenMethod.OnScreenActivate);
                }
                return otbInfo.Window;
            }

            MultiColorInfo mcInfo = null;
            if (!MainWindow.MultiColorOutputInfo.TryGetValue(channelName, out mcInfo))
            {
                mcInfo = new MultiColorInfo("multiColor" + channelName, Widget.DeserializeColor(color));
                MainWindow.MultiColorOutputInfo.Add(channelName, mcInfo);
            }
            else
            {
                mcInfo.DefaultColor = Widget.DeserializeColor(color);
            }

            // create the text box
            SuspendableRichTextBox rtb = new SuspendableRichTextBox();
            rtb.BackColor = System.Drawing.SystemColors.Window;
            rtb.ContextMenuStrip = this.richTextBoxesContextMenuStrip;
            rtb.Dock = System.Windows.Forms.DockStyle.Fill;
            rtb.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            rtb.ForeColor = mcInfo.DefaultColor;
            rtb.HideSelection = false;
            rtb.Location = new System.Drawing.Point(0, 0);
            rtb.Name = channelName + "RichTextBox";
            rtb.ReadOnly = true;
            rtb.Size = new System.Drawing.Size(1228, 210);
            rtb.TabIndex = 0;
            rtb.Text = "";
            rtb.MouseEnter += new System.EventHandler(this.richTextBoxes_MouseEnter);

            DockableWindow window = new DockableWindow(this.sandDockManager1, rtb, channelName);
            window.AllowClose = true;
            window.DockingRules = new DockingRules(true, true, true);
            window.CloseAction = String.IsNullOrEmpty(strCloseAction)
                ? DockControlCloseAction.HideOnly
                : (TD.SandDock.DockControlCloseAction)Enum.Parse(typeof(TD.SandDock.DockControlCloseAction), strCloseAction);
            window.Guid = guid;
            window.Location = new System.Drawing.Point(0, 20);
            window.Name = channelName + "DockableWindow";
            window.PersistState = true;
            window.ShowOptions = true;
            window.Size = new System.Drawing.Size(1228, 210);
            window.TabImage = global::rag.Properties.Resources.output;
            window.TabIndex = 0;
            window.Text = channelName;
            window.Closed += new EventHandler(outputWindow_Closed);

            otbInfo = new OutputTextBoxInfo(channelName, mcInfo, rtb, window);
            if (m_subChannelMappings.ContainsKey(channelName))
            {
                otbInfo.SubChannelNames.AddRange(m_subChannelMappings[channelName]);
            }
            m_outputTextBoxDictionary.Add(rtb.Name, otbInfo);

            if (DockableViewManager.Instance.OpenView(window, false, TD.SandDock.ContainerDockLocation.Bottom, _log))
            {
                m_userCreatedOutputWindows.Add(window);
            }

            return window;
        }

        private void outputWindow_Closed(object sender, EventArgs e)
        {
            DockControl window = sender as DockControl;
            if (sender != null)
            {
                if (window.CloseAction == DockControlCloseAction.Dispose)
                {
                    string windowName = window.Text.Replace(" (Paused)", "");
                    string otbName = String.Format("{0}RichTextBox", windowName);

                    OutputTextBoxInfo otbInfo;
                    if (m_outputTextBoxDictionary.TryGetValue(otbName, out otbInfo))
                    {
                        ClearOutputWindow(otbInfo);
                    }

                    m_outputTextBoxDictionary.Remove(otbName);
                    MainWindow.MultiColorOutputInfo.Remove(windowName);

                    m_userCreatedOutputWindows.Remove(window);
                }
            }
        }

        public DockControl CreateOutputWindow(string channelName, string color)
        {
            return CreateOutputWindow(channelName, color, Guid.NewGuid(), string.Empty);
        }

        public DockControl CreateOutputWindow(string encodedMessage)
        {
            int indexOf = encodedMessage.LastIndexOf("NamedColor");
            if (indexOf == -1)
            {
                indexOf = encodedMessage.LastIndexOf("ARGBColor");
            }

            if (indexOf > -1)
            {
                string channelName = encodedMessage.Substring(0, indexOf);
                string color = encodedMessage.Substring(indexOf);
                return CreateOutputWindow(channelName, color);
            }

            return null;
        }

        private void ClearOutputWindow(OutputTextBoxInfo otbInfo)
        {
            // clear these out to try to prevent System.OutOfMemoryException when disposing of the RichTextBox
            otbInfo.PauseMessages.Clear();
            otbInfo.VisibleTextBox.Clear();
        }

        private void ClearAllOutputWindows()
        {
            foreach (OutputTextBoxInfo otbInfo in m_outputTextBoxDictionary.Values)
            {
                ClearOutputWindow(otbInfo);
            }
        }

        private void CloseAllUserCreatedOutputWindows()
        {
            while (m_userCreatedOutputWindows.Count > 0)
            {
                m_userCreatedOutputWindows[0].CloseAction = DockControlCloseAction.Dispose;
                m_userCreatedOutputWindows[0].Close();
            }
        }

        private void AddSubChannelInfo(string channelName, string subChannelName)
        {
            OutputTextBoxInfo info = m_outputTextBoxDictionary.Values.FirstOrDefault(item => item.ChannelName == channelName);

            // If the mapping doesn't contain the channel, we need to add it and add any sub channels that the
            // child (sub) channel has.
            if (!m_subChannelMappings.ContainsKey(channelName))
            {
                m_subChannelMappings.Add(channelName, new HashSet<string>());

                if (m_subChannelMappings.ContainsKey(subChannelName))
                {
                    m_subChannelMappings[channelName].AddRange(m_subChannelMappings[subChannelName]);

                    // Cache it in the info object
                    if (info != null)
                    {
                        info.SubChannelNames.AddRange(m_subChannelMappings[subChannelName]);
                    }
                }
            }

            // Add the sub channel
            m_subChannelMappings[channelName].Add(subChannelName);

            // Check if anything includes this channel and add it to that as well.
            foreach (string parentChannel in m_subChannelMappings.Where(item => item.Value.Contains(channelName)).Select(item => item.Key))
            {
                AddSubChannelInfo(parentChannel, subChannelName);
            }

            // Cache it in the info object
            if (info != null)
            {
                info.SubChannelNames.Add(subChannelName);
            }
        }
        #endregion

        #region Exception Handling
        public void HandleException_EH(object sender, ThreadExceptionEventArgs e)
        {
            HandleException(e.Exception);
        }

        public void HandleException(Exception e)
        {
            this.updateTimer.Enabled = false;
            bool showMessage = false;
            try
            {
                List<string> attachments = new List<string>();

                if (RagTrayIcon.Instance != null
                    && File.Exists(LogFactory.ApplicationLogFilename))
                {
                    attachments.Add(LogFactory.ApplicationLogFilename);
                }

                string userConfigFilename = rageFileUtilities.GetRageApplicationUserConfigFilename("ragApp.exe");
                if (!String.IsNullOrEmpty(userConfigFilename) && File.Exists(userConfigFilename))
                {
                    attachments.Add(userConfigFilename);
                }

                try
                {
                    string outputFilename = Path.Combine(Environment.GetEnvironmentVariable("TEMP"),
                    String.Format("rag\\{0}.txt", rageFileUtilities.GenerateTimeBasedFilename()));

                    SaveOutputLog(outputFilename, false, true);

                    if (File.Exists(outputFilename))
                    {
                        attachments.Add(outputFilename);
                    }
                }
                catch
                {

                }

                UnhandledExceptionDialog dialog = new UnhandledExceptionDialog("Rag", attachments,
                    Settings.Default.ExceptionToEmailAdress, e);

                dialog.ShowDialog(this);

                // The Restart Handler, if it exists, will call these at the appropriate time in the right order.
                // If were to call these 2 functions AND the Restart Handler, our layout and preference files don't get saved correctly.
                if (sm_RestartHandler == null)
                {
                    ShutdownPlugIns();
                    ClearPlugIns();

                    _log.MessageCtx(LogCtx, "HandleException calling StopAppsFinal");

                    RageApplication.StopAppsFinal(_log);
                }
            }
            catch (Exception newE)
            {
                rageMessageBox.ShowError(this,
                    String.Format("RAG failed to stop the running application because: '{0}'\n\nYou will probably need to kill the application from the task manager.\n\nRAG will be restarted.", newE.Message),
                    "Stopping Applications Failed!");
                showMessage = false;
            }

            if (sm_RestartHandler != null)
            {
                sm_RestartHandler(e, showMessage);
            }
        }
        #endregion

        #region Unused
        private void PipeReadWriteFailure()
        {
            rageMessageBox.ShowError(this, "Read/write communication failure with application.", "Read/write Failure");

            if (!String.IsNullOrEmpty(m_currentLayoutFilename))
            {
                SaveLayout(m_currentLayoutFilename, m_ExePathName);
            }
            else
            {
                SaveLayout(m_defaultLayoutFilename, m_ExePathName);
            }

            DockableViewManager.Instance.CloseAllViews();
            CloseAllUserCreatedOutputWindows();
            m_currentLayoutFilename = null;
            m_defaultLayoutFilename = null;
            m_ExePathName = null;
        }

        /// <summary>
        /// Handles the events of StdErrReceived and StdOutReceived.
        /// </summary>
        /// <remarks>
        /// If stderr were handled in a separate function, it could possibly
        /// be displayed in red in the richText box, but that is beyond
        /// the scope of this demo.
        /// </remarks>
        private void WriteStreamInfo(object sender, DataReceivedEventArgs e)
        {
            // make sure we don't get in here if the main window was destroyed:
            if (IsHandleCreated == false)
                return;

            ProcessCaller.m_Mutex.WaitOne();

            if (ProcessCaller.m_StringBufferOutput.Length > 0)
            {
                this.OutputRichTextBox.AppendText(ProcessCaller.m_StringBufferOutput);
                this.OutputRichTextBox.LimitTextLength();
                ProcessCaller.m_StringBufferOutput = "";
            }
            if (ProcessCaller.m_StringBufferError.Length > 0)
            {
                this.ErrorRichTextBox.AppendText(ProcessCaller.m_StringBufferError);
                this.ErrorRichTextBox.LimitTextLength();
                ProcessCaller.m_StringBufferError = "";
                this.lastErrorToolStripStatusLabel.Text = this.ErrorRichTextBox.Lines[this.ErrorRichTextBox.Lines.Length - 2];
            }
            if (ProcessCaller.m_StringBufferWarning.Length > 0)
            {
                this.WarningRichTextBox.AppendText(ProcessCaller.m_StringBufferWarning);
                this.WarningRichTextBox.LimitTextLength();
                ProcessCaller.m_StringBufferWarning = "";
            }

            ProcessCaller.m_Mutex.ReleaseMutex();
        }

        /// <summary>
        /// Handles the events of processCompleted & processCanceled
        /// </summary>
        private void ProcessCompletedOrCanceled(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Default;
        }
        #endregion

        #region DockControl Menus
        public static ToolStripMenuItem CreateMenuItemForDockControl(DockControl ctrl)
        {
            ToolStripMenuItem menuItem = new ToolStripMenuItem(ctrl.Text.Replace(" (Paused)", ""));
            menuItem.DropDownOpening += new EventHandler(toolStripMenuItem_DropDownOpening);
            menuItem.Tag = ctrl;

            ToolStripMenuItem subItem = new ToolStripMenuItem("&Show");
            subItem.Click += new EventHandler(showToolStripMenuItem_Click);
            subItem.Tag = ctrl;
            menuItem.DropDownItems.Add(subItem);

            subItem = new ToolStripMenuItem("&Center");
            subItem.Click += new EventHandler(centerToolStripMenuItem_Click);
            subItem.Tag = ctrl;
            menuItem.DropDownItems.Add(subItem);

            subItem = new ToolStripMenuItem("D&ock");
            subItem.Click += new EventHandler(dockToolStripMenuItem_Click);
            subItem.Tag = ctrl;
            menuItem.DropDownItems.Add(subItem);

            subItem = new ToolStripMenuItem("&Float");
            subItem.Click += new EventHandler(floatToolStripMenuItem_Click);
            subItem.Tag = ctrl;
            menuItem.DropDownItems.Add(subItem);

            if (ctrl.ShowOptions)
            {
                menuItem.DropDownItems.Add(new ToolStripSeparator());

                subItem = new ToolStripMenuItem("&Dispose when closed");
                subItem.CheckOnClick = true;
                subItem.Click += new EventHandler(disposeToolStripMenuItem_Click);
                subItem.Tag = ctrl;
                menuItem.DropDownItems.Add(subItem);

                subItem = new ToolStripMenuItem("&Hide when closed");
                subItem.CheckOnClick = true;
                subItem.Click += new EventHandler(hideToolStripMenuItem_Click);
                subItem.Tag = ctrl;
                menuItem.DropDownItems.Add(subItem);
            }

            return menuItem;
        }

        private static void toolStripMenuItem_DropDownOpening(object sender, EventArgs e)
        {
            if (sender is ToolStripMenuItem)
            {
                ToolStripMenuItem menuItem = sender as ToolStripMenuItem;

                DockControl window = menuItem.Tag as DockControl;
                if ((window != null) && window.ShowOptions && (menuItem.DropDownItems.Count >= 7))
                {
                    (menuItem.DropDownItems[5] as ToolStripMenuItem).Checked = window.CloseAction == DockControlCloseAction.Dispose;
                    (menuItem.DropDownItems[6] as ToolStripMenuItem).Checked = window.CloseAction == DockControlCloseAction.HideOnly;
                }
            }
        }

        private static void showToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (sender is ToolStripMenuItem)
            {
                DockControl window = (sender as ToolStripMenuItem).Tag as DockControl;
                if (window != null)
                {
                    window.Open(WindowOpenMethod.OnScreenActivate);
                }
            }
        }

        private static void centerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (sender is ToolStripMenuItem)
            {
                DockControl window = (sender as ToolStripMenuItem).Tag as DockControl;
                if (window != null)
                {
                    if (window.DockSituation == DockSituation.Floating)
                    {
                        if (RagTrayIcon.Instance.MainWindow != null)
                        {
                            Rectangle rect = RagTrayIcon.Instance.MainWindow.Bounds;

                            int x = (rect.Width / 2) + rect.Left - (window.FloatingSize.Width / 2);
                            int y = (rect.Height / 2) + rect.Top - (window.FloatingSize.Height / 2);

                            window.FloatingLocation = new Point(x, y);
                        }
                    }
                }
            }
        }

        private static void dockToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (sender is ToolStripMenuItem)
            {
                DockControl window = (sender as ToolStripMenuItem).Tag as DockControl;
                if (window != null)
                {
                    window.OpenDocked();
                }
            }
        }

        private static void floatToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (sender is ToolStripMenuItem)
            {
                DockControl window = (sender as ToolStripMenuItem).Tag as DockControl;
                if (window != null)
                {
                    window.OpenFloating();
                }
            }
        }

        private static void disposeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (sender is ToolStripMenuItem)
            {
                DockControl window = (sender as ToolStripMenuItem).Tag as DockControl;
                if (window != null)
                {
                    window.CloseAction = DockControlCloseAction.Dispose;
                }
            }
        }

        private static void hideToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (sender is ToolStripMenuItem)
            {
                DockControl window = (sender as ToolStripMenuItem).Tag as DockControl;
                if (window != null)
                {
                    window.CloseAction = DockControlCloseAction.HideOnly;
                }
            }
        }
        #endregion

        private void OutputRichTextBox_LinkClicked(object sender, LinkClickedEventArgs e)
        {
            string strLinkText = e.LinkText;
            System.Diagnostics.Process.Start(strLinkText);
        }

        private void enableShiftNumPadOverrideToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Settings.Default.NumPadOverride = !Settings.Default.NumPadOverride;
            enableNumPadOverrideToolStripMenuItem.Checked = Settings.Default.NumPadOverride;
        }
    }
}
