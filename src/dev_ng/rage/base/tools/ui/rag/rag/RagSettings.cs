using System;
using System.Xml.Serialization;

namespace rag
{

    [XmlRoot( ElementName = "RagSettings" )]
    public class RagSettings
    {
        public RagSettings()
        {

        }

        #region Variables
        private string m_SharedLayoutsDir = String.Empty;
        private string m_SharedFavoritesDir = String.Empty;
        #endregion

        #region Properties
        public string SharedLayoutsDir
        {
            get
            {
                return m_SharedLayoutsDir;
            }
            set
            {
                m_SharedLayoutsDir = value;
            }
        }

        public string SharedFavoritesDir
        {
            get
            {
                return m_SharedFavoritesDir;
            }
            set
            {
                m_SharedFavoritesDir = value;
            }
        }
        #endregion
    }

}
