namespace rag
{
    partial class FindView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && (components != null) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.findWhatGroupBox = new System.Windows.Forms.GroupBox();
            this.includeGroupsCheckBox = new System.Windows.Forms.CheckBox();
            this.searchInHelpDescriptionsCheckBox = new System.Windows.Forms.CheckBox();
            this.useRegularExpressionsCheckBox = new System.Windows.Forms.CheckBox();
            this.matchWholeWordCheckBox = new System.Windows.Forms.CheckBox();
            this.matchCaseCheckBox = new System.Windows.Forms.CheckBox();
            this.findWhatComboBox = new System.Windows.Forms.ComboBox();
            this.findButton = new System.Windows.Forms.Button();
            this.displayPathCheckBox = new System.Windows.Forms.CheckBox();
            this.exactMatchCheckBox = new System.Windows.Forms.CheckBox();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.findWhatGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add( this.displayPathCheckBox );
            this.splitContainer1.Panel1.Controls.Add( this.findButton );
            this.splitContainer1.Panel1.Controls.Add( this.findWhatGroupBox );
            // 
            // splitContainer2
            // 
            this.splitContainer2.Size = new System.Drawing.Size( 361, 541 );
            // 
            // findWhatGroupBox
            // 
            this.findWhatGroupBox.Anchor = ( (System.Windows.Forms.AnchorStyles)( ( ( System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left )
                        | System.Windows.Forms.AnchorStyles.Right ) ) );
            this.findWhatGroupBox.Controls.Add( this.exactMatchCheckBox );
            this.findWhatGroupBox.Controls.Add( this.includeGroupsCheckBox );
            this.findWhatGroupBox.Controls.Add( this.searchInHelpDescriptionsCheckBox );
            this.findWhatGroupBox.Controls.Add( this.useRegularExpressionsCheckBox );
            this.findWhatGroupBox.Controls.Add( this.matchWholeWordCheckBox );
            this.findWhatGroupBox.Controls.Add( this.matchCaseCheckBox );
            this.findWhatGroupBox.Controls.Add( this.findWhatComboBox );
            this.findWhatGroupBox.Location = new System.Drawing.Point( 3, 3 );
            this.findWhatGroupBox.Name = "findWhatGroupBox";
            this.findWhatGroupBox.Size = new System.Drawing.Size( 173, 181 );
            this.findWhatGroupBox.TabIndex = 0;
            this.findWhatGroupBox.TabStop = false;
            this.findWhatGroupBox.Text = "Find What";
            // 
            // includeGroupsCheckBox
            // 
            this.includeGroupsCheckBox.AutoSize = true;
            this.includeGroupsCheckBox.Location = new System.Drawing.Point( 6, 161 );
            this.includeGroupsCheckBox.Name = "includeGroupsCheckBox";
            this.includeGroupsCheckBox.Size = new System.Drawing.Size( 96, 17 );
            this.includeGroupsCheckBox.TabIndex = 5;
            this.includeGroupsCheckBox.Text = "Include groups";
            this.includeGroupsCheckBox.UseVisualStyleBackColor = true;
            // 
            // searchInHelpDescriptionsCheckBox
            // 
            this.searchInHelpDescriptionsCheckBox.AutoSize = true;
            this.searchInHelpDescriptionsCheckBox.Location = new System.Drawing.Point( 6, 138 );
            this.searchInHelpDescriptionsCheckBox.Name = "searchInHelpDescriptionsCheckBox";
            this.searchInHelpDescriptionsCheckBox.Size = new System.Drawing.Size( 153, 17 );
            this.searchInHelpDescriptionsCheckBox.TabIndex = 4;
            this.searchInHelpDescriptionsCheckBox.Text = "Search in help descriptions";
            this.searchInHelpDescriptionsCheckBox.UseVisualStyleBackColor = true;
            // 
            // useRegularExpressionsCheckBox
            // 
            this.useRegularExpressionsCheckBox.AutoSize = true;
            this.useRegularExpressionsCheckBox.Location = new System.Drawing.Point( 6, 115 );
            this.useRegularExpressionsCheckBox.Name = "useRegularExpressionsCheckBox";
            this.useRegularExpressionsCheckBox.Size = new System.Drawing.Size( 138, 17 );
            this.useRegularExpressionsCheckBox.TabIndex = 3;
            this.useRegularExpressionsCheckBox.Text = "Use regular expressions";
            this.useRegularExpressionsCheckBox.UseVisualStyleBackColor = true;
            // 
            // matchWholeWordCheckBox
            // 
            this.matchWholeWordCheckBox.AutoSize = true;
            this.matchWholeWordCheckBox.Location = new System.Drawing.Point( 6, 69 );
            this.matchWholeWordCheckBox.Name = "matchWholeWordCheckBox";
            this.matchWholeWordCheckBox.Size = new System.Drawing.Size( 113, 17 );
            this.matchWholeWordCheckBox.TabIndex = 2;
            this.matchWholeWordCheckBox.Text = "Match whole word";
            this.matchWholeWordCheckBox.UseVisualStyleBackColor = true;
            // 
            // matchCaseCheckBox
            // 
            this.matchCaseCheckBox.AutoSize = true;
            this.matchCaseCheckBox.Location = new System.Drawing.Point( 6, 46 );
            this.matchCaseCheckBox.Name = "matchCaseCheckBox";
            this.matchCaseCheckBox.Size = new System.Drawing.Size( 82, 17 );
            this.matchCaseCheckBox.TabIndex = 1;
            this.matchCaseCheckBox.Text = "Match case";
            this.matchCaseCheckBox.UseVisualStyleBackColor = true;
            // 
            // findWhatComboBox
            // 
            this.findWhatComboBox.Anchor = ( (System.Windows.Forms.AnchorStyles)( ( ( System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left )
                        | System.Windows.Forms.AnchorStyles.Right ) ) );
            this.findWhatComboBox.FormattingEnabled = true;
            this.findWhatComboBox.Location = new System.Drawing.Point( 6, 19 );
            this.findWhatComboBox.Name = "findWhatComboBox";
            this.findWhatComboBox.Size = new System.Drawing.Size( 161, 21 );
            this.findWhatComboBox.TabIndex = 0;
            this.findWhatComboBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler( this.findWhatComboBox_KeyPress );
            this.findWhatComboBox.SelectedValueChanged += new System.EventHandler( this.findWhatComboBox_SelectedValueChanged );
            // 
            // findButton
            // 
            this.findButton.Location = new System.Drawing.Point( 3, 190 );
            this.findButton.Name = "findButton";
            this.findButton.Size = new System.Drawing.Size( 75, 23 );
            this.findButton.TabIndex = 1;
            this.findButton.Text = "&Find";
            this.findButton.UseVisualStyleBackColor = true;
            this.findButton.Click += new System.EventHandler( this.findButton_Click );
            // 
            // displayPathCheckBox
            // 
            this.displayPathCheckBox.AutoSize = true;
            this.displayPathCheckBox.Checked = true;
            this.displayPathCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.displayPathCheckBox.Location = new System.Drawing.Point( 3, 219 );
            this.displayPathCheckBox.Name = "displayPathCheckBox";
            this.displayPathCheckBox.Size = new System.Drawing.Size( 85, 17 );
            this.displayPathCheckBox.TabIndex = 2;
            this.displayPathCheckBox.Text = "Display Path";
            this.displayPathCheckBox.UseVisualStyleBackColor = true;
            this.displayPathCheckBox.CheckedChanged += new System.EventHandler( this.displayPathCheckBox_CheckedChanged );
            // 
            // exactMatchCheckBox
            // 
            this.exactMatchCheckBox.AutoSize = true;
            this.exactMatchCheckBox.Location = new System.Drawing.Point( 6, 92 );
            this.exactMatchCheckBox.Name = "exactMatchCheckBox";
            this.exactMatchCheckBox.Size = new System.Drawing.Size( 85, 17 );
            this.exactMatchCheckBox.TabIndex = 3;
            this.exactMatchCheckBox.Text = "Exact match";
            this.exactMatchCheckBox.UseVisualStyleBackColor = true;
            // 
            // FindView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
            this.Name = "FindView";
            this.Controls.SetChildIndex( this.splitContainer1, 0 );
            this.splitContainer1.Panel1.ResumeLayout( false );
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout( false );
            this.splitContainer1.ResumeLayout( false );
            this.splitContainer2.Panel1.ResumeLayout( false );
            this.splitContainer2.ResumeLayout( false );
            this.findWhatGroupBox.ResumeLayout( false );
            this.findWhatGroupBox.PerformLayout();
            this.ResumeLayout( false );

        }

        #endregion

        private System.Windows.Forms.GroupBox findWhatGroupBox;
        private System.Windows.Forms.CheckBox searchInHelpDescriptionsCheckBox;
        private System.Windows.Forms.CheckBox useRegularExpressionsCheckBox;
        private System.Windows.Forms.CheckBox matchWholeWordCheckBox;
        private System.Windows.Forms.CheckBox matchCaseCheckBox;
        private System.Windows.Forms.ComboBox findWhatComboBox;
        private System.Windows.Forms.CheckBox includeGroupsCheckBox;
        private System.Windows.Forms.Button findButton;
        private System.Windows.Forms.CheckBox displayPathCheckBox;
        private System.Windows.Forms.CheckBox exactMatchCheckBox;
    }
}
