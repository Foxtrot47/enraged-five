using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using System.Xml;

namespace rag
{
	/// <summary>
	/// Summary description for WidgetViewForm.
	/// </summary>
	public class WidgetViewForm : WidgetViewBase
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public WidgetViewForm()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			// TODO: Add any initialization after the InitializeComponent call

		}

		public WidgetViewForm(String appName,BankManager bankMgr,TD.SandDock.DockControl dockControl) : base(appName,bankMgr,dockControl)
		{
		}

		public static WidgetViewBase CreateWidgetViewForm(String appName,BankManager bankMgr,TD.SandDock.DockControl dockControl)
		{
			WidgetViewForm formView = new WidgetViewForm(appName,bankMgr,dockControl);
			return formView;
		}
		public override bool DisplayPath
		{
			get {return false;}
		}

		public override bool CanSerialize
		{
			get {return false;}
		}

		protected override void ClearView()
		{
		}

		public override void DeserializeView(XmlTextReader xmlReader,BankManager bankMgr)
		{
		}

		protected override void SerializeView(XmlTextWriter xmlWriter,String exePathName) 
		{
		}

		public void Init(Control control)
		{
			Controls.Add(control);
			control.SetBounds(0, 0, Width, Height);
			control.Dock = DockStyle.Fill;
		}


		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			// 
			// WidgetViewForm
			// 
			this.Name = "WidgetViewForm";
			this.Size = new System.Drawing.Size(440, 424);

		}
		#endregion
	}
}
