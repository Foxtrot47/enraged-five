using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

using RSG.Base.IO;
using RSG.Base.Logging;

namespace rag
{
	/// <summary>
	/// Summary description for GlobalPrefs.
	/// </summary>

	[XmlRoot(ElementName = "RagGlobalPreferences")]
	public class GlobalPrefs
	{
		public GlobalPrefs()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public ragCore.DllInfo[] KnownDlls
		{
			get
            {
				ragCore.DllInfo[] infos = new ragCore.DllInfo[m_KnownDlls.Count];
				m_KnownDlls.Values.CopyTo(infos, 0);
				return infos;
			}
			set
            {
				m_KnownDlls.Clear();
				foreach(ragCore.DllInfo d in value)
                {
					m_KnownDlls.Add(d.Name, d);
				}
			}
		}

		public bool ShouldLoad(string name)
		{
			string key = System.IO.Path.GetFileName(name);
			return !m_KnownDlls.ContainsKey(key) || m_KnownDlls[key].ShouldLoad;
		}

		public bool IsLoaded(string name)
		{
			string key = System.IO.Path.GetFileName(name);
			return m_KnownDlls.ContainsKey(key) && m_KnownDlls[key].Loaded;
		}

		public void LoadedDll(string fullname)
		{
			string key = System.IO.Path.GetFileName(fullname);
			ragCore.DllInfo info;
			if (m_KnownDlls.ContainsKey(key))
            {
				info = m_KnownDlls[key];
			}
			else
            {
				info = new ragCore.DllInfo();
				info.Name = key;
				m_KnownDlls.Add(key, info);
			}
			info.FullName = fullname;
			info.Loaded = true;
			info.ShouldLoad = true;
		}

		public ragCore.DllInfo FindInfo(string name)
		{
			string key = System.IO.Path.GetFileName(name);
			if (m_KnownDlls.ContainsKey(key))
			{
				return m_KnownDlls[key];
			}
			return null;
		}

		[XmlIgnore]
        public Dictionary<string, ragCore.DllInfo> KnownDllHash
		{
			get {
				return m_KnownDlls;
			}
		}

        protected Dictionary<string, ragCore.DllInfo> m_KnownDlls = new Dictionary<string, ragCore.DllInfo>();

		[XmlType("Pref")]
		public class NameValuePair
		{
            [XmlAttribute]
			public string Name, Value;
		}

		public NameValuePair[] UiPrefs
		{
			get {
				return ConvertHashtableToArray(m_UiData);
			}
			set {
				m_UiData = ConvertArrayToHashtable(value);
			}
		}

		public string GetUiDataItem(string name, string def) {
			if (m_UiData.ContainsKey(name))
			{
				return m_UiData[name];
			}
			return def;
		}

		public void SetUiDataItem(string name, string value) {
			m_UiData[name] = value;
		}


        private NameValuePair[] ConvertHashtableToArray( Dictionary<string, string> table )
		{
			if (table == null)
			{
				return null;
			}
			NameValuePair[] arr = new NameValuePair[table.Count];
			int i=0;
            foreach ( KeyValuePair<string,string> pair in table )
            {
				arr[i] = new NameValuePair();
				arr[i].Name = pair.Key;
				arr[i].Value = pair.Value;
				i++;
            }
			return arr;
		}

        private Dictionary<string, string> ConvertArrayToHashtable( NameValuePair[] arr )
		{
			if (arr == null)
			{
                return new Dictionary<string, string>();
			}
            Dictionary<string, string> table = new Dictionary<string, string>();
			foreach(NameValuePair d in arr)
			{
				table.Add(d.Name, d.Value);
			}
			return table;
		}

        private Dictionary<string, string> m_UiData = new Dictionary<string, string>();
	}

    public class LayoutFileInfo
    {
        public LayoutFileInfo()
        {

        }

        public LayoutFileInfo( string appName )
        {
            m_appName = appName;
        }

        #region Variables
        private string m_appName = string.Empty;
        private string m_lastLayoutFilename = string.Empty;
        private List<string> m_recentLayoutFilenames = new List<string>();
        #endregion

        #region Properties
        public string AppName
        {
            get
            {
                return m_appName;
            }
            set
            {
                m_appName = value;
            }
        }

        public string LastLayoutFilename
        {
            get
            {
                return m_lastLayoutFilename;
            }
            set
            {
                m_lastLayoutFilename = value;
            }
        }

        [XmlIgnore]
        public List<string> RecentLayoutFilenames
        {
            get
            {
                return m_recentLayoutFilenames;
            }
        }

        [XmlElement( "RecentLayoutFilenames" )]
        public string[] XmlRecentLayoutFilenames
        {
            get
            {
                return m_recentLayoutFilenames.ToArray();
            }
            set
            {
                m_recentLayoutFilenames.Clear();

                if ( value != null )
                {
                    m_recentLayoutFilenames.AddRange( value );
                }
            }
        }
        #endregion
    }

    public class LayoutFileInfos
    {
        private readonly ILog _log;

        public LayoutFileInfos()
        {
        }

        public LayoutFileInfos(ILog log)
        {
            _log = log;
        }

        #region Variables
        private Dictionary<string, LayoutFileInfo> m_layoutFileInfos = new Dictionary<string, LayoutFileInfo>();
        #endregion

        #region Properties
        [XmlIgnore]
        public Dictionary<string, LayoutFileInfo> LayoutInfos
        {
            get
            {
                return m_layoutFileInfos;
            }
        }

        [XmlElement( "LayoutInfos" )]
        public LayoutFileInfo[] XmlLayoutFileInfos
        {
            get
            {
                List<LayoutFileInfo> infos = new List<LayoutFileInfo>( m_layoutFileInfos.Values );
                return infos.ToArray();
            }
            set
            {
                m_layoutFileInfos.Clear();

                if ( value != null )
                {
                    foreach ( LayoutFileInfo info in value )
                    {
                        if ( !m_layoutFileInfos.ContainsKey( info.AppName ) )
                        {
                            m_layoutFileInfos.Add( info.AppName, info );
                        }
                    }
                }
            }
        }
        #endregion

        #region Public Functions
        public string Serialize()
        {
	        try
	        {
		        XmlSerializer serializer = new XmlSerializer( this.GetType() );
		        using (StringWriter writer = new StringWriter())
		        {
			        serializer.Serialize( writer, this );
			        return writer.ToString();
		        }
	        }
	        catch ( Exception e )
	        {
		        _log.ToolException(e, "LayoutFileInfos Error serializing.");
		        return null;
	        }
        }

        public bool Deserialize( string serializedText )
        {
	        try
	        {
		        XmlSerializer serializer = new XmlSerializer(this.GetType());
		        using (TextReader reader = new StringReader(serializedText))
		        {
			        LayoutFileInfos infos = serializer.Deserialize(reader) as LayoutFileInfos;

			        if ( infos == null )
			        {
				        _log.Message("LayoutFileInfos unable to deserialize text.");
				        return false;
			        }

			        this.XmlLayoutFileInfos = infos.XmlLayoutFileInfos;
			        return true;
		        }
	        }
	        catch ( Exception e )
	        {
		        _log.ToolException(e, "LayoutFileInfos Error deserializing string.");
		        return false;
	        }
        }
        #endregion
    }
}
