namespace rag
{
    partial class RagTrayIcon
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && (components != null) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager( typeof( RagTrayIcon ) );
            this.notifyIcon = new System.Windows.Forms.NotifyIcon( this.components );
            this.notifyIconContextMenuStrip = new System.Windows.Forms.ContextMenuStrip( this.components );
            this.viewLogToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.notifyIconContextMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // notifyIcon
            // 
            this.notifyIcon.ContextMenuStrip = this.notifyIconContextMenuStrip;
            this.notifyIcon.Icon = ((System.Drawing.Icon)(resources.GetObject( "notifyIcon.Icon" )));
            // 
            // notifyIconContextMenuStrip
            // 
            this.notifyIconContextMenuStrip.Items.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.viewLogToolStripMenuItem,
            this.exitToolStripMenuItem} );
            this.notifyIconContextMenuStrip.Name = "contextMenuStrip";
            this.notifyIconContextMenuStrip.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.notifyIconContextMenuStrip.ShowImageMargin = false;
            this.notifyIconContextMenuStrip.Size = new System.Drawing.Size( 127, 70 );
            // 
            // viewLogToolStripMenuItem
            // 
            this.viewLogToolStripMenuItem.Name = "viewLogToolStripMenuItem";
            this.viewLogToolStripMenuItem.Size = new System.Drawing.Size( 126, 22 );
            this.viewLogToolStripMenuItem.Text = "View Log";
            this.viewLogToolStripMenuItem.Click += new System.EventHandler( this.viewLogToolStripMenuItem_Click );
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size( 126, 22 );
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler( this.exitToolStripMenuItem_Click );
            // 
            // RagTrayIcon
            // 
            this.ClientSize = new System.Drawing.Size( 292, 273 );
            this.Name = "RagTrayIcon";
            this.ShowInTaskbar = false;
            this.Text = "RagTrayIcon";
            this.WindowState = System.Windows.Forms.FormWindowState.Minimized;
            this.VisibleChanged += new System.EventHandler( this.RagTrayIcon_VisibleChanged );
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler( this.RagTrayIcon_FormClosed );
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler( this.RagTrayIcon_FormClosing );
            this.notifyIconContextMenuStrip.ResumeLayout( false );
            this.ResumeLayout( false );

        }

        #endregion

        private System.Windows.Forms.NotifyIcon notifyIcon;
        private System.Windows.Forms.ContextMenuStrip notifyIconContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem viewLogToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
    }
}