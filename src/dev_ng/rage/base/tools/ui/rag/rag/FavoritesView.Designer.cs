namespace rag
{
    partial class FavoritesView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && (components != null) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.viewNameGroupBox = new System.Windows.Forms.GroupBox();
            this.viewNameTextBox = new System.Windows.Forms.TextBox();
            this.fileLocationLabel = new System.Windows.Forms.Label();
            this.loadButton = new System.Windows.Forms.Button();
            this.saveButton = new System.Windows.Forms.Button();
            this.allowDragNDropCheckBox = new System.Windows.Forms.CheckBox();
            this.lockThisViewCheckBox = new System.Windows.Forms.CheckBox();
            this.displayPathCheckBox = new System.Windows.Forms.CheckBox();
            this.saveAsButton = new System.Windows.Forms.Button();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.viewNameGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add( this.saveAsButton );
            this.splitContainer1.Panel1.Controls.Add( this.displayPathCheckBox );
            this.splitContainer1.Panel1.Controls.Add( this.lockThisViewCheckBox );
            this.splitContainer1.Panel1.Controls.Add( this.allowDragNDropCheckBox );
            this.splitContainer1.Panel1.Controls.Add( this.saveButton );
            this.splitContainer1.Panel1.Controls.Add( this.loadButton );
            this.splitContainer1.Panel1.Controls.Add( this.fileLocationLabel );
            this.splitContainer1.Panel1.Controls.Add( this.viewNameGroupBox );
            // 
            // splitContainer2
            // 
            this.splitContainer2.Size = new System.Drawing.Size( 361, 541 );
            // 
            // viewNameGroupBox
            // 
            this.viewNameGroupBox.Anchor = ( (System.Windows.Forms.AnchorStyles)( ( ( System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left )
                        | System.Windows.Forms.AnchorStyles.Right ) ) );
            this.viewNameGroupBox.Controls.Add( this.viewNameTextBox );
            this.viewNameGroupBox.Location = new System.Drawing.Point( 3, 3 );
            this.viewNameGroupBox.Name = "viewNameGroupBox";
            this.viewNameGroupBox.Size = new System.Drawing.Size( 173, 50 );
            this.viewNameGroupBox.TabIndex = 0;
            this.viewNameGroupBox.TabStop = false;
            this.viewNameGroupBox.Text = "View Name";
            // 
            // viewNameTextBox
            // 
            this.viewNameTextBox.Anchor = ( (System.Windows.Forms.AnchorStyles)( ( ( System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left )
                        | System.Windows.Forms.AnchorStyles.Right ) ) );
            this.viewNameTextBox.Location = new System.Drawing.Point( 6, 19 );
            this.viewNameTextBox.Name = "viewNameTextBox";
            this.viewNameTextBox.Size = new System.Drawing.Size( 161, 20 );
            this.viewNameTextBox.TabIndex = 0;
            this.viewNameTextBox.TextChanged += new System.EventHandler( this.viewNameTextBox_TextChanged );
            // 
            // fileLocationLabel
            // 
            this.fileLocationLabel.Anchor = ( (System.Windows.Forms.AnchorStyles)( ( ( System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left )
                        | System.Windows.Forms.AnchorStyles.Right ) ) );
            this.fileLocationLabel.Location = new System.Drawing.Point( 9, 60 );
            this.fileLocationLabel.Name = "fileLocationLabel";
            this.fileLocationLabel.Size = new System.Drawing.Size( 186, 53 );
            this.fileLocationLabel.TabIndex = 1;
            this.fileLocationLabel.Text = "<no file>";
            // 
            // loadButton
            // 
            this.loadButton.Location = new System.Drawing.Point( 3, 116 );
            this.loadButton.Name = "loadButton";
            this.loadButton.Size = new System.Drawing.Size( 87, 23 );
            this.loadButton.TabIndex = 2;
            this.loadButton.Text = "Load Favorites";
            this.loadButton.UseVisualStyleBackColor = true;
            this.loadButton.Click += new System.EventHandler( this.loadButton_Click );
            // 
            // saveButton
            // 
            this.saveButton.Location = new System.Drawing.Point( 2, 145 );
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size( 87, 23 );
            this.saveButton.TabIndex = 3;
            this.saveButton.Text = "Save Favorites";
            this.saveButton.UseVisualStyleBackColor = true;
            this.saveButton.Click += new System.EventHandler( this.saveButton_Click );
            // 
            // allowDragNDropCheckBox
            // 
            this.allowDragNDropCheckBox.AutoSize = true;
            this.allowDragNDropCheckBox.Location = new System.Drawing.Point( 3, 203 );
            this.allowDragNDropCheckBox.Name = "allowDragNDropCheckBox";
            this.allowDragNDropCheckBox.Size = new System.Drawing.Size( 112, 17 );
            this.allowDragNDropCheckBox.TabIndex = 4;
            this.allowDragNDropCheckBox.Text = "Allow Drag && Drop";
            this.allowDragNDropCheckBox.UseVisualStyleBackColor = true;
            // 
            // lockThisViewCheckBox
            // 
            this.lockThisViewCheckBox.AutoSize = true;
            this.lockThisViewCheckBox.Location = new System.Drawing.Point( 3, 226 );
            this.lockThisViewCheckBox.Name = "lockThisViewCheckBox";
            this.lockThisViewCheckBox.Size = new System.Drawing.Size( 94, 17 );
            this.lockThisViewCheckBox.TabIndex = 5;
            this.lockThisViewCheckBox.Text = "Lock this view";
            this.lockThisViewCheckBox.UseVisualStyleBackColor = true;
            // 
            // displayPathCheckBox
            // 
            this.displayPathCheckBox.AutoSize = true;
            this.displayPathCheckBox.Checked = true;
            this.displayPathCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.displayPathCheckBox.Location = new System.Drawing.Point( 3, 249 );
            this.displayPathCheckBox.Name = "displayPathCheckBox";
            this.displayPathCheckBox.Size = new System.Drawing.Size( 85, 17 );
            this.displayPathCheckBox.TabIndex = 6;
            this.displayPathCheckBox.Text = "Display Path";
            this.displayPathCheckBox.UseVisualStyleBackColor = true;
            this.displayPathCheckBox.CheckedChanged += new System.EventHandler( this.displayPathCheckBox_CheckedChanged );
            // 
            // saveAsButton
            // 
            this.saveAsButton.Location = new System.Drawing.Point( 2, 174 );
            this.saveAsButton.Name = "saveAsButton";
            this.saveAsButton.Size = new System.Drawing.Size( 102, 23 );
            this.saveAsButton.TabIndex = 7;
            this.saveAsButton.Text = "Save Favorites As";
            this.saveAsButton.UseVisualStyleBackColor = true;
            this.saveAsButton.Click += new System.EventHandler( this.saveAsButton_Click );
            // 
            // FavoritesView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
            this.Name = "FavoritesView";
            this.Controls.SetChildIndex( this.splitContainer1, 0 );
            this.splitContainer1.Panel1.ResumeLayout( false );
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout( false );
            this.splitContainer1.ResumeLayout( false );
            this.splitContainer2.Panel1.ResumeLayout( false );
            this.splitContainer2.ResumeLayout( false );
            this.viewNameGroupBox.ResumeLayout( false );
            this.viewNameGroupBox.PerformLayout();
            this.ResumeLayout( false );

        }

        #endregion

        private System.Windows.Forms.GroupBox viewNameGroupBox;
        private System.Windows.Forms.TextBox viewNameTextBox;
        private System.Windows.Forms.Label fileLocationLabel;
        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.Button loadButton;
        private System.Windows.Forms.CheckBox lockThisViewCheckBox;
        private System.Windows.Forms.CheckBox allowDragNDropCheckBox;
        private System.Windows.Forms.CheckBox displayPathCheckBox;
        private System.Windows.Forms.Button saveAsButton;
    }
}
