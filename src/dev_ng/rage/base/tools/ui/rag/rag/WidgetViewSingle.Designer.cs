namespace rag
{
    partial class WidgetViewSingle
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && (components != null) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.xPanderList = new ragCore.XPanderList();
            this.SuspendLayout();
            // 
            // xPanderList
            // 
            this.xPanderList.AutoScroll = true;
            this.xPanderList.BackColor = System.Drawing.Color.FromArgb( ((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))) );
            this.xPanderList.BackColorDark = System.Drawing.SystemColors.Control;
            this.xPanderList.BackColorLight = System.Drawing.SystemColors.Control;
            this.xPanderList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xPanderList.Location = new System.Drawing.Point( 0, 0 );
            this.xPanderList.MinimumWidth = 0;
            this.xPanderList.Name = "xPanderList";
            this.xPanderList.NestingBottomPad = 10;
            this.xPanderList.NestingIndent = 10;
            this.xPanderList.Size = new System.Drawing.Size( 527, 467 );
            this.xPanderList.TabIndex = 0;
            this.xPanderList.Visible = false;
            // 
            // WidgetViewSingle
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
            this.Controls.Add( this.xPanderList );
            this.Name = "WidgetViewSingle";
            this.Size = new System.Drawing.Size( 527, 467 );
            this.ResumeLayout( false );

        }

        #endregion

        private ragCore.XPanderList xPanderList;
    }
}
