using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Text;
using System.Threading;
using System.Windows.Forms;

using ragCore;
using RSG.Base.Forms;
using ragWidgets;
using System.Diagnostics;
using RSG.Base.Logging;

namespace rag
{
	/// <summary>
	/// Summary description for BankManager.
	/// </summary>
	public class BankManager : IBankManager, IKeyUpProcessor
	{
        private const String LogCtx = "Bank Manager";
        private readonly ILog _log;

		public BankManager(ILog log)
		{
            _log = log;
		}

        private static uint m_id;
		public delegate void TimeStringDelegate(String str);
		public event ragCore.InitFinishedHandler InitFinished;
		public static event TimeStringDelegate TimeStringHandler;

		public static int GetWidgetTypeGUID() {return GetStaticGUID();}
		public static int GetStaticGUID()		{return Widget.ComputeGUID('b','m','g','r');}

		public string GetSavedWidgetsFileName(String exeName)
		{
			return Path.Combine( MainWindow.UserPrefDir, ".ragwidgets." + exeName + "." + Environment.UserName + ".xml" );
		}

        private static bool RemoteHandler( BankRemotePacket packet, out String errorMessage )
        {
            bool handled = false;
            errorMessage = null;

            switch ( packet.BankCommand )
            {
                // open file dialog support #1:
                case BankRemotePacket.EnumPacketType.USER_0:
                    {
                        // Ignore if we are running against the proxy.
                        if (CommandLineUtils.UseProxy())
                        {
                            handled = true;
                            break;
                        }

                        packet.Begin();
                        string origDest = packet.Read_const_char();
                        uint maxDest = packet.Read_u32();
                        string mask = packet.Read_const_char();
                        bool save = packet.Read_bool();
                        string desc = packet.Read_const_char();
                        packet.End();

                        string filter;
                        filter = (desc != string.Empty ? desc : "(" + mask + ")") + "|" + mask;
                        filter += "|" + "All Files (*.*)|*.*";
                        if ( save )
                        {
                            SaveFileDialog saveDialog = new SaveFileDialog();
                            saveDialog.Filter = filter;
                            DialogResult result = saveDialog.ShowDialog();
                            BankRemotePacket p = new BankRemotePacket( packet.Pipe as BankPipe );
                            p.Begin(BankRemotePacket.EnumPacketType.USER_0, GetWidgetTypeGUID(), m_id);
                            p.Write_bool( result == DialogResult.OK );
                            if ( result == DialogResult.OK )
                                p.Write_const_char( saveDialog.FileName );
                            p.Send();
                        }
                        else
                        {
                            OpenFileDialog openDialog = new OpenFileDialog();
                            openDialog.CheckFileExists = true;
                            openDialog.Filter = filter;
                            openDialog.Multiselect = false;
                            DialogResult result = openDialog.ShowDialog();
                            BankRemotePacket p = new BankRemotePacket( packet.Pipe as BankPipe );
                            p.Begin(BankRemotePacket.EnumPacketType.USER_0, GetWidgetTypeGUID(), m_id);
                            p.Write_bool( result == DialogResult.OK );
                            if ( result == DialogResult.OK )
                                p.Write_const_char( openDialog.FileName );
                            p.Send();
                        }

                        handled = true;
                    }
                    break;

                // signal that the client has finished his initialization phase:
                case BankRemotePacket.EnumPacketType.USER_2:
                    {
                        RageApplication app = RageApplication.FindApplication( packet.Pipe );
                        BankManager bankMgr = app.BankMgr as BankManager;

                        bankMgr.OnInitFinished();

                        // read saved widget information:
                        bankMgr.DeSerializeBanks(app.ExeName);
                        handled = true;
                    }
                    break;

                // see if its timer information:
                case BankRemotePacket.EnumPacketType.USER_5:
                    {
                        packet.Begin();

                        String timeString = packet.Read_const_char();
                        if ( TimeStringHandler != null )
                            TimeStringHandler( timeString );

                        packet.End();
                        handled = true;
                    }
                    break;

                // see if it's a ping response
                case BankRemotePacket.EnumPacketType.USER_6:
                    {
                        packet.Begin();
                        uint data = packet.Read_u32();
                        bool delayed = packet.Read_bool();
                        packet.End();

                        // Call the ping response.
                        RageApplication app = RageApplication.FindApplication( packet.Pipe );
                        BankManager bankMgr = app.BankMgr as BankManager;

                        bankMgr.OnPingResponse();
                        handled = true;
                    }
                    break;

                // CLIPBOARD_COPY:
                case BankRemotePacket.EnumPacketType.USER_7:
                    {
                        packet.Begin();
                        string theString = packet.Read_const_char();
                        // update clipboard string:
                        RageApplication app = RageApplication.FindApplication( packet.Pipe );
                        BankManager bankMgr = app.BankMgr as BankManager;
                        bankMgr.m_ClipboardString += theString;
                        Clipboard.SetDataObject( bankMgr.m_ClipboardString, true );
                        packet.End();
                        handled = true;
                    }
                    break;

                // CLIPBOARD_CLEAR:
                case BankRemotePacket.EnumPacketType.USER_8:
                    {
                        packet.Begin();
                        RageApplication app = RageApplication.FindApplication( packet.Pipe );
                        BankManager bankMgr = app.BankMgr as BankManager;
                        bankMgr.m_ClipboardString = "";
                        Clipboard.SetDataObject( bankMgr.m_ClipboardString, true );
                        packet.End();
                        handled = true;
                    }
                    break;

                // CREATE_OUTPUT_WINDOW:
                case BankRemotePacket.EnumPacketType.USER_14:
                    {
                        packet.Begin();
                        string channelName = packet.Read_const_char();
                        string color = packet.Read_const_char();
                        packet.End();

                        RageApplication app = RageApplication.FindApplication( packet.Pipe );
                        if ( app != null )
                        {
                            app.AddOutputWindow(channelName, color);
                        }
                        handled = true;
                    }
                    break;

                // INFORM_SUB_CHANNEL:
                case BankRemotePacket.EnumPacketType.USER_15:
                    {
                        packet.Begin();
                        string channelName = packet.Read_const_char();
                        string subChannelName = packet.Read_const_char();
                        packet.End();

                        RageApplication app = RageApplication.FindApplication( packet.Pipe );
                        if (app != null)
                        {
                            app.AddSubChannelInfo(channelName, subChannelName);
                        }
                        handled = true;
                        //Debug.WriteLine(String.Format("Parent: {0}, Child: {1}", channelName, subChannelName));
                    }
                    break;

                case BankRemotePacket.EnumPacketType.CREATE:
                    m_id = packet.Id;
                    handled = true;
                    break;

                case BankRemotePacket.EnumPacketType.CHANGED:
                    handled = true;
                    break;

                default:
                    handled = Widget.RemoteHandlerBase(packet, GetStaticGUID(), out errorMessage);
                    break;
            }

            return handled;
        }

        public void Reset()
        {
            OnPingResponse();
        }

		protected void OnPingResponse()
		{
			if (m_PingResponse != null)
			{
				// m_PingResponse (the member variable) needs to be null before the
				// event is signalled, so that we can add a new ping request from that event.

				EventHandler pingEvent = m_PingResponse;
				m_PingResponse = null;
				pingEvent(this, System.EventArgs.Empty);
			}
		}

		private event EventHandler m_PingResponse;

        public void SendDelayedPing(EventHandler callback, BankPipe pipe)
		{
			// What we should really do here is see if there is already a ping response handler. If so we should
			// add a new handler that sends this as a second (or Nth) ping and then responds.
			// For now we just punt and have the new handler called for the old message.

			if (m_PingResponse == null)
			{
				BankRemotePacket p = new BankRemotePacket(pipe);
                p.Begin(BankRemotePacket.EnumPacketType.USER_6, GetWidgetTypeGUID(), m_id);
				p.Write_u32(0);
				p.Write_bool(true);
				p.Send();
			}
			m_PingResponse += callback;
		}

        public void SendPing(EventHandler callback, BankPipe pipe)
        {
            // What we should really do here is see if there is already a ping response handler. If so we should
            // add a new handler that sends this as a second (or Nth) ping and then responds.
            // For now we just punt and have the new handler called for the old message.

            if (m_PingResponse == null)
            {
                BankRemotePacket p = new BankRemotePacket(pipe);
                p.Begin(BankRemotePacket.EnumPacketType.USER_6, GetWidgetTypeGUID(), m_id);
                p.Write_u32(0);
                p.Write_bool(false);
                p.Send();
            }
            m_PingResponse += callback;
        }


        public void OnInitFinished()
        {
            if ( this.InitFinished != null )
            {
                this.InitFinished();
            }

            if ( !m_initFinished )
            {
                Widget.WidgetAdded += new WidgetEventHandler( Widget_WidgetAdded );
            }

            m_initFinished = true;
        }

        private void Widget_WidgetAdded( object sender, WidgetEventArgs e )
        {
            foreach ( IDockableView view in DockableViewManager.Instance.DockableViews )
            {
                if ( view is WidgetViewSingle )
                {
                    WidgetViewSingle viewSingle = view as WidgetViewSingle;
                    if ( viewSingle.PlaceholderWidgetPath == e.Widget.Path )
                    {
                        viewSingle.AddWidget( e.Widget );
                    }
                }
                else if ( view is WidgetViewPlaceholder )
                {
                    WidgetViewPlaceholder viewPlaceholder = view as WidgetViewPlaceholder;
                    if ( viewPlaceholder.PlaceholderWidgetPaths.ContainsKey( e.Widget.Path ) )
                    {
                        viewPlaceholder.AddWidget( e.Widget );
                    }
                }
            }
        }

        public static void AddHandlers( BankRemotePacketProcessor packetProcessor )
		{
            packetProcessor.AddType( GetStaticGUID(), new BankRemotePacketProcessor.HandlerDel( RemoteHandler ) );
		}

		public void AddBank(WidgetBank bank)
		{
			if (!m_Banks.Contains(bank))
				m_Banks.Add(bank);
		}

		public void RemoveBank(WidgetBank bank)
		{
			m_Banks.Remove(bank);
		}

        public List<Widget> AllBanks
		{
			get { return m_Banks; }
		}

        public void SerializeBanks( String appName )
        {
            string tempFilename = Path.Combine(Path.GetTempPath(), string.Format("{0}.tmp", Guid.NewGuid()));
            string filename = GetSavedWidgetsFileName( appName );

            XmlTextWriter xmlWriter = null;
            try
            {
                xmlWriter = new XmlTextWriter( tempFilename, System.Text.Encoding.Unicode );
            }
            catch ( Exception e )
            {
                _log.ToolExceptionCtx(LogCtx, e, "Unable to create temporary Widget save file, '{0}'.", tempFilename);
                return;
            }

            try
            {
                if ( xmlWriter != null )
                {
                    xmlWriter.WriteStartDocument();
                    {
                        xmlWriter.WriteStartElement( "SavedWidgets" );
                        {
                            foreach ( Widget widget in m_Banks )
                            {
                                String path = "";
                                widget.Serialize( xmlWriter, path );
                            }
                        }
                        xmlWriter.WriteEndElement();
                    }
                    xmlWriter.WriteEndDocument();
                    xmlWriter.Close();
                }
            }
            catch ( Exception e )
            {
                if ( xmlWriter != null )
                {
                    xmlWriter.Close();
                }

                rageMessageBox.ShowWarning( String.Format( "Problem serializing widgets file '{0}': {1}",
                    tempFilename, e.Message ), "Problem serializing widgets file!" );
                return;
            }

            RageApplication.MoveFileSafe( tempFilename, filename, _log );
        }

		public void DeSerializeBanks(String appName)
		{
			String pathName=GetSavedWidgetsFileName(appName);
			if (!File.Exists(pathName))
				return;

			try
			{
				XmlTextReader xmlReader=new XmlTextReader(pathName);
				xmlReader.WhitespaceHandling = WhitespaceHandling.None;

				while (xmlReader.Read())
				{
					if (xmlReader.NodeType == XmlNodeType.Element && xmlReader.Name=="Widget")
					{
						String[] path=xmlReader["Path"].Split(new Char[]{'\\'});
						int startIndex=0;
						foreach (String pathPart in path)
						{
							if (pathPart!="")
								break;
							startIndex++;
						}

						foreach (Widget widget in m_Banks)
						{
							int index=startIndex;

							// find the widget, if we do deserialize:
							Widget widgetFound=widget.ComparePath(path,ref index);
							if (widgetFound!=null)
							{
								widgetFound.DeSerialize(xmlReader);
							}
						}
					}
				}

				xmlReader.Close();
			}
			catch (Exception e)
			{
				rageMessageBox.ShowWarning(
                    String.Format( "Problem parsing widget save file '{0}':\n{1}\n\n RAG will continue but it will not load saved widgets.", pathName, e.Message ),
                    "Problem parsing widget save file!" );
			}

		}

		public Widget FindFirstWidgetFromPath(string path)
		{
			List<Widget> list = FindWidgetsFromPath(path);
			if (list == null || list.Count == 0)
			{
				return null;
			}
			return list[0];
		}

		public List<Widget> FindWidgetsFromPath(string path)
		{
            if (path == "/")
            {
                return m_Banks;
            }
			return WidgetGroupBase.FindWidgetsFromPath(null, m_Banks, path);
		}

        public List<Widget> FindWidgetsFromPath( String[] path )
		{
            if (path.Length == 0)
            {
                return m_Banks;
            }
			return WidgetGroupBase.FindWidgetsFromPath(null, m_Banks, path, 0);
		}


        Dictionary<object, ProcessKeyUpDelegate> m_ProcessKeyUpDelegates = new Dictionary<object, ProcessKeyUpDelegate>();
		public void AddProcessKeyUpEvent(object key,ProcessKeyUpDelegate theDelegate)
		{
			if (!m_ProcessKeyUpDelegates.ContainsKey(key))
			{
				m_ProcessKeyUpDelegates.Add(key,theDelegate);
			}
		}

		public void RemoveProcessKeyUpEvent(object key)
		{
			m_ProcessKeyUpDelegates.Remove(key);
		}

		public bool ProcessKeyUp(System.Windows.Forms.KeyEventArgs e)
		{
			bool sendEventToApplication=true;

			// send key event:
			foreach ( ProcessKeyUpDelegate del in m_ProcessKeyUpDelegates.Values )
			{
				sendEventToApplication &= del(e);
			}

			return sendEventToApplication;
		}

        private List<Widget> m_Banks = new List<Widget>();
		private string m_ClipboardString="";
        private bool m_initFinished = false;

        public bool IsInitFinished
        {
            get
            {
                return m_initFinished;
            }
        }


        public Widget FindWidgetFromID( uint id )
        {
            throw new NotImplementedException();
        }
    }
}
