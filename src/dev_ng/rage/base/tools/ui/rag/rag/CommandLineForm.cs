using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace rag
{
    public partial class CommandLineForm : Form
    {
        public CommandLineForm()
        {
            InitializeComponent();
        }

        #region Event Handlers
        private void CommandLineForm_Load( object sender, EventArgs e )
        {
            this.Text = "Command Line For " + RageApplication.MainApp.ExeName;
            this.commandLineRichTextBox.Text = GetCommandLineText();
        }

        private void multiLineCheckBox_CheckedChanged( object sender, EventArgs e )
        {
            this.commandLineRichTextBox.Text = GetCommandLineText();
        }
        #endregion

        #region Private Functions
        private string GetCommandLineText()
        {
            if ( this.multiLineCheckBox.Checked )
            {
                string[] argSplit = RageApplication.MainApp.Args.Split( new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries );
                
                StringBuilder msg = new StringBuilder( RageApplication.MainApp.ExePath );

                bool inQuoteBlock = false;
                foreach ( string arg in argSplit )
                {
                    int quoteIndex = arg.IndexOf( '"' );
                    while ( quoteIndex > -1 )
                    {
                        inQuoteBlock = !inQuoteBlock;

                        if ( quoteIndex + 1 < arg.Length )
                        {
                            quoteIndex = arg.IndexOf( '"', quoteIndex + 1 );
                        }
                        else
                        {
                            break;
                        }
                    }

                    if ( !inQuoteBlock && arg.StartsWith( "-" ) && (arg.Length > 1) && !Char.IsDigit( arg[1] ) )
                    {
                        msg.Append( '\n' );
                    }
                    else
                    {
                        msg.Append( ' ' );
                    }

                    msg.Append( arg );
                }

                return msg.ToString();
            }
            else
            {
                return RageApplication.MainApp.ExePath + " " + RageApplication.MainApp.Args;
            }
        }
        #endregion
    }
}