﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using ragCore;
using ragWidgets;
using System.Collections;

namespace rag
{
    public class RagWidgetVisualiser : WidgetVisualiser
    {
        public static void InitialiseHandlers()
        {
            WidgetGroupBase.ChildAdded += OnChildAdded;
            WidgetGroupBase.ChildRemoved += OnChildRemoved;

            // show
            AddDefaultHandlerShowVisible( typeof( WidgetAngle ), typeof( WidgetAngleVisible ) );
            AddDefaultHandlerShowVisible( typeof( WidgetButton ), typeof( WidgetButtonVisible ) );
            AddDefaultHandlerShowVisible( typeof( WidgetColor ), typeof( WidgetColorVisible ) );
            AddDefaultHandlerShowVisible( typeof( WidgetCombo ), typeof( WidgetComboVisible ) );
            AddDefaultHandlerShowVisible( typeof( WidgetImageViewer ), typeof( WidgetImageViewerVisible ) );
            AddDefaultHandlerShowVisible( typeof( WidgetList ), typeof( WidgetListVisible ) );
            AddDefaultHandlerShowVisible( typeof( WidgetMatrix ), typeof( WidgetMatrixVisible ) );
            AddDefaultHandlerShowVisible( typeof( WidgetMatrix33 ), typeof( WidgetMatrixVisible ) );
            AddDefaultHandlerShowVisible( typeof( WidgetMatrix34 ), typeof( WidgetMatrixVisible ) );
            AddDefaultHandlerShowVisible( typeof( WidgetMatrix44 ), typeof( WidgetMatrixVisible ) );
            AddDefaultHandlerShowVisible( typeof( WidgetPlaceholder ), typeof( WidgetPlaceholderVisible ) );
            AddDefaultHandlerShowVisible( typeof( WidgetSeparator ), typeof( WidgetSeparatorVisible ) );
            AddDefaultHandlerShowVisible( typeof( WidgetSliderFloat ), typeof( WidgetSliderFloatVisible ) );
            AddDefaultHandlerShowVisible( typeof( WidgetSliderInt ), typeof( WidgetSliderIntVisible ) );
            AddDefaultHandlerShowVisible( typeof( WidgetText ), typeof( WidgetTextVisible ) );
            AddDefaultHandlerShowVisible( typeof( WidgetTitle ), typeof( WidgetTitleVisible ) );
            AddDefaultHandlerShowVisible( typeof( WidgetToggle ), typeof( WidgetToggleVisible ) );
            AddDefaultHandlerShowVisible( typeof( WidgetVector2 ), typeof( WidgetVectorVisible ) );
            AddDefaultHandlerShowVisible( typeof( WidgetVector3 ), typeof( WidgetVectorVisible ) );
            AddDefaultHandlerShowVisible( typeof( WidgetVector4 ), typeof( WidgetVectorVisible ) );
            AddDefaultHandlerShowVisible( typeof( WidgetVCR ), typeof( WidgetVCRVisible ) );

            AddHandlerShowVisible( typeof( WidgetTreeList ), ShowVisibleWidgetTreeList );
            AddHandlerShowVisible( typeof( WidgetGroup ), ShowVisibleWidgetGroup );
            AddHandlerShowVisible( typeof( WidgetBank ), ShowVisibleWidgetBank );
            AddHandlerShowVisible( typeof( WidgetData ), ShowVisibleWidgetData );

            // hide
            AddDefaultHandlerHideVisible( typeof( WidgetAngle ) );
            AddDefaultHandlerHideVisible( typeof( WidgetButton ) );
            AddDefaultHandlerHideVisible( typeof( WidgetColor ) );
            AddDefaultHandlerHideVisible( typeof( WidgetCombo ) );
            AddDefaultHandlerHideVisible( typeof( WidgetImageViewer ) );
            AddDefaultHandlerHideVisible( typeof( WidgetList ) );
            AddDefaultHandlerHideVisible( typeof( WidgetMatrix ) );
            AddDefaultHandlerHideVisible( typeof( WidgetMatrix33 ) );
            AddDefaultHandlerHideVisible( typeof( WidgetMatrix34 ) );
            AddDefaultHandlerHideVisible( typeof( WidgetMatrix44 ) );
            AddDefaultHandlerHideVisible( typeof( WidgetPlaceholder ) );
            AddDefaultHandlerHideVisible( typeof( WidgetSeparator ) );
            AddDefaultHandlerHideVisible( typeof( WidgetSliderFloat ) );
            AddDefaultHandlerHideVisible( typeof( WidgetSliderInt ) );
            AddDefaultHandlerHideVisible( typeof( WidgetText ) );
            AddDefaultHandlerHideVisible( typeof( WidgetTitle ) );
            AddDefaultHandlerHideVisible( typeof( WidgetToggle ) );
            AddDefaultHandlerHideVisible( typeof( WidgetVector2 ) );
            AddDefaultHandlerHideVisible( typeof( WidgetVector3 ) );
            AddDefaultHandlerHideVisible( typeof( WidgetVector4 ) );
            AddDefaultHandlerHideVisible( typeof( WidgetVCR ) );

            AddDefaultHandlerHideVisible( typeof( WidgetTreeList ) );
            AddDefaultHandlerHideVisible( typeof( WidgetData ) );

            AddHandlerHideVisible( typeof( WidgetBank ), HideVisibleWidgetBank );
            AddHandlerHideVisible( typeof( WidgetGroup ), HideVisibleWidgetGroup );

        }

        private static WidgetVisible ShowVisibleWidgetTreeList(Widget widget, IWidgetView widgetView, Control parent, string hashKey, bool forceOpen, ToolTip tooltip)
        {
            WidgetTreeList widgetTreeList = (WidgetTreeList)widget;
            WidgetTreeListVisible visible = new WidgetTreeListVisible(widgetView, widgetTreeList, parent, tooltip, widgetTreeList.AllowDrag, widgetTreeList.AllowDrop, widgetTreeList.AllowDelete, widgetTreeList.ShowRootLines);

            foreach ( TreeListColumnHeader colHeader in widgetTreeList.ColHeaders )
            {
                visible.AddColumn( colHeader );
            }

            foreach ( TreeListNode node in widgetTreeList.NodeList )
            {
                visible.AddNode( node );
            }

            foreach ( TreeListItem item in widgetTreeList.ItemList )
            {
                visible.AddItem( item );
            }

            visible.ValidateNodes();

            WidgetShowVisibleSetup( widget, visible, hashKey );
            return visible;
        }

        private static WidgetVisible ShowVisibleWidgetGroup(Widget widget, IWidgetView widgetView, Control parent, string hashKey, bool forceOpen, ToolTip tooltip)
        {
            WidgetGroup widgetTreeList = (WidgetGroup)widget;
            if ( !forceOpen )
            {
                forceOpen = ViewUtils.IsGroupExpanded( widgetView, widgetTreeList );

                // If we have multiple widgets at the same level with the same names, don't open them.
                if (forceOpen && widget.Parent is WidgetGroup)
                {
                    WidgetGroup parentGroup = (WidgetGroup)widget.Parent;

                    if (parentGroup.WidgetList.Count(item => item.Title == widget.Title) > 1)
                    {
                        forceOpen = false;
                    }
                }
            }

            WidgetGroupVisible visible = new WidgetGroupVisible(widgetView, widgetTreeList, parent, tooltip, forceOpen);
            WidgetShowVisibleSetup( widget, visible, hashKey );

            if ( forceOpen )
            {
                visible.BeginUpdate();
                foreach ( Widget subWidget in widgetTreeList.WidgetList )
                {
                    try
                    {
                        WidgetVisible widgetVisible = WidgetVisualiser.ShowVisible(subWidget, widgetView, visible.Control, visible.Control.Name, false, tooltip);
                    }
                    catch ( Exception e )
                    {
                        DialogResult result = RSG.Base.Forms.rageMessageBox.ShowExclamation(
                            String.Format( "There was an error creating a Widget Visible for the Widget {0}:\n\n{1}\nWould you like to disable the widget?",
                            subWidget.Title, e.ToString() ), "Widget Error", MessageBoxButtons.YesNo );
                        if ( result == DialogResult.Yes )
                        {
                            subWidget.Enabled = false;

                            if ( !WidgetVisualiser.PacketProcessor.DisabledWidgets.Contains( subWidget ) )
                            {
                                WidgetVisualiser.PacketProcessor.DisabledWidgets.Add( subWidget );
                            }
                        }
                    }
                }

                string path = visible.Widget == null ? string.Empty : visible.Widget.FindPath();
                if ( visible.WidgetView != null && WidgetGroupBase.IsSortedGroup( visible.WidgetView.Guid.ToString(), path ) )
                {
                    visible.Sort();
                }

                visible.EndUpdate();
            }

            return visible;
        }

        private static WidgetVisible ShowVisibleWidgetData(Widget widget, IWidgetView widgetView, Control parent, string hashKey, bool forceOpen, ToolTip tooltip)
        {
            WidgetData widgetData = (WidgetData)widget;

            if ( !widgetData.ShowDataView )
            {
                return null;
            }

            WidgetDataVisible visible = new WidgetDataVisible(widgetView, widgetData, parent, tooltip, widgetData.MaxLength);
            WidgetShowVisibleSetup( widget, visible, hashKey );
            return visible;
        }

        private static WidgetVisible ShowVisibleWidgetBank(Widget widget, IWidgetView widgetView, Control parent, string hashKey, bool forceOpen, ToolTip tooltip)
        {
            WidgetBank widgetBank = (WidgetBank)widget;

            // add this set of temp groups to the hash table:
            List<WidgetVisible> tempGroups = new List<WidgetVisible>();
            RegisterVisible( widget, tempGroups, hashKey );
            widgetBank.AddRefGroup();

            if ( parent is XPanderList )
            {
                (parent as XPanderList).BeginUpdate();
            }

            WidgetGroupVisible visible = new WidgetGroupVisible(widgetView, widgetBank, parent, tooltip, false);
            tempGroups.Add( visible );

            visible.BeginUpdate();

            foreach ( Widget subWidget in widgetBank.WidgetList )
            {
                try
                {
                    ShowVisible( subWidget, widgetView, visible.Control, visible.Control.Name, false, tooltip);
                }
                catch ( Exception e )
                {
                    DialogResult result = RSG.Base.Forms.rageMessageBox.ShowExclamation(
                        String.Format( "There was an error creating a Widget Visible for the Widget {0}:\n\n{1}\nWould you like to disable the widget?",
                        subWidget.Title, e.ToString() ), "Widget Error", MessageBoxButtons.YesNo );
                    if ( result == DialogResult.Yes )
                    {
                        subWidget.Enabled = false;

                        if ( !PacketProcessor.DisabledWidgets.Contains( subWidget ) )
                        {
                            PacketProcessor.DisabledWidgets.Add( subWidget );
                        }
                    }
                }
            }

            string path = visible.Widget == null ? string.Empty : visible.Widget.FindPath();
            if ( visible.WidgetView != null && WidgetGroupBase.IsSortedGroup( visible.WidgetView.Guid.ToString(), path ) )
            {
                visible.Sort();
            }

            visible.EndUpdate();

            if ( parent is XPanderList )
            {
                (parent as XPanderList).PerformLayout();
                (parent as XPanderList).EndUpdate();
            }

            return visible;
        }



        //////////////////////////////////////////////////////////////////////////
        /// Hide handlers
        /// 

        public static void HideVisibleWidgetBank( Widget widget, String hashKey )
        {
            WidgetBank widgetBank = widget as WidgetBank;
            if ( widgetBank == null )
            {
                return;
            }

            widgetBank.ReleaseGroup();

            List<WidgetVisible> tempGroups = (List<WidgetVisible>)WidgetVisualiser.GetVisibleHash( widgetBank )[hashKey];

            if ( tempGroups != null && tempGroups.Count > 0 )
            {
                // Always delete widgets from the single item in tempGroups
                WidgetGroupVisible group = tempGroups[0] as WidgetGroupVisible;
                group.BeginUpdate();

                foreach ( Widget subWidget in widgetBank.List )
                {
                    HideVisible( subWidget, group.Control.Name );
                }

                group.EndUpdate();

                // finish group removal:
                if ( group != null )
                {
                    group.Remove();
                    group = null;
                }

                // get rid of all those groups:
                tempGroups.Clear();
            }



            if ( tempGroups != null )
            {
                WidgetVisualiser.UnregisterVisible( widgetBank, hashKey );
            }
        }

        public static void HideVisibleWidgetGroup( Widget widget, String hashKey )
        {
            WidgetGroup widgetGroup = widget as WidgetGroup;
            if ( widgetGroup == null )
            {
                return;
            }

            Hashtable table = WidgetVisualiser.GetVisibleHash( widgetGroup );
            System.Diagnostics.Trace.Assert( table != null );
            WidgetGroupVisible visible = (WidgetGroupVisible)table[hashKey];
            if ( visible != null )
            {
                // remove children
                System.Diagnostics.Trace.Assert( visible.Control != null );
                //if ( visible.Control != null ) // fairly sure this is a legit check.
                {
                    if ( visible.Expanded )
                    {
                        widgetGroup.ReleaseGroup();
                    }

                    visible.BeginUpdate();

                    foreach ( Widget subWidget in widgetGroup.List )
                    {
                        HideVisible( subWidget, visible.Control.Name );
                    }

                    visible.EndUpdate();
                }
            }

            WidgetVisualiser.WidgetDefaultHideVisible( widget, hashKey );
        }



        public static void OnChildAdded( WidgetGroupBase parent, Widget child )
        {
            // For each group visible that is open, add a visible child
            IDictionaryEnumerator enumerator = WidgetVisualiser.GetVisibleHash( parent ).GetEnumerator();
            while ( enumerator.MoveNext() )
            {
                if ( enumerator.Value is WidgetGroupVisible )
                {
                    WidgetGroupVisible visibleParent = enumerator.Value as WidgetGroupVisible;
                    if (visibleParent.Expanded || visibleParent.EverOpen)
                    {
                        visibleParent.BeginUpdate();
                        WidgetVisible newVisible = ShowVisible(child, visibleParent.WidgetView, visibleParent.Control, visibleParent.Control.Name, false, visibleParent.ToolTip);
                        if (!visibleParent.Expanded)
                        {
                            newVisible.Control.Visible = false;
                        }
                        visibleParent.EndUpdate();
                    }
                }
                else if ( enumerator.Value is List<WidgetVisible> )
                {
                    // just add to the first element (there can be only one!)
                    WidgetGroupVisible visibleParent = (enumerator.Value as List<WidgetVisible>)[0] as WidgetGroupVisible;
                    if (visibleParent.Expanded || visibleParent.EverOpen)
                    {
                        visibleParent.BeginUpdate();
                        WidgetVisible newVisible = ShowVisible(child, visibleParent.WidgetView, visibleParent.Control, visibleParent.Control.Name, false, visibleParent.ToolTip);
                        if (!visibleParent.Expanded)
                        {
                            newVisible.Control.Visible = false;
                        }
                        visibleParent.EndUpdate();
                    }
                }
            }

        }

        public static void OnChildRemoved(WidgetGroupBase parent, Widget child)
        {
            // For each group visible that is open, add a visible child
            IDictionaryEnumerator enumerator = WidgetVisualiser.GetVisibleHash(parent).GetEnumerator();
            while (enumerator.MoveNext())
            {
                if (enumerator.Value is WidgetGroupVisible)
                {
                    WidgetGroupVisible visibleParent = enumerator.Value as WidgetGroupVisible;
                    if (visibleParent.Expanded || visibleParent.EverOpen)
                    {
                        visibleParent.BeginUpdate();
                        HideVisible(child, visibleParent.Control.Name);
                        visibleParent.EndUpdate();
                    }
                }
                else if (enumerator.Value is List<WidgetVisible>)
                {
                    // just add to the first element (there can be only one!)
                    WidgetGroupVisible visibleParent = (enumerator.Value as List<WidgetVisible>)[0] as WidgetGroupVisible;
                    if (visibleParent.Expanded || visibleParent.EverOpen)
                    {
                        visibleParent.BeginUpdate();
                        HideVisible(child, visibleParent.Control.Name);
                        visibleParent.EndUpdate();
                    }
                }
            }
        }

    }
}
