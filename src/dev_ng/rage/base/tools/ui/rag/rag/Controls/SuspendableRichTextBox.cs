﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace rag.Controls
{
    /// <summary>
    /// 
    /// </summary>
    public partial class SuspendableRichTextBox : RichTextBox
    {
        #region Constants
        /// <summary>
        /// 
        /// </summary>
        const short WM_PAINT = 0x00f;
        #endregion // Constants

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public SuspendableRichTextBox()
            : base()
        {
        }
        #endregion // Constructor(s)

        #region Fields
        /// <summary>
        /// 
        /// </summary>
        private bool _paintSuspended = false;
        #endregion // Fields

        #region Public Methods
        /// <summary>
        /// 
        /// </summary>
        public void SuspendPaint()
        {
            _paintSuspended = true;
        }

        /// <summary>
        /// 
        /// </summary>
        public void ResumePaint()
        {
            _paintSuspended = false;
        }
        #endregion // Public Methods

        #region Overrides
        /*
        protected override void OnPaint(PaintEventArgs e)
        {
            if (!_paintSuspended)
            {
                base.OnPaint(e);
            }
        }
        */
        /// <summary>
        /// 
        /// </summary>
        /// <param name="m"></param>
        protected override void WndProc(ref System.Windows.Forms.Message m)
        {
            if (m.Msg == WM_PAINT && _paintSuspended)
            {
                m.Result = IntPtr.Zero;
            }
            else
            {
                base.WndProc(ref m);
            }
        }
        #endregion // Overrides
    } // SuspendableRichTextBox
}
