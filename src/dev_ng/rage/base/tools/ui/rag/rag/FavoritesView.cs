using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Xml;

using ragCore;
using RSG.Base.Forms;
using ragWidgets;

namespace rag
{
    public partial class FavoritesView : rag.WidgetViewPlaceholder
    {
        protected FavoritesView()
        {
            InitializeComponent();
        }

        public FavoritesView( String appName, IBankManager bankMgr, TD.SandDock.DockControl dockControl )
            : base( appName, bankMgr, dockControl )
        {
            // This call is required by the Windows.Forms Form Designer.
            InitializeComponent();

            InitializeXpanderList( dockControl );

            if ( sm_TabIcon == null )
            {
                sm_TabIcon = global::rag.Properties.Resources.favorites;
            }

            m_ViewNum = ++sm_ViewNum;
            
            this.viewNameTextBox.Text = GetTabText();

            dockControl.Closed += new EventHandler( dockControl_Closed );
        }

        #region Variables
        static int sm_ViewNum = 0;
        private static Bitmap sm_TabIcon = null;

        private string m_WidgetsManifestFile;
        private List<List<string>> m_widgetFullPaths = new List<List<string>>();
        private bool m_initFinished = false;
        private bool m_shared = false;
        #endregion

        #region Properties

        public bool Locked
        {
            get
            {                
                return this.lockThisViewCheckBox.Checked;
            }
        }

        public bool Shared
        {
            get
            {
                return m_shared;
            }
            set
            {
                m_shared = value;

                // If this is a shared favorites file, we have to lock it down so that
                // people can't change it or overwrite it accidentally
                this.lockThisViewCheckBox.Checked = m_shared && !MainWindow.AdminMode;

                this.viewNameTextBox.Enabled = !m_shared || MainWindow.AdminMode;
                this.saveButton.Enabled = !m_shared || MainWindow.AdminMode;
                this.allowDragNDropCheckBox.Enabled = !m_shared || MainWindow.AdminMode;
                this.lockThisViewCheckBox.Enabled = !m_shared || MainWindow.AdminMode;
                this.displayPathCheckBox.Enabled = !m_shared || MainWindow.AdminMode;

                this.DockControl.Text = this.DockControl.TabText = GetTabText();
            }
        }
        #endregion

        #region Overrides
        public override Bitmap GetBitmap() 
        { 
            return sm_TabIcon; 
        }
        
        public override string GetTabText()
        {
            StringBuilder s = new StringBuilder();
            
            if ( this.Shared )
            {
                s.Append( "Shared " );
            }

            s.Append( "Favorites View " );
            s.Append( m_ViewNum );
            
            return s.ToString();
        }

        public override bool AllowDragDrop
        {
            get
            {                
                return this.allowDragNDropCheckBox.Checked;
            }
        }

        public override void DeserializeView( XmlTextReader xmlReader, IBankManager bankMgr )
        {
            base.DeserializeView( xmlReader, bankMgr );
            m_WidgetsManifestFile = xmlReader["WidgetsManifestFile"];

            if (m_WidgetsManifestFile != null && m_WidgetsManifestFile.IndexOf(":") == -1)
            {
                m_WidgetsManifestFile = Environment.GetEnvironmentVariable("RS_TOOLSROOT") + "\\" + m_WidgetsManifestFile;
            }

            // Make sure the widget manifest file exists.
            if (!File.Exists(m_WidgetsManifestFile))
            {
                throw new FileNotFoundException(String.Format("The favorites file located at '{0}' no longer exists.", m_WidgetsManifestFile));
            }

            DeserializeWidgets();

            if (MainWindow.IsSharedFavoritesFile(m_WidgetsManifestFile))
            {
                this.Shared = true;
            }

            bankMgr.InitFinished += new ragCore.InitFinishedHandler( InitFinished );
        }

        protected override void SerializeView( XmlTextWriter xmlWriter, String exePathName )
        {
            // write check boxes (but only if our dirtiness occurred after initialization)
            if ( m_initFinished && m_IsDirty )
            {
                DialogResult result = rageMessageBox.ShowQuestion( this.DockControl,
                    String.Format( "You have made changes to favorites view '{0} ({1})'.\nDo you wish to save these changes?", this.Name, m_ViewNum ), 
                    "Save Changes?" );
                if ( result == DialogResult.Yes )
                {
                    saveButton_Click(this, EventArgs.Empty);
                    SaveFavoritesFile( m_WidgetsManifestFile );
                }
            }

            base.SerializeView( xmlWriter, exePathName );

            if ( m_WidgetsManifestFile != null )
            {
                xmlWriter.WriteAttributeString( "WidgetsManifestFile", m_WidgetsManifestFile );
            }
        }
        
        public override void ClearView()
		{
            base.ClearView();
		           
			m_widgetFullPaths.Clear();

            m_initFinished = false;
		}

        public override void UserPrefDirChanged( object sender, UserPrefDirChangedEventArgs e )
        {
            // change the current filename's path if it used to live in the old dir
            if ( !String.IsNullOrEmpty( m_WidgetsManifestFile ) )
            {
                string currWidgetsManifestFile = m_WidgetsManifestFile.ToUpper();
                if ( currWidgetsManifestFile.StartsWith( e.PreviousDir.ToUpper() ) )
                {
                    currWidgetsManifestFile = m_WidgetsManifestFile.Substring( e.PreviousDir.Length );
                    currWidgetsManifestFile = e.Dir + currWidgetsManifestFile;
                    m_WidgetsManifestFile = currWidgetsManifestFile;
                }
            }
        }
        #endregion

        #region Event Handlers
        private void dockControl_Closed( object sender, EventArgs e )
        {
            TD.SandDock.DockControl dockControl = sender as TD.SandDock.DockControl;
            if (dockControl.CloseAction == TD.SandDock.DockControlCloseAction.Dispose)
            {
                m_BankManager.InitFinished -= new ragCore.InitFinishedHandler( InitFinished );
            }
        }

        private void viewNameTextBox_TextChanged( object sender, EventArgs e )
        {
            m_DockControl.TabText = m_DockControl.Text = this.Name = this.viewNameTextBox.Text;
            m_IsDirty = true;
        }

        private void loadButton_Click( object sender, EventArgs e )
        {
            string initialDir = String.Empty;
            if ( String.IsNullOrEmpty( m_WidgetsManifestFile ) )
            {
                initialDir = MainWindow.UserPrefDir;
            }
            else
            {
                initialDir = Path.GetDirectoryName( m_WidgetsManifestFile );
            }

            string favoritesFile = ShowOpenDialog( initialDir );
            if ( !String.IsNullOrEmpty( favoritesFile ) )
            {
                OpenFavoritesFile( favoritesFile );
            }
        }

        private void saveButton_Click( object sender, EventArgs e )
        {
            if ( !String.IsNullOrEmpty(m_WidgetsManifestFile) )
            {
               SaveFavoritesFile( m_WidgetsManifestFile );
            }
            else
            {
                saveAsButton_Click( sender, e );
            }
        }

        private void saveAsButton_Click( object sender, EventArgs e )
        {
            if ( this.Shared && !MainWindow.AdminMode )
            {
                rageMessageBox.ShowWarning( this.DockControl, 
                    "You are about to save a local copy of this shared favorites view, and will no longer see any changes made to the shared favorites.\nYou can re-add this shared favorites view from the View menu at any time.",
                    "Shared Favorites View" );
            }

            string initialDir = String.Empty;
            string filename = String.Empty;
            if ( String.IsNullOrEmpty( m_WidgetsManifestFile ) )
            {
                initialDir = MainWindow.UserPrefDir;
                filename ="FavoritesView" + m_ViewNum + "." + Environment.UserName + ".xml";
            }
            else
            {
                initialDir = Path.GetDirectoryName( m_WidgetsManifestFile );
                filename = Path.GetFileName( m_WidgetsManifestFile );
            }

            string favoritesFile = ShowSaveDialog( filename, initialDir );
            if ( !String.IsNullOrEmpty( favoritesFile ) )
            {
                SaveFavoritesFile( favoritesFile );
            }
        }

        private void displayPathCheckBox_CheckedChanged( object sender, EventArgs e )
        {
            SetDisplayPath( this.displayPathCheckBox.Checked );
        }
        #endregion

        #region Public Functions

        public bool SameManifest(String manifestFilename)
        {
            return manifestFilename.ToLower() == m_WidgetsManifestFile.ToLower();
        }

        public void SerializeWidgets()
        {
            XmlTextWriter writer = null;
            try
            {
                writer = new XmlTextWriter( m_WidgetsManifestFile, Encoding.Unicode );

                writer.WriteStartDocument();

                writer.WriteWhitespace( "\n" );
                writer.WriteStartElement( "rag" );

                writer.WriteElementString( "LockThisView", this.lockThisViewCheckBox.Checked.ToString() );
                writer.WriteElementString( "DisplayPath", m_displayPath.ToString() );

                // write search strings:
                if ( m_widgetsAdded != null )
                {
                    writer.WriteElementString( "ViewName", this.Name );
                    foreach ( Widget widget in m_widgetsAdded )
                    {
                        SerializeWidgetPath( writer, widget );
                    }
                }

                writer.WriteEndElement();

                writer.Close();

                this.fileLocationLabel.Text = m_WidgetsManifestFile;
                m_IsDirty = false;
            }
            catch ( Exception e )
            {
                if ( writer != null )
                {
                    writer.Close();
                }

                rageMessageBox.ShowError( this.DockControl, e.Message, "Error saving " + m_WidgetsManifestFile );
            }
        }

        public void DeserializeWidgets()
        {
            // open the file:
            XmlTextReader xmlReader = null;
            try
            {
                xmlReader = new XmlTextReader(m_WidgetsManifestFile);
                xmlReader.WhitespaceHandling = WhitespaceHandling.None;
            }
            catch (Exception e)
            {
                throw new XmlException("Problem loading favorites file '" + m_WidgetsManifestFile + "': " + e.Message);
            }

            // read the file:
            try
            {
                while (!xmlReader.EOF)
                {
                    if ((xmlReader.NodeType != XmlNodeType.EndElement) && (xmlReader.Name == "LockThisView"))
                    {
                        xmlReader.Read();
                        this.lockThisViewCheckBox.Checked = (xmlReader.Value.ToLower() == "true") ? true : false;
                    }
                    else if ((xmlReader.NodeType != XmlNodeType.EndElement) && (xmlReader.Name == "DisplayPath"))
                    {
                        xmlReader.Read();
                        this.displayPathCheckBox.Checked = (xmlReader.Value.ToLower() == "true") ? true : false;
                    }
                    else if ((xmlReader.NodeType != XmlNodeType.EndElement) && (xmlReader.Name == "ViewName"))
                    {
                        xmlReader.Read();
                        this.viewNameTextBox.Text = m_DockControl.TabText = m_DockControl.Text = this.Name = xmlReader.Value;
                    }
                    else
                    {
                        List<string> fullPath = DeserializeWidgetPath(xmlReader);
                        if (fullPath.Count > 0)
                        {
                            m_widgetFullPaths.Add(fullPath);
                        }
                    }

                    xmlReader.Read();
                }

                xmlReader.Close();

                this.fileLocationLabel.Text = m_WidgetsManifestFile;

                m_IsDirty = false;
            }
            catch (Exception e)
            {
                throw new XmlException("Problem parsing favorites file '" + m_WidgetsManifestFile + "': " + e.Message);
            }
        }

        public void OpenFavoritesFile( string favoritesFile )
        {
            if ( !String.IsNullOrEmpty(favoritesFile) )
            {
                ClearView();
                m_WidgetsManifestFile = favoritesFile;
                DeserializeWidgets();
                InitFinished();

                if ( MainWindow.IsSharedFavoritesFile(m_WidgetsManifestFile) )
                {
                    this.Shared = true;
                    this.DockControl.CloseAction = TD.SandDock.DockControlCloseAction.Dispose;
                }
            }
        }

        public void SaveFavoritesFile( string favoritesFile )
        {
            if ( !String.IsNullOrEmpty(favoritesFile) )
            {
                m_WidgetsManifestFile = favoritesFile;
                SerializeWidgets();

                if ( !MainWindow.IsSharedFavoritesFile(m_WidgetsManifestFile) )
                {
                    this.Shared = false;
                }
            }
        }
        #endregion

        #region Private Functions
        private void InitFinished()
        {
            this.BeginUpdate();

            // loop through all banks to find the widget:
            foreach ( List<string> fullPath in m_widgetFullPaths )
            {
                Widget widget = ResolveWidgetPath( m_BankManager, fullPath, true );
                if ( widget != null )
                {
                    AddWidget( widget );
                }
                else
                {
                    AddWidget( new WidgetPlaceholder( fullPath, Color.Empty ) );
                }
            }

            this.EndUpdate();

            m_IsDirty = false; // it's not dirty anymore since we just loaded it

            m_initFinished = true;
        }

        private string ShowOpenDialog( string initialDir )
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Title = "Load Favorites File";
            dialog.Filter = "Favorites Files (*.xml)|*.xml|All Files (*.*)|*.*";
            dialog.RestoreDirectory = true;
            dialog.InitialDirectory = initialDir;

            DialogResult result = dialog.ShowDialog( FindForm() );
            if ( result == DialogResult.OK )
            {
                return dialog.FileName;
            }

            return String.Empty;
        }

        private string ShowSaveDialog( string filename, string initialDir )
        {
            SaveFileDialog dialog = new SaveFileDialog();
            dialog.Title = "Save Favorites File";
            dialog.Filter = "Favorites Files (*.xml)|*.xml|All Files (*.*)|*.*";
            dialog.RestoreDirectory = true;
            dialog.InitialDirectory = initialDir;
            dialog.FileName = filename;

            DialogResult result = dialog.ShowDialog( FindForm() );
            if ( result == DialogResult.OK )
            {
                return dialog.FileName;
            }

            return String.Empty;
        }       
        #endregion

        #region Public Static Functions
        public static WidgetViewBase CreateFavoritesView( String appName, IBankManager bankMgr, TD.SandDock.DockControl dockControl )
        {
            FavoritesView view = new FavoritesView( appName, bankMgr, dockControl );
            dockControl.CloseAction = TD.SandDock.DockControlCloseAction.Dispose;
            return view;
        }

        public static void AddContextMenuItems( IWidgetView currentWidgetView, ContextMenuStrip menu, Widget widget, WidgetVisible widgetVisible )
        {
            if ( widget == null )
            {
                return;
            }

            FavoritesView favView = null;
            foreach ( IDockableView view in DockableViewManager.Instance.DockableViews )
            {
                if ( (view is FavoritesView) && (currentWidgetView != favView) )
                {
                    favView = view as FavoritesView;

                    ToolStripMenuItem menuItem = new ToolStripMenuItem();

                    bool containsWidget = false;
                    if ( widget is WidgetBank )
                    {
                        containsWidget = true;

                        WidgetBank widgetBank = (WidgetBank)widget;
                        foreach ( Widget subWidget in widgetBank.WidgetList )
                        {
                            if ( !favView.ContainsWidget( subWidget ) )
                            {
                                containsWidget = false;
                                break;
                            }
                        }
                    }
                    else
                    {
                        containsWidget = favView.ContainsWidget( widget );
                    }

                    if ( containsWidget )
                    {
                        menuItem.Text = "Already Added To " + favView.GetTabText();
                        menuItem.Enabled = false;
                    }
                    else if ( favView.Locked )
                    {
                        menuItem.Text = favView.GetTabText() + " is Locked";
                        menuItem.Enabled = false;
                    }
                    else
                    {
                        FavoriteWidgetAdder adder = new FavoriteWidgetAdder( favView, widget, widgetVisible );
                        menuItem.Text = String.Format("Add To '{0}' Favorites View", favView.Name);
                        menuItem.Click += new EventHandler( adder.AddMenuHandler );
                    }

                    menu.Items.Add( menuItem );
                }
            }

            favView = currentWidgetView as FavoritesView;
            if ( favView != null )
            {
                // Add menu items for finding and showing widgets in each of the bank tree views
                bool addedSeparator = false;
                foreach ( IDockableView view in DockableViewManager.Instance.DockableViews )
                {
                    if ( view is BankTreeView )
                    {
                        BankTreeView bankView = (BankTreeView)view;

                        if ( !addedSeparator && menu.Items.Count > 0 )
                        {
                            menu.Items.Add( new ToolStripSeparator() );
                            addedSeparator = true;
                        }

                        ToolStripMenuItem findItem = new ToolStripMenuItem( "Show In " + bankView.GetTabText() );
                        findItem.Enabled = false;
                        menu.Items.Add( findItem );

                        WidgetTreeNode treeNode = bankView.FindDeepestMatchingTreeNode( widget.Path );
                        if ( treeNode != null )
                        {
                            WidgetFinder finder = new WidgetFinder( bankView, treeNode );
                            findItem.Enabled = true;
                            findItem.Click += new EventHandler( finder.FindWidgetHandler );
                        }
                    }
                }

                // add remove option if needed:
                if ( favView.ContainsWidget( widget ) )
                {
                    if ( menu.Items.Count > 0 )
                    {
                        menu.Items.Add( new ToolStripSeparator() );
                    }

                    FavoriteWidgetAdder adder = new FavoriteWidgetAdder( favView, widget, widgetVisible );
                    ToolStripMenuItem item = new ToolStripMenuItem( "Remove" );
                    item.Enabled = !favView.Locked;
                    item.Click += new EventHandler( adder.RemoveMenuHandler );
                    menu.Items.Add( item );
                }
            }
        }

        public static void ClearStatics()
        {
            sm_ViewNum = 0;
        }
        #endregion

        #region Helper Classes
        class FavoriteWidgetAdder
        {
            public FavoriteWidgetAdder( FavoritesView favView, Widget widget, WidgetVisible widgetVisible )
            {
                m_FavoriteView = favView;
                m_Widget = widget;
                m_WidgetVisible = widgetVisible;
            }

            public void AddMenuHandler( Object obj, System.EventArgs e )
            {
                if ( m_Widget is WidgetBank )
                {
                    WidgetBank widgetBank = (WidgetBank)m_Widget;
                    foreach ( Widget widget in widgetBank.WidgetList )
                    {
                        if ( !m_FavoriteView.ContainsWidget( widget ) )
                        {
                            m_FavoriteView.AddWidget( widget );
                        }
                    }
                }
                else
                {
                    m_FavoriteView.AddWidget( m_Widget );
                }
            }

            public void RemoveMenuHandler( Object obj, System.EventArgs e )
            {
                m_FavoriteView.RemoveWidget( m_WidgetVisible );
            }

            private FavoritesView m_FavoriteView;
            private Widget m_Widget;
            private WidgetVisible m_WidgetVisible;
        }
        #endregion
    }
}

