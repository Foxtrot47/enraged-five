using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using System.Xml;
using RSG.Base.Logging;
using TD.SandDock;
using rag.Properties;

namespace rag
{
    /// <summary>
    /// Summary description for RenderWindow.
    /// </summary>
    public class RenderWindow : System.Windows.Forms.UserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.Container components = null;
        private ToolStripMenuItem fillMenuToolStripMenuItem = new ToolStripMenuItem();
        private ToolStripMenuItem widescreenMenuToolStripMenuItem = new ToolStripMenuItem();
        private ToolStripMenuItem standardMenuToolStripMenuItem = new ToolStripMenuItem();
        private ToolStripMenuItem pspMenuToolStripMenuItem = new ToolStripMenuItem();
        private ToolStripMenuItem gameResolutionMenuToolStripMenuItem = new ToolStripMenuItem();

        private ToolStripMenuItem fillContextMenuToolStripMenuItem = new ToolStripMenuItem();
        private ToolStripMenuItem widescreenContextMenuToolStripMenuItem = new ToolStripMenuItem();
        private ToolStripMenuItem standardContextMenuToolStripMenuItem = new ToolStripMenuItem();
        private ToolStripMenuItem pspContextMenuToolStripMenuItem = new ToolStripMenuItem();
        private ToolStripMenuItem gameResolutionContextMenuToolStripMenuItem = new ToolStripMenuItem();

        private ToolStripMenuItem preventMouseLeavingContextMenuToolStripMenuItem = new ToolStripMenuItem();

        private Font m_drawFont = new Font("Arial", 16);
        private SolidBrush m_drawBrush = new SolidBrush(Color.DarkBlue);
        private readonly ILog _log;

        public RenderWindow(ILog log)
        {
            _log = log;

            // This call is required by the Windows.Forms Form Designer.
            InitializeComponent();

            // TODO: Add any initialization after the InitializeComponent call
            InitializeAdditionalComponents();
        }

        private void InitializeAdditionalComponents()
        {
            //
            // forceMouseContextMenuToolStripMenuItem
            //
            preventMouseLeavingContextMenuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            preventMouseLeavingContextMenuToolStripMenuItem.CheckOnClick = true;
            preventMouseLeavingContextMenuToolStripMenuItem.CheckedChanged += new EventHandler(preventMouseLeavingContextMenuToolStripMenuItem_CheckedChanged);
            preventMouseLeavingContextMenuToolStripMenuItem.Name = "fillMenuToolStripMenuItem";
            preventMouseLeavingContextMenuToolStripMenuItem.Text = "Prevent Mouse Leaving";

            //
            // fillMenuToolStripMenuItem
            //
            fillMenuToolStripMenuItem.CheckOnClick = true;
            fillMenuToolStripMenuItem.Click += new EventHandler( fillToolStripMenuItem_Click );
            fillMenuToolStripMenuItem.Name = "fillMenuToolStripMenuItem";
            fillMenuToolStripMenuItem.Text = "Fill Window";
            //
            // widescreenMenuToolStripMenuItem
            //
            widescreenMenuToolStripMenuItem.CheckOnClick = true;
            widescreenMenuToolStripMenuItem.Click += new EventHandler( widescreenToolStripMenuItem_Click );
            widescreenMenuToolStripMenuItem.Name = "widescreenMenuToolStripMenuItem";
            widescreenMenuToolStripMenuItem.Text = "Widescreen (16:9)";
            //
            // standardMenuToolStripMenuItem
            //
            standardMenuToolStripMenuItem.CheckOnClick = true;
            standardMenuToolStripMenuItem.Click += new EventHandler( standardToolStripMenuItem_Click );
            standardMenuToolStripMenuItem.Name = "standardMenuToolStripMenuItem";
            standardMenuToolStripMenuItem.Text = "Standard (4:3)";
            //
            // pspMenuToolStripMenuItem
            //
            pspMenuToolStripMenuItem.CheckOnClick = true;
            pspMenuToolStripMenuItem.Click += new EventHandler( pspToolStripMenuItem_Click );
            pspMenuToolStripMenuItem.Name = "pspMenuToolStripMenuItem";
            pspMenuToolStripMenuItem.Text = "PSP (480x272)";
            //
            // gameMenuResolutionToolStripMenuItem
            //
            gameResolutionMenuToolStripMenuItem.CheckOnClick = true;
            gameResolutionMenuToolStripMenuItem.Click += new EventHandler( gameResolutionToolStripMenuItem_Click );
            gameResolutionMenuToolStripMenuItem.Name = "gameResolutionMenuToolStripMenuItem";
            gameResolutionMenuToolStripMenuItem.Text = "Game Resolution";
            gameResolutionMenuToolStripMenuItem.Visible = false;
            //
            // fillContextMenuToolStripMenuItem
            //
            fillContextMenuToolStripMenuItem.CheckOnClick = true;
            fillContextMenuToolStripMenuItem.Click += new EventHandler( fillToolStripMenuItem_Click );
            fillContextMenuToolStripMenuItem.Name = "fillContextMenuToolStripMenuItem";
            fillContextMenuToolStripMenuItem.Text = "Fill Window";
            //
            // widescreenContextMenuToolStripMenuItem
            //
            widescreenContextMenuToolStripMenuItem.CheckOnClick = true;
            widescreenContextMenuToolStripMenuItem.Click += new EventHandler( widescreenToolStripMenuItem_Click );
            widescreenContextMenuToolStripMenuItem.Name = "widescreenContextMenuToolStripMenuItem";
            widescreenContextMenuToolStripMenuItem.Text = "Widescreen (16:9)";
            //
            // standardContextMenuToolStripMenuItem
            //
            standardContextMenuToolStripMenuItem.CheckOnClick = true;
            standardContextMenuToolStripMenuItem.Click += new EventHandler( standardToolStripMenuItem_Click );
            standardContextMenuToolStripMenuItem.Name = "standardContextMenuToolStripMenuItem";
            standardContextMenuToolStripMenuItem.Text = "Standard (4:3)";
            //
            // pspContextMenuToolStripMenuItem
            //
            pspContextMenuToolStripMenuItem.CheckOnClick = true;
            pspContextMenuToolStripMenuItem.Click += new EventHandler( pspToolStripMenuItem_Click );
            pspContextMenuToolStripMenuItem.Name = "pspContextMenuToolStripMenuItem";
            pspContextMenuToolStripMenuItem.Text = "PSP (480x272)";
            //
            // gameContextMenuResolutionToolStripMenuItem
            //
            gameResolutionContextMenuToolStripMenuItem.CheckOnClick = true;
            gameResolutionContextMenuToolStripMenuItem.Click += new EventHandler( gameResolutionToolStripMenuItem_Click );
            gameResolutionContextMenuToolStripMenuItem.Name = "gameResolutionContextMenuToolStripMenuItem";
            gameResolutionContextMenuToolStripMenuItem.Text = "Game Resolution";
            gameResolutionContextMenuToolStripMenuItem.Visible = false;
        }

        #region Enums
        private enum Resolution
        {
            Maximize,            // When docked, fills the render window inside the DockControl, otherwise makes the DockControl as large as the desktop.
            Widescreen,
            Standard,
            PSP,
            Game,
            Other,
        }
        #endregion

        #region Constants
        private float c_Widescreen_Ratio = 16.0f / 9.0f;
        private float c_Standard_Ratio = 4.0f / 3.0f;
        private float c_PSP_Ratio = 480.0f / 272.0f;
        #endregion

        #region Variables
        private static String sm_PlatformString;
        private static String sm_BuildConfigString;
        private static TD.SandDock.DocumentContainer sm_DocumentContainer;
        private static List<TD.SandDock.DockControl> sm_DockControls = new List<TD.SandDock.DockControl>();
        private static List<RenderWindow> sm_RenderWindows = new List<RenderWindow>();
        private static Dictionary<TD.SandDock.DockControl, RenderWindow> sm_DockControlToRenderWindow = new Dictionary<TD.SandDock.DockControl, RenderWindow>();

        private RageApplication m_RageApplication;
        private TD.SandDock.DockControl m_DockControl;

        private Resolution m_resolution = Resolution.Maximize;

        private bool m_displayImage = false;
        private string m_imageFilename = null;
        private FileStream m_imageFileStream = null;
        private FileSystemWatcher m_imageFileSystemWatcher = null;
        private Image m_image = null;

        private bool m_selfPaintWin32 = false;

        private Size m_gameResolution = new Size();
        private Point m_previousMouseLocation;
        private Rectangle m_clipRectangle;
        #endregion

        #region Properties
        public IntPtr PictureBoxHandle
        {
            get
            {
                return this.Handle;
            }
        }

        public RageApplication MyRageApplication
        {
            set
            {
                m_RageApplication = value;
            }
            get
            {
                return m_RageApplication;
            }
        }

        public TD.SandDock.DockControl DockControl
        {
            get
            {
                return m_DockControl;
            }
        }

        public bool DisplayImage
        {
            get
            {
                return m_displayImage;
            }
            set
            {
                if ( sm_PlatformString == "Win32" || sm_PlatformString == "Win64" )
                {
                    return;
                }

                m_displayImage = value;
            }
        }

        public string ImageFilename
        {
            get
            {
                return m_imageFilename;
            }
            set
            {
                if (sm_PlatformString == "Win32" || sm_PlatformString == "Win64")
                {
                    return;
                }

                if ( m_image != null )
                {
                    m_image.Dispose();
                    m_image = null;
                }

                if ( m_imageFileStream != null )
                {
                    m_imageFileStream.Close();
                    m_imageFileStream.Dispose();
                    m_imageFileStream = null;
                }

                if ( m_imageFileSystemWatcher != null )
                {
                    m_imageFileSystemWatcher.EnableRaisingEvents = false;
                    
                    if ( m_imageFilename != value )
                    {
                        m_imageFileSystemWatcher.Dispose();
                        m_imageFileSystemWatcher = null;
                    }
                }

                m_imageFilename = value;

                if ( !String.IsNullOrEmpty( value ) && File.Exists( value ) )
                {
                    try
                    {
                        m_imageFileStream = new FileStream( value, FileMode.Open, FileAccess.Read, FileShare.ReadWrite );

                        if ( m_imageFileSystemWatcher == null )
                        {
                            m_imageFileSystemWatcher = new FileSystemWatcher( Path.GetDirectoryName( value ), Path.GetFileName( value ) );
                            m_imageFileSystemWatcher.Changed += new FileSystemEventHandler( imageFileSystemWatcher_Changed );
                            m_imageFileSystemWatcher.NotifyFilter = NotifyFilters.LastWrite;
                        }

                        m_image = Image.FromStream( m_imageFileStream );

                        m_displayImage = true;

                        // force a redraw
                        this.Invalidate();                        
                    }
                    catch ( Exception e )
                    {
                        if ( m_image != null )
                        {
                            m_image.Dispose();
                            m_image = null;
                        }

                        if ( m_imageFileStream != null )
                        {
                            m_imageFileStream.Close();
                            m_imageFileStream.Dispose();
                            m_imageFileStream = null;
                        }

                        _log.ToolException(e, "Set ImageFilename error: '{0}'.", value);

                        m_displayImage = false;
                    }

                    if ( m_imageFileSystemWatcher != null )
                    {
                        m_imageFileSystemWatcher.EnableRaisingEvents = true;
                    }
                }
                else
                {
                    m_displayImage = false;

                    // force a redraw
                    this.Invalidate();
                }
            }
        }

        /// <summary>
        /// Indicates whether we need to paint ourselves, or the Win32 application will do the painting.
        /// </summary>
        public bool SelfPaintWin32
        {
            get
            {
                return m_selfPaintWin32;
            }
            set
            {
                m_selfPaintWin32 = value;
            }
        }

        public static string PlatformString
        {
            get
            {
                return sm_PlatformString;
            }

            set
            {
                sm_PlatformString = value;
            }
        }

        public static String BuildConfigString
        {
            get { return sm_BuildConfigString; }
            set { sm_BuildConfigString = value; }
        }

        public static TD.SandDock.DocumentContainer DocumentContainer
        {
            set
            {
                sm_DocumentContainer = value;
            }
        }
        #endregion

        #region Overrides
        protected override bool IsInputKey( Keys keyData )
        {
            if ( keyData == Keys.Tab ||
                keyData == Keys.Left ||
                keyData == Keys.Right ||
                keyData == Keys.Up ||
                keyData == Keys.Down )
            {
                return true;
            }
            else
                return base.IsInputKey( keyData );

        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose( bool disposing )
        {
            if ( disposing )
            {
                m_drawBrush.Dispose();
                m_drawFont.Dispose();

                sm_DockControlToRenderWindow.Remove(DockControl);
                sm_DockControls.Remove(DockControl);

                if ((DockControl.Controls.Count > 0) && (DockControl.Controls[0] is RenderWindow))
                {
                    sm_RenderWindows.Remove(DockControl.Controls[0] as RenderWindow);
                }

                if ( components != null )
                {
                    components.Dispose();
                }

                // this will clean up the image stuff
                this.ImageFilename = null;
            }
            base.Dispose( disposing );
        }
        #endregion

        #region Component Designer generated code
        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            SetStyle( ControlStyles.AllPaintingInWmPaint, true );
            SetStyle( ControlStyles.UserPaint, true );
            SetStyle( ControlStyles.Opaque, true );
            SetStyle( ControlStyles.ResizeRedraw, true );
            this.UpdateStyles();

            // 
            // RenderWindow
            // 
            this.Name = "RenderWindow";
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler( this.RenderWindow_KeyPress );
            this.MouseUp += new System.Windows.Forms.MouseEventHandler( this.RenderWindow_MouseUp );
            this.Paint += new System.Windows.Forms.PaintEventHandler( this.RenderWindow_Paint );
            this.KeyUp += new System.Windows.Forms.KeyEventHandler( this.RenderWindow_KeyUp );
            this.KeyDown += new System.Windows.Forms.KeyEventHandler( this.RenderWindow_KeyDown );
            this.MouseMove += new System.Windows.Forms.MouseEventHandler( this.RenderWindow_MouseMove );
            this.MouseLeave += new System.EventHandler(this.RenderWindow_MouseLeave);
            this.MouseWheel += new System.Windows.Forms.MouseEventHandler( this.RenderWindow_MouseWheel );
            this.MouseDown += new System.Windows.Forms.MouseEventHandler( this.RenderWindow_MouseDown );
        }
        #endregion

        #region Event Handlers
        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message message, System.Windows.Forms.Keys keyData)
        {
            if (Settings.Default.NumPadOverride)
            {
                // url:bugstar:6909380 - Gen9 - The cloud is not refreshed when using Shift+NumPad3 (twice) on the debug keyboard.
                //
                // When a user is using the PC keyboard the OS will overrider the num lock key, this is the default behavior and 
                // this code overrides the keys sent over to make sure the numpad values are sent as expected similar to a physical
                // keyboard plugged into a kit.
                //

                const int WM_KEYDOWN = 0x0100;
                const int WM_KEYUP = 0x0101;
                if ((message.Msg == WM_KEYDOWN) || (message.Msg == WM_KEYUP))
                {
                    int lParam = message.LParam.ToInt32();
                    int scancode = (lParam >> 16) & 0xff;

                    // UK | US keyboard Scancodes for the numberpad
                    const int KEY_NUMPAD9 = 0x49;
                    const int KEY_NUMPAD8 = 0x48;
                    const int KEY_NUMPAD7 = 0x47;
                    const int KEY_NUMPAD6 = 0x4D;
                    const int KEY_NUMPAD5 = 0x4C;
                    const int KEY_NUMPAD4 = 0x4B;
                    const int KEY_NUMPAD3 = 0x51;
                    const int KEY_NUMPAD2 = 0x50;
                    const int KEY_NUMPAD1 = 0x4F;
                    const int KEY_NUMPAD0 = 0x52;
                    const int KEY_DECEMAL = 0x53;

                    Keys keyoverride = Keys.None;
                    switch (scancode)
                    {
                        case KEY_NUMPAD9:
                            keyoverride = Keys.NumPad9;
                            break;
                        case KEY_NUMPAD8:
                            keyoverride = Keys.NumPad8;
                            break;
                        case KEY_NUMPAD7:
                            keyoverride = Keys.NumPad7;
                            break;
                        case KEY_NUMPAD6:
                            keyoverride = Keys.NumPad6;
                            break;
                        case KEY_NUMPAD5:
                            keyoverride = Keys.NumPad5;
                            break;
                        case KEY_NUMPAD4:
                            keyoverride = Keys.NumPad4;
                            break;
                        case KEY_NUMPAD3:
                            keyoverride = Keys.NumPad3;
                            break;
                        case KEY_NUMPAD2:
                            keyoverride = Keys.NumPad2;
                            break;
                        case KEY_NUMPAD1:
                            keyoverride = Keys.NumPad1;
                            break;
                        case KEY_NUMPAD0:
                            keyoverride = Keys.NumPad0;
                            break;
                        case KEY_DECEMAL:
                            keyoverride = Keys.Decimal;
                            break;
                    }

                    if (keyoverride != Keys.None)
                    {
                        if (keyData.HasFlag(System.Windows.Forms.Keys.Shift))
                        {
                            keyoverride |= System.Windows.Forms.Keys.Shift;
                        }
                        if (keyData.HasFlag(System.Windows.Forms.Keys.Control))
                        {
                            keyoverride |= System.Windows.Forms.Keys.Control;
                        }
                        if (keyData.HasFlag(System.Windows.Forms.Keys.Alt))
                        {
                            keyoverride |= System.Windows.Forms.Keys.Alt;
                        }

                        if (message.Msg == WM_KEYDOWN)
                        {
                            RenderWindow_KeyDown(null, new KeyEventArgs(keyoverride));
                        }
                        else
                        {
                            RenderWindow_KeyUp(null, new KeyEventArgs(keyoverride));
                        }

                        return true;
                    }
                }
            }
            return base.ProcessCmdKey(ref message, keyData);
        }

        private void dockControl_Closed( object sender, System.EventArgs e )
        {
            TD.SandDock.DockControl dockControl = sender as TD.SandDock.DockControl;
            if ( dockControl != null )
            {
                if ( dockControl.CloseAction == TD.SandDock.DockControlCloseAction.Dispose )
                {
                }
            }
        }

        private void dockControl_ClientSizeChanged( object sender, EventArgs e )
        {
            // Force the window to update its size and position
            UpdateSize();
        }

        protected void RenderWindow_KeyDown( object sender, System.Windows.Forms.KeyEventArgs e )
        {
            if ( MyRageApplication != null && MyRageApplication.InputHandler != null )
            {
                MyRageApplication.InputHandler.SendKeyboardEvent( Input.CommandKeyboard.QUEUE_KEYDOWN, e.KeyCode );
            }
        }

        protected void RenderWindow_KeyPress( object sender, System.Windows.Forms.KeyPressEventArgs e )
        {
            // nothing needed?
        }

        protected void RenderWindow_KeyUp( object sender, System.Windows.Forms.KeyEventArgs e )
        {
            if ( MyRageApplication != null )
            {
                // let other things process input:
                bool sendEventToApplication = MyRageApplication.KeyProcessor.ProcessKeyUp( e );

                if ( sendEventToApplication && MyRageApplication.InputHandler != null )
                {
                    MyRageApplication.InputHandler.SendKeyboardEvent( Input.CommandKeyboard.QUEUE_KEYUP, e.KeyCode );
                }
            }
        }

        protected void RenderWindow_MouseDown( object sender, System.Windows.Forms.MouseEventArgs e )
        {
            if (MyRageApplication != null && MyRageApplication.InputHandler != null)
            {
                MyRageApplication.InputHandler.SendMouseEvent(
                    e.Button,
                    true,
                    e.X - m_clipRectangle.X,
                    e.Y - m_clipRectangle.Y,
                    e.Delta,
                    this.Width - m_clipRectangle.X,
                    this.Height - m_clipRectangle.Y);
            }
        }

        protected void RenderWindow_MouseMove( object sender, System.Windows.Forms.MouseEventArgs e )
        {
            m_previousMouseLocation = e.Location;

            if (MyRageApplication != null && MyRageApplication.InputHandler != null)
            {
                MyRageApplication.InputHandler.SendMouseEvent(
                    e.X - m_clipRectangle.X,
                    e.Y - m_clipRectangle.Y,
                    e.Delta,
                    this.Width - m_clipRectangle.X,
                    this.Height - m_clipRectangle.Y);
            }
        }

        protected void RenderWindow_MouseLeave(object sender, System.EventArgs e)
        {
            bool escapeCode = (System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.LeftAlt) ||
                System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.RightAlt)) &&
                (System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.LeftShift) ||
                System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.RightShift));

            if (preventMouseLeavingContextMenuToolStripMenuItem.Checked && escapeCode)
            {
                preventMouseLeavingContextMenuToolStripMenuItem.Checked = false;
            }
            else if (m_previousMouseLocation != null && preventMouseLeavingContextMenuToolStripMenuItem.Checked)
            {
                System.Windows.Forms.Cursor.Position = PointToScreen(m_previousMouseLocation);
            }
        }

        protected void RenderWindow_MouseUp( object sender, System.Windows.Forms.MouseEventArgs e )
        {
            if (MyRageApplication != null && MyRageApplication.InputHandler != null)
            {
                MyRageApplication.InputHandler.SendMouseEvent(
                    e.Button,
                    false,
                    e.X - m_clipRectangle.X,
                    e.Y - m_clipRectangle.Y,
                    e.Delta,
                    this.Width - m_clipRectangle.X,
                    this.Height - m_clipRectangle.Y);
            }
        }

        protected void RenderWindow_MouseWheel( object sender, System.Windows.Forms.MouseEventArgs e )
        {
            if (MyRageApplication != null && MyRageApplication.InputHandler != null)
            {
                MyRageApplication.InputHandler.SendMouseEvent(
                    e.X - m_clipRectangle.X,
                    e.Y - m_clipRectangle.Y,
                    e.Delta,
                    this.Width - m_clipRectangle.X,
                    this.Height - m_clipRectangle.Y);
            }
        }

        protected void RenderWindow_Paint( object sender, System.Windows.Forms.PaintEventArgs e )
        {
            m_clipRectangle = e.ClipRectangle;

            if (sm_PlatformString != "Win32" || sm_PlatformString == "Win64")
            {
                if ( m_displayImage )
                {
                    if ( m_image != null )
                    {
                        e.Graphics.DrawImage( m_image, e.ClipRectangle );
                    }
                }
                else
                {
                    e.Graphics.Clear( System.Drawing.Color.Gray );
                }
            }
            else if ( m_selfPaintWin32 )
            {
                e.Graphics.Clear( System.Drawing.Color.Gray );
            }

            if (preventMouseLeavingContextMenuToolStripMenuItem.Checked)
            {
                e.Graphics.DrawString("Hold ALT+SHIFT when leaving the viewport to disable mouse lock", m_drawFont, m_drawBrush, new PointF(10.0f, 10.0f));
            }
        }

        protected void toolStripMenuItem_DropDownOpening( object sender, EventArgs e )
        {
            this.fillMenuToolStripMenuItem.Checked = ( m_resolution == Resolution.Maximize );
            this.widescreenMenuToolStripMenuItem.Checked = ( m_resolution == Resolution.Widescreen );
            this.standardMenuToolStripMenuItem.Checked = ( m_resolution == Resolution.Standard );
            this.pspMenuToolStripMenuItem.Checked = ( m_resolution == Resolution.PSP );

            if ( this.gameResolutionMenuToolStripMenuItem != null )
            {
                this.gameResolutionMenuToolStripMenuItem.Checked = ( m_resolution == Resolution.Game );
            }
        }

        protected void preventMouseLeavingContextMenuToolStripMenuItem_CheckedChanged(Object sender, System.EventArgs e)
        {
            Invalidate();
        }

        protected void fillToolStripMenuItem_Click( Object sender, System.EventArgs e )
        {
            m_resolution = Resolution.Maximize;
            UpdateSize();
        }

        protected void widescreenToolStripMenuItem_Click( Object sender, System.EventArgs e )
        {
            m_resolution = Resolution.Widescreen;
            UpdateSize();
        }

        protected void standardToolStripMenuItem_Click( Object sender, System.EventArgs e )
        {
            m_resolution = Resolution.Standard;
            UpdateSize();
        }

        protected void pspToolStripMenuItem_Click( Object sender, System.EventArgs e )
        {
            m_resolution = Resolution.PSP;
            UpdateSize();
        }

        protected void gameResolutionToolStripMenuItem_Click( object sender, EventArgs e )
        {
            m_resolution = Resolution.Game;
            UpdateSize();
        }

        // lame toggle.  there's got to be a better way to deal with receiving 2 Changed events for each new screenshot
        private bool m_imageFileChangedToggle = true;
        private void imageFileSystemWatcher_Changed( object sender, FileSystemEventArgs e )
        {
            m_imageFileChangedToggle = !m_imageFileChangedToggle;
            if ( m_imageFileChangedToggle )
            {
                // reload the same file
                this.ImageFilename = e.FullPath;
            }
        }
        #endregion        

        #region Public Functions
        /// <summary>
        /// Adds the specified resolution to the menu options, if it doesn't exist already.
        /// </summary>
        /// <param name="res"></param>
        public void SetGameResolution( Size res )
        {
            m_gameResolution = res;

            this.gameResolutionMenuToolStripMenuItem.Text = "Game Resolution (" + m_gameResolution.Width + "x" + m_gameResolution.Height + ")";
            this.gameResolutionMenuToolStripMenuItem.Visible = true;

            this.gameResolutionContextMenuToolStripMenuItem.Text = "Game Resolution (" + m_gameResolution.Width + "x" + m_gameResolution.Height + ")";
            this.gameResolutionContextMenuToolStripMenuItem.Visible = true;

            UpdateSize();
        }

        public void DetachFromLayoutManager()
        {
            Debug.Assert(!sm_DocumentContainer.IsDisposed, "DocumentContainer has been disposed");
            if (sm_DocumentContainer.LayoutSystem.LayoutSystems.Count > 0)
            {
                bool isInManager = false;

                // check to see if the manager knows about it:
                TD.SandDock.DockControl[] dockControls = sm_DocumentContainer.Manager.GetDockControls();
                foreach (TD.SandDock.DockControl checkControl in dockControls)
                {
                    if (DockControl == checkControl)
                    {
                        isInManager = true;
                        break;
                    }
                }

                if (isInManager == true)
                {
                    TD.SandDock.ControlLayoutSystem controlSystem = FindControlLayoutSystem(sm_DocumentContainer.LayoutSystem);
                    Debug.Assert(controlSystem != null, "controlSystem != null");
                    if (controlSystem != null && controlSystem.Controls.Contains(DockControl) == true)
                    {
                        controlSystem.Controls.Remove(DockControl);
                    }
                }
            }

        }

        public void AttachToLayoutManager()
        {
            // find the widget view dock:
            Debug.Assert(!sm_DocumentContainer.IsDisposed, "DocumentContainer has been disposed");
            if (sm_DocumentContainer.LayoutSystem.LayoutSystems.Count > 0)
            {
                bool isInManager = false;

                // check to see if the manager knows about it:
                TD.SandDock.DockControl[] dockControls = sm_DocumentContainer.Manager.GetDockControls();
                foreach (TD.SandDock.DockControl checkControl in dockControls)
                {
                    if (DockControl == checkControl)
                    {
                        isInManager = true;
                        break;
                    }
                }

                if (isInManager == false)
                {
                    TD.SandDock.ControlLayoutSystem controlSystem = FindControlLayoutSystem(sm_DocumentContainer.LayoutSystem);
                    Debug.Assert(controlSystem != null, "controlSystem != null");
                    if (controlSystem != null && controlSystem.Controls.Contains(DockControl) == false)
                    {
                        controlSystem.Controls.Add(DockControl);
                    }
                }
            }
            else
            {
                //Debug.Fail("will fix this");
                TD.SandDock.ControlLayoutSystem controlSystem = sm_DocumentContainer.CreateNewLayoutSystem(DockControl, new SizeF(DockControl.Size));
                if (!sm_DocumentContainer.LayoutSystem.LayoutSystems.Contains(controlSystem))
                {
                    sm_DocumentContainer.LayoutSystem.LayoutSystems.Add(controlSystem);
                }
            }

        }
        #endregion
        
        #region Private Functions
        private void UpdateSize()
        {
            try
            {
                Resolution res = m_resolution;

                if ( res == Resolution.Game && m_gameResolution.IsEmpty )
                {
                    res = Resolution.Maximize;
                }

                Size parentSize = m_DockControl.ClientSize;

                // Based on resolution enum, determine which aspect ratio to use
                float aspectRatio;
                switch ( res )
                {
                    case Resolution.Maximize:
                        aspectRatio = ( (float)parentSize.Width ) / ( (float)parentSize.Height );
                        break;
                    case Resolution.Widescreen:
                        aspectRatio = c_Widescreen_Ratio;
                        break;
                    case Resolution.Standard:
                        aspectRatio = c_Standard_Ratio;
                        break;
                    case Resolution.PSP:
                        aspectRatio = c_PSP_Ratio;
                        break;
                    case Resolution.Game:
                        aspectRatio = ( (float)m_gameResolution.Width ) / ( (float)m_gameResolution.Height );
                        break;
                    default:
                        aspectRatio = 1.0f;
                        break;
                }

                // Maximize width and height while maintaining its aspect ratio
                int width = Convert.ToInt32( parentSize.Height * aspectRatio );
                int height = Convert.ToInt32( parentSize.Width / aspectRatio );
                if ( parentSize.Width > parentSize.Height && width <= parentSize.Width )
                {
                    height = parentSize.Height;
                }
                else
                {
                    width = parentSize.Width;
                }
                this.Size = new Size( width, height );

                // Position the window in the center of the dock window
                int x = ( parentSize.Width / 2 ) - ( this.Width / 2 );
                int y = ( parentSize.Height / 2 ) - ( this.Height / 2 );
                this.Location = new Point( x, y );
            }
            catch
            {

            }
        }
        #endregion

        #region Virtual Functions
        public virtual void DeserializeView( XmlTextReader xmlReader, String exePathName )
        {
            string resolution = xmlReader["resolution"];
            if ( !String.IsNullOrEmpty( resolution ) )
            {
                try
                {
                    m_resolution = (Resolution)Enum.Parse( typeof( Resolution ), resolution );
                }
                catch
                {
                }
            }

            string gameResolution = xmlReader["gameResolution"];
            if ( !String.IsNullOrEmpty( gameResolution ) )
            {
                string[] split = gameResolution.Split( ',' );
                int w = 0;
                int h = 0;
                if ( split.Length >= 1 )
                {
                    w = int.Parse( split[0] );

                    if ( split.Length >= 2 )
                    {
                        h = int.Parse( split[1] );
                    }
                    else
                    {
                        h = w;
                    }
                }

                SetGameResolution( new Size( w, h ) );
            }

            UpdateSize();
        }

        protected virtual void SerializeView( XmlTextWriter xmlWriter, String exePathName )
        {            
            xmlWriter.WriteAttributeString( "guid", this.DockControl.Guid.ToString() );
            xmlWriter.WriteAttributeString( "appname", this.MyRageApplication.ExeName );
            xmlWriter.WriteAttributeString( "start", this.MyRageApplication.IsStarted.ToString() );
            xmlWriter.WriteAttributeString( "resolution", m_resolution.ToString() );
            xmlWriter.WriteAttributeString( "gameResolution", m_gameResolution.Width + "," + m_gameResolution.Height );
        }

        public virtual void PopulateContextMenu( ContextMenuStrip contextMenu )
        {
            contextMenu.Items.Clear();

            this.fillContextMenuToolStripMenuItem.Checked = ( m_resolution == Resolution.Maximize );
            this.widescreenContextMenuToolStripMenuItem.Checked = ( m_resolution == Resolution.Widescreen );
            this.standardContextMenuToolStripMenuItem.Checked = ( m_resolution == Resolution.Standard );
            this.pspContextMenuToolStripMenuItem.Checked = ( m_resolution == Resolution.PSP );

            if ( this.gameResolutionContextMenuToolStripMenuItem != null )
            {
                this.gameResolutionContextMenuToolStripMenuItem.Checked = ( m_resolution == Resolution.Game );
            }

            contextMenu.Items.Add(this.fillContextMenuToolStripMenuItem);
            contextMenu.Items.Add(this.widescreenContextMenuToolStripMenuItem);
            contextMenu.Items.Add(this.standardContextMenuToolStripMenuItem);
            contextMenu.Items.Add(this.pspContextMenuToolStripMenuItem);
            contextMenu.Items.Add(this.gameResolutionContextMenuToolStripMenuItem);
            contextMenu.Items.Add(new ToolStripSeparator());
            contextMenu.Items.Add(this.preventMouseLeavingContextMenuToolStripMenuItem);
        }
        #endregion

        #region Static Functions
        public static TD.SandDock.ControlLayoutSystem FindControlLayoutSystem( TD.SandDock.SplitLayoutSystem splitLayoutSystem )
        {
            if ( splitLayoutSystem != null )
            {
                foreach ( TD.SandDock.LayoutSystemBase layoutSystemBase in splitLayoutSystem.LayoutSystems )
                {
                    TD.SandDock.ControlLayoutSystem controlSystem = layoutSystemBase as TD.SandDock.ControlLayoutSystem;
                    if ( controlSystem != null )
                    {
                        return controlSystem;
                    }
                    else
                    {
                        // recurse:
                        return FindControlLayoutSystem( layoutSystemBase as TD.SandDock.SplitLayoutSystem );
                    }
                }
            }

            return null;
        }

        public static RenderWindow CreateWindow( BankManager bankMgr, TD.SandDock.DockControl dockControl, ILog log )
        {
            // create the widget view:
            //RenderWindow widgetView = createViewDel(bankMgr,documentContainer);
            RenderWindow renderWindow = new RenderWindow(log); // only one type for now

            dockControl.Controls.Add( renderWindow );
            dockControl.AllowClose = false;
            dockControl.DockingRules = new DockingRules( true, true, true );
            dockControl.FloatingSize = new System.Drawing.Size( 550, 400 );
            dockControl.Location = new System.Drawing.Point( 1, 21 );
            dockControl.Name = "gameViewTabbedDocument";
            dockControl.ShowOptions = false;
            dockControl.Size = new System.Drawing.Size( 626, 391 );
            dockControl.TabIndex = 0;
            dockControl.ClientSizeChanged += new EventHandler( renderWindow.dockControl_ClientSizeChanged );
            dockControl.Closed += new EventHandler( renderWindow.dockControl_Closed );
            dockControl.Manager = sm_DocumentContainer.Manager;

            renderWindow.Anchor = (AnchorStyles)(AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top);
            renderWindow.Dock = DockStyle.None;
            renderWindow.m_DockControl = dockControl;
            renderWindow.UpdateSize();

            //renderWindow.AttachToLayoutManager();

            sm_DockControls.Add( dockControl );
            sm_RenderWindows.Add( renderWindow );
            sm_DockControlToRenderWindow.Add( dockControl, renderWindow );

            return renderWindow;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="renderWindow"></param>
        public static void DestroyWindow(RenderWindow renderWindow)
        {
            renderWindow.DetachFromLayoutManager();
            renderWindow.DockControl.Manager = null;
            renderWindow.DockControl.Close();
        }

        public static void SerializeViews( XmlTextWriter xmlWriter, String exePathName )
        {
            xmlWriter.WriteStartElement( "RenderWindows" );

            foreach ( RenderWindow renderWindow in sm_RenderWindows )
            {
                xmlWriter.WriteWhitespace( "\n" );
                xmlWriter.WriteStartElement( renderWindow.GetType().ToString() );
                {
                    renderWindow.SerializeView( xmlWriter, exePathName );
                }
                xmlWriter.WriteEndElement();
            }

            xmlWriter.WriteWhitespace( "\n" );
            xmlWriter.WriteEndElement();
        }

        public static RenderWindow DockControlToRenderWindow( TD.SandDock.DockControl dockControl )
        {
            if ( sm_DockControlToRenderWindow.ContainsKey( dockControl ) )
            {
                return sm_DockControlToRenderWindow[dockControl];
            }

            return null;
        }

        public static ToolStripMenuItem CreateMenuItemForDockControl( TD.SandDock.DockControl dockControl )
        {
            ToolStripMenuItem menuItem = MainWindow.CreateMenuItemForDockControl( dockControl );

            // Add menu items for setting resolution
            RenderWindow renderWindow = RenderWindow.DockControlToRenderWindow( dockControl );
            if ( renderWindow != null )
            {
                menuItem.DropDownOpening += new EventHandler( renderWindow.toolStripMenuItem_DropDownOpening );

                menuItem.DropDownItems.Add( new ToolStripSeparator() );
                menuItem.DropDownItems.Add( renderWindow.preventMouseLeavingContextMenuToolStripMenuItem);
                menuItem.DropDownItems.Add( new ToolStripSeparator() );
                menuItem.DropDownItems.Add( renderWindow.fillMenuToolStripMenuItem );
                menuItem.DropDownItems.Add( renderWindow.widescreenMenuToolStripMenuItem );
                menuItem.DropDownItems.Add( renderWindow.standardMenuToolStripMenuItem );
                menuItem.DropDownItems.Add( renderWindow.pspMenuToolStripMenuItem );
                if ( renderWindow.gameResolutionMenuToolStripMenuItem != null )
                {
                    menuItem.DropDownItems.Add( renderWindow.gameResolutionMenuToolStripMenuItem );
                }
            }

            return menuItem;
        }

        public static void ClearReloadStatics()
        {
            sm_DockControls.Clear();
            sm_RenderWindows.Clear();
            sm_DockControlToRenderWindow.Clear();
        }

        public static void ClearStatics()
        {
            sm_DocumentContainer = null;

            ClearReloadStatics();
        }
        #endregion
    }
}
