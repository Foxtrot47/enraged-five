﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ragCore;
using System.Drawing;
using System.Collections;
using ragWidgets;

namespace rag
{


    public class WidgetTreeViewManager
    {
        protected Dictionary<WidgetGroupBase, WidgetTreeNode> m_TreeViewNodes = new Dictionary<WidgetGroupBase, WidgetTreeNode>();
        protected List<WidgetTreeNode> m_originalTreeViewNodeOrder = new List<WidgetTreeNode>();

        private static Font s_DefaultFont = SystemFonts.DefaultFont;
        private static Font s_ModifiedFont = new Font( s_DefaultFont, FontStyle.Underline );

        private TreeView m_Treeview;
        private IWidgetView _widgetView;

        public WidgetTreeViewManager( TreeView treeView, IWidgetView widgetView )
        {
            m_Treeview = treeView;
            _widgetView = widgetView;
        }



        public void WidgetBank_AddBankEvent( BankPipe pipe, WidgetBank bank )
        {
            bool hidden = ViewUtils.IsBankHidden(_widgetView, bank);
            
            m_Treeview.SuspendLayout();
            PopulateTreeView(bank, null, m_Treeview, !hidden);
            m_Treeview.ResumeLayout();

            if (hidden)
            {
                ViewUtils.SetBankHidden(_widgetView, bank, true);
            }
        }

        public void WidgetBank_RemoveBankEvent( BankPipe pipe, WidgetBank bank )
        {
            m_Treeview.SuspendLayout();
            UnpopulateTreeView( bank, m_Treeview );
            m_Treeview.ResumeLayout();
        }

        private bool GetNode( TreeView treeView, WidgetGroupBase widget, out WidgetTreeNode node )
        {
            return m_TreeViewNodes.TryGetValue( widget, out node );
        }

        public void OnChildAdded( WidgetGroupBase parent, Widget child )
        {
            if ( child is WidgetGroupBase )
            {
                WidgetTreeNode node;

                if ( GetNode( m_Treeview, parent, out node ) )
                {
                    m_Treeview.SuspendLayout();
                    PopulateTreeView( (WidgetGroupBase)child, node, m_Treeview );
                    m_Treeview.ResumeLayout();
                }
            }
        }

        internal void OnChildRemoved( WidgetGroupBase parent, Widget child )
        {
            m_Treeview.SuspendLayout();
            UnpopulateTreeView( child, m_Treeview );
            m_Treeview.ResumeLayout();
        }

        private void PopulateTreeView( WidgetGroupBase child, WidgetTreeNode root, TreeView treeView, bool addToRoot = true )
        {
            WidgetTreeNode dummyNode;
            if ( GetNode( treeView, child, out dummyNode ) )
            {
                return;
            }

            int imageindex = child is WidgetBank ? 0 : 1; // todo make pretty code

            WidgetTreeNode newNode = new WidgetTreeNode( child, imageindex );
            m_TreeViewNodes.Add( child, newNode );
            if ( root != null )
            {
                if (addToRoot)
                {
                    root.Nodes.Add(newNode);
                }

                root.OriginalWidgetTreeNodeOrder.Add( newNode );
            }
            else
            {
                if (addToRoot)
                {
                    treeView.Nodes.Add(newNode);
                }

                m_originalTreeViewNodeOrder.Add( newNode );
            }

            // loop through children and create views for the children:
            foreach ( Widget widget in child.WidgetList )
            {
                if ( widget is WidgetGroupBase )
                {
                    PopulateTreeView( (WidgetGroupBase)widget, newNode, treeView );
                }
            }
        }

        private void UnpopulateTreeView( Widget child, TreeView treeView )
        {
            WidgetGroupBase group = child as WidgetGroupBase;
            if (group != null)
            {
                WidgetTreeNode oldNode;
                if ( GetNode( treeView, group, out oldNode ) )
                {
                    foreach ( Widget widget in group.WidgetList )
                    {
                        UnpopulateTreeView( widget, treeView );
                    }

                    if ( oldNode == treeView.SelectedNode )
                    {
                        treeView.SelectedNode = null;
                    }

                    if ( oldNode.Parent != null )
                    {
                        (oldNode.Parent as WidgetTreeNode).OriginalWidgetTreeNodeOrder.Remove( oldNode );

                        oldNode.Parent.Nodes.Remove( oldNode );
                    }
                    else
                    {
                        m_originalTreeViewNodeOrder.Remove( oldNode );
                        treeView.Nodes.Remove( oldNode );
                    }

                    System.Diagnostics.Debug.Assert( m_TreeViewNodes.ContainsKey( group ) );
                    m_TreeViewNodes.Remove( group );
                }
            }

        }



        public void PopulateTreeViewForAllBanks( IBankManager bankMgr )
        {
            WidgetGroupBase.ChildAdded += OnChildAdded;
            WidgetGroupBase.ChildRemoved += OnChildRemoved;
            WidgetGroupBase.WidgetDestroyed += OnWidgetDestroyed;
            WidgetGroupBase.WidgetModified += UpdateWindow;

            WidgetBank.AddBankEvent += WidgetBank_AddBankEvent;
            WidgetBank.RemoveBankEvent += WidgetBank_RemoveBankEvent;

            ViewSettings.SettingsChanged += ViewSettings_SettingsChanged;

            foreach ( WidgetBank bank in bankMgr.AllBanks )
            {
                PopulateTreeView( bank, null, m_Treeview );
            }
        }

        public void UnpopulateTreeViewForAllBanks( IBankManager bankMgr )
        {
            foreach ( WidgetBank bank in bankMgr.AllBanks )
            {
                UnpopulateTreeView( bank, m_Treeview );
            }

            WidgetGroupBase.ChildAdded -= OnChildAdded;
            WidgetGroupBase.ChildRemoved -= OnChildRemoved;
            WidgetGroupBase.WidgetDestroyed -= OnWidgetDestroyed;
            WidgetGroupBase.WidgetModified -= UpdateWindow;

            WidgetBank.AddBankEvent -= WidgetBank_AddBankEvent;
            WidgetBank.RemoveBankEvent -= WidgetBank_AddBankEvent;

            ViewSettings.SettingsChanged -= ViewSettings_SettingsChanged;
        }


        internal void OnWidgetDestroyed( Widget widgetGroup )
        {
            if ( !(widgetGroup is WidgetGroup) ) // ugly, todo fix.
            {
                return;
            }

            m_Treeview.SuspendLayout();
            UnpopulateTreeView( widgetGroup, m_Treeview );
            m_Treeview.ResumeLayout();
        }


        public void UnsortTreeView()
        {
            m_Treeview.SuspendLayout();

            m_Treeview.Nodes.Clear();

            foreach ( WidgetTreeNode widgetNode in m_originalTreeViewNodeOrder )
            {
                m_Treeview.Nodes.Add( widgetNode );

                widgetNode.Unsort();
            }

            m_Treeview.ResumeLayout();

        }

        public List<WidgetTreeNode> GetOriginalTreeViewNodeOrder()
        {
            return m_originalTreeViewNodeOrder;
        }





        //////////////////////////////////////////////////////////////////////////
        /// Tree node customization stuff
        /// 


        void ViewSettings_SettingsChanged( object sender, EventArgs e )
        {
            m_Treeview.SuspendLayout();
            foreach ( TreeNode treenode in m_Treeview.Nodes )
            {
                ClearTreeNodeCustomizationsRecursively( treenode );
            }
            m_Treeview.ResumeLayout();
        }

        private void ClearTreeNodeCustomizationsRecursively( TreeNode root )
        {
            root.BackColor = m_Treeview.BackColor;
            root.NodeFont = s_DefaultFont;

            foreach ( TreeNode treenode in root.Nodes )
            {
                ClearTreeNodeCustomizationsRecursively( treenode );
            }
        }


        internal void UpdateWindow( Widget widget )
        {
            if ( !ViewSettings.ShowModifiedColours )
            {
                return;
            }

            WidgetGroupBase widgetGroup = widget as WidgetGroupBase;
            if ( widgetGroup == null )
            {
                // This shouldn't happen afaik
                return;
            }

            WidgetTreeNode treenode;
            if ( GetNode( m_Treeview, widgetGroup, out treenode ) )
            {
                if ( widgetGroup.IsModified() && treenode.NodeFont != s_ModifiedFont )
                {
                    m_Treeview.SuspendLayout();
                    treenode.BackColor = ViewSettings.ModificationColor;
                    treenode.NodeFont = s_ModifiedFont;
                    m_Treeview.ResumeLayout();
                }
                else if ( !widgetGroup.IsModified() && treenode.NodeFont != s_DefaultFont )
                {
                    m_Treeview.SuspendLayout();
                    treenode.BackColor = m_Treeview.BackColor;
                    treenode.NodeFont = s_DefaultFont;
                    m_Treeview.ResumeLayout();
                }
            }
        }
    }
}
