using System;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.ComponentModel;
using System.Runtime.InteropServices;
using RSG.Base.Logging;

namespace rag
{
	public class HiResTimer
	{
		[DllImport("Kernel32.dll", SetLastError=true)]
		private static extern bool
			QueryPerformanceCounter(out long lpPerformanceCount);

		[DllImport("Kernel32.dll", SetLastError=true)]
		private static extern bool
			QueryPerformanceFrequency(out long lpFrequency);

		public static long Ticks
		{
			get
			{
				long t;
				QueryPerformanceCounter(out t);
				return t;
			}
		}

		public static long TicksPerSecond
		{
			get
			{
				long freq;
				QueryPerformanceFrequency(out freq);
				return freq;
			}
		}
	}    
	
	/// <summary>
	/// Delegate used by the events StdOutReceived and
    /// StdErrReceived...
    /// </summary>
    public delegate void DataReceivedHandler(object sender,
        DataReceivedEventArgs e);

    /// <summary>
    /// Event Args for above delegate
    /// </summary>
    public class DataReceivedEventArgs : EventArgs
    {
        /// <summary>
        /// The text that was received
        /// </summary>
        public string Text;
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="text">The text that was received for this event to be triggered.</param>
        public DataReceivedEventArgs(string text)
        {
            Text = text;
        }
    }

    /// <summary>
    /// This class can launch a process (like a bat file, perl
    /// script, etc) and return all of the StdOut and StdErr
    /// to GUI app for display in textboxes, etc.
    /// </summary>
    /// <remarks>
    /// This class (c) 2003 Michael Mayer
    /// Use it as you like (public domain licensing).
    /// Please post any bugs / fixes to the page where
    /// you downloaded this code.
    /// </remarks>
	public class ProcessCaller : AsyncOperation
	{

        /// <summary>
        /// The command to run (should be made into a property)
        /// </summary>
        public string FileName;
        /// <summary>
        /// The Arguments for the cmd (should be made into a property)
        /// </summary>
        public string Arguments;

        /// <summary>
        /// The WorkingDirectory (should be made into a property)
        /// </summary>
        public string WorkingDirectory;

        /// <summary>
        /// Fired for every line of stdOut received.
        /// </summary>
        public event DataReceivedHandler StdOutReceived;

        /// <summary>
        /// Fired for every line of stdErr received.
        /// </summary>
        public event DataReceivedHandler StdErrReceived;

        /// <summary>
        /// Amount of time to sleep on threads while waiting
        /// for the process to finish.
        /// </summary>
        public int SleepTime = 500;

		/// <summary>
		/// The process ID of the task
		/// </summary>
		public int ProcessID
		{
			get
			{
				lock(this)
				{
					if (process==null)
						return -1;
					else
						return process.Id;
				}
			}
		}

        /// <summary>
        /// The process used to run your task
        /// </summary>
        private Process process;

        /// <summary>
        /// 
        /// </summary>
        private readonly ILog _log;

        /// <summary>
        /// Initialises a ProcessCaller with an association to the
        /// supplied ISynchronizeInvoke.  All events raised from this
        /// object will be delivered via this target.  (This might be a
        /// Control object, so events would be delivered to that Control's
        /// UI thread.)
        /// </summary>
        /// <param name="isi">An object implementing the
        /// ISynchronizeInvoke interface.  All events will be delivered
        /// through this target, ensuring that they are delivered to the
        /// correct thread.</param>
        public ProcessCaller(ISynchronizeInvoke isi, ILog log)
            : base(isi)
        {
            _log = log;
        }

// This constructor only works with changes to AsyncOperation...
//        /// <summary>
//        /// Initialises a ProcessCaller without an association to an
//        /// ISynchronizeInvoke.  All events raised from this object
//        /// will be delievered via the worker thread.
//        /// </summary>
//        public ProcessCaller()
//        {
//        }

        /// <summary>
        /// Launch a process, but do not return until the process has exited.
        /// That way we can kill the process if a cancel is requested.
        /// </summary>
        protected override void DoWork()
        {
            StartProcess();

            // Wait for the process to end, or cancel it
            while (! process.HasExited)
            {
                Thread.Sleep(SleepTime); // sleep
                if (CancelRequested)
                {
                    // Not a very nice way to end a process,
                    // but effective.
                    process.Kill();
                    AcknowledgeCancel();
                }
            }
        }

        /// <summary>
        /// This method is generally called by DoWork()
        /// which is called by the base classs Start()
        /// </summary>
        protected virtual void StartProcess()
        {
			lock (this)
			{
				// Start a new process for the cmd
				process = new Process();
				process.StartInfo.UseShellExecute = false;
				process.StartInfo.RedirectStandardOutput = true;
				process.StartInfo.RedirectStandardError = true;
				process.StartInfo.CreateNoWindow = true;
				process.StartInfo.FileName = FileName;
				process.StartInfo.Arguments = Arguments;
				process.StartInfo.WorkingDirectory = WorkingDirectory;

				// These are the Win32 error code for file not found or access denied.
				const int ERROR_FILE_NOT_FOUND =2;
				const int ERROR_ACCESS_DENIED = 5;

				try
				{
					process.Start();
				}
				catch (Win32Exception e)
				{
					if(e.NativeErrorCode == ERROR_FILE_NOT_FOUND)
					{
                        _log.Message("{0}. Check the path.", e.Message);
					} 

					else if (e.NativeErrorCode == ERROR_ACCESS_DENIED)
					{
						// Note that if your word processor might generate exceptions
						// such as this, which are handled first.
                        _log.Message("{0}. You do not have permission to print this file.", e.Message);
					}
				}

            
				// Invoke stdOut and stdErr readers - each
				// has its own thread to guarantee that they aren't
				// blocked by, or cause a block to, the actual
				// process running (or the gui).
				new MethodInvoker(ReadStdOut).BeginInvoke(null, null);
				new MethodInvoker(ReadStdErr).BeginInvoke(null, null);
			}
        }

		public static Mutex m_Mutex = new Mutex();
		public static string m_StringBufferOutput;
		public static string m_StringBufferError;
		public static string m_StringBufferWarning;
		
		private void ReadOutput(StreamReader sr, DataReceivedHandler handler)
		{
			m_StringBufferOutput="";
			m_StringBufferError="";
			m_StringBufferWarning="";

			long startTicks=HiResTimer.Ticks;
			string str;
			int numLines=0;
			while ((str = sr.ReadLine()) != null)
			{
				long endTicks=HiResTimer.Ticks ;

				float milliseconds=((float)(endTicks-startTicks)/ (float)HiResTimer.TicksPerSecond) * 1000.0f;
				
				// trigger UI thread to empty string buffer:
				if (milliseconds>33.0f || numLines++>=100)
				{
					string shit;
					shit="test";
					FireAsync(handler, this, new DataReceivedEventArgs(shit));
					startTicks=HiResTimer.Ticks;
					numLines=0;
				}

				// store the string:
				m_Mutex.WaitOne();
				m_StringBufferOutput+=str + Environment.NewLine;
				if (str.StartsWith("Warning:"))
					m_StringBufferWarning+=str + Environment.NewLine;
				if (str.StartsWith("Error:"))
					m_StringBufferError+=str + Environment.NewLine;
				m_Mutex.ReleaseMutex();
			}	
		}

        /// <summary>
        /// Handles reading of stdout and firing an event for
        /// every line read
        /// </summary>
        protected virtual void ReadStdOut()
        {
			ReadOutput(process.StandardOutput,StdOutReceived);
        }

        /// <summary>
        /// Handles reading of stdErr
        /// </summary>
        protected virtual void ReadStdErr()
        {
			ReadOutput(process.StandardError,StdErrReceived);
        }

	}
}
