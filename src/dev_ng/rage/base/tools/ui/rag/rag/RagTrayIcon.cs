using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Windows.Forms;

using rag.Properties;
using ragCore;
using RSG.Base.Forms;
using RSG.Base.IO;
using RSG.Base.Logging;
using ragWidgets;

namespace rag
{
    public partial class RagTrayIcon : Form
    {
        private readonly ILog _log;

        private String LogCtx
        {
            get { return "RagTrayIcon"; }
        }

        public RagTrayIcon(ILog log)
        {
            _log = log;
            InitializeComponent();

            Thread.CurrentThread.Name = "MainThread";

            int handle = Handle.ToInt32(); // ensures the window handle is created
            Debug.Assert( handle > 0, "Handle not created" );

            // find the version number of the main rag assembly
            string versionString = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();

            this.notifyIcon.Text = "RAG (version " + versionString + ")";

            if ( (m_ipAddress != null) && (m_ipAddress != string.Empty) )
            {
                this.notifyIcon.Text += " on ";
                this.notifyIcon.Text += m_ipAddress;
            }

            _log.MessageCtx(LogCtx, "Rag Version: {0}", versionString);


            try
            {
                bool crashTest = Settings.Default.ShowColouredModifications;
            }
            catch ( System.Exception e)
            {
                _log.ToolExceptionCtx(LogCtx, e, "Load settings exception.");
                _log.MessageCtx(LogCtx, "Couldn't load settings, please delete them and try again.");
                _log.MessageCtx(LogCtx, @"Look here: C:\Users\<username>\AppData\Local\Rockstar_Games\ragApp.exe_Url_emui54uicspcvcgh5e2mq2wtwovdlxwu");
                Shutdown();
                Close();
            }
        }

        #region Delegates
        private delegate MainWindow CreateMainWindowDel(INamedPipe pipe, MainWindow.RestartHandler handler, GameConnection connection, ILog log);
        private delegate int ReturnsIntDelegate();
        #endregion

        #region Variables
        private static RagTrayIcon sm_Instance;
        private Int32 m_PortNumber = 2000;

        private ProxyConnection m_ProxyConnection;
        public bool ConnectedToProxy
        {
            get { return m_ProxyConnection != null; }
        }

        private MainWindow m_MainWindow;
        private PipeID m_RagPipeName;
        private INamedPipe m_RagPipe = PipeID.CreateNamedPipe();
        private Thread m_RagePipeThread;
        private bool m_Quiting;
        private bool m_StoppingPipe;
        private bool m_StartThread = true;

        private string m_ipAddress = string.Empty;
        private string m_ExeArgs;

        private GameConnection _currentConnection = null;
        #endregion

        #region Properties
        public static RagTrayIcon Instance
        {
            get
            {
                return sm_Instance;
            }
        }

        public bool AppExiting
        {
            set
            {
                m_StartThread = value;
            }
        }

        public MainWindow MainWindow
        {
            get
            {
                return m_MainWindow;
            }
        }

        public string IpAddress
        {
            get
            {
                return m_ipAddress;
            }
            set
            {
                m_ipAddress = value;
            }
        }

        public IPAddress Address
        {
            get
            {
                if ( (m_ipAddress != null) && (m_ipAddress != string.Empty) )
                {
                    string[] split = m_ipAddress.Split( new char[] { '.' } );
                    byte[] byteArray = new byte[split.Length];
                    for ( int i = 0; i < split.Length; ++i )
                    {
                        byteArray[i] = byte.Parse( split[i] );
                    }

                    return new IPAddress( byteArray );
                }

                return IPAddress.Any;
            }
        }
        #endregion

        #region Overrides
        // the code below waits for a quit message for an instance of rag executed with "-exit".
        // We have to do this since a close message just closes the current application with RAG 
        // instead of closing RAG itself.
        protected override void WndProc( ref Message message )
        {
            //filter the RF_TESTMESSAGE
            if ( message.Msg == MainWindow.MSG_REALLY_QUIT )
            {
                exitToolStripMenuItem_Click( this, EventArgs.Empty );
            }
            //be sure to pass along all messages to the base also
            base.WndProc( ref message );
        }
        #endregion

        #region Public Functions
        public bool Init( string ipAddress, string[] args )
        {
            this.notifyIcon.Visible = true;

            m_ipAddress = ipAddress;

            // find the version number of the main rag assembly
            string versionString = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
            this.notifyIcon.Text = "RAG (version " + versionString + ")";
            if ( (m_ipAddress != null) && (m_ipAddress != string.Empty) )
            {
                this.notifyIcon.Text += " on ";
                this.notifyIcon.Text += m_ipAddress;
            }

            // get plugin path:
            string pluginPath = null;
            string admin = Boolean.FalseString;
            string noquitmsg = Boolean.FalseString;
            string pipeTimeout = String.Empty;
            bool hasNoQuitMsg = false;
            int proxyPort = -1;
            bool useProxy = true;

            m_ExeArgs = null;
            foreach ( string arg in args )
            {
                if ( arg.ToLower() == "-admin" )
                {
                    admin = Boolean.TrueString;
                }
                else if ( arg.ToLower() == "-noquitmsg" )
                {
                    noquitmsg = Boolean.TrueString;
                    hasNoQuitMsg = true;
                }
                else if ( arg.ToLower() == "-noproxy" )
                {
                    useProxy = false;
                }
                else if ( arg.Contains( "proxyport=" ) )
                {
                    bool res = int.TryParse( arg.Split( '=' )[1], out proxyPort );
                    if ( !res )
                        _log.MessageCtx(LogCtx, "tried to parse proxyport but failed");
                }
                else
                {
                    ragCore.Args.CheckArg( arg, "-pluginPath", ref pluginPath );
                    ragCore.Args.CheckArg( arg, "-admin", ref admin );
                    ragCore.Args.CheckArg( arg, "-pipeTimeout", ref pipeTimeout );

                    if ( !hasNoQuitMsg )
                    {
                        hasNoQuitMsg = ragCore.Args.CheckArg( arg, "-noquitmsg", ref noquitmsg );
                    }

                    if ( arg.StartsWith( "-multiColor" ) )
                    {
                        string name = arg.Substring( 1 );
                        bool found = false;
                        foreach ( KeyValuePair<string, MultiColorInfo> pair in MainWindow.MultiColorOutputInfo )
                        {
                            string multiColor = Boolean.TrueString;
                            if ( ragCore.Args.CheckArg( arg, "-" + pair.Value.CommandLine, ref multiColor ) )
                            {
                                if ( multiColor != null )
                                {
                                    pair.Value.MultiColor = Boolean.Parse( multiColor );
                                    pair.Value.SetByCommandLine = true;
                                }

                                found = true;
                                break;
                            }
                        }

                        if ( !found )
                        {
                            int end = arg.IndexOf( '=' );
                            if ( end > -1 )
                            {
                                int start = arg.IndexOf( 'r' ) + 1;
                                string key = arg.Substring( start, end - start );

                                MultiColorInfo mcInfo = new MultiColorInfo( "multiColor" + key, Color.Empty );
                                MainWindow.MultiColorOutputInfo.Add( key, mcInfo );

                                string multiColor = Boolean.TrueString;
                                if ( ragCore.Args.CheckArg( arg, "-" + mcInfo.CommandLine, ref multiColor ) )
                                {
                                    if ( multiColor != null )
                                    {
                                        mcInfo.MultiColor = Boolean.Parse( multiColor );
                                        mcInfo.SetByCommandLine = true;
                                    }
                                }
                            }
                        }
                    }
                }

                if ( arg.ToLower() != "-restarting" )
                {
                    m_ExeArgs += " " + arg;
                }
            }

            if (useProxy)
            {
                m_ProxyConnection = new ProxyConnection("Rag Proxy", _log);
                m_ProxyConnection.ProxyDisconnected += new EventHandler(m_ProxyConnection_ProxyDisconnected);
                this.notifyIcon.Visible = false;
                _log.MessageCtx(LogCtx, "Rag running against proxy.");
            }

            MainWindow.PluginPath = pluginPath;
            MainWindow.AdminMode = Boolean.Parse( admin );
            MainWindow.IpAddress = m_ipAddress;

            int timeoutValue;
            if ( Int32.TryParse( pipeTimeout, out timeoutValue ) )
            {
                NamedPipeSocket.PipeTimeoutSeconds = timeoutValue;
            }

            if ( hasNoQuitMsg )
            {
                MainWindow.SuppressAssertQuitCrashDialog = Boolean.Parse( noquitmsg );
            }


            _currentConnection = GetCurrentGameConnection(proxyPort);

            if (_currentConnection != null)
            {
                //TODO: Remove this once the ragconsole isn't using it anymore.
                GameConnection.CurrentClientGameConnection = _currentConnection;

                // start our application:
                sm_Instance = this;

                StartRagePipeThread();
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Tries to set the current game connection.
        /// </summary>
        private GameConnection GetCurrentGameConnection(int proxyPort)
        {
            GameConnection conn = null;
            int retryCount = 10;

            // Check whether we were provided the proxy port.
            if (proxyPort != -1)
            {
                m_ProxyConnection.RemotePort = proxyPort;
                IList<GameConnection> games = GameConnection.AcquireGameList(_log);

                while (conn == null && retryCount > 0)
                {
                    conn = games.FirstOrDefault(item => item.Port == proxyPort);

                    // Didn't manage to get the game connection, so sleep for half a second.
                    if (conn == null)
                    {
                        _log.WarningCtx(LogCtx, "No games listed on proxy for port {0}, retrying...", proxyPort);
                        Thread.Sleep(1000);
                    }

                    if (--retryCount < 0)
                    {
                        String msg = String.Format("RAG: Can not find the game for the specified port {0} in the proxy. Continue trying?", proxyPort);
                        if (MessageBox.Show(msg, "RAG: No games found", MessageBoxButtons.RetryCancel) == DialogResult.Retry)
                        {
                            retryCount = 20;
                        }
                        else
                        {
                            Shutdown();
                            break;
                        }
                    }
                }
            }
            else
            {
                while (conn == null && retryCount > 0)
                {
                    IList<GameConnection> games = GameConnection.AcquireGameList(_log);
                    if (games.Any())
                    {
                        int validChoices = games.Where(item => item.Connected).Count();
                        
                        if (validChoices == 1)
                        {
                            conn = games.First(item => item.Connected);
                            m_ProxyConnection.RemotePort = conn.Port;
                            break;
                        }
                        else if (validChoices > 1)
                        {
                            ChooseGameConnectionForm form = new ChooseGameConnectionForm(games);
                            form.ShowDialog();
                            if (form.ChosenConnection == null)
                            {
                                Shutdown();
                                break;
                            }

                            if (!form.ChosenConnection.Connected)
                            {
                                _log.ErrorCtx(LogCtx, "Only found non-connected \"game\", retrying...");
                                retryCount = 10;
                            }
                            else
                            {
                                conn = form.ChosenConnection;
                                m_ProxyConnection.RemotePort = conn.Port;
                                break;
                            }
                        }
                    }

                    if (conn == null)
                    {
                        _log.WarningCtx(LogCtx, "No games listed on proxy, retrying...");
                        Thread.Sleep(1000);
                    }

                    if (--retryCount == 0)
                    {
                        String msg = "RAG UI: No game connections were retrieved from the RAG Proxy.  " +
                            "Please make sure the RAG proxy is running and that that it is successfully connected to at least one running instance of the game.\n\n" +
                            "Continue trying?";

                        if (MessageBox.Show(msg, "RAG UI: No game connections found", MessageBoxButtons.RetryCancel) == DialogResult.Retry)
                        {
                            retryCount = 10;
                        }
                        else
                        {
                            Shutdown();
                            break;
                        }
                    }
                }
            }

            return conn;
        }

        void m_ProxyConnection_ProxyDisconnected( object sender, EventArgs e )
        {
            if ( m_RagePipeThread != null && m_RagePipeThread.IsAlive )
            {
                m_RagPipe.CancelCreate();
            }
        }

        public void Restart()
        {
            if ( m_ProxyConnection != null )
            {
                return;
            }

            _log.MessageCtx(LogCtx, "Restart");

            if ( !String.IsNullOrEmpty( m_ExeArgs ) && m_ExeArgs.ToLower().Contains( "-restarting " ) )
            {
                Process.Start( Application.ExecutablePath, m_ExeArgs );
            }
            else
            {
                Process.Start( Application.ExecutablePath, "-restarting " + m_ExeArgs );
            }
        }

        public void RestartAndExit( Exception e, bool showMessage )
        {
            _log.MessageCtx(LogCtx, "RestartAndExit");
            if ( showMessage )
            {
                List<string> attachments = new List<string>();

                if ( RagTrayIcon.Instance != null
                    && File.Exists(LogFactory.ApplicationLogFilename))
                {
                    attachments.Add(LogFactory.ApplicationLogFilename);
                }

                string userConfigFilename = rageFileUtilities.GetRageApplicationUserConfigFilename( "ragApp.exe" );
                if ( !String.IsNullOrEmpty( userConfigFilename ) && File.Exists( userConfigFilename ) )
                {
                    attachments.Add( userConfigFilename );
                }

                UnhandledExceptionDialog dialog = new UnhandledExceptionDialog( "Rag", attachments,
                    Settings.Default.ExceptionToEmailAdress, e );

                if ( m_MainWindow != null )
                {
                    dialog.ShowDialog( this );
                }
                else
                {
                    dialog.ShowDialog();
                }
            }

            // need to close main window first, so we save layout and preferences properly
            if ( m_MainWindow != null )
            {
                _log.MessageCtx(LogCtx, "RestartAndExit closing MainWindow");

                m_MainWindow.Close();
            }
            else
            {
                // we know that m_MainWindow.MainWindow_FormClosing() will call these, so only call it here
                _log.MessageCtx(LogCtx, "RestartAndExit calling StopApps");

                RageApplication.StopApps(_log);
                this.Close();
                Restart();
            }
        }

        public void Shutdown()
        {
            exitToolStripMenuItem_Click( this, EventArgs.Empty );
        }
        #endregion

        #region Private Functions
        private void StartRagePipeThread()
        {
            if ( m_StartThread && !m_Quiting )
            {
                m_StoppingPipe = false;
                m_RagePipeThread = new Thread( new ThreadStart( RagePipeProc ) );
                m_RagePipeThread.Name = "Rag Connection Thread";
                m_RagePipeThread.Start();
            }

            m_StartThread = false;
        }

        private bool StartMainWindow()
        {
            _log.MessageCtx(LogCtx, "Creating Main Window...");

            Object[] args = new object[4];
            args[0] = m_RagPipe;
            args[1] = new MainWindow.RestartHandler( this.RestartAndExit );
            args[2] = _currentConnection;
            args[3] = _log;

            CreateMainWindowDel del = new CreateMainWindowDel( MainWindow.Create );
            // run this on the main thread since we're on a separate thread:
            m_MainWindow = Invoke( del, args ) as MainWindow;

            if ( m_MainWindow != null )
            {
                m_MainWindow.Disposed += new EventHandler( MainWindow_Disposed );
                m_MainWindow.Closed += new EventHandler( MainWindow_Closed );
                _log.MessageCtx(LogCtx, "Main Window created.");
                return true;
            }

            _log.ErrorCtx(LogCtx, "Could not create the Main Window.");
            return false;
        }

        private void RagePipeProc()
        {
            string threadName = m_RagePipeThread.Name;

            _log.MessageCtx(LogCtx, "Started {0}", threadName);

            ProxyLock proxyLock = null;
            if ( m_RagPipeName == null )
            {
                if ( m_ProxyConnection != null )
                {
                    try
                    {
                        proxyLock = m_ProxyConnection.ConnectToProxy( 60000 );
                    }
                    catch ( TimeoutException )
                    {
                        _log.WarningCtx(LogCtx, "RagePipeProc timed out while waiting for proxy connection.");
                        object[] args = new object[2];
                        args[0] = this;
                        args[1] = new EventArgs();
                        Invoke( new EventHandler( exitToolStripMenuItem_Click ), args );
                        return;
                    }

                    m_PortNumber = m_ProxyConnection.LocalPort;
                }

                m_RagPipeName = new PipeID( "pipe_rag", this.Address, m_PortNumber );
            }

            _log.MessageCtx(LogCtx, "Waiting for client to connect to pipe '{0}' on port {1}...", m_RagPipeName.ToString(), m_RagPipeName.Port);

            proxyLock.Release();
            bool mainWindowStarted = false;
            while ( !m_StoppingPipe )
            {
                try
                {
                    // wait until we get a connection:                
                    bool result = m_RagPipe.Create( m_RagPipeName, true );
                    if ( !result )
                    {
                        _log.MessageCtx(LogCtx, "Could not connect to pipe '{0}'!", m_RagPipeName.ToString());

                        if ( !m_StoppingPipe )
                        {
                            object[] args = new object[2];
                            args[0] = this;
                            args[1] = new EventArgs();
                            Invoke( new EventHandler( exitToolStripMenuItem_Click ), args );
                        }

                        break;
                    }
                    else if ( !m_StoppingPipe )
                    {
                        _log.MessageCtx(LogCtx, "Connected to pipe '{0}'.", m_RagPipeName.ToString());

                        // What is this for?!? Doesn't appear to be used anywhere...
                        try
                        {
                            int handle =
                                (int)this.Invoke(
                                (ReturnsIntDelegate)delegate()
                                {
                                    return this.Handle.ToInt32();
                                }
                                );
                            Debug.Assert( handle > 0, "Handle not created" );
                        }
                        catch ( Exception )
                        {
                            Debug.Print( "Would have failed\n" );
                        }

                        if ( !Debugger.IsAttached )
                        {
                            try
                            {
                                if ( StartMainWindow() )
                                {
                                    mainWindowStarted = true;
                                    break;
                                }
                                else
                                {
                                    m_RagPipe.Close();
                                    _log.MessageCtx(LogCtx, "Disconnected from pipe '{0}'.", m_RagPipe.Name);

                                    RageApplication.ClearStatics();
                                }
                            }
                            catch ( Exception e )
                            {
                                _log.ToolExceptionCtx(LogCtx, e, "Exception");

                                Object[] args = new object[2];
                                args[0] = e;
                                args[1] = true;
                                Invoke( new MainWindow.RestartHandler( RestartAndExit ), args );
                            }
                        }
                        else
                        {
                            if ( StartMainWindow() )
                            {
                                mainWindowStarted = true;
                                break;
                            }
                            else
                            {
                                m_RagPipe.Close();
                                _log.MessageCtx(LogCtx, "Disconnected from pipe '{0}'.", m_RagPipe.Name);

                                RageApplication.ClearStatics();
                            }
                        }
                    }
                }
                catch ( ThreadAbortException )
                {
                    break;
                }
            }

            _log.MessageCtx(LogCtx, "Ended {0}", threadName);

            if ( mainWindowStarted )
            {
                _log.MessageCtx(LogCtx, "Connection complete.");
            }
        }
        #endregion

        #region Event Handlers
        private void RagTrayIcon_FormClosing( object sender, FormClosingEventArgs e )
        {
            _log.MessageCtx(LogCtx, "Rag Closing");

            this.notifyIcon.Visible = false;

            m_Quiting = true;
            MainWindow_Closed( sender, e );
        }

        private void RagTrayIcon_FormClosed( object sender, FormClosedEventArgs e )
        {
            _log.MessageCtx(LogCtx, "Rag Closed");
        }

        private void RagTrayIcon_VisibleChanged( object sender, EventArgs e )
        {
            // make sure the form always stays hidden
            this.Visible = false;
        }

        private void viewLogToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( File.Exists(LogFactory.ApplicationLogFilename) )
            {
                Process.Start(LogFactory.ApplicationLogFilename);
            }
        }

        private void exitToolStripMenuItem_Click( object sender, EventArgs e )
        {
            _log.MessageCtx(LogCtx, "Exit Rag");


            if ( m_RagePipeThread != null && m_RagePipeThread.IsAlive )
            {
                m_StoppingPipe = true;
                m_RagPipe.CancelCreate();
            }

            // no main window, so exit ourselves...
            if ( m_MainWindow == null )
            {
                MainWindow_Closed( sender, EventArgs.Empty );
                this.notifyIcon.Visible = false;
                Application.Exit(); // don't call this.Close() because of what RagTrayIcon_Closing does
            }
            // ... otherwise let main window handle exit:
            else
            {
                m_MainWindow.Quit();
            }
        }

        private void MainWindow_Closed( object sender, System.EventArgs e )
        {
            m_StoppingPipe = true;

            if ( m_RagPipe != null )
            {
                m_RagPipe.CancelCreate();
                m_RagPipe = null;
            }

            if ( m_RagePipeThread != null && m_RagePipeThread.IsAlive )
            {
                m_RagePipeThread.Abort();
                m_RagePipeThread = null;
            }

            // clear all statics:
            BankTreeView.ClearStatics();
            FavoritesView.ClearStatics();
            FindView.ClearStatics();
            WidgetViewSingle.ClearStatics();
            MainWindow.ClearStatics();
            RageApplication.ClearStatics();
            RenderWindow.ClearStatics();
            WidgetGroup.ClearStatics();
            WidgetGroupVisible.ClearStatics();
            DockableViewManager.Instance.ClearStaticsWidgetView();

            m_MainWindow = null;
            GC.Collect();
        }

        private void MainWindow_Disposed( object sender, EventArgs e )
        {
            StartRagePipeThread();
        }
        #endregion

        internal void PrepareToQuit()
        {
            if ( m_ProxyConnection != null )
            {
                m_ProxyConnection.DisconnectFromProxy();
            }
        }
    }
}
