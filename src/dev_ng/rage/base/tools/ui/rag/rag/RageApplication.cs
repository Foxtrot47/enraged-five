using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Xml;

using ragCore;
using RSG.Base.Command;
using RSG.Base.Extensions;
using RSG.Base.Forms;
using RSG.Base.IO;
using ragWidgets;
using TD.SandDock;
using System.Text.RegularExpressions;
using RSG.Base.Logging;

namespace rag
{
    /// <summary>
    /// 
    /// </summary>
    public class RageApplication : IRageApplication, IDisposable
    {
        #region Constants
        /// <summary>
        /// Handshake value to help ensure that Rag code remains synchronized with Rage bank code.  Increment this value 
        /// whenever there's a change in Rag code that requires a change in Rage code.  Keep this in sync with 
        /// RAG_VERSION in rage/base/tools/rag/RageApplication.cs.
        /// </summary>
        private const float RAG_VERSION = 2.0f;

        /// <summary>
        /// 
        /// </summary>
        private const int c_defaultBasePortNumber = 50000;

        /// <summary>
        /// Number of sockets we need to reserve.
        /// </summary>
        private const int c_numSocketsToReserve = 50;

        /// <summary>
        /// 
        /// </summary>
        private const String LogCtx = "Rage Application";
        #endregion

        #region Delegates
        public delegate void OutputTextEventHandler(object sender, OutputTextEventArgs e);
        public delegate TD.SandDock.DockControl DockControlCreatorDel(System.Guid guid);
        public delegate TD.SandDock.DockControl CreateOutputWindowDel(string channelName, string color);
        public delegate void AddSubChannelInfoDel(string channelName, string subChannelName);
        #endregion

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public RageApplication(IPAddress address, ILog log)
        {
            _log = log;
            m_BankManager = new BankManager(log);
            m_InputHandler = new Input(log);
            m_IPAddress = address;
            FindAvailableSockets();

            m_PipeNameBank = GetRagPipeSocket((int)PipeID.EnumSocketOffset.SOCKET_OFFSET_BANK );
            m_PipeNameOutput = GetRagPipeSocket( (int)PipeID.EnumSocketOffset.SOCKET_OFFSET_OUTPUT );
            m_PipeNameEvents = GetRagPipeSocket( (int)PipeID.EnumSocketOffset.SOCKET_OFFSET_EVENTS );

            m_BankManager.InitFinished += new InitFinishedHandler( BankManager_InitFinished );

            NamedPipeSocket.ReadWriteFailure += new UnhandledExceptionEventHandler( NamedPipeSocket_ReadWriteFailure );
        }
        #endregion // Constructor(s)

        #region Static Members
        private static EventHandler sm_BeginAddOutputText;
        private static OutputTextEventHandler sm_AddOutputText;
        private static EventHandler sm_EndAddOutputText;

        private static CreateOutputWindowDel sm_CreateOutputWindowDel;
        private static AddSubChannelInfoDel sm_AddSubChannelInfoDel;
        private static DockControlCreatorDel sm_DockControlCreatorWidgetView;
        private static DockControlCreatorDel sm_DockControlCreatorRenderWindow;

        private static Mutex sm_updatePipeThreadMutex = new Mutex();
        private static Thread sm_updatePipeThread = null;

        private static int sm_BasePortNumber = c_defaultBasePortNumber;

        private static RageApplication sm_mainApplication;
        private static RageApplication[] sm_Applications;
        private static uint sm_MasterAppIndex = 0;
        #endregion // Static Members

        #region Member Variables
        private string m_Path;
        private string m_VisibleName;
        private string m_Args;

        private readonly ILog _log;
        private IPAddress m_IPAddress = IPAddress.Any;
        private int m_PortNumber = sm_BasePortNumber;
        private List<TcpListener> m_reservedSockets = new List<TcpListener>();

        private PipeID m_PipeNameBank;
        private PipeID m_PipeNameOutput;
        private PipeID m_PipeNameEvents;
        private ProcessCaller m_Process;
        private Process m_KillProcess;
        private TD.SandDock.DockControl m_DockControlRenderWindow;
        private RenderWindow m_RenderWindow;
        private Control m_MainForm;
        private Input m_InputHandler;

        private bool m_Started = false;
        private bool m_Stopped = false;
        private Mutex m_stoppingMutex = new Mutex();
        private bool m_ShouldStart = false;
        private bool m_IsValid = false;
        private bool m_WasConnected = false;
        private readonly BankManager m_BankManager;
        private BankPipe m_PipeBank = new BankPipe();
        private INamedPipe m_PipeOutput = PipeID.CreateNamedPipe();

        private Queue<OutputTextEventArgs> m_outputQueue = new Queue<OutputTextEventArgs>();
        private object m_syncObject = new object();
        private WidgetText m_widgetTextWidth;
        private WidgetText m_widgetTextHeight;

        private String m_currentLine = String.Empty;
        #endregion // Member Variables

        #region Properties
        public static DockControlCreatorDel DockControlCreatorWidgetView
        {
            set
            {
                sm_DockControlCreatorWidgetView = value;
            }
        }

        public static DockControlCreatorDel DockControlCreatorRenderWindow
        {
            set
            {
                sm_DockControlCreatorRenderWindow = value;
            }
        }

        public static RageApplication[] Applications
        {
            get
            {
                return sm_Applications;
            }
        }

        public static RageApplication MainApp
        {
            get
            {
                Debug.Assert( sm_Applications != null, "sm_Applications != null" );
                Debug.Assert( sm_Applications.Length > 0, "No applications found" );
                Debug.Assert( sm_MasterAppIndex < sm_Applications.Length, "sm_MasterAppIndex < sm_Applications.Length" );
                Debug.Assert( sm_Applications[sm_MasterAppIndex] != null, "sm_Applications[sm_MasterAppIndex] != null" );
                return sm_Applications[sm_MasterAppIndex];
            }
        }

        public RenderWindow RenderWindow
        {
            get
            {
                return m_RenderWindow;
            }
        }

        public string RenderWindowImageFilename
        {
            get
            {
                if ( m_RenderWindow != null )
                {
                    return m_RenderWindow.ImageFilename;
                }

                return null;
            }
            set
            {
                if ( m_RenderWindow != null )
                {
                    m_RenderWindow.ImageFilename = value;
                }
            }
        }

        public Size RenderWindowResolution
        {
            get
            {
                return m_RenderWindow.Size;
            }
            set
            {
                m_RenderWindow.SetGameResolution( value );
            }
        }

        public IBankManager BankMgr
        {
            get
            {
                return m_BankManager;
            }
        }


        public IKeyUpProcessor KeyProcessor
        {
            get
            {
                return m_BankManager;
            }
        }

        public string VisibleName
        {
            get
            {
                return m_VisibleName;
            }
        }

        public string ExePath
        {
            get
            {
                return m_Path;
            }
        }

        public string Args
        {
            get
            {
                return m_Args;
            }
        }

        public string ExeName
        {
            get
            {
                return Path.GetFileName( m_Path );
            }
        }

        public Input InputHandler
        {
            get
            {
                return m_InputHandler;
            }
        }

        public BankPipe BankPipe
        {
            get
            {
                return m_PipeBank;
            }
        }

        public bool ShouldStart
        {
            set
            {
                m_ShouldStart = value;
            }
            get
            {
                return m_ShouldStart;
            }
        }

        public bool IsStarted
        {
            get
            {
                return m_Started;
            }
        }

        public bool IsStopped
        {
            get
            {
                m_stoppingMutex.WaitOne();

                bool rtn = m_Stopped;

                m_stoppingMutex.ReleaseMutex();

                return rtn;
            }
        }

        public string Guid
        {
            get
            {
                return (m_DockControlRenderWindow != null) ? m_DockControlRenderWindow.Guid.ToString() : string.Empty;
            }
        }

        public bool PipesDisconnected
        {
            get
            {
                return !m_PipeOutput.IsConnected() || !m_PipeBank.IsConnected()
                    || ((RageApplication.MainApp == this) && !m_InputHandler.IsConnected())
                    || Sensor.CurrentSensor.HasQuit;
            }
        }

        public INamedPipe TheOutputPipe
        {
            get
            {
                return m_PipeOutput;
            }
        }

        public INamedPipe TheBankPipe
        {
            get
            {
                return m_PipeBank;
            }
        }

        public bool WasConnected
        {
            get { return m_WasConnected; }
        }

        private static BankRemotePacketProcessor ms_PacketProcessor;
        public static BankRemotePacketProcessor PacketProcessor
        {
            get { return ms_PacketProcessor; }
            set { ms_PacketProcessor = value; }
        }

        #endregion

        #region Event Handlers
        public void StartApp( System.Object sender, EventArgs args )
        {
            StartApp( true );
        }

        private void Widgets_AddView_Click( object sender, System.EventArgs e )
        {
            AddWidgetView( ExeName, new WidgetViewBase.CreateDel( BankTreeView.CreateBankTreeView ), false );
        }

        private void MenuItemAddFindView_Click( object sender, System.EventArgs e )
        {
            AddWidgetView( ExeName, new WidgetViewBase.CreateDel( FindView.CreateFindView ), false );
        }

        private void MenuItemAddFavoritesView_Click( object sender, System.EventArgs e )
        {
            AddWidgetView( ExeName, new WidgetViewBase.CreateDel( FavoritesView.CreateFavoritesView ), false );
        }

        /// <summary>
        /// Called when the Bank Manager receives the "first frame" message from the game.  By this point,
        /// most, if not all, of the banks and widgets have been created.
        /// It is at this point that it is safe to attempt to setup our widget binding.
        /// </summary>
        private void BankManager_InitFinished()
        {
            m_widgetTextWidth = m_BankManager.FindFirstWidgetFromPath( "rage - Gfx/Screen Width" ) as WidgetText;
            if ( m_widgetTextWidth == null )
            {
                List<Widget> foundWidgets = new List<Widget>();
                WidgetBank.FindWidgetsWithMatchingString( m_BankManager, false, true, true, true, false,
                    "Screen Width", ref foundWidgets, -1 );
                if ( (foundWidgets.Count > 0) && (foundWidgets[0] is WidgetText) )
                {
                    m_widgetTextWidth = foundWidgets[0] as WidgetText;
                }
            }

            m_widgetTextHeight = m_BankManager.FindFirstWidgetFromPath( "rage - Gfx/Screen Height" ) as WidgetText;
            if ( m_widgetTextHeight == null )
            {
                List<Widget> foundWidgets = new List<Widget>();
                WidgetBank.FindWidgetsWithMatchingString( m_BankManager, false, true, true, true, false,
                    "Screen Height", ref foundWidgets, -1 );
                if ( (foundWidgets.Count > 0) && (foundWidgets[0] is WidgetText) )
                {
                    m_widgetTextHeight = foundWidgets[0] as WidgetText;
                }
            }

            if ( (m_widgetTextWidth != null) && (m_widgetTextHeight != null) )
            {
                m_widgetTextWidth.ValueChanged += new EventHandler( widgetText_StringChanged );
                m_widgetTextHeight.ValueChanged += new EventHandler( widgetText_StringChanged );

                // initialize
                widgetText_StringChanged( this, EventArgs.Empty );
            }
        }

        private void widgetText_StringChanged( object sender, EventArgs e )
        {
            try
            {
                int width = int.Parse( m_widgetTextWidth.String );
                int height = int.Parse( m_widgetTextHeight.String );

                this.RenderWindowResolution = new Size( width, height );
            }
            catch
            {

            }
        }
        #endregion

        #region Public Functions
        public PipeID GetRagPipeSocket( int offset )
        {
            _log.MessageCtx(LogCtx, "TrayApplication: Freeing up port {0}", m_PortNumber + offset);
            Debug.Assert(offset < m_reservedSockets.Count, "Requesting port that hasn't been reserved by the TrayApplication.");
            if (offset >= m_reservedSockets.Count)
            {
                throw new ArgumentOutOfRangeException("offset", "Requesting port that hasn't been reserved by the TrayApplication.");
            }

            // Stop the listener that is running on that port and create a pipe for it instead.
            //m_reservedSockets[offset].Stop();
            if (m_reservedSockets[offset] == null)
            {
                throw new ArgumentNullException("offset", "Listener has already been claimed.");
            }

            PipeID pipeId = new PipeID(RagTrayIcon.Instance.Address, m_PortNumber + offset, m_reservedSockets[offset]);
            m_reservedSockets[offset] = null;
            return pipeId;
        }

        public void FocusRenderWindow()
        {
            if ( m_RenderWindow != null )
            {
                m_RenderWindow.Focus();
            }
        }

        public bool CheckIsValid()
        {
            if ( m_Path == null )
            {
                m_IsValid = false;
                rageMessageBox.ShowError( m_DockControlRenderWindow, "No path was specified", "No Path Specified" );
            }
            else
                m_IsValid = true;

            return m_IsValid;
        }

        /// <summary>
        /// This is the thread function
        /// </summary>
        public void ReadOutputPipe()
        {
            IList<TTYLine> lines = new List<TTYLine>();

            if ( m_PipeOutput.IsValid() )
            {
                while ( m_PipeOutput.HasData() )
                {
                    const int BUFFER_SIZE = 4 * 1024;
                    Byte[] chars = new Byte[BUFFER_SIZE];
                    int numRead = m_PipeOutput.ReadData( chars, BUFFER_SIZE, false );

                    
                    for ( int i = 0; i < numRead; i++ )
                    {
                        Char charRead = Convert.ToChar( chars[i] );
                        if ( charRead == 0 )
                        {
                            if (m_currentLine.Length > 0)
                            {
                                lines.Add(new TTYLine(m_currentLine));
                            }

                            m_currentLine = string.Empty;
                        }
                        else
                        {
                            m_currentLine += charRead;
                        }
                    }
                }

                // Lock after we've processed all the data.
                lock (m_syncObject)
                {
                    foreach (TTYLine line in lines)
                    {
                        m_outputQueue.Enqueue(new OutputTextEventArgs(line));
                    }
                }
            }
        }

        public class TTYLine
        {
            private static readonly Regex _ttyRegex;

            static TTYLine()
            {
                // Construct the components of the regex.
                // e.g.
                // ^(?<fileline>.*?\(\d+\): )?(?<frametime>\[\d{8}:\d{8}\] )?(?<prefix>Warning: |Error: )?(?<thread>\<.*?\> )?(?<channel>\[[^\s]*\] )?(?<message>.*)
                StringBuilder sb = new StringBuilder();
                sb.Append(@"(?<fileline>.*\(\d+?\): )?");
                sb.Append(@"(?<frametime>\[\d{8}:\d{8}\] )?");
                sb.AppendFormat(@"(?<prefix>{0})?", String.Join("|", SeverityPrefix.Where(item => item != "")));
                sb.Append(@"(?<thread>\<.*?\> )?");
                sb.Append(@"(?<channel>\[[^\s]*\] )?");
                sb.Append(@"(?<message>.*)");

                _ttyRegex = new Regex(sb.ToString());
            }

            public TTYLine( string text )
            {
                if ( text.EndsWith( TTYLine.ColorPostfixCode ) )
                {
                    m_text = text.Remove( text.LastIndexOf( TTYLine.ColorPostfixCode ), TTYLine.ColorPostfixCode.Length );
                    m_text += "\n";
                }
                else
                {
                    m_text = text;
                }

                if ( m_text.StartsWith( TTYLine.ColorCodeCharacter, StringComparison.Ordinal ) )
                {
                    m_text = GetTextColors( m_text, ref m_foreColor, ref m_backColor );
                }

                Match match = _ttyRegex.Match(m_text);
                if (match.Success)
                {
                    Group prefixGroup = match.Groups["prefix"];
                    if (prefixGroup.Success)
                    {
                        int prefixIndex = Array.IndexOf(SeverityPrefix, prefixGroup.Value);
                        if (prefixIndex != -1)
                        {
                            m_severity = (Severity)prefixIndex;
                        }
                    }

                    Group channelGroup = match.Groups["channel"];
                    if (channelGroup.Success)
                    {
                        m_channelName = channelGroup.Value.Trim(new char[] { ' ', '[', ']' });
                    }
                }

                if (!String.IsNullOrEmpty(m_channelName))
                {
                    Color channelColor;
                    if ((m_severity >= Severity.Display) && TTYLine.ChannelColors.TryGetValue(m_channelName, out channelColor))
                    {
                        m_foreColor = channelColor;
                        m_backColor = SystemColors.Window;
                    }
                }
            }

            #region Enums
            public enum Severity
            {
                FatalError,
                Error,
                Warning,
                Display,
                Debug1,
                Debug2,
                Debug3
            }
            #endregion

            #region Variables
            private string m_text = string.Empty;
            private Severity m_severity = Severity.Display;
            private string m_channelName = string.Empty;
            private Color m_foreColor = SystemColors.WindowText;
            private Color m_backColor = SystemColors.Window;

            private static string sm_textWarningPrefix = "Warning: ";
            private static string sm_textErrorPrefix = "Error: ";
            private static string sm_textQuitPrefix = "Fatal Error: ";
            private static string sm_textDebug1Prefix = "Debug1: ";
            private static string sm_textDebug2Prefix = "Debug2: ";
            private static string sm_textDebug3Prefix = "Debug3: ";
            private static string sm_textScriptPrefix = "[Script] ";

            private static string sm_textColorCodePrefix = Convert.ToChar( 027 ).ToString();
            private static string sm_textNormalCode = sm_textColorCodePrefix + "[0m";    // this code come from //rage/dev/rage/base/src/diag/output.h
            private static string sm_textRedCode = sm_textColorCodePrefix + "[31m";      // this code come from //rage/dev/rage/base/src/diag/output.h
            private static string sm_textYellowCode = sm_textColorCodePrefix + "[33m";   // this code come from //rage/dev/rage/base/src/diag/output.h
            private static string sm_textBlueCode = sm_textColorCodePrefix + "[34m";     // this code come from //rage/dev/rage/base/src/diag/output.h
            private static string sm_textOrangeCode = sm_textColorCodePrefix + "[37m";   // except this one.  this is made up

            private static string sm_textColorizedWarningPrefix = sm_textYellowCode + sm_textWarningPrefix;
            private static string sm_textColorizedErrorPrefix = sm_textRedCode + sm_textErrorPrefix;
            private static string sm_textColorizedQuitPrefix = sm_textRedCode + sm_textQuitPrefix;
            private static string sm_textColorizedDebug1Prefix = sm_textBlueCode + sm_textDebug1Prefix;
            private static string sm_textColorizedDebug2Prefix = sm_textBlueCode + sm_textDebug2Prefix;
            private static string sm_textColorizedDebug3Prefix = sm_textBlueCode + sm_textDebug3Prefix;
            private static string sm_textColorizedScriptPrefix = sm_textOrangeCode + sm_textScriptPrefix;

            private static int sm_textColorizedWarningPrefixLength = sm_textColorizedWarningPrefix.Length;
            private static int sm_textColorizedErrorPrefixLength = sm_textColorizedErrorPrefix.Length;
            private static int sm_textColorizedQuitPrefixLength = sm_textColorizedQuitPrefix.Length;
            private static int sm_textColorizedDebug1PrefixLength = sm_textColorizedDebug1Prefix.Length;
            private static int sm_textColorizedDebug2PrefixLength = sm_textColorizedDebug2Prefix.Length;
            private static int sm_textColorizedDebug3PrefixLength = sm_textColorizedDebug3Prefix.Length;
            private static int sm_textColorizedScriptPrefixLength = sm_textColorizedScriptPrefix.Length;

            private static Dictionary<string, Color> sm_foreColorCodes = BuildForeColorCodeDictionary();
            private static Dictionary<string, Color> sm_backColorCodes = BuildBackColorCodeDictionary();

            private static Dictionary<string, Color> sm_channelColors = BuildChannelColorsDictionary();
            #endregion

            #region Properties
            public static string[] SeverityPrefix = new string[]
            {
                "Fatal Error: ",
                "Error: ",
                "Warning: ",
                "",
                "Debug1: ",
                "Debug2: ",
                "Debug3: "
            };

            public static Color[] SeverityForeColor = new Color[]
            {
                Color.Red,
                Color.Red,
                Color.Goldenrod,
                Color.Empty,
                Color.Blue,
                Color.Blue,
                Color.Blue
            };

            public static string[] SeverityText = new string[]
            {
                "): Fatal Error : ",
                "): Error : ",
                "): Warning : ",
                "",
                "): Debug1 : ",
                "): Debug2 : ",
                "): Debug3 : "
            };

            public static string ColorPostfixCode = sm_textNormalCode + "\n";

            public static string ColorCodeCharacter = Convert.ToChar( 027 ).ToString();

            public static Dictionary<string, Color> ChannelColors
            {
                get
                {
                    return sm_channelColors;
                }
            }

            public string Text
            {
                get
                {
                    return m_text;
                }
            }

            public Severity TextSeverity
            {
                get
                {
                    return m_severity;
                }
            }

            public string ChannelName
            {
                get
                {
                    return m_channelName;
                }
            }

            public Color ForeColor
            {
                get
                {
                    return m_foreColor;
                }
            }

            public Color BackColor
            {
                get
                {
                    return m_backColor;
                }
            }
            #endregion

            #region Private Functions
            /// <summary>
            /// Checks the start of text for a color code and returns by references what the foreground and 
            /// background colors should be.
            /// </summary>
            /// <param name="text"></param>
            /// <param name="foreColor">pass in the default foreground color</param>
            /// <param name="backColor">pass in the default background color</param>
            /// <returns>the string without the color code</returns>
            private string GetTextColors( string text, ref Color foreColor, ref Color backColor )
            {
                string prefix = text.Substring( 0, text.Length < 5 ? text.Length : 5 );
                if ( prefix.StartsWith( sm_textNormalCode, StringComparison.Ordinal ) )
                {
                    // normal, just trim the color code
                    return text.Remove( 0, sm_textNormalCode.Length );
                }

                Color c;

                // see if it's a regular color
                if ( sm_foreColorCodes.TryGetValue( prefix, out c ) )
                {
                    foreColor = c;
                    return text.Remove( 0, prefix.Length );
                }

                // see if it's an inverse color
                if ( sm_backColorCodes.TryGetValue( prefix, out c ) )
                {
                    backColor = c;
                    return text.Remove( 0, prefix.Length );
                }

                // see if it's a long inverse color
                if ( text.Length >= 10 )
                {
                    prefix = text.Substring( 0, 10 );
                    if ( sm_backColorCodes.TryGetValue( prefix, out c ) )
                    {
                        backColor = c;
                        return text.Remove( 0, prefix.Length );
                    }
                }

                return text;
            }

            private static Dictionary<string, Color> BuildForeColorCodeDictionary()
            {
                Dictionary<string, Color> colorCodes = new Dictionary<string, Color>();

                colorCodes.Add( sm_textColorCodePrefix + "[30m", Color.Black );
                colorCodes.Add( sm_textRedCode, Color.Red );
                colorCodes.Add( sm_textColorCodePrefix + "[32m", Color.Green );
                colorCodes.Add( sm_textYellowCode, Color.Goldenrod );
                colorCodes.Add( sm_textColorCodePrefix + "[34m", Color.Blue );
                colorCodes.Add( sm_textColorCodePrefix + "[35m", Color.Purple );
                colorCodes.Add( sm_textColorCodePrefix + "[36m", Color.Cyan );

                colorCodes.Add( sm_textOrangeCode, Color.Chocolate );
                
                return colorCodes;
            }

            private static Dictionary<string, Color> BuildBackColorCodeDictionary()
            {
                Dictionary<string, Color> colorCodes = new Dictionary<string, Color>();

                colorCodes.Add( sm_textColorCodePrefix + "[41m", Color.Red );
                colorCodes.Add( sm_textColorCodePrefix + "[42m" + sm_textColorCodePrefix + "[30m", Color.Green );
                colorCodes.Add( sm_textColorCodePrefix + "[43m" + sm_textColorCodePrefix + "[30m", Color.Goldenrod );
                colorCodes.Add( sm_textColorCodePrefix + "[44m", Color.Blue );
                colorCodes.Add( sm_textColorCodePrefix + "[45m", Color.Purple );
                colorCodes.Add( sm_textColorCodePrefix + "[46m", Color.Cyan );

                return colorCodes;
            }

            private static Dictionary<string, Color> BuildChannelColorsDictionary()
            {
                Dictionary<string, Color> channelColors = new Dictionary<string, Color>();
                
                channelColors.Add( "Script", Color.Chocolate );

                return channelColors;
            }
            #endregion
        }

        /// <summary>
        /// This is called to update the UI.
        /// </summary>
        public void ProcessOutputPipe()
        {
            if ( m_MainForm != null )
            {
                // No need to lock for reading count.  If the value we get is wrong the "missing" lines of text
                // will be included in the next call to this method.
                int count = Math.Min( (m_MainForm as MainWindow).PauseRichTextBoxAllOutput ? 2000 : 500, m_outputQueue.Count );
                if ( count > 0 )
                {
                    m_MainForm.Invoke( sm_BeginAddOutputText, new object[] { this, EventArgs.Empty } );

                    while ( count > 0 )
                    {
                        OutputTextEventArgs e;
                        lock (m_syncObject)
                        {
                            e = m_outputQueue.Dequeue();
                        }

                        m_MainForm.Invoke( sm_AddOutputText, new object[] { this, e } );

                        --count;
                    }

                    m_MainForm.Invoke( sm_EndAddOutputText, new object[] { this, EventArgs.Empty } );
                }
            }
        }

        public void AddOutputText( TTYLine outputLine )
        {
            if ( (m_MainForm != null) && (sm_AddOutputText != null) )
            {
                m_MainForm.Invoke( sm_BeginAddOutputText, new object[] { this, EventArgs.Empty } );

                OutputTextEventArgs e = new OutputTextEventArgs( outputLine );
                m_MainForm.Invoke( sm_AddOutputText, new object[] { this, e } );

                m_MainForm.Invoke( sm_EndAddOutputText, new object[] { this, EventArgs.Empty } );
            }
        }

        public WidgetViewBase AddWidgetView( string appName, WidgetViewBase.CreateDel createDel, bool startup )
        {
            return AddWidgetView( appName, System.Guid.NewGuid(), createDel, startup );
        }

        public WidgetViewBase AddWidgetView( string appName, System.Guid guid, WidgetViewBase.CreateDel createDel, bool startup )
        {
            Debug.Assert( sm_DockControlCreatorWidgetView != null, "sm_DockControlCreatorWidgetView is null" );
            TD.SandDock.DockControl dockControl = sm_DockControlCreatorWidgetView( guid );

            // don't create a new view if we already have one for this DockControl
            if ( (dockControl.Controls.Count > 0) && (dockControl.Controls[0] is WidgetViewBase) )
            {
                DockableViewManager.Instance.PostOpenView( dockControl, dockControl.Controls[0] as WidgetViewBase );
                dockControl.Open();
                return dockControl.Controls[0] as WidgetViewBase;
            }

            WidgetViewBase view = createDel( appName, m_BankManager, dockControl );
            view.Guid = guid;

            dockControl.SuspendLayout();
            DockableViewManager.Instance.CreateView( dockControl, view, _log );
            if ( DockableViewManager.Instance.OpenView( dockControl, view.InitiallyFloating, view.InitialDockLocation, _log ) )
            {
                DockableViewManager.Instance.PostOpenView( dockControl, view );
            }
            dockControl.ResumeLayout( !startup );

            return view;
        }

        public RenderWindow AddRenderWindow( System.Guid guid )
        {
            Debug.Assert( sm_DockControlCreatorRenderWindow != null, "sm_DockControlCreatorRenderWindow is null" );
            m_DockControlRenderWindow = sm_DockControlCreatorRenderWindow( guid );

            // don't create a new render window if we already have one for this DockControl
            if ( (m_DockControlRenderWindow.Controls.Count > 0) && (m_DockControlRenderWindow.Controls[0] is RenderWindow) )
            {
                m_DockControlRenderWindow.Open();
                m_RenderWindow = m_DockControlRenderWindow.Controls[0] as RenderWindow;
                return m_RenderWindow;
            }

            m_DockControlRenderWindow.Text = m_DockControlRenderWindow.TabText = m_VisibleName;
            m_RenderWindow = RenderWindow.CreateWindow( m_BankManager, m_DockControlRenderWindow, _log );
            m_RenderWindow.MyRageApplication = this;

            if ( RenderWindow.PlatformString == "Win32" || RenderWindow.PlatformString == "Win64")
            {
                Process[] processes = Process.GetProcesses();
                foreach ( Process p in processes )
                {
                    // a bit of a hack, but this is what we get when the user started their Win32 application
                    // without Rag, then used the "Start Rag" button in the "rage - Bank" bank to start Rag.
                    if ( p.MainWindowTitle == "rage - Bank" )
                    {
                        m_RenderWindow.SelfPaintWin32 = true;
                        break;
                    }
                }
            }

            m_RenderWindow.DockControl.Open();

            if ( DockControlManager.Instance.MasterDockControlViews != null )
            {
                DockControlManager.Instance.MasterDockControlViews.Close();
                DockControlManager.Instance.MasterDockControlViews = null;
            }

            return m_RenderWindow;
        }

        public void RemoveRenderWindow()
        {
            RenderWindow.DestroyWindow(m_RenderWindow);
            m_DockControlRenderWindow = null;
            m_RenderWindow = null;
        }

        public void AddOutputWindow(string channelName, string color)
        {
            if (!TTYLine.ChannelColors.ContainsKey(channelName))
            {
                TTYLine.ChannelColors.Add(channelName, Widget.DeserializeColor(color));
            }

            if ((m_MainForm != null) && (sm_CreateOutputWindowDel != null))
            {
                m_MainForm.Invoke(sm_CreateOutputWindowDel, new object[] { channelName, color });
            }
        }

        public void AddSubChannelInfo(string channelName, string subChannelName)
        {
            if (m_MainForm != null && sm_AddSubChannelInfoDel != null)
            {
                m_MainForm.Invoke(sm_AddSubChannelInfoDel, new object[] { channelName, subChannelName });
            }
        }

        public bool ConnectPipes()
        {
            if ( (RageApplication.MainApp == this) || (m_Process != null) )
            {
                // start output pipe:
                if ( !ConnectToPipe( m_PipeNameOutput, m_PipeOutput ) )
                {
                    rageMessageBox.ShowError(
                        String.Format("Could not connect to application '{0}'.  Unable to connect to pipe {1}.   Rag will be terminated.", this.ExeName, m_PipeOutput.Name), "Connection Failed");
                    return false;
                }

                // start bank pipe:
                if ( ConnectToPipe( m_PipeNameBank, m_PipeBank ) )
                {
                    m_PipeBank.NoDelay = true;
                }
                else
                {
                    rageMessageBox.ShowError(
                        String.Format("Could not connect to Rage application '{0}'.  Unable to connect to pipe {1}.  Rag will be terminated.", this.ExeName, m_PipeBank.Name), "Connection Failed");
                    return false;
                }

                BankRemotePacket packet = new BankRemotePacket( m_PipeBank );
                packet.Begin( BankRemotePacket.EnumPacketType.USER_3, BankManager.GetStaticGUID(), 0 );
                m_RenderWindow = GetOrAddRenderWindow();

                Debug.Assert( m_RenderWindow != null, "RenderWindow is null" );
                Debug.Assert( !m_RenderWindow.IsDisposed, "RenderWindow disposed" );
                Debug.Assert( m_RenderWindow.DockControl != null, "RenderWindow.DockControl is null" );
                Debug.Assert( !m_RenderWindow.DockControl.IsDisposed, "RenderWindow.DockControl disposed" );

                packet.Write_u32( (uint)m_RenderWindow.PictureBoxHandle );
                packet.Send();

                // connect to the input handler:
                if ( !m_InputHandler.Connect( m_PipeNameEvents ) )
                {

                    rageMessageBox.ShowError(
                        String.Format( "Could not connect to Rage application '{0}'.  Unable to connect to pipe {1}. Rag will be terminated.", this.ExeName, m_PipeNameEvents.Name ), "Connection Failed" );
                    return false;
                }
            }

            m_WasConnected = true;
            return true;
        }

        public void StartApp( bool connectPipes )
        {
            // find our render window:
            m_RenderWindow = GetOrAddRenderWindow();

            if ( (m_Process != null) && m_Process.IsDone )
            {
                m_Process = null;
            }

            if ( m_Process == null )
            {
                m_Process = new ProcessCaller( m_MainForm, _log );
                m_Process.FileName = ExePath;

                if ( String.IsNullOrEmpty( m_PipeNameBank.ToString() ) )
                {
                    m_PipeNameBank.AddString( "Bank2" + DateTime.Now.ToString( "yyMMddHHmmssf" ) );
                }

                m_Process.Arguments = String.Format( "-waitforpipe -bankpipename=\"{0}\" -socketbaseport={1} -remotebank -setHwndMain={2} {3}",
                    m_PipeNameBank, m_PortNumber, m_RenderWindow.PictureBoxHandle, this.Args );

                if ( RageApplication.MainApp != this )
                {
                    m_Process.Arguments += String.Format( " -ragviewer -appname \"{1}\"", this.VisibleName );
                }

                //m_Process.StdErrReceived += new DataReceivedHandler(WriteStreamInfo);
                //m_Process.StdOutReceived += new DataReceivedHandler(WriteStreamInfo);
                //m_Process.Completed += new EventHandler(ProcessCompletedOrCanceled);
                //m_Process.Cancelled += new EventHandler(ProcessCompletedOrCanceled);
                //m_Process.Failed += no event handler for this one, yet.

                // start the child app:
                m_Process.Start();
            }

            if ( (m_KillProcess != null) && m_KillProcess.HasExited )
            {
                m_KillProcess = null;
            }

            if ( m_KillProcess == null )
            {
                // wait until the previous process has been started:
                m_KillProcess = new Process();
                m_KillProcess.StartInfo.FileName = Application.ExecutablePath;

                // wait until the child app has started.  NOTE that this isn't perfect -
                // if the parent process is killed before we get here, the process we 
                // wish to kill will still be running!
                int processId = m_Process.ProcessID;
                while ( processId == -1 )
                {
                    Thread.Sleep( 100 );
                    processId = m_Process.ProcessID;
                }

                Process currentProcess = Process.GetCurrentProcess();
                m_KillProcess.StartInfo.Arguments = String.Format( "-processToKill={0} -processToWatch={1}", processId, currentProcess.Id );
                m_KillProcess.StartInfo.CreateNoWindow = true;

                // start the killing process:
                m_KillProcess.Start();
            }

            if ( connectPipes && !ConnectPipes() )
            {
                StopApp( false );

                return;
            }

            m_Started = true;
        }

        public void StopApp( bool isFinalStop )
        {
            m_stoppingMutex.WaitOne();

            if ( this.IsStopped )
            {
                return;
            }

            _log.MessageCtx(LogCtx, "StopApp (isFinalStop={0}, WasConnected={1}, '{2}' connected={3}, '{4}' connected={5}, Sensor {6} quit={7})",
                isFinalStop, this.WasConnected, m_PipeOutput.Name, m_PipeOutput.IsConnected(),
                m_PipeBank.Name, m_PipeBank.IsConnected(), Sensor.CurrentSensor.Platform, Sensor.CurrentSensor.HasQuit );
            
            m_PipeBank.SendQuitMessage();
            m_PipeBank.Close();
            _log.MessageCtx(LogCtx, "Disconnected from pipe '{0}'.", m_PipeBank.Name);

            m_PipeOutput.Close();
            _log.MessageCtx(LogCtx, "Disconnected from pipe '{0}'.", m_PipeOutput.Name);
            
            m_InputHandler.Close();

            // keep track if we've been started so serialization will know when it saves application state:
            if ( !isFinalStop )
            {
                m_Started = false;
            }

            m_Stopped = true;

            m_stoppingMutex.ReleaseMutex();
        }
        #endregion

        #region Private Functions
        private void FindAvailableSockets()
        {
            int basePort = sm_BasePortNumber;

            _log.MessageCtx(LogCtx, "Reserving Sockets...");

            m_reservedSockets.Clear();
            int i;
            for (i = 0; i < c_numSocketsToReserve && (basePort + i) < UInt16.MaxValue; ++i)
            {
                try
                {
                    TcpListener listener = new TcpListener( m_IPAddress, basePort + i );
                    listener.Start();
                    m_reservedSockets.Add( listener );
                }
                catch (SocketException)
                {
                    _log.WarningCtx(LogCtx, "Could not reserve port {0}", basePort + i);

                    // Change the base port to be the next potentially available one.
                    basePort += i + 1;

                    // Set i to -1 restart the loop.
                    i = -1;

                    // Clear out any sockets we've already reserved.
                    StopListeners(m_reservedSockets);
                    m_reservedSockets.Clear();
                }
            }

            if (i == UInt16.MaxValue)
            {
                throw new ArgumentOutOfRangeException("Unable to reserver port as couldn't reserve a consecutive block of 50 ports.");
            }

            m_PortNumber = basePort;
            sm_BasePortNumber = m_PortNumber + 100;

            _log.MessageCtx(LogCtx, "Done Reserving Sockets.  Port = {0}", basePort);
        }

        /// <summary>
        /// Stops a list of listeners.
        /// </summary>
        private void StopListeners(IList<TcpListener> listeners)
        {
            foreach (TcpListener listener in listeners)
            {
                if (listener != null)
                {
                    listener.Stop();
                }
            }
        }

        private RenderWindow AddRenderWindow()
        {
            return AddRenderWindow( System.Guid.NewGuid() );
        }

        private bool ConnectToPipe( PipeID pipeId, INamedPipe pipe )
        {
            if ( pipe.IsConnected() )
            {
                return true;
            }

            _log.MessageCtx(LogCtx, "Waiting for client '{0}' to connect to pipe '{1}' on port {2}...", this.ExeName, pipeId.ToString(), pipeId.Port);
            
            bool result = pipe.Create( pipeId, false );
            if ( result )
            {
                _log.MessageCtx(LogCtx, "Connected to pipe '{0}' on remote port {1}", pipeId.ToString(), ((IPEndPoint)pipe.Socket.RemoteEndPoint).Port);
            }
            else
            {
                _log.ErrorCtx(LogCtx, "Could not connect to pipe '{0}'!", pipeId.ToString());
            }

            return result;
        }

        private RenderWindow GetOrAddRenderWindow()
        {
            // find our render window:
            RenderWindow renderWindow = null;
            if ( m_DockControlRenderWindow != null )
            {
                renderWindow = RenderWindow.DockControlToRenderWindow( m_DockControlRenderWindow );
            }

            if ( renderWindow == null )
            {
                AddRenderWindow();

                // this may look weird, but AddRenderWindow will get or set m_DockControlRenderWindow
                renderWindow = RenderWindow.DockControlToRenderWindow( m_DockControlRenderWindow );
                Debug.Assert( renderWindow != null, "renderWindow is null" );
            }

            return renderWindow;
        }
        #endregion

        #region Public Static Functions
        public static void SerializeBanks()
        {
            if ( sm_Applications != null )
            {
                foreach ( RageApplication app in sm_Applications )
                {
                    app.m_BankManager.SerializeBanks( app.ExeName );
                }
            }
        }

        public static bool MoveFileSafe( string srcFilename, string destFilename, ILog log )
        {
            int retryCount = 10;
            bool success = false;
            do
            {
                try
                {
                    rageStatus status;
                    rageFileUtilities.CopyFile( srcFilename, destFilename, out status );
                    success = status.Success();
                    break;
                }
                catch
                {
                    --retryCount;
                    Thread.Sleep( 100 );
                }
            }
            while ( retryCount > 0 );

            if ( !success )
            {
                log.Warning("Unable to copy '{0}' to '{1}'.", srcFilename, destFilename);
            }
            else
            {
                try
                {
                    if ( File.Exists( srcFilename ) )
                    {
                        rageStatus status;
                        rageFileUtilities.DeleteLocalFile( srcFilename, out status );
                    }
                }
                catch ( Exception e )
                {
                    log.ToolException(e, "Unable to delete '{0}'.", srcFilename);
                }
            }

            return success;
        }

        /// <summary>
        /// This is the thread function
        /// </summary>
        public static void UpdatePipes(ILog log)
        {
            log.Message("Started {0}", sm_updatePipeThread.Name);

            while ( true )
            {
                sm_updatePipeThreadMutex.WaitOne();

                try
                {
                    bool stopAllApps = false;
                    if ( MainApp == null || MainApp.PipesDisconnected )
                    {
                        stopAllApps = true;
                    }

                    List<Socket> readSockets = new List<Socket>();
                    foreach ( RageApplication app in sm_Applications )
                    {
                        readSockets.Add( app.TheOutputPipe.Socket );
                        readSockets.Add( app.TheBankPipe.Socket );
                    }

                    List<Socket> writeSockets = new List<Socket>();
                    List<Socket> errorSockets = new List<Socket>();

                    try
                    {
                        Socket.Select( readSockets, writeSockets, errorSockets, 1000000 );
                    }
                    catch ( ThreadAbortException )
                    {
                        break;
                    }
                    catch ( Exception e )
                    {
                        if ( !stopAllApps )
                        {
                            log.ToolException(e, "Exception");
                            sm_updatePipeThreadMutex.ReleaseMutex();
                            continue;
                        }
                    }

                    foreach ( RageApplication app in sm_Applications )
                    {
                        // make sure the main application has disconnected:
                        if ( stopAllApps || (app.PipesDisconnected && app.WasConnected) )
                        {
                            if ( !app.IsStopped )
                            {
                                app.StopApp( stopAllApps );
                            }
                        }
                        else
                        {
                            if ( app.TheOutputPipe.IsValid() && readSockets.Contains( app.TheOutputPipe.Socket ) )
                            {
                                app.ReadOutputPipe();
                            }

                            if ( app.TheBankPipe.IsValid() && readSockets.Contains( app.TheBankPipe.Socket ) )
                            {
                                PacketProcessor.Update( app.m_PipeBank );
                            }
                        }
                    }

                    sm_updatePipeThreadMutex.ReleaseMutex();
                }
                catch ( ThreadAbortException )
                {
                    break;
                }                
            }

            sm_updatePipeThreadMutex.ReleaseMutex();

            log.Message("Ended {0}", sm_updatePipeThread.Name);
            sm_updatePipeThread = null;
        }

        /// <summary>
        /// This updates the UI
        /// </summary>
        public static void ProcessPipes()
        {
            bool stopAllApps = RageApplication.MainApp.PipesDisconnected;

            foreach ( RageApplication app in sm_Applications )
            {
                // make sure the main application has disconnected:
                if ( !stopAllApps && !(app.PipesDisconnected && app.WasConnected) )
                {
                    app.ProcessOutputPipe();

                    PacketProcessor.Process( app.m_PipeBank );
                }
            }
        }

        public static bool ReadMainAppInfo( INamedPipe pipe, ILog log, String logCtx )
        {
            log.MessageCtx(logCtx, "RageApplication::ReadMainAppInfo" );
            IPAddress address = IPAddress.Parse( pipe.Socket.LocalEndPoint.ToString().Split( ':' )[0] );
            sm_Applications = null;
            sm_MasterAppIndex = 0;
            sm_mainApplication = new RageApplication( address, log );

            // look for handshake packet
            log.MessageCtx(logCtx, "Processing handshake packets...");
            bool timedOut;
            if ( !AppRemotePacket.ReadPackets( pipe, log, logCtx, out timedOut ) )
            {
                if ( timedOut )
                {
                    rageMessageBox.ShowError( "Rag timed out waiting to handshake with the application.  Either an unauthorized application " +
                        "attempted to connect with Rag, or your application may not be using Rage bank code version " + RAG_VERSION + ".",
                        "Connection Timed Out" );
                }
                return false;
            }

            // send the base socket and version numbers
            log.MessageCtx(logCtx, "Sending port and version");
            AppRemotePacket p = new AppRemotePacket( pipe );
            p.BeginWrite();
            p.Write_s32( sm_mainApplication.m_PortNumber );
            p.Write_float( RAG_VERSION );
            p.Send();

            // look for additional packets
            log.MessageCtx(logCtx, "Reading additional app packets...");
            if ( !AppRemotePacket.ReadPackets( pipe, log, logCtx, out timedOut ) )
            {
                if ( timedOut )
                {
                    rageMessageBox.ShowError( "Rag timed out waiting for the application startup data.", "Connection Timed Out" );
                }
                return false;
            }

            // make sure we have something to run:
            if ( sm_Applications.Length < 1 )
            //if (sm_Applications == null || sm_Applications.Length < 1)
            {
                rageMessageBox.ShowError( "No applications were specified to run!", "No Applications" );
                return false;
            }

            log.MessageCtx(logCtx, "Handshake on pipe {0} done.", pipe.Name);
            log.MessageCtx(logCtx, "Establishing connection with client: {0} {1}", sm_mainApplication.ExeName, sm_mainApplication.Args);
            return true;
        }

        public static RageApplication FindApplication( INamedPipe bankPipe )
        {
            foreach ( RageApplication app in sm_Applications )
            {
                if ( app.m_PipeBank == bankPipe )
                    return app;
            }

            return null;
        }

        public static RageApplication FindApplication( string name )
        {
            foreach ( RageApplication app in sm_Applications )
            {
                if ( app.ExeName.ToLower() == name.ToLower() )
                    return app;
            }

            return null;
        }

        public static bool ReadAppInfo( string fileName, IPAddress address, ILog log )
        {
            XmlTextReader xmlReader = null;

            // make sure our file exists:
            if ( File.Exists( fileName ) == false )
                return false;

            // parse the layout file:
            try
            {
                xmlReader = new XmlTextReader( fileName );
                xmlReader.WhitespaceHandling = WhitespaceHandling.None;

                List<RageApplication> appList = new List<RageApplication>();
                // read until </Applications>:
                while ( xmlReader.NodeType != XmlNodeType.EndElement || xmlReader.Name != "Applications" )
                {
                    xmlReader.Read();

                    RageApplication app = null;
                    if ( xmlReader.NodeType == XmlNodeType.Element && xmlReader.Name == "Application" )
                    {
                        // create the application:
                        app = new RageApplication( address, log );
                        appList.Add( app );

                        string filePath = xmlReader["Path"];
                        if ( filePath != string.Empty )
                        {
                            app.m_Path = filePath;
                        }

                        string args = xmlReader["Args"];
                        if ( args != string.Empty )
                        {
                            app.m_Args = args;
                        }

                        string visibleName = xmlReader["VisibleName"];
                        if ( visibleName != null && visibleName != string.Empty )
                        {
                            app.m_VisibleName = visibleName;
                        }
                        else if ( filePath != null && filePath != string.Empty )
                        {
                            app.m_VisibleName = System.IO.Path.GetFileNameWithoutExtension( filePath );
                        }
                        else
                        {
                            app.m_VisibleName = "Unnamed Application";
                        }
                    }

                    // create pipe names:
                    if ( app != null )
                    {
                        app.m_PipeNameBank.Name = app.m_VisibleName + ".pipe.bank";
                        app.m_PipeNameOutput.Name = app.m_VisibleName + ".pipe.output";
                        app.m_PipeNameEvents.Name = app.m_VisibleName + ".pipe.event_queue";
                    }
                }

                // copy application info:
                if ( appList.Count > 0 )
                {
                    // add the main application as well:
                    appList.Insert( 0, sm_Applications[0] );
                    sm_Applications = new RageApplication[appList.Count];
                    appList.CopyTo( sm_Applications );
                }
            }
            catch ( Exception e )
            {
                rageMessageBox.ShowWarning( "Problem parsing layout file: " + e.Message, "Problem parsing layout file!" );
                return false;
            }

            return true;
        }

        public static bool PacketHandler( AppRemotePacket packet, ILog log, String logCtx )
        {
            uint index;
            bool success = true;

            // our initial handshake packet containing the game's rag version number
            if ( packet.AppCommand == AppRemotePacket.EnumPacketType.HANDSHAKE )
            {
                log.MessageCtx(logCtx, "Received initial handshake packet.");
                packet.Begin();
                float ragVersion = ragVersion = packet.Read_float();
                if (packet.CanRead())
                {
                    packet.Read_const_char();   // Application name, required for protocol but not used in this version of rag
                }
                packet.End();
                log.MessageCtx(logCtx, "RAG Version: {0}", ragVersion);

                if ( ragVersion != RAG_VERSION )
                {
                    string title;
                    string msg;
                    if ( ragVersion == 0.0f )
                    {
                        title = "Rag Version Number Not Received";
                        msg = "Rag and Rage bank code are out of sync.  Need " + RAG_VERSION + " from Bank.\n\nPlease update your bank code.";
                    }
                    else
                    {
                        title = "Rag Version Mismatch";
                        msg = "Rag and Rage bank code are out of sync.  Expected " + RAG_VERSION + " but received " + ragVersion + "from Bank.\n\nPlease update " + ((RAG_VERSION < ragVersion) ? "Rag." : "your bank code.");
                    }

                    rageMessageBox.ShowError( msg, title );
                    Debug.Fail( title );
                    return false;
                }
            }
            // # of applications to create:
            else if ( packet.AppCommand == AppRemotePacket.EnumPacketType.NUM_APPS )
            {
                log.MessageCtx(logCtx, "Received number of applications packet.");
                packet.Begin();
                uint numApps = packet.Read_u32();
                sm_MasterAppIndex = packet.Read_u32();

                Debug.Assert( sm_MasterAppIndex < numApps, "sm_MasterAppIndex < numApps" );

                if ( packet.Length > 8 )
                {
                    rageMessageBox.ShowError( "Rag and Rage bank code are out of sync.  Need " + RAG_VERSION + " from Bank.\n\nPlease update your bank code.", "Rag Version Mismatch" );
                    Debug.Fail( "Rag Version Mismatch" );
                    return false;
                }

                packet.End();
                log.MessageCtx(logCtx, "Num Apps: {0}", numApps);

                sm_Applications = new RageApplication[numApps];
                for ( int i = 0; i < numApps; i++ )
                {
                    if ( i == sm_MasterAppIndex )
                    {
                        // we already created the master app, so add it at the correct index
                        sm_Applications[i] = sm_mainApplication;
                    }
                    else
                    {
                        sm_Applications[i] = new RageApplication(IPAddress.Any, log);
                    }
                }

                // try to retrieve the ip address of the application we're connected to
                if ( packet.Pipe is NamedPipeSocket )
                {
                    NamedPipeSocket namedPipe = packet.Pipe as NamedPipeSocket;
                    Sensor.MainApplicationIpAddress = namedPipe.GetRemoteIpAddress();
                }
            }
            // the path of an application:
            else if ( packet.AppCommand == AppRemotePacket.EnumPacketType.APP_NAME )
            {
                log.MessageCtx(logCtx, "Received application name packet.");
                packet.Begin();
                index = packet.Read_u32();
                String appName = packet.Read_const_char();
                sm_Applications[index].m_Path += appName;
                packet.End();
                log.MessageCtx(logCtx, "App{0} Name: {1}", index, appName);
            }
            // the visible name of an application:
            else if ( packet.AppCommand == AppRemotePacket.EnumPacketType.APP_VISIBLE_NAME )
            {
                log.MessageCtx(logCtx, "Received visible application name packet.");
                packet.Begin();
                index = packet.Read_u32();
                String visibleName = packet.Read_const_char();
                sm_Applications[index].m_VisibleName += visibleName;
                packet.End();
                log.MessageCtx(logCtx, "Visible App{0} Name: {1}", index, visibleName);
            }
            // the application args:
            else if ( packet.AppCommand == AppRemotePacket.EnumPacketType.APP_ARGS )
            {
                log.MessageCtx(logCtx, "Received application arguments packet.");
                packet.Begin();
                index = packet.Read_u32();
                String args = packet.Read_const_char();
                if (!String.IsNullOrEmpty(args))
                {
                    if (!String.IsNullOrEmpty(sm_Applications[index].m_Args))
                    {
                        sm_Applications[index].m_Args += " ";
                    }
                    sm_Applications[index].m_Args += args;
                }
                packet.End();
                log.MessageCtx(logCtx, "App{0} Args: {1}", index, args);
            }
            // the name of the pipe for bank communications:
            else if ( packet.AppCommand == AppRemotePacket.EnumPacketType.PIPE_NAME_BANK )
            {
                log.MessageCtx(logCtx, "Received bank pipe name packet.");
                packet.Begin();
                index = packet.Read_u32();
                String name = packet.Read_const_char();
                sm_Applications[index].m_PipeNameBank.AddString(name);
                packet.End();
                log.MessageCtx(logCtx, "Bank pipe {0}: {1}", index, name);
            }
            // the name of the pipe for text output communications:
            else if ( packet.AppCommand == AppRemotePacket.EnumPacketType.PIPE_NAME_OUTPUT )
            {
                log.MessageCtx(logCtx, "Received output pipe name packet.");
                packet.Begin();
                index = packet.Read_u32();
                String name = packet.Read_const_char();
                sm_Applications[index].m_PipeNameOutput.AddString(name);
                packet.End();
                log.MessageCtx(logCtx, "Output pipe {0}: {1}", index, name);
            }
            // the name of the pipe for event handling communications:
            else if ( packet.AppCommand == AppRemotePacket.EnumPacketType.PIPE_NAME_EVENTS )
            {
                log.MessageCtx(logCtx, "Received events pipe name packet.");
                packet.Begin();
                index = packet.Read_u32();
                String name = packet.Read_const_char();
                sm_Applications[index].m_PipeNameEvents.AddString(name);
                packet.End();
                log.MessageCtx(logCtx, "Event pipe {0}: {1}", index, name);
            }
            else if ( packet.AppCommand == AppRemotePacket.EnumPacketType.PLATFORM_INFO )
            {
                log.MessageCtx(logCtx, "Received platform info packet.");
                packet.Begin();
                index = packet.Read_u32();
                string platformString = packet.Read_const_char();
                RenderWindow.PlatformString = platformString;
                packet.End();
                log.MessageCtx(logCtx, "Platform name {0}: {1}", index, platformString);
            }
            else if ( packet.AppCommand == AppRemotePacket.EnumPacketType.PS3_TARGET_ADDRESS )
            {
                log.MessageCtx(logCtx, "Received ps3 target address packet.");
                packet.Begin();
                index = packet.Read_u32();
                string ps3TargetAddr = packet.Read_const_char();
                Sensor.PS3TargetIpAddress = ps3TargetAddr;
                packet.End();
                log.MessageCtx(logCtx, "PS3 target ip {0}: {1}", index, ps3TargetAddr);
            }
            else if (packet.AppCommand == AppRemotePacket.EnumPacketType.BUILD_CONFIG)
            {
                log.MessageCtx(logCtx, "Received build config packet.");
                packet.Begin();
                index = packet.Read_u32();
                string buildConfigString = packet.Read_const_char();
                RenderWindow.BuildConfigString = buildConfigString;
                packet.End();
                log.MessageCtx(logCtx, "Build Config {0}: {1}", index, buildConfigString);
            }
            else
            {
                log.WarningCtx(logCtx, "Unknown handshake packet received {0}.", packet.AppCommand);
                success = false;
            }

            return success;
        }

        public static void LoadApps( Control mainForm, ToolStripMenuItem menuItem, ILog log,
            EventHandler beginAddOutputTextDel, OutputTextEventHandler addOutputTextDel, EventHandler endAddOutputTextDel,
            CreateOutputWindowDel createOutputWindowDel,
            AddSubChannelInfoDel addSubChannelInfoDel)
        {
            sm_BeginAddOutputText = beginAddOutputTextDel;
            sm_AddOutputText = addOutputTextDel;
            sm_EndAddOutputText = endAddOutputTextDel;
            sm_CreateOutputWindowDel = createOutputWindowDel;
            sm_AddSubChannelInfoDel = addSubChannelInfoDel;

            // set the default state for the menu item:

            menuItem.DropDownItems.Clear();
            menuItem.Visible = false;
            menuItem.Enabled = false;

            if ( sm_Applications != null )
            {
                // start the applications:
                foreach ( RageApplication app in sm_Applications )
                {
                    app.m_MainForm = mainForm;

                    if ( app != RageApplication.MainApp )
                    {
                        app.ShouldStart = true;
                    }

                    // start it if we have to:
                    if ( app.ShouldStart && !app.IsStarted )
                    {
                        log.Message("Starting app {0}", app.VisibleName);
                        app.StartApp( false );
                    }
                }
                
               
                // connect all pipes:
                foreach ( RageApplication app in sm_Applications )
                {
                    if ( !app.ConnectPipes() )
                    {
                        app.StopApp( false );
                    }
                }
            }

            BankRemotePacketProcessor.BeginBankUpdate = new BankRemotePacketProcessor.BeginBankUpdateDelegate( RageApplication.BeginBankUpdate );
            BankRemotePacketProcessor.EndBankUpdate = new BankRemotePacketProcessor.EndBankUpdateDelegate( RageApplication.EndBankUpdate );

            if ( sm_updatePipeThread == null )
            {
                sm_updatePipeThread = new Thread(() => UpdatePipes(log));
                sm_updatePipeThread.Name = "Update Pipe Thread";
                sm_updatePipeThread.Priority = ThreadPriority.Highest;
            }

            if ( !sm_updatePipeThread.IsAlive )
            {
                sm_updatePipeThread.Start();
            }
        }

        public static void PrepareToStopAppsFinal()
        {
            sm_updatePipeThreadMutex.WaitOne();
            if ( (sm_updatePipeThread != null) && sm_updatePipeThread.IsAlive )
            {
                sm_updatePipeThread.Abort();
            }
            sm_updatePipeThreadMutex.ReleaseMutex();
        }

        public static void StopAppsFinal(ILog log)
        {
            StopApps(log, true );
        }

        public static void StopApps(ILog log)
        {
            StopApps(log, false );
        }

        public static void ActivateAllRenderWindows()
        {
            // make sure we see the floating windows:
            if ( sm_Applications != null )
            {
                foreach ( RageApplication app in sm_Applications )
                {
                    if ( (app.m_DockControlRenderWindow != null) && (app.m_DockControlRenderWindow.DockSituation == DockSituation.Floating) )
                    {
                        app.m_DockControlRenderWindow.Open();
                    }
                }
            }
        }

        public static void CallInitFinished()
        {
            foreach ( RageApplication app in sm_Applications )
            {
                (app.BankMgr as BankManager).OnInitFinished();
            }
        }

        public static void AddBank( BankPipe pipe, WidgetBank bank )
        {
            RageApplication app = FindApplication( pipe );
            if ( app != null )
            {
                (app.BankMgr as BankManager).AddBank( bank );
            }
        }

        public static void RemoveBank( BankPipe pipe, WidgetBank bank )
        {
            RageApplication app = FindApplication( pipe );
            if ( app != null )
            {
                (app.BankMgr as BankManager).RemoveBank( bank );
            }
        }

        public static void ClearStatics()
        {
            sm_Applications = null;
            sm_mainApplication = null;
            sm_BasePortNumber = c_defaultBasePortNumber;
            sm_MasterAppIndex = 0;
            sm_AddOutputText = null;
            sm_CreateOutputWindowDel = null;
            sm_DockControlCreatorWidgetView = null;
            sm_DockControlCreatorRenderWindow = null;
        }
        #endregion

        #region Private Static Functions
        private void NamedPipeSocket_ReadWriteFailure( object sender, UnhandledExceptionEventArgs e )
        {
            _log.ToolExceptionCtx(LogCtx, (e.ExceptionObject as Exception), "ReadWriteFailure on pipe '{0}'.", sender.ToString());
        }

        private static void StopApps(ILog log, bool isFinalStop )
        {
            log.Message("StopApps (isFinalStop={0})", isFinalStop );

            if ( sm_Applications != null )
            {
                foreach ( RageApplication app in sm_Applications )
                {
                    if ( !app.IsStopped )
                    {
                        app.StopApp( isFinalStop );
                    }
                }
            }

            // Wait for the UpdatePipe thread to abort
            sm_updatePipeThreadMutex.WaitOne();
            while ( (sm_updatePipeThread != null) && sm_updatePipeThread.IsAlive )
            {
                sm_updatePipeThread.Abort();
                Thread.Sleep( 100 );
            }
            sm_updatePipeThreadMutex.ReleaseMutex();

            sm_Applications = null;
            sm_mainApplication.Dispose();
            sm_mainApplication = null;
            sm_MasterAppIndex = 0;
            sm_BasePortNumber = c_defaultBasePortNumber;
        }

        private static void BeginBankUpdate()
        {
            foreach ( IDockableView view in DockableViewManager.Instance.DockableViews )
            {
                if ( view is WidgetView )
                {
                    // begin update for BankTreeView, FavoritesView and FindView
                    (view as WidgetView).BeginUpdate();
                }
            }
        }

        private static void EndBankUpdate()
        {
            foreach ( IDockableView view in DockableViewManager.Instance.DockableViews )
            {
                if ( view is WidgetView )
                {
                    // end update for BankTreeView, FavoritesView and FindView
                    (view as WidgetView).EndUpdate();
                }
            }
        }
        #endregion

        #region IDisposable Implementation
        /// <summary>
        /// Make sure the reserved sockets are correctly disposed of.
        /// </summary>
        public void Dispose()
        {
            StopListeners(m_reservedSockets);
            m_reservedSockets.Clear();
        }
        #endregion // IDisposable Implementation

        #region EventArgs
        public class OutputTextEventArgs : EventArgs
        {
            public OutputTextEventArgs()
            {

            }

            public OutputTextEventArgs( TTYLine outputLine )
            {
                m_outputLine = outputLine;
            }

            #region Variables
            private TTYLine m_outputLine = new TTYLine( string.Empty );
            #endregion

            #region Properties
            public TTYLine Output
            {
                get
                {
                    return m_outputLine;
                }
            }
            #endregion
        };
        #endregion
    }

    /// <summary>
    /// Summary description for AppRemotePacket.
    /// </summary>
    public class AppRemotePacket : RemotePacket
    {
        public enum EnumPacketType
        {
            HANDSHAKE,
            NUM_APPS,
            APP_NAME,
            APP_VISIBLE_NAME,
            APP_ARGS,
            PIPE_NAME_BANK,
            PIPE_NAME_OUTPUT,
            PIPE_NAME_EVENTS,
            END_OUTPUT,
            WINDOW_HANDLE,
            PLATFORM_INFO,
            PS3_TARGET_ADDRESS,
            BUILD_CONFIG
        };

        public EnumPacketType AppCommand
        {
            get
            {
                return (EnumPacketType)m_Header.m_Command;
            }
        }

        public static bool ReadPackets(INamedPipe pipe, ILog log, String logCtx, out bool timedOut)
        {
            AppRemotePacket p = new AppRemotePacket(pipe);

            timedOut = false;
            int retries = 40;   // 4 seconds

            bool loop = true;
            while (loop)
            {
                if (!pipe.HasData())
                {
                    --retries;
                    if (retries == 0)
                    {
                        timedOut = true;
                        return false;
                    }

                    Thread.Sleep(100);
                }
                else
                {
                    retries = 40;   // reset
                    while (pipe.HasData())
                    {
                        p.ReceiveHeader(pipe);
                        if (p.m_Header.m_Length > 0)
                        {
                            p.ReceiveStorage(pipe);
                        }

                        // see if we're done parsing:
                        if (p.AppCommand == EnumPacketType.END_OUTPUT)
                        {
                            loop = false;
                            break;
                        }
                        else if (!RageApplication.PacketHandler(p, log, logCtx))
                        {
                            return false;
                        }
                        else if (p.AppCommand == EnumPacketType.HANDSHAKE)
                        {
                            loop = false;
                            break;
                        }
                    }
                }
            }

            return true;
        }

        public AppRemotePacket(INamedPipe pipe)
            : base(pipe)
        {
        }
    }
}
