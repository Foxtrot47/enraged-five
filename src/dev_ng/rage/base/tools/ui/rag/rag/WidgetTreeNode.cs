﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ragCore;

namespace rag
{

    public class WidgetTreeNode : TreeNode
    {
        public WidgetTreeNode( Widget w, int imageIndex )
            : base( w.Title, imageIndex, imageIndex )
        {
            m_Widget = w;
            Text = w.Title;
        }

        #region Variables
        private Widget m_Widget;
        private List<WidgetTreeNode> m_originalWidgetTreeNodeOrder = new List<WidgetTreeNode>();
        #endregion

        #region Properties
        public Widget TheWidget
        {
            get
            {
                return m_Widget;
            }
        }

        public List<WidgetTreeNode> OriginalWidgetTreeNodeOrder
        {
            get
            {
                return m_originalWidgetTreeNodeOrder;
            }
        }
        #endregion

        #region Public Functions
        public void Unsort()
        {
            this.Nodes.Clear();

            foreach ( WidgetTreeNode widgetNode in m_originalWidgetTreeNodeOrder )
            {
                this.Nodes.Add( widgetNode );

                widgetNode.Unsort();
            }
        }
        #endregion
    }
}
