using System;
using System.Windows.Forms;
using System.Diagnostics;

using ragCore;

namespace rag
{
	/// <summary>
	/// Summary description for WidgetTwoPanelVisible.
	/// </summary>
	public abstract class WidgetTwoPanelVisible : WidgetVisible
	{
		public WidgetTwoPanelVisible(IWidgetView widgetView,Control parent,Widget widget,TD.SandBar.MenuItemBase[] menuItems)  : base(widgetView,parent,widget,menuItems)
		{
			m_PanelWhole=new System.Windows.Forms.Panel();
			m_PanelRight=new System.Windows.Forms.Panel();
			m_PanelLeft=new System.Windows.Forms.Panel();
			m_Splitter=new System.Windows.Forms.Splitter();

			m_PanelWhole.Controls.Add(m_PanelRight);
			m_PanelWhole.Controls.Add(m_Splitter);
			m_PanelWhole.Controls.Add(m_PanelLeft);

			// 
			// m_PanelWhole
			// 
			m_PanelWhole.Size = new System.Drawing.Size(ParentWidth, GetHeight());			
			m_PanelWhole.Dock = DockStyle.None;
			m_PanelWhole.Name = "m_PanelWhole";
			m_PanelWhole.TabIndex = 0;
			// 
			// m_PanelLeft
			// 
			m_PanelLeft.Dock = System.Windows.Forms.DockStyle.Left;
			m_PanelLeft.Location = new System.Drawing.Point(0, 0);
			m_PanelLeft.Name = "m_PanelLeft";
			m_PanelLeft.Size = new System.Drawing.Size(87, GetHeight());
			m_PanelLeft.TabIndex = 0;
			// 
			// m_Splitter
			// 
			m_Splitter.Location = sm_SplitterPoint;
			m_Splitter.Name = "m_Splitter";
			m_Splitter.Size = new System.Drawing.Size(3, GetHeight());
			m_Splitter.TabIndex = 1;
			m_Splitter.TabStop = false;
			// 
			// m_PanelRight
			// 
			m_PanelRight.Dock = System.Windows.Forms.DockStyle.Fill;
			m_PanelRight.Location = new System.Drawing.Point(83, 0);
			m_PanelRight.Name = "m_PanelRight";
			m_PanelRight.Size = new System.Drawing.Size(150, GetHeight());
			m_PanelRight.TabIndex = 2;

			parent.Controls.Add(m_PanelWhole);
			AddMouseEnter(m_PanelWhole);
			AddContextMenu(m_PanelWhole);

			// add event handlers to child controls:
			foreach (Control control in this.m_PanelWhole.Controls)
			{
				control.MouseEnter += new EventHandler(ShowHelp);
				control.MouseUp += new MouseEventHandler(OnMouseUp);
				control.MouseDown += new MouseEventHandler(OnMouseDown);
			}
		}

		protected void AddToLeftPanel(Control cntrl)
		{
			m_PanelLeft.Controls.Add(cntrl);
			AddHandlers(cntrl);
		}

		protected void AddToRightPanel(Control cntrl)
		{
			m_PanelRight.Controls.Add(cntrl);
			AddHandlers(cntrl);
		}

		private void AddHandlers(Control cntrl)
		{
			cntrl.MouseEnter += new EventHandler(ShowHelp);
			cntrl.MouseUp += new MouseEventHandler(OnMouseUp);
			cntrl.MouseDown += new MouseEventHandler(OnMouseDown);
			SetDragDropHandlers(cntrl);
		}

		public override Control DragDropControl
		{
			get {return m_PanelWhole;}
		}

		public override void Remove()
		{
			m_PanelWhole.Controls.Remove(m_PanelRight);
			m_PanelWhole.Controls.Remove(m_Splitter);
			m_PanelWhole.Controls.Remove(m_PanelLeft);
			m_Parent.Controls.Remove(m_PanelWhole);
			base.Remove();
		}

		public override void Destroy()
		{
			base.Destroy();
			m_PanelWhole=null;
			m_PanelRight=null;
			m_PanelLeft=null;
			m_Splitter=null;
		}
		
		public override Control Control 
		{
			get
			{
				return m_PanelWhole;
			}
		}
	
		private static System.Drawing.Point sm_SplitterPoint = new System.Drawing.Point(150,0);

		private System.Windows.Forms.Panel m_PanelWhole;
		private System.Windows.Forms.Panel m_PanelRight;
		private System.Windows.Forms.Panel m_PanelLeft;
		private System.Windows.Forms.Splitter m_Splitter;
	}
}
