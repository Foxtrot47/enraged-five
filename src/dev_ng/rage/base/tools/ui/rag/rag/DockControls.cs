using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Xml;
using TD.SandDock;

namespace rag
{
	/// <summary>
	/// Summary description for DockControlManager.
	/// </summary>
	public class DockControlManager
	{
        private DockControlManager()
        {

        }
        
        private static DockControlManager sm_instance = new DockControlManager();
        public static DockControlManager Instance
        {
            get
            {
                return sm_instance;
            }
        }

		#region Variables and Properties
        private List<TD.SandDock.DockControl> m_DockControlWidgets = new List<TD.SandDock.DockControl>();
        private List<TD.SandDock.DockControl> m_DockControlViews = new List<TD.SandDock.DockControl>();
		private int m_NumViews=1;
		private int m_WidgetDockControlNum=1;

		//private bool m_UseMastersFirstWidgets;
		//private bool m_UseMastersFirstViews;
		
        //public bool UseMastersFirst
		//{
		//	set 
		//	{
		//		m_UseMastersFirstWidgets=m_UseMastersFirstViews=value;
		//	}
		//}

		private DockControl m_MasterDockControlWidgets;
		public DockControl MasterDockControlWidgets
		{
            get
            {
                return m_MasterDockControlWidgets;
            }
			set 
			{
				//Debug.Assert(m_DockControlWidgets.Contains(value)==false);
				//m_DockControlWidgets.Add(value);
				m_MasterDockControlWidgets=value;
			}
		}

		private DockControl m_MasterDockControlViews;
		public DockControl MasterDockControlViews
		{
            get
            {
                return m_MasterDockControlViews;
            }
			set 
			{
				//Debug.Assert(m_DockControlViews.Contains(value)==false);
                //m_DockControlViews.Add(value);
				m_MasterDockControlViews=value;
			}
		}

        public List<TD.SandDock.DockControl> WidgetViewDockControls
        {
            get
            {
                return m_DockControlWidgets;
            }
        }

        public List<TD.SandDock.DockControl> RenderWindowDockControls
        {
            get
            {
                return m_DockControlViews;
            }
        }
		#endregion

		public void Initialize()
		{
			m_DockControlViews.Clear();
			m_DockControlWidgets.Clear();
		}

        public DockControl CreateWidgetDockControl( System.Guid guid )
        {
            // special case for the dock control that's always visible:
            //if (guid==m_MasterDockControlWidgets.Guid || m_UseMastersFirstWidgets==true)
            //{
            //    m_UseMastersFirstWidgets = false;
            //    return m_MasterDockControlWidgets;
            //}

            // don't create a new DockControl if we already have one for this guid
            foreach ( DockControl ctrl in m_DockControlWidgets )
            {
                if ( ctrl.Guid == guid )
                {
                    return ctrl;
                }
            }

            DockableWindow dockControl = new DockableWindow();
            m_DockControlWidgets.Add( dockControl );

            // setup the dock control:         
            dockControl.DockingRules = new DockingRules( true, true, true );
            dockControl.Guid = guid;
            dockControl.Location = new System.Drawing.Point( 0, 18 );
            dockControl.Name = "m_DockControl" + m_WidgetDockControlNum;
            dockControl.Size = new System.Drawing.Size( 596, 307 );
            dockControl.TabIndex = m_WidgetDockControlNum - 1;
            dockControl.Text = "dockControl" + m_WidgetDockControlNum;
            dockControl.Visible = false;
            dockControl.CloseAction = (m_WidgetDockControlNum == 1) ? DockControlCloseAction.HideOnly : DockControlCloseAction.Dispose;
            dockControl.Closed += new EventHandler( dockControlWidget_Closed );

            m_WidgetDockControlNum++;

            if ( m_MasterDockControlWidgets != null )
            {
                m_MasterDockControlWidgets.Close();
                m_MasterDockControlWidgets = null;
            }

            return dockControl;
        }

        private void dockControlWidget_Closed(object sender, EventArgs e)
        {
            TD.SandDock.DockControl dockControl = sender as TD.SandDock.DockControl;
            if ( dockControl != null )
            {
                if ( dockControl.CloseAction == DockControlCloseAction.Dispose )
                {
                    DockControlManager.Instance.m_DockControlWidgets.Remove( dockControl );
                }
            }
        }

        public DockControl CreateViewDockControl( System.Guid guid )
        {
			// special case for the dock control that's always visible:
			//if (guid==m_MasterDockControlViews.Guid || m_UseMastersFirstViews==true)
			//{
			//	m_UseMastersFirstViews=false;
			//	return m_MasterDockControlViews;
			//}

            // don't create a new DockControl if we already have one for this guid
            foreach ( DockControl ctrl in m_DockControlViews )
            {
                if ( ctrl.Guid == guid )
                {
                    return ctrl;
                }
            }

            TabbedDocument tabDoc = new TabbedDocument();
            m_DockControlViews.Add( tabDoc );
		
			// dock control:
            tabDoc.Manager = DockableViewManager.Instance.SandDock;
            tabDoc.Guid = guid;
            tabDoc.Name = "m_DockControlGameView" + m_NumViews;
            tabDoc.Size = new System.Drawing.Size( 762, 321 );
            tabDoc.TabIndex = m_NumViews - 1;
            tabDoc.Text = "Game View " + m_NumViews;
            tabDoc.AllowClose = false;
            tabDoc.PersistState = true;    // so window position and dock/float state gets saved/loaded with layout
            tabDoc.Closed += new EventHandler(dockControlView_Closed);

			m_NumViews++;

            return tabDoc;
        }

        private void dockControlView_Closed(object sender, EventArgs e)
        {
            TD.SandDock.DockControl dockControl = sender as TD.SandDock.DockControl;
            if (dockControl != null)
            {
                if (dockControl.CloseAction == DockControlCloseAction.Dispose)
                {
                    DockControlManager.Instance.m_DockControlViews.Remove(dockControl);
                }
            }
        }

        public bool DockControlIsADocumentView(DockControl dockControl)
		{
			return m_DockControlViews.Contains(dockControl);
		}

		public bool DockControlIsAWidgetView(DockControl dockControl)
		{
			return m_DockControlWidgets.Contains(dockControl);
		}

	
		public void Serialize(XmlTextWriter xmlWriter,DockControl[] viewDockControls,DockControl[] widgetDockControls)
		{
			xmlWriter.WriteStartElement("DocumentViewDockControls");

			// view dock controls;
			foreach (DockControl dockControl in viewDockControls)
			{
				if (DockControlIsADocumentView(dockControl))
				{
					xmlWriter.WriteStartElement("DocumentViewDockControls");
					xmlWriter.WriteAttributeString("guid",dockControl.Guid.ToString());
					xmlWriter.WriteEndElement();
				}
			}
	
			// widget dock controls;
			foreach (DockControl dockControl in widgetDockControls)
			{
				if (DockControlIsADocumentView(dockControl))
				{
					xmlWriter.WriteStartElement("DocumentViewDockWidgets");
					xmlWriter.WriteAttributeString("guid",dockControl.Guid.ToString());
					xmlWriter.WriteEndElement();
				}
			}
	
			xmlWriter.WriteEndElement();	
		}
	
	}
}
