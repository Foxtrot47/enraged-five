using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using ragCore;
using RSG.Base.Forms;
using ragWidgets;
using RSG.Base.Logging;

namespace rag
{
    public partial class WidgetViewPlaceholder : rag.WidgetView
    {
        public WidgetViewPlaceholder()
        {
            InitializeComponent();
        }

        public WidgetViewPlaceholder( String appName, IBankManager bankMgr, TD.SandDock.DockControl dockControl )
            : base( appName, bankMgr, dockControl )
        {
            InitializeComponent();
        }

        #region Variables
        protected int m_ViewNum = 0;
        protected bool m_displayPath = true;
        protected bool m_IsDirty;
        protected Dictionary<string, int> m_placeholderWidgetPaths = new Dictionary<string, int>();
        protected List<Widget> m_widgetsAdded = new List<Widget>();
        protected List<Node> m_nodes = new List<Node>();
        protected Dictionary<Widget, Node> m_widgetNodes = new Dictionary<Widget, Node>();
        #endregion

        #region Properties
        /// <summary>
        /// Returns the Full Path if it was stored without a widget
        /// </summary>
        public Dictionary<string, int> PlaceholderWidgetPaths
        {
            get
            {
                return m_placeholderWidgetPaths;
            }
        }
        #endregion

        #region Overrides
        public override bool DisplayPath
        {
            get
            {
                return m_displayPath;
            }
        }

        public override void SetIndexForWidget( WidgetVisible visible, int newIndex )
        {
            // find the node
            Node node = null;
            if ( visible.Widget == null )
            {
                foreach ( Node n in m_nodes )
                {
                    if ( n.Visible == visible )
                    {
                        node = n;
                    }
                    else if ( n.IsTemp )
                    {
                        foreach ( Node childN in n.Nodes )
                        {
                            if ( childN.Visible == visible )
                            {
                                node = childN;
                                break;
                            }
                        }
                    }

                    if ( node != null )
                    {
                        break;
                    }
                }
            }
            else
            {
                m_widgetNodes.TryGetValue( visible.Widget, out node );
            }

            // move it
            if ( node != null )
            {
                List<Node> nodes = node.IsGroup ? m_nodes : node.Parent.Nodes;
                nodes.Remove( node );
                nodes.Insert( newIndex, node );

                // recalculate indexes
                for ( int i = newIndex; i < nodes.Count; ++i )
                {
                    nodes[i].LastIndex = nodes[i].Visible.GetParentIndex();
                }
            }

            // make sure our list is in the correct order
            RebuildWidgetsAdded();
        }

        public override void ClearView()
        {
            // wrapped with a mutex to prevent destroying a widget while it's being viewed
            BankRemotePacketProcessor.MakingVisibleMutex.WaitOne();

            // remove widgets in temp groups:
            do
            {
                if ( m_widgetsAdded.Count == 0 )
                {
                    break;
                }

                RemoveWidget( m_widgetsAdded[0] );
            } while ( true );

            BankRemotePacketProcessor.MakingVisibleMutex.ReleaseMutex();
        }
        #endregion

        #region Public Functions
        public bool ContainsWidget( Widget widget )
        {
            // make sure it's not already added:
            return m_widgetsAdded.Contains( widget );
        }

        public void AddWidget( Widget widget )
        {
            // wrapped with a mutex to prevent destroying a widget while it's being viewed:
            BankRemotePacketProcessor.MakingVisibleMutex.WaitOne();

            bool replacementMade = false;

            if ( widget is WidgetPlaceholder )
            {
                Node tempGroupNode;
                GetOrAddTempGroup( m_nodes.Count - 1, -1, out tempGroupNode );

                AddWidgetToNode( widget, tempGroupNode, -1 );
            }
            else
            {
                foreach ( Widget w in m_widgetsAdded )
                {
                    if ( w is WidgetPlaceholder )
                    {
                        WidgetPlaceholder placeholder = w as WidgetPlaceholder;
                        Node placeholderNode;
                        if ( (placeholder.ActualPath == widget.Path) && m_widgetNodes.TryGetValue( w, out placeholderNode ) )
                        {
                            Node tempGroupNode = placeholderNode.Parent;
                            int indexOfNode = tempGroupNode.Nodes.IndexOf( placeholderNode );

                            if ( widget.IsGroup() )
                            {
                                Node node = AddGroupWidget( widget, tempGroupNode.LastIndex + ((indexOfNode == 0) ? 0 : 1) );

                                // move the rest of the nodes down into the next temp group (or create a new one)
                                if ( (indexOfNode > 0) && (indexOfNode + 1 < tempGroupNode.Nodes.Count) )
                                {
                                    // remove the trailing widgets from our parent
                                    List<Widget> widgetsToMove = new List<Widget>();
                                    int i = indexOfNode + 1;
                                    while ( i < tempGroupNode.Nodes.Count )
                                    {
                                        widgetsToMove.Add( tempGroupNode.Nodes[i].Visible.Widget );

                                        RemoveWidget( tempGroupNode.Nodes[i].Visible.Widget );
                                    }

                                    int indexOfGroupNode = m_nodes.IndexOf( node );
                                    Node nextTempGroupNode = null;

                                    // get or add a group to move them to
                                    int moveToIndex = -1;
                                    if ( !GetOrAddTempGroup( indexOfGroupNode + 1, indexOfGroupNode + 1, out nextTempGroupNode ) )
                                    {
                                        moveToIndex = 0;
                                    }

                                    // move them, inserting at the beginning if necessary
                                    foreach ( Widget widgetToMove in widgetsToMove )
                                    {
                                        AddWidgetToNode( widgetToMove, nextTempGroupNode, moveToIndex );

                                        if ( moveToIndex > -1 )
                                        {
                                            moveToIndex += this.DisplayPath ? 2 : 1;
                                        }
                                    }

                                    if ( moveToIndex == -1 )
                                    {
                                        // make sure our list is in the correct order
                                        RebuildWidgetsAdded();
                                    }
                                }
                            }
                            else
                            {
                                AddWidgetToNode( widget, tempGroupNode, placeholderNode.LastIndex );
                            }

                            // now finally remove the placeholder
                            RemoveWidget( placeholder );

                            replacementMade = true;
                            break;
                        }
                    }
                }

                if ( !replacementMade )
                {
                    if ( widget.IsGroup() )
                    {
                        AddGroupWidget( widget, -1 );
                    }
                    else
                    {
                        Node tempGroupNode;
                        GetOrAddTempGroup( m_nodes.Count - 1, -1, out tempGroupNode );

                        AddWidgetToNode( widget, tempGroupNode, -1 );
                    }
                }
            }

            m_IsDirty = !replacementMade;

            BankRemotePacketProcessor.MakingVisibleMutex.ReleaseMutex();
        }

        public void RemoveWidget( WidgetVisible widgetVisible )
        {
            RemoveWidget( widgetVisible.Widget );
        }

        public void RemoveWidget( Widget widget )
        {
            NotifyParentThatWidgetVisibleIsNotVisible( widget );

            BankRemotePacketProcessor.MakingVisibleMutex.WaitOne();

            // remove it if it's a group:
            if ( widget.IsGroup() )
            {
                WidgetVisualiser.HideVisible( widget, GetType().ToString() + m_ViewNum );
            }
            else
            {
                Node node;
                if ( m_widgetNodes.TryGetValue( widget, out node ) )
                {
                    if ( node.Parent != null )
                    {
                        WidgetVisualiser.HideVisible( widget, node.Parent.Visible.Control.Name );
                    }
                }
            }

            WidgetVisibleRemoved( widget, false );

            BankRemotePacketProcessor.MakingVisibleMutex.ReleaseMutex();

            m_IsDirty = true;
        }
        #endregion

        #region Protected Functions
        protected void SetDisplayPath( bool displayPath )
        {
            if ( displayPath != m_displayPath )
            {
                // crapy way to do it, but the current architecture sucks
                BankRemotePacketProcessor.MakingVisibleMutex.WaitOne();

                // remove the widgets
                List<Widget> widgestToReAdd = new List<Widget>( m_widgetsAdded );
                while ( m_widgetsAdded.Count > 0 )
                {
                    RemoveWidget( m_widgetsAdded[0] );
                }

                // flip the flag (the WidgetVisibles check this and add the path, if needed)
                m_displayPath = displayPath;

                // add the widgets back on
                foreach ( Widget widget in widgestToReAdd )
                {
                    AddWidget( widget );
                }

                m_IsDirty = true;

                BankRemotePacketProcessor.MakingVisibleMutex.ReleaseMutex();
            }
        }
        #endregion

        #region Event Handlers
        private void widget_DestroyEvent( object sender, EventArgs e )
        {
            if ( sender is Widget )
            {
                bool currentIsDirty = m_IsDirty;

                WidgetVisibleRemoved( sender as Widget, true );

                m_IsDirty = currentIsDirty;
            }
        }
        #endregion

        #region Private Functions
        /// <summary>
        /// Tries to locate a Temp Group Node at the specified index.  If one is not found, a new one is created
        /// at the end of the list.
        /// </summary>
        /// <param name="index">The index to check.</param>
        /// <param name="moveToIndex">If a <see cref="Node"/> must be created, which index it should be created at.</param>
        /// <param name="tempGroupNode">The <see cref="Node"/>.</param>
        /// <returns><c>true</c> if a new <see cref="Node"/> was created</returns>
        private bool GetOrAddTempGroup( int index, int moveToIndex, out Node tempGroupNode )
        {
            tempGroupNode = null;
            if ( (index > -1) && (index < m_nodes.Count) && m_nodes[index].IsTemp )
            {
                tempGroupNode = m_nodes[index];

                return false;
            }
            else
            {
                tempGroupNode = new Node();
                tempGroupNode.Visible = new WidgetGroupVisible( this, null, this.xPanderList, toolTip1, false);
                tempGroupNode.LastIndex = tempGroupNode.Visible.GetParentIndex();

                m_nodes.Add( tempGroupNode );

                if ( (moveToIndex > -1) && (moveToIndex < m_nodes.Count) )
                {
                    tempGroupNode.Visible.MoveVisible( moveToIndex );
                }

                return true;
            }
        }

        private Node AddWidgetToNode( Widget widget, Node tempGroupNode, int index )
        {
            try
            {
                WidgetVisualiser.ShowVisible(widget, this, tempGroupNode.Visible.Control, tempGroupNode.Visible.Control.Name, false, toolTip1);
            }
            catch ( Exception e )
            {
                // Output the exception to the log.
                ExceptionFormatter formatter = new ExceptionFormatter(e);
                String exceptionText = "";

                foreach (String line in formatter.Format())
                {
                    exceptionText += line;
                }

                DialogResult result = rageMessageBox.ShowExclamation( this.DockControl,
                    String.Format( "There was an error creating a Widget Visible for the Widget {0}.\n\nWould you like to disable the widget?\n\n{1}",
                    widget.Title, exceptionText ), "Widget Error", MessageBoxButtons.YesNo);
                if ( result == DialogResult.Yes )
                {
                    widget.Enabled = false;

                    if ( !WidgetVisualiser.PacketProcessor.DisabledWidgets.Contains( widget ) )
                    {
                        WidgetVisualiser.PacketProcessor.DisabledWidgets.Add( widget );
                    }
                }
            }

            Node node = new Node();
            node.Visible = WidgetVisualiser.FindVisible( widget, tempGroupNode.Visible.Control.Name );
            node.LastIndex = node.Visible.GetParentIndex();
            node.Parent = tempGroupNode;

            if ( widget is WidgetPlaceholder )
            {
                node.Path = (widget as WidgetPlaceholder).ActualPath;

                // reference count the placeholder's path
                int count = 0;
                m_placeholderWidgetPaths.TryGetValue( node.Path, out count );

                ++count;
                m_placeholderWidgetPaths[node.Path] = count;
            }
            else
            {
                node.Path = widget.Path;

                widget.DestroyEvent += new EventHandler( widget_DestroyEvent );
            }

            tempGroupNode.Nodes.Add( node );

            m_widgetNodes[widget] = node;

            m_widgetsAdded.Add( widget );

            if ( index > -1 )
            {
                node.Visible.MoveVisible( index );
            }

            NotifyParentThatWidgetVisibleIsVisible( widget );

            return node;
        }

        private Node AddGroupWidget( Widget widget, int index )
        {
            WidgetVisualiser.ShowVisible(widget, this, this.xPanderList, GetType().ToString() + m_ViewNum, false, toolTip1);

            Node node = new Node();
            node.Visible = WidgetVisualiser.FindVisible(widget, GetType().ToString() + m_ViewNum );
            node.LastIndex = node.Visible.GetParentIndex();
            node.Path = widget.Path;

            m_nodes.Add( node );

            m_widgetNodes[widget] = node;

            m_widgetsAdded.Add( widget );

            widget.DestroyEvent += new EventHandler( widget_DestroyEvent );

            if ( index > -1 )
            {
                node.Visible.MoveVisible( index );
            }

            NotifyParentThatWidgetVisibleIsVisible( widget );

            return node;
        }

        private void WidgetVisibleRemoved( Widget widget, bool swapForPlaceholder )
        {
            Node node;
            if ( !m_widgetNodes.TryGetValue( widget, out node ) )
            {
                return;
            }

            m_widgetsAdded.Remove( widget );

            m_widgetNodes.Remove( widget );

            if ( widget is WidgetPlaceholder )
            {
                // decrement the reference to the placeholder widget
                string path = (widget as WidgetPlaceholder).ActualPath;

                int count;
                if ( m_placeholderWidgetPaths.TryGetValue( path, out count ) )
                {
                    --count;
                    if ( count == 0 )
                    {
                        m_placeholderWidgetPaths.Remove( path );
                    }
                    else
                    {
                        m_placeholderWidgetPaths[path] = count;
                    }
                }
            }
            else
            {
                widget.DestroyEvent -= new EventHandler( widget_DestroyEvent );
            }

            if ( widget.IsGroup() )
            {
                int indexOf = m_nodes.IndexOf( node );
                m_nodes.RemoveAt( indexOf );

                // recalculate indexes
                for ( int i = indexOf; i < m_nodes.Count; ++i )
                {
                    m_nodes[i].LastIndex = m_nodes[i].Visible.GetParentIndex();
                }

                if ( swapForPlaceholder )
                {
                    if ( (indexOf > 0) && (indexOf - 1 < m_nodes.Count) && m_nodes[indexOf - 1].IsTemp )
                    {
                        // add the placeholder
                        AddWidgetToNode( new WidgetPlaceholder( node.Path, Color.Empty ), m_nodes[indexOf - 1], -1 );

                        // make sure our list is in the correct order
                        RebuildWidgetsAdded();
                    }
                    else if ( (indexOf < m_nodes.Count) && m_nodes[indexOf].IsTemp )
                    {
                        // add the placeholder
                        AddWidgetToNode( new WidgetPlaceholder( node.Path, Color.Empty ), m_nodes[indexOf], 0 );
                    }
                    else
                    {
                        // create a new temp group
                        Node tempGroupNode;
                        GetOrAddTempGroup( indexOf, indexOf, out tempGroupNode );

                        // add the placeholder
                        AddWidgetToNode( new WidgetPlaceholder( node.Path, Color.Empty ), tempGroupNode, -1 );

                        // make sure our list is in the correct order
                        RebuildWidgetsAdded();
                    }
                }

                // try to consolidate adjacent temp groups
                if ( (indexOf > 0) && (indexOf < m_nodes.Count) && m_nodes[indexOf - 1].IsTemp && m_nodes[indexOf].IsTemp )
                {
                    Node tempGroupNode = m_nodes[indexOf];
                    List<Widget> widgetsToMove = new List<Widget>();
                    while ( tempGroupNode.Nodes.Count > 0 )
                    {
                        widgetsToMove.Add( tempGroupNode.Nodes[0].Visible.Widget );

                        RemoveWidget( tempGroupNode.Nodes[0].Visible.Widget );
                    }

                    tempGroupNode = m_nodes[indexOf - 1];
                    foreach ( Widget widgetToMove in widgetsToMove )
                    {
                        AddWidgetToNode( widgetToMove, tempGroupNode, -1 );
                    }

                    // make sure our list is in the correct order
                    RebuildWidgetsAdded();
                }
            }
            else
            {
                int indexOf = node.Parent.Nodes.IndexOf( node );
                node.Parent.Nodes.RemoveAt( indexOf );

                // recalculate indexes
                for ( int i = indexOf; i < node.Parent.Nodes.Count; ++i )
                {
                    node.Parent.Nodes[i].LastIndex = node.Parent.Nodes[i].Visible.GetParentIndex();
                }

                if ( swapForPlaceholder )
                {
                    // add the placeholder
                    AddWidgetToNode( new WidgetPlaceholder( node.Path, Color.Empty ), node.Parent, node.LastIndex );
                }
                else
                {
                    // if this group is empty, remove it
                    if ( node.Parent.Nodes.Count == 0 )
                    {
                        m_nodes.Remove( node.Parent );

                        node.Parent.Visible.Remove();
                        node.Parent.Visible.Destroy();
                    }
                }
            }
        }

        private void RebuildWidgetsAdded()
        {
            m_widgetsAdded.Clear();

            foreach ( Node n in m_nodes )
            {
                if ( n.IsTemp )
                {
                    foreach ( Node childN in n.Nodes )
                    {
                        m_widgetsAdded.Add( childN.Visible.Widget );
                    }
                }
                else
                {
                    m_widgetsAdded.Add( n.Visible.Widget );
                }
            }
        }
        #endregion

        #region Helper Classes
        protected class Node
        {
            public Node()
            {

            }

            public Node Parent = null;
            public WidgetVisible Visible = null;
            public string Path = string.Empty;
            public int LastIndex = -1;
            public List<Node> Nodes = new List<Node>();

            public bool IsGroup
            {
                get
                {
                    return this.IsTemp || ((this.Visible != null) && (this.Visible.Widget != null) && this.Visible.Widget.IsGroup());
                }
            }

            public bool IsTemp
            {
                get
                {
                    if ( this.Visible != null )
                    {
                        return this.Visible.Widget == null;
                    }

                    return false;
                }
            }
        };
        #endregion
    }
}

