namespace rag
{
    partial class CommandLineForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && (components != null) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.okButton = new System.Windows.Forms.Button();
            this.commandLineRichTextBox = new System.Windows.Forms.RichTextBox();
            this.multiLineCheckBox = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // okButton
            // 
            this.okButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.okButton.Location = new System.Drawing.Point( 342, 125 );
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size( 75, 23 );
            this.okButton.TabIndex = 0;
            this.okButton.Text = "OK";
            this.okButton.UseVisualStyleBackColor = true;
            // 
            // commandLineRichTextBox
            // 
            this.commandLineRichTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.commandLineRichTextBox.Location = new System.Drawing.Point( 12, 12 );
            this.commandLineRichTextBox.Name = "commandLineRichTextBox";
            this.commandLineRichTextBox.ReadOnly = true;
            this.commandLineRichTextBox.Size = new System.Drawing.Size( 405, 107 );
            this.commandLineRichTextBox.TabIndex = 1;
            this.commandLineRichTextBox.Text = "";
            // 
            // multiLineCheckBox
            // 
            this.multiLineCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.multiLineCheckBox.AutoSize = true;
            this.multiLineCheckBox.Checked = true;
            this.multiLineCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.multiLineCheckBox.Location = new System.Drawing.Point( 12, 129 );
            this.multiLineCheckBox.Name = "multiLineCheckBox";
            this.multiLineCheckBox.Size = new System.Drawing.Size( 141, 17 );
            this.multiLineCheckBox.TabIndex = 2;
            this.multiLineCheckBox.Text = "Arguments On Own Line";
            this.multiLineCheckBox.UseVisualStyleBackColor = true;
            this.multiLineCheckBox.CheckedChanged += new System.EventHandler( this.multiLineCheckBox_CheckedChanged );
            // 
            // CommandLineForm
            // 
            this.AcceptButton = this.okButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.okButton;
            this.ClientSize = new System.Drawing.Size( 429, 160 );
            this.Controls.Add( this.multiLineCheckBox );
            this.Controls.Add( this.commandLineRichTextBox );
            this.Controls.Add( this.okButton );
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.MinimumSize = new System.Drawing.Size( 252, 170 );
            this.Name = "CommandLineForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Command Line For .exe";
            this.Load += new System.EventHandler( this.CommandLineForm_Load );
            this.ResumeLayout( false );
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.RichTextBox commandLineRichTextBox;
        private System.Windows.Forms.CheckBox multiLineCheckBox;
    }
}