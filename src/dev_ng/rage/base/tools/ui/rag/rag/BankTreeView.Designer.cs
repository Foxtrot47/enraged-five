namespace rag
{
    partial class BankTreeView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.treeView = new System.Windows.Forms.TreeView();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.mainPaneSortBanksToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mainPaneUnsortBanksToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.sortAllWidgetsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.unsortAllWidgetsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sortBanksToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.unsortBanksToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.treeViewMenuStrip = new System.Windows.Forms.MenuStrip();
            this.displayToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.treeViewMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add( this.treeView );
            this.splitContainer1.Panel1.Controls.Add( this.treeViewMenuStrip );
            // 
            // splitContainer2
            // 
            this.splitContainer2.Size = new System.Drawing.Size( 361, 541 );
            // 
            // treeView
            // 
            this.treeView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeView.HideSelection = false;
            this.treeView.Location = new System.Drawing.Point( 0, 24 );
            this.treeView.Name = "treeView";
            this.treeView.Size = new System.Drawing.Size( 179, 515 );
            this.treeView.TabIndex = 0;
            this.treeView.MouseUp += new System.Windows.Forms.MouseEventHandler( this.treeView_MouseUp );
            this.treeView.AfterSelect += new System.Windows.Forms.TreeViewEventHandler( this.treeView_AfterSelect );
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size( 154, 6 );
            // 
            // mainPaneSortBanksToolStripMenuItem
            // 
            this.mainPaneSortBanksToolStripMenuItem.Name = "mainPaneSortBanksToolStripMenuItem";
            this.mainPaneSortBanksToolStripMenuItem.Size = new System.Drawing.Size( 157, 22 );
            this.mainPaneSortBanksToolStripMenuItem.Text = "Sort Banks";
            this.mainPaneSortBanksToolStripMenuItem.Click += new System.EventHandler( this.sortBanksToolStripMenuItem_Click );
            // 
            // mainPaneUnsortBanksToolStripMenuItem
            // 
            this.mainPaneUnsortBanksToolStripMenuItem.Name = "mainPaneUnsortBanksToolStripMenuItem";
            this.mainPaneUnsortBanksToolStripMenuItem.Size = new System.Drawing.Size( 157, 22 );
            this.mainPaneUnsortBanksToolStripMenuItem.Text = "Unsort Banks";
            this.mainPaneUnsortBanksToolStripMenuItem.Click += new System.EventHandler( this.unsortBanksToolStripMenuItem_Click );
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size( 170, 6 );
            // 
            // sortAllWidgetsToolStripMenuItem
            // 
            this.sortAllWidgetsToolStripMenuItem.Name = "sortAllWidgetsToolStripMenuItem";
            this.sortAllWidgetsToolStripMenuItem.Size = new System.Drawing.Size( 173, 22 );
            this.sortAllWidgetsToolStripMenuItem.Text = "Sort All Widgets";
            this.sortAllWidgetsToolStripMenuItem.Click += new System.EventHandler( this.sortAllWidgetsToolStripMenuItem_Click );
            // 
            // unsortAllWidgetsToolStripMenuItem
            // 
            this.unsortAllWidgetsToolStripMenuItem.Name = "unsortAllWidgetsToolStripMenuItem";
            this.unsortAllWidgetsToolStripMenuItem.Size = new System.Drawing.Size( 173, 22 );
            this.unsortAllWidgetsToolStripMenuItem.Text = "Unsort All Widgets";
            this.unsortAllWidgetsToolStripMenuItem.Click += new System.EventHandler( this.unsortAllWidgetsToolStripMenuItem_Click );
            // 
            // sortBanksToolStripMenuItem
            // 
            this.sortBanksToolStripMenuItem.Name = "sortBanksToolStripMenuItem";
            this.sortBanksToolStripMenuItem.Size = new System.Drawing.Size( 157, 22 );
            this.sortBanksToolStripMenuItem.Text = "Sort Banks";
            this.sortBanksToolStripMenuItem.Click += new System.EventHandler( this.sortBanksToolStripMenuItem_Click );
            // 
            // unsortBanksToolStripMenuItem
            // 
            this.unsortBanksToolStripMenuItem.Name = "unsortBanksToolStripMenuItem";
            this.unsortBanksToolStripMenuItem.Size = new System.Drawing.Size( 157, 22 );
            this.unsortBanksToolStripMenuItem.Text = "Unsort Banks";
            this.unsortBanksToolStripMenuItem.Click += new System.EventHandler( this.unsortBanksToolStripMenuItem_Click );
            // 
            // treeViewMenuStrip
            // 
            this.treeViewMenuStrip.Items.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.displayToolStripMenuItem} );
            this.treeViewMenuStrip.Location = new System.Drawing.Point( 0, 0 );
            this.treeViewMenuStrip.Name = "treeViewMenuStrip";
            this.treeViewMenuStrip.Size = new System.Drawing.Size( 179, 24 );
            this.treeViewMenuStrip.TabIndex = 1;
            // 
            // displayToolStripMenuItem
            // 
            this.displayToolStripMenuItem.Name = "displayToolStripMenuItem";
            this.displayToolStripMenuItem.Size = new System.Drawing.Size( 53, 20 );
            this.displayToolStripMenuItem.Text = "Display";
            this.displayToolStripMenuItem.DropDownOpening += new System.EventHandler( this.displayToolStripMenuItem_DropDownOpening );
            // 
            // BankTreeView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
            this.Name = "BankTreeView";
            this.Controls.SetChildIndex( this.splitContainer1, 0 );
            this.splitContainer1.Panel1.ResumeLayout( false );
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout( false );
            this.splitContainer1.ResumeLayout( false );
            this.splitContainer2.Panel1.ResumeLayout( false );
            this.splitContainer2.ResumeLayout( false );
            this.treeViewMenuStrip.ResumeLayout( false );
            this.treeViewMenuStrip.PerformLayout();
            this.ResumeLayout( false );

        }

        #endregion

        private System.Windows.Forms.TreeView treeView;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem mainPaneSortBanksToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mainPaneUnsortBanksToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem sortAllWidgetsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem unsortAllWidgetsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sortBanksToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem unsortBanksToolStripMenuItem;
        private System.Windows.Forms.MenuStrip treeViewMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem displayToolStripMenuItem;
    }
}
