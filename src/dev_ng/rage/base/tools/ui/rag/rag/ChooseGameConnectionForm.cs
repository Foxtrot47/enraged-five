﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ragCore;

namespace rag
{
    public partial class ChooseGameConnectionForm : Form
    {
        private IList<GameConnection> games;

        public GameConnection ChosenConnection { get; set; }

        public ChooseGameConnectionForm(IList<ragCore.GameConnection> games)
        {
            InitializeComponent();
            this.games = games;



            int validChoices = 0;
            foreach (var game in games)
            {
                if ( game.Connected )
                {
                    ChosenConnection = game;
                    ++validChoices;
                }
            }

            if ( validChoices < 2 )
            {
                // Pick the nonconnected one and wait for it to connect
                ChosenConnection = games[0];
                this.Shown += new EventHandler( ChooseGameConnectionForm_Shown );
                this.Visible = false;
            }
            else
            {
                int y = headerLabel.Bottom + 20;

                foreach (var game in games)
                {
                    if ( !game.Connected )
                        continue;

                    var b = new Button();
                    b.Tag = game;
                    b.Text = game.Platform + " @ " + game.IPAddress;
                    b.Top = y;
                    b.Width = 200;
                    b.Left = Width / 2 - b.Width / 2;
                    b.Height = 35;
                    b.BackColor = Control.DefaultBackColor;
                    b.Click += new EventHandler( b_Click );

                    this.Controls.Add( b );
                    y += b.Height + 10;
                }

                y += 20;

                var cancelButton = new Button();
                cancelButton.Text = "Cancel starting rag";
                cancelButton.Top = y;
                cancelButton.Width = 200;
                cancelButton.Left = Width / 2 - cancelButton.Width / 2;
                cancelButton.Height = 20;
                cancelButton.BackColor = Control.DefaultBackColor;
                cancelButton.Click += new EventHandler( cancelButton_Click );
                y += cancelButton.Height + 10;


                Height = y + 10;
            }
        }

        void cancelButton_Click( object sender, EventArgs e )
        {
            ChosenConnection = null;
            Close();
        }

        void ChooseGameConnectionForm_Shown( object sender, EventArgs e )
        {
            Close();
        }

        void b_Click( object sender, EventArgs e )
        {
            Button b = sender as Button;
            GameConnection gc = b.Tag as GameConnection;
            ChosenConnection = gc;
            Close();
        }

    }
}
