using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using ragCore;

namespace rag
{
    public partial class DisabledWidgetsForm : Form
    {
        public DisabledWidgetsForm()
        {
            InitializeComponent();
        }

        public DisabledWidgetsForm( List<Widget> disabledWidgets )
            : this()
        {
            m_disabledWidgets = disabledWidgets;

            foreach ( Widget disabledWidget in disabledWidgets )
            {
                this.disabledWidgetsCheckedListBox.Items.Add( disabledWidget, disabledWidget.Enabled );
            }
        }

        #region Variables
        private List<Widget> m_disabledWidgets;
        #endregion

        #region Event Handlers
        private void okButton_Click( object sender, EventArgs e )
        {
            foreach ( Widget checkedItem in this.disabledWidgetsCheckedListBox.CheckedItems )
            {
                if ( !checkedItem.IsDestroyed )
                {
                    checkedItem.Enabled = true;
                }
            }

            foreach ( Widget disabledWidget in m_disabledWidgets )
            {
                if ( !this.disabledWidgetsCheckedListBox.CheckedItems.Contains( disabledWidget )
                    && !disabledWidget.IsDestroyed )
                {
                    disabledWidget.Enabled = false;
                }
            }
        }

        private void disabledWidgetsCheckedListBox_SelectedIndexChanged( object sender, EventArgs e )
        {
            Widget widget = this.disabledWidgetsCheckedListBox.Items[this.disabledWidgetsCheckedListBox.SelectedIndex] as Widget;

            if ( !widget.IsDestroyed )
            {
                this.fullPathTextBox.Text = widget.FindPath();
            }
            else
            {
                this.disabledWidgetsCheckedListBox.Items.Remove( widget );
            }
        }
        #endregion
    }
}