using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

using TD.SandDock;
using ragCore;
using ragWidgets;
using RSG.Base.Logging;

namespace rag
{
	/// <summary>
	/// Summary description for DockableViewManager.
	/// </summary>
	public class DockableViewManager : IDockableViewManager
    {
        private DockableViewManager()
        {

        }

        #region Delegates
        private delegate void UserPrefDirChangedEventHandler( object sender, UserPrefDirChangedEventArgs e );
        #endregion

        #region Variables
        private static DockableViewManager sm_Instance = new DockableViewManager();

        protected List<DockControl> m_DockControls = new List<DockControl>();
        protected List<IDockableView> m_DockableViews = new List<IDockableView>();

        protected SandDockManager m_sandDockManager = null;
        #endregion

        #region Events
        private event UserPrefDirChangedEventHandler UserPrefDirChanged;
        #endregion

        #region Properties
        public static DockableViewManager Instance
		{
			get 
            {
				return sm_Instance;
			}
		}

		public List<IDockableView> DockableViews
		{
			get
			{
				return m_DockableViews;
			}
		}

		public List<TD.SandDock.DockControl> DockControls
		{
			get
			{
				return m_DockControls;
			}
		}

        public SandDockManager SandDock
        {
            get
            {
                return m_sandDockManager;
            }

            set
            {
                m_sandDockManager = value;
            }
        }
        #endregion

        #region IDockableViewManager Interface
        public IDockableView CreateView(TD.SandDock.DockControl dockControl, IDockableView dockableView, ILog log)
        {
            System.Diagnostics.Debug.Assert( dockableView is System.Windows.Forms.Control, "dockableView is not a System.Windows.Forms.Control" );

            // create the widget view:
            dockableView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.UserPrefDirChanged += new UserPrefDirChangedEventHandler( dockableView.UserPrefDirChanged );

            dockControl.TabImage = dockableView.GetBitmap();
            dockControl.Controls.Add( dockableView as System.Windows.Forms.Control );           
            dockControl.ShowOptions = !dockableView.IsSingleInstance;
            dockControl.Text = dockableView.GetTabText();
            dockControl.TabText = dockableView.GetTabText();
            dockControl.Manager = m_sandDockManager;
            dockControl.Tag = dockableView;
            dockControl.DockingRules = new DockingRules(true, true, true);
            dockControl.Closed += new EventHandler( dockControl_Closed );

            return dockableView;
        }

        public bool OpenView( TD.SandDock.DockControl dockControl,
            bool initiallyFloating, TD.SandDock.ContainerDockLocation dockLocation, ILog log )
        {
            DockContainer dockContainer = m_sandDockManager.FindDockContainer( dockLocation );
            if ( dockContainer == null )
            {
                dockContainer = m_sandDockManager.CreateNewDockContainer( dockLocation, ContainerDockEdge.Inside, dockControl.Width );
                if ( dockContainer == null )
                {
                    Debug.Assert( dockContainer != null, "Could not create DockContainer for " + dockControl.TabText );
                    return false;
                }
            }

            TD.SandDock.ControlLayoutSystem controlSystem = null;
            if ( dockContainer.LayoutSystem.LayoutSystems.Count <= 0 )
            {
                controlSystem = dockContainer.CreateNewLayoutSystem( dockControl, new SizeF( dockControl.Size ) );
                if ( controlSystem == null )
                {
                    Debug.Assert( controlSystem != null, "Could not create new ControlLayoutSystem for " + dockControl.TabText );
                    return false;
                }

                if ( !dockContainer.LayoutSystem.LayoutSystems.Contains( controlSystem ) )
                {
                    dockContainer.LayoutSystem.LayoutSystems.Add( controlSystem );
                }

                log.Message( "Created a new ControlLayoutSystem to open {0}", dockControl.TabText );
            }
            else
            {
                // find the ControlLayoutSystem
                foreach ( LayoutSystemBase layoutSystem in dockContainer.LayoutSystem.LayoutSystems )
                {
                    if ( layoutSystem is ControlLayoutSystem )
                    {
                        controlSystem = layoutSystem as ControlLayoutSystem;
                        break;
                    }
                }

                if ( controlSystem != null )
                {
                    // add it to the layout:
                    if ( !controlSystem.Controls.Contains( dockControl ) )
                    {
                        controlSystem.Controls.Add( dockControl );
                    }
                }
                else
                {
                    log.Message( "Creating a new ControlLayoutSystem to open {0}", dockControl.TabText );
                    
                    controlSystem = dockContainer.CreateNewLayoutSystem( dockControl, new SizeF( dockControl.Size ) );
                    if ( controlSystem == null )
                    {
                        Debug.Assert( controlSystem != null, "Could not create new ControlLayoutSystem for " + dockControl.TabText );
                        return false;
                    }

                    if ( !dockContainer.LayoutSystem.LayoutSystems.Contains( controlSystem ) )
                    {
                        dockContainer.LayoutSystem.LayoutSystems.Add( controlSystem );
                    }
                }
            }

            if ( initiallyFloating )
            {
                dockControl.OpenFloating( WindowOpenMethod.OnScreenActivate );
            }
            else
            {
                dockControl.OpenDocked( dockLocation, WindowOpenMethod.OnScreenActivate );
            }

            if ( DockControlManager.Instance.MasterDockControlWidgets != null )
            {
                DockControlManager.Instance.MasterDockControlWidgets.Close();
                DockControlManager.Instance.MasterDockControlWidgets = null;
            }

            return true;
        }

        public void PostOpenView( TD.SandDock.DockControl dockControl, IDockableView dockableView )
        {
            if ( !DockControls.Contains( dockControl ) )
            {
                DockControls.Add( dockControl );
            }

            if ( !DockableViews.Contains( dockableView ) )
            {
                DockableViews.Add( dockableView );
            }
        }

        public TD.SandDock.DockControl ResolveDockControl(String guid, ILog log)
        {
            throw new NotImplementedException( "DockableViewManager.ResolveDockControl" );
        }
        #endregion

        #region Public Functions
        public void CloseAllViews()
        {
            while ( m_DockableViews.Count > 0 )
            {
                if (m_DockableViews[0].DockControl != null)
                {
                    m_DockableViews[0].DockControl.CloseAction = DockControlCloseAction.Dispose;
                    m_DockableViews[0].DockControl.Close();
                }
                else
                {
                    m_DockableViews.Remove(m_DockableViews[0]);
                }
            }
        }

		public void ClearStaticsWidgetView()
		{
			m_DockControls.Clear();
			m_DockableViews.Clear();
        }
        #endregion

        #region Event Dispatchers
        public void OnUserPrefDirChanged( string previousDir, string dir )
        {
            if ( this.UserPrefDirChanged != null )
            {
                this.UserPrefDirChanged( this, new UserPrefDirChangedEventArgs( previousDir, dir ) );
            }
        }
        #endregion

        #region Event Handlers
        private void dockControl_Closed( object sender, EventArgs e )
        {
            TD.SandDock.DockControl dockControl = sender as TD.SandDock.DockControl;
            if ( dockControl != null )
            {
                IDockableView view = dockControl.Tag as IDockableView;

                if ( (dockControl.CloseAction == DockControlCloseAction.Dispose) && (view != null) )
                {
                    this.UserPrefDirChanged -= new UserPrefDirChangedEventHandler( view.UserPrefDirChanged );
                    this.DockControls.Remove( dockControl );
                    this.DockableViews.Remove( view );
                }
            }
        }
        #endregion
    }
}
