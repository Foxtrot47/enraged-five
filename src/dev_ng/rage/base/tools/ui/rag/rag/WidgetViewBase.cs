using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Xml;

using ragCore;
using ragWidgets;
using TD.SandDock;

namespace rag
{
    public partial class WidgetViewBase : UserControl, IWidgetView, IDockableView
    {
        protected WidgetViewBase()
        {
            InitializeComponent();
        }

        public WidgetViewBase( String appName, IBankManager bankMgr, TD.SandDock.DockControl dockControl )
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();
	
			m_BankManager=bankMgr;
			m_AppName=appName;
			m_DockControl=dockControl;

            m_DockControl.Closing += new TD.SandDock.DockControlClosingEventHandler( dockControl_Closing );
		}

        #region Delegates
        public delegate WidgetViewBase CreateDel( String appName, IBankManager bankMgr, TD.SandDock.DockControl dockControl );
        #endregion

        #region Variables
        public IBankManager m_BankManager;
        protected TD.SandDock.DockControl m_DockControl;
        protected System.Guid m_guid;
        private string m_AppName;
        #endregion

        #region Virtual Properties
        public virtual bool CanSerialize
        {
            get { return true; }
        }
        #endregion

        #region IDockableView Interface
        public TD.SandDock.DockControl DockControl
        {
            get
            {
                return m_DockControl;
            }
        }

        public virtual Bitmap GetBitmap()
        {
            return null;
        }
        
        public virtual string GetTabText()
        {
            return "Widget View";
        }

        public virtual bool InitiallyFloating
        {
            get
            {
                return false;
            }
        }

        public virtual TD.SandDock.ContainerDockLocation InitialDockLocation
        {
            get
            {
                return TD.SandDock.ContainerDockLocation.Left;
            }
        }

        public virtual bool IsSingleInstance
        {
            get
            {
                return false;
            }
        }

        public virtual void UserPrefDirChanged( object sender, UserPrefDirChangedEventArgs e )
        {
            // nothing to do
        }
        #endregion

        #region IWidgetView Interface
        public virtual string LabelHelp
        {
            set
            {

            }
        }

        public virtual string LabelDescription
        {
            set
            {

            }
        }

        public virtual bool DisplayPath
        {
            get { return true; }
        }

        public virtual bool AllowDragDrop
        {
            get { return false; }
        }

        public System.Guid Guid
        {
            get
            {
                return m_guid;
            }
            set
            {
                m_guid = value;
            }
        }

        public virtual void SetIndexForWidget( WidgetVisible visible, int newIndex )
        {

        }

        public virtual void AddContextMenuOptions( ContextMenuStrip menu, Widget widget, WidgetVisible visible )
        {

        }

        public virtual void SetDirty()
        {

        }

        public virtual Control GetControlAtCursorPoint()
        {
            return null;
        }

        public virtual void ResizeAndReposition()
        {

        }
        #endregion

        #region Public Virtual Functions
        public virtual void DeserializeView( XmlTextReader xmlReader, IBankManager bankMgr )
        {
            string closeAction = xmlReader["CloseAction"];
            if ( !String.IsNullOrEmpty( closeAction ) )
            {
                this.DockControl.CloseAction = (TD.SandDock.DockControlCloseAction)Enum.Parse(
                    typeof( TD.SandDock.DockControlCloseAction ), closeAction );
            }
        }

        public virtual void ClearView()
        {
        
        }
        #endregion

        #region Protected Virtual Functions
        protected virtual void widgetView_SizeChanged( object sender, EventArgs e )
        {
            
        }

        protected virtual void SerializeView( XmlTextWriter xmlWriter, String exePathName )
        {
            xmlWriter.WriteAttributeString( "CloseAction", this.DockControl.CloseAction.ToString() );
        }
        #endregion

        #region Protected Functions
        protected void dockControl_Closing( object sender, TD.SandDock.DockControlClosingEventArgs e )
        {
            if ( e.DockControl.CloseAction == DockControlCloseAction.Dispose )
            {
                ClearView();
                m_DockControl = null;

                // if we're disposing, forget the group open/close states
                WidgetGroupBase.ExpandedWidgetViewGroups.Remove( this.Guid.ToString() );
            }
        }

        protected void NotifyParentThatWidgetVisibleIsVisible( Widget widget )
        {
            WidgetGroupBase group = widget as WidgetGroupBase;
            do
            {
                if ( group != null )
                    group.AddRefGroup();
                group = widget.Parent as WidgetGroupBase;
                widget = group;
            }
            while ( group != null );
        }

        protected void NotifyParentThatWidgetVisibleIsNotVisible( Widget widget )
        {
            WidgetGroupBase group = widget as WidgetGroupBase;
            do
            {
                if ( group != null )
                    group.ReleaseGroup();
                group = widget.Parent as WidgetGroupBase;
                widget = group;
            }
            while ( group != null );
        }
        #endregion

        #region Public Static Functions
        public static void SerializeViews( XmlTextWriter xmlWriter, String exePathName )
        {
            xmlWriter.WriteStartElement( "WidgetViews" );
            {
                foreach ( IDockableView view in DockableViewManager.Instance.DockableViews )
                {
                    WidgetViewBase widgetView = view as WidgetViewBase;
                    if ( (widgetView != null) && (widgetView.DockControl != null) && widgetView.CanSerialize )
                    {
                        xmlWriter.WriteWhitespace( "\n" );
                        xmlWriter.WriteStartElement( widgetView.GetType().ToString() );
                        xmlWriter.WriteAttributeString( "guid", widgetView.DockControl.Guid.ToString() );
                        xmlWriter.WriteAttributeString( "appname", widgetView.m_AppName );
                        {
                            widgetView.SerializeView( xmlWriter, exePathName );
                        }
                        xmlWriter.WriteEndElement();
                    }
                }

                xmlWriter.WriteWhitespace( "\n" );
            }
            xmlWriter.WriteEndElement();
        }
        #endregion

        #region Protected Static Functions
        protected static void SerializeWidgetPath( XmlTextWriter xmlWriter, Widget widget )
        {
            if ( widget is WidgetPlaceholder )
            {
                SerializeWidgetPath( xmlWriter, (widget as WidgetPlaceholder).ActualFullPath );
                return;
            }

            xmlWriter.WriteWhitespace( "\n  " );
            xmlWriter.WriteStartElement( "Widget" );
            {
                // figure out path
                xmlWriter.WriteWhitespace( "\n  " );
                xmlWriter.WriteElementString( "WidgetName", widget.Title );

                Widget parentWidget = widget.Parent;
                while ( parentWidget != null )
                {
                    xmlWriter.WriteWhitespace( "\n  " );
                    xmlWriter.WriteElementString( "GroupName", parentWidget.Title );
                    parentWidget = parentWidget.Parent;
                }
            }
            xmlWriter.WriteEndElement();
        }

        protected static void SerializeWidgetPath( XmlTextWriter xmlWriter, List<string> list )
        {
            xmlWriter.WriteWhitespace( "\n  " );
            xmlWriter.WriteStartElement( "Widget" );
            {
                // figure out path
                for ( int i = list.Count - 1; i >= 0 ; --i )
                {
                    xmlWriter.WriteWhitespace( "\n  " );
                    xmlWriter.WriteElementString( i == list.Count - 1 ? "WidgetName" : "GroupName", list[i] );
                }
            }
            xmlWriter.WriteEndElement();
        }

        protected static List<string> DeserializeWidgetPath( XmlTextReader xmlReader )
        {
            List<string> fullPath = new List<string>();

            if ( (xmlReader.NodeType != XmlNodeType.EndElement) && (xmlReader.Name == "Widget") )
            {
                // get the widget name:
                xmlReader.Read();
                if ( (xmlReader.NodeType != XmlNodeType.EndElement) && (xmlReader.Name == "WidgetName") )
                {
                    xmlReader.Read();
                    fullPath.Add( xmlReader.Value );
                }

                // read the end element
                xmlReader.Read(); 
                
                // get the group name
                xmlReader.Read();
                while ( !xmlReader.EOF && (xmlReader.Name == "GroupName") )
                {
                    xmlReader.Read();
                    fullPath.Insert( 0, xmlReader.Value );

                    xmlReader.Read(); // read end element
                    xmlReader.Read(); // read start element
                }

                if ( !xmlReader.EOF )
                {
                    Debug.Assert( xmlReader.NodeType == XmlNodeType.EndElement, "xmlReader.NodeType == XmlNodeType.EndElement" );
                    Debug.Assert( xmlReader.Name == "Widget", "xmlReader.Name == \"Widget\"" );
                }
            }

            return fullPath;
        }

        /// <summary>
        /// Searches for the widget that correspondes to the given widgetPath.  
        /// </summary>
        /// <param name="bankMgr">The <see cref="IBankManager"/> containing the banks to search.</param>
        /// <param name="widgetPath">The path to find.</param>
        /// <param name="canBeWidgetGroup"><c>true</c> if the returned Widget can be a <see cref="WidgetGroup"/>, otherwise <c>false</c>.</param>
        /// <returns></returns>
        protected static Widget ResolveWidgetPath( IBankManager bankMgr, List<string> widgetPath, bool canBeWidgetGroup )
        {
            int i = 0;
            List<Widget> banksFound = new List<Widget>();
            List<Widget> widgetsFound = new List<Widget>();

            // find any match banks:
            if ( i < widgetPath.Count )
            {
                WidgetBank.FindWidgetsWithMatchingString( bankMgr, true, true, true, true, false, widgetPath[i++], ref banksFound, 1 );

                foreach ( Widget bank in banksFound )
                {
                    SearchForWidgetName( bank, widgetPath, i, canBeWidgetGroup, ref widgetsFound );
                    if ( widgetsFound.Count > 0 )
                    {
                        // instantiate widget since we found it:
                        return widgetsFound[0];
                    }
                }
            }

            return null;
        }
        #endregion

        #region Private Static Functions
        private static void SearchForWidgetName( Widget widgetToSearchFrom, List<string> widgetPath, int index, bool includeGroups, ref List<Widget> foundWidgets )
        {
            if ( index >= widgetPath.Count )
            {
                return;
            }

            // see if we're at the last level:
            List<Widget> foundWidgetsInternal = new List<Widget>();
            widgetToSearchFrom.FindWidgetWithMatchingString( true, true, true, true, false, widgetPath[index], ref foundWidgetsInternal, 2 );

            // see if we're at the end and we found a widget that matched:
            if ( (index == widgetPath.Count - 1) && (foundWidgetsInternal.Count > 0) )
            {
                Widget widget = foundWidgetsInternal[0];
                if ( !widget.IsGroup() || includeGroups )
                {
                    foundWidgets.Add( widget );
                    return;
                }
            }

            // otherwise keep searching:
            foreach ( Widget widget in foundWidgetsInternal )
            {
                SearchForWidgetName( widget, widgetPath, index + 1, includeGroups, ref foundWidgets );
            }
        }        
        #endregion
    }
}
