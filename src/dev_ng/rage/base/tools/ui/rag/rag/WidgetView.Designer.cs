namespace rag
{
    partial class WidgetView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && (components != null) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WidgetView));
			this.splitContainer1 = new System.Windows.Forms.SplitContainer();
			this.leftPanelContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.leftPaneHideShowLeftPaneToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.leftPaneHideShowHelpPaneToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.splitContainer2 = new System.Windows.Forms.SplitContainer();
			this.xPanderList = new ragCore.XPanderList();
			this.mainPaneContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.mainPaneHideShowLeftPaneToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.mainPaneHideShowHelpPaneToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.helpPaneContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.helpPaneHideShowLeftPaneToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.helpPaneHideShowHelpPaneToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.helpDescriptionLabel = new System.Windows.Forms.RichTextBox();
			this.helpLabel = new System.Windows.Forms.Label();
			this.splitContainer1.Panel2.SuspendLayout();
			this.splitContainer1.SuspendLayout();
			this.leftPanelContextMenuStrip.SuspendLayout();
			this.splitContainer2.Panel1.SuspendLayout();
			this.splitContainer2.Panel2.SuspendLayout();
			this.splitContainer2.SuspendLayout();
			this.mainPaneContextMenuStrip.SuspendLayout();
			this.helpPaneContextMenuStrip.SuspendLayout();
			this.SuspendLayout();
			// 
			// splitContainer1
			// 
			this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
			this.splitContainer1.Location = new System.Drawing.Point(0, 0);
			this.splitContainer1.Name = "splitContainer1";
			// 
			// splitContainer1.Panel1
			// 
			this.splitContainer1.Panel1.ContextMenuStrip = this.leftPanelContextMenuStrip;
			this.splitContainer1.Panel1MinSize = 100;
			// 
			// splitContainer1.Panel2
			// 
			this.splitContainer1.Panel2.Controls.Add(this.splitContainer2);
			this.splitContainer1.Panel2MinSize = 100;
			this.splitContainer1.Size = new System.Drawing.Size(546, 541);
			this.splitContainer1.SplitterDistance = 181;
			this.splitContainer1.TabIndex = 0;
			// 
			// leftPanelContextMenuStrip
			// 
			this.leftPanelContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.leftPaneHideShowLeftPaneToolStripMenuItem,
            this.leftPaneHideShowHelpPaneToolStripMenuItem});
			this.leftPanelContextMenuStrip.Name = "leftPanelContextMenuStrip";
			this.leftPanelContextMenuStrip.Size = new System.Drawing.Size(147, 48);
			this.leftPanelContextMenuStrip.Opening += new System.ComponentModel.CancelEventHandler(this.helpPaneContextMenuStrip_Opening);
			// 
			// leftPaneHideShowLeftPaneToolStripMenuItem
			// 
			this.leftPaneHideShowLeftPaneToolStripMenuItem.Name = "leftPaneHideShowLeftPaneToolStripMenuItem";
			this.leftPaneHideShowLeftPaneToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
			this.leftPaneHideShowLeftPaneToolStripMenuItem.Text = "Hide Left Pane";
			this.leftPaneHideShowLeftPaneToolStripMenuItem.Click += new System.EventHandler(this.hideShowLeftPaneToolStripMenuItem_Click);
			// 
			// leftPaneHideShowHelpPaneToolStripMenuItem
			// 
			this.leftPaneHideShowHelpPaneToolStripMenuItem.Name = "leftPaneHideShowHelpPaneToolStripMenuItem";
			this.leftPaneHideShowHelpPaneToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
			this.leftPaneHideShowHelpPaneToolStripMenuItem.Text = "Hide Help Pane";
			this.leftPaneHideShowHelpPaneToolStripMenuItem.Click += new System.EventHandler(this.hideShowHelpPaneToolStripMenuItem_Click);
			// 
			// splitContainer2
			// 
			this.splitContainer2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.splitContainer2.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
			this.splitContainer2.Location = new System.Drawing.Point(0, 0);
			this.splitContainer2.Name = "splitContainer2";
			this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
			// 
			// splitContainer2.Panel1
			// 
			this.splitContainer2.Panel1.Controls.Add(this.xPanderList);
			this.splitContainer2.Panel1MinSize = 100;
			// 
			// splitContainer2.Panel2
			// 
			this.splitContainer2.Panel2.ContextMenuStrip = this.helpPaneContextMenuStrip;
			this.splitContainer2.Panel2.Controls.Add(this.helpDescriptionLabel);
			this.splitContainer2.Panel2.Controls.Add(this.helpLabel);
			this.splitContainer2.Panel2MinSize = 28;
			this.splitContainer2.Size = new System.Drawing.Size(361, 541);
			this.splitContainer2.SplitterDistance = 508;
			this.splitContainer2.TabIndex = 0;
			// 
			// xPanderList
			// 
			this.xPanderList.AutoScroll = true;
			this.xPanderList.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
			this.xPanderList.BackColorDark = System.Drawing.SystemColors.Control;
			this.xPanderList.BackColorLight = System.Drawing.SystemColors.Control;
			this.xPanderList.ContextMenuStrip = this.mainPaneContextMenuStrip;
			this.xPanderList.Dock = System.Windows.Forms.DockStyle.Fill;
			this.xPanderList.Location = new System.Drawing.Point(0, 0);
			this.xPanderList.MinimumWidth = 0;
			this.xPanderList.Name = "xPanderList";
			this.xPanderList.NestingBottomPad = 10;
			this.xPanderList.NestingIndent = 10;
			this.xPanderList.Size = new System.Drawing.Size(359, 506);
			this.xPanderList.TabIndex = 0;
			// 
			// mainPaneContextMenuStrip
			// 
			this.mainPaneContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mainPaneHideShowLeftPaneToolStripMenuItem,
            this.mainPaneHideShowHelpPaneToolStripMenuItem});
			this.mainPaneContextMenuStrip.Name = "mainPaneContextMenuStrip";
			this.mainPaneContextMenuStrip.Size = new System.Drawing.Size(147, 48);
			this.mainPaneContextMenuStrip.Opening += new System.ComponentModel.CancelEventHandler(this.helpPaneContextMenuStrip_Opening);
			// 
			// mainPaneHideShowLeftPaneToolStripMenuItem
			// 
			this.mainPaneHideShowLeftPaneToolStripMenuItem.Name = "mainPaneHideShowLeftPaneToolStripMenuItem";
			this.mainPaneHideShowLeftPaneToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
			this.mainPaneHideShowLeftPaneToolStripMenuItem.Text = "Hide Left Pane";
			this.mainPaneHideShowLeftPaneToolStripMenuItem.Click += new System.EventHandler(this.hideShowLeftPaneToolStripMenuItem_Click);
			// 
			// mainPaneHideShowHelpPaneToolStripMenuItem
			// 
			this.mainPaneHideShowHelpPaneToolStripMenuItem.Name = "mainPaneHideShowHelpPaneToolStripMenuItem";
			this.mainPaneHideShowHelpPaneToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
			this.mainPaneHideShowHelpPaneToolStripMenuItem.Text = "Hide Help Pane";
			this.mainPaneHideShowHelpPaneToolStripMenuItem.Click += new System.EventHandler(this.hideShowHelpPaneToolStripMenuItem_Click);
			// 
			// helpPaneContextMenuStrip
			// 
			this.helpPaneContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.helpPaneHideShowLeftPaneToolStripMenuItem,
            this.helpPaneHideShowHelpPaneToolStripMenuItem});
			this.helpPaneContextMenuStrip.Name = "helpPaneContextMenuStrip";
			this.helpPaneContextMenuStrip.Size = new System.Drawing.Size(162, 48);
			this.helpPaneContextMenuStrip.Opening += new System.ComponentModel.CancelEventHandler(this.helpPaneContextMenuStrip_Opening);
			// 
			// helpPaneHideShowLeftPaneToolStripMenuItem
			// 
			this.helpPaneHideShowLeftPaneToolStripMenuItem.Name = "helpPaneHideShowLeftPaneToolStripMenuItem";
			this.helpPaneHideShowLeftPaneToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
			this.helpPaneHideShowLeftPaneToolStripMenuItem.Text = "Hide Left Panel";
			this.helpPaneHideShowLeftPaneToolStripMenuItem.Click += new System.EventHandler(this.hideShowLeftPaneToolStripMenuItem_Click);
			// 
			// helpPaneHideShowHelpPaneToolStripMenuItem
			// 
			this.helpPaneHideShowHelpPaneToolStripMenuItem.Name = "helpPaneHideShowHelpPaneToolStripMenuItem";
			this.helpPaneHideShowHelpPaneToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
			this.helpPaneHideShowHelpPaneToolStripMenuItem.Text = "Hide Bottom Panel";
			this.helpPaneHideShowHelpPaneToolStripMenuItem.Click += new System.EventHandler(this.hideShowHelpPaneToolStripMenuItem_Click);
			// 
			// helpDescriptionLabel
			// 
			this.helpDescriptionLabel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.helpDescriptionLabel.Location = new System.Drawing.Point(0, 27);
			this.helpDescriptionLabel.Name = "helpDescriptionLabel";
			this.helpDescriptionLabel.ReadOnly = true;
			this.helpDescriptionLabel.Size = new System.Drawing.Size(359, 0);
			this.helpDescriptionLabel.TabIndex = 1;
			this.helpDescriptionLabel.Text = "";
			this.helpDescriptionLabel.LinkClicked += new System.Windows.Forms.LinkClickedEventHandler(this.helpDescriptionLabel_LinkClicked);
			// 
			// helpLabel
			// 
			this.helpLabel.Dock = System.Windows.Forms.DockStyle.Top;
			this.helpLabel.Image = ((System.Drawing.Image)(resources.GetObject("helpLabel.Image")));
			this.helpLabel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.helpLabel.Location = new System.Drawing.Point(0, 0);
			this.helpLabel.Name = "helpLabel";
			this.helpLabel.Size = new System.Drawing.Size(359, 27);
			this.helpLabel.TabIndex = 0;
			this.helpLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// WidgetView
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.Controls.Add(this.splitContainer1);
			this.Name = "WidgetView";
			this.Size = new System.Drawing.Size(546, 541);
			this.splitContainer1.Panel2.ResumeLayout(false);
			this.splitContainer1.ResumeLayout(false);
			this.leftPanelContextMenuStrip.ResumeLayout(false);
			this.splitContainer2.Panel1.ResumeLayout(false);
			this.splitContainer2.Panel2.ResumeLayout(false);
			this.splitContainer2.ResumeLayout(false);
			this.mainPaneContextMenuStrip.ResumeLayout(false);
			this.helpPaneContextMenuStrip.ResumeLayout(false);
			this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.RichTextBox helpDescriptionLabel;
        private System.Windows.Forms.Label helpLabel;
        protected ragCore.XPanderList xPanderList;
        protected System.Windows.Forms.SplitContainer splitContainer1;
        protected System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.ToolStripMenuItem helpPaneHideShowLeftPaneToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpPaneHideShowHelpPaneToolStripMenuItem;
        protected System.Windows.Forms.ContextMenuStrip helpPaneContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem leftPaneHideShowLeftPaneToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem leftPaneHideShowHelpPaneToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mainPaneHideShowLeftPaneToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mainPaneHideShowHelpPaneToolStripMenuItem;
        protected System.Windows.Forms.ContextMenuStrip leftPanelContextMenuStrip;
        protected System.Windows.Forms.ContextMenuStrip mainPaneContextMenuStrip;
    }
}
