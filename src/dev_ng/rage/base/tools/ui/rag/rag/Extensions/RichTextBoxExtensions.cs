﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace rag.Extensions
{
    /// <summary>
    /// 
    /// </summary>
    public static class RichTextBoxExtensions
    {
        /// <summary>
        /// Limit it to 50 MB
        /// </summary>
        private const int c_textBoxCharLimit = 10 * 1024 * 1024;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obTextBox"></param>
        public static void LimitTextLength(this RichTextBox textBox, int charLimit = c_textBoxCharLimit)
        {
            while (textBox.TextLength > charLimit)
            {
                // It would be really nice if I could just remove the top n chars from the RichBox text,
                // but there is no way to do this :(
                // The best you can do is get a copy of the text, remove the top n chars, delete the 
                // text in the richtext box and add the new text.  This method is crap though becuase
                // it is slow, uses up a ton of memory and causes the text in the richtext box to flicker 
                // like a crazy thing :(
                // So when we see that we need to do this, remove A LOT of the chars so it will be a while
                // until we need to do it again

                // Remove half the text
                string strNewString = textBox.Rtf.Substring(textBox.Rtf.Length / 2);

                int iNewLineIndex = strNewString.IndexOf('\n');
                if (iNewLineIndex > -1)
                {
                    strNewString = strNewString.Substring(iNewLineIndex + 1);
                }

                strNewString = @"{\rtf1\ansi\ansicpg1252\deff0\deflang1033{\fonttbl{\f0\fnil\fcharset0 Courier New;}}
{\colortbl ;\red218\green165\blue32;\red255\green255\blue255;}
\viewkind4\uc1\pard\f0\fs17 " + strNewString;
                textBox.Rtf = strNewString;
                // obTextBox.AppendText(strNewString);
            }
        }

        /// <summary>
        ///  Appends coloured text.
        /// </summary>
        /// <param name="box"></param>
        /// <param name="text"></param>
        /// <param name="color"></param>
        public static void AppendText(this RichTextBox textBox, String text, Color foreground, Color background)
        {
            textBox.SelectionStart = textBox.TextLength;
            textBox.SelectionLength = 0;

            textBox.SelectionColor = foreground;
            textBox.SelectionBackColor = background;
            textBox.AppendText(text);
            //textBox.DeselectAll();
        }
    } // RichTextBoxExtensions
}
