using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Xml;

using ragCore;
using RSG.Base.Forms;
using ragWidgets;

namespace rag
{
    public partial class WidgetViewSingle : rag.WidgetViewBase
    {
        protected WidgetViewSingle()
        {
            InitializeComponent();
        }

        public WidgetViewSingle( String appName, IBankManager bankMgr, TD.SandDock.DockControl dockControl )
            : base( appName, bankMgr, dockControl )
		{
            InitializeComponent();

            m_ViewNum = ++sm_ViewNum;

            dockControl.Closed += new EventHandler( dockControl_Closed );
        }

        #region Variables
        private static int sm_ViewNum = 0;

        private int m_ViewNum;
        private List<string> m_FullPath = new List<string>();
        private Widget m_TheWidget;
        #endregion

        #region Properties
        /// <summary>
        /// Returns the Full Path if it was stored without a widget
        /// </summary>
        public string PlaceholderWidgetPath
        {
            get
            {
                if ( m_TheWidget is WidgetPlaceholder )
                {
                    return (m_TheWidget as WidgetPlaceholder).ActualPath;
                }
                else
                {
                    return string.Empty;
                }
            }
        }
        #endregion

        #region Overrides
        public override bool DisplayPath
        {
            get
            {
                return false;
            }
        }

        public override bool CanSerialize
        {
            get
            {
                return ((m_TheWidget != null) && (m_TheWidget.Parent != null)) || (m_TheWidget != null);
            }
        }

        public override void ClearView()
        {
            BankRemotePacketProcessor.MakingVisibleMutex.WaitOne();

            if ( m_TheWidget != null )
            {
                NotifyParentThatWidgetVisibleIsNotVisible( m_TheWidget );

                WidgetVisualiser.HideVisible( m_TheWidget, GetType().ToString() + m_ViewNum );

                m_TheWidget.DestroyEvent -= new EventHandler( widget_DestroyEvent );

                m_TheWidget = null;
            }

            BankRemotePacketProcessor.MakingVisibleMutex.ReleaseMutex();
        }

        public override void DeserializeView( XmlTextReader xmlReader, IBankManager bankMgr )
        {
            base.DeserializeView( xmlReader, bankMgr );

            while ( (xmlReader.NodeType != XmlNodeType.EndElement) || (xmlReader.Name != GetType().ToString()) )
            {
                m_FullPath = DeserializeWidgetPath( xmlReader );

                xmlReader.Read();
            }

            bankMgr.InitFinished += new ragCore.InitFinishedHandler( InitFinished );
        }

        protected override void SerializeView( XmlTextWriter xmlWriter, String exePathName )
        {
            base.SerializeView( xmlWriter, exePathName );

            if ( m_TheWidget != null )
            {
                WidgetViewBase.SerializeWidgetPath( xmlWriter, m_TheWidget );
            }
            else if ( m_FullPath != null )
            {
                WidgetViewBase.SerializeWidgetPath( xmlWriter, m_FullPath );
            }
        }
        #endregion

        #region Public Functions
        public void AddWidget( Widget widget )
        {
            BankRemotePacketProcessor.MakingVisibleMutex.WaitOne();

            // close it if it doesn't exit:
            if ( widget == null )
            {
                widget = new WidgetPlaceholder( m_FullPath, Color.Empty );
            }
            else if ( (m_TheWidget != null) && (m_TheWidget is WidgetPlaceholder) )
            {
                WidgetPlaceholder placeholder = m_TheWidget as WidgetPlaceholder;
                if ( placeholder.ActualPath != this.PlaceholderWidgetPath )
                {
                    // the widget we're trying to add wasn't intended for this view
                    return;
                }
                else
                {
                    WidgetVisualiser.HideVisible( m_TheWidget, GetType().ToString() + m_ViewNum );
                }
            }
            else if ( !(widget is WidgetPlaceholder) )
            {
                // store the path
                m_FullPath.Clear();
                m_FullPath.AddRange( widget.Path.Split( new char[] { '\\' } ) );
            }
               
            if ( widget.IsGroup() )
            {
                try
                {
                    WidgetVisualiser.ShowVisible(widget, this, this.xPanderList, GetType().ToString() + m_ViewNum, true, toolTip1);
                }
                catch ( Exception e )
                {
                    DialogResult result = rageMessageBox.ShowExclamation( this.DockControl,
                        String.Format( "There was an error creating a Widget Visible for the Widget {0}:\n\n{1}\nWould you like to disable the widget?",
                        widget.Title, e.ToString() ), "Widget Error", MessageBoxButtons.YesNo );
                    if ( result == DialogResult.Yes )
                    {
                        widget.Enabled = false;

                        if ( !WidgetVisualiser.PacketProcessor.DisabledWidgets.Contains( widget ) )
                        {
                            WidgetVisualiser.PacketProcessor.DisabledWidgets.Add( widget );
                        }
                    }
                }

                Debug.Assert( this.Controls.Count <= 1, "Too many controls" );
                Debug.Assert( this.xPanderList.Controls.Count >= 1, "Too few controls" );

                m_DockControl.TabText = m_DockControl.Text = this.xPanderList.Controls[0].Text;
                this.xPanderList.Visible = true;
            }
            else
            {
                try
                {
                    WidgetVisualiser.ShowVisible(widget, this, this, GetType().ToString() + m_ViewNum, true, toolTip1);
                }
                catch ( Exception e )
                {
                    DialogResult result = rageMessageBox.ShowExclamation( this.DockControl,
                        String.Format( "There was an error creating a Widget Visible for the Widget {0}:\n\n{1}\nWould you like to disable the widget?",
                        widget.Title, e.ToString() ), "Widget Error", MessageBoxButtons.YesNo );
                    if ( result == DialogResult.Yes )
                    {
                        widget.Enabled = false;

                        if ( !WidgetVisualiser.PacketProcessor.DisabledWidgets.Contains( widget ) )
                        {
                            WidgetVisualiser.PacketProcessor.DisabledWidgets.Add( widget );
                        }
                    }
                }

                Debug.Assert( this.Controls.Count <= 2, "Too many controls" );

                this.Controls[1].Dock = DockStyle.Fill;
                m_DockControl.TabText = m_DockControl.Text = this.Controls[1].Text;
                this.xPanderList.Visible = false;
            }

            if ( !(widget is WidgetPlaceholder) )
            {
                widget.DestroyEvent += new EventHandler( widget_DestroyEvent );
            }

            NotifyParentThatWidgetVisibleIsVisible( widget );

            m_TheWidget = widget;

            BankRemotePacketProcessor.MakingVisibleMutex.ReleaseMutex();
        }
        #endregion

        #region Event Handlers
        private void dockControl_Closed( object sender, EventArgs e )
        {
            TD.SandDock.DockControl dockControl = sender as TD.SandDock.DockControl;
            if ( dockControl != null && dockControl.CloseAction == TD.SandDock.DockControlCloseAction.Dispose )
            {
                m_BankManager.InitFinished -= new ragCore.InitFinishedHandler( InitFinished );
            }
        }

        private void widget_DestroyEvent( object sender, EventArgs e )
        {
            if ( sender is Widget )
            {
                Widget widget = sender as Widget;
                widget.DestroyEvent -= new EventHandler( widget_DestroyEvent );
            }

            // add a placeholder with the original fullPath in case the widget comes back at a later time
            AddWidget( new WidgetPlaceholder( m_FullPath, Color.Empty ) );
        }
        #endregion

        #region Private Functions
        private void InitFinished()
        {
            AddWidget( ResolveWidgetPath( m_BankManager, m_FullPath, true ) );
        }
        #endregion

        #region Static Functions
        public static WidgetViewBase CreateWidgetViewSingle( String appName, IBankManager bankMgr, TD.SandDock.DockControl dockControl )
        {
            WidgetViewSingle singleView = new WidgetViewSingle( appName, bankMgr, dockControl );
            dockControl.CloseAction = TD.SandDock.DockControlCloseAction.Dispose;
            return singleView;
        }

        public static void ClearStatics()
        {
            sm_ViewNum = 0;
        }
        #endregion
    }
}

