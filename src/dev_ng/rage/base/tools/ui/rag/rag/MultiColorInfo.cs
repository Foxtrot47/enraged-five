﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace rag
{
    /// <summary>
    /// 
    /// </summary>
    internal class MultiColorInfo
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="commandLine"></param>
        /// <param name="defaultColor"></param>
        public MultiColorInfo(String commandLine, Color defaultColor)
        {
            _commandLine = commandLine;
            _defaultColor = defaultColor;
        }
        #endregion // Constructor(s)

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public String CommandLine
        {
            get { return _commandLine; }
        }
        private String _commandLine;

        /// <summary>
        /// 
        /// </summary>
        public Color DefaultColor
        {
            get { return _defaultColor; }
            set { _defaultColor = value; }
        }
        private Color _defaultColor;

        /// <summary>
        /// 
        /// </summary>
        public bool SetByCommandLine
        {
            get { return _setByCommandLine; }
            set { _setByCommandLine = value; }
        }
        private bool _setByCommandLine = false;

        /// <summary>
        /// 
        /// </summary>
        public bool MultiColor
        {
            get { return _multiColor; }
            set { _multiColor = value; }
        }
        private bool _multiColor = true;
        #endregion // Properties
    } // MultiColorInfo
}
