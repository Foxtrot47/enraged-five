using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows.Forms;

using ragCore;
using TD.SandDock;
using ragWidgets;
using RSG.Base.Logging;

namespace ragWidgetPage
{
    public abstract class WidgetPagePlugIn : IViewPlugIn, IDockableViewManager
    {
        #region IViewPlugIn Interface
        public abstract string Name { get; }

        /// <summary>
        /// In your derived function, you should register all of your WidgetPage.CreatePageDel's with m_pageNameToCreateDelMap
        /// using a static string MyName that returns the same thing as your override GetMyName() function.
        /// </summary>
        /// <param name="hostData"></param>
        public virtual void InitPlugIn( PlugInHostData hostData )
        {
            sm_HostData = hostData;
            this.DllInfo = hostData.DllInfo;

            m_MenuItem = new ToolStripMenuItem( this.Name, null, new EventHandler( ActivatePlugInMenuItem ) );
            hostData.PlugInToolStripMenu.DropDownItems.Add( m_MenuItem );

            m_initFinishedEventHandler = new ragCore.InitFinishedHandler( MainBankManager_InitFinished );
            hostData.MainRageApplication.BankMgr.InitFinished += m_initFinishedEventHandler;

            m_setPluginStatusEventHandler = new EventHandler( SetPluginStatus );
            hostData.PlugInToolStripMenu.DropDownOpening += m_setPluginStatusEventHandler;

            LoadPluginData( DllInfo.PluginDataHash );
        }

        public virtual void RemovePlugIn()
        {
            if ( (m_MenuItem != null) && (m_MenuItem.Container != null) )
            {
                m_MenuItem.Container.Remove( m_MenuItem );
                m_MenuItem = null;
            }

            if ( sm_HostData.MainRageApplication.BankMgr != null )
            {
                sm_HostData.MainRageApplication.BankMgr.InitFinished -= m_initFinishedEventHandler;
            }

            if ( sm_HostData.PlugInToolStripMenu != null )
            {
                sm_HostData.PlugInToolStripMenu.DropDownOpening -= m_setPluginStatusEventHandler;
            }

            foreach ( WidgetPage page in m_WidgetPages )
            {
                page.SaveWidgetPageData( DllInfo.PluginDataHash );
            }

            SavePluginData( DllInfo.PluginDataHash );
        }
        #endregion

        #region IDockableViewManager Interface
        public IDockableView CreateView(TD.SandDock.DockControl dockControl, IDockableView dockableView, ILog log)
        {
            return sm_HostData.ViewManager.CreateView(dockControl, dockableView, log);
        }

        public bool OpenView( TD.SandDock.DockControl dockControl,
            bool initiallyFloating, TD.SandDock.ContainerDockLocation dockLocation, ILog log )
        {
            return sm_HostData.ViewManager.OpenView( dockControl, initiallyFloating, dockLocation, log );
        }

        public void PostOpenView( TD.SandDock.DockControl dockControl, IDockableView dockableView )
        {
            WidgetPage page = dockableView as WidgetPage;

            if ( !m_WidgetPages.Contains( page ) )
            {
                m_WidgetPages.Add( page );
            }
    
            sm_HostData.ViewManager.PostOpenView( dockControl, dockableView );

            if ( ShowOnActivation )
            {
                dockControl.Activate();
            }
        }

        /// <summary>
        /// This function gets called at startup when Rag is loading the SandDock layout.  Override this 
        /// if you need alternate behavior.  A good example is if you can have multiple instances of the 
        /// same WidgetPage, in which case DllInfo.GuidToNameHash[guid]'s pageName isn't going to exist in 
        /// m_pageNameToCreateDelMap.
        /// </summary>
        /// <param name="guid"></param>
        /// <returns></returns>
        public virtual TD.SandDock.DockControl ResolveDockControl(String guid, ILog log)
        {
            if ( DllInfo.GuidToNameHash.ContainsKey( guid ) )
            {
                String pageName = DllInfo.GuidToNameHash[guid];
                if ( m_pageNameToCreateDelMap.ContainsKey( pageName ) )
                {
                    IDockableView view = CreateView( m_pageNameToCreateDelMap[pageName], log );

                    // we can go ahead and call PostOpenView because SandDockManager is going to do the opening for us
                    PostOpenView( view.DockControl, view );
                    return view.DockControl;
                }
            }

            return null;
        }
        #endregion

        #region WidgetPage handling
        /// <summary>
        /// Convenience function for derived classes.  Typically called from ActivatePlugInMenuItem.
        /// </summary>
        /// <param name="createPageDel"></param>
        /// <returns>true if the WidgetPage was opened</returns>
        protected bool CreateAndOpenView(WidgetPage.CreatePageDel createPageDel, ILog log)
        {
            IDockableView view = CreateView( createPageDel, log );
            if ( OpenView( view.DockControl, view.InitiallyFloating, view.InitialDockLocation,LogFactory.ApplicationLog ) )
            {
                PostOpenView( view.DockControl, view );
                return true;
            }

            return false;
        }

        /// <summary>
        /// Wrapper for creating our WidgetPage and initializing its data from DllInfo.PluginDataHash.
        /// Called by ResolveDockControl and CreateAndOpenView, but can be called by a derived class, typically
        /// in its ActivatePlugInMenuItem function.  In that case, make sure to also call OpenView and PostOpenView
        /// in a similar fashion to CreateAndOpenView.
        /// </summary>
        /// <param name="factory"></param>
        /// <returns>the WidgetPage that was created</returns>
        protected WidgetPage CreateView(WidgetPage.CreatePageDel createPageDel, ILog log)
        {
            TD.SandDock.DockableWindow dockControl = new TD.SandDock.DockableWindow();

            WidgetPage page = createPageDel( sm_HostData.MainRageApplication.VisibleName, sm_HostData.MainRageApplication.BankMgr, sm_HostData.KeyUpProcessor, dockControl, log );
            page.LoadWidgetPageData( DllInfo.PluginDataHash );
            page.m_WidgetPagePlugIn = this;

            if ( DllInfo.NameToGuidHash.ContainsKey( page.GetMyName() ) )
            {
                dockControl.Guid = new System.Guid( DllInfo.NameToGuidHash[page.GetMyName()] );
            }
            else
            {
                DllInfo.NameToGuidHash[page.GetMyName()] = dockControl.Guid.ToString();
                DllInfo.GuidToNameHash[dockControl.Guid.ToString()] = page.GetMyName();

                // should only have to do this if we didn't load our layout from last session
                System.Drawing.Size size = page.Size;
                size.Height += 20; // account for title bar height
                dockControl.FloatingSize = size;
            }

            RemoveFromActiveListFunctor functor = new RemoveFromActiveListFunctor( page, this );
            dockControl.Closing += new TD.SandDock.DockControlClosingEventHandler( functor.RemoveFromActiveList );

            return CreateView( dockControl, page, log ) as WidgetPage;
        }

        /// <summary>
        /// Class used to clean up a WidgetPage when it is closed during program execution.  Only used when
        /// A WidgetPage's DockControl.CloseAction is TD.SandDock.DockControlCloseAction.Dispose (default is HideOnly).
        /// No persistent information about this WidgetPage will be saved.
        /// </summary>
        class RemoveFromActiveListFunctor
        {
            public RemoveFromActiveListFunctor( WidgetPage p, WidgetPagePlugIn plugin ) { Page = p; Plugin = plugin; }
            public WidgetPage Page;
            public WidgetPagePlugIn Plugin;
            public void RemoveFromActiveList( object sender, TD.SandDock.DockControlClosingEventArgs e )
            {
                if ( e.DockControl.CloseAction == TD.SandDock.DockControlCloseAction.Dispose )
                {
                    Plugin.m_WidgetPages.Remove( Page );

                    Plugin.DllInfo.NameToGuidHash.Remove( Page.GetMyName() );
                    Plugin.DllInfo.GuidToNameHash.Remove( Page.DockControl.Guid.ToString() );

                    // remove any settings from PluginData
                    List<String> removeKeys = new List<String>();
                    foreach ( KeyValuePair<String, String> pair in Plugin.DllInfo.PluginDataHash )
                    {
                        if ( pair.Key.StartsWith( Page.GetMyName() ) )
                        {
                            removeKeys.Add( pair.Key );
                        }
                    }

                    foreach ( String key in removeKeys )
                    {
                        Plugin.DllInfo.PluginDataHash.Remove( key );
                    }
                }
            }
        }
        #endregion

        #region Virtuals and abstracts
        /// <summary>
        /// Called when the Main Bank Manager receives the "first frame" message from the game.  By this point,
        /// most, if not all, of the banks and widgets have been created.
        /// If your plugin relies on a certain bank or widget (usually tested by CheckPluginStatus), now is the 
        /// time to perform any bindings.  
        /// By default, this Event Handler will close any WidgetPages if CheckPluginStatus returns false.
        /// </summary>
        protected virtual void MainBankManager_InitFinished()
        {
            if ( !CheckPluginStatus() )
            {
                // hide (do not dispose) all of the pages, but restore the CloseAction.
                foreach ( WidgetPage page in m_WidgetPages )
                {
                    DockControlCloseAction closeAction = page.DockControl.CloseAction;
                    
                    page.DockControl.CloseAction = DockControlCloseAction.HideOnly;
                    page.DockControl.Close();
                    page.DockControl.CloseAction = closeAction;
                }
            }
        }
        
        /// <summary>
        /// This function gets called when this Plugin's Menu Item is clicked AND LoadLayout has returne false.
        /// All derived classes need to override this to create and/or open their WidgetPage(s).
        /// Typically, this function should
        ///  1) If the WidgetPage's DockControl.CloseAction is TD.SandDock.DockControlCloseAction.HideOnly (the default)
        ///     and the WidgetPage has already been created and opened before, call DockControl.Open().
        ///  2) Otherwise, call CreateAndOpenView.  For advanced users not happy with the behavior of 
        ///     CreateAndOpenView, see the summary for CreateView.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        public abstract void ActivatePlugInMenuItem( object sender, System.EventArgs args );

        /// <summary>
        /// Return true to show the WidgetPage and give it focus when created with CreateView or CreateAndOpenView.
        /// </summary>
        public virtual bool ShowOnActivation
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Override this to check when the plug-in can and can't run.
        /// For example certain widgets may need to be present for the plugin to run.
        /// </summary>
        /// <returns></returns>
        public virtual bool CheckPluginStatus()
        {
            return true;
        }

        /// <summary>
        /// Event Handler attached to the Plugins Menu that calls CheckPluginStatus
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SetPluginStatus( object sender, EventArgs e )
        {
            m_MenuItem.Enabled = CheckPluginStatus();
        }

        /// <summary>
        /// Override this to load plugin-level data from DllInfo.PluginDataHash.  Call it from your overridden
        /// function if you wish to load layout data.
        /// </summary>
        /// <param name="data"></param>
        protected virtual void LoadPluginData( Dictionary<string, string> data )
        {
        }

        /// <summary>
        /// Override this to save plugin-level data to DllInfo.PluginDataHash.  Call it from your overridden
        /// function if you wish to save layout data.
        /// </summary>
        /// <param name="data"></param>
        protected virtual void SavePluginData( Dictionary<string, string> data )
        {
        }
        #endregion

        #region Properties
        public ragCore.DllInfo DllInfo;
        public ToolStripMenuItem m_MenuItem;
        public List<WidgetPage> m_WidgetPages = new List<WidgetPage>();
        #endregion

        #region Static Variables
        public static PlugInHostData sm_HostData;
        #endregion

        #region Variables
        protected Dictionary<WidgetPage, object> m_SavedStateData = new Dictionary<WidgetPage, object>();
        protected Dictionary<string, WidgetPage.CreatePageDel> m_pageNameToCreateDelMap = new Dictionary<string, WidgetPage.CreatePageDel>();
        private EventHandler m_setPluginStatusEventHandler;
        private ragCore.InitFinishedHandler m_initFinishedEventHandler;
        #endregion

        #region Events
        protected EventHandler m_PostRebuildEvent;
        #endregion

        #region State data
        /// <summary>
        /// Save state data from every WidgetPage and sends it to the MainBankManager
        /// </summary>
        public virtual void RebuildAllPanels()
        {
            m_SavedStateData.Clear();
            foreach ( WidgetPage page in m_WidgetPages )
            {
                object data = page.GetStateData();
                if ( data != null )
                {
                    m_SavedStateData.Add( page, page.GetStateData() );
                    page.RemoveBindings();
                }
            }

            ragCore.Widget w = sm_HostData.MainRageApplication.BankMgr.AllBanks[0] as ragCore.Widget;
            ragCore.BankPipe pipe = w.Pipe;
            sm_HostData.MainRageApplication.BankMgr.SendDelayedPing( new EventHandler( RebuildAfterWidgetUpdate ), pipe );
        }

        /// <summary>
        /// Sets a post-rebuild event and calls RebuildAllPanels()
        /// </summary>
        /// <param name="postRebuildEvent"></param>
        public void RebuildAllPanels( EventHandler postRebuildEvent )
        {
            m_PostRebuildEvent = postRebuildEvent;
            RebuildAllPanels();
        }

        /// <summary>
        /// Loads state date for WidgetPages and executes the post-rebuild event, if any.
        /// The post-rebuild event is called only once.  Call RebuildAllPanels(EventHandler ) again
        /// to specify a new post-build event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void RebuildAfterWidgetUpdate( object sender, EventArgs e )
        {
            foreach ( WidgetPage page in m_WidgetPages )
            {
                object val;
                if ( m_SavedStateData.TryGetValue( page, out val ) )
                {
                    page.SetStateData( val );
                }
            }
            m_SavedStateData.Clear();

            if ( m_PostRebuildEvent != null )
            {
                // prevent infinite loops
                EventHandler myEvent = m_PostRebuildEvent;
                m_PostRebuildEvent = null;
                myEvent( sender, e );
            }
        }

        /// <summary>
        /// Data for a WidgetPage
        /// </summary>
        public class StateData
        {
            public string m_WidgetRootPath;
        }

        /// <summary>
        /// Creates a new StateData object and saves the path of a WidgetPage's associated
        /// Widget
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public static object BasicGetStateData( WidgetPage p )
        {
            StateData data = new StateData();
            if ( p.m_WidgetRoot != null )
            {
                data.m_WidgetRootPath = p.m_WidgetRoot.FindPath();
            }
            else
            {
                data.m_WidgetRootPath = null;
            }
            return data;
        }

        /// <summary>
        /// Loads the path of a WidgetPage's associated widget from o
        /// </summary>
        /// <param name="p"></param>
        /// <param name="o"></param>
        public static void BasicSetStateData( WidgetPage p, object o )
        {
            StateData data = o as StateData;
            if ( data.m_WidgetRootPath != null )
            {
                p.m_WidgetRoot = sm_HostData.MainRageApplication.BankMgr.FindFirstWidgetFromPath( data.m_WidgetRootPath ) as WidgetGroup;
            }
            else
            {
                p.m_WidgetRoot = null;
            }
        }
        #endregion
    }
}
