using System;
using System.Reflection;

using ragCore;
using RSG.Base.Forms;
using ragUi;
using ragWidgets;

namespace ragWidgets
{
	/// <summary>
	/// Some of this is based on the code here: http://www.codeproject.com/csharp/simpledatabinding.asp
	/// </summary>
	public class WidgetBinding
	{
		protected Widget m_Widget;
		protected System.Windows.Forms.Control m_Control;
		protected bool m_NeedsTypeConversion;
		protected PropertyInfo m_WidgetValueProperty;
		protected PropertyInfo m_ControlValueProperty;

		public System.Windows.Forms.Control Control {
			get {
				return m_Control;
			}
		}

		public Widget Widget {
			get {
				return m_Widget;
			}
		}

		private void ControlValueChanged(Object sender, EventArgs e)
		{
			object val = m_ControlValueProperty.GetValue(m_Control, null);
			val = ConvertType(val, m_WidgetValueProperty.PropertyType);
			m_WidgetValueProperty.SetValue(m_Widget, val, null);
		}

		private void WidgetValueChanged(Object sender, EventArgs e)
		{
			Object val = m_WidgetValueProperty.GetValue(m_Widget, null);
			val = ConvertType(val, m_ControlValueProperty.PropertyType);
			m_ControlValueProperty.SetValue(m_Control, val, null);
		}

		// decimal <-> int64
		// decimal <-> single
		// int64 <-> int32
		protected object ConvertType(object src, Type destType) {
			if (destType.IsInstanceOfType(src)) {
				return src;
			}
			if (src is System.Decimal) {
				if (destType == typeof(System.Int64)) {
					return (int)(System.Decimal)(src);
				}
				else if (destType == typeof(System.Single)) {
					return (float)(System.Decimal)(src);
				}
			}	
			else if (src is System.Single) {
				if (destType == typeof(System.Decimal)) {
					return (Decimal)(System.Single)(src);
				}
			}
			else if (src is System.Int64) {
				if (destType == typeof(System.Decimal)) 
				{
					return (Decimal)(System.Int64)(src);
				}
				else if (destType == typeof(System.Int32)) 
				{
					return (System.Int32)(System.Int64)(src);
				}
			}
			else if (src is System.Int32) {
				if (destType == typeof(System.Int64))
				{
					return (System.Int64)(System.Int32)(src);
				}
			}

            rageMessageBox.ShowExclamation( this.Control, 
                String.Format( "Can't find conversion from {0} to {1}", src.GetType().ToString(), destType.ToString() ), 
                "Conversion Problem" );
			return null;
		}

		protected WidgetBinding(Widget widget, string widgetProp, System.Windows.Forms.Control ctrl, string controlProp)
		{
			m_Widget = widget;
			m_WidgetValueProperty = widget.GetType().GetProperty(widgetProp);
			m_Control = ctrl;
			m_ControlValueProperty = ctrl.GetType().GetProperty(controlProp);

			ConnectEvents();

			// send initial data from widget to control
			WidgetValueChanged(this, System.EventArgs.Empty);
		}

		public void ConnectEvents()
		{
            EventInfo widgetChangedEvent = m_Widget.GetType().GetEvent( "ValueChanged" );
			if (widgetChangedEvent != null)
			{
				widgetChangedEvent.AddEventHandler(m_Widget, new EventHandler(WidgetValueChanged));
			}

			EventInfo controlChangedEvent = m_Control.GetType().GetEvent(m_ControlValueProperty.Name + "Changed");
			if (controlChangedEvent != null)
			{
				controlChangedEvent.AddEventHandler(m_Control, new EventHandler(ControlValueChanged));
			}
		}

		public void DisconnectEvents() {
			EventInfo widgetChangedEvent = m_Widget.GetType().GetEvent("ValueChanged");
			if (widgetChangedEvent != null) 
			{
				widgetChangedEvent.RemoveEventHandler(m_Widget, new EventHandler(WidgetValueChanged));
			}

			EventInfo controlChangedEvent = m_Control.GetType().GetEvent(m_ControlValueProperty.Name + "Changed");
			if (controlChangedEvent != null) 
			{
				controlChangedEvent.RemoveEventHandler(m_Control, new EventHandler(ControlValueChanged));
			}
		}

		public static string GetStandardWidgetProperty(Widget w) {
			if (w is WidgetToggle) 
			{
				return "Checked";
			}
			else if (w is WidgetText) 
			{
				return "String";
			}
			return "Value";
		}

		public static string GetStandardControlProperty(ControlBase ctrl) {
			return "Value";
		}

		public static WidgetBinding Bind(Widget widget, ControlBase ctrl) 
		{
			return Bind(widget, ctrl, GetStandardControlProperty(ctrl));
		}

		public static WidgetBinding Bind(Widget widget, System.Windows.Forms.Control control, string controlProp)
		{
			if (widget == null || control == null) {
				if (control != null) 
				{
					control.Enabled = false;
				}
				return null;
			}
			control.Enabled = true;
			return new WidgetBinding(widget, GetStandardWidgetProperty(widget), control, controlProp);
		}

	}
}
