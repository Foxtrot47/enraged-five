using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

using ragCore;
using ragWidgets;
using RSG.Base.Logging;

namespace ragWidgetPage
{
    /// <summary>
    /// Summary description for WidgetPage.
    /// </summary>
    public class WidgetPage : System.Windows.Forms.UserControl, IDockableView
    {
        public WidgetPage()
        {
            InitializeComponent();
        }

        public WidgetPage( String appName, IBankManager bankMgr, IKeyUpProcessor keyProcessor, TD.SandDock.DockControl dockControl, ILog log )
        {
            InitializeComponent();
            m_log = log;
            m_BankManager = bankMgr;
            m_KeyUpProcessor = keyProcessor;
            m_AppName = appName;
            m_DockControl = dockControl;

            if (dockControl != null)
            {
                dockControl.Closed += DockControl_Closed;
            }
        }

        private void InitializeComponent()
        {
            // 
            // WidgetPage
            // 
            this.Name = "WidgetPage";
            this.Size = new System.Drawing.Size( 596, 480 );
        }

        protected override void Dispose( bool disposing )
        {
            if ( disposing )
            {
                RemoveBindings();
            }
            base.Dispose( disposing );
        }

        /// <summary>
        /// This function should be used to identify your WidgetPage in DllInfo.PluginDataHash,  
        /// DllInfo.NameToGuidHash, DllInfo.GuidToNameHash.
        /// 
        /// Additionally, you should create a static string MyName to register your static 
        /// CreatePage function with WidgetPagePlugIn.m_pageNameToCreateDelMap.        
        /// </summary>
        /// <returns></returns>
        public virtual string GetMyName()
        {
            return "Widget Page";
        }

        #region Delegates
        public delegate WidgetPage CreatePageDel( String appName, IBankManager bankMgr, IKeyUpProcessor keyProcessor, TD.SandDock.DockControl dockControl, ILog log );
        #endregion

        #region Properties
        public WidgetGroupBase m_WidgetRoot;
        public IBankManager m_BankManager;
        public IKeyUpProcessor m_KeyUpProcessor;
        public WidgetPagePlugIn m_WidgetPagePlugIn;

        protected ILog Log
        {
            get { return m_log; }
        }
        #endregion

        #region Variables
        private List<ragWidgets.WidgetBinding> m_Bindings = new List<ragWidgets.WidgetBinding>();
        private List<WidgetGroupBase> m_RefGroups = new List<WidgetGroupBase>();
        
        protected TD.SandDock.DockControl m_DockControl;
        protected string m_AppName;

        private readonly ILog m_log;
        #endregion

        #region IDockableView Interface
        public TD.SandDock.DockControl DockControl
        {
            get
            {
                return m_DockControl;
            }
            set
            {
                if (m_DockControl != value)
                {
                    if (m_DockControl != null)
                    {
                        m_DockControl.Closed -= DockControl_Closed;
                    }

                    m_DockControl = value;

                    if (m_DockControl != null)
                    {
                        m_DockControl.Closed += DockControl_Closed;
                    }
                }
            }
        }

        public virtual System.Drawing.Bitmap GetBitmap()
        {
            return null;
        }
        
        public virtual string GetTabText()
        {
            return "Widget Page";
        }

        public virtual void ClearView()
        {
            RemoveBindings();
        }

        public virtual bool InitiallyFloating
        {
            get
            {
                return false;
            }
        }

        public virtual TD.SandDock.ContainerDockLocation InitialDockLocation
        {
            get
            {
                return TD.SandDock.ContainerDockLocation.Left;
            }
        }

        public virtual bool IsSingleInstance
        {
            get
            {
                return false;
            }
        }

        public virtual void UserPrefDirChanged( object sender, UserPrefDirChangedEventArgs e )
        {
            // nothing to do
        }
        #endregion

        #region State Data
        public virtual object GetStateData()
        {
            throw (new NotImplementedException( "Derived classes should override this" ));
        }

        public virtual void SetStateData( object o )
        {
            throw (new NotImplementedException( "Derived classes should override this" ));
        }
        #endregion

        #region Bindings
        public void RemoveBindings()
        {
            foreach ( ragWidgets.WidgetBinding b in m_Bindings )
            {
                b.DisconnectEvents();
            }
            m_Bindings.Clear();

            foreach ( WidgetGroupBase g in m_RefGroups )
            {
                g.DestroyEvent -= new EventHandler( DestroyGroupReference );

                // If we're removing binding because the widget was destroyed 
                // (outside of this WidgetPage's control)
                // then don't release the group, because the group may not exist
                // anymore on the other end.
                // TODO: Remove me once we have better widget ID tracking on the game side.
                if (!sm_ExternallyDestroyed)
                {
                    g.ReleaseGroup();
                }
            }
            m_RefGroups.Clear();
        }

        protected bool Bind( ragCore.Widget widget, ragUi.ControlBase control )
        {
            ragWidgets.WidgetBinding binding = ragWidgets.WidgetBinding.Bind( widget, control );
            if ( binding != null )
            {
                m_Bindings.Add( binding );
                return true;
            }
            return false;
        }

        protected bool Bind( string path, ragUi.ControlBase control )
        {
            if ( m_WidgetRoot != null )
            {
                return Bind( m_WidgetRoot.FindFirstWidgetFromPath( path ), control );
            }
            else
            {
                return Bind( m_BankManager.FindFirstWidgetFromPath( path ), control );
            }
        }

        protected bool Bind( WidgetGroup group, string name, ragUi.ControlBase control )
        {
            return Bind( group.FindFirstWidgetFromPath( name ), control );
        }

        protected void ReferenceGroup( string path )
        {
            WidgetGroupBase group;
            if ( m_WidgetRoot != null )
            {
                group = m_WidgetRoot.FindFirstWidgetFromPath( path ) as WidgetGroupBase;
            }
            else
            {
                group = m_BankManager.FindFirstWidgetFromPath( path ) as WidgetGroupBase;
            }
            if ( group != null )
            {
                group.DestroyEvent += new EventHandler( DestroyGroupReference );
                group.AddRefGroup();
                m_RefGroups.Add( group );
            }
        }

        private static bool sm_ExternallyDestroyed = false;

        protected void DestroyGroupReference( object sender, System.EventArgs e )
        {
            m_RefGroups.Remove( sender as WidgetGroupBase );
            // need to rebind controls, since current bindings may no longer be valid
            sm_ExternallyDestroyed = true;
            m_WidgetPagePlugIn.RebuildAllPanels();
            sm_ExternallyDestroyed = false;
        }

        protected bool Bind( WidgetGroup group, string name, System.Windows.Forms.Control control, string prop )
        {
            ragWidgets.WidgetBinding binding;
            binding = ragWidgets.WidgetBinding.Bind( group.FindFirstWidgetFromPath( name ), control, prop );
            if ( binding != null )
            {
                m_Bindings.Add( binding );
                return true;
            }
            return false;
        }

        protected bool Bind( string path, System.Windows.Forms.Control control, string prop )
        {
            ragWidgets.WidgetBinding binding;
            if ( m_WidgetRoot != null )
            {
                binding = ragWidgets.WidgetBinding.Bind( m_WidgetRoot.FindFirstWidgetFromPath( path ), control, prop );
            }
            else
            {
                binding = ragWidgets.WidgetBinding.Bind( m_BankManager.FindFirstWidgetFromPath( path ), control, prop );
            }
            if ( binding != null )
            {
                m_Bindings.Add( binding );
                return true;
            }
            return false;
        }

        protected void SetLabel( Label l, string str, Widget widget )
        {
            object widgetValue = null;
            if ( widget is ragWidgets.WidgetSliderFloat )
            {
                widgetValue = (widget as ragWidgets.WidgetSliderFloat).Value;
            }
            else if ( widget is ragWidgets.WidgetSliderInt )
            {
                widgetValue = (widget as ragWidgets.WidgetSliderInt).Value;
            }
            l.Text = String.Format( str, widgetValue );
        }
        #endregion

        #region Save/Load WidgetPage data
        /// <summary>
        /// Save data that we want restored the next time we run Rag.  It is highly recommended that derived
        /// classes add GetTabText() as a prefix to all keys added to data, especially when 
        /// DockControl.CloseAction is set to Dispose.  That way, saved data will be removed properly
        /// (and automatically) when the window is closed.
        /// </summary>
        /// <param name="data"></param>
        public virtual void SaveWidgetPageData( Dictionary<string, string> data )
        {
            if ( !this.IsSingleInstance )
            {
                data[GetTabText() + ".CloseAction"] = this.DockControl.CloseAction.ToString();
            }
        }

        /// <summary>
        /// Load data that we saved from the last time we ran Rag
        /// </summary>
        /// <param name="data"></param>
        public virtual void LoadWidgetPageData( Dictionary<string, string> data )
        {
            if ( !this.IsSingleInstance )
            {
                string closeAction;
                if ( data.TryGetValue( GetTabText() + ".CloseAction", out closeAction ) )
                {
                    this.DockControl.CloseAction
                        = (TD.SandDock.DockControlCloseAction)Enum.Parse( typeof( TD.SandDock.DockControlCloseAction ), closeAction );
                }
            }
        }
        #endregion

        #region Event Handlers
        /// <summary>
        /// Called when the dock control associated with this console is closed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DockControl_Closed(object sender, System.EventArgs e)
        {
            TD.SandDock.DockControl dockControl = sender as TD.SandDock.DockControl;
            if (dockControl != null && dockControl.CloseAction == TD.SandDock.DockControlCloseAction.Dispose)
            {
                // If the dock control is set to be disposed, remove the reference we are keeping for it.
                m_DockControl = null;
            }
        }
        #endregion // Event Handlers
    }
}
