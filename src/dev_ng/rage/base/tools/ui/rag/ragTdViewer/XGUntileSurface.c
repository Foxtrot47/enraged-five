#define STRICT
#include <windows.h>
#include "d3d9types.h"

VOID WINAPI XGUntileSurface(
  VOID *pDestination,
  UINT RowPitch,
  CONST POINT *pPoint,
  CONST VOID *pSource,
  UINT Width,
  UINT Height,
  CONST RECT *pRect,
  UINT TexelPitch
);

__declspec(dllexport) void UntileSurface(
	void *pDestination,
	unsigned RowPitch,
	const void *pSource,
	unsigned Width,
	unsigned Height,
	unsigned TexelPitch)
{
	POINT origin = {0,0};
	RECT rect = {0,0,Width,Height};
	XGUntileSurface(pDestination,RowPitch,&origin,pSource,Width,Height,
		&rect,TexelPitch);
}
