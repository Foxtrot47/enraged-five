using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;

using EV.Windows.Forms;

namespace ragTdViewer
{
	/// <summary>
	/// Summary description for TdViewer.
	/// </summary>
	public class TdViewer : System.Windows.Forms.UserControl
	{
		private System.Windows.Forms.ListView listView1;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Splitter splitter1;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.PictureBox pictureBox1;
		private System.Windows.Forms.Splitter splitter2;
		private System.Windows.Forms.ColumnHeader textureName;
		private System.Windows.Forms.ColumnHeader width;
		private System.Windows.Forms.ColumnHeader height;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.ContextMenu m_ContextMenu_ListView;
		private System.Windows.Forms.MenuItem m_MenuItem_ViewLargeIcon;
		private System.Windows.Forms.MenuItem m_MenuItem_ViewSmallIcon;
		private System.Windows.Forms.MenuItem m_MenuItem_ViewDetails;
		private System.Windows.Forms.ColumnHeader mipCount;
		private System.Windows.Forms.ColumnHeader textureType;
		private System.Windows.Forms.ColumnHeader clampS;
		private System.Windows.Forms.ColumnHeader clampT;
		private System.Windows.Forms.ContextMenu m_ContextMenu_Picture;
		private System.Windows.Forms.MenuItem m_MenuItem_Copy;
		private System.Windows.Forms.ColumnHeader mipBias;
		private System.Windows.Forms.ColumnHeader memory;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public TdViewer()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();


			m_CurrentMenuItemView=m_MenuItem_ViewDetails;
			m_CurrentMenuItemView.Checked=true;
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if( components != null )
					components.Dispose();
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.listView1 = new System.Windows.Forms.ListView();
			this.textureName = new System.Windows.Forms.ColumnHeader();
			this.width = new System.Windows.Forms.ColumnHeader();
			this.height = new System.Windows.Forms.ColumnHeader();
			this.memory = new System.Windows.Forms.ColumnHeader();
			this.textureType = new System.Windows.Forms.ColumnHeader();
			this.mipCount = new System.Windows.Forms.ColumnHeader();
			this.mipBias = new System.Windows.Forms.ColumnHeader();
			this.clampS = new System.Windows.Forms.ColumnHeader();
			this.clampT = new System.Windows.Forms.ColumnHeader();
			this.m_ContextMenu_ListView = new System.Windows.Forms.ContextMenu();
			this.m_MenuItem_ViewLargeIcon = new System.Windows.Forms.MenuItem();
			this.m_MenuItem_ViewSmallIcon = new System.Windows.Forms.MenuItem();
			this.m_MenuItem_ViewDetails = new System.Windows.Forms.MenuItem();
			this.label1 = new System.Windows.Forms.Label();
			this.splitter1 = new System.Windows.Forms.Splitter();
			this.panel1 = new System.Windows.Forms.Panel();
			this.panel2 = new System.Windows.Forms.Panel();
			this.m_ContextMenu_Picture = new System.Windows.Forms.ContextMenu();
			this.m_MenuItem_Copy = new System.Windows.Forms.MenuItem();
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			this.splitter2 = new System.Windows.Forms.Splitter();
			this.panel1.SuspendLayout();
			this.panel2.SuspendLayout();
			this.SuspendLayout();
			// 
			// listView1
			// 
			this.listView1.AllowDrop = true;
			this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
																						this.textureName,
																						this.width,
																						this.height,
																						this.memory,
																						this.textureType,
																						this.mipCount,
																						this.mipBias,
																						this.clampS,
																						this.clampT});
			this.listView1.ContextMenu = this.m_ContextMenu_ListView;
			this.listView1.Dock = System.Windows.Forms.DockStyle.Top;
			this.listView1.Location = new System.Drawing.Point(0, 0);
			this.listView1.Name = "listView1";
			this.listView1.Size = new System.Drawing.Size(752, 272);
			this.listView1.TabIndex = 0;
			this.listView1.View = System.Windows.Forms.View.Details;
			this.listView1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.listView1_MouseDown);
			this.listView1.DragDrop += new System.Windows.Forms.DragEventHandler(this.listView1_DragDrop);
			this.listView1.DragEnter += new System.Windows.Forms.DragEventHandler(this.listView1_DragEnter);
			this.listView1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.listView1_MouseMove);
			this.listView1.SelectedIndexChanged += new System.EventHandler(this.listView1_SelectedIndexChanged);
			// 
			// textureName
			// 
			this.textureName.Text = "Texture Name";
			this.textureName.Width = 296;
			// 
			// width
			// 
			this.width.Text = "Width";
			// 
			// height
			// 
			this.height.Text = "Height";
			// 
			// memory
			// 
			this.memory.Text = "Memory";
			this.memory.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.memory.Width = 51;
			// 
			// textureType
			// 
			this.textureType.Text = "Type";
			this.textureType.Width = 52;
			// 
			// mipCount
			// 
			this.mipCount.Text = "# Mips";
			this.mipCount.Width = 59;
			// 
			// mipBias
			// 
			this.mipBias.Text = "Mip Bias";
			// 
			// clampS
			// 
			this.clampS.Text = "Clamp S";
			this.clampS.Width = 51;
			// 
			// clampT
			// 
			this.clampT.Text = "Clamp T";
			// 
			// m_ContextMenu_ListView
			// 
			this.m_ContextMenu_ListView.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																								   this.m_MenuItem_ViewLargeIcon,
																								   this.m_MenuItem_ViewSmallIcon,
																								   this.m_MenuItem_ViewDetails});
			// 
			// m_MenuItem_ViewLargeIcon
			// 
			this.m_MenuItem_ViewLargeIcon.Enabled = false;
			this.m_MenuItem_ViewLargeIcon.Index = 0;
			this.m_MenuItem_ViewLargeIcon.Text = "&Large Icon View";
			this.m_MenuItem_ViewLargeIcon.Click += new System.EventHandler(this.menuItem1_Click);
			// 
			// m_MenuItem_ViewSmallIcon
			// 
			this.m_MenuItem_ViewSmallIcon.Enabled = false;
			this.m_MenuItem_ViewSmallIcon.Index = 1;
			this.m_MenuItem_ViewSmallIcon.Text = "&Small Icon View";
			this.m_MenuItem_ViewSmallIcon.Click += new System.EventHandler(this.menuItem2_Click);
			// 
			// m_MenuItem_ViewDetails
			// 
			this.m_MenuItem_ViewDetails.Index = 2;
			this.m_MenuItem_ViewDetails.Text = "&Detail View";
			this.m_MenuItem_ViewDetails.Click += new System.EventHandler(this.menuItem3_Click);
			// 
			// label1
			// 
			this.label1.Dock = System.Windows.Forms.DockStyle.Top;
			this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label1.Location = new System.Drawing.Point(0, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(752, 32);
			this.label1.TabIndex = 1;
			this.label1.Text = "Texture Dict Name";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// splitter1
			// 
			this.splitter1.Dock = System.Windows.Forms.DockStyle.Top;
			this.splitter1.Enabled = false;
			this.splitter1.Location = new System.Drawing.Point(0, 32);
			this.splitter1.Name = "splitter1";
			this.splitter1.Size = new System.Drawing.Size(752, 3);
			this.splitter1.TabIndex = 2;
			this.splitter1.TabStop = false;
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.panel2);
			this.panel1.Controls.Add(this.splitter2);
			this.panel1.Controls.Add(this.listView1);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel1.Location = new System.Drawing.Point(0, 35);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(752, 549);
			this.panel1.TabIndex = 3;
			// 
			// panel2
			// 
			this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.panel2.ContextMenu = this.m_ContextMenu_Picture;
			this.panel2.Controls.Add(this.pictureBox1);
			this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel2.Location = new System.Drawing.Point(0, 275);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(752, 274);
			this.panel2.TabIndex = 3;
			this.panel2.SizeChanged += new System.EventHandler(this.panel2_Resize);
			// 
			// m_ContextMenu_Picture
			// 
			this.m_ContextMenu_Picture.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																								  this.m_MenuItem_Copy});
			// 
			// m_MenuItem_Copy
			// 
			this.m_MenuItem_Copy.Index = 0;
			this.m_MenuItem_Copy.Text = "&Copy Image To Clipboard";
			this.m_MenuItem_Copy.Click += new System.EventHandler(this.m_MenuItem_Copy_Click);
			// 
			// pictureBox1
			// 
			this.pictureBox1.ContextMenu = this.m_ContextMenu_Picture;
			this.pictureBox1.Location = new System.Drawing.Point(0, 0);
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new System.Drawing.Size(520, 240);
			this.pictureBox1.TabIndex = 1;
			this.pictureBox1.TabStop = false;
			// 
			// splitter2
			// 
			this.splitter2.Dock = System.Windows.Forms.DockStyle.Top;
			this.splitter2.Location = new System.Drawing.Point(0, 272);
			this.splitter2.Name = "splitter2";
			this.splitter2.Size = new System.Drawing.Size(752, 3);
			this.splitter2.TabIndex = 2;
			this.splitter2.TabStop = false;
			// 
			// TdViewer
			// 
			this.AllowDrop = true;
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.splitter1);
			this.Controls.Add(this.label1);
			this.Name = "TdViewer";
			this.Size = new System.Drawing.Size(752, 584);
			this.panel1.ResumeLayout(false);
			this.panel2.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		ImageList m_BigImageList=new ImageList();
		ImageList m_SmallImageList=new ImageList();
		ListViewSortManager m_SortMgr;
		
		public void Init(string directoryToLoad)
		{
			Type[] comparers=new Type [] 
			{
					typeof(ListViewTextCaseInsensitiveSort),
					typeof(ListViewInt32Sort),
					typeof(ListViewInt32Sort),
					typeof(ListViewKbSort),
					typeof(ListViewTextCaseInsensitiveSort),
					typeof(ListViewInt32Sort),
					typeof(ListViewDoubleSort),
					typeof(ListViewTextCaseInsensitiveSort),
					typeof(ListViewTextCaseInsensitiveSort)
			};
			m_SortMgr = new ListViewSortManager(listView1,comparers,0,SortOrder.Ascending);

			LoadTextureDictionary(directoryToLoad);
		}

		public void LoadTextureDictionary(string dictionaryName)
		{
			pictureBox1.Image=null;
			m_SortMgr.SortEnabled=false;

			m_ImageIndex=0;
			m_BigImageList.Images.Clear();
			m_BigImageList.ImageSize=new Size(64,64);
			m_SmallImageList.Images.Clear();
			m_SmallImageList.ImageSize=new Size(16,16);

			LoadDictionary(dictionaryName);

			SetImageLists();

			m_SortMgr.SortEnabled=true;
		}

		void SetImageLists()
		{
			foreach (ListViewItem item in listView1.Items)
			{
				Texture texture = m_Textures[item.Text] as Texture;
				m_BigImageList.Images.Add(new Bitmap(texture.Bitmap,new Size(48,48)));
				m_SmallImageList.Images.Add(new Bitmap(texture.Bitmap,new Size(32,32)));
			}

			listView1.LargeImageList=m_BigImageList;
			listView1.SmallImageList=m_SmallImageList;
			m_MenuItem_ViewSmallIcon.Enabled=true;	
			m_MenuItem_ViewLargeIcon.Enabled=true;	
			listView1.Invalidate();
		}

		void LoadDictionary(string dictName)
		{
			m_Textures.Clear();
			listView1.Items.Clear();

			label1.Text="loading...";
			TXDLoader loader = new TXDLoader(dictName,new AddTextureDel(AddTexture));
			label1.Text=dictName + " (num items: " + listView1.Items.Count + ")";
		}

		private void ShowImage(ListView.SelectedIndexCollection indicies)
		{
			if (indicies.Count==1)
			{
				string textureName=listView1.Items[indicies[0]].Text;
				Texture texture=m_Textures[textureName] as Texture;
	
				FitImage(texture.Bitmap);

				pictureBox1.Image=texture.Bitmap;
			}
			else
				pictureBox1.Image=null;
		}

		private void FitImage(Image image)
		{
			int newWidth=image.Size.Width;
			int newHeight=image.Size.Height;
			if (panel2.Size.Width<image.Size.Width)
			{
				float newRatio=(float)panel2.Size.Width/(float)image.Size.Width;
				newHeight=(int)(newRatio*(float)image.Size.Height);
				if (newHeight>panel2.Size.Height)
				{
					newRatio*=(float)panel2.Size.Height/(float)newHeight;
					newWidth=(int)(newRatio*(float)image.Size.Width);
					newHeight=panel2.Size.Height;
				}
				else
					newWidth=(int)(newRatio*(float)image.Size.Width);
			}
			else if (panel2.Size.Height<image.Size.Height)
			{
				float newRatio=(float)panel2.Size.Height/(float)image.Size.Height;
				newWidth=(int)(newRatio*(float)image.Size.Width);
				if (newWidth>panel2.Size.Width)
				{
					newRatio*=(float)panel2.Size.Width/(float)newWidth;
					newHeight=(int)(newRatio*(float)image.Size.Height);
					newWidth=panel2.Size.Width;
				}
				else 
					newHeight=(int)(newRatio*(float)image.Size.Height);
			}

			pictureBox1.SizeMode=PictureBoxSizeMode.StretchImage;
			pictureBox1.Size=new Size(newWidth,newHeight);
		}

		bool mouseDown=false;
		private Hashtable m_Textures=new Hashtable();

		private void listView1_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			ShowImage(listView1.SelectedIndices);
		}

		private void listView1_DragDrop(object sender, System.Windows.Forms.DragEventArgs e)
		{
			string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);
			foreach (string file in files)
			{
				if (Path.GetExtension(file)==".wtd")
					LoadTextureDictionary(file);
					
					//listView1.Items.Add(new ListViewItem(file,0));
			}
				
			mouseDown = false ;
		}

		private void listView1_DragEnter(object sender, System.Windows.Forms.DragEventArgs e)
		{
			if (e.Data.GetDataPresent(DataFormats.FileDrop)) 
				e.Effect = DragDropEffects.Copy;
			else
				e.Effect = DragDropEffects.None; 
		}

		private void listView1_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			mouseDown=true;
		}

		private void listView1_MouseMove(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			if ( ! mouseDown ) return ;
			if ( e.Button == MouseButtons.Right ) return ;

			string str = GetItemText() ;
			if ( str == "" ) return ;

			//listView1.DoDragDrop(str , DragDropEffects.Copy | DragDropEffects.Move ) ;
		}

		public string GetItemText()
		{
			int nTotalSelected = listView1.SelectedIndices.Count;
			if ( nTotalSelected <= 0 ) return "";
			IEnumerator selCol = listView1.SelectedItems.GetEnumerator();
			selCol.MoveNext() ;
			ListViewItem lvi = (ListViewItem)selCol.Current;
			string mDir = "";
			for ( int i=0; i < lvi.SubItems.Count;i++)
				mDir += lvi.SubItems[i].Text +",";

			mDir = mDir.Substring(0,mDir.Length-1);
			return mDir ;
		}

		int m_ImageIndex=0;
	
		Bitmap AddTexture(string textureName,int width,int height,uint memoryInBytes,TextureType type,int mipCount,float mipBias,bool clampS,bool clampT)
		{
			Texture texture=new Texture(textureName,width,height);
			m_Textures.Add(textureName,texture);

			float memoryInKB=memoryInBytes/1024;

			ListViewItem item=new ListViewItem(textureName,m_ImageIndex++);
			item.SubItems.Add(width.ToString());
			item.SubItems.Add(height.ToString());
			item.SubItems.Add(memoryInKB.ToString("#,###") + " KB");
			item.SubItems.Add(type.ToString());
			item.SubItems.Add(mipCount.ToString());
			item.SubItems.Add(mipBias.ToString());
			item.SubItems.Add(clampS.ToString());
			item.SubItems.Add(clampT.ToString());
			listView1.Items.Add(item);

			return texture.Bitmap;
		}

		MenuItem m_CurrentMenuItemView;

		private void SetView(MenuItem item,View view)
		{
			m_CurrentMenuItemView.Checked=false;
			listView1.View=view;
			m_CurrentMenuItemView=item;
			m_CurrentMenuItemView.Checked=true;
		}

		private void menuItem1_Click(object sender, System.EventArgs e)
		{
			SetView(m_MenuItem_ViewLargeIcon,View.LargeIcon);
		}

		private void menuItem2_Click(object sender, System.EventArgs e)
		{
			SetView(m_MenuItem_ViewSmallIcon,View.SmallIcon);
		}

		private void menuItem3_Click(object sender, System.EventArgs e)
		{
			SetView(m_MenuItem_ViewDetails,View.Details);
		}

		private void panel2_Resize(object sender, System.EventArgs e)
		{
			if (pictureBox1.Image!=null)
				FitImage(pictureBox1.Image);
		}

		private void m_MenuItem_Copy_Click(object sender, System.EventArgs e)
		{
			CopyToClipboard();
		}

		public void CopyToClipboard()
		{
			if (pictureBox1.Image!=null)
				Clipboard.SetDataObject(pictureBox1.Image);				
		}
	}

	class Texture
	{
		string m_TextureName;
		Bitmap m_Bitmap;

		public Bitmap Bitmap
		{
			get 
			{
				return m_Bitmap;
			}
		}

		public Texture(string textureName,int width,int height)
		{
			m_TextureName=textureName;
			m_Bitmap=new Bitmap(width,height,PixelFormat.Format32bppArgb);
		}
	}
}
