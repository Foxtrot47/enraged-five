using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;

namespace tester
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class Form1 : System.Windows.Forms.Form
	{
		private ragTdViewer.TdViewer tdViewer1;
		private System.Windows.Forms.MainMenu mainMenu1;
		private System.Windows.Forms.MenuItem menuItem1;
		private System.Windows.Forms.OpenFileDialog openFileDialog1;
		private System.Windows.Forms.MenuItem menuItem2;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public Form1()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(Form1));
			this.tdViewer1 = new ragTdViewer.TdViewer();
			this.mainMenu1 = new System.Windows.Forms.MainMenu();
			this.menuItem1 = new System.Windows.Forms.MenuItem();
			this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
			this.menuItem2 = new System.Windows.Forms.MenuItem();
			this.SuspendLayout();
			// 
			// tdViewer1
			// 
			this.tdViewer1.AllowDrop = true;
			this.tdViewer1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tdViewer1.Location = new System.Drawing.Point(0, 0);
			this.tdViewer1.Name = "tdViewer1";
			this.tdViewer1.Size = new System.Drawing.Size(792, 653);
			this.tdViewer1.TabIndex = 0;
			// 
			// mainMenu1
			// 
			this.mainMenu1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					  this.menuItem1,
																					  this.menuItem2});
			// 
			// menuItem1
			// 
			this.menuItem1.Index = 0;
			this.menuItem1.Shortcut = System.Windows.Forms.Shortcut.CtrlO;
			this.menuItem1.Text = "&Open...";
			this.menuItem1.Click += new System.EventHandler(this.menuItem1_Click);
			// 
			// openFileDialog1
			// 
			this.openFileDialog1.Filter = "Texture Dictionary Files (*.wtd; *.xtd; *.ctd)|*.wtd;*.xtd;*.ctd|Windows Texture Dictionary Fi" +
				"les (*.wtd)|*.wtd|Xenon Texture Dictionary Files (*.xtd)|*.xtd|PS3 Texture Dictionary Files (*.ctd)|*.ctd|All Files (*.*)|*" +
				".*";
			// 
			// menuItem2
			// 
			this.menuItem2.Index = 1;
			this.menuItem2.Shortcut = System.Windows.Forms.Shortcut.CtrlC;
			this.menuItem2.Text = "&Copy Image (To Clipboard)";
			this.menuItem2.Click += new System.EventHandler(this.menuItem2_Click);
			// 
			// Form1
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(792, 653);
			this.Controls.Add(this.tdViewer1);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Menu = this.mainMenu1;
			this.Name = "Form1";
			this.Text = "Texture Dictionary Viewer";
			this.Load += new System.EventHandler(this.Form1_Load);
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new Form1());
		}

		private void Form1_Load(object sender, System.EventArgs e)
		{
			
			String[] args = Environment.GetCommandLineArgs();
			string dictName=null;
			if (args.Length <= 1)
			{
				DialogResult result=DialogResult.No;
			
				result=openFileDialog1.ShowDialog();
				if (result!=DialogResult.OK)
				{
					Application.Exit();
					return;
				}

				dictName=openFileDialog1.FileName;
			}
			else
				dictName=args[1];
			
			tdViewer1.Init(dictName);

			
		}

		private void menuItem1_Click(object sender, System.EventArgs e)
		{
			DialogResult result=openFileDialog1.ShowDialog();
			if (result==DialogResult.OK)
			{
				tdViewer1.LoadTextureDictionary(openFileDialog1.FileName);
			}

		}

		private void menuItem2_Click(object sender, System.EventArgs e)
		{
			tdViewer1.CopyToClipboard();
		}
	}
}
