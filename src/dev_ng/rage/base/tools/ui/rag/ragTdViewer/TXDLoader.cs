using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Diagnostics;

using System.Security;
using System.Runtime.InteropServices;

namespace ragTdViewer
{
	public enum TextureType
	{
		DXT1,
		DXT5,
		A8R8G8B8
	}

	public delegate Bitmap AddTextureDel(string textureName,int width,int height,uint memoryInBytes,TextureType type,int mipCount,float mipBias,bool clampS,bool clampT);

	public sealed class Tiler 
	{
		[DllImport("XGUntileSurface.dll", SetLastError=true)]
		public static extern bool UntileSurface(byte[] pDestination,int RowPitch,byte[] pSource,
			int Width,int Height,int TexelPitch);
	}

	public class TXDLoader
	{
		private byte[] m_data;

		private long m_offset, m_length;

		private bool m_swap;

		private bool m_xenon;

		private bool m_ps3;
			
		private uint Swap(uint x)
		{
			if (m_swap)
			{
				return (x >> 24) | (x << 24) | ((x >> 8) & 0xFF00) | ((x << 8) & 0xFF0000);
			}
			else
				return x;
		}

		private int Swap(int x)
		{
			return (int)Swap((uint)x);
		}

		private ushort Swap(ushort x)
		{
			if (m_swap)
			{
				return (ushort)((x >> 8) | (x << 8));
			}
			else
				return x;
		}

		private uint ReadUInt32()
		{
			uint v = m_data[m_offset++];
			v |= (uint)(m_data[m_offset++] << 8);
			v |= (uint)(m_data[m_offset++] << 16);
			v |= (uint)(m_data[m_offset++] << 24);
			return Swap(v);
		}

		private ushort ReadUInt16()
		{
			ushort v = m_data[m_offset++];
			v |= (ushort)(m_data[m_offset++] << 8);
			return Swap(v);
		}

		private int ReadInt32()
		{
			return (int) ReadUInt32();
		}

		private byte ReadByte()
		{
			return m_data[m_offset++];
		}

		private float ReadSingle()
		{
			return ReadInt32();
		}

		private char ReadChar()
		{
			return (char) m_data[m_offset++];
		}

		private byte[] ReadBytes(int count)
		{
			Byte[] result = new Byte[count];
			Array.Copy(m_data,m_offset,result,0,count);
			m_offset += count;
			return result;
		}

		private void Seek(long offset)
		{
			m_offset = offset;
		}

		public TXDLoader(String filename,AddTextureDel addTexture)
		{
			//
			// TODO: Add constructor logic here
			//
			FileStream fs = new FileStream(filename,FileMode.Open,FileAccess.Read);
			m_xenon = (Path.GetExtension(filename) == ".xtd");
			m_ps3 = (Path.GetExtension(filename) == ".ctd");
			m_swap = m_xenon || m_ps3;
			m_length = fs.Length;
			m_data = new BinaryReader(fs).ReadBytes((int)m_length);
			m_offset = 0;
			Load(addTexture);
		}

		static int BlueOf(ushort rgb) 
		{
			int result = rgb & 31;
			return (result << 3) | (result >> 2);
		}

		static int GreenOf(ushort rgb)
		{
			int result = (rgb >> 5) & 61;
			return (result << 2) | (result >> 4);
		}

		static int RedOf(ushort rgb)
		{
			int result = (rgb >> 11) & 31;
			return (result << 3) | (result >> 2);
		}

		static uint get_bits(ref uint accum,byte[] src,ref uint offset,uint count)
		{
			uint result = 0;
			do 
			{
				if ((accum & 0x10000) != 0)
					accum = 0x100U | src[offset++];
				result = (result<<1) | ((accum & 0x80) >> 7);
				accum <<= 1;
			} while (--count != 0);
			return result;
		}

		static bool decompress(byte[] dest,uint destSize,byte[] src,uint srcSize)
		{
			uint accum = 0x10000;
			uint offset = 0;

			const int c_InitialWidth = 2;
			const int c_WidthStep = 2;

			const byte c_Magic_1 = 0xDC;
			const byte c_Magic_2 = 0xE0;

			if (src[0] != c_Magic_1 || src[1] != c_Magic_2)
				return false;
			uint srcOffset = 2;

			while (offset < destSize) {
				// Decode length (either literal count or window length minus one)
				uint length = 0;
				if (get_bits(ref accum,src,ref srcOffset,1) != 0)
					length = 1;
				else if (get_bits(ref accum,src,ref srcOffset,1) != 0) {	// 4 or 5-20
					if (get_bits(ref accum,src,ref srcOffset,1) != 0) {	// 5-20+
						uint width = c_InitialWidth + c_WidthStep;
						uint bias = 1 + (1 << c_InitialWidth);
						while (get_bits(ref accum,src,ref srcOffset,1) != 0) {
							bias += (1U<<(int)width);
							width += c_WidthStep;
						}
						length = bias + get_bits(ref accum,src,ref srcOffset,width);
					}
					else			// 4
						length = 4;
				}
				else if (get_bits(ref accum,src,ref srcOffset,1) != 0)
					length = 3;
				else
					length = 2;

				// Frame type:
				if (get_bits(ref accum,src,ref srcOffset,1) != 0) {		// window
					++length;			// min window length is two
					uint width = c_InitialWidth;
					uint bias = 1;
					while (get_bits(ref accum,src,ref srcOffset,1) != 0) {
						bias += (1U<<(int)width);
						width += c_WidthStep;
					}
					uint backup = bias + get_bits(ref accum,src,ref srcOffset,width);
					if (backup > offset)
						return false;
					do {
						if (offset >= destSize)
							return false;
						dest[offset] = dest[offset-backup];
						++offset;
					} while (--length != 0);
				}
				else {					// literals
					do {
						if (offset >= destSize)
							return false;
						dest[offset++] = src[srcOffset++];
					} while (--length != 0);
				}
			}

			if (offset != destSize)
				return false;
			else
				return true;
		}

		void Load(AddTextureDel addTexture)
		{
			// Resource header (in paging/rscbuilder.h)
			uint Magic = ReadUInt32();
			int Version = ReadInt32();
			uint VirtualAddr = ReadUInt32();
			uint PhysicalAddr = ReadUInt32();

			uint VirtualSize = ReadUInt32();
			uint PhysicalSize = ReadUInt32();
			uint VirtualChunkSize = ReadUInt32();
			uint PhysicalChunkSize = ReadUInt32();
			uint CompressedSize = 0;

			uint Size = VirtualSize + PhysicalSize;

			Trace.Assert(Magic == 0x435352,"Not a resource file.");
			Trace.Assert(Version == 6,"Bad version in txd");

			if (CompressedSize != 0)
			{
				uint slop = (uint)m_data.Length - CompressedSize - 32;
				Byte[] dest = new Byte[Size];
				Byte[] srcData = new Byte[CompressedSize];
				Array.Copy(m_data,slop+32,srcData,0,CompressedSize);
				Trace.Assert(decompress(dest,Size,srcData,CompressedSize));
				m_data = dest;
			}
			else 
			{
				Byte[] temp = new Byte[Size];
				Array.Copy(m_data,32,temp,0,Size);
				m_data = temp;
			}
			m_offset = 0;

			// pgDictionary object (in paging/dictionary.h)
			// Contains two atArrays, which store pointer, count, and capactity each
			// Virtual part is after physical part in the file now.
			Seek(PhysicalSize);
			int DictionaryVptr = ReadInt32();
			int Parent = ReadInt32();
			int RefCount = ReadInt32();
			uint CodeOffset = ReadUInt32();
			int CodeCount = ReadUInt16();
			int CodeCapacity = ReadUInt16();

			uint TextureOffset = ReadUInt32();
			int TextureCount = ReadUInt16();
			int TextureCapacity = ReadUInt16();

			Trace.Assert(CodeCount == TextureCount);
			Trace.Assert(CodeCount == CodeCapacity);
			Trace.Assert(TextureCount == TextureCapacity);

			// Texture pointer array follows.  We do need these.
			uint[] offsets = new uint[TextureCount];
			Seek(PhysicalSize + TextureOffset - VirtualAddr);
			for (int i=0; i<TextureCount; i++)
			{
				offsets[i] = ReadUInt32();
			}

			// TODO: Do we keep the file open and read as necessary, or process everything now?
			for (int i=0; i<TextureCount; i++)
			{
				uint texOffset = PhysicalSize + offsets[i] - VirtualAddr;
				Seek(texOffset);

				// grcTexture base class, in grcore/texture.h
				int TextureVptr = ReadInt32();
				byte ResourceType = ReadByte();
				byte LayerCount = ReadByte();
				ushort TexRefCount = ReadUInt16();
				uint LastBind = ReadUInt32();
				Trace.Assert(ResourceType == 0);
				Trace.Assert(RefCount == 1);

				uint NameOffset = 0;
				uint DeviceTextureOffset = 0;
				uint TextureFormat = 0;
				uint BackingStoreOffset = 0;
				ushort Width = 0;
				ushort Height = 0;
				int MipCount = 0;
				TextureType type=TextureType.DXT1;
				if (m_ps3)
				{
					/* CellGcmTexture:
						uint8_t format;
						uint8_t mipmap;
						uint8_t dimension;
						uint8_t cubemap;

						uint32_t remap;

						uint16_t width;
						uint16_t height;
						uint16_t depth;
						uint8_t location;
						uint8_t _padding;

						uint32_t pitch;
						uint32_t offset;*/
					// m_Name
					// m_Bits
					TextureFormat = ReadByte();
					if (TextureFormat == 6)
						type = TextureType.DXT1;
					else if (TextureFormat == 8)
						type = TextureType.DXT5;
					else
						Console.WriteLine("Unknown PS3 texture format");
					MipCount = ReadByte();
					Byte dimension = ReadByte();
					Byte isCube = ReadByte();

					uint remap = ReadUInt32();

					Width = ReadUInt16();
					Height = ReadUInt16();
					ushort Depth = ReadUInt16();
					Byte location = ReadByte();
					Byte padding = ReadByte();
					uint Pitch = ReadUInt32();
					uint Offset = ReadUInt32();
					NameOffset = ReadUInt32();
					BackingStoreOffset = ReadUInt32();
				}
				else
				{
					// grcTexturePC class, in grcore/texturepc.h
					NameOffset = ReadUInt32();
					DeviceTextureOffset = ReadUInt32();
					Width = ReadUInt16();
					Height = ReadUInt16();
					TextureFormat = 0;
					MipCount = 0;
					bool IsCube = false;
					BackingStoreOffset = 0;

					if (m_xenon)
					{
						MipCount = ReadInt32();
					}
					else
					{
						TextureFormat = ReadUInt32();
						int MipStride = ReadUInt16();
						IsCube = ReadByte() != 0;
						MipCount = ReadByte();
						UInt32 ScaleR = ReadUInt32();
						UInt32 ScaleG = ReadUInt32();
						UInt32 ScaleB = ReadUInt32();
						UInt32 OfsR = ReadUInt32();
						UInt32 OfsG = ReadUInt32();
						UInt32 OfsB = ReadUInt32();
						UInt32 Next = ReadUInt32();
						BackingStoreOffset = ReadUInt32();
					}

					if (TextureFormat == 0x31545844) // DXT1
						type=TextureType.DXT1;
					else if (TextureFormat == 0x35545844)	// DXT5
						type=TextureType.DXT5;
					else if (TextureFormat == 21)
						type=TextureType.A8R8G8B8;
					else if (!m_xenon)
						Console.WriteLine("Unknown PC texture format");

				}
				String Name = "";
				if (NameOffset != 0)
				{
					// Name is located in the virtual part.
					Seek(PhysicalSize + NameOffset - VirtualAddr);
					// Name should be here
					char ch;
					while ((ch = ReadChar()) != 0)
						Name += ch;
				}
				else Name = "Unnamed." + DeviceTextureOffset;

				if (m_xenon)
				{
					Seek(PhysicalSize + DeviceTextureOffset - VirtualAddr);
					uint Common = ReadUInt32();
					uint Fence = ReadUInt32();
					if ((Common & 15) != 1) 
					{	// March XeDK or newer
						uint ReferenceCount = ReadUInt32();
						uint ReadFence = ReadUInt32();
						uint Identifier = ReadUInt32();
					}
					uint BaseFlush = ReadUInt32();
					uint MipFlush = ReadUInt32();
					uint Format0 = ReadUInt32();
					uint Format1 = ReadUInt32();
					BackingStoreOffset = Format1 & ~4095U;
					if ((Format1 & 63) == 18)
						type=TextureType.DXT1;
					else if ((Format1 & 63) == 20)
						type=TextureType.DXT5;
					else if ((Format1 & 63) == 6)
						type=TextureType.A8R8G8B8;
					else
						Console.WriteLine("Unknown Xenon texture format");
				}

				// figure out memory size:
				int numMips=MipCount;
				uint memoryInBytes=0;
				int w=Width;
				int h=Height;
				do 
				{
					switch (type)
					{
						case TextureType.DXT1:
							memoryInBytes+=(uint)(w*h/2);
							break;
						case TextureType.DXT5:
							memoryInBytes+=(uint)(w*h);
							break;
						case TextureType.A8R8G8B8:
							memoryInBytes+=(uint)(w*h*4);
							break;
					}
					w>>=1;
					h>>=1;
				} while (--numMips>0);
				
				Bitmap bmp = addTexture(Name,Width,Height,memoryInBytes,type,MipCount,0,false,false);
	
				Seek(BackingStoreOffset - PhysicalAddr);
				Color[] colors = new Color[4];
				if (type==TextureType.DXT1)	
				{
					Byte[] image = ReadBytes(Width*Height/2);
					Byte[] newimage;
					if (m_xenon)
					{
						newimage = new Byte[Width*Height/2];
						Tiler.UntileSurface(newimage,Width*2,image,Width/4,Height/4,8);
					}
					else
						newimage = image;
					int offset = 0;

					for (int r=0; r<Height; r+=4)
					{
						for (int c=0; c<Width; c+=4)
						{
							ushort anchor0, anchor1;
							uint bits;
							if (m_xenon)
							{
								anchor0 = (ushort)((newimage[offset+0]<<8) | (newimage[offset+1]));
								anchor1 = (ushort)((newimage[offset+2]<<8) | (newimage[offset+3]));
								bits = (uint)((newimage[offset+6]<<24) | (newimage[offset+7]<<16) | (newimage[offset+4]<<8) | newimage[offset+5]);
							}
							else
							{
								anchor0 = (ushort)((newimage[offset+1]<<8) | (newimage[offset+0]));
								anchor1 = (ushort)((newimage[offset+3]<<8) | (newimage[offset+2]));
								bits = (uint)((newimage[offset+7]<<24) | (newimage[offset+6]<<16) | (newimage[offset+5]<<8) | newimage[offset+4]);
							}
							offset += 8;
							if (anchor0 <= anchor1)
							{
								colors[0] = Color.FromArgb(RedOf(anchor0),GreenOf(anchor0),BlueOf(anchor0));
								colors[1] = Color.FromArgb(RedOf(anchor1),GreenOf(anchor1),BlueOf(anchor1));
								colors[2] = Color.FromArgb((RedOf(anchor0)+RedOf(anchor1))/2,(GreenOf(anchor0)+GreenOf(anchor1))/2,(BlueOf(anchor0)+BlueOf(anchor1))/2);
								colors[3] = Color.FromArgb(0,0,0,0);
							}
							else
							{
								colors[0] = Color.FromArgb(RedOf(anchor0),GreenOf(anchor0),BlueOf(anchor0));
								colors[1] = Color.FromArgb(RedOf(anchor1),GreenOf(anchor1),BlueOf(anchor1));
								colors[2] = Color.FromArgb((2*RedOf(anchor0)+RedOf(anchor1))/3,(2*GreenOf(anchor0)+GreenOf(anchor1))/3,(2*BlueOf(anchor0)+BlueOf(anchor1))/3);
								colors[3] = Color.FromArgb((RedOf(anchor0)+2*RedOf(anchor1))/3,(GreenOf(anchor0)+2*GreenOf(anchor1))/3,(BlueOf(anchor0)+2*BlueOf(anchor1))/3);
							}
							for (int r1=0; r1<4; r1++) 
							{
								for (int c1=0; c1<4; c1++)
								{
									bmp.SetPixel(c+c1,r+r1,colors[bits & 3]);
									bits >>= 2;
								}
							}
						}
					}
				}
				else if (type==TextureType.DXT5)
				{
					int[] alphalut = new int[8];
					Byte[] image = ReadBytes(Width*Height);
					Byte[] newimage;
					if (m_xenon)
					{
						newimage = new Byte[Width*Height];
						Tiler.UntileSurface(newimage,Width*4,image,Width/4,Height/4,16);
					}
					else
						newimage = image;

					int offset = 0;
					for (int r=0; r<Height; r+=4)
					{
						for (int c=0; c<Width; c+=4)
						{
							byte alpha0, alpha1;
							ulong alphabits = 0;
							if (m_xenon)
							{
								alpha0 = newimage[offset+1];
								alpha1 = newimage[offset+0];
								alphabits = (ulong)newimage[offset+3] | ((ulong)newimage[offset+2]<<8) |
									((ulong)newimage[offset+5]<<16) | ((ulong)newimage[offset+4]<<24) |
									((ulong)newimage[offset+7]<<32) | ((ulong)newimage[offset+6]<<40);
							}
							else
							{
								alpha0 = newimage[offset+0];
								alpha1 = newimage[offset+1];
								alphabits = (ulong)newimage[offset+2] | ((ulong)newimage[offset+3]<<8) |
									((ulong)newimage[offset+4]<<16) | ((ulong)newimage[offset+5]<<24) |
									((ulong)newimage[offset+6]<<32) | ((ulong)newimage[offset+7]<<40);
							}
							offset += 8;
							alphalut[0] = alpha0;
							alphalut[1] = alpha1;
							if (alpha0 <= alpha1)
							{
								alphalut[2] = (4*alpha0 + 1*alpha1) / 5;
								alphalut[3] = (3*alpha0 + 2*alpha1) / 5;
								alphalut[4] = (2*alpha0 + 3*alpha1) / 5;
								alphalut[5] = (1*alpha0 + 4*alpha1) / 5;
								alphalut[6] = 0;
								alphalut[7] = 255;
							}
							else
							{
								alphalut[2] = (6*alpha0 + 1*alpha1) / 7;
								alphalut[3] = (5*alpha0 + 2*alpha1) / 7;
								alphalut[4] = (4*alpha0 + 3*alpha1) / 7;
								alphalut[5] = (3*alpha0 + 4*alpha1) / 7;
								alphalut[6] = (2*alpha0 + 5*alpha1) / 7;
								alphalut[7] = (1*alpha0 + 6*alpha1) / 7;
							}

							ushort anchor0, anchor1;
							uint bits;
							if (m_xenon)
							{
								anchor0 = (ushort)((newimage[offset+0]<<8) | (newimage[offset+1]));
								anchor1 = (ushort)((newimage[offset+2]<<8) | (newimage[offset+3]));
								bits = (uint)((newimage[offset+6]<<24) | (newimage[offset+7]<<16) | (newimage[offset+4]<<8) | newimage[offset+5]);
							}
							else
							{
								anchor0 = (ushort)((newimage[offset+1]<<8) | (newimage[offset+0]));
								anchor1 = (ushort)((newimage[offset+3]<<8) | (newimage[offset+2]));
								bits = (uint)((newimage[offset+7]<<24) | (newimage[offset+6]<<16) | (newimage[offset+5]<<8) | newimage[offset+4]);
							}
							offset += 8;
							if (anchor0 <= anchor1)
							{
								colors[0] = Color.FromArgb(RedOf(anchor0),GreenOf(anchor0),BlueOf(anchor0));
								colors[1] = Color.FromArgb(RedOf(anchor1),GreenOf(anchor1),BlueOf(anchor1));
								colors[2] = Color.FromArgb((RedOf(anchor0)+RedOf(anchor1))/2,(GreenOf(anchor0)+GreenOf(anchor1))/2,(BlueOf(anchor0)+BlueOf(anchor1))/2);
								colors[3] = Color.FromArgb(0,0,0,0);
							}
							else
							{
								colors[0] = Color.FromArgb(RedOf(anchor0),GreenOf(anchor0),BlueOf(anchor0));
								colors[1] = Color.FromArgb(RedOf(anchor1),GreenOf(anchor1),BlueOf(anchor1));
								colors[2] = Color.FromArgb((2*RedOf(anchor0)+RedOf(anchor1))/3,(2*GreenOf(anchor0)+GreenOf(anchor1))/3,(2*BlueOf(anchor0)+BlueOf(anchor1))/3);
								colors[3] = Color.FromArgb((RedOf(anchor0)+2*RedOf(anchor1))/3,(GreenOf(anchor0)+2*GreenOf(anchor1))/3,(BlueOf(anchor0)+2*BlueOf(anchor1))/3);
							}
							for (int r1=0; r1<4; r1++) 
							{
								for (int c1=0; c1<4; c1++)
								{

									bmp.SetPixel(c+c1,r+r1,Color.FromArgb(
										alphalut[alphabits & 7],
										colors[bits & 3].R,
										colors[bits & 3].G,
										colors[bits & 3].B));
									bits >>= 2;
									alphabits >>= 3;
								}
							}
						}
					}
				}
				else if (type==TextureType.A8R8G8B8)
				{
					Byte[] image = ReadBytes(Width*Height*4);
					int offset = 0;
					Byte[] newimage = image;
					for (int r=0; r<Height; r++)
					{
						for (int c=0; c<Width; c++)
						{
							int blue = newimage[offset+0];
							int green = newimage[offset+1];
							int red = newimage[offset+2];
							int alpha = newimage[offset+3];
							offset += 4;
							bmp.SetPixel(c,r,Color.FromArgb(255,red,green,blue));
						}	
					}
				}
				else
					Console.WriteLine("Unhandled d3d format %d\n",TextureFormat);
			}
		}
	}
}
