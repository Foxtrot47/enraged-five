using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Xml;

using ragCore;

using ragKeyframeEditorControl;
using ragWidgets;

namespace ragKeyframeEditor
{
	/// <summary>
	/// Summary description for WidgetKeyframeEditor.
	/// </summary>
	public class WidgetKeyframeEditor : Widget
	{
        public WidgetKeyframeEditor(BankPipe pipe, uint id, String title, String memo, Color fillColor, bool readOnly)
            : base(pipe, id, title, memo, fillColor, readOnly)
        {
            // useful for debugging this plug-in:
            // Debugger.Break();

            m_KeyframeCurves = new List<KeyframeCurveEditor.RawCurve>( 4 );
            for ( int i = 0; i < 4; i++ )
            {
                m_KeyframeCurves.Add( new KeyframeCurveEditor.RawCurve() );
            }
            m_Labels = new String[4];
            m_Colors = new System.Drawing.Color[4];
            m_Colors[0] = System.Drawing.Color.Red;
            m_Colors[1] = System.Drawing.Color.Green;
            m_Colors[2] = System.Drawing.Color.Blue;
            m_Colors[3] = System.Drawing.Color.DarkGray;
        }

		public override int GetWidgetTypeGUID() {return GetStaticGUID();}
		public static int GetStaticGUID()		{return ComputeGUID('r','k','e','y');}

		public override void Serialize(XmlTextWriter writer,string path) {}
		public override void DeSerialize(XmlTextReader reader) {}
		public override bool AllowPersist {get {return false;}}

        /// <summary>
        /// Thread synchronisation object.
        /// </summary>
        private static object s_syncObj = new object();

        /// <summary>
        /// Reference count for number of times this object has been initialised.
        /// </summary>
        private static int s_refCount = 0;

        public static WidgetVisible ShowVisible(Widget widget, IWidgetView widgetView, Control parent, String hashKey, bool forceOpen, ToolTip tooltip)
        {
            WidgetKeyframeEditor widgetEditor = (WidgetKeyframeEditor)widget;
            WidgetKeyframeEditorVisible visible = new WidgetKeyframeEditorVisible( widgetView, widgetEditor, parent );
            widgetEditor.ConfigureControl( visible.Control as KeyframeEditor );
            WidgetVisualiser.WidgetShowVisibleSetup( widgetEditor, visible, hashKey );

            return visible;
		}

        public void ConfigureControl( KeyframeEditor editor )
        {
            editor.ConfigureEditor( m_Labels, m_Colors, m_XMin, m_XMax, m_YMin, m_YMax, m_XDelta, m_YDelta );
        }



		public override void UpdateRemote() 
		{
            List<KeyframeCurveEditor.Float5> networkKeyframeData = ConvertToFloat5();

			if (networkKeyframeData.Count == 0) {
				System.Diagnostics.Debug.WriteLine("Warning: No keyframes found in keyframe object");
				return;
			}
			if (networkKeyframeData.Count < 15) {
				BankRemotePacket p = new BankRemotePacket(Pipe);
                p.Begin(BankRemotePacket.EnumPacketType.USER_0, GetStaticGUID(), Id);
				p.Write_u32((uint)networkKeyframeData.Count);
				foreach ( KeyframeCurveEditor.Float5 key in networkKeyframeData )
                {
					p.Write_float(key.t);
					p.Write_float(key.x);
					p.Write_float(key.y);
					p.Write_float(key.z);
					p.Write_float(key.w);
				}
				p.Send();
			}
			else {
				BankRemotePacket begin = new BankRemotePacket(Pipe);
                begin.Begin(BankRemotePacket.EnumPacketType.USER_1, GetStaticGUID(), Id);
				begin.Write_u32((uint)networkKeyframeData.Count);
				begin.Send();

				int stepSize = 15;

				for(int group = 0; group < networkKeyframeData.Count; group += stepSize)
				{
					BankRemotePacket p = new BankRemotePacket(Pipe);
                    p.Begin(BankRemotePacket.EnumPacketType.USER_2, GetStaticGUID(), Id);
					p.Write_u32((uint)group);
					p.Write_u32((uint)System.Math.Min(group + stepSize, networkKeyframeData.Count));
					int end = System.Math.Min(networkKeyframeData.Count - group, stepSize);
					for(int i = 0; i < end; i++) {
						KeyframeCurveEditor.Float5 key = networkKeyframeData[group + i] as KeyframeCurveEditor.Float5;
						p.Write_float(key.t);
						p.Write_float(key.x);
						p.Write_float(key.y);
						p.Write_float(key.z);
						p.Write_float(key.w);
					}
					p.Send();
				}

				BankRemotePacket endpacket = new BankRemotePacket(Pipe);
                endpacket.Begin(BankRemotePacket.EnumPacketType.USER_3, GetStaticGUID(), Id);
				endpacket.Send();
			}
		}

		enum WidgetType
		{
			KF_VECTOR_X = 0,
			KF_VECTOR_Y,
			KF_VECTOR_Z,
			KF_VECTOR_W,
			KF_VECTOR2,
			KF_VECTOR3,
			KF_VECTOR4
		};

        public class ConfigChangedArgs
        {
            public string[] Labels;
            public System.Drawing.Color[] Colors;
            public float XMin;
            public float XMax;
            public float YMin;
            public float YMax;
            public float XDelta;
            public float YDelta;
        }

        private static bool RemoteHandler( BankRemotePacket packet, out String errorMessage )
        {
            // USER_0: Full set of keyframes
            // USER_1: Begin partial update
            // USER_2: Partial update contents
            // USER_3: End partial update
            bool handled = false;

            switch ( packet.BankCommand )
            {
                case BankRemotePacket.EnumPacketType.CREATE:
                    {

                        packet.Begin();
                        uint remoteParent = packet.Read_bkWidget();
                        string title = packet.Read_const_char();
                        string memo = packet.Read_const_char();
                        Color fillColor = Widget.DeserializeColor(packet.Read_const_char());
                        bool readOnly = packet.Read_bool();

                        Widget parent;
                        if (packet.BankPipe.RemoteToLocal(remoteParent, out parent, out errorMessage))
                        {
                            WidgetKeyframeEditor local = new WidgetKeyframeEditor(packet.BankPipe, packet.Id, title, memo, fillColor, readOnly);
                            local.m_Labels[0] = packet.Read_const_char();
                            local.m_Labels[1] = packet.Read_const_char();
                            local.m_Labels[2] = packet.Read_const_char();
                            local.m_Labels[3] = packet.Read_const_char();
                            local.m_YMin = packet.Read_float();
                            local.m_YMax = packet.Read_float();
                            local.m_Type = (WidgetType)packet.Read_u8();
                            packet.End();

                            local.FormatLabels();

                            packet.BankPipe.Associate(packet.Id, local);
                            parent.AddChild(local);
                            handled = true;
                        }
                    }
                    break;
                case BankRemotePacket.EnumPacketType.USER_0: // full set of keyframes
                    {
                        packet.Begin();

				        // find the local version of the the remote widget:
				        WidgetKeyframeEditor editor;
                        if (packet.BankPipe.RemoteToLocal(packet.Id, out editor, out errorMessage))
                        {

                            // get the number of keyframes
                            uint newSize = packet.Read_u32();

                            List<KeyframeCurveEditor.Float5> keyframeData = new List<KeyframeCurveEditor.Float5>((int)newSize);
                            for (int i = 0; i < newSize; i++)
                            {
                                KeyframeCurveEditor.Float5 key = new KeyframeCurveEditor.Float5();
                                key.t = packet.Read_float();
                                key.x = packet.Read_float();
                                key.y = packet.Read_float();
                                key.z = packet.Read_float();
                                key.w = packet.Read_float();
                                keyframeData.Add(key);
                            }
                            packet.End();
                            editor.ConvertFromFloat5(keyframeData);
                            editor.OnValueChanged();
                            handled = true;
                        }
                    }
                    break;
                case BankRemotePacket.EnumPacketType.USER_1: // begin partial update
                    {
                        packet.Begin();
                        
                        // find the local version of the the remote widget:
                        WidgetKeyframeEditor editor;
                        if (packet.BankPipe.RemoteToLocal(packet.Id, out editor, out errorMessage))
                        {

                            // get the number of keyframes
                            uint newSize = packet.Read_u32();

                            editor.m_KeyframeData = new List<KeyframeCurveEditor.Float5>((int)newSize);
                            for (int i = 0; i < newSize; i++)
                            {
                                editor.m_KeyframeData.Add(new KeyframeCurveEditor.Float5());
                            }

                            packet.End();
                            handled = true;
                        }
                    }
                    break;
                case BankRemotePacket.EnumPacketType.USER_2: // partial update data
                    {
                        packet.Begin();

				        // find the local version of the the remote widget:
				        WidgetKeyframeEditor editor;
                        if (packet.BankPipe.RemoteToLocal(packet.Id, out editor, out errorMessage))
                        {
                            uint begin = packet.Read_u32();
                            uint end = packet.Read_u32();

                            for (uint i = begin; i < end; i++)
                            {
                                KeyframeCurveEditor.Float5 key = new KeyframeCurveEditor.Float5();
                                key.t = packet.Read_float();
                                key.x = packet.Read_float();
                                key.y = packet.Read_float();
                                key.z = packet.Read_float();
                                key.w = packet.Read_float();
                                editor.m_KeyframeData[(int)i] = key;
                            }

                            packet.End();
                            handled = true;
                        }
                    }
                    break;
                case BankRemotePacket.EnumPacketType.USER_3: // end partial update
                    {
                        packet.Begin();

				        // find the local version of the the remote widget:
				        WidgetKeyframeEditor editor;
                        if (packet.BankPipe.RemoteToLocal(packet.Id, out editor, out errorMessage))
                        {
                            packet.End();
                            editor.ConvertFromFloat5(editor.m_KeyframeData);
                            editor.OnValueChanged();
                            editor.m_KeyframeData = null;
                            handled = true;
                        }
                    }
                    break;

                case BankRemotePacket.EnumPacketType.USER_4:
                    {
                        packet.Begin();

				        // find the local version of the the remote widget:
				        WidgetKeyframeEditor editor;
                        if (packet.BankPipe.RemoteToLocal(packet.Id, out editor, out errorMessage))
                        {
                            for (int i = 0; i < 4; i++)
                            {
                                editor.m_Labels[i] = packet.Read_const_char();
                                int a = packet.Read_u8();
                                int r = packet.Read_u8();
                                int g = packet.Read_u8();
                                int b = packet.Read_u8();
                                editor.m_Colors[i] = System.Drawing.Color.FromArgb(a, r, g, b);

                            }
                            editor.m_YMin = packet.Read_float();
                            editor.m_YMax = packet.Read_float();
                            editor.m_XMin = packet.Read_float();
                            editor.m_XMax = packet.Read_float();
                            editor.m_XDelta = packet.Read_float();
                            editor.m_YDelta = packet.Read_float();
                            packet.End();
                            editor.FormatLabels();

                            if (editor.ConfigurationChanged != null)
                            {
                                editor.ConfigurationChanged(editor, new GenericValueArgs(new ConfigChangedArgs()
                                {
                                    Labels = editor.m_Labels,
                                    Colors = editor.m_Colors,
                                    XMin = editor.m_XMin,
                                    XMax = editor.m_XMax,
                                    YMin = editor.m_YMin,
                                    YMax = editor.m_YMax,
                                    XDelta = editor.m_XDelta,
                                    YDelta = editor.m_YDelta
                                }
                                ));
                            }
                            handled = true;
                        }
                    }
                    break;
                default:
                    handled = Widget.RemoteHandlerBase(packet, GetStaticGUID(), out errorMessage);
                    break;
            }

            return handled;
        }
		
		public static void AddHandlers( BankRemotePacketProcessor packetProcessor )
		{
			packetProcessor.AddType(GetStaticGUID(),new BankRemotePacketProcessor.HandlerDel(RemoteHandler));

            lock (s_syncObj)
            {
                // Check whether the script editor is currently set up.
                if (s_refCount == 0)
                {
                    WidgetVisualiser.AddHandlerShowVisible(typeof(WidgetKeyframeEditor), WidgetKeyframeEditor.ShowVisible);
                    WidgetVisualiser.AddDefaultHandlerHideVisible(typeof(WidgetKeyframeEditor));
                }

                ++s_refCount;
            }
		}

        public static void RemoveHandlers( BankRemotePacketProcessor packetProcessor )
		{
            packetProcessor.RemoveType( GetStaticGUID() );


            lock (s_syncObj)
            {
                --s_refCount;

                if (s_refCount == 0)
                {
                    WidgetVisualiser.RemoveHandlers(typeof(WidgetKeyframeEditor));
                }
            }
		}

		// returns an array of Float5 keyframes, where each keyframe is a time value and N track values
        public List<KeyframeCurveEditor.Float5> ConvertToFloat5()
		{
            return KeyframeCurveEditor.ConvertRawCurvesToFloat5s(m_KeyframeCurves);
        }

        public void ConvertFromFloat5( List<KeyframeCurveEditor.Float5> data )
		{
            m_KeyframeCurves = KeyframeCurveEditor.ConvertFloat5sToRawCurves(data);
		}

		public List<KeyframeCurveEditor.RawCurve> Value
		{
			get {
				return m_KeyframeCurves;
			}
			set {
				m_KeyframeCurves = value;
				UpdateRemote();
                OnValueChanged();
            }
		}


		public void FormatLabels()
		{
			string[] defLabel = new string[4];
			switch(m_Type) 
			{
				case WidgetType.KF_VECTOR_X:
					defLabel[0] = m_Title;
					break;
				case WidgetType.KF_VECTOR_Y:
					defLabel[1] = m_Title;
					break;
				case WidgetType.KF_VECTOR_Z:
					defLabel[2] = m_Title;
					break;
				case WidgetType.KF_VECTOR_W:
					defLabel[3] = m_Title;
					break;
				case WidgetType.KF_VECTOR4:
					defLabel[0] = m_Title + " X";
					defLabel[1] = m_Title + " Y";
					defLabel[2] = m_Title + " Z";
					defLabel[3] = m_Title + " W";
					break;
				case WidgetType.KF_VECTOR3:
					defLabel[0] = m_Title + " X";
					defLabel[1] = m_Title + " Y";
					defLabel[2] = m_Title + " Z";
					break;
				case WidgetType.KF_VECTOR2:
					defLabel[0] = m_Title + " X";
					defLabel[1] = m_Title + " Y";
					break;
			}
			for(int i = 0; i < 4; i++) 
			{
				if (m_Labels[i] == "" && defLabel[i] != null)
				{
					m_Labels[i] = defLabel[i];
				}
			}
		}

		public string[] m_Labels;
		public System.Drawing.Color[] m_Colors;
        public float m_YMin;
		public float m_YMax;
		public float m_XMin;
		public float m_XMax;
		public float m_YDelta;
		public float m_XDelta;
		WidgetType m_Type;


        public List<KeyframeCurveEditor.Float5> m_KeyframeData; // Only used for partial data messages

		public List<KeyframeCurveEditor.RawCurve> m_KeyframeCurves; // array of arrays of Vector2 objects

        public event GenericValueEventHandler ConfigurationChanged;
	}
}
