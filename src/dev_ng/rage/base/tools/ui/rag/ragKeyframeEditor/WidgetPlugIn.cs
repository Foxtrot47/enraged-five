using System;
using ragCore;

namespace ragKeyframeEditor
{
	/// <summary>
	/// Summary description for WidgetPlugIn.
	/// </summary>
	public class WidgetPlugIn : ragCore.IWidgetPlugIn
	{
        public void AddHandlers( BankRemotePacketProcessor packetProcessor )
        {
            WidgetKeyframeEditor.AddHandlers( packetProcessor );
		}
        public void RemoveHandlers( BankRemotePacketProcessor packetProcessor )
        {
            WidgetKeyframeEditor.RemoveHandlers( packetProcessor );
		}
	}
}
