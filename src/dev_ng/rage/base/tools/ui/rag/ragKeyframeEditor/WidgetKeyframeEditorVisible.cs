using System;
using System.Windows.Forms;

using ragCore;
using ragKeyframeEditorControl;
using ragWidgets;

namespace ragKeyframeEditor
{
    /// <summary>
    /// Summary description for WidgetKeyframeEditorVisible.
    /// </summary>
    public class WidgetKeyframeEditorVisible : WidgetVisible
    {
        public WidgetKeyframeEditorVisible( IWidgetView widgetView, WidgetKeyframeEditor keyframeEditor, Control parent )
            : base( widgetView, parent, keyframeEditor )
        {
            m_Widget = keyframeEditor;
            m_Widget.ConfigurationChanged += widget_ConfigurationChanged;
            m_KeyframeEditor = new KeyframeEditor();
            m_KeyframeEditor.Enabled = !m_Widget.IsReadOnly && m_Widget.Enabled;

            parent.Controls.Add( m_KeyframeEditor );

            m_KeyframeEditor.ValueChanged += new EventHandler( control_ValueChanged );

            m_KeyframeEditor.Value = keyframeEditor.m_KeyframeCurves;
        }

        protected override void widget_ValueChanged( object sender, EventArgs e )
        {
            WidgetKeyframeEditor widget = (WidgetKeyframeEditor)sender;
            KeyframeEditor editor = this.Control as KeyframeEditor;
            editor.Value = widget.m_KeyframeCurves;
        }


        public void control_ValueChanged( object obj, System.EventArgs e )
        {
            KeyframeEditor ctl = (KeyframeEditor)obj;
            if ( ctl != null )
            {
                m_Widget.Value = ctl.Value;
            }
        }

        private void widget_ConfigurationChanged( object sender, GenericValueArgs args )
        {
            //string[] labels,System.Drawing.Color[] colors, float xMin, float xMax,float yMin, float yMax,float xDelta,float yDelta
            var t = (WidgetKeyframeEditor.ConfigChangedArgs)args.Value;
            m_KeyframeEditor.ConfigureEditor(t.Labels,t.Colors,t.XMin,t.XMax,t.YMin,t.YMax,t.XDelta,t.YDelta );
        }


        public override Control Control
        {
            get
            {
                return m_KeyframeEditor;
            }
        }

        public override Control DragDropControl
        {
            get
            {
                return null;
            }
        }

        public override Widget Widget
        {
            get
            {
                return m_Widget;
            }
        }

        public override void Destroy()
        {
            base.Destroy();
            m_Widget = null;
            m_KeyframeEditor.Destroy();
            m_KeyframeEditor = null;
        }

        public override void Remove()
        {
            m_Parent.Controls.Remove( Control );
            base.Remove();
        }

        private WidgetKeyframeEditor m_Widget;
        private KeyframeEditor m_KeyframeEditor;
    }
}
