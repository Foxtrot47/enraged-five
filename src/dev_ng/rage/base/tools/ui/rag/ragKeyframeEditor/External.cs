using System;

namespace ragKeyframeEditor
{
	/// <summary>
	/// Summary description for External.
	/// </summary>
	public class External
	{
		public External()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public static void AddStaticHandlers()
		{
			WidgetKeyframeEditor.AddHandlers();
		}

		public static void RemoveStaticHandlers()
		{
			WidgetKeyframeEditor.RemoveHandlers();
		}
	}
}
