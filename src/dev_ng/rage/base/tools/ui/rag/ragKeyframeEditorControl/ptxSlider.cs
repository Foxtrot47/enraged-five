using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace ragKeyframeEditorControl
{
    public partial class ptxSlider : TextBox
    {
        protected float m_Value = 0.0f;
        protected float m_LastValue = 0.0f;
        protected float m_ReDoValue = 0.0f;
        protected int m_Decimals = 3;
        protected float m_Inc = 0.1f;

        protected float m_MinValue = 0.0f;
        protected float m_MaxValue = 100.0f;
        
        protected bool m_MouseDown = false;
        protected bool m_ScrubMode = false;
        protected bool m_ModShift = false;
        protected bool m_ModControl = false;

        protected Point m_MouseAnchorPos;

        #region Properties

        public delegate void ptxSliderEventDelegate(ptxSlider slider);
        [Description("Occurs when user changes the value"), Category("Action")]
        public event ptxSliderEventDelegate OnValueChanged = null;


        [Description("Value"), Category("Data")]
        public float Value
        {
            get { return m_Value; }
            set 
            {
                float oldVal = m_Value;
                m_Value = value; 
                ValidateValue(); 
                DisplayValue();
                if (m_Value != oldVal)
                    if (OnValueChanged != null)
                        OnValueChanged(this);
            }
        }

        public float SetValue
        {
            get { return m_Value; }
            set { m_Value = value; ValidateValue(); DisplayValue(); }
        }

        [Description("Number of Decimal places"), Category("Data")]
        public int Decimals
        {
            get { return m_Decimals; }
            set { m_Decimals = value; ValidateValue(); DisplayValue(); }
        }

        [Description("Min Value"), Category("Data")]
        public float MinValue
        {
            get { return m_MinValue; }
            set { m_MinValue = value; ValidateValue(); DisplayValue(); }
        }

        [Description("Max Value"), Category("Data")]
        public float MaxValue
        {
            get { return m_MaxValue; }
            set { m_MaxValue = value; ValidateValue(); DisplayValue(); }
        }

        [Description("Increment and Decriment value"), Category("Data")]
        public float Increment
        {
            get { return m_Inc; }
            set { m_Inc = value; }
        }

        #endregion

        public ptxSlider()
        {
            Text = "0.000";
            InitializeComponent();
            DisplayValue();
            this.Multiline = true;
            this.Lines = new String[1];
            this.MouseWheel += new MouseEventHandler(ptxSlider_MouseWheel);  
        }


        void ptxSlider_MouseWheel(object sender, MouseEventArgs e)
        {
            float inc = m_Inc;
            //This causes the form to scroll ????  --so lame
            if (m_ModShift)
            {
                inc = 1.0f;
                for (int i = 0; i < m_Decimals; i++)
                    inc /= 10.0f;
            }

            if (e.Delta > 0)
                Value += (inc);
            if (e.Delta < 0)
                Value -= (inc);
        }

        void DisplayValue()
        {
            String format = "{0:F" + m_Decimals + "}";
            Text = String.Format(format, m_Value);
        }

        void ValidateValue()
        {
            if (m_Value > m_MaxValue)
                m_Value = m_MaxValue;
            else
                if (m_Value < m_MinValue)
                    m_Value = m_MinValue;
        }

        float SetValueFromText()
        {
            float val = m_Value;
            try
            {
                if (float.TryParse(Text, out val))
                    return val;
                return m_Value;
            }
            catch
            {
                return m_Value;
            }

        }

        private void ptxSlider_KeyPress(object sender, KeyPressEventArgs e)
        {
            if((e.KeyChar == 122))
            {
                if (Value == m_LastValue)
                    Value = m_ReDoValue;
                else
                {
                    m_ReDoValue = Value;
                    Value = m_LastValue;
                }
                e.Handled = true;
                return;
            }

            if (e.KeyChar == 13)
            {
                m_LastValue = Value;
                Value = SetValueFromText();
                e.Handled = true;
                return;
            }

            if (e.KeyChar == 45)
                return;

            if (e.KeyChar == 46)
                return;

            if (e.KeyChar == 8)
                return;

            if (e.KeyChar < 48)
            {
                e.Handled = true;
                return;
            }
            if (e.KeyChar > 57)
            {
                e.Handled = true;
                return;
            }
        }

        private void ptxSlider_Leave(object sender, EventArgs e)
        {
            //m_LastValue = Value;
            Value = SetValueFromText();
        }

        private void ptxSlider_MouseDown(object sender, MouseEventArgs e)
        {
            m_ScrubMode = false;
            
            if ((ModifierKeys == Keys.Shift) || (ModifierKeys == Keys.Control) || (ModifierKeys == Keys.Alt))
                m_ScrubMode = true;
            if (this.SelectedText == this.Text)
                m_ScrubMode = true;
            
            if(m_ScrubMode)
            {
                m_LastValue = Value;
                m_ScrubMode = true;
                Cursor.Hide();
            }

            m_MouseDown = true;
            m_MouseAnchorPos = new Point(Cursor.Position.X,Cursor.Position.Y);
        }

        private void ptxSlider_MouseUp(object sender, MouseEventArgs e)
        {
            if (m_ScrubMode)
            {
                Cursor.Show();
                m_ScrubMode = false;
            }
            m_MouseDown = false;
        }

        private void ptxSlider_MouseMove(object sender, MouseEventArgs e)
        {
            if(m_MouseDown && m_ScrubMode)
            {
                int dx = Cursor.Position.X - m_MouseAnchorPos.X;
                int dy = m_MouseAnchorPos.Y - Cursor.Position.Y;
                int dd = 0;
                if (Math.Abs(dx) > Math.Abs(dy))
                    dd = dx;
                else
                    dd = dy;
                
                float inc = m_Inc;
                if(ModifierKeys == Keys.Shift)
                {
                    inc = 1.0f;
                    for (int i = 0; i < m_Decimals; i++)
                        inc /= 10.0f;
                }

                Value += (dd * inc);
                Cursor.Position = m_MouseAnchorPos;
            }
        }

        private void ptxSlider_KeyDown(object sender, KeyEventArgs e)
        {
            m_ModControl = e.Control;
            m_ModShift = e.Shift;

        }

        private void ptxSlider_KeyUp(object sender, KeyEventArgs e)
        {
            m_ModControl = e.Control;
            m_ModShift = e.Shift;

        }

    }
}
