using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Data;
using System.Windows.Forms;
using Sano.PersonalProjects.ColorPicker.Controls;

namespace ragKeyframeEditorControl
{
	/// <summary>
	/// Summary description for ColorGradientControl.
	/// </summary>

	public class ColorGradientControl : System.Windows.Forms.UserControl
	{
		static int m_sSelPegWidth = 10;
		static int m_sSelPegHeight = 6;
		public System.Windows.Forms.Panel PanelWorkspace;
		public System.Windows.Forms.Label Label_FontA;
		public System.Windows.Forms.Label Label_FontB;
		public System.Windows.Forms.Panel PanelColorScrub;
		private System.Windows.Forms.ContextMenu PegMenu;
		private System.Windows.Forms.MenuItem PegMenu_Delete;
		public System.Windows.Forms.Panel PanelAlphaScrub;
		private System.Windows.Forms.Label label1;
		private System.ComponentModel.Container components = null;

		public ColorGradientControl()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			SetupHelp();
			// TODO: Add any initialization after the InitializeComponent call
			m_OffScreenBmp = new Bitmap(PanelWorkspace.Width, PanelWorkspace.Height); 
			m_OffScreenDC = Graphics.FromImage(m_OffScreenBmp);
			m_CPegSelectedBmp = new Bitmap(m_sSelPegWidth,m_sSelPegHeight);
			m_CPegUnSelectedBmp = new Bitmap(m_sSelPegWidth,m_sSelPegHeight);
			m_APegSelectedBmp = new Bitmap(m_sSelPegWidth,m_sSelPegHeight);
			m_APegUnSelectedBmp = new Bitmap(m_sSelPegWidth,m_sSelPegHeight);

			SetupGraphics();

			AddColorKey(0.0f,new Vector4(1.0f,0.0f,0.0f,1.0f));
			AddColorKey(0.5f,new Vector4(0.0f,1.0f,0.0f,1.0f));
			AddColorKey(1.0f,new Vector4(0.0f,0.0f,1.0f,1.0f));
			AddAlphaKey(0.0f,new Vector4(1.0f,1.0f,1.0f,1.0f));
		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.PanelWorkspace = new System.Windows.Forms.Panel();
			this.Label_FontA = new System.Windows.Forms.Label();
			this.Label_FontB = new System.Windows.Forms.Label();
			this.PanelColorScrub = new System.Windows.Forms.Panel();
			this.PegMenu = new System.Windows.Forms.ContextMenu();
			this.PegMenu_Delete = new System.Windows.Forms.MenuItem();
			this.PanelAlphaScrub = new System.Windows.Forms.Panel();
			this.label1 = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// PanelWorkspace
			// 
			this.PanelWorkspace.BackColor = System.Drawing.Color.Transparent;
			this.PanelWorkspace.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.PanelWorkspace.Location = new System.Drawing.Point(16, 56);
			this.PanelWorkspace.Name = "PanelWorkspace";
			this.PanelWorkspace.Size = new System.Drawing.Size(520, 144);
			this.PanelWorkspace.TabIndex = 0;
			this.PanelWorkspace.Paint += new System.Windows.Forms.PaintEventHandler(this.PanelWorkspace_Paint);
			// 
			// Label_FontA
			// 
			this.Label_FontA.Location = new System.Drawing.Point(16, 120);
			this.Label_FontA.Name = "Label_FontA";
			this.Label_FontA.TabIndex = 1;
			this.Label_FontA.Text = "Label_Font";
			this.Label_FontA.Visible = false;
			// 
			// Label_FontB
			// 
			this.Label_FontB.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.Label_FontB.Location = new System.Drawing.Point(120, 120);
			this.Label_FontB.Name = "Label_FontB";
			this.Label_FontB.TabIndex = 2;
			this.Label_FontB.Text = "Label_FontB";
			this.Label_FontB.Visible = false;
			// 
			// PanelColorScrub
			// 
			this.PanelColorScrub.BackColor = System.Drawing.Color.Transparent;
			this.PanelColorScrub.Cursor = System.Windows.Forms.Cursors.Hand;
			this.PanelColorScrub.ForeColor = System.Drawing.Color.Transparent;
			this.PanelColorScrub.Location = new System.Drawing.Point(16, 200);
			this.PanelColorScrub.Name = "PanelColorScrub";
			this.PanelColorScrub.Size = new System.Drawing.Size(520, 32);
			this.PanelColorScrub.TabIndex = 3;
			this.PanelColorScrub.MouseDown += new System.Windows.Forms.MouseEventHandler(this.PanelColorScrub_MouseDown);
			// 
			// PegMenu
			// 
			this.PegMenu.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					this.PegMenu_Delete});
			this.PegMenu.Popup += new System.EventHandler(this.PegMenu_Popup);
			// 
			// PegMenu_Delete
			// 
			this.PegMenu_Delete.Index = 0;
			this.PegMenu_Delete.Text = "Delete";
			this.PegMenu_Delete.Click += new System.EventHandler(this.PegMenu_Delete_Click);
			// 
			// PanelAlphaScrub
			// 
			this.PanelAlphaScrub.Cursor = System.Windows.Forms.Cursors.Hand;
			this.PanelAlphaScrub.Location = new System.Drawing.Point(16, 32);
			this.PanelAlphaScrub.Name = "PanelAlphaScrub";
			this.PanelAlphaScrub.Size = new System.Drawing.Size(520, 24);
			this.PanelAlphaScrub.TabIndex = 4;
			this.PanelAlphaScrub.MouseDown += new System.Windows.Forms.MouseEventHandler(this.PanelAlphaScrub_MouseDown);
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(424, 8);
			this.label1.Name = "label1";
			this.label1.TabIndex = 5;
			this.label1.Text = "label1";
			this.label1.Visible = false;
			// 
			// ColorGradientControl
			// 
			this.Controls.Add(this.label1);
			this.Controls.Add(this.Label_FontB);
			this.Controls.Add(this.Label_FontA);
			this.Controls.Add(this.PanelWorkspace);
			this.Controls.Add(this.PanelColorScrub);
			this.Controls.Add(this.PanelAlphaScrub);
			this.Name = "ColorGradientControl";
			this.Size = new System.Drawing.Size(552, 240);
			this.Paint += new System.Windows.Forms.PaintEventHandler(this.ColorGradientControl_Paint);
			this.ResumeLayout(false);

		}
		#endregion

		#region DataTypes
		public class Vector2 
		{
			public Vector2(){x=y=0.0f;}
			public Vector2(float xx, float yy){x=xx;y=yy;}
			public void Set(float xx,float yy){x=xx;y=yy;}
			public float x,y;
		}
		public class Vector4
		{
			public Vector4(){x=y=z=w=0.0f;}
			public Vector4(float xx,float yy,float zz,float ww){x=xx;y=yy;z=zz;w=ww;}
			public Vector4(Vector4 v)
			{
				Set(v.x,v.y,v.z,v.w);
			}
			public void Set(float xx,float yy,float zz,float ww){x=xx;y=yy;z=zz;w=ww;}
			public float x,y,z,w;
		}
		public class Keyframe
		{
			public Keyframe()
			{
				m_ColorRGB = new Vector4();
				m_ColorHSB = new Vector4();
				m_Time =0.0f;
			}
			public Keyframe(float t,Vector4 colorrgb,Vector4 colorhsb)
			{
				m_Time=t;
				m_ColorRGB =new Vector4(colorrgb);
				m_ColorHSB =new Vector4(colorhsb);
			}
			public Keyframe(Keyframe d)
			{
				Set(d.m_Time,d.m_ColorRGB,d.m_ColorHSB);
			}
			public void Set(float t,Vector4 crgb,Vector4 chsb){m_Time=t;m_ColorRGB=new Vector4(crgb);m_ColorHSB=new Vector4(chsb);}
			public bool RGBIsEqualTo(Vector4 rgb)
			{
				float dx = Math.Abs(m_ColorRGB.x-rgb.x);
				float dy = Math.Abs(m_ColorRGB.y-rgb.y);
				float dz = Math.Abs(m_ColorRGB.z-rgb.z);
				if( (dx<0.001f) && (dy<0.001f) && (dz<0.001f) )
					return true;
				return false;
			}
			public bool AIsEqualTo(float alpha)
			{
				if(Math.Abs(m_ColorRGB.w-alpha)<0.001f)
					return true;
				return false;
			}
			public bool RGBIsMidpointOf(Keyframe A, Keyframe B)
			{
				float mx = (A.m_ColorRGB.x+(B.m_ColorRGB.x-A.m_ColorRGB.x)*0.5f);
				float my = (A.m_ColorRGB.y+(B.m_ColorRGB.y-A.m_ColorRGB.y)*0.5f);
				float mz = (A.m_ColorRGB.z+(B.m_ColorRGB.z-A.m_ColorRGB.z)*0.5f);
				return RGBIsEqualTo(new Vector4(mx,my,mz,0.0f));
			}
			public bool AIsMidpointOf(Keyframe A, Keyframe B)
			{
				float mw = (A.m_ColorRGB.x+(B.m_ColorRGB.x-A.m_ColorRGB.x)*0.5f);
				return AIsEqualTo(mw);
			}
			public bool RGBAIsEqualTo(Vector4 rgba)
			{
				float dx = Math.Abs(m_ColorRGB.x-rgba.x);
				float dy = Math.Abs(m_ColorRGB.y-rgba.y);
				float dz = Math.Abs(m_ColorRGB.z-rgba.z);
				float dw = Math.Abs(m_ColorRGB.w-rgba.w);
				if( (dx<0.0001f) && (dy<0.0001f) && (dz<0.0001f) && (dw<0.0001f) )
					return true;
				return false;
			}
			public bool RGBAIsMidpointOf(Keyframe A, Keyframe B)
			{
				float mx = (A.m_ColorRGB.x+(B.m_ColorRGB.x-A.m_ColorRGB.x)*0.5f);
				float my = (A.m_ColorRGB.y+(B.m_ColorRGB.y-A.m_ColorRGB.y)*0.5f);
				float mz = (A.m_ColorRGB.z+(B.m_ColorRGB.z-A.m_ColorRGB.z)*0.5f);
				float mw = (A.m_ColorRGB.w+(B.m_ColorRGB.w-A.m_ColorRGB.w)*0.5f);
				return RGBAIsEqualTo(new Vector4(mx,my,mz,mw));
			}

			public bool RGBAIsImportant(Keyframe A,Keyframe B)
			{
				//Determine if we are a linear interpolation 
				Keyframe lerp = ColorLerp(m_Time,A,B);
				return !RGBAIsEqualTo(lerp.m_ColorRGB);
			}
			public bool AIsImportant(Keyframe A,Keyframe B)
			{
				//Determine if we are a linear interpolation 
				Keyframe lerp = ColorLerp(m_Time,A,B);
				if(Math.Abs(m_ColorRGB.w-lerp.m_ColorRGB.w)<0.001)
					return false;
				return true;
			}
			public bool RGBIsImportant(Keyframe A,Keyframe B)
			{
				//Determine if we are a linear interpolation 
				Keyframe lerp = ColorLerp(m_Time,A,B);
				if(RGBIsEqualTo(lerp.m_ColorRGB))
					return false;
				return true;
			}
			public static Keyframe ColorLerp(float time, Keyframe A,Keyframe B)
			{
				float dt = A.m_Time- B.m_Time;
				if(dt==0.0f)
					return A;
				float dr = A.m_ColorRGB.x - B.m_ColorRGB.x;
				float dg = A.m_ColorRGB.y - B.m_ColorRGB.y;
				float db = A.m_ColorRGB.z - B.m_ColorRGB.z;

				float da = A.m_ColorRGB.w - B.m_ColorRGB.w;

				float dh = A.m_ColorHSB.x - B.m_ColorHSB.x;
				float ds = A.m_ColorHSB.y - B.m_ColorHSB.y;
				float dv = A.m_ColorHSB.z - B.m_ColorHSB.z;

				float t = (time-A.m_Time)/dt;
				float xx = t * dr + A.m_ColorRGB.x;
				float yy = t * dg + A.m_ColorRGB.y;
				float zz = t * db + A.m_ColorRGB.z;

				float hh = t * dh + A.m_ColorHSB.x;
				float ss = t * ds + A.m_ColorHSB.y;
				float vv = t * dv + A.m_ColorHSB.z;

				float ww = t * da + A.m_ColorRGB.w;

				return new Keyframe(time,new Vector4(xx,yy,zz,ww),new Vector4(hh,ss,vv,ww));

			}
			public Vector4 m_ColorRGB;
			public Vector4 m_ColorHSB;
			public float m_Time;
		}
		public class KeyPeg
		{
			public KeyPeg(Control parent, bool createicon)
			{
				Create(parent,createicon);
			}
			public KeyPeg(Control parent)
			{
				Create(parent,false);
			}
			
			public void Create(Control parent,bool createIcon)
			{
				if(parent==null)return;
				m_KeyData = new Keyframe();
				m_Parent=parent;
				m_Panel = new Panel();
				m_Panel.Tag=this;
				m_Panel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
				m_Panel.Width=11;
				m_Panel.Height=11;
				m_Parent.SuspendLayout();
				if(createIcon)
				{
					m_SelIcon = new Panel();
					m_SelIcon.Tag =this;
					m_SelIcon.BorderStyle = System.Windows.Forms.BorderStyle.None;
					m_SelIcon.Width = m_sSelPegWidth;
					m_SelIcon.Height = m_sSelPegHeight;
					m_Parent.Controls.Add(m_SelIcon);
					m_SelIcon.BringToFront();
				}

				m_Parent.Controls.Add(m_Panel);
				m_Panel.BringToFront();
				m_Parent.ResumeLayout();
				m_IsLocked=false;
				m_IsVisible=true;
				m_IsMidKey=false;
				m_MidLocation = 0.5f;
				m_MouseScalar = 1.0f;
				m_MidPoint=null;
				m_PrevKeyPeg =null;
				//Add mouse events
				
			}
			public Keyframe m_KeyData;
			public Control m_Parent;
			public Panel   m_Panel;
			public Panel   m_SelIcon;
			public bool	   m_IsAlphaKey;
			public bool	   m_IsMidKey;
			public bool	   m_IsLocked;
			public bool	   m_IsVisible;
			public float   m_MidLocation;
			public float   m_MouseScalar;
			public KeyPeg  m_MidPoint;
			public KeyPeg  m_PrevKeyPeg;
			public void SetVisible(bool v)
			{
				
				m_Panel.Visible=v;
				m_IsVisible=v;
			}
		}

		protected class KeyPegTimeComparer : IComparer<KeyPeg>
		{
			public int Compare(KeyPeg x, KeyPeg y)
			{
				KeyPeg pegA = x as KeyPeg;
				KeyPeg pegB = y as KeyPeg;
				if(pegA.m_KeyData.m_Time<pegB.m_KeyData.m_Time)
					return -1;
				else
					if(pegA.m_KeyData.m_Time>pegB.m_KeyData.m_Time)
					return 1;
				return 0;
			}
		}
		protected class KeyframeTimeComparer : IComparer<Keyframe>
		{
			public int Compare(Keyframe x, Keyframe y)
			{
				Keyframe pegA = x as Keyframe;
				Keyframe pegB = y as Keyframe;
				if(pegA.m_Time<pegB.m_Time)
					return -1;
				else
					if(pegA.m_Time>pegB.m_Time)
					return 1;
				return 0;
			}
		}

		public class ColorGradientEventArgs
		{
			public KeyPeg CurPeg
			{
				get {return m_Peg;}
			}
			private KeyPeg m_Peg;
			internal ColorGradientEventArgs(KeyPeg peg)
			{
				m_Peg = peg;
			}
		}

		public class ColorGradientRangeEventArgs
		{
			public Vector2 NewRange
			{
				get {return m_Range;}
			}
			private Vector2 m_Range;
			internal ColorGradientRangeEventArgs(Vector2 range)
			{
				m_Range = range;
			}
		}
		public class HelpItem
		{
			public HelpItem(Object obj,string helptext)
			{
				m_Object = obj;
				m_HelpText = helptext;
			}
			public Object m_Object;
			public string m_HelpText;
		}

		#endregion

		#region Variables
		Vector2				m_XRange = new Vector2(0.0f,1.0f);
		int					m_Granularity=3;
		List<KeyPeg>		m_PegColorArray = new List<KeyPeg>();
        List<Keyframe>      m_PegColorMaxTestArray = new List<Keyframe>();
		List<KeyPeg>		m_PegAlphaArray = new List<KeyPeg>();
        List<Keyframe>      m_PegDataArray = new List<Keyframe>();
		List<KeyPeg>		m_SelPegArray = new List<KeyPeg>();
		List<HelpItem>  	m_HelpArray = new List<HelpItem>();
        public List<Keyframe> m_KeyframeData = new List<Keyframe>();
		public Point		m_MouseDownAnchor;
		public Point		m_MouseMoveDelta;
		Bitmap				m_OffScreenBmp; 
		Graphics			m_OffScreenDC; 
		Bitmap				m_CPegSelectedBmp;
		Bitmap				m_CPegUnSelectedBmp;
		Bitmap				m_APegSelectedBmp;
		Bitmap				m_APegUnSelectedBmp;
		bool				m_HSBMode=false;
		bool				m_MinMaxMode=false;
		bool				m_AllowHSBMode=true;
		bool				m_AllowEditAlpha=true;
		bool				m_AllowEditColor=true;
		bool				m_ShowLabels=true;
		#endregion
		
		#region Properties
		public delegate void ColorGradientEventHandler( object sender, ColorGradientEventArgs e );
		public delegate void ColorGradientRangeEventHandler( object sender, ColorGradientRangeEventArgs e );
		public event ColorGradientEventHandler OnSelectedPeg;
		public event ColorGradientEventHandler OnPegColorChanged;
		public event ColorGradientEventHandler OnPegLocationChanged;
		public event ColorGradientRangeEventHandler OnRangeChanged;


		[Description("Allow Alpha Editing"),Category("Behavior")]
		public bool AllowEditAlpha
		{
			get
			{
				return m_AllowEditAlpha;
			}
			set
			{
				m_AllowEditAlpha=value;
				UpdateColorControl();
			}
		}
		[Description("Allow Color Editing"),Category("Behavior")]
		public bool AllowEditColor
		{
			get
			{
				return m_AllowEditColor;
			}
			set
			{
				m_AllowEditColor=value;
				UpdateColorControl();
			}
		}
		[Description("Allow HSB Mode"),Category("Behavior")]
		public bool AllowHSBMode
		{
			get
			{
				return m_AllowHSBMode;
			}
			set
			{
				m_AllowHSBMode = value;
				if(!value && m_HSBMode)
					SetHSBMode(false);
				UpdateColorControl();			}
		}
		[Description("Show Labels"),Category("Appearance")]
		public bool ShowLables
		{
			get
			{
				return m_ShowLabels;
			}
			set
			{
				m_ShowLabels=value;
				Invalidate();
			}
		}
		[Description("Minimum range of the control"),Category("Data")]
		public float MinRange
		{	
			get 
			{ 
				return GetRange().x;
			}
			set 
			{
				SetRange(value,GetRange().y);
			}
		}
		[Description("Maximum range of the control"),Category("Data")]
		public float MaxRange
		{	
			get 
			{ 
				return GetRange().y;
			}
			set 
			{
				SetRange(GetRange().x,value);
			}
		}
		[Description("Label Resolution"),Category("Data")]
		public int LabelRes
		{	
			get 
			{ 
				return GetLabelResolution();
			}
			set 
			{
				SetLabelResolution(value);
			}
		}


		#endregion

		public void SetHSBMode(bool m)
		{
			if(m_HSBMode && !m)
			{
				m_HSBMode=false;
				UpdateColorControl();
				return;
			}
			if(!m_HSBMode && m && m_AllowHSBMode)
			{
				m_HSBMode=true;
				UpdateColorControl();
				return;
			}
		}
		public void SetMinMaxMode(bool m)
		{
			if(m_MinMaxMode && !m)
			{
				m_MinMaxMode=false;
				UpdateColorControl();
				return;
			}
			if(!m_MinMaxMode && m)
			{
				m_MinMaxMode=true;
				UpdateColorControl();
				return;
			}
		}
		public void ClearKeyframes()
		{
			ClearKeyframes(true);
		}
		public void ClearKeyframes(bool update)
		{
			for(int i = m_PegColorArray.Count-1;i>=0;i--)
			{
				KeyPeg peg = m_PegColorArray[i] as KeyPeg;
				DelKey(peg);
			}
			for(int i = m_PegAlphaArray.Count-1;i>=0;i--)
			{
				KeyPeg peg = m_PegAlphaArray[i] as KeyPeg;
				DelKey(peg);
			}
			m_PegColorArray.Clear();
			m_PegAlphaArray.Clear();
			m_PegDataArray.Clear();
			if(update)
				UpdateColorControl();
		}
		public Vector2 GetRange()
		{
			return m_XRange;
		}
		public void SetRange(float min, float max)
		{
			if(m_XRange.x != min || m_XRange.y != max)
			{
				m_XRange.Set(min,max);
				Invalidate();
				UpdateColorControl();
				if(OnRangeChanged != null)
					OnRangeChanged(this,new ColorGradientRangeEventArgs(m_XRange));
			}
		}

		public int GetLabelResolution()
		{
			return m_Granularity;
		}
		public void SetLabelResolution(int rez)
		{
			if(m_Granularity!=rez)
			{
				m_Granularity = rez;
				Invalidate();
			}
		}

		public void UpdateColorControl()
		{
			PanelColorScrub.Visible = m_AllowEditColor;
			PanelAlphaScrub.Visible = m_AllowEditAlpha;
			PanelColorScrub.SendToBack();
			PanelAlphaScrub.SendToBack();

			foreach(KeyPeg peg in m_PegAlphaArray)
				UpdatePeg(peg);
			foreach(KeyPeg peg in m_PegColorArray)
				UpdatePeg(peg);
			SortPegColorArray();
			SortPegAlphaArray();
			UpdateMidPoints(m_PegColorArray);
			UpdateMidPoints(m_PegAlphaArray);
			MergeKeyframes();
			CreateGradient();
		}
		public void UpdatePegData(KeyPeg peg)
		{
			if(peg == null)
				return;

			UpdatePeg(peg);
			SortPegColorArray();
			SortPegAlphaArray();
			UpdateMidPoints(m_PegColorArray);
			UpdateMidPoints(m_PegAlphaArray);
			if(!peg.m_IsMidKey)
				ShowMidpoints();
			MergeKeyframes();
			CreateGradient();


		}
		internal static double GetMinimumValue( params double[] values ) 
		{

			double minValue = values[0];

			if ( values.Length >= 2 ) 
			{

				double num;

				for ( int i=1; i < values.Length; i++ ) 
				{
			
					num = values[i];
					minValue = Math.Min( minValue, num );

				}

			}

			return minValue;

		} // GetMinimumValue
		internal static double GetMaximumValue( params double[] values ) 
		{

			double maxValue = values[0];

			if ( values.Length >= 2 ) 
			{

				double num;

				for ( int i=1; i < values.Length; i++ ) 
				{
			
					num = values[i];
					maxValue = Math.Max( maxValue, num );

				}

			}

			return maxValue;

		} // GetMaximumValue

		public static Vector4 RGBToHSB(Vector4 rgb)
		{
			// NOTE #1: Even though we're dealing with a very small range of
			// numbers, the accuracy of all calculations is fairly important.
			// For this reason, I've opted to use double data types instead
			// of float, which gives us a little bit extra precision (recall
			// that precision is the number of significant digits with which
			// the result is expressed).

			double r = rgb.x;
			double g = rgb.y;
			double b = rgb.z;

			double minValue = GetMinimumValue( r, g, b );
			double maxValue = GetMaximumValue( r, g, b );
			double delta = maxValue - minValue;

			double hue = 0;
			double saturation = 0;
			double brightness = maxValue * 100;

			if ( maxValue == 0 || delta == 0 ) 
			{

				hue = 0;
				saturation = 0;

			} 
			else 
			{

				// NOTE #2: FXCop insists that we avoid testing for floating 
				// point equality (CA1902). Instead, we'll perform a series of
				// tests with the help of Double.Epsilon that will provide 
				// a more accurate equality evaluation.

				if ( minValue == 0 ) 
				{
					saturation = 100;
				} 
				else 
				{
					saturation = ( delta / maxValue ) * 100;
				}

				if ( Math.Abs( r - maxValue ) < Double.Epsilon ) 
				{				
					hue = ( g - b) / delta;
				} 
				else if ( Math.Abs( g - maxValue ) < Double.Epsilon ) 
				{					
					hue = 2 + ( b - r ) / delta;
				} 
				else if ( Math.Abs( b - maxValue ) < Double.Epsilon ) 
				{
					hue = 4 + ( r - g ) / delta;
				}

			}

			hue *= 60;
			if ( hue < 0 ) 
			{
				hue += 360;
			}

			return new Vector4(
				(float) ( int ) Math.Round( hue ),
				(float) ( int ) Math.Round( saturation ),
				(float) ( int ) Math.Round( brightness ),
				rgb.w);
		}

		public static Vector4 HSBToRGB(Vector4 hsb ) 
		{
			double h, s, b;
			double red = 0, green = 0, blue = 0;
			
			h = (double) hsb.x;
			s = ( ( double ) hsb.y ) / 100;
			b = ( ( double ) hsb.z ) / 100;

			if ( s == 0 ) 
			{
				
				red = b;
				green = b;
				blue = b;

			} 
			else 
			{
				
				double p, q, t;

				// the color wheel has six sectors.
				double fractionalSector;
				int sectorNumber;
				double sectorPosition;

				sectorPosition = h / 60;
				sectorNumber = ( int ) Math.Floor( sectorPosition );
				fractionalSector = sectorPosition - sectorNumber;

				p = b * ( 1 - s );
				q = b * ( 1 - ( s * fractionalSector ) );
				t = b * ( 1 - ( s * ( 1 - fractionalSector ) ) );

				// Assign the fractional colors to r, g, and b
				// based on the sector the angle is in.
				switch (sectorNumber) 
				{
					case 0:
						red = b;
						green = t;
						blue = p;
						break;

					case 1:
						red = q;
						green = b;
						blue = p;
						break;

					case 2:
						red = p;
						green = b;
						blue = t;
						break;

					case 3:
						red = p;
						green = q;
						blue = b;
						break;

					case 4:
						red = t;
						green = p;
						blue = b;
						break;

					case 5:
						red = b;
						green = p;
						blue = q;
						break;
				}

			}

			/*int nRed, nGreen, nBlue;
			nRed   = ( int )( red   * 255.0f + 0.5f );
			nGreen = ( int )( green * 255.0f + 0.5f );
			nBlue  = ( int )( blue  * 255.0f + 0.5f );
			*/

			return new Vector4((float)red,(float)green,(float)blue,hsb.w);

		} // HsbToRgb


        public List<ColorGradientControl.Keyframe> GetKeyframeData()
		{
			return m_KeyframeData;
		}
		private void UpdatePeg(KeyPeg peg)
		{
			if(peg.m_KeyData.m_Time<m_XRange.x)
				peg.m_KeyData.m_Time=m_XRange.x;
			if(peg.m_KeyData.m_Time>m_XRange.y)
				peg.m_KeyData.m_Time=m_XRange.y;

			if(peg.m_IsMidKey)
			{
				if(peg.m_MidLocation<0.01f)
					peg.m_MidLocation=0.01f;
				if(peg.m_MidLocation>0.99f)
					peg.m_MidLocation=0.99f;
			}

			int pos = (int)((peg.m_KeyData.m_Time/(m_XRange.y-m_XRange.x))*(float)(PanelWorkspace.Width)); 
			pos+=PanelWorkspace.Left;
			peg.m_Panel.Left= pos - (peg.m_Panel.Width/2);

            int alpha = (int)System.Math.Round(peg.m_KeyData.m_ColorRGB.w * 255.0f, MidpointRounding.AwayFromZero);
            int red = (int)System.Math.Round(peg.m_KeyData.m_ColorRGB.x * 255.0f, MidpointRounding.AwayFromZero);
            int green = (int)System.Math.Round(peg.m_KeyData.m_ColorRGB.y * 255.0f, MidpointRounding.AwayFromZero);
            int blue = (int)System.Math.Round(peg.m_KeyData.m_ColorRGB.z * 255.0f, MidpointRounding.AwayFromZero);

			if(m_HSBMode)
			{
				Vector4 c = HSBToRGB(peg.m_KeyData.m_ColorHSB);
                red = (int)System.Math.Round(c.x * 255.0f, MidpointRounding.AwayFromZero);
                green = (int)System.Math.Round(c.y * 255.0f, MidpointRounding.AwayFromZero);
                blue = (int)System.Math.Round(c.z * 255.0f, MidpointRounding.AwayFromZero);
			}
			
			if(peg.m_IsAlphaKey)
			{
				peg.m_Panel.BackColor = Color.FromArgb(255,alpha,alpha,alpha);
				if(peg.m_IsMidKey)
					peg.m_IsVisible = peg.m_IsVisible&&m_AllowEditAlpha;
				else
					peg.m_IsVisible =m_AllowEditAlpha;
				if(peg.m_SelIcon!=null)
					peg.m_SelIcon.Visible =m_AllowEditAlpha;
			}
			else
			{
				peg.m_Panel.BackColor = Color.FromArgb(255,red,green,blue);
				if(peg.m_IsMidKey)
					peg.m_IsVisible = peg.m_IsVisible&&m_AllowEditColor;
				else
					peg.m_IsVisible = m_AllowEditColor;
				if(peg.m_SelIcon!=null)
					peg.m_SelIcon.Visible =m_AllowEditColor;

			}
			if(peg.m_SelIcon!=null)
			{
				peg.m_SelIcon.Left = peg.m_Panel.Left;
				if(peg.m_IsAlphaKey)
					peg.m_SelIcon.Top  = peg.m_Panel.Top+peg.m_Panel.Height-1;
				else
					peg.m_SelIcon.Top  = peg.m_Panel.Top - peg.m_SelIcon.Height;
			}
			peg.m_Panel.Visible=peg.m_IsVisible;
		}
		private void MoveSelectedPegs()
		{
			float timeperpixel = (m_XRange.y-m_XRange.x)/((float)(PanelWorkspace.Width));
			float dtime = ((float)m_MouseMoveDelta.X)*timeperpixel;

			foreach(KeyPeg peg in m_SelPegArray)
			{
				if(peg.m_IsLocked)
					continue;
				if(peg.m_IsMidKey)
				{
					float dt = ((float)m_MouseMoveDelta.X)*peg.m_MouseScalar;
					peg.m_MidLocation+=(dt);
				}
				else
				{
					peg.m_KeyData.m_Time-=dtime;
				}
				UpdatePeg(peg);
				if(OnPegLocationChanged!=null)
					OnPegLocationChanged(this,new ColorGradientEventArgs(peg));
			}
		}
		private void ShowMidpoints()
		{
			for(int i=m_PegColorArray.Count-1;i>=0;i--)
			{
				KeyPeg peg = m_PegColorArray[i] as KeyPeg;
				if(peg.m_MidPoint!=null)
					peg.m_MidPoint.m_IsVisible=(false);
				if(m_SelPegArray.Contains(peg) || m_SelPegArray.Contains(peg.m_PrevKeyPeg) )
				{
					if(peg.m_MidPoint!=null)
						peg.m_MidPoint.SetVisible(true&&m_AllowEditColor);
				}
			}
			for(int i=m_PegAlphaArray.Count-1;i>=0;i--)
			{
				KeyPeg peg = m_PegAlphaArray[i] as KeyPeg;
				if(peg.m_MidPoint!=null)
					peg.m_MidPoint.m_IsVisible=false;
				if(m_SelPegArray.Contains(peg) || m_SelPegArray.Contains(peg.m_PrevKeyPeg) )
				{
					if(peg.m_MidPoint!=null)
						peg.m_MidPoint.SetVisible(true&&m_AllowEditAlpha);
				}
			}
			UpdateMidPoints(m_PegColorArray);
			UpdateMidPoints(m_PegAlphaArray);
		}
		private void UpdateSelected()
		{
			DrawIcons();
			if(m_SelPegArray.Count>0)
				OnSelectedPeg(this,new ColorGradientEventArgs(m_SelPegArray[0] as KeyPeg));
		}
		private void UpdateMidPoints(List<KeyPeg> list)
		{
			foreach(KeyPeg peg in list)
			{
				if( (peg.m_MidPoint != null) && (peg.m_PrevKeyPeg!=null) )
				{
					KeyPeg mid = peg.m_MidPoint;
					float time = (peg.m_KeyData.m_Time - peg.m_PrevKeyPeg.m_KeyData.m_Time)*mid.m_MidLocation+peg.m_PrevKeyPeg.m_KeyData.m_Time;
					float midtime = (peg.m_KeyData.m_Time - peg.m_PrevKeyPeg.m_KeyData.m_Time)*0.5f+peg.m_PrevKeyPeg.m_KeyData.m_Time;
					Keyframe lerpd = ColorLerp(midtime,peg.m_PrevKeyPeg,peg);
					mid.m_KeyData.Set(time,lerpd.m_ColorRGB,lerpd.m_ColorHSB);
					mid.m_MouseScalar = 1.0f/(peg.m_PrevKeyPeg.m_Panel.Left-peg.m_Panel.Left);
					UpdatePeg(mid);
				}
			}

		}
		private void CreateMidPoint(KeyPeg peg)
		{
			CreateMidPoint(peg,0.5f);
		}
		private void CreateMidPoint(KeyPeg peg,float midlocation)
		{
			//no midpoint on leftmost key
			if(peg.m_PrevKeyPeg == null)
				return;

			KeyPeg mid = peg.m_MidPoint = new KeyPeg(peg.m_Parent);
			mid.m_Panel.Width=7;
			mid.m_Panel.Height=7;
			mid.m_IsMidKey = true;
			mid.m_IsAlphaKey = false;
			mid.m_IsVisible=false;
			mid.m_MidLocation=midlocation;
			float midtime = (peg.m_KeyData.m_Time - peg.m_PrevKeyPeg.m_KeyData.m_Time)*mid.m_MidLocation+peg.m_PrevKeyPeg.m_KeyData.m_Time;
			Keyframe lerpd = GetColorFromPegArray(midtime,m_PegColorArray);
			mid.m_KeyData.Set(midtime,lerpd.m_ColorRGB,lerpd.m_ColorHSB);
			if(peg.m_IsAlphaKey)
				mid.m_Panel.Top=PanelWorkspace.Top-6-mid.m_Panel.Height+3;
			else
				mid.m_Panel.Top=PanelWorkspace.Bottom+6-3;
			UpdatePeg(mid);
			mid.m_Panel.MouseMove += new System.Windows.Forms.MouseEventHandler(this.KeyPeg_MouseMove);
			mid.m_Panel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.KeyPeg_MouseDown);
			mid.m_Panel.MouseUp += new System.Windows.Forms.MouseEventHandler(this.KeyPeg_MouseUp);
		}
		public KeyPeg AddColorKey(int pos ,Vector4 color,Keyframe midpoint)
		{
			float timeperpixel = (m_XRange.y-m_XRange.x)/((float)(PanelWorkspace.Width));
			float t = pos * timeperpixel;
			return AddColorKey(t,color,midpoint);
		}
		public KeyPeg AddColorKey(int pos ,Vector4 color)
		{
			float timeperpixel = (m_XRange.y-m_XRange.x)/((float)(PanelWorkspace.Width));
			float t = pos * timeperpixel;
			return AddColorKey(t,color,null);
		}
		public KeyPeg AddColorKey(float time, Vector4 color)
		{
			return AddColorKey(time,color,null);
		}
		public KeyPeg AddColorKey(float time, Vector4 color,Keyframe midpoint)
		{
			//See if we are too close to an existing keyframe
			foreach(KeyPeg peg in m_PegColorArray)
			{
				float t = Math.Abs(time-peg.m_KeyData.m_Time);
				if(t<0.005f)
				{
					peg.m_KeyData.Set(peg.m_KeyData.m_Time,color,RGBToHSB(color));
					return peg;
				}
			}

			if(m_PegColorArray.Count==0)
				time=0.0f;
			if(m_PegAlphaArray.Count==0)
				AddAlphaKey(0.0f,new Vector4(color));

			KeyPeg newPeg = new KeyPeg(this,true);
			newPeg.m_IsAlphaKey=false;
			newPeg.m_KeyData.Set(time,color,RGBToHSB(color));
			newPeg.m_Panel.Top=PanelWorkspace.Bottom+6;
			UpdatePeg(newPeg);
			newPeg.m_Panel.ContextMenu = PegMenu;
			newPeg.m_Panel.MouseMove += new System.Windows.Forms.MouseEventHandler(this.KeyPeg_MouseMove);
			newPeg.m_Panel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.KeyPeg_MouseDown);
			newPeg.m_Panel.MouseUp += new System.Windows.Forms.MouseEventHandler(this.KeyPeg_MouseUp);
			if(newPeg.m_SelIcon!=null)
			{
				newPeg.m_SelIcon.MouseMove += new System.Windows.Forms.MouseEventHandler(this.KeyPeg_MouseMove);
				newPeg.m_SelIcon.MouseDown += new System.Windows.Forms.MouseEventHandler(this.KeyPeg_MouseDown);
				newPeg.m_SelIcon.MouseUp += new System.Windows.Forms.MouseEventHandler(this.KeyPeg_MouseUp);
			}
			m_PegColorArray.Add(newPeg);
			m_SelPegArray.Clear();
			m_SelPegArray.Add(newPeg);
			if(newPeg.m_KeyData.m_Time == this.m_XRange.x)
				newPeg.m_IsLocked=true;
			SortPegColorArray();
			if(midpoint!=null)
				CreateMidPoint(newPeg,midpoint.m_Time/(newPeg.m_KeyData.m_Time+newPeg.m_PrevKeyPeg.m_KeyData.m_Time));
			else
				CreateMidPoint(newPeg);
			ShowMidpoints();
			MergeKeyframes();
			CreateGradient();
			//first3 are added to the maxarray as a test
			if(m_PegColorMaxTestArray.Count<3)
			{
				Keyframe key = new Keyframe(newPeg.m_KeyData.m_Time,newPeg.m_KeyData.m_ColorRGB,newPeg.m_KeyData.m_ColorHSB);
				m_PegColorMaxTestArray.Add(key);
			}
				
			return newPeg;
		}
		
		public KeyPeg AddAlphaKey(int pos ,Vector4 color,Keyframe midpoint)
		{
			float timeperpixel = (m_XRange.y-m_XRange.x)/((float)(PanelWorkspace.Width));
			float t = pos * timeperpixel;
			return AddAlphaKey(t,color,midpoint);
		}

		public KeyPeg AddAlphaKey(int pos ,Vector4 color)
		{
			float timeperpixel = (m_XRange.y-m_XRange.x)/((float)(PanelWorkspace.Width));
			float t = pos * timeperpixel;
			return AddAlphaKey(t,color,null);
		}
		public KeyPeg AddAlphaKey(float time, Vector4 color)
		{
			return AddAlphaKey(time,color,null);
		}
		public KeyPeg AddAlphaKey(float time, Vector4 color, Keyframe midpoint)
		{
			//See if we are too close to an existing keyframe
			foreach(KeyPeg peg in m_PegAlphaArray)
			{
				float t = Math.Abs(time-peg.m_KeyData.m_Time);
				if(t<0.005f)
				{
					peg.m_KeyData.Set(peg.m_KeyData.m_Time,color,RGBToHSB(color));
					return peg;
				}
			}

			if(m_PegAlphaArray.Count==0)
				time=0.0f;


			KeyPeg newPeg = new KeyPeg(this,true);
			newPeg.m_IsAlphaKey=true;
			newPeg.m_KeyData.Set(time,color,RGBToHSB(color));
			newPeg.m_Panel.Top=PanelWorkspace.Top-6-newPeg.m_Panel.Height;
			UpdatePeg(newPeg);
			newPeg.m_Panel.ContextMenu = PegMenu;
			newPeg.m_Panel.MouseMove += new System.Windows.Forms.MouseEventHandler(this.KeyPeg_MouseMove);
			newPeg.m_Panel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.KeyPeg_MouseDown);
			newPeg.m_Panel.MouseUp += new System.Windows.Forms.MouseEventHandler(this.KeyPeg_MouseUp);
			if(newPeg.m_SelIcon!=null)
			{
				newPeg.m_SelIcon.MouseMove += new System.Windows.Forms.MouseEventHandler(this.KeyPeg_MouseMove);
				newPeg.m_SelIcon.MouseDown += new System.Windows.Forms.MouseEventHandler(this.KeyPeg_MouseDown);
				newPeg.m_SelIcon.MouseUp += new System.Windows.Forms.MouseEventHandler(this.KeyPeg_MouseUp);
			}
			m_PegAlphaArray.Add(newPeg);
			if(newPeg.m_KeyData.m_Time == this.m_XRange.x)
				newPeg.m_IsLocked=true;

			SortPegAlphaArray();
			if(midpoint!=null)
				CreateMidPoint(newPeg,midpoint.m_Time/(newPeg.m_KeyData.m_Time+newPeg.m_PrevKeyPeg.m_KeyData.m_Time));
			else
				CreateMidPoint(newPeg);
			if(newPeg.m_MidPoint!=null)
			{
				newPeg.m_MidPoint.m_IsAlphaKey=true;
			}
			ShowMidpoints();
			if(newPeg.m_MidPoint!= null && midpoint !=null)
			{
				newPeg.m_MidPoint.m_KeyData.m_Time = midpoint.m_Time;
				UpdatePeg(newPeg.m_MidPoint);
			}
			
			m_SelPegArray.Clear();
			m_SelPegArray.Add(newPeg);
			
			MergeKeyframes();
			CreateGradient();
			return newPeg;
		}
		
		public void UserDelKey(KeyPeg peg)
		{
			if(peg== null || peg.m_IsMidKey)
				return;
			DelKey(peg);
			SortPegColorArray();
			SortPegAlphaArray();
			ShowMidpoints();
			UpdateMidPoints(m_PegColorArray);
			UpdateMidPoints(m_PegAlphaArray);
			MergeKeyframes();
			CreateGradient();

		}
		private void DelKey(KeyPeg peg)
		{
			peg.m_Parent.SuspendLayout();
			peg.m_Parent.Controls.Remove(peg.m_Panel);
			if(peg.m_MidPoint != null)
				peg.m_Parent.Controls.Remove(peg.m_MidPoint.m_Panel);
			if(peg.m_SelIcon != null)
				peg.m_Parent.Controls.Remove(peg.m_SelIcon);
			peg.m_Parent.ResumeLayout();
			if(peg.m_IsAlphaKey)
				m_PegAlphaArray.Remove(peg);
			else
				m_PegColorArray.Remove(peg);
			m_SelPegArray.Remove(peg);
		}
		private void DelSelectedKeys()
		{
			for(int i=m_SelPegArray.Count-1;i>=0;i--)
			{
				KeyPeg peg = m_SelPegArray[i] as KeyPeg;
				if(!peg.m_IsMidKey)
					DelKey(peg);
			}
			SortPegColorArray();
			SortPegAlphaArray();
			ShowMidpoints();
			UpdateMidPoints(m_PegColorArray);
			UpdateMidPoints(m_PegAlphaArray);
			MergeKeyframes();
			CreateGradient();
		}

		
		private void OptimizeData()
		{
			//Create the m_KeyframeData list
			//Assumes that m_PegDataArray is built and sorted
			m_KeyframeData.Clear();
			
			if(m_PegDataArray.Count==0)
				return;
			
			//Add first keyframe
			m_KeyframeData.Add(m_PegDataArray[0] as Keyframe);
			
			for(int i=1;i<m_PegDataArray.Count;i++)
			{
				Keyframe key = m_PegDataArray[i] as Keyframe;
				Keyframe preKey = m_PegDataArray[i-1] as Keyframe;
				Keyframe nextKey = null;
				if((i+1)<m_PegDataArray.Count)
					nextKey = m_PegDataArray[i+1] as Keyframe;

				//only add keys which are important
				//don't add if key is equal to next key or if key is an interpolation of prekey to nextkey
				if(nextKey!=null)
				{
//					if(!key.RGBAIsEqualTo(nextKey.m_ColorRGB))
						if(key.RGBAIsImportant(preKey,nextKey))
							m_KeyframeData.Add(new Keyframe(key));
				}
				else
				{
					//no next key so don't add if key is equal to preKey
					if(!key.RGBAIsEqualTo(preKey.m_ColorRGB))
						m_KeyframeData.Add(new Keyframe(key));
				}
			}
			this.label1.Text = m_KeyframeData.Count.ToString();
		}

        public void BuildFromData(List<Keyframe> keyframeData)
		{
			if(keyframeData.Count==0)
				return;
			keyframeData.Sort(new KeyframeTimeComparer());
			ClearKeyframes(false);

			List<Keyframe> alphaKeys = new List<Keyframe>();
            List<Keyframe> colorKeys = new List<Keyframe>();
            List<Keyframe> midKeys = new List<Keyframe>();

			//Add the first key
			alphaKeys.Add(new Keyframe(keyframeData[0] as Keyframe));
			colorKeys.Add(new Keyframe(keyframeData[0] as Keyframe));

			for(int i=1;i<keyframeData.Count;i++)
			{
				Keyframe preKey = keyframeData[i-1] as Keyframe;
				Keyframe key = keyframeData[i] as Keyframe;
				Keyframe nextKey = null;
				if((i+1)<keyframeData.Count)
					nextKey=keyframeData[i+1] as Keyframe;
				
				if(nextKey!=null)
				{
					if(key.AIsImportant(preKey,nextKey))
						alphaKeys.Add( new Keyframe(key));
					if(key.RGBIsImportant(preKey,nextKey))
						colorKeys.Add( new Keyframe(key));
				}
				else
				{
					if(!key.AIsEqualTo(preKey.m_ColorRGB.w))
						alphaKeys.Add(new Keyframe(key));
					if(!key.RGBIsEqualTo(preKey.m_ColorRGB))
						colorKeys.Add(new Keyframe(key));
				}
			}

			//Now add keyframes and determine which are midpoints
			//Add first element
			Keyframe fkey = alphaKeys[0] as Keyframe;
			AddAlphaKey(fkey.m_Time,fkey.m_ColorRGB);
			AddColorKey(fkey.m_Time,fkey.m_ColorRGB);

			for(int i=1;i<alphaKeys.Count;i++)
			{
				Keyframe key = alphaKeys[i] as Keyframe;
				Keyframe preKey = alphaKeys[i-1] as Keyframe;
				Keyframe nextKey = null;
				if((i+1)<alphaKeys.Count)
					nextKey = alphaKeys[i+1] as Keyframe;

				if(nextKey!=null)
				{
					if(key.AIsMidpointOf(preKey,nextKey))
					{
						AddAlphaKey(nextKey.m_Time,nextKey.m_ColorRGB,key);
						i+=2;
					}
					else
						AddAlphaKey(key.m_Time,key.m_ColorRGB);
				}
				else
					AddAlphaKey(key.m_Time,key.m_ColorRGB);
			}
			for(int i=1;i<colorKeys.Count;i++)
			{
				Keyframe key = colorKeys[i] as Keyframe;
				Keyframe preKey = colorKeys[i-1] as Keyframe;
				Keyframe nextKey = null;
				if((i+1)<colorKeys.Count)
					nextKey = colorKeys[i+1] as Keyframe;

				if(nextKey!=null)
				{
					if(key.RGBIsMidpointOf(preKey,nextKey))
					{
						AddColorKey(nextKey.m_Time,nextKey.m_ColorRGB,key);
						i+=2;
					}
					else
						AddColorKey(key.m_Time,key.m_ColorRGB);
				}
				else
					AddColorKey(key.m_Time,key.m_ColorRGB);
			}
		}
		public void BuildFromGradientControl(ColorGradientControl cgc)
		{
			int minpeg = Math.Min(m_PegAlphaArray.Count,cgc.m_PegAlphaArray.Count);
			for(int i=m_PegAlphaArray.Count-1;i>minpeg;i--)
				DelKey(m_PegAlphaArray[i] as KeyPeg);

			for(int i=0;i<minpeg;i++)
			{
				KeyPeg speg = cgc.m_PegAlphaArray[i] as KeyPeg;;
				KeyPeg dpeg = m_PegAlphaArray[i] as KeyPeg;;
				dpeg.m_KeyData = new Keyframe(speg.m_KeyData);
				if(dpeg.m_MidPoint!=null)
				{
					speg.m_MidPoint.m_KeyData = new Keyframe(dpeg.m_MidPoint.m_KeyData);
					speg.m_MidPoint.m_MidLocation = dpeg.m_MidPoint.m_MidLocation;
				}
			}
			for(int i=minpeg;i<cgc.m_PegAlphaArray.Count;i++)
			{
				KeyPeg dpeg = cgc.m_PegAlphaArray[i] as KeyPeg;
				if(dpeg.m_MidPoint!=null)
					AddAlphaKey(dpeg.m_KeyData.m_Time,dpeg.m_KeyData.m_ColorRGB,dpeg.m_MidPoint.m_KeyData);
				else
					AddAlphaKey(dpeg.m_KeyData.m_Time,dpeg.m_KeyData.m_ColorRGB);
			}


			minpeg = Math.Min(m_PegColorArray.Count,cgc.m_PegColorArray.Count);
			for(int i=m_PegColorArray.Count-1;i>minpeg;i--)
				DelKey(m_PegColorArray[i] as KeyPeg);

			for(int i=0;i<minpeg;i++)
			{
				KeyPeg speg = cgc.m_PegColorArray[i] as KeyPeg;;
				KeyPeg dpeg = m_PegColorArray[i] as KeyPeg;;
				dpeg.m_KeyData = new Keyframe(speg.m_KeyData);
				if(dpeg.m_MidPoint!=null)
				{
					speg.m_MidPoint.m_KeyData = new Keyframe(dpeg.m_MidPoint.m_KeyData);
					speg.m_MidPoint.m_MidLocation = dpeg.m_MidPoint.m_MidLocation;
				}
			}
			for(int i=minpeg;i<cgc.m_PegColorArray.Count;i++)
			{
				KeyPeg dpeg = cgc.m_PegColorArray[i] as KeyPeg;
				if(dpeg.m_MidPoint!=null)
					AddColorKey(dpeg.m_KeyData.m_Time,dpeg.m_KeyData.m_ColorRGB,dpeg.m_MidPoint.m_KeyData);
				else
					AddColorKey(dpeg.m_KeyData.m_Time,dpeg.m_KeyData.m_ColorRGB);
			}
			this.UpdateColorControl();
//
//			ClearKeyframes(false);
//			foreach(KeyPeg peg in cgc.m_PegAlphaArray)
//			{
//				if(peg.m_MidPoint!=null)
//					AddAlphaKey(peg.m_KeyData.m_Time,peg.m_KeyData.m_ColorRGB,peg.m_MidPoint.m_KeyData);
//				else
//					AddAlphaKey(peg.m_KeyData.m_Time,peg.m_KeyData.m_ColorRGB);
//			}
//			foreach(KeyPeg peg in cgc.m_PegColorArray)
//			{
//				if(peg.m_MidPoint!=null)
//					AddColorKey(peg.m_KeyData.m_Time,peg.m_KeyData.m_ColorRGB,peg.m_MidPoint.m_KeyData);
//				else
//					AddColorKey(peg.m_KeyData.m_Time,peg.m_KeyData.m_ColorRGB);
//			}
		}
		private void SetupHelp()
		{
			m_HelpArray.Add(new HelpItem(PanelColorScrub,"This is the panel color scrub"));
		}
		private void SetupGraphics()
		{
			//Draw the icons into their offscreen bitmaps

			int pegHeight = m_CPegUnSelectedBmp.Height;
			int pegWidth = m_CPegUnSelectedBmp.Width;
			//Color unselected
			Graphics g = Graphics.FromImage(m_CPegUnSelectedBmp);
			Point[] p = new Point[3];
			p[0] = new Point(0,pegHeight);
			p[1] = new Point((pegWidth/2),0);
			p[2] = new Point(pegWidth,pegHeight);
			g.FillRectangle(new SolidBrush(Color.FromKnownColor(KnownColor.Control)),0,0,pegWidth,pegHeight);
			g.DrawPolygon(new Pen(Color.Black),p);
			//Color Selected
			g = Graphics.FromImage(m_CPegSelectedBmp);
			g.FillRectangle(new SolidBrush(Color.FromKnownColor(KnownColor.Control)),0,0,pegWidth,pegHeight);
			g.FillPolygon(new SolidBrush(Color.Black),p);
			
			
			//Alpha unselected
			g = Graphics.FromImage(m_APegUnSelectedBmp);
			p[0] = new Point(0,0);
			p[1] = new Point((pegWidth/2),pegHeight);
			p[2] = new Point(pegWidth,0);
			g.FillRectangle(new SolidBrush(Color.FromKnownColor(KnownColor.Control)),0,0,pegWidth,pegHeight);
			g.DrawPolygon(new Pen(Color.Black),p);
			//Color Selected
			g = Graphics.FromImage(m_APegSelectedBmp);
			g.FillRectangle(new SolidBrush(Color.FromKnownColor(KnownColor.Control)),0,0,pegWidth,pegHeight);
			g.FillPolygon(new SolidBrush(Color.Black),p);
		}
		private Keyframe ColorLerp(float time,KeyPeg pegA,KeyPeg pegB)
		{
			return ColorLerp(time,pegA.m_KeyData,pegB.m_KeyData);
		}

		private Keyframe ColorLerp(float time,Keyframe pegA,Keyframe pegB)
		{
			return Keyframe.ColorLerp(time,pegA,pegB);
		}

		private Keyframe GetColorFromPegArray(float time,List<KeyPeg> list)
		{
			//Prolly do something to handle case where pegs have same time
			if(list.Count==0)
				return new Keyframe(time,new Vector4(1.0f,1.0f,1.0f,1.0f),RGBToHSB(new Vector4(1.0f,1.0f,1.0f,1.0f)));
			if(list.Count==1)
			{
				KeyPeg peg = list[0] as KeyPeg;
				return peg.m_KeyData;
			}
			
			//walk back thru list and look for adjecent keys
			for(int i=list.Count-1;i>=0;i--)
			{
				KeyPeg curPeg = list[i] as KeyPeg;
				if(curPeg.m_KeyData.m_Time<=time)
				{
					if( (i==(list.Count-1)) || (curPeg.m_KeyData.m_Time==time) )
					{
						return curPeg.m_KeyData;
					}
					else
						return ColorLerp(time,curPeg,list[i+1]as KeyPeg);
				}
			}
			return new Keyframe(time,new Vector4(1.0f,1.0f,1.0f,1.0f),RGBToHSB(new Vector4(1.0f,1.0f,1.0f,1.0f)));
		}
		private Keyframe GetColorFromPegArray(int pos,List<KeyPeg> list)
		{
			float timeperpixel = (m_XRange.y-m_XRange.x)/((float)(PanelWorkspace.Width));
			float t = pos * timeperpixel;
			return GetColorFromPegArray(t,list);
		}
        private Keyframe GetColorFromKeyframeArray(int pos, List<Keyframe> list)
		{
			float timeperpixel = (m_XRange.y-m_XRange.x)/((float)(PanelWorkspace.Width));
			float t = pos * timeperpixel;
			return GetColorFromKeyframeArray(t,list);
		}

        private Keyframe GetColorFromKeyframeArray(float time, List<Keyframe> list)
		{
			//Prolly do something to handle case where pegs have same time
			if(list.Count==0)
				return new Keyframe(time, new Vector4(1.0f,1.0f,1.0f,1.0f),RGBToHSB(new Vector4(1.0f,1.0f,1.0f,1.0f)));
			if(list.Count==1)
			{
				Keyframe peg = list[0] as Keyframe;
				return peg;
			}
			
			//walk back thru list and look for adjecent keys
			for(int i=list.Count-1;i>=0;i--)
			{
				Keyframe curPeg = list[i] as Keyframe;
				if(curPeg.m_Time<=time)
				{
					if( (i==(list.Count-1)) || (curPeg.m_Time==time) )
					{
						return curPeg;
					}
					else
						return ColorLerp(time,curPeg,list[i+1]as Keyframe);
				}
			}
			return new Keyframe(time, new Vector4(1.0f,1.0f,1.0f,1.0f),RGBToHSB(new Vector4(1.0f,1.0f,1.0f,1.0f)));		
		}


		private void SortPegColorArray()
		{
			if(m_PegColorArray.Count==0)
				return;
			m_PegColorArray.Sort(new KeyPegTimeComparer());
			KeyPeg first = m_PegColorArray[0] as KeyPeg;
			first.m_PrevKeyPeg = null;
			for(int i=1;i<m_PegColorArray.Count;i++)
			{
				KeyPeg curpeg = m_PegColorArray[i] as KeyPeg;
				curpeg.m_PrevKeyPeg = m_PegColorArray[i-1] as KeyPeg;
			}
		}
		private void SortPegAlphaArray()
		{
			if(m_PegAlphaArray.Count==0)
				return;
			m_PegAlphaArray.Sort(new KeyPegTimeComparer());
			KeyPeg first = m_PegAlphaArray[0] as KeyPeg;
			first.m_PrevKeyPeg = null;
			for(int i=1;i<m_PegAlphaArray.Count;i++)
			{
				KeyPeg curpeg = m_PegAlphaArray[i] as KeyPeg;
				curpeg.m_PrevKeyPeg = m_PegAlphaArray[i-1] as KeyPeg;
			}
		}
		private void MergeKeyframes()
		{
			m_PegDataArray.Clear();
            List<Keyframe> pegAlphaData = new List<Keyframe>();
            List<Keyframe> pegColorData = new List<Keyframe>();
			
			//Merge alpha keys
			foreach(KeyPeg pega in m_PegAlphaArray)
			{
				Keyframe key = new Keyframe(pega.m_KeyData);
				pegAlphaData.Add(key);
				if(pega.m_MidPoint!=null)
				{
					key = new Keyframe(pega.m_MidPoint.m_KeyData);
					pegAlphaData.Add(key);
				}
			}
			pegAlphaData.Sort(new KeyframeTimeComparer());
			
			//Merge color keys
			foreach(KeyPeg pegc in m_PegColorArray)
			{
				Keyframe key = new Keyframe(pegc.m_KeyData);
				pegColorData.Add(key);
				if(pegc.m_MidPoint!=null)
				{
					key = new Keyframe(pegc.m_MidPoint.m_KeyData);
					pegColorData.Add(key);
				}
			}
			pegColorData.Sort(new KeyframeTimeComparer());
			
			foreach(Keyframe keya in pegAlphaData)
			{
				Keyframe lerp = GetColorFromKeyframeArray(keya.m_Time,pegColorData);
				Vector4 colorrgb = new Vector4(lerp.m_ColorRGB);
				Vector4 colorhsb = new Vector4(lerp.m_ColorHSB);
				colorhsb.w = colorrgb.w = keya.m_ColorRGB.w;
				Keyframe key = new Keyframe(keya.m_Time,colorrgb,colorhsb);
				m_PegDataArray.Add(key);
			}
			
			foreach(Keyframe keyc in pegColorData)
			{
				bool add=true;
				foreach(Keyframe k in m_PegDataArray)
					if(Math.Abs(k.m_Time-keyc.m_Time)<0.0001f)
						add = false;
				if(add)
				{
					Vector4 colorrgb = new Vector4(keyc.m_ColorRGB);
					Vector4 colorhsb = new Vector4(keyc.m_ColorHSB);
					colorhsb.w = colorrgb.w = GetColorFromKeyframeArray(keyc.m_Time,pegAlphaData).m_ColorRGB.w;
					Keyframe key = new Keyframe(keyc.m_Time,colorrgb,colorhsb);
					m_PegDataArray.Add(key);
				}
			}
			
			m_PegDataArray.Sort(new KeyframeTimeComparer());
			if(!m_HSBMode)
				OptimizeData();
		}

		private void DrawMidLabel(Graphics g, float min, float max, int left, int right, int top, int res)
		{
			float midVal = (min+max)*0.5f;
			int midPos = (left+right)/2;

			if(res>0)
			{
				DrawMidLabel(g,min,midVal,left,midPos,top,res-1);
				DrawMidLabel(g,midVal,max,midPos,right,top,res-1);
				
				//Draw middle value
				Size labelSize;
				labelSize = g.MeasureString(midVal.ToString("0.000"),Label_FontA.Font).ToSize();
				Point pos = new Point();
				pos.X = midPos-(labelSize.Width/2);
				pos.Y = top;
				g.DrawString(midVal.ToString("0.000"), Label_FontA.Font, new SolidBrush(Color.Black),pos);
			}
		}

		private void DrawLabels(Graphics g)
		{
			if(!m_ShowLabels)
				return;
			SizeF lsize = g.MeasureString("0.0f",Label_FontA.Font);
			float min = m_XRange.x;
			float max = m_XRange.y;
			int left = PanelWorkspace.Left;
			int right = left + (PanelWorkspace.Width);
			int top = PanelAlphaScrub.Top-(int)lsize.Height;

			DrawMidLabel(g,min,max,left,right,top,m_Granularity);

			//Draw min label
			Size labelSize;
			labelSize = g.MeasureString(min.ToString("0.00"),Label_FontB.Font).ToSize();
			Point pos = new Point();
			pos.X = left-(labelSize.Width/2);
			pos.Y = top;
			g.DrawString(min.ToString("0.00"), Label_FontB.Font, new SolidBrush(Color.Black),pos);
							
			//Draw max label
			labelSize = g.MeasureString(max.ToString("0.00"),Label_FontB.Font).ToSize();
			pos.X = right-(labelSize.Width/2);
			pos.Y = top;
			g.DrawString(max.ToString("0.00"), Label_FontB.Font, new SolidBrush(Color.Black),pos);

		}

		private void CreateGradientHSB()
		{
			for(int i=0;i<PanelWorkspace.Width;i++)
			{
				Keyframe lerpd = GetColorFromKeyframeArray(i,m_PegDataArray);
				Vector4 vcolor = lerpd.m_ColorHSB;
				vcolor = HSBToRGB(vcolor);
				int alpha	=(int)System.Math.Round(vcolor.w*255.0f, MidpointRounding.AwayFromZero);
				int red		=(int)System.Math.Round(vcolor.x*255.0f, MidpointRounding.AwayFromZero);
				int green	=(int)System.Math.Round(vcolor.y*255.0f, MidpointRounding.AwayFromZero);
                int blue = (int)System.Math.Round(vcolor.z * 255.0f, MidpointRounding.AwayFromZero);
				Color c = Color.FromArgb(alpha,red,green,blue);
				Pen pen = new Pen(c);
				m_OffScreenDC.DrawLine(pen,new Point(i,0),new Point(i,PanelWorkspace.Height));
			}
		}
		private void CreateGradientMinMax()
		{
			for(int i=0;i<PanelWorkspace.Width;i++)
			{
				Keyframe lerpd = GetColorFromKeyframeArray(i,m_PegDataArray);
				Vector4 vcolor;
				if(m_HSBMode)
				{
					vcolor= lerpd.m_ColorHSB;
					vcolor = HSBToRGB(vcolor);
				}
				else
					vcolor= lerpd.m_ColorRGB;
				int alpha	=(int)System.Math.Round(vcolor.w*255.0f, MidpointRounding.AwayFromZero);
				int red		=(int)System.Math.Round(vcolor.x*255.0f, MidpointRounding.AwayFromZero);
				int green	=(int)System.Math.Round(vcolor.y*255.0f, MidpointRounding.AwayFromZero);
                int blue = (int)System.Math.Round(vcolor.z * 255.0f, MidpointRounding.AwayFromZero);
				Color c = Color.FromArgb(alpha,red,green,blue);
				//Pen pen = new Pen(c);
				//m_OffScreenDC.DrawLine(pen,new Point(i,0),new Point(i,PanelWorkspace.Height));
				
				Keyframe maxkey = GetColorFromKeyframeArray(i,m_PegColorMaxTestArray);
				Color m;
				if(m_HSBMode)
				{
					Vector4 mcol = maxkey.m_ColorHSB;
					mcol = HSBToRGB(mcol);

                    m = Color.FromArgb(alpha, (int)System.Math.Round(mcol.x * 255.0f + 0.5f), (int)System.Math.Round(mcol.y * 255.0f + 0.5f), (int)System.Math.Round(mcol.z * 255.0f + 0.5f));
				}
				else
                    m = Color.FromArgb(alpha, (int)System.Math.Round(maxkey.m_ColorRGB.x * 255.0f, MidpointRounding.AwayFromZero), (int)System.Math.Round(maxkey.m_ColorRGB.y * 255.0f, MidpointRounding.AwayFromZero), (int)System.Math.Round(maxkey.m_ColorRGB.z * 255.0f, MidpointRounding.AwayFromZero));
			

				LinearGradientBrush linGraBrush = new LinearGradientBrush(new Point(0,0),new Point(0,PanelWorkspace.Height),c,m);
				m_OffScreenDC.FillRectangle(linGraBrush,i,0,1,PanelWorkspace.Height);
			}
		}
		private void CreateGradientRGB()
		{
            List<Keyframe> keyData = m_KeyframeData;
			ColorBlend cBlend;
			Keyframe keyA = keyData[0] as Keyframe;
			int cblendIndx=0;
			int cnt = Math.Max(keyData.Count+1,2);
			cBlend = new ColorBlend(cnt);

			for(int i=0;i<keyData.Count;i++)
			{
				Keyframe key = keyData[i] as Keyframe;
				int alpha	=(int)System.Math.Round(key.m_ColorRGB.w*255.0f, MidpointRounding.AwayFromZero);
				int red		=(int)System.Math.Round(key.m_ColorRGB.x*255.0f, MidpointRounding.AwayFromZero);
				int green	=(int)System.Math.Round(key.m_ColorRGB.y*255.0f, MidpointRounding.AwayFromZero);
                int blue = (int)System.Math.Round(key.m_ColorRGB.z * 255.0f, MidpointRounding.AwayFromZero);
				keyA=key;
				float percent = key.m_Time/(m_XRange.y - m_XRange.x);

				cBlend.Colors[cblendIndx] = Color.FromArgb(alpha,red,green,blue);
				cBlend.Positions[cblendIndx] = percent;
				if( (cblendIndx==0) && (percent!=0.0f) )
					cBlend.Positions[cblendIndx]=0;
				cblendIndx++;

			}
			//Fill in last key
			int lindx = Math.Max(keyData.Count-1,1);
			cBlend.Colors[cblendIndx ]= Color.FromArgb((int)System.Math.Round(keyA.m_ColorRGB.w*255.0f, MidpointRounding.AwayFromZero),(int)System.Math.Round(keyA.m_ColorRGB.x*255.0f, MidpointRounding.AwayFromZero),(int)System.Math.Round(keyA.m_ColorRGB.y*255.0f, MidpointRounding.AwayFromZero),(int)System.Math.Round(keyA.m_ColorRGB.z*255.0f, MidpointRounding.AwayFromZero));
			cBlend.Positions[cblendIndx ] =1.0f;

			LinearGradientBrush linGrBrush = new LinearGradientBrush(new Point(0,10),new Point(PanelWorkspace.Width,10),Color.Black,Color.Black);
			linGrBrush.InterpolationColors=cBlend;

			m_OffScreenDC.FillRectangle(linGrBrush,0,0,PanelWorkspace.Width,PanelWorkspace.Height);

		}
		private void CreateGradient()
		{
			//Draw to the offscreen bitmap
			//Checkerboard
			System.Drawing.Drawing2D.HatchBrush cbrush = new System.Drawing.Drawing2D.HatchBrush(System.Drawing.Drawing2D.HatchStyle.LargeCheckerBoard,Color.FromArgb(204,204,204),Color.FromArgb(255,255,255));
			m_OffScreenDC.FillRectangle(cbrush,0,0,PanelWorkspace.Width,PanelWorkspace.Height);

			if(m_PegDataArray.Count>=1)
			{

				if(m_MinMaxMode)
					CreateGradientMinMax();
				else
					if(!m_HSBMode)
						CreateGradientRGB();
					else
						CreateGradientHSB();
			}
			Graphics.FromHwnd(PanelWorkspace.Handle).DrawImage(m_OffScreenBmp,0,0);
			DrawIcons();
			
		}
		private void DrawIcons()
		{
			foreach(KeyPeg peg in m_PegColorArray)
			{
				if(peg.m_SelIcon == null)
					continue;
				Graphics g = Graphics.FromHwnd(peg.m_SelIcon.Handle);
				if(m_SelPegArray.Contains(peg))
					g.DrawImage(m_CPegSelectedBmp,new Point(0,0));
				else
					g.DrawImage(m_CPegUnSelectedBmp,new Point(0,0));
			}
			foreach(KeyPeg peg in m_PegAlphaArray)
			{
				if(peg.m_SelIcon == null)
					continue;
				Graphics g = Graphics.FromHwnd(peg.m_SelIcon.Handle);
				if(m_SelPegArray.Contains(peg))
					g.DrawImage(m_APegSelectedBmp,new Point(0,0));
				else
					g.DrawImage(m_APegUnSelectedBmp,new Point(0,0));
			}

		}
		private void PanelWorkspace_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			CreateGradient();
		}

		private void ColorGradientControl_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			DrawLabels(e.Graphics);
			DrawIcons();
		}

		public void ShowColorPicker(KeyPeg peg)
		{
			ColorPickerDialog dlg = new ColorPickerDialog();
            int red = (int)System.Math.Round(peg.m_KeyData.m_ColorRGB.x * 255.0f, MidpointRounding.AwayFromZero);
            int green = (int)System.Math.Round(peg.m_KeyData.m_ColorRGB.y * 255.0f, MidpointRounding.AwayFromZero);
            int blue = (int)System.Math.Round(peg.m_KeyData.m_ColorRGB.z * 255.0f, MidpointRounding.AwayFromZero);
			dlg.CurrentColor = Color.FromArgb(255,red,green,blue);
			
			dlg.ColorChanged += new ColorSelectedEventHandler(OnColorChanged);
			dlg.UseAlpha=false;
			
			if (dlg.ShowDialog() == DialogResult.OK)
			{
							
			}
		}
		public void ShowAlphaPicker(KeyPeg peg)
		{
			OpacitySlider dlg = new OpacitySlider();
            dlg.Value = (int)System.Math.Round(peg.m_KeyData.m_ColorRGB.w * 255.0f, MidpointRounding.AwayFromZero);
			Point pos = PointToScreen(peg.m_Panel.Location);
			pos.X = Math.Max(0,pos.X-(dlg.Width/4));
			pos.Y -= dlg.Height;
			dlg.Location = pos;
			dlg.OnValueChanged += new OpacitySlider.ValueChangedEventHandler(OnAlphaChanged);
			dlg.ShowDialog(this);
		}
		private void KeyPeg_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
		{

			Panel pan = sender as Panel;
			KeyPeg peg = pan.Tag as KeyPeg;

			if(e.Button == MouseButtons.Left)
			{
				m_MouseDownAnchor = Cursor.Position;
				m_SelPegArray.Clear();
				m_SelPegArray.Add(peg);
				if(!peg.m_IsMidKey)
					ShowMidpoints();
				UpdateSelected();

				if( (e.Clicks>=2) && (!peg.m_IsMidKey))
					if(peg.m_IsAlphaKey)
						ShowAlphaPicker(peg);
					else
						ShowColorPicker(peg);
			}
			if(e.Button == MouseButtons.Right)
			{
				m_SelPegArray.Clear();
				m_SelPegArray.Add(peg);
				if(!peg.m_IsMidKey)
					ShowMidpoints();
				UpdateSelected();
			}

		}
		
		private void OnAlphaChanged(object sender,OpacitySlider.ValueEventArgs e) 
		{
			foreach(KeyPeg peg in m_SelPegArray)
			{
				float alpha = e.Value;
				peg.m_KeyData.m_ColorRGB.w = ((float)e.Value)/255.0f;
				peg.m_KeyData.m_ColorHSB.w = ((float)e.Value)/255.0f;
				UpdatePeg(peg);
				SortPegColorArray();
				ShowMidpoints();
				UpdateMidPoints(m_PegColorArray);
				MergeKeyframes();
				CreateGradient();
				if(OnPegColorChanged!=null)
					OnPegColorChanged(this,new ColorGradientEventArgs(peg));
			}
		}

		private void OnColorChanged(object sender, ColorSelectedEventArgs e) 
		{
			foreach(KeyPeg peg in m_SelPegArray)
			{
				Color c = e.Color;
				peg.m_KeyData.m_ColorRGB.x = ((float)c.R)/255.0f;
				peg.m_KeyData.m_ColorRGB.y = ((float)c.G)/255.0f;
				peg.m_KeyData.m_ColorRGB.z = ((float)c.B)/255.0f;
				peg.m_KeyData.m_ColorRGB.w = 1.0f;
				peg.m_KeyData.m_ColorHSB = RGBToHSB(peg.m_KeyData.m_ColorRGB);
				UpdatePeg(peg);
				SortPegColorArray();
				ShowMidpoints();
				UpdateMidPoints(m_PegColorArray);
				MergeKeyframes();
				CreateGradient();
				if(OnPegColorChanged!=null)
					OnPegColorChanged(this,new ColorGradientEventArgs(peg));
			}
		}

		private void KeyPeg_MouseUp(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			if(e.Button == MouseButtons.Left)
			{
			}
		}
		private void KeyPeg_MouseMove(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			//see if we are scrubbing
			if(e.Button == MouseButtons.Left)
			{
				m_MouseMoveDelta.X = m_MouseDownAnchor.X-Cursor.Position.X;
				m_MouseMoveDelta.Y = m_MouseDownAnchor.Y-Cursor.Position.Y;
				m_MouseDownAnchor = Cursor.Position;

				MoveSelectedPegs();
				if(m_MouseMoveDelta.X!=0)
				{
					Panel pan =sender as Panel;
					KeyPeg peg = pan.Tag as KeyPeg;
					SortPegColorArray();
					SortPegAlphaArray();
					UpdateMidPoints(m_PegColorArray);
					UpdateMidPoints(m_PegAlphaArray);
					if(!peg.m_IsMidKey)
						ShowMidpoints();
					MergeKeyframes();
					CreateGradient();
				}
				
			}
		}

		private void PegMenu_Delete_Click(object sender, System.EventArgs e)
		{
			DelSelectedKeys();
		}

		private void PanelColorScrub_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			if(e.Button == MouseButtons.Left)
				if(e.Clicks>=2)
				{
					Panel pan = sender as Panel;
					Point pos = pan.PointToClient(Cursor.Position);
					Keyframe lerpd = GetColorFromKeyframeArray(pos.X,m_PegDataArray);
					Vector4 color = lerpd.m_ColorRGB;
					KeyPeg peg = AddColorKey(pos.X,new Vector4(color.x,color.y,color.z,1.0f));
					UpdateSelected();
					ShowColorPicker(peg);
				}
		}

		private void PanelAlphaScrub_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			if(e.Button == MouseButtons.Left)
				if(e.Clicks>=2)
				{
					Panel pan = sender as Panel;
					Point pos = pan.PointToClient(Cursor.Position);
					Keyframe lerpd = GetColorFromKeyframeArray(pos.X,m_PegDataArray);
					Vector4 color = lerpd.m_ColorRGB;
					KeyPeg peg = AddAlphaKey(pos.X,new Vector4(1.0f,1.0f,1.0f,color.w));
					UpdateSelected();
					ShowAlphaPicker(peg);
				}
		}

		private void PegMenu_Popup(object sender, System.EventArgs e)
		{
			Panel pan = PegMenu.SourceControl as Panel;
			KeyPeg peg = pan.Tag as KeyPeg;
			PegMenu_Delete.Enabled=false;
			if(m_SelPegArray.Contains(peg) && !peg.m_IsLocked)
				PegMenu_Delete.Enabled=true;
				
		}
	}
}
