using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;


namespace ragKeyframeEditorControl
{
	/// <summary>
	/// Summary description for UserControl1.
	/// </summary>
	public class KeyframeEditor : System.Windows.Forms.UserControl
	{
        private ragKeyframeEditorControl.ptxSlider XValueSlider;
        private ragKeyframeEditorControl.ptxSlider YValueSlider;
		private System.Windows.Forms.Button AddPointButton;
		private System.Windows.Forms.Panel KeyframeControlPanel;
		private System.Windows.Forms.Panel KeyframePanel;
		private System.Windows.Forms.HScrollBar XScrollBar;
		private System.Windows.Forms.Panel ControlPanel;
		private ragKeyframeEditorControl.KeyframeCurveEditor keyframeCurveEditor1;
        private ragKeyframeEditorControl.RagVScrollBar YRagScrollBar;
        private ragKeyframeEditorControl.RagVScrollBar YZoomBar;
		private System.Windows.Forms.Button SnapButton;
		private System.Windows.Forms.Label NormalFont;
		private System.Windows.Forms.Label BoldFont;
        private Label label2;
        private Label label1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public KeyframeEditor()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			SetStyle(ControlStyles.DoubleBuffer, true);
			SetStyle(ControlStyles.AllPaintingInWmPaint, true);
			SetStyle(ControlStyles.UserPaint, true);
			SetStyle(ControlStyles.SupportsTransparentBackColor, true);
			SetStyle(ControlStyles.ContainerControl, true);

			// TODO: Add any initialization after the InitComponent call
			keyframeCurveEditor1.m_Parent = this;
			InitChannels();
		/*
			//debugging
			m_ChannelInfo[0].m_Show=true;
			m_ChannelInfo[0].m_Enabled=true;
			m_ChannelInfo[0].m_Label="Channel X";
			m_ChannelInfo[0].m_Color = Color.Red;
			m_ChannelInfo[0].m_Curve = new  KeyframeCurveEditor.KeyCurve();
			m_ChannelInfo[0].m_Curve.m_Color = m_ChannelInfo[0].m_Color;
			keyframeCurveEditor1.AddCurve(m_ChannelInfo[0].m_Curve);
			m_ChannelInfo[0].m_Curve.AddKey(0.0f,0.0f);
			m_ChannelInfo[0].m_Curve.AddKey(0.5f,5.0f);
			m_ChannelInfo[0].m_CheckBox.Visible=m_ChannelInfo[0].m_Show;
			m_ChannelInfo[0].m_CheckBox.Text=m_ChannelInfo[0].m_Label;
			m_ChannelInfo[0].m_CheckBox.ForeColor =m_ChannelInfo[0].m_Color;
            
			m_ChannelInfo[1].m_Show=true;
			m_ChannelInfo[1].m_Enabled=true;
			m_ChannelInfo[1].m_Label="Channel Y";
			m_ChannelInfo[1].m_Color = Color.Green;
			m_ChannelInfo[1].m_Curve = new  KeyframeCurveEditor.KeyCurve();
			m_ChannelInfo[1].m_Curve.m_Color = m_ChannelInfo[1].m_Color;
			keyframeCurveEditor1.AddCurve(m_ChannelInfo[1].m_Curve);
			m_ChannelInfo[1].m_Curve.AddKey(0.0f,-10.0f);
			m_ChannelInfo[1].m_Curve.AddKey(1.0f,10.0f);
			m_ChannelInfo[1].m_CheckBox.Visible=m_ChannelInfo[1].m_Show;
			m_ChannelInfo[1].m_CheckBox.Text=m_ChannelInfo[1].m_Label;
			m_ChannelInfo[1].m_CheckBox.ForeColor =m_ChannelInfo[1].m_Color;

			m_ChannelInfo[2].m_Show=true;
			m_ChannelInfo[2].m_Enabled=true;
			m_ChannelInfo[2].m_Label="Channel Z";
			m_ChannelInfo[2].m_Color = Color.Blue;
			m_ChannelInfo[2].m_Curve = new  KeyframeCurveEditor.KeyCurve();
			m_ChannelInfo[2].m_Curve.m_Color = m_ChannelInfo[1].m_Color;
			keyframeCurveEditor1.AddCurve(m_ChannelInfo[2].m_Curve);
			m_ChannelInfo[2].m_Curve.AddKey(0.0f,-4.0f);
			m_ChannelInfo[2].m_Curve.AddKey(1.0f,5.0f);
			m_ChannelInfo[2].m_CheckBox.Visible=m_ChannelInfo[1].m_Show;
			m_ChannelInfo[2].m_CheckBox.Text=m_ChannelInfo[1].m_Label;
			m_ChannelInfo[2].m_CheckBox.ForeColor =m_ChannelInfo[1].m_Color;

			m_ChannelInfo[3].m_Show=false;
			m_ChannelInfo[3].m_Enabled=false;
			m_ChannelInfo[3].m_Label="Channel W";
			m_ChannelInfo[3].m_Color = Color.DarkGray;

            */
			keyframeCurveEditor1.SetMaxRange(0.0f,24.0f,-10000.0f,10000.0f);
			
		
			OrganizeLayout();
			ReMapCurves();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if( components != null )
					components.Dispose();
			}
			base.Dispose( disposing );
		}

        public void Destroy()
        {
            this.Dispose();
        }

        #region Component Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
		{
            this.ControlPanel = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.AddPointButton = new System.Windows.Forms.Button();
            this.YValueSlider = new ragKeyframeEditorControl.ptxSlider();
            this.XValueSlider = new ragKeyframeEditorControl.ptxSlider();
            this.SnapButton = new System.Windows.Forms.Button();
            this.BoldFont = new System.Windows.Forms.Label();
            this.NormalFont = new System.Windows.Forms.Label();
            this.KeyframeControlPanel = new System.Windows.Forms.Panel();
            this.XScrollBar = new System.Windows.Forms.HScrollBar();
            this.KeyframePanel = new System.Windows.Forms.Panel();
            this.keyframeCurveEditor1 = new ragKeyframeEditorControl.KeyframeCurveEditor();
            this.YRagScrollBar = new ragKeyframeEditorControl.RagVScrollBar();
            this.YZoomBar = new ragKeyframeEditorControl.RagVScrollBar();
            this.ControlPanel.SuspendLayout();
            this.KeyframeControlPanel.SuspendLayout();
            this.KeyframePanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // ControlPanel
            // 
            this.ControlPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ControlPanel.Controls.Add(this.label2);
            this.ControlPanel.Controls.Add(this.label1);
            this.ControlPanel.Controls.Add(this.AddPointButton);
            this.ControlPanel.Controls.Add(this.YValueSlider);
            this.ControlPanel.Controls.Add(this.XValueSlider);
            this.ControlPanel.Controls.Add(this.SnapButton);
            this.ControlPanel.Controls.Add(this.BoldFont);
            this.ControlPanel.Controls.Add(this.NormalFont);
            this.ControlPanel.Location = new System.Drawing.Point(35, 302);
            this.ControlPanel.Name = "ControlPanel";
            this.ControlPanel.Size = new System.Drawing.Size(700, 85);
            this.ControlPanel.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(87, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(30, 13);
            this.label2.TabIndex = 18;
            this.label2.Text = "Time";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(221, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 13);
            this.label1.TabIndex = 17;
            this.label1.Text = "Value";
            // 
            // AddPointButton
            // 
            this.AddPointButton.ForeColor = System.Drawing.SystemColors.ControlText;
            this.AddPointButton.Location = new System.Drawing.Point(408, 8);
            this.AddPointButton.Name = "AddPointButton";
            this.AddPointButton.Size = new System.Drawing.Size(88, 23);
            this.AddPointButton.TabIndex = 2;
            this.AddPointButton.Text = "Add Point";
            this.AddPointButton.Click += new System.EventHandler(this.AddPointButton_Click);
            // 
            // YValueSlider
            // 
            this.YValueSlider.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.YValueSlider.Decimals = 3;
            this.YValueSlider.Increment = 0.1F;
            this.YValueSlider.Location = new System.Drawing.Point(261, 11);
            this.YValueSlider.MaxValue = 10000F;
            this.YValueSlider.MinValue = -10000F;
            this.YValueSlider.Multiline = true;
            this.YValueSlider.Name = "YValueSlider";
            this.YValueSlider.SetValue = 0F;
            this.YValueSlider.Size = new System.Drawing.Size(77, 18);
            this.YValueSlider.TabIndex = 1;
            this.YValueSlider.Text = "0.000";
            this.YValueSlider.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.YValueSlider.Value = 0F;
            this.YValueSlider.OnValueChanged += new ragKeyframeEditorControl.ptxSlider.ptxSliderEventDelegate(this.YValueSlider_ValueChanged);
            // 
            // XValueSlider
            // 
            this.XValueSlider.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.XValueSlider.Decimals = 3;
            this.XValueSlider.Increment = 0.005F;
            this.XValueSlider.Location = new System.Drawing.Point(123, 11);
            this.XValueSlider.MaxValue = 10000F;
            this.XValueSlider.MinValue = 0F;
            this.XValueSlider.Multiline = true;
            this.XValueSlider.Name = "XValueSlider";
            this.XValueSlider.SetValue = 0F;
            this.XValueSlider.Size = new System.Drawing.Size(77, 18);
            this.XValueSlider.TabIndex = 0;
            this.XValueSlider.Text = "0.000";
            this.XValueSlider.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.XValueSlider.Value = 0F;
            this.XValueSlider.OnValueChanged += new ragKeyframeEditorControl.ptxSlider.ptxSliderEventDelegate(this.XValueSlider_ValueChanged);
            // 
            // SnapButton
            // 
            this.SnapButton.Location = new System.Drawing.Point(509, 7);
            this.SnapButton.Name = "SnapButton";
            this.SnapButton.Size = new System.Drawing.Size(88, 24);
            this.SnapButton.TabIndex = 3;
            this.SnapButton.Text = "Focus on data";
            this.SnapButton.Click += new System.EventHandler(this.SnapButton_Click);
            // 
            // BoldFont
            // 
            this.BoldFont.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BoldFont.Location = new System.Drawing.Point(120, 45);
            this.BoldFont.Name = "BoldFont";
            this.BoldFont.Size = new System.Drawing.Size(100, 23);
            this.BoldFont.TabIndex = 16;
            this.BoldFont.Text = "Bold Font";
            this.BoldFont.Visible = false;
            // 
            // NormalFont
            // 
            this.NormalFont.Location = new System.Drawing.Point(24, 48);
            this.NormalFont.Name = "NormalFont";
            this.NormalFont.Size = new System.Drawing.Size(100, 23);
            this.NormalFont.TabIndex = 15;
            this.NormalFont.Text = "Normal Font";
            this.NormalFont.Visible = false;
            // 
            // KeyframeControlPanel
            // 
            this.KeyframeControlPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.KeyframeControlPanel.Controls.Add(this.XScrollBar);
            this.KeyframeControlPanel.Controls.Add(this.KeyframePanel);
            this.KeyframeControlPanel.Controls.Add(this.YRagScrollBar);
            this.KeyframeControlPanel.Location = new System.Drawing.Point(3, 3);
            this.KeyframeControlPanel.Name = "KeyframeControlPanel";
            this.KeyframeControlPanel.Size = new System.Drawing.Size(880, 296);
            this.KeyframeControlPanel.TabIndex = 9;
            this.KeyframeControlPanel.Resize += new System.EventHandler(this.KeyframeControlPanel_Resize);
            this.KeyframeControlPanel.Paint += new System.Windows.Forms.PaintEventHandler(this.KeyframeControlPanel_Paint);
            // 
            // XScrollBar
            // 
            this.XScrollBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.XScrollBar.LargeChange = 20;
            this.XScrollBar.Location = new System.Drawing.Point(72, 272);
            this.XScrollBar.Name = "XScrollBar";
            this.XScrollBar.Size = new System.Drawing.Size(784, 16);
            this.XScrollBar.TabIndex = 8;
            this.XScrollBar.Visible = false;
            this.XScrollBar.ValueChanged += new System.EventHandler(this.XScrollBar_ValueChanged);
            // 
            // KeyframePanel
            // 
            this.KeyframePanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.KeyframePanel.CausesValidation = false;
            this.KeyframePanel.Controls.Add(this.keyframeCurveEditor1);
            this.KeyframePanel.Location = new System.Drawing.Point(72, 40);
            this.KeyframePanel.Name = "KeyframePanel";
            this.KeyframePanel.Size = new System.Drawing.Size(784, 232);
            this.KeyframePanel.TabIndex = 7;
            // 
            // keyframeCurveEditor1
            // 
            this.keyframeCurveEditor1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.keyframeCurveEditor1.BackColor = System.Drawing.SystemColors.Window;
            this.keyframeCurveEditor1.Location = new System.Drawing.Point(0, 0);
            this.keyframeCurveEditor1.Name = "keyframeCurveEditor1";
            this.keyframeCurveEditor1.Size = new System.Drawing.Size(784, 232);
            this.keyframeCurveEditor1.TabIndex = 0;
            // 
            // YRagScrollBar
            // 
            this.YRagScrollBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.YRagScrollBar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.YRagScrollBar.DrawAsSlider = false;
            this.YRagScrollBar.LargeChange = 10F;
            this.YRagScrollBar.Location = new System.Drawing.Point(856, 40);
            this.YRagScrollBar.Maximum = 100F;
            this.YRagScrollBar.Minimum = -100F;
            this.YRagScrollBar.Name = "YRagScrollBar";
            this.YRagScrollBar.ReverseRange = true;
            this.YRagScrollBar.ScrubBarSize = 20;
            this.YRagScrollBar.ScrubHeight = 203;
            this.YRagScrollBar.ScrubScalar = 0.1F;
            this.YRagScrollBar.ShowDownArrow = true;
            this.YRagScrollBar.ShowUpArrow = true;
            this.YRagScrollBar.Size = new System.Drawing.Size(16, 235);
            this.YRagScrollBar.SmallChange = 5F;
            this.YRagScrollBar.TabIndex = 14;
            this.YRagScrollBar.Value = -55F;
            this.YRagScrollBar.ValueChanged += new System.EventHandler(this.YRagScrollBar_ValueChanged);
            // 
            // YZoomBar
            // 
            this.YZoomBar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.YZoomBar.BackColor = System.Drawing.SystemColors.Control;
            this.YZoomBar.DrawAsSlider = true;
            this.YZoomBar.LargeChange = 10F;
            this.YZoomBar.Location = new System.Drawing.Point(883, 203);
            this.YZoomBar.Maximum = 100F;
            this.YZoomBar.Minimum = -100F;
            this.YZoomBar.Name = "YZoomBar";
            this.YZoomBar.ReverseRange = false;
            this.YZoomBar.ScrubBarSize = 10;
            this.YZoomBar.ScrubHeight = 96;
            this.YZoomBar.ScrubScalar = 0.01F;
            this.YZoomBar.ShowDownArrow = false;
            this.YZoomBar.ShowUpArrow = false;
            this.YZoomBar.Size = new System.Drawing.Size(16, 96);
            this.YZoomBar.SmallChange = 5F;
            this.YZoomBar.TabIndex = 14;
            this.YZoomBar.Value = 0F;
            this.YZoomBar.ValueChanged += new System.EventHandler(this.YZoomBar_ValueChanged);
            // 
            // KeyframeEditor
            // 
            this.Controls.Add(this.YZoomBar);
            this.Controls.Add(this.ControlPanel);
            this.Controls.Add(this.KeyframeControlPanel);
            this.Name = "KeyframeEditor";
            this.Size = new System.Drawing.Size(904, 390);
            this.ControlPanel.ResumeLayout(false);
            this.ControlPanel.PerformLayout();
            this.KeyframeControlPanel.ResumeLayout(false);
            this.KeyframePanel.ResumeLayout(false);
            this.ResumeLayout(false);

		}
		#endregion

			
		#region Variables
		
		class ChannelInfo
		{
			public bool m_Show;
			public bool m_Enabled;
			public Color m_Color;
			public string m_Label;
			public System.Windows.Forms.CheckBox m_CheckBox;
			public KeyframeCurveEditor.KeyCurve  m_Curve;

			public int UpdateVisual(int left)
			{
				if(m_Show)
				{
					m_CheckBox.Visible=true;
					m_CheckBox.Text = m_Label;

                    Font boldFont = new Font(m_CheckBox.Font, FontStyle.Bold);

					Size size = Graphics.FromHwnd(m_CheckBox.Handle).MeasureString(m_CheckBox.Text,boldFont).ToSize();
                    boldFont.Dispose();

					m_CheckBox.Width = (int)(size.Width * 1.2f) + 25; // assume bold is 20% wider than what MS says
					m_CheckBox.Checked=m_Enabled;
					//m_CheckBox.Top=8;
					m_CheckBox.Left=left;
					m_CheckBox.ForeColor = m_Color;
					return m_CheckBox.Width;
				}
				else 
				{	
					m_CheckBox.Visible=false;
					return 0;
				}
			}
		};
		ChannelInfo[] m_ChannelInfo = new ChannelInfo[4];
		float m_Precision = 0.5f;
		float m_CenterLine = 0.0f;
		bool  m_AsColorGrad = false;

		#endregion
		
		#region Properties
		[Description("Display as a Color Gradient Editor"),Category("Appearance")]
		public bool AsColorGrad
		{	
			get 
			{ 
					return m_AsColorGrad;
			}
			set 
			{
				m_AsColorGrad = value;
			}
		}

		#endregion


		public List<KeyframeCurveEditor.RawCurve> Value
		{
			get {
                List<KeyframeCurveEditor.RawCurve> ret = new List<KeyframeCurveEditor.RawCurve>(4);
				foreach(ChannelInfo c in m_ChannelInfo) 
				{
					if(c.m_Curve!= null)
					{
                        KeyframeCurveEditor.RawCurve curveData = new KeyframeCurveEditor.RawCurve();
						foreach(KeyframeCurveEditor.KeyValue val in c.m_Curve.m_Values) 
						{
							curveData.Add(new KeyframeCurveEditor.Vector2(val.x, val.y));
						}
						ret.Add(curveData);
					}
				}
				return ret;
			}
			set {
				for(int i = 0; i < value.Count; i++) 
				{
					KeyframeCurveEditor.KeyCurve curve = m_ChannelInfo[i].m_Curve;
					if (curve == null) 
					{
						m_ChannelInfo[i].m_Curve = new KeyframeCurveEditor.KeyCurve();
						m_ChannelInfo[i].m_Curve.m_Color=m_ChannelInfo[i].m_Color;
						m_ChannelInfo[i].m_Curve.m_Show=m_ChannelInfo[i].m_Show;
						m_ChannelInfo[i].m_Curve.m_Enabled=m_ChannelInfo[i].m_Enabled;
						keyframeCurveEditor1.AddCurve(m_ChannelInfo[i].m_Curve);
						curve = m_ChannelInfo[i].m_Curve;
					}
					curve.Clear();
                    for (int pointIdx = 0; pointIdx < value[i].Count; pointIdx++)
                    {
                        curve.AddKey(value[i][pointIdx].X, value[i][pointIdx].Y, false);
                    }
				}
			}
		}

        public event EventHandler ValueChanged;

		public void UpdateData()
		{
            OnValueChanged();
		}

        protected virtual void OnValueChanged()
        {
            if (ValueChanged != null)
            {
                System.EventArgs e = new System.EventArgs();
                ValueChanged(this, e);
            }
        }
		
		public void DetermineAutoZoom()
		{
			//default zoom control
			YZoomBar.Maximum=keyframeCurveEditor1.m_MaxYRange.Y-keyframeCurveEditor1.m_MaxYRange.X;
			YZoomBar.Minimum = m_Precision;
			YZoomBar.Value = YZoomBar.Maximum;

			//Zoom in Y
			KeyframeCurveEditor.Vector2 range = keyframeCurveEditor1.FindValueYRange();
			range.X+=range.X*0.20f;
			range.Y+=range.Y*0.20f;

			float maxrange = keyframeCurveEditor1.m_MaxYRange.Y-keyframeCurveEditor1.m_MaxYRange.X;
			float datarange = range.Y-range.X;
			float center = (datarange*0.5f)+range.X;

			

			if(datarange > YZoomBar.Maximum)
				datarange =YZoomBar.Maximum;
			else
				if(datarange< YZoomBar.Minimum)
				datarange = YZoomBar.Minimum;
			
			if((datarange/maxrange)<0.5f)
			{
				YZoomBar.Value = datarange;
			}
			
			m_CenterLine=center;

			UpdateVisibleRange();
			UpdateScrollBars();
		}
		public void UpdateScrollBars()
		{
			//Y Range
			if( (YZoomBar.Value!=YZoomBar.Maximum))
				YRagScrollBar.Visible=true;
			else
				YRagScrollBar.Visible=false;

			float TYmin = keyframeCurveEditor1.m_MaxYRange.X;
			float TYmax = keyframeCurveEditor1.m_MaxYRange.Y;
			float Ymin = keyframeCurveEditor1.m_VisYRange.X;
			float Ymax = keyframeCurveEditor1.m_VisYRange.Y;
			float maxrange = TYmax-TYmin;
			float viewrange = Ymax-Ymin;
			//Calc centerline
			float ycenter = m_CenterLine;

			//Set the scrollbar ranges and step size
			YRagScrollBar.ScrubScalar=(YZoomBar.Value/YZoomBar.Maximum);
			int scrubBarHeight = (int)((viewrange/maxrange)*(float)YRagScrollBar.ScrubHeight);
			scrubBarHeight = System.Math.Max(scrubBarHeight,10);

			YRagScrollBar.Maximum = TYmax-(viewrange*0.5f);
			YRagScrollBar.Minimum = TYmin+(viewrange*0.5f);
			YRagScrollBar.Value=ycenter;
			YRagScrollBar.ScrubBarSize=scrubBarHeight;

			float r = (YZoomBar.Value+YZoomBar.Minimum)/(YZoomBar.Maximum-YZoomBar.Minimum);
			YZoomBar.ScrubScalar = (YZoomBar.Value/YZoomBar.Maximum);


		}

		public void UpdateVisibleRange()
		{
			//Y range
			float TYmin = keyframeCurveEditor1.m_MaxYRange.X;
			float TYmax = keyframeCurveEditor1.m_MaxYRange.Y;
			float Xmin = keyframeCurveEditor1.m_MaxXRange.X;
			float Xmax = keyframeCurveEditor1.m_MaxXRange.Y;
			float Ymin;
			float Ymax;

			float viewrange = YZoomBar.Value;
			float newycenter = m_CenterLine;

			Ymax = newycenter+ (viewrange*0.5f);
			Ymin = Ymax-(viewrange);

			
			//clamp to true range 
			if(Ymin<TYmin)
			{
				Ymin = TYmin;
				Ymax = Ymin + viewrange;
			}
			if(Ymax>TYmax)
			{
				Ymax=TYmax;
				Ymin = Ymax- viewrange;
			}


			//need to calc new centerline  -in case of clamping
			m_CenterLine = (Ymax+Ymin)*0.5f;
			
			
			YRagScrollBar.Maximum = TYmax-(viewrange*0.5f);
			YRagScrollBar.Minimum = TYmin+(viewrange*0.5f);
			YRagScrollBar.Value=m_CenterLine;
			
			
			keyframeCurveEditor1.SetVisibleRange(Xmin,Xmax,Ymin,Ymax);
			this.KeyframeControlPanel.Invalidate();
		}

		public void InitChannels()
		{
			int checkboxTop = 34;
			for(int i=0;i<4;i++)
			{
				m_ChannelInfo[i] = new ChannelInfo();
				m_ChannelInfo[i].m_Show=false;
				m_ChannelInfo[i].m_Enabled=false;
				m_ChannelInfo[i].m_Label="";
				m_ChannelInfo[i].m_CheckBox = new System.Windows.Forms.CheckBox();
				m_ChannelInfo[i].m_CheckBox.Location = new System.Drawing.Point(8, checkboxTop);
				m_ChannelInfo[i].m_CheckBox.Size = new System.Drawing.Size(80, 32);
				m_ChannelInfo[i].m_CheckBox.Tag = i;
				m_ChannelInfo[i].m_CheckBox.CheckedChanged += new System.EventHandler(this.FilterBox_CheckedChanged);

				ControlPanel.Controls.Add(m_ChannelInfo[i].m_CheckBox);
			}
		}

		public void ConfigureEditor(string[] labels,System.Drawing.Color[] colors, float xMin, float xMax,float yMin, float yMax,float xDelta,float yDelta)
		{
			for(int i = 0; i < 4; i++) 
			{
				if (labels[i] != null && labels[i] != "")
				{
					m_ChannelInfo[i].m_Show = true;
					m_ChannelInfo[i].m_Enabled = true;
					m_ChannelInfo[i].m_Label = labels[i];
					m_ChannelInfo[i].m_Color = colors[i];
					m_ChannelInfo[i].m_CheckBox.Checked = true;
				}
				else {
					m_ChannelInfo[i].m_Show = false;
					m_ChannelInfo[i].m_Enabled = false;
					m_ChannelInfo[i].m_Label = "";
					m_ChannelInfo[i].m_CheckBox.Checked = false;
				}
			}
			keyframeCurveEditor1.SetMaxRange(xMin,xMax,yMin,yMax);
			OrganizeLayout();
			ReMapCurves();
		}

		protected void OrganizeLayout()
		{
			int left=8;
			for(int i=0;i<4;i++)
			{
				left+=m_ChannelInfo[i].UpdateVisual(left);
			}
		}

		public void DisplayCurrentCurve(KeyframeCurveEditor.KeyCurve curve)
		{
			
			for(int i=0;i<4;i++)
				if(m_ChannelInfo[i].m_Curve == curve)
					m_ChannelInfo[i].m_CheckBox.Font = BoldFont.Font;
				else
					m_ChannelInfo[i].m_CheckBox.Font = NormalFont.Font;

		}
		public void ReMapCurves()
		{
			for(int i=0;i<4;i++)
			{
				if(keyframeCurveEditor1.m_CurveList.Count>i)
				{
					KeyframeCurveEditor.KeyCurve curve = keyframeCurveEditor1.m_CurveList[i] as KeyframeCurveEditor.KeyCurve;
					m_ChannelInfo[i].m_Curve=curve;
					m_ChannelInfo[i].m_Curve.m_Show=m_ChannelInfo[i].m_Show;
					m_ChannelInfo[i].m_Curve.m_Enabled=m_ChannelInfo[i].m_Enabled;
					m_ChannelInfo[i].m_Curve.m_Color=m_ChannelInfo[i].m_Color;
					curve.m_Enabled = m_ChannelInfo[i].m_CheckBox.Checked;
				}
			}
			DetermineAutoZoom();
		}
		
		public void SetSelected(KeyframeCurveEditor.KeyValue val)
		{
            this.XValueSlider.OnValueChanged -= this.XValueSlider_ValueChanged;
            this.YValueSlider.OnValueChanged -= this.YValueSlider_ValueChanged;

			//Link up the sliders
			this.XValueSlider.Tag = val;
			this.YValueSlider.Tag = val;
			this.XValueSlider.Value = val.x;
			this.YValueSlider.Value = val.y;
			this.XValueSlider.Enabled = true;
			this.YValueSlider.Enabled = true;
			this.AddPointButton.Enabled=false;

            this.XValueSlider.OnValueChanged += this.XValueSlider_ValueChanged;
            this.YValueSlider.OnValueChanged += this.YValueSlider_ValueChanged;
        }

		public void SetNoSelection()
		{
			this.XValueSlider.Tag = null;
			this.YValueSlider.Tag = null;
			this.XValueSlider.Enabled = true;
			this.YValueSlider.Enabled = true;
			this.AddPointButton.Enabled=true;
		}

		public void SetInvalidSelection()
		{
			this.XValueSlider.Tag = null;
			this.YValueSlider.Tag = null;
			this.XValueSlider.Enabled=false;
			this.YValueSlider.Enabled=false;
			this.AddPointButton.Enabled=false;
			this.XValueSlider.Value = 0.0f;
			this.YValueSlider.Value = 0.0f;
		}
		
		public void CalcRanges()
		{

		}
		private void keyframeCurveEditor1_Load(object sender, System.EventArgs e)
		{
			OrganizeLayout();
		}

		private void FilterBox_CheckedChanged(object sender, System.EventArgs e)
		{
			CheckBox cbx = sender as CheckBox;
			if(cbx !=null)
			{
				int indx =(int)cbx.Tag;
				m_ChannelInfo[indx].m_Enabled = cbx.Checked;
				if(m_ChannelInfo[indx].m_Curve!=null)
				{
					KeyframeCurveEditor.KeyCurve curve = m_ChannelInfo[indx].m_Curve as KeyframeCurveEditor.KeyCurve;
					curve.m_Enabled=cbx.Checked;
					curve.m_Show = m_ChannelInfo[indx].m_Show;
					if(cbx.Checked)
						curve.DeselectPegs();
					if(curve.m_Parent!=null)
						curve.m_Parent.Invalidate();
				}
				keyframeCurveEditor1.DetermineCurrentCurve();
				keyframeCurveEditor1.UpdateSelected();
			}
		}

		private void AddPointButton_Click(object sender, System.EventArgs e)
		{
			keyframeCurveEditor1.AddPoint((float)XValueSlider.Value,(float)YValueSlider.Value);
			keyframeCurveEditor1.Invalidate();
		}

		private void XValueSlider_ValueChanged(ptxSlider slider)
		{
			if(XValueSlider.Tag == null)
				return;
			KeyframeCurveEditor.KeyValue val = XValueSlider.Tag as KeyframeCurveEditor.KeyValue;
			val.Set((float)XValueSlider.Value,val.y);
			XValueSlider.Value = val.x;
			keyframeCurveEditor1.Invalidate();
			UpdateData();
		}

		private void YValueSlider_ValueChanged(ptxSlider slider)
		{
			if(YValueSlider.Tag == null)
				return;
			KeyframeCurveEditor.KeyValue val = YValueSlider.Tag as KeyframeCurveEditor.KeyValue;
			val.Set(val.x,(float)YValueSlider.Value);
			YValueSlider.Value = val.y;
			keyframeCurveEditor1.Invalidate();
			UpdateData();
		}


		private void XScrollBar_ValueChanged(object sender, System.EventArgs e)
		{
			
		/*	if(m_InvalidateXValueChangedEvent)
			{
				m_InvalidateXValueChangedEvent=false;
				return;
			}
			UpdateFromScrollBars();
			this.KeyframeControlPanel.Invalidate();
			//this.XLabelPanel.Invalidate();
			//this.keyframeCurveEditor1.Invalidate();
			this.keyframeCurveEditor1.Invalidate();
			*/
		}

		private void KeyframeControlPanel_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			this.keyframeCurveEditor1.DrawLabelsX(e.Graphics,this.KeyframePanel.Left,this.KeyframePanel.Top);
			this.keyframeCurveEditor1.DrawLabelsY(e.Graphics,this.KeyframePanel.Left,this.KeyframePanel.Top);
		}

		private void YRagScrollBar_ValueChanged(object sender, System.EventArgs e)
		{
			m_CenterLine=YRagScrollBar.Value;
			UpdateVisibleRange();
		}

		private void YZoomBar_ValueChanged(object sender, System.EventArgs e)
		{
			UpdateVisibleRange();
			UpdateScrollBars();
		}

		private void SnapButton_Click(object sender, System.EventArgs e)
		{
			DetermineAutoZoom();
		}

        private void KeyframeControlPanel_Resize(object sender, EventArgs e)
        {
            // repaint the whole thing
            keyframeCurveEditor1.ReSize();
            KeyframeControlPanel.Invalidate();
        }
	}
}

