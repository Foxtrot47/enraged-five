using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace ragKeyframeEditorControl
{
	/// <summary>
	/// Summary description for OpacitySlider.
	/// </summary>
	public class OpacitySlider : System.Windows.Forms.Form
	{
		private System.Windows.Forms.TrackBar m_Slider;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public OpacitySlider()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.m_Slider = new System.Windows.Forms.TrackBar();
			((System.ComponentModel.ISupportInitialize)(this.m_Slider)).BeginInit();
			this.SuspendLayout();
			// 
			// m_Slider
			// 
			this.m_Slider.Dock = System.Windows.Forms.DockStyle.Fill;
			this.m_Slider.LargeChange = 1;
			this.m_Slider.Location = new System.Drawing.Point(0, 0);
			this.m_Slider.Maximum = 255;
			this.m_Slider.Name = "m_Slider";
			this.m_Slider.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
			this.m_Slider.Size = new System.Drawing.Size(368, 29);
			this.m_Slider.TabIndex = 0;
			this.m_Slider.TickStyle = System.Windows.Forms.TickStyle.None;
			this.m_Slider.ValueChanged += new System.EventHandler(this.m_Slider_ValueChanged);
			// 
			// OpacitySlider
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(368, 29);
			this.Controls.Add(this.m_Slider);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "OpacitySlider";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
			this.Text = "OpacitySlider";
			this.Load += new System.EventHandler(this.OpacitySlider_Load);
			((System.ComponentModel.ISupportInitialize)(this.m_Slider)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		public class ValueEventArgs : EventArgs 
		{
		
			private int m_Value;
		
			public int Value
			{
				get { return m_Value; }
			}

			internal ValueEventArgs( int val) 
			{
				m_Value= val;
			}

		}


		private void m_Slider_ValueChanged(object sender, System.EventArgs e)
		{
			this.Text="Opacity: "+m_Slider.Value.ToString();
			if(OnValueChanged!=null)
				OnValueChanged(this, new ValueEventArgs(m_Slider.Value));
		}

		private void OpacitySlider_Load(object sender, System.EventArgs e)
		{
			this.Text="Opacity: "+m_Slider.Value.ToString();
		}

		public delegate void ValueChangedEventHandler( object sender, ValueEventArgs e );
		public event ValueChangedEventHandler OnValueChanged;

		#region Properties
		public int Value
		{	
			get 
			{ 
				return m_Slider.Value;
			}
			set 
			{
				m_Slider.Value=value;
			}
		}
		#endregion

	}
}
