using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;

namespace ragKeyframeEditorControl
{
	/// <summary>
	/// Summary description for KeyframeCurveEditor.
	/// </summary>
	public class KeyframeCurveEditor : System.Windows.Forms.UserControl //: ragCurveEditor.CurveEditor
	{
		#region Variables
		
		public bool m_KeyShiftState=false;
		public bool m_KeyControlState=false;
		public bool m_BoundSelecting=false;
		public bool m_LockXAxisScrub=false;
		public bool m_LockYAxisScrub=false;

		public float m_MinSizeForAdaptation = 100.0f;
		public float m_MinAdaptiveSize = 1.0f;
		public Vector2 m_VisXRange = new Vector2(0.0f,1.0f);
		public Vector2 m_VisYRange = new Vector2(-10.0f,10.0f);
		public Vector2 m_MaxXRange = new Vector2(0.0f, 1.0f);
		public Vector2 m_MaxYRange = new Vector2(-10.0f, 10.0f);
		public Vector2 m_ScaleFactor = new Vector2(1.0f,1.0f);
		public Point[] m_SelectionBox = new Point[2];
		public Point m_OrigSize = new Point(0,0);
        public KeyCurveList m_CurveList = new KeyCurveList();
        // unused: public ArrayList m_IOData = new ArrayList();
        List<KeyCurveList> m_UndoCurveList = new List<KeyCurveList>();
        List<KeyCurveList> m_RedoCurveList = new List<KeyCurveList>();
        List<KeyValue> m_CopyPegBuffer = new List<KeyValue>();

		public KeyframeEditor m_Parent =null;
		
		private System.Windows.Forms.ContextMenu PegMenu;  //Array of KeyCurves
		private System.Windows.Forms.MenuItem menuItemDeletePoint;
		private System.Windows.Forms.MenuItem menuItemAddPoint;
		private System.Windows.Forms.MenuItem menuItemUndo;
		private System.Windows.Forms.Label LabelFont;
		private System.Windows.Forms.Label MarkerFont;
		KeyCurve m_CurrentCurve=null;
		#endregion
		private System.Windows.Forms.MenuItem menuItemCopy;
		private System.Windows.Forms.MenuItem menuItemPaste;
        //private int m_UndoLevel = -1;
        private MenuItem menuItemredo;
        public static bool realtimeUpdateMode = false;

		#region Properties
		#endregion


		private System.ComponentModel.Container components = null;

		public KeyframeCurveEditor()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			// TODO: Add any initialization after the InitializeComponent call
			SetStyle(ControlStyles.DoubleBuffer, true);
			SetStyle(ControlStyles.AllPaintingInWmPaint, true);
			SetStyle(ControlStyles.UserPaint, true);
			SetStyle(ControlStyles.SupportsTransparentBackColor, true);
			SetStyle(ControlStyles.ContainerControl, true);
		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.PegMenu = new System.Windows.Forms.ContextMenu();
            this.menuItemUndo = new System.Windows.Forms.MenuItem();
            this.menuItemAddPoint = new System.Windows.Forms.MenuItem();
            this.menuItemDeletePoint = new System.Windows.Forms.MenuItem();
            this.menuItemCopy = new System.Windows.Forms.MenuItem();
            this.menuItemPaste = new System.Windows.Forms.MenuItem();
            this.LabelFont = new System.Windows.Forms.Label();
            this.MarkerFont = new System.Windows.Forms.Label();
            this.menuItemredo = new System.Windows.Forms.MenuItem();
            this.SuspendLayout();
            // 
            // PegMenu
            // 
            this.PegMenu.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuItemUndo,
            this.menuItemredo,
            this.menuItemAddPoint,
            this.menuItemDeletePoint,
            this.menuItemCopy,
            this.menuItemPaste});
            this.PegMenu.Popup += new System.EventHandler(this.PegMenu_Popup);
            // 
            // menuItemUndo
            // 
            this.menuItemUndo.Index = 0;
            this.menuItemUndo.Text = "Undo";
            this.menuItemUndo.Click += new System.EventHandler(this.menuItem1_Click_1);
            // 
            // menuItemAddPoint
            // 
            this.menuItemAddPoint.Index = 2;
            this.menuItemAddPoint.Text = "Add Point";
            this.menuItemAddPoint.Click += new System.EventHandler(this.menuItemAddPoint_Click);
            // 
            // menuItemDeletePoint
            // 
            this.menuItemDeletePoint.Index = 3;
            this.menuItemDeletePoint.Text = "Delete Point(s)";
            this.menuItemDeletePoint.Click += new System.EventHandler(this.menuItem1_Click);
            // 
            // menuItemCopy
            // 
            this.menuItemCopy.Index = 4;
            this.menuItemCopy.Text = "Copy";
            this.menuItemCopy.Click += new System.EventHandler(this.menuItemCopy_Click);
            // 
            // menuItemPaste
            // 
            this.menuItemPaste.Index = 5;
            this.menuItemPaste.Text = "Paste";
            this.menuItemPaste.Click += new System.EventHandler(this.menuItemPaste_Click);
            // 
            // LabelFont
            // 
            this.LabelFont.Location = new System.Drawing.Point(32, 88);
            this.LabelFont.Name = "LabelFont";
            this.LabelFont.Size = new System.Drawing.Size(100, 23);
            this.LabelFont.TabIndex = 0;
            this.LabelFont.Text = "LabelFont";
            this.LabelFont.Visible = false;
            // 
            // MarkerFont
            // 
            this.MarkerFont.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MarkerFont.Location = new System.Drawing.Point(208, 96);
            this.MarkerFont.Name = "MarkerFont";
            this.MarkerFont.Size = new System.Drawing.Size(100, 23);
            this.MarkerFont.TabIndex = 1;
            this.MarkerFont.Text = "MarkerFont";
            this.MarkerFont.Visible = false;
            // 
            // menuItemredo
            // 
            this.menuItemredo.Index = 1;
            this.menuItemredo.Text = "Redo";
            this.menuItemredo.Click += new System.EventHandler(this.menuItemredo_Click);
            // 
            // KeyframeCurveEditor
            // 
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ContextMenu = this.PegMenu;
            this.Controls.Add(this.MarkerFont);
            this.Controls.Add(this.LabelFont);
            this.Name = "KeyframeCurveEditor";
            this.Size = new System.Drawing.Size(520, 150);
            this.Load += new System.EventHandler(this.KeyframeCurveEditor_Load);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.KeyframeCurveEditor_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.KeyframeCurveEditor_MouseMove);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.KeyframeCurveEditor_KeyUp);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.KeyframeCurveEditor_KeyPress);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.KeyframeCurveEditor_Paint);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.KeyframeCurveEditor_MouseUp);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.KeyframeCurveEditor_KeyDown);
            this.ResumeLayout(false);

		}

		#endregion
        [Serializable]
		public class Vector2
		{
			public float X;
			public float Y;
			public Vector2()
			{
				X=0.0f;
				Y=0.0f;
			}
			public Vector2(float x, float y)
			{
				X=x;
				Y=y;
			}

			public void Set(float x,float y)
			{
				X=x;
				Y=y;
			}
		}

		public class Float5
		{
			public float t,x,y,z,w;

			public Float5()
			{
				t=x=y=z=w=0.0f;
			}
			public void Zero()
			{
				t=x=y=z=w=0.0f;
			}
		}

        [Serializable]
        public class RawCurve
        {
            public List<Vector2> m_Points = new List<Vector2>();

            // gets the Y value of the curve at a given time.
		    // time must between time at the index-1 keyframe and the index keyframe
            public float GetCurveValue(int index, float time)
            {
                if (index >= m_Points.Count)
                {
                    Vector2 keyframe = m_Points[m_Points.Count - 1];
                    return keyframe.Y;
                }
                else
                {
                    Vector2 keyframe = m_Points[index];
                    if (keyframe.X == time)
                    {
                        return keyframe.Y;
                    }
                    else
                    {
                        // find neighbors, lerp
                        KeyframeCurveEditor.Vector2 prev = m_Points[index - 1];

                        float dx = keyframe.X - prev.X;
                        float dy = keyframe.Y - prev.Y;

                        float t = (time - prev.X) / dx;
                        return t * dy + prev.Y;
                    }
                }
            }

            public float Count {
                get {
                    return m_Points.Count;
                }
            }

	        public Vector2 this[int index]
	        {
                get { return m_Points[index]; }
		        set { m_Points[index] = value; }
	        }

            public void Clear()
            {
                m_Points.Clear();
            }

            public void Add(Vector2 vec)
            {
                 m_Points.Add(vec);
            }

            public void Remove(int i)
            {
                m_Points.RemoveAt(i);
            }


        };

        public static List<Float5> ConvertRawCurvesToFloat5s(List<RawCurve> curves)
        {
            List<Float5> keyframes = new List<Float5>();

            int numCurves = System.Math.Min(4, curves.Count);

            int[] currKey = new int[numCurves];

            // Basic algorithm:
            // Find the smallest time T out of all the currKeys
            // create a new keyframe at this time
            // If currKey[i] is close enough to T, snap it to T
            // for each curve, get value at T.
            // increment currKey for any currKeys near T.

            bool done = false;
            while (!done)
            {
                float minT = System.Single.MaxValue;
                for (int i = 0; i < numCurves; i++)
                {
                    RawCurve curve = curves[i];
                    if (currKey[i] >= curve.Count)
                    {
                        continue;
                    }
                    Vector2 currVal = curve[currKey[i]];
                    if (currVal.X < minT)
                    {
                        minT = currVal.X;
                    }
                }
                if (minT == System.Single.MaxValue)
                {
                    done = true;
                    break;
                }

                // Now snap nearby keys onto the minT value
                float epsilon = 0.01f;
                for (int i = 0; i < numCurves; i++)
                {
                    RawCurve curve = curves[i];
                    if (currKey[i] >= curve.Count)
                    {
                        continue;
                    }
                    Vector2 currVal = curve[currKey[i]];
                    if (currVal.X < minT + epsilon)
                    {
                        currVal.X = minT;
                    }
                }

                Float5 newKey = new Float5();
                newKey.t = minT;

                float[] keyVals = new float[4];

                for (int i = 0; i < numCurves; i++)
                {
                    if (numCurves > i)
                    {
                        RawCurve curve = curves[i];
                        keyVals[i] = curve.GetCurveValue(currKey[i], minT);
                        if (currKey[i] < curve.Count && minT == curve[currKey[i]].X)
                        {
                            currKey[i]++; // move to the next key
                        }
                    }
                }
                newKey.x = keyVals[0];
                newKey.y = keyVals[1];
                newKey.z = keyVals[2];
                newKey.w = keyVals[3];

                keyframes.Add(newKey);
            }

            return keyframes;

        }

        public static List<RawCurve> ConvertFloat5sToRawCurves(List<Float5> data)
        {
            List<RawCurve> curves = new List<RawCurve>();
            curves.Add(new RawCurve());
            curves.Add(new RawCurve());
            curves.Add(new RawCurve());
            curves.Add(new RawCurve());

            int numCurves = 4;

            foreach (Float5 key in data)
            {
                if (numCurves > 0)
                {
                    RawCurve curve = curves[0];
                    curve.Add(new Vector2(key.t, key.x));
                }
                if (numCurves > 1)
                {
                    RawCurve curve = curves[1];
                    curve.Add(new Vector2(key.t, key.y));
                }
                if (numCurves > 2)
                {
                    RawCurve curve = curves[2];
                    curve.Add(new Vector2(key.t, key.z));
                }
                if (numCurves > 3)
                {
                    RawCurve curve = curves[3];
                    curve.Add(new Vector2(key.t, key.w));
                }
            }

            // now for each keyframe beyond the first, delete it if it's just a lerp
            // between the neighbors
            foreach (KeyframeCurveEditor.RawCurve curve in curves)
            {
                for (int i = 1; i < curve.Count; i++)
                {
                    KeyframeCurveEditor.Vector2 curr = curve[i];
                    KeyframeCurveEditor.Vector2 prev = curve[i - 1];
                    if (i + 1 == curve.Count)
                    {
                        if (prev.Y == curr.Y)
                        {
                            curve.Remove(i);
                            i--;
                            continue;
                        }
                    }
                    else
                    {
                        KeyframeCurveEditor.Vector2 next = curve[i + 1];
                        // get lerp value
                        float dx = next.X - prev.X;
                        float dy = next.Y - prev.Y;

                        float t = (curr.X - prev.X) / dx;
                        float lerpVal = t * dy + prev.Y;

                        if (System.Math.Abs(lerpVal - curr.Y) < 0.001f)
                        {
                            curve.Remove(i);
                            i--;
                            continue;
                        }
                    }
                }
            }

            return curves;

        }

        // RSGNWE, Nicholas Howe, June 5, 2008
        // I made KeyValue serializable so that it can be copied to the clipboard.

        [Serializable]
		public class KeyValue
		{
			public float x;
			public float y;

            [NonSerialized]
            public Panel m_Peg;

            //public bool m_PegMoving;
            public bool m_PegSelected;

            [NonSerialized]
            public KeyCurve m_Curve;

			public KeyValue()
			{
				x=0.0f;
				y=0.0f;
				m_PegSelected=false;
				m_Curve = null;
			}
			public KeyValue(float xx, float yy,KeyCurve curve)
			{
				x=xx;
				y=yy;
				m_PegSelected=false;
				m_Curve = curve;
			}

			public void CopyDataTo(KeyValue val)
			{
				val.x=x;
				val.y=y;
				val.m_PegSelected=m_PegSelected;
			}
			public void Set(float xx, float yy)
			{
				//Does the clamping
				m_Curve.SetKey(this,xx,yy);
			}
			public void UpdatePeg()
			{
				if(m_Peg==null)
					return;
				
				//clamp to ranges
				if(x<m_Curve.m_Parent.m_MaxXRange.X)
					x = m_Curve.m_Parent.m_MaxXRange.X;
				if(x>m_Curve.m_Parent.m_MaxXRange.Y)
					x = m_Curve.m_Parent.m_MaxXRange.Y;
				if(y<m_Curve.m_Parent.m_MaxYRange.X)
					y = m_Curve.m_Parent.m_MaxYRange.X;
				if(y>m_Curve.m_Parent.m_MaxYRange.Y)
					y = m_Curve.m_Parent.m_MaxYRange.Y;

				m_Peg.Left = m_Curve.m_Parent.ConvertXToScreen(x) -(m_Peg.Width/2);
				m_Peg.Top = m_Curve.m_Parent.ConvertYToScreen(y) -(m_Peg.Height/2);
			}
		}

        // RSGNWE, Nicholas Howe, June 5, 2008
        // I made KeyCurve serializable so that it can be copied to the clipboard.

        [Serializable]
		public class KeyCurve
		{
            [NonSerialized]
			public KeyframeCurveEditor m_Parent;
			public bool  m_Enabled;
			public bool  m_Show;
			public Color m_Color;
            public List<KeyValue> m_Values = new List<KeyValue>(); //Array of keyValue
			// unused: public ArrayList m_Pegs = new ArrayList(); //Array of panels
			
			public void Clear()
			{
				foreach ( KeyValue val in m_Values )
				{
					if(val.m_Peg!=null)
						m_Parent.Controls.Remove(val.m_Peg);
				}
				m_Values.Clear();
			}
			public void CopyDataTo(KeyCurve curve)
			{
				if(curve.m_Values.Count!=0)
					return;
				curve.m_Parent=m_Parent;
				curve.m_Color =m_Color;
				curve.m_Enabled=m_Enabled;
				curve.m_Show = m_Show;
				foreach ( KeyValue a in m_Values )
				{
					KeyValue b = new KeyValue();
					a.CopyDataTo(b);
					curve.m_Values.Add(b);
				}
			}
			public void CreateFrom(KeyCurve curve)
			{
				m_Parent = curve.m_Parent;
				m_Color = curve.m_Color;
				m_Enabled = curve.m_Enabled;
				m_Show = curve.m_Show;
				foreach ( KeyValue val in curve.m_Values )
				{
					AddKey(val.x,val.y,val.m_PegSelected);					
				}
			}
			public void DrawCurve(Graphics graphics)
			{
				if(m_Values.Count==0)
					return;
                List<Point> points = new List<Point>();
				int lasty=0;
				int halfpegx = (m_Values[0]).m_Peg.Width /2;
				int halfpegy = (m_Values[0]).m_Peg.Height /2;
				foreach ( KeyValue val in m_Values )
				{
					//Determine position in interface based on scalers and ranges
					Point p = new Point(val.m_Peg.Left+halfpegx,val.m_Peg.Top+halfpegy);
					points.Add(p);
					lasty = p.Y;

					val.m_Peg.Enabled=m_Enabled;
					val.m_Peg.Visible=m_Enabled;
					if(val.m_PegSelected)
						val.m_Peg.BackColor = Color.FromArgb(255,255,255,64);
					else
						val.m_Peg.BackColor = Color.White;
				}
				if (!m_Show) 
				{
					return;
				}
				Point lp = new Point(m_Parent.Width,lasty);
				points.Add(lp);

				byte alpha = 255;
				if(!m_Enabled)
					alpha =50;
				graphics.DrawLines(new Pen(Color.FromArgb(alpha,m_Color.R,m_Color.G,m_Color.B),1),points.ToArray());
				
			}

			public Panel CreatePanel(KeyValue val)
			{
				Panel pan = new Panel();
				pan.Width = 12;
				pan.Height = 12;
				pan.BackColor = Color.White;
				pan.Left = m_Parent.ConvertXToScreen(val.x)-(pan.Width/2);
				pan.Top = m_Parent.ConvertYToScreen(val.y)-(pan.Height/2);
				pan.BorderStyle = BorderStyle.FixedSingle;
				pan.Tag = val;
				this.m_Parent.Controls.Add(pan);
				val.m_PegSelected=false;
				//pan.ContextMenu = m_Parent.PegMenu;
				pan.MouseDown += new System.Windows.Forms.MouseEventHandler(Peg_MouseDown);
				pan.MouseUp += new System.Windows.Forms.MouseEventHandler(Peg_MouseUp);
				pan.MouseMove += new System.Windows.Forms.MouseEventHandler(Peg_MouseMove);

				return pan;
			}

			public void DelKey(KeyValue key)
			{
				//can't remove peg index 0
				if(m_Values.IndexOf(key)==0)
					return;

				m_Values.Remove(key);
				m_Parent.Controls.Remove(key.m_Peg);	
				m_Parent.CompressData();
			}

			public void AddKey(float x, float y)
			{
				AddKey(x,y,true);

			}
			public void AddKey(float x, float y,bool selected)
			{
				foreach ( KeyValue value in m_Values )
				{
					//Dont add if points exactly match
                    if ( (x == value.x) && (y == value.y) )
						return;
				}

				//Clamp point to range
				if(x<m_Parent.m_MaxXRange.X)
					x=m_Parent.m_MaxXRange.X;
				if(x>m_Parent.m_MaxXRange.Y)
					x=m_Parent.m_MaxXRange.Y;
				if(y<m_Parent.m_MaxYRange.X)
					y=m_Parent.m_MaxYRange.X;
				if(y>m_Parent.m_MaxYRange.Y)
					y=m_Parent.m_MaxYRange.Y;

				//m_Parent.MakeUndo();
				//Add a point and control panel
				KeyValue val = new KeyValue(x,y,this);
				Panel pan = CreatePanel(val);
				val.m_Peg = pan;
				//m_Parent.DeselectPegs();
				val.m_PegSelected=selected;

				//Insert it in the list
				for(int i=0;i<m_Values.Count;i++)
				{
                    if ( x < m_Values[i].x )
                    {
                        m_Values.Insert( i, val );
                        m_Parent.CompressData();
                        return;
                    }
				}
				m_Values.Add(val);
			}

			public void SetKey(KeyValue val,float xx, float yy)
			{
				//Point newPos = new Point(((int)val.x+xx),((int)val.y+yy));
				float newPosX = xx;
				float newPosY = yy;
				int indx = m_Values.IndexOf(val);
				//First point cannont move in X
				if(indx>0)
				{
					//can't move past prev or next point
					KeyValue prev = val.m_Curve.m_Values[indx -1];
					KeyValue next = null;
					if(indx<val.m_Curve.m_Values.Count-1)
						next = val.m_Curve.m_Values[indx + 1];

				
					if(newPosX < prev.x)
						newPosX = prev.x;
					if(next!=null)
						if(newPosX>next.x)
							newPosX=next.x;
					
					//Can't move past maxrange
					if(newPosX>m_Parent.m_MaxXRange.Y)
						newPosX=m_Parent.m_MaxXRange.Y;
					//Can't move past minrange
					if(newPosX<m_Parent.m_MaxXRange.X)
						newPosX=m_Parent.m_MaxXRange.X;
					
					val.x=newPosX;
				}
				//Can't move past maxrange
				if(newPosY>m_Parent.m_MaxYRange.Y)
					newPosY=m_Parent.m_MaxYRange.Y;
				//Can't move past minrange
				if(newPosY<m_Parent.m_MaxYRange.X)
					newPosY=m_Parent.m_MaxYRange.X;

				val.y=newPosY;
				val.UpdatePeg();

			}
			public void DeselectPegs()
			{
				foreach ( KeyValue val in m_Values )
				{
					val.m_PegSelected=false;
				}
				m_Parent.UpdateSelected();
			}
			public void DelSelPegs()
			{
				//m_Parent.MakeUndo();
				for(int i=m_Values.Count-1;i>=0;i--)
				{
					KeyValue val = m_Values[i];
					if(val.m_PegSelected)
					{
						DelKey(val);
					}
				}
				m_Parent.UpdateSelected();
				m_Parent.Invalidate();
			}
			public void Peg_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
			{
				if(e.Button!=MouseButtons.Left)
					return;
				//m_Parent.MakeUndo();
				Panel pan = sender as Panel;
				KeyValue val = pan.Tag as KeyValue;
				if(!m_Parent.m_KeyShiftState && !val.m_PegSelected)
					m_Parent.DeselectPegs();
				val.m_PegSelected=true;
				val.m_Peg.BackColor = Color.FromArgb(255,255,255,64);
				m_Parent.SelectCurve(val.m_Curve);
				m_Parent.Focus();
				m_Parent.UpdateSelected();
				m_Parent.MakeUndo();
			}

			public void Peg_MouseUp(object sender, System.Windows.Forms.MouseEventArgs e)
			{
                Point point = m_Parent.PointToClient(new Point(MousePosition.X, MousePosition.Y));
                Panel pan = sender as Panel;
				KeyValue val = pan.Tag as KeyValue;
				//val.m_PegMoving=false;
				//val.m_Peg.BackColor = Color.White;
                m_Parent.MoveSelectedPegs(val, point);
				m_Parent.UpdateSelected();
			}

			public void Peg_MouseMove(object sender, System.Windows.Forms.MouseEventArgs e)
			{
				Point point=m_Parent.PointToClient(new Point(MousePosition.X,MousePosition.Y));
				if(e.Button != MouseButtons.Left)
					return;

				Panel pan = sender as Panel;
				KeyValue val = pan.Tag as KeyValue;
				if(!val.m_PegSelected)
					return;
                m_Parent.MoveSelectedPegs(val, point, realtimeUpdateMode);
				m_Parent.UpdateSelected();
			}

			public void UpdateRange()
			{
				foreach(KeyValue val in m_Values)
				{
					val.UpdatePeg();
				}
			}

			public Vector2 FindValueRange()
			{
				float min = 0.0f;
				float max = 0.0f;
				if (m_Show) 
				{
					foreach(KeyValue val in m_Values)
					{
						min = System.Math.Min(min, val.y);
						max = System.Math.Max(max, val.y);
					}
				}
				return new Vector2(min, max);
			}
		}

        // RSGNWE, Nicholas Howe, June 5, 2008
        // I added KeyCurveList, a serializable list of KeyCurves that can be copied to the clipboard.

        [Serializable]
        public class KeyCurveList : List<KeyCurve>
        {
        };

		public Vector2 FindValueYRange()
		{
			float min = 0.0f;
			float max = 0.0f;
			foreach(KeyCurve curve in m_CurveList)
			{
				Vector2 vec = curve.FindValueRange();
				min = System.Math.Min(min, vec.X);
				max = System.Math.Max(max, vec.Y);
			}
			return new Vector2(min,max);
		}

		public void AddPoint(float xx, float yy)
		{
			if(m_CurrentCurve != null)
			{
				//MakeUndo();
				if(!m_CurrentCurve.m_Enabled)
					return;
				MakeUndo();
				m_CurrentCurve.AddKey(xx,yy);
				this.Invalidate();
			}
			UpdateSelected();
		}
		public void AddCurve(KeyCurve curve)
		{
			curve.m_Parent = this;
			m_CurveList.Add(curve);
			if(m_CurrentCurve==null)
			{
				m_CurrentCurve = curve;
				m_Parent.DisplayCurrentCurve(curve);
			}
		}

		public void SelectCurve(KeyCurve curve)
		{
			m_CurrentCurve = curve;
			this.m_Parent.DisplayCurrentCurve(m_CurrentCurve);
		}

		public void DetermineCurrentCurve()
		{
			if(m_CurrentCurve!=null)
				if(m_CurrentCurve.m_Enabled)
					return;

			foreach ( KeyCurve curve in m_CurveList )
			{
				if(curve.m_Enabled)
				{
					SelectCurve(curve);
					return;
				}
			}
			m_CurrentCurve = null;
		}

		public void UpdateSelected()
		{
			int cnt =0;
			KeyValue selected =null;
            foreach ( KeyCurve curve in m_CurveList )
			{
                if ( curve.m_Enabled )
                {
                    foreach ( KeyValue val in curve.m_Values )
                    {
                        if ( val.m_PegSelected )
                        {
                            cnt++;
                            selected = val;
                        }
                    }
                }
			}
			
			if(cnt==0)
				m_Parent.SetNoSelection();
			if(cnt==1)
				m_Parent.SetSelected(selected);
			if(cnt>1)
				m_Parent.SetInvalidSelection();
		}
        public void MakeUndoFromRedo()
        {
            //pop one on the stack:
            m_UndoCurveList.Add(new KeyCurveList());


            //Copy data
            foreach (KeyCurve curve in m_CurveList)
            {
                KeyCurve bcurve = new KeyCurve();
                curve.CopyDataTo(bcurve);
                m_UndoCurveList[m_UndoCurveList.Count - 1].Add(bcurve);
            }
        }
		public void MakeUndo()
		{
            //pop one on the stack:
            m_UndoCurveList.Add(new KeyCurveList());

            //Copy data
			foreach ( KeyCurve curve in m_CurveList )
			{
                KeyCurve bcurve = new KeyCurve();
                curve.CopyDataTo(bcurve);
                m_UndoCurveList[m_UndoCurveList.Count - 1].Add(bcurve);
			}

            //Clear all the redos
            m_RedoCurveList.Clear();
        }

        public void MakeRedo()
        {

            //pop one on the stack:
            m_RedoCurveList.Add(new KeyCurveList());


            //Copy data
            foreach (KeyCurve curve in m_CurveList)
            {
                KeyCurve bcurve = new KeyCurve();
                curve.CopyDataTo(bcurve);
                m_RedoCurveList[m_RedoCurveList.Count - 1].Add(bcurve);
            }
        }

        // RSGNWE, Nicholas Howe, June 5, 2008
        // I moved most of Undo into a new function called CreateFrom.

        public void Undo()
		{
            if((m_UndoCurveList==null)||(m_UndoCurveList.Count==0))
				return;

            MakeRedo();

            KeyCurveList undoList = m_UndoCurveList[m_UndoCurveList.Count-1];

            CreateFrom(undoList);

            //Clear Undo
			m_UndoCurveList.Remove(undoList);

            CompressData();
        }

        // RSGNWE, Nicholas Howe, June 5, 2008
        // I moved most of Redo into a new function called CreateFrom.

        public void Redo()
        {
            if ((m_RedoCurveList == null) || (m_RedoCurveList.Count == 0))
                return;

            MakeUndoFromRedo();

            KeyCurveList redoList = m_RedoCurveList[m_RedoCurveList.Count - 1];

            CreateFrom(redoList);

            //Clear Redo
            m_RedoCurveList.Remove(redoList);

            CompressData();
        }

        // RSGNWE, Nicholas Howe, June 5, 2008, BEGIN

        private void CopyCurves()
        {
            // Copy curves to clipboard
            Clipboard.SetData("KeyCurveList", m_CurveList);
        }

        private void PasteCurves()
        {
            // Paste curves from clipboard
            KeyCurveList pasteList = Clipboard.GetData("KeyCurveList") as KeyCurveList;
            if (pasteList == null)
                return;

            // Set the curves' parents
            foreach (KeyCurve curve in pasteList)
                curve.m_Parent = this;

            MakeUndo();

            CreateFrom(pasteList);

            CompressData();
        }

        private void CreateFrom(KeyCurveList curveList)
        {
            int indx = -1;
            if (m_CurrentCurve != null)
                indx = m_CurveList.IndexOf(m_CurrentCurve);

            //Clear Curvelist
            int count = m_CurveList.Count;
            for (int i = 0; i != count; ++i)
            {
                KeyCurve curve = m_CurveList[i];
                //Disable curves that aren't showing
                if (i < curveList.Count && curveList[i].m_Show)
                    curveList[i].m_Show = curve.m_Show;
                curve.Clear();
            }
            m_CurveList.Clear();

            //Create the same number of curves as were present before
            for (int i = 0; i != count; ++i)
            {
                KeyCurve curve = new KeyCurve();
                //Determine if there is a visible curve to copy
                if (i < curveList.Count && curveList[i].m_Show)
                {
                    //Create a curve from the list
                    curve.CreateFrom(curveList[i]);
                }
                else
                {
                    //Create a curve with default settings
                    curve.m_Parent = this;
                    curve.m_Values = new List<KeyValue>();
                    curve.AddKey(0, 0, false);
                }
                AddCurve(curve);
            }

            UpdateSelected();
            if (indx >= 0)
                m_CurrentCurve = m_CurveList[indx];
            else
                m_CurrentCurve = null;

            m_Parent.ReMapCurves();
            DetermineCurrentCurve();
            Invalidate();
        }

        // RSGNWE, Nicholas Howe, June 5, 2008, END

        public void CompressData()
		{
			m_Parent.UpdateData();
		}
		public void DrawZeroLine(Graphics graphics)
		{
            List<Point> points = new List<Point>();
			Point A = new Point();
			Point B = new Point();
			A.Y = ConvertYToScreen(0.0f);
			A.X=0;
			B.X=this.Size.Width;
			B.Y=A.Y;
			points.Add(A);
			points.Add(B);
			Pen pen = new Pen(Color.FromArgb(200,0,0,0),1);
			float[] dashes = new float[2];
			dashes[0] = 4;
			dashes[1] = 4;
			pen.DashPattern = dashes;
			pen.DashStyle = System.Drawing.Drawing2D.DashStyle.Custom;
			graphics.DrawLines(pen ,points.ToArray());
		}


		public void ReSize()
		{
			//Given new width and height -reallign values
			foreach ( KeyCurve curve in m_CurveList )
			{
				foreach ( KeyValue value in curve.m_Values )
				{
					value.UpdatePeg();
				}
			}
			Invalidate();
		}

		public void DrawLabelsX(Graphics graphics,int offX, int offY)
		{
			//horizontal  //Need more accurate way to do this  and to make markers bold without redraw
			//also, why is this getting called all the time?
			System.Drawing.Font labelFont = this.LabelFont.Font;
			System.Drawing.Font markerFont = this.MarkerFont.Font;
			System.Drawing.Font font = labelFont;


			float stepSize = 8.0000f*m_ScaleFactor.X; //probably based on zoomlevel
			float delta = (m_MaxXRange.Y-m_MaxXRange.X)/stepSize;
			float valuex = m_MaxXRange.X;
			Point pos = new Point();
			Size labelSize;
			
			while(valuex <= (m_MaxXRange.Y+0.001f)) //add fudge factor due to percision errors
			{
				if( (valuex == 0.0f) || (valuex == m_MaxXRange.X) || (valuex == m_MaxXRange.Y ))
					font = markerFont;
				else
					font = labelFont;

				labelSize = graphics.MeasureString(valuex.ToString("0.000"),font).ToSize();
				pos.X = offX+ConvertXToScreen(valuex)+Left;
				pos.Y = offY-labelSize.Height-7;
				pos.X -= (labelSize.Width/2);
				graphics.DrawString(valuex.ToString("0.000"), font, new SolidBrush(Color.Black),pos);
				valuex += delta;
			}
		}

		public void DrawLabelsY(Graphics graphics,int offX, int offY)
		{
			//Vertical  
			System.Drawing.Font labelFont = this.LabelFont.Font;
			System.Drawing.Font markerFont = this.MarkerFont.Font;
			System.Drawing.Font font = labelFont;

			Size labelSize;
			float maxRange =(m_MaxYRange.Y-m_MaxYRange.X);
			float visRange = (m_VisYRange.Y-m_VisYRange.X);

			int percision = 10;
			int iterations = ((int)(maxRange/visRange))*percision;

				
			float start = m_MaxYRange.Y;
			float end = m_MaxYRange.X;
			float delta = (end-start)/(float)iterations;
			delta = (float)Math.Round(delta,3);

			int startindx = Math.Abs((int)((maxRange-m_VisYRange.Y)/delta));
			float valuey = maxRange+(startindx * delta);
			Point pos = new Point();
			
			Size min = graphics.MeasureString(m_MaxYRange.X.ToString("0.000"),markerFont).ToSize();
			Size max = graphics.MeasureString(m_MaxYRange.Y.ToString("0.000"),markerFont).ToSize();
			int maxSizeX=Math.Max(min.Width,max.Width)+3;

			offX-=maxSizeX/2;
			offY-=min.Height/2;

			for(int i=startindx;i<=(startindx+percision+2);i++)
			{
				float cacheval = valuey;
				if( (valuey<=(m_VisYRange.Y+0.0001f)) && (valuey>=(m_VisYRange.X-0.0001f)) )
				{	
					valuey = (float)Math.Round(valuey,3);
					if((valuey == 0.0f) ||(valuey==m_MaxYRange.X)||(valuey==m_MaxYRange.Y))
						font = markerFont;
					else
						font = labelFont;

					labelSize = graphics.MeasureString(valuey.ToString("0.000"),font).ToSize();
					pos.X = offX;
					pos.Y = offY+ConvertYToScreen(valuey)+Top;
					pos.X -= (labelSize.Width/2);
					graphics.DrawString(valuey.ToString("0.000"), font, new SolidBrush(Color.Black),pos);
				}
				valuey = maxRange+(i * delta);
			}

		}

		public int ConvertXToScreen(float x)
		{
			float scale = (x-m_VisXRange.X) / (m_VisXRange.Y-m_VisXRange.X);
			return (int)(scale*this.Size.Width);
		}
		public float ConvertXFromScreen(int x)
		{
			float scale = (float)(x / (float)this.Size.Width);
			return scale*((m_VisXRange.Y - m_VisXRange.X)+m_VisXRange.X);
		}

		public int ConvertYToScreen(float y)
		{
			float scale = 1.0f - (y-m_VisYRange.X) / (m_VisYRange.Y-m_VisYRange.X);
			return (int)(scale*this.Size.Height);
		}
		public float ConvertYFromScreen(int y)
		{
			float scale = 1.0f - (float)(y / (float)this.Size.Height);
			float v= (scale*(m_VisYRange.Y - m_VisYRange.X))+m_VisYRange.X;
			return v;
		}

		public void CreateCopyBuffer()
		{
			m_CopyPegBuffer.Clear();
			//copy values from currently selected curve
			if(m_CurrentCurve== null) return;
			for(int p=0;p<m_CurrentCurve.m_Values.Count;p++)
			{
				KeyValue val = m_CurrentCurve.m_Values[p] as KeyValue;
				if(val.m_PegSelected)
					m_CopyPegBuffer.Add(val);
			}

		}
		public void PasteCopyBuffer()
		{
			if(m_CurrentCurve== null) return;
			MakeUndo();
			foreach ( KeyValue val in m_CopyPegBuffer )
			{
				m_CurrentCurve.AddKey(val.x,val.y);
			}
		}

		public bool DetermineCanCopy()
		{
			//Can only copy if points are selected from one curve
			int cnt = CountSelectedPegs();
			int cc=0;
			for(int p=0;p<m_CurrentCurve.m_Values.Count;p++)
			{
				KeyValue val = m_CurrentCurve.m_Values[p] as KeyValue;
				if(val.m_PegSelected)
					cc++;
			}
			if(cnt==0)
				return false;
			if(cc==cnt)
				return true;
			return false;
		}

		public void SetVisibleRange(float xmin,float xmax,float ymin, float ymax)
		{
			m_VisXRange = new Vector2(xmin,xmax);
			m_VisYRange = new Vector2(ymin,ymax);
			ReSize();
		}
		
		public void SetMaxRange(float xmin,float xmax,float ymin, float ymax)
		{
			m_MaxXRange.Set(xmin,xmax);
			m_MaxYRange.Set(ymin,ymax);
		}
		

		public void BoundSelectPegs()
		{
			int left = Math.Min(m_SelectionBox[0].X,m_SelectionBox[1].X);
			int top =  Math.Min(m_SelectionBox[0].Y,m_SelectionBox[1].Y);
			int right = Math.Max(m_SelectionBox[0].X,m_SelectionBox[1].X);
			int bottom = Math.Max(m_SelectionBox[0].Y,m_SelectionBox[1].Y);

			int cnt=0;
			KeyValue lastSelected=null;
			foreach ( KeyCurve curve in m_CurveList )
			{
				if(curve.m_Enabled)
				{
					foreach ( KeyValue val in curve.m_Values )
					{
						int x = val.m_Peg.Left+(val.m_Peg.Width/2);
						int y = val.m_Peg.Top+(val.m_Peg.Height/2);

						if((x<=right)&&(x>=left)&&(y<=bottom)&&(y>=top))
						{
							val.m_PegSelected=true;
							lastSelected = val;
							cnt++;
						}
					}
				}
			}
			//If you've only selected one peg then make that curve the active curve
			if((cnt==1) && (lastSelected!=null))
			{
				SelectCurve(lastSelected.m_Curve);
				m_Parent.SetSelected(lastSelected);
			}
			UpdateSelected();
			Invalidate();
		}
		public void DeselectPegs()
		{
			foreach ( KeyCurve curve in m_CurveList )
			{
				curve.DeselectPegs();
			}
			Invalidate();
		}

		public void DeletePegs()
		{
			foreach ( KeyCurve curve in m_CurveList )
			{
				curve.DelSelPegs();
			}
		}

		public int CountSelectedPegs()
		{
			int cnt=0;
			foreach ( KeyCurve curve in m_CurveList )
			{
				if(curve.m_Enabled)
				{
					foreach ( KeyValue val in curve.m_Values )
					{
						if(val.m_PegSelected)
							cnt++;
					}
				}
			}
			return cnt;
		}

		public void MoveSelectedPegs(KeyValue basVal,Point point,bool sendData = true)
		{
			//Clamp values
			int halfpegx = basVal.m_Peg.Width/2;
			int halfpegy = basVal.m_Peg.Height/2;

			float xOff = ConvertXFromScreen(point.X)-(basVal.x);
			float yOff = ConvertYFromScreen(point.Y)-(basVal.y);

			if(m_KeyShiftState)
			{
				if(!(m_LockXAxisScrub || m_LockYAxisScrub) )
				{
					if(xOff>yOff)
						m_LockYAxisScrub=true;
					else 
						m_LockXAxisScrub=true;
				}

				if(m_LockYAxisScrub)
					yOff=0.0f;
				if(m_LockXAxisScrub)
					xOff=0.0f;
			}

			//For all the selected points:
			foreach ( KeyCurve curve in m_CurveList )
			{	
				foreach ( KeyValue val in curve.m_Values )
				{
					if(val.m_PegSelected)
					{
						curve.SetKey(val,(val.x+xOff),(val.y+yOff));
					}
				}
			}

            if (sendData)
			    CompressData();

			Invalidate();
		}
		private void KeyframeCurveEditor_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
            DrawZeroLine(e.Graphics);
			//Ned to get panel zorder working before enabling this (bringtofront is too slow)
            foreach ( KeyCurve curve in m_CurveList )
            {
                //if(m_CurrentCurve != m_CurveList[i] as KeyCurve)
                curve.DrawCurve( e.Graphics );
            }
			//if(m_CurrentCurve!= null)
			//	m_CurrentCurve.DrawCurve(e.Graphics);

			//Draw selection box
			if(m_BoundSelecting)
			{
				int left = Math.Min(m_SelectionBox[0].X,m_SelectionBox[1].X);
				int top = Math.Min(m_SelectionBox[0].Y,m_SelectionBox[1].Y);
				int width = Math.Abs(m_SelectionBox[0].X-m_SelectionBox[1].X);
				int height = Math.Abs(m_SelectionBox[0].Y-m_SelectionBox[1].Y);

				Pen pen = new Pen(Color.Black,1);
				pen.DashStyle = System.Drawing.Drawing2D.DashStyle.Dash;
				e.Graphics.DrawRectangle(pen,left,top,width,height);
			}
		}

		private void KeyframeCurveEditor_Load(object sender, System.EventArgs e)
		{
			m_OrigSize.X = ClientSize.Width;
			m_OrigSize.Y = ClientSize.Height;
		}

		private void menuItem1_Click(object sender, System.EventArgs e)
		{
			MakeUndo();
			DeletePegs();
		}

		private void KeyframeCurveEditor_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
		{
			if(e.KeyCode == Keys.ShiftKey)
				m_KeyShiftState = true;

			if(e.KeyCode == Keys.Delete)
			{
				MakeUndo();
				DeletePegs();
				KeyframeCurveEditor kfedit = sender as KeyframeCurveEditor;
				kfedit.Focus();
			}
			if(e.KeyCode == Keys.ControlKey)
				m_KeyControlState = true;
			if(e.KeyCode == Keys.Z)
				if(m_KeyControlState)
				{
					Undo();
				}
            if (e.KeyCode == Keys.Y)
                if (m_KeyControlState)
                {
                    Redo();
                }

			//Copy
			if(m_KeyControlState)
				if(e.KeyCode == Keys.C)
				{
                    // RSGNWE, Nicholas Howe, June 5, 2008
                    // Instead of copying points, copy curves to the clipboard.
/*
					if(DetermineCanCopy())
					{
						CreateCopyBuffer();
					}
/*/
                    CopyCurves();
//*/
				}
			//Paste
			if(m_KeyControlState)
				if(e.KeyCode == Keys.V)
				{
                    // RSGNWE, Nicholas Howe, June 5, 2008
                    // Instead of pasting points, paste curves from the clipboard.
/*
					if(DetermineCanCopy())
					{
						PasteCopyBuffer();
					}
/*/
                    PasteCurves();
//*/
                }
			this.Focus();
		}

		private void KeyframeCurveEditor_KeyUp(object sender, System.Windows.Forms.KeyEventArgs e)
		{
			if(e.KeyCode == Keys.ShiftKey)
				m_KeyShiftState = false;
			if(e.KeyCode == Keys.ControlKey)
				m_KeyControlState = false;
		
			if(!m_KeyShiftState)
			{
				m_LockXAxisScrub=false;
				m_LockYAxisScrub=false;
			}
		}

		private void KeyframeCurveEditor_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			if(e.Button == MouseButtons.Right)
				return;
			if(!m_KeyShiftState)
				DeselectPegs();
			Point MousePos=this.PointToClient(new Point(MousePosition.X,MousePosition.Y));
			//double click adds a keyvalue
			if (e.Button == MouseButtons.Left && e.Clicks == 2)
			{
				if(m_CurrentCurve != null)
				{
					if(!m_CurrentCurve.m_Enabled)
						return;
					MakeUndo();
					m_CurrentCurve.AddKey(ConvertXFromScreen(MousePos.X),ConvertYFromScreen(MousePos.Y));
					this.Invalidate();
					m_Parent.UpdateData();
				}
			}
			//Selecting
			if(e.Button == MouseButtons.Left)
			{
				m_BoundSelecting = true;
				m_SelectionBox[0].X = MousePos.X;
				m_SelectionBox[0].Y = MousePos.Y;
				m_SelectionBox[1].X = MousePos.X;
				m_SelectionBox[1].Y = MousePos.Y;
			}
		}

		private void KeyframeCurveEditor_MouseMove(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			Point MousePos=this.PointToClient(new Point(MousePosition.X,MousePosition.Y));
			//Selection box
			if(e.Button == MouseButtons.Left && m_BoundSelecting)
			{
				m_SelectionBox[1].X = MousePos.X;
				m_SelectionBox[1].Y = MousePos.Y;
				
				//Ensure we are centered around the dragging mouse
				//KeepPointOnScreen(MousePosition.X,MousePosition.X);

				Invalidate();
			}
		}

		private void KeyframeCurveEditor_MouseUp(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			if(m_BoundSelecting)
			{
				Invalidate();
				BoundSelectPegs();
			}
			m_BoundSelecting = false;
			m_LockXAxisScrub = false;
			m_LockYAxisScrub = false;
		}

		private void PegMenu_Popup(object sender, System.EventArgs e)
		{
			//if we have a current curve
			if(m_CurrentCurve==null)
				this.menuItemAddPoint.Enabled=false;
			else
				this.menuItemAddPoint.Enabled=true;
			
			//if nothing is selected then don't show the Delete option
			if(CountSelectedPegs()==0)
				this.menuItemDeletePoint.Enabled=false;
			else
				this.menuItemDeletePoint.Enabled=true;
			
			//If there is an undo
			if((m_UndoCurveList==null)||(m_UndoCurveList.Count==0))
				this.menuItemUndo.Enabled=false;
			else
				this.menuItemUndo.Enabled=true;

            //If there is a redo
            if ((m_RedoCurveList == null) || (m_RedoCurveList.Count == 0))
                this.menuItemredo.Enabled = false;
            else
                this.menuItemredo.Enabled = true;

            // RSGNWE, Nicholas Howe, June 5, 2008
            // I commented the following in favour of the below.
/*
			//If there is a copy
			menuItemCopy.Enabled = DetermineCanCopy();

			//If there is a paste
			if((m_CopyPegBuffer.Count>0)&&(m_CurrentCurve !=null))
				menuItemPaste.Enabled=true;
			else
				menuItemPaste.Enabled=false;
*/
            // RSGNWE, Nicholas Howe, June 5, 2008, BEGIN

            //If there is a copy
            menuItemCopy.Enabled = true;

            //If there is a paste
            if (Clipboard.ContainsData("KeyCurveList"))
                menuItemPaste.Enabled = true;
            else
                menuItemPaste.Enabled = false;

            // RSGNWE, Nicholas Howe, June 5, 2008, END
        }

		private void menuItemAddPoint_Click(object sender, System.EventArgs e)
		{
			Point MousePos=this.PointToClient(new Point(MousePosition.X,MousePosition.Y));
			AddPoint(ConvertXFromScreen(MousePos.X),ConvertYFromScreen(MousePos.Y));
		}

		private void menuItem1_Click_1(object sender, System.EventArgs e)
		{
			Undo();
		}

		private void menuItemCopy_Click(object sender, System.EventArgs e)
		{
            // RSGNWE, Nicholas Howe, June 5, 2008
            // Instead of copying points, copy curves to the clipboard.

            //CreateCopyBuffer();
            CopyCurves();
		}

		private void menuItemPaste_Click(object sender, System.EventArgs e)
		{
            // RSGNWE, Nicholas Howe, June 5, 2008
            // Instead of pasting points, paste curves from the clipboard.

            //PasteCopyBuffer();
            //Invalidate();
            PasteCurves();
        }

		private void KeyframeCurveEditor_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
		{
		}

        private void menuItemredo_Click(object sender, EventArgs e)
        {
            Redo();
        }
	}
}

