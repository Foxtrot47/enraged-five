namespace ragKeyframeEditorControl
{
    partial class ptxSlider
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // ptxSlider
            // 
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Text = "0.00";
            this.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ptxSlider_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.ptxSlider_MouseMove);
            this.Leave += new System.EventHandler(this.ptxSlider_Leave);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ptxSlider_KeyUp);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ptxSlider_KeyPress);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.ptxSlider_MouseUp);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ptxSlider_KeyDown);
            this.ResumeLayout(false);

        }

        #endregion

    }
}
