using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;

namespace ragKeyframeEditorControl
{
	/// <summary>
	/// Summary description for RagVScrollBar.
	/// </summary>
	public class RagVScrollBar : System.Windows.Forms.UserControl
	{
		bool  m_StateMouseDown;
		bool  m_StateMinArrowMouseDown;
		bool  m_StateMaxArrowMouseDown;
		bool  m_StatePanelMouseDown;
		bool  m_StateLargeChangeTop;
		bool  m_StateLargeChangeBottom;
		bool  m_ReverseRange;
		bool  m_DrawAsSlider;
		bool  m_ShowMinArrow;
		bool  m_ShowMaxArrow;
		Point m_MouseCapturePos;
		Point m_MouseScrubPos;
		int   m_ScrubBarSizeX;
		int   m_ScrubBarSizeY;
		float m_MouseDeltaPosX;
		float m_MouseDeltaPosY;
		float m_ScrubScaleFactorX;
		float m_ScrubScaleFactorY;
		float m_Minimum;
		float m_Maximum;
		float m_Value;
		float m_OldValue;
		float m_LargeValueChange;
		float m_SmallValueChange;

		[Description("Occurs when the value of the control changes"),Category("Action")]
        public event EventHandler ValueChanged=null;
		
		#region Properties

		[Description("A float between minimum and maximum. Also determins position of the scrub"),Category("Behavior")]
		public float Value  
		{	
			get 
			{ 
				if(m_ReverseRange)
					return m_Maximum-m_Value+m_Minimum;
				else
					return m_Value;
			}
			set 
			{
				if(m_ReverseRange)
					UpdateValue((m_Maximum-value+m_Minimum));
				else
					UpdateValue(value);
			}
		}

		[Description("Reverse the data range (True = min on top. False =max on top"),Category("Behavior")]
		public bool ReverseRange
		{	
			get { return m_ReverseRange;}
			set 
			{
				m_ReverseRange = value;
				Value=m_Value;
			}
		}

		[Description("Draw control as a slider"),Category("Appearance")]
		public bool DrawAsSlider
		{	
			get { return m_DrawAsSlider;}
			set 
			{ 
				m_DrawAsSlider=value;
				Invalidate();
			}
		}

		[Description("Set the up arrow's visibility"),Category("Appearance")]
		public bool ShowUpArrow
		{	
			get { return m_ShowMinArrow;}
			set 
			{ 
				m_ShowMinArrow=value;
				MinArrow.Visible=value;
				UpdateScrubBar();
			}
		}

		[Description("Set the down arrow's visibility"),Category("Appearance")]
		public bool ShowDownArrow
		{	
			get { return m_ShowMaxArrow;}
			set 
			{ 
				m_ShowMaxArrow=value;
				MaxArrow.Visible=value;
				UpdateScrubBar();
			}
		}

		[Description("The maximum value allowed"),Category("Behavior")]
		public float Maximum
		{
			get { return m_Maximum;}
			set
			{
				if(value>m_Minimum)
				{
					m_Maximum=value;
					UpdateValue(m_Value);
				}
			}
		}
		[Description("The minimum value allowed"),Category("Behavior")]
		public float Minimum
		{
			get { return m_Minimum;}
			set
			{
				if(value<m_Maximum)
				{
					m_Minimum=value;
					UpdateValue(m_Value);
				}
			}
		}
		[Description("The change in the value when user clicks in the scrollbar area"),Category("Behavior")]
		public float LargeChange
		{
			get { return m_LargeValueChange;}
			set { m_LargeValueChange=value; }
		}

		[Description("The change in the value when user clicks the up or down arrow"),Category("Behavior")]
		public float SmallChange
		{
			get { return m_SmallValueChange;}
			set { m_SmallValueChange=value;}
		}

		[Description("A scalar to convert mouse movement to value changes. 1.0 is normal (1 to 1) behavior"),Category("Behavior")]
		public float ScrubScalar
		{
			get { return m_ScrubScaleFactorY;}
			set	{ m_ScrubScaleFactorY=value;}
		}

		[Description("The size (height) of the scrub bar"),Category("Behavior")]
		public int ScrubBarSize
		{
			get { return m_ScrubBarSizeY;}
			set	
			{	if(value > ScrubBar.Parent.Height)
					value = ScrubBar.Parent.Height;
				if(ScrubBar.Height !=value) 
					ScrubBar.Height = m_ScrubBarSizeY=value;UpdateValue(m_Value);ScrubBar.Invalidate();Invalidate();}
		}

		public int ScrubHeight
		{
			get 
			{
				int displayheight = (this.ClientSize.Height);
				if(m_ShowMinArrow)
					displayheight-=MinArrow.Height;
				if(m_ShowMaxArrow)
					displayheight-=MaxArrow.Height;
				return displayheight;
			}
			set	
			{
			}
		}

		#endregion

		private System.Windows.Forms.Panel ScrubBar;
		private System.Windows.Forms.Timer LargeChangeTimer;
		private System.Windows.Forms.Panel MaxArrow;
		private System.Windows.Forms.Panel MinArrow;
		private System.Windows.Forms.Timer SmallChangeTimer;
		private System.ComponentModel.IContainer components;

		public RagVScrollBar()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			// TODO: Add any initialization after the InitializeComponent call
			m_StateMouseDown=false;
			m_StateLargeChangeTop=false;
			m_StateLargeChangeBottom=false;
			m_StateMinArrowMouseDown=false;
			m_StateMaxArrowMouseDown=false;
			m_ReverseRange=false;
			m_DrawAsSlider=false;
			m_ShowMinArrow = true;
			m_ShowMaxArrow = true;
			m_MouseCapturePos = new Point(0,0);
			m_MouseScrubPos = new Point(0,0);
			m_MouseDeltaPosX=0;
			m_MouseDeltaPosY=0;
			m_ScrubScaleFactorX=1;
			m_ScrubScaleFactorY=1.0f;
			m_Minimum=0.0f;
			m_Maximum=100.0f;
			m_Value=m_Maximum;
			m_OldValue=m_Value-1.0f;
			m_ScrubBarSizeX = ScrubBar.Width;
			m_ScrubBarSizeY = ScrubBar.Height;
			m_LargeValueChange = 10.0f;
			m_SmallValueChange = m_LargeValueChange/2.0f;
			ValueChanged += new System.EventHandler(this.RagVScrollBar_ValueChanged);

			//SetStyle(ControlStyles.Selectable, true);
			SetStyle(ControlStyles.DoubleBuffer, true);
			SetStyle(ControlStyles.AllPaintingInWmPaint, true);
			SetStyle(ControlStyles.UserPaint, true);
			SetStyle(ControlStyles.SupportsTransparentBackColor, true);
			SetStyle(ControlStyles.ContainerControl, false);
		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.ScrubBar = new System.Windows.Forms.Panel();
			this.LargeChangeTimer = new System.Windows.Forms.Timer(this.components);
			this.MaxArrow = new System.Windows.Forms.Panel();
			this.MinArrow = new System.Windows.Forms.Panel();
			this.SmallChangeTimer = new System.Windows.Forms.Timer(this.components);
			this.SuspendLayout();
			// 
			// ScrubBar
			// 
			this.ScrubBar.BackColor = System.Drawing.SystemColors.ScrollBar;
			this.ScrubBar.Location = new System.Drawing.Point(-16, 72);
			this.ScrubBar.Name = "ScrubBar";
			this.ScrubBar.Size = new System.Drawing.Size(72, 48);
			this.ScrubBar.TabIndex = 4;
			this.ScrubBar.MouseUp += new System.Windows.Forms.MouseEventHandler(this.ScrubBar_MouseUp);
			this.ScrubBar.Paint += new System.Windows.Forms.PaintEventHandler(this.ScrubBar_Paint);
			this.ScrubBar.MouseEnter += new System.EventHandler(this.ScrubBar_MouseEnter);
			this.ScrubBar.MouseMove += new System.Windows.Forms.MouseEventHandler(this.ScrubBar_MouseMove);
			this.ScrubBar.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ScrubBar_MouseDown);
			// 
			// LargeChangeTimer
			// 
			this.LargeChangeTimer.Interval = 1000;
			this.LargeChangeTimer.Tick += new System.EventHandler(this.LargeChangeTimer_Tick);
			// 
			// MaxArrow
			// 
			this.MaxArrow.BackColor = System.Drawing.SystemColors.ScrollBar;
			this.MaxArrow.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.MaxArrow.Location = new System.Drawing.Point(0, 208);
			this.MaxArrow.Name = "MaxArrow";
			this.MaxArrow.Size = new System.Drawing.Size(16, 16);
			this.MaxArrow.TabIndex = 5;
			this.MaxArrow.MouseUp += new System.Windows.Forms.MouseEventHandler(this.MaxArrow_MouseUp);
			this.MaxArrow.Paint += new System.Windows.Forms.PaintEventHandler(this.MaxArrow_Paint);
			this.MaxArrow.MouseDown += new System.Windows.Forms.MouseEventHandler(this.MaxArrow_MouseDown);
			// 
			// MinArrow
			// 
			this.MinArrow.BackColor = System.Drawing.SystemColors.ScrollBar;
			this.MinArrow.Dock = System.Windows.Forms.DockStyle.Top;
			this.MinArrow.Location = new System.Drawing.Point(0, 0);
			this.MinArrow.Name = "MinArrow";
			this.MinArrow.Size = new System.Drawing.Size(16, 16);
			this.MinArrow.TabIndex = 6;
			this.MinArrow.MouseUp += new System.Windows.Forms.MouseEventHandler(this.MinArrow_MouseUp);
			this.MinArrow.Paint += new System.Windows.Forms.PaintEventHandler(this.MinArrow_Paint);
			this.MinArrow.MouseDown += new System.Windows.Forms.MouseEventHandler(this.MinArrow_MouseDown);
			// 
			// SmallChangeTimer
			// 
			this.SmallChangeTimer.Tick += new System.EventHandler(this.SmallChangeTimer_Tick);
			// 
			// RagVScrollBar
			// 
			this.BackColor = System.Drawing.SystemColors.Control;
			this.Controls.Add(this.MinArrow);
			this.Controls.Add(this.MaxArrow);
			this.Controls.Add(this.ScrubBar);
			this.Name = "RagVScrollBar";
			this.Size = new System.Drawing.Size(16, 224);
			this.Resize += new System.EventHandler(this.RagVScrollBar_Resize);
			this.Load += new System.EventHandler(this.RagVScrollBar_Load);
			this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.RagVScrollBar_MouseUp);
			this.Paint += new System.Windows.Forms.PaintEventHandler(this.RagVScrollBar_Paint);
			this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.RagVScrollBar_MouseDown);
			this.ResumeLayout(false);

		}
		#endregion

		private void UpdateScrubBar()
		{
			float displayheight = (this.ClientSize.Height-ScrubBar.Height);
			if(m_ShowMinArrow)
				displayheight-=MinArrow.Height;
			if(m_ShowMaxArrow)
				displayheight-=MaxArrow.Height;

			float val = ((m_Value-m_Minimum)/(m_Maximum-m_Minimum))*displayheight;
			
			if(m_ShowMaxArrow)
				val+=MaxArrow.ClientSize.Height;

			ScrubBar.Top = (int)val;
			//ScrubBar.Invalidate();
		}
		private void UpdateValue(float newval)
		{
			m_Value=newval;
			if(m_Value>m_Maximum)
				m_Value=m_Maximum;
			if(m_Value<m_Minimum)
				m_Value = m_Minimum;
			
			UpdateScrubBar();
			if(m_OldValue != m_Value)
			{
				m_OldValue=m_Value;
				if(ValueChanged!=null)
					ValueChanged(this, new System.EventArgs());
			}
			//Invalidate();
		}

		private void UpdateLargeChangeEvent()
		{
			m_StatePanelMouseDown = true;
			m_StateLargeChangeTop=false;
			m_StateLargeChangeBottom=false;
			//determine twords min or max
			int y = Cursor.Position.Y;
			Point pos = PointToClient(Cursor.Position);
			if(pos.Y<(ScrubBar.Top))
			{
				m_Value-=m_LargeValueChange;
				m_StateLargeChangeTop=true;
				UpdateValue(m_Value);
			
			}
			else
				if(pos.Y>(ScrubBar.Bottom))
				{
					m_Value+=m_LargeValueChange;
					m_StateLargeChangeBottom=true;
					UpdateValue(m_Value);

				}
		}
		
		private void UpdateSmallValueChange(float d)
		{
			m_Value+=d;
			UpdateValue(m_Value);
		}

		private void PaintPanelAsButton(Graphics g,Panel pan, bool depressed)
		{
			Point A = new Point(0,0);
			Point B = new Point(pan.Width-1,pan.Height-1);

			if(!depressed)			
			{
				System.Drawing.Pen pen = new System.Drawing.Pen(Color.FromKnownColor(KnownColor.ControlDarkDark),1);
				g.DrawLine(pen,new Point(B.X,A.Y),B);
				g.DrawLine(pen,new Point(A.X,B.Y),B);
				pen = new System.Drawing.Pen(Color.FromKnownColor(KnownColor.ControlDark),1);
				g.DrawLine(pen,new Point(B.X-1,A.Y+1),new Point(B.X-1,B.Y-1));
				g.DrawLine(pen,new Point(A.X+1,B.Y-1),new Point(B.X-1,B.Y-1));
				pen = new System.Drawing.Pen(Color.FromKnownColor(KnownColor.ControlLightLight),1);
				g.DrawLine(pen,new Point(A.X+1,A.Y+1),new Point(B.X-2,A.Y+1));
				g.DrawLine(pen,new Point(A.X+1,A.Y+1),new Point(A.X+1,B.Y-2));
			}
			else
			{
				System.Drawing.Pen pen = new System.Drawing.Pen(Color.FromKnownColor(KnownColor.ControlDark),1);
				g.DrawRectangle(pen,0,0,B.X,B.Y);
			}
		}

		private void RagVScrollBar_ValueChanged(object sender, System.EventArgs e)
		{
			RagVScrollBar bar = sender as RagVScrollBar;
			//label1.Text = bar.Value.ToString();
			//label1.Invalidate();
		}

		private void ScrubBar_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			Cursor.Hide();
			m_StateMouseDown=true;
			m_MouseScrubPos = m_MouseCapturePos = Cursor.Position;
			m_MouseDeltaPosX=0;
			m_MouseDeltaPosY=0;
		}

		private void ScrubBar_MouseUp(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			m_StateMouseDown=false;
			Point pos = PointToScreen(new Point(ScrubBar.Left,ScrubBar.Top));
			Point pos2 = PointToScreen(new Point(ScrubBar.Left+ScrubBar.Width,ScrubBar.Top+m_ScrubBarSizeY));
			int x = (pos.X+pos2.X)/2;
			int y = (pos.Y+pos2.Y)/2;
			Cursor.Position = new Point(x,y);
			Cursor.Show();
		}

		private void ScrubBar_MouseMove(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			if(m_StateMouseDown)
			{
				Point newpos = Cursor.Position;
				float dx  = newpos.X-m_MouseScrubPos.X;
				float dy  = newpos.Y-m_MouseScrubPos.Y;
				dx*=m_ScrubScaleFactorX;
				dy*=m_ScrubScaleFactorY;
				m_MouseDeltaPosX+=dx;
				m_MouseDeltaPosY+=dy;

				m_MouseScrubPos.X = newpos.X;
				m_MouseScrubPos.Y = newpos.Y;
				
				float truerange = m_Maximum-m_Minimum;
				truerange/=ScrubHeight;
				m_Value+=dy*truerange;
				
				if(Math.Abs(m_MouseDeltaPosY)>1.0f)
				{
					Cursor.Position = m_MouseCapturePos;
					m_MouseScrubPos = m_MouseCapturePos = Cursor.Position;
					m_MouseDeltaPosX=0;
					m_MouseDeltaPosY=0;
				}
				UpdateValue(m_Value);
			}
		}

		private void ScrubBar_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			Panel pan = sender as Panel;
			pan.Left=0;
			pan.Width = Width;
			pan.Height = m_ScrubBarSizeY;
			PaintPanelAsButton(e.Graphics,pan,false);
		}

		private void RagVScrollBar_Load(object sender, System.EventArgs e)
		{
			UpdateScrubBar();
		}

		private void panel1_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
		}

		private void RagVScrollBar_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			RagVScrollBar bar = sender as RagVScrollBar;
			if(m_DrawAsSlider)
			{
				//Draw as a slider
				int center = Width/2;
				System.Drawing.Pen pen = new System.Drawing.Pen(Color.FromKnownColor(KnownColor.ControlDarkDark),1);
                List<Point> points = new List<Point>();
				points.Add(new Point(center,1));
				points.Add(new Point(center,Height-3));
                e.Graphics.DrawLines( pen, points.ToArray() );
				
				pen = new System.Drawing.Pen(Color.FromKnownColor(KnownColor.ControlDark),1);
				points.Clear();
				points.Add(new Point(center+1,0));
				points.Add(new Point(center-1,0));
				points.Add(new Point(center-1,Height-2));
                e.Graphics.DrawLines( pen, points.ToArray() );

				pen = new System.Drawing.Pen(Color.FromKnownColor(KnownColor.ControlLightLight),1);
				points.Clear();
				points.Add(new Point(center+2,0));
				points.Add(new Point(center+2,Height));
				points.Add(new Point(center-2,Height));
                e.Graphics.DrawLines( pen, points.ToArray() );
			}
			else
			{
				System.Drawing.Drawing2D.HatchBrush brush = new System.Drawing.Drawing2D.HatchBrush(System.Drawing.Drawing2D.HatchStyle.Percent50,Color.FromKnownColor(KnownColor.ControlLightLight),Color.FromKnownColor(KnownColor.ControlLight));
				e.Graphics.FillRectangle(brush,0,0,bar.Width,bar.Height);
				if(m_StatePanelMouseDown)
				{
					Point A = new Point(0,0);
					Point B = new Point(0,0);
					brush = new System.Drawing.Drawing2D.HatchBrush(System.Drawing.Drawing2D.HatchStyle.Percent50,Color.FromKnownColor(KnownColor.ControlDarkDark),Color.FromKnownColor(KnownColor.ControlDark));
					if(m_StateLargeChangeTop)				
					{
						A.X=0;
						A.Y=0;
						B.X=Width;
						B.Y=ScrubBar.Top-1;
					}
					else
						if(m_StateLargeChangeBottom)				
					{
						A.X=0;
						A.Y=ScrubBar.Bottom;
						B.X=Width;
						B.Y=Height;
					}

					e.Graphics.FillRectangle(brush,A.X,A.Y,B.X,B.Y);
				}
			}
		}

		private void RagVScrollBar_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			UpdateLargeChangeEvent();
			LargeChangeTimer.Interval=500;
			LargeChangeTimer.Start();
		}

		private void RagVScrollBar_MouseUp(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			m_StatePanelMouseDown=false;
			m_StateLargeChangeTop=false;
			m_StateLargeChangeBottom=false;
			Invalidate();
		}

		private void LargeChangeTimer_Tick(object sender, System.EventArgs e)
		{
			if(m_StatePanelMouseDown)
			{
				UpdateLargeChangeEvent();
				LargeChangeTimer.Interval=30;
				LargeChangeTimer.Start();
			}
			else
				LargeChangeTimer.Stop();
		}

		private void ScrubBar_MouseEnter(object sender, System.EventArgs e)
		{
			m_StatePanelMouseDown=false;
		}

		private void MaxArrow_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			PaintPanelAsButton(e.Graphics,sender as Panel,m_StateMaxArrowMouseDown);

			Point B = new Point(MaxArrow.Width-1,MaxArrow.Height-1);
			Point center = new Point((B.X)/2,(B.Y)/2);
			//center.X+=1;
			center.Y+=2;

			if(m_StateMaxArrowMouseDown)
			{
				center.X+=1;
				center.Y+=1;
			}

            List<Point> points = new List<Point>();
			points.Add(new Point(center.X-3,center.Y-3));
			points.Add(new Point(center.X+4,center.Y-3));
			points.Add(new Point(center.X,center.Y+1));
			System.Drawing.SolidBrush brush = new System.Drawing.SolidBrush(Color.Black);
            e.Graphics.FillPolygon( brush, points.ToArray() );
		}

		private void MinArrow_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			PaintPanelAsButton(e.Graphics,sender as Panel,m_StateMinArrowMouseDown);

			Point B = new Point(MinArrow.Width-1,MinArrow.Height-1);
			Point center = new Point((B.X)/2,(B.Y)/2);
			if(m_StateMinArrowMouseDown)
			{
				center.X+=1;
				center.Y+=1;
			}
            List<Point> points = new List<Point>();
			points.Add(new Point(center.X-4,center.Y+3));
			points.Add(new Point(center.X+4,center.Y+3));
			points.Add(new Point(center.X,center.Y-2));
			System.Drawing.SolidBrush brush = new System.Drawing.SolidBrush(Color.Black);
            e.Graphics.FillPolygon( brush, points.ToArray() );
		}

		private void MinArrow_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			m_StateMinArrowMouseDown=true;
			UpdateSmallValueChange(-m_SmallValueChange);
			MinArrow.Invalidate();
			SmallChangeTimer.Interval=500;
			SmallChangeTimer.Start();
		}

		private void MinArrow_MouseUp(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			m_StateMinArrowMouseDown=false;
			MinArrow.Invalidate();
			SmallChangeTimer.Stop();
		}

		private void MaxArrow_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			m_StateMaxArrowMouseDown=true;
			UpdateSmallValueChange(m_SmallValueChange);
			MaxArrow.Invalidate();
			SmallChangeTimer.Interval=500;
			SmallChangeTimer.Start();
		}

		private void MaxArrow_MouseUp(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			m_StateMaxArrowMouseDown=false;
			MaxArrow.Invalidate();
			SmallChangeTimer.Stop();
		}

		private void SmallChangeTimer_Tick(object sender, System.EventArgs e)
		{
			SmallChangeTimer.Stop();
			if(m_StateMaxArrowMouseDown)
			{
				UpdateSmallValueChange(m_SmallValueChange);
				SmallChangeTimer.Interval=110;
				SmallChangeTimer.Start();
			}

			if(m_StateMinArrowMouseDown)
			{
				UpdateSmallValueChange(-m_SmallValueChange);
				SmallChangeTimer.Interval=110;
				SmallChangeTimer.Start();
			}
		}

		private void RagVScrollBar_Resize(object sender, System.EventArgs e)
		{
			UpdateScrubBar();
			MinArrow.Invalidate();
			MaxArrow.Invalidate();
			Invalidate();
		}
	}
}
