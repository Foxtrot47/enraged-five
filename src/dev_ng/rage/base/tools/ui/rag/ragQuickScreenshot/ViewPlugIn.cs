using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows.Forms;

using ragCore;
using ragWidgets;

namespace ragQuickScreenshot
{
	/// <summary>
	/// Summary description for ViewPlugIn.
	/// </summary>
	public class ViewPlugIn : IViewPlugIn
	{
		public ViewPlugIn()
		{
			//
			// TODO: Add constructor logic here
			//
        }

        #region IViewPlugIn Interface
        public string Name
        {
            get
            {
                return "Quick Screenshot";
            }
        }

        public void InitPlugIn( PlugInHostData hostData )
        {
            // save a reference to the host data
            m_hostData = hostData;

            // build the menu items
            m_screenshotsMenuItem = new ToolStripMenuItem( "Screenshots" );
                        
            ToolStripMenuItem item = new ToolStripMenuItem( "Take Screenshot" );
            item.Click += new EventHandler( OnTakeScreenshot );
            item.ShortcutKeys = (Keys)(Keys.Control | Keys.Shift | Keys.S);
            m_screenshotsMenuItem.DropDownItems.Add( item );

            item = new ToolStripMenuItem( "Take Screenshot with Console" );
            item.Click += new EventHandler( OnTakeScreenshotWithConsole );
            item.Enabled = false;
            m_screenshotsMenuItem.DropDownItems.Add( item );

            m_screenshotsMenuItem.DropDownItems.Add( new ToolStripSeparator() );

            item = new ToolStripMenuItem( "Start Screenshots in Application Window" );
            item.Click += new EventHandler( OnStartApplicationWindowScreenshots );
            m_screenshotsMenuItem.DropDownItems.Add( item );

            item = new ToolStripMenuItem( "Stop Screenshots" );
            item.Click += new EventHandler( OnStopScreenshots );
            m_screenshotsMenuItem.DropDownItems.Add( item );

            hostData.PlugInToolStripMenu.DropDownItems.Add( m_screenshotsMenuItem );

            // set the InitFinished event so we can locate the widgets that our menu items connect with
            m_initFinishedEventHandler = new InitFinishedHandler( MainBankManager_InitFinished );
            hostData.MainRageApplication.BankMgr.InitFinished += m_initFinishedEventHandler;

            // set the SetPluginStatus event so we can determine when our menu items are enabled
            m_pluginStatusEventHandler = new EventHandler( SetPluginStatus );
            hostData.PlugInToolStripMenu.DropDownOpening += m_pluginStatusEventHandler;
        }

		public void RemovePlugIn()
		{
            if ( (m_screenshotsMenuItem != null) && (m_screenshotsMenuItem.Container != null) )
            {
                m_screenshotsMenuItem.Container.Remove( m_screenshotsMenuItem );
            }

            if ( m_hostData.PlugInToolStripMenu != null )
            {
                m_hostData.PlugInToolStripMenu.DropDownOpening -= m_pluginStatusEventHandler;
            }

            if ( m_hostData.MainRageApplication.BankMgr != null )
            {
                m_hostData.MainRageApplication.BankMgr.InitFinished -= m_initFinishedEventHandler;
            }
		}
        #endregion

        #region Event Handlers
        /// <summary>
        /// Called when the Main Bank Manager receives the "first frame" message from the game.  By this point,
        /// most, if not all, of the banks and widgets have been created.
        /// It is at this point that it is safe to attempt to setup our widget binding.
        /// </summary>
        void MainBankManager_InitFinished()
        {
            m_saveScreenshotButton = m_hostData.MainRageApplication.BankMgr.FindFirstWidgetFromPath( "rage - Gfx/Save Screenshot" ) as WidgetButton;
            if ( m_saveScreenshotButton == null )
            {
                m_saveScreenshotButton = m_hostData.MainRageApplication.BankMgr.FindFirstWidgetFromPath( "rage - Gfx/Save Screenshot (.jpg)" ) as WidgetButton;
                if ( m_saveScreenshotButton == null )
                {
                    List<Widget> foundWidgets = new List<Widget>();
                    WidgetBank.FindWidgetsWithMatchingString( m_hostData.MainRageApplication.BankMgr, false, true,
                        true, true, false, "Save Screenshot", ref foundWidgets, -1 );
                    if ( foundWidgets.Count == 0 )
                    {
                        WidgetBank.FindWidgetsWithMatchingString( m_hostData.MainRageApplication.BankMgr, false, true,
                            true, true, false, "Save Screenshot (.jpg)", ref foundWidgets, -1 );
                    }

                    if ( (foundWidgets.Count > 0) && (foundWidgets[0] is WidgetButton) )
                    {
                        m_saveScreenshotButton = foundWidgets[0] as WidgetButton;
                    }
                }
            }

            m_screenshotsInApplicationWindowButton 
                = m_hostData.MainRageApplication.BankMgr.FindFirstWidgetFromPath( "rage - Gfx/Start Screenshots in Rag's Application Window" ) as WidgetButton;
            if ( m_screenshotsInApplicationWindowButton == null )
            {
                List<Widget> foundWidgets = new List<Widget>();
                WidgetBank.FindWidgetsWithMatchingString( m_hostData.MainRageApplication.BankMgr, false, true,
                    true, true, false, "Start Screenshots in Rag's Application Window", ref foundWidgets, -1 );
                if ( (foundWidgets.Count > 0) && (foundWidgets[0] is WidgetButton) )
                {
                    m_screenshotsInApplicationWindowButton = foundWidgets[0] as WidgetButton;
                }
            }

            m_stopScreenshotsButton
                = m_hostData.MainRageApplication.BankMgr.FindFirstWidgetFromPath( "rage - Gfx/Stop Screenshots" ) as WidgetButton;
            if ( m_stopScreenshotsButton == null )
            {
                List<Widget> foundWidgets = new List<Widget>();
                WidgetBank.FindWidgetsWithMatchingString( m_hostData.MainRageApplication.BankMgr, false, true,
                    true, true, false, "Stop Screenshots", ref foundWidgets, -1 );
                if ( (foundWidgets.Count > 0) && (foundWidgets[0] is WidgetButton) )
                {
                    m_stopScreenshotsButton = foundWidgets[0] as WidgetButton;
                }
            }

            WidgetToggle toggle = m_hostData.MainRageApplication.BankMgr.FindFirstWidgetFromPath( "rage - Gfx/Display last Screenshot in Rag's Application Window" ) as WidgetToggle;
            if ( toggle == null )
            {
                List<Widget> foundWidgets = new List<Widget>();
                WidgetBank.FindWidgetsWithMatchingString( m_hostData.MainRageApplication.BankMgr, false, true,
                    true, true, false, "Display last Screenshot in Rag's Application Window", ref foundWidgets, -1 );
                if ( (foundWidgets.Count > 0) && (foundWidgets[0] is WidgetToggle) )
                {
                    toggle = foundWidgets[0] as WidgetToggle;
                }
            }

            if ( toggle != null )
            {
                toggle.ValueChanged += new EventHandler( applicationWindowScreenshotsToggle_CheckedChanged );
            }
        }

        private void SetPluginStatus( object sender, EventArgs e )
        {
            m_screenshotsMenuItem.DropDownItems[0].Enabled = m_saveScreenshotButton != null;
            m_screenshotsMenuItem.DropDownItems[1].Enabled = Sensor.CurrentSensor.CanTakeScreenShot;
            m_screenshotsMenuItem.DropDownItems[3].Enabled = m_screenshotsInApplicationWindowButton != null;
            m_screenshotsMenuItem.DropDownItems[4].Enabled = m_stopScreenshotsButton != null;
        }

        public void OnTakeScreenshot(object sender, EventArgs e)
		{
            if ( m_saveScreenshotButton != null )
			{
                m_saveScreenshotButton.Activate();
			}
        }

        public void OnTakeScreenshotWithConsole( object sender, EventArgs e )
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Bitmap (*.bmp)|*.bmp|Direct Draw Surface (*.dds)|*.dds";
            saveFileDialog.Title = "Save Screenshot";

            if ( saveFileDialog.ShowDialog() == DialogResult.OK )
            {
                Sensor.CurrentSensor.SaveScreenShot( saveFileDialog.FileName );
            }
        }

        public void OnStartApplicationWindowScreenshots( object sender, EventArgs e )
        {
            if ( m_screenshotsInApplicationWindowButton != null )
            {
                m_screenshotsInApplicationWindowButton.Activate();
            }
        }

        public void OnStopScreenshots( object sender, EventArgs e )
        {
            if ( m_stopScreenshotsButton != null )
            {
                m_stopScreenshotsButton.Activate();
            }
        }

        private void applicationWindowScreenshotsToggle_CheckedChanged( object sender, EventArgs e )
        {
            WidgetToggle toggle = sender as WidgetToggle;
            if ( toggle != null )
            {
                WidgetImageViewer imgViewer
                    = m_hostData.MainRageApplication.BankMgr.FindFirstWidgetFromPath( "rage - Gfx/Screen Shot Viewer" ) as WidgetImageViewer;
                if ( imgViewer != null )
                {
                    m_hostData.MainRageApplication.RenderWindowImageFilename = toggle.Checked ? imgViewer.ImageFile : null;
                }
            }
        }
        #endregion

        #region Variables
        private EventHandler m_pluginStatusEventHandler;
        private InitFinishedHandler m_initFinishedEventHandler;
        private PlugInHostData m_hostData;
        private ToolStripMenuItem m_screenshotsMenuItem;
        private WidgetButton m_saveScreenshotButton;
        private WidgetButton m_screenshotsInApplicationWindowButton;
        private WidgetButton m_stopScreenshotsButton;
        #endregion
    }
}
