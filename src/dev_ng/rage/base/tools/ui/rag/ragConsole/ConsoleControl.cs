using System;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Data;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace ragConsole
{
	/// <summary>
	/// Summary description for ragConsoleControl.
	/// </summary>
	public class ragConsoleControl : UILibrary.ShellControl
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public ragConsoleControl()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			AddCommand("alias",new CommandDelegate(CommandAlias));
		}

		const int CONSOLE_PORT=5050;

		public static void Connect()
		{
			sm_ConnectThread = new Thread(new ThreadStart(ConnectProc));
			sm_ConnectThread.Name = "Console Connect Thread";
			sm_ConnectThread.Start();			
		}

		public static void Shutdown()
		{
			if (sm_ConnectThread!=null)
			{
				sm_ConnectThread.Abort();
				sm_ConnectThread=null;
			}
		}

		public static void ConnectProc()
		{
			sm_Listener = new TcpListener(IPAddress.Any,CONSOLE_PORT);
			sm_Listener.Start();

			// wait until we have a connection:
			while (!sm_Listener.Pending())
			{
				Thread.Sleep(100);
			}
			sm_Socket=sm_Listener.AcceptSocket();
				
			if (sm_Socket!=null)
			{
				//sm_Socket.Blocking=false;
				sm_IsConnected = true;
			}
		}

		private static TcpListener sm_Listener;
		private static Socket sm_Socket;
		private static Thread sm_ConnectThread;
		private static bool sm_IsConnected;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if( components != null )
					components.Dispose();
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			// 
			// ragConsoleControl
			// 
			this.Name = "ragConsoleControl";
			this.ShellTextFont = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.Size = new System.Drawing.Size(464, 456);
			this.CommandEntered += new UILibrary.EventCommandEntered(this.ragConsoleControl_CommandEntered);

		}
		#endregion

		private string ConvertToString(ArrayList chars)
		{
			char[] charArray;
			charArray=chars.ToArray(typeof(char)) as char[];
			return new string(charArray);
		}

		private string[] SplitArgs(string theCommandString)
		{
			ArrayList strings=new ArrayList();
			ArrayList argChars=new ArrayList();
			bool lookingForEndQuote=false;

			// extract arguments ('"' keeps spaces together):
			for (int i=0;i<theCommandString.Length;i++)
			{
				char theChar=theCommandString[i];
				if (theChar=='"')
				{
					if (lookingForEndQuote)
					{
						strings.Add(ConvertToString(argChars));
						argChars.Clear();
						lookingForEndQuote=false;
					}
					else
					{
						lookingForEndQuote=true;
					}
				}
				else if (Char.IsWhiteSpace(theChar))
				{
					if (lookingForEndQuote)
					{
						argChars.Add(theChar);
					}
					else
					{
						if (argChars.Count>0)
						{
							strings.Add(ConvertToString(argChars));		
							argChars.Clear();
						}
					}
				}
				else
					argChars.Add(theChar);
			}

			// convert the final arg:
			if (argChars.Count>0)
				strings.Add(ConvertToString(argChars));		


			string[] returnValue;
			returnValue=strings.ToArray(typeof(string)) as string[];

			return returnValue;
		}

		private void ragConsoleControl_CommandEntered(object sender, UILibrary.CommandEnteredEventArgs e)
		{
			//string[] split=e.Command.Split();
			string[] split=SplitArgs(e.Command);
			
			if (split.Length>0)
			{
				// see if it's an alias:
				string[] aliasCommandArgs=m_CommandAliases[split[0]] as string[];
				// just replace the current command args:
				if (aliasCommandArgs!=null)
				{
					string[] newSplit=new string[split.Length-1+aliasCommandArgs.Length];
					if (split.Length>1)
					{
						Array.Copy(split,1,newSplit,0,split.Length-1);
					}
					Array.Copy(aliasCommandArgs,0,newSplit,0,aliasCommandArgs.Length);
	
					split=aliasCommandArgs;
				}

				// see if it's a rag side command:
				split[0]=split[0].Trim();
				CommandDelegate theCommand=m_CommandMaps[split[0]] as CommandDelegate;
				if (theCommand!=null)
				{
					string[] splitWithoutCommand = new string[split.Length-1];
					Array.Copy(split,1,splitWithoutCommand,0,split.Length-1);
					WriteText(theCommand(splitWithoutCommand));
				}
				else if (sm_IsConnected==true)
				{
					// it's a game side command:
					SendCommand(split);		
				}
			}
		}

		private void AddInt(byte[] buffer,ref int index,int theInt)
		{
			buffer[index++] = (byte) (theInt);
			buffer[index++] = (byte) (theInt >> 8);
			buffer[index++] = (byte) (theInt >> 16);
			buffer[index++] = (byte) (theInt >> 24);
		}

		private void AddShort(byte[] buffer,ref int index,ushort theShort)
		{
			buffer[index++]=(byte)(theShort);
			buffer[index++]=(byte)(theShort >> 8);
		}

		private void AddString(byte[] buffer,ref int index,string theString)
		{
			if (theString == null)
				buffer[index++] = 0;
			else 
			{
				int sl = theString.Length;
				for (int i=0;i<sl;i++)
					buffer[index++] = Convert.ToByte(theString[i]);
				buffer[index++] = 0x0; // null byte
			}
		}

		private void SendCommand(string[] commandArray)
		{
			int commandLen=0;

			// collect valid strings:
			ArrayList strings=new ArrayList();
			string commandName=null;
			foreach (string theStringIter in commandArray)
			{
				string theString=theStringIter.Trim();
				if (theString!=null)
				{
					if (commandName==null)
					{
						commandName=theString;
					}
					else
					{
						strings.Add(theString);
						commandLen+=theString.Length+1; // +1 for NULL
					}
				}
			}

			const int HEADER_SIZE=2*3; // 2*3 == sizeof(unsigned short)*3
			const int OFFSET_SIZE=4; // 4 == sizeof(unsigned int)
			int commandSize=commandLen+HEADER_SIZE+(OFFSET_SIZE*strings.Count)+commandName.Length+1;

			byte[] buffer=new byte[commandSize];
			int index=0;
			
			// add header:
			AddShort(buffer,ref index,(ushort)strings.Count);
			AddShort(buffer,ref index,(ushort)commandLen);
			AddShort(buffer,ref index,(ushort)(commandName.Length+1));

			// add arguments:
			foreach (string theString in strings)
			{
				AddString(buffer,ref index,theString);
			}

			// add offset:
			int offset=0;
			foreach (string theString in strings)
			{
				AddInt(buffer,ref index,offset);
				offset+=theString.Length+1;
			}

			// add command name:
			AddString(buffer,ref index,commandName);
			
			// send to the application:
			Debug.Assert(index==buffer.Length);
			sm_Socket.Send(buffer);

			// wait for return message:
			int available;
			while (true)
			{
				available=sm_Socket.Available;

				if (available>0)
				{
					byte[] readBytes=new byte[available];
					int len=sm_Socket.Receive(readBytes);

					// see if we find the end of command signal:
					if (readBytes[readBytes.Length-1]==1)
					{
						String theString=new string(Encoding.ASCII.GetChars(readBytes,0,readBytes.Length-1));
						theString=theString.Replace("\n","\r\n");
						WriteText(theString);
						break;
					}
					else if (readBytes[readBytes.Length-1]==0)
					{
						this.WriteText("'" + commandName + "' is not recognized as a command");
						break;
					}
					else
					{
						String theString=new string(Encoding.ASCII.GetChars(readBytes));
						theString=theString.Replace("\n","\r\n");
						WriteText(theString);
					}
	
				}
			}	

		}

		private string CommandAlias(string[] args)
		{
			if (args.Length<1)
			{
				string aliases="aliases:\r\n";
				IDictionaryEnumerator enumAliases=m_CommandAliases.GetEnumerator();
				while (enumAliases.MoveNext())
				{
					string aliasName = enumAliases.Key as String;
					string[] aliasArgs = enumAliases.Value as string[];
					string aliasLine="";
					foreach (string arg in aliasArgs)
					{
						if (arg.IndexOf(" ")>=0 || arg.IndexOf("\t")>=0)
						{
							aliasLine+= "\"" + arg + "\"" + " ";
						}
						else
							aliasLine+=arg + " ";
					}
					aliases += "\t" + aliasName + " -> " + aliasLine;
				}

				return aliases;
			}

			// help command...
			if (args[0].ToLower()=="-help")
			{
				string helpString="ALIAS [-help] [[-delete] [alias name]] [-deleteall] [alias name] [alias command] [alias arg1] ... [alias argN]\r\n\r\n" +
					"\t-help\t\t\tshows this message.\r\n" + 
					"\t-delete\t\t\tdeletes an alias.\r\n" +
					"\talias name\t\tthe alias to delete.\r\n" +
					"\t-deleteall \t\tdeletes all aliases.\r\n" +
					"\talias name\t\tthe name of the alias to create.\r\n" +
					"\talias command\t\tthe command name to alias to.\r\n" +
					"\talias arg1\t\tthe first argument to the command to be aliased.";
				return helpString;
			}
			// ...delete command...
			else if (args[0].ToLower()=="-delete")
			{
				if (args.Length<=1)
					return "You must specify what alias you wish to delete";
				m_CommandAliases.Remove(args[1]);
			}
			// ...delete all command...
			else if (args[0].ToLower()=="-deleteall")
			{
				m_CommandAliases.Clear();
			}
			// ...an actual alias:
			else 
			{
				if (args.Length<2)
				{
					return "Incorrect # of arguments to alias command";
				}

				// nice try a**hole:
				if (m_CommandAliases.Contains(args[0]))
				{
					return "'" + args[0] +"' already aliased";
				}

				// nice try a**hole - the sequel:
				if (m_CommandMaps.Contains(args[0]))
				{
					return "Alias name '" + args[0] +"' is already a command";
				}

				string[] newArgs=new string[args.Length-1];
				Array.Copy(args,1,newArgs,0,newArgs.Length);
				
				m_CommandAliases.Add(args[0],newArgs);
			}

			return "";
		}

		public delegate string CommandDelegate(string[] args);

		public void AddCommand(string commandName,CommandDelegate theDelegate)
		{
			Debug.Assert(!m_CommandMaps.Contains(commandName));
			m_CommandMaps[commandName]=theDelegate;
		}
																  
		Hashtable m_CommandMaps=new Hashtable();
		Hashtable m_CommandAliases=new Hashtable();
	}
}
