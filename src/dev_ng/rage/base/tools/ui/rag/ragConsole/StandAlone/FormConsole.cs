using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;

using ragConsole;

namespace StandAlone
{
	/// <summary>
	/// Summary description for FormConsole.
	/// </summary>
	public class FormConsole : System.Windows.Forms.Form
	{
		private ragConsole.ConsolePanel consolePanel1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public FormConsole()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			ConsolePanel.Connect();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(FormConsole));
			this.consolePanel1 = new ragConsole.ConsolePanel();
			this.SuspendLayout();
			// 
			// consolePanel1
			// 
			this.consolePanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.consolePanel1.Location = new System.Drawing.Point(0, 0);
			this.consolePanel1.Name = "consolePanel1";
			this.consolePanel1.Size = new System.Drawing.Size(808, 589);
			this.consolePanel1.TabIndex = 0;
			// 
			// FormConsole
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(808, 589);
			this.Controls.Add(this.consolePanel1);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "FormConsole";
			this.Text = "FormConsole";
			this.Closing += new System.ComponentModel.CancelEventHandler(this.FormConsole_Closing);
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new FormConsole());
		}

		private void FormConsole_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			ConsolePanel.Shutdown();
		}
	}
}
