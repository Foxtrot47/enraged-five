using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Data;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using ragCore;

using RSG.Base.Forms;
using ragWidgets;
using RSG.Base.Logging;

namespace ragConsole
{
	/// <summary>
	/// Summary description for ConsolePanel.
	/// </summary>
    public class ConsolePanel : System.Windows.Forms.UserControl
    {
        private UILibrary.ShellControl ragConsoleControl1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button LoadButton;
        private System.Windows.Forms.OpenFileDialog OpenCommandFile;

        public static bool ConnectedToProxy { get; protected set; }
        public static ConsoleEngine Console { get; protected set; }

        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.Container components = null;

        public ConsolePanel()
        {
            // This call is required by the Windows.Forms Form Designer.
            InitializeComponent();

            this.CreateHandle();

            Console.AddCommand("cls", CommandClearScreen);

            // delegate to a delegate... because the callback might come from any thread, and we need to write from the UI thread
            Console.DisplayLocalMessage += delegate(string s) {
                this.BeginInvoke((MethodInvoker)(() => WriteResultToConsole(s, true, true)));
            };
            Console.SlowCommandWarning = SlowCommandWarning;

            Debug.Assert( sm_Instance == null );
            sm_Instance = this;

            ragConsoleControl1.FocusTextBox();

            if (GameConnection.CurrentClientGameConnection != null)
            {
                CONSOLE_PROXY_PORT = GameConnection.CurrentClientGameConnection.ConsolePort;
            }
        }

        private static ConsolePanel sm_Instance;
        private static BankManagerConsoleSupport m_BankManagerSupport;
        private static IBankManager m_BankManager;

        public static ConsolePanel Instance
        {
            get
            {
                return sm_Instance;
            }
        }

        public static void ShutdownWindow()
        {

            sm_Instance = null;
        }

        public static void Initialize(ragCore.IBankManager bankManager, bool connectedToProxy, ILog log)
        {
            ConnectedToProxy = connectedToProxy;

            Console = new ConsoleEngine(log);
            ConnectProc();

            //This call initializes the Console object with essential commands, even though it
            //looks like it is doing nothing.
            m_BankManager = bankManager;
            m_BankManagerSupport = new BankManagerConsoleSupport(Console, m_BankManager);

            ConsolePanel.Console.AddCommand("sync", SynchronizeCommand);
            ConsolePanel.Console.AddCommand("getupdates", GetUpdatesCommand);

            if (ConnectedToProxy == false)
            {
				//The ConsoleProxy will be already listening for remote connection on this port if the proxy is available.
                ConsolePanel.Console.ListenForRemoteConnections();
            }
        }

        public static void Shutdown()
        {
            Console.Shutdown();

            ShutdownWindow();
        }

        const int RAG_CONSOLE_PORT_OFFSET = (int)ragCore.PipeID.EnumSocketOffset.SOCKET_OFFSET_USER5 + 1;
        const int CONSOLE_PORT = 5050;
        static int CONSOLE_PROXY_PORT = 5105;

        public static void ConnectProc()
        {
            ragCore.PipeID pipe;
            if (ConnectedToProxy == false)
            {
                if (PlugIn.Instance != null)
                {
                    pipe = PlugIn.Instance.HostData.MainRageApplication.GetRagPipeSocket(RAG_CONSOLE_PORT_OFFSET);
                }
                else
                {
                    pipe = new ragCore.PipeID(CONSOLE_PORT);
                }

                Console.ConnectToGame(pipe);             
            }
            else
            {
                pipe = new ragCore.PipeID(CONSOLE_PROXY_PORT);

                Console.ConnectToProxy(pipe);
            }

        }

        public void DisplayString(string message)
        {
            Invoke((MethodInvoker)delegate() { FlushCommandResults(message + "\n"); });
        }

        private void FlushCommandResults( string result )
        {
            if (true) // TODO: How do we handle this? ( m_CommandQueue.Count == 0 )
            {
                ragConsoleControl1.DisableInput = false;
                ragConsoleControl1.WriteText( result, false, true );
            }
            else
            {
                //ragConsoleControl1.WriteText( result );
            }
        }

        delegate void FlushCommandResultsDel( string result );

        private string CommandClearScreen( string[] args )
        {
            ragConsoleControl1.Clear();
            return "";
        }


        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose( bool disposing )
        {
            if ( disposing )
            {
                if ( components != null )
                {
                    components.Dispose();
                }
            }
            base.Dispose( disposing );
        }

        #region Component Designer generated code
        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.LoadButton = new System.Windows.Forms.Button();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.panel2 = new System.Windows.Forms.Panel();
            this.ragConsoleControl1 = new UILibrary.ShellControl();
            this.OpenCommandFile = new System.Windows.Forms.OpenFileDialog();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add( this.LoadButton );
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point( 0, 0 );
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size( 368, 32 );
            this.panel1.TabIndex = 1;
            // 
            // LoadButton
            // 
            this.LoadButton.Location = new System.Drawing.Point( 8, 6 );
            this.LoadButton.Name = "LoadButton";
            this.LoadButton.Size = new System.Drawing.Size( 64, 24 );
            this.LoadButton.TabIndex = 1;
            this.LoadButton.Text = "Load...";
            this.LoadButton.Click += new System.EventHandler( this.LoadButton_Click );
            // 
            // splitter1
            // 
            this.splitter1.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter1.Enabled = false;
            this.splitter1.Location = new System.Drawing.Point( 0, 32 );
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size( 368, 3 );
            this.splitter1.TabIndex = 0;
            this.splitter1.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.Controls.Add( this.ragConsoleControl1 );
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point( 0, 35 );
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size( 368, 445 );
            this.panel2.TabIndex = 0;
            // 
            // ragConsoleControl1
            // 
            this.ragConsoleControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ragConsoleControl1.Location = new System.Drawing.Point( 0, 0 );
            this.ragConsoleControl1.Name = "ragConsoleControl1";
            this.ragConsoleControl1.Prompt = ">>>";
            this.ragConsoleControl1.ShellTextBackColor = System.Drawing.Color.Black;
            this.ragConsoleControl1.ShellTextFont = new System.Drawing.Font( "Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)) );
            this.ragConsoleControl1.ShellTextForeColor = System.Drawing.Color.LawnGreen;
            this.ragConsoleControl1.Size = new System.Drawing.Size( 368, 445 );
            this.ragConsoleControl1.TabIndex = 0;
            this.ragConsoleControl1.CommandEntered += new UILibrary.EventCommandEntered( this.ragConsoleControl_CommandEntered );
            // 
            // OpenCommandFile
            // 
            this.OpenCommandFile.DefaultExt = "txt";
            this.OpenCommandFile.Filter = "Text Files|*.txt|All files|*.*";
            this.OpenCommandFile.Multiselect = true;
            this.OpenCommandFile.Title = "Open Command File";
            // 
            // ConsolePanel
            // 
            this.Controls.Add( this.panel2 );
            this.Controls.Add( this.splitter1 );
            this.Controls.Add( this.panel1 );
            this.Name = "ConsolePanel";
            this.Size = new System.Drawing.Size( 368, 480 );
            this.panel1.ResumeLayout( false );
            this.panel2.ResumeLayout( false );
            this.ResumeLayout( false );

        }
        #endregion

        public bool PromptAfterTimeout { get; set; }

        bool SlowCommandWarning(string commandName)
        {
            DialogResult res = DialogResult.No;

            if (this.PromptAfterTimeout)
            {
                res = rageMessageBox.ShowQuestion(
                String.Format("This command '{0}' is taking a long time to complete.  Would you like to keep waiting?", commandName),
                "Console Command Timed Out");
            }

            return (res == DialogResult.Yes);
        }

        public void LoadCommandFileWhenConnected( string filename, bool writeResult )
        {
            Console.WhenConnectedToGame(delegate()
            {
                string res = Console.LoadCommandFile(filename);
                if (writeResult)
                {
                    WriteResultToConsole(res, false, false); // TODO: Only print the prompt on the _last_ command file?
                }
            });
        }

        private void LoadButton_Click( object sender, System.EventArgs e )
        {

            DialogResult result = OpenCommandFile.ShowDialog();
            if ( result != DialogResult.OK )
                return;

            string resultString = "";
            ragConsoleControl1.WriteText( "", true, false );
            foreach ( string fileName in OpenCommandFile.FileNames )
            {
                resultString += Console.LoadCommandFile( fileName ) + "\r\n";
            }

            WriteResultToConsole(resultString, false, true);
        }

        private void WriteResultToConsole(string result, bool initialNewline, bool finalPrompt)
        {
            // make sure we end with a \n
            if (result.EndsWith("\n"))
            {
                ragConsoleControl1.WriteText(result, initialNewline, finalPrompt);
            }
            else
            {
                ragConsoleControl1.WriteText(result, initialNewline, false);
                ragConsoleControl1.WriteText("\r\n", false, finalPrompt);
            }
        }

        public void ExecuteCommandAndWriteToConsole( string commandString )
        {
            string result = Console.ExecuteCommandAndWait(commandString);
            WriteResultToConsole(result, false, false);
        }

        private void ragConsoleControl_CommandEntered( object sender, UILibrary.CommandEnteredEventArgs e )
        {
            ExecuteCommandAndWriteToConsole( e.Command );
        }

        private static string SynchronizeCommand(string[] args)
        {
            // sends a delayed ping message to the game. That way we're sure all previous commands have finished executing
            // and can read any widgets that changed as a result

            // this is ugly - we need a better way to maintain the bankpipe:
            BankPipe p = m_BankManager.AllBanks[0].Pipe;

            using (Semaphore sema = new Semaphore(0, 1))
            {
                m_BankManager.SendDelayedPing(
                    (EventHandler)((object owner, EventArgs e) => sema.Release())
                    , p);

                sema.WaitOne();
            }
            return "";
        }


        private static Dictionary<Widget, Widget> m_GetupdatesWidgets = new Dictionary<Widget, Widget>();
        private static string GetUpdatesCommand(string[] args)
        {
            if (args.Length == 1 && args[0] == "-help")
            {
                return "Usage: getupdates <widgetname> [on|off]";
            }

            if (args.Length == 1)
            {
                Widget w = m_BankManager.FindFirstWidgetFromPath(args[0]);
                if (w != null && m_GetupdatesWidgets.ContainsKey(w))
                {
                    return "true";
                }
                return "false";
            }

            if (args.Length == 2)
            {
                Widget widget = m_BankManager.FindFirstWidgetFromPath(args[0]);

                if (widget == null)
                {
                    return "false";
                }

                if (args[1] == "on")
                { // turn on updates
                    if (m_GetupdatesWidgets.ContainsKey(widget))
                    {
                        // already getting updates for this widget
                        return "already on";
                    }
                    else
                    {
                        WidgetGroupBase group = widget as WidgetGroupBase;
                        if (group == null)
                        {
                            return "not a group widget";
                        }
                        do
                        {
                            if (group != null)
                                group.AddRefGroup();
                            group = group.Parent as WidgetGroupBase;
                        }
                        while (group != null);

                        m_GetupdatesWidgets.Add(widget, widget);
                    }
                }
                else if (args[1] == "off")
                {
                    if (!m_GetupdatesWidgets.ContainsKey(widget))
                    {
                        return "already off";
                    }
                    else
                    {
                        WidgetGroupBase group = widget as WidgetGroupBase;
                        if (group == null)
                        {
                            return "not a group widget";
                        }
                        do
                        {
                            if (group != null)
                                group.ReleaseGroup();
                            group = group.Parent as WidgetGroupBase;
                        }
                        while (group != null);

                        m_GetupdatesWidgets.Remove(widget);
                    }
                }
            }

            return "";

        }
    }
}
