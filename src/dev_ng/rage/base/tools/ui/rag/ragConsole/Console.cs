using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Data;
using System.Net;
using System.Net.Sockets;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Windows.Forms;

using ragCore;
using ragWidgetPage;
using ragWidgets;
using RSG.Base.Logging;

namespace ragConsole
{
	/// <summary>
	/// Summary description for UserControl1.
	/// </summary>
	public class Console : ragWidgetPage.WidgetPage
    {
        #region Static Member Data
        /// <summary>
        /// Name of this console (shouldn't really be public?)
        /// </summary>
        public static string MyName = "Console";

        /// <summary>
        /// Singleton instance of this console.
        /// </summary>
        public static Console sm_Instance;

        /// <summary>
        /// Image to use for this plugin.
        /// </summary>
        private static System.Drawing.Bitmap sm_Bitmap = null;
        #endregion // Static Member Data

        #region Private Member Data
        private KeyboardConsoleSupport m_KeyboardSupport;
        private ragConsole.ConsolePanel consolePanel1;
        #endregion // Private Member Data

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public Console()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();
		}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="appName"></param>
        /// <param name="bankMgr"></param>
        /// <param name="keyProcessor"></param>
        /// <param name="dockControl"></param>
        public Console(String appName, IBankManager bankMgr, IKeyUpProcessor keyProcessor, TD.SandDock.DockControl dockControl, ILog log)
			: base(appName, bankMgr,keyProcessor, dockControl, log)
		{
			InitializeComponent();

            // does it kill us to not do this?
			//int handle=this.Handle.ToInt32();  // *sigh* this insures that the windows handle is actually created!
			//Debug.Assert(handle>0);

            m_KeyUpProcessor = keyProcessor;

            m_KeyboardSupport = new KeyboardConsoleSupport(ConsolePanel.Console, 
                (ConsoleEngine.ProcessResultCallback)delegate(string s, bool remote)
                {
                    consolePanel1.DisplayString(s);
                }
            );
            m_KeyUpProcessor.AddProcessKeyUpEvent(this, m_KeyboardSupport.ProcessKeyEvent);
		}
        #endregion // Constructor(s)

        #region WidgetPage Overrides
        public override string GetMyName()
        {
            return MyName;
        }

		public override System.Drawing.Bitmap GetBitmap() 
		{
			if (sm_Bitmap==null)
			{
				sm_Bitmap=new Bitmap(Assembly.GetExecutingAssembly().GetManifestResourceStream("ragConsole.console.ico"));
			}
			return sm_Bitmap;
		}

		public override string GetTabText() 
		{
			return MyName;
		}

        public override bool IsSingleInstance
        {
            get
            {
                return true;
            }
        }
        #endregion // WidgetPage Overrides

        #region Static Methods
        public static ragWidgetPage.WidgetPage CreatePage(String appName, IBankManager bankMgr, IKeyUpProcessor keyProcessor, TD.SandDock.DockControl dockControl, ILog log)
		{
            sm_Instance = new Console( appName, bankMgr, keyProcessor, dockControl, log );

            String commandFile = PlugIn.Instance.GetCommandFile();
            if ( commandFile != String.Empty )
            {
                sm_Instance.consolePanel1.LoadCommandFileWhenConnected(commandFile, true);
            }

			return sm_Instance;
		}

		public static void Shutdown()
		{
            sm_Instance.m_KeyUpProcessor.RemoveProcessKeyUpEvent(sm_Instance);
            ConsolePanel.Shutdown();
            sm_Instance = null;
		}
        #endregion // Static Methods

        #region Component Designer generated code
        /// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.consolePanel1 = new ragConsole.ConsolePanel();
            this.SuspendLayout();
            // 
            // consolePanel1
            // 
            this.consolePanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.consolePanel1.Location = new System.Drawing.Point(0, 0);
            this.consolePanel1.Name = "consolePanel1";
            this.consolePanel1.Size = new System.Drawing.Size(714, 255);
            this.consolePanel1.TabIndex = 0;
            // 
            // Console
            // 
            this.Controls.Add(this.consolePanel1);
            this.Name = "Console";
            this.Size = new System.Drawing.Size(714, 255);
            this.ResumeLayout(false);

		}
		#endregion // Component Designer Generated Code
    } // Console
}
