using System;
using System.Collections.Generic;
using System.Windows.Forms;
using ragWidgets;
using RSG.Base.Logging;

namespace ragConsole
{
	/// <summary>
	/// Summary description for Class1.
	/// </summary>
    public class PlugIn : ragWidgetPage.WidgetPagePlugIn
    {
        #region WidgetPagePlugIn overrides
        public override string Name
        {
            get
            {
                return "Console";
            }
        }

        public override void InitPlugIn( PlugInHostData hostData )
        {
            m_hostData = hostData;

            base.InitPlugIn( hostData );
            sm_Instance = this;

            m_MenuItem.ShortcutKeys = (Keys)(Keys.Control | Keys.Shift | Keys.C);

            // register our WidgetPage with its CreatePageDel
            base.m_pageNameToCreateDelMap[Console.MyName] = new ragWidgetPage.WidgetPage.CreatePageDel( Console.CreatePage );

            hostData.MainRageApplication.BankMgr.InitFinished += new ragCore.InitFinishedHandler( BankMgr_InitFinished );

            ConsolePanel.Initialize(hostData.MainRageApplication.BankMgr, hostData.ConnectedToProxy, hostData.Log); // For annoying reasons - need to start up the ConsoleEngine and the connection thread here 
            // otherwise rag itself will try listening on that port
        }

        public override void RemovePlugIn()
        {
            base.RemovePlugIn();
            ConsolePanel.Shutdown();
            sm_Instance = null;
        }

        public override bool CheckPluginStatus()
        {
            return true;
        }

        public override void ActivatePlugInMenuItem( object sender, EventArgs args )
        {
            // Create the the window if it doesn't exist:
            if ( Console.sm_Instance == null || Console.sm_Instance.DockControl == null)
            {
                base.CreateAndOpenView(base.m_pageNameToCreateDelMap[Console.MyName], LogFactory.ApplicationLog);
            }
            else if ( !Console.sm_Instance.DockControl.IsOpen )
            {
                Console.sm_Instance.DockControl.Open( TD.SandDock.WindowOpenMethod.OnScreenActivate );
            }
        }

        public override bool ShowOnActivation
        {
            get
            {
                return true;
            }
        }
        #endregion

        public static PlugIn Instance
        {
            get
            {
                return sm_Instance;
            }
        }

        protected static PlugIn sm_Instance;

        /// <summary>
        /// Parses the arguments from HostData and retrieves the value of the rag.Console.LoadCommandFile parameter
        /// </summary>
        /// <returns>string.Empty when not found</returns>
        public string GetCommandFile()
        {
            string[] splitArgs = ragCore.Args.SplitArgs( sm_HostData.MainRageApplication.Args );
            if ( splitArgs != null )
            {
                foreach ( string arg in splitArgs )
                {
                    string theValue = null;
                    ragCore.Args.CheckArg( arg, "-rag.Console.LoadCommandFile", ref theValue );
                    if ( theValue != null && theValue != "" )
                    {
                        return theValue;
                    }
                }
            }

            return String.Empty;
        }

        PlugInHostData m_hostData;

        public PlugInHostData HostData
        {
            get
            {
                return m_hostData;
            }
        }

        private void BankMgr_InitFinished()
        {
            // now it's ok to prompt after a timeout
            if ( ConsolePanel.Instance != null )
            {
                string theValue = null;
                bool timeouts = true;
                string[] splitArgs = ragCore.Args.SplitArgs( sm_HostData.MainRageApplication.Args );
                if (splitArgs != null)
                {
                    foreach (string arg in splitArgs)
                    {
                        if (ragCore.Args.CheckArg(arg, "-rag_notimeouts", ref theValue))
                        {
                            timeouts = false;
                        }
                    }
                }

                if (timeouts == true)
                {
                    ConsolePanel.Instance.PromptAfterTimeout = true;
                }
            }
        }
    }
}
