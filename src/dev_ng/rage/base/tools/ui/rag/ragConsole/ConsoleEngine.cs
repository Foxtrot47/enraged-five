﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using RSG.Base.Logging;

namespace ragConsole
{
    // PURPOSE:
    //  Provides the back-end processing for the rag console, incl. communication with the game and communication with
    //  remote consoles.
    public class ConsoleEngine
    {
        #region Types and Constants

        public delegate string CommandDelegate(string[] args);
        public delegate void ProcessResultCallback(string result, bool executedRemotely);

        public delegate bool SlowCommandWarningDelegate(string s);

        #endregion


        public ConsoleEngine(ILog log)
        {
            _log = log;

            // Core functions
            AddCommand("alias", CommandAlias);
            AddCommand("loadCommandFile", CommandLoadCommandFile);

            // Remote console connection functions
            AddCommand("listen", CommandListen);
            AddCommand("remoteecho", CommandRemoteEcho);

            AddCommand("test", delegate(string[] args) {return "passed!";});

            // Standard warning for slow commands - users can replace this
            SlowCommandWarning = DefaultSlowCommandWarning;
        }

        public void ConnectToGame(ragCore.PipeID pipe)
        {
            m_GamePipe = pipe;
            m_GameConnectionThread = new Thread(GameConnectionProc);
            m_GameConnectionThread.Name = "Console Connection Thread";
            m_GameConnectionThread.Start();
            m_GameConnectionThread.Priority = ThreadPriority.BelowNormal;
        }

        public void ConnectToProxy(ragCore.PipeID pipe)
        {
            m_GamePipe = pipe;
            m_GameConnectionThread = new Thread(ProxyConnectionProc);
            m_GameConnectionThread.Name = "Console Connection Proxy Thread";
            m_GameConnectionThread.Start();
            m_GameConnectionThread.Priority = ThreadPriority.BelowNormal;
        }

        public void Shutdown()
        {
            m_CreateResetEvent.Set();

            if (m_GameConnectionThread != null)
            {
                m_TerminateGameThread = true;
                m_GameConnectionThread.Join(); // wait for thread to terminate
            }
            
            if (m_GameConnectionThread != null)
            {
                m_GameConnectionThread.Abort();
                m_GameConnectionThread = null;
            }
        }

        #region Events

        /// <summary>
        /// Called when the local console should display a message 
        /// </summary>
        public event Action<string> DisplayLocalMessage;

        /// <summary>
        /// Called when a console command is taking a long time to complete. 
        /// Return 'true' if you want to keep waiting.
        /// </summary>
        /// Note: this is not an event - you can replace the warning with your own.
        public SlowCommandWarningDelegate SlowCommandWarning;

        #endregion

        #region Member variables

        private readonly ILog _log;

        Dictionary<string, CommandDelegate> m_CommandMaps = new Dictionary<string, CommandDelegate>();
        Dictionary<string, string[]> m_CommandAliases = new Dictionary<string, string[]>();

        private bool m_RemoteEcho = false;

        private TcpListener m_RemoteServerListener;
        private Thread m_RemoteServerThread;

        private List<TcpClient> m_AllRemoteClients = new List<TcpClient>();

        private Queue<Action> m_OnConnectionActions = new Queue<Action>();

        private object m_ConnectEventLock = new object();
        private object m_CommandLock = new object();

        ragCore.PipeID m_GamePipe;
        private Socket m_GameSocket;
        private Thread m_GameConnectionThread;
        private bool m_IsConnectedToGame = false;

        private bool m_TerminateGameThread = false;

        class RemoteCommand
        {
            public string[] arguments;
            public ConsoleEngine.ProcessResultCallback resultCb;
       }

        private Mutex m_CommandMutex = new Mutex();
        private Queue<RemoteCommand> m_CommandQueue = new Queue<RemoteCommand>();

        // eventually this needs to be per-connection
        bool        m_DosNewlines = true;
        Regex       m_FindBareNL = new Regex(@"(?<!\r)\n"); // finds \n not preceeded by \r

        #endregion

        #region Connection to the game


        /// <summary>
        /// Perform an action as soon as a connection to the game is established (or immediately, if there is already a connection)
        /// </summary>
        /// <param name="a"></param>
        public void WhenConnectedToGame(Action a)
        {
            lock (m_ConnectEventLock)
            {
                if (m_IsConnectedToGame)
                {
                    // execute immediately
                    a();
                }
                else
                {
                    // add to the queue
                    m_OnConnectionActions.Enqueue(a);
                }
            }
        }

        private void ProxyConnectionProc()
        {
            try
            {
                TcpClient client = new TcpClient(IPAddress.Loopback.ToString(), m_GamePipe.Port);
                m_GameSocket = client.Client;
            }
            catch(Exception)
            {
                m_GameSocket = null;
            }

            if (m_GameSocket == null)
            {
                m_IsConnectedToGame = false;
                return;
            }

            m_IsConnectedToGame = true;
            ProcessCommands();
        }

        private void GameConnectionProc()
        {
            WaitForGameConnection(m_GamePipe);

            if (m_GameSocket == null)
            {
                // couldn't get a connection for some reason - don't process any commands.
                return;
            }

            ProcessCommands();
        }

        private void WaitForGameConnection(ragCore.PipeID pipe)
        {
            TcpListener listener = new TcpListener(pipe.Address, pipe.Port);
            listener.Start();

            while (!listener.Pending() && !m_TerminateGameThread)
            {
                Thread.Sleep(100);
            }

            if (m_TerminateGameThread)
            {
                return;
            }

            m_GameSocket = listener.AcceptSocket();

            lock (m_ConnectEventLock)
            {
                m_IsConnectedToGame = true;
                while (m_OnConnectionActions.Count > 0)
                {
                    Action a = m_OnConnectionActions.Dequeue();
                    a();
                }
            }
        }

        private void ProcessCommands()
        {
            // TODO: Make this not spin forver
            while (!m_TerminateGameThread)
            {
                // grab the command from the queue:
                m_CommandMutex.WaitOne();
                RemoteCommand cmd = null;

                int commandBufferCount = m_CommandQueue.Count;
                if (commandBufferCount > 0)
                {
                    cmd = m_CommandQueue.Dequeue();
                }

                m_CommandMutex.ReleaseMutex();

                string result = null;

                if (cmd == null)
                {
                    // Wait here or the thread will just loop and take lots of cpu
                    // if there's no action going on...
                    Thread.Sleep( 10 );
                    continue;
                }

                // dispatch the command
                if (cmd.arguments != null)
                {
                    SendCommand(cmd.arguments);

                    result = ReceiveResponse(cmd.arguments[0]);
                }
                else
                {
                    result = ReceiveResponse();
                }

                if (cmd.resultCb != null)
                {
                    result = ConvertNewlines(result);
                    cmd.resultCb(result, true);
                }
            }
        }


        private void AddInt(byte[] buffer, ref int index, int theInt)
        {
            buffer[index++] = (byte)(theInt);
            buffer[index++] = (byte)(theInt >> 8);
            buffer[index++] = (byte)(theInt >> 16);
            buffer[index++] = (byte)(theInt >> 24);
        }

        private void AddShort(byte[] buffer, ref int index, ushort theShort)
        {
            buffer[index++] = (byte)(theShort);
            buffer[index++] = (byte)(theShort >> 8);
        }

        private void AddString(byte[] buffer, ref int index, string theString)
        {
            if (theString == null)
                buffer[index++] = 0;
            else
            {
                int sl = theString.Length;
                for (int i = 0; i < sl; i++)
                    buffer[index++] = Convert.ToByte(theString[i]);
                buffer[index++] = 0x0; // null byte
            }
        }

        private void SendCommand(string[] commandArray)
        {
            if (ConsolePanel.ConnectedToProxy == true)
            {
                string commandString = "";
                int arrayIndex = 0;
                foreach (string command in commandArray)
                {
                    commandString += command;
                    if (arrayIndex != commandArray.Length - 1)
                        commandString += " ";
                    else
                        commandString += "\n";

                    arrayIndex++;
                }

                //Send over the data as-is to the proxy to properly process.
                ASCIIEncoding encoder = new ASCIIEncoding();
                byte[] buffer = encoder.GetBytes(commandString);
                m_GameSocket.Send(buffer);
            }
            else
            {
                int commandLen = 0;

                // collect valid strings:
                List<string> strings = new List<string>();
                string commandName = null;
                foreach (string theStringIter in commandArray)
                {
                    string theString = theStringIter.Trim();
                    if (theString != null)
                    {
                        if (commandName == null)
                        {
                            commandName = theString;
                        }
                        else
                        {
                            strings.Add(theString);
                            commandLen += theString.Length + 1; // +1 for NULL
                        }
                    }
                }

                const int HEADER_SIZE = 2 * 3; // 2*3 == sizeof(unsigned short)*3
                const int OFFSET_SIZE = 4; // 4 == sizeof(unsigned int)
                int commandSize = commandLen + HEADER_SIZE + (OFFSET_SIZE * strings.Count) + commandName.Length + 1;

                byte[] buffer = new byte[commandSize];
                int index = 0;

                // add header:
                AddShort(buffer, ref index, (ushort)strings.Count);
                AddShort(buffer, ref index, (ushort)commandLen);
                AddShort(buffer, ref index, (ushort)(commandName.Length + 1));

                // add arguments:
                foreach (string theString in strings)
                {
                    AddString(buffer, ref index, theString);
                }

                // add offset:
                int offset = 0;
                foreach (string theString in strings)
                {
                    AddInt(buffer, ref index, offset);
                    offset += theString.Length + 1;
                }

                // add command name:
                AddString(buffer, ref index, commandName);

                // send to the application:
                Debug.Assert(index == buffer.Length);
                m_GameSocket.Send(buffer);
            }
        }

        private string ReceiveResponse(string commandName)
        {
            string resultString = string.Empty;

            int loopCount = 0;
            int loopMax = 1000;
            while (loopCount < loopMax)
            {
                int available = m_GameSocket.Available;
                if (available > 0)
                {
                    byte[] readBytes = new byte[available];
                    int len = m_GameSocket.Receive(readBytes);

                    // see if we find the end of command signal:
                    if (ConsolePanel.ConnectedToProxy == false)
                    {
                        if (readBytes[readBytes.Length - 1] == 1)
                        {
                            String theString = new string(Encoding.ASCII.GetChars(readBytes, 0, readBytes.Length - 1));
                            resultString += theString;

                            return resultString;
                        }
                        else if (readBytes[readBytes.Length - 1] == 0)
                        {
                            resultString = String.Format("'{0}' is not recognized as a command.", commandName);
                            return resultString;
                        }
                        else
                        {
                            String theString = new string(Encoding.ASCII.GetChars(readBytes));
                            resultString += theString;
                        }
                    }
                    else
                    {
                        //When using the proxy, send over the string data wholesale for the Proxy to process.
                        if (readBytes[readBytes.Length - 1] == 0)
                        {
                            resultString = new string(Encoding.ASCII.GetChars(readBytes));
                            return resultString;
                        }
                        else
                        {
                            String theString = new string(Encoding.ASCII.GetChars(readBytes));
                            resultString += theString;
                        }
                    }
                }
                else
                {
                    ++loopCount;

                    // if we just dispatched a command and it has timed out, prompt the user
                    if (loopCount == loopMax)
                    {
                        if (SlowCommandWarning != null)
                        {
                            if (SlowCommandWarning(commandName))
                            {
                                loopCount = 0;
                            }
                            else
                            {
                                return string.Empty;
                            }
                        }
                    }
                    else
                    {
                        Thread.Sleep(10);
                    }
                }
            }

            return null;
        }

        private string ReceiveResponse()
        {
            string resultString = string.Empty;

            int loopCount = 0;
            int loopMax = 1;
            while (loopCount < loopMax)
            {
                int available = m_GameSocket.Available;
                if (available > 0)
                {
                    byte[] readBytes = new byte[available];
                    int len = m_GameSocket.Receive(readBytes);

					if (ConsolePanel.ConnectedToProxy == false)
                    {
	                    // see if we find the end of command signal:
	                    if (readBytes[readBytes.Length - 1] == 1)
	                    {
	                        String theString = new string(Encoding.ASCII.GetChars(readBytes, 0, readBytes.Length - 1));
	                        resultString += theString;
	                        return resultString;
	                    }
	                    else if (readBytes[readBytes.Length - 1] == 0)
	                    {
	                        resultString = "Not recognized as a command";
	                        return resultString;
	                    }
	                    else
	                    {
	                        String theString = new string(Encoding.ASCII.GetChars(readBytes));
	                        resultString += theString;

	                        loopMax = 1000; // we found a partial string, so reset loopMax so that we can receive the rest of it
	                    }
					}
					else
                    {
                        //When using the proxy, send over the string data wholesale for the Proxy to process.
                     	if (readBytes[readBytes.Length - 1] == 0)
	                    {
                            resultString = new string(Encoding.ASCII.GetChars(readBytes));
	                        return resultString;
	                    }
	                    else
	                    {
	                        String theString = new string(Encoding.ASCII.GetChars(readBytes));
	                        resultString += theString;

	                        loopMax = 1000; // we found a partial string, so reset loopMax so that we can receive the rest of it
	                    }
                    }
                }
                else
                {
                    ++loopCount;

                    Thread.Sleep(10);
                }
            }

            return null;
        }

        private void SendCommandToThread(string[] commandArray, ConsoleEngine.ProcessResultCallback resultCb)
        {
            m_CommandMutex.WaitOne();
            RemoteCommand rc = new RemoteCommand();
            rc.arguments = commandArray;
            rc.resultCb = resultCb;
            m_CommandQueue.Enqueue(rc);
            m_CommandMutex.ReleaseMutex();
        }

        #endregion

        #region Connection to remote clients

        protected void DoDisplayLocalMessage(string message)
        {
            if (DisplayLocalMessage != null)
            {
                DisplayLocalMessage(message);
            }
        }

        public void ListenForRemoteConnections()
        {
            m_RemoteServerListener = new TcpListener(IPAddress.Any, 5105); // Some random unassigned port

            DoDisplayLocalMessage(String.Format("Listening for commands on port {0}\n", ((IPEndPoint)m_RemoteServerListener.LocalEndpoint).Port));
            m_RemoteServerThread = new Thread(ServerThread);
            m_RemoteServerThread.IsBackground = true;
            m_RemoteServerThread.Name = "Console remote server thread";
            m_RemoteServerThread.Start();
        }

        protected delegate TcpClient AcceptClientDel();
        private ManualResetEvent m_CreateResetEvent = new ManualResetEvent(false);

        private void ServerThread()
        {
            try
            {
                m_RemoteServerListener.Start();
            }
            catch (SocketException e)
            {
                _log.ToolExceptionCtx("Console Engine", e, "Could not start listening on port: {0}.", m_RemoteServerListener.LocalEndpoint );
                return;
            }

            while (true)
            {
                AcceptClientDel del = new AcceptClientDel(m_RemoteServerListener.AcceptTcpClient);
                IAsyncResult result = del.BeginInvoke(null, null);

                // Either wait indefinitely or for a given timeout period to accept a socket
                // or until another thread asks to cancel.
                WaitHandle[] handles = new WaitHandle[] { m_CreateResetEvent, result.AsyncWaitHandle };
                int handleIndex = WaitHandle.WaitAny(handles);
               
                if (handleIndex == WaitHandle.WaitTimeout)
                {
                    _log.ErrorCtx("Console Engine", "Timed out while waiting for connection to port {0}.", m_RemoteServerListener.LocalEndpoint.ToString());
                    break;
                }
                else if (handleIndex == 0)
                {
                    _log.MessageCtx("Console Engine", "Reset event received while waiting for connection to port {0}.", m_RemoteServerListener.LocalEndpoint.ToString());
                    break;
                }

                TcpClient client = del.EndInvoke(result);

                DoDisplayLocalMessage(String.Format("Got a new connection {0}\n\n", ((IPEndPoint)client.Client.RemoteEndPoint)));

                // Spawn a new thread for this clent
                Thread clientThread = new Thread(ClientThread);
                // make it a background thread? killing should be OK - client needs to handle shutdown
                clientThread.IsBackground = true;
                clientThread.Name = "Console client thread @ " + client.Client.RemoteEndPoint.ToString();
                clientThread.Start(client);
            }
        }

        private void ClientThread(object client)
        {
            TcpClient tcpClient = (TcpClient)client;
            NetworkStream stream = tcpClient.GetStream();

            string partialCommand = "";
            ASCIIEncoding encoder = new ASCIIEncoding();

            byte[] buffer = new byte[4096];

            char[] stringSeperators = new char[] { '\n' };

            bool bailingOut = false;

            // Call this function for every command result
            ProcessResultCallback resultCb = delegate(string result, bool executedRemotely)
            {
                lock (stream) // Not sure if the callback gets called from the same thread in remote and local execution... I think they
                // are different so lock just in case, so we don't get the results mixed up from seperate calls.
                {
                    byte[] endMarker = new byte[1] { 0 };
                    byte[] bytes = encoder.GetBytes(result);
                    stream.Write(bytes, 0, bytes.Length);
                    stream.Write(endMarker, 0, endMarker.Length);
                    if (m_RemoteEcho)
                    {
                        DoDisplayLocalMessage(String.Format("Response for {0}: {1}", (IPEndPoint)tcpClient.Client.RemoteEndPoint, result));
                    }
                }
            };

            while (true)
            {
                int bytesRead = 0;
                try
                {
                    bytesRead = stream.Read(buffer, 0, buffer.Length);
                }
                catch
                {
                    bailingOut = true;
                    break; // something bad happened to the stream - get out of here
                }

                if (bytesRead == 0)
                {
                    // stream was closed
                    break;
                }

                string bufferString = encoder.GetString(buffer, 0, bytesRead);
                string fullCommandStr = partialCommand + bufferString; // last bit of old string plus the new string

                fullCommandStr.Replace("\r", ""); // get rid of any unnecessary \rs

                string[] commands = fullCommandStr.Split(stringSeperators, StringSplitOptions.RemoveEmptyEntries);
                int numCommands = commands.Length;

                if (!fullCommandStr.EndsWith("\n"))
                {
                    partialCommand = commands[commands.Length - 1];
                    numCommands--;
                }
                else
                {
                    partialCommand = "";
                }

                for (int i = 0; i < numCommands; i++)
                {
                    bool gameSideCmd = false;
                    if (m_RemoteEcho)
                    {
                        DoDisplayLocalMessage(String.Format("Command for {0}: {1}", (IPEndPoint)tcpClient.Client.RemoteEndPoint, commands[i]));
                    }
                    ExecuteCommand(commands[i], ref gameSideCmd, resultCb); // calls the function above for each result
                }

            }

            if (!bailingOut)
            {
                DoDisplayLocalMessage(String.Format("Closing connection {0}\n", ((IPEndPoint)tcpClient.Client.RemoteEndPoint)));
            }

        }

        #endregion

        #region Command Processing

        public string ConvertNewlines(string s)
        {
            if (m_DosNewlines)
            {
                return m_FindBareNL.Replace(s, "\r\n");
            }
            return s;
        }

        public void AddCommand(string commandName, CommandDelegate theDelegate)
        {
            lock (m_CommandLock)
            {
                Debug.Assert(!m_CommandMaps.ContainsKey(commandName));
                m_CommandMaps[commandName.ToLower()] = theDelegate;
            }
        }

        public bool DefaultSlowCommandWarning(string s)
        {
            DoDisplayLocalMessage(String.Format("Command {0} is taking too long to execute. Not waiting anymore.\n", s));
            return false;
        }

        private string ConvertToString(List<char> chars)
        {
            char[] charArray;
            charArray = chars.ToArray();
            return new string(charArray);
        }

        private string[] SplitArgs(string theCommandString)
        {
            List<string> strings = new List<string>();
            List<char> argChars = new List<char>();
            bool lookingForEndQuote = false;

            // extract arguments ('"' keeps spaces together):
            for (int i = 0; i < theCommandString.Length; i++)
            {
                char theChar = theCommandString[i];
                if (theChar == '"')
                {
                    if (lookingForEndQuote)
                    {
                        strings.Add(ConvertToString(argChars));
                        argChars.Clear();
                        lookingForEndQuote = false;
                    }
                    else
                    {
                        lookingForEndQuote = true;
                    }
                }
                else if (Char.IsWhiteSpace(theChar))
                {
                    if (lookingForEndQuote)
                    {
                        argChars.Add(theChar);
                    }
                    else
                    {
                        if (argChars.Count > 0)
                        {
                            strings.Add(ConvertToString(argChars));
                            argChars.Clear();
                        }
                    }
                }
                else
                    argChars.Add(theChar);
            }

            // convert the final arg:
            if (argChars.Count > 0)
                strings.Add(ConvertToString(argChars));


            string[] returnValue;
            returnValue = strings.ToArray();

            return returnValue;
        }

        public string ExecuteCommand(string commandString, ref bool sendingCommand)
        {
            string[] split = SplitArgs(commandString);
            return ExecuteCommand(split, ref sendingCommand, null);
        }

        public string ExecuteCommand(string commandString, ref bool sendingCommand, ProcessResultCallback resultCb)
        {
            string[] split = SplitArgs(commandString);
            return ExecuteCommand(split, ref sendingCommand, resultCb);
        }

        public string ExecuteCommand(string[] split, ref bool sendingCommand, ProcessResultCallback resultCb)
        {
            sendingCommand = false;

            if (split.Length > 0)
            {
                // check if it's a comment:
                if (split[0].Trim().StartsWith("//"))
                    return "";

                // see if it's an alias:
                string[] aliasCommandArgs;
                // just replace the current command args:
                if (m_CommandAliases.TryGetValue(split[0].ToLower(), out aliasCommandArgs))
                {
                    string[] newSplit = new string[split.Length - 1 + aliasCommandArgs.Length];
                    Array.Copy(aliasCommandArgs, 0, newSplit, 0, aliasCommandArgs.Length);

                    // copy old arguments over so you can pass arguments through the alias:
                    if (split.Length > 1)
                    {
                        Array.Copy(split, 1, newSplit, aliasCommandArgs.Length, split.Length - 1);
                    }
                    split = newSplit;
                }

                // see if it's a rag side command:
                split[0] = split[0].Trim();
                CommandDelegate theCommand;
                lock (m_CommandLock)
                {
                    if (m_CommandMaps.TryGetValue(split[0].ToLower(), out theCommand))
                    {
                        string[] splitWithoutCommand = new string[split.Length - 1];
                        Array.Copy(split, 1, splitWithoutCommand, 0, split.Length - 1);
                        string result = theCommand(splitWithoutCommand);
                        result = ConvertNewlines(result);
                        if (resultCb != null)
                        {
                            resultCb(result, false);
                        }
                        return result;
                    }
                    else if (m_IsConnectedToGame == true)
                    {
                        sendingCommand = true;

                        // it's a game side command:
                        SendCommandToThread(split, resultCb);
                        return "";
                    }
                    else
                    {
                        string result = "Command '" + split[0] + "' unknown (NOTE that the game is not currently connected to the console.)";
                        if (resultCb != null) { resultCb(result, false); }
                        return result;
                    }
                }
            }

            // empty string is still a result (tho really, don't send an empty string)
            if (resultCb != null) { resultCb("", false); }
            return "";
        }

        public string ExecuteCommandAndWait(string commandString)
        {
            string result = "";

            using (Semaphore sema = new Semaphore(0, 1))
            {

                bool sendingCommand = false;
                ExecuteCommand(commandString, ref sendingCommand,
                    delegate(string s, bool remote)              // delegate for when the command is complete
                    {
                        result = s;                 // store the result string somewhere
                        sema.Release();
                    }
                );

                sema.WaitOne();
            }

            return result;
        }

        public string LoadCommandFile(string fileName)
        {
            string resultString = "";

            // try to open the file:
            TextReader reader = null;
            try
            {
                reader = File.OpenText(fileName);
            }
            catch (System.Exception)
            {
                return "Failed to open command file '" + fileName + "'";
            }

            string eol = ConvertNewlines("\n");

            // execute commands:
            string commandsString = reader.ReadToEnd();
            string[] commands = commandsString.Split(new char[] { '\n' });
            foreach (string command in commands)
            {
                string cmdResult = ExecuteCommandAndWait(command.Trim());

                if (cmdResult != null && cmdResult != "")
                {
                    resultString += cmdResult + eol;
                }
            }
            reader.Close();

            resultString += "loaded command file '" + fileName + "'";

            return resultString;
        }

        #endregion

        #region Standard Commands

        private string CommandLoadCommandFile(string[] args)
        {
            if (args.Length < 1)
            {
                string helpString = "Loads command files (*.txt) and executes the commands contained within the file.\n\n" +
                    "LOADCOMMANDFILE [fileName1] ... [fileNameN]\n\n" +
                    "\tfileName1\t\tthe first command file to load.\n" +
                    "\tfileNameN\t\tthe Nth command file to load.";
                return helpString;
            }

            string resultString = "";

            string eol = ConvertNewlines("\n");

            foreach (string fileName in args)
            {
                resultString += LoadCommandFile(fileName) + eol;
            }
            return resultString;
        }


        public static string GetArgArray(IDictionaryEnumerator enumArray, bool nameUpper)
        {
            string result = "";

            while (enumArray.MoveNext())
            {
                string name = enumArray.Key as String;
                if (nameUpper)
                    name = name.ToUpper();
                string[] args = enumArray.Value as string[];
                string line = "";
                // Recreate the command the alias will run - quoting any strings that contain whitespace
                foreach (string arg in args)
                {
                    if (arg.IndexOf(" ") >= 0 || arg.IndexOf("\t") >= 0)
                    {
                        line += "\"" + arg + "\"" + " ";
                    }
                    else
                        line += arg + " ";
                }
                result += "\t" + name + " -> " + line + "\n";
            }

            return result;
        }

        private string CommandAlias(string[] args)
        {
            if (args.Length < 1)
            {
                string aliases = "aliases:\n";
                aliases += GetArgArray(m_CommandAliases.GetEnumerator(), false);
                return aliases;
            }

            // help command...
            if (args[0].ToLower() == "-help")
            {
                string helpString = "aliases a one word name to a command string.\n\n" +
                    "ALIAS [-help] [[-delete] [alias name]] [-deleteall] [alias name] [alias command] [alias arg1] ... [alias argN]\n\n" +
                    "\t-help\t\t\tshows this message.\n" +
                    "\t-delete\t\t\tdeletes an alias.\n" +
                    "\talias name\t\tthe alias to delete.\n" +
                    "\t-deleteall \t\tdeletes all aliases.\n" +
                    "\talias name\t\tthe name of the alias to create.\n" +
                    "\talias command\t\tthe command name to alias to.\n" +
                    "\talias arg1\t\tthe first argument to the command to be aliased.";
                return helpString;
            }
            // ...delete command...
            else if (args[0].ToLower() == "-delete")
            {
                if (args.Length <= 1)
                    return "You must specify what alias you wish to delete";
                m_CommandAliases.Remove(args[1].ToLower());
            }
            // ...delete all command...
            else if (args[0].ToLower() == "-deleteall")
            {
                m_CommandAliases.Clear();
            }
            // ...an actual alias:
            else
            {
                string theCommand = args[0].ToLower();

                if (args.Length < 2)
                {
                    return "Incorrect # of arguments to alias command";
                }

                // nice try a**hole:
                if (m_CommandAliases.ContainsKey(theCommand))
                {
                    return "'" + args[0] + "' already aliased";
                }

                // nice try a**hole - the sequel:
                if (m_CommandMaps.ContainsKey(theCommand)) // note don't need to lock here, already locked by ExecuteCommand (the caller)
                {
                    return "Alias name '" + args[0] + "' is already a command";
                }

                string[] newArgs = new string[args.Length - 1];
                Array.Copy(args, 1, newArgs, 0, newArgs.Length);


                m_CommandAliases.Add(theCommand, newArgs);
            }

            return "";
        }


        private string CommandListen(string[] args)
        {
            ListenForRemoteConnections();
            return "";
        }

        private string CommandRemoteEcho(string[] args)
        {
            if (args.Length == 1 && args[0] == "-help")
            {
                string helpString = "Echos all remote commands to the local console.\n" +
                    "remoteecho [true | false]";
                return helpString;
            }

            if (args.Length == 0)
            {
                return m_RemoteEcho ? "true" : "false";
            }

            if (args.Length == 1)
            {
                if (args[0] == "true")
                {
                    m_RemoteEcho = true;
                }
                else
                {
                    m_RemoteEcho = false;
                }
            }
            return "";
        }

        #endregion
    }

    // PURPOSE:
    //  Attaches the console to a widget tree, provides the commands for manipulating that tree
    public class BankManagerConsoleSupport
    {
        public BankManagerConsoleSupport(ConsoleEngine engine, ragCore.IBankManager bankMgr)
        {
            m_BankManager = bankMgr;

            engine.AddCommand("widget", CommandWidget);
            engine.AddCommand("dir", CommandDir);
            engine.AddCommand("widget_id", AcquireLocalWidgetIDCommand);
            engine.AddCommand("widget_exists", WidgetExistCommand);
            engine.AddCommand("rc_ping", PingCommand);
        }

        public string CommandWidget(string[] args)
        {
            if (args.Length == 0)
            {
                return "no widget specified";
            }

            if (args[0] == "-help" || args.Length < 1)
            {
                string helpString = "Command that allows you to set widget values.\n\n" +
                    "WIDGET [-help] [widget name] [widget value1] ... [widget valueN]\n\n" +
                    "\t-help\t\t\tshows this message.  Put -help after the widget name if you wish information specific to that widget.\n" +
                    "\twidget name\t\t\tthe full path name to the widget to change or get help information.\n" +
                    "\twidget value1\t\tthe first value to pass to the widget (value is widget dependent).\n" +
                    "\twidget valueN\t\tthe Nth value to pass to the widget (value is widget dependent)\n";
                return helpString;
            }

            List<ragCore.Widget> list = m_BankManager.FindWidgetsFromPath(args[0]);

            // none found...
            if (list == null || list.Count == 0)
            {
                return "Couldn't find widget '" + args[0] + "'";
            }
            // ...more than one found...
            else if (list.Count > 1)
            {
                string returnStr = "More than one widget with the name '" + args[0] + "':\n";
                foreach (ragCore.Widget aWidget in list)
                {
                    returnStr += "\t" + aWidget.Title + "\n";
                }
                return returnStr;
            }

            // only one found!
            ragCore.Widget theWidget = list[0];
            string[] argsWithoutWidgetName = new string[args.Length - 1];
            Array.Copy(args, 1, argsWithoutWidgetName, 0, args.Length - 1);
            return theWidget.ProcessCommand(argsWithoutWidgetName);
        }

        /// <summary>
        /// The widget_id command allows user to get the ID of a particular widget.  
        /// It will return the id of the widget as a string, in this format: "id=123" (without the quotes)
        /// Returns the ID that the widget has.
        /// If more or less than one widget is found on the path, an error string will be returned instead.  
        /// </summary>
        /// <param name="args">Should contain a single string, the full path to the widget.</param>
        /// <returns></returns>
        private string AcquireLocalWidgetIDCommand(string[] args)
        {
            if (args.Length == 0)
            {
                return "no widget specified";
            }

            if (args[0] == "-help" || args.Length < 1)
            {
                string helpString = "Command that allows you to get widget IDs from a path.\n\n" +
                    "WIDGET_ID [-help] [widget name]\n\n" +
                    "\t-help\t\t\tshows this message. \n" +
                    "\twidget name\t\t\tthe full path name to the widget to change or get help information.\n";
                return helpString;
            }

            if (m_BankManager == null)
            {
                return "Bank Manager is not loaded.  RAG is not connected to the game.";
            }

            List<ragCore.Widget> list = m_BankManager.FindWidgetsFromPath(args[0]);

            // none found...
            if (list == null || list.Count == 0)
            {
                return "Couldn't find widget '" + args[0] + "'";
            }
            // ...more than one found...
            else if (list.Count > 1)
            {
                string returnStr = "More than one widget with the name '" + args[0] + "':\n";
                foreach (ragCore.Widget aWidget in list)
                {
                    returnStr += "\t" + aWidget.Title + "\n";
                }
                return returnStr;
            }

            // only one found!
            ragCore.Widget theWidget = list[0];
            return string.Format("id={0}", theWidget.Pipe.LocalToRemote(theWidget));
        }

        private bool WidgetExist(string widgetName, out ragCore.Widget theWidget)
        {
            theWidget = null;
            if (m_BankManager == null)
            {
                return false;
            }

            int remote_id = 0;
            if (int.TryParse(widgetName, out remote_id))
            {

            }
            else
            {
                List<ragCore.Widget> list = m_BankManager.FindWidgetsFromPath(widgetName);

                if (list == null || list.Count == 0)
                {
                    return false;
                }
                else if (list.Count > 1)
                {
                    //There are multiple widgets with this particular path.  
                    //Ambiguous -- assume that the widget does not exist.
                    return false;
                }
            }

            return true;
        }

        private string WidgetExistCommand(string[] args)
        {
            if (args[0] == "-help" || args.Length < 1)
            {
                string helpString = "Command that allows you to set widget values.\n\n" +
                    "WIDGET [-help] [widget name] [widget value1] ... [widget valueN]\n\n" +
                    "\t-help\t\t\tshows this message.  Put -help after the widget name if you wish information specific to that widget.\n" +
                    "\twidget name\t\t\tthe full path name to the widget to change or get help information.\n";
                return helpString;
            }

            ragCore.Widget theWidget = null;
            return (WidgetExist(args[0], out theWidget) == true) ? "true" : "false";
        }

        //PURPOSE:  A general ping between the client of the ConsoleProxy to the ConsoleProxy.
        //Sends back a ping message.
        private string PingCommand(string[] args)
        {
            return "rc_ping";
        }

        public string CommandDir(string[] args)
        {
            if (args.Length < 1 || args[0] == "-help")
            {
                string helpString = "Command that lists all of the widgets in part of the tree\n\n" +
                    "DIR [-help] [-t] [-i] [pathName]\n\n" +
                    "\t-help\t\t\tshows this message.\n" +
                    "\t-t\t\t\tPrints the type of each widget.\n" +
                    "\t-i\t\t\tPrints the ID of each widget.\n" +
                    "\t-p\t\t\tPrints the full widget path.\n";
                return helpString;
            }

            bool printTypes = false;
            bool printIds = false;
            bool fullPath = false;
            string queryString = "";

            foreach (string arg in args)
            {
                if (arg == "-t")
                {
                    printTypes = true;
                }
                else if (arg == "-i")
                {
                    printIds = true;
                }
                else if (arg == "-p")
                {
                    fullPath = true;
                }
                else
                {
                    queryString = arg;
                }
            }

            if (!queryString.EndsWith("/"))
            {
                queryString = queryString + "/";
            }

            List<ragCore.Widget> list = m_BankManager.FindWidgetsFromPath(queryString);
            ASCIIEncoding encoder = new ASCIIEncoding();

            string resultString = "";
            foreach (ragCore.Widget w in list)
            {
                string widgetString = "";
                if (fullPath)
                {
                    widgetString = w.Path;
                }
                else
                {
                    widgetString = w.Title;
                }

                if (printTypes)
                {
                    int guid = w.GetWidgetTypeGUID();
                    byte[] guidBytes = new byte[4];
                    guidBytes[0] = (byte)(guid & 0xFF);
                    guidBytes[1] = (byte)((guid >> 8) & 0xFF);
                    guidBytes[2] = (byte)((guid >> 16) & 0xFF);
                    guidBytes[3] = (byte)((guid >> 24) & 0xFF);

                    widgetString += "\t" + encoder.GetString(guidBytes);
                }
                if (printIds)
                {
                    widgetString += "\t" + "0";
                }
                resultString += widgetString + "\n";
            }

            return resultString;

        }

        private ragCore.IBankManager m_BankManager;
    }

    public class KeyboardConsoleSupport
    {
        public KeyboardConsoleSupport(ConsoleEngine engine, ConsoleEngine.ProcessResultCallback onResult)
        {
            m_Engine = engine;

            m_Engine.AddCommand("bind", CommandBind);

            m_OnResult = onResult;
        }

        ConsoleEngine m_Engine;
        ConsoleEngine.ProcessResultCallback m_OnResult;

        private Dictionary<string, string[]> m_KeyProcessHash = new Dictionary<string, string[]>();

        public bool ProcessKeyString(string keyString)
        {
            string[] theCommandArray = null;

            if (m_KeyProcessHash.TryGetValue(keyString, out theCommandArray) && theCommandArray != null)
            {
                bool ignore = false;
                m_Engine.ExecuteCommand(theCommandArray, ref ignore, m_OnResult);
            }

            return true;
        }

        public bool ProcessKeyEvent(System.Windows.Forms.KeyEventArgs e)
        {
            string keyString = "";

            // check modifiers:
            if (e.Control)
                keyString += "control+";
            if (e.Alt)
                keyString += "alt+";
            if (e.Shift)
                keyString += "shift+";

            keyString += e.KeyCode.ToString().ToLower();

            return ProcessKeyString(keyString);
        }

        public string CommandBind(string[] args)
        {
            if (args.Length < 1)
            {
                string binds = "keys bound:\n";
                binds += ConsoleEngine.GetArgArray(m_KeyProcessHash.GetEnumerator(), true);
                return binds;
            }

            if (args[0] == "-help")
            {
                string helpString = "Allows you to bind a key to a console command.\n\n" +
                    "BIND [-help] [keyboard keys] [command name and arguments]\n\n" +
                    "\t-help\t\t\tshows this message.\n" +
                    "\tkeyboard keys\t\t\tthe keyboard key and its modifiers that will bind to the command.\n" +
                    "\tcommand name and arguments\t\tthe command name, and its arguments that the keyboard key will bind to.\n";
                return helpString;
            }
            // delete it:
            else if (args[0] == "-delete")
            {
                if (args.Length <= 1)
                    return "You must specify what binding you wish to delete";
                m_KeyProcessHash.Remove(args[1].ToLower());
            }
            else if (args[0] == "-deleteall")
            {
                m_KeyProcessHash.Clear();
            }
            else
            {
                string[] keys = args[0].Split(new char[] { '+' });
                bool keyControl = false;
                bool keyAlt = false;
                bool keyShift = false;
                string theKey = null;

                if (keys != null)
                {
                    foreach (string key in keys)
                    {
                        switch (key.ToLower())
                        {
                            case "cntrl":
                            case "control":
                            case "controlkey":
                                keyControl = true;
                                break;

                            case "alt":
                            case "altkey":
                                keyAlt = true;
                                break;

                            case "shift":
                            case "shiftkey":
                                keyShift = true;
                                break;

                            default:
                                {
                                    if (theKey != null)
                                        return "Couldn't parse key combo string '" + args[0] + "'";
                                    else
                                    {
                                        theKey = ConvertKey(key);
                                    }
                                }
                                break;
                        }
                    }

                    if (theKey == null)
                    {
                        return "Couldn't find key in string '" + args[0] + "'";
                    }
                }
                else
                    theKey = ConvertKey(args[0]);

                // parse the key:
                try
                {
                    Enum.Parse(typeof(System.Windows.Forms.Keys), theKey, false);
                }
                catch (System.Exception)
                {
                    return "Couldn't parse keyboard key string '" + args[0].ToLower() + "'";
                }

                // copy command:
                string[] argsWithoutKeyName = new string[args.Length - 1];
                Array.Copy(args, 1, argsWithoutKeyName, 0, args.Length - 1);

                string hashKey = (keyControl ? "control+" : "") + (keyAlt ? "alt+" : "") + (keyShift ? "shift+" : "") + theKey.ToLower();

                // make sure it's not already bound:
                if (m_KeyProcessHash.ContainsKey(hashKey))
                {
                    string result = "WARNING: The current binding to the key combination '" + args[0] + "' will be lost.";
                    m_KeyProcessHash[hashKey] = argsWithoutKeyName;
                    return result;
                }
                else
                    m_KeyProcessHash.Add(hashKey, argsWithoutKeyName);
            }

            return "";
        }


        private string ConvertKey(string theKey)
        {
            string result = "";
            switch (theKey)
            {
                case "1":
                    result = "D1";
                    break;
                case "2":
                    result = "D2";
                    break;
                case "3":
                    result = "D3";
                    break;
                case "4":
                    result = "D4";
                    break;
                case "5":
                    result = "D5";
                    break;
                case "6":
                    result = "D6";
                    break;
                case "7":
                    result = "D7";
                    break;
                case "8":
                    result = "D8";
                    break;
                case "9":
                    result = "D9";
                    break;
                case "0":
                    result = "D0";
                    break;
                default:
                    result = theKey;
                    break;
            }
            return result;
        }
    }
}
