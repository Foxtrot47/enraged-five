using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;

namespace test
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class Form1 : System.Windows.Forms.Form
	{
		private ragCurveEditor.CurveEditor CurveEditor1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public Form1()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.CurveEditor1 = new ragCurveEditor.CurveEditor();
			this.SuspendLayout();
			// 
			// CurveEditor1
			// 
			this.CurveEditor1.AutoScroll = true;
			this.CurveEditor1.BackColor = System.Drawing.Color.White;
			this.CurveEditor1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.CurveEditor1.Location = new System.Drawing.Point(0, 0);
			this.CurveEditor1.Name = "CurveEditor1";
			this.CurveEditor1.Size = new System.Drawing.Size(712, 197);
			this.CurveEditor1.TabIndex = 0;
			this.CurveEditor1.Load += new System.EventHandler(this.CurveEditor1_Load);
			// 
			// Form1
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.AutoScroll = true;
			this.ClientSize = new System.Drawing.Size(712, 197);
			this.Controls.Add(this.CurveEditor1);
			this.Name = "Form1";
			this.Text = "Form1";
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new Form1());
		}

		private void CurveEditor1_Load(object sender, System.EventArgs e)
		{
		
		}
	}
}
