using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;

namespace ragCurveEditor
{
	/// <summary>
	/// Summary description for CurveEditor.
	/// </summary>
	public class CurveEditor : System.Windows.Forms.UserControl
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public CurveEditor()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			// TODO: Add any initialization after the InitComponent call
			SetStyle(ControlStyles.DoubleBuffer, true);
			SetStyle(ControlStyles.AllPaintingInWmPaint, true);
			SetStyle(ControlStyles.UserPaint, true);
			SetStyle(ControlStyles.SupportsTransparentBackColor, true);
			SetStyle(ControlStyles.ContainerControl, true);

			m_PointList.Add(new Point(this.Left,0));
			m_PointList.Add(new Point(100,100));
			m_PointList.Add(new Point(this.Right,50));

	
			for (int i=0;i<m_PointList.Count;i++)
			{
				Point point=(Point)m_PointList[i];
				Peg peg=new Peg(i,point);
                m_Pegs.Add(peg);
                this.Controls.Add(peg);

                peg.UpdatePoint=new Peg.UpdatePointEH(UpdatePoint);
				peg.DeletePeg=new Peg.DeletePegEH(DeletePeg);
			}

			CalculateClamps();
		}

		void CalculateClamps()
		{
			for (int i=0;i<m_PointList.Count;i++)
			{
				Peg peg=m_Pegs[i] as Peg;
				peg.Index=i;
				if (i==0 || i==m_PointList.Count-1)
				{
					peg.ClampPrev=peg.ClampNext=-1;
				}
				else
				{
					Peg prevPeg=m_Pegs[i-1] as Peg;
					peg.ClampPrev=prevPeg.Left+prevPeg.Size.Width;
					Peg nextPeg=m_Pegs[i+1] as Peg;
					peg.ClampNext=nextPeg.Left-prevPeg.Size.Width;
				}
			}
		}

		void UpdatePoint(int index,int x,int y)
		{
			m_PointList[index]=new Point(x,y);
			if (index!=0 && index!=m_PointList.Count-1)
			{
				Peg pegPrev=m_Pegs[index-1] as Peg;
				Peg pegNext=m_Pegs[index+1] as Peg;
			}
			CalculateClamps();

			this.Invalidate(true);
		}

		void DeletePeg(Peg peg)
		{
			int index=m_Pegs.IndexOf(peg);
            if (index!=0 && index!=m_PointList.Count-1)
			{
				m_Pegs.Remove(peg);
				this.Controls.Remove(peg);
				m_PointList.RemoveAt(index);

				CalculateClamps();

				this.Invalidate(true);
			}
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
                m_CaptionFont.Dispose();
				if( components != null )
					components.Dispose();
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			// 
			// CurveEditor
			// 
			this.BackColor = System.Drawing.SystemColors.ControlLightLight;
			this.Name = "CurveEditor";
			this.Size = new System.Drawing.Size(520, 168);
			this.Resize += new System.EventHandler(this.CurveEditor_Resize);
			this.Paint += new System.Windows.Forms.PaintEventHandler(this.CurveEditor_Paint);
			this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.CurveEditor_MouseDown);

		}
		#endregion

		#region Variables
		ArrayList m_PointList=new ArrayList();
		ArrayList m_Pegs=new ArrayList();
		float m_ScaleX=1.0f;
		float m_ScaleY=1.0f;
		float m_OffsetX=0.0f;
		float m_OffsetY=0.0f;
		float m_GridSeparationX=10.0f;
		float m_GridSeparationY=10.0f;
		bool  m_DrawLabelX=false;
		bool  m_DrawGridX=false;
		bool  m_DrawLabelY=false;
		bool  m_DrawGridY=false;
		#endregion
				   
		#region	Properties
		float GraphExtentX
		{
			get
			{
				if (m_PointList.Count>=1)
				{
					Point point=(Point)m_PointList[m_PointList.Count-1];
					return (((float)point.X*m_ScaleX)+ m_OffsetX);
				}
				else 
					return 0.0f;
			}
		}

		float GraphExtentY	
		{
			get
			{
				float maxY=m_OffsetY;
				foreach (Point point in m_PointList)
				{
					if (((float)point.Y)>maxY)
						maxY=(float)point.Y;
				}

				return maxY*m_ScaleY;
			}
		}
		#endregion
					 

		private Font m_CaptionFont = new Font("Microsoft Sans Serif", 7f, FontStyle.Regular);

		private void DrawLabels(Graphics graphics)
		{
			float sizeMinY=m_OffsetY;
			float sizeMaxY=GraphExtentY;

			// figure out sizes of largest 
			Size sizeTextMin = graphics.MeasureString(sizeMinY.ToString(), m_CaptionFont).ToSize();
			Size sizeTextMax = graphics.MeasureString(sizeMaxY.ToString(), m_CaptionFont).ToSize();

			int maxSizeX=Math.Max(sizeTextMin.Width,sizeTextMax.Width);
			float sizeMaxX=GraphExtentX;

			// draw the Y labels and grids:
			Pen pen=new Pen(Color.LightGray,1);
			if (m_DrawGridY || m_DrawLabelY)
				for (float initialY=sizeMinY;initialY<=sizeMaxY;initialY+=m_GridSeparationY)
				{	
					if (m_DrawGridY)
						graphics.DrawLine(pen,new PointF((float)maxSizeX,initialY-m_OffsetY),new PointF(sizeMaxX+maxSizeX,initialY-m_OffsetY));

					if (m_DrawLabelY)
						graphics.DrawString(initialY.ToString(), m_CaptionFont, new SolidBrush(Color.Black), 
							(float)0, (initialY-m_OffsetY)-0.5f*sizeTextMin.Height);
				
				}

			// draw the X labels and grids:
			if (m_DrawGridX || m_DrawLabelX)
			{
				System.Drawing.StringFormat drawFormat = new System.Drawing.StringFormat();
				drawFormat.FormatFlags = StringFormatFlags.DirectionVertical;
				for (float initialX=m_OffsetX;initialX<=sizeMaxX;initialX+=m_GridSeparationX)
				{	
					if (m_DrawGridX)
						graphics.DrawLine(pen,new PointF((float)initialX+maxSizeX,sizeMinY),new PointF((float)initialX+maxSizeX,sizeMaxY));

					if (m_DrawLabelX)
						graphics.DrawString(initialX.ToString(), m_CaptionFont, new SolidBrush(Color.Black), 
							(float)initialX+maxSizeX, sizeMaxY,drawFormat);
				}

			}
		}


		private void CurveEditor_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			e.Graphics.TranslateTransform(AutoScrollPosition.X,AutoScrollPosition.Y);  
			DrawLabels(e.Graphics);
            if (m_PointList.Count > 1)
				e.Graphics.DrawLines(new Pen(Color.Black,1),m_PointList.ToArray(m_PointList[0].GetType()) as Point[]);
		}

		private void CurveEditor_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
		{
            // only allow left clicks to add a button
            if (e.Button == MouseButtons.Left && e.Clicks == 2)
            {
                Point MousePos=this.PointToClient(new Point(MousePosition.X,MousePosition.Y));

                int i=0;
                for (;i<m_PointList.Count;i++)
                {
                    Point point=(Point)m_PointList[i];
                    if (MousePos.X<point.X)
                    {
                        point=new Point(MousePos.X,MousePos.Y);
                        m_PointList.Insert(i,point);
                        Peg peg=new Peg(i,point);
                        m_Pegs.Insert(i,peg);
                        this.Controls.Add(peg);	
                        this.Controls.SetChildIndex(peg,i);

                        peg.UpdatePoint=new Peg.UpdatePointEH(UpdatePoint);
                        peg.DeletePeg=new Peg.DeletePegEH(DeletePeg);

                        break;
                    }
                }

                // change indicies:
                for (;i<m_PointList.Count;i++)
                {
                    Peg peg=m_Pegs[i] as Peg;
                    peg.Index=i;
                }

                CalculateClamps();

                this.Invalidate(true);
            }
        }

		private void CurveEditor_Resize(object sender, System.EventArgs e)
		{
			this.Invalidate(true);
		}
	}
}
