using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;

namespace ragCurveEditor
{
	/// <summary>
	/// Summary description for Peg.
	/// </summary>
	public class Peg : System.Windows.Forms.UserControl
	{
		private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ContextMenu contextMenu1;
        private System.Windows.Forms.MenuItem pointMenu;
        private System.Windows.Forms.MenuItem pointDelete;
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		private Peg()
		{
            // This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			// TODO: Add any initialization after the InitializeComponent call

		}

		public Peg(int index,Point p) : this() // make sure InitializeComponent() gets called.
		{
            this.Left=p.X-4;
			this.Top=p.Y-4;
			m_Index=index;
		}
		

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.button1 = new System.Windows.Forms.Button();
			this.contextMenu1 = new System.Windows.Forms.ContextMenu();
			this.pointMenu = new System.Windows.Forms.MenuItem();
			this.pointDelete = new System.Windows.Forms.MenuItem();
			this.SuspendLayout();
			// 
			// button1
			// 
			this.button1.Cursor = System.Windows.Forms.Cursors.Cross;
			this.button1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.button1.Location = new System.Drawing.Point(0, 0);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(8, 8);
			this.button1.TabIndex = 0;
			this.button1.Click += new System.EventHandler(this.button1_Click);
			this.button1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Peg_MouseUp);
			this.button1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.button1_KeyDown);
			this.button1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Peg_MouseMove);
			this.button1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Peg_MouseDown);
			// 
			// contextMenu1
			// 
			this.contextMenu1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																						 this.pointMenu});
			// 
			// pointMenu
			// 
			this.pointMenu.Index = 0;
			this.pointMenu.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					  this.pointDelete});
			this.pointMenu.Text = "Point Menu";
			// 
			// pointDelete
			// 
			this.pointDelete.Index = 0;
			this.pointDelete.Text = "Delete";
			this.pointDelete.Click += new System.EventHandler(this.pointDelete_Click);
			// 
			// Peg
			// 
			this.ContextMenu = this.contextMenu1;
			this.Controls.Add(this.button1);
			this.Name = "Peg";
			this.Size = new System.Drawing.Size(8, 8);
			this.ResumeLayout(false);

		}
		#endregion
	
		public delegate void UpdatePointEH(int index,int x,int y);
		public delegate void DeletePegEH(Peg peg);

		#region Variables
		bool m_MovingPoint;
		UpdatePointEH m_UpdatePointEH;
		DeletePegEH m_DeletePegEH;
		int m_Index;
		int m_ClampPrev;
		int m_ClampNext;
		#endregion

		#region Properties
		public UpdatePointEH UpdatePoint
		{
			set
			{
				m_UpdatePointEH=value;
			}
		}

		public DeletePegEH DeletePeg
		{
			set
			{
				m_DeletePegEH=value;
			}
		}

		public int Index
		{
			set
			{
				m_Index=value;
				
			}
		}

		public int ClampPrev
		{
			set
			{
				m_ClampPrev=value;
			}

			get
			{
				return m_ClampPrev;
			}
		}

		public int ClampNext
		{
			set
			{
				m_ClampNext=value;
			}

			get
			{
				return m_ClampNext;
			}
		}
		#endregion

		private void Peg_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			m_MovingPoint=true;
		}

		private void Peg_MouseMove(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			Point point=this.Parent.PointToClient(new Point(MousePosition.X,MousePosition.Y));

			if (m_MovingPoint==true)
			{
				if (m_ClampPrev>=0 && m_ClampNext>=0)
				{
					if (point.X<m_ClampPrev)
						this.Left=m_ClampPrev;
					else if (point.X>m_ClampNext)
						this.Left=m_ClampNext;
					else 
						this.Left=point.X;
				}
				this.Top=point.Y;
				m_UpdatePointEH(m_Index,this.Left,this.Top);
			}
		}

		private void Peg_MouseUp(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			m_MovingPoint=false;
		}

		private void button1_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
		{
            if (e.KeyCode == Keys.Delete)
            {
                if (m_DeletePegEH!=null)
                    m_DeletePegEH(this);
            }
		}

        private void pointDelete_Click(object sender, System.EventArgs e)
        {
            if (m_DeletePegEH!=null)
                m_DeletePegEH(this);
        }

		private void button1_Click(object sender, System.EventArgs e)
		{
		
		}
	}
}
