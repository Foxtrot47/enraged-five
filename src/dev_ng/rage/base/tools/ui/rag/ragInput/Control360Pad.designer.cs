namespace ragInput
{
    partial class Control360Pad
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && (components != null) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.titleLabel = new System.Windows.Forms.Label();
            this.LeftTriggerButton = new System.Windows.Forms.CheckBox();
            this.LeftButtonButton = new System.Windows.Forms.CheckBox();
            this.RightTriggerButton = new System.Windows.Forms.CheckBox();
            this.RightButtonButton = new System.Windows.Forms.CheckBox();
            this.LeftAnalogStickButton = new System.Windows.Forms.CheckBox();
            this.LUpButton = new System.Windows.Forms.CheckBox();
            this.LLeftButton = new System.Windows.Forms.CheckBox();
            this.LRightButton = new System.Windows.Forms.CheckBox();
            this.LDownButton = new System.Windows.Forms.CheckBox();
            this.BackButton = new System.Windows.Forms.CheckBox();
            this.StartButton = new System.Windows.Forms.CheckBox();
            this.RightAnalogStickButton = new System.Windows.Forms.CheckBox();
            this.RUpButton = new System.Windows.Forms.CheckBox();
            this.RLeftButton = new System.Windows.Forms.CheckBox();
            this.RRightButton = new System.Windows.Forms.CheckBox();
            this.RDownButton = new System.Windows.Forms.CheckBox();
            this.LeftThumbButton = new System.Windows.Forms.CheckBox();
            this.RightThumbButton = new System.Windows.Forms.CheckBox();
            this.sendInputsCheckBox = new System.Windows.Forms.CheckBox();
            this.clearButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // titleLabel
            // 
            this.titleLabel.AutoSize = true;
            this.titleLabel.BackColor = System.Drawing.Color.Transparent;
            this.titleLabel.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)) );
            this.titleLabel.Location = new System.Drawing.Point( 140, 0 );
            this.titleLabel.Name = "titleLabel";
            this.titleLabel.Size = new System.Drawing.Size( 40, 13 );
            this.titleLabel.TabIndex = 0;
            this.titleLabel.Text = "Pad 0";
            this.titleLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // LeftTriggerButton
            // 
            this.LeftTriggerButton.Appearance = System.Windows.Forms.Appearance.Button;
            this.LeftTriggerButton.BackColor = System.Drawing.Color.Transparent;
            this.LeftTriggerButton.FlatAppearance.BorderSize = 0;
            this.LeftTriggerButton.FlatAppearance.CheckedBackColor = System.Drawing.Color.WhiteSmoke;
            this.LeftTriggerButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.WhiteSmoke;
            this.LeftTriggerButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.LeftTriggerButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LeftTriggerButton.ForeColor = System.Drawing.Color.Transparent;
            this.LeftTriggerButton.Location = new System.Drawing.Point( 46, 3 );
            this.LeftTriggerButton.Name = "LeftTriggerButton";
            this.LeftTriggerButton.Size = new System.Drawing.Size( 33, 50 );
            this.LeftTriggerButton.TabIndex = 1;
            this.LeftTriggerButton.Tag = "L2";
            this.LeftTriggerButton.UseVisualStyleBackColor = false;
            this.LeftTriggerButton.MouseDown += new System.Windows.Forms.MouseEventHandler( this.DigitalButton_MouseDown );
            this.LeftTriggerButton.KeyUp += new System.Windows.Forms.KeyEventHandler( this.Button_KeyUp );
            this.LeftTriggerButton.CheckedChanged += new System.EventHandler( this.DigitalButton_CheckedChanged );
            this.LeftTriggerButton.MouseUp += new System.Windows.Forms.MouseEventHandler( this.DigitalButton_MouseUp );
            this.LeftTriggerButton.KeyDown += new System.Windows.Forms.KeyEventHandler( this.Button_KeyDown );
            // 
            // LeftButtonButton
            // 
            this.LeftButtonButton.Appearance = System.Windows.Forms.Appearance.Button;
            this.LeftButtonButton.BackColor = System.Drawing.Color.Transparent;
            this.LeftButtonButton.FlatAppearance.BorderSize = 0;
            this.LeftButtonButton.FlatAppearance.CheckedBackColor = System.Drawing.Color.WhiteSmoke;
            this.LeftButtonButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.WhiteSmoke;
            this.LeftButtonButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.LeftButtonButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LeftButtonButton.ForeColor = System.Drawing.Color.Transparent;
            this.LeftButtonButton.Location = new System.Drawing.Point( 18, 56 );
            this.LeftButtonButton.Name = "LeftButtonButton";
            this.LeftButtonButton.Size = new System.Drawing.Size( 67, 21 );
            this.LeftButtonButton.TabIndex = 3;
            this.LeftButtonButton.Tag = "L1";
            this.LeftButtonButton.UseVisualStyleBackColor = false;
            this.LeftButtonButton.MouseDown += new System.Windows.Forms.MouseEventHandler( this.DigitalButton_MouseDown );
            this.LeftButtonButton.KeyUp += new System.Windows.Forms.KeyEventHandler( this.Button_KeyUp );
            this.LeftButtonButton.CheckedChanged += new System.EventHandler( this.DigitalButton_CheckedChanged );
            this.LeftButtonButton.MouseUp += new System.Windows.Forms.MouseEventHandler( this.DigitalButton_MouseUp );
            this.LeftButtonButton.KeyDown += new System.Windows.Forms.KeyEventHandler( this.Button_KeyDown );
            // 
            // RightTriggerButton
            // 
            this.RightTriggerButton.Appearance = System.Windows.Forms.Appearance.Button;
            this.RightTriggerButton.BackColor = System.Drawing.Color.Transparent;
            this.RightTriggerButton.FlatAppearance.BorderSize = 0;
            this.RightTriggerButton.FlatAppearance.CheckedBackColor = System.Drawing.Color.WhiteSmoke;
            this.RightTriggerButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.WhiteSmoke;
            this.RightTriggerButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.RightTriggerButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.RightTriggerButton.ForeColor = System.Drawing.Color.Transparent;
            this.RightTriggerButton.Location = new System.Drawing.Point( 242, 3 );
            this.RightTriggerButton.Name = "RightTriggerButton";
            this.RightTriggerButton.Size = new System.Drawing.Size( 33, 50 );
            this.RightTriggerButton.TabIndex = 2;
            this.RightTriggerButton.Tag = "R2";
            this.RightTriggerButton.UseVisualStyleBackColor = false;
            this.RightTriggerButton.MouseDown += new System.Windows.Forms.MouseEventHandler( this.DigitalButton_MouseDown );
            this.RightTriggerButton.KeyUp += new System.Windows.Forms.KeyEventHandler( this.Button_KeyUp );
            this.RightTriggerButton.CheckedChanged += new System.EventHandler( this.DigitalButton_CheckedChanged );
            this.RightTriggerButton.MouseUp += new System.Windows.Forms.MouseEventHandler( this.DigitalButton_MouseUp );
            this.RightTriggerButton.KeyDown += new System.Windows.Forms.KeyEventHandler( this.Button_KeyDown );
            // 
            // RightButtonButton
            // 
            this.RightButtonButton.Appearance = System.Windows.Forms.Appearance.Button;
            this.RightButtonButton.BackColor = System.Drawing.Color.Transparent;
            this.RightButtonButton.FlatAppearance.BorderSize = 0;
            this.RightButtonButton.FlatAppearance.CheckedBackColor = System.Drawing.Color.WhiteSmoke;
            this.RightButtonButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.WhiteSmoke;
            this.RightButtonButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.RightButtonButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.RightButtonButton.ForeColor = System.Drawing.Color.Transparent;
            this.RightButtonButton.Location = new System.Drawing.Point( 235, 55 );
            this.RightButtonButton.Name = "RightButtonButton";
            this.RightButtonButton.Size = new System.Drawing.Size( 67, 21 );
            this.RightButtonButton.TabIndex = 4;
            this.RightButtonButton.Tag = "R1";
            this.RightButtonButton.UseVisualStyleBackColor = false;
            this.RightButtonButton.MouseDown += new System.Windows.Forms.MouseEventHandler( this.DigitalButton_MouseDown );
            this.RightButtonButton.KeyUp += new System.Windows.Forms.KeyEventHandler( this.Button_KeyUp );
            this.RightButtonButton.CheckedChanged += new System.EventHandler( this.DigitalButton_CheckedChanged );
            this.RightButtonButton.MouseUp += new System.Windows.Forms.MouseEventHandler( this.DigitalButton_MouseUp );
            this.RightButtonButton.KeyDown += new System.Windows.Forms.KeyEventHandler( this.Button_KeyDown );
            // 
            // LeftAnalogStickButton
            // 
            this.LeftAnalogStickButton.Appearance = System.Windows.Forms.Appearance.Button;
            this.LeftAnalogStickButton.BackColor = System.Drawing.Color.Transparent;
            this.LeftAnalogStickButton.FlatAppearance.BorderSize = 0;
            this.LeftAnalogStickButton.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.LeftAnalogStickButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.LeftAnalogStickButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.LeftAnalogStickButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LeftAnalogStickButton.ForeColor = System.Drawing.Color.Transparent;
            this.LeftAnalogStickButton.Location = new System.Drawing.Point( 39, 196 );
            this.LeftAnalogStickButton.Name = "LeftAnalogStickButton";
            this.LeftAnalogStickButton.Size = new System.Drawing.Size( 47, 47 );
            this.LeftAnalogStickButton.TabIndex = 5;
            this.LeftAnalogStickButton.Tag = "LEFT_STICK";
            this.LeftAnalogStickButton.UseVisualStyleBackColor = false;
            this.LeftAnalogStickButton.MouseDown += new System.Windows.Forms.MouseEventHandler( this.AnalogStick_MouseDown );
            this.LeftAnalogStickButton.MouseMove += new System.Windows.Forms.MouseEventHandler( this.AnalogStick_MouseMove );
            this.LeftAnalogStickButton.KeyUp += new System.Windows.Forms.KeyEventHandler( this.Button_KeyUp );
            this.LeftAnalogStickButton.Paint += new System.Windows.Forms.PaintEventHandler( this.AnalogStick_Paint );
            this.LeftAnalogStickButton.MouseUp += new System.Windows.Forms.MouseEventHandler( this.AnalogStick_MouseUp );
            this.LeftAnalogStickButton.KeyDown += new System.Windows.Forms.KeyEventHandler( this.Button_KeyDown );
            // 
            // LUpButton
            // 
            this.LUpButton.Appearance = System.Windows.Forms.Appearance.Button;
            this.LUpButton.BackColor = System.Drawing.Color.Transparent;
            this.LUpButton.FlatAppearance.BorderSize = 0;
            this.LUpButton.FlatAppearance.CheckedBackColor = System.Drawing.Color.Gray;
            this.LUpButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Gray;
            this.LUpButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.LUpButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LUpButton.ForeColor = System.Drawing.Color.Transparent;
            this.LUpButton.Location = new System.Drawing.Point( 96, 241 );
            this.LUpButton.Name = "LUpButton";
            this.LUpButton.Size = new System.Drawing.Size( 25, 25 );
            this.LUpButton.TabIndex = 14;
            this.LUpButton.Tag = "LUP";
            this.LUpButton.UseVisualStyleBackColor = false;
            this.LUpButton.MouseDown += new System.Windows.Forms.MouseEventHandler( this.DigitalButton_MouseDown );
            this.LUpButton.KeyUp += new System.Windows.Forms.KeyEventHandler( this.Button_KeyUp );
            this.LUpButton.CheckedChanged += new System.EventHandler( this.DigitalButton_CheckedChanged );
            this.LUpButton.MouseUp += new System.Windows.Forms.MouseEventHandler( this.DigitalButton_MouseUp );
            this.LUpButton.KeyDown += new System.Windows.Forms.KeyEventHandler( this.Button_KeyDown );
            // 
            // LLeftButton
            // 
            this.LLeftButton.Appearance = System.Windows.Forms.Appearance.Button;
            this.LLeftButton.BackColor = System.Drawing.Color.Transparent;
            this.LLeftButton.FlatAppearance.BorderSize = 0;
            this.LLeftButton.FlatAppearance.CheckedBackColor = System.Drawing.Color.Gray;
            this.LLeftButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Gray;
            this.LLeftButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.LLeftButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LLeftButton.ForeColor = System.Drawing.Color.Transparent;
            this.LLeftButton.Location = new System.Drawing.Point( 72, 265 );
            this.LLeftButton.Name = "LLeftButton";
            this.LLeftButton.Size = new System.Drawing.Size( 25, 25 );
            this.LLeftButton.TabIndex = 13;
            this.LLeftButton.Tag = "LLEFT";
            this.LLeftButton.UseVisualStyleBackColor = false;
            this.LLeftButton.MouseDown += new System.Windows.Forms.MouseEventHandler( this.DigitalButton_MouseDown );
            this.LLeftButton.KeyUp += new System.Windows.Forms.KeyEventHandler( this.Button_KeyUp );
            this.LLeftButton.CheckedChanged += new System.EventHandler( this.DigitalButton_CheckedChanged );
            this.LLeftButton.MouseUp += new System.Windows.Forms.MouseEventHandler( this.DigitalButton_MouseUp );
            this.LLeftButton.KeyDown += new System.Windows.Forms.KeyEventHandler( this.Button_KeyDown );
            // 
            // LRightButton
            // 
            this.LRightButton.Appearance = System.Windows.Forms.Appearance.Button;
            this.LRightButton.BackColor = System.Drawing.Color.Transparent;
            this.LRightButton.FlatAppearance.BorderSize = 0;
            this.LRightButton.FlatAppearance.CheckedBackColor = System.Drawing.Color.Gray;
            this.LRightButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Gray;
            this.LRightButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.LRightButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LRightButton.ForeColor = System.Drawing.Color.Transparent;
            this.LRightButton.Location = new System.Drawing.Point( 120, 265 );
            this.LRightButton.Name = "LRightButton";
            this.LRightButton.Size = new System.Drawing.Size( 25, 25 );
            this.LRightButton.TabIndex = 15;
            this.LRightButton.Tag = "LRIGHT";
            this.LRightButton.UseVisualStyleBackColor = false;
            this.LRightButton.MouseDown += new System.Windows.Forms.MouseEventHandler( this.DigitalButton_MouseDown );
            this.LRightButton.KeyUp += new System.Windows.Forms.KeyEventHandler( this.Button_KeyUp );
            this.LRightButton.CheckedChanged += new System.EventHandler( this.DigitalButton_CheckedChanged );
            this.LRightButton.MouseUp += new System.Windows.Forms.MouseEventHandler( this.DigitalButton_MouseUp );
            this.LRightButton.KeyDown += new System.Windows.Forms.KeyEventHandler( this.Button_KeyDown );
            // 
            // LDownButton
            // 
            this.LDownButton.Appearance = System.Windows.Forms.Appearance.Button;
            this.LDownButton.BackColor = System.Drawing.Color.Transparent;
            this.LDownButton.FlatAppearance.BorderSize = 0;
            this.LDownButton.FlatAppearance.CheckedBackColor = System.Drawing.Color.Gray;
            this.LDownButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Gray;
            this.LDownButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.LDownButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LDownButton.ForeColor = System.Drawing.Color.Transparent;
            this.LDownButton.Location = new System.Drawing.Point( 96, 290 );
            this.LDownButton.Name = "LDownButton";
            this.LDownButton.Size = new System.Drawing.Size( 25, 25 );
            this.LDownButton.TabIndex = 16;
            this.LDownButton.Tag = "LDOWN";
            this.LDownButton.UseVisualStyleBackColor = false;
            this.LDownButton.MouseDown += new System.Windows.Forms.MouseEventHandler( this.DigitalButton_MouseDown );
            this.LDownButton.KeyUp += new System.Windows.Forms.KeyEventHandler( this.Button_KeyUp );
            this.LDownButton.CheckedChanged += new System.EventHandler( this.DigitalButton_CheckedChanged );
            this.LDownButton.MouseUp += new System.Windows.Forms.MouseEventHandler( this.DigitalButton_MouseUp );
            this.LDownButton.KeyDown += new System.Windows.Forms.KeyEventHandler( this.Button_KeyDown );
            // 
            // BackButton
            // 
            this.BackButton.Appearance = System.Windows.Forms.Appearance.Button;
            this.BackButton.BackColor = System.Drawing.Color.Transparent;
            this.BackButton.FlatAppearance.BorderSize = 0;
            this.BackButton.FlatAppearance.CheckedBackColor = System.Drawing.Color.WhiteSmoke;
            this.BackButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.WhiteSmoke;
            this.BackButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.BackButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BackButton.ForeColor = System.Drawing.Color.Transparent;
            this.BackButton.Location = new System.Drawing.Point( 112, 212 );
            this.BackButton.Name = "BackButton";
            this.BackButton.Size = new System.Drawing.Size( 24, 21 );
            this.BackButton.TabIndex = 7;
            this.BackButton.Tag = "SELECT";
            this.BackButton.UseVisualStyleBackColor = false;
            this.BackButton.MouseDown += new System.Windows.Forms.MouseEventHandler( this.DigitalButton_MouseDown );
            this.BackButton.KeyUp += new System.Windows.Forms.KeyEventHandler( this.Button_KeyUp );
            this.BackButton.CheckedChanged += new System.EventHandler( this.DigitalButton_CheckedChanged );
            this.BackButton.MouseUp += new System.Windows.Forms.MouseEventHandler( this.DigitalButton_MouseUp );
            this.BackButton.KeyDown += new System.Windows.Forms.KeyEventHandler( this.Button_KeyDown );
            // 
            // StartButton
            // 
            this.StartButton.Appearance = System.Windows.Forms.Appearance.Button;
            this.StartButton.BackColor = System.Drawing.Color.Transparent;
            this.StartButton.FlatAppearance.BorderSize = 0;
            this.StartButton.FlatAppearance.CheckedBackColor = System.Drawing.Color.WhiteSmoke;
            this.StartButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.WhiteSmoke;
            this.StartButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.StartButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.StartButton.ForeColor = System.Drawing.Color.Transparent;
            this.StartButton.Location = new System.Drawing.Point( 182, 212 );
            this.StartButton.Name = "StartButton";
            this.StartButton.Size = new System.Drawing.Size( 21, 22 );
            this.StartButton.TabIndex = 8;
            this.StartButton.Tag = "START";
            this.StartButton.UseVisualStyleBackColor = false;
            this.StartButton.MouseDown += new System.Windows.Forms.MouseEventHandler( this.DigitalButton_MouseDown );
            this.StartButton.KeyUp += new System.Windows.Forms.KeyEventHandler( this.Button_KeyUp );
            this.StartButton.CheckedChanged += new System.EventHandler( this.DigitalButton_CheckedChanged );
            this.StartButton.MouseUp += new System.Windows.Forms.MouseEventHandler( this.DigitalButton_MouseUp );
            this.StartButton.KeyDown += new System.Windows.Forms.KeyEventHandler( this.Button_KeyDown );
            // 
            // RightAnalogStickButton
            // 
            this.RightAnalogStickButton.Appearance = System.Windows.Forms.Appearance.Button;
            this.RightAnalogStickButton.BackColor = System.Drawing.Color.Transparent;
            this.RightAnalogStickButton.FlatAppearance.BorderSize = 0;
            this.RightAnalogStickButton.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.RightAnalogStickButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.RightAnalogStickButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.RightAnalogStickButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.RightAnalogStickButton.ForeColor = System.Drawing.Color.Transparent;
            this.RightAnalogStickButton.Location = new System.Drawing.Point( 180, 252 );
            this.RightAnalogStickButton.Name = "RightAnalogStickButton";
            this.RightAnalogStickButton.Size = new System.Drawing.Size( 47, 47 );
            this.RightAnalogStickButton.TabIndex = 17;
            this.RightAnalogStickButton.Tag = "RIGHT_STICK";
            this.RightAnalogStickButton.UseVisualStyleBackColor = false;
            this.RightAnalogStickButton.MouseDown += new System.Windows.Forms.MouseEventHandler( this.AnalogStick_MouseDown );
            this.RightAnalogStickButton.MouseMove += new System.Windows.Forms.MouseEventHandler( this.AnalogStick_MouseMove );
            this.RightAnalogStickButton.KeyUp += new System.Windows.Forms.KeyEventHandler( this.Button_KeyUp );
            this.RightAnalogStickButton.Paint += new System.Windows.Forms.PaintEventHandler( this.AnalogStick_Paint );
            this.RightAnalogStickButton.MouseUp += new System.Windows.Forms.MouseEventHandler( this.AnalogStick_MouseUp );
            this.RightAnalogStickButton.KeyDown += new System.Windows.Forms.KeyEventHandler( this.Button_KeyDown );
            // 
            // RUpButton
            // 
            this.RUpButton.Appearance = System.Windows.Forms.Appearance.Button;
            this.RUpButton.BackColor = System.Drawing.Color.Transparent;
            this.RUpButton.FlatAppearance.BorderSize = 0;
            this.RUpButton.FlatAppearance.CheckedBackColor = System.Drawing.Color.Yellow;
            this.RUpButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Yellow;
            this.RUpButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.RUpButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.RUpButton.ForeColor = System.Drawing.Color.Transparent;
            this.RUpButton.Location = new System.Drawing.Point( 238, 183 );
            this.RUpButton.Name = "RUpButton";
            this.RUpButton.Size = new System.Drawing.Size( 25, 25 );
            this.RUpButton.TabIndex = 10;
            this.RUpButton.Tag = "RUP";
            this.RUpButton.UseVisualStyleBackColor = false;
            this.RUpButton.MouseDown += new System.Windows.Forms.MouseEventHandler( this.DigitalButton_MouseDown );
            this.RUpButton.KeyUp += new System.Windows.Forms.KeyEventHandler( this.Button_KeyUp );
            this.RUpButton.CheckedChanged += new System.EventHandler( this.DigitalButton_CheckedChanged );
            this.RUpButton.MouseUp += new System.Windows.Forms.MouseEventHandler( this.DigitalButton_MouseUp );
            this.RUpButton.KeyDown += new System.Windows.Forms.KeyEventHandler( this.Button_KeyDown );
            // 
            // RLeftButton
            // 
            this.RLeftButton.Appearance = System.Windows.Forms.Appearance.Button;
            this.RLeftButton.BackColor = System.Drawing.Color.Transparent;
            this.RLeftButton.FlatAppearance.BorderSize = 0;
            this.RLeftButton.FlatAppearance.CheckedBackColor = System.Drawing.Color.Blue;
            this.RLeftButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Blue;
            this.RLeftButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.RLeftButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.RLeftButton.ForeColor = System.Drawing.Color.Transparent;
            this.RLeftButton.Location = new System.Drawing.Point( 214, 208 );
            this.RLeftButton.Name = "RLeftButton";
            this.RLeftButton.Size = new System.Drawing.Size( 25, 25 );
            this.RLeftButton.TabIndex = 9;
            this.RLeftButton.Tag = "RLEFT";
            this.RLeftButton.UseVisualStyleBackColor = false;
            this.RLeftButton.MouseDown += new System.Windows.Forms.MouseEventHandler( this.DigitalButton_MouseDown );
            this.RLeftButton.KeyUp += new System.Windows.Forms.KeyEventHandler( this.Button_KeyUp );
            this.RLeftButton.CheckedChanged += new System.EventHandler( this.DigitalButton_CheckedChanged );
            this.RLeftButton.MouseUp += new System.Windows.Forms.MouseEventHandler( this.DigitalButton_MouseUp );
            this.RLeftButton.KeyDown += new System.Windows.Forms.KeyEventHandler( this.Button_KeyDown );
            // 
            // RRightButton
            // 
            this.RRightButton.Appearance = System.Windows.Forms.Appearance.Button;
            this.RRightButton.BackColor = System.Drawing.Color.Transparent;
            this.RRightButton.FlatAppearance.BorderSize = 0;
            this.RRightButton.FlatAppearance.CheckedBackColor = System.Drawing.Color.Red;
            this.RRightButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Red;
            this.RRightButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.RRightButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.RRightButton.ForeColor = System.Drawing.Color.Transparent;
            this.RRightButton.Location = new System.Drawing.Point( 263, 208 );
            this.RRightButton.Name = "RRightButton";
            this.RRightButton.Size = new System.Drawing.Size( 25, 25 );
            this.RRightButton.TabIndex = 11;
            this.RRightButton.Tag = "RRIGHT";
            this.RRightButton.UseVisualStyleBackColor = false;
            this.RRightButton.MouseDown += new System.Windows.Forms.MouseEventHandler( this.DigitalButton_MouseDown );
            this.RRightButton.KeyUp += new System.Windows.Forms.KeyEventHandler( this.Button_KeyUp );
            this.RRightButton.CheckedChanged += new System.EventHandler( this.DigitalButton_CheckedChanged );
            this.RRightButton.MouseUp += new System.Windows.Forms.MouseEventHandler( this.DigitalButton_MouseUp );
            this.RRightButton.KeyDown += new System.Windows.Forms.KeyEventHandler( this.Button_KeyDown );
            // 
            // RDownButton
            // 
            this.RDownButton.Appearance = System.Windows.Forms.Appearance.Button;
            this.RDownButton.BackColor = System.Drawing.Color.Transparent;
            this.RDownButton.FlatAppearance.BorderSize = 0;
            this.RDownButton.FlatAppearance.CheckedBackColor = System.Drawing.Color.Green;
            this.RDownButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Green;
            this.RDownButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.RDownButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.RDownButton.ForeColor = System.Drawing.Color.Transparent;
            this.RDownButton.Location = new System.Drawing.Point( 239, 233 );
            this.RDownButton.Name = "RDownButton";
            this.RDownButton.Size = new System.Drawing.Size( 25, 25 );
            this.RDownButton.TabIndex = 12;
            this.RDownButton.Tag = "RDOWN";
            this.RDownButton.UseVisualStyleBackColor = false;
            this.RDownButton.MouseDown += new System.Windows.Forms.MouseEventHandler( this.DigitalButton_MouseDown );
            this.RDownButton.KeyUp += new System.Windows.Forms.KeyEventHandler( this.Button_KeyUp );
            this.RDownButton.CheckedChanged += new System.EventHandler( this.DigitalButton_CheckedChanged );
            this.RDownButton.MouseUp += new System.Windows.Forms.MouseEventHandler( this.DigitalButton_MouseUp );
            this.RDownButton.KeyDown += new System.Windows.Forms.KeyEventHandler( this.Button_KeyDown );
            // 
            // LeftThumbButton
            // 
            this.LeftThumbButton.Appearance = System.Windows.Forms.Appearance.Button;
            this.LeftThumbButton.BackColor = System.Drawing.Color.Transparent;
            this.LeftThumbButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.LeftThumbButton.FlatAppearance.CheckedBackColor = System.Drawing.Color.WhiteSmoke;
            this.LeftThumbButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.WhiteSmoke;
            this.LeftThumbButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.LeftThumbButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LeftThumbButton.Location = new System.Drawing.Point( 15, 247 );
            this.LeftThumbButton.Name = "LeftThumbButton";
            this.LeftThumbButton.Size = new System.Drawing.Size( 50, 23 );
            this.LeftThumbButton.TabIndex = 6;
            this.LeftThumbButton.Tag = "L3";
            this.LeftThumbButton.Text = "Thumb";
            this.LeftThumbButton.UseVisualStyleBackColor = false;
            this.LeftThumbButton.MouseDown += new System.Windows.Forms.MouseEventHandler( this.DigitalButton_MouseDown );
            this.LeftThumbButton.KeyUp += new System.Windows.Forms.KeyEventHandler( this.Button_KeyUp );
            this.LeftThumbButton.CheckedChanged += new System.EventHandler( this.DigitalButton_CheckedChanged );
            this.LeftThumbButton.MouseUp += new System.Windows.Forms.MouseEventHandler( this.DigitalButton_MouseUp );
            this.LeftThumbButton.KeyDown += new System.Windows.Forms.KeyEventHandler( this.Button_KeyDown );
            // 
            // RightThumbButton
            // 
            this.RightThumbButton.Appearance = System.Windows.Forms.Appearance.Button;
            this.RightThumbButton.BackColor = System.Drawing.Color.Transparent;
            this.RightThumbButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.RightThumbButton.FlatAppearance.CheckedBackColor = System.Drawing.Color.WhiteSmoke;
            this.RightThumbButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.WhiteSmoke;
            this.RightThumbButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.RightThumbButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.RightThumbButton.Location = new System.Drawing.Point( 203, 302 );
            this.RightThumbButton.Name = "RightThumbButton";
            this.RightThumbButton.Size = new System.Drawing.Size( 50, 23 );
            this.RightThumbButton.TabIndex = 18;
            this.RightThumbButton.Tag = "R3";
            this.RightThumbButton.Text = "Thumb";
            this.RightThumbButton.UseVisualStyleBackColor = false;
            this.RightThumbButton.MouseDown += new System.Windows.Forms.MouseEventHandler( this.DigitalButton_MouseDown );
            this.RightThumbButton.KeyUp += new System.Windows.Forms.KeyEventHandler( this.Button_KeyUp );
            this.RightThumbButton.CheckedChanged += new System.EventHandler( this.DigitalButton_CheckedChanged );
            this.RightThumbButton.MouseUp += new System.Windows.Forms.MouseEventHandler( this.DigitalButton_MouseUp );
            this.RightThumbButton.KeyDown += new System.Windows.Forms.KeyEventHandler( this.Button_KeyDown );
            // 
            // sendInputsCheckBox
            // 
            this.sendInputsCheckBox.Appearance = System.Windows.Forms.Appearance.Button;
            this.sendInputsCheckBox.AutoSize = true;
            this.sendInputsCheckBox.Checked = true;
            this.sendInputsCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.sendInputsCheckBox.ForeColor = System.Drawing.SystemColors.ControlText;
            this.sendInputsCheckBox.Location = new System.Drawing.Point( 84, 353 );
            this.sendInputsCheckBox.Name = "sendInputsCheckBox";
            this.sendInputsCheckBox.Size = new System.Drawing.Size( 74, 23 );
            this.sendInputsCheckBox.TabIndex = 19;
            this.sendInputsCheckBox.Text = "Send Inputs";
            this.sendInputsCheckBox.UseVisualStyleBackColor = true;
            this.sendInputsCheckBox.CheckedChanged += new System.EventHandler( this.sendInputsCheckBox_CheckedChanged );
            // 
            // clearButton
            // 
            this.clearButton.ForeColor = System.Drawing.SystemColors.ControlText;
            this.clearButton.Location = new System.Drawing.Point( 164, 353 );
            this.clearButton.Name = "clearButton";
            this.clearButton.Size = new System.Drawing.Size( 75, 23 );
            this.clearButton.TabIndex = 20;
            this.clearButton.Text = "Clear";
            this.clearButton.UseVisualStyleBackColor = true;
            this.clearButton.Click += new System.EventHandler( this.clearButton_Click );
            // 
            // Control360Pad
            // 
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImage = global::ragInput.Properties.Resources.xbox360;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.Controls.Add( this.clearButton );
            this.Controls.Add( this.sendInputsCheckBox );
            this.Controls.Add( this.RightThumbButton );
            this.Controls.Add( this.LeftThumbButton );
            this.Controls.Add( this.RDownButton );
            this.Controls.Add( this.RRightButton );
            this.Controls.Add( this.RLeftButton );
            this.Controls.Add( this.RUpButton );
            this.Controls.Add( this.RightAnalogStickButton );
            this.Controls.Add( this.StartButton );
            this.Controls.Add( this.BackButton );
            this.Controls.Add( this.LDownButton );
            this.Controls.Add( this.LRightButton );
            this.Controls.Add( this.LLeftButton );
            this.Controls.Add( this.LUpButton );
            this.Controls.Add( this.LeftAnalogStickButton );
            this.Controls.Add( this.RightButtonButton );
            this.Controls.Add( this.RightTriggerButton );
            this.Controls.Add( this.LeftButtonButton );
            this.Controls.Add( this.LeftTriggerButton );
            this.Controls.Add( this.titleLabel );
            this.Name = "Control360Pad";
            this.Size = new System.Drawing.Size( 318, 383 );
            this.Leave += new System.EventHandler( this.Control360Pad_Leave );
            this.ResumeLayout( false );
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label titleLabel;
        private System.Windows.Forms.CheckBox LeftTriggerButton;
        private System.Windows.Forms.CheckBox LeftButtonButton;
        private System.Windows.Forms.CheckBox RightTriggerButton;
        private System.Windows.Forms.CheckBox RightButtonButton;
        private System.Windows.Forms.CheckBox LeftAnalogStickButton;
        private System.Windows.Forms.CheckBox LUpButton;
        private System.Windows.Forms.CheckBox LLeftButton;
        private System.Windows.Forms.CheckBox LRightButton;
        private System.Windows.Forms.CheckBox LDownButton;
        private System.Windows.Forms.CheckBox BackButton;
        private System.Windows.Forms.CheckBox StartButton;
        private System.Windows.Forms.CheckBox RightAnalogStickButton;
        private System.Windows.Forms.CheckBox RUpButton;
        private System.Windows.Forms.CheckBox RLeftButton;
        private System.Windows.Forms.CheckBox RRightButton;
        private System.Windows.Forms.CheckBox RDownButton;
        private System.Windows.Forms.CheckBox LeftThumbButton;
        private System.Windows.Forms.CheckBox RightThumbButton;
        private System.Windows.Forms.CheckBox sendInputsCheckBox;
        private System.Windows.Forms.Button clearButton;
    }
}
