using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

using ragCore;
using ragWidgets;

namespace ragInput
{
    public class WidgetPad : Widget
    {
        public WidgetPad(BankPipe pipe, uint id, String title, String memo, Color fillColor, bool readOnly, WidgetPadVisible.Platform platform)
            : base(pipe, id, title, memo, fillColor, readOnly)
		{
			//
			// TODO: Add constructor logic here
			//
            m_platform = platform;
        }

        #region Variables
        private WidgetPadVisible.Platform m_platform;
        private bool m_sendInput = true;
        private Dictionary<ControlPad.DigitalPadButton, bool> m_digitalPadButtonsChecked = new Dictionary<ControlPad.DigitalPadButton, bool>();
        private Dictionary<ControlPad.AnalogPadButton, bool> m_analogPadButtonsChecked = new Dictionary<ControlPad.AnalogPadButton, bool>();
        private Point[] m_analogStickPositions = new Point[2];


        public event GenericValueEventHandler DigitalButtonChanged;
        public event GenericValueEventHandler AnalogButtonChanged;
        public event GenericValueEventHandler SendInputChanged;

        /// <summary>
        /// Thread synchronisation object.
        /// </summary>
        private static object s_syncObj = new object();

        /// <summary>
        /// Reference count for number of times this object has been initialised.
        /// </summary>
        private static int s_refCount = 0;
        #endregion

        #region Properties
        public WidgetPadVisible.Platform Platform
        {
            get { return m_platform; }
        }

        public bool SendInput
        {
            get
            {
                return m_sendInput;
            }
        }

        public Dictionary<ControlPad.DigitalPadButton, bool> DigitalPadButtonsChecked
        {
            get
            {
                return m_digitalPadButtonsChecked;
            }
        }

        public Dictionary<ControlPad.AnalogPadButton, bool> AnalogPadButtonsChecked
        {
            get
            {
                return m_analogPadButtonsChecked;
            }
        }

        public Point[] AnalogStickPositions
        {
            get
            {
                return m_analogStickPositions;
            }
        }
        #endregion

        #region Overrides
        public override bool AllowPersist
        {
            get
            {
                return false;
            }
        }

        public override int GetWidgetTypeGUID()
        {
            return WidgetPad.GetStaticGUID();
        }

        public override void UpdateRemote()
        {
            // do nothing
        }

        #endregion

        #region Public Functions
        public void ChangeSendInputs( bool send )
        {
            m_sendInput = send;

            if ( SendInputChanged != null)
            {
                SendInputChanged( this, new GenericValueArgs( send ) );
            }
        }

        public void DigitalButtonPressed( ControlPad.DigitalPadButton button )
        {
            m_digitalPadButtonsChecked[button] = true;

            BankRemotePacket p = new BankRemotePacket( this.Pipe );
            p.Begin(BankRemotePacket.EnumPacketType.USER_0, GetWidgetTypeGUID(), Id);
            p.Write_s32( (int)button );
            p.Send();

            if ( DigitalButtonChanged != null )
            {
                DigitalButtonChanged( this, new GenericValueArgs( Tuple.Create( button, true ) ) );
            }
        }

        public void DigitalButtonReleased( ControlPad.DigitalPadButton button, bool stillChecked )
        {
            m_digitalPadButtonsChecked[button] = stillChecked;

            BankRemotePacket p = new BankRemotePacket( this.Pipe );
            p.Begin(BankRemotePacket.EnumPacketType.USER_1, GetWidgetTypeGUID(), Id);
            p.Write_s32( (int)button );
            p.Send();
            
            if ( DigitalButtonChanged != null )
            {
                DigitalButtonChanged( this, new GenericValueArgs( Tuple.Create( button, stillChecked ) ) );
            }
        }

        public void AnalogButtonPresssed( ControlPad.AnalogPadButton button, byte x, byte y )
        {
            m_analogPadButtonsChecked[button] = true;
            m_analogStickPositions[(int)button - 1] = new Point( x, y );

            BankRemotePacket p = new BankRemotePacket( this.Pipe );
            p.Begin(BankRemotePacket.EnumPacketType.USER_2, GetWidgetTypeGUID(), Id);
            p.Write_s32( (int)button );
            p.Write_u8( x );
            p.Write_u8( y );
            p.Send();

            if ( AnalogButtonChanged != null )
            {
                AnalogButtonChanged( this, new GenericValueArgs( Tuple.Create( button, x, y, true ) ) );
            }
        }

        public void AnalogButtonMoved( ControlPad.AnalogPadButton button, byte x, byte y )
        {
            m_analogPadButtonsChecked[button] = true;
            m_analogStickPositions[(int)button - 1] = new Point( x, y );

            BankRemotePacket p = new BankRemotePacket( this.Pipe );
            p.Begin(BankRemotePacket.EnumPacketType.USER_3, GetWidgetTypeGUID(), Id);
            p.Write_s32( (int)button );
            p.Write_u8( x );
            p.Write_u8( y );
            p.Send();

            if ( AnalogButtonChanged != null )
            {
                AnalogButtonChanged( this, new GenericValueArgs( Tuple.Create( button, x, y, true ) ) );
            }
        }

        public void AnalogButtonReleased( ControlPad.AnalogPadButton button, byte x, byte y, bool isChecked )
        {
            m_analogPadButtonsChecked[button] = isChecked;
            m_analogStickPositions[(int)button - 1] = new Point( x, y );

            BankRemotePacket p = new BankRemotePacket( this.Pipe );
            p.Begin(BankRemotePacket.EnumPacketType.USER_4, GetWidgetTypeGUID(), Id);
            p.Write_s32( (int)button );
            p.Write_u8( x );
            p.Write_u8( y );
            p.Send();

            if ( AnalogButtonChanged != null )
            {
                AnalogButtonChanged( this, new GenericValueArgs( Tuple.Create( button, x, y, isChecked ) ) );
            }
        }
        #endregion

        #region Static Functions
        public static void AddHandlers( BankRemotePacketProcessor packetProcessor )
        {
            packetProcessor.AddType( GetStaticGUID(), new BankRemotePacketProcessor.HandlerDel( RemoteHandler ) );

            lock (s_syncObj)
            {
                // Check whether the script editor is currently set up.
                if (s_refCount == 0)
                {
                    WidgetVisualiser.AddDefaultHandlerShowVisible(typeof(WidgetPad), typeof(WidgetPadVisible));
                    WidgetVisualiser.AddDefaultHandlerHideVisible(typeof(WidgetPad));
                }

                ++s_refCount;
            }
        }

        public static int GetStaticGUID()
        {
            return ComputeGUID( 'i', 'n', 'p', 't' );
        }

        public static bool RemoteHandler( BankRemotePacket packet, out String errorMessage )
        {
            bool handled = false;

            if ( packet.BankCommand == BankRemotePacket.EnumPacketType.CREATE )
            {
                packet.Begin();

                uint remoteParent = packet.Read_bkWidget();
                String title = packet.Read_const_char();
                String memo = packet.Read_const_char();
                Color fillColor = Widget.DeserializeColor(packet.Read_const_char());
                bool readOnly = packet.Read_bool();
                int padIndex = packet.Read_s32();
                WidgetPadVisible.Platform platform = (WidgetPadVisible.Platform)packet.Read_s32();
                packet.End();
                
                Widget parent;
                if (packet.BankPipe.RemoteToLocal(remoteParent, out parent, out errorMessage))
                {
                    WidgetPad local = new WidgetPad(packet.BankPipe, packet.Id, title, memo, fillColor, readOnly, platform);
                    packet.BankPipe.Associate(packet.Id, local);
                    parent.AddChild(local);
                    handled = true;
                }
            }
            else if (packet.BankCommand == BankRemotePacket.EnumPacketType.USER_0 ||
                packet.BankCommand == BankRemotePacket.EnumPacketType.USER_1 ||
                packet.BankCommand == BankRemotePacket.EnumPacketType.USER_2 ||
                packet.BankCommand == BankRemotePacket.EnumPacketType.USER_3 ||
                packet.BankCommand == BankRemotePacket.EnumPacketType.USER_4)
            {
                handled = true;
                errorMessage = null;
            }
            else
            {
                handled = Widget.RemoteHandlerBase(packet, GetStaticGUID(), out errorMessage);
            }

            return handled;
        }

        public static void RemoveHandlers( BankRemotePacketProcessor packetProcessor )
        {
            packetProcessor.RemoveType( GetStaticGUID() );

            lock (s_syncObj)
            {
                --s_refCount;

                if (s_refCount == 0)
                {
                    WidgetVisualiser.RemoveHandlers(typeof(WidgetPad));
                }
            }
        }
        #endregion
    }
}
