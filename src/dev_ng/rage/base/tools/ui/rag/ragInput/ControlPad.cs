using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ragInput
{
    /// <summary>
    /// Base class for the PS3 and Xbox360 controller
    /// </summary>
    public abstract partial class ControlPad : ragUi.ControlBase
    {
        public ControlPad()
        {
            InitializeComponent();
        }

        #region Enums
        /// <summary>
        /// Keep in sync with rage/base/src/input/pad.h
        /// </summary>
        public enum DigitalPadButton
        {
            None = 0x0,
            L2 = 0x0001,
            R2 = 0x0002,
            L1 = 0x0004,
            R1 = 0x0008,
            RUP = 0x0010,
            RRIGHT = 0x0020,
            RDOWN = 0x0040,
            RLEFT = 0x0080,
            SELECT = 0x0100,
            L3 = 0x0200,
            R3 = 0x0400,
            START = 0x0800,
            LUP = 0x1000,
            LRIGHT = 0x2000,
            LDOWN = 0x4000,
            LLEFT = 0x8000
        };

        /// <summary>
        /// Keep in sync with rage/base/src/input/pad.h
        /// </summary>
        public enum AnalogPadButton
        {
            None,
            LEFT_STICK,
            RIGHT_STICK
        };
        #endregion

        #region Variables
        protected bool m_shiftIsDown = false;
        protected CheckBox m_lastDigitalPadButtonDown = null;
        protected CheckBox m_lastAnalogButtonDown = null;
        protected bool m_sendInputs = true;
        protected Point[] m_analogStickPosition = new Point[2];
        #endregion

        #region Properties
        public virtual bool SendInputs
        {
            get
            {
                return m_sendInputs;
            }
            set
            {
                m_sendInputs = value;
            }
        }
        #endregion

        #region Overrides
        public override string Text
        {
            get
            {
                return this.Text;
            }
            set
            {
                this.Text = value;
            }
        }
        #endregion

        #region Event Args
        public class DigitalPadButtonEventArgs : EventArgs
        {
            public DigitalPadButtonEventArgs( DigitalPadButton button, bool isChecked )
            {
                m_button = button;
                m_checked = isChecked;
            }

            #region Variables
            private DigitalPadButton m_button;
            private bool m_checked;
            #endregion

            #region Properties
            public DigitalPadButton Button
            {
                get
                {
                    return m_button;
                }
            }

            public bool Checked
            {
                get
                {
                    return m_checked;
                }
            }
            #endregion
        };

        public delegate void DigitalPadButtonEventHandler( object sender, DigitalPadButtonEventArgs e );

        public class AnalogPadButtonEventArgs : EventArgs
        {
            public AnalogPadButtonEventArgs( AnalogPadButton button, byte x, byte y, bool isChecked )
            {
                m_button = button;
                m_x = x;
                m_y = y;
                m_checked = isChecked;
            }

            #region Variables
            private AnalogPadButton m_button;
            private byte m_x;
            private byte m_y;
            private bool m_checked;
            #endregion

            #region Properties
            public AnalogPadButton Button
            {
                get
                {
                    return m_button;
                }
            }

            public byte X
            {
                get
                {
                    return m_x;
                }
            }

            public byte Y
            {
                get
                {
                    return m_y;
                }
            }

            public bool Checked
            {
                get
                {
                    return m_checked;
                }
            }
            #endregion
        };

        public delegate void AnalogPadButtonEventHandler( object sender, AnalogPadButtonEventArgs e );
        #endregion

        #region Events
        public event EventHandler SendInputsChanged;

        public event DigitalPadButtonEventHandler DigitalPadButtonDown;
        public event DigitalPadButtonEventHandler DigitalPadButtonUp;

        public event AnalogPadButtonEventHandler AnalogPadButtonDown;
        public event AnalogPadButtonEventHandler AnalogPadButtonMove;
        public event AnalogPadButtonEventHandler AnalogPadButtonUp;
        #endregion

        #region Public Functions
        public void SetDigitalPadButton( DigitalPadButton button, bool check )
        {
            foreach ( Control ctrl in this.Controls )
            {
                if ( ctrl is CheckBox )
                {
                    CheckBox buttonCtrl = ctrl as CheckBox;
                    if ( (buttonCtrl.Tag != null) && (buttonCtrl.Tag as string == button.ToString()) && !buttonCtrl.Focused )
                    {
                        buttonCtrl.Checked = check;
                        break;
                    }
                }
            }
        }

        public void SetAnalogPadbutton( AnalogPadButton button, byte x, byte y, bool check )
        {
            foreach ( Control ctrl in this.Controls )
            {
                if ( ctrl is CheckBox )
                {
                    CheckBox buttonCtrl = ctrl as CheckBox;
                    if ( (buttonCtrl.Tag != null) && (buttonCtrl.Tag as string == button.ToString()) && !buttonCtrl.Focused )
                    {
                        int cursorX = (int)(((float)x / (float)0xff) * (float)buttonCtrl.Width);
                        int cursorY = (int)(((float)y / (float)0xff) * (float)buttonCtrl.Height);

                        m_analogStickPosition[(int)button - 1] = new Point( cursorX, cursorY );
                        buttonCtrl.Checked = check;
                        buttonCtrl.Invalidate();
                        break;
                    }
                }
            }
        }
        #endregion

        #region Event Dispatchers
        protected void OnSendInputsChanged()
        {
            if ( this.SendInputsChanged != null )
            {
                this.SendInputsChanged( this, EventArgs.Empty );
            }
        }

        protected void OnDigitalPadButtonDown( CheckBox button )
        {
            m_lastDigitalPadButtonDown = button;

            button.Checked = true;
            button.Capture = true;

            if ( m_sendInputs && (this.DigitalPadButtonDown != null) )
            {
                this.DigitalPadButtonDown( this, 
                    new DigitalPadButtonEventArgs( (DigitalPadButton)Enum.Parse( typeof( DigitalPadButton ), button.Tag as string ),
                    button.Checked ) );
            }

            Cursor.Current = Cursors.Hand;
        }

        protected void OnDigitalPadButtonUp( CheckBox button )
        {
            if ( button.Capture )
            {
                if ( !m_shiftIsDown )
                {
                    button.Checked = false;

                    if ( m_sendInputs && (this.DigitalPadButtonUp != null) )
                    {
                        this.DigitalPadButtonUp( this, 
                            new DigitalPadButtonEventArgs( (DigitalPadButton)Enum.Parse( typeof( DigitalPadButton ), button.Tag as string ),
                            button.Checked ) );
                    }
                }
                else
                {
                    button.Checked = true;
                }

                button.Capture = false;

                if ( button == m_lastDigitalPadButtonDown )
                {
                    m_lastDigitalPadButtonDown = null;
                }

                Cursor.Current = Cursors.Default;
            }
        }

        protected void OnAnalogPadButtonDown( CheckBox button )
        {
            m_lastAnalogButtonDown = button;

            button.Checked = true;
            button.Capture = true;

            Point center = new Point( button.Width / 2, button.Height / 2 );
            Cursor.Position = button.PointToScreen( center );

            if ( m_sendInputs && (this.AnalogPadButtonDown != null) )
            {
                this.AnalogPadButtonDown( this, 
                    new AnalogPadButtonEventArgs( (AnalogPadButton)Enum.Parse( typeof( AnalogPadButton ), button.Tag as string ), 
                    0x80, 0x80, button.Checked ) );
            }

            Cursor.Current = Cursors.Hand;
        }

        protected void OnAnalogPadButtonMove( CheckBox button )
        {
            if ( button.Capture && button.Checked )
            {
                Point p = button.PointToClient( Cursor.Position );

                float xPercentage = (float)p.X / (float)button.Width;
                float yPercentage = (float)p.Y / (float)button.Height;

                byte x = (byte)Math.Min( xPercentage * 0xff, 0xff );
                byte y = (byte)Math.Min( yPercentage * 0xff, 0xff );

                AnalogPadButton padButton = (AnalogPadButton)Enum.Parse( typeof( AnalogPadButton ), button.Tag as string );
                m_analogStickPosition[(int)padButton - 1] = p;

                if ( m_sendInputs && (this.AnalogPadButtonMove != null) )
                {
                    this.AnalogPadButtonMove( this, new AnalogPadButtonEventArgs( padButton, x, y, button.Checked ) );
                }

                // force a redraw
                button.Invalidate();
            }
        }

        protected void OnAnalogPadButtonUp( CheckBox button )
        {
            if ( button.Capture )
            {
                if ( !m_shiftIsDown || button.Checked )
                {
                    button.Checked = false;

                    if ( m_sendInputs && (this.AnalogPadButtonUp != null) )
                    {
                        this.AnalogPadButtonUp( this,
                            new AnalogPadButtonEventArgs( (AnalogPadButton)Enum.Parse( typeof( AnalogPadButton ), button.Tag as string ), 
                            0x80, 0x80, button.Checked ) );
                    }
                }
                else
                {
                    button.Checked = true;
                }

                button.Capture = false;

                if ( button == m_lastAnalogButtonDown )
                {
                    m_lastAnalogButtonDown = null;
                }

                Cursor.Current = Cursors.Default;

                // force a redraw
                button.Invalidate();
            }
        }
        #endregion

        #region Protected Functions
        protected void ControlPad_Leave( object sender, EventArgs e )
        {
            if ( m_lastDigitalPadButtonDown != null )
            {
                OnDigitalPadButtonUp( m_lastDigitalPadButtonDown );
            }

            if ( m_lastAnalogButtonDown != null )
            {
                OnAnalogPadButtonUp( m_lastAnalogButtonDown );
            }

            m_shiftIsDown = false;
        }

        protected void ControlPad_DigitalButton_CheckChanged( object sender, EventArgs e )
        {
            CheckBox button = sender as CheckBox;
            if ( button != null )
            {
                button.FlatAppearance.MouseOverBackColor = button.Checked ? button.FlatAppearance.CheckedBackColor : Color.Transparent;
            }
        }

        protected void ControlPad_AnalogStick_Paint( object sender, PaintEventArgs e )
        {
            CheckBox button = sender as CheckBox;
            if ( button.Checked )
            {
                Point center = new Point( button.Width / 2, button.Height / 2 );

                AnalogPadButton padButton = (AnalogPadButton)Enum.Parse( typeof( AnalogPadButton ), button.Tag as string );

                e.Graphics.DrawLine( new Pen( Brushes.White, 2.0f ), center, m_analogStickPosition[(int)padButton - 1] );
            }
        }

        protected void ControlPad_Button_KeyDown( object sender, KeyEventArgs e )
        {
            m_shiftIsDown = e.Shift;
        }

        protected void ControlPad_Button_KeyUp( object sender, KeyEventArgs e )
        {
            m_shiftIsDown = e.Shift;
        }

        protected void SetSendInputs( bool send )
        {
            m_sendInputs = send;

            foreach ( Control ctrl in this.Controls )
            {
                if ( ctrl is CheckBox )
                {
                    CheckBox button = ctrl as CheckBox;
                    if ( (button.Tag != null) && button.Checked )
                    {
                        if ( m_sendInputs )
                        {
                            if ( (button.Tag as string).Contains( "_STICK" ) )
                            {
                                if ( this.AnalogPadButtonMove != null )
                                {
                                    AnalogPadButton padButton = (AnalogPadButton)Enum.Parse( typeof( AnalogPadButton ), button.Tag as string );
                                    float xPercentage = (float)m_analogStickPosition[(int)padButton - 1].X / (float)button.Width;
                                    float yPercentage = (float)m_analogStickPosition[(int)padButton - 1].Y / (float)button.Height;

                                    byte x = (byte)Math.Min( xPercentage * 0xff, 0xff );
                                    byte y = (byte)Math.Min( yPercentage * 0xff, 0xff );

                                    this.AnalogPadButtonMove( this, new AnalogPadButtonEventArgs( padButton, x, y, button.Checked ) );
                                }
                            }
                            else
                            {
                                if ( this.DigitalPadButtonDown != null )
                                {
                                    this.DigitalPadButtonDown( this,
                                        new DigitalPadButtonEventArgs( (DigitalPadButton)Enum.Parse( typeof( DigitalPadButton ), button.Tag as string ),
                                        button.Checked ) );
                                }
                            }
                        }
                        else
                        {
                            if ( (button.Tag as string).Contains( "_STICK" ) )
                            {
                                if ( this.AnalogPadButtonUp != null )
                                {
                                    this.AnalogPadButtonUp( this,
                                        new AnalogPadButtonEventArgs( (AnalogPadButton)Enum.Parse( typeof( AnalogPadButton ), button.Tag as string ),
                                        0x80, 0x80, button.Checked ) );
                                }
                            }
                            else
                            {
                                if ( this.DigitalPadButtonUp != null )
                                {
                                    this.DigitalPadButtonUp( this,
                                        new DigitalPadButtonEventArgs( (DigitalPadButton)Enum.Parse( typeof( DigitalPadButton ), button.Tag as string ),
                                        button.Checked ) );
                                }
                            }
                        }
                    }
                }
            }

            OnSendInputsChanged();
        }

        protected void Clear()
        {
            foreach ( Control ctrl in this.Controls )
            {
                if ( ctrl is CheckBox )
                {
                    CheckBox button = ctrl as CheckBox;
                    if ( (button.Tag != null) && button.Checked )
                    {
                        button.Capture = true;

                        if ( (button.Tag as string).Contains( "_STICK" ) )
                        {                            
                            OnAnalogPadButtonUp( button );
                        }
                        else
                        {
                            OnDigitalPadButtonUp( button );
                        }
                    }
                }
            }
        }
        #endregion
    }
}

