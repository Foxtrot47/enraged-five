using ragCore;

namespace ragInput
{
    public class External : IWidgetPlugIn
    {
        public void AddHandlers( BankRemotePacketProcessor packetProcessor )
        {
            WidgetPad.AddHandlers( packetProcessor );
        }

        public void RemoveHandlers( BankRemotePacketProcessor packetProcessor )
        {
            WidgetPad.RemoveHandlers( packetProcessor );
        }
    };
}