using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ragInput
{
    public partial class Control360Pad : ControlPad
    {
        public Control360Pad()
        {
            InitializeComponent();
            
            // initialize our analog stick positions
            int cursorX = (int)(0.5f * (float)this.LeftAnalogStickButton.Width);
            int cursorY = (int)(0.5f * (float)this.LeftAnalogStickButton.Height);
            m_analogStickPosition[(int)AnalogPadButton.LEFT_STICK - 1] = new Point( cursorX, cursorY );

            cursorX = (int)(0.5f * (float)this.RightAnalogStickButton.Width);
            cursorY = (int)(0.5f * (float)this.RightAnalogStickButton.Height);
            m_analogStickPosition[(int)AnalogPadButton.RIGHT_STICK - 1] = new Point( cursorX, cursorY );
        }

        #region Overrides
        public override bool SendInputs
        {
            get
            {
                return base.SendInputs;
            }
            set
            {
                base.SendInputs = value;

                EventHandler eh = new EventHandler( this.sendInputsCheckBox_CheckedChanged );
                this.sendInputsCheckBox.CheckedChanged -= eh;
                this.sendInputsCheckBox.Checked = value;
                this.sendInputsCheckBox.CheckedChanged += eh;
            }
        }

        public override bool IsReadOnly
        {
            get { return !this.SendInputs; }
            set { this.SendInputs = !value; }
        }

        public override string Text
        {
            get
            {
                return this.titleLabel.Text;
            }
            set
            {
                this.titleLabel.Text = value;
            }
        }

        protected override void OnIconChanged()
        {
            PadControlTextForIcon( this.titleLabel, m_Icon );
            this.titleLabel.Image = m_Icon;
        }
        #endregion

        #region Event Handlers
        private void Control360Pad_Leave( object sender, EventArgs e )
        {
            ControlPad_Leave( sender, e );
        }

        private void DigitalButton_CheckedChanged( object sender, EventArgs e )
        {
            ControlPad_DigitalButton_CheckChanged( sender, e );
        }

        private void DigitalButton_MouseDown( object sender, MouseEventArgs e )
        {
            if ( e.Button == MouseButtons.Left )
            {
                OnDigitalPadButtonDown( sender as CheckBox );
            }
        }

        private void DigitalButton_MouseUp( object sender, MouseEventArgs e )
        {
            if ( e.Button == MouseButtons.Left )
            {
                OnDigitalPadButtonUp( sender as CheckBox );
            }
        }

        private void AnalogStick_MouseDown( object sender, MouseEventArgs e )
        {
            if ( e.Button == MouseButtons.Left )
            {
                OnAnalogPadButtonDown( sender as CheckBox );
            }
        }

        private void AnalogStick_MouseMove( object sender, MouseEventArgs e )
        {
            if ( e.Button == MouseButtons.Left )
            {
                OnAnalogPadButtonMove( sender as CheckBox );
            }
        }

        private void AnalogStick_MouseUp( object sender, MouseEventArgs e )
        {
            if ( e.Button == MouseButtons.Left )
            {
                OnAnalogPadButtonUp( sender as CheckBox );
            }
        }

        private void AnalogStick_Paint( object sender, PaintEventArgs e )
        {
            ControlPad_AnalogStick_Paint( sender, e );
        }

        private void Button_KeyDown( object sender, KeyEventArgs e )
        {
            ControlPad_Button_KeyDown( sender, e );
        }

        private void Button_KeyUp( object sender, KeyEventArgs e )
        {
            ControlPad_Button_KeyUp( sender, e );
        }

        private void sendInputsCheckBox_CheckedChanged( object sender, EventArgs e )
        {
            SetSendInputs( this.sendInputsCheckBox.Checked );
        }

        private void clearButton_Click( object sender, EventArgs e )
        {
            Clear();
        }
        #endregion
    }
}

