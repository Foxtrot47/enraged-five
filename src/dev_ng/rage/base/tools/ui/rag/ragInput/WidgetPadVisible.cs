using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

using ragCore;
using ragWidgets;
using System.Diagnostics;

namespace ragInput
{
    public class WidgetPadVisible : WidgetControlBaseVisible
    {
        public WidgetPadVisible(IWidgetView widgetView, WidgetPad widget, Control parent, ToolTip tooltip)
            : base( widgetView, parent, widget )
        {
            m_widget = widget;
            m_widget.DigitalButtonChanged += widget_DigitalButtonChanged;
            m_widget.AnalogButtonChanged += widget_AnalogButtonChanged;
            m_widget.SendInputChanged += widget_SendInputChanged;

            switch ( widget.Platform)
            {
                case Platform.PS3:
                    m_controlPad = new ControlPS3Pad();
                    break;
                default:
                    m_controlPad = new Control360Pad();
                    break;
            }

            m_controlPad.AnalogPadButtonDown += new ControlPad.AnalogPadButtonEventHandler( controlPad_AnalogPadButtonDown );
            m_controlPad.AnalogPadButtonMove += new ControlPad.AnalogPadButtonEventHandler( controlPad_AnalogPadButtonMove );
            m_controlPad.AnalogPadButtonUp += new ControlPad.AnalogPadButtonEventHandler( controlPad_AnalogPadButtonUp );
            m_controlPad.DigitalPadButtonDown += new ControlPad.DigitalPadButtonEventHandler( controlPad_DigitalPadButtonDown );
            m_controlPad.DigitalPadButtonUp += new ControlPad.DigitalPadButtonEventHandler( controlPad_DigitalPadButtonUp );
            m_controlPad.SendInputsChanged += new EventHandler( controlPad_SendInputsChanged );
            m_controlPad.Text = m_widget.Title;

            m_controlPad.IsReadOnly = m_widget.IsReadOnly;
            m_controlPad.SendInputs = m_widget.SendInput;

            bool isChecked;
            if ( m_widget.DigitalPadButtonsChecked.TryGetValue( ControlPad.DigitalPadButton.L1, out isChecked ) )
            {
                m_controlPad.SetDigitalPadButton( ControlPad.DigitalPadButton.L1, isChecked );
            }

            if ( m_widget.DigitalPadButtonsChecked.TryGetValue( ControlPad.DigitalPadButton.L2, out isChecked ) )
            {
                m_controlPad.SetDigitalPadButton( ControlPad.DigitalPadButton.L2, isChecked );
            }

            if ( m_widget.DigitalPadButtonsChecked.TryGetValue( ControlPad.DigitalPadButton.L3, out isChecked ) )
            {
                m_controlPad.SetDigitalPadButton( ControlPad.DigitalPadButton.L3, isChecked );
            }

            if ( m_widget.DigitalPadButtonsChecked.TryGetValue( ControlPad.DigitalPadButton.LDOWN, out isChecked ) )
            {
                m_controlPad.SetDigitalPadButton( ControlPad.DigitalPadButton.LDOWN, isChecked );
            }

            if ( m_widget.DigitalPadButtonsChecked.TryGetValue( ControlPad.DigitalPadButton.LLEFT, out isChecked ) )
            {
                m_controlPad.SetDigitalPadButton( ControlPad.DigitalPadButton.LLEFT, isChecked );
            }

            if ( m_widget.DigitalPadButtonsChecked.TryGetValue( ControlPad.DigitalPadButton.LRIGHT, out isChecked ) )
            {
                m_controlPad.SetDigitalPadButton( ControlPad.DigitalPadButton.LRIGHT, isChecked );
            }

            if ( m_widget.DigitalPadButtonsChecked.TryGetValue( ControlPad.DigitalPadButton.LUP, out isChecked ) )
            {
                m_controlPad.SetDigitalPadButton( ControlPad.DigitalPadButton.LUP, isChecked );
            }

            if ( m_widget.DigitalPadButtonsChecked.TryGetValue( ControlPad.DigitalPadButton.R1, out isChecked ) )
            {
                m_controlPad.SetDigitalPadButton( ControlPad.DigitalPadButton.R1, isChecked );
            }

            if ( m_widget.DigitalPadButtonsChecked.TryGetValue( ControlPad.DigitalPadButton.R2, out isChecked ) )
            {
                m_controlPad.SetDigitalPadButton( ControlPad.DigitalPadButton.R2, isChecked );
            }

            if ( m_widget.DigitalPadButtonsChecked.TryGetValue( ControlPad.DigitalPadButton.R3, out isChecked ) )
            {
                m_controlPad.SetDigitalPadButton( ControlPad.DigitalPadButton.R3, isChecked );
            }

            if ( m_widget.DigitalPadButtonsChecked.TryGetValue( ControlPad.DigitalPadButton.RDOWN, out isChecked ) )
            {
                m_controlPad.SetDigitalPadButton( ControlPad.DigitalPadButton.RDOWN, isChecked );
            }

            if ( m_widget.DigitalPadButtonsChecked.TryGetValue( ControlPad.DigitalPadButton.RLEFT, out isChecked ) )
            {
                m_controlPad.SetDigitalPadButton( ControlPad.DigitalPadButton.RLEFT, isChecked );
            }

            if ( m_widget.DigitalPadButtonsChecked.TryGetValue( ControlPad.DigitalPadButton.RRIGHT, out isChecked ) )
            {
                m_controlPad.SetDigitalPadButton( ControlPad.DigitalPadButton.RRIGHT, isChecked );
            }

            if ( m_widget.DigitalPadButtonsChecked.TryGetValue( ControlPad.DigitalPadButton.RUP, out isChecked ) )
            {
                m_controlPad.SetDigitalPadButton( ControlPad.DigitalPadButton.RUP, isChecked );
            }

            if ( m_widget.DigitalPadButtonsChecked.TryGetValue( ControlPad.DigitalPadButton.SELECT, out isChecked ) )
            {
                m_controlPad.SetDigitalPadButton( ControlPad.DigitalPadButton.SELECT, isChecked );
            }

            if ( m_widget.DigitalPadButtonsChecked.TryGetValue( ControlPad.DigitalPadButton.START, out isChecked ) )
            {
                m_controlPad.SetDigitalPadButton( ControlPad.DigitalPadButton.START, isChecked );
            }

            isChecked = false;
            m_widget.AnalogPadButtonsChecked.TryGetValue( ControlPad.AnalogPadButton.LEFT_STICK, out isChecked );
            m_controlPad.SetAnalogPadbutton( ControlPad.AnalogPadButton.LEFT_STICK,
                (byte)m_widget.AnalogStickPositions[(int)ControlPad.AnalogPadButton.LEFT_STICK - 1].X,
                (byte)m_widget.AnalogStickPositions[(int)ControlPad.AnalogPadButton.LEFT_STICK - 1].Y,
                isChecked );

            isChecked = false;
            m_widget.AnalogPadButtonsChecked.TryGetValue( ControlPad.AnalogPadButton.RIGHT_STICK, out isChecked );
            m_controlPad.SetAnalogPadbutton( ControlPad.AnalogPadButton.RIGHT_STICK,
                (byte)m_widget.AnalogStickPositions[(int)ControlPad.AnalogPadButton.RIGHT_STICK - 1].X,
                (byte)m_widget.AnalogStickPositions[(int)ControlPad.AnalogPadButton.RIGHT_STICK - 1].Y,
                isChecked );

            FinishCreation(tooltip);
        }

        #region Enums
        public enum Platform
        {
            Win32,
            Xbox360,
            PS3,
        }
        #endregion

        #region Variables
        private ControlPad m_controlPad;
        private WidgetPad m_widget;
        #endregion

        #region Properties
        public bool SendInputs
        {
            get
            {
                return m_controlPad.SendInputs;
            }
            set
            {
                m_controlPad.SendInputs = value;
            }
        }
        #endregion

        #region Public Functions
        public void SetDigitalPadButton( ControlPad.DigitalPadButton button, bool check )
        {
            m_controlPad.SetDigitalPadButton( button, check );
        }

        public void SetAnalogPadbutton( ControlPad.AnalogPadButton button, byte x, byte y, bool check )
        {
            m_controlPad.SetAnalogPadbutton( button, x, y, check );
        }
        #endregion

        #region Overrides
        public override ragUi.ControlBase ControlBase
        {
            get
            {
                return m_controlPad;
            }
        }

        public override Widget Widget
        {
            get
            {
                return m_widget;
            }
        }
        #endregion

        #region Event Handlers
        private void controlPad_AnalogPadButtonDown( object sender, ControlPad.AnalogPadButtonEventArgs e )
        {
            m_widget.AnalogButtonPresssed( e.Button, e.X, e.Y );
        }

        private void controlPad_AnalogPadButtonMove( object sender, ControlPad.AnalogPadButtonEventArgs e )
        {
            m_widget.AnalogButtonMoved( e.Button, e.X, e.Y );
        }

        private void controlPad_AnalogPadButtonUp( object sender, ControlPad.AnalogPadButtonEventArgs e )
        {
            m_widget.AnalogButtonReleased( e.Button, e.X, e.Y, e.Checked );
        }

        private void controlPad_DigitalPadButtonDown( object sender, ControlPad.DigitalPadButtonEventArgs e )
        {
            m_widget.DigitalButtonPressed( e.Button );
        }

        private void controlPad_DigitalPadButtonUp( object sender, ControlPad.DigitalPadButtonEventArgs e )
        {
            m_widget.DigitalButtonReleased( e.Button, e.Checked );
        }

        private void controlPad_SendInputsChanged( object sender, EventArgs e )
        {
            m_widget.ChangeSendInputs( m_controlPad.SendInputs );
        }

        private void widget_DigitalButtonChanged( object sender, GenericValueArgs args )
        {
            Debug.Assert( sender == m_widget );

            var t = (Tuple<ControlPad.DigitalPadButton, bool>)args.Value;
            SetDigitalPadButton( t.Item1, t.Item2 );
        }

        private void widget_AnalogButtonChanged( object sender, GenericValueArgs args )
        {
            Debug.Assert( sender == m_widget );

            var t = (Tuple<ControlPad.AnalogPadButton, byte, byte, bool>)args.Value;
            SetAnalogPadbutton( t.Item1, t.Item2, t.Item3, t.Item4 );
        }

        private void widget_SendInputChanged( object sender, GenericValueArgs args )
        {
            Debug.Assert( sender == m_widget );
            SendInputs = (bool)args.Value;
        }
        #endregion
    }
}
