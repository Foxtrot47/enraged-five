namespace ragInput
{
    partial class ControlPS3Pad
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && (components != null) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.L1Button = new System.Windows.Forms.CheckBox();
            this.L2Button = new System.Windows.Forms.CheckBox();
            this.R2Button = new System.Windows.Forms.CheckBox();
            this.R1Button = new System.Windows.Forms.CheckBox();
            this.LUpButton = new System.Windows.Forms.CheckBox();
            this.LLeftButton = new System.Windows.Forms.CheckBox();
            this.LRightButton = new System.Windows.Forms.CheckBox();
            this.LDownButton = new System.Windows.Forms.CheckBox();
            this.SelectButton = new System.Windows.Forms.CheckBox();
            this.StartButton = new System.Windows.Forms.CheckBox();
            this.RUpButton = new System.Windows.Forms.CheckBox();
            this.RLeftButton = new System.Windows.Forms.CheckBox();
            this.RRightButton = new System.Windows.Forms.CheckBox();
            this.RDownButton = new System.Windows.Forms.CheckBox();
            this.LeftAnalogStickButton = new System.Windows.Forms.CheckBox();
            this.RightAnalogStickButton = new System.Windows.Forms.CheckBox();
            this.titleLabel = new System.Windows.Forms.Label();
            this.L3Button = new System.Windows.Forms.CheckBox();
            this.R3Button = new System.Windows.Forms.CheckBox();
            this.sendInputsCheckBox = new System.Windows.Forms.CheckBox();
            this.clearButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // L1Button
            // 
            this.L1Button.Appearance = System.Windows.Forms.Appearance.Button;
            this.L1Button.BackColor = System.Drawing.Color.Transparent;
            this.L1Button.FlatAppearance.BorderSize = 0;
            this.L1Button.FlatAppearance.CheckedBackColor = System.Drawing.Color.Gray;
            this.L1Button.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Gray;
            this.L1Button.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.L1Button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.L1Button.ForeColor = System.Drawing.Color.Transparent;
            this.L1Button.Location = new System.Drawing.Point( 59, 77 );
            this.L1Button.Name = "L1Button";
            this.L1Button.Size = new System.Drawing.Size( 47, 23 );
            this.L1Button.TabIndex = 3;
            this.L1Button.Tag = "L1";
            this.L1Button.UseVisualStyleBackColor = false;
            this.L1Button.MouseDown += new System.Windows.Forms.MouseEventHandler( this.DigitalButton_MouseDown );
            this.L1Button.KeyUp += new System.Windows.Forms.KeyEventHandler( this.Button_KeyUp );
            this.L1Button.CheckedChanged += new System.EventHandler( this.DigitalButton_CheckedChanged );
            this.L1Button.MouseUp += new System.Windows.Forms.MouseEventHandler( this.DigitalButton_MouseUp );
            this.L1Button.KeyDown += new System.Windows.Forms.KeyEventHandler( this.Button_KeyDown );
            // 
            // L2Button
            // 
            this.L2Button.Appearance = System.Windows.Forms.Appearance.Button;
            this.L2Button.BackColor = System.Drawing.Color.Transparent;
            this.L2Button.FlatAppearance.BorderSize = 0;
            this.L2Button.FlatAppearance.CheckedBackColor = System.Drawing.Color.Gray;
            this.L2Button.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Gray;
            this.L2Button.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.L2Button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.L2Button.ForeColor = System.Drawing.Color.Transparent;
            this.L2Button.Location = new System.Drawing.Point( 59, 18 );
            this.L2Button.Name = "L2Button";
            this.L2Button.Size = new System.Drawing.Size( 47, 41 );
            this.L2Button.TabIndex = 1;
            this.L2Button.Tag = "L2";
            this.L2Button.UseVisualStyleBackColor = false;
            this.L2Button.MouseDown += new System.Windows.Forms.MouseEventHandler( this.DigitalButton_MouseDown );
            this.L2Button.KeyUp += new System.Windows.Forms.KeyEventHandler( this.Button_KeyUp );
            this.L2Button.CheckedChanged += new System.EventHandler( this.DigitalButton_CheckedChanged );
            this.L2Button.MouseUp += new System.Windows.Forms.MouseEventHandler( this.DigitalButton_MouseUp );
            this.L2Button.KeyDown += new System.Windows.Forms.KeyEventHandler( this.Button_KeyDown );
            // 
            // R2Button
            // 
            this.R2Button.Appearance = System.Windows.Forms.Appearance.Button;
            this.R2Button.BackColor = System.Drawing.Color.Transparent;
            this.R2Button.FlatAppearance.BorderSize = 0;
            this.R2Button.FlatAppearance.CheckedBackColor = System.Drawing.Color.Gray;
            this.R2Button.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Gray;
            this.R2Button.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.R2Button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.R2Button.ForeColor = System.Drawing.Color.Transparent;
            this.R2Button.Location = new System.Drawing.Point( 294, 16 );
            this.R2Button.Name = "R2Button";
            this.R2Button.Size = new System.Drawing.Size( 47, 41 );
            this.R2Button.TabIndex = 2;
            this.R2Button.Tag = "R2";
            this.R2Button.UseVisualStyleBackColor = false;
            this.R2Button.MouseDown += new System.Windows.Forms.MouseEventHandler( this.DigitalButton_MouseDown );
            this.R2Button.KeyUp += new System.Windows.Forms.KeyEventHandler( this.Button_KeyUp );
            this.R2Button.CheckedChanged += new System.EventHandler( this.DigitalButton_CheckedChanged );
            this.R2Button.MouseUp += new System.Windows.Forms.MouseEventHandler( this.DigitalButton_MouseUp );
            this.R2Button.KeyDown += new System.Windows.Forms.KeyEventHandler( this.Button_KeyDown );
            // 
            // R1Button
            // 
            this.R1Button.Appearance = System.Windows.Forms.Appearance.Button;
            this.R1Button.BackColor = System.Drawing.Color.Transparent;
            this.R1Button.FlatAppearance.BorderSize = 0;
            this.R1Button.FlatAppearance.CheckedBackColor = System.Drawing.Color.Gray;
            this.R1Button.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Gray;
            this.R1Button.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.R1Button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.R1Button.ForeColor = System.Drawing.Color.Transparent;
            this.R1Button.Location = new System.Drawing.Point( 296, 76 );
            this.R1Button.Name = "R1Button";
            this.R1Button.Size = new System.Drawing.Size( 47, 23 );
            this.R1Button.TabIndex = 4;
            this.R1Button.Tag = "R1";
            this.R1Button.UseVisualStyleBackColor = false;
            this.R1Button.MouseDown += new System.Windows.Forms.MouseEventHandler( this.DigitalButton_MouseDown );
            this.R1Button.KeyUp += new System.Windows.Forms.KeyEventHandler( this.Button_KeyUp );
            this.R1Button.CheckedChanged += new System.EventHandler( this.DigitalButton_CheckedChanged );
            this.R1Button.MouseUp += new System.Windows.Forms.MouseEventHandler( this.DigitalButton_MouseUp );
            this.R1Button.KeyDown += new System.Windows.Forms.KeyEventHandler( this.Button_KeyDown );
            // 
            // LUpButton
            // 
            this.LUpButton.Appearance = System.Windows.Forms.Appearance.Button;
            this.LUpButton.BackColor = System.Drawing.Color.Transparent;
            this.LUpButton.FlatAppearance.BorderSize = 0;
            this.LUpButton.FlatAppearance.CheckedBackColor = System.Drawing.Color.Gray;
            this.LUpButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Gray;
            this.LUpButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.LUpButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LUpButton.ForeColor = System.Drawing.Color.Transparent;
            this.LUpButton.Location = new System.Drawing.Point( 67, 171 );
            this.LUpButton.Name = "LUpButton";
            this.LUpButton.Size = new System.Drawing.Size( 31, 29 );
            this.LUpButton.TabIndex = 6;
            this.LUpButton.Tag = "LUP";
            this.LUpButton.UseVisualStyleBackColor = false;
            this.LUpButton.MouseDown += new System.Windows.Forms.MouseEventHandler( this.DigitalButton_MouseDown );
            this.LUpButton.KeyUp += new System.Windows.Forms.KeyEventHandler( this.Button_KeyUp );
            this.LUpButton.CheckedChanged += new System.EventHandler( this.DigitalButton_CheckedChanged );
            this.LUpButton.MouseUp += new System.Windows.Forms.MouseEventHandler( this.DigitalButton_MouseUp );
            this.LUpButton.KeyDown += new System.Windows.Forms.KeyEventHandler( this.Button_KeyDown );
            // 
            // LLeftButton
            // 
            this.LLeftButton.Appearance = System.Windows.Forms.Appearance.Button;
            this.LLeftButton.BackColor = System.Drawing.Color.Transparent;
            this.LLeftButton.FlatAppearance.BorderSize = 0;
            this.LLeftButton.FlatAppearance.CheckedBackColor = System.Drawing.Color.Gray;
            this.LLeftButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Gray;
            this.LLeftButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.LLeftButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LLeftButton.ForeColor = System.Drawing.Color.Transparent;
            this.LLeftButton.Location = new System.Drawing.Point( 38, 201 );
            this.LLeftButton.Name = "LLeftButton";
            this.LLeftButton.Size = new System.Drawing.Size( 31, 29 );
            this.LLeftButton.TabIndex = 5;
            this.LLeftButton.Tag = "LLEFT";
            this.LLeftButton.UseVisualStyleBackColor = false;
            this.LLeftButton.MouseDown += new System.Windows.Forms.MouseEventHandler( this.DigitalButton_MouseDown );
            this.LLeftButton.KeyUp += new System.Windows.Forms.KeyEventHandler( this.Button_KeyUp );
            this.LLeftButton.CheckedChanged += new System.EventHandler( this.DigitalButton_CheckedChanged );
            this.LLeftButton.MouseUp += new System.Windows.Forms.MouseEventHandler( this.DigitalButton_MouseUp );
            this.LLeftButton.KeyDown += new System.Windows.Forms.KeyEventHandler( this.Button_KeyDown );
            // 
            // LRightButton
            // 
            this.LRightButton.Appearance = System.Windows.Forms.Appearance.Button;
            this.LRightButton.BackColor = System.Drawing.Color.Transparent;
            this.LRightButton.FlatAppearance.BorderSize = 0;
            this.LRightButton.FlatAppearance.CheckedBackColor = System.Drawing.Color.Gray;
            this.LRightButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Gray;
            this.LRightButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.LRightButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LRightButton.ForeColor = System.Drawing.Color.Transparent;
            this.LRightButton.Location = new System.Drawing.Point( 97, 201 );
            this.LRightButton.Name = "LRightButton";
            this.LRightButton.Size = new System.Drawing.Size( 31, 29 );
            this.LRightButton.TabIndex = 7;
            this.LRightButton.Tag = "LRIGHT";
            this.LRightButton.UseVisualStyleBackColor = false;
            this.LRightButton.MouseDown += new System.Windows.Forms.MouseEventHandler( this.DigitalButton_MouseDown );
            this.LRightButton.KeyUp += new System.Windows.Forms.KeyEventHandler( this.Button_KeyUp );
            this.LRightButton.CheckedChanged += new System.EventHandler( this.DigitalButton_CheckedChanged );
            this.LRightButton.MouseUp += new System.Windows.Forms.MouseEventHandler( this.DigitalButton_MouseUp );
            this.LRightButton.KeyDown += new System.Windows.Forms.KeyEventHandler( this.Button_KeyDown );
            // 
            // LDownButton
            // 
            this.LDownButton.Appearance = System.Windows.Forms.Appearance.Button;
            this.LDownButton.BackColor = System.Drawing.Color.Transparent;
            this.LDownButton.FlatAppearance.BorderSize = 0;
            this.LDownButton.FlatAppearance.CheckedBackColor = System.Drawing.Color.Gray;
            this.LDownButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Gray;
            this.LDownButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.LDownButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LDownButton.ForeColor = System.Drawing.Color.Transparent;
            this.LDownButton.Location = new System.Drawing.Point( 67, 228 );
            this.LDownButton.Name = "LDownButton";
            this.LDownButton.Size = new System.Drawing.Size( 31, 29 );
            this.LDownButton.TabIndex = 8;
            this.LDownButton.Tag = "LDOWN";
            this.LDownButton.UseVisualStyleBackColor = false;
            this.LDownButton.MouseDown += new System.Windows.Forms.MouseEventHandler( this.DigitalButton_MouseDown );
            this.LDownButton.KeyUp += new System.Windows.Forms.KeyEventHandler( this.Button_KeyUp );
            this.LDownButton.CheckedChanged += new System.EventHandler( this.DigitalButton_CheckedChanged );
            this.LDownButton.MouseUp += new System.Windows.Forms.MouseEventHandler( this.DigitalButton_MouseUp );
            this.LDownButton.KeyDown += new System.Windows.Forms.KeyEventHandler( this.Button_KeyDown );
            // 
            // SelectButton
            // 
            this.SelectButton.Appearance = System.Windows.Forms.Appearance.Button;
            this.SelectButton.BackColor = System.Drawing.Color.Transparent;
            this.SelectButton.FlatAppearance.BorderSize = 0;
            this.SelectButton.FlatAppearance.CheckedBackColor = System.Drawing.Color.Gray;
            this.SelectButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Gray;
            this.SelectButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.SelectButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SelectButton.ForeColor = System.Drawing.Color.Transparent;
            this.SelectButton.Location = new System.Drawing.Point( 152, 204 );
            this.SelectButton.Name = "SelectButton";
            this.SelectButton.Size = new System.Drawing.Size( 27, 22 );
            this.SelectButton.TabIndex = 10;
            this.SelectButton.Tag = "SELECT";
            this.SelectButton.UseVisualStyleBackColor = false;
            this.SelectButton.MouseDown += new System.Windows.Forms.MouseEventHandler( this.DigitalButton_MouseDown );
            this.SelectButton.KeyUp += new System.Windows.Forms.KeyEventHandler( this.Button_KeyUp );
            this.SelectButton.CheckedChanged += new System.EventHandler( this.DigitalButton_CheckedChanged );
            this.SelectButton.MouseUp += new System.Windows.Forms.MouseEventHandler( this.DigitalButton_MouseUp );
            this.SelectButton.KeyDown += new System.Windows.Forms.KeyEventHandler( this.Button_KeyDown );
            // 
            // StartButton
            // 
            this.StartButton.Appearance = System.Windows.Forms.Appearance.Button;
            this.StartButton.BackColor = System.Drawing.Color.Transparent;
            this.StartButton.FlatAppearance.BorderSize = 0;
            this.StartButton.FlatAppearance.CheckedBackColor = System.Drawing.Color.Gray;
            this.StartButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Gray;
            this.StartButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.StartButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.StartButton.ForeColor = System.Drawing.Color.Transparent;
            this.StartButton.Location = new System.Drawing.Point( 223, 204 );
            this.StartButton.Name = "StartButton";
            this.StartButton.Size = new System.Drawing.Size( 27, 23 );
            this.StartButton.TabIndex = 11;
            this.StartButton.Tag = "START";
            this.StartButton.UseVisualStyleBackColor = false;
            this.StartButton.MouseDown += new System.Windows.Forms.MouseEventHandler( this.DigitalButton_MouseDown );
            this.StartButton.KeyUp += new System.Windows.Forms.KeyEventHandler( this.Button_KeyUp );
            this.StartButton.CheckedChanged += new System.EventHandler( this.DigitalButton_CheckedChanged );
            this.StartButton.MouseUp += new System.Windows.Forms.MouseEventHandler( this.DigitalButton_MouseUp );
            this.StartButton.KeyDown += new System.Windows.Forms.KeyEventHandler( this.Button_KeyDown );
            // 
            // RUpButton
            // 
            this.RUpButton.Appearance = System.Windows.Forms.Appearance.Button;
            this.RUpButton.BackColor = System.Drawing.Color.Transparent;
            this.RUpButton.FlatAppearance.BorderSize = 0;
            this.RUpButton.FlatAppearance.CheckedBackColor = System.Drawing.Color.Green;
            this.RUpButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Green;
            this.RUpButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.RUpButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.RUpButton.ForeColor = System.Drawing.Color.Transparent;
            this.RUpButton.Location = new System.Drawing.Point( 303, 169 );
            this.RUpButton.Name = "RUpButton";
            this.RUpButton.Size = new System.Drawing.Size( 33, 31 );
            this.RUpButton.TabIndex = 14;
            this.RUpButton.Tag = "RUP";
            this.RUpButton.UseVisualStyleBackColor = false;
            this.RUpButton.MouseDown += new System.Windows.Forms.MouseEventHandler( this.DigitalButton_MouseDown );
            this.RUpButton.KeyUp += new System.Windows.Forms.KeyEventHandler( this.Button_KeyUp );
            this.RUpButton.CheckedChanged += new System.EventHandler( this.DigitalButton_CheckedChanged );
            this.RUpButton.MouseUp += new System.Windows.Forms.MouseEventHandler( this.DigitalButton_MouseUp );
            this.RUpButton.KeyDown += new System.Windows.Forms.KeyEventHandler( this.Button_KeyDown );
            // 
            // RLeftButton
            // 
            this.RLeftButton.Appearance = System.Windows.Forms.Appearance.Button;
            this.RLeftButton.BackColor = System.Drawing.Color.Transparent;
            this.RLeftButton.FlatAppearance.BorderSize = 0;
            this.RLeftButton.FlatAppearance.CheckedBackColor = System.Drawing.Color.Pink;
            this.RLeftButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Pink;
            this.RLeftButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.RLeftButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.RLeftButton.ForeColor = System.Drawing.Color.Transparent;
            this.RLeftButton.Location = new System.Drawing.Point( 270, 200 );
            this.RLeftButton.Name = "RLeftButton";
            this.RLeftButton.Size = new System.Drawing.Size( 33, 31 );
            this.RLeftButton.TabIndex = 13;
            this.RLeftButton.Tag = "RLEFT";
            this.RLeftButton.UseVisualStyleBackColor = false;
            this.RLeftButton.MouseDown += new System.Windows.Forms.MouseEventHandler( this.DigitalButton_MouseDown );
            this.RLeftButton.KeyUp += new System.Windows.Forms.KeyEventHandler( this.Button_KeyUp );
            this.RLeftButton.CheckedChanged += new System.EventHandler( this.DigitalButton_CheckedChanged );
            this.RLeftButton.MouseUp += new System.Windows.Forms.MouseEventHandler( this.DigitalButton_MouseUp );
            this.RLeftButton.KeyDown += new System.Windows.Forms.KeyEventHandler( this.Button_KeyDown );
            // 
            // RRightButton
            // 
            this.RRightButton.Appearance = System.Windows.Forms.Appearance.Button;
            this.RRightButton.BackColor = System.Drawing.Color.Transparent;
            this.RRightButton.FlatAppearance.BorderSize = 0;
            this.RRightButton.FlatAppearance.CheckedBackColor = System.Drawing.Color.Red;
            this.RRightButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Red;
            this.RRightButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.RRightButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.RRightButton.ForeColor = System.Drawing.Color.Transparent;
            this.RRightButton.Location = new System.Drawing.Point( 337, 200 );
            this.RRightButton.Name = "RRightButton";
            this.RRightButton.Size = new System.Drawing.Size( 33, 31 );
            this.RRightButton.TabIndex = 15;
            this.RRightButton.Tag = "RRIGHT";
            this.RRightButton.UseVisualStyleBackColor = false;
            this.RRightButton.MouseDown += new System.Windows.Forms.MouseEventHandler( this.DigitalButton_MouseDown );
            this.RRightButton.KeyUp += new System.Windows.Forms.KeyEventHandler( this.Button_KeyUp );
            this.RRightButton.CheckedChanged += new System.EventHandler( this.DigitalButton_CheckedChanged );
            this.RRightButton.MouseUp += new System.Windows.Forms.MouseEventHandler( this.DigitalButton_MouseUp );
            this.RRightButton.KeyDown += new System.Windows.Forms.KeyEventHandler( this.Button_KeyDown );
            // 
            // RDownButton
            // 
            this.RDownButton.Appearance = System.Windows.Forms.Appearance.Button;
            this.RDownButton.BackColor = System.Drawing.Color.Transparent;
            this.RDownButton.FlatAppearance.BorderSize = 0;
            this.RDownButton.FlatAppearance.CheckedBackColor = System.Drawing.Color.Blue;
            this.RDownButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Blue;
            this.RDownButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.RDownButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.RDownButton.ForeColor = System.Drawing.Color.Transparent;
            this.RDownButton.Location = new System.Drawing.Point( 304, 231 );
            this.RDownButton.Name = "RDownButton";
            this.RDownButton.Size = new System.Drawing.Size( 33, 31 );
            this.RDownButton.TabIndex = 16;
            this.RDownButton.Tag = "RDOWN";
            this.RDownButton.UseVisualStyleBackColor = false;
            this.RDownButton.MouseDown += new System.Windows.Forms.MouseEventHandler( this.DigitalButton_MouseDown );
            this.RDownButton.KeyUp += new System.Windows.Forms.KeyEventHandler( this.Button_KeyUp );
            this.RDownButton.CheckedChanged += new System.EventHandler( this.DigitalButton_CheckedChanged );
            this.RDownButton.MouseUp += new System.Windows.Forms.MouseEventHandler( this.DigitalButton_MouseUp );
            this.RDownButton.KeyDown += new System.Windows.Forms.KeyEventHandler( this.Button_KeyDown );
            // 
            // LeftAnalogStickButton
            // 
            this.LeftAnalogStickButton.Appearance = System.Windows.Forms.Appearance.Button;
            this.LeftAnalogStickButton.BackColor = System.Drawing.Color.Transparent;
            this.LeftAnalogStickButton.FlatAppearance.BorderSize = 0;
            this.LeftAnalogStickButton.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.LeftAnalogStickButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.LeftAnalogStickButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.LeftAnalogStickButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LeftAnalogStickButton.ForeColor = System.Drawing.Color.Transparent;
            this.LeftAnalogStickButton.Location = new System.Drawing.Point( 113, 241 );
            this.LeftAnalogStickButton.Name = "LeftAnalogStickButton";
            this.LeftAnalogStickButton.Size = new System.Drawing.Size( 58, 58 );
            this.LeftAnalogStickButton.TabIndex = 9;
            this.LeftAnalogStickButton.Tag = "LEFT_STICK";
            this.LeftAnalogStickButton.UseVisualStyleBackColor = false;
            this.LeftAnalogStickButton.MouseDown += new System.Windows.Forms.MouseEventHandler( this.AnalogStick_MouseDown );
            this.LeftAnalogStickButton.MouseMove += new System.Windows.Forms.MouseEventHandler( this.AnalogStick_MouseMove );
            this.LeftAnalogStickButton.KeyUp += new System.Windows.Forms.KeyEventHandler( this.Button_KeyUp );
            this.LeftAnalogStickButton.Paint += new System.Windows.Forms.PaintEventHandler( this.AnalogStick_Paint );
            this.LeftAnalogStickButton.MouseUp += new System.Windows.Forms.MouseEventHandler( this.AnalogStick_MouseUp );
            this.LeftAnalogStickButton.KeyDown += new System.Windows.Forms.KeyEventHandler( this.Button_KeyDown );
            // 
            // RightAnalogStickButton
            // 
            this.RightAnalogStickButton.Appearance = System.Windows.Forms.Appearance.Button;
            this.RightAnalogStickButton.BackColor = System.Drawing.Color.Transparent;
            this.RightAnalogStickButton.FlatAppearance.BorderSize = 0;
            this.RightAnalogStickButton.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.RightAnalogStickButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.RightAnalogStickButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.RightAnalogStickButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.RightAnalogStickButton.ForeColor = System.Drawing.Color.Transparent;
            this.RightAnalogStickButton.Location = new System.Drawing.Point( 234, 242 );
            this.RightAnalogStickButton.Name = "RightAnalogStickButton";
            this.RightAnalogStickButton.Size = new System.Drawing.Size( 58, 58 );
            this.RightAnalogStickButton.TabIndex = 12;
            this.RightAnalogStickButton.Tag = "RIGHT_STICK";
            this.RightAnalogStickButton.UseVisualStyleBackColor = false;
            this.RightAnalogStickButton.MouseDown += new System.Windows.Forms.MouseEventHandler( this.AnalogStick_MouseDown );
            this.RightAnalogStickButton.MouseMove += new System.Windows.Forms.MouseEventHandler( this.AnalogStick_MouseMove );
            this.RightAnalogStickButton.KeyUp += new System.Windows.Forms.KeyEventHandler( this.Button_KeyUp );
            this.RightAnalogStickButton.Paint += new System.Windows.Forms.PaintEventHandler( this.AnalogStick_Paint );
            this.RightAnalogStickButton.MouseUp += new System.Windows.Forms.MouseEventHandler( this.AnalogStick_MouseUp );
            this.RightAnalogStickButton.KeyDown += new System.Windows.Forms.KeyEventHandler( this.Button_KeyDown );
            // 
            // titleLabel
            // 
            this.titleLabel.AutoSize = true;
            this.titleLabel.BackColor = System.Drawing.Color.Transparent;
            this.titleLabel.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)) );
            this.titleLabel.Location = new System.Drawing.Point( 181, 0 );
            this.titleLabel.Name = "titleLabel";
            this.titleLabel.Size = new System.Drawing.Size( 40, 13 );
            this.titleLabel.TabIndex = 0;
            this.titleLabel.Text = "Pad 0";
            this.titleLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // L3Button
            // 
            this.L3Button.Appearance = System.Windows.Forms.Appearance.Button;
            this.L3Button.BackColor = System.Drawing.Color.Transparent;
            this.L3Button.FlatAppearance.CheckedBackColor = System.Drawing.Color.Gray;
            this.L3Button.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Gray;
            this.L3Button.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.L3Button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.L3Button.ForeColor = System.Drawing.Color.White;
            this.L3Button.Location = new System.Drawing.Point( 76, 277 );
            this.L3Button.Name = "L3Button";
            this.L3Button.Size = new System.Drawing.Size( 31, 23 );
            this.L3Button.TabIndex = 17;
            this.L3Button.Tag = "L3";
            this.L3Button.Text = "L3";
            this.L3Button.UseVisualStyleBackColor = false;
            this.L3Button.MouseDown += new System.Windows.Forms.MouseEventHandler( this.DigitalButton_MouseDown );
            this.L3Button.KeyUp += new System.Windows.Forms.KeyEventHandler( this.Button_KeyUp );
            this.L3Button.CheckedChanged += new System.EventHandler( this.DigitalButton_CheckedChanged );
            this.L3Button.MouseUp += new System.Windows.Forms.MouseEventHandler( this.DigitalButton_MouseUp );
            this.L3Button.KeyDown += new System.Windows.Forms.KeyEventHandler( this.Button_KeyDown );
            // 
            // R3Button
            // 
            this.R3Button.Appearance = System.Windows.Forms.Appearance.Button;
            this.R3Button.BackColor = System.Drawing.Color.Transparent;
            this.R3Button.FlatAppearance.CheckedBackColor = System.Drawing.Color.Gray;
            this.R3Button.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Gray;
            this.R3Button.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.R3Button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.R3Button.ForeColor = System.Drawing.Color.White;
            this.R3Button.Location = new System.Drawing.Point( 298, 277 );
            this.R3Button.Name = "R3Button";
            this.R3Button.Size = new System.Drawing.Size( 31, 23 );
            this.R3Button.TabIndex = 18;
            this.R3Button.Tag = "R3";
            this.R3Button.Text = "R3";
            this.R3Button.UseVisualStyleBackColor = false;
            this.R3Button.MouseDown += new System.Windows.Forms.MouseEventHandler( this.DigitalButton_MouseDown );
            this.R3Button.KeyUp += new System.Windows.Forms.KeyEventHandler( this.Button_KeyUp );
            this.R3Button.CheckedChanged += new System.EventHandler( this.DigitalButton_CheckedChanged );
            this.R3Button.MouseUp += new System.Windows.Forms.MouseEventHandler( this.DigitalButton_MouseUp );
            this.R3Button.KeyDown += new System.Windows.Forms.KeyEventHandler( this.Button_KeyDown );
            // 
            // sendInputsCheckBox
            // 
            this.sendInputsCheckBox.Appearance = System.Windows.Forms.Appearance.Button;
            this.sendInputsCheckBox.AutoSize = true;
            this.sendInputsCheckBox.Checked = true;
            this.sendInputsCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.sendInputsCheckBox.ForeColor = System.Drawing.SystemColors.ControlText;
            this.sendInputsCheckBox.Location = new System.Drawing.Point( 124, 350 );
            this.sendInputsCheckBox.Name = "sendInputsCheckBox";
            this.sendInputsCheckBox.Size = new System.Drawing.Size( 74, 23 );
            this.sendInputsCheckBox.TabIndex = 19;
            this.sendInputsCheckBox.Text = "Send Inputs";
            this.sendInputsCheckBox.UseVisualStyleBackColor = true;
            this.sendInputsCheckBox.CheckedChanged += new System.EventHandler( this.sendInputsCheckBox_CheckedChanged );
            // 
            // clearButton
            // 
            this.clearButton.ForeColor = System.Drawing.SystemColors.ControlText;
            this.clearButton.Location = new System.Drawing.Point( 204, 350 );
            this.clearButton.Name = "clearButton";
            this.clearButton.Size = new System.Drawing.Size( 75, 23 );
            this.clearButton.TabIndex = 20;
            this.clearButton.Text = "Clear";
            this.clearButton.UseVisualStyleBackColor = true;
            this.clearButton.Click += new System.EventHandler( this.clearButton_Click );
            // 
            // ControlPS3Pad
            // 
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImage = global::ragInput.Properties.Resources.ps3;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.Controls.Add( this.clearButton );
            this.Controls.Add( this.sendInputsCheckBox );
            this.Controls.Add( this.R3Button );
            this.Controls.Add( this.L3Button );
            this.Controls.Add( this.titleLabel );
            this.Controls.Add( this.RightAnalogStickButton );
            this.Controls.Add( this.LeftAnalogStickButton );
            this.Controls.Add( this.RDownButton );
            this.Controls.Add( this.RRightButton );
            this.Controls.Add( this.RLeftButton );
            this.Controls.Add( this.RUpButton );
            this.Controls.Add( this.StartButton );
            this.Controls.Add( this.SelectButton );
            this.Controls.Add( this.LDownButton );
            this.Controls.Add( this.LRightButton );
            this.Controls.Add( this.LLeftButton );
            this.Controls.Add( this.LUpButton );
            this.Controls.Add( this.R1Button );
            this.Controls.Add( this.R2Button );
            this.Controls.Add( this.L2Button );
            this.Controls.Add( this.L1Button );
            this.DoubleBuffered = true;
            this.Name = "ControlPS3Pad";
            this.Size = new System.Drawing.Size( 403, 380 );
            this.Leave += new System.EventHandler( this.Control360Pad_Leave );
            this.ResumeLayout( false );
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox L1Button;
        private System.Windows.Forms.CheckBox L2Button;
        private System.Windows.Forms.CheckBox R2Button;
        private System.Windows.Forms.CheckBox R1Button;
        private System.Windows.Forms.CheckBox LUpButton;
        private System.Windows.Forms.CheckBox LLeftButton;
        private System.Windows.Forms.CheckBox LRightButton;
        private System.Windows.Forms.CheckBox LDownButton;
        private System.Windows.Forms.CheckBox SelectButton;
        private System.Windows.Forms.CheckBox StartButton;
        private System.Windows.Forms.CheckBox RUpButton;
        private System.Windows.Forms.CheckBox RLeftButton;
        private System.Windows.Forms.CheckBox RRightButton;
        private System.Windows.Forms.CheckBox RDownButton;
        private System.Windows.Forms.CheckBox LeftAnalogStickButton;
        private System.Windows.Forms.CheckBox RightAnalogStickButton;
        private System.Windows.Forms.Label titleLabel;
        private System.Windows.Forms.CheckBox L3Button;
        private System.Windows.Forms.CheckBox R3Button;
        private System.Windows.Forms.CheckBox sendInputsCheckBox;
        private System.Windows.Forms.Button clearButton;
    }
}
