using System;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Text;
using System.Xml;

using ragCore;

namespace ragWidgets
{
    /// <summary>
    /// Summary description for WidgetColor.
    /// </summary>
    public class WidgetColor : Widget
    {
        protected WidgetColor(BankPipe pipe, uint id, ColorType type, String title, String memo, Color fillColor, bool readOnly, float red, float green, float blue, float alpha)
            : base(pipe, id, title, memo, fillColor, readOnly)
        {
            m_Type = type;
            m_Red = red;
            m_Green = green;
            m_Blue = blue;
            m_Alpha = alpha;


            m_DefaultRed = m_Red;
            m_DefaultGreen = m_Green;
            m_DefaultBlue = m_Blue;
            m_DefaultAlpha = m_Alpha;
        }

        #region Enums
        public enum ColorType
        {
            VECTOR3 = 0,
            VECTOR4 = 1,
            COLOR32 = 2
        }
        #endregion

        #region Variables
        private ColorType m_Type;
        private float m_Red;
        private float m_Green;
        private float m_Blue;
        private float m_Alpha;
        private float m_DefaultRed;
        private float m_DefaultGreen;
        private float m_DefaultBlue;
        private float m_DefaultAlpha;
        #endregion

        #region Properties
        public ColorType Type
        {
            get { return m_Type; }
        }

        public byte R
        {
            get { return (byte)Math.Round(m_Red * 255.0f, MidpointRounding.AwayFromZero); }
        }

        public byte G
        {
			get { return (byte)Math.Round(m_Green * 255.0f, MidpointRounding.AwayFromZero); }
        }

        public byte B
        {
			get { return (byte)Math.Round(m_Blue * 255.0f, MidpointRounding.AwayFromZero); }
        }

        public byte A
        {
			get { return (byte)Math.Round(m_Alpha * 255.0f, MidpointRounding.AwayFromZero); }
        }
        #endregion

        #region Overrides
        public override int GetWidgetTypeGUID() { return GetStaticGUID(); }

        public override void Serialize( XmlTextWriter writer, String path )
        {
            if ( SerializeStart( writer, ref path ) )
            {
                writer.WriteAttributeString( "Red", m_Red.ToString() );
                writer.WriteAttributeString( "Green", m_Green.ToString() );
                writer.WriteAttributeString( "Blue", m_Blue.ToString() );
                writer.WriteAttributeString( "Alpha", m_Alpha.ToString() );
                writer.WriteEndElement();
            }
        }

        public override void DeSerialize( XmlTextReader reader )
        {
            m_Red = Convert.ToSingle( reader["Red"] );
            m_Green = Convert.ToSingle( reader["Green"] );
            m_Blue = Convert.ToSingle( reader["Blue"] );
            m_Alpha = Convert.ToSingle( reader["Alpha"] );

            DeSerializeShared();
        }

        public override bool AllowPersist { get { return true; } }

        public override bool SerializeToJSON( LitJson.JsonData data )
        {
            data["red"] = m_Red;
            data["green"] = m_Green;
            data["blue"] = m_Blue;
            data["alpha"] = m_Alpha;

            if ( IsModified() )
            {
                data["default"] = string.Format("rgba = {0}, {1}, {2}, {3}", m_DefaultRed, m_DefaultGreen, m_DefaultBlue, m_DefaultAlpha);
            }

            return true;
        }

        public override void DeSerializeFromJSON( LitJson.JsonData data )
        {
            float red = (float)(Double)data["red"];
            float green = (float)(Double)data["green"];
            float blue = (float)(Double)data["blue"];
            float alpha = (float)(Double)data["alpha"];

            SetColor( red, green, blue, alpha );
        }

        public override string ProcessCommand( string[] args )
        {
            if ( args.Length > 0 && args[0] == "-help" )
            {
                return "TYPE: " + this.GetType().ToString() + "\n" +
                    "ARGUMENTS: [red] [green] [blue] [alpha]\n\n" +
                    "\t[red]     the red channel (must be between 0.0f-1.0f)\n" +
                    "\t[green]   the green channel (must be between 0.0f-1.0f)\n" +
                    "\t[blue]    the blue channel (must be between 0.0f-1.0f)\n" +
                    "\t[alpha]   the alpha channel (must be between 0.0f-1.0f)\n";
            }

            if ( args.Length == 0 )
            {
                return String.Format( "{0:r} {1:r} {2:r} {3:r}", m_Red, m_Green, m_Blue, m_Alpha );
            }

            if ( args.Length < 1 || args.Length > 4 )
                return "Incorrect # of arguments: You need to pass in 1-4 channels in this order: RED GREEN BLUE ALPHA";

            float[] colors = new float[4] { m_Red, m_Green, m_Blue, m_Alpha };

            // convert string to floats:
            try
            {
                for ( int i = 0; i < args.Length; i++ )
                {
                    colors[i] = Convert.ToSingle( args[i] );
                    if ( colors[i] < 0.0f || colors[i] > 1.0f )
                    {
                        return "Color channel must be a floating point value between 0.0-1.0";
                    }
                }
            }
            catch ( System.Exception e )
            {
                return e.Message;
            }

            // set the color:
            try
            {
                SetColor( colors[0], colors[1], colors[2], colors[3] );
            }
            catch ( System.Exception e )
            {
                return e.Message;
            }

            return "";
        }

        public override void ReceiveData(byte[] bytes)
        {
            int argCount = bytes.Length / sizeof(float);
            if (argCount < 1 || argCount > 4)
                return; 

            float[] colors = new float[4] { m_Red, m_Green, m_Blue, m_Alpha };

            try
            {
                int offset = 0;
                for (int i = 0; i < argCount; i++)
                {
                    float newColor = BitConverter.ToSingle(bytes, offset);
                    offset += sizeof(float);
                    if (colors[i] >= 0.0f && colors[i] <= 1.0f)
                        colors[i] = newColor;
                }
            }
            catch (System.Exception)
            {
                return;
            }

            // set the color:
            try
            {
                SetColor(colors[0], colors[1], colors[2], colors[3]);
            }
            catch (System.Exception) { }
        }

        public override void SendData(Stream stream)
        {
            int dataSize = 4 * sizeof(float);
            byte[] buffer = BitConverter.GetBytes(dataSize);
            stream.Write(buffer, 0, buffer.Length);

            buffer = BitConverter.GetBytes(m_Red);
            stream.Write(buffer, 0, buffer.Length);

            buffer = BitConverter.GetBytes(m_Green);
            stream.Write(buffer, 0, buffer.Length);
            
            buffer = BitConverter.GetBytes(m_Blue);
            stream.Write(buffer, 0, buffer.Length);

            buffer = BitConverter.GetBytes(m_Alpha);
            stream.Write(buffer, 0, buffer.Length);
        }

        public override void UpdateRemote()
        {
            BankRemotePacket p = new BankRemotePacket( Pipe );
            p.Begin(BankRemotePacket.EnumPacketType.CHANGED, GetStaticGUID(), Id);
            p.Write_float( m_Red );
            p.Write_float( m_Green );
            p.Write_float( m_Blue );
            p.Write_float( m_Alpha );
            p.Send();
        }

        public override bool IsModified()
        {
            return m_Alpha != m_DefaultAlpha ||
                m_Red != m_DefaultRed ||
                m_Green != m_DefaultGreen ||
                m_Blue != m_DefaultBlue;
        }

        public override void ResetToDefault()
        {
            SetColor( m_DefaultRed, m_DefaultGreen, m_DefaultBlue, m_DefaultAlpha );
        }


        #endregion

        #region Public Functions

        public void SetColor( float newRed, float newGreen, float newBlue, float newAlpha )
        {
            if ( m_Red != newRed || m_Green != newGreen || m_Blue != newBlue || m_Alpha != newAlpha )
            {
                m_Red = newRed;
                m_Green = newGreen;
                m_Blue = newBlue;
                m_Alpha = newAlpha;
                OnValueChanged();
                UpdateRemote();
            }
        }

        public void SetColor( System.Drawing.Color c )
        {
            float newRed   = (float)c.R / 255.0f;
            float newGreen = (float)c.G / 255.0f;
            float newBlue  = (float)c.B / 255.0f;
            float newAlpha = (float)c.A / 255.0f;
            SetColor( newRed, newGreen, newBlue, newAlpha );
        }
        #endregion

        #region Private Functions
        private void ReadColor( BankRemotePacket packet )
        {
            m_Red = packet.Read_float();
            m_Green = packet.Read_float();
            m_Blue = packet.Read_float();
            m_Alpha = packet.Read_float();

            OnValueChanged();
        }
        #endregion

        #region Static Functions
        public static int GetStaticGUID() { return ComputeGUID( 'v', 'c', 'l', 'r' ); }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="packet"></param>
        public static bool RemoteHandler(BankRemotePacket packet, out String errorMessage)
        {
            bool handled = false;

            if ( packet.BankCommand == BankRemotePacket.EnumPacketType.CREATE )
            {
                packet.Begin();

                uint remoteParent = packet.Read_bkWidget();
                String title = packet.Read_const_char();
                String memo = packet.Read_const_char();
                Color fillColor = Widget.DeserializeColor(packet.Read_const_char());
                bool readOnly = packet.Read_bool();
                ColorType type = (ColorType)(packet.Read_u32());

                float red = packet.Read_float();
                float green = packet.Read_float();
                float blue = packet.Read_float();
                float alpha = packet.Read_float();

                packet.End();

                Widget parent;
                if (packet.BankPipe.RemoteToLocal(remoteParent, out parent, out errorMessage))
                {
                    WidgetColor newWidget = new WidgetColor(packet.BankPipe, packet.Id, type, title, memo, fillColor, readOnly, red, green, blue, alpha);
                    packet.BankPipe.Associate(packet.Id, newWidget);
                    parent.AddChild(newWidget);
                    handled = true;
                }    
            }
            else if ( packet.BankCommand == BankRemotePacket.EnumPacketType.CHANGED )
            {
                packet.Begin();
                
                WidgetColor widget;
                if (packet.BankPipe.RemoteToLocal(packet.Id, out widget, out errorMessage))
                {
                    widget.ReadColor(packet);
                    packet.End();
                    handled = true;
                }
            }
            else
            {
                handled = Widget.RemoteHandlerBase(packet, GetStaticGUID(), out errorMessage);
            }

            return handled;
        }

        public static void AddHandlers( BankRemotePacketProcessor packetProcessor )
        {
            packetProcessor.AddType( GetStaticGUID(), new BankRemotePacketProcessor.HandlerDel( RemoteHandler ) );
        }
        #endregion
    }
}
