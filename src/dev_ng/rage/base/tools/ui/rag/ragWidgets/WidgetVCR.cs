using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

using ragCore;
using ragWidgets;

namespace ragWidgets
{
    public class WidgetVCR : Widget
    {
        public WidgetVCR(BankPipe pipe, uint id, String title, String memo, Color fillColor, bool readOnly, uint style, EnabledButton enabledButtons)
            : base(pipe, id, title, memo, fillColor, readOnly)
        {
            m_vcrButtonStyle = style;
            m_enabledButtons = enabledButtons;
        }

        #region Enums
        /// <summary>
        /// Keep these in sync with base\src\cranimation\crVCR.h
        /// </summary>
        public enum EnabledButton
        {
            RewindOrGoToStart = 0x01,
            GoToPreviousOrStepBackward = 0x02,
            PlayBackwards = 0x04,
            Pause = 0x08,
            PlayForwards = 0x10,
            GoToNextOrStepForward = 0x20,
            FastForwardOrGoToEnd = 0x40,

            All = 0x7f,
        }
        #endregion

        #region Variables
        private uint m_vcrButtonStyle = 0;
        private EnabledButton m_enabledButtons = EnabledButton.All;
        #endregion

        #region Properties
        public uint VCRButtonStyle
        {
            get
            {
                return m_vcrButtonStyle;
            }
            set
            {
                if ( this.VCRButtonStyle != value )
                {
                    m_vcrButtonStyle = value;

                    OnValueChanged();
                }
            }
        }

        public bool CanRewindOrGoToStart
        {
            get
            {
                return (m_enabledButtons & EnabledButton.RewindOrGoToStart) != 0;
            }
            set
            {
                if ( this.CanRewindOrGoToStart != value )
                {
                    if ( value )
                    {
                        m_enabledButtons &= EnabledButton.RewindOrGoToStart;
                    }
                    else
                    {
                        m_enabledButtons &= ~EnabledButton.RewindOrGoToStart;
                    }

                    OnValueChanged();
                }
            }
        }

        public bool CanGoToPreviousOrStepBackward
        {
            get
            {
                return (m_enabledButtons & EnabledButton.GoToPreviousOrStepBackward) != 0;
            }
            set
            {
                if ( this.CanGoToPreviousOrStepBackward != value )
                {
                    if ( value )
                    {
                        m_enabledButtons &= EnabledButton.GoToPreviousOrStepBackward;
                    }
                    else
                    {
                        m_enabledButtons &= ~EnabledButton.GoToPreviousOrStepBackward;
                    }

                    OnValueChanged();
                }
            }
        }

        public bool CanPlayBackwards
        {
            get
            {
                return (m_enabledButtons & EnabledButton.PlayBackwards) != 0;
            }
            set
            {
                if ( this.CanPlayBackwards != value )
                {
                    if ( value )
                    {
                        m_enabledButtons &= EnabledButton.PlayBackwards;
                    }
                    else
                    {
                        m_enabledButtons &= ~EnabledButton.PlayBackwards;
                    }

                    OnValueChanged();
                }
            }
        }

        public bool CanPause
        {
            get
            {
                return (m_enabledButtons & EnabledButton.Pause) != 0;
            }
            set
            {
                if ( this.CanPause != value )
                {
                    if ( value )
                    {
                        m_enabledButtons &= EnabledButton.Pause;
                    }
                    else
                    {
                        m_enabledButtons &= ~EnabledButton.Pause;
                    }

                    OnValueChanged();
                }
            }
        }

        public bool CanPlayForwards
        {
            get
            {
                return (m_enabledButtons & EnabledButton.PlayForwards) != 0;
            }
            set
            {
                if ( this.CanPlayForwards != value )
                {
                    if ( value )
                    {
                        m_enabledButtons &= EnabledButton.PlayForwards;
                    }
                    else
                    {
                        m_enabledButtons &= ~EnabledButton.PlayForwards;
                    }

                    OnValueChanged();
                }
            }
        }

        public bool CanGoToNextOrStepForward
        {
            get
            {
                return (m_enabledButtons & EnabledButton.GoToNextOrStepForward) != 0;
            }
            set
            {
                if ( this.CanGoToNextOrStepForward != value )
                {
                    if ( value )
                    {
                        m_enabledButtons &= EnabledButton.GoToNextOrStepForward;
                    }
                    else
                    {
                        m_enabledButtons &= ~EnabledButton.GoToNextOrStepForward;
                    }

                    OnValueChanged();
                }
            }
        }

        public bool CanFastForwardOrGoToEnd
        {
            get
            {
                return (m_enabledButtons & EnabledButton.FastForwardOrGoToEnd) != 0;
            }
            set
            {
                if ( this.CanFastForwardOrGoToEnd != value )
                {
                    if ( value )
                    {
                        m_enabledButtons &= EnabledButton.FastForwardOrGoToEnd;
                    }
                    else
                    {
                        m_enabledButtons &= ~EnabledButton.FastForwardOrGoToEnd;
                    }

                    OnValueChanged();
                }
            }
        }
        #endregion

        #region Overrides
        public override bool AllowPersist
        {
            get
            {
                return false;
            }
        }

        public override int GetWidgetTypeGUID()
        {
            return WidgetVCR.GetStaticGUID();
        }

        public override void UpdateRemote()
        {
            // do nothing
        }


        public override string ProcessCommand(string[] args)
        {
            if (args.Length > 0 && args[0] == "-help")
            {
                return "TYPE: " + this.GetType().ToString() + "\n" +
                    "ARGUMENTS: [play|stop|pause|rewind|fastforward|stepforwards|stepbackwards]  the action on the VCR widget\n\n";
            }

            if (args.Length != 1)
            {
                return "Incorrect number of arguments for VCR widget.";
            }

            string state = args[0].ToLower();
            switch (state)
            {
                case "rewind":
                    RewindOrGoToStart();
                    break;
                case "stepbackwards":
                    GoToPreviousOrStepBackward();
                    break;
                case "playbackwards":
                    PlayBackwards();
                    break;
                case "pause":
                    Pause();
                    break;
                case "playforwards":
                    PlayForwards();
                    break;
                case "stepforwards":
                    GoToNextOrStepForward();
                    break;
                case "fastforward":
                    FastForwardOrGoToEnd();
                    break;
            }

            return "";
        }

        #endregion

        #region Public Functions
        public void RewindOrGoToStart()
        {
            BankRemotePacket p = new BankRemotePacket( this.Pipe );
            p.Begin(BankRemotePacket.EnumPacketType.USER_0, GetWidgetTypeGUID(), Id);
            p.Send();
        }

        public void GoToPreviousOrStepBackward()
        {
            BankRemotePacket p = new BankRemotePacket( this.Pipe );
            p.Begin(BankRemotePacket.EnumPacketType.USER_1, GetWidgetTypeGUID(), Id);
            p.Send();
        }

        public void PlayBackwards()
        {
            BankRemotePacket p = new BankRemotePacket( this.Pipe );
            p.Begin(BankRemotePacket.EnumPacketType.USER_2, GetWidgetTypeGUID(), Id);
            p.Send();
        }

        public void Pause()
        {
            BankRemotePacket p = new BankRemotePacket( this.Pipe );
            p.Begin(BankRemotePacket.EnumPacketType.USER_3, GetWidgetTypeGUID(), Id);
            p.Send();
        }

        public void PlayForwards()
        {
            BankRemotePacket p = new BankRemotePacket( this.Pipe );
            p.Begin(BankRemotePacket.EnumPacketType.USER_4, GetWidgetTypeGUID(), Id);
            p.Send();
        }

        public void GoToNextOrStepForward()
        {
            BankRemotePacket p = new BankRemotePacket( this.Pipe );
            p.Begin(BankRemotePacket.EnumPacketType.USER_5, GetWidgetTypeGUID(), Id);
            p.Send();
        }

        public void FastForwardOrGoToEnd()
        {
            BankRemotePacket p = new BankRemotePacket( this.Pipe );
            p.Begin(BankRemotePacket.EnumPacketType.USER_6, GetWidgetTypeGUID(), Id);
            p.Send();
        }
        #endregion

        #region Static Functions
        public static void AddHandlers( BankRemotePacketProcessor packetProcessor )
        {
            packetProcessor.AddType( GetStaticGUID(), new BankRemotePacketProcessor.HandlerDel( RemoteHandler ) );
        }

        public static int GetStaticGUID()
        {
            return ComputeGUID( 'a', 'v', 'c', 'r' );
        }

        public static bool RemoteHandler( BankRemotePacket packet, out String errorMessage )
        {
            bool handled = false;

            switch ( packet.BankCommand )
            {
                case BankRemotePacket.EnumPacketType.CREATE:
                    {
                        packet.Begin();

                        uint remoteParent = packet.Read_bkWidget();
                        String title = packet.Read_const_char();
                        String memo = packet.Read_const_char();
                        Color fillColor = Widget.DeserializeColor(packet.Read_const_char());
                        bool readOnly = packet.Read_bool();
                        uint vcrButtonStyle = packet.Read_u32();
                        uint enabledButtons = packet.Read_u32();

                        packet.End();
                        
                        Widget parent;
                        if (packet.BankPipe.RemoteToLocal(remoteParent, out parent, out errorMessage))
                        {
                            WidgetVCR local = new WidgetVCR(packet.BankPipe, packet.Id, title, memo, fillColor, readOnly,
                                vcrButtonStyle, (EnabledButton)enabledButtons);

                            packet.BankPipe.Associate(packet.Id, local);
                            parent.AddChild(local);
                            handled = true;
                        }
                    }
                    break;
                case BankRemotePacket.EnumPacketType.CHANGED:
                    {
                        packet.Begin();

                        uint enabledButtons = packet.Read_u32();

                        packet.End();


				        // find the local version of the the remote widget:
				        WidgetVCR vcr;
                        if (packet.BankPipe.RemoteToLocal(packet.Id, out vcr, out errorMessage))
                        {
                            vcr.m_enabledButtons = (EnabledButton)enabledButtons;
                            vcr.OnValueChanged();
                            handled = true;
                        }
                    }
                    break;
                case BankRemotePacket.EnumPacketType.USER_0:
                case BankRemotePacket.EnumPacketType.USER_1:
                case BankRemotePacket.EnumPacketType.USER_2:
                case BankRemotePacket.EnumPacketType.USER_3:
                case BankRemotePacket.EnumPacketType.USER_4:
                case BankRemotePacket.EnumPacketType.USER_5:
                case BankRemotePacket.EnumPacketType.USER_6:
                    handled = true;
                    errorMessage = null;
                    break;
                default:
                    {
                        handled = Widget.RemoteHandlerBase(packet, GetStaticGUID(), out errorMessage);
                    }
                    break;
            }

            return handled;
        }
        #endregion
    }
}
