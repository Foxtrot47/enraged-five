using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Text;
using System.Xml;

using ragCore;


namespace ragWidgets
{
    abstract public class WidgetMatrix : Widget
    {
        protected WidgetMatrix(BankPipe pipe, uint id, String title, String memo, float[][] matrix, EnumType enumType, float min, float max, float step, Color fillColor, bool readOnly)
            : base(pipe, id, title, memo, fillColor, readOnly)
        {
            m_matrix = matrix;
            m_DefaultMatrix = (float[][])matrix.Clone();
            m_enumType = enumType;
            m_minimum = min;
            m_maximum = max;
            m_step = step;
        }

        #region Enums
        protected enum EnumType
        {
            Matrix33,
            Matrix34,
            Matrix44,
        };

        public enum Operation
        {
            Identity,
            Identity3x3,
            Zero,
            Zero3x3,
            SetDiagonal,
            Add,
            Add3x3,
            Subtract,
            Subtract3x3, 
            Abs,
            Negate,
            Negate3x3,            
            Dot,
            Dot3x3,
            DotFromLeft,
            Dot3x3FromLeft,
            DotTranspose,
            Dot3x3Transpose,
            CrossProduct,
            OuterProduct,
            Rotate,
            RotateX,
            RotateY,
            RotateZ,
            RotateUnitAxis,
            RotateLocalX,
            RotateLocalY,
            RotateLocalZ,
            RotateLocalAxis,
            MakeRotate,
            MakeRotateX,
            MakeRotateY,
            MakeRotateZ,
            MakeRotateUnitAxis,
            RotateTo,
            MakeRotateTo,
            MakeUpright,
            Scale,
            MakeScale,
            Inverse,
            Inverse3x3,
            FastInverse,
            Transpose,
            Transpose3x4,
            CoordinateInverseSafe,
            DotCrossProdMtx,
            Dot3x3CrossProdMtx,
            DotCrossProdTranspose,
            Dot3x3CrossProdTranspose,
            MakeDoubleCrossMatrix,
            Normalize,
            NormalizeSafe,
            MirrorOnPlane,                                    
            RotateFull,
            RotateFullX,
            RotateFullY,
            RotateFullZ,
            RotateFullUnitAxis,
            ScaleFull,
            Translate,
            MakeTranslate,                
            LookDown,
            LookAt,
            PolarView,
            Interpolate,
            MakeScaleFull,
            MakePos,
            MakePosRotY,
            MakeRotX,
            MakeRotY,
            MakeRotZ,
            MakeReflect
        };
        #endregion

        #region Overrides
        public override void Serialize( XmlTextWriter writer, String path )
        {
            if ( SerializeStart( writer, ref path ) )
            {
                StringBuilder strMatrix = new StringBuilder();
                for ( int i = 0; i < this.NumRows; ++i )
                {
                    for ( int j = 0; j < this.NumColumns; ++j )
                    {
                        strMatrix.Append( m_matrix[i][j] );

                        if ( (i != this.NumRows - 1) && (j != this.NumColumns - 1) )
                        {
                            strMatrix.Append( ',' );
                        }
                    }
                }

                writer.WriteAttributeString( "matrix", strMatrix.ToString() );

                writer.WriteEndElement();
            }
        }

        public override void DeSerialize( XmlTextReader reader )
        {
            string strMatrix = reader["matrix"];
            if ( strMatrix != null )
            {
                string[] split = strMatrix.Split( new char[] { ',' } );
                if ( split.Length == this.NumRows * this.NumColumns )
                {
                    for ( int i = 0; i < this.NumRows; ++i )
                    {
                        for ( int j = 0; j < this.NumColumns; ++j )
                        {
                            m_matrix[i][j] = float.Parse( split[(this.NumColumns * i) + j] );
                        }
                    }
                }
            }

            DeSerializeShared();
        }

        public override bool AllowPersist { get { return true; } }

        public override void UpdateRemote()
        {
            if ( !m_updateRemote )
            {
                return;
            }

            BankRemotePacket p = new BankRemotePacket( Pipe );
            p.Begin(BankRemotePacket.EnumPacketType.CHANGED, this.GetWidgetTypeGUID(), Id);

            for ( int i = 0; i < this.NumRows; ++i )
            {
                for ( int j = 0; j < this.NumColumns; ++j )
                {
                    p.Write_float( m_matrix[i][j] );
                }
            }

            p.Send();
        }

        public override string ProcessCommand(string[] args)
        {
            if (args.Length > 0 && args[0] == "-help")
            {
                switch (m_enumType)
                {
                    case EnumType.Matrix33:
                        return "TYPE: " + this.GetType().ToString() + "\n" +
                            "ARGUMENTS: ax ay az bx by bz cx cy cz \n\n" +
                            "\twhere ax..cz are the new floating point values for the matrix";
                    case EnumType.Matrix34:
                        return "TYPE: " + this.GetType().ToString() + "\n" +
                            "ARGUMENTS: ax ay az bx by bz cx cy cz dx dy dz\n\n" +
                            "\twhere ax..dz are the new floating point values for the matrix";
                    case EnumType.Matrix44:
                        return "TYPE: " + this.GetType().ToString() + "\n" +
                            "ARGUMENTS: ax ay az aw bx by bz bw cx cy cz cw dx dy dz dw\n\n" +
                            "\twhere ax..dw are the new floating point values for the matrix";
                }
            }

            if (args.Length == 0)
            {
                string s = "";
                for (int i = 0; i < NumRows; i++)
                {
                    for (int j = 0; j < NumColumns; j++)
                    {
                        s += String.Format("{0:r} ", Value[i][j]);
                    }
                    s += "\n";
                }
                return s;
            }

            int reqValues = NumRows * NumColumns;
            if (args.Length != reqValues)
            {
                return String.Format("Incorrect # of arguments: {0} required", reqValues);
            }

            float[][] newVals = new float[4][];
            for (int i = 0; i < 4; i++)
            {
                newVals[i] = new float[4];
            }

            for (int i = 0; i < NumRows; i++)
            {
                for (int j = 0; j < NumColumns; j++)
                {
                    float newVal = Convert.ToSingle(args[i * NumColumns + j]);
                    if (newVal < m_minimum || newVal > m_maximum)
                    {
                        return String.Format("Value {0} is out of range ({1}, {2})", newVal, m_minimum, m_maximum);
                    }
                    newVals[i][j] = newVal;
                }
            }

            Value = newVals;
            return "";
        }

        public override void ReceiveData(byte[] bytes)
        {
            int argCount = bytes.Length / sizeof(float);
            int reqValues = NumRows * NumColumns;
            if (argCount != reqValues)
            {
                return;
            }

            float[][] newVals = new float[4][];
            for (int i = 0; i < 4; i++)
            {
                newVals[i] = new float[4];
            }

            int offset = 0;
            for (int i = 0; i < NumRows; i++)
            {
                for (int j = 0; j < NumColumns; j++)
                {
                    float newVal = BitConverter.ToSingle(bytes, offset);
                    offset += sizeof(float);
                    if (newVal < m_minimum || newVal > m_maximum)
                    {
                        return;
                    }
                    newVals[i][j] = newVal;
                }
            }

            Value = newVals;
        }

        public override void SendData(Stream stream)
        {
            int dataSize = NumColumns * NumRows * sizeof(float);
            byte[] buffer = BitConverter.GetBytes(dataSize);
            stream.Write(buffer, 0, buffer.Length);

            for (int i = 0; i < NumRows; i++)
            {
                for (int j = 0; j < NumColumns; j++)
                {
                    buffer = BitConverter.GetBytes(Value[i][j]);
                    stream.Write(buffer, 0, buffer.Length);
                }
            }
        }

        public override bool IsModified()
        {
            for (int i = 0; i < m_matrix.Length; i++)
            {
                for (int j = 0; j < m_matrix[i].Length; j++)
                {
                    if (m_matrix[i][j] != m_DefaultMatrix[i][j])
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public override void ResetToDefault()
        {
            Value = (float[][])m_DefaultMatrix.Clone();
        }
            

        #endregion

        #region Variables
        protected float[][] m_matrix = new float[4][];
        protected float[][] m_DefaultMatrix = new float[4][];
        protected EnumType m_enumType = EnumType.Matrix44;
        protected float m_minimum = float.MinValue;
        protected float m_maximum = float.MaxValue;
        protected float m_step = 1.0f;
        private bool m_updateRemote = true;
        #endregion

        #region Properties
        public float[][] Value
        {
            get
            {
                return m_matrix;
            }
            set
            {
                if ( m_matrix != value )
                {
                    m_matrix = value;

                    UpdateRemote();
                    OnValueChanged();
                }
            }
        }

        public int NumRows
        {
            get
            {
                switch ( m_enumType )
                {
                    case EnumType.Matrix33:
                        return 3;
                    case EnumType.Matrix34:
                        return 4;
                    case EnumType.Matrix44:
                        return 4;
                }

                return 0;
            }
        }

        public int NumColumns
        {
            get
            {
                switch ( m_enumType )
                {
                    case EnumType.Matrix33:
                        return 3;
                    case EnumType.Matrix34:
                        return 3;
                    case EnumType.Matrix44:
                        return 4;
                }

                return 0;
            }
        }

        public float Minimum
        {
            get
            {
                return m_minimum;
            }
            set
            {
                m_minimum = value;
            }
        }

        public float Maximum
        {
            get
            {
                return m_maximum;
            }
            set
            {
                m_maximum = value;
            }
        }

        public float Step
        {
            get
            {
                return m_step;
            }
            set
            {
                m_step = value;
            }
        }
        #endregion

        #region Event Handlers

        public void ZeroClickEH( object sender, EventArgs e )
        {
            BankRemotePacket p = new BankRemotePacket( Pipe );
            p.Begin(BankRemotePacket.EnumPacketType.USER_0, this.GetWidgetTypeGUID(), Id);

            p.Write_s32( (int)Operation.Zero );

            p.Send();
        }

        public void Zero3x3ClickEH( object sender, EventArgs e )
        {
            BankRemotePacket p = new BankRemotePacket( Pipe );
            p.Begin(BankRemotePacket.EnumPacketType.USER_0, this.GetWidgetTypeGUID(), Id);

            p.Write_s32( (int)Operation.Zero3x3 );

            p.Send();
        }

        public void TransposeClickEH( object sender, EventArgs e )
        {
            BankRemotePacket p = new BankRemotePacket( Pipe );
            p.Begin(BankRemotePacket.EnumPacketType.USER_0, this.GetWidgetTypeGUID(), Id);

            p.Write_s32( (int)Operation.Transpose );

            p.Send();
        }

        public void Transpose3x4ClickEH( object sender, EventArgs e )
        {
            BankRemotePacket p = new BankRemotePacket( Pipe );
            p.Begin(BankRemotePacket.EnumPacketType.USER_0, this.GetWidgetTypeGUID(), Id);

            p.Write_s32( (int)Operation.Transpose3x4 );

            p.Send();
        }

        public void UpdateRemoteOperation( BankRemotePacket.EnumPacketType enumType, Operation op, float floatValue, int intValue )
        {
            BankRemotePacket p = new BankRemotePacket( this.Pipe );
            p.Begin(enumType, this.GetWidgetTypeGUID(), Id);

            p.Write_s32( (int)op );
            p.Write_float( floatValue );
            p.Write_s32( intValue );

            p.Send();
        }

        public void UpdateRemoteOperation( BankRemotePacket.EnumPacketType enumType, Operation op, float[] value )
        {
            BankRemotePacket p = new BankRemotePacket( this.Pipe );
            p.Begin(enumType, this.GetWidgetTypeGUID(), Id);

            p.Write_s32( (int)op );
            WriteVector( p, value );

            p.Send();
        }

        public void UpdateRemoteOperation( BankRemotePacket.EnumPacketType enumType, Operation op, float[] vectorValue, float floatValue )
        {
            BankRemotePacket p = new BankRemotePacket( this.Pipe );
            p.Begin(enumType, this.GetWidgetTypeGUID(), Id);

            p.Write_s32( (int)op );
            WriteVector( p, vectorValue );
            p.Write_float( floatValue );

            p.Send();
        }


        public void UpdateRemoteOperation( BankRemotePacket.EnumPacketType enumType, Operation op, float floatValue, float[] vectorValue )
        {
            BankRemotePacket p = new BankRemotePacket( this.Pipe );
            p.Begin(enumType, this.GetWidgetTypeGUID(), Id);

            p.Write_s32( (int)op );
            p.Write_float( floatValue );
            WriteVector( p, vectorValue );

            p.Send();
        }


        public void UpdateRemoteOperation( BankRemotePacket.EnumPacketType enumType, Operation op, float[] vectorValue1, float[] vectorValue2 )
        {
            BankRemotePacket p = new BankRemotePacket( this.Pipe );
            p.Begin(enumType, this.GetWidgetTypeGUID(), Id);

            p.Write_s32( (int)op );
            WriteVector( p, vectorValue1 );
            WriteVector( p, vectorValue2 );

            p.Send();
        }



        public void UpdateRemoteOperation( BankRemotePacket.EnumPacketType enumType, Operation op, float[] vectorValue1, float[] vectorValue2, float floatValue )
        {
            BankRemotePacket p = new BankRemotePacket( this.Pipe );
            p.Begin(enumType, this.GetWidgetTypeGUID(), Id);

            p.Write_s32( (int)op );
            WriteVector( p, vectorValue1 );
            WriteVector( p, vectorValue2 );
            p.Write_float( floatValue );

            p.Send();
        }



        public void UpdateRemoteOperation( BankRemotePacket.EnumPacketType enumType, Operation op, float[][] matrixValue, float floatValue )
        {
            BankRemotePacket p = new BankRemotePacket( this.Pipe );
            p.Begin(enumType, this.GetWidgetTypeGUID(), Id);

            p.Write_s32( (int)op );
            WriteMatrix( p, matrixValue );
            p.Write_float( floatValue );

            p.Send();
        }

        public void UpdateRemoteOperation( BankRemotePacket.EnumPacketType enumType, Operation op, float[][] value )
        {
            BankRemotePacket p = new BankRemotePacket( this.Pipe );
            p.Begin(enumType, this.GetWidgetTypeGUID(), Id);

            p.Write_s32( (int)op );
            WriteMatrix( p, value );

            p.Send();
        }

        public void UpdateRemoteOperationDot3x3( BankRemotePacket.EnumPacketType enumType, Operation op, float[][] value )
        {
            BankRemotePacket p = new BankRemotePacket( this.Pipe );
            p.Begin(enumType, this.GetWidgetTypeGUID(), Id);

            p.Write_s32( (int)op );
            for ( int i = 0; i < 4; ++i )
            {
                for ( int j = 0; j < 3; ++j )
                {
                    p.Write_float( value[i][j] );
                }
            }

            p.Send();
        }

        public void UpdateRemoteOperation( BankRemotePacket.EnumPacketType enumType, Operation op, float value )
        {
            BankRemotePacket p = new BankRemotePacket( this.Pipe );
            p.Begin(enumType, this.GetWidgetTypeGUID(), Id);

            p.Write_s32( (int)op );
            p.Write_float( value );

            p.Send();
        }

        public void UpdateRemoteOperation( BankRemotePacket.EnumPacketType enumType, Operation op, float value1, float value2, float value3 )
        {
            BankRemotePacket p = new BankRemotePacket( this.Pipe );
            p.Begin(enumType, this.GetWidgetTypeGUID(), Id);

            p.Write_s32( (int)op );
            p.Write_float( value1 );
            p.Write_float( value2 );
            p.Write_float( value3 );

            p.Send();
        }

        public void UpdateRemoteOperation( BankRemotePacket.EnumPacketType enumType, Operation op, float value1, float value2, float value3, float value4 )
        {
            BankRemotePacket p = new BankRemotePacket( this.Pipe );
            p.Begin(enumType, this.GetWidgetTypeGUID(), Id);

            p.Write_s32( (int)op );
            p.Write_float( value1 );
            p.Write_float( value2 );
            p.Write_float( value3 );
            p.Write_float( value4 );

            p.Send();
        }

        public void UpdateRemoteOperation( BankRemotePacket.EnumPacketType enumType, Operation op, float value1, float value2, float value3, float value4, float value5 )
        {
            BankRemotePacket p = new BankRemotePacket( this.Pipe );
            p.Begin(enumType, this.GetWidgetTypeGUID(), Id);

            p.Write_s32( (int)op );
            p.Write_float( value1 );
            p.Write_float( value2 );
            p.Write_float( value3 );
            p.Write_float( value4 );
            p.Write_float( value5 );

            p.Send();
        }

        public void UpdateRemoteOperation( BankRemotePacket.EnumPacketType enumType, Operation op, int value )
        {
            BankRemotePacket p = new BankRemotePacket( this.Pipe );
            p.Begin(enumType, this.GetWidgetTypeGUID(), Id);

            p.Write_s32( (int)op );
            p.Write_s32( value );

            p.Send();
        }

        /////////////////////////




        public void NormalizeSafeClickEH( object sender, EventArgs e )
        {
            BankRemotePacket p = new BankRemotePacket( Pipe );
            p.Begin(BankRemotePacket.EnumPacketType.USER_0, this.GetWidgetTypeGUID(), Id);

            p.Write_s32( (int)Operation.NormalizeSafe );

            p.Send();
        }

        public void NormalizeClickEH( object sender, EventArgs e )
        {
            BankRemotePacket p = new BankRemotePacket( Pipe );
            p.Begin(BankRemotePacket.EnumPacketType.USER_0, this.GetWidgetTypeGUID(), Id);

            p.Write_s32( (int)Operation.Normalize );

            p.Send();
        }

        public void NegateClickEH( object sender, EventArgs e )
        {
            BankRemotePacket p = new BankRemotePacket( Pipe );
            p.Begin(BankRemotePacket.EnumPacketType.USER_0, this.GetWidgetTypeGUID(), Id);

            p.Write_s32( (int)Operation.Negate );

            p.Send();
        }

        public void Negate3x3ClickEH( object sender, EventArgs e )
        {
            BankRemotePacket p = new BankRemotePacket( Pipe );
            p.Begin(BankRemotePacket.EnumPacketType.USER_0, this.GetWidgetTypeGUID(), Id);

            p.Write_s32( (int)Operation.Negate3x3 );

            p.Send();
        }


        public void MakeUprightClickEH( object sender, EventArgs e )
        {
            BankRemotePacket p = new BankRemotePacket( Pipe );
            p.Begin(BankRemotePacket.EnumPacketType.USER_0, this.GetWidgetTypeGUID(), Id);

            p.Write_s32( (int)Operation.MakeUpright );

            p.Send();
        }


        public void InverseClickEH( object sender, EventArgs e )
        {
            BankRemotePacket p = new BankRemotePacket( Pipe );
            p.Begin(BankRemotePacket.EnumPacketType.USER_0, this.GetWidgetTypeGUID(), Id);

            p.Write_s32( (int)Operation.Inverse );

            p.Send();
        }

        public void Inverse3x3ClickEH( object sender, EventArgs e )
        {
            BankRemotePacket p = new BankRemotePacket( Pipe );
            p.Begin(BankRemotePacket.EnumPacketType.USER_0, this.GetWidgetTypeGUID(), Id);

            p.Write_s32( (int)Operation.Inverse3x3 );

            p.Send();
        }

        public void IdentityClickEH( object sender, EventArgs e )
        {
            BankRemotePacket p = new BankRemotePacket( Pipe );
            p.Begin(BankRemotePacket.EnumPacketType.USER_0, this.GetWidgetTypeGUID(), Id);

            p.Write_s32( (int)Operation.Identity );

            p.Send();
        }

        public void Identity3x3ClickEH( object sender, EventArgs e )
        {
            BankRemotePacket p = new BankRemotePacket( Pipe );
            p.Begin(BankRemotePacket.EnumPacketType.USER_0, this.GetWidgetTypeGUID(), Id);

            p.Write_s32( (int)Operation.Identity3x3 );

            p.Send();
        }

        public void FastInverseClickEH( object sender, EventArgs e )
        {
            BankRemotePacket p = new BankRemotePacket( Pipe );
            p.Begin(BankRemotePacket.EnumPacketType.USER_0, this.GetWidgetTypeGUID(), Id);

            p.Write_s32( (int)Operation.FastInverse );

            p.Send();
        }


        public void CoordinateInverseSafeClickEH( object sender, EventArgs e )
        {
            BankRemotePacket p = new BankRemotePacket( Pipe );
            p.Begin(BankRemotePacket.EnumPacketType.USER_0, this.GetWidgetTypeGUID(), Id);

            p.Write_s32( (int)Operation.CoordinateInverseSafe );

            p.Send();
        }

        public void AbsClickEH( object sender, EventArgs e )
        {
            BankRemotePacket p = new BankRemotePacket( Pipe );
            p.Begin(BankRemotePacket.EnumPacketType.USER_0, this.GetWidgetTypeGUID(), Id);

            p.Write_s32( (int)Operation.Abs );

            p.Send();
        }
        #endregion

        #region Private Functions
        private void WriteVector( BankRemotePacket p, float[] vector )
        {
            for ( int i = 0; i < this.NumColumns; ++i )
            {
                p.Write_float( vector[i] );
            }
        }

        private void WriteMatrix( BankRemotePacket p, float[][] matrix )
        {
            for ( int i = 0; i < this.NumRows; ++i )
            {
                for ( int j = 0; j < this.NumColumns; ++j )
                {
                    p.Write_float( matrix[i][j] );
                }
            }
        }
        #endregion

        #region Static Functions
        /// <summary>
        /// 
        /// </summary>
        /// <param name="packet"></param>
        /// <param name="enumType"></param>
        protected static bool RemoteHandler(BankRemotePacket packet, out String errorMessage, EnumType enumType, int guid)
        {
            bool handled = false;

            if ( packet.BankCommand == BankRemotePacket.EnumPacketType.CREATE )
            {
                packet.Begin();

                uint remoteParent = packet.Read_bkWidget();
                String title = packet.Read_const_char();
                String memo = packet.Read_const_char();
                Color fillColor = Widget.DeserializeColor(packet.Read_const_char());
                bool readOnly = packet.Read_bool();

                float[][] matrix = new float[4][];
                for ( int i = 0; i < 4; ++i )
                {
                    matrix[i] = new float[4];
                }

                int rows = 0;
                int cols = 0;
                switch ( enumType )
                {
                    case EnumType.Matrix33:
                        rows = 3;
                        cols = 3;
                        break;
                    case EnumType.Matrix34:
                        rows = 4;
                        cols = 3;
                        break;
                    case EnumType.Matrix44:
                        rows = 4;
                        cols = 4;
                        break;
                }

                for ( int i = 0; i < rows; ++i )
                {
                    for ( int j = 0; j < cols; ++j )
                    {
                        matrix[i][j] = packet.Read_float();
                    }
                }

                float min = packet.Read_float();
                float max = packet.Read_float();
                float step = packet.Read_float();

                packet.End();

                Widget parent;
                if (packet.BankPipe.RemoteToLocal(remoteParent, out parent, out errorMessage))
                {
                    WidgetMatrix local = null;
                    switch (enumType)
                    {
                        case EnumType.Matrix33:
                            local = new WidgetMatrix33(packet.BankPipe, packet.Id, title, memo, matrix, min, max, step, fillColor, readOnly);
                            break;
                        case EnumType.Matrix34:
                            local = new WidgetMatrix34(packet.BankPipe, packet.Id, title, memo, matrix, min, max, step, fillColor, readOnly);
                            break;
                        case EnumType.Matrix44:
                            local = new WidgetMatrix44(packet.BankPipe, packet.Id, title, memo, matrix, min, max, step, fillColor, readOnly);
                            break;
                    }

                    packet.BankPipe.Associate(packet.Id, local);
                    parent.AddChild(local);
                    handled = true;
                }
            }
            else if ( packet.BankCommand == BankRemotePacket.EnumPacketType.CHANGED )
            {
                packet.Begin();

				// find the local version of the the remote widget:
				WidgetMatrix local;
                if (packet.BankPipe.RemoteToLocal(packet.Id, out local, out errorMessage))
                {
                    for (int i = 0; i < local.NumRows; ++i)
                    {
                        for (int j = 0; j < local.NumColumns; ++j)
                        {
                            local.m_matrix[i][j] = packet.Read_float();
                        }
                    }

                    packet.End();

                    local.m_updateRemote = false;
                    local.OnValueChanged();
                    local.m_updateRemote = true;
                    handled = true;
                }
            }
            else if (packet.BankCommand >= BankRemotePacket.EnumPacketType.USER_0 &&
                packet.BankCommand <= BankRemotePacket.EnumPacketType.USER_11)
            {
                handled = true;
                errorMessage = null;
            }
            else
            {
                handled = Widget.RemoteHandlerBase(packet, guid, out errorMessage);
            }

            return handled;
        }

        public static void AddHandlers( BankRemotePacketProcessor packetProcessor )
        {
            WidgetMatrix33.AddHandlers( packetProcessor );
            WidgetMatrix34.AddHandlers( packetProcessor );
            WidgetMatrix44.AddHandlers( packetProcessor );
        }
        #endregion
    }

    public class WidgetMatrix33 : WidgetMatrix
    {
        public WidgetMatrix33(BankPipe pipe, uint id, String title, String memo, float[][] matrix, float min, float max, float step, Color fillColor, bool readOnly)
            : base(pipe, id, title, memo, matrix, EnumType.Matrix33, min, max, step, fillColor, readOnly)
        {

        }

        public override int GetWidgetTypeGUID() { return GetStaticGUID(); }
        public static int GetStaticGUID() { return ComputeGUID( 'm', 't', 'x', '9' ); }

        private static bool RemoteHandler(BankRemotePacket packet, out String errorMessage)
        {
            return WidgetMatrix.RemoteHandler(packet, out errorMessage, EnumType.Matrix33, GetStaticGUID());
        }

        public static new void AddHandlers( BankRemotePacketProcessor packetProcessor )
        {
            packetProcessor.AddType( GetStaticGUID(), new BankRemotePacketProcessor.HandlerDel( RemoteHandler ) );
        }

    }

    public class WidgetMatrix34 : WidgetMatrix
    {
        public WidgetMatrix34(BankPipe pipe, uint id, String title, String memo, float[][] matrix, float min, float max, float step, Color fillColor, bool readOnly)
            : base(pipe, id, title, memo, matrix, EnumType.Matrix34, min, max, step, fillColor, readOnly)
        {

        }

        public override int GetWidgetTypeGUID() { return GetStaticGUID(); }
        public static int GetStaticGUID() { return ComputeGUID( 'm', 't', 'x', '2' ); }

        private static bool RemoteHandler(BankRemotePacket packet, out String errorMessage)
        {
            return WidgetMatrix.RemoteHandler(packet, out errorMessage, EnumType.Matrix34, GetStaticGUID());
        }

        public static new void AddHandlers( BankRemotePacketProcessor packetProcessor )
        {
            packetProcessor.AddType( GetStaticGUID(), new BankRemotePacketProcessor.HandlerDel( RemoteHandler ) );
        }
    }

    public class WidgetMatrix44 : WidgetMatrix
    {
        public WidgetMatrix44(BankPipe pipe, uint id, String title, String memo, float[][] matrix, float min, float max, float step, Color fillColor, bool readOnly)
            : base(pipe, id, title, memo, matrix, EnumType.Matrix44, min, max, step, fillColor, readOnly)
        {

        }

        public override int GetWidgetTypeGUID() { return GetStaticGUID(); }
        public static int GetStaticGUID() { return ComputeGUID( 'm', 't', 'x', '6' ); }

        private static bool RemoteHandler(BankRemotePacket packet, out String errorMessage)
        {
            return WidgetMatrix.RemoteHandler(packet, out errorMessage, EnumType.Matrix44, GetStaticGUID());
        }

        public static new void AddHandlers( BankRemotePacketProcessor packetProcessor )
        {
            packetProcessor.AddType( GetStaticGUID(), new BankRemotePacketProcessor.HandlerDel( RemoteHandler ) );
        }
    }
}
