using System;
using System.Collections.Generic;
using System.Drawing;
using System.Xml;

using ragCore;

namespace ragWidgets
{
	/// <summary>
	/// Summary description for WidgetTreeList.
	/// </summary>
	public class WidgetTreeList : Widget
	{

        #region Enums:  keep these in sync with bank/treelist.h
        public enum ColumnControlType
        {
            None,            // This column or item is not editable.
            ColumnTemplate,  // Use the same ItemControlType to edit all items in this column.
            InPlaceTemplate, // Use a ItemControlType to edit items in this column.  Each item must specify what to use.
            TextBox,         // built-in type
            ComboBox,        // built-in type
            DateTimePicker   // built-in type
        }

        public enum EditCondition
        {
            None,           // Column will be read-only
            SingleClick,    // Activate the item editor with 1 click
            DoubleClick,    // Activate the item editor with a double click
            AlwaysVisible   // item editor is always visible.  only works with ColumnControlTypeColumnTemplate
        }

        public enum ItemControlType
        {
            None,      // When ColumnControlTypeInPlaceTemplate is used on a column, item(s) with this ItemControlType will be read-only.  Invalid with ColumnControlTypeColumnTemplate.
            TextBox,
            ComboBox,
            DateTimePicker,
            NumericUpDown,
            Button,
            ProgressBar,
            Slider
        }

        public enum CheckBoxAlign
        {
            None,  // No check boxes on items in the column
            Left,
            Right,
            Center
        }

        public enum CheckBoxStyle
        {
            None,      // No check boxes on items in the column
            TwoState,
            ThreeState,
        }

        public enum CheckBoxState
        {
            Unchecked,
            Indeterminate,
            Checked
        }

        public enum DateTimeFormat
        {
            Long,     // Tuesday, July 3, 2006
            Short,    // 6/29/2006
            Time,     // 12:00:00 AM			
        }
        #endregion




        public WidgetTreeList(BankPipe pipe, uint id, String title, String memo, bool allowDrag, bool allowDrop, bool allowDelete, bool showRootLines, Color fillColor, bool readOnly)
            : base(pipe, id, title, memo, fillColor, readOnly)
		{
			m_allowDrag = allowDrag;
			m_allowDrop = allowDrop;
			m_allowDelete = allowDelete;
            m_showRootLines = showRootLines;

			//
			// TODO: Add constructor logic here
			//
		}

		public override int GetWidgetTypeGUID() {return GetStaticGUID();}
		public static int GetStaticGUID()		{return ComputeGUID('t','l','s','t');}
		
		public override void Serialize(XmlTextWriter writer,string path) {}
		public override void DeSerialize(XmlTextReader reader) {}
		public override bool AllowPersist {get {return false;}}

        public delegate void ColumnDelegate( object sender, TreeListColumnEventArgs args );
        public delegate void NodeDelegate( object sender, TreeListNodeEventArgs args );
        public delegate void ItemDelegate( object sender, TreeListItemEventArgs args );
        public delegate void KeyValueChangedDelegate( object sender, KeyValueEventArgs args );

        public event ColumnDelegate ColumnAdded;
        public event NodeDelegate NodeAdded;
        public event ItemDelegate ItemAdded;
        public event KeyValueChangedDelegate NodeRemoved;
        public event KeyValueChangedDelegate ItemRemoved;
        public event KeyValueChangedDelegate ItemValueChanged;
        public event KeyValueChangedDelegate CheckStateChanged;


		public override void UpdateRemote()
		{	
		}

		public void ModifyItem( int key, TreeListItemData.DataType dataType, string newValue )
		{
            TreeListItem item;
			if ( !m_ItemHashtable.TryGetValue( key, out item ) || (item.Val == newValue) )
			{
				// don't send the message if we're not changing the value
				return;
			}

			BankRemotePacket p = new BankRemotePacket( Pipe );
            p.Begin(BankRemotePacket.EnumPacketType.USER_5, GetWidgetTypeGUID(), Id);
			p.Write_s32( key );
			p.Write_s32( (int)dataType );
			p.Write_const_char( newValue );
			p.Send();
		}

		public void ItemChecked( int checkBoxKey, CheckBoxState checkedState )
		{
            TreeListItem item;
			if ( !m_ItemHashtable.TryGetValue( checkBoxKey, out item ) || (item.CheckedState == checkedState) )
			{
				// don't send the message if we're not changing the value
				return;
			}

			BankRemotePacket p = new BankRemotePacket( Pipe );
            p.Begin(BankRemotePacket.EnumPacketType.USER_6, GetWidgetTypeGUID(), Id);
			p.Write_s32( checkBoxKey );
			p.Write_s32( (int)checkedState );
			p.Send();
		}

		public void ButtonClicked( int key )
		{
			TreeListItem item;
            if ( !m_ItemHashtable.TryGetValue( key, out item ) )
			{
				return;
			}

			BankRemotePacket p = new BankRemotePacket( Pipe );
            p.Begin(BankRemotePacket.EnumPacketType.USER_9, GetWidgetTypeGUID(), Id);
			p.Write_s32( key );
			p.Write_s32( 0 );
			p.Send();
		}

		public void DragDropNotification( int key )
		{
			BankRemotePacket p = new BankRemotePacket( Pipe );
            p.Begin(BankRemotePacket.EnumPacketType.USER_9, GetWidgetTypeGUID(), Id);
			p.Write_s32( key );
			p.Write_s32( 1 );
			p.Send();
		}

		public void DeleteNode( int key )
		{
			BankRemotePacket p = new BankRemotePacket( Pipe );
            p.Begin(BankRemotePacket.EnumPacketType.USER_9, GetWidgetTypeGUID(), Id);
			p.Write_s32( key );
			p.Write_s32( 2 );
			p.Send();
		}
		
		public static bool RemoteHandler( BankRemotePacket packet, out String errorMessage )
		{
            bool handled = false;

			if ( packet.BankCommand == BankRemotePacket.EnumPacketType.CREATE )
			{
				packet.Begin();

				uint remoteParent		= packet.Read_bkWidget();
				String title			= packet.Read_const_char();
				String memo				= packet.Read_const_char();
                Color fillColor = Widget.DeserializeColor(packet.Read_const_char());
                bool readOnly = packet.Read_bool();
				bool allowDrag			= packet.Read_bool();
				bool allowDrop			= packet.Read_bool();
				bool allowDelete		= packet.Read_bool();
                bool showRootLines = packet.Read_bool();
							
				packet.End();
                
                Widget parent;
                if (packet.BankPipe.RemoteToLocal(remoteParent, out parent, out errorMessage))
                {
                    WidgetTreeList local = new WidgetTreeList(packet.BankPipe, packet.Id, title, memo, allowDrag, allowDrop, allowDelete, showRootLines, fillColor, readOnly);
                    packet.BankPipe.Associate(packet.Id, local);
                    parent.AddChild(local);
                    handled = true;
                }
			}
			// Add Column
			else if ( packet.BankCommand == BankRemotePacket.EnumPacketType.USER_0 )
			{
				packet.Begin();	

				// find the local version of the the remote widget:
				WidgetTreeList local;
                if (packet.BankPipe.RemoteToLocal(packet.Id, out local, out errorMessage))
                {
                    TreeListColumnHeader colHeader = new TreeListColumnHeader();
                    colHeader.Name = packet.Read_const_char();
                    colHeader.ColumnControlType = (ColumnControlType)packet.Read_s32();
                    colHeader.EditCondition = (EditCondition)packet.Read_s32();
                    colHeader.CheckStyle = (CheckBoxStyle)packet.Read_s32();
                    colHeader.CheckAlign = (CheckBoxAlign)packet.Read_s32();
                    colHeader.ItemControlType = (ItemControlType)packet.Read_s32();

                    local.m_ColHeaders.Add(colHeader);

                    if (local.ColumnAdded != null)
                    {
                        local.ColumnAdded(local, new TreeListColumnEventArgs(colHeader));
                    }

                    packet.End();
                    handled = true;
                }
			}
			// Add Node
			else if ( packet.BankCommand == BankRemotePacket.EnumPacketType.USER_1 )
			{
				packet.Begin();
				
				// find the local version of the the remote widget:
                WidgetTreeList local;
                if (packet.BankPipe.RemoteToLocal(packet.Id, out local, out errorMessage))
                {
                    TreeListNode node = new TreeListNode();
                    node.Name = packet.Read_const_char();
                    node.Key = packet.Read_s32();
                    node.Type = (TreeListNodeData.DataType)packet.Read_s32();
                    node.ParentKey = packet.Read_s32();
                    node.Expand = packet.Read_bool();

                    local.AddNode(node);

                    if (local.NodeAdded != null)
                    {
                        local.NodeAdded(local, new TreeListNodeEventArgs(node));
                    }

                    packet.End();
                    handled = true;
                }
			}
			// Add SubItem
			else if ( packet.BankCommand == BankRemotePacket.EnumPacketType.USER_2 ) 
			{
                packet.Begin();
				
				// find the local version of the the remote widget:
                WidgetTreeList local;
                if (packet.BankPipe.RemoteToLocal(packet.Id, out local, out errorMessage))
                {
				    TreeListItem item = new TreeListItem();
				    item.Key = packet.Read_s32();
				    item.Val = packet.Read_const_char();
				    item.NodeKey = packet.Read_s32();
				    item.Type = (TreeListItemData.DataType)packet.Read_s32();
				    item.UpdateParent = packet.Read_bool();
				    item.ReadOnly = packet.Read_bool();
				    item.CheckBoxKey = packet.Read_s32();
				    item.CheckedState = (WidgetTreeList.CheckBoxState)packet.Read_s32();
				    item.ItemControlType = (WidgetTreeList.ItemControlType)packet.Read_s32();
				    item.HasQuotes = item.Val.IndexOf("\"") > -1;

				    int count = packet.Read_s32();
				    if ( count > 0 )
				    {
					    item.ComboBoxItems = new string[count];
					    for (int i = 0; i < count; ++i)
					    {
						    item.ComboBoxItems[i] = packet.Read_const_char();
					    }
				    }

				    local.AddItem( item );

                    if ( local.ItemAdded != null )
                    {
                        local.ItemAdded( local, new TreeListItemEventArgs( item ) );
                    }

				    packet.End();
                    handled = true;
                }
			}
			// Remove Node
			else if ( packet.BankCommand == BankRemotePacket.EnumPacketType.USER_3 ) 
			{
				packet.Begin();
				
				// find the local version of the the remote widget:
                WidgetTreeList local;
                if (packet.BankPipe.RemoteToLocal(packet.Id, out local, out errorMessage))
                {
                    int key = packet.Read_s32();

                    local.RemoveNode(key);

                    if (local.NodeRemoved != null)
                    {
                        local.NodeRemoved(local, new KeyValueEventArgs(key, null));
                    }

                    packet.End();
                    handled = true;
                }
			}
			// Remove SubItem
			else if ( packet.BankCommand == BankRemotePacket.EnumPacketType.USER_4 ) 
			{
				packet.Begin();
				
				// find the local version of the the remote widget:
                WidgetTreeList local;
                if (packet.BankPipe.RemoteToLocal(packet.Id, out local, out errorMessage))
                {
                    int key = packet.Read_s32();

                    local.RemoveItem(key);

                    if (local.ItemRemoved != null)
                    {
                        local.ItemRemoved(local, new KeyValueEventArgs(key, null));
                    }

                    packet.End();
                    handled = true;
                }
			}
			// change value of a subItem
			else if ( packet.BankCommand == BankRemotePacket.EnumPacketType.USER_7 )
			{
				packet.Begin();
				
				// find the local version of the the remote widget:
                WidgetTreeList local;
                if (packet.BankPipe.RemoteToLocal(packet.Id, out local, out errorMessage))
                {
                    int key = packet.Read_s32();
                    string newVal = packet.Read_const_char();

                    // update the TreeListItem (in case we create more visibles)
                    TreeListItem item;
                    if (local.m_ItemHashtable.TryGetValue(key, out item))
                    {
                        item.Val = newVal;
                    }

                    if (local.ItemValueChanged != null)
                    {
                        local.ItemValueChanged(local, new KeyValueEventArgs(key, newVal));
                    }

                    packet.End();
                    handled = true;
                }
			}
			// change value of subItem check box
			else if ( packet.BankCommand == BankRemotePacket.EnumPacketType.USER_8 )
			{
				packet.Begin();
				
				// find the local version of the the remote widget:
                WidgetTreeList local;
                if (packet.BankPipe.RemoteToLocal(packet.Id, out local, out errorMessage))
                {
                    int checkBoxKey = packet.Read_s32();
                    WidgetTreeList.CheckBoxState state = (WidgetTreeList.CheckBoxState)packet.Read_s32();

                    // update the TreeListItem (in case we create more visibles)
                    TreeListItem item;
                    if (local.m_ItemHashtable.TryGetValue(checkBoxKey, out item))
                    {
                        item.CheckedState = state;
                    }

                    if (local.CheckStateChanged != null)
                    {
                        local.CheckStateChanged(local, new KeyValueEventArgs(checkBoxKey, state));
                    }

                    packet.End();
                    handled = true;
                }
            }
            else if (packet.BankCommand == BankRemotePacket.EnumPacketType.USER_5 ||
                packet.BankCommand == BankRemotePacket.EnumPacketType.USER_6 ||
                packet.BankCommand == BankRemotePacket.EnumPacketType.USER_9)
            {
                handled = true;
                errorMessage = null;
            }
            else
            {
                handled = Widget.RemoteHandlerBase(packet, GetStaticGUID(), out errorMessage);
            }

            return handled;
		}

		private void AddNode( TreeListNode node )
		{
            TreeListNode otherNode;
			if ( m_NodeHashtable.TryGetValue( node.Key, out otherNode ) )
			{
				RemoveNode( node.Key );
			}

			// add ourself to our parent
            TreeListNode parentNode;
			if ( (node.ParentKey != -1) && m_NodeHashtable.TryGetValue( node.ParentKey, out parentNode ) )
			{
				parentNode.Nodes.Add( node );
			}

			// add to the list and hashtable
			m_NodeList.Add( node );
			m_NodeHashtable.Add( node.Key, node );
		}

		private void RemoveNode( int nodeKey )
		{
            TreeListNode node;
			if ( !m_NodeHashtable.TryGetValue( nodeKey, out node ) )
			{
				return;
			}
			
			// remove self from parent
            TreeListNode parentNode;
			if ( (node.ParentKey != -1) && m_NodeHashtable.TryGetValue( node.ParentKey, out parentNode ) )
			{                
				parentNode.Nodes.Remove( node );
			}

			// remove items
			while ( node.SubItems.Count > 0 )
			{
				TreeListItem item = node.SubItems[0];
				RemoveItem( item.Key );
			}

			// recursively remove sub nodes
			while ( node.Nodes.Count > 0 )
			{
				TreeListNode subNode = node.Nodes[0];
				RemoveNode( subNode.Key );
			}

			// remove ourself from the list and hash tables
			m_NodeList.Remove( node );
			m_NodeHashtable.Remove( nodeKey );
		}

		private void AddItem( TreeListItem item )
		{
            TreeListItem otherItem;
			if ( m_ItemHashtable.TryGetValue( item.Key, out otherItem ) )
			{				
				RemoveItem( item.Key );
			}

			// add to the node it belongs to
            TreeListNode node;
			if ( (item.NodeKey != -1) && m_NodeHashtable.TryGetValue( item.NodeKey, out node ) )
			{                
				node.SubItems.Add( item );
			}

			// add ourself to the list and hash tables
			m_ItemList.Add( item );
			m_ItemHashtable.Add( item.Key, item );
			if ( item.CheckBoxKey != -1 )
			{
				m_ItemHashtable.Add( item.CheckBoxKey, item );
			}
		}

		private void RemoveItem( int itemKey )
		{
            TreeListItem item;
			if ( !m_ItemHashtable.TryGetValue( itemKey, out item ) )
			{
				return;
			}
						
			// remove ourself from our node
            TreeListNode node;
			if ( (item.NodeKey != -1) && m_NodeHashtable.TryGetValue( item.NodeKey, out node ) )
			{                
				node.SubItems.Remove( item );
			}

			// remove ourself from the list and hash tables
			m_ItemList.Remove( item );
			m_ItemHashtable.Remove( itemKey );

			if ( item.CheckBoxKey != -1 )
			{
				m_ItemHashtable.Remove( item.CheckBoxKey );
			}
		}

		public static void AddHandlers( BankRemotePacketProcessor packetProcessor )
		{
			packetProcessor.AddType( GetStaticGUID(), new BankRemotePacketProcessor.HandlerDel(RemoteHandler) );
		}

		#region Member Variables
		bool m_allowDrag = false;
        public bool AllowDrag
        {
            get { return m_allowDrag; }
        }

		bool m_allowDrop = false;
        public bool AllowDrop
        {
            get { return m_allowDrop; }
        }

		bool m_allowDelete = false;
        public bool AllowDelete
        {
            get { return m_allowDelete; }
        }

        bool m_showRootLines = true;
        public bool ShowRootLines
        {
            get { return m_showRootLines; }
        }

        List<TreeListColumnHeader> m_ColHeaders = new List<TreeListColumnHeader>();
        public List<TreeListColumnHeader> ColHeaders
        {
            get { return m_ColHeaders; }
        }

        List<TreeListNode> m_NodeList = new List<TreeListNode>();
        public List<TreeListNode> NodeList
        {
            get { return m_NodeList; }
        }


        List<TreeListItem> m_ItemList = new List<TreeListItem>();
        public List<TreeListItem> ItemList
        {
            get { return m_ItemList; }
        }

        Dictionary<int, TreeListNode> m_NodeHashtable = new Dictionary<int, TreeListNode>();
        Dictionary<int, TreeListItem> m_ItemHashtable = new Dictionary<int, TreeListItem>();		
		#endregion
	}













    //////////////////////////////////////////////////////////////////////////



    public class TreeListColumnHeader
    {
        public string Name;
        public WidgetTreeList.ColumnControlType ColumnControlType;
        public WidgetTreeList.EditCondition EditCondition;
        public WidgetTreeList.CheckBoxStyle CheckStyle;
        public WidgetTreeList.CheckBoxAlign CheckAlign;
        public WidgetTreeList.ItemControlType ItemControlType;

        public TreeListColumnHeader()
        {
        }
    }

    public class TreeListNode
    {
        public string Name;
        public int Key
        {
            get
            {
                return this.NodeData.Key;
            }
            set
            {
                this.NodeData.Key = value;
            }
        }
        public TreeListNodeData.DataType Type
        {
            get
            {
                return this.NodeData.Type;
            }
            set
            {
                this.NodeData.Type = value;
            }
        }
        public int ParentKey;
        public bool Expand;

        public TreeListNodeData NodeData;

        public List<TreeListNode> Nodes = new List<TreeListNode>();
        public List<TreeListItem> SubItems = new List<TreeListItem>();

        public TreeListNode()
        {
            this.NodeData = new TreeListNodeData();
        }
    }

    public class TreeListItem
    {
        public int Key
        {
            get
            {
                return this.ItemData.Key;
            }
            set
            {
                this.ItemData.Key = value;
            }
        }
        public string Val;
        public int NodeKey;
        public TreeListItemData.DataType Type
        {
            get
            {
                return this.ItemData.Type;
            }
            set
            {
                this.ItemData.Type = value;
            }
        }
        public bool UpdateParent
        {
            get
            {
                return this.ItemData.UpdateParentSubItemsAfterEdit;
            }
            set
            {
                this.ItemData.UpdateParentSubItemsAfterEdit = value;
            }
        }
        public bool ReadOnly;
        public bool NoCheckBox
        {
            get
            {
                return this.ItemData.CheckBoxKey == -1;
            }
        }
        public int CheckBoxKey
        {
            get
            {
                return this.ItemData.CheckBoxKey;
            }
            set
            {
                this.ItemData.CheckBoxKey = value;
            }
        }
        public WidgetTreeList.CheckBoxState CheckedState;
        public WidgetTreeList.ItemControlType ItemControlType;
        public string[] ComboBoxItems
        {
            get
            {
                return this.ItemData.ComboBoxItems;
            }
            set
            {
                this.ItemData.ComboBoxItems = value;
            }
        }
        public bool HasQuotes
        {
            get
            {
                return this.ItemData.HasQuotes;
            }
            set
            {
                this.ItemData.HasQuotes = value;
            }
        }

        public TreeListItemData ItemData;

        public TreeListItem()
        {
            this.ItemData = new TreeListItemData();
        }
    }

    [Serializable]
    public class TreeListNodeData
    {
        #region Enums:  keep these in sync with bank/treelist.h
        public enum DataType
        {
            InvalidType,
            Native,		// no sub nodes; sub items will be int, float, and/or string
            Struct,		// sub nodes will be any combination of natives and vectors.
            Vector,		// special case struct which will have 3 sub nodes for x, y, and z as floats
            Thread,		// all sub nodes belong to a thread
        }
        #endregion

        public int Key;
        public DataType Type;

        public TreeListNodeData()
        {
            this.Key = -1;
            this.Type = DataType.InvalidType;
        }

        public TreeListNodeData( int key, DataType type )
        {
            this.Key = key;
            this.Type = type;
        }
    }

    public class TreeListItemData
    {
        #region Enums:  keep these in sync with bank/treelist.h
        public enum DataType
        {
            InvalidType,
            Float,
            Int,
            String,
            Bool,
            Vector
        }
        #endregion

        public int Key;
        public int CheckBoxKey;
        public DataType Type;
        public bool UpdateParentSubItemsAfterEdit;
        public string[] ComboBoxItems = null;
        public bool HasQuotes;
        public string PreviousValue = null;

        public TreeListItemData()
        {
            this.Key = -1;
            this.CheckBoxKey = -1;
            this.Type = DataType.InvalidType;
            this.UpdateParentSubItemsAfterEdit = false;
            this.HasQuotes = false;
        }

        public TreeListItemData( int key, int checkBoxKey, DataType type, bool updateParent, bool hasQuotes )
        {
            this.Key = key;
            this.CheckBoxKey = checkBoxKey;
            this.Type = type;
            this.UpdateParentSubItemsAfterEdit = updateParent;
            this.HasQuotes = hasQuotes;
        }
    }

















    ///////////////////////////////
    /// Event classes
    /// 


    public class TreeListColumnEventArgs : EventArgs
    {
        public TreeListColumnEventArgs( TreeListColumnHeader item )
        {
            Item = item;
        }

        #region Variables
        public TreeListColumnHeader Item;
        #endregion
    };


    public class TreeListNodeEventArgs : EventArgs
    {
        public TreeListNodeEventArgs( TreeListNode item )
        {
            Item = item;
        }

        #region Variables
        public TreeListNode Item;
        #endregion
    };


    public class TreeListItemEventArgs : EventArgs
    {
        public TreeListItemEventArgs( TreeListItem item )
        {
            Item = item;
        }

        #region Variables
        public TreeListItem Item;
        #endregion
    };


    public class KeyValueEventArgs : EventArgs
    {
        public KeyValueEventArgs( int item, object newValue )
        {
            Item = item;
            NewValue = newValue;
        }

        #region Variables
        public int Item;
        public object NewValue;
        #endregion
    };



}
