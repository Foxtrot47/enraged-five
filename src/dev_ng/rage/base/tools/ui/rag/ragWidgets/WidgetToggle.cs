using System;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Xml;

using ragCore;

namespace ragWidgets
{
    /// <summary>
    /// Summary description for WidgetToggle.
    /// </summary>
    public class WidgetToggle : Widget
    {
        protected enum EnumType
        {
            BOOL,
            S32,
            U8,
            U16,
            U32,
            BIT_SET,
            FIXED_BIT_SET,
            F32
        };

        protected WidgetToggle(BankPipe pipe, uint id, String title, String memo, bool value, EnumType type, Color fillColor, bool readOnly)
            : base(pipe, id, title, memo, fillColor, readOnly)
        {
            m_DefaultValue = value;
            m_Value = value;
            m_Type = type;
        }

        public override int GetWidgetTypeGUID()
        {
            return GetStaticGUID(m_Type);
        }

        private static int GetStaticGUID(EnumType type)
        {
            if (type == EnumType.BOOL)
                return ComputeGUID('t', 'g', 'b', 'o');
            else if (type == EnumType.S32)
                return ComputeGUID('t', 'g', 's', '3');
            else if (type == EnumType.U8)
                return ComputeGUID('t', 'g', 'u', '8');
            else if (type == EnumType.U16)
                return ComputeGUID('t', 'g', 'u', '1');
            else if (type == EnumType.U32)
                return ComputeGUID('t', 'g', 'u', '3');
            else if (type == EnumType.BIT_SET)
                return ComputeGUID('t', 'g', 'b', 's');
            else if (type == EnumType.FIXED_BIT_SET)
                return ComputeGUID('t', 'g', 'f', 'b');
            else if (type == EnumType.F32)
                return ComputeGUID('t', 'g', 'f', 'l');
            Debug.Fail("Unknown type " + type);
            return 0;
        }


        public override void Serialize( XmlTextWriter writer, String path )
        {
            if ( SerializeStart( writer, ref path ) )
            {
                writer.WriteAttributeString( "Value", m_Value.ToString() );
                writer.WriteEndElement();
            }
        }

        public override void DeSerialize( XmlTextReader reader )
        {
            m_Value = Convert.ToBoolean( reader["Value"] );

            DeSerializeShared();
        }


        public override bool SerializeToJSON( LitJson.JsonData data )
        {
            data["value"] = Checked;
            if ( IsModified() )
            {
                data["default"] = m_DefaultValue;
            }
            return true;
        }

        public override void DeSerializeFromJSON( LitJson.JsonData data )
        {
            bool newValue = (bool)data["value"];
            Checked = newValue;
        }

        public override bool AllowPersist { get { return true; } }

        public override string ProcessCommand( string[] args )
        {
            if ( args.Length > 0 && args[0] == "-help" )
            {
                return "TYPE: " + this.GetType().ToString() + "\n" +
                    "ARGUMENTS: [1,true,t,0,false,f] [-toggle]\n\n" +
                    "\t[1,true,t,0,false,f]    the value to set (1,true,t=true; 0,false,f=false)\n" +
                    "\t[-toggle]               toggles the widget to its opposite value\n";
            }

            if ( args.Length == 0 )
            {
                return Checked ? "true" : "false";
            }

            if ( args.Length != 1 )
            {
                return "Incorrect # of arguments: only one argument required for toggle widget";
            }

            switch ( args[0].ToLower() )
            {
                case "1":
                case "true":
                case "t":
                    Checked = true;
                    break;

                case "0":
                case "false":
                case "f":
                    Checked = false;
                    break;

                case "-toggle":
                    Checked = !Checked;
                    break;

                default:
                    return "Unknown argument '" + args[0] + "' for toggle widget";
            }

            return "";
        }

        public override void ReceiveData(byte[] bytes)
        {
            if (bytes.Length == 0)
            {
                return;
            }

            Checked = BitConverter.ToBoolean(bytes, 0);
        }

        public override void SendData(Stream stream)
        {
            byte[] buffer = BitConverter.GetBytes(Checked);
            stream.Write(BitConverter.GetBytes(buffer.Length), 0, sizeof(int));
            stream.Write(buffer, 0, buffer.Length);
        }

        public override void UpdateRemote()
        {
            BankRemotePacket p = new BankRemotePacket( Pipe );
            p.Begin(BankRemotePacket.EnumPacketType.CHANGED, GetWidgetTypeGUID(), Id);
            p.Write_bool( m_Value );
            p.Send();
        }

        public static bool RemoteHandler_BOOL(BankRemotePacket packet, out String errorMessage) { return RemoteHandler(packet, out errorMessage, EnumType.BOOL); }
        public static bool RemoteHandler_S32(BankRemotePacket packet, out String errorMessage) { return RemoteHandler(packet, out errorMessage, EnumType.S32); }
        public static bool RemoteHandler_U8(BankRemotePacket packet, out String errorMessage) { return RemoteHandler(packet, out errorMessage, EnumType.U8); }
        public static bool RemoteHandler_U16(BankRemotePacket packet, out String errorMessage) { return RemoteHandler(packet, out errorMessage, EnumType.U16); }
        public static bool RemoteHandler_U32(BankRemotePacket packet, out String errorMessage) { return RemoteHandler(packet, out errorMessage, EnumType.U32); }
        public static bool RemoteHandler_BIT_SET(BankRemotePacket packet, out String errorMessage) { return RemoteHandler(packet, out errorMessage, EnumType.BIT_SET); }
        public static bool RemoteHandler_FIXED_BIT_SET(BankRemotePacket packet, out String errorMessage) { return RemoteHandler(packet, out errorMessage, EnumType.FIXED_BIT_SET); }
        public static bool RemoteHandler_F32(BankRemotePacket packet, out String errorMessage) { return RemoteHandler(packet, out errorMessage, EnumType.F32); }

        protected static bool RemoteHandler(BankRemotePacket packet, out String errorMessage, EnumType type)
        {
            bool handled = false;

            if ( packet.BankCommand == BankRemotePacket.EnumPacketType.CREATE )
            {
                packet.Begin();

                uint remoteParent = packet.Read_bkWidget();
                String title = packet.Read_const_char();
                String memo = packet.Read_const_char();
                Color fillColor = Widget.DeserializeColor(packet.Read_const_char());
                bool readOnly = packet.Read_bool();
                bool value = packet.Read_bool();

                // read mask, but we don't care about the value:
                switch ( type )
                {
                    case EnumType.BOOL:
                        packet.Read_bool();
                        break;

                    case EnumType.S32:
                        packet.Read_s32();
                        break;

                    case EnumType.U8:
                        packet.Read_u8();
                        break;

                    case EnumType.U16:
                        packet.Read_u16();
                        break;

                    case EnumType.U32:
                        packet.Read_u32();
                        break;

                    case EnumType.BIT_SET:
                        packet.Read_u8();
                        break;

                    case EnumType.FIXED_BIT_SET:
                        packet.Read_u32();
                        break;

                    case EnumType.F32:
                        packet.Read_float();
                        break;
                }

                packet.End();
                
                Widget parent;
                if (packet.BankPipe.RemoteToLocal(remoteParent, out parent, out errorMessage))
                {
                    WidgetToggle local = new WidgetToggle(packet.BankPipe, packet.Id, title, memo, value, type, fillColor, readOnly);
                    packet.BankPipe.Associate(packet.Id, local);
                    parent.AddChild(local);
                    handled = true;
                }
            }
            else if ( packet.BankCommand == BankRemotePacket.EnumPacketType.CHANGED )
            {
                packet.Begin();

                WidgetToggle local;
                if (packet.BankPipe.RemoteToLocal(packet.Id, out local, out errorMessage))
                {
                    if (local.m_Type != type)
                    {
                        errorMessage = String.Format("Type mismatch.  Expected {0} but received {1}.", local.m_Type, type);
                    }
                    else
                    {
                        local.m_Value = packet.Read_bool();
                        packet.End();
                        local.OnValueChanged();
                        handled = true;
                    }
                }
            }
            else
            {
                handled = Widget.RemoteHandlerBase(packet, GetStaticGUID(type), out errorMessage);
            }

            return handled;
        }

        public static void AddHandlers( BankRemotePacketProcessor packetProcessor )
        {
            packetProcessor.AddType( ComputeGUID( 't', 'g', 'b', 'o' ), new BankRemotePacketProcessor.HandlerDel( RemoteHandler_BOOL ) );
            packetProcessor.AddType( ComputeGUID( 't', 'g', 's', '3' ), new BankRemotePacketProcessor.HandlerDel( RemoteHandler_S32 ) );
            packetProcessor.AddType( ComputeGUID( 't', 'g', 'u', '8' ), new BankRemotePacketProcessor.HandlerDel( RemoteHandler_U8 ) );
            packetProcessor.AddType( ComputeGUID( 't', 'g', 'u', '1' ), new BankRemotePacketProcessor.HandlerDel( RemoteHandler_U16 ) );
            packetProcessor.AddType( ComputeGUID( 't', 'g', 'u', '3' ), new BankRemotePacketProcessor.HandlerDel( RemoteHandler_U32 ) );
            packetProcessor.AddType( ComputeGUID( 't', 'g', 'b', 's' ), new BankRemotePacketProcessor.HandlerDel( RemoteHandler_BIT_SET ) );
            packetProcessor.AddType( ComputeGUID( 't', 'g', 'f', 'b' ), new BankRemotePacketProcessor.HandlerDel( RemoteHandler_FIXED_BIT_SET ) );
            packetProcessor.AddType( ComputeGUID( 't', 'g', 'f', 'l' ), new BankRemotePacketProcessor.HandlerDel( RemoteHandler_F32 ) );
        }

        public bool Checked
        {
            get { return m_Value; }
            set
            {
                if ( m_Value != value )
                {
                    m_Value = value;
                    UpdateRemote();
                    OnValueChanged();
                }
            }
        }


        public override bool IsModified()
        {
            return m_DefaultValue != m_Value;
        }

        public override void ResetToDefault()
        {
            Checked = m_DefaultValue;
        }

        private bool m_DefaultValue;
        private bool m_Value;

        private EnumType m_Type;
    }
}
