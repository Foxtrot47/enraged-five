using System;
using System.Collections;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Xml;

using ragCore;

namespace ragWidgets
{
    abstract public class WidgetSlider : Widget
    {
        public WidgetSlider(BankPipe pipe, uint id, String title, String memo, Color fillColor, bool readOnly)
            : base(pipe, id, title, memo, fillColor, readOnly)
        {

        }

        protected enum EnumType
        {
            FLOAT,
            U8,
            S8,
            U16,
            S16,
            U32,
            S32
        };

        abstract public void UpdateEdit();

        protected EnumType m_Type;

        public event EventHandler SliderInfoChanged;
        protected void OnSliderInfoChanged()
        {
            if ( SliderInfoChanged != null )
            {
                SliderInfoChanged( this, EventArgs.Empty );
            }
        }
    };

    /// <summary>
    /// Summary description for WidgetSliderFloat.
    /// </summary>
    public class WidgetSliderFloat : WidgetSlider
    {
        protected WidgetSliderFloat(BankPipe pipe, uint id, String title, String memo, float value, float mini, float maxi, float step, Color fillColor, bool readOnly,
            bool exponential )
            : base(pipe, id, title, memo, fillColor, readOnly)
        {
            m_Value = m_PrevValue = m_DefaultValue = value;
            m_Mini = mini;
            m_Maxi = maxi;
            m_Step = step;
            m_Type = EnumType.FLOAT;
            m_exponential = exponential;
        }

        public override int GetWidgetTypeGUID() { return GetStaticGUID(); }
        public static int GetStaticGUID() { return ComputeGUID( 's', 'l', 'f', 'l' ); }

        public override void Serialize( XmlTextWriter writer, String path )
        {
            if ( SerializeStart( writer, ref path ) )
            {
                writer.WriteAttributeString( "Value", m_Value.ToString() );
                writer.WriteEndElement();
            }
        }

        public override void DeSerialize( XmlTextReader reader )
        {
            m_Value = Convert.ToSingle( reader["Value"] );

            DeSerializeShared();
        }

        public override bool AllowPersist { get { return true; } }

        public override bool SerializeToJSON( LitJson.JsonData data )
        {
            data["value"] = Value;
            if ( IsModified() )
            {
                data["default"] = m_DefaultValue;
            }
            return true;
        }

        public override void DeSerializeFromJSON( LitJson.JsonData data )
        {
            Value = (float)(Double)data["value"];
        }

        public override string ProcessCommand( string[] args )
        {
            if ( args.Length > 0 && args[0] == "-help" )
            {
                return "TYPE: " + this.GetType().ToString() + "\n" +
                    "ARGUMENTS: [float value] [-min] [-max]\n\n" +
                    "\t[float value]     the slider value to set\n" + 
                    "\t[-min]            gets the minimum value that can be set\n" +
                    "\t[-max]            gets the maximum value that can be set";
            }

            if (args.Length == 0)
            {
                return String.Format("{0:r}", Value);
            }

            if ( args.Length != 1 )
            {
                return "Incorrect # of arguments: only one argument required for float slider";
            }

            switch (args[0].ToLower())
            {
                case "-min":
                    return String.Format("{0:r}", Minimum);
                case "-max":
                    return String.Format("{0:r}", Maximum);
                default:
                {
                    float theValue;
                    try
                    {
                        theValue = Convert.ToSingle(args[0]);
                    }
                    catch (System.Exception e)
                    {
                        return e.Message;
                    }

                    if (theValue < Minimum || theValue > Maximum)
                        return "value '" + theValue + "' is not with the limits (" + Minimum + " - " + Maximum + ")";
                    if (Step == 0)
                        return "widget is read only";

                    Value = theValue;
                    return "";
                }
            }
        }

        public override void ReceiveData(byte[] bytes)
        {
            int argCount = bytes.Length / sizeof(float);
            if (argCount != 1)
            {
                return;
            }

            float theValue;
            try
            {
                theValue = BitConverter.ToSingle(bytes, 0);
            }
            catch (System.Exception)
            {
                return;
            }

            if (theValue < Minimum || theValue > Maximum)
                return;
            if (Step == 0)
                return;

            Value = theValue;
        }

        public override void SendData(Stream stream)
        {
            byte[] buffer = BitConverter.GetBytes(Value);
            stream.Write(BitConverter.GetBytes(buffer.Length), 0, sizeof(int));
            stream.Write(buffer, 0, buffer.Length);
        }

        public override void UpdateEdit()
        {
            if ( m_Value != m_PrevValue )
            {
                m_PrevValue = m_Value;
                //UpdateFinish(); 
            }
        }

        public override void UpdateRemote()
        {
            // don't update remotely if the remote is updating us:
            if ( sm_UpdatingFromRemote )
                return;

            BankRemotePacket p = new BankRemotePacket( Pipe );
            p.Begin(BankRemotePacket.EnumPacketType.CHANGED, GetStaticGUID(), Id);
            p.Write_float( m_Value );
            p.Send();
        }


        public static bool RemoteHandler(BankRemotePacket packet, out String errorMessage)
        {
            bool handled = false;

            if ( packet.BankCommand == BankRemotePacket.EnumPacketType.CREATE )
            {
                packet.Begin();

                uint remoteParent = packet.Read_bkWidget();
                String title = packet.Read_const_char();
                String memo = packet.Read_const_char();
                Color fillColor = Widget.DeserializeColor(packet.Read_const_char());
                bool readOnly = packet.Read_bool();

                float data = packet.Read_float();
                float mini = packet.Read_float();
                float maxi = packet.Read_float();
                float step = packet.Read_float();
                bool exponential = packet.Read_bool();

                packet.End();
                
                Widget parent;
                if (packet.BankPipe.RemoteToLocal(remoteParent, out parent, out errorMessage))
                {
                    WidgetSliderFloat local = new WidgetSliderFloat(packet.BankPipe, packet.Id, title, memo, data, mini, maxi, step, fillColor, readOnly, exponential);
                    packet.BankPipe.Associate(packet.Id, local);
                    parent.AddChild(local);
                    handled = true;
                }
            }
            else if ( packet.BankCommand == BankRemotePacket.EnumPacketType.CHANGED )
            {
                packet.Begin();

				// find the local version of the the remote widget:
                WidgetSliderFloat local;
                if (packet.BankPipe.RemoteToLocal(packet.Id, out local, out errorMessage))
                {
                    float newValue = packet.Read_float();
                    if (!Single.IsNaN(newValue))
                    {
                        local.m_Value = local.m_PrevValue = newValue;
                        packet.End();
                        local.OnValueChanged();
                    }
                    handled = true;
                }
            }
            else if ( packet.BankCommand == BankRemotePacket.EnumPacketType.USER_0 )
            {
                packet.Begin();

				// find the local version of the the remote widget:
                WidgetSliderFloat local;
                if (packet.BankPipe.RemoteToLocal(packet.Id, out local, out errorMessage))
                {
                    local.m_Mini = packet.Read_float();
                    local.m_Maxi = packet.Read_float();

                    local.OnSliderInfoChanged();
                    handled = true;
                }
            }
            else if ( packet.BankCommand == BankRemotePacket.EnumPacketType.USER_1 )
            {
                packet.Begin();

				// find the local version of the the remote widget:
                WidgetSliderFloat local;
                if (packet.BankPipe.RemoteToLocal(packet.Id, out local, out errorMessage))
                {
                    local.m_Step = packet.Read_float();
                    local.OnSliderInfoChanged();
                    handled = true;
                }
            }
            else
            {
                handled = Widget.RemoteHandlerBase(packet, GetStaticGUID(), out errorMessage);
            }

            return handled;
        }

        public override bool IsModified()
        {
            return m_DefaultValue != m_Value;
        }

        public override void ResetToDefault()
        {
            Value = m_DefaultValue;
        }

        public override String HelpTitle
        {
            get { return Title + " (Min=" + Minimum + ", Max=" + Maximum + ")"; }
        }

        public float Value
        {
            get { return m_Value; }
            set
            {
                if ( m_Value != value )
                {
                    m_Value = value;
                    UpdateRemote();
                    OnValueChanged();
                }
            }
        }

        public static bool UpdatingFromRemote
        {
            set
            {
                sm_UpdatingFromRemote = value;
            }
        }

        private static bool sm_UpdatingFromRemote = false;

        public float Minimum { get { return m_Mini; } }
        public float Maximum { get { return m_Maxi; } }
        public float Step { get { return m_Step; } }
        public bool Exponential { get { return m_exponential; } }

        private float m_Value;
        private float m_PrevValue;
        private float m_DefaultValue;
        private float m_Mini;
        private float m_Maxi;
        private float m_Step;
        private bool m_exponential;

        public static void AddHandlers( BankRemotePacketProcessor packetProcessor )
        {
            packetProcessor.AddType( GetStaticGUID(), new BankRemotePacketProcessor.HandlerDel( RemoteHandler ) );
        }
    }

    /// <summary>
    /// Summary description for WidgetSliderInt.
    /// </summary>
    public class WidgetSliderInt : WidgetSlider
    {
        private WidgetSliderInt(BankPipe pipe, uint id, EnumType type, String title, String memo, long value, long mini, long maxi, long step, Color fillColor, bool readOnly,
            bool exponential )
            : base(pipe, id, title, memo, fillColor, readOnly)
        {
            m_Value = m_PrevValue = m_DefaultValue = value;
            m_Mini = mini;
            m_Maxi = maxi;
            m_Step = step;
            m_Type = type;
            m_exponential = exponential;
        }

        public override int GetWidgetTypeGUID()
        {
            return GetStaticGUID(m_Type);
        }

        private static int GetStaticGUID(EnumType type)
        {
            if (type == EnumType.U8)
                return ComputeGUID('s', 'l', 'u', '8');
            else if (type == EnumType.S8)
                return ComputeGUID('s', 'l', 's', '8');
            else if (type == EnumType.U16)
                return ComputeGUID('s', 'l', 'u', '1');
            else if (type == EnumType.S16)
                return ComputeGUID('s', 'l', 's', '1');
            else if (type == EnumType.U32)
                return ComputeGUID('s', 'l', 'u', '3');
            else if (type == EnumType.S32)
                return ComputeGUID('s', 'l', 's', '3');
            Debug.Fail("Unknown type " + type);
            return 0;
        }


        public override void Serialize( XmlTextWriter writer, String path )
        {
            if ( SerializeStart( writer, ref path ) )
            {
                writer.WriteAttributeString( "Value", m_Value.ToString() );
                writer.WriteEndElement();
            }
        }

        public override void DeSerialize( XmlTextReader reader )
        {
            m_Value = Convert.ToInt64( reader["Value"] );

            DeSerializeShared();
        }

        public override bool AllowPersist { get { return true; } }

        public override bool SerializeToJSON( LitJson.JsonData data )
        {
            data["value"] = Value;
            if ( IsModified() )
            {
                data["default"] = m_DefaultValue;
            }

            return true;
        }

        public override void DeSerializeFromJSON( LitJson.JsonData data )
        {
            Value = (int)data["value"];
        }

        public override string ProcessCommand( string[] args )
        {
            if ( args.Length > 0 && args[0] == "-help" )
            {
                return "TYPE: " + this.GetType().ToString() + "\n" +
                    "ARGUMENTS: [integer value]\n\n" +
                    "\t[integer value]     the slider value to set\n" +
                    "\t[-min]              gets the minimum value that can be set\n" +
                    "\t[-max]              gets the maximum value that can be set";

            }

            if (args.Length == 0)
            {
                return Value.ToString();
            }

            if ( args.Length != 1 )
            {
                return "Incorrect # of arguments: only one argument required for int slider";
            }

            switch (args[0].ToLower())
            {
                case "-min":
                    return Minimum.ToString();
                case "-max":
                    return Maximum.ToString();
                default:
                    {
                        long theValue;
                        try
                        {
                            theValue = Convert.ToInt64(args[0]);
                        }
                        catch (System.Exception e)
                        {
                            return e.Message;
                        }

                        if (theValue < Minimum || theValue > Maximum)
                            return "value '" + theValue + "' is not with the limits (" + Minimum + " - " + Maximum + ")";
                        if (Step == 0)
                            return "widget is read only";

                        Value = theValue;
                        return "";
                    }
            }
        }

        public override void ReceiveData(byte[] bytes)
        {
            if (bytes.Length == 0) return;

            long theValue;
            try
            {
                theValue = BitConverter.ToInt32(bytes, 0);
            }
            catch (System.Exception) { return; }

            if (theValue < Minimum || theValue > Maximum)
                return;
            if (Step == 0)
                return;

            Value = theValue;
        }

        public override void SendData(Stream stream)
        {
            byte[] buffer = BitConverter.GetBytes(Value);
            stream.Write(BitConverter.GetBytes(buffer.Length), 0, sizeof(int));
            stream.Write(buffer, 0, buffer.Length);
        }

        public override void UpdateEdit()
        {
            if ( m_Value != m_PrevValue )
            {
                m_PrevValue = m_Value;
                //UpdateFinish(); 
            }
        }

        public override void UpdateRemote()
        {
            BankRemotePacket p = new BankRemotePacket( Pipe );
            p.Begin(BankRemotePacket.EnumPacketType.CHANGED, GetWidgetTypeGUID(), Id);
            switch ( m_Type )
            {
                case EnumType.U8: p.Write_u8( (byte)m_Value ); break;
                case EnumType.S8: p.Write_s8( (sbyte)m_Value ); break;
                case EnumType.U16: p.Write_u16( (ushort)m_Value ); break;
                case EnumType.S16: p.Write_s16( (short)m_Value ); break;
                case EnumType.U32: p.Write_u32( (uint)m_Value ); break;
                case EnumType.S32: p.Write_s32( (int)m_Value ); break;
                default: Debug.Fail( "unknown slider type " + m_Type ); break;
            }
            p.Send();
        }


        public static bool RemoteHandler_U8(BankRemotePacket packet, out String errorMessage) { return RemoteHandler(packet, out errorMessage, EnumType.U8); }
        public static bool RemoteHandler_S8(BankRemotePacket packet, out String errorMessage) { return RemoteHandler(packet, out errorMessage, EnumType.S8); }
        public static bool RemoteHandler_U16(BankRemotePacket packet, out String errorMessage) { return RemoteHandler(packet, out errorMessage, EnumType.U16); }
        public static bool RemoteHandler_S16(BankRemotePacket packet, out String errorMessage) { return RemoteHandler(packet, out errorMessage, EnumType.S16); }
        public static bool RemoteHandler_U32(BankRemotePacket packet, out String errorMessage) { return RemoteHandler(packet, out errorMessage, EnumType.U32); }
        public static bool RemoteHandler_S32(BankRemotePacket packet, out String errorMessage) { return RemoteHandler(packet, out errorMessage, EnumType.S32); }

        private static bool RemoteHandler( BankRemotePacket packet, out String errorMessage, EnumType type )
        {
            bool handled = false;

            if ( packet.BankCommand == BankRemotePacket.EnumPacketType.CREATE )
            {
                packet.Begin();

                uint remoteParent = packet.Read_bkWidget();
                String title = packet.Read_const_char();
                String memo = packet.Read_const_char();
                Color fillColor = Widget.DeserializeColor(packet.Read_const_char());
                bool readOnly = packet.Read_bool();

                long data = 0;
                long mini = 0;
                long maxi = 0;
                long step = 0;

                switch ( type )
                {
                    case EnumType.U8:
                        data = packet.Read_u8();
                        mini = packet.Read_u8();
                        maxi = packet.Read_u8();
                        step = packet.Read_u8();
                        break;
                    case EnumType.S8:
                        data = packet.Read_s8();
                        mini = packet.Read_s8();
                        maxi = packet.Read_s8();
                        step = packet.Read_s8();
                        break;
                    case EnumType.U16:
                        data = packet.Read_u16();
                        mini = packet.Read_u16();
                        maxi = packet.Read_u16();
                        step = packet.Read_u16();
                        break;
                    case EnumType.S16:
                        data = packet.Read_s16();
                        mini = packet.Read_s16();
                        maxi = packet.Read_s16();
                        step = packet.Read_s16();
                        break;
                    case EnumType.U32:
                        data = packet.Read_u32();
                        mini = packet.Read_u32();
                        maxi = packet.Read_u32();
                        step = packet.Read_u32();
                        break;
                    case EnumType.S32:
                        data = packet.Read_s32();
                        mini = packet.Read_s32();
                        maxi = packet.Read_s32();
                        step = packet.Read_s32();
                        break;
                    default:
                        Debug.Fail( "unknown slider type " + type );
                        break;
                }

                bool exponential = packet.Read_bool();
                packet.End();
                
                Widget parent;
                if (packet.BankPipe.RemoteToLocal(remoteParent, out parent, out errorMessage))
                {
                    WidgetSliderInt local = new WidgetSliderInt(packet.BankPipe, packet.Id, type, title, memo, data, mini, maxi, step, fillColor, readOnly, exponential);
                    packet.BankPipe.Associate(packet.Id, local);
                    parent.AddChild(local);
                    handled = true;
                }
            }
            else if ( packet.BankCommand == BankRemotePacket.EnumPacketType.CHANGED )
            {
                packet.Begin();
                
				// find the local version of the the remote widget:
                WidgetSliderInt local;
                if (packet.BankPipe.RemoteToLocal(packet.Id, out local, out errorMessage))
                {
                    if (local.m_Type != type)
                    {
                        errorMessage = String.Format("Type mismatch.  Expected {0} but received {1}.", local.m_Type, type);
                    }
                    else
                    {
                        switch (local.m_Type)
                        {
                            case EnumType.U8: local.m_Value = local.m_PrevValue = packet.Read_u8(); break;
                            case EnumType.S8: local.m_Value = local.m_PrevValue = packet.Read_s8(); break;
                            case EnumType.U16: local.m_Value = local.m_PrevValue = packet.Read_u16(); break;
                            case EnumType.S16: local.m_Value = local.m_PrevValue = packet.Read_s16(); break;
                            case EnumType.U32: local.m_Value = local.m_PrevValue = packet.Read_u32(); break;
                            case EnumType.S32: local.m_Value = local.m_PrevValue = packet.Read_s32(); break;
                            default: Debug.Fail("unknown slider type " + local.m_Type); break;
                        }
                        packet.End();
                        local.OnValueChanged();
                        handled = true;
                    }
                }

            }
            else if ( packet.BankCommand == BankRemotePacket.EnumPacketType.USER_0 )
            {
                packet.Begin();

				// find the local version of the the remote widget:
                WidgetSliderInt local;
                if (packet.BankPipe.RemoteToLocal(packet.Id, out local, out errorMessage))
                {
                    switch (local.m_Type)
                    {
                        case EnumType.U8:
                            local.m_Mini = (long)packet.Read_u8();
                            local.m_Maxi = (long)packet.Read_u8();
                            break;
                        case EnumType.S8:
                            local.m_Mini = (long)packet.Read_s8();
                            local.m_Maxi = (long)packet.Read_s8();
                            break;
                        case EnumType.U16:
                            local.m_Mini = (long)packet.Read_u16();
                            local.m_Maxi = (long)packet.Read_u16();
                            break;
                        case EnumType.S16:
                            local.m_Mini = (long)packet.Read_s16();
                            local.m_Maxi = (long)packet.Read_s16();
                            break;
                        case EnumType.U32:
                            local.m_Mini = (long)packet.Read_u32();
                            local.m_Maxi = (long)packet.Read_u32();
                            break;
                        case EnumType.S32:
                            local.m_Mini = (long)packet.Read_s32();
                            local.m_Maxi = (long)packet.Read_s32();
                            break;
                        default:
                            Debug.Fail("unknown slider type " + local.m_Type);
                            break;
                    }

                    local.OnSliderInfoChanged();
                    handled = true;
                }
            }
            else if ( packet.BankCommand == BankRemotePacket.EnumPacketType.USER_1 )
            {
                packet.Begin();

				// find the local version of the the remote widget:
                WidgetSliderInt local;
                if (packet.BankPipe.RemoteToLocal(packet.Id, out local, out errorMessage))
                {
                    switch (local.m_Type)
                    {
                        case EnumType.U8:
                            local.m_Step = packet.Read_u8();
                            break;
                        case EnumType.S8:
                            local.m_Step = packet.Read_s8();
                            break;
                        case EnumType.U16:
                            local.m_Step = packet.Read_u16();
                            break;
                        case EnumType.S16:
                            local.m_Step = packet.Read_s16();
                            break;
                        case EnumType.U32:
                            local.m_Step = packet.Read_u32();
                            break;
                        case EnumType.S32:
                            local.m_Step = packet.Read_s32();
                            break;
                        default:
                            Debug.Fail("unknown slider type " + local.m_Type);
                            break;
                    }

                    local.OnSliderInfoChanged();
                    handled = true;
                }
            }
            else
            {
                handled = Widget.RemoteHandlerBase(packet, GetStaticGUID(type), out errorMessage);
            }

            return handled;
        }

        public override bool IsModified()
        {
            return m_DefaultValue != m_Value;
        }

        public override void ResetToDefault()
        {
            Value = m_DefaultValue;
        }

        public override String HelpTitle
        {
            get { return this.Title + " (Min=" + this.Minimum + ", Max=" + this.Maximum + ")"; }
        }

        public long Value
        {
            get { return m_Value; }
            set
            {
                if ( m_Value != value )
                {
                    m_Value = value;
                    UpdateRemote();
                    OnValueChanged();
                }
            }
        }
        public long Minimum { get { return m_Mini; } }
        public long Maximum { get { return m_Maxi; } }
        public long Step { get { return m_Step; } }
        public bool Exponential { get { return m_exponential; } }

        private long m_Value;
        private long m_PrevValue;
        private long m_DefaultValue;
        private long m_Mini;
        private long m_Maxi;
        private long m_Step;
        private bool m_exponential;

        public static void AddHandlers( BankRemotePacketProcessor packetProcessor )
        {
            packetProcessor.AddType( ComputeGUID( 's', 'l', 'u', '8' ), new BankRemotePacketProcessor.HandlerDel( RemoteHandler_U8 ) );
            packetProcessor.AddType( ComputeGUID( 's', 'l', 's', '8' ), new BankRemotePacketProcessor.HandlerDel( RemoteHandler_S8 ) );
            packetProcessor.AddType( ComputeGUID( 's', 'l', 'u', '1' ), new BankRemotePacketProcessor.HandlerDel( RemoteHandler_U16 ) );
            packetProcessor.AddType( ComputeGUID( 's', 'l', 's', '1' ), new BankRemotePacketProcessor.HandlerDel( RemoteHandler_S16 ) );
            packetProcessor.AddType( ComputeGUID( 's', 'l', 'u', '3' ), new BankRemotePacketProcessor.HandlerDel( RemoteHandler_U32 ) );
            packetProcessor.AddType( ComputeGUID( 's', 'l', 's', '3' ), new BankRemotePacketProcessor.HandlerDel( RemoteHandler_S32 ) );
        }
    }
}
