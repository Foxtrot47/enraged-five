using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Xml;
using ragCore;

namespace ragWidgets
{
    /// <summary>
    /// Base class for groups of Widgets.
    /// </summary>
    public abstract class WidgetGroupBase : Widget
    {
        protected WidgetGroupBase(BankPipe pipe, uint id, String title, String memo, bool open, Color fillColor, bool readOnly)
            : base(pipe, id, title, memo, fillColor, readOnly)
        {
            m_Open = m_PrevOpen = (open ? 1 : 0);
        }

        #region Variables
	// TODO: Move these to a non-ragCore class
        public static Dictionary<string, Dictionary<string, bool>> sm_expandedGroups = new Dictionary<string, Dictionary<string, bool>>();
        public static Dictionary<string, Dictionary<string, bool>> sm_sortedGroups = new Dictionary<string, Dictionary<string, bool>>();
        public static Dictionary<string, Dictionary<string, bool>> sm_hiddenBanks = new Dictionary<string, Dictionary<string, bool>>();

        protected int m_Open;
        protected int m_PrevOpen;
        protected List<Widget> m_List = new List<Widget>();
        protected bool m_WasDestroyed = false; // true if this widget was destroyed but we still have a reference to it

        #endregion

        #region Properties
        public List<Widget> WidgetList
        {
            get
            {
                return m_List;
            }
        }

        public bool IsOpen
        {
            get
            {
                return m_Open > 0;
            }
        }

        public bool WasOpen
        {
            get
            {
                return m_PrevOpen > 0;
            }
        }

        public List<Widget> List
        {
            get { return m_List; }
        }

        public static Dictionary<string, Dictionary<string, bool>> ExpandedWidgetViewGroups
        {
            get
            {
                return sm_expandedGroups;
            }
        }

        public static Dictionary<string, Dictionary<string, bool>> SortedWidgetViewGroups
        {
            get
            {
                return sm_sortedGroups;
            }
        }

        public static Dictionary<string, Dictionary<string, bool>> HiddenWidgetViewBanks
        {
            get
            {
                return sm_hiddenBanks;
            }
        }
        #endregion

        #region Overrides
        public override void FindWidgetWithMatchingString( bool includeGroup, bool matchCase, bool matchWholeWord, bool exactMatch,
            bool checkHelpDesc, String text, ref List<Widget> list, int depthToSearch )
        {
            if ( depthToSearch > 0 )
                depthToSearch--;

            // if we can include groups, compare my name vs. the search string (but
            // don't check children since we'll already include any child widgets:)
            if ( depthToSearch <= 0 &&
                ((includeGroup && MatchStringWithWidgetText( text, Title, matchCase, matchWholeWord, exactMatch )) ||
                (checkHelpDesc && MatchStringWithWidgetText( text, Memo, matchCase, matchWholeWord, exactMatch ))) )
            {
                list.Add( this );
            }
            else if ( depthToSearch != 0 )
            {
                // loop through children and see if the match:
                lock (m_List)
                {
                    foreach (Widget widget in m_List)
                    {
                        widget.FindWidgetWithMatchingString(includeGroup, matchCase, matchWholeWord, exactMatch, checkHelpDesc, text, ref list, depthToSearch);
                    }
                }
            }
        }

        public delegate void WidgetHierarchyEventHandler( WidgetGroupBase parent, Widget child );
        public static event WidgetHierarchyEventHandler ChildAdded;
        public static  event WidgetHierarchyEventHandler ChildRemoved;

        public override void AddChild( Widget widget )
        {
            Debug.Assert( widget != null, "widget != null" );
            lock (m_List)
            {
                m_List.Add(widget);
            }

            widget.Parent = this;

            // need Parent set
            base.AddChild( widget );

            if ( ChildAdded != null )
            {
                ChildAdded( this, widget );
            }
        }

        public override void RemoveChild( Widget widget )
        {
            base.RemoveChild( widget );

            lock (m_List)
            {
                m_List.Remove(widget);
            }

            widget.Parent = null;

            if ( ChildRemoved != null )
            {
                ChildRemoved( this, widget );
            }
        }

        public override void UpdateRemote()
        {
            if ( m_WasDestroyed )
                return; // shouldn't call an update function on a widget that was destroyed
            // let application now that open has been incremented:
            BankRemotePacket p = new BankRemotePacket(Pipe);
            p.Begin(BankRemotePacket.EnumPacketType.CHANGED, GetWidgetTypeGUID(), Id);
            p.Write_s16((short)m_Open);
            p.Send();

            // RAG internal (need to differentiate between those coming from the game/internal rag communication).
            p = new BankRemotePacket(Pipe);
            p.Begin(BankRemotePacket.EnumPacketType.USER_0, GetWidgetTypeGUID(), Id);
            p.Write_s32(m_Open);
            p.Send();
        }

        public static event WidgetEventHandler WidgetDestroyed;

        public override void Destroy()
        {
            if ( WidgetDestroyed != null )
            {
                WidgetDestroyed( this );
            }

            m_WasDestroyed = true;
            // destroy kids
            // need to copy the list because Destroy removes the kid from the parent's list
            Widget[] kids = new Widget[m_List.Count];
            lock (m_List)
            {
                for (int i = 0; i < m_List.Count; i++)
                {
                    kids[i] = m_List[i];
                }
            }
            foreach ( Widget kid in kids )
            {
                kid.Destroy();
            }

            base.Destroy();
        }


        public delegate void WidgetEventHandler( Widget widget );
        public static WidgetEventHandler WidgetModified;

        public void UpdateWindow()
        {
            if ( WidgetModified != null )
            {
                WidgetModified( this );
            }
        }

        public override void Serialize( XmlTextWriter writer, string path )
        {
            string newPath = path + @"\" + Title;
            lock (m_List)
            {
                foreach (Widget widget in m_List)
                {
                    widget.Serialize(writer, newPath);
                }
            }
        }

        public override void DeSerialize( XmlTextReader reader )
        {
            lock (m_List)
            {
                foreach (Widget widget in m_List)
                {
                    widget.DeSerialize(reader);
                }
            }
        }

        public override bool SerializeToJSON( LitJson.JsonData data )
        {
            LitJson.JsonData subwidgetList = new LitJson.JsonData();
            subwidgetList.SetJsonType( LitJson.JsonType.Array );

            lock (m_List)
            {
                foreach (Widget widget in m_List)
                {
                    LitJson.JsonData widgetData = new LitJson.JsonData();
                    widgetData["name"] = widget.Title;
                    widgetData["type"] = widget.GetType().ToString();
                    if (widget.SerializeToJSON(widgetData))
                    {
                        subwidgetList.Add(widgetData);
                    }
                }
            }

            data["subwidgets"] = subwidgetList;
            return true;
        }

        public override void DeSerializeFromJSON( LitJson.JsonData data )
        {
            foreach ( LitJson.JsonData widgetdata in data["subwidgets"] )
            {
                string name = (string)widgetdata["name"];
                Widget subwidget = FindFirstWidgetFromPath( name );
                if ( subwidget != null && subwidget.GetType().ToString() == (string)widgetdata["type"] )
                {
                    subwidget.DeSerializeFromJSON( widgetdata );
                }
            }

            UpdateWindow();
        }

        public override bool AllowPersist
        {
            get
            {
                return false;
            }
        }

        public override Widget ComparePath( String[] path, ref int index )
        {
            if ( index < path.Length && path[index] == Title )
            {
                index++;
                if ( index <= path.Length )
                {
                    lock (m_List)
                    {
                        foreach (Widget widget in m_List)
                        {
                            Widget retWidget = widget.ComparePath(path, ref index);
                            if (retWidget != null)
                            {
                                return retWidget;
                            }
                        }
                    }
                }
            }

            return null;
        }

        public override List<Widget> FindWidgetsFromPath( String[] path, int index )
        {
            lock (m_List)
            {
                return FindWidgetsFromPath(this, this.m_List, path, index);
            }
        }

        public override void ResetToDefault()
        {
            lock (m_List)
            {
                foreach (Widget w in m_List)
                {
                    w.ResetToDefault();
                }
            }

            // Make sure tree node is up to date
            UpdateWindow();
        }

        #endregion

        #region Static Functions

        public static List<Widget> FindWidgetsFromPath( Widget parent, List<Widget> kids, String path )
        {
            if ( path == null )
            {
                return null;
            }
            string[] pathComponents;
            if ( path.StartsWith( "\\" ) )
            {
                string newPath = path.Remove( 0, 1 );
                pathComponents = newPath.Split( new char[] { '\\' } );
            }
            else
            {
                pathComponents = GetPathComponents(path);

                /* WIP - split on '/', but also perform backslash escaping
                 * Don't do this yet, because saved widgets will save with '\' seperators
                 * (meaning we probably have too many ways to get the widget path from a widget
                 
                List<string> pathCompList = new List<string>();

                string workingString = "";
                bool sawEscape = false;

                for (int i = 0; i < path.Length; i++)
                {
                    if (sawEscape)
                    {
                        workingString += path[i];
                        sawEscape = false;
                    }
                    else if (path[i] == '\\')
                    {
                        sawEscape = true;
                    }
                    else if (path[i] == '/')
                    {
                        pathCompList.Add((string)workingString.Clone());
                        workingString = "";
                    }
                }

                pathComps = pathCompList.ToArray();
                */
            }
            return FindWidgetsFromPath( parent, kids, pathComponents, 0 );
        }

        private static string[] GetPathComponents(string path)
        {
            // Split on "/" chars, but not on "//", these are escaped.
            string[] pathComponents = Regex.Split(path, "(?<!/)/(?!/)");
            UnEscapeCharacters(pathComponents);
            return pathComponents;
        }

        private static void UnEscapeCharacters(string[] pathComps)
        {
            for (int i = 0; i < pathComps.Length; i++)
            {
                if (pathComps[i].Contains("//"))
                {
                    pathComps[i] = pathComps[i].Replace("//", "/");
                }
            }
        }

        public static List<Widget> FindWidgetsFromPath( Widget parent, List<Widget> kids, String[] path, int index )
        {
            // only recurse when we see /, otherwise process names, *s, and []s then
            // pass that list down to the children

            // "Root", "Item[1]", "*[1]", ".."
            // For each widget, see if it matches the spec
            // if spec is ..
            //   recurse w/ parent
            // else
            //   recurse


            List<Widget> ret = new List<Widget>();

            // take path apart
            Regex pathSpec = new Regex( @"^(.*?)(\[(\d+)\])?$", RegexOptions.RightToLeft );

            Match m = pathSpec.Match( path[index] );

            String nameToMatch = String.Empty;

            int searchIndex = -1;

            if ( m.Success )
            {
                nameToMatch = m.Groups[1].Value;

                if ( m.Groups.Count >= 2 )
                {
                    String indexPart = m.Groups[3].Value;
                    if ( indexPart != String.Empty )
                    {
                        searchIndex = System.Int32.Parse( indexPart );
                    }
                }
            }

            int matchType = 0;
            // 0 = name match
            // 1 = *
            // 2 = ..
            if ( nameToMatch == "*" || nameToMatch == String.Empty )
            {
                matchType = 1;
            }
            else if ( nameToMatch == ".." )
            {
                matchType = 2;
            }

            int matchesFound = 0;

            if ( matchType == 2 )
            {
                if ( parent != null && parent.Parent != null )
                {
                    // Handle the case of ".." at the end of a search string
                    if ( index + 1 == path.Length )
                    {
                        ret.Add( parent.Parent );
                    }
                    else
                    {
                        ret = parent.Parent.FindWidgetsFromPath( path, index + 1 );
                    }
                }
                return ret;
            }

            foreach (Widget widget in kids)
            {
                // Match the name part
                Widget matchingWidget = null;
                switch (matchType)
                {
                    case 0: // normal name
                        if (String.Compare(widget.Title, nameToMatch, true) == 0)
                        {
                            matchingWidget = widget;
                        }
                        break;
                    case 1: // *
                        matchingWidget = widget;
                        break;
                }

                // This widget failed to match, continue.
                if (matchingWidget == null)
                {
                    continue;
                }

                // Track # of matches, if there's an index specified make sure
                // # matches == index
                matchesFound++;
                if (searchIndex != -1 && searchIndex != matchesFound)
                {
                    continue;
                }

                // Add the widget (or recurse if there's more to the path)
                if (path.Length == index + 1)
                {
                    ret.Add(matchingWidget);
                }
                else
                {
                    List<Widget> foundWidgets = matchingWidget.FindWidgetsFromPath(path, index + 1);
                    if (foundWidgets != null)
                    {
                        ret.AddRange(foundWidgets);
                    }
                }
            }
        
            return ret;
        }

        public static void SerializeGroups( XmlTextWriter xmlWriter )
        {
            xmlWriter.WriteStartElement( "WidgetGroups" );
            {
                xmlWriter.WriteStartElement( "ExpandedGroups" );
                {
                    foreach ( KeyValuePair<string, Dictionary<string, bool>> pair in sm_expandedGroups )
                    {
                        bool writeStartElement = true;
                        foreach ( KeyValuePair<string, bool> group in pair.Value )
                        {
                            if ( group.Value )
                            {
                                if ( writeStartElement )
                                {
                                    xmlWriter.WriteWhitespace( "\n" );
                                    xmlWriter.WriteStartElement( "WidgetView" );
                                    xmlWriter.WriteAttributeString( "Guid", pair.Key );
                                    writeStartElement = false;
                                }

                                xmlWriter.WriteWhitespace( "\n" );
                                xmlWriter.WriteElementString( "WidgetGroup", group.Key );
                            }
                        }

                        if ( !writeStartElement )
                        {
                            xmlWriter.WriteWhitespace( "\n" );
                            xmlWriter.WriteEndElement();
                        }
                    }

                    xmlWriter.WriteWhitespace( "\n" );
                }
                xmlWriter.WriteEndElement();

                xmlWriter.WriteStartElement( "SortedGroups" );
                {
                    foreach ( KeyValuePair<string, Dictionary<string, bool>> pair in sm_sortedGroups )
                    {
                        bool writeStartElement = true;
                        foreach ( KeyValuePair<string, bool> group in pair.Value )
                        {
                            if ( group.Value )
                            {
                                if ( writeStartElement )
                                {
                                    xmlWriter.WriteWhitespace( "\n" );
                                    xmlWriter.WriteStartElement( "WidgetView" );
                                    xmlWriter.WriteAttributeString( "Guid", pair.Key );
                                    writeStartElement = false;
                                }

                                xmlWriter.WriteWhitespace( "\n" );
                                xmlWriter.WriteElementString( "WidgetGroup", group.Key );
                            }
                        }

                        if ( !writeStartElement )
                        {
                            xmlWriter.WriteWhitespace( "\n" );
                            xmlWriter.WriteEndElement();
                        }
                    }

                    xmlWriter.WriteWhitespace( "\n" );
                }
                xmlWriter.WriteEndElement();

                xmlWriter.WriteStartElement( "HiddenBanks" );
                {
                    foreach ( KeyValuePair<string, Dictionary<string, bool>> pair in sm_hiddenBanks )
                    {
                        bool writeStartElement = true;
                        foreach ( KeyValuePair<string, bool> bank in pair.Value )
                        {
                            if ( bank.Value )
                            {
                                if ( writeStartElement )
                                {
                                    xmlWriter.WriteWhitespace( "\n" );
                                    xmlWriter.WriteStartElement( "WidgetView" );
                                    xmlWriter.WriteAttributeString( "Guid", pair.Key );
                                    writeStartElement = false;
                                }

                                xmlWriter.WriteWhitespace( "\n" );
                                xmlWriter.WriteElementString( "WidgetBank", bank.Key );
                            }
                        }

                        if ( !writeStartElement )
                        {
                            xmlWriter.WriteWhitespace( "\n" );
                            xmlWriter.WriteEndElement();
                        }
                    }

                    xmlWriter.WriteWhitespace( "\n" );
                }
                xmlWriter.WriteEndElement();
            }
            xmlWriter.WriteEndElement();
        }

        public static void DeserializeGroups( XmlTextReader xmlReader )
        {
            string parentName = xmlReader.Name;

            while ( xmlReader.Read() )
            {
                if ( xmlReader.Name == parentName )
                {
                    if ( !xmlReader.IsStartElement() )
                    {
                        break;
                    }
                }
                else if ( xmlReader.Name == "ExpandedGroups" )
                {
                    while ( xmlReader.Read() )
                    {
                        if ( xmlReader.Name == "ExpandedGroups" )
                        {
                            if ( !xmlReader.IsStartElement() )
                            {
                                break;
                            }
                        }
                        else if ( xmlReader.Name == "WidgetView" )
                        {
                            string guid = xmlReader["Guid"];
                            if ( !String.IsNullOrEmpty( guid ) )
                            {
                                Dictionary<string, bool> expandedGroups = null;
                                if ( !sm_expandedGroups.TryGetValue( guid, out expandedGroups ) )
                                {
                                    expandedGroups = new Dictionary<string, bool>();
                                    sm_expandedGroups.Add( guid, expandedGroups );
                                }

                                while ( xmlReader.Read() )
                                {
                                    if ( xmlReader.Name == "WidgetView" )
                                    {
                                        if ( !xmlReader.IsStartElement() )
                                        {
                                            break;
                                        }
                                    }
                                    else if ( xmlReader.Name == "WidgetGroup" )
                                    {
                                        string group = xmlReader.ReadString();
                                        if ( !String.IsNullOrEmpty( group ) )
                                        {
                                            if ( !expandedGroups.ContainsKey( group ) )
                                            {
                                                expandedGroups.Add( group, true );
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else if ( xmlReader.Name == "SortedGroups" )
                {
                    while ( xmlReader.Read() )
                    {
                        if ( xmlReader.Name == "SortedGroups" )
                        {
                            if ( !xmlReader.IsStartElement() )
                            {
                                break;
                            }
                        }
                        else if ( xmlReader.Name == "WidgetView" )
                        {
                            string guid = xmlReader["Guid"];
                            if ( !String.IsNullOrEmpty( guid ) )
                            {
                                Dictionary<string, bool> sortedGroups = null;
                                if ( !sm_sortedGroups.TryGetValue( guid, out sortedGroups ) )
                                {
                                    sortedGroups = new Dictionary<string, bool>();
                                    sm_sortedGroups.Add( guid, sortedGroups );
                                }

                                while ( xmlReader.Read() )
                                {
                                    if ( xmlReader.Name == "WidgetView" )
                                    {
                                        if ( !xmlReader.IsStartElement() )
                                        {
                                            break;
                                        }
                                    }
                                    else if ( xmlReader.Name == "WidgetGroup" )
                                    {
                                        string group = xmlReader.ReadString();
                                        if ( !String.IsNullOrEmpty( group ) )
                                        {
                                            if ( !sortedGroups.ContainsKey( group ) )
                                            {
                                                sortedGroups.Add( group, true );
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else if ( xmlReader.Name == "HiddenBanks" )
                {
                    while ( xmlReader.Read() )
                    {
                        if ( xmlReader.Name == "HiddenBanks" )
                        {
                            if ( !xmlReader.IsStartElement() )
                            {
                                break;
                            }
                        }
                        else if ( xmlReader.Name == "WidgetView" )
                        {
                            string guid = xmlReader["Guid"];
                            if ( !String.IsNullOrEmpty( guid ) )
                            {
                                Dictionary<string, bool> hiddenBanks = null;
                                if ( !sm_hiddenBanks.TryGetValue( guid, out hiddenBanks ) )
                                {
                                    hiddenBanks = new Dictionary<string, bool>();
                                    sm_hiddenBanks.Add( guid, hiddenBanks );
                                }

                                while ( xmlReader.Read() )
                                {
                                    if ( xmlReader.Name == "WidgetView" )
                                    {
                                        if ( !xmlReader.IsStartElement() )
                                        {
                                            break;
                                        }
                                    }
                                    else if ( xmlReader.Name == "WidgetBank" )
                                    {
                                        string bank = xmlReader.ReadString();
                                        if ( !String.IsNullOrEmpty( bank ) )
                                        {
                                            if ( !hiddenBanks.ContainsKey( bank ) )
                                            {
                                                hiddenBanks.Add( bank, true );
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                // old format, kept for backwards compatibility
                else if ( xmlReader.Name == "WidgetView" )
                {
                    string guid = xmlReader["Guid"];
                    if ( !String.IsNullOrEmpty( guid ) )
                    {
                        Dictionary<string, bool> expandedGroups = null;
                        if ( !sm_expandedGroups.TryGetValue( guid, out expandedGroups ) )
                        {
                            expandedGroups = new Dictionary<string, bool>();
                            sm_expandedGroups.Add( guid, expandedGroups );
                        }

                        while ( xmlReader.Read() )
                        {
                            if ( xmlReader.Name == "WidgetView" )
                            {
                                if ( !xmlReader.IsStartElement() )
                                {
                                    break;
                                }
                            }
                            else if ( xmlReader.Name == "WidgetGroup" )
                            {
                                string group = xmlReader.ReadString();
                                if ( !String.IsNullOrEmpty( group ) )
                                {
                                    if ( !expandedGroups.ContainsKey( group ) )
                                    {
                                        expandedGroups.Add( group, true );
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        public static void ClearStatics()
        {
            sm_expandedGroups.Clear();
            sm_hiddenBanks.Clear();
        }


        public static void SetSortedGroup( string guid, string path, bool sorted )
        {
            Dictionary<string, bool> sortedGroups;
            if ( !sm_sortedGroups.TryGetValue( guid, out sortedGroups ) )
            {
                sortedGroups = new Dictionary<string, bool>();
                sm_sortedGroups.Add( guid, sortedGroups );
            }

            sortedGroups[path] = sorted;
        }


        public static bool IsSortedGroup( string guid, string path )
        {
            Dictionary<string, bool> sortedGroups;
            if ( sm_sortedGroups.TryGetValue( guid, out sortedGroups ) )
            {
                bool sorted;
                if ( sortedGroups.TryGetValue( path, out sorted ) )
                {
                    return sorted;
                }
            }

            return false;
        }


        #endregion

        #region Virtual Functions
        protected virtual int GetTreeViewImageIndex() { return 1; }


        public override bool IsModified()
        {
            lock (m_List)
            {
                foreach (var item in m_List)
                {
                    if (item.IsModified())
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        #endregion

        #region Public Functions
        public void xpander_ReleaseGroup( object sender, EventArgs e )
        {
            ReleaseGroup();
        }

        public void ReleaseGroup()
        {
            m_Open--;
            m_PrevOpen++;

            Debug.Assert( m_Open >= 0, "m_Open >= 0" );
            UpdateRemote();

            // go up through all the parent groups, make sure they're reference counted:
            WidgetGroupBase parent = Parent as WidgetGroupBase;
            if ( parent != null )
            {
                parent.ReleaseGroup();
            }
        }

        public void xpander_AddRefGroup( object sender, EventArgs e )
        {
            AddRefGroup();
        }

        public void AddRefGroup()
        {
            m_Open++;
            m_PrevOpen--;

            UpdateRemote();

            // make sure we increment ref the count on the parent bank:
            WidgetGroupBase parent = Parent as WidgetGroupBase;
            if ( parent != null )
            {
                parent.AddRefGroup();
            }
        }

        public bool ContainsWidget( Widget widget )
        {
            lock (m_List)
            {
                return m_List.Contains(widget);
            }
        }

        public Widget FindFirstWidgetFromPath( string path )
        {
            if ( path == null || path == "" )
            {
                return null;
            }
            List<Widget> list = FindWidgetsFromPath( path );
            if ( list == null || list.Count == 0 )
            {
                return null;
            }
            return list[0] as Widget;
        }

        public List<Widget> FindWidgetsFromPath( string path )
        {
            lock (m_List)
            {
                return FindWidgetsFromPath(this, m_List, path);
            }
        }
        #endregion
    }

    /// <summary>
    /// This class represents a Bank, which is a top-level collection of Widgets.
    /// </summary>
    public class WidgetBank : WidgetGroupBase
    {
        public WidgetBank(BankPipe pipe, uint id, String title, String memo, bool open, Color fillColor, bool readOnly)
            : base(pipe, id, title, memo, open, fillColor, readOnly)
        {
            OnAddBankEvent();
        }

        #region Delegates
        public delegate void BankDelegate( BankPipe pipe, WidgetBank bank );
        #endregion

        #region Events
        static public event BankDelegate AddBankEvent;
        static public event BankDelegate RemoveBankEvent;
        #endregion

        #region Event Dispatchers
        private void OnAddBankEvent()
        {
            if ( WidgetBank.AddBankEvent != null )
            {
                WidgetBank.AddBankEvent( this.Pipe, this );
            }
        }

        private void OnRemoveBankEvent()
        {
            if ( WidgetBank.RemoveBankEvent != null )
            {
                WidgetBank.RemoveBankEvent( this.Pipe, this );
            }
        }
        #endregion

        #region Overrides
        public override void Destroy()
        {
            OnRemoveBankEvent();

            base.Destroy();
        }

        public override int GetWidgetTypeGUID() { return GetStaticGUID(); }
        public override bool IsBank() { return true; }

        protected override int GetTreeViewImageIndex() { return 0; }

        public override String CategoryDescription
        {
            get { return "Bank"; }
        }



        #endregion

        #region Static Functions
        public static int GetStaticGUID() { return ComputeGUID( 'b', 'a', 'n', 'k' ); }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="packet"></param>
        /// <param name="errorMessage"></param>
        private static bool RemoteHandler(BankRemotePacket packet, out String errorMessage)
        {
            bool handled = false;

            if ( packet.BankCommand == BankRemotePacket.EnumPacketType.CREATE )
            {
                // read packet:
                packet.Begin();
                uint remoteParent = packet.Read_bkWidget();
                String title = packet.Read_const_char();
                String memo = packet.Read_const_char();
                Color fillColor = Widget.DeserializeColor(packet.Read_const_char());
                bool readOnly = packet.Read_bool();
                int open = packet.Read_s32();
                packet.End();

                // Create the bank regardless of whether it has a parent or not.
                WidgetBank newWidget = new WidgetBank(packet.BankPipe, packet.Id, title, memo, open > 0, fillColor, readOnly);
                packet.BankPipe.Associate(packet.Id, newWidget);

                Widget parent;
                if (remoteParent != BankPipe.INVALID_ID)
                {
                    if (packet.BankPipe.RemoteToLocal(remoteParent, out parent, out errorMessage))
                    {
                        parent.AddChild(newWidget);
                        handled = true;
                    }
                }
                else
                {
                    errorMessage = null;
                    handled = true;
                }
            }
            else if (packet.BankCommand == BankRemotePacket.EnumPacketType.CHANGED)
            {
                handled = true;
                errorMessage = null;
            }
            else if (packet.BankCommand == BankRemotePacket.EnumPacketType.USER_0) 
            {
                // read packet:
                packet.Begin();
                int open = packet.Read_s32();
                packet.End();

                WidgetBank widget;
                if (packet.BankPipe.RemoteToLocal(packet.Id, out widget, out errorMessage))
                {
                    widget.m_Open = open;
                    widget.m_PrevOpen = -widget.m_PrevOpen;
                    widget.UpdateWindow();
                    handled = true;
                }
            }
            else
            {
                handled = Widget.RemoteHandlerBase(packet, GetStaticGUID(), out errorMessage);
            }

            return handled;
        }

        public static void AddHandlers( BankRemotePacketProcessor packetProcessor )
        {
            packetProcessor.AddType( GetStaticGUID(), new BankRemotePacketProcessor.HandlerDel( RemoteHandler ) );
        }

        public static void FindWidgetsWithMatchingString( IBankManager bankMgr, bool includeGroup, bool matchCase, bool matchWholeWord, bool exactMatch,
            bool checkHelpDesc, String text, ref List<Widget> list, int depthToSearch )
        {
            // find any nodes and add them to the find panel if the text matches:
            foreach ( WidgetBank bank in bankMgr.AllBanks )
            {
                bank.FindWidgetWithMatchingString( includeGroup, matchCase, matchWholeWord, exactMatch, checkHelpDesc, text, ref list, depthToSearch );
            }
        }
        #endregion
    }


    /// <summary>
    /// This class manages a collection of Widgets.
    /// </summary>
    public class WidgetGroup : WidgetGroupBase
    {
        private WidgetGroup(BankPipe pipe, uint id, String title, String memo, bool open, Color fillColor, bool readOnly)
            : base(pipe, id, title, memo, open, fillColor, readOnly)
        {

        }

        #region Overrides

        public override int GetWidgetTypeGUID() { return GetStaticGUID(); }
        public static int GetStaticGUID() { return ComputeGUID( 'g', 'r', 'u', 'p' ); }
        public override bool IsGroup() { return true; }

        public override String CategoryDescription
        {
            get { return "Group"; }
        }


        #endregion

        #region Static Functions
        /// <summary>
        /// 
        /// </summary>
        /// <param name="packet"></param>
        /// <returns></returns>
        private static bool RemoteHandler(BankRemotePacket packet, out String errorMessage)
        {
            bool handled = false;

            if ( packet.BankCommand == BankRemotePacket.EnumPacketType.CREATE )
            {
                // read packet:
                packet.Begin();
                uint remoteParent = packet.Read_bkWidget();
                String title = packet.Read_const_char();
                String memo = packet.Read_const_char();
                Color fillColor = Widget.DeserializeColor(packet.Read_const_char());
                bool readOnly = packet.Read_bool();
                int open = packet.Read_s32();

                packet.End();
                
                Widget parent;
                if (packet.BankPipe.RemoteToLocal(remoteParent, out parent, out errorMessage))
                {
                    WidgetGroup local = new WidgetGroup(packet.BankPipe, packet.Id, title, memo, open > 0, fillColor, readOnly);
                    packet.BankPipe.Associate(packet.Id, local);
                    parent.AddChild(local);
                    handled = true;
                }
            }
            else if (packet.BankCommand == BankRemotePacket.EnumPacketType.CHANGED)
            {
                handled = true;
                errorMessage = null;
            }
            else if (packet.BankCommand == BankRemotePacket.EnumPacketType.USER_0) 
            {
                // read packet:
                packet.Begin();
                int open = packet.Read_s32();
                packet.End();

                WidgetGroup widget;
                if (packet.BankPipe.RemoteToLocal(packet.Id, out widget, out errorMessage))
                {
                    widget.m_Open = open;
                    widget.m_PrevOpen = -widget.m_PrevOpen;
                    widget.UpdateWindow();
                    handled = true;
                }
            }
            else
            {
                handled = Widget.RemoteHandlerBase(packet, GetStaticGUID(), out errorMessage);
            }

            return handled;
        }

        public static void AddHandlers( BankRemotePacketProcessor packetProcessor )
        {
            packetProcessor.AddType( GetStaticGUID(), new BankRemotePacketProcessor.HandlerDel( RemoteHandler ) );
        }
        #endregion
    }
}

