using System;
using System.Drawing;
using System.IO;
using System.Text;

using ragCore;

namespace ragWidgets
{
	/// <summary>
	/// This is the widget representation for the image viewer.  It handles communication
	/// between the widget and the remote application.
	/// </summary>
	public class WidgetImageViewer : Widget
	{
		/// <summary>
		/// The constructor of the widget image viewer.
		/// </summary>
		/// <param name="pipe">The remote application communicates with the widget over this pipe.</param>
		/// <param name="title">The title of the widget.</param>
		/// <param name="memo">The help string that describes the purpose of this widget.</param>
        public WidgetImageViewer(BankPipe pipe, uint id, String title, String memo, Color fillColor, bool readOnly)
            : base(pipe, id, title, memo, fillColor, readOnly)
        {
        }

		#region Variables
		// this stores the current file name for this widget:
		private string m_ImageFile;
        private static readonly ASCIIEncoding Encoder = new ASCIIEncoding();
		#endregion

		#region Properties
		public string ImageFile
		{
			get
			{
				return m_ImageFile;
			}
            set
            {
                if ( m_ImageFile != value )
                {
                    m_ImageFile = value;

                    UpdateRemote();
                    OnValueChanged();
                }
            }
		}
		#endregion

		#region Statics
		/// <summary>
		/// A static version of GetWidgetTypeGUID().  Returns the integer version of a four letter
		/// string.
		/// </summary>
		/// <returns></returns>
		public static int GetStaticGUID()		
		{
			return ComputeGUID('i','m','g','v');
		}

		/// <summary>
		/// This function handles all communication with the remote application for this widget type.
		/// </summary>
		/// <param name="packet"></param>
        public static bool RemoteHandler(BankRemotePacket packet, out String errorMessage)
		{
            bool handled = false;

			// Usually every widget has handles the creation message.
			if (packet.BankCommand == BankRemotePacket.EnumPacketType.CREATE) 
			{
				// start reading the packet:
				packet.Begin();
							
				// read the values from the packet - these values are application defined:
				uint remoteParent		= packet.Read_bkWidget();
				String title			= packet.Read_const_char();
				String memo				= packet.Read_const_char();
                Color fillColor = Widget.DeserializeColor(packet.Read_const_char());
                bool readOnly = packet.Read_bool();
						
				// specify we're done reading the packet:
				packet.End();
                
                Widget parent;
                if (packet.BankPipe.RemoteToLocal(remoteParent, out parent, out errorMessage))
                {
                    WidgetImageViewer local = new WidgetImageViewer(packet.BankPipe, packet.Id, title, memo, fillColor, readOnly);
                    packet.BankPipe.Associate(packet.Id, local);
					parent.AddChild(local);
                    handled = true;
                }
			}
			// this event is called when the remote value has changed.  We let the 
			// let this widget know about the change by calling UpdateWindow():
			else if (packet.BankCommand == BankRemotePacket.EnumPacketType.CHANGED) 
			{
				// start reading the packet:
                packet.Begin();

				// find the local version of the the remote widget:
				WidgetImageViewer local;
                if (packet.BankPipe.RemoteToLocal(packet.Id, out local, out errorMessage))
                {
					// read our data (in this case, it's just a new string):                    
					local.m_ImageFile=packet.Read_const_char();

					// specify we're done reading this packet:
					packet.End();

					// update ui
                    local.OnValueChanged();
                    handled = true;
				}
			}
            else
            {
                handled = Widget.RemoteHandlerBase(packet, GetStaticGUID(), out errorMessage);
            }

            return handled;
		}

		/// <summary>
		/// This is called by the External class - it registers its remote handler with packet handling
		/// so this widget will get properly created.
		/// </summary>
		public static void AddHandlers( BankRemotePacketProcessor packetProcessor )
		{
			packetProcessor.AddType(GetStaticGUID(),new BankRemotePacketProcessor.HandlerDel(RemoteHandler));
		}
		#endregion

		#region Overrides
		/// <summary>
		/// This is a unique ID used to signify this widget type.  
		/// This usually calls the GetStaticGUID() function.
		/// </summary>
		/// <returns>An globally unique identifier for this widget type.</returns>
		public override int GetWidgetTypeGUID()
		{
			return GetStaticGUID();
		}

		/// <summary>
		/// This function should be called when the remote application needs to be updated with a new 
		/// value changed by the widget's visible UI.
		/// </summary>
		public override void UpdateRemote()
		{
            BankRemotePacket p = new BankRemotePacket( Pipe );
            p.Begin(BankRemotePacket.EnumPacketType.CHANGED, this.GetWidgetTypeGUID(), Id);
            p.Write_const_char( m_ImageFile );
            p.Send();
        }


		/// <summary>
		/// This property allows the widget type to specify if it allows the user to save his tuned value
		/// so the next time the user runs that application with RAG, it'll restore the saved value.
		/// </summary>
		public override bool AllowPersist
		{
			get
			{
				return false;
			}
		}


        public override string ProcessCommand(string[] args)
        {
            if (args.Length > 0 && args[0] == "-help")
            {
                return "TYPE: " + this.GetType().ToString() + "\n" +
                    "ARGUMENTS: [string value]\n\n" +
                    "\t[string value]       the image file name to set";
            }

            if (args.Length == 0)
            {
                return ImageFile;
            }

            if (!(args.Length == 1))
            {
                return "Incorrect # of arguments: only one argument required for image name";
            }

            ImageFile = args[0];

            return "";
        }

        public override void ReceiveData(byte[] bytes)
        {
            if (bytes.Length != 0)
                return;

            string imageFile = Encoder.GetString(bytes, 0, bytes.Length);
            ImageFile = imageFile;
        }

        public override void SendData(Stream stream)
        {
            string value = ImageFile;
            byte[] lengthBytes = BitConverter.GetBytes(value.Length);
            stream.Write(BitConverter.GetBytes(value.Length), 0, lengthBytes.Length);

            byte[] buffer = Encoder.GetBytes(value);
            stream.Write(buffer, 0, buffer.Length);
        }

		#endregion

        #region Public Functions
        #endregion
    }
}
