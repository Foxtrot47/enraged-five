using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Text;
using System.Xml;

using ragCore;

namespace ragWidgets
{
	/// <summary>
	/// Summary description for WidgetSliderInt.
	/// </summary>
	public class WidgetCombo : Widget
	{
		protected enum EnumType 
		{
			U8,
			S8,
			U16,
			S16,
			U32,
			S32
		};

        private WidgetCombo(BankPipe pipe, uint id, EnumType type, String title, String memo, long value, long count, long offset, Color fillColor, bool readOnly)
            : base(pipe, id, title, memo, fillColor, readOnly)
		{
			m_Type=type;
			m_Value = value;
			m_DefaultValue = value;
			m_Count=count;
			m_Offset=offset;
		}

		public override int GetWidgetTypeGUID() 
		{
            return GetStaticGUID(m_Type);
		}

        protected static int GetStaticGUID(EnumType type)
        {
            if (type == EnumType.U8)
                return ComputeGUID('c', 'o', 'u', '8');
            else if (type == EnumType.S8)
                return ComputeGUID('c', 'o', 's', '8');
            else if (type == EnumType.U16)
                return ComputeGUID('c', 'o', 'u', '1');
            else if (type == EnumType.S16)
                return ComputeGUID('c', 'o', 's', '1');
            else if (type == EnumType.U32)
                return ComputeGUID('c', 'o', 'u', '3');
            else if (type == EnumType.S32)
                return ComputeGUID('c', 'o', 's', '3');
            else
            {
                Debug.Fail("Unknown type " + type);
                return 0;
            }
        }

		public override void Serialize(XmlTextWriter writer,String path) 
		{
			if (SerializeStart(writer,ref path))
			{
				writer.WriteAttributeString("Value",m_Value.ToString());
				writer.WriteEndElement();
			}
		}

		public override void DeSerialize(XmlTextReader reader) 
		{
			m_Value=Convert.ToInt64(reader["Value"]);

			DeSerializeShared();
		} 

		public override bool AllowPersist {get {return true;}}

        public override bool SerializeToJSON( LitJson.JsonData data )
        {
            data["value"] = m_Items[(int)Value];
            if ( IsModified() )
            {
                data["default"] = m_Items[(int)m_DefaultValue];
            }

            return true;
        }

        public override void DeSerializeFromJSON( LitJson.JsonData data )
        {
            int index = m_Items.IndexOf( (string)data["value"] );
            Value = index;
        }

		public override string ProcessCommand(string[] args)
		{
			if (args.Length>0 && args[0]=="-help")
			{
				return "TYPE: " + this.GetType().ToString() + "\n" +
					"ARGUMENTS: [combo selection]\n\n" +
					"\t[combo selection]     the string to select";
			}

            if (args.Length == 0)
            {
                return m_Items[(int)Value];
            }

			if (args.Length!=1)
			{
				return "Incorrect # of arguments: pass in one string for the combo item that you wish to select";
			}

			for (int i=0;i<m_Items.Count;i++)
			{
				string item=m_Items[i] as String;
				if (item.ToLower()==args[0].ToLower())
				{
					Value = i - m_Offset;
					return "";
				}
			}

			// didn't find the item:
			return "couldn't find item '" + args[0] + "'";
		}

        public override void ReceiveData(byte[] bytes)
        {
            if (bytes.Length ==  0)
                return;

            string itemString = Encoder.GetString(bytes, 0, bytes.Length);
            for (int i = 0; i < m_Items.Count; i++)
            {
                string item = m_Items[i] as String;
                if (item.ToLower() == itemString.ToLower())
                {
                    Value = i - m_Offset;
                    return;
                }
            }
        }

        public override void SendData(Stream stream)
        {
            string value = m_Items[(int)Value];
            byte[] lengthBytes = BitConverter.GetBytes(value.Length);
            stream.Write(BitConverter.GetBytes(value.Length), 0, lengthBytes.Length);

            byte[] buffer = Encoder.GetBytes(value);
            stream.Write(buffer, 0, buffer.Length);
        }

        public override bool IsModified()
        {
            return m_DefaultValue != m_Value;
        }

        public override void ResetToDefault()
        {
            Value = m_DefaultValue;
        }


		public override void UpdateRemote()
		{
			BankRemotePacket p = new BankRemotePacket(Pipe);
            p.Begin(BankRemotePacket.EnumPacketType.CHANGED, GetWidgetTypeGUID(), Id);
			switch(m_Type) 
			{
				case EnumType.U8:	p.Write_u8((byte)m_Value);				break;
				case EnumType.S8:	p.Write_s8((sbyte)m_Value);				break;
				case EnumType.U16:	p.Write_u16((ushort)m_Value);			break;
				case EnumType.S16:	p.Write_s16((short)m_Value);			break;
				case EnumType.U32:	p.Write_u32((uint)m_Value);				break;
				case EnumType.S32:	p.Write_s32((int)m_Value);				break;
				default:	Debug.Fail("unknown combo type " + m_Type);		break;
			}
			p.Send();
		}


        public static bool RemoteHandler_U8(BankRemotePacket packet, out String errorMessage)
        {
            return RemoteHandler(packet, out errorMessage, EnumType.U8);
        }
        public static bool RemoteHandler_S8(BankRemotePacket packet, out String errorMessage)
        {
            return RemoteHandler(packet, out errorMessage, EnumType.S8);
        }
        public static bool RemoteHandler_U16(BankRemotePacket packet, out String errorMessage)
        {
            return RemoteHandler(packet, out errorMessage, EnumType.U16);
        }
        public static bool RemoteHandler_S16(BankRemotePacket packet, out String errorMessage)
        {
            return RemoteHandler(packet, out errorMessage, EnumType.S16);
        }
        public static bool RemoteHandler_U32(BankRemotePacket packet, out String errorMessage)
        {
            return RemoteHandler(packet, out errorMessage, EnumType.U32);
        }
        public static bool RemoteHandler_S32(BankRemotePacket packet, out String errorMessage)
        {
            return RemoteHandler(packet, out errorMessage, EnumType.S32);
        }

		private static bool RemoteHandler(BankRemotePacket packet, out String errorMessage, EnumType type)
        {
            bool handled = false;

			if (packet.BankCommand == BankRemotePacket.EnumPacketType.CREATE) 
			{
				packet.Begin();
				
				uint remoteParent		= packet.Read_bkWidget();
				String title			= packet.Read_const_char();
				String memo				= packet.Read_const_char();
                Color fillColor = Widget.DeserializeColor(packet.Read_const_char());
                bool readOnly = packet.Read_bool();
				
				long data=0;

				switch(type) 
				{
					case EnumType.U8:	
						data=packet.Read_u8();		
						break;
					case EnumType.S8:	
						data=packet.Read_s8();		
						break;
					case EnumType.U16:	
						data=packet.Read_u16();		
						break;
					case EnumType.S16:	
						data=packet.Read_s16();		
						break;
					case EnumType.U32:	
						data=packet.Read_u32();		
						break;
					case EnumType.S32:	
						data=packet.Read_s32();		
						break;
					default:
						Debug.Fail("unknown combo type " + type);
						break;
				}
				
				long count = packet.Read_s32();
				long offset = packet.Read_s32();

				packet.End();
                
                Widget parent;
                if (packet.BankPipe.RemoteToLocal(remoteParent, out parent, out errorMessage))
                {
                    WidgetCombo newWidget = new WidgetCombo(packet.BankPipe, packet.Id, type, title, memo, data, count, offset, fillColor, readOnly);
                    packet.BankPipe.Associate(packet.Id, newWidget);
                    parent.AddChild(newWidget);
                    handled = true;
                }
			}
			else if (packet.BankCommand == BankRemotePacket.EnumPacketType.CHANGED) 
			{
                packet.Begin();
                
                WidgetCombo widget;
                if (packet.BankPipe.RemoteToLocal(packet.Id, out widget, out errorMessage))
                {
                    if (widget.m_Type != type)
                    {
                        errorMessage = String.Format("Type mismatch.  Expected {0} but received {1}.", widget.m_Type, type);
                    }
                    else
                    {
                        switch (widget.m_Type)
                        {
                            case EnumType.U8:
                                widget.m_Value = widget.m_PrevValue = packet.Read_u8();
                                break;
                            case EnumType.S8:
                                widget.m_Value = widget.m_PrevValue = packet.Read_s8();
                                break;
                            case EnumType.U16:
                                widget.m_Value = widget.m_PrevValue = packet.Read_u16();
                                break;
                            case EnumType.S16:
                                widget.m_Value = widget.m_PrevValue = packet.Read_s16();
                                break;
                            case EnumType.U32:
                                widget.m_Value = widget.m_PrevValue = packet.Read_u32();
                                break;
                            case EnumType.S32:
                                widget.m_Value = widget.m_PrevValue = packet.Read_s32();
                                break;
                            default:
                                // Internal error
                                Debug.Fail("Unknown combo type " + widget.m_Type);
                                throw new ArgumentException("Unknown combo widget type.");
                        }
                        packet.End();
                        widget.OnValueChanged();
                        handled = true;
                    }
                }
			}
			else if (packet.BankCommand == BankRemotePacket.EnumPacketType.USER_0) 
			{
                // USER_0: Update a single item in the combo.
                packet.Begin();
				int i = packet.Read_s32();
				String data = packet.Read_const_char();
				packet.End();

                WidgetCombo widget;
                if (packet.BankPipe.RemoteToLocal(packet.Id, out widget, out errorMessage))
                {
                    widget.SetItem(i, data);
                    if (i + 1 == widget.m_Count)
                    {
                        widget.OnValueChanged();
                    }
                    handled = true;
                }
			}
			else if ( packet.BankCommand == BankRemotePacket.EnumPacketType.USER_1 )
            {
                // USER_1: Update the entire combo.
				packet.Begin();
                
                WidgetCombo widget;
                if (packet.BankPipe.RemoteToLocal(packet.Id, out widget, out errorMessage))
                {
                    // read title
                    widget.m_Title = packet.Read_const_char();

                    // read tool tip
                    widget.m_Memo = packet.Read_const_char();

                    // read value
                    switch (type)
                    {
                        case EnumType.U8:
                            widget.m_Value = packet.Read_u8();
                            break;
                        case EnumType.S8:
                            widget.m_Value = packet.Read_s8();
                            break;
                        case EnumType.U16:
                            widget.m_Value = packet.Read_u16();
                            break;
                        case EnumType.S16:
                            widget.m_Value = packet.Read_s16();
                            break;
                        case EnumType.U32:
                            widget.m_Value = packet.Read_u32();
                            break;
                        case EnumType.S32:
                            widget.m_Value = packet.Read_s32();
                            break;
                        default:
                            // Internal error
                            Debug.Fail("Unknown combo type " + widget.m_Type);
                            throw new ArgumentException("Unknown combo widget type.");
                    }

                    long lastCount = widget.m_Count;

                    // read item count
                    widget.m_Count = packet.Read_s32();

                    // read offset
                    widget.m_Offset = packet.Read_s32();

                    packet.End();

                    // clear the items
                    widget.m_Items.Clear();

                    if (widget.m_Count < lastCount)
                    {
                        widget.OnValueChanged();
                    }
                    else if (widget.m_Count <= 0)
                    {
                        widget.OnValueChanged();
                    }

                    handled = true;
                }
			}
            else
            {
                handled = Widget.RemoteHandlerBase(packet, GetStaticGUID(type), out errorMessage);
            }

            return handled;
		}

		public List<string> Items {get {return m_Items;}}
		public long Value
        {
			get { return m_Value - m_Offset; }
			set 
            {
				if (m_Value != value + m_Offset) 
                {
					m_Value = value + m_Offset;
					UpdateRemote();
					OnValueChanged();
				}
			}
		}



		public long Count	{get {return m_Count;}}
		public long Offset	{get {return m_Offset;}}

		private EnumType	m_Type;
		private long		m_Value;
		private long        m_DefaultValue;
		private long		m_PrevValue;
		private long		m_Count;
		private long		m_Offset;
        private List<string> m_Items = new List<string>();

        public class WidgetComboItemEventArgs : EventArgs
        {
            public WidgetComboItemEventArgs( Widget widget, int item, string data )
            {
                Widget = widget;
                Item = item;
                Data = data;
            }

            #region Variables
            public Widget Widget;
            public int Item;
            public string Data;
            #endregion
        };

        public delegate void WidgetItemEventHandler( object sender, WidgetComboItemEventArgs e );
        public event WidgetItemEventHandler WidgetItemSet;
        private static readonly ASCIIEncoding Encoder = new ASCIIEncoding();

        private void SetItem( int item, String str )
        {
            while ( m_Items.Count < item + 1 )
            {
                m_Items.Add( "<no name>" );
            }

            if ( str != null )
            {
                m_Items[item] = str;
            }

            if (WidgetItemSet != null)
            {
                WidgetItemSet( this, new WidgetComboItemEventArgs( this, item, str ) );
            }
        }

		public static void AddHandlers( BankRemotePacketProcessor packetProcessor )
		{
			packetProcessor.AddType(ComputeGUID('c','o','u','8'),new BankRemotePacketProcessor.HandlerDel(RemoteHandler_U8));
			packetProcessor.AddType(ComputeGUID('c','o','s','8'),new BankRemotePacketProcessor.HandlerDel(RemoteHandler_S8));
			packetProcessor.AddType(ComputeGUID('c','o','u','1'),new BankRemotePacketProcessor.HandlerDel(RemoteHandler_U16));
			packetProcessor.AddType(ComputeGUID('c','o','s','1'),new BankRemotePacketProcessor.HandlerDel(RemoteHandler_S16));
			packetProcessor.AddType(ComputeGUID('c','o','u','3'),new BankRemotePacketProcessor.HandlerDel(RemoteHandler_U32));
			packetProcessor.AddType(ComputeGUID('c','o','s','3'),new BankRemotePacketProcessor.HandlerDel(RemoteHandler_S32));
		}
	}
}
