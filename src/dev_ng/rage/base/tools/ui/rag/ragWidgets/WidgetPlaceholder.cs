using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

using ragCore;

namespace ragWidgets
{
    public class WidgetPlaceholder : WidgetTitle
    {
        public WidgetPlaceholder( string actualPath, Color fillColor )
            : base(new BankPipe(), BankPipe.INVALID_ID, actualPath + " (not found)", null, fillColor, true)
        {
            m_fullPath.AddRange( actualPath.Split( new char[] { '\\' } ) );
        }

        public WidgetPlaceholder( List<string> fullPath, Color fillColor )
            : base(new BankPipe(), BankPipe.INVALID_ID, string.Empty, null, fillColor, true)
        {
            m_fullPath = fullPath;

            m_Title = this.ActualPath + " (not found)";
            m_String = m_Title;
        }

        #region Variables
        private List<string> m_fullPath = new List<string>();
        #endregion

        #region Properties
        public string ActualPath
        {
            get
            {
                return BuildFullPath( m_fullPath );
            }
        }

        public List<string> ActualFullPath
        {
            get
            {
                return m_fullPath;
            }
        }
        #endregion

        #region Static Functions
        public static string BuildFullPath( List<string> path )
        {
            StringBuilder fullPath = new StringBuilder();

            if ( (path != null) && (path.Count > 0) )
            {
                fullPath.Append( path[0] );

                for ( int i = 1; i < path.Count; ++i )
                {
                    fullPath.Append( @"\" );
                    fullPath.Append( path[i] );
                }
            }

            return fullPath.ToString();
        }
        #endregion
    }
}
