using System;
using System.Drawing;
using System.Xml;

using ragCore;

namespace ragWidgets
{
	/// <summary>
	/// Summary description for WidgetButton.
	/// </summary>
	public class WidgetButton : Widget
	{
        protected WidgetButton(BankPipe pipe, uint id, String title, String memo, Color fillColor, bool readOnly)
            : base(pipe, id, title, memo, fillColor, readOnly)
		{
		}

		public override int GetWidgetTypeGUID() {return GetStaticGUID();}
		public static int GetStaticGUID()		{return ComputeGUID('b','u','t','n');}

		public override void UpdateRemote()
		{
			BankRemotePacket p=new BankRemotePacket(Pipe);
			p.Begin(BankRemotePacket.EnumPacketType.CHANGED,GetStaticGUID(),Id); 
			p.Send();
		}
		

		public override void Serialize(XmlTextWriter writer,string path) {}
		public override void DeSerialize(XmlTextReader reader) {} 
		public override bool AllowPersist {get {return false;}}

		public override string ProcessCommand(string[] args)
		{
			if (args.Length>0 && args[0]=="-help")
			{
				return "TYPE: " + this.GetType().ToString() + "\n\n" +
					"takes no arguments, simulates a button press.";
			}

			// simulate a button press:
			UpdateRemote();
			return "";
		}

        public override void ReceiveData(byte[] args)
        {
            UpdateRemote();
        }

        public static bool RemoteHandler(BankRemotePacket packet, out String errorMessage)
		{
            bool handled = false;

			if (packet.BankCommand == BankRemotePacket.EnumPacketType.CREATE) 
			{
				packet.Begin();
							
				uint remoteParent		= packet.Read_bkWidget();
				String title			= packet.Read_const_char();
				String memo				= packet.Read_const_char();
                Color fillColor = Widget.DeserializeColor(packet.Read_const_char());
                bool readOnly = packet.Read_bool();
							
				packet.End();

                Widget parent;
                if (packet.BankPipe.RemoteToLocal(remoteParent, out parent, out errorMessage))
                {
                    WidgetButton newWidget = new WidgetButton(packet.BankPipe, packet.Id, title, memo, fillColor, readOnly);
                    packet.BankPipe.Associate(packet.Id, newWidget);
                    parent.AddChild(newWidget);
                    handled = true;
                }
			}
			else if (packet.BankCommand == BankRemotePacket.EnumPacketType.CHANGED) 
			{
				packet.Begin();
				packet.End();
                
                WidgetButton widget;
                if (packet.BankPipe.RemoteToLocal(packet.Id, out widget, out errorMessage))
                {
                    // Nothing to do.
                    handled = true;
                }
			}
            else
            {
                handled = Widget.RemoteHandlerBase(packet, GetStaticGUID(), out errorMessage);
            }

            return handled;
		}

		public static void AddHandlers( BankRemotePacketProcessor packetProcessor )
		{
			packetProcessor.AddType(GetStaticGUID(),new BankRemotePacketProcessor.HandlerDel(RemoteHandler));
		}

		public void Activate()
        {
			UpdateRemote();
		}
	}
}
