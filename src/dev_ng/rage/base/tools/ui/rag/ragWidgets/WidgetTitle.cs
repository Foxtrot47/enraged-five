using System;
using System.Drawing;
using System.Xml;

using ragCore;

namespace ragWidgets
{
	/// <summary>
	/// Summary description for WidgetTitle.
	/// </summary>
	public class WidgetTitle : Widget
	{
        protected WidgetTitle(BankPipe pipe, uint id, String title, String memo, Color fillColor, bool readOnly)
            : base(pipe, id, title, null, fillColor, readOnly)
		{
			m_String=title;
		}

		public override int GetWidgetTypeGUID() {return GetStaticGUID();}
		public static int GetStaticGUID()		{return ComputeGUID('t','i','t','l');}

		public override void Serialize(XmlTextWriter writer,string path) {}
		public override void DeSerialize(XmlTextReader reader) {} 

		public override bool AllowPersist {get {return false;}}

		public override void UpdateRemote()
		{
		}

		public static bool RemoteHandler(BankRemotePacket packet, out String errorMessage)
		{
            bool handled = false;

			if (packet.BankCommand == BankRemotePacket.EnumPacketType.CREATE) 
			{
				packet.Begin();

				uint remoteParent		= packet.Read_bkWidget();
				String title			= packet.Read_const_char();
                String memo             = packet.Read_const_char();
                Color fillColor = Widget.DeserializeColor(packet.Read_const_char());
                bool readOnly = packet.Read_bool();
				packet.End();

                Widget parent;
                if (packet.BankPipe.RemoteToLocal(remoteParent, out parent, out errorMessage))
                {
                    WidgetTitle newWidget = new WidgetTitle(packet.BankPipe, packet.Id, title, memo, fillColor, readOnly);
                    packet.BankPipe.Associate(packet.Id, newWidget);
                    parent.AddChild(newWidget);
                    handled = true;
                }
			}
            else
            {
                handled = Widget.RemoteHandlerBase(packet, GetStaticGUID(), out errorMessage);
            }

            return handled;
		}

		public static void AddHandlers( BankRemotePacketProcessor packetProcessor )
		{
			packetProcessor.AddType(GetStaticGUID(),new BankRemotePacketProcessor.HandlerDel(RemoteHandler));
		}

		public String String 
		{
			get {return m_String;}
		}

		protected String	m_String;
	}
}
