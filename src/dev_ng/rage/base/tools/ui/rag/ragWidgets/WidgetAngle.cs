using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Text;
using System.Xml;

using ragCore;

namespace ragWidgets
{
    public class WidgetAngle : Widget
    {
        public WidgetAngle(BankPipe pipe, uint id, String title, String memo, Color fillColor, bool readOnly, 
            AngleType angleType, float[] val, float min, float max )
            : base(pipe, id, title, memo, fillColor, readOnly)
        {
            m_angleType = angleType;
            m_value = val;
            m_DefaultValue = (float[])m_value.Clone();
            m_minValue = min;
            m_maxValue = max;
        }

        #region Enums
        public enum AngleType
        {
            Degrees,
            Fraction,
            Radians,
            Vector2,
        }
        #endregion

        #region Variables
        private float[] m_value = new float[2];
        private float[] m_DefaultValue = new float[2];
        private float m_minValue;
        private float m_maxValue;
        private AngleType m_angleType;
        #endregion

        #region Properties
        public float[] Value
        {
            get
            {
                return m_value;
            }
            set
            {
                float[] val = (value != null) ? value : new float[2];
                for ( int i = 0; i < m_value.Length && i < val.Length; ++i )
                {
                    if ( m_value[i] != val[i] )
                    {
                        m_value = val;
                        UpdateRemote();
                        OnValueChanged();
                        break;
                    }
                }
            }
        }

        public AngleType DisplayType
        {
            get
            {
                return m_angleType;
            }
        }

        public float MinValue
        {
            get
            {
                return m_minValue;
            }
        }

        public float MaxValue
        {
            get
            {
                return m_maxValue;
            }
        }
        #endregion


        #region Overrides
        public override bool AllowPersist
        {
            get
            {
                return true;
            }
        }

        public override String HelpTitle
        {
            get
            {
                StringBuilder help = new StringBuilder( this.Title );
                switch ( m_angleType )
                {
                    case AngleType.Degrees:
                        help.Append( " (degrees)" );
                        break;
                    case AngleType.Fraction:
                        help.Append( " (0.0 to 1.0)" );
                        break;
                    case AngleType.Radians:
                        help.Append( " (radians)" );
                        break;
                    default:
                        break;
                }
                
                return help.ToString();
            }
        }

        public override int GetWidgetTypeGUID()
        {
            return GetStaticGUID(); 
        }

        public override void Serialize( XmlTextWriter writer, String path )
        {
            if ( SerializeStart( writer, ref path ) )
            {
                for ( int i = 0; i < m_value.Length; ++i )
                {
                    writer.WriteAttributeString( "Value" + i, m_value[i].ToString() );
                }

                writer.WriteEndElement();
            }
        }

        public override void DeSerialize( XmlTextReader reader )
        {
            for ( int i = 0; i < m_value.Length; ++i )
            {
                m_value[i] = Convert.ToSingle( reader["Value" + i] );
            }

            DeSerializeShared();
        }


        public override bool SerializeToJSON( LitJson.JsonData data )
        {
            LitJson.JsonData jsonList = new LitJson.JsonData();
            foreach (var v in m_value)
            {
                jsonList.Add( v );
            }

            data["value"] = jsonList;

            if ( IsModified() )
            {
                data["default"] = string.Join(", ", m_DefaultValue);
            }
            return true;
        }

        public override void DeSerializeFromJSON( LitJson.JsonData data )
        {
            float[] value = new float[2];
            int i = 0;
            foreach (LitJson.JsonData o in data["value"])
            {
                value[i++] = (float)(Double)o;
            }

            Value = value; 
        }

        public override void UpdateRemote()
        {
            BankRemotePacket p = new BankRemotePacket( Pipe );
            p.Begin(BankRemotePacket.EnumPacketType.CHANGED, GetStaticGUID(), Id);
            
            p.Write_float( m_value[0] );
            if ( m_angleType == AngleType.Vector2 )
            {
                p.Write_float( m_value[1] );
            }

            p.Send();
        }

        public override string ProcessCommand(string[] args)
        {
            if (args.Length > 0 && args[0] == "-help")
            {
                return "TYPE: " + this.GetType().ToString() + "\n" +
                    "ARGUMENTS: [float value]\n\n" +
                    "\t[float value] [,float value]     the angle value(s) to set";
            }

            if (args.Length == 0)
            {
                if (m_angleType == AngleType.Vector2)
                {
                    return String.Format("{0:r} {1:r}", m_value[0], m_value[1]);
                }
                else
                {
                    return String.Format("{0:r}", m_value[0]);
                }
            }

            if (! (args.Length == 1 || (args.Length == 2 && m_angleType == AngleType.Vector2)))
            {
                return "Incorrect # of arguments: only one or two argument required for angle slider";
            }

            float[] newValue = new float[2];

            try
            {
                newValue[0] = Convert.ToSingle(args[0]);
                if (args.Length == 2)
                {
                    newValue[1] = Convert.ToSingle(args[1]);
                }
            }
            catch (System.Exception e)
            {
                return e.Message;
            }

            // TODO: Range check - what are the ranges for the angle widgets? 0 -> 360? -180 -> 180? -360 -> 360?

            Value = newValue;

            return "";
        }

        public override void ReceiveData(byte[] bytes)
        {
            //Parse the data size.
            int offset = 0;
            int argCount = bytes.Length / sizeof(int);  //Number of integers in this package.
            if (!(argCount == 1 || (argCount == 2 && m_angleType == AngleType.Vector2)))
                return;

            float[] newValue = new float[2];

            try
            {
                newValue[0] = BitConverter.ToInt32(bytes, offset); 
                if (argCount == 2)
                {
                    offset += sizeof(int);
                    newValue[1] = BitConverter.ToInt32(bytes, offset); 
                }
            }
            catch (System.Exception)
            {
                return;
            }

            Value = newValue;
        }

        public override void SendData(Stream stream)
        {
            if (m_angleType == AngleType.Vector2)
            {
                int dataSize = 2 * sizeof(float);
                byte[] buffer = BitConverter.GetBytes(dataSize);
                stream.Write(buffer, 0, buffer.Length);

                buffer = BitConverter.GetBytes(m_value[0]);
                stream.Write(buffer, 0, buffer.Length);

                buffer = BitConverter.GetBytes(m_value[1]);
                stream.Write(buffer, 0, buffer.Length);
            }
            else
            {
                int dataSize = 1 * sizeof(float);
                byte[] buffer = BitConverter.GetBytes(dataSize);
                stream.Write(buffer, 0, buffer.Length);

                buffer = BitConverter.GetBytes(m_value[0]);
                stream.Write(buffer, 0, buffer.Length);
            }
        }

        public override bool IsModified()
        {
            if (m_value.Length != m_DefaultValue.Length)
            {
                return true;
            }
            for (int i = 0; i < m_value.Length; i++)
            {
                if (m_value[i] != m_DefaultValue[i])
                {
                    return true;
                }
            }
            return false;
        }

        public override void ResetToDefault()
        {
            Value = (float[])m_DefaultValue.Clone();
        }


        #endregion


        #region Public Static Functions
        public static int GetStaticGUID()
        {
            return ComputeGUID( 'a', 'n', 'g', 'l' );
        }

        public static void AddHandlers( BankRemotePacketProcessor packetProcessor )
        {
            packetProcessor.AddType( GetStaticGUID(), new BankRemotePacketProcessor.HandlerDel( RemoteHandler ) );
        }

        public static bool RemoteHandler(BankRemotePacket packet, out String errorMessage)
        {
            bool handled = false;

            if ( packet.BankCommand == BankRemotePacket.EnumPacketType.CREATE )
            {
                packet.Begin();

                uint remoteParent = packet.Read_bkWidget();
                String title = packet.Read_const_char();
                String memo = packet.Read_const_char();
                Color fillColor = Widget.DeserializeColor( packet.Read_const_char() );
                bool readOnly = packet.Read_bool();
                AngleType angleType = (AngleType)packet.Read_s32();
                float min = packet.Read_float();
                float max = packet.Read_float();

                float[] val = new float[2];
                val[0] = packet.Read_float();
                if ( angleType == AngleType.Vector2 )
                {
                    val[1] = packet.Read_float();
                }

                packet.End();

                Widget parent;
                if (packet.BankPipe.RemoteToLocal(remoteParent, out parent, out errorMessage))
                {
                    WidgetAngle newWidget = new WidgetAngle(packet.BankPipe, packet.Id, title, memo, fillColor, readOnly, angleType, val, min, max);
                    packet.BankPipe.Associate(packet.Id, newWidget);
                    parent.AddChild(newWidget);
                    handled = true;
                }
            }
            else if ( packet.BankCommand == BankRemotePacket.EnumPacketType.CHANGED )
            {
                packet.Begin();

                WidgetAngle widget;
                if (packet.BankPipe.RemoteToLocal(packet.Id, out widget, out errorMessage))
                {
                    float[] val = new float[2];
                    val[0] = packet.Read_float();
                    if (widget.m_angleType == AngleType.Vector2)
                    {
                        val[1] = packet.Read_float();
                    }
                    packet.End();

                    widget.Value = val;
                    handled = true;
                }
            }
            else
            {
                handled = Widget.RemoteHandlerBase(packet, GetStaticGUID(), out errorMessage);
            }

            return handled;
        }
        #endregion
    }
}
