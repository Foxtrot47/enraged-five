using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

using ragCore;

namespace ragWidgets
{
    public class WidgetSeparator : Widget
    {
        protected WidgetSeparator(BankPipe pipe, uint id, String title, String memo, Color fillColor, bool readOnly)
            : base(pipe, id, title, memo, fillColor, readOnly)
		{

        }

        #region Overrides
        public override bool AllowPersist
        {
            get
            {
                return false;
            }
        }

        public override int GetWidgetTypeGUID()
        {
            return WidgetSeparator.GetStaticGUID();
        }


        public override void UpdateRemote()
        {
            // do nothing
        }

        public override void FindWidgetWithMatchingString( bool includeGroup, bool matchCase, bool matchWholeWord, bool exactMatch, bool checkHelpDesc, string text, ref List<Widget> list, int depthToSearch )
        {
            // do nothing, we don't want this widget showing up in any searches
        }
        #endregion
            

        #region Static Functions
        public static int GetStaticGUID()
        {
            return ComputeGUID( 's', 'e', 'p', 'r' );
        }

        public static bool RemoteHandler(BankRemotePacket packet, out String errorMessage)
        {
            bool handled = false;

            if ( packet.BankCommand == BankRemotePacket.EnumPacketType.CREATE )
            {
                packet.Begin();

                uint remoteParent = packet.Read_bkWidget();
                String title = packet.Read_const_char();
                String memo = packet.Read_const_char();
                Color fillColor = Widget.DeserializeColor(packet.Read_const_char());
                bool readOnly = packet.Read_bool();

                packet.End();
                
                Widget parent;
                if (packet.BankPipe.RemoteToLocal(remoteParent, out parent, out errorMessage))
                {
                    WidgetSeparator local = new WidgetSeparator(packet.BankPipe, packet.Id, title, memo, fillColor, readOnly);
                    packet.BankPipe.Associate(packet.Id, local);
                    parent.AddChild(local);
                    handled = true;
                }
            }
            else
            {
                handled = Widget.RemoteHandlerBase(packet, GetStaticGUID(), out errorMessage);
            }

            return handled;
        }

        public static void AddHandlers( BankRemotePacketProcessor packetProcessor )
        {
            packetProcessor.AddType( WidgetSeparator.GetStaticGUID(), new BankRemotePacketProcessor.HandlerDel( WidgetSeparator.RemoteHandler ) );
        }
        #endregion
    }
}
