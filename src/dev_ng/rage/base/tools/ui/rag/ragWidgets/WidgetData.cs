using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Text;
using ragCore;


namespace ragWidgets
{
    public class WidgetData : Widget
    {
        protected WidgetData(BankPipe pipe, uint id, String title, String memo, ushort maxLength, bool showDataView, Color fillColor, bool readOnly)
            : base(pipe, id, title, memo, fillColor, readOnly)
		{
            m_maxLength = maxLength;
            m_showDataView = showDataView;
        }

        #region Variables
        private byte[] m_data;
        private ushort m_maxLength;
        private bool m_showDataView;
        #endregion

        #region Properties
        public ushort MaxLength
        {
            get
            {
                return m_maxLength;
            }
        }

        public byte[] Data
        {
            get
            {
                return m_data;
            }
            set
            {
                m_data = value;
            }
        }

        public bool ShowDataView
        {
            get
            {
                return m_showDataView;
            }
        }
        #endregion

        #region Delegates
        public delegate void DataChangedEventHandler( object sender, WidgetDataChangedEventArgs e );
        #endregion

        #region EventArgs
        public class WidgetDataChangedEventArgs : EventArgs
        {
            public WidgetDataChangedEventArgs( ushort offset, ushort count, bool allocatedData )
            {
                this.Offset = offset;
                this.Count = count;
                this.AllocatedData = allocatedData;
            }

            public ushort Offset;
            public ushort Count;
            public bool AllocatedData;
        }
        #endregion

        #region Events
        /// <summary>
        /// Called after each packet is received and processed.  If the Data is longer
        /// than 504 bytes, additional packets may be received.
        /// </summary>
        public event DataChangedEventHandler DataReceived;

        /// <summary>
        /// Called after all the data has been received.
        /// </summary>
        public event EventHandler AllDataReceived;

        /// <summary>
        /// Called when we send packet(s) to the remote client after we've changed Data.
        /// </summary>
        public event DataChangedEventHandler DataSent;
        #endregion

        #region Overrides
        public override void UpdateRemote()
        {
            RemoteUpdate();
        }
        

        public override int GetWidgetTypeGUID() 
        { 
            return GetStaticGUID(); 
        }

        public override bool AllowPersist
        {
            get
            {
                return false;
            }
        }

        public override string ProcessCommand(string[] args)
        {
            if (args.Length > 0 && args[0] == "-help")
            {
                return "TYPE: " + this.GetType().ToString() + "\n" +
                    "ARGUMENTS: [string]\n\n" +
                    "\t[string]     the string value to set";
            }

            if (args.Length == 0)
            {
                char[] chars = new char[this.Data.Length];
                for (int i = 0; i < this.Data.Length; ++i)
                {
                    chars[i] = Convert.ToChar(this.Data[i]);
                }

                return new string (chars);
            }

            if (args.Length != 1)
            {
                return "Incorrect # of arguments: only one argument required for text widget";
            }

            // convert the value:
            int length = args[0].Length;

            bool allocatedData = false;
            if ((Data == null) || ((MaxLength == 0) && (m_data.Length < length)))
            {
                if (MaxLength == 0)
                {
                    Data = new byte[length];
                }
                else
                {
                    Data = new byte[MaxLength];

                    if (length > MaxLength)
                    {
                        length = MaxLength;
                    }
                }

                allocatedData = true;
            }

            int dataLength = Math.Min(args[0].Length, MaxLength);
            for (int i = 0; i < dataLength; ++i)
            {
                this.Data[i] = Convert.ToByte(args[0][i]);
            }
            if (dataLength < MaxLength)
            {
                this.Data[dataLength] = 0;
            }

            OnDataReceived(new WidgetDataChangedEventArgs(0, (ushort)length, allocatedData));
            OnAllDataReceived( EventArgs.Empty );

            UpdateRemote();
            OnValueChanged();
            return "";
        }

        public override void ReceiveData(byte[] bytes)
        {
            SetValue(bytes);
        }

        public override void SendData(Stream stream)
        {
            byte[] buffer = this.Data; 
            stream.Write(buffer, 0, buffer.Length);
        }

        #endregion

        #region Functions
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        public void SetValue(byte[] value)
        {
            int length = value.Length;
            bool allocatedData = false;
            if ((Data == null) || ((MaxLength == 0) && (m_data.Length < length)))
            {
                if (MaxLength == 0)
                {
                    Data = new byte[length];
                }
                else
                {
                    Data = new byte[MaxLength];

                    if (length > MaxLength)
                    {
                        length = MaxLength;
                    }
                }

                allocatedData = true;
            }

            int dataLength = Math.Min(value.Length, MaxLength);
            for (int i = 0; i < dataLength; ++i)
            {
                this.Data[i] = value[i];
            }
            if (dataLength < MaxLength)
            {
                this.Data[dataLength] = 0;
            }

            OnDataReceived(new WidgetDataChangedEventArgs(0, (ushort)length, allocatedData));
            OnAllDataReceived(EventArgs.Empty);

            UpdateRemote();
            OnValueChanged();
        }

        public void SetValue(Stream dataStream)
        {
            byte[] data = new byte[dataStream.Length];
            dataStream.Read(data, 0, (int)dataStream.Length);
            SetValue(data);
        }

        /// <summary>
        /// Call this function to send all of the data to the game
        /// </summary>
        public void RemoteUpdate()
        {
            RemoteUpdate( 0, (ushort)m_data.Length, false );
        }

        /// <summary>
        /// Call this function to send all of the data to the game, passing whether we allocated the data or not
        /// </summary>
        /// <param name="allocatedData"></param>
        public void RemoteUpdate( bool allocatedData )
        {
            RemoteUpdate( 0, (ushort)m_data.Length, allocatedData );
        }

        /// <summary>
        /// Call this function to send the indicated range of data to the game
        /// </summary>
        /// <param name="offset"></param>
        /// <param name="length"></param>
        /// <param name="allocatedData"></param>
        public void RemoteUpdate( ushort offset, ushort count, bool allocatedData )
        {
            BankRemotePacket p = new BankRemotePacket( Pipe );
            if ( m_data != null )
            {
                if ( count > (ushort)m_data.Length )
                {
                    count = (ushort)m_data.Length;
                }

                // 8 = size of length + count + offset + remaining packets
                ushort payloadSize = RemotePacket.STORAGE_SIZE - 8;
                int packetsRemaining = (int)Math.Ceiling((decimal)Data.Length / payloadSize) - 1;
                while ( packetsRemaining >= 0 )
                {
                    p.Begin(BankRemotePacket.EnumPacketType.CHANGED, GetStaticGUID(), Id);

                    ushort pcount = (ushort)(count - offset > payloadSize ? payloadSize : count - offset);
                    p.Write_u16( (ushort)m_data.Length );
                    p.Write_u16( pcount );
                    p.Write_u16( offset );
                    p.Write_u16( (ushort)packetsRemaining );

                    for ( int i = 0; i < pcount; ++i )
                    {
                        p.Write_u8( m_data[offset + i] );
                    }

                    p.Send();
                    offset += pcount;
                    --packetsRemaining;
                }
            }
            else
            {
                p.Begin(BankRemotePacket.EnumPacketType.CHANGED, GetStaticGUID(), Id);
                p.Write_u16( 0 );
                p.Write_u16( 0 );
                p.Write_u16( 0 );
                p.Write_u16( 0 );
                p.Send();
            }

            OnDataSent( new WidgetDataChangedEventArgs( offset, count, allocatedData ) );
        }
        #endregion

        #region Static functions
        /// <summary>
        /// 
        /// </summary>
        /// <param name="packet"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public static bool RemoteHandler(BankRemotePacket packet, out String errorMessage)
        {
            bool handled = false;

            if ( packet.BankCommand == BankRemotePacket.EnumPacketType.CREATE )
            {
                packet.Begin();

                uint remoteParent = packet.Read_bkWidget();
                String title = packet.Read_const_char();
                String memo = packet.Read_const_char();
                Color fillColor = Widget.DeserializeColor(packet.Read_const_char());
                bool readOnly = packet.Read_bool();
                ushort maxLength = packet.Read_u16();
                bool showDataView = packet.Read_bool();
                 
                packet.End();

                Widget parent;
                if (packet.BankPipe.RemoteToLocal(remoteParent, out parent, out errorMessage))
                {
                    WidgetData newWidget = new WidgetData(packet.BankPipe, packet.Id, title, memo, maxLength, showDataView, fillColor, readOnly);
                    packet.BankPipe.Associate(packet.Id, newWidget);
                    parent.AddChild(newWidget);
                    handled = true;
                }
            }
            else if ( packet.BankCommand == BankRemotePacket.EnumPacketType.CHANGED )
            {
                packet.Begin();

                WidgetData widget;
                if (packet.BankPipe.RemoteToLocal(packet.Id, out widget, out errorMessage))
                {
                    ushort length = packet.Read_u16();
                    ushort count = packet.Read_u16();
                    ushort offset = packet.Read_u16();
                    ushort packetsRemaining = packet.Read_u16();

                    bool allocatedData = false;
                    if (length == 0)
                    {
                        widget.m_data = null;
                    }
                    else if ((widget.m_data == null) || ((widget.MaxLength == 0) && (widget.m_data.Length < length)))
                    {
                        if (widget.m_maxLength == 0)
                        {
                            widget.m_data = new byte[length];
                        }
                        else
                        {
                            widget.m_data = new byte[widget.m_maxLength];

                            if (length > widget.m_maxLength)
                            {
                                length = widget.m_maxLength;
                            }
                        }

                        allocatedData = true;
                    }

                    if (widget.m_data != null)
                    {
                        for (int i = offset; (i < offset + count) && (i < length); ++i)
                        {
                            widget.m_data[i] = packet.Read_u8();
                        }

                        widget.OnDataReceived(new WidgetDataChangedEventArgs(offset, count, allocatedData));

                        if (packetsRemaining == 0)
                        {
                            widget.OnAllDataReceived(EventArgs.Empty);
                        }
                    }

                    packet.End();
                    handled = true;
                }
            }
            else
            {
                handled = Widget.RemoteHandlerBase(packet, GetStaticGUID(), out errorMessage);
            }

            return handled;
        }

		public static void AddHandlers( BankRemotePacketProcessor packetProcessor )
		{
            packetProcessor.AddType( GetStaticGUID(), new BankRemotePacketProcessor.HandlerDel( RemoteHandler ) );
		}

        public static int GetStaticGUID()
        {
            return ComputeGUID( 'd', 'a', 't', 'a' ); 
        }
        #endregion

        #region Event Dispatchers
        private void OnDataReceived( WidgetDataChangedEventArgs e )
        {
            if ( DataReceived != null )
            {
                DataReceived( this, e );
            }
        }

        private void OnAllDataReceived( EventArgs e )
        {
            if ( AllDataReceived != null )
            {
                AllDataReceived( this, e );
            }
        }

        private void OnDataSent( WidgetDataChangedEventArgs e )
        {
            if ( DataSent != null )
            {
                DataSent( this, e );
            }
        }
        #endregion
    }
}
