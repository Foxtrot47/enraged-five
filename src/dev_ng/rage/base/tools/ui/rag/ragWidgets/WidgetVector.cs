using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Text;
using System.Xml;

using ragCore;


namespace ragWidgets
{
    abstract public class WidgetVector : Widget
    {
        protected WidgetVector(BankPipe pipe, uint id, String title, String memo, float[] vector, EnumType enumType, float min, float max, float step, Color fillColor, bool readOnly)
            : base(pipe, id, title, memo, fillColor, readOnly)
        {
            m_vector = vector;
            vector.CopyTo( m_DefaultVector, 0 );
            m_enumType = enumType;
            m_minimum = min;
            m_maximum = max;
            m_step = step;
        }

        #region Enums
        protected enum EnumType
        {
            Vector2 = 2,
            Vector3 = 3,
            Vector4 = 4,
        };

        public enum Operation
        {
            Scale,
            InvScale,
            Extend,
            Scale3,
            Add,
            Subtract,
            Multiply,
            Invert,
            InvertSafe,
            Negate,
            Abs,
            Average,
            Normalize,
            NormalizeSafe,
            NormalizeSaveV,
            NormalizeFast,
            Normalize3,
            Normalize3V,
            NormalizeFast3,
            Rotate,
            RotateX,
            RotateY,
            RotateZ,
            RotateAboutAxis,
            Cross,
            DotV,
            Dot3V,
            Lerp,
            Log,
            Log10,
            ReflectAbout,
            ReflectAboutFast,
            AddNet
        };
        #endregion

        #region Overrides
        public override void Serialize( XmlTextWriter writer, String path )
        {
            if ( SerializeStart( writer, ref path ) )
            {
                writer.WriteAttributeString( "X", m_vector[0].ToString() );
                writer.WriteAttributeString( "Y", m_vector[1].ToString() );

                if ( (m_enumType == EnumType.Vector3) || (m_enumType == EnumType.Vector4) )
                {
                    writer.WriteAttributeString( "Z", m_vector[2].ToString() );
                }

                if ( m_enumType == EnumType.Vector4 )
                {
                    writer.WriteAttributeString( "W", m_vector[3].ToString() );
                }

                writer.WriteEndElement();
            }
        }

        public override void DeSerialize( XmlTextReader reader )
        {
            m_vector[0] = float.Parse( reader["X"] );
            m_vector[1] = float.Parse( reader["Y"] );

            if ( (m_enumType == EnumType.Vector3) || (m_enumType == EnumType.Vector4) )
            {
                m_vector[2] = float.Parse( reader["Z"] );
            }

            if ( m_enumType == EnumType.Vector4 )
            {
                m_vector[3] = float.Parse( reader["W"] );
            }

            DeSerializeShared();
        }

        public override bool AllowPersist { get { return true; } }

        public override bool SerializeToJSON( LitJson.JsonData data )
        {
            LitJson.JsonData jsonList = new LitJson.JsonData();
            foreach ( float f in m_vector )
            {
                jsonList.Add( f );
            }

            data["value"] = jsonList;
            if (IsModified())
            {
                data["default"] = string.Join( ", ", m_DefaultVector );
            }

            return true;
        }

        public override void DeSerializeFromJSON( LitJson.JsonData data )
        {
            float[] value = new float[4];
            int i = 0;
            foreach ( LitJson.JsonData o in data["value"] )
            {
                double d = (Double)o;
                value[i++] = (float)d;
            }

            Value = value;
        }

        public override void UpdateRemote()
        {
            if ( !m_updateRemote )
            {
                return;
            }

            BankRemotePacket p = new BankRemotePacket( this.Pipe );
            p.Begin(BankRemotePacket.EnumPacketType.CHANGED, this.GetWidgetTypeGUID(), Id);
            WriteVector( p, m_vector );

            p.Send();
        }


        public override string ProcessCommand( string[] args )
        {
            if ( args.Length > 0 && args[0] == "-help" )
            {
                switch ( m_enumType )
                {
                    case EnumType.Vector2:
                        return "TYPE: " + this.GetType().ToString() + "\n" +
                            "ARGUMENTS: [float value] [float value]\n\n" +
                            "\t[float value]     the values to set";
                    case EnumType.Vector3:
                        return "TYPE: " + this.GetType().ToString() + "\n" +
                            "ARGUMENTS: [float value] [float value] [float value]\n\n" +
                            "\t[float value]     the values to set";
                    case EnumType.Vector4:
                        return "TYPE: " + this.GetType().ToString() + "\n" +
                            "ARGUMENTS: [float value] [float value] [float value] [float value]\n\n" +
                            "\t[float value]     the values to set";
                }
            }

            if ( args.Length == 0 )
            {
                switch ( m_enumType )
                {
                    case EnumType.Vector2:
                        return String.Format( "{0:r} {1:r}", m_vector[0], m_vector[1] );
                    case EnumType.Vector3:
                        return String.Format( "{0:r} {1:r} {2:r}", m_vector[0], m_vector[1], m_vector[2] );
                    case EnumType.Vector4:
                        return String.Format( "{0:r} {1:r} {2:r} {3:r}", m_vector[0], m_vector[1], m_vector[2], m_vector[3] );
                }
            }

            float[] newVector = new float[4];

            switch ( m_enumType )
            {
                case EnumType.Vector2:
                    if ( args.Length != 2 ) { return "Incorrect # of arguments, two required for vector"; }
                    break;
                case EnumType.Vector3:
                    if ( args.Length != 3 ) { return "Incorrect # of arguments, three required for vector"; }
                    break;
                case EnumType.Vector4:
                    if ( args.Length != 2 ) { return "Incorrect # of arguments, four required for vector"; }
                    break;
            }

            for ( int i = 0; i < args.Length; i++ )
            {
                newVector[i] = Convert.ToSingle( args[i] );
                if ( newVector[i] < m_minimum || newVector[i] > m_maximum )
                {
                    return String.Format( "New value {0} outside of range ({1}, {2})", newVector[i], m_minimum, m_maximum );
                }
            }

            Value = newVector;

            return "";
        }

        public override void ReceiveData(byte[] bytes)
        {
            int argCount = bytes.Length / sizeof(float);

            if ( (m_enumType == EnumType.Vector2 && argCount != 2) 
                || (m_enumType == EnumType.Vector3 && argCount != 3) 
                || (m_enumType == EnumType.Vector4 && argCount != 4) )
            {
                return;
            }

            float[] newVector = new float[4];
            int offset = 0;

            for (int i = 0; i < argCount; i++)
            {
                newVector[i] = BitConverter.ToSingle(bytes, offset);
                offset += sizeof(float);
                if (newVector[i] < m_minimum || newVector[i] > m_maximum)
                {
                    return; 
                }
            }

            Value = newVector;
        }

        public override void SendData(Stream stream)
        {
            switch (m_enumType)
            {
                case EnumType.Vector2:
                    int dataSize = 2 * sizeof(float);
                    byte[] buffer = BitConverter.GetBytes(dataSize);
                    stream.Write(buffer, 0, buffer.Length);
                    stream.Write(BitConverter.GetBytes(m_vector[0]), 0, sizeof(float));
                    stream.Write(BitConverter.GetBytes(m_vector[1]), 0, sizeof(float));
                    break;
                case EnumType.Vector3:
                    dataSize = 3 * sizeof(float);
                    buffer = BitConverter.GetBytes(dataSize);
                    stream.Write(buffer, 0, buffer.Length);
                    stream.Write(BitConverter.GetBytes(m_vector[0]), 0, sizeof(float));
                    stream.Write(BitConverter.GetBytes(m_vector[1]), 0, sizeof(float));
                    stream.Write(BitConverter.GetBytes(m_vector[2]), 0, sizeof(float));
                    break;
                case EnumType.Vector4:
                    dataSize = 4 * sizeof(float);
                    buffer = BitConverter.GetBytes(dataSize);
                    stream.Write(buffer, 0, buffer.Length);
                    stream.Write(BitConverter.GetBytes(m_vector[0]), 0, sizeof(float));
                    stream.Write(BitConverter.GetBytes(m_vector[1]), 0, sizeof(float));
                    stream.Write(BitConverter.GetBytes(m_vector[2]), 0, sizeof(float));
                    stream.Write(BitConverter.GetBytes(m_vector[3]), 0, sizeof(float));
                    break;
            }
        }

        public override bool IsModified()
        {
            if ( m_vector.Length != m_DefaultVector.Length )
            {
                return true;
            }
            for ( int i = 0; i < m_vector.Length; i++ )
            {
                if ( m_vector[i] != m_DefaultVector[i] )
                {
                    return true;
                }
            }
            return false;
        }

        public override void ResetToDefault()
        {
            Value = (float[])m_DefaultVector.Clone();
        }


        #endregion

        #region Variables
        protected float[] m_vector = new float[4];
        protected float[] m_DefaultVector = new float[4];
        protected EnumType m_enumType = EnumType.Vector4;
        protected float m_minimum = float.MinValue;
        protected float m_maximum = float.MaxValue;
        protected float m_step = 1.0f;
        private bool m_updateRemote = true;
        #endregion

        #region Properties
        public float[] Value
        {
            get
            {
                return m_vector;
            }
            set
            {
                if ( m_vector != value )
                {
                    m_vector = value;

                    UpdateRemote();
                    OnValueChanged();
                }
            }
        }

        public int NumComponents
        {
            get
            {
                return (int)m_enumType;
            }
        }

        public float Minimum
        {
            get
            {
                return m_minimum;
            }
            set
            {
                m_minimum = value;
            }
        }

        public float Maximum
        {
            get
            {
                return m_maximum;
            }
            set
            {
                m_maximum = value;
            }
        }

        public float Step
        {
            get
            {
                return m_step;
            }
            set
            {
                m_step = value;
            }
        }
        #endregion

        #region Event Handlers
        public void UpdateRemoteOperation( BankRemotePacket.EnumPacketType enumType, Operation op, float floatValue, int intValue )
        {
            BankRemotePacket p = new BankRemotePacket( this.Pipe );
            p.Begin(enumType, this.GetWidgetTypeGUID(), Id);

            p.Write_s32( (int)op );
            p.Write_float( floatValue );
            p.Write_s32( intValue );

            p.Send();
        }

        public void UpdateRemoteOperation( BankRemotePacket.EnumPacketType enumType, Operation op, float[] vectorValue, float floatValue )
        {
            BankRemotePacket p = new BankRemotePacket( this.Pipe );
            p.Begin(enumType, this.GetWidgetTypeGUID(), Id);

            p.Write_s32( (int)op );
            WriteVector( p, vectorValue );
            p.Write_float( floatValue );

            p.Send();
        }
        public void UpdateRemoteOperation( BankRemotePacket.EnumPacketType enumType, Operation op, float floatValue, float[] vectorValue )
        {
            BankRemotePacket p = new BankRemotePacket( this.Pipe );
            p.Begin(enumType, this.GetWidgetTypeGUID(), Id);

            p.Write_s32( (int)op );
            p.Write_float( floatValue );
            WriteVector( p, vectorValue );

            p.Send();
        }

        public void UpdateRemoteOperation( BankRemotePacket.EnumPacketType enumType, Operation op, float[] value )
        {
            BankRemotePacket p = new BankRemotePacket( this.Pipe );
            p.Begin(enumType, this.GetWidgetTypeGUID(), Id);

            p.Write_s32( (int)op );
            WriteVector( p, value );

            p.Send();
        }

        public void UpdateRemoteOperation( BankRemotePacket.EnumPacketType enumType, Operation op, float value )
        {
            BankRemotePacket p = new BankRemotePacket( this.Pipe );
            p.Begin(enumType, this.GetWidgetTypeGUID(), Id);

            p.Write_s32( (int)op );
            p.Write_float( value );

            p.Send();
        }

        public void UpdateRemoteOperation( BankRemotePacket.EnumPacketType enumType, Operation op, int value )
        {
            BankRemotePacket p = new BankRemotePacket( this.Pipe );
            p.Begin(enumType, this.GetWidgetTypeGUID(), Id);

            p.Write_s32( (int)op );
            p.Write_s32( value );

            p.Send();
        }


        public void InvertClickEH( object sender, EventArgs e )
        {
            BankRemotePacket p = new BankRemotePacket( this.Pipe );
            p.Begin(BankRemotePacket.EnumPacketType.USER_0, this.GetWidgetTypeGUID(), Id);

            p.Write_s32( (int)Operation.Invert );

            p.Send();
        }

        public void InvertSafeClickEH( object sender, EventArgs e )
        {
            BankRemotePacket p = new BankRemotePacket( this.Pipe );
            p.Begin(BankRemotePacket.EnumPacketType.USER_0, this.GetWidgetTypeGUID(), Id);

            p.Write_s32( (int)Operation.InvertSafe );

            p.Send();
        }

        public void NegateClickEH( object sender, EventArgs e )
        {
            BankRemotePacket p = new BankRemotePacket( this.Pipe );
            p.Begin(BankRemotePacket.EnumPacketType.USER_0, this.GetWidgetTypeGUID(), Id);

            p.Write_s32( (int)Operation.Negate );

            p.Send();
        }

        public void AbsClickEH( object sender, EventArgs e )
        {
            BankRemotePacket p = new BankRemotePacket( this.Pipe );
            p.Begin(BankRemotePacket.EnumPacketType.USER_0, this.GetWidgetTypeGUID(), Id);

            p.Write_s32( (int)Operation.Abs );

            p.Send();
        }

        public void NormalizeClickEH( object sender, EventArgs e )
        {
            BankRemotePacket p = new BankRemotePacket( this.Pipe );
            p.Begin(BankRemotePacket.EnumPacketType.USER_0, this.GetWidgetTypeGUID(), Id);

            p.Write_s32( (int)Operation.Normalize );

            p.Send();
        }

        public void NormalizeSafeClickEH( object sender, EventArgs e )
        {
            BankRemotePacket p = new BankRemotePacket( this.Pipe );
            p.Begin(BankRemotePacket.EnumPacketType.USER_0, this.GetWidgetTypeGUID(), Id);

            p.Write_s32( (int)Operation.NormalizeSafe );

            p.Send();
        }

        public void NormalizeSafeVClickEH( object sender, EventArgs e )
        {
            BankRemotePacket p = new BankRemotePacket( this.Pipe );
            p.Begin(BankRemotePacket.EnumPacketType.USER_0, this.GetWidgetTypeGUID(), Id);

            p.Write_s32( (int)Operation.NormalizeSaveV );

            p.Send();
        }

        public void NormalizeFastClickEH( object sender, EventArgs e )
        {
            BankRemotePacket p = new BankRemotePacket( this.Pipe );
            p.Begin(BankRemotePacket.EnumPacketType.USER_0, this.GetWidgetTypeGUID(), Id);

            p.Write_s32( (int)Operation.NormalizeFast );

            p.Send();
        }

        public void Normalize3ClickEH( object sender, EventArgs e )
        {
            BankRemotePacket p = new BankRemotePacket( this.Pipe );
            p.Begin(BankRemotePacket.EnumPacketType.USER_0, this.GetWidgetTypeGUID(), Id);

            p.Write_s32( (int)Operation.Normalize3 );

            p.Send();
        }

        public void Normalize3VClickEH( object sender, EventArgs e )
        {
            BankRemotePacket p = new BankRemotePacket( this.Pipe );
            p.Begin(BankRemotePacket.EnumPacketType.USER_0, this.GetWidgetTypeGUID(), Id);

            p.Write_s32( (int)Operation.Normalize3V );

            p.Send();
        }

        public void NormalizeFast3ClickEH( object sender, EventArgs e )
        {
            BankRemotePacket p = new BankRemotePacket( this.Pipe );
            p.Begin(BankRemotePacket.EnumPacketType.USER_0, this.GetWidgetTypeGUID(), Id);

            p.Write_s32( (int)Operation.NormalizeFast3 );

            p.Send();
        }


        public void LogClickEH( object sender, EventArgs e )
        {
            BankRemotePacket p = new BankRemotePacket( this.Pipe );
            p.Begin(BankRemotePacket.EnumPacketType.USER_0, this.GetWidgetTypeGUID(), Id);

            p.Write_s32( (int)Operation.Log );

            p.Send();
        }

        public void Log10ClickEH( object sender, EventArgs e )
        {
            BankRemotePacket p = new BankRemotePacket( this.Pipe );
            p.Begin(BankRemotePacket.EnumPacketType.USER_0, this.GetWidgetTypeGUID(), Id);

            p.Write_s32( (int)Operation.Log10 );

            p.Send();
        }
        #endregion

        #region Private Functions
        private void WriteVector( BankRemotePacket p, float[] vector )
        {
            p.Write_float( vector[0] );
            p.Write_float( vector[1] );

            if ( (m_enumType == EnumType.Vector3) || (m_enumType == EnumType.Vector4) )
            {
                p.Write_float( vector[2] );
            }

            if ( m_enumType == EnumType.Vector4 )
            {
                p.Write_float( vector[3] );
            }
        }
        #endregion

        #region Static Functions
        protected static bool RemoteHandler( BankRemotePacket packet, out String errorMessage, int guid, EnumType enumType )
        {
            bool handled = false;

            if ( packet.BankCommand == BankRemotePacket.EnumPacketType.CREATE )
            {
                packet.Begin();

                uint remoteParent = packet.Read_bkWidget();
                String title = packet.Read_const_char();
                String memo = packet.Read_const_char();
                Color fillColor = Widget.DeserializeColor(packet.Read_const_char());
                bool readOnly = packet.Read_bool();

                float[] vector = new float[4];
                for ( int i = 0; (i < 4) && (i < (int)enumType); ++i )
                {
                    vector[i] = packet.Read_float();
                }

                float min = packet.Read_float();
                float max = packet.Read_float();
                float step = packet.Read_float();

                packet.End();
                
                Widget parent;
                if (packet.BankPipe.RemoteToLocal(remoteParent, out parent, out errorMessage))
                {
                    WidgetVector local = null;
                    switch (enumType)
                    {
                        case EnumType.Vector2:
                            local = new WidgetVector2(packet.BankPipe, packet.Id, title, memo, vector, min, max, step, fillColor, readOnly);
                            break;
                        case EnumType.Vector3:
                            local = new WidgetVector3(packet.BankPipe, packet.Id, title, memo, vector, min, max, step, fillColor, readOnly);
                            break;
                        case EnumType.Vector4:
                            local = new WidgetVector4(packet.BankPipe, packet.Id, title, memo, vector, min, max, step, fillColor, readOnly);
                            break;
                    }

                    packet.BankPipe.Associate(packet.Id, local);
                    parent.AddChild(local);
                    handled = true;
                }
            }
            else if ( packet.BankCommand == BankRemotePacket.EnumPacketType.CHANGED )
            {
                packet.Begin();
                
				// find the local version of the the remote widget:
				WidgetVector local;
                if (packet.BankPipe.RemoteToLocal(packet.Id, out local, out errorMessage))
                {
                    for (int i = 0; i < local.NumComponents; ++i)
                    {
                        local.m_vector[i] = packet.Read_float();
                    }

                    packet.End();

                    local.m_updateRemote = false;
                    local.OnValueChanged();
                    local.m_updateRemote = true;
                    handled = true;
                }
            }
            else if (packet.BankCommand >= BankRemotePacket.EnumPacketType.USER_0 &&
                packet.BankCommand <= BankRemotePacket.EnumPacketType.USER_4)
            {
                handled = true;
                errorMessage = null;
            }
            else
            {
                handled = Widget.RemoteHandlerBase(packet, guid, out errorMessage);
            }

            return handled;
        }

        public static void AddHandlers( BankRemotePacketProcessor packetProcessor )
        {
            WidgetVector2.AddHandlers( packetProcessor );
            WidgetVector3.AddHandlers( packetProcessor );
            WidgetVector4.AddHandlers( packetProcessor );
        }
        #endregion
    }

    public class WidgetVector2 : WidgetVector
    {
        public WidgetVector2(BankPipe pipe, uint id, String title, String memo, float[] vector, float min, float max, float step, Color fillColor, bool readOnly)
            : base(pipe, id, title, memo, vector, EnumType.Vector2, min, max, step, fillColor, readOnly)
        {

        }

        public override int GetWidgetTypeGUID() { return GetStaticGUID(); }
        public static int GetStaticGUID() { return ComputeGUID( 'v', 'e', 'c', '2' ); }

        private static bool RemoteHandler( BankRemotePacket packet, out String errorMessage )
        {
            return RemoteHandler( packet, out errorMessage, GetStaticGUID(), EnumType.Vector2 );
        }

        public static new void AddHandlers( BankRemotePacketProcessor packetProcessor )
        {
            packetProcessor.AddType( GetStaticGUID(), new BankRemotePacketProcessor.HandlerDel( RemoteHandler ) );
        }
    }

    public class WidgetVector3 : WidgetVector
    {
        public WidgetVector3(BankPipe pipe, uint id, String title, String memo, float[] vector, float min, float max, float step, Color fillColor, bool readOnly)
            : base(pipe, id, title, memo, vector, EnumType.Vector3, min, max, step, fillColor, readOnly)
        {

        }

        public override int GetWidgetTypeGUID() { return GetStaticGUID(); }
        public static int GetStaticGUID() { return ComputeGUID( 'v', 'e', 'c', '3' ); }

        private static bool RemoteHandler( BankRemotePacket packet, out String errorMessage )
        {
            return RemoteHandler( packet, out errorMessage, GetStaticGUID(), EnumType.Vector3 );
        }

        public static new void AddHandlers( BankRemotePacketProcessor packetProcessor )
        {
            packetProcessor.AddType( GetStaticGUID(), new BankRemotePacketProcessor.HandlerDel( RemoteHandler ) );
        }
    }

    public class WidgetVector4 : WidgetVector
    {
        public WidgetVector4(BankPipe pipe, uint id, String title, String memo, float[] vector, float min, float max, float step, Color fillColor, bool readOnly)
            : base(pipe, id, title, memo, vector, EnumType.Vector4, min, max, step, fillColor, readOnly)
        {

        }

        public override int GetWidgetTypeGUID() { return GetStaticGUID(); }
        public static int GetStaticGUID() { return ComputeGUID( 'v', 'e', 'c', '4' ); }

        private static bool RemoteHandler( BankRemotePacket packet, out String errorMessage )
        {
            return RemoteHandler( packet, out errorMessage, GetStaticGUID(), EnumType.Vector4 );
        }

        public static new void AddHandlers( BankRemotePacketProcessor packetProcessor )
        {
            packetProcessor.AddType( GetStaticGUID(), new BankRemotePacketProcessor.HandlerDel( RemoteHandler ) );
        }
    }
}
