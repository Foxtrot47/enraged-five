using System;
using System.Drawing;
using System.IO;
using System.Text;
using System.Xml;

using ragCore;

namespace ragWidgets
{
    /// <summary>
    /// Summary description for WidgetText.
    /// </summary>
    public class WidgetText : Widget
    {
        private WidgetText(BankPipe pipe, uint id, String title, String memo, bool data, bool readOnly, Color fillColor)
            : base(pipe, id, title, memo, fillColor, readOnly)
        {
            m_dataType = DataType.BOOL;
            m_String = data.ToString();
            m_DefaultString = (String)m_String.Clone();
        }

        private WidgetText(BankPipe pipe, uint id, String title, String memo, float data, bool readOnly, Color fillColor)
            : base(pipe, id, title, memo, fillColor, readOnly)
        {
            m_dataType = DataType.FLOAT;
            m_String = data.ToString();
            m_DefaultString = (String)m_String.Clone();
        }

        private WidgetText(BankPipe pipe, uint id, String title, String memo, int data, bool readOnly, Color fillColor)
            : base(pipe, id, title, memo, fillColor, readOnly)
        {
            m_dataType = DataType.INT;
            m_String = data.ToString();
            m_DefaultString = (String)m_String.Clone();
        }

        private WidgetText(BankPipe pipe, uint id, String title, String memo, long data, bool readOnly, Color fillColor)
            : base(pipe, id, title, memo, fillColor, readOnly)
        {
            m_dataType = DataType.LONG;
            m_String = data.ToString();
            m_DefaultString = (String)m_String.Clone();
        }

        private WidgetText( BankPipe pipe, uint id, String title, String memo, String data, uint stringSize, bool readOnly, Color fillColor )
            : base( pipe, id, title, memo, fillColor, readOnly )
        {
            m_dataType = DataType.STRING;
            m_String = data;
            m_StringSize = stringSize;
            m_DefaultString = (String)m_String.Clone();
        }

        #region Enums
        public enum DataType
        {
            BOOL,
            FLOAT,
            INT,
            LONG,
            STRING
        }
        #endregion

        #region Variables
        private DataType m_dataType;
        private String m_String;
        private String m_DefaultString;
        private uint m_StringSize;

        private bool m_DefaultStringInitialised = false;
        #endregion

        #region Properties
        public DataType TypeOfData
        {
            get
            {
                return m_dataType;
            }
        }

        public String String
        {
            get
            {
                return m_String;
            }
            set
            {
                if ( m_String != value )
                {
                    bool changed = false;
                    switch ( m_dataType )
                    {
                        case DataType.BOOL:
                            {
                                bool b;
                                changed = bool.TryParse( value, out b );
                            }
                            break;
                        case DataType.FLOAT:
                            {
                                float f;
                                changed = float.TryParse( value, out f );
                            }
                            break;
                        case DataType.INT:
                            {
                                int i;
                                changed = int.TryParse( value, out i );
                            }
                            break;
                        case DataType.LONG:
                            {
                                long l;
                                changed = long.TryParse(value, out l);
                            }
                            break;
                        case DataType.STRING:
                            changed = true;
                            break;
                    }

                    if ( changed )
                    {
                        if ((value.Length > m_StringSize) && (m_StringSize > 0))
                            m_String = value.Substring(0, (int)(m_StringSize));
                        else
                            m_String = value;
                     
                        UpdateRemote();
                        OnValueChanged();
                    }
                }
            }
        }

        public uint MaxStringLength
        {
            get
            {
                return m_StringSize;
            }
        }
        #endregion

        #region Overrides
        public override int GetWidgetTypeGUID() { return GetStaticGUID(); }
        public static int GetStaticGUID() { return ComputeGUID( 't', 'e', 'x', 't' ); }
        private static readonly ASCIIEncoding Encoder = new ASCIIEncoding();

        public override void Serialize( XmlTextWriter writer, String path )
        {
            if ( SerializeStart( writer, ref path ) )
            {
                switch ( m_dataType )
                {
                    case DataType.BOOL:
                        writer.WriteAttributeString( "Bool", m_String );
                        break;
                    case DataType.FLOAT:
                        writer.WriteAttributeString( "Float", m_String );
                        break;
                    case DataType.INT:
                        writer.WriteAttributeString( "Int", m_String );
                        break;
                    case DataType.LONG:
                        writer.WriteAttributeString("Long", m_String);
                        break;
                    case DataType.STRING:
                        writer.WriteAttributeString( "String", m_String );
                        break;
                }
                
                writer.WriteEndElement();
            }
        }

        public override void DeSerialize( XmlTextReader reader )
        {
            switch ( m_dataType )
            {
                case DataType.BOOL:
                    m_String = reader["Bool"];
                    break;
                case DataType.FLOAT:
                    m_String = reader["Float"];
                    break;
                case DataType.INT:
                    m_String = reader["Int"];
                    break;
                case DataType.LONG:
                    m_String = reader["Long"];
                    break;
                case DataType.STRING:
                    m_String = reader["String"];
                    break;
            }            

            DeSerializeShared();
        }

        public override bool AllowPersist
        {
            get
            {
                return true;
            }
        }

        public override bool SerializeToJSON( LitJson.JsonData data )
        {
            data["value"] = m_String;
            if ( IsModified() )
            {
                data["default"] = m_DefaultString;
            }

            return true;
        }

        public override void DeSerializeFromJSON( LitJson.JsonData data )
        {
            m_String = (string)data["value"];
            OnValueChanged();
        }

        public override string ProcessCommand( string[] args )
        {
            if ( args.Length > 0 && args[0] == "-help" )
            {
                return "TYPE: " + this.GetType().ToString() + "\n" +
                    "ARGUMENTS: [string]\n\n" +
                    "\t[string]     the string value to set";
            }

            if (args.Length == 0)
            {
                return this.String;
            }

            if ( args.Length != 1 )
            {
                return "Incorrect # of arguments: only one argument required for text widget";
            }

            // convert the value:
            this.String = args[0];
            return "";
        }

        public override void ReceiveData(byte[] bytes)
        {
            if (bytes.Length != 0)
                return;

            string itemString = Encoder.GetString(bytes, 0, bytes.Length);

            // convert the value:
            this.String = itemString;
        }

        public override void SendData(Stream stream)
        {
            string value = this.String;
            byte[] lengthBytes = BitConverter.GetBytes(value.Length);
            stream.Write(lengthBytes, 0, lengthBytes.Length);

            byte[] buffer = Encoder.GetBytes(value);
            stream.Write(buffer, 0, buffer.Length);
        }

        public override void UpdateRemote()
        {
            BankRemotePacket p = new BankRemotePacket( Pipe );

            ushort offset = 0;
            ushort maxPacketSize = RemotePacket.STORAGE_SIZE - 6;
            short packetsRemaining = 0;
            if ( m_dataType == DataType.STRING )
            {
                packetsRemaining = (short)( m_StringSize / maxPacketSize );
            }

            while ( packetsRemaining >= 0 )
            {
                ushort count = 0;
                if (m_dataType == DataType.STRING)
                {
                    count = (ushort)((m_StringSize - offset > maxPacketSize) ? maxPacketSize : (m_StringSize - offset));
                }

                p.Begin(BankRemotePacket.EnumPacketType.CHANGED, GetStaticGUID(), Id);
                p.Write_u16( count );
                p.Write_u16( offset );
                p.Write_s16( packetsRemaining );

                switch ( m_dataType )
                {
                    case DataType.BOOL:
                        p.Write_bool( bool.Parse( m_String ) );
                        break;
                    case DataType.FLOAT:
                        p.Write_float( float.Parse( m_String ) );
                        break;
                    case DataType.INT:
                        p.Write_s32( int.Parse( m_String ) );
                        break;
                    case DataType.LONG:
                        p.Write_s64(long.Parse(m_String));
                        break;
                    case DataType.STRING:
                        {
                            byte[] stringBytes = new byte[m_StringSize];

                            if (m_StringSize < m_String.Length)
                            {
                                string msg = string.Format(
                                    "ERROR: {0}: String buffer too small, was {1}, needs to be at least {2} to fit this value: {3}", 
                                    Path, m_StringSize, m_String.Length, m_String);

                                throw new Exception( msg );
                            }
                            else
                            {
                                Encoding.Default.GetBytes( m_String, 0, m_String.Length, stringBytes, 0 );
                            }
                            
                            for ( int i = 0; i < count; ++i )
                            {
                                p.Write_u8( stringBytes[offset + i] );
                            }
                        }
                        break;
                }

                p.Send();

                offset += count;
                --packetsRemaining;
            }
        }


        public override bool IsModified()
        {
            return m_DefaultString != m_String;
        }

        public override void ResetToDefault()
        {
            m_String = m_DefaultString;
            OnValueChanged();
        }


        #endregion


        #region Static Functions
        public static bool RemoteHandler( BankRemotePacket packet, out String errorMessage )
        {
            bool handled = false;

            if ( packet.BankCommand == BankRemotePacket.EnumPacketType.CREATE )
            {
                packet.Begin();

                uint remoteParent = packet.Read_bkWidget();
                String title = packet.Read_const_char();
                String memo = packet.Read_const_char();
                Color fillColor = Widget.DeserializeColor( packet.Read_const_char() );
                bool readOnly = packet.Read_bool();
                DataType dataType = (DataType)packet.Read_s32();

                WidgetText local = null;
                switch ( dataType )
                {
                    case DataType.BOOL:
                        {
                            bool initData = false; //packet.Read_bool();

                            local = new WidgetText(packet.BankPipe, packet.Id, title, memo, initData, readOnly, fillColor);
                        }
                        break;
                    case DataType.FLOAT:
                        {
                            float initData = 0.0f; //packet.Read_float();

                            local = new WidgetText(packet.BankPipe, packet.Id, title, memo, initData, readOnly, fillColor);
                        }
                        break;
                    case DataType.INT:
                        {
                            int initData = 0; //packet.Read_s32();

                            local = new WidgetText(packet.BankPipe, packet.Id, title, memo, initData, readOnly, fillColor);
                        }
                        break;
                    case DataType.LONG:
                        {
                            long initData = 0; //packet.Read_s64();

                            local = new WidgetText(packet.BankPipe, packet.Id, title, memo, initData, readOnly, fillColor);
                        }
                        break;
                    case DataType.STRING:
                        {
                            String initData = ""; //packet.Read_const_char();
                            uint stringSize = packet.Read_u32();

                            local = new WidgetText(packet.BankPipe, packet.Id, title, memo, initData, stringSize, readOnly, fillColor);
                        }
                        break;
                }

                packet.End();
                
                Widget parent;
                if (packet.BankPipe.RemoteToLocal(remoteParent, out parent, out errorMessage) && local != null)
                {
                    packet.BankPipe.Associate(packet.Id, local);
                    parent.AddChild( local );
                    handled = true;
                }
            }
            else if ( packet.BankCommand == BankRemotePacket.EnumPacketType.CHANGED )
            {
                packet.Begin();

                WidgetText local;
                if (packet.BankPipe.RemoteToLocal(packet.Id, out local, out errorMessage))
                {
                    ushort count = packet.Read_u16();
                    ushort offset = packet.Read_u16();
                    short packetsRemaining = packet.Read_s16();

                    switch ( local.m_dataType )
                    {
                        case DataType.BOOL:
                            local.m_String = packet.Read_bool().ToString();
                            break;
                        case DataType.FLOAT:
                            local.m_String = packet.Read_float().ToString();
                            break;
                        case DataType.INT:
                            local.m_String = packet.Read_s32().ToString();
                            break;
                        case DataType.LONG:
                            local.m_String = packet.Read_s64().ToString();
                            break;
                        case DataType.STRING:
                            {
                                byte[] stringBytes = new byte[local.m_StringSize];
                                Encoding.Default.GetBytes(local.String, 0, local.String.Length, stringBytes, 0);
                                
                                for ( int i = offset; (i < offset + count) && (i < local.m_StringSize); ++i )
                                {
                                    stringBytes[i] = packet.Read_u8();
                                }

                                local.m_String = Encoding.Default.GetString(stringBytes);

                                if (local.m_String.Contains("\0") == false)
                                {
                                    local.m_String += "\0";
                                }

								// The above line doesn't terminate the string if it finds \0 so doing that manually.
                                local.m_String = local.m_String.Substring( 0, local.m_String.IndexOf( '\0' ) );
                            }
                            break;
                    }

                    // This here code is a bit hacky - it's because the game initialises the widget
                    // with text=="", and then sends an update packet with the real value.
                    if ( !local.m_DefaultStringInitialised )
                    {
                        local.m_DefaultString = (String)local.m_String.Clone();
                        local.m_DefaultStringInitialised = true;
                    }

                    if ( packetsRemaining == 0 )
                    {
                        local.OnValueChanged();
                    }

                    packet.End();
                    handled = true;
                }
            }
            else
            {
                handled = Widget.RemoteHandlerBase(packet, GetStaticGUID(), out errorMessage);
            }

            return handled;
        }

        public static void AddHandlers( BankRemotePacketProcessor packetProcessor )
        {
            packetProcessor.AddType( GetStaticGUID(), new BankRemotePacketProcessor.HandlerDel( RemoteHandler ) );
        }
        #endregion

    }
}
