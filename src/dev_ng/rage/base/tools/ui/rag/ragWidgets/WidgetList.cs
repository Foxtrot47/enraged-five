using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Xml;

using ragCore;

namespace ragWidgets
{
	/// <summary>
	/// Summary description for WidgetList.
	/// </summary>
	public class WidgetList : Widget
	{
        protected WidgetList(BankPipe pipe, uint id, String title, String memo, Color fillColor, bool readOnly)
            : base(pipe, id, title, memo, fillColor, readOnly)
		{
		}

		public override int GetWidgetTypeGUID() {return GetStaticGUID();}
		public static int GetStaticGUID()		{return ComputeGUID('l','i','s','t');}
		
		public override void Serialize(XmlTextWriter writer,string path) {}
		public override void DeSerialize(XmlTextReader reader) {}
		public override bool AllowPersist {get {return false;}}

		public enum Type
		{
			STRING,
			INT,
			FLOAT
		};

		#region Properties
		public List<string> ColumnHeaders
		{
			get 
			{
				return m_ColumnHeaders;
			}
		}

        public List<Type> ColumnTypes
		{
			get 
			{
				return m_ColumnTypes;
			}
		}

        public Dictionary<int, ArrayList> ValueHash
		{
			get 
			{
				return m_ValueHash;
			}
		}
		#endregion

        private List<string> m_ColumnHeaders = new List<string>();
        private List<Type> m_ColumnTypes = new List<Type>();

        public class ItemRemovedEventArgs : EventArgs
        {
            public ItemRemovedEventArgs(int item)
            {   
                Item = item;                
            }

            #region Variables
            public int Item;
            #endregion
        };

        public class ItemAddedEventArgs : EventArgs
        {
            public ItemAddedEventArgs(int item, int column, object value)
            {
                Item = item;
                Column = column;
                Value = value;
            }

            public int Item;
            public int Column;
            public object Value;
        }


        public delegate void ItemRemovedEventHandler(object sender, ItemRemovedEventArgs args);
        public delegate void ItemAddedEventHandler(object sender, ItemAddedEventArgs args);
        public event EventHandler StructureChanged;
        public event ItemAddedEventHandler ItemAdded;
        public event ItemRemovedEventHandler ItemRemoved;

		private void AddColumnHeader(int column,String heading,Type type)
		{
			// make sure we have enough columns:
			if (column>=m_ColumnHeaders.Count)
			{
				int numToAdd=column-(m_ColumnHeaders.Count);
				for (int i=0;i<numToAdd;i++)
				{
					m_ColumnHeaders.Add(" ");
					m_ColumnTypes.Add(Type.STRING);
				}
			}
			
			m_ColumnHeaders.Add(heading);
			m_ColumnTypes.Add(type);

            OnStructureChanged();
		}


        private void OnStructureChanged()
        {
            if ( StructureChanged != null )
            {
                StructureChanged( this, EventArgs.Empty );
            }
        }

        // Use ArrayList so that we may use our type info to store a float, an int, or a string
        private Dictionary<int, ArrayList> m_ValueHash = new Dictionary<int, ArrayList>();
		
        private ArrayList FindRow(int key,int column)
		{
            if ( m_ValueHash.ContainsKey( key ) )
            {
                return m_ValueHash[key];
            }
            else
            {
                ArrayList row = new ArrayList();
                m_ValueHash.Add( key, row );

                // fill out columns in the row:
                for ( int i = 0; i < m_ColumnHeaders.Count; i++ )
                {
                    if ( m_ColumnTypes[i] == Type.FLOAT )
                        row.Add( 0.0f );
                    else if ( m_ColumnTypes[i] == Type.INT )
                        row.Add( 0 );
                    else
                        row.Add( " " );
                }
                return row;
            }
		}

		private void AddItem(int key,int column,String columnStr)
		{
			ArrayList rows=FindRow(key,column);

            if ( m_ColumnTypes[column] != Type.STRING )
            {
                throw (new Exception( "Column type mismatch.  Expected " + m_ColumnTypes[column].ToString() + " but received STRING." ));
            }

            rows[column] = columnStr;

            if (ItemAdded != null)
            {
                ItemAdded(this, new ItemAddedEventArgs(key, column, columnStr));
            }
		}

		private void AddItem(int key,int column,int columnInt)
		{
			ArrayList rows=FindRow(key,column);

            if ( m_ColumnTypes[column] != Type.INT )
            {
                throw (new Exception( "Column type mismatch.  Expected " + m_ColumnTypes[column].ToString() + " but received INT." ));
            }

            rows[column] = columnInt;

            if (ItemAdded != null)
            {
                ItemAdded(this, new ItemAddedEventArgs(key, column, columnInt));
            }
		}

		private void AddItem(int key,int column,float columnFloat)
		{
			ArrayList rows=FindRow(key,column);

            if ( m_ColumnTypes[column] != Type.FLOAT )
            {
                throw (new Exception( "Column type mismatch.  Expected " + m_ColumnTypes[column].ToString() + " but received FLOAT." ));
            }

            rows[column] = columnFloat;

            if (ItemAdded != null)
            {
                ItemAdded(this, new ItemAddedEventArgs(key, column, columnFloat));
            }
		}



		private void RemoveItem(int key)
		{
			m_ValueHash.Remove(key);
				
            if (ItemRemoved != null)
            {
                ItemRemoved( this, new ItemRemovedEventArgs(key) );
            }
		}

		public void ModifyItem(int key,int column,String newValue)
		{
			// USER_3 == GUI sends update event

			BankRemotePacket p = new BankRemotePacket(Pipe);
            p.Begin(BankRemotePacket.EnumPacketType.USER_3, GetWidgetTypeGUID(), Id); 
			p.Write_s32(key);
			p.Write_s32(column);
			p.Write_const_char(newValue);
			p.Send();
		}

        public void DoubleClick(int key)
        {
            // USER_4 == GUI sends double click event
            BankRemotePacket p = new BankRemotePacket(Pipe);
            p.Begin(BankRemotePacket.EnumPacketType.USER_4, GetWidgetTypeGUID(), Id);
            p.Write_s32(key);
            p.Send();
        }

        public void SingleClick(int key)
        {
            // USER_5 == GUI sends single click event
            BankRemotePacket p = new BankRemotePacket(Pipe);
            p.Begin(BankRemotePacket.EnumPacketType.USER_5, GetWidgetTypeGUID(), Id);
            p.Write_s32(key);
            p.Send();
        }

		public override void UpdateRemote()
		{	
		}

		public void RemoveRow(string rowName)
		{
			// USER_4 == GUI sends remove event

			BankRemotePacket p = new BankRemotePacket(Pipe);
            p.Begin(BankRemotePacket.EnumPacketType.USER_4, GetWidgetTypeGUID(), Id); 
			p.Write_const_char(rowName);
			p.Send();
		}

        public static bool RemoteHandler(BankRemotePacket packet, out String errorMessage)
		{
			// USER_0 == column header
			// USER_1 == add item
			// USER_2 == remove item
            bool handled = false;

			if (packet.BankCommand == BankRemotePacket.EnumPacketType.CREATE) 
			{
				packet.Begin();

				uint remoteParent		= packet.Read_bkWidget();
				String title			= packet.Read_const_char();
				String memo				= packet.Read_const_char();
                Color fillColor = Widget.DeserializeColor( packet.Read_const_char() );
                bool readOnly = packet.Read_bool();
							
				packet.End();
                
                Widget parent;
                if (packet.BankPipe.RemoteToLocal(remoteParent, out parent, out errorMessage))
                {
                    WidgetList local = new WidgetList(packet.BankPipe, packet.Id, title, memo, fillColor, readOnly);
                    packet.BankPipe.Associate(packet.Id, local);
                    parent.AddChild(local);
                    handled = true;
                }
			}
			else if (packet.BankCommand == BankRemotePacket.EnumPacketType.USER_0)
            {
                // add column type:
				packet.Begin();

				// find the local version of the the remote widget:
				WidgetList local;
                if (packet.BankPipe.RemoteToLocal(packet.Id, out local, out errorMessage))
                {
                    int column = packet.Read_s32();
                    String heading = packet.Read_const_char();
                    Type type = (Type)packet.Read_u32();
                    local.AddColumnHeader(column, heading, type);
                    packet.End();
                    handled = true;
                }
			}
			else if (packet.BankCommand == BankRemotePacket.EnumPacketType.USER_1)
            {
                // add an item to the list view:
				packet.Begin();

				// find the local version of the the remote widget:
				WidgetList local;
                if (packet.BankPipe.RemoteToLocal(packet.Id, out local, out errorMessage))
                {
                    int key = packet.Read_s32();
                    int column = packet.Read_s32();
                    if (local.m_ColumnTypes[column] == Type.STRING)
                        local.AddItem(key, column, packet.Read_const_char());
                    else if (local.m_ColumnTypes[column] == Type.INT)
                        local.AddItem(key, column, packet.Read_s32());
                    else if (local.m_ColumnTypes[column] == Type.FLOAT)
                        local.AddItem(key, column, packet.Read_float());

                    packet.End();
                    handled = true;
                }
			}
			else if (packet.BankCommand == BankRemotePacket.EnumPacketType.USER_2)
            {
                // remove an item:
				packet.Begin();

				// find the local version of the the remote widget:
				WidgetList local;
                if (packet.BankPipe.RemoteToLocal(packet.Id, out local, out errorMessage))
                {
                    int key = packet.Read_s32();
                    local.RemoveItem(key);
                    packet.End();
                    handled = true;
                }
            }
            else if (packet.BankCommand == BankRemotePacket.EnumPacketType.USER_3 ||
                packet.BankCommand == BankRemotePacket.EnumPacketType.USER_4 ||
                packet.BankCommand == BankRemotePacket.EnumPacketType.USER_5)
            {
                handled = true;
                errorMessage = null;
            }
            else
            {
                handled = Widget.RemoteHandlerBase(packet, GetStaticGUID(), out errorMessage);
            }

            return handled;
		}

		public static void AddHandlers( BankRemotePacketProcessor packetProcessor )
		{
			packetProcessor.AddType(GetStaticGUID(),new BankRemotePacketProcessor.HandlerDel(RemoteHandler));
		}
	}
}
