using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace ragCompression
{
    /// <summary>
    /// Abstract class for a set of compressed and uncompressed data that is used
    /// by ragCompression.CompressionFactory.  Mirrored in C++ in rage/base/src/system.
    /// 
    /// Derived classes determine the header data and compression/decompression methods used.
    /// GetHeaderPrefix() should be unique.  The derived class must be registered with its
    /// Compression Factory so a stream of compressed data can be decompressed properly.
    /// </summary>
    public abstract class CompressionData
    {
        public CompressionData()
        {
        }

        #region Variables
        private byte[] m_compressedData;
        private UInt32 m_compressedSize;

        private byte[] m_decompressedData;
        private UInt32 m_decompressedSize;
        #endregion

        #region Properties
        /// <summary>
        /// Get:  Retrieves the CompressedData buffer.  This could be null or "empty" until one of the Compress() functions has been 
        ///     called, or SetCompressedData() is called.
        /// Set:  Sets the CompressedData buffer which is managed externally.  To decompress, set CompressedSize before calling 
        ///     Decompress(bool) or Decompress(UInt32,bool), or just call Decompress(UInt32 compressedSize,UInt32,bool).
        /// </summary>
        public byte[] CompressedData
        {
            get { return m_compressedData; }
        }

        /// <summary>
        /// Get:  Retrieves the size of the compressed data in CompressedData buffer.  Always less than or equal to CompressedData.Length.
        ///     0 if we haven't compressed the data yet.
        /// Set:  Sets the size of the data in CompressedData.  Useful when calling SetDecompressedData(...) followed by Compress(...). 
        /// </summary>
        public UInt32 CompressedSize
        {
            get { return m_compressedSize; }
            set { m_compressedSize = value; }
        }

        /// <summary>
        /// Get:  Retrieves the DecompressedData buffer.  This could be null or "empty" until one of the Decompress() functions 
        ///     has been called, or SetDecompressedData() is called.
        /// Set:  Sets the DecompressedData buffer which is managed externally.  To compress, set DecompressedSize before calling 
        ///     Compress(bool), or just call Compress(UInt32 uncompressedSize,bool).  To decompress, set DecompressedSize before calling 
        ///     Decompress(bool), or just call Decompress(UInt32 uncompressedSize,bool), or just call 
        ///     Decompress(UInt32,UInt32 uncompressedSize,bool).
        /// </summary>
        public byte[] DecompressedData
        {
            get { return m_decompressedData; }
        }

        /// <summary>
        /// Get:  Retrieves the size of the decompressed data in DecompressedData buffer.  Always less than or equal to DecompressedData.Length.
        ///     0 if we haven't decompressed the data yet.
        /// Set:  Sets the decompressed size of the data in CompressionData.  Useful when calling SetCompressedData(...) followed by 
        ///     Decompress(...).  Not needed if ParseHeader(...) was called before Decompress(...)
        /// </summary>
        public UInt32 DecompressedSize
        {
            get { return m_decompressedSize; }
            set { m_decompressedSize = value; }
        }
        #endregion

        #region Abstract Functions
        /// <summary>
        /// Creates a new instance of this class to be used by the ragCompression.CompressionFactory
        /// </summary>
        /// <returns></returns>
        public abstract CompressionData CreateNewInstance();

        /// <summary>
        /// Retrieves an estimate of the maximum length the of the compressed data with the given size
        /// </summary>
        /// <param name="uncompressedSize"></param>
        /// <returns></returns>
        public abstract UInt32 GetCompressUpperBound( UInt32 uncompressedSize );

        /// <summary>
        /// Retrieves the size of this class's header data
        /// </summary>
        /// <returns></returns>
        public abstract int GetHeaderSize();

        /// <summary>
        /// Retrieves the unique header prefix string so the Compression Factory can find the start of a CompressedData set
        /// </summary>
        /// <returns></returns>
        public abstract string GetHeaderPrefix();

        /// <summary>
        /// Builds the header section for the Compression Factory
        /// </summary>
        /// <param name="buf">buffer to write to</param>
        /// <param name="startIndex">index to start at</param>
        /// <returns>length of header</returns>
        public abstract int BuildHeader( byte[] buf, int startIndex );

        /// <summary>
        /// Retrieves the compressed data size and decompressed data size
        /// </summary>
        /// <param name="data">compressed data, starting with the header prefix</param>
        /// <param name="startIndex">index to start at</param>
        /// <returns>true if the header was parsed correctly</returns>
        public abstract bool ParseHeader( byte[] data, int startIndex );

        /// <summary>
        /// Compresses the data into a buffer
        /// </summary>
        /// <param name="compressedData">buffer to write the compressed data to</param>
        /// <param name="uncompressedData">data that we want to compress</param>
        /// <param name="uncompressedSize">number of bytes to be compressed</param>
        /// <returns>the number of bytes compressed to compressedData.  0 on error.</returns>
        public abstract UInt32 Compress( byte[] compressedData, byte[] uncompressedData, UInt32 uncompressedSize );

        /// <summary>
        /// Decompresses the data into a buffer of the specified length
        /// </summary>
        /// <param name="decompressedData">buffer to decompress the data to</param>
        /// <param name="decompressedSize">number of bytes in the uncompressed stream (the original DecompressedSize)</param>
        /// <param name="compressedData">data to be decompressed</param>
        /// <param name="compressedSize">number of bytes to be decompressed</param>
        /// <returns>true on success</returns>
        public abstract bool Decompress( byte[] decompressedData, UInt32 decompressedSize, byte[] compressedData, UInt32 compressedSize );
        #endregion

        #region Public Functions
        /// <summary>
        /// Sets the CompressedData buffer. Such as before calling one of the Decompress functions.  Unlike the game-side code,
        /// we will always make a copy of the buffer and the new size will match compressedSize if it is non-zero.
        /// </summary>
        /// <param name="compressedData">compressed data buffer</param>
        /// <param name="startIndex">index to start copying from</param>
        /// <param name="compressedSize">number of bytes of compressed data.  0 if no data present.</param>
        public void SetCompressedData( byte[] compressedData, int startIndex, UInt32 compressedSize )
        {
            int size = (compressedSize == 0) ? m_compressedData.Length : (int)compressedSize;
            m_compressedData = new byte[size];
            Array.ConstrainedCopy( compressedData, startIndex, m_compressedData, 0, size );

            m_compressedSize = compressedSize;
        }

        /// <summary>
        /// Sets the DecompressedData buffer. Such as before calling one of the Compress functions.  Unlike the game-side code,
        /// we will always make a copy of the buffer and the new size will match compressedSize if it is non-zero.
        /// </summary>
        /// <param name="decompressedData">the data we will want to compress</param>
        /// <param name="startIndex">index to start copying from</param>
        /// <param name="decompressedSize">number of bytes used.  0 if no data present.</param>
        public void SetDecompressedData( byte[] decompressedData, int startIndex, UInt32 decompressedSize )
        {
            Debug.Assert( m_decompressedData == null );

            int size = (decompressedSize == 0) ? decompressedData.Length : (int)decompressedSize;
            m_decompressedData = new byte[size];
            Array.ConstrainedCopy( decompressedData, startIndex, m_decompressedData, 0, size );

            m_decompressedSize = decompressedSize;
        }

        /// <summary>
        /// Compresses the data in DecompressedData, saving everything in our member variables
        /// NOTE: Can only be called if the Decompressed Size was set to a non-zero value
        /// </summary>
        /// <param name="resize">true will resize CompressedData so that CompressedData.Length == CompressedSize after 
        ///     compressing the data, but only if OwnsCompressedData returns true.</param>
        /// <returns>true on success</returns>
        public bool Compress( bool resize )
        {
            Debug.Assert( m_decompressedData != null );
            Debug.Assert( m_decompressedSize > 0 );

            // call the derived GetCompressUpperBound method
            UInt32 compressedLength = GetCompressUpperBound( m_decompressedSize );
            if ( m_compressedData != null && (m_decompressedData.Length < compressedLength) )
            {
                m_compressedData = null;
            }

            if ( m_compressedData == null )
            {
                m_compressedData = new byte[compressedLength];
            }

            // call the derived Compress method
            m_compressedSize = Compress( m_compressedData, m_decompressedData, m_decompressedSize );
            Debug.Assert( m_compressedSize <= m_compressedData.Length );

            if (resize && m_compressedData != null && (m_compressedSize > 0) && (m_compressedSize < m_decompressedData.Length))
            {
                byte[] data = new byte[m_compressedSize];
                Array.ConstrainedCopy( m_compressedData, 0, data, 0, (int)m_compressedSize );

                m_compressedData = data;
            }

            return m_compressedSize > 0;
        }

        /// <summary>
        /// Compresses the data in DecompressedData, saving everything in our member variables
        /// NOTE: This needs to be called instead of Compress(bool) if the Decompressed Size was never set
        /// </summary>
        /// <param name="uncompressedSize">the number of bytes that was added to DeompressedData</param>
        /// <param name="resize">true will resize CompressedData so that CompressedData.Length == CompressedSize after 
        ///     compressing the data, but only if OwnsCompressedData returns true.</param>
        /// <returns>true on success</returns>
        public bool Compress( UInt32 uncompressedSize, bool resize )
        {
            m_decompressedSize = uncompressedSize;
            return Compress( resize ); 
        }

        /// <summary>
        /// Decompresses the data in CompressedData, saving everything in our member variables
        /// </summary>
        /// <param name="resize">true will resize DecompressedData so that DecompressedData.Length == DecompressedSize after 
        ///     compressing the data, but only if OwnsDecompresedData returns true.</param>
        /// <returns>true on success</returns>
        public bool Decompress( bool resize )
        {
            Debug.Assert( m_compressedData != null );
            Debug.Assert( m_compressedSize > 0 );
            Debug.Assert( m_decompressedSize > 0 );

            if ( m_decompressedData != null && (m_decompressedData.Length < m_decompressedSize) )
            {
                m_decompressedData = null;
            }

            if ( m_decompressedData == null )
            {
                m_decompressedData = new byte[m_decompressedSize];
            }

            // call the derived Decompress method
            bool result = Decompress( m_decompressedData, m_decompressedSize, m_compressedData, m_compressedSize );
            if ( result && resize && m_decompressedData != null && (m_decompressedSize > 0) && (m_decompressedSize < m_decompressedData.Length) )
            {
                byte[] data = new byte[m_decompressedSize];
                Array.ConstrainedCopy( m_decompressedData, 0, data, 0, (int)m_decompressedSize );

                m_decompressedData = data;
            }

            return result;
        }

        /// <summary>
        /// Decompresses the data in CompressedData, saving everything in our member variables
        /// NOTE: Needs to be called if CompressedSize was set but not DecompressedSize, or ParseHeader(...) was not called.
        /// </summary>
        /// <param name="uncompressedSize">the size of the data before it was compressed</param>
        /// <param name="resize">true will resize DecompressedData so that DecompressedData.Length == DecompressedSize after 
        ///     compressing the data, but only if OwnsDecompresedData returns true.</param>
        /// <returns>true on success</returns>
        public bool Decompress( UInt32 uncompressedSize, bool resize )
        {
            m_decompressedSize = uncompressedSize;
            return Decompress( resize ); 
        }

        /// <summary>
        /// Decompresses the data in CompressedData, saving everything in our member variables
        /// NOTE: Needs to be called if both CompressedSize and DecompressedSize were not set, or ParseHeader(...) was not called.
        /// </summary>
        /// <param name="compressedSize">the number of bytes that was added to CompressedData</param>
        /// <param name="uncompressedSize">the size of the data before it was compressed</param>
        /// <param name="resize">true will resize DecompressedData so that DecompressedData.Length == DecompressedSize after 
        ///     compressing the data, but only if OwnsDecompresedData returns true.</param>
        /// <returns>true on success</returns>
        public bool Decompress( UInt32 compressedSize, UInt32 uncompressedSize, bool resize )
        {
            m_compressedSize = compressedSize;
            m_decompressedSize = uncompressedSize;
            return Decompress( resize );
        }
        #endregion
    }
}
