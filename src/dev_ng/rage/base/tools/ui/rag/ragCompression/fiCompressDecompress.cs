using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace ragCompression
{
    /// <summary>
    /// Mirrored in C++ in rage/base/src/file.
    /// </summary>
    public class fiCompressDecompress
    {
        #region Constants
        // Initial width currently has to be two so that initial bias is correct when invoked from encode_length
        private const Int32 c_InitialWidth = 2;
        private const Int32 c_WidthStep = 2;

        // Magic number (to allow for future configurability)
        private const byte c_Magic_1 = 0xDC;
        private const byte c_Magic_2 = 0xE0;
        #endregion

        #region Public Functions
        /// <summary>
        /// Compress data using a sliding window algorithm
        /// </summary>
        /// <param name="dest">destination buffer</param>
        /// <param name="src">src buffer (data to compress)</param>
        /// <param name="srcSize">Amount of data to compress</param>
        /// <param name="maxLookback">Maximum lookback (log2).  15 or 16 is pretty fast and still yields very good results.</param>
        /// <param name="fullUpdate">If true, does more exhaustive table update which is substantially more expensive but 
        ///     improves compression by about 10% or so.  Varies by data.</param>
        /// <returns>Number of bytes written to destination buffer.  If this is greater than or equal to srcSize, 
        ///     calling code should assume the compression failed (common with very small datasets)</returns>
        public static UInt32 Compress( byte[] dest, byte[] src, UInt32 srcSize, UInt32 maxLookback, bool fullUpdate )
        {
            if ( srcSize < 4 )
            {
                return srcSize;
            }

            UInt32 c_MaxLookback = (UInt32)(1 << (Int32)maxLookback);

            const int lookup_size = 65536;

            node[] lookup = new node[lookup_size];  //node** lookup = (node**)(new node*[lookup_size]);
            UInt32 poolSize = srcSize + 1024;
            node[] pool = new node[poolSize];   //node* pool = (node*)(new node[poolSize]);
            UInt32 poolCount = 0;

            UInt32 literalCount = 0, windowCount = 0;
            UInt32 literalRun = 0;
            UInt64 estimate = 0;

            EncoderState state = new EncoderState();
            state.m_Accum = 1;
            state.m_Offset = 2;
            state.m_NextAccum = 2;
            state.m_Output = dest;
            state.m_OutputSize = (UInt32)dest.Length;

            dest[0] = c_Magic_1;
            dest[1] = c_Magic_2;

            UInt32 offset = 0;
            while ( offset < srcSize - 2 )
            {
                int key = src[offset] | (src[offset + 1] << 8);
                int piKey = key;
                bool piIsNext = false;
                node pi = lookup[piKey];  //node** pi = &lookup[key];
                node i = lookup[piKey];//node i = pi;    //node* i = *pi;
                UInt32 bestRun = 0;
                UInt32 bestOffset = 0;
                while ( i != null )
                {
                    // Runs are stored with closest ones first in the list, so earlier runs will always be cheaper.
                    // For short runs, do a sanity check to make sure that storing it as a literal run wouldn't be cheaper.
                    UInt32 thisRun = leading_match( src, i.offset, src, offset, srcSize - offset );
                    if ( thisRun > bestRun &&
                        ((thisRun == 2 && 2 + cost_for_offset( offset - i.offset ) < (2 + 2 * 8) /* size of a 2-literal run */)
                        || (thisRun == 3 && 4 + cost_for_offset( offset - i.offset ) < (4 + 3 * 8) /* size of a 3-literal run */)
                        || (thisRun == 4 && 4 + cost_for_offset( offset - i.offset ) < (4 + 4 * 8) /* size of a 4-literal run */)
                        || (thisRun > 4)
                        ) )
                    {
                        bestRun = thisRun;
                        bestOffset = i.offset;
                    }
                    else if ( offset - i.offset > c_MaxLookback )
                    {
                        // Delete nodes that are too far back.
                        if ( piKey != -1 )
                        {
                            pi = lookup[piKey] = i;//pi = i.next;    //*pi = i->next;
                            piKey = -1;
                            piIsNext = true;
                        }
                        else if ( piIsNext )
                        {
                            pi.next = i.next;
                        }
                        else
                        {
                            pi = i.next;
                        }

                        i = i.next; //i = i->next;
                        continue;
                    }

                    pi = i; //pi = i.next;    //pi = &i->next;
                    piKey = -1;
                    piIsNext = true;
                    i = i.next; //i = pi; //i = *pi;
                }
                Debug.Assert( poolCount < poolSize );
                node nn = new node();
                pool[poolCount++] = nn;
                nn.offset = offset;
                nn.next = lookup[key];
                lookup[key] = nn;

                if ( bestRun > 1 )
                {
                    FlushRun( ref literalRun, ref state, estimate, src, offset );

                    estimate += encode_length( ref state, bestRun - 1 );
                    put_bits( ref state, 1, 1 );
                    estimate += 1 + encode_offset( ref state, offset - bestOffset );

                    ++windowCount;

                    // Add "closer" copy of the windowed data to the work queue.
                    // (the initial two bytes of the window run were already added above)
                    while ( --bestRun != 0 )
                    {
                        ++offset;
                        // This produces slightly better results but is never practical except for lookbacks 64k or less
                        if ( fullUpdate )
                        {
                            int key1 = src[offset] | (src[offset + 1] << 8);
                            Debug.Assert( poolCount < poolSize );
                            node nn1 = new node();
                            pool[poolCount++] = nn1;
                            nn1.offset = offset;
                            nn1.next = lookup[key1];
                            lookup[key1] = nn1;
                        }
                    }
                    ++offset;
                }
                else
                {
                    ++literalRun;
                    ++literalCount;
                    ++offset;
                }
            }

            literalRun += (srcSize - offset);
            offset = srcSize;
            FlushRun( ref literalRun, ref state, estimate, src, offset );

            // Flush the last control byte if necessary.
            if ( state.m_Accum > 1 )
            {
                put_bits( ref state, 0, 8 );
            }

            // clean up allocations
            pool = null;
            lookup = null;

            return state.m_Offset;
        }

        /// <summary>
        /// Compute a reasonable upper bound on the compression space needed
        /// </summary>
        /// <param name="srcSize">Size of source buffer</param>
        /// <returns>Estimated upper bound (for use in fiCompress destSize)</returns>
        public static UInt32 CompressUpperBound( UInt32 srcSize )
        {
            if ( srcSize < 256 )
            {
                return 512;
            }
            else
            {
                return (srcSize * 5) >> 2;
            }
        }

        /// <summary>
        /// Decompress data compressed with fiCompress
        /// </summary>
        /// <param name="dest">Output buffer</param>
        /// <param name="destSize">number of bytes to decompress</param>
        /// <param name="src">Compressed data stream</param>
        /// <returns>True if decompression was successful, or false on failure (due to bad input data).  
        ///     Should never crash on malformed inputs.</returns>
        public static bool Decompress( byte[] dest, UInt32 destSize, byte[] src )
        {
            Int32 accum = 0x10000;
            UInt32 offset = 0;

            //bool overlapped = false;

            UInt32 indexSrc = 0;
            if ( src[0] != c_Magic_1 || src[1] != c_Magic_2 )
            {
                return false;
            }
            indexSrc += 2;

            while ( offset < destSize )
            {
                /*
                if ( overlapped && dest + offset > src )
                {
                    return false;
                }
                */

                // Decode length (either literal count or window length minus one)
                UInt32 length = 0;
                if ( get_bits( ref accum, src, ref indexSrc, 1 ) != 0 )
                {
                    length = 1;
                }
                else if ( get_bits( ref accum, src, ref indexSrc, 1 ) != 0 )
                {	// 4 or 5-20
                    if ( get_bits( ref accum, src, ref indexSrc, 1 ) != 0 )
                    {	// 5-20+
                        UInt32 width = c_InitialWidth + c_WidthStep;
                        UInt32 bias = 1 + (1 << c_InitialWidth);
                        while ( get_bits( ref accum, src, ref indexSrc, 1 ) != 0 )
                        {
                            bias += (UInt32)(1 << (Int32)width);
                            width += c_WidthStep;
                        }
                        length = bias + get_bits( ref accum, src, ref indexSrc, (Int32)width );
                    }
                    else			// 4
                    {
                        length = 4;
                    }
                }
                else if ( get_bits( ref accum, src, ref indexSrc, 1 ) != 0 )
                {
                    length = 3;
                }
                else
                {
                    length = 2;
                }

                // Frame type:
                if ( get_bits( ref accum, src, ref indexSrc, 1 ) != 0 )
                {		// window
                    ++length;			// min window length is two
                    UInt32 width = c_InitialWidth;
                    UInt32 bias = 1;
                    while ( get_bits( ref accum, src, ref indexSrc, 1 ) != 0 )
                    {
                        bias += (UInt32)(1 << (Int32)width);
                        width += c_WidthStep;
                    }
                    UInt32 backup = bias + get_bits( ref accum, src, ref indexSrc, (Int32)width );
                    if ( backup > offset )
                    {
                        return false;
                    }

                    do
                    {
                        if ( offset >= destSize )
                        {
                            return false;
                        }

                        dest[offset] = dest[offset - backup];
                        ++offset;
                    } while ( --length != 0 );
                }
                else
                {					// literals
                    do
                    {
                        if ( offset >= destSize )
                        {
                            return false;
                        }

                        dest[offset] = src[indexSrc];
                        ++offset;
                        ++indexSrc;
                    } while ( --length != 0 );
                }
            }

            if ( offset != destSize )
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        #endregion

        #region Helper Structures
        private class node
        {
            public UInt32 offset;		// offset of the two leader bytes
            public node next;		 // pointer to next node
        };

        private class EncoderState
        {
            public UInt32 m_Accum;
            public UInt32 m_NextAccum;
            public UInt32 m_Offset;
            public byte[] m_Output;
            public UInt32 m_OutputSize;
        };
        #endregion

        #region Helper Functions
        private static void FlushRun( ref UInt32 literalRun, ref EncoderState state, UInt64 estimate, byte[] src, UInt32 offset )
        {
            if ( literalRun != 0 )
            {
                estimate += 1 + encode_length( ref state, literalRun );
                put_bits( ref state, 0, 1 );
                for ( UInt32 i = 0; i < literalRun; i++ )
                {
                    estimate += 8;
                    state.m_Output[state.m_Offset++] = src[offset - literalRun + i];
                    Debug.Assert( state.m_Offset < state.m_OutputSize );
                }

                literalRun = 0;
            }
        }

        private static UInt32 leading_match( byte[] a, UInt32 indexA, byte[] b, UInt32 indexB, UInt32 maxLen )
        {
            UInt32 count = 0;
            while ( (count < maxLen) && (a[indexA] == b[indexB]) )
            {
                ++count;
                ++indexA;
                ++indexB;
            }
            return count;
        }

        private static void put_bits( ref EncoderState state, UInt32 value, UInt32 width )
        {
            Debug.Assert( width < 32 );
            Debug.Assert( value < (UInt32)(1 << (Int32)width) );

            while ( width != 0 )
            {
                if ( state.m_Accum == 1 )
                {
                    state.m_NextAccum = state.m_Offset++;
                    Debug.Assert( state.m_NextAccum < state.m_OutputSize );
                }
                state.m_Accum = (state.m_Accum << 1) | ((UInt32)((Int32)value >> (Int32)(--width)) & 1);
                if ( (state.m_Accum & 0x100) != 0 )
                {
                    state.m_Output[state.m_NextAccum] = (byte)state.m_Accum;
                    state.m_Accum = 1;
                }
            }
        }

        private static UInt32 encode_offset( ref EncoderState state, UInt32 len )
        {
            // 0xx = 1,2,3,4 (when c_InitialWidth=2)
            // 10xxxxxx = 5,6,7...
            // 110xxxxxxxxxx
            // Every leading one adds c_WidthStep more bits (and builds on the previous range)
            Debug.Assert( len != 0 );
            UInt32 leader = 1;
            UInt32 width = c_InitialWidth;
            UInt32 mini = 1;
            UInt32 maxi = 1 << c_InitialWidth;
            while ( len > maxi )
            {
                put_bits( ref state, 1, 1 );
                mini = maxi + 1;
                width += c_WidthStep;
                maxi += (UInt32)(1 << (Int32)width);
                ++leader;
            }
            put_bits( ref state, 0, 1 );
            Debug.Assert( len >= mini && len <= maxi );
            put_bits( ref state, len - mini, width );
            return leader + width;
        }

        private static UInt32 cost_for_offset( UInt32 len )
        {
            UInt32 leader = 1;
            UInt32 width = c_InitialWidth;
            UInt32 mini = 1;
            UInt32 maxi = 1 << c_InitialWidth;
            while ( len > maxi )
            {
                mini = maxi + 1;
                width += c_WidthStep;
                maxi += (UInt32)(1 << (Int32)width);
                ++leader;
            }
            return leader + width;
        }

        private static UInt32 encode_length( ref EncoderState state, UInt32 len )
        {
            // 1   -> 1
            // 000 -> 2
            // 001 -> 3
            // 010 -> 4
            // 011xxx -> as per encode_offset
            if ( len == 1 )
            {
                put_bits( ref state, 1, 1 );
                return 1;
            }
            else if ( len == 2 || len == 3 || len == 4 )
            {
                put_bits( ref state, len - 2, 3 );
                return 3;
            }
            else
            {
                // fall through to common encoding (albeit with longer prefix)
                put_bits( ref state, 1, 2 );
                return 2 + encode_offset( ref state, len );
            }
        }

        private static UInt32 get_bits( ref Int32 accum, byte[] src, ref UInt32 indexSrc, Int32 count )
        {
            UInt32 result = 0;
            Debug.Assert( count != 0 );
            do
            {
                if ( (accum & 0x10000) != 0 )
                {
                    // FIXME: indexSrc might need to be ref'd back to calling function
                    accum = 0x100 | src[indexSrc];
                    ++indexSrc;
                }

                result = (result << 1) | (UInt32)((accum & 0x80) >> 7);
                accum <<= 1;
            } while ( --count != 0 );

            return result;
        }
        #endregion
    }
}
