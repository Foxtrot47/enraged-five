using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace ragCompression
{
    /// <summary>
    /// Class for integrating uncompressed data buffers with a compression factory.
    /// Mirrored in C++ in rage/base/src/system.
    /// </summary>
    public class noCompressionData : CompressionData
    {
        public noCompressionData()
        {

        }

        #region Abstract Function Overrides
        /// <summary>
        /// Creates a new instance of this class to be used by the ragCompression.CompressionFactory
        /// </summary>
        /// <returns></returns>
        public override CompressionData CreateNewInstance()
        {
            return new noCompressionData();
        }

        /// <summary>
        /// Retrieves an estimate of the maximum length the of the compressed data with the given size using
        /// datCompressDecompress.CompressUpperBound.
        /// </summary>
        /// <param name="uncompressedSize"></param>
        /// <returns></returns>
        public override UInt32 GetCompressUpperBound( UInt32 uncompressedSize )
        {
            return uncompressedSize;
        }

        /// <summary>
        /// Retrieves the size of this class's header data
        /// </summary>
        /// <returns></returns>
        public override int GetHeaderSize()
        {
            return 9; // header: CHN:0000: where the 0000 is a u32 and everything else is a character
        }

        /// <summary>
        /// Retrieves the unique header prefix string so the Compression Factory can find the start of a CompressedData set
        /// </summary>
        /// <returns></returns>
        public override string GetHeaderPrefix()
        {
            return noCompressionData.GetStaticHeaderPrefix();
        }

        /// <summary>
        /// Builds the header section for the Compression Factory
        /// </summary>
        /// <param name="buf">buffer to write to</param>
        /// <param name="startIndex">index to start at</param>
        /// <returns>length of header</returns>
        public override int BuildHeader( byte[] buf, int startIndex )
        {
            string prefix = GetHeaderPrefix();
            int len = prefix.Length;
            for ( int i = 0; i < len; ++i )
            {
                buf[startIndex + i] = (byte)prefix[i];
            }

            int index = startIndex + len;

            Debug.Assert( this.CompressedSize == this.DecompressedSize );
            UInt32 compressedSize = this.CompressedSize;
            buf[index++] = (byte)(compressedSize);
            buf[index++] = (byte)(compressedSize >> 8);
            buf[index++] = (byte)(compressedSize >> 16);
            buf[index++] = (byte)(compressedSize >> 24);
            buf[index++] = (byte)':';

            Debug.Assert( index < buf.Length );
            return index;
        }

        /// <summary>
        /// Retrieves the compressed data size and decompressed data size
        /// </summary>
        /// <param name="data">compressed data, starting with the header prefix</param>
        /// <param name="startIndex">index to start at</param>
        /// <returns>true if the header was parsed correctly</returns>
        public override bool ParseHeader( byte[] data, int startIndex )
        {
            if ( data.Length - startIndex < (UInt32)GetHeaderSize() )
            {
                // not enough data
                return false;
            }

            string prefix = GetHeaderPrefix();
            int len = prefix.Length;
            for ( int i = 0; i < len; ++i )
            {
                if ( data[startIndex + i] != (byte)prefix[i] )
                {
                    // bad header prefix
                    return false;
                }
            }

            int index = startIndex + len;

            UInt32 cSize = (UInt32)(data[index++]);
            cSize |= (UInt32)(data[index++] << 8);
            cSize |= (UInt32)(data[index++] << 16);
            cSize |= (UInt32)(data[index++] << 24);

            if ( data[index++] != (byte)':' )
            {
                // bad header
                return false;
            }

            if ( data.Length - startIndex < GetHeaderSize() + cSize )
            {
                // not enough data
                return false;
            }

            this.CompressedSize = cSize;
            this.DecompressedSize = cSize;
            return true;
        }

        /// <summary>
        /// Compresses the data into a buffer
        /// </summary>
        /// <param name="compressedData">buffer to write the compressed data to</param>
        /// <param name="uncompressedData">data that we want to compress</param>
        /// <param name="uncompressedSize">number of bytes to be compressed</param>
        /// <returns>the number of bytes compressed to compressedData.  0 on error.</returns>
        public override UInt32 Compress( byte[] compressedData, byte[] uncompressedData, UInt32 uncompressedSize )
        {
            uncompressedData.CopyTo( compressedData, 0 );
            return uncompressedSize;
        }

        /// <summary>
        /// Decompresses the data into a buffer of the specified length
        /// </summary>
        /// <param name="decompressedData">buffer to decompress the data to</param>
        /// <param name="decompressedSize">number of bytes in the uncompressed stream (the original DecompressedSize)</param>
        /// <param name="compressedData">data to be decompressed</param>
        /// <param name="compressedSize">number of bytes to be decompressed</param>
        /// <returns>true on success</returns>
        public override bool Decompress( byte[] decompressedData, UInt32 decompressedSize, byte[] compressedData, UInt32 compressedSize )
        {
            compressedData.CopyTo( decompressedData, 0 );
            return true;
        }
        #endregion

        #region Static Functions
        public static string GetStaticHeaderPrefix()
        {
            return "CHN:";
        }
        #endregion
    }
}
