using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Data;

namespace ragCompression
{
    /// <summary>
    /// A class to construct and destruct multiple sets of compressed and uncompressed
    /// data into consecutive streams.  Mirrored in C++ in rage/base/src/system/compressionfactory.
    /// 
    /// Classes derived from ragCompression.CompressionData must be registered in order to operate the 
    /// compress and decompress functions.
    /// </summary>
    public class CompressionFactory
    {
        public CompressionFactory()
        {

        }

        #region Variables
	    private byte[] m_readData;
	    private Dictionary<string,CompressionData> m_registeredTypes = new Dictionary<string,CompressionData>();
	    private List<CompressionData> m_compressionData = new List<CompressionData>();
        #endregion

        #region Properties
        /// <summary>
        /// Get:  Retrieves the list of ragCompression.CompressionData objects.  This can be used to grow the list before calling 
        /// CompressData or to read the list after calling DecompressData.  Clearing the list and deleting every item 
        /// is up to the user.
        /// </summary>
        public List<CompressionData> Data
        {
            get { return m_compressionData; }
        }
        #endregion

        #region Functions
        public bool HasDataLeftToProcess()
        {
            return m_readData != null;
        }
        /// <summary>
        /// Adds a derived ragCompression.CompressionData class to the types that can be handled by this factory.
        /// </summary>
        /// <param name="data"></param>
	    public void RegisterCompressionData( CompressionData cData )
        {
            if ( !m_registeredTypes.ContainsKey( cData.GetHeaderPrefix() ) )
            {
                m_registeredTypes.Add( cData.GetHeaderPrefix(), cData );
            }
        }

        /// <summary>
        /// Using all items in the ragCompression.CompressionData list, builds one massive stream out of each set of compressed data,
        /// adding the header info to each one.  Compression is performed if it wasn't already.
        /// </summary>
        /// <returns>the compressed data.  null on error.</returns>
	    public byte[] CompressData()
        {
	        // Compress if needed and determine total length
	        UInt32 dataSize = 0;
	        foreach ( CompressionData cData in m_compressionData )
	        {
		        if ( cData.CompressedSize == 0 )
		        {
			        cData.Compress( false );
		        }

		        if ( cData.CompressedSize > 0 )
		        {
			        dataSize += (UInt32)cData.GetHeaderSize() + cData.CompressedSize;
		        }
	        }

	        if ( dataSize == 0 )
	        {
		        return null;
	        }

	        // build the whole stream
	        byte[] compressedStream = new byte[dataSize];
	        int writeIndex = 0;
	        foreach ( CompressionData cData in m_compressionData )
	        {
		        if ( cData.CompressedSize > 0 )
		        {
			        int headerSize = cData.BuildHeader( compressedStream, writeIndex );
			        writeIndex += headerSize;
                    Array.ConstrainedCopy( cData.CompressedData, 0, compressedStream, writeIndex, (int)cData.CompressedSize );
                    writeIndex += (int)cData.CompressedSize;
		        }
	        }

	        return compressedStream;
        }


        bool ReadHeaderPrefix(byte[] data, int start, out string prefix)
        {
            const int c_maxHeaderSize = 4;         // The first 4 characters should contain the type of compression used in this header

            if (start + c_maxHeaderSize > data.Length)
            {
				// not enough data, incomplete header!
                prefix = null;
                return false;
            }

            char[] headerBytes = new char[c_maxHeaderSize];
            int i = 0;
            for (i = 0; i < c_maxHeaderSize; i++)
            {
                headerBytes[i] = (char)data[i + start];
                if (headerBytes[i] == ':') // all headers MUST end with a ':'
                {
                    break;
                }
            }

            if (i == c_maxHeaderSize)
            {
                string msg = "Received badly formatted data at start=" + start + Environment.NewLine + BitConverter.ToString( data );
                throw new DataException(msg);
            }

            prefix = new string(headerBytes, 0, i+1);
            return true;
        }

        /// <summary>
        /// Takes in an array of bytes and processes each compressed stream, decompressing as it goes.
        /// Multiple sets of compressed data could exist in the data array, and a partial set may exist at the end
        /// of the array.  When this partial data exists, we return true.  Call DecompressData again with the rest of the data
        /// (example: reading from a Socket) to process the remaining data and continue.
        /// </summary>
        /// <param name="data">the data we want decompressed</param>
        /// <param name="dataSize">length of data or the number of bytes we want processed</param>
        /// <returns>true if not all of the data could be processed</returns>
        public bool DecompressData(byte[] data, UInt32 dataSize)
        {
	        bool rtn = false;

	        if ( m_readData == null )
	        {
		        m_readData = data;
	        }
	        else // append the new data to the end of the existing data
	        {
		        UInt32 newSize = dataSize + (UInt32)m_readData.Length;
		        byte[] newData = new byte[newSize];
		        int writeIndex = 0;
                Array.ConstrainedCopy( m_readData, 0, newData, writeIndex, m_readData.Length );
		        writeIndex += m_readData.Length;
                Array.ConstrainedCopy( data, 0, newData, writeIndex, (int)dataSize );

		        m_readData = newData;
	        }

            int packetStart = 0;
            bool keepReading = true;

            while(keepReading)
            {
                if (packetStart >= dataSize)
                {
                    keepReading = false;
                    rtn = true;
                    break;
                }

                string headerStr = null;
                if (!ReadHeaderPrefix(m_readData, packetStart, out headerStr))
                {
                    keepReading = false;
                    rtn = true;
                    break;
                }
                
		        CompressionData cData = null;

		        // loop through our registered types and try to match a prefix, the earliest if multiple are found
                foreach (KeyValuePair<string, CompressionData> pair in m_registeredTypes)
                {
                    if (headerStr == pair.Value.GetHeaderPrefix())
                    {
                        cData = pair.Value;
                    }
                }

                Debug.Assert(cData != null, "Couldn't find a decompresser for type " + headerStr);

                if (cData.ParseHeader(m_readData, packetStart))
                {
                    CompressionData newData = cData.CreateNewInstance();

                    newData.SetCompressedData(m_readData, packetStart + cData.GetHeaderSize(), cData.CompressedSize);

                    // have to pass in DecompressedSize because we didn't call newData->ParseHeader(...)
                    if (newData.Decompress(cData.DecompressedSize, true))
                    {
                        m_compressionData.Add(newData);
                        packetStart += cData.GetHeaderSize() + (int)newData.CompressedSize;
                    }
                    else
                    {
                        Debug.Fail("Decompression Failed!");
                    }
                }
                else
                {
                    // either we didn't have full header or were missing data
                    keepReading = false;
                    rtn = true;
                }
            }

            // now we've read everything from 0 to packetStart. If packetStart == m_readData.Length there's no leftover data
            if (packetStart == m_readData.Length)
            {
                m_readData = null;
            }
            else
            {
                // remove data we've already processed
                int newSize = m_readData.Length - packetStart;
                byte[] newData = new byte[newSize];
                Array.ConstrainedCopy(m_readData, packetStart, newData, 0, newSize);

                m_readData = newData;
            }

	        return rtn;
        }
        #endregion
    }
}
