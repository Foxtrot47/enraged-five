using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;

namespace timelinetestapp
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class Form1 : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Label Label_CurChannel;
		private System.Windows.Forms.TextBox Text_CurStartTime;
		private ragPtxTimeLine.ptxTimeLine ptxTimeLine1;
		private System.Windows.Forms.ListBox listBox1;
		private System.Windows.Forms.Button button1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public Form1()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//

			MenuItem AddEmitter = new MenuItem("Add Emitter");
			MenuItem AddEffect = new MenuItem("Add Effect");
			MenuItem AddAction = new MenuItem("Add Action");
			ptxTimeLine1.Menu_Context.MenuItems.Add(AddEmitter);
			ptxTimeLine1.Menu_Context.MenuItems.Add(AddEffect);
			ptxTimeLine1.Menu_Context.MenuItems.Add(AddAction);
			AddEmitter.Click += new System.EventHandler(this.Menu_AddEmitter_Click);
			AddEffect.Click += new System.EventHandler(this.Menu_AddEffect_Click);
			AddAction.Click += new System.EventHandler(this.Menu_AddAction_Click);
			TestListBox();
			
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.Label_CurChannel = new System.Windows.Forms.Label();
            this.Text_CurStartTime = new System.Windows.Forms.TextBox();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.button1 = new System.Windows.Forms.Button();
            this.ptxTimeLine1 = new ragPtxTimeLine.ptxTimeLine();
            this.SuspendLayout();
            // 
            // Label_CurChannel
            // 
            this.Label_CurChannel.Location = new System.Drawing.Point(80, 368);
            this.Label_CurChannel.Name = "Label_CurChannel";
            this.Label_CurChannel.Size = new System.Drawing.Size(100, 23);
            this.Label_CurChannel.TabIndex = 2;
            // 
            // Text_CurStartTime
            // 
            this.Text_CurStartTime.Location = new System.Drawing.Point(192, 368);
            this.Text_CurStartTime.Name = "Text_CurStartTime";
            this.Text_CurStartTime.Size = new System.Drawing.Size(100, 20);
            this.Text_CurStartTime.TabIndex = 3;
            // 
            // listBox1
            // 
            this.listBox1.Location = new System.Drawing.Point(531, 342);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(280, 95);
            this.listBox1.TabIndex = 4;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(375, 379);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 5;
            this.button1.Text = "button1";
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // ptxTimeLine1
            // 
            this.ptxTimeLine1.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ptxTimeLine1.ChanelLabelWidth = 100;
            this.ptxTimeLine1.ChannelHeight = 20;
            this.ptxTimeLine1.Duration = 4F;
            this.ptxTimeLine1.Granularity = 4;
            this.ptxTimeLine1.Location = new System.Drawing.Point(12, 12);
            this.ptxTimeLine1.Name = "ptxTimeLine1";
            this.ptxTimeLine1.Right_Padding = 40;
            this.ptxTimeLine1.Size = new System.Drawing.Size(918, 258);
            this.ptxTimeLine1.TabIndex = 0;
            // 
            // Form1
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1094, 526);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.ptxTimeLine1);
            this.Controls.Add(this.Text_CurStartTime);
            this.Controls.Add(this.Label_CurChannel);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion

		#region Variables
		ragPtxTimeLine.ptxTimeLine.ptxChannel m_CurChannel = null;
		#endregion
		private void TestListBox()
		{
			listBox1.BeginUpdate();
			listBox1.Items.Clear();
			listBox1.ClearSelected();
			listBox1.Items.Add("Hello!");
			listBox1.EndUpdate();
		}
		private void Menu_AddEmitter_Click(object sender, System.EventArgs e)
		{
			Color emitColor = Color.FromArgb(255,127,177,255);
			ragPtxTimeLine.ptxTimeLine.ptxChannel channel = new ragPtxTimeLine.ptxTimeLine.ptxChannel("Emitter 1",2.0f,0.0f,emitColor);
			ptxTimeLine1.AddChannel(channel);

		}
		private void Menu_AddEffect_Click(object sender, System.EventArgs e)
		{
			Color effectColor = Color.FromArgb(255,127,255,177);
			ragPtxTimeLine.ptxTimeLine.ptxChannel channel = new ragPtxTimeLine.ptxTimeLine.ptxChannel("Effect 1",2.0f,0.0f,effectColor);
			ptxTimeLine1.AddChannel(channel);

		}
		private void Menu_AddAction_Click(object sender, System.EventArgs e)
		{
			Color actionColor = Color.FromArgb(255,255,127,177);
			ragPtxTimeLine.ptxTimeLine.ptxChannel channel = new ragPtxTimeLine.ptxTimeLine.ptxChannel("Action 1",2.0f,0.0f,actionColor);
			ptxTimeLine1.AddChannel(channel);
		}
		private void OnChannelInfoChanged(ragPtxTimeLine.ptxTimeLine.ptxChannel channel)
		{
			if(channel != m_CurChannel)
				return;
			Label_CurChannel.Text = channel.m_Name;
			Text_CurStartTime.Text = channel.m_StartTime.ToString("0.00");
		}
		private void ptxTimeLine1_OnSelectChannel(ragPtxTimeLine.ptxTimeLine.ptxChannel channel)
		{
			m_CurChannel = channel;
			Label_CurChannel.Text = channel.m_Name;
			Text_CurStartTime.Text = channel.m_StartTime.ToString("0.00");
		}

		private void ptxTimeLine1_OnDeleteChannel(ragPtxTimeLine.ptxTimeLine.ptxChannel channel)
		{
			if(channel == m_CurChannel)
				m_CurChannel=null;
			Label_CurChannel.Text = "";
			Text_CurStartTime.Text = "";
		
		}

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new Form1());
		}

		private void button1_Click(object sender, System.EventArgs e)
		{
			TestListBox();
		}
		

	}
}
