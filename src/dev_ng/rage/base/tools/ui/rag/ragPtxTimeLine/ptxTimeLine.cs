using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;

namespace ragPtxTimeLine
{
	/// <summary>
	/// Summary description for UserControl1.
	/// </summary>
	public class ptxTimeLine : System.Windows.Forms.UserControl
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		private System.Windows.Forms.Label Label_FontA;
		private System.Windows.Forms.Label Label_FontB;

		public class ptxChannel
		{
			public ptxChannel(string name, float startRatio, float endRatio, Color color)
			{
				InitChannel(name, startRatio, endRatio, color);
			}

             private void InitChannel(string name,float startRatio,float endRatio,Color color)
            {
				m_Name=name;
//                m_DurationB = durationMin;
//				m_Duration=durationMax;
				m_StartRatio = startRatio;
				m_EndRatio = endRatio;
				m_Color = color;
				m_Panel = new Panel();
				m_LabelPanel = new Panel();
				m_LabelStartTime = new Label();
				m_Panel.Tag = this;
				m_LabelPanel.Tag = this;
				m_LabelStartTime.Tag =this;
				m_Index = 0;
				m_Selected=false;
				m_LabelMouseDown=false;
				m_Panel.SuspendLayout();
				m_Panel.Controls.Add(m_LabelStartTime);
				m_LabelStartTime.Left=1;
				m_LabelStartTime.Height=12;
				m_LabelStartTime.Visible=false;
				m_Panel.ResumeLayout();
			}

			
			#region Variables
            public Bitmap m_OffScreenBmp;
            public Graphics m_OffScreenDC; 

			public String m_Name;
//			public float  m_Duration;
//            public float m_DurationB = -1.0f;
			public float m_StartRatio;
			public float m_EndRatio;
			public Color  m_Color;
			public int	  m_Index;
			public bool   m_Selected;
			public bool   m_LabelMouseDown;
            public bool   m_Solo;
            public bool   m_Mute;
			public System.Windows.Forms.Panel m_LabelPanel;
			public System.Windows.Forms.Panel m_Panel;
			public System.Windows.Forms.Label m_LabelStartTime;  //For the font
            public RageUserControls.ButtonEX m_SoloButton;
            public RageUserControls.ButtonEX m_MuteButton;
			public event ChannelEventDelegate OnChannelInfoChanged=null;
			public Object m_Data=null;
			#endregion

			#region Properties
			public float StartRatio
			{
				get
				{
					return m_StartRatio;
				}
				set
				{
					if (value != m_StartRatio)
					{
						m_StartRatio = value;
						if(OnChannelInfoChanged != null)
							OnChannelInfoChanged(this);
						System.Drawing.Rectangle rect = new System.Drawing.Rectangle(2,2,30,13);
						m_Panel.Invalidate(rect);
                        m_Panel.Update();
                        m_Panel.Parent.Update();

					}
				}
			}
			public float EndRatio
			{
				get
				{
					return m_EndRatio;
				}
				set
				{
					if (value != m_EndRatio)
					{
						m_EndRatio = value;
						if (OnChannelInfoChanged != null)
							OnChannelInfoChanged(this);
						System.Drawing.Rectangle rect = new System.Drawing.Rectangle(2, 2, 30, 13);
						m_Panel.Invalidate();
						m_Panel.Update();
						m_Panel.Parent.Update();

					}
				}
			}
//			public float DurationB
//            {
//                get
//                { 
//                    return m_DurationB; 
//                }
//                set
//                {
//                    if (m_DurationB == value)
//                        return;
//                    m_DurationB = value;
//                   if (OnChannelInfoChanged != null)
//                        OnChannelInfoChanged(this);
//                    m_Panel.Invalidate();
//                    m_Panel.Parent.Update();
//                }
//            }
//            public float Duration
//			{
//				get
//				{
//					return m_Duration;
//				}
//				set
//				{
//					if(m_Duration==value)
//						return;
//					m_Duration=value;
//					if(OnChannelInfoChanged != null)
//						OnChannelInfoChanged(this);
//                   if (m_Panel != null)
//                   {
//                        m_Panel.Invalidate();
//                        if (m_Panel.Parent != null)
//                        {
//                            m_Panel.Parent.Update();
//                        }
//                    }
//				}
//			}

			#endregion
		}

		#region Variables
        Bitmap      m_OffScreenBmp;
        Graphics    m_OffScreenDC;

        bool        m_SuspendLayout = false;
        bool		m_ShiftKeyDown;
		bool		m_MovingLabel;
		Point       m_MovingChannelPos;
		Panel       m_InsertIndicator;
		Panel       m_InsertIndicatorShadow;
		float		m_Duration;
        float       m_MarkerPos = -1.0f;
		int			m_Granularity;
		int			m_ChannelHeight;
		int         m_RightPadding;
		int         m_ChannelLabelWidth;
		public List<ptxChannel>	m_Channels;
		bool		m_ScrubChannel;
		Point       m_MouseDownPos;
		Point       m_DeltaMouseMove;
		ptxChannel  m_MovingChannel;
		int			m_MovingChannelToIndex;
		public Point[] m_SelectionBox = new Point[2];
		public bool m_BoundSelecting=false;
		private System.Windows.Forms.Panel Panel_WorkSpaceClip;
		public System.Windows.Forms.ContextMenu Menu_Context;
		public System.Windows.Forms.MenuItem Menu_DeleteChannel;
        public System.Windows.Forms.MenuItem Menu_AddChannel;
		private System.Windows.Forms.Panel Panel_WorkSpace;

        public const int m_MaxTimelineEvents = 32;
		#endregion

		#region Properties

		public delegate void ChannelEventDelegate(ptxChannel channel);
		[Description("Occurs when user selects a channel"),Category("Action")]
		public event ChannelEventDelegate OnSelectChannel=null;
		
		[Description("Occurs after a channel is deleted from the timeline"),Category("Action")]
		public event ChannelEventDelegate OnDeleteChannel=null;
		
		[Description("Occurs when a channel's info has been changed"),Category("Action")]
		public event ChannelEventDelegate OnChannelInfoChanged=null;

		[Description("Occurs when channel's order has been changed"),Category("Action")]
		public event EventHandler OnReOrderChannels=null;

        public event EventHandler OnSoloMuteChanged = null;

		[Description("Max Duration in Seconds of the timeline"),Category("Data")]
		public float Duration  
		{	
			get 
			{ 
				return m_Duration;
			}
			set 
			{
				if(m_Duration!= value)
				{
					m_Duration=value;
					foreach ( ptxChannel c in m_Channels )
					{
                        UpdateChannel(c);
                        c.m_Panel.Invalidate();
                        c.m_LabelStartTime.Invalidate();
                        //c.m_Panel.Update();
                        //c.m_LabelStartTime.Update();
					}
					this.Invalidate();
                    this.Update();
					this.Panel_WorkSpace.Invalidate();
                    this.Panel_WorkSpace.Update();
                    
				}
			}
		}
        [Description("Mark a position on the timeline"), Category("Data")]
        public float MarkerPos
        {
            get
            {
                return m_MarkerPos;
            }
            set
            {
                if (m_MarkerPos != value)
                {
                    m_MarkerPos = value;
                    CreateGrid();
                    this.Panel_WorkSpace.Invalidate();
                    this.Panel_WorkSpace.Update();
                }
            }
        }
        [Description("Controls the resolution of timeline labels"), Category("Appearance")]
		public int Granularity  
		{	
			get 
			{ 
				return m_Granularity;
			}
			set 
			{
				m_Granularity=value;
                Invalidate(); Panel_WorkSpace.Invalidate();
			}
		}
		[Description("Controls the height of each channel"),Category("Appearance")]
		public int ChannelHeight
		{	
			get 
			{ 
				return m_ChannelHeight;
			}
			set 
			{
				m_ChannelHeight=value;
                Invalidate(); Panel_WorkSpace.Invalidate();
			}
		}
		[Description("Channel Label Width"),Category("Appearance")]
        public int ChannelLabelWidth
		{	
			get 
			{ 
				return m_ChannelLabelWidth;
			}
			set 
			{
                m_ChannelLabelWidth = value; Invalidate();Panel_WorkSpace.Invalidate();
			}
		}

		[Description("Amount of padding to right side of timeline"),Category("Appearance")]
		public int Right_Padding 
		{	
			get 
			{ 
				return m_RightPadding;
			}
			set 
			{
                m_RightPadding = value; Invalidate(); Panel_WorkSpace.Invalidate();
			}
		}
		#endregion

		public ptxTimeLine()
		{
            // This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			// TODO: Add any initialization after the InitComponent call
			m_Duration=4.0f;
			m_Granularity = 4;
			m_ChannelHeight =20;
            m_Channels = new List<ptxChannel>();
			m_ScrubChannel=false;
			m_MouseDownPos = new Point(0,0);
			m_DeltaMouseMove = new Point(0,0);
			m_RightPadding=40;
			m_ChannelLabelWidth=100;
			m_ShiftKeyDown=false;
			m_MovingLabel = false;
			m_InsertIndicator = new Panel();
			m_InsertIndicatorShadow = new Panel();
			Panel_WorkSpace.SuspendLayout();
			Panel_WorkSpace.ResumeLayout();
			m_MovingChannelPos = new Point(0,0);

			this.SuspendLayout();
			this.Controls.Add(m_InsertIndicatorShadow);
			m_InsertIndicatorShadow.Visible=false;
			m_InsertIndicatorShadow.BackColor = Color.Black;
			m_InsertIndicatorShadow.Height=m_ChannelHeight;
			m_InsertIndicatorShadow.Left=Panel_WorkSpaceClip.Left-8;
			m_InsertIndicatorShadow.Width=Panel_WorkSpaceClip.Left + m_ChannelLabelWidth-m_InsertIndicator.Left+1;
			//m_InsertIndicatorShadow.Paint += new System.Windows.Forms.PaintEventHandler(this.ChannelLabel_Paint);
			m_InsertIndicatorShadow.BringToFront();
			this.Controls.Add(m_InsertIndicator);
			m_InsertIndicator.Visible=false;
			m_InsertIndicator.BackColor = Color.Black;
			m_InsertIndicator.Height=m_ChannelHeight;
			m_InsertIndicator.Left=Panel_WorkSpaceClip.Left-8;
			m_InsertIndicator.Width=Panel_WorkSpaceClip.Left + m_ChannelLabelWidth-m_InsertIndicator.Left+1;
			m_InsertIndicator.Paint += new System.Windows.Forms.PaintEventHandler(this.ChannelLabel_Paint);
			m_InsertIndicator.BringToFront();

            Panel_WorkSpace.Height = m_MaxTimelineEvents * m_ChannelHeight;

			this.ResumeLayout();

			SetStyle(ControlStyles.DoubleBuffer | 
				ControlStyles.UserPaint | 
				ControlStyles.AllPaintingInWmPaint,
				true);
			SetStyle(ControlStyles.ContainerControl, true);
			SetStyle(ControlStyles.SupportsTransparentBackColor,true);
			this.UpdateStyles();
			this.OnChannelInfoChanged +=new ChannelEventDelegate(this.ptxTimeLine_OnChannelInfoChanged);

            //int alpha = 255;
            //Color emitColor = Color.FromArgb(alpha,127,177,255);
            //Color actionColor = Color.FromArgb(alpha,255,127,177);
            //Color effectColor = Color.FromArgb(alpha,127,255,177);

            //ptxChannel channel = new ptxChannel("Emitter 1",2.0f,1.0f,emitColor);
            //AddChannel(channel);
            //channel = new ptxChannel("Emitter 2",2.0f,0.0f,emitColor);
            //AddChannel(channel);
            //channel = new ptxChannel("Action 1",1.0f,0.0f,actionColor);
            //AddChannel(channel);
            //channel = new ptxChannel("Effect 1",1.0f,0.0f,effectColor);
            //AddChannel(channel);
            //channel = new ptxChannel("Emitter 3",1.0f,0.0f,emitColor);
            //AddChannel(channel);
            //channel = new ptxChannel("Emitter 3",1.0f,0.0f,emitColor);
            //AddChannel(channel);
            //channel = new ptxChannel("Emitter 3",1.0f,0.0f,emitColor);
            //AddChannel(channel);
            //channel = new ptxChannel("Emitter 3",1.0f,0.0f,emitColor);
            //AddChannel(channel);
            //channel = new ptxChannel("Long Emitter name",1.0f,0.0f,emitColor);
            //AddChannel(channel);
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if( components != null )
					components.Dispose();
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.Panel_WorkSpaceClip = new System.Windows.Forms.Panel();
            this.Panel_WorkSpace = new System.Windows.Forms.Panel();
            this.Label_FontA = new System.Windows.Forms.Label();
            this.Label_FontB = new System.Windows.Forms.Label();
            this.Menu_Context = new System.Windows.Forms.ContextMenu();
            this.Menu_DeleteChannel = new System.Windows.Forms.MenuItem();
            this.Menu_AddChannel = new System.Windows.Forms.MenuItem();
            this.Panel_WorkSpaceClip.SuspendLayout();
            this.SuspendLayout();
            // 
            // Panel_WorkSpaceClip
            // 
            this.Panel_WorkSpaceClip.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Panel_WorkSpaceClip.AutoScroll = true;
            this.Panel_WorkSpaceClip.AutoScrollMinSize = new System.Drawing.Size(808, 500);
            this.Panel_WorkSpaceClip.BackColor = System.Drawing.Color.White;
            this.Panel_WorkSpaceClip.Controls.Add(this.Panel_WorkSpace);
            this.Panel_WorkSpaceClip.Location = new System.Drawing.Point(16, 24);
            this.Panel_WorkSpaceClip.Name = "Panel_WorkSpaceClip";
            this.Panel_WorkSpaceClip.Size = new System.Drawing.Size(808, 169);
            this.Panel_WorkSpaceClip.TabIndex = 0;
            this.Panel_WorkSpaceClip.SizeChanged += new System.EventHandler(this.Panel_WorkSpaceClip_SizeChanged);
            this.Panel_WorkSpaceClip.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Panel_WorkSpaceClip_MouseDown);
            // 
            // Panel_WorkSpace
            // 
            this.Panel_WorkSpace.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Panel_WorkSpace.Location = new System.Drawing.Point(0, 0);
            this.Panel_WorkSpace.Name = "Panel_WorkSpace";
            this.Panel_WorkSpace.Size = new System.Drawing.Size(768, 1164);
            this.Panel_WorkSpace.TabIndex = 0;
            this.Panel_WorkSpace.Paint += new System.Windows.Forms.PaintEventHandler(this.Panel_WorkSpace_Paint);
            this.Panel_WorkSpace.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Panel_WorkSpace_MouseDown);
            this.Panel_WorkSpace.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Panel_WorkSpace_MouseMove);
            this.Panel_WorkSpace.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Panel_WorkSpace_MouseUp);
            this.Panel_WorkSpace.Move += new System.EventHandler(this.Panel_WorkSpace_Move);
            // 
            // Label_FontA
            // 
            this.Label_FontA.Location = new System.Drawing.Point(544, 8);
            this.Label_FontA.Name = "Label_FontA";
            this.Label_FontA.Size = new System.Drawing.Size(100, 23);
            this.Label_FontA.TabIndex = 1;
            this.Label_FontA.Text = "TimelineFontA";
            this.Label_FontA.Visible = false;
            // 
            // Label_FontB
            // 
            this.Label_FontB.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label_FontB.Location = new System.Drawing.Point(432, 8);
            this.Label_FontB.Name = "Label_FontB";
            this.Label_FontB.Size = new System.Drawing.Size(100, 23);
            this.Label_FontB.TabIndex = 2;
            this.Label_FontB.Text = "TimelineFontB";
            this.Label_FontB.Visible = false;
            // 
            // Menu_Context
            // 
            this.Menu_Context.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.Menu_DeleteChannel,
            this.Menu_AddChannel});
            this.Menu_Context.Popup += new System.EventHandler(this.Menu_Context_Popup);
            // 
            // Menu_DeleteChannel
            // 
            this.Menu_DeleteChannel.Index = 0;
            this.Menu_DeleteChannel.Text = "Delete Channel(s)";
            this.Menu_DeleteChannel.Click += new System.EventHandler(this.Menu_DeleteChannel_Click);
            // 
            // Menu_AddChannel
            // 
            this.Menu_AddChannel.Index = 1;
            this.Menu_AddChannel.Text = "Add Channel";
            this.Menu_AddChannel.Visible = false;
            // 
            // ptxTimeLine
            // 
            this.BackColor = System.Drawing.SystemColors.ControlLight;
            this.Controls.Add(this.Label_FontB);
            this.Controls.Add(this.Label_FontA);
            this.Controls.Add(this.Panel_WorkSpaceClip);
            this.Name = "ptxTimeLine";
            this.Size = new System.Drawing.Size(840, 208);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.ptxTimeLine_Paint);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ptxTimeLine_KeyDown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ptxTimeLine_KeyUp);
            this.Leave += new System.EventHandler(this.ptxTimeLine_Leave);
            this.Panel_WorkSpaceClip.ResumeLayout(false);
            this.ResumeLayout(false);

		}
		#endregion

        public void Suspend()
        {
            m_SuspendLayout = true;
        }

        public void Resume()
        {
            m_SuspendLayout = false;
            CreateGrid();
            Panel_WorkSpace.Invalidate();
        }

        public void SelectChannel(int idx)
        {
            DeSelectChannels();
            foreach(ptxChannel c in m_Channels)
            {
                if (c.m_Index == idx)
                {
                    c.m_Selected = true;
                    UpdateChannel(c);
                    return;
                }
            }
        }
        public void SelectChannel(ptxChannel c)
        {
            DeSelectChannels();
            c.m_Selected = true;
            UpdateChannel(c);
        }
        public ptxChannel GetSelectedChannel()
        {
            foreach (ptxChannel c in m_Channels)
                if (c.m_Selected)
                    return c;
            return null;
        }
        public int GetSelectedChannelIndex()
        {
            ptxChannel c = GetSelectedChannel();
            if (c == null)
                return -1;
            return c.m_Index;
        }

		public void Clear()
		{
			for(int i=m_Channels.Count-1;i>=0;i--)
			{
				ptxChannel c = m_Channels[i];
				DelChannel(c,false);
			}
		}
		public void DelChannel(ptxChannel c)
		{
			DelChannel(c,true);
		}
		public void DelChannel(ptxChannel c,bool triggerevent)
		{
			m_Channels.Remove(c);
			Panel_WorkSpace.SuspendLayout();
			Panel_WorkSpace.Controls.Remove(c.m_Panel);
			Panel_WorkSpace.Controls.Remove(c.m_LabelPanel);
			Panel_WorkSpace.ResumeLayout();
			Panel_WorkSpace.Height = m_Channels.Count * m_ChannelHeight+1;
			Panel_WorkSpace.Height = Math.Max(Panel_WorkSpace.Height,m_ChannelHeight+1);

			if(triggerevent)
				if(OnDeleteChannel!=null)
					OnDeleteChannel(c);
            CreateGrid();
		}
		public void AddChannel(ptxChannel c)
		{
			c.m_Index = m_Channels.Count;
			m_Channels.Add(c);

			Panel_WorkSpace.SuspendLayout();
			Panel_WorkSpace.Controls.Add(c.m_Panel);
			Panel_WorkSpace.Controls.Add(c.m_LabelPanel);
			c.m_Panel.Paint += new System.Windows.Forms.PaintEventHandler(this.Channel_Paint);
			c.m_Panel.MouseDown +=new System.Windows.Forms.MouseEventHandler(this.Channel_MouseDown);
			c.m_Panel.MouseUp +=new System.Windows.Forms.MouseEventHandler(this.Panel_WorkSpace_MouseUp);
			c.m_Panel.MouseMove +=new System.Windows.Forms.MouseEventHandler(this.Panel_WorkSpace_MouseMove);
			c.m_LabelPanel.Paint += new System.Windows.Forms.PaintEventHandler(this.ChannelLabel_Paint);
			c.m_LabelPanel.MouseDown +=new System.Windows.Forms.MouseEventHandler(this.ChannelLabel_MouseDown);
			c.m_LabelPanel.MouseUp +=new System.Windows.Forms.MouseEventHandler(this.ChannelLabel_MouseUp);
			c.m_LabelPanel.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Panel_WorkSpace_MouseMove_AdjustLabel);


            int x = 1;
            int y = 1;
            c.m_SoloButton = new RageUserControls.ButtonEX();
            c.m_LabelPanel.Controls.Add(c.m_SoloButton);
            c.m_SoloButton.ButtonText = "S";
            c.m_SoloButton.ColorDown = Color.FromKnownColor(KnownColor.Yellow);
            c.m_SoloButton.SetBounds(x, y, 19, 17);
            c.m_SoloButton.OnButtonStateChangedByUser += delegate
            {
                c.m_Solo = !c.m_SoloButton.ButtonState;
                UpdateSoloMute();
            };

            c.m_MuteButton = new RageUserControls.ButtonEX();
            c.m_LabelPanel.Controls.Add(c.m_MuteButton);
            c.m_MuteButton.ButtonText = "M";
            c.m_MuteButton.ColorDown = Color.FromKnownColor(KnownColor.OrangeRed);
            c.m_MuteButton.SetBounds(x + c.m_SoloButton.Width, y, 19, 17);
            c.m_MuteButton.OnButtonStateChangedByUser += delegate
            {
                c.m_Mute = !c.m_MuteButton.ButtonState;
                UpdateSoloMute();
            };

			Panel_WorkSpace.ResumeLayout();
			c.OnChannelInfoChanged+=OnChannelInfoChanged;
			UpdateChannel(c);
			Panel_WorkSpace.Height = m_Channels.Count * m_ChannelHeight+1;
            CreateGrid();
		}

        public void UpdateSoloMute()
        {
            if (OnSoloMuteChanged != null)
            {
                OnSoloMuteChanged(this, EventArgs.Empty);
            }
        }

		private void DeleteSelectedChannels()
		{
			for(int i=m_Channels.Count-1;i>=0;i--)
			{
                ptxChannel c = m_Channels[i];
				if(c.m_Selected)
				{
					DelChannel(c);
				}
			}
			ReIndexChannels();
		}
		private void ReOrderChannels()
		{
			if(m_MovingChannelToIndex == m_MovingChannel.m_Index)
				return;

			m_Channels.Remove(m_MovingChannel);
			if(m_MovingChannelToIndex>=0)
			{
				if(m_MovingChannelToIndex>m_MovingChannel.m_Index)
					m_Channels.Insert(m_MovingChannelToIndex-1,m_MovingChannel);
				else
					m_Channels.Insert(m_MovingChannelToIndex,m_MovingChannel);
			}
			else
				m_Channels.Add(m_MovingChannel);

			ReIndexChannels();
			if(OnReOrderChannels != null)
				OnReOrderChannels(this, new System.EventArgs());
		}
		private int CountNumSelected()
		{
			int sel=0;
			foreach ( ptxChannel c in m_Channels )
			{
				if(c.m_Selected)
					sel ++;
			}
			return sel;
		}
		private void ReIndexChannels()
		{
			for(int i=0;i<m_Channels.Count;i++)
			{
                ptxChannel c = m_Channels[i];
				c.m_Index =i;
				UpdateChannel(c);
			}
			Panel_WorkSpace.Height = m_Channels.Count * m_ChannelHeight+1;
			Panel_WorkSpace.Height = Math.Max(Panel_WorkSpace.Height,m_ChannelHeight+1);
		}
		private void UpdateChannel(ptxChannel c)
		{
			//See if we need to clamp start time
			float startRatio = c.m_StartRatio;
			if (startRatio < 0.0f)
				startRatio = 0.0f;
			else
				if (startRatio > 1.0f)
					startRatio = 1.0f;
			c.StartRatio = startRatio;

			//Set its width and height
			int start = (int)((c.m_StartRatio) * (float)(Panel_WorkSpace.Width - m_RightPadding - m_ChannelLabelWidth));
			int end = (int)((c.m_EndRatio) * (float)(Panel_WorkSpace.Width - m_RightPadding - m_ChannelLabelWidth));
			int width = end - start;
			
			if(width<10)
			{
				width = Math.Max(10,width);
				c.m_Panel.BackColor = Color.FromArgb(255,255,100,0);
			}
			else
				c.m_Panel.BackColor = c.m_Color;

			c.m_Panel.Height = m_ChannelHeight-1;
			c.m_Panel.Width=width;
			c.m_Panel.Left=start+m_ChannelLabelWidth;
			c.m_Panel.Top=((c.m_Index) * (m_ChannelHeight))+1;

            c.m_LabelPanel.Left=0;
			c.m_LabelPanel.Top=c.m_Panel.Top;

			int red = (int)((float)c.m_Color.R*0.75f);
			int green = (int)((float)c.m_Color.G*0.75f);
			int blue = (int)((float)c.m_Color.B*0.75f);
			c.m_LabelPanel.BackColor = Color.FromArgb(c.m_Color.A,red,green,blue);
			c.m_LabelPanel.Width = m_ChannelLabelWidth;
			c.m_LabelPanel.Left=0;
			c.m_LabelPanel.Top=c.m_Panel.Top;
			c.m_LabelPanel.Height = m_ChannelHeight-1;
			c.m_LabelStartTime.Left=0;
			
			c.m_LabelStartTime.Top=3;
			c.m_LabelStartTime.Text = "0.234";// c.m_StartRatio.ToString("0.00");

            //Work on this later (faster drawing methods)
            //c.m_OffScreenBmp = new Bitmap(c.m_Panel.Width, c.m_Panel.Height);
            //c.m_OffScreenDC = Graphics.FromImage(c.m_OffScreenBmp);
            //System.Drawing.SolidBrush cbrush = new System.Drawing.SolidBrush(Color.FromArgb(255, 255, 255));
            //c.m_OffScreenDC.FillRectangle(cbrush, 0, 0, c.m_OffScreenBmp.Width, c.m_OffScreenBmp.Height);
            //Graphics.FromHwnd(c.m_Panel.Handle).DrawImage(c.m_OffScreenBmp, 0, 0);
        }

		private int  DetermineLabelInsertSpot()
		{
			int midpoint = m_MovingChannelPos.Y;//(m_CurLabel.Top+m_CurLabel.Bottom)/2;

            for ( int i = 0; i < m_Channels.Count; ++i )
            {
                ptxChannel c = m_Channels[i];
                int testpoint = (c.m_Panel.Top + c.m_Panel.Bottom) / 2;
                if ( midpoint < testpoint )
                    return i;

            }
			return -1;
		}

		private void SelectBoundChannels()
		{
			int left = Math.Min(m_SelectionBox[0].X,m_SelectionBox[1].X);
			int top =  Math.Min(m_SelectionBox[0].Y,m_SelectionBox[1].Y);
			int right = Math.Max(m_SelectionBox[0].X,m_SelectionBox[1].X);
			int bottom = Math.Max(m_SelectionBox[0].Y,m_SelectionBox[1].Y);
			
			foreach ( ptxChannel c in m_Channels )
			{
				int x1 = c.m_Panel.Left;
				int y1 = c.m_Panel.Top;
				int x2 = c.m_Panel.Left;
				int y2 = c.m_Panel.Bottom;
				int x3 = c.m_Panel.Right;
				int y3 = c.m_Panel.Top;
				int x4 = c.m_Panel.Right;
				int y4 = c.m_Panel.Bottom;
				if( (x1<=right)&&(x1>=left)&&(y1<=bottom)&&(y1>=top) ||
					(x2<=right)&&(x2>=left)&&(y2<=bottom)&&(y2>=top) ||
					(x3<=right)&&(x3>=left)&&(y3<=bottom)&&(y3>=top) ||
					(x4<=right)&&(x4>=left)&&(y4<=bottom)&&(y4>=top) 
					)
				{
					c.m_Selected=true;
				}
				else
					c.m_Selected=false;
			}
            

		}
		private void DeSelectChannels()
		{
			foreach ( ptxChannel c in m_Channels )
			{
				if(c.m_Selected)
					c.m_Panel.Invalidate();
				c.m_Selected=false;
			}
		}

        private void SelectChannel(Point pt)
        {
            DeSelectChannels();
            m_SelectionBox[0].X = m_SelectionBox[1].X = pt.X;
            m_SelectionBox[0].Y = m_SelectionBox[1].Y = pt.Y;
            SelectBoundChannels();

            ptxChannel selected = null;
            foreach (ptxChannel c in m_Channels)
            {
                if (c.m_Selected)
                {
                    selected = c;
                    break;
                }
            }

            if (OnSelectChannel != null)
                OnSelectChannel(selected);
        }

		private void MoveSelectedChannels()
		{
			if(m_DeltaMouseMove.X==0) return;
			float timeperpixel = m_Duration/((float)(Panel_WorkSpace.Width-m_RightPadding-m_ChannelLabelWidth));
			float dtime = ((float)m_DeltaMouseMove.X)*timeperpixel;
			foreach ( ptxChannel c in m_Channels )
			{
				if(c.m_Selected)
				{
					float startRatio = c.m_StartRatio + dtime;
					float endRatio = c.m_EndRatio + dtime;
					if (startRatio < 0.0f)
						startRatio = 0.0f;
					if (startRatio > 1.0f)
						startRatio = 1.0f;
					if (endRatio < 0.0f)
						endRatio = 0.0f;
					if (endRatio > 1.0f)
						endRatio = 1.0f;
					c.m_StartRatio = startRatio;
					c.m_EndRatio = endRatio;
					UpdateChannel(c);
					System.Drawing.Rectangle rect = new System.Drawing.Rectangle(2,2,30,13);
					c.m_Panel.Invalidate(rect);
				}
			}
		}
		private void DrawMidLabel(Graphics g, float min, float max, int left, int right, int top, int res)
		{
			float midVal = (min+max)*0.5f;
			int midPos = (left+right)/2;

			if(res>0)
			{
				DrawMidLabel(g,min,midVal,left,midPos,top,res-1);
				DrawMidLabel(g,midVal,max,midPos,right,top,res-1);
				
				//Draw middle value
				Size labelSize;
				labelSize = g.MeasureString(midVal.ToString("0.000"),Label_FontA.Font).ToSize();
				Point pos = new Point();
				pos.X = midPos-(labelSize.Width/2);
				pos.Y = top;
				g.DrawString(midVal.ToString("0.000"), Label_FontA.Font, new SolidBrush(Color.Black),pos);
			}
		}

		private void DrawLabels(Graphics g)
		{
			float min = 0.0f;
			float max = m_Duration;
			int left = Panel_WorkSpaceClip.Left+Panel_WorkSpace.Left+m_ChannelLabelWidth;
            int right = left + (Panel_WorkSpace.Width - m_RightPadding - m_ChannelLabelWidth);
			int top = 4;

			DrawMidLabel(g,min,max,left,right,top,m_Granularity);

			//Draw min label
			Size labelSize;
			labelSize = g.MeasureString(min.ToString("0.000"),Label_FontA.Font).ToSize();
			Point pos = new Point();
			pos.X = left-(labelSize.Width/2);
			pos.Y = top;
			g.DrawString(min.ToString("0.000"), Label_FontA.Font, new SolidBrush(Color.Black),pos);
							
			//Draw max label
			labelSize = g.MeasureString(max.ToString("0.000"),Label_FontA.Font).ToSize();
			pos.X = right-(labelSize.Width/2);
			pos.Y = top;
			g.DrawString(max.ToString("0.000"), Label_FontA.Font, new SolidBrush(Color.Black),pos);

		}
		private void DrawMidGrid(Graphics g,int left, int right, int top, int res)
		{
			int midPos = (left+right)/2;

			if(res>0)
			{
				DrawMidGrid(g,left,midPos,top,res-1);
				DrawMidGrid(g,midPos,right,top,res-1);
			
				Point A = new Point(midPos,top);
				Point B = new Point(midPos,Panel_WorkSpace.Height);
				Pen pen = new Pen(Color.FromArgb(32,0,0,0),1);
				if(res>=m_Granularity)
					pen = new Pen(Color.FromArgb(128,0,0,0),1);
				//if(res==1)
				//	pen = new Pen(Color.FromArgb(32,0,0,0),1);


				g.DrawLine(pen,A,B);
			}
		}
		private void CreateGrid()
        {
            if (m_SuspendLayout) return;

            //Adjust the scroll size of the Panel_WorkspaceClip based on number of channels
            int channelcnt = Math.Max(m_Channels.Count + 1, 2);
            Size maxAutoScrollSize = Panel_WorkSpaceClip.AutoScrollMinSize;
            maxAutoScrollSize.Height = m_ChannelHeight * channelcnt;
            Panel_WorkSpaceClip.AutoScrollMinSize = maxAutoScrollSize;

            m_OffScreenBmp = new Bitmap(Panel_WorkSpace.Width, Panel_WorkSpace.Height);
            m_OffScreenDC = Graphics.FromImage(m_OffScreenBmp);
            System.Drawing.SolidBrush cbrush = new System.Drawing.SolidBrush(Color.FromArgb(255, 255, 255));
            m_OffScreenDC.FillRectangle(cbrush, 0, 0, m_OffScreenBmp.Width, m_OffScreenBmp.Height);
            Graphics g = m_OffScreenDC;

			//Draw to the workspace panel
			int left = m_ChannelLabelWidth;
			int right = Panel_WorkSpace.Width-m_RightPadding;
			int top = 0;
			DrawMidGrid(g,left,right,top,m_Granularity+3);

			//Draw Min Grid line
			Point A = new Point(left,top);
			Point B = new Point(left,Panel_WorkSpace.Height);
			Pen pen = new Pen(Color.FromArgb(128,0,0,0),1);
			g.DrawLine(pen,A,B);

			//Draw Max Grid line
			A = new Point(right,top);
			B = new Point(right,Panel_WorkSpace.Height);
			pen = new Pen(Color.FromArgb(128,0,0,0),1);
			g.DrawLine(pen,A,B);
			
			//Draw The channel Separators (should be based on number of channels
            int y = 0;
			for(int i=0;i<channelcnt;i++)
			{
				left =0;
				right = Panel_WorkSpace.Width;
				A = new Point(left,y);
				B = new Point(right,y);
				pen = new Pen(Color.FromArgb(255,0,0,0),1);
				g.DrawLine(pen,A,B);
				y+=m_ChannelHeight;
			}

            //Draw the marker
            if(m_MarkerPos>=0.0f)
            {
                Pen markerPen = new Pen(Color.Red, 1);
                markerPen.DashStyle = System.Drawing.Drawing2D.DashStyle.Dash;
                int pos = (int)((m_MarkerPos / m_Duration) * (Panel_WorkSpace.Width - m_RightPadding-m_ChannelLabelWidth)) + m_ChannelLabelWidth;
                g.DrawLine(markerPen, pos, 0, pos, Panel_WorkSpace.Height);
            }
		}

		private void ptxTimeLine_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			DrawLabels(e.Graphics);
		}

		private void Channel_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			Panel pan = sender as Panel;
			ptxChannel c = pan.Tag as ptxChannel;

            //Draw a secondary zone
//            if (c.m_DurationB < c.m_Duration)
//            {
//                int width2 = (int)((c.m_DurationB / m_Duration) * (float)(Panel_WorkSpace.Width - m_RightPadding - m_ChannelLabelWidth));
//                //lowering alpha causes flicker
//                int red = (c.m_Color.R + 255) / 2;
//                int green = (c.m_Color.G + 255) / 2;
//                int blue = (c.m_Color.B + 255) / 2;
//
//                Pen pen = new Pen(Color.Black);
//                SolidBrush brush = new SolidBrush(Color.FromArgb(255, red, green, blue));
//                //Graphics g = c.m_Panel.CreateGraphics();
//                e.Graphics.DrawLine(pen, width2, 0, width2, c.m_Panel.Height);
//                e.Graphics.FillRectangle(brush, width2 + 1, 0, pan.Width - width2 + 1, pan.Height);
//                //c.m_Panel.BackColor = c.m_Color;
//            }

            if (c.m_Selected)
            {
                Pen pen = new Pen(Color.FromArgb(255, 0, 0, 0), 1);
                e.Graphics.DrawRectangle(pen, 0, 0, pan.Width - 1, pan.Height - 1);
            }

			c.m_LabelStartTime.Text = "0.234";// c.m_StartRatio.ToString("0.00");
			string text = c.m_StartRatio.ToString("0.00");
            SizeF size = e.Graphics.MeasureString(text, c.m_LabelStartTime.Font);
            int top = (m_ChannelHeight / 2) - (int)(size.Height * 0.5f);
            e.Graphics.DrawString(text, c.m_LabelStartTime.Font, new SolidBrush(Color.Black), new Point(0, top));
		}
	
		private void Channel_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			Panel pan = sender as Panel;
			ptxChannel c = pan.Tag as ptxChannel;

			
			if(!c.m_Selected && !m_ShiftKeyDown)
				DeSelectChannels();
			if(c.m_Selected && m_ShiftKeyDown)
				c.m_Selected=false;
			else
			{
				c.m_Selected=true;
				if(OnSelectChannel!=null)
					OnSelectChannel(c);
			}
            
            if (e.Button == MouseButtons.Right)
            {
                Menu_Context.Show(c.m_Panel, c.m_Panel.PointToClient(Cursor.Position));
                return;
            }
            
            m_ScrubChannel = true;
			
			m_MouseDownPos = Cursor.Position;
			m_DeltaMouseMove = new Point(0,0);
			pan.Invalidate();
		}
	
		private void ChannelLabel_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			Panel pan = sender as Panel;
			ptxChannel c = pan.Tag as ptxChannel;
			
			SizeF size = e.Graphics.MeasureString(c.m_Name,c.m_LabelStartTime.Font);
			int top = (m_ChannelHeight/2)-(int)(size.Height*0.5f);
			e.Graphics.DrawString(c.m_Name,c.m_LabelStartTime.Font,new SolidBrush(Color.Black),new Point(40,top));
			
			if(c.m_LabelMouseDown)
			{
				Pen pen = new Pen(Color.FromArgb(255,0,0,0),2);
				e.Graphics.DrawRectangle(pen,0,0,pan.Width,pan.Height);
			}

		}
		private void ChannelLabel_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			if(e.Button == MouseButtons.Right)
				return;

			Panel pan = sender as Panel;
			ptxChannel c = pan.Tag as ptxChannel;
			c.m_LabelMouseDown=true;
			pan.Invalidate();
			DeSelectChannels();
			m_MovingLabel=true;
			m_MovingChannel = c;
			m_MovingChannelToIndex = c.m_Index;
			m_InsertIndicator.Tag = c;
			int alpha = c.m_LabelPanel.BackColor.A;
			int red = (int)Math.Min(255.0f,(float)c.m_LabelPanel.BackColor.R*1.5f);
			int green = (int)Math.Min(255.0f,(float)c.m_LabelPanel.BackColor.G*1.5f);
			int blue = (int)Math.Min(255.0f,(float)c.m_LabelPanel.BackColor.B*1.5f);

			m_InsertIndicator.BackColor = Color.FromArgb(alpha,red,green,blue);
			m_InsertIndicator.Width = c.m_LabelPanel.Width;
			m_InsertIndicator.Height= c.m_LabelPanel.Height;
			m_InsertIndicator.Left=c.m_LabelPanel.Left+Panel_WorkSpaceClip.Left+Panel_WorkSpace.Left;
			m_MovingChannelPos.Y = c.m_LabelPanel.Top;
		}

		private void ChannelLabel_MouseUp(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			Panel pan = sender as Panel;
			ptxChannel c = pan.Tag as ptxChannel;
			c.m_LabelMouseDown=false;
			pan.Invalidate();
			if(m_MovingLabel)
				ReOrderChannels();
			m_MovingLabel=false;
			m_InsertIndicator.Visible=false;
			m_InsertIndicatorShadow.Visible=false;
		}

		private void Panel_WorkSpace_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
		{
            
            if (e.Button == MouseButtons.Right)
			{
                SelectChannel(Panel_WorkSpace.PointToClient(Cursor.Position));
                Menu_Context.Show(this, this.PointToClient(Cursor.Position));
				return;
			}
			
			//Selecting
			if(e.Button == MouseButtons.Left)
			{
                DeSelectChannels();
                Point MousePos = Panel_WorkSpace.PointToClient(Cursor.Position);
                m_BoundSelecting = false;  //Disable bound selection
                m_SelectionBox[0].X = MousePos.X;
                m_SelectionBox[0].Y = MousePos.Y;
                m_SelectionBox[1].X = MousePos.X;
                m_SelectionBox[1].Y = MousePos.Y;
                // do anything specific here?
			}
		}
		private void Panel_WorkSpace_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
            if (m_SuspendLayout)
                return;
            //First time around we create the offscreen bmp
            if (m_OffScreenBmp == null)
                CreateGrid();

            int labelWidth = 100;
            Graphics.FromHwnd(Panel_WorkSpace.Handle).DrawImage(m_OffScreenBmp, 0, 0);

            foreach (ptxChannel c in m_Channels)
            {
                Size labelSize;
                labelSize = Graphics.FromHwnd(Panel_WorkSpace.Handle).MeasureString(c.m_Name, c.m_LabelPanel.Font).ToSize();

                int cwidth = labelSize.Width+50;
                labelWidth = Math.Max(labelWidth, cwidth);
            }

            m_ChannelLabelWidth = labelWidth;

            foreach (ptxChannel c in m_Channels)
            {
                UpdateChannel(c);
            }

            if (m_BoundSelecting)
            {
                int left = Math.Min(m_SelectionBox[0].X, m_SelectionBox[1].X);
                int top = Math.Min(m_SelectionBox[0].Y, m_SelectionBox[1].Y);
                int width = Math.Abs(m_SelectionBox[0].X - m_SelectionBox[1].X);
                int height = Math.Abs(m_SelectionBox[0].Y - m_SelectionBox[1].Y);

                Pen pen = new Pen(Color.Black, 1);
                pen.DashStyle = System.Drawing.Drawing2D.DashStyle.Dash;
                e.Graphics.DrawRectangle(pen, left, top, width, height);
            }
		}

		private void Panel_WorkSpace_MouseUp(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			m_ScrubChannel=false;
			//DeSelectChannels();

			if(m_BoundSelecting)
			{
				SelectBoundChannels();
				Panel_WorkSpace.Invalidate();
				
			}
			m_BoundSelecting = false;
			if(m_MovingLabel)
				ReOrderChannels();
			m_MovingLabel=false;
		}

		private void Panel_WorkSpace_MouseMove(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			if(m_ScrubChannel)
			{
				Point point=Cursor.Position;
				int dx = point.X - m_MouseDownPos.X;
				int dy = point.Y - m_MouseDownPos.Y;
				m_DeltaMouseMove.X+=dx;
				m_DeltaMouseMove.Y+=dy;
				m_MouseDownPos=Cursor.Position;
//				MoveSelectedChannels();
				m_DeltaMouseMove = new Point(0,0);
			}

			//Selection box
			if(m_BoundSelecting)
			{
				Point MousePos = Panel_WorkSpace.PointToClient(Cursor.Position);
				m_SelectionBox[1].X = MousePos.X;
				m_SelectionBox[1].Y = MousePos.Y;
				
				//Ensure we are centered around the dragging mouse
				//KeepPointOnScreen(MousePosition.X,MousePosition.X);
			}
		}
		private void Panel_WorkSpace_MouseMove_AdjustLabel(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			//if(m_MovingLabel && m_CurLabel != null)
			if(m_MovingLabel)
			{
				m_InsertIndicator.Visible=true;
				m_InsertIndicatorShadow.Visible=true;
				Point pos = Panel_WorkSpace.PointToClient(Cursor.Position);
				if( (pos.Y+m_ChannelHeight) > (Panel_WorkSpace.Bottom+m_ChannelHeight))
					pos.Y = Panel_WorkSpace.Bottom;
				else
					if( (pos.Y) < (Panel_WorkSpace.Top-m_ChannelHeight))
					pos.Y = Panel_WorkSpace.Top-m_ChannelHeight;

				
				m_MovingChannelPos.Y = pos.Y;

				//Determine where we insert and draw a line
				int indx = DetermineLabelInsertSpot();
				m_MovingChannelToIndex = indx;
				if(indx>=0)
				{
					ptxChannel c = m_Channels[indx];
					m_InsertIndicator.Top = (c.m_Panel.Top + Panel_WorkSpaceClip.Top + Panel_WorkSpace.Top)-(m_InsertIndicator.Height/2);
				}
				else
				{
                    ptxChannel c = m_Channels[m_Channels.Count - 1];
					m_InsertIndicator.Top = (c.m_Panel.Bottom + Panel_WorkSpaceClip.Top + Panel_WorkSpace.Top)-(m_InsertIndicator.Height/2);
				}
				m_InsertIndicatorShadow.Left = m_InsertIndicator.Left+4;
				m_InsertIndicatorShadow.Top = m_InsertIndicator.Top+4;
				m_InsertIndicatorShadow.Width= m_InsertIndicator.Width;
				m_InsertIndicatorShadow.Height = m_InsertIndicator.Height;

			}
			else
			{
				m_InsertIndicator.Visible=false;
				m_InsertIndicatorShadow.Visible=false;
			}

		}

		private void ptxTimeLine_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
		{
			m_ShiftKeyDown = e.Shift;
		}

		private void ptxTimeLine_KeyUp(object sender, System.Windows.Forms.KeyEventArgs e)
		{
			m_ShiftKeyDown = false;
		}

		private void Menu_DeleteChannel_Click(object sender, System.EventArgs e)
		{
			DeleteSelectedChannels();
		}

		private void Menu_Context_Popup(object sender, System.EventArgs e)
		{
			int numsel =CountNumSelected();
			if(numsel==0)
				Menu_DeleteChannel.Visible=false;
			else
				Menu_DeleteChannel.Visible=true;
		}

		private void Panel_WorkSpace_Move(object sender, System.EventArgs e)
		{
			this.Invalidate();
		}

		private void ptxTimeLine_Leave(object sender, System.EventArgs e)
		{
			this.Focus();
		}
		private void ptxTimeLine_OnChannelInfoChanged(ragPtxTimeLine.ptxTimeLine.ptxChannel channel)
		{
			foreach(ptxChannel c in m_Channels)
			{
				UpdateChannel(c);
			}
		}

		private void Panel_WorkSpaceClip_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			if(e.Button == MouseButtons.Right)
			{
                //SelectChannel(Panel_WorkSpace.PointToClient(Cursor.Position));
				Menu_Context.Show(this,this.PointToClient(Cursor.Position));
				return;
			}
		}

        private void Panel_WorkSpaceClip_SizeChanged(object sender, EventArgs e)
        {
            //Panel_WorkSpace.Width = Panel_WorkSpaceClip.Width;
            //Panel_WorkSpace.Height = Panel_WorkSpaceClip.Height;
            //DrawGrid();

        }

	}
}
