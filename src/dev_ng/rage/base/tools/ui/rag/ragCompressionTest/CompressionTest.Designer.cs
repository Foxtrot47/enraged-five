namespace ragCompressionTest
{
    partial class CompressionTest
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && (components != null) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.messageLabel = new System.Windows.Forms.Label();
            this.echoLabel = new System.Windows.Forms.Label();
            this.echoComboBox = new System.Windows.Forms.ComboBox();
            this.testDataNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.decompressedDataRichTextBox = new System.Windows.Forms.RichTextBox();
            this.decompressedResultLabel = new System.Windows.Forms.Label();
            this.decompressedLabel = new System.Windows.Forms.Label();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.compressedDataRichTextBox = new System.Windows.Forms.RichTextBox();
            this.compressedResultLabel = new System.Windows.Forms.Label();
            this.compressedLabel = new System.Windows.Forms.Label();
            this.recompressedDataRichTextBox = new System.Windows.Forms.RichTextBox();
            this.recompressedResultLabel = new System.Windows.Forms.Label();
            this.recompressedLabel = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer( this.components );
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.testDataNumericUpDown)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point( 0, 0 );
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add( this.messageLabel );
            this.splitContainer1.Panel1.Controls.Add( this.echoLabel );
            this.splitContainer1.Panel1.Controls.Add( this.echoComboBox );
            this.splitContainer1.Panel1.Controls.Add( this.testDataNumericUpDown );
            this.splitContainer1.Panel1.Controls.Add( this.decompressedDataRichTextBox );
            this.splitContainer1.Panel1.Controls.Add( this.decompressedResultLabel );
            this.splitContainer1.Panel1.Controls.Add( this.decompressedLabel );
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add( this.splitContainer2 );
            this.splitContainer1.Size = new System.Drawing.Size( 582, 629 );
            this.splitContainer1.SplitterDistance = 228;
            this.splitContainer1.TabIndex = 0;
            // 
            // messageLabel
            // 
            this.messageLabel.AutoSize = true;
            this.messageLabel.Location = new System.Drawing.Point( 187, 5 );
            this.messageLabel.Name = "messageLabel";
            this.messageLabel.Size = new System.Drawing.Size( 53, 13 );
            this.messageLabel.TabIndex = 6;
            this.messageLabel.Text = "Message:";
            // 
            // echoLabel
            // 
            this.echoLabel.AutoSize = true;
            this.echoLabel.Location = new System.Drawing.Point( 3, 6 );
            this.echoLabel.Name = "echoLabel";
            this.echoLabel.Size = new System.Drawing.Size( 35, 13 );
            this.echoLabel.TabIndex = 5;
            this.echoLabel.Text = "Echo:";
            this.echoLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // echoComboBox
            // 
            this.echoComboBox.FormattingEnabled = true;
            this.echoComboBox.Items.AddRange( new object[] {
            "None",
            "After each message",
            "After all messages"} );
            this.echoComboBox.Location = new System.Drawing.Point( 44, 2 );
            this.echoComboBox.Name = "echoComboBox";
            this.echoComboBox.Size = new System.Drawing.Size( 121, 21 );
            this.echoComboBox.TabIndex = 4;
            this.echoComboBox.Text = "After each message";
            this.echoComboBox.SelectedIndexChanged += new System.EventHandler( this.echoComboBox_SelectedIndexChanged );
            // 
            // testDataNumericUpDown
            // 
            this.testDataNumericUpDown.Location = new System.Drawing.Point( 246, 3 );
            this.testDataNumericUpDown.Maximum = new decimal( new int[] {
            0,
            0,
            0,
            0} );
            this.testDataNumericUpDown.Name = "testDataNumericUpDown";
            this.testDataNumericUpDown.Size = new System.Drawing.Size( 120, 20 );
            this.testDataNumericUpDown.TabIndex = 3;
            this.testDataNumericUpDown.ValueChanged += new System.EventHandler( this.testDataNumericUpDown_ValueChanged );
            // 
            // decompressedDataRichTextBox
            // 
            this.decompressedDataRichTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.decompressedDataRichTextBox.BackColor = System.Drawing.SystemColors.Window;
            this.decompressedDataRichTextBox.Location = new System.Drawing.Point( 3, 50 );
            this.decompressedDataRichTextBox.Name = "decompressedDataRichTextBox";
            this.decompressedDataRichTextBox.ReadOnly = true;
            this.decompressedDataRichTextBox.Size = new System.Drawing.Size( 576, 175 );
            this.decompressedDataRichTextBox.TabIndex = 2;
            this.decompressedDataRichTextBox.Text = "";
            // 
            // decompressedResultLabel
            // 
            this.decompressedResultLabel.AutoSize = true;
            this.decompressedResultLabel.Location = new System.Drawing.Point( 87, 34 );
            this.decompressedResultLabel.Name = "decompressedResultLabel";
            this.decompressedResultLabel.Size = new System.Drawing.Size( 123, 13 );
            this.decompressedResultLabel.TabIndex = 1;
            this.decompressedResultLabel.Text = "Decompressed Result = ";
            // 
            // decompressedLabel
            // 
            this.decompressedLabel.AutoSize = true;
            this.decompressedLabel.Location = new System.Drawing.Point( 0, 34 );
            this.decompressedLabel.Name = "decompressedLabel";
            this.decompressedLabel.Size = new System.Drawing.Size( 81, 13 );
            this.decompressedLabel.TabIndex = 0;
            this.decompressedLabel.Text = "Decompressed:";
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point( 0, 0 );
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add( this.compressedDataRichTextBox );
            this.splitContainer2.Panel1.Controls.Add( this.compressedResultLabel );
            this.splitContainer2.Panel1.Controls.Add( this.compressedLabel );
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add( this.recompressedDataRichTextBox );
            this.splitContainer2.Panel2.Controls.Add( this.recompressedResultLabel );
            this.splitContainer2.Panel2.Controls.Add( this.recompressedLabel );
            this.splitContainer2.Size = new System.Drawing.Size( 582, 397 );
            this.splitContainer2.SplitterDistance = 288;
            this.splitContainer2.TabIndex = 0;
            // 
            // compressedDataRichTextBox
            // 
            this.compressedDataRichTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.compressedDataRichTextBox.BackColor = System.Drawing.SystemColors.Window;
            this.compressedDataRichTextBox.Location = new System.Drawing.Point( 3, 21 );
            this.compressedDataRichTextBox.Name = "compressedDataRichTextBox";
            this.compressedDataRichTextBox.ReadOnly = true;
            this.compressedDataRichTextBox.Size = new System.Drawing.Size( 282, 373 );
            this.compressedDataRichTextBox.TabIndex = 2;
            this.compressedDataRichTextBox.Text = "";
            // 
            // compressedResultLabel
            // 
            this.compressedResultLabel.AutoSize = true;
            this.compressedResultLabel.Location = new System.Drawing.Point( 78, 4 );
            this.compressedResultLabel.Name = "compressedResultLabel";
            this.compressedResultLabel.Size = new System.Drawing.Size( 110, 13 );
            this.compressedResultLabel.TabIndex = 1;
            this.compressedResultLabel.Text = "Compressed Result = ";
            // 
            // compressedLabel
            // 
            this.compressedLabel.AutoSize = true;
            this.compressedLabel.Location = new System.Drawing.Point( 4, 4 );
            this.compressedLabel.Name = "compressedLabel";
            this.compressedLabel.Size = new System.Drawing.Size( 68, 13 );
            this.compressedLabel.TabIndex = 0;
            this.compressedLabel.Text = "Compressed:";
            // 
            // recompressedDataRichTextBox
            // 
            this.recompressedDataRichTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.recompressedDataRichTextBox.BackColor = System.Drawing.SystemColors.Window;
            this.recompressedDataRichTextBox.Location = new System.Drawing.Point( 3, 21 );
            this.recompressedDataRichTextBox.Name = "recompressedDataRichTextBox";
            this.recompressedDataRichTextBox.ReadOnly = true;
            this.recompressedDataRichTextBox.Size = new System.Drawing.Size( 283, 373 );
            this.recompressedDataRichTextBox.TabIndex = 2;
            this.recompressedDataRichTextBox.Text = "";
            // 
            // recompressedResultLabel
            // 
            this.recompressedResultLabel.AutoSize = true;
            this.recompressedResultLabel.Location = new System.Drawing.Point( 90, 4 );
            this.recompressedResultLabel.Name = "recompressedResultLabel";
            this.recompressedResultLabel.Size = new System.Drawing.Size( 123, 13 );
            this.recompressedResultLabel.TabIndex = 1;
            this.recompressedResultLabel.Text = "Recompressed Result = ";
            // 
            // recompressedLabel
            // 
            this.recompressedLabel.AutoSize = true;
            this.recompressedLabel.Location = new System.Drawing.Point( 3, 4 );
            this.recompressedLabel.Name = "recompressedLabel";
            this.recompressedLabel.Size = new System.Drawing.Size( 81, 13 );
            this.recompressedLabel.TabIndex = 0;
            this.recompressedLabel.Text = "Recompressed:";
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler( this.timer1_Tick );
            // 
            // CompressionTest
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add( this.splitContainer1 );
            this.Name = "CompressionTest";
            this.Size = new System.Drawing.Size( 582, 629 );
            this.splitContainer1.Panel1.ResumeLayout( false );
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout( false );
            this.splitContainer1.ResumeLayout( false );
            ((System.ComponentModel.ISupportInitialize)(this.testDataNumericUpDown)).EndInit();
            this.splitContainer2.Panel1.ResumeLayout( false );
            this.splitContainer2.Panel1.PerformLayout();
            this.splitContainer2.Panel2.ResumeLayout( false );
            this.splitContainer2.Panel2.PerformLayout();
            this.splitContainer2.ResumeLayout( false );
            this.ResumeLayout( false );

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Label decompressedLabel;
        private System.Windows.Forms.Label decompressedResultLabel;
        private System.Windows.Forms.RichTextBox decompressedDataRichTextBox;
        private System.Windows.Forms.NumericUpDown testDataNumericUpDown;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.Label compressedResultLabel;
        private System.Windows.Forms.Label compressedLabel;
        private System.Windows.Forms.RichTextBox compressedDataRichTextBox;
        private System.Windows.Forms.Label recompressedLabel;
        private System.Windows.Forms.Label recompressedResultLabel;
        private System.Windows.Forms.RichTextBox recompressedDataRichTextBox;
        private System.Windows.Forms.ComboBox echoComboBox;
        private System.Windows.Forms.Label messageLabel;
        private System.Windows.Forms.Label echoLabel;
        private System.Windows.Forms.Timer timer1;


    }
}
