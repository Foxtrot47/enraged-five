using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace ragCompressionTest
{
    /// <summary>
    /// Class for compressing and decompressing with the fiCompress and fiDecompress methods.
    /// Mirrored in C++ in rage/base/samples/sample_ragcompression, this class is a test bed for 
    /// ragCompression.fiCompressionData.
    /// </summary>
    public class fiCompressionData : CompressionData
    {
        public fiCompressionData()
        {

        }

        #region Abstract Function Overrides
        /// <summary>
        /// Creates a new instance of this class to be used by the ragCompressionTest.CompressionFactory
        /// </summary>
        /// <returns></returns>
        public override CompressionData CreateNewInstance()
        {
            return new fiCompressionData();
        }

        /// <summary>
        /// Retrieves an estimate of the maximum length the of the compressed data with the given size using
        /// fiCompressDecompress.CompressUpperBound.
        /// </summary>
        /// <param name="uncompressedSize"></param>
        /// <returns></returns>
        public override UInt32 GetCompressUpperBound( UInt32 uncompressedSize )
        {
            return fiCompressDecompress.CompressUpperBound( uncompressedSize );
        }

        /// <summary>
        /// Retrieves the size of this class's header data
        /// </summary>
        /// <returns></returns>
        public override int GetHeaderSize()
        {
            return 14; // header: CHF:0000:0000: where the 0000 is a u32 and everything else is a character
        }

        /// <summary>
        /// Retrieves the unique header prefix string so the Compression Factory can find the start of a CompressedData set
        /// </summary>
        /// <returns></returns>
        public override string GetHeaderPrefix()
        {
            return "CHF:";
        }

        /// <summary>
        /// Builds the header section for the Compression Factory
        /// </summary>
        /// <param name="buf">buffer to write to</param>
        /// <param name="startIndex">index to start at</param>
        /// <returns>length of header</returns>
        public override int BuildHeader( byte[] buf, int startIndex )
        {
            string prefix = GetHeaderPrefix();
            int len = prefix.Length;
            for ( int i = 0; i < len; ++i )
            {
                buf[startIndex + i] = (byte)prefix[i];
            }

            int index = startIndex + len;

            UInt32 compressedSize = this.CompressedSize;
            buf[index++] = (byte)(compressedSize);
            buf[index++] = (byte)(compressedSize >> 8);
            buf[index++] = (byte)(compressedSize >> 16);
            buf[index++] = (byte)(compressedSize >> 24);
            buf[index++] = (byte)':';

            UInt32 decompressedSize = this.DecompressedSize;
            buf[index++] = (byte)(decompressedSize);
            buf[index++] = (byte)(decompressedSize >> 8);
            buf[index++] = (byte)(decompressedSize >> 16);
            buf[index++] = (byte)(decompressedSize >> 24);
            buf[index++] = (byte)':';

            Debug.Assert( index < buf.Length );
            return index;
        }

        /// <summary>
        /// Retrieves the compressed data size and decompressed data size
        /// </summary>
        /// <param name="data">compressed data, starting with the header prefix</param>
        /// <param name="startIndex">index to start at</param>
        /// <returns>true if the header was parsed correctly</returns>
        public override bool ParseHeader( byte[] data, int startIndex )
        {
            if ( data.Length - startIndex < (UInt32)GetHeaderSize() )
            {
                // not enough data
                return false;
            }

            string prefix = GetHeaderPrefix();
            int len = prefix.Length;
            for ( int i = 0; i < len; ++i )
            {
                if ( data[startIndex + i] != (byte)prefix[i] )
                {
                    // bad header prefix
                    return false;
                }
            }

            int index = startIndex + len;

            UInt32 cSize = (UInt32)(data[index++]);
            cSize |= (UInt32)(data[index++] << 8);
            cSize |= (UInt32)(data[index++] << 16);
            cSize |= (UInt32)(data[index++] << 24);

            if ( data[index++] != (byte)':' )
            {
                // bad header
                return false;
            }

            UInt32 dSize = (UInt32)(data[index++]);
            dSize |= (UInt32)(data[index++] << 8);
            dSize |= (UInt32)(data[index++] << 16);
            dSize |= (UInt32)(data[index++] << 24);

            if ( data[index++] != (byte)':' )
            {
                // bad header
                return false;
            }

            if ( data.Length - startIndex < GetHeaderSize() + cSize )
            {
                // not enough data
                return false;
            }

            this.CompressedSize = cSize;
            this.DecompressedSize = dSize;
            return true;
        }

        /// <summary>
        /// Compresses the data into a buffer
        /// </summary>
        /// <param name="compressedData">buffer to write the compressed data to</param>
        /// <param name="uncompressedData">data that we want to compress</param>
        /// <param name="uncompressedSize">number of bytes to be compressed</param>
        /// <returns>the number of bytes compressed to compressedData.  0 on error.</returns>
        public override UInt32 Compress( byte[] compressedData, byte[] uncompressedData, UInt32 uncompressedSize )
        {
            UInt32 compressedSize = fiCompressDecompress.Compress( compressedData, uncompressedData, uncompressedSize, 16, false );
            Debug.Assert( compressedSize <= compressedData.Length );
            return compressedSize;
        }

        /// <summary>
        /// Decompresses the data into a buffer of the specified length
        /// </summary>
        /// <param name="decompressedData">buffer to decompress the data to</param>
        /// <param name="decompressedSize">number of bytes in the uncompressed stream (the original DecompressedSize)</param>
        /// <param name="compressedData">data to be decompressed</param>
        /// <param name="compressedSize">number of bytes to be decompressed</param>
        /// <returns>true on success</returns>
        public override bool Decompress( byte[] decompressedData, UInt32 decompressedSize, byte[] compressedData, UInt32 compressedSize )
        {
            return fiCompressDecompress.Decompress( decompressedData, decompressedSize, compressedData );
        }
        #endregion
    }
}
