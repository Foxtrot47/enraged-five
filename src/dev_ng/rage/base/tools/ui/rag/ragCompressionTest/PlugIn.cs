using System;
using System.Collections.Generic;
using System.Text;

namespace ragCompressionTest
{
    public class PlugIn : ragWidgetPage.WidgetPagePlugIn
    {
        #region WidgetPagePlugIn overrides
        public override string Name
        {
            get
            {
                return "Compression Test";
            }
        }

        public override void InitPlugIn( ragCore.PlugInHostData hostData )
        {
            m_hostData = hostData;

            base.InitPlugIn( hostData );
            m_Instance = this;

            base.m_pageNameToCreateDelMap[CompressionTest.MyName] = new ragWidgetPage.WidgetPage.CreatePageDel( CompressionTest.CreatePage );

            CompressionTest.Initialize();
        }

        public override void RemovePlugIn()
        {
            base.RemovePlugIn();
            CompressionTest.Shutdown();
            m_Instance = null;
        }

        public override void ActivatePlugInMenuItem( object sender, EventArgs args )
        {
            // Create the the window if it doesn't exist:
            if ( CompressionTest.Instance == null )
            {
                base.CreateAndOpenView( base.m_pageNameToCreateDelMap[CompressionTest.MyName] );
            }
            else if ( !CompressionTest.Instance.DockControl.IsOpen )
            {
                CompressionTest.Instance.DockControl.Open( TD.SandDock.WindowOpenMethod.OnScreenActivate );
            }
        }

        public override bool CheckPluginStatus()
        {
            return CompressionTest.IsConnected;
        }
        #endregion

        public static PlugIn Instance
        {
            get
            {
                return m_Instance;
            }
        }

        protected static PlugIn m_Instance;

        private ragCore.PlugInHostData m_hostData;

        public ragCore.PlugInHostData HostData
        {
            get
            {
                return m_hostData;
            }
        }
    }
}
