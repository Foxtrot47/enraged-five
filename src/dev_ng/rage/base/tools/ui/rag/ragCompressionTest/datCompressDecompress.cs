using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace ragCompressionTest
{
    /// <summary>
    /// Mirrored in C++ in rage/base/src/data, this class is a test bed for ragCompression.datCompressDecompress.
    /// </summary>
    public class datCompressDecompress
    {
        #region Constants
        static Int32 min_k = 3;

        // Determines max number of bytes in a window
        static Int32 k_width = 4;
        static Int32 max_k = (min_k + (1 << k_width) - 1);

        // Max window distance backward
        static int comp_slide_size = 1 << (16 - k_width);

        // Int32 ignore_mismatch;

        static Int32 HEADER_SIZE = 2;
        #endregion

        #region Public Functions
        /// <summary>
        /// Compress a chunk of memory
        /// NOTES:  If gzip compressed data to about 50% of its original size,
        /// this method will compress it to about 65% of its original size.
        /// Only uses a 4k lookback, so it's designed for smaller datasets
        /// than the fiCompress code.  It also doesn't use the heap either.
        /// Maximum compressed output size is capped at 64k.
        /// </summary>
        /// <param name="dest">output buffer (could be about 12% larger than src in the absolute worst case)</param>
        /// <param name="src">source buffer</param>
        /// <param name="srcSize">number of bytes to compress</param>
        /// <returns>number of bytes in output buffer</returns>
        public static UInt32 Compress( byte[] dest, byte[] src, UInt32 srcSize )
        {
            if ( srcSize == 0 )
            {
                return 0;
            }

            UInt32 i = 0;
            heap_entry[] heap = new heap_entry[comp_slide_size];
            queue_entry[] queue = new queue_entry[256];
            int freePtr = 0;

            UInt32 here = 0, follow = 0;

            //u8 *output = outbase + HEADER_SIZE;
            int outputIndex = HEADER_SIZE;

            int control = 1;
            //u8 *controlPtr = output++;
            int controlIndex = outputIndex++;

            /* set all Oldest ptrs to -1 */
            //memset(queue,-1,sizeof(queue));
            for ( int x = 0; x < queue.Length; ++x )
            {
                queue[x] = new queue_entry();
                queue[x].newest = queue[x].oldest = -1;
            }

            // initialize heap
            for ( int x = 0; x < heap.Length; ++x )
            {
                heap[x] = new heap_entry();
                heap[x].follow = 0;
                heap[x].next = 0;
                heap[x].restIndex = 0;
            }

            here = src[0];
            if ( min_k == 3 )
            {
                follow = (UInt32)(src[1] | (src[2] << 8));
            }
            else
            {
                follow = src[1];
            }

            while ( i < srcSize )
            {
                UInt32 best_k = 1, best_j = 0;
                int search = queue[here].oldest;

                if ( i < srcSize - 2 )
                {
                    while ( search != -1 )
                    {
                        /* definitely compressible! */
                        if ( follow == heap[search].follow )
                        {
                            UInt32 k = (UInt32)min_k;
                            while ( (i + k < src.Length) && (k < max_k)
                                && (src[i + k] == src[heap[search].restIndex + k]) )
                            {
                                k++;
                            }

                            /* if this is as good, but closer, use it
                                instead, hoping that cache is
                                fresher */
                            if ( k > best_k )
                            {
                                best_k = k;
                                best_j = heap[search].restIndex;
                            }
                        }

                        search = heap[search].next;
                    }
                }

                control <<= 1;			// make room for incoming control bit

                if ( best_k != 1 )
                {
                    UInt32 offset = i - best_j - 1;
                    UInt32 count = best_k - (UInt32)min_k;
                    // Assert(count >= 0 && count <= 15);
                    // Assert(offset >= 0 && offset <= 4095);
                    dest[outputIndex++] = (byte)(count | (offset << k_width));
                    dest[outputIndex++] = (byte)(offset >> (8 - k_width));
                    // fprintf(log,"[WINDOW %d,%d]\n",offset,count);
                }
                else
                {
                    control |= 1;
                    dest[outputIndex++] = (byte)here;
                    // fprintf(log,"[LITERAL %d]\n",here);
                }

                if ( (control & 256) != 0 )
                {	// it's full, so dump it
                    dest[controlIndex] = (byte)control;
                    controlIndex = outputIndex++;
                    control = 1;
                }

                /* update heap for however many we compressed */
                do
                {
                    if ( i >= comp_slide_size )
                    {
                        int oldest = queue[src[i - comp_slide_size]].oldest;
                        Debug.Assert( oldest == (int)freePtr );
                        queue[src[i - comp_slide_size]].oldest = heap[oldest].next;
                    }

                    if ( queue[here].oldest == -1 )
                    {
                        /* nothing in its queue, 
                            no chance of compression */
                        queue[here].oldest = queue[here].newest = freePtr;
                    }
                    else
                    {
                        /* link old Newest to this new entry */
                        heap[queue[here].newest].next = (Int16)freePtr;
                        /* update Newest to here */
                        queue[here].newest = freePtr;
                    }
                    heap[freePtr].follow = (UInt16)follow;
                    heap[freePtr].next = -1;
                    heap[freePtr].restIndex = i;
                    freePtr = (freePtr + 1) & (comp_slide_size - 1);
                    i++;

                    if ( min_k == 3 )
                    {
                        here = follow & 255;
                        follow >>= 8;

                        if ( i + 2 < srcSize )
                        {
                            follow |= (UInt32)(src[i + 2] << 8);
                        }
                    }
                    else
                    {
                        here = follow;

                        if ( i + 1 < srcSize )
                        {
                            follow = src[i + 1];
                        }
                        else
                        {
                            follow = 0;
                        }
                    }
                }
                while ( (--best_k) != 0 );
            }

            // make sure last control byte is flushed
            while ( (control & 256) == 0 )
            {
                control <<= 1;
            }

            dest[controlIndex] = (byte)control;

            int length = outputIndex - HEADER_SIZE;

            // Encode length at start of compressed stream
            Debug.Assert( length <= 65535 );
            dest[0] = (byte)length;
            dest[1] = (byte)(length >> 8);

            return (UInt32)(length + HEADER_SIZE);
        }

        /// <summary>
        /// Returns the maximum amount of space required to compress a set of data of the given size
        /// NOTES:  Despite the comments above about the upper bound being around 112% of size, we'll pad it up to 125%.
        /// </summary>
        /// <param name="srcSize">number of bytes</param>
        /// <returns>upper bound</returns>
        public static UInt32 CompressUpperBound( UInt32 srcSize )
        {
            return (UInt32)(srcSize * 1.25f);
        }

        /// <summary>
        /// Decompress a chunk of memory compressed with datCompress
        /// </summary>
        /// <param name="dest">output buffer</param>
        /// <param name="src">source buffer containing compressed data</param>
        /// <returns>number of bytes stored in output buffer</returns>
        public static UInt32 Decompress( byte[] dest, byte[] src )
        {
            int srcIndex = 0, destIndex = 0;
            int length = src[srcIndex++];
            length += (src[srcIndex++] << 8);

            do
            {
                byte control = src[srcIndex++];
                --length;
                for ( int i = 0; length != 0 && i < 8; i++, control <<= 1 )
                {
                    if ( (control & 0x80) != 0 )
                    {
                        // fprintf(log,"[LITERAL %d]\n",*src);
                        dest[destIndex++] = src[srcIndex++];
                        --length;
                    }
                    else
                    {
                        byte count = src[srcIndex];
                        int offset = (src[srcIndex+1] << 4) | (count >> 4);
                        // fprintf(log,"[WINDOW %d,%d]\n",offset,count & 15);

                        //const u8* ptr = dest - offset - 1;
                        int ptrIndex = destIndex - offset - 1;
                        
                        srcIndex += 2;
                        length -= 2;
                        count = (byte)((count & 15) + min_k);
                        do
                        {
                            dest[destIndex++] = dest[ptrIndex++];
                        } 
                        while ( (--count) != 0 );
                    }
                }
            } while ( length != 0 );

            return (UInt32)destIndex;
        }

        /// <summary>
        /// Decompress a chunk of memory compressed with datCompress, verifying that the stored
        /// compressed size agrees with the one we pass in.
        /// </summary>
        /// <param name="dest">output buffer</param>
        /// <param name="src">source buffer containing compressed data</param>
        /// <param name="compressedSize">size returned by Compress</param>
        /// <returns>number of bytes stored in output buffer</returns>
        public static UInt32 Decompress( byte[] dest, byte[] src, UInt32 compressedSize )
        {
            int length = src[0];
            length += (src[1] << 8);
            return (length <= compressedSize) ? Decompress( dest, src ) : 0;
        }
        #endregion

        #region Helper Structures
        struct heap_entry 
        {
	        public UInt16 follow;
	        public Int16 next;
	        //const byte *rest;
            public UInt32 restIndex;
        }

        struct queue_entry 
        {
	        public int oldest;
            public int newest;
        }
        #endregion
    }
}
