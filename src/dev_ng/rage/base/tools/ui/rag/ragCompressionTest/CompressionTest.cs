using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Data;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Windows.Forms;

using ragCore;
using ragUi;
using ragWidgetPage;

namespace ragCompressionTest
{
    public partial class CompressionTest : ragWidgetPage.WidgetPage
    {
        public CompressionTest()
        {
            InitializeComponent();
        }
        
        public CompressionTest( String appName, IBankManager bankMgr, TD.SandDock.DockControl dockControl )
            : base( appName, bankMgr, dockControl )
        {
            InitializeComponent();

            //int handle = this.Handle.ToInt32();  // *sigh* this insures that the windows handle is actually created!
            //Debug.Assert( handle > 0 );

            // register our compression data types
            sm_compressionFactory.RegisterCompressionData( new fiCompressionData() );
            sm_compressionFactory.RegisterCompressionData( new datCompressionData() );

            this.timer1.Start();
        }

        #region Enums
        private enum Echo
        {
            None,
            AfterEach,
            AfterAll
        }
        #endregion

        #region Constants
        private const int c_portOffset = (int)ragCore.PipeID.EnumSocketOffset.SOCKET_OFFSET_USER5 + 24;
        #endregion

        #region Static Variables
        private static TcpListener sm_Listener;
        private static Socket sm_Socket;
        private static Thread sm_ConnectThread;
        private static Thread sm_ReadThread;
        private static Mutex sm_Mutex = new Mutex();
        private static bool sm_IsConnected = false;
        private static CompressionFactory sm_compressionFactory = new CompressionFactory();
        private static Echo sm_echo = Echo.AfterEach;
        private static int sm_updateIndex = -1;
        #endregion

        #region Properties
        public static string MyName = "Compression Test";
        public static CompressionTest Instance;
        
        public static bool IsConnected
        {
            get
            {
                return sm_IsConnected;
            }
        }
        #endregion

        #region Overrides
        public override string GetMyName()
        {
            return MyName;
        }

        public override string GetTabText()
        {
            return MyName;
        }

        public override bool InitiallyFloating
        {
            get
            {
                return true;
            }
        }

        public override bool IsSingleInstance
        {
            get
            {
                return true;
            }
        }
        #endregion

        #region Init and Shutdown
        public static ragWidgetPage.WidgetPage CreatePage( String appName, IBankManager bankMgr, TD.SandDock.DockControl dockControl )
        {
            if ( Instance == null )
            {
                Instance = new CompressionTest( appName, bankMgr, dockControl );
                return Instance;
            }
            else
            {
                return Instance;
            }
        }

        public static void Initialize()
        {
            sm_ConnectThread = new Thread( new ThreadStart( ConnectProc ) );
            sm_ConnectThread.Name = "Compression Test Connect Thread";
            sm_ConnectThread.Start();
        }

        public static void Shutdown()
        {
            sm_Mutex.WaitOne();
            sm_Mutex.ReleaseMutex();

            if ( sm_ConnectThread != null )
            {
                sm_ConnectThread.Abort();
            }

            if ( sm_ReadThread != null )
            {
                sm_ReadThread.Abort();
            }

            if ( sm_Socket != null )
            {
                sm_Socket.Shutdown( SocketShutdown.Both );
                sm_Socket.Close();
            }

            if ( Instance != null )
            {
                Instance.ClearView();
            }
        }
        #endregion

        #region Network Connection and Message Processing
        private static void StartReadThread()
        {
            sm_ReadThread = new Thread( new ThreadStart( ReadProc ) );
            sm_ReadThread.Name = "Compression Test Read Thread";
            sm_ReadThread.Start();
        }

        private static void ConnectProc()
        {
            try
            {
                PipeID pipeID = PlugIn.Instance.HostData.MainRageApplication.GetRagPipeSocket( c_portOffset );
                sm_Listener = new TcpListener( IPAddress.Any, pipeID.Port );
                sm_Listener.Start();

                // wait until we have a connection:
                while ( !sm_Listener.Pending() )
                {
                    Thread.Sleep( 100 );
                }
                sm_Socket = sm_Listener.AcceptSocket();

                if ( sm_Socket != null )
                {
                    sm_Socket.Blocking = false;
                    sm_IsConnected = true;
                    StartReadThread();
                }
            }
            catch ( Exception e )
            {
                if ( e is ThreadAbortException )
                {
                    return;
                }
            }
        }

        private static void ReadProc()
        {
            int preCountAll = 0;

            while ( true )
            {
                sm_Mutex.WaitOne();

                try
                {
                    if ( sm_Socket.Available > 0 )
                    {                                               
                        byte[] data = new byte[sm_Socket.Available];
                        int bytes = sm_Socket.Receive( data );
                        
                        int preCountEach, postCountEach;

                        preCountEach = sm_compressionFactory.Data.Count;
                        sm_compressionFactory.DecompressData( data, (UInt32)bytes );
                        postCountEach = sm_compressionFactory.Data.Count;
                        
                        if ( sm_echo == Echo.AfterEach )
                        {
                            for ( int i = preCountEach; i < postCountEach; ++i )
                            {
                                CompressionData cData = sm_compressionFactory.Data[i];
                                
                                // compress each entry individually and send it                                
                                byte[] compressedStream = sm_compressionFactory.CompressData( cData.GetHeaderPrefix(),
                                    cData.DecompressedData, cData.DecompressedSize );

                                if ( compressedStream != null )
                                {
                                    sm_Socket.Send( compressedStream );
                                }
                                else
                                {
                                    string error = "Echo Each Error";
                                    byte[] msg = new byte[error.Length];
                                    for ( int j = 0; j < error.Length; ++j )
                                    {
                                        msg[j] = Convert.ToByte( error[j] );
                                    }
                                    sm_Socket.Send( msg );
                                }
                            }
                        }

                        if ( sm_echo != Echo.AfterAll )
                        {
                            // keep this updated when not this mode so we don't send any messages
                            preCountAll = postCountEach;
                            sm_updateIndex = preCountAll;
                        }
                    }
                    else
                    {
                        if ( (sm_echo == Echo.AfterAll) && (sm_compressionFactory.Data.Count > preCountAll) )
                        {
                            // create a new factory and copy over just the items we want
                            CompressionFactory cf = new CompressionFactory();
                            for ( int i = preCountAll; i < sm_compressionFactory.Data.Count; ++i )
                            {
                                cf.Data.Add( sm_compressionFactory.Data[i] );
                            }

                            // compress the whole thing
                            byte[] compressedData = cf.CompressData();

                            // send the whole thing
                            if ( compressedData != null )
                            {
                                sm_Socket.Send( compressedData );
                            }
                            else
                            {
                                string error = "Echo All Error";
                                byte[] msg = new byte[error.Length];
                                for ( int i = 0; i < error.Length; ++i )
                                {
                                    msg[i] = Convert.ToByte( error[i] );
                                }
                                sm_Socket.Send( msg );
                            }

                            preCountAll = sm_compressionFactory.Data.Count;
                            sm_updateIndex = preCountAll;
                        }
                    }
                }
                catch ( Exception e )
                {
                    if ( e is ThreadAbortException )
                    {
                        sm_Mutex.ReleaseMutex();
                        break;
                    }
                }

                sm_Mutex.ReleaseMutex();
            }
        }
        #endregion        

        #region Event Handlers
        private void testDataNumericUpDown_ValueChanged( object sender, EventArgs e )
        {
            sm_Mutex.WaitOne();

            int val = Convert.ToInt32( this.testDataNumericUpDown.Value ) - 1;
            if ( (val > -1) && (val < sm_compressionFactory.Data.Count) )
            {
                FillDataWindows( sm_compressionFactory.Data[val] );
            }
            else
            {
                this.compressedDataRichTextBox.Text = string.Empty;
                this.compressedResultLabel.Text = string.Empty;

                this.decompressedDataRichTextBox.Text = string.Empty;
                this.decompressedResultLabel.Text = string.Empty;

                this.recompressedDataRichTextBox.Text = string.Empty;
                this.recompressedResultLabel.Text = string.Empty;
            }

            sm_Mutex.ReleaseMutex();
        }

        private void echoComboBox_SelectedIndexChanged( object sender, EventArgs e )
        {
            sm_Mutex.WaitOne();

            if ( this.echoComboBox.SelectedIndex > -1 )
            {
                sm_echo = (Echo)this.echoComboBox.SelectedIndex;
            }

            sm_Mutex.ReleaseMutex();
        }
        #endregion

        #region Misc
        private void FillDataWindows( CompressionData cData )
        {
            System.Text.StringBuilder str = new System.Text.StringBuilder();
            for ( UInt32 i = 0; i < cData.CompressedSize; ++i )
            {
                string s = String.Format( "{0,2:X}", cData.CompressedData[i] );
                str.Append( s.Replace( ' ', '0' ) );
                str.Append( " " );
            }
            this.compressedDataRichTextBox.Text = str.ToString();
            this.compressedResultLabel.Text = "Received " + cData.CompressedSize + " bytes";

            str = new System.Text.StringBuilder();
            for ( UInt32 i = 0; i < cData.DecompressedSize; ++i )
            {
                str.Append( Convert.ToChar( cData.DecompressedData[i] ) );
            }
            this.decompressedDataRichTextBox.Text = str.ToString();
            this.decompressedResultLabel.Text = cData.GetHeaderPrefix() + " " + cData.DecompressedSize + " bytes";

            byte[] recompressedData = new byte[cData.GetCompressUpperBound( cData.DecompressedSize )];
            UInt32 recompressedSize = cData.Compress( recompressedData, cData.DecompressedData, cData.DecompressedSize );

            int errorChar = -1;

            bool success = true;
            if ( cData.CompressedSize != recompressedSize )
            {
                success = false;
            }
            else
            {
                for ( int i = 0; i < cData.CompressedSize; ++i )
                {
                    if ( cData.CompressedData[i] != recompressedData[i] )
                    {
                        success = false;
                        errorChar = i;
                        break;
                    }
                }
            }

            str = new System.Text.StringBuilder();
            for ( int i = 0; i < recompressedSize; ++i )
            {
                string s = String.Format( "{0,2:X}", recompressedData[i] );
                str.Append( s.Replace( ' ', '0' ) );
                str.Append( " " );
            }
            this.recompressedDataRichTextBox.Text = str.ToString();
            this.recompressedResultLabel.Text = "Compressed to " + recompressedSize + ". " + (success ? "Matches" : "Does not match");

            if ( errorChar > -1 )
            {
                this.recompressedDataRichTextBox.Select( errorChar, this.recompressedDataRichTextBox.Text.Length - errorChar );
            }
        }

        private void timer1_Tick( object sender, EventArgs e )
        {
            sm_Mutex.WaitOne();

            if ( sm_updateIndex > -1 )
            {
                if ( sm_updateIndex > testDataNumericUpDown.Maximum )
                {
                    testDataNumericUpDown.Maximum = sm_updateIndex;
                }

                testDataNumericUpDown.Value = sm_updateIndex;
                sm_updateIndex = -1;
            }

            sm_Mutex.ReleaseMutex();
        }
        #endregion
    }
}