using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace ragCompressionTest
{
    /// <summary>
    /// A class to construct and destruct multiple sets of compressed and uncompressed
    /// data into consecutive streams.  Mirrored in C++ in rage/base/src/samples/sample_ragcompression, 
    /// this class serves as a test bed for ragCompression.CompressionFactory.
    /// 
    /// Classes derived from ragCompressionTest.CompressionData must be registered in order to operate the 
    /// compress and decompress functions.
    /// </summary>
    public class CompressionFactory
    {
        public CompressionFactory()
        {

        }

        #region Variables
       	private bool m_copyCompressedData;
	    private byte[] m_readData;
	    private int m_start;
	    private int m_end;
	    private Dictionary<string,CompressionData> m_registeredTypes = new Dictionary<string,CompressionData>();
	    private List<CompressionData> m_compressionData = new List<CompressionData>();
        #endregion

        #region Properties
        /// <summary>
        /// Get:  Retrieves the list of ragCompressionTest.CompressionData objects.  This can be used to grow the list before calling 
        /// CompressData or to read the list after calling DecompressData.  Clearing the list and deleting every item 
        /// is up to the user.
        /// </summary>
        public List<CompressionData> Data
        {
            get
            {
                return m_compressionData;
            }
        }

        /// <summary>
        /// Get:  Return the state of m_copyCompressedData
        /// Set:  Specifies whether each ragCompressionTest.CompressionData should hold a copy of the compressed data when decompressed 
        ///     via DecompressData.
        /// </summary>
        public bool CopyCompressionData
        {
            get
            {
                return m_copyCompressedData;
            }
            set
            {
                m_copyCompressedData = value;
            }
        }
        #endregion

        /// <summary>
        /// Adds a derived ragCompressionTest.CompressionData class to the types that can be handled by this factory.
        /// </summary>
        /// <param name="data"></param>
	    public void RegisterCompressionData( CompressionData cData )
        {
            if ( !m_registeredTypes.ContainsKey( cData.GetHeaderPrefix() ) )
            {
                m_registeredTypes.Add( cData.GetHeaderPrefix(), cData );
            }
        }

        /// <summary>
        /// Using all items in the ragCompressionTest.CompressionData list, builds one massive stream out of each set of compressed data,
        /// adding the header info to each one.  Compression is performed if it wasn't already.
        /// </summary>
        /// <returns>the compressed data.  null on error.</returns>
	    public byte[] CompressData()
        {
	        // Compress if needed and determine total length
	        UInt32 dataSize = 0;
	        foreach ( CompressionData cData in m_compressionData )
	        {
		        if ( cData.CompressedSize == 0 )
		        {
			        cData.Compress( false );
		        }

		        if ( cData.CompressedSize > 0 )
		        {
			        dataSize += (UInt32)cData.GetHeaderSize() + cData.CompressedSize;
		        }
	        }

	        if ( dataSize == 0 )
	        {
		        return null;
	        }

	        // build the whole stream
	        byte[] compressedStream = new byte[dataSize];
	        int writeIndex = 0;
	        foreach ( CompressionData cData in m_compressionData )
	        {
		        if ( cData.CompressedSize > 0 )
		        {
			        int headerSize = cData.BuildHeader( compressedStream, writeIndex );
			        writeIndex += headerSize;
                    Array.ConstrainedCopy( cData.CompressedData, 0, compressedStream, writeIndex, (int)cData.CompressedSize );
                    writeIndex += (int)cData.CompressedSize;
		        }
	        }

	        return compressedStream;
        }

        /// <summary>
        /// Takes in an array of bytes and processes each compressed stream, decompressing as it goes.
        /// Multiple sets of compressed data could exist in the data array, and a partial set may exist at the end
        /// of the array.  When this partial data exists, we return true.  Call DecompressData again with the rest of the data
        /// (example: reading from a Socket) to process the remaining data and continue.
        /// </summary>
        /// <param name="data">the data we want decompressed</param>
        /// <param name="dataSize">length of data or the number of bytes we want processed</param>
        /// <returns>true if not all of the data could be processed</returns>
	    public bool DecompressData( byte[] data, UInt32 dataSize )
        {
	        bool rtn = false;

	        if ( m_readData == null )
	        {
		        m_readData = data;
	        }
	        else
	        {
		        UInt32 newSize = dataSize + (UInt32)m_readData.Length;
		        byte[] newData = new byte[newSize];
		        int writeIndex = 0;
                Array.ConstrainedCopy( m_readData, 0, newData, writeIndex, m_readData.Length );
		        writeIndex += m_readData.Length;
                Array.ConstrainedCopy( data, 0, newData, writeIndex, (int)dataSize );

		        m_readData = newData;
	        }

            StringBuilder readStrBuilder = new StringBuilder();
            foreach ( byte b in m_readData )
            {
                readStrBuilder.Append( Convert.ToChar( b ) );
            }
            string readStr = readStrBuilder.ToString();

	        while ( true )
	        {
		        m_start = -1;

		        CompressionData cData = null;
                int startIndex = int.MaxValue;
        		
		        // loop through our registered types and try to match a prefix, the earliest if multiple are found
                foreach ( KeyValuePair<string,CompressionData> pair in m_registeredTypes )
                {
                    int indexOf = readStr.IndexOf( pair.Value.GetHeaderPrefix(), m_end );
                    if ( (indexOf > -1) && (indexOf < startIndex) )
                    {
                        startIndex = indexOf;
                        cData = pair.Value;
                    }
                }
        		
		        if ( cData != null )
		        {
			        m_start = startIndex;

			        if ( cData.ParseHeader( m_readData, m_start ) )
			        {
				        CompressionData newData = cData.CreateNewInstance();

				        newData.SetCompressedData( m_readData, m_start + cData.GetHeaderSize(), cData.CompressedSize );

				        // have to pass in DecompressedSize because we didn't call newData->ParseHeader(...)
				        if ( newData.Decompress( cData.DecompressedSize, false ) )
				        {
					        m_compressionData.Add( newData );
					        m_end = m_start + cData.GetHeaderSize() + (int)newData.CompressedSize;
				        }
				        else
				        {
					        Console.WriteLine( "Decompression failed" );
				        }
			        }
			        else
			        {
				        // more data to receive for this set of compressed data
				        rtn = true;
				        break;
			        }
		        }
		        else
		        {
			        // end of string
			        rtn = false;
			        break;
		        }
	        }

	        if ( m_start > -1 )
	        {
		        // remove data we've already processed
		        int newSize = m_readData.Length - m_start;
		        byte[] newData = new byte[newSize];
                Array.ConstrainedCopy( m_readData, m_start, newData, 0, newSize );

		        m_readData = newData;
		        m_start = m_end = 0;
	        }
	        else
	        {
                m_readData = null;
		        m_start = m_end = 0;
	        }

	        return rtn;
        }

        /// <summary>
        /// Compresses the given data and adds the header.  This will not add an entry to the compression data list.
        /// </summary>
        /// <param name="type">which registered ragCompressionTest.CompressionData to use.  What is returned by 
        ///     ragCompressionTest.CompressionData.GetHeaderPrefix()</param>
        /// <param name="uncompressedData">data that we want to compress</param>
        /// <param name="uncompressedSize">number of bytes to compress</param>
        /// <returns>the compressed data.  null on error.</returns>
	    public byte[] CompressData( string type, byte[] uncompressedData, UInt32 uncompressedSize )
        {
            CompressionData cData;
            if ( !m_registeredTypes.TryGetValue( type, out cData ) )
            {
                return null;
            }

            cData.SetDecompressedData( uncompressedData, 0, uncompressedSize );

            UInt32 compressedDataLength = cData.GetCompressUpperBound( uncompressedSize );
            byte[] compressedData = new byte[compressedDataLength];

            // retain ownership of CompressedData
            cData.CompressedData = compressedData;

            if ( cData.Compress( false ) )
            {
                int headerSize = cData.GetHeaderSize();
                UInt32 dataSize = (UInt32)headerSize + cData.CompressedSize;
                byte[] compressedStream = new byte[dataSize];

                int hSize = cData.BuildHeader( compressedStream, 0 );
                Debug.Assert( hSize == headerSize );

                Array.ConstrainedCopy( compressedData, 0, compressedStream, headerSize, (int)cData.CompressedSize );

                // zero out so we don't get confused or try to access something we shouldn't
                cData.DecompressedData = null;
                cData.CompressedData = null;

                return compressedStream;
            }

            return null;
        }
    }
}
