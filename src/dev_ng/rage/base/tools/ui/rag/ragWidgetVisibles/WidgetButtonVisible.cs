using System;
using System.Windows.Forms;

using ragCore;

namespace ragWidgets
{
	/// <summary>
	/// Summary description for WidgetButtonVisible.
	/// </summary>
	public class WidgetButtonVisible : WidgetVisible
	{
        public WidgetButtonVisible(IWidgetView widgetView, WidgetButton button, Control parent, ToolTip tooltip) 
			: base(widgetView,parent,button)
		{
			m_Widget=button;

			m_Button = new System.Windows.Forms.Button();
			m_Button.Name = button.Title;
			m_Button.TabIndex = 0;
			m_Button.Text = button.Title;
			m_Button.Click += new EventHandler(Button_Click);
            m_Button.Enabled = !button.IsReadOnly && button.Enabled;

            // Add tooltip
            if (!String.IsNullOrEmpty(Widget.Memo))
            {
                String memo = Widget.Memo ?? "Add useful tool tips with the memo field in the runtime code";
                String tooltipValue = String.Format("{0}\r\n{1}", button.HelpTitle, memo);
                tooltip.SetToolTip(m_Button, tooltipValue);
            }

			parent.Controls.Add(m_Button);
			AddMouseEnter(m_Button);
			m_Button.MouseUp += new MouseEventHandler(OnMouseUp);
			m_Button.MouseDown += new MouseEventHandler(OnMouseDown);
			SetDragDropHandlers(m_Button);
		}

		public override Widget Widget {get {return m_Widget;}}

		public override Control DragDropControl
		{
			get {return m_Button;}
		}

		public override void Remove()
		{
            RemoveDragDropHandlers(m_Button);
			m_Parent.Controls.Remove(m_Button);
			base.Remove();
		}

		public override void Destroy()
		{
			base.Destroy();
			m_Widget=null;
			m_Button=null;
		}

		public override Control Control 
		{
			get
			{
				return m_Button;
			}
		}
	
		public override void UpdatePersist(bool persist) {}

		private System.Windows.Forms.Button m_Button;
		private WidgetButton m_Widget;

		private void Button_Click(object sender, EventArgs e)
		{
			m_Widget.UpdateRemote();
		}
	}

	
}
