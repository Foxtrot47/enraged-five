using System;
using System.Windows.Forms;

using ragCore;

namespace ragWidgets
{
    /// <summary>
    /// Summary description for WidgetTextVisible.
    /// </summary>
    public class WidgetToggleVisible : WidgetControlBaseVisible
    {
        public WidgetToggleVisible(IWidgetView widgetView, WidgetToggle toggle, Control parent, ToolTip tooltip)
            : base(widgetView, parent, toggle)
        {
            m_Widget = toggle;

            m_CheckBox.Text = toggle.Title;
            m_CheckBox.Value = toggle.Checked;
            m_CheckBoxEH = new EventHandler( control_ValueChanged );
            m_CheckBox.ValueChanged += m_CheckBoxEH;
            m_CheckBox.IsReadOnly = m_Widget.IsReadOnly;

            FinishCreation(tooltip);
            if (Widget.IsModified())
            {
                UpdateModifiedIndication();
            }
        }

        protected override void widget_ValueChanged( object sender, EventArgs e )
        {
            WidgetToggle widget = (WidgetToggle)sender;
            UpdateValueFromRemote( widget.Checked );
        }

        public void control_ValueChanged( object obj, System.EventArgs e )
        {
            // get our value from the visible:
            ragUi.ControlToggle checkBox = obj as ragUi.ControlToggle;
            if ( checkBox != null && m_Widget.Checked != checkBox.Value )
            {
                m_Widget.Checked = checkBox.Value;
            }
        }

        #region Variables
        private ragUi.ControlToggle m_CheckBox = new ragUi.ControlToggle();

        private EventHandler m_CheckBoxEH;
        private WidgetToggle m_Widget;
        private delegate void UpdateInvoke(bool inValue);
        #endregion

        #region Overrides
        public override ragUi.ControlBase ControlBase
        {
            get
            {
                return m_CheckBox;
            }
        }

        public override Widget Widget { get { return m_Widget; } }

        public override void Destroy()
        {
            base.Destroy();
            m_Widget = null;
            m_CheckBox.Dispose();
            m_CheckBox = null;
            m_CheckBoxEH = null;

        }
        #endregion

        #region Public Functions
        /// <summary>
        /// disable event handler temporarily.
        /// </summary>
        public void UpdateValueFromRemote(bool value)
        {
            if (m_CheckBox.InvokeRequired)
            {
                UpdateInvoke updateInvoke = delegate(bool inValue)
                {
                    m_CheckBox.ValueChanged -= m_CheckBoxEH; // disable temporarily
                    m_CheckBox.Value = value;
                    m_CheckBox.ValueChanged += m_CheckBoxEH; // enable again
                };

                object[] args = new object[1];
                args[0] = value;
                m_CheckBox.Invoke(updateInvoke, args);
            }
            else
            {
                m_CheckBox.ValueChanged -= m_CheckBoxEH; // disable temporarily
                m_CheckBox.Value = value;
                m_CheckBox.ValueChanged += m_CheckBoxEH; // enable again
            }

            UpdateModifiedIndication();
        }
        #endregion
    }
}
