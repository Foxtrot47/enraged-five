using System;
using System.Diagnostics;
using System.Windows.Forms;

using ragCore;
using ragUi;
using ListViewEx;

namespace ragWidgets
{
	/// <summary>
	/// Summary description for WidgetListVisible.
	/// </summary>
	public class WidgetListVisible : WidgetControlBaseVisible
	{
        public WidgetListVisible(IWidgetView widgetView, WidgetList list, Control parent, ToolTip tooltip) 
			: base(widgetView,parent,list)
		{
            m_Widget = list;
            m_Widget.StructureChanged += widget_StructureChanged;
            m_Widget.ItemRemoved += widget_ItemRemoved;
            m_Widget.ItemAdded += widget_ItemAdded;

			m_List=new ControlListRag();
			m_List.IsReadOnly=m_Widget.IsReadOnly;
			m_List.Text=m_Widget.Title;
			m_List.EditComplete+=new ControlListRag.EditCompleteDelegate(SendEdit);
            m_List.ControlListDoubleClick += new ControlListRag.DoubleClickDelegate(SendDoubleClick);
            m_List.ControlListSingleClick += new ControlListRag.SingleClickDelegate(SendSingleClick);
            m_List.Rebuild(m_Widget);

            FinishCreation(tooltip);
		}

        void widget_StructureChanged( object sender, EventArgs e )
        {
            Rebuild();
        }

        void widget_ItemRemoved(object sender, ragWidgets.WidgetList.ItemRemovedEventArgs e)
        {
            Remove( e.Item );
        }

        void widget_ItemAdded(object sender, ragWidgets.WidgetList.ItemAddedEventArgs e)
        {
            Add(e.Item, e.Column, e.Value);
        }

		public override Widget Widget {get {return m_Widget;}}

		public override ControlBase ControlBase
		{
			get
			{
				return m_List;
			}
		}


		public override Control DragDropControl
		{
			get {return null;}
		}

        public override void Destroy()
        {
            base.Destroy();
            m_Widget.StructureChanged -= widget_StructureChanged;
            m_Widget.ItemRemoved -= widget_ItemRemoved;
            m_Widget.ItemAdded -= widget_ItemAdded;
            m_Widget = null;
            m_List.Dispose();
            m_List = null;
        }

        /* 
         * Not used anymore
		public void Add(int key,int column,String val)
		{
            m_List.SuspendLayout();
			m_List.Add(key,column,val);
            m_List.ResumeLayout();
		}
         * */

		public void Remove(int key)
		{
            m_List.SuspendLayout();
			m_List.Remove(key);
            m_List.ResumeLayout();
		}

        public void Add(int key, int column, object value)
        {
            m_List.SuspendLayout();
            m_List.Add(key, column, value.ToString());
            m_List.ResumeLayout();
        }

		public void Rebuild()
		{
            m_List.SuspendLayout();
			m_List.Rebuild(m_Widget);
            m_List.ResumeLayout();
		}

		public void SendEdit(ListViewEx.ListViewEx.SubItemEndEditingEventArgs e,int key)
		{
			m_Widget.ModifyItem(key,e.SubItem,e.DisplayText);
		}

        public void SendDoubleClick(int key)
        {
            m_Widget.DoubleClick(key);
        }

        public void SendSingleClick(int key)
        {
            m_Widget.SingleClick(key);
        }

		private WidgetList m_Widget;
		private ControlListRag m_List;
	}

	
}
