using System;
using System.Collections.Generic;
using System.Windows.Forms;
using ragCore;
using RSG.Base.Logging;

namespace ragWidgets
{
	/// <summary>
	/// Summary description for IViewPlugIn.
	/// </summary>

    public class PlugInHostData
    {
        public PlugInHostData( IRageApplication mainApp, IRageApplication[] apps,
            IDockableViewManager viewMgr, ToolStripMenuItem pluginsMenu, ToolStrip menuToolstrip,
            DllInfo dllInfo, string platform, bool connectedToProxy,
            IKeyUpProcessor keyProcessor, ILog log)
        {
            m_mainApp = mainApp;
            m_apps = apps;
            m_viewManager = viewMgr;
            m_pluginsMenu = pluginsMenu;
            m_menuToolstrip = menuToolstrip;
            m_dllInfo = dllInfo;
            m_platform = platform;
            m_connectedToProxy = connectedToProxy;
            m_KeyUpProcessor = keyProcessor;
            m_log = log;
        }

        #region Variables
        private IRageApplication m_mainApp;
        private IRageApplication[] m_apps;
        private IDockableViewManager m_viewManager;
        private ToolStripMenuItem m_pluginsMenu;
        private ToolStrip m_menuToolstrip;
        private DllInfo m_dllInfo;
        private string m_platform;
        private bool m_connectedToProxy;
        private IKeyUpProcessor m_KeyUpProcessor;
        private ILog m_log;
        #endregion

        /// <summary>
        /// Main Rage Application.
        /// </summary>
        public IRageApplication MainRageApplication
        {
            get
            {
                return m_mainApp;
            }
        }

        /// <summary>
        /// The list of all running applications, including the Main App.
        /// </summary>
        public IRageApplication[] RageApplications
        {
            get
            {
                return m_apps;
            }
        }

        /// <summary>
        /// The main View Manager
        /// </summary>
        public IDockableViewManager ViewManager
        {
            get
            {
                return m_viewManager;
            }
        }

        /// <summary>
        /// The "Plugins" menu item.
        /// </summary>
        public ToolStripMenuItem PlugInToolStripMenu
        {
            get
            {
                return m_pluginsMenu;
            }
        }

        /// <summary>
        /// The main tool strip.
        /// </summary>
        public ToolStrip MenuToolStrip
        {
            get
            {
                return m_menuToolstrip;
            }
        }

        /// <summary>
        /// Information about this plugin, including any saved data
        /// </summary>
        public DllInfo DllInfo
        {
            get
            {
                return m_dllInfo;
            }
        }

        /// <summary>
        /// A string representing the current platform: "Win32", "Xbox360", "PlayStation3", "PlayStationPortable" or "Unknown"
        /// </summary>
        public string Platform
        {
            get
            {
                return m_platform;
            }
        }

        public bool ConnectedToProxy
        {
            get
            {
                return m_connectedToProxy;
            }
        }

        public IKeyUpProcessor KeyUpProcessor
        {
            get { return m_KeyUpProcessor; }
            set { m_KeyUpProcessor = value; }
        
        }

        public ILog Log
        {
            get { return m_log; }
        }
    }

    /// <summary>
    /// Interface class for user-defined Rag plugins.
    /// </summary>
	public interface IViewPlugIn
	{
		string Name {get;}

        /// <summary>
        /// Initialize the custom plugin
        /// 1) Add your menu item(s) to PlugInHostData.m_PlugInToolStripMenu.DropDownItems (the Plugins menu) and/or to 
        ///    PlugInHostData.m_MenuToolStrip.Items.
        /// 2) If your Plugin menu item(s) should be disabled for any reason, add an event handler to 
        ///    PlugInHostData.m_PlugInToolStripMenu.DropDownOpenning.
        /// 3) Do not perform any widget bindings here because none will exist when the function is called.  Use 
        ///    the PlugInHostData.m_MainBankManager.InitFinished event instead.
        /// 4) Load relevant data from DllInfo.PluginDataHash
        /// </summary>
        /// <param name="data"></param>
		void InitPlugIn(PlugInHostData data);

        /// <summary>
        /// Shutdown the custom plugin
        /// 1) Remove any menu item that were created in InitPlugIn
        /// 2) Remove any event handlers that were added.
        /// 3) Save relevant data to DllInfo.PluginDataHash
        /// </summary>
		void RemovePlugIn();
	}
}
