using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

using EV.Windows.Forms;
using ListViewEx;
using ragWidgets;

namespace ragWidgets
{
	public class ControlListRag : ragUi.ControlList
	{
		private System.ComponentModel.IContainer components = null;

		public ControlListRag()
		{
			// This call is required by the Windows Form Designer.
			InitializeComponent();

			// TODO: Add any initialization after the InitializeComponent call
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		public void Rebuild(WidgetList list)
		{
			ColumnData[] headers = new ColumnData[list.ColumnTypes.Count];

			// setup column headers:
			for (int i=0;i<list.ColumnTypes.Count;i++)
			{
				headers[i] = new ColumnData();
				headers[i].Name = list.ColumnHeaders[i];
				headers[i].DataType = (ColumnData.Type)list.ColumnTypes[i];
			}

			m_KeyToListItem.Clear();
			m_ListItemToKey.Clear();
			
			// setup row information:
			RowData[] rows = new RowData[list.ValueHash.Count];
			int rowNum = 0;

            foreach ( KeyValuePair<int,ArrayList> pair in list.ValueHash )
            {
                rows[rowNum] = new RowData();
                rows[rowNum].Key = pair.Key;
                rows[rowNum].Item = new ListViewItem( pair.Value[0].ToString(), 0 );

                for ( int i = 1; i < pair.Value.Count; i++ )
                {
                    rows[rowNum].Item.SubItems.Add( pair.Value[i].ToString() );
                }

                rowNum++;
            }

			Rebuild(headers, rows);
		}

		protected override void OnIconChanged()
		{
			// this space intentionally left blank
		}

		#region Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			components = new System.ComponentModel.Container();
		}
		#endregion
	}
}

