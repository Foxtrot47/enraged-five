using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

using ragCore;
using ragUi;

namespace ragWidgets
{
    public class WidgetAngleVisible : WidgetControlBaseVisible
    {
        public WidgetAngleVisible(IWidgetView widgetView, WidgetAngle angle, Control parent, ToolTip tooltip)
            : base( widgetView, parent, angle )
        {
            m_Widget = angle;

            switch ( angle.DisplayType )
            {
                case WidgetAngle.AngleType.Degrees:
                    {
                        ControlAngleSlider angleSlider = new ControlAngleSlider( ControlAngleSlider.Mode.Degrees, angle.MinValue, angle.MaxValue );
                        angleSlider.Text = angle.Title;
                        angleSlider.ValueChanged += new EventHandler( control_ValueChanged );
                        angleSlider.UpdateValueFromRemote( angle.Value[0] );

                        m_controlAngle = angleSlider;
                    }
                    break;
                case WidgetAngle.AngleType.Fraction:
                    {
                        ControlAngleSlider angleSlider = new ControlAngleSlider( ControlAngleSlider.Mode.Fraction, angle.MinValue, angle.MaxValue );
                        angleSlider.Text = angle.Title;
                        angleSlider.ValueChanged += new EventHandler( control_ValueChanged );
                        angleSlider.UpdateValueFromRemote( angle.Value[0] );

                        m_controlAngle = angleSlider;
                    }
                    break;
                case WidgetAngle.AngleType.Radians:
                    {
                        ControlAngleSlider angleSlider = new ControlAngleSlider( ControlAngleSlider.Mode.Radians, angle.MinValue, angle.MaxValue );
                        angleSlider.Text = angle.Title;
                        angleSlider.ValueChanged += new EventHandler( control_ValueChanged );
                        angleSlider.UpdateValueFromRemote( angle.Value[0] );

                        m_controlAngle = angleSlider;
                    }
                    break;
                default:
                    {
                        ControlAngleVector2 angleVector2 = new ControlAngleVector2( angle.MinValue, angle.MaxValue );
                        angleVector2.Text = angle.Title;
                        angleVector2.ValueChanged += new EventHandler( control_ValueChanged );
                        angleVector2.UpdateValueFromRemote( angle.Value );

                        m_controlAngle = angleVector2;
                    }
                    break;
            }
            m_controlAngle.IsReadOnly = angle.IsReadOnly;

            FinishCreation(tooltip);

            if (Widget.IsModified())
            {
                UpdateModifiedIndication();
            }
        }

        protected override void widget_ValueChanged( object sender, EventArgs e )
        {
            WidgetAngle widget = (WidgetAngle)sender;
            UpdateValueFromRemote( widget.Value );
        }

        void control_ValueChanged( object sender, System.EventArgs e )
        {
            if ( sender is ragUi.ControlAngleSlider )
            {
                // get our value from the visible:
                ragUi.ControlAngleSlider ctl = sender as ragUi.ControlAngleSlider;
                if ( m_Widget.Value[0] != (float)ctl.Value )
                {
                    m_Widget.Value = new float[] { (float)ctl.Value, 0.0f };
                }
            }
            else if ( sender is ragUi.ControlAngleVector2 )
            {
                // get our value from the visible:
                ragUi.ControlAngleVector2 ctl = sender as ragUi.ControlAngleVector2;
                decimal[] val = ctl.Value;
                if ( (m_Widget.Value[0] != (float)val[0]) || (m_Widget.Value[1] != (float)val[1]) )
                {
                    m_Widget.Value = new float[] { (float)val[0], (float)val[1] };
                }
            }
        }

        #region Variables
        private WidgetAngle m_Widget;
        private ControlAngleBase m_controlAngle;
        #endregion

        #region Overrides
        public override ragUi.ControlBase ControlBase
        {
            get
            {
                return m_controlAngle;
            }
        }

        public override Widget Widget
        {
            get
            {
                return m_Widget;
            }
        }

        public override void Destroy()
        {
            base.Destroy();
            m_Widget.ValueChanged -= widget_ValueChanged;
            m_Widget = null;
            m_controlAngle.Dispose();
            m_controlAngle = null;
        }
        #endregion

        #region Public Functions
        public void UpdateValueFromRemote( float[] value )
        {
            if ( m_controlAngle is ControlAngleSlider )
            {
                (m_controlAngle as ControlAngleSlider).UpdateValueFromRemote( value[0] );
            }
            else if ( m_controlAngle is ControlAngleVector2 )
            {
                (m_controlAngle as ControlAngleVector2).UpdateValueFromRemote( value );
            }

            UpdateModifiedIndication();
        }
        #endregion
    }
}
