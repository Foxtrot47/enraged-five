using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

using ragCore;
using ragUi;

namespace ragWidgets
{
    public class WidgetMatrixVisible : WidgetControlBaseVisible
    {
        public WidgetMatrixVisible(IWidgetView widgetView, WidgetMatrix widget, Control parent, ToolTip tooltip)
            : base( widgetView, parent, widget )
        {
            m_Widget = widget;

            m_controlMatrix = new ControlMatrix();

            m_controlMatrix.BeginInit();
            m_controlMatrix.Text = widget.Title;
            m_controlMatrix.NumColumns = widget.NumColumns;
            m_controlMatrix.SetRange( widget.Minimum, widget.Maximum );
            m_controlMatrix.SetStep( widget.Step );
            m_controlMatrix.NumRows = widget.NumRows;
            m_controlMatrix.NumColumns = widget.NumColumns;
            AddEventHandlers();
            m_controlMatrix.UpdateValueFromRemote( m_Widget.Value );
            m_controlMatrix.IsReadOnly = widget.IsReadOnly;
            m_controlMatrix.EndInit();


            FinishCreation(tooltip);
            if (Widget.IsModified())
            {
                UpdateModifiedIndication();
            }
        }

        protected override void widget_ValueChanged( object sender, EventArgs e )
        {
            WidgetMatrix widget = (WidgetMatrix)sender;
            UpdateValueFromRemote( widget.Value );
        }

        public void control_ValueChanged( object sender, EventArgs e )
        {
            ControlMatrix ctrl = sender as ControlMatrix;

            float[][] matrix = new float[4][];
            for ( int i = 0; i < 4; ++i )
            {
                matrix[i] = new float[4];
                for ( int j = 0; j < 4; ++j )
                {
                    matrix[i][j] = (float)ctrl.Matrix[i][j];
                }
            }

            m_Widget.Value = matrix;
        }

        #region Variables
        private WidgetMatrix m_Widget;
        private ControlMatrix m_controlMatrix;
        private List<EventHandler> m_eventHandlers = new List<EventHandler>();
        private List<ControlMatrix.ParameterizedOperationEventHandler> m_matrixEventHandlers = new List<ControlMatrix.ParameterizedOperationEventHandler>();
        private delegate void UpdateInvoke(float[][] inValue);
        #endregion

        #region Overrides
        public override ragUi.ControlBase ControlBase
        {
            get
            {
                return m_controlMatrix;
            }
        }

        public override Widget Widget
        {
            get
            {
                return m_Widget;
            }
        }

        public override void Remove()
        {
            base.Remove();

            RemoveEventHandlers();
        }

        public override void Destroy()
        {
            base.Destroy();
            m_Widget = null;
            m_controlMatrix.Dispose();
            m_controlMatrix = null;
        }
        #endregion

        #region Properties
        public float[][] Value
        {
            get
            {
                float[][] m = new float[4][];
                for ( int i = 0; i < 4; ++i )
                {
                    m[i] = new float[4];
                    for ( int j = 0; j < 4; ++j )
                    {
                        m[i][j] = (float)m_controlMatrix.Matrix[i][j];
                    }
                }

                return m;
            }
        }
        #endregion

        #region Public Functions
        public void UpdateValueFromRemote( float[][] value )
        {
            if (m_controlMatrix.InvokeRequired == true)
            {
                UpdateInvoke updateInvoke = new UpdateInvoke(m_controlMatrix.UpdateValueFromRemote);

                object[] args = new object[1];
                args[0] = value;
                m_controlMatrix.Invoke(updateInvoke, args);
            }
            else
            {
                m_controlMatrix.UpdateValueFromRemote(value);
            }

            UpdateModifiedIndication();
        }
        #endregion

        #region Private Functions
        private void AddEventHandlers()
        {
            m_eventHandlers.Add( new EventHandler( control_ValueChanged ) );
            m_eventHandlers.Add( new EventHandler( m_Widget.AbsClickEH ) );
            m_matrixEventHandlers.Add( new ControlMatrix.ParameterizedOperationEventHandler( Add3x3ClickEH ) );
            m_matrixEventHandlers.Add( new ControlMatrix.ParameterizedOperationEventHandler( AddClickEH ) );
            m_eventHandlers.Add( new EventHandler( m_Widget.CoordinateInverseSafeClickEH ) );
            m_matrixEventHandlers.Add( new ControlMatrix.ParameterizedOperationEventHandler( CrossProductClickEH ) );
            m_matrixEventHandlers.Add( new ControlMatrix.ParameterizedOperationEventHandler( Dot3x3ClickEH ) );
            m_matrixEventHandlers.Add( new ControlMatrix.ParameterizedOperationEventHandler( Dot3x3CrossProdMtxClickEH ) );
            m_matrixEventHandlers.Add( new ControlMatrix.ParameterizedOperationEventHandler( Dot3x3CrossProdTransposeClickEH ) );
            m_matrixEventHandlers.Add( new ControlMatrix.ParameterizedOperationEventHandler( Dot3x3FromLeftClickEH ) );
            m_matrixEventHandlers.Add( new ControlMatrix.ParameterizedOperationEventHandler( Dot3x3TransposeClickEH ) );
            m_matrixEventHandlers.Add( new ControlMatrix.ParameterizedOperationEventHandler( DotClickEH ) );
            m_matrixEventHandlers.Add( new ControlMatrix.ParameterizedOperationEventHandler( DotCrossProdMtxClickEH ) );
            m_matrixEventHandlers.Add( new ControlMatrix.ParameterizedOperationEventHandler( DotCrossProdTransposeClickEH ) );
            m_matrixEventHandlers.Add( new ControlMatrix.ParameterizedOperationEventHandler( DotFromLeftClickEH ) );
            m_matrixEventHandlers.Add( new ControlMatrix.ParameterizedOperationEventHandler( DotTransposeClickEH ) );
            m_eventHandlers.Add( new EventHandler( m_Widget.FastInverseClickEH ) );
            m_eventHandlers.Add( new EventHandler( m_Widget.Identity3x3ClickEH ) );
            m_eventHandlers.Add( new EventHandler( m_Widget.IdentityClickEH ) );
            m_matrixEventHandlers.Add( new ControlMatrix.ParameterizedOperationEventHandler( InterpolateClickEH ) );
            m_eventHandlers.Add( new EventHandler( m_Widget.Inverse3x3ClickEH ) );
            m_eventHandlers.Add( new EventHandler( m_Widget.InverseClickEH ) );
            m_matrixEventHandlers.Add( new ControlMatrix.ParameterizedOperationEventHandler( LookAtClickEH ) );
            m_matrixEventHandlers.Add( new ControlMatrix.ParameterizedOperationEventHandler( LookDownClickEH ) );
            m_matrixEventHandlers.Add( new ControlMatrix.ParameterizedOperationEventHandler( MakeDoubleCrossMatrixClickEH ) );
            m_matrixEventHandlers.Add( new ControlMatrix.ParameterizedOperationEventHandler( MakePosClickEH ) );
            m_matrixEventHandlers.Add( new ControlMatrix.ParameterizedOperationEventHandler( MakePosRotYClickEH ) );
            m_matrixEventHandlers.Add( new ControlMatrix.ParameterizedOperationEventHandler( MakeReflectClickEH ) );
            m_matrixEventHandlers.Add( new ControlMatrix.ParameterizedOperationEventHandler( MakeRotateClickEH ) );
            m_matrixEventHandlers.Add( new ControlMatrix.ParameterizedOperationEventHandler( MakeRotateToClickEH ) );
            m_matrixEventHandlers.Add( new ControlMatrix.ParameterizedOperationEventHandler( MakeRotateUnitAxisClickEH ) );
            m_matrixEventHandlers.Add( new ControlMatrix.ParameterizedOperationEventHandler( MakeRotateXClickEH ) );
            m_matrixEventHandlers.Add( new ControlMatrix.ParameterizedOperationEventHandler( MakeRotateYClickEH ) );
            m_matrixEventHandlers.Add( new ControlMatrix.ParameterizedOperationEventHandler( MakeRotateZClickEH ) );
            m_matrixEventHandlers.Add( new ControlMatrix.ParameterizedOperationEventHandler( MakeRotXClickEH ) );
            m_matrixEventHandlers.Add( new ControlMatrix.ParameterizedOperationEventHandler( MakeRotYClickEH ) );
            m_matrixEventHandlers.Add( new ControlMatrix.ParameterizedOperationEventHandler( MakeRotZClickEH ) );
            m_matrixEventHandlers.Add( new ControlMatrix.ParameterizedOperationEventHandler( MakeScaleClickEH ) );
            m_matrixEventHandlers.Add( new ControlMatrix.ParameterizedOperationEventHandler( MakeScaleFullClickEH ) );
            m_matrixEventHandlers.Add( new ControlMatrix.ParameterizedOperationEventHandler( MakeTranslateClickEH ) );
            m_eventHandlers.Add( new EventHandler( m_Widget.MakeUprightClickEH ) );
            m_matrixEventHandlers.Add( new ControlMatrix.ParameterizedOperationEventHandler( MirrorOnPlaneClickEH ) );
            m_eventHandlers.Add( new EventHandler( m_Widget.Negate3x3ClickEH ) );
            m_eventHandlers.Add( new EventHandler( m_Widget.NegateClickEH ) );
            m_eventHandlers.Add( new EventHandler( m_Widget.NormalizeClickEH ) );
            m_eventHandlers.Add( new EventHandler( m_Widget.NormalizeSafeClickEH ) );
            m_matrixEventHandlers.Add( new ControlMatrix.ParameterizedOperationEventHandler( OuterProductClickEH ) );
            m_matrixEventHandlers.Add( new ControlMatrix.ParameterizedOperationEventHandler( PolarViewClickEH ) );
            m_matrixEventHandlers.Add( new ControlMatrix.ParameterizedOperationEventHandler( RotateClickEH ) );
            m_matrixEventHandlers.Add( new ControlMatrix.ParameterizedOperationEventHandler( RotateFullClickEH ) );
            m_matrixEventHandlers.Add( new ControlMatrix.ParameterizedOperationEventHandler( RotateFullUnitAxisClickEH ) );
            m_matrixEventHandlers.Add( new ControlMatrix.ParameterizedOperationEventHandler( RotateFullXClickEH ) );
            m_matrixEventHandlers.Add( new ControlMatrix.ParameterizedOperationEventHandler( RotateFullYClickEH ) );
            m_matrixEventHandlers.Add( new ControlMatrix.ParameterizedOperationEventHandler( RotateFullZClickEH ) );
            m_matrixEventHandlers.Add( new ControlMatrix.ParameterizedOperationEventHandler( RotateLocalAxisClickEH ) );
            m_matrixEventHandlers.Add( new ControlMatrix.ParameterizedOperationEventHandler( RotateLocalXClickEH ) );
            m_matrixEventHandlers.Add( new ControlMatrix.ParameterizedOperationEventHandler( RotateLocalYClickEH ) );
            m_matrixEventHandlers.Add( new ControlMatrix.ParameterizedOperationEventHandler( RotateLocalZClickEH ) );
            m_matrixEventHandlers.Add( new ControlMatrix.ParameterizedOperationEventHandler( RotateToClickEH ) );
            m_matrixEventHandlers.Add( new ControlMatrix.ParameterizedOperationEventHandler( RotateUnitAxisClickEH ) );
            m_matrixEventHandlers.Add( new ControlMatrix.ParameterizedOperationEventHandler( RotateXClickEH ) );
            m_matrixEventHandlers.Add( new ControlMatrix.ParameterizedOperationEventHandler( RotateYClickEH ) );
            m_matrixEventHandlers.Add( new ControlMatrix.ParameterizedOperationEventHandler( RotateZClickEH ) );
            m_matrixEventHandlers.Add( new ControlMatrix.ParameterizedOperationEventHandler( ScaleClickEH ) );
            m_matrixEventHandlers.Add( new ControlMatrix.ParameterizedOperationEventHandler( ScaleFullClickEH ) );
            m_matrixEventHandlers.Add( new ControlMatrix.ParameterizedOperationEventHandler( SetDiagonalClickEH ) );
            m_matrixEventHandlers.Add( new ControlMatrix.ParameterizedOperationEventHandler( Subtract3x3ClickEH ) );
            m_matrixEventHandlers.Add( new ControlMatrix.ParameterizedOperationEventHandler( SubtractClickEH ) );
            m_matrixEventHandlers.Add( new ControlMatrix.ParameterizedOperationEventHandler( TranslateClickEH ) );
            m_eventHandlers.Add( new EventHandler( m_Widget.Transpose3x4ClickEH ) );
            m_eventHandlers.Add( new EventHandler( m_Widget.TransposeClickEH ) );
            m_eventHandlers.Add( new EventHandler( m_Widget.Zero3x3ClickEH ) );
            m_eventHandlers.Add( new EventHandler( m_Widget.ZeroClickEH ) );

            m_controlMatrix.ValueChanged += m_eventHandlers[0];
            m_controlMatrix.AbsClick += m_eventHandlers[1];
            m_controlMatrix.Add3x3Click += m_matrixEventHandlers[0];
            m_controlMatrix.AddClick += m_matrixEventHandlers[1];
            m_controlMatrix.CoordinateInverseSafeClick += m_eventHandlers[2];
            m_controlMatrix.CrossProductClick += m_matrixEventHandlers[2];
            m_controlMatrix.Dot3x3Click += m_matrixEventHandlers[3];
            m_controlMatrix.Dot3x3CrossProdMtxClick += m_matrixEventHandlers[4];
            m_controlMatrix.Dot3x3CrossProdTransposeClick += m_matrixEventHandlers[5];
            m_controlMatrix.Dot3x3FromLeftClick += m_matrixEventHandlers[6];
            m_controlMatrix.Dot3x3TransposeClick += m_matrixEventHandlers[7];
            m_controlMatrix.DotClick += m_matrixEventHandlers[8];
            m_controlMatrix.DotCrossProdMtxClick += m_matrixEventHandlers[9];
            m_controlMatrix.DotCrossProdTransposeClick += m_matrixEventHandlers[10];
            m_controlMatrix.DotFromLeftClick += m_matrixEventHandlers[11];
            m_controlMatrix.DotTransposeClick += m_matrixEventHandlers[12];
            m_controlMatrix.FastInverseClick += m_eventHandlers[3];
            m_controlMatrix.Identity3x3Click += m_eventHandlers[4];
            m_controlMatrix.IdentityClick += m_eventHandlers[5];
            m_controlMatrix.InterpolateClick += m_matrixEventHandlers[13];
            m_controlMatrix.Inverse3x3Click += m_eventHandlers[6];
            m_controlMatrix.InverseClick += m_eventHandlers[7];
            m_controlMatrix.LookAtClick += m_matrixEventHandlers[14];
            m_controlMatrix.LookDownClick += m_matrixEventHandlers[15];
            m_controlMatrix.MakeDoubleCrossMatrixClick += m_matrixEventHandlers[16];
            m_controlMatrix.MakePosClick += m_matrixEventHandlers[17];
            m_controlMatrix.MakePosRotYClick += m_matrixEventHandlers[18];
            m_controlMatrix.MakeReflectClick += m_matrixEventHandlers[19];
            m_controlMatrix.MakeRotateClick += m_matrixEventHandlers[20];
            m_controlMatrix.MakeRotateToClick += m_matrixEventHandlers[21];
            m_controlMatrix.MakeRotateUnitAxisClick += m_matrixEventHandlers[22];
            m_controlMatrix.MakeRotateXClick += m_matrixEventHandlers[23];
            m_controlMatrix.MakeRotateYClick += m_matrixEventHandlers[24];
            m_controlMatrix.MakeRotateZClick += m_matrixEventHandlers[25];
            m_controlMatrix.MakeRotXClick += m_matrixEventHandlers[26];
            m_controlMatrix.MakeRotYClick += m_matrixEventHandlers[27];
            m_controlMatrix.MakeRotZClick += m_matrixEventHandlers[28];
            m_controlMatrix.MakeScaleClick += m_matrixEventHandlers[29];
            m_controlMatrix.MakeScaleFullClick += m_matrixEventHandlers[30];
            m_controlMatrix.MakeTranslateClick += m_matrixEventHandlers[31];
            m_controlMatrix.MakeUprightClick += m_eventHandlers[8];
            m_controlMatrix.MirrorOnPlaneClick += m_matrixEventHandlers[32];
            m_controlMatrix.Negate3x3Click += m_eventHandlers[9];
            m_controlMatrix.NegateClick += m_eventHandlers[10];
            m_controlMatrix.NormalizeClick += m_eventHandlers[11];
            m_controlMatrix.NormalizeSafeClick += m_eventHandlers[12];
            m_controlMatrix.OuterProductClick += m_matrixEventHandlers[33];
            m_controlMatrix.PolarViewClick += m_matrixEventHandlers[34];
            m_controlMatrix.RotateClick += m_matrixEventHandlers[35];
            m_controlMatrix.RotateFullClick += m_matrixEventHandlers[36];
            m_controlMatrix.RotateFullUnitAxisClick += m_matrixEventHandlers[37];
            m_controlMatrix.RotateFullXClick += m_matrixEventHandlers[38];
            m_controlMatrix.RotateFullYClick += m_matrixEventHandlers[39];
            m_controlMatrix.RotateFullZClick += m_matrixEventHandlers[40];
            m_controlMatrix.RotateLocalAxisClick += m_matrixEventHandlers[41];
            m_controlMatrix.RotateLocalXClick += m_matrixEventHandlers[42];
            m_controlMatrix.RotateLocalYClick += m_matrixEventHandlers[43];
            m_controlMatrix.RotateLocalZClick += m_matrixEventHandlers[44];
            m_controlMatrix.RotateToClick += m_matrixEventHandlers[45];
            m_controlMatrix.RotateUnitAxisClick += m_matrixEventHandlers[46];
            m_controlMatrix.RotateXClick += m_matrixEventHandlers[47];
            m_controlMatrix.RotateYClick += m_matrixEventHandlers[48];
            m_controlMatrix.RotateZClick += m_matrixEventHandlers[49];
            m_controlMatrix.ScaleClick += m_matrixEventHandlers[50];
            m_controlMatrix.ScaleFullClick += m_matrixEventHandlers[51];
            m_controlMatrix.SetDiagonalClick += m_matrixEventHandlers[52];
            m_controlMatrix.Subtract3x3Click += m_matrixEventHandlers[53];
            m_controlMatrix.SubtractClick += m_matrixEventHandlers[54];
            m_controlMatrix.TranslateClick += m_matrixEventHandlers[55];
            m_controlMatrix.Transpose3x4Click += m_eventHandlers[13];
            m_controlMatrix.TransposeClick += m_eventHandlers[14];
            m_controlMatrix.Zero3x3Click += m_eventHandlers[15];
            m_controlMatrix.ZeroClick += m_eventHandlers[16];
        }

        private void RemoveEventHandlers()
        {
            m_controlMatrix.ValueChanged -= m_eventHandlers[0];
            m_controlMatrix.AbsClick -= m_eventHandlers[1];
            m_controlMatrix.Add3x3Click -= m_matrixEventHandlers[0];
            m_controlMatrix.AddClick -= m_matrixEventHandlers[1];
            m_controlMatrix.CoordinateInverseSafeClick -= m_eventHandlers[2];
            m_controlMatrix.CrossProductClick -= m_matrixEventHandlers[2];
            m_controlMatrix.Dot3x3Click -= m_matrixEventHandlers[3];
            m_controlMatrix.Dot3x3CrossProdMtxClick -= m_matrixEventHandlers[4];
            m_controlMatrix.Dot3x3CrossProdTransposeClick -= m_matrixEventHandlers[5];
            m_controlMatrix.Dot3x3FromLeftClick -= m_matrixEventHandlers[6];
            m_controlMatrix.Dot3x3TransposeClick -= m_matrixEventHandlers[7];
            m_controlMatrix.DotClick -= m_matrixEventHandlers[8];
            m_controlMatrix.DotCrossProdMtxClick -= m_matrixEventHandlers[9];
            m_controlMatrix.DotCrossProdTransposeClick -= m_matrixEventHandlers[10];
            m_controlMatrix.DotFromLeftClick -= m_matrixEventHandlers[11];
            m_controlMatrix.DotTransposeClick -= m_matrixEventHandlers[12];
            m_controlMatrix.FastInverseClick -= m_eventHandlers[3];
            m_controlMatrix.Identity3x3Click -= m_eventHandlers[4];
            m_controlMatrix.IdentityClick -= m_eventHandlers[5];
            m_controlMatrix.InterpolateClick -= m_matrixEventHandlers[13];
            m_controlMatrix.Inverse3x3Click -= m_eventHandlers[6];
            m_controlMatrix.InverseClick -= m_eventHandlers[7];
            m_controlMatrix.LookAtClick -= m_matrixEventHandlers[14];
            m_controlMatrix.LookDownClick -= m_matrixEventHandlers[15];
            m_controlMatrix.MakeDoubleCrossMatrixClick -= m_matrixEventHandlers[16];
            m_controlMatrix.MakePosClick -= m_matrixEventHandlers[17];
            m_controlMatrix.MakePosRotYClick -= m_matrixEventHandlers[18];
            m_controlMatrix.MakeReflectClick -= m_matrixEventHandlers[19];
            m_controlMatrix.MakeRotateClick -= m_matrixEventHandlers[20];
            m_controlMatrix.MakeRotateToClick -= m_matrixEventHandlers[21];
            m_controlMatrix.MakeRotateUnitAxisClick -= m_matrixEventHandlers[22];
            m_controlMatrix.MakeRotateXClick -= m_matrixEventHandlers[23];
            m_controlMatrix.MakeRotateYClick -= m_matrixEventHandlers[24];
            m_controlMatrix.MakeRotateZClick -= m_matrixEventHandlers[25];
            m_controlMatrix.MakeRotXClick -= m_matrixEventHandlers[26];
            m_controlMatrix.MakeRotYClick -= m_matrixEventHandlers[27];
            m_controlMatrix.MakeRotZClick -= m_matrixEventHandlers[28];
            m_controlMatrix.MakeScaleClick -= m_matrixEventHandlers[29];
            m_controlMatrix.MakeScaleFullClick -= m_matrixEventHandlers[30];
            m_controlMatrix.MakeTranslateClick -= m_matrixEventHandlers[31];
            m_controlMatrix.MakeUprightClick -= m_eventHandlers[8];
            m_controlMatrix.MirrorOnPlaneClick -= m_matrixEventHandlers[32];
            m_controlMatrix.Negate3x3Click -= m_eventHandlers[9];
            m_controlMatrix.NegateClick -= m_eventHandlers[10];
            m_controlMatrix.NormalizeClick -= m_eventHandlers[11];
            m_controlMatrix.NormalizeSafeClick -= m_eventHandlers[12];
            m_controlMatrix.OuterProductClick -= m_matrixEventHandlers[33];
            m_controlMatrix.PolarViewClick -= m_matrixEventHandlers[34];
            m_controlMatrix.RotateClick -= m_matrixEventHandlers[35];
            m_controlMatrix.RotateFullClick -= m_matrixEventHandlers[36];
            m_controlMatrix.RotateFullUnitAxisClick -= m_matrixEventHandlers[37];
            m_controlMatrix.RotateFullXClick -= m_matrixEventHandlers[38];
            m_controlMatrix.RotateFullYClick -= m_matrixEventHandlers[39];
            m_controlMatrix.RotateFullZClick -= m_matrixEventHandlers[40];
            m_controlMatrix.RotateLocalAxisClick -= m_matrixEventHandlers[41];
            m_controlMatrix.RotateLocalXClick -= m_matrixEventHandlers[42];
            m_controlMatrix.RotateLocalYClick -= m_matrixEventHandlers[43];
            m_controlMatrix.RotateLocalZClick -= m_matrixEventHandlers[44];
            m_controlMatrix.RotateToClick -= m_matrixEventHandlers[45];
            m_controlMatrix.RotateUnitAxisClick -= m_matrixEventHandlers[46];
            m_controlMatrix.RotateXClick -= m_matrixEventHandlers[47];
            m_controlMatrix.RotateYClick -= m_matrixEventHandlers[48];
            m_controlMatrix.RotateZClick -= m_matrixEventHandlers[49];
            m_controlMatrix.ScaleClick -= m_matrixEventHandlers[50];
            m_controlMatrix.ScaleFullClick -= m_matrixEventHandlers[51];
            m_controlMatrix.SetDiagonalClick -= m_matrixEventHandlers[52];
            m_controlMatrix.Subtract3x3Click -= m_matrixEventHandlers[53];
            m_controlMatrix.SubtractClick -= m_matrixEventHandlers[54];
            m_controlMatrix.TranslateClick -= m_matrixEventHandlers[55];
            m_controlMatrix.Transpose3x4Click -= m_eventHandlers[13];
            m_controlMatrix.TransposeClick -= m_eventHandlers[14];
            m_controlMatrix.Zero3x3Click -= m_eventHandlers[15];
            m_controlMatrix.ZeroClick -= m_eventHandlers[16];

            m_eventHandlers.Clear();
            m_matrixEventHandlers.Clear();
        }
        #endregion

        #region EventHandlers


        public void TranslateClickEH( object sender, MatrixOperationParameterEventArgs e )
        {
            m_Widget.UpdateRemoteOperation(
                BankRemotePacket.EnumPacketType.USER_4,
                WidgetMatrix.Operation.Translate,
                e.Vector1 );
        }

        public void SubtractClickEH( object sender, MatrixOperationParameterEventArgs e )
        {
            m_Widget.UpdateRemoteOperation(
                BankRemotePacket.EnumPacketType.USER_9,
                WidgetMatrix.Operation.Subtract,
                e.Matrix );
        }

        public void Subtract3x3ClickEH( object sender, MatrixOperationParameterEventArgs e )
        {
            m_Widget.UpdateRemoteOperation(
                BankRemotePacket.EnumPacketType.USER_9,
                WidgetMatrix.Operation.Subtract3x3,
                e.Matrix );
        }

        public void SetDiagonalClickEH( object sender, MatrixOperationParameterEventArgs e )
        {
            m_Widget.UpdateRemoteOperation(
                BankRemotePacket.EnumPacketType.USER_4,
                WidgetMatrix.Operation.SetDiagonal,
                e.Vector1 );
        }



        public void ScaleFullClickEH( object sender, MatrixOperationParameterEventArgs e )
        {
            m_Widget.UpdateRemoteOperation(
                BankRemotePacket.EnumPacketType.USER_1,
                WidgetMatrix.Operation.Scale,
                e.FloatValue1 );
        }

        public void ScaleClickEH( object sender, MatrixOperationParameterEventArgs e )
        {
            m_Widget.UpdateRemoteOperation(
                BankRemotePacket.EnumPacketType.USER_1,
                WidgetMatrix.Operation.ScaleFull,
                e.FloatValue1 );
        }

        public void RotateZClickEH( object sender, MatrixOperationParameterEventArgs e )
        {
            m_Widget.UpdateRemoteOperation(
                BankRemotePacket.EnumPacketType.USER_1,
                WidgetMatrix.Operation.RotateZ,
                e.FloatValue1 );
        }

        public void RotateYClickEH( object sender, MatrixOperationParameterEventArgs e )
        {
            m_Widget.UpdateRemoteOperation(
                BankRemotePacket.EnumPacketType.USER_1,
                WidgetMatrix.Operation.RotateY,
                e.FloatValue1 );
        }

        public void RotateXClickEH( object sender, MatrixOperationParameterEventArgs e )
        {
            m_Widget.UpdateRemoteOperation(
                BankRemotePacket.EnumPacketType.USER_1,
                WidgetMatrix.Operation.RotateX,
                e.FloatValue1 );
        }


        public void RotateUnitAxisClickEH( object sender, MatrixOperationParameterEventArgs e )
        {
            m_Widget.UpdateRemoteOperation(
                BankRemotePacket.EnumPacketType.USER_7,
                WidgetMatrix.Operation.RotateUnitAxis,
                e.Vector1,
                e.FloatValue1 );
        }

        public void RotateToClickEH( object sender, MatrixOperationParameterEventArgs e )
        {
            m_Widget.UpdateRemoteOperation(
                BankRemotePacket.EnumPacketType.USER_6,
                WidgetMatrix.Operation.RotateTo,
                e.Vector1,
                e.Vector2,
                e.FloatValue1 );
        }

        public void RotateLocalZClickEH( object sender, MatrixOperationParameterEventArgs e )
        {
            m_Widget.UpdateRemoteOperation(
                BankRemotePacket.EnumPacketType.USER_1,
                WidgetMatrix.Operation.RotateLocalZ,
                e.FloatValue1 );
        }

        public void RotateLocalYClickEH( object sender, MatrixOperationParameterEventArgs e )
        {
            m_Widget.UpdateRemoteOperation(
                BankRemotePacket.EnumPacketType.USER_1,
                WidgetMatrix.Operation.RotateLocalY,
                e.FloatValue1 );
        }

        public void RotateLocalXClickEH( object sender, MatrixOperationParameterEventArgs e )
        {
            m_Widget.UpdateRemoteOperation(
                BankRemotePacket.EnumPacketType.USER_1,
                WidgetMatrix.Operation.RotateLocalX,
                e.FloatValue1 );
        }

        public void RotateLocalAxisClickEH( object sender, MatrixOperationParameterEventArgs e )
        {
            m_Widget.UpdateRemoteOperation(
                BankRemotePacket.EnumPacketType.USER_11,
                WidgetMatrix.Operation.RotateLocalAxis,
                e.FloatValue1,
                e.IntValue);
        }

        public void RotateFullZClickEH( object sender, MatrixOperationParameterEventArgs e )
        {
            m_Widget.UpdateRemoteOperation(
                BankRemotePacket.EnumPacketType.USER_1,
                WidgetMatrix.Operation.RotateFullZ,
                e.FloatValue1 );
        }

        public void RotateFullYClickEH( object sender, MatrixOperationParameterEventArgs e )
        {
            m_Widget.UpdateRemoteOperation(
                BankRemotePacket.EnumPacketType.USER_1,
                WidgetMatrix.Operation.RotateFullY,
                e.FloatValue1 );
        }

        public void RotateFullXClickEH( object sender, MatrixOperationParameterEventArgs e )
        {
            m_Widget.UpdateRemoteOperation(
                BankRemotePacket.EnumPacketType.USER_1,
                WidgetMatrix.Operation.RotateFullX,
                e.FloatValue1 );
        }


        public void RotateFullUnitAxisClickEH( object sender, MatrixOperationParameterEventArgs e )
        {
            m_Widget.UpdateRemoteOperation(
                BankRemotePacket.EnumPacketType.USER_7,
                WidgetMatrix.Operation.RotateFullUnitAxis,
                e.Vector1,
                e.FloatValue1 );
        }

        public void RotateFullClickEH( object sender, MatrixOperationParameterEventArgs e )
        {
            m_Widget.UpdateRemoteOperation(
                BankRemotePacket.EnumPacketType.USER_7,
                WidgetMatrix.Operation.RotateFull,
                e.Vector1,
                e.FloatValue1 );
        }

        public void RotateClickEH( object sender, MatrixOperationParameterEventArgs e )
        {
            m_Widget.UpdateRemoteOperation(
                BankRemotePacket.EnumPacketType.USER_7,
                WidgetMatrix.Operation.Rotate,
                e.Vector1,
                e.FloatValue1 );
        }



        public void PolarViewClickEH( object sender, MatrixOperationParameterEventArgs e )
        {
            m_Widget.UpdateRemoteOperation(
                BankRemotePacket.EnumPacketType.USER_3,
                WidgetMatrix.Operation.PolarView,
                e.FloatValue1,
                e.FloatValue2,
                e.FloatValue3,
                e.FloatValue4);
        }

        public void OuterProductClickEH( object sender, MatrixOperationParameterEventArgs e )
        {
            m_Widget.UpdateRemoteOperation(
                BankRemotePacket.EnumPacketType.USER_5,
                WidgetMatrix.Operation.OuterProduct,
                e.Vector1,
                e.Vector2 );
        }


        public void MirrorOnPlaneClickEH( object sender, MatrixOperationParameterEventArgs e )
        {
            m_Widget.UpdateRemoteOperation(
                BankRemotePacket.EnumPacketType.USER_4,
                WidgetMatrix.Operation.MirrorOnPlane,
                e.Vector1[0],
                e.Vector1[1],
                e.Vector1[2],
                e.Vector1[3]);
        }


        public void MakeTranslateClickEH( object sender, MatrixOperationParameterEventArgs e )
        {
            m_Widget.UpdateRemoteOperation(
                BankRemotePacket.EnumPacketType.USER_4,
                WidgetMatrix.Operation.MakeTranslate,
                e.Vector1);
        }

        public void MakeScaleFullClickEH( object sender, MatrixOperationParameterEventArgs e )
        {
            m_Widget.UpdateRemoteOperation(
                BankRemotePacket.EnumPacketType.USER_1,
                WidgetMatrix.Operation.MakeScaleFull,
                e.FloatValue1 );
        }

        public void MakeScaleClickEH( object sender, MatrixOperationParameterEventArgs e )
        {
            m_Widget.UpdateRemoteOperation(
                BankRemotePacket.EnumPacketType.USER_1,
                WidgetMatrix.Operation.MakeScale,
                e.FloatValue1 );
        }

        public void MakeRotZClickEH( object sender, MatrixOperationParameterEventArgs e )
        {
            m_Widget.UpdateRemoteOperation(
                BankRemotePacket.EnumPacketType.USER_1,
                WidgetMatrix.Operation.MakeRotZ,
                e.FloatValue1 );
        }

        public void MakeRotYClickEH( object sender, MatrixOperationParameterEventArgs e )
        {
            m_Widget.UpdateRemoteOperation(
                BankRemotePacket.EnumPacketType.USER_1,
                WidgetMatrix.Operation.MakeRotY,
                e.FloatValue1 );
        }

        public void MakeRotXClickEH( object sender, MatrixOperationParameterEventArgs e )
        {
            m_Widget.UpdateRemoteOperation(
                BankRemotePacket.EnumPacketType.USER_1,
                WidgetMatrix.Operation.MakeRotX,
                e.FloatValue1 );
        }

        public void MakeRotateZClickEH( object sender, MatrixOperationParameterEventArgs e )
        {
            m_Widget.UpdateRemoteOperation(
                BankRemotePacket.EnumPacketType.USER_1,
                WidgetMatrix.Operation.MakeRotateZ,
                e.FloatValue1 );
        }

        public void MakeRotateYClickEH( object sender, MatrixOperationParameterEventArgs e )
        {
            m_Widget.UpdateRemoteOperation(
                BankRemotePacket.EnumPacketType.USER_1,
                WidgetMatrix.Operation.MakeRotateY,
                e.FloatValue1 );
        }

        public void MakeRotateXClickEH( object sender, MatrixOperationParameterEventArgs e )
        {
            m_Widget.UpdateRemoteOperation(
                BankRemotePacket.EnumPacketType.USER_1,
                WidgetMatrix.Operation.MakeRotateX,
                e.FloatValue1 );
        }





        public void MakeRotateUnitAxisClickEH( object sender, MatrixOperationParameterEventArgs e )
        {
            m_Widget.UpdateRemoteOperation(
                BankRemotePacket.EnumPacketType.USER_7,
                WidgetMatrix.Operation.MakeRotateUnitAxis,
                e.Vector1,
                e.FloatValue1);
        }

        public void MakeRotateToClickEH( object sender, MatrixOperationParameterEventArgs e )
        {
            m_Widget.UpdateRemoteOperation(
                BankRemotePacket.EnumPacketType.USER_5,
                WidgetMatrix.Operation.MakeRotateTo,
                e.Vector1,
                e.Vector2);
        }

        public void MakeRotateClickEH( object sender, MatrixOperationParameterEventArgs e )
        {
            m_Widget.UpdateRemoteOperation(
                BankRemotePacket.EnumPacketType.USER_7,
                WidgetMatrix.Operation.MakeRotate,
                e.Vector1,
                e.FloatValue1);
        }

        public void MakeReflectClickEH( object sender, MatrixOperationParameterEventArgs e )
        {
            m_Widget.UpdateRemoteOperation(
                BankRemotePacket.EnumPacketType.USER_9,
                WidgetMatrix.Operation.MakeReflect,
                e.Vector1 );
        }

        public void MakePosRotYClickEH( object sender, MatrixOperationParameterEventArgs e )
        {
            m_Widget.UpdateRemoteOperation(
                BankRemotePacket.EnumPacketType.USER_8,
                WidgetMatrix.Operation.MakePosRotY,
                e.Vector1[0],
                e.Vector1[1],
                e.Vector1[2],
                e.FloatValue1,
                e.FloatValue2 );
        }

        public void MakePosClickEH( object sender, MatrixOperationParameterEventArgs e )
        {
            m_Widget.UpdateRemoteOperation(
                BankRemotePacket.EnumPacketType.USER_2,
                WidgetMatrix.Operation.MakePos,
                e.FloatValue1,
                e.FloatValue2,
                e.FloatValue3);
        }

        public void MakeDoubleCrossMatrixClickEH( object sender, MatrixOperationParameterEventArgs e )
        {
            m_Widget.UpdateRemoteOperation(
                BankRemotePacket.EnumPacketType.USER_5,
                WidgetMatrix.Operation.MakeDoubleCrossMatrix,
                e.Vector1,
                e.Vector2 );
        }

        public void LookDownClickEH( object sender, MatrixOperationParameterEventArgs e )
        {
            m_Widget.UpdateRemoteOperation(
                BankRemotePacket.EnumPacketType.USER_5,
                WidgetMatrix.Operation.LookDown,
                e.Vector1,
                e.Vector2 );
        }

        public void LookAtClickEH( object sender, MatrixOperationParameterEventArgs e )
        {
            m_Widget.UpdateRemoteOperation(
                BankRemotePacket.EnumPacketType.USER_5,
                WidgetMatrix.Operation.LookAt,
                e.Vector1,
                e.Vector2);
        }



        public void InterpolateClickEH( object sender, MatrixOperationParameterEventArgs e )
        {
            m_Widget.UpdateRemoteOperation(
                BankRemotePacket.EnumPacketType.USER_10,
                WidgetMatrix.Operation.Interpolate,
                e.Matrix,
                e.FloatValue1 );
        }



        public void DotTransposeClickEH( object sender, MatrixOperationParameterEventArgs e )
        {
            m_Widget.UpdateRemoteOperation(
                BankRemotePacket.EnumPacketType.USER_9,
                WidgetMatrix.Operation.DotTranspose,
                e.Matrix );
        }

        public void DotFromLeftClickEH( object sender, MatrixOperationParameterEventArgs e )
        {
            m_Widget.UpdateRemoteOperation(
                BankRemotePacket.EnumPacketType.USER_9,
                WidgetMatrix.Operation.DotFromLeft,
                e.Matrix );
        }

        public void DotCrossProdTransposeClickEH( object sender, MatrixOperationParameterEventArgs e )
        {
            m_Widget.UpdateRemoteOperation(
                BankRemotePacket.EnumPacketType.USER_4,
                WidgetMatrix.Operation.DotCrossProdTranspose,
                e.Vector1 );
        }

        public void DotCrossProdMtxClickEH( object sender, MatrixOperationParameterEventArgs e )
        {
            m_Widget.UpdateRemoteOperation(
                BankRemotePacket.EnumPacketType.USER_4,
                WidgetMatrix.Operation.DotCrossProdMtx,
                e.Vector1 );
        }

        public void DotClickEH( object sender, MatrixOperationParameterEventArgs e )
        {
            m_Widget.UpdateRemoteOperation(
                BankRemotePacket.EnumPacketType.USER_9,
                WidgetMatrix.Operation.Dot,
                e.Matrix );
        }

        public void Dot3x3TransposeClickEH( object sender, MatrixOperationParameterEventArgs e )
        {
            m_Widget.UpdateRemoteOperation(
                BankRemotePacket.EnumPacketType.USER_9,
                WidgetMatrix.Operation.Dot3x3Transpose,
                e.Matrix );
        }

        public void Dot3x3FromLeftClickEH( object sender, MatrixOperationParameterEventArgs e )
        {
            m_Widget.UpdateRemoteOperation(
                BankRemotePacket.EnumPacketType.USER_9,
                WidgetMatrix.Operation.Dot3x3FromLeft,
                e.Matrix );
        }



        public void Dot3x3CrossProdTransposeClickEH( object sender, MatrixOperationParameterEventArgs e )
        {
            m_Widget.UpdateRemoteOperation(
                BankRemotePacket.EnumPacketType.USER_4,
                WidgetMatrix.Operation.Dot3x3CrossProdTranspose,
                e.Vector1 );
        }

        public void Dot3x3CrossProdMtxClickEH( object sender, MatrixOperationParameterEventArgs e )
        {
            m_Widget.UpdateRemoteOperation(
                BankRemotePacket.EnumPacketType.USER_4,
                WidgetMatrix.Operation.Dot3x3CrossProdMtx,
                e.Vector1 );
        }

        public void Dot3x3ClickEH( object sender, MatrixOperationParameterEventArgs e )
        {
            m_Widget.UpdateRemoteOperationDot3x3(
                BankRemotePacket.EnumPacketType.USER_9,
                WidgetMatrix.Operation.Dot3x3,
                e.Matrix );
        }

        public void CrossProductClickEH( object sender, MatrixOperationParameterEventArgs e )
        {
            m_Widget.UpdateRemoteOperation(
                BankRemotePacket.EnumPacketType.USER_4,
                WidgetMatrix.Operation.CrossProduct,
                e.Vector1 );
        }


        public void AddClickEH( object sender, MatrixOperationParameterEventArgs e )
        {
            m_Widget.UpdateRemoteOperation(
                BankRemotePacket.EnumPacketType.USER_9,
                WidgetMatrix.Operation.Add,
                e.Matrix );
        }

        public void Add3x3ClickEH( object sender, MatrixOperationParameterEventArgs e )
        {
            m_Widget.UpdateRemoteOperation(
                BankRemotePacket.EnumPacketType.USER_9,
                WidgetMatrix.Operation.Add3x3,
                e.Matrix );
        }
        #endregion
    }
}
