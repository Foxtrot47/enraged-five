﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ragCore;

namespace ragWidgets
{
    public class ViewUtils
    {
        public static void SetExpandedGroup( IWidgetView widgetView, WidgetGroupBase group, bool expanded )
        {
            Dictionary<string, bool> expandedGroups;
            if ( widgetView != null )
            {
                if ( !WidgetGroup.sm_expandedGroups.TryGetValue( widgetView.Guid.ToString(), out expandedGroups ) )
                {
                    expandedGroups = new Dictionary<string, bool>();
                    WidgetGroup.sm_expandedGroups.Add( widgetView.Guid.ToString(), expandedGroups );
                }


                string path = string.Empty;
                if ( group != null )
                {
                    path = group.FindPath();

                    // Don't keep track of groups that have the same names as their siblings.
                    if (group.Parent is WidgetGroup)
                    {
                        WidgetGroup parentGroup = (WidgetGroup)group.Parent;

                        if (parentGroup.WidgetList.Count(item => item.Title == group.Title) > 1)
                        {
                            expanded = false;
                        }
                    }
                }

                expandedGroups[path] = expanded;
            }
        }

        public static bool IsGroupExpanded( IWidgetView widgetView, WidgetGroupBase group )
        {
            Dictionary<string, bool> expandedGroups;
            if ( widgetView == null )
            {
                return true;
            }
            if ( WidgetGroup.sm_expandedGroups.TryGetValue( widgetView.Guid.ToString(), out expandedGroups ) )
            {
                string path = string.Empty;
                if ( group != null )
                {
                    path = group.FindPath();
                }

                bool expanded;
                if ( expandedGroups.TryGetValue( path, out expanded ) )
                {
                    return expanded;
                }
            }

            return false;
        }


        public static void SetBankHidden( IWidgetView widgetView, WidgetBank widgetBank, bool hidden )
        {
            if ( widgetBank != null )
            {
                Dictionary<string, bool> hiddenBanks;
                if ( !WidgetGroup.sm_hiddenBanks.TryGetValue( widgetView.Guid.ToString(), out hiddenBanks ) )
                {
                    hiddenBanks = new Dictionary<string, bool>();
                    WidgetGroup.sm_hiddenBanks.Add( widgetView.Guid.ToString(), hiddenBanks );
                }

                hiddenBanks[widgetBank.Title] = hidden;
            }
        }

        public static bool IsBankHidden( IWidgetView widgetView, WidgetBank widgetBank )
        {
            if ( widgetBank != null )
            {
                Dictionary<string, bool> hiddenBanks;
                if ( WidgetGroup.sm_hiddenBanks.TryGetValue( widgetView.Guid.ToString(), out hiddenBanks ) )
                {
                    bool hidden;
                    if ( hiddenBanks.TryGetValue( widgetBank.Title, out hidden ) )
                    {
                        return hidden;
                    }
                }
            }

            return false;
        }
    }
}
