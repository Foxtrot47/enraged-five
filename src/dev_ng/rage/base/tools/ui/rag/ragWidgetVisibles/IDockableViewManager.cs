using System;
using RSG.Base.Logging;

namespace ragWidgets
{
	/// <summary>
	/// Not used.
	/// </summary>
    /// 
    public class DockableViewSettings
    {
        public bool m_DockLeft;
        public bool m_DockTop;
        public bool m_DockRight;
        public bool m_DockBottom;
        public bool m_DockCenter;
    };

    /// <summary>
    /// Interface for a Manager that can add DockControls to a SandDockManager
    /// </summary>
	public interface IDockableViewManager
	{
        /// <summary>
        /// Should initialize the DockControl and adds the IDockableView to it
        /// </summary>
        /// <param name="dockControl"></param>
        /// <param name="dockableView"></param>
        /// <returns></returns>
        IDockableView CreateView(TD.SandDock.DockControl dockControl, IDockableView dockableView, ILog log);

        /// <summary>
        /// Should opens the DockControl using the given parameters
        /// </summary>
        /// <param name="dockControl"></param>
        /// <param name="initiallyFloating">typically IDockableView.InitiallyFloating</param>
        /// <param name="dockLocation">typically IDockableView.InitialDockLocation</param>
        /// <returns>true if the DockControl was opened</returns>
        bool OpenView( TD.SandDock.DockControl dockControl,
            bool initiallyFloating, TD.SandDock.ContainerDockLocation dockLocation, ILog log );

        /// <summary>
        /// Should perform any additional steps after the DockControl has been opened
        /// </summary>
        /// <param name="dockControl"></param>
        /// <param name="dockableView"></param>
        void PostOpenView( TD.SandDock.DockControl dockControl, IDockableView dockableView );

        /// <summary>
        /// Should find or create the DockControl for the given guid
        /// </summary>
        /// <param name="guid"></param>
        /// <returns>null if DockControl could not be created</returns>
        TD.SandDock.DockControl ResolveDockControl(String guid, ILog log);
	}
}
