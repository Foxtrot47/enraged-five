using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

using ragCore;
using ragUi;

namespace ragWidgets
{
    public class WidgetDataVisible : WidgetControlBaseVisible
    {
        public WidgetDataVisible(IWidgetView widgetView, WidgetData widget, Control parent, ToolTip tooltip, ushort maxLength)
            : base( widgetView, parent, widget )
		{
            m_widget = widget;

            m_dataReceivedEH = new WidgetData.DataChangedEventHandler( widget_DataReceived );
            m_allDataReceivedEH = new EventHandler( widget_AllDataReceived );
            m_dataSentEH = new WidgetData.DataChangedEventHandler( widget_DataSent );

            m_widget.DataReceived += m_dataReceivedEH;
            m_widget.AllDataReceived += m_allDataReceivedEH;
            m_widget.DataSent += m_dataSentEH;

            m_dataViewControl = new ControlDataView();
            m_dataViewControl.Text = m_widget.Title;
            
            if ( maxLength != 0 )
            {
                m_dataViewControl.MaxLength = maxLength;
            }

            m_dataViewControl.SendButtonClick += new EventHandler( this.dataViewControl_SendButtonClick );
            m_dataViewControl.IsReadOnly = widget.IsReadOnly;
            FinishCreation(tooltip);

            // this will initialize the rich text box
            widget_AllDataReceived( m_widget, EventArgs.Empty );
		}

        #region Variables
        private WidgetData m_widget;
        private ControlDataView m_dataViewControl;

        private WidgetData.DataChangedEventHandler m_dataReceivedEH;
        private EventHandler m_allDataReceivedEH;
        private WidgetData.DataChangedEventHandler m_dataSentEH;
        #endregion

        #region Overrides
        public override Widget Widget
        {
            get
            {
                return m_widget;
            }
        }

        public override ragUi.ControlBase ControlBase
        {
            get
            {
                return m_dataViewControl;
            }
        }

        public override void Remove()
        {
            base.Remove();

            m_widget.DataReceived -= m_dataReceivedEH;
            m_widget.AllDataReceived -= m_allDataReceivedEH;
            m_widget.DataSent -= m_dataSentEH;

            m_dataReceivedEH = null;
            m_allDataReceivedEH = null;
            m_dataSentEH = null;
        }

        public override void Destroy()
        {
            base.Destroy();
            m_widget = null;
            m_dataViewControl.Dispose();
            m_dataViewControl = null;
        }
        #endregion

        #region Event Handlers
        private void dataViewControl_SendButtonClick( object sender, EventArgs e )
        {
            string text = m_dataViewControl.TheText;

            bool allocatedData = false;
            if ( (m_widget.Data == null) || ((m_widget.MaxLength == 0) && (m_widget.Data.Length < text.Length + 1)) )
            {
                // allocate or resize
                if ( m_widget.MaxLength == 0 )
                {
                    m_widget.Data = new byte[text.Length + 1];
                }
                else
                {
                    m_widget.Data = new byte[m_widget.MaxLength];
                }

                allocatedData = true;
            }

            for ( int i = 0; (i < m_widget.Data.Length) && (i < text.Length); ++i )
            {
                char c = text[i];
                try
                {
                    m_widget.Data[i] = Convert.ToByte( c );
                }
                catch
                {
                    m_widget.Data[i] = Convert.ToByte( ' ' );
                }
            }

            for ( int i = text.Length; i < m_widget.Data.Length; ++i )
            {
                m_widget.Data[i] = 0;
            }

            m_widget.RemoteUpdate( allocatedData );
        }

        private void widget_DataReceived( object sender, WidgetData.WidgetDataChangedEventArgs e )
        {
            WidgetData widget = sender as WidgetData;
            if ( e.AllocatedData )
            {
                m_dataViewControl.TheText = string.Empty;

                if ( widget.MaxLength != 0 )
                {
                    m_dataViewControl.MaxLength = widget.MaxLength;
                }
            }
        }

        private void widget_AllDataReceived( object sender, EventArgs e )
        {
            WidgetData widget = sender as WidgetData;
            if ( widget.Data != null )
            {
                char[] chars = new char[widget.Data.Length];
                for ( int i = 0; i < widget.Data.Length; ++i )
                {
                    chars[i] = Convert.ToChar( widget.Data[i] );
                }

                m_dataViewControl.TheText = new string( chars );
            }
            else
            {
                m_dataViewControl.TheText = string.Empty;
            }
        }

        private void widget_DataSent( object sender, WidgetData.WidgetDataChangedEventArgs e )
        {
            // received by other visibles tied to the same widget so that when one gets updated
            // by the GUI, we all get updated.
            widget_AllDataReceived( sender, e );
        }
        #endregion
    }
}
