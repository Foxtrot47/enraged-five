using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

using ragUi;
using ragCore;
using ragWidgets;

namespace ragWidgets
{
    public class WidgetVCRVisible : WidgetControlBaseVisible
    {
        public WidgetVCRVisible(IWidgetView widgetView, WidgetVCR widget, Control parent, ToolTip tooltip)
            : base( widgetView, parent, widget )
        {
            m_widget = widget;

            m_controlVCR.IsReadOnly = widget.IsReadOnly;
            m_controlVCR.VCR.FastForwardOrGoToEndButtonClick += new EventHandler( VCR_FastForwardOrGoToEndButtonClick );
            m_controlVCR.VCR.GoToNextOrStepForwardButtonClick += new EventHandler( VCR_GoToNextOrStepForwardButtonClick );
            m_controlVCR.VCR.GoToPreviousOrStepBackwardButtonClick += new EventHandler( VCR_GoToPreviousOrStepBackwardButtonClick );
            m_controlVCR.VCR.PauseButtonClick += new EventHandler( VCR_PauseButtonClick );
            m_controlVCR.VCR.PlayBackwardsButtonClick += new EventHandler( VCR_PlayBackwardsButtonClick );
            m_controlVCR.VCR.PlayForwardsButtonClick += new EventHandler( VCR_PlayForwardsButtonClick );
            m_controlVCR.VCR.RewindOrGoToStartButtonClick += new EventHandler( VCR_RewindOrGoToStartButtonClick );

            Refresh();

            FinishCreation(tooltip);
        }

        protected override void widget_ValueChanged( object sender, EventArgs e )
        {
            WidgetVCR widget = (WidgetVCR)sender;
            Refresh();
        }

        #region Variables
        private ControlVCR m_controlVCR = new ControlVCR();
        private WidgetVCR m_widget;
        #endregion

        #region Overrides
        public override ControlBase ControlBase
        {
            get
            {
                return m_controlVCR;
            }
        }

        public override Widget Widget
        {
            get
            {
                return m_widget;
            }
        }
        #endregion

        #region Event Handlers
        private void VCR_FastForwardOrGoToEndButtonClick( object sender, EventArgs e )
        {
            m_widget.FastForwardOrGoToEnd();
        }

        private void VCR_GoToNextOrStepForwardButtonClick( object sender, EventArgs e )
        {
            m_widget.GoToNextOrStepForward();
        }

        private void VCR_GoToPreviousOrStepBackwardButtonClick( object sender, EventArgs e )
        {
            m_widget.GoToPreviousOrStepBackward();
        }

        private void VCR_PauseButtonClick( object sender, EventArgs e )
        {
            m_widget.Pause();
        }

        private void VCR_PlayBackwardsButtonClick( object sender, EventArgs e )
        {
            m_widget.PlayBackwards();
        }

        private void VCR_PlayForwardsButtonClick( object sender, EventArgs e )
        {
            m_widget.PlayForwards();
        }

        private void VCR_RewindOrGoToStartButtonClick( object sender, EventArgs e )
        {
            m_widget.RewindOrGoToStart();
        }
        #endregion

        #region Public Functions
        public void Refresh()
        {
            m_controlVCR.Text = m_widget.Title;

            m_controlVCR.VCR.Style = (PlaybackControls.VCRControl.ButtonStyle)m_widget.VCRButtonStyle;

            m_controlVCR.VCR.CanFastForwardOrGoToEnd = m_widget.CanFastForwardOrGoToEnd;
            m_controlVCR.VCR.CanGoToNextOrStepForward = m_widget.CanGoToNextOrStepForward;
            m_controlVCR.VCR.CanGoToPreviousOrStepBackward = m_widget.CanGoToPreviousOrStepBackward;
            m_controlVCR.VCR.CanPause = m_widget.CanPause;
            m_controlVCR.VCR.CanPlayBackwards = m_widget.CanPlayBackwards;
            m_controlVCR.VCR.CanPlayForwards = m_widget.CanPlayForwards;
            m_controlVCR.VCR.CanRewindOrGoToStart = m_widget.CanRewindOrGoToStart;
        }
        #endregion
    }
}
