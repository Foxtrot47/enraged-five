using System;

namespace ragWidgets
{
    public class UserPrefDirChangedEventArgs
    {
        public UserPrefDirChangedEventArgs( string previousDir, string dir )
        {
            m_previousDir = previousDir;
            m_dir = dir;
        }

        #region Properties
        public string PreviousDir
        {
            get
            {
                return m_previousDir;
            }
        }

        public string Dir
        {
            get
            {
                return m_dir;
            }
        }
        #endregion

        #region Variables
        private string m_previousDir;
        private string m_dir;
        #endregion
    };

	/// <summary>
	/// Summary description for IDockableView.
	/// </summary>
    public interface IDockableView
    {
        /// <summary>
        /// Style of docking that this view should have in its DockControl
        /// </summary>
        System.Windows.Forms.DockStyle Dock { get; set; }

        /// <summary>
        /// The DockControl that this view attached to
        /// </summary>
        TD.SandDock.DockControl DockControl { get; }

        /// <summary>
        /// Image to be used as the TabImage for the associated DockControl
        /// </summary>
        /// <returns></returns>
        System.Drawing.Bitmap GetBitmap();

        /// <summary>
        /// Text placed in the Tab of the DockControl
        /// </summary>
        /// <returns></returns>
        string GetTabText();

        /// <summary>
        /// Removes Controls associated with this view
        /// </summary>
        void ClearView();

        /// <summary>
        /// True if the view's DockControl should start floating.  False will have
        /// it start docked using LastDockLocation
        /// </summary>
        bool InitiallyFloating { get; }

        /// <summary>
        /// If not InitiallyFloating, specify which dock to add the control to:  left, right, top, bottom, or center.
        /// </summary>
        TD.SandDock.ContainerDockLocation InitialDockLocation { get; }

        /// <summary>
        /// Indicates whether we will show the Option menu for hide/dispose the window when it is closed
        /// </summary>
        bool IsSingleInstance { get; }

        /// <summary>
        /// An event handler that informs us when the user preferences directory changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void UserPrefDirChanged( object sender, UserPrefDirChangedEventArgs e );
    }
}
