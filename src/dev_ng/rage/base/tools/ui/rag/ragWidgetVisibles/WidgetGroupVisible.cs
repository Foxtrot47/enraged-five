using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

using RSG.Base.Forms;
using ragCore;

namespace ragWidgets
{

	/// <summary>
	/// Summary description for WidgetGroupVisible.
	/// </summary>
	public class WidgetGroupVisible : WidgetVisible
	{
        public WidgetGroupVisible(IWidgetView widgetView, WidgetGroupBase group, Control parent, ToolTip tooltip, bool forceOpen)
            : base( widgetView, parent, group )
        {
            bool groupIsBank = group == null || group is WidgetBank;
            m_EverOpen = groupIsBank || forceOpen;

            m_Group = group;
            m_tooltip = tooltip;

            // 
            // m_XPander
            // 
            m_XPander = new XPander( m_EverOpen );
            m_XPander.SuspendLayout();

            m_XPander.Anchor = (AnchorStyles)(AnchorStyles.Top | AnchorStyles.Right | AnchorStyles.Left);
            m_XPander.AutoScroll = false;            
            m_XPander.CaptionColor = System.Drawing.Color.FromArgb( ((System.Byte)(192)), ((System.Byte)(192)), ((System.Byte)(192)) );
            //m_XPander.CaptionFont = ;
            m_XPander.CaptionText = m_Group == null ? null : m_Group.Title;
            m_XPander.CaptionTextColor = System.Drawing.Color.FromArgb( ((System.Byte)(0)), ((System.Byte)(0)), ((System.Byte)(0)) );
            m_XPander.CaptionTextHighlightColor = System.Drawing.Color.FromArgb( ((System.Byte)(66)), ((System.Byte)(142)), ((System.Byte)(255)) );
            m_XPander.Location = new System.Drawing.Point( 0, 0 );
            m_XPander.MouseUp += new MouseEventHandler( OnMouseUp );
            m_XPander.Name = "Group " + sm_GroupIndex++;            
            m_XPander.Sorted += new EventHandler( xPander_Sorted );            
            m_XPander.TabIndex = 0;
            m_XPander.Text = m_Group == null ? null : m_Group.Title;
            m_XPander.Unsorted += new EventHandler( xPander_Unsorted );
            m_XPander.XPanderCollapsed += new EventHandler( xpander_XPanderCollapsed );
            m_XPander.XPanderExpanding += new CancelEventHandler( xpander_XPanderExpanding );
            m_XPander.XPanderExpanded += new EventHandler( xpander_XPanderExpanded );

            if ( !groupIsBank )
            {
                if ( m_EverOpen )
                {
                    group.AddRefGroup();
                }
                
                m_XPander.AddRefGroup += new EventHandler( group.xpander_AddRefGroup );
                m_XPander.ReleaseGroup += new EventHandler( group.xpander_ReleaseGroup );

                m_XPander.BorderFillColor = group.FillColor;
            }

            parent.Controls.Add( m_XPander );

            XPanderList parentXPanderList = m_XPander.ParentXPanderList;
            if ( (parentXPanderList != null) && !parentXPanderList.Updating )
            {
                // only resume layout if our parent XPanderList is not updating
                m_XPander.ResumeLayout( false );
            }
        }

        #region Variables
        private static int sm_GroupIndex = 0;

        private bool m_EverOpen = false;
        private WidgetGroupBase m_Group;
        private XPander m_XPander;

        /// <summary>
        /// Reference to the view's shared tooltip.
        /// </summary>
        private ToolTip m_tooltip;
        #endregion

        #region Properties
        public bool Expanded
        {
            get
            {
                return m_XPander.Expanded;
            }
            set
            {
                m_XPander.Expanded = value;
            }
        }

        public ToolTip ToolTip
        {
            get { return m_tooltip; }
        }
        #endregion

        #region Overrides
        public override Control Control 
		{
			get
            {
                return m_XPander;
            }
		}

		public override Control DragDropControl
		{
			get
            {
                return null;    // TODO: allow drag and drop
            }
        }

        public override Color FillColor
        {
            get
            {
                return base.FillColor;
            }
            set
            {
                m_fillColor = value;

                if ( m_XPander != null )
                {
                    m_XPander.BorderFillColor = m_fillColor;
                }
            }
        }

		public override void Remove()
		{
			Debug.Assert(m_Parent.Controls.Contains(m_XPander), "m_Parent.Controls.Contains(m_XPander)" );

            m_XPander.MouseUp -= OnMouseUp;
            m_XPander.Sorted -= xPander_Sorted;
            m_XPander.Unsorted -= xPander_Unsorted;
            m_XPander.XPanderCollapsed -= xpander_XPanderCollapsed;
            m_XPander.XPanderExpanding -= xpander_XPanderExpanding;
            m_XPander.XPanderExpanded -= xpander_XPanderExpanded;

            if ( m_Group != null )
            {
                m_XPander.AddRefGroup -= new EventHandler( m_Group.xpander_AddRefGroup );
                m_XPander.ReleaseGroup -= new EventHandler( m_Group.xpander_ReleaseGroup );
            }

			m_Parent.Controls.Remove(m_XPander);
			base.Remove();
		}

		public override void Destroy()
		{
			base.Destroy();
			m_Group=null;
			m_XPander=null;
		}

        public override bool DisplayPath
	    {
		    get
		    {
                return false;
		    }
	    }

        public override void UpdatePersist( bool persist )
        {

        }

        public override Widget Widget 
        {
            get
            {
                return m_Group;
            }
        }

        public bool EverOpen
        {
            get { return m_EverOpen; }
        }
        #endregion

        #region Public Functions


        public void MakeChildrenVisible()
        {

            //return ySize;
        }

        public void BeginUpdate()
        {
            if ( m_XPander != null )
            {
                m_XPander.SuspendLayout();
            }
        }

        public void EndUpdate()
        {
            if ( m_XPander != null )
            {
                m_XPander.ResumeLayout();
            }
        }

        public int GetCaptionHeight()
        {
            return m_XPander.CaptionHeight; // check children
        }

        public void Sort()
        {
            // wrapped with a mutex to prevent destroying a widget while it's being viewed:
            BankRemotePacketProcessor.MakingVisibleMutex.WaitOne();

            m_XPander.Sort();

            BankRemotePacketProcessor.MakingVisibleMutex.ReleaseMutex();
        }

        public void UnSort()
        {
            // wrapped with a mutex to prevent destroying a widget while it's being viewed:
            BankRemotePacketProcessor.MakingVisibleMutex.WaitOne();

            m_XPander.Unsort();

            BankRemotePacketProcessor.MakingVisibleMutex.ReleaseMutex();
        }

        public void sortGroupMenuItem_Click( object sender, EventArgs e )
        {
            Sort();
        }

        public void unsortGroupMenuItem_Click( object sender, EventArgs e )
        {
            UnSort();
        }
        #endregion

        #region Event Handlers
        private void xPander_Sorted( object sender, EventArgs e )
        {
            string path = string.Empty;
            if ( Widget != null )
            {
                path = Widget.FindPath();
            }

            WidgetGroupBase.SetSortedGroup( WidgetView.Guid.ToString(), path, true );
        }

        private void xPander_Unsorted( object sender, EventArgs e )
        {
            string path = string.Empty;
            if ( Widget != null )
            {
                path = Widget.FindPath();
            }

            WidgetGroupBase.SetSortedGroup( WidgetView.Guid.ToString(), path, false );
        }

        private void xpander_XPanderCollapsed( object sender, EventArgs e )
        {
            ViewUtils.SetExpandedGroup( this.WidgetView, m_Group, false );
        }

        private void xpander_XPanderExpanding( object sender, CancelEventArgs e )
        {
            if ( !m_EverOpen )
            {
                if ( m_Group != null )
                {
                    BeginUpdate();

                    foreach ( Widget widget in m_Group.List )
                    {
                        try
                        {
                            WidgetVisualiser.ShowVisible(widget, this.WidgetView, this.Control, this.Control.Name, false, m_tooltip);
                        }
                        catch ( Exception ex )
                        {
                            DialogResult result = rageMessageBox.ShowExclamation( this.Control,
                                String.Format( "There was an error creating a Widget Visible for the Widget {0}:\n\n{1}\nWould you like to disable the widget?",
                                widget.Title, ex.ToString() ), "Widget Error", MessageBoxButtons.YesNo );
                            if ( result == DialogResult.Yes )
                            {
                                widget.Enabled = false;

                                if ( !WidgetVisualiser.PacketProcessor.DisabledWidgets.Contains( widget ) )
                                {
                                    WidgetVisualiser.PacketProcessor.DisabledWidgets.Add( widget );
                                }
                            }
                        }
                    }

                    string path = this.Widget == null ? string.Empty : this.Widget.FindPath();
                    if ( WidgetView != null && WidgetGroupBase.IsSortedGroup( WidgetView.Guid.ToString(), path ) )
                    {
                        Sort();
                    }

                    EndUpdate();
                }

                m_EverOpen = true;
            }
        }

        private void xpander_XPanderExpanded( object sender, EventArgs e )
        {
            ViewUtils.SetExpandedGroup( this.WidgetView, m_Group, true );
        }
        #endregion

        #region Static Functions
        public static void ClearStatics()
		{
			sm_GroupIndex=0;
        }
        #endregion
    }
}
