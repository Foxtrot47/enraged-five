using System;
using System.Windows.Forms;
using ragCore;

namespace ragWidgets
{
	/// <summary>
	/// Summary description for Class1.
	/// </summary>
    public interface IWidgetView
    {
        string LabelHelp { set; }
        string LabelDescription { set; }
        bool DisplayPath { get; }
        bool AllowDragDrop { get; }
        System.Guid Guid { get; set; }

        void SetIndexForWidget( WidgetVisible visible, int newIndex );
        void AddContextMenuOptions( ContextMenuStrip menu, Widget widget, WidgetVisible visible );
        void SetDirty();
        Control GetControlAtCursorPoint();
        void ResizeAndReposition();
    }

}
