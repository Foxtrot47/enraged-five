using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

using ragCore;

namespace ragWidgets
{
    public class WidgetPlaceholderVisible : WidgetTitleVisible
    {
        public WidgetPlaceholderVisible(IWidgetView widgetView, WidgetTitle title, Control parent, ToolTip tooltip)
            : base( widgetView, title, parent, tooltip )
        {
            AddContextMenu( this.Control );
        }
    };
}
