using System;
using System.Windows.Forms;
using System.Collections.Generic;

using ragCore;

namespace ragWidgets
{
	/// <summary>
	/// Summary description for WidgetSliderIntVisible.
	/// </summary>
	public class WidgetComboVisible : WidgetControlBaseVisible
	{
        public WidgetComboVisible(IWidgetView widgetView, WidgetCombo combo, Control parent, ToolTip tooltip)
            : base( widgetView, parent, combo )
        {
            m_Widget = combo;
            m_Widget.WidgetItemSet += widget_ItemSet;

            m_ControlCombo = new ragUi.ControlCombo();
            m_ControlCombo.Text = m_Widget.Title;
            if ( combo.Items.Count == 0 && combo.Count > 0 ) // special case, we're creating a visible w/o knowing all the items yet
            {
                List<string> items = new List<string>();
                for ( int i = 0; i < combo.Count; i++ )
                {
                    items.Add( "" );
                }
                m_ControlCombo.SetItems( items );
            }
            else
            {
                m_ControlCombo.SetItems( combo.Items );
            }


            // this 'if' might not be true if the combo box doesn't have any items at initial creation time:
            if ( m_Widget.Value < combo.Items.Count )
            {
                m_ControlCombo.UpdateValueFromRemote( (int)m_Widget.Value );
            }

            m_ComboBoxEH = new EventHandler( control_ValueChanged );
            m_ControlCombo.ValueChanged += m_ComboBoxEH;
            m_ControlCombo.IsReadOnly = combo.IsReadOnly;


            FinishCreation(tooltip);
            if (Widget.IsModified())
            {
                UpdateModifiedIndication();
            }
        }

        void widget_ItemSet( object sender, WidgetCombo.WidgetComboItemEventArgs e )
        {
            SetItem( e.Item, e.Data );
        }

        protected override void widget_ValueChanged( object sender, EventArgs e )
        {
            WidgetCombo widget = (WidgetCombo)sender;
            UpdateValueFromRemote( (int)widget.Value );
            Resize( (int)widget.Count );
        }


        public void control_ValueChanged( object obj, System.EventArgs e )
        {
            ragUi.ControlCombo combo = (ragUi.ControlCombo)obj;
            if ( combo != null && m_Widget.Value != combo.SelectedIndex )
            {
                // adickinson [11/15/2005] Value=combo.SelectedIndex+m_Offset;
                m_Widget.Value = combo.SelectedIndex; // offset will be added in by Value::set
            }
        }
        #region Variables
        private ragUi.ControlCombo m_ControlCombo;
        private EventHandler m_ComboBoxEH;
        private WidgetCombo m_Widget;
        private delegate void UpdateDelegate(int value);
        #endregion

        #region Properties
        public int Value
        {
            get
            {
                return m_ControlCombo.SelectedIndex;
            }
        }
        #endregion

        #region Overrides
        public override void Destroy()
		{
            base.Destroy();
            m_Widget.WidgetItemSet -= widget_ItemSet;
			m_Widget=null;
			m_ComboBoxEH=null;
            m_ControlCombo.Dispose();
			m_ControlCombo=null;
		}

		public override ragUi.ControlBase ControlBase
		{
			get
			{
				return m_ControlCombo;
			}
        }

        public override Widget Widget { get { return m_Widget; } }
        #endregion

        #region Public Functions
        /// <summary>
		/// disable event handler temporarily.
		/// </summary>
		public void UpdateValueFromRemote(int value) 
		{
            if (m_ControlCombo.InvokeRequired == true)
            {
                UpdateDelegate updateDelegate = new UpdateDelegate(m_ControlCombo.UpdateValueFromRemote);

                object[] args = new object[1];
                args[0] = value;
                m_ControlCombo.Invoke(updateDelegate, args);
            }
            else
            {
                m_ControlCombo.UpdateValueFromRemote(value);
            }

            UpdateModifiedIndication();
		}

		public void SetItem(int which,string str)
		{
			m_ControlCombo.SetItem(which,str);
		}

		public void SetItems( List<string> items )
		{
			m_ControlCombo.SetItems( items );
        }

        public void Resize( int numItems )
        {
            m_ControlCombo.ResizeItems( numItems );
        }

        #endregion
	}
}
