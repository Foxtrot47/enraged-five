using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

using ragCore;
using ragUi;

namespace ragWidgets
{
    public class WidgetSeparatorVisible : WidgetVisible
    {
        public WidgetSeparatorVisible(IWidgetView widgetView, WidgetSeparator separator, Control parent, ToolTip tooltip)
            : base( widgetView, parent, separator )
		{
            m_widget = separator;

            m_separator = new ControlSeparator();
            m_separator.Name = m_widget.Title;
            m_separator.TabIndex = 0;
            m_separator.Text = m_widget.Title;
            m_separator.Enabled = !m_widget.IsReadOnly && m_widget.Enabled;

            parent.Controls.Add( m_separator );
        }

        #region Variables
        private ControlSeparator m_separator;
        private WidgetSeparator m_widget;
        #endregion

        #region Overrides
        public override Control Control
        {
            get
            {
                return m_separator;
            }
        }

        public override Control DragDropControl
        {
            get
            { 
                return null;
            }
        }

        public override Widget Widget
        {
            get
            {
                return m_widget;
            }
        }

        public override void Destroy()
        {
            base.Destroy();
            
            m_separator = null;
            m_widget = null;
        }

        public override void Remove()
        {
            m_Parent.Controls.Remove( m_separator );
            
            base.Remove();
        }
        #endregion
    }
}
