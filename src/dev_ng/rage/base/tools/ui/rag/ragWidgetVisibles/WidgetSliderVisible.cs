using System;
using System.Windows.Forms;
using System.Diagnostics;

using ragCore;
using ragUi;

namespace ragWidgets
{
	/// <summary>
	/// Summary description for WidgetSliderFloatVisible.
	/// </summary>
	public class WidgetSliderFloatVisible : WidgetControlBaseVisible
	{
        public WidgetSliderFloatVisible(IWidgetView widgetView, WidgetSliderFloat slider, Control parent, ToolTip tooltip) 
			: base(widgetView,parent,slider)
		{
            m_Widget = slider;
            m_Widget.SliderInfoChanged += widget_SliderInfoChanged;

			m_ControlSlider = new ControlSlider();
			m_ControlSlider.IsFloat = true;
			m_ControlSlider.Text = slider.Title;
            m_ControlSlider.SetRange( slider.Minimum, slider.Maximum );
			m_ControlSlider.SetStep( slider.Step );
			m_ControlSlider.ValueChanged += new EventHandler(control_ValueChanged);
            m_ControlSlider.UpdateValueFromRemote( m_Widget.Value );
            m_ControlSlider.IsReadOnly = m_Widget.IsReadOnly;

			FinishCreation(tooltip);

            if (Widget.IsModified())
            {
                UpdateModifiedIndication();
            }
        }

        protected override void widget_ValueChanged( object sender, EventArgs e )
        {
            WidgetSliderFloat widget = (WidgetSliderFloat)sender;
            UpdateValueFromRemote( widget.Value );
        }

        public void control_ValueChanged( object obj, System.EventArgs e )
        {
            // get our value from the visible:
            ragUi.ControlSlider ctl = (ragUi.ControlSlider)obj;
            if ( ctl != null && m_Widget.Value != (float)ctl.Value )
            {
                // Should really be wrapped in a temporary unsubscription of event but seem to be ok anyway
                m_Widget.Value = (float)ctl.Value;
            }
        }

        private void widget_SliderInfoChanged( object sender, EventArgs e )
        {
            WidgetSliderFloat widget = (WidgetSliderFloat)sender;
            Step = widget.Step;
            Minimum = widget.Minimum;
            Maximum = widget.Maximum;
        }

        #region Variables
        private WidgetSliderFloat m_Widget;
        private ControlSlider m_ControlSlider;
        private delegate void UpdateInvoke(float inValue);
        #endregion

        #region Properties
        public float Value
        {
            get
            {
                return (float)m_ControlSlider.Value;
            }
        }

        public float Minimum
        {
            get
            {
                return (float)Convert.ToDouble( m_ControlSlider.Minimum );
            }
            set
            {
                m_ControlSlider.SetRange( value, this.Maximum );
            }
        }

        public float Maximum
        {
            get
            {
                return (float)Convert.ToDouble( m_ControlSlider.Maximum );
            }
            set
            {
                m_ControlSlider.SetRange( this.Minimum, value );
            }
        }

        public float Step
        {
            get
            {
                return (float)Convert.ToDouble( m_ControlSlider.Step );
            }
            set
            {
                m_ControlSlider.SetStep( value );
            }
        }
        #endregion

        #region Overrides
        public override ragUi.ControlBase ControlBase
		{
			get
			{
				return m_ControlSlider;
			}
        }
        
        public override Widget Widget { get { return m_Widget; } }

        public override void Destroy()
        {
            base.Destroy();
            m_Widget.SliderInfoChanged -= widget_SliderInfoChanged;
            m_Widget = null;
            m_ControlSlider.Dispose();
            m_ControlSlider = null;
        }
        #endregion

        #region Public Functions
        private void UpdateValueFromRemote(float value) 
		{
            if (m_ControlSlider.InvokeRequired == true)
            {
                UpdateInvoke updateInvoke = new UpdateInvoke(m_ControlSlider.UpdateValueFromRemote);

                object[] args = new object[1];
                args[0] = value;
                m_ControlSlider.Invoke(updateInvoke, args);
            }
            else
            {
                m_ControlSlider.UpdateValueFromRemote(value);
            }

            UpdateModifiedIndication();
        }
        #endregion
	}

	/// <summary>
	/// Summary description for WidgetSliderIntVisible.
	/// </summary>
	public class WidgetSliderIntVisible : WidgetControlBaseVisible
	{
        public WidgetSliderIntVisible(IWidgetView widgetView, WidgetSliderInt slider, Control parent, ToolTip tooltip) 
			: base(widgetView,parent,slider)
		{
            m_Widget = slider;
            m_Widget.SliderInfoChanged += widget_SliderInfoChanged;

			m_ControlSlider = new ControlSlider();
			m_ControlSlider.IsFloat = false;

			m_ControlSlider.Text = slider.Title;
			m_ControlSlider.Minimum = slider.Minimum;
			m_ControlSlider.Maximum = slider.Maximum;
			m_ControlSlider.Step = slider.Step;
            m_ControlSlider.ValueChanged += new EventHandler( control_ValueChanged );
            m_ControlSlider.UpdateValueFromRemote(m_Widget.Value);
            m_ControlSlider.IsReadOnly = m_Widget.IsReadOnly;


            FinishCreation(tooltip);
            if (Widget.IsModified())
            {
                UpdateModifiedIndication();
            }
        }

        protected override void widget_ValueChanged( object sender, EventArgs e )
        {
            WidgetSliderInt widget = (WidgetSliderInt)sender;
            UpdateValueFromRemote( (int)widget.Value );
        }

        public void control_ValueChanged( object obj, System.EventArgs e )
        {
            // get our value from the visible:
            ragUi.ControlSlider ctl = (ragUi.ControlSlider)obj;
            if ( ctl != null && m_Widget.Value != (int)ctl.Value )
            {
                m_Widget.Value = (int)ctl.Value;
            }
        }

        private void widget_SliderInfoChanged( object sender, EventArgs e )
        {
            WidgetSliderInt widget = (WidgetSliderInt)sender;
            Step = widget.Step;
            Minimum = widget.Minimum;
            Maximum = widget.Maximum;
        }


        #region Variables
        private WidgetSliderInt m_Widget;
        private ControlSlider m_ControlSlider;
        private delegate void UpdateInvoke(float inValue);
        #endregion

        #region Properties
        public int Value
        {
            get
            {
                return (int)m_ControlSlider.Value;
            }
        }

        public float Minimum
        {
            get
            {
                return (float)Convert.ToDouble( m_ControlSlider.Minimum );
            }
            set
            {
                m_ControlSlider.SetRange( value, this.Maximum );
            }
        }

        public float Maximum
        {
            get
            {
                return (float)Convert.ToDouble( m_ControlSlider.Maximum );
            }
            set
            {
                m_ControlSlider.SetRange( this.Minimum, value );
            }
        }

        public float Step
        {
            get
            {
                return (float)Convert.ToDouble( m_ControlSlider.Step );
            }
            set
            {
                m_ControlSlider.SetStep( value );
            }
        }
        #endregion

        #region Overrides
        public override ragUi.ControlBase ControlBase
        {
            get
            {
                return m_ControlSlider;
            }
        }

        public override Widget Widget { get { return m_Widget; } }

        public override void Destroy()
        {
            base.Destroy();
            m_Widget.SliderInfoChanged -= widget_SliderInfoChanged;
            m_Widget = null;
            m_ControlSlider.Dispose();
            m_ControlSlider = null;
        }
        #endregion

        #region Public Functions
        public void UpdateValueFromRemote(int value) 
		{
            if (m_ControlSlider.InvokeRequired == true)
            {
                UpdateInvoke updateInvoke = new UpdateInvoke(m_ControlSlider.UpdateValueFromRemote);

                object[] args = new object[1];
                args[0] = value;
                m_ControlSlider.Invoke(updateInvoke, args);
            }
            else
            {
                m_ControlSlider.UpdateValueFromRemote(value);
            }

            UpdateModifiedIndication();
        }
        #endregion
	}
}
