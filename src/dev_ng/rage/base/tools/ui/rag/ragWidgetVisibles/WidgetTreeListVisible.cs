using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows.Forms;
using System.Xml;

using ragCore;
using RSG.Base.Forms;
using ragUi;

using GlacialComponents.Controls.GTLCommon;
using GlacialComponents.Controls.GlacialTreeList;
using RSG.Base.Logging;

namespace ragWidgets
{
    /// <summary>
    /// Summary description for WidgetTreeListVisible.
    /// </summary>
    public class WidgetTreeListVisible : WidgetVisible
    {

        public WidgetTreeListVisible(IWidgetView widgetView, WidgetTreeList list, Control parent, ToolTip tooltip,
            bool allowDrag, bool allowDrop, bool allowDelete, bool showRootLines )
            : base( widgetView, parent, list )
        {
            m_Widget = list;
            m_Widget.ColumnAdded += widget_ColumnAdded;
            m_Widget.NodeAdded += widget_NodeAdded;
            m_Widget.ItemAdded += widget_ItemAdded;
            m_Widget.NodeRemoved += widget_NodeRemoved;
            m_Widget.ItemRemoved += widget_ItemRemoved;
            m_Widget.ItemValueChanged += widget_ItemValueChanged;
            m_Widget.CheckStateChanged += widget_CheckStateChanged;



            m_TreeListControl = new ControlTreeList();
            m_TreeListControl.Text = m_Widget.Title;
            m_TreeListControl.Title = m_Widget.Title;
            m_TreeListControl.Enabled = !m_Widget.IsReadOnly && m_Widget.Enabled;
            m_TreeListControl.TheTreeList.TreeList.BeforeCellEdit += new GTLBeforeEditEventHandlerDelegate( TreeList_BeforeCellEdit );
            m_TreeListControl.TheTreeList.TreeList.AfterCellEdit += new GTLEmbeddedControlEventHandler( TreeList_AfterCellEdit );
            m_TreeListControl.TheTreeList.TreeList.BeforeCheck += new GTLCheckChangeEventHandler( TreeList_BeforeCheck );
            m_TreeListControl.TheTreeList.TreeList.EmbeddedControlShow += new GTLBeforeEditEventHandlerDelegate( TreeList_EmbeddedControlShow );
            m_TreeListControl.TheTreeList.TreeList.EmbeddedControlHide += new GTLEmbeddedControlEventHandler( TreeList_EmbeddedControlHide );

            if ( allowDrag )
            {
                m_TreeListControl.TheTreeList.TreeList.AllowDrop = true;
                m_TreeListControl.TheTreeList.TreeList.NodeDrag += new GTLEventHandler( TreeList_NodeDrag );
                m_TreeListControl.TheTreeList.TreeList.KeyDown += new KeyEventHandler( TreeList_CopyKeyDown );

                if ( allowDrop )
                {
                    m_TreeListControl.TheTreeList.TreeList.DragDrop += new DragEventHandler( TreeList_DragDrop );
                    m_TreeListControl.TheTreeList.TreeList.DragEnter += new DragEventHandler( TreeList_DragEnter );
                    m_TreeListControl.TheTreeList.TreeList.DragOver += new DragEventHandler( TreeList_DragOver );
                    m_TreeListControl.TheTreeList.TreeList.KeyDown += new KeyEventHandler( TreeList_PasteKeyDown );
                }
            }

            if ( allowDelete )
            {
                m_TreeListControl.TheTreeList.TreeList.KeyDown += new KeyEventHandler( TreeList_DeleteKeyDown );
            }

            m_TreeListControl.TheTreeList.TreeList.ShowRootLines = showRootLines;

            parent.Controls.Add( m_TreeListControl );

            AddMouseEnter( m_TreeListControl.TheLabel );
            m_TreeListControl.TheLabel.MouseUp += new MouseEventHandler( OnMouseUp );
            m_TreeListControl.TheLabel.MouseDown += new MouseEventHandler( OnMouseDown );
            SetDragDropHandlers( m_TreeListControl.TheLabel );

            AddMouseEnter( m_TreeListControl.TheTreeList );
            m_TreeListControl.TheTreeList.MouseUp += new MouseEventHandler( OnMouseUp );
            m_TreeListControl.TheTreeList.MouseDown += new MouseEventHandler( OnMouseDown );
            SetDragDropHandlers( m_TreeListControl.TheTreeList );
        }


        private void widget_ColumnAdded( object sender, TreeListColumnEventArgs e )
        {
            AddColumn( e.Item );
        }

        private void widget_NodeAdded( object sender, TreeListNodeEventArgs e )
        {
            AddNode( e.Item );
        }

        private void widget_ItemAdded( object sender, TreeListItemEventArgs e )
        {
            AddItem( e.Item );
        }

        private void widget_NodeRemoved( object sender, KeyValueEventArgs e )
        {
            RemoveNode( e.Item );
        }

        private void widget_ItemRemoved( object sender, KeyValueEventArgs e )
        {
            RemoveItem( e.Item );
        }

        private void widget_ItemValueChanged( object sender, KeyValueEventArgs e )
        {
            SetItemValue( e.Item, (string)e.NewValue );
        }

        private void widget_CheckStateChanged( object sender, KeyValueEventArgs e )
        {
            SetItemCheckBox( e.Item, (WidgetTreeList.CheckBoxState)e.NewValue );
        }

        #region Variables
        private WidgetTreeList m_Widget;
        private ControlTreeList m_TreeListControl;
        private Dictionary<int, Control> m_alwaysVisibleEditControls = new Dictionary<int, Control>();

        private Dictionary<int, GTLTreeNode> m_treeNodesByKey = new Dictionary<int, GTLTreeNode>();
        private Dictionary<int, GTLSubItem> m_subItemsByKey = new Dictionary<int, GTLSubItem>();
        private Dictionary<int, GTLSubItem> m_checkBoxesByKey = new Dictionary<int, GTLSubItem>();

        private static DataFormats.Format TreeListNodeData_DataFormat = DataFormats.GetFormat( "TreeListNodeData" );
        #endregion

        #region Properties
        public override Control Control
        {
            get
            {
                return m_TreeListControl;
            }
        }

        public override Control DragDropControl
        {
            get
            {
                return null;
            }
        }

        public override Widget Widget
        {
            get
            {
                return m_Widget;
            }
        }
        #endregion

        #region Overrides
        public override void Remove()
        {
            base.Remove();
            base.RemoveContextMenu( m_TreeListControl.TheLabel );
            base.RemoveContextMenu( m_TreeListControl.TheTreeList );

            m_Parent.Controls.Remove( m_TreeListControl );
        }

        public override void Destroy()
        {
            base.Destroy();
            m_Widget.ColumnAdded -= widget_ColumnAdded;
            m_Widget.NodeAdded -= widget_NodeAdded;
            m_Widget.ItemAdded -= widget_ItemAdded;
            m_Widget.NodeRemoved -= widget_NodeRemoved;
            m_Widget.ItemRemoved -= widget_ItemRemoved;
            m_Widget.ItemValueChanged -= widget_ItemValueChanged;
            m_Widget.CheckStateChanged -= widget_CheckStateChanged;
            m_Widget = null;
            m_TreeListControl.Dispose();
            m_TreeListControl = null;
        }
        #endregion

        #region Public Functions
        /// <summary>
        /// Add a column to the Tree List
        /// </summary>
        /// <param name="colHeader"></param>
        public void AddColumn( TreeListColumnHeader colHeader )
        {
            GTLColumn gtlColumn = new GTLColumn();
            gtlColumn.Width = 130;
            gtlColumn.Text = colHeader.Name;

            switch ( colHeader.ColumnControlType )
            {
                case WidgetTreeList.ColumnControlType.None:
                    gtlColumn.EmbeddedControlType = GEmbeddedControlTypes.None;
                    break;
                case WidgetTreeList.ColumnControlType.ColumnTemplate:
                    gtlColumn.EmbeddedControlType = GEmbeddedControlTypes.ColumnTemplate;

                    System.Type columnTemplateType = GetTypeFromItemControlType( colHeader.ItemControlType );
                    if ( columnTemplateType == null )
                    {
                        gtlColumn.EmbeddedControlType = GEmbeddedControlTypes.None;
                    }
                    else
                    {
                        gtlColumn.EmbeddedControlTemplateType = columnTemplateType;
                    }
                    break;
                case WidgetTreeList.ColumnControlType.InPlaceTemplate:
                    gtlColumn.EmbeddedControlType = GEmbeddedControlTypes.InPlaceTemplate;
                    break;
                case WidgetTreeList.ColumnControlType.TextBox:
                    gtlColumn.EmbeddedControlType = GEmbeddedControlTypes.TextBox;
                    break;
                case WidgetTreeList.ColumnControlType.ComboBox:
                    gtlColumn.EmbeddedControlType = GEmbeddedControlTypes.ComboBox;
                    break;
                case WidgetTreeList.ColumnControlType.DateTimePicker:
                    gtlColumn.EmbeddedControlType = GEmbeddedControlTypes.DateTimePicker;
                    break;
            }

            switch ( colHeader.EditCondition )
            {
                case WidgetTreeList.EditCondition.None:
                    gtlColumn.EmbeddedDisplayConditions = GEmbeddedDisplayConditions.None;
                    break;
                case WidgetTreeList.EditCondition.SingleClick:
                    gtlColumn.EmbeddedDisplayConditions = GEmbeddedDisplayConditions.SingleClick;
                    break;
                case WidgetTreeList.EditCondition.DoubleClick:
                    gtlColumn.EmbeddedDisplayConditions = GEmbeddedDisplayConditions.DoubleClick;
                    break;
                case WidgetTreeList.EditCondition.AlwaysVisible:
                    if ( colHeader.ColumnControlType == WidgetTreeList.ColumnControlType.ColumnTemplate )
                    {
                        gtlColumn.EmbeddedDisplayConditions = GEmbeddedDisplayConditions.AlwaysVisible;
                    }
                    else
                    {
                        gtlColumn.EmbeddedDisplayConditions = GEmbeddedDisplayConditions.SingleClick;
                    }
                    break;
            }

            if ( colHeader.CheckStyle != WidgetTreeList.CheckBoxStyle.None )
            {
                gtlColumn.CheckBoxes = true;

                switch ( colHeader.CheckStyle )
                {
                    case WidgetTreeList.CheckBoxStyle.TwoState:
                        gtlColumn.CheckBoxStyle = GTLCheckBoxTypes.TwoState;
                        break;
                    case WidgetTreeList.CheckBoxStyle.ThreeState:
                        gtlColumn.CheckBoxStyle = GTLCheckBoxTypes.ThreeState;
                        break;
                }

                switch ( colHeader.CheckAlign )
                {
                    case WidgetTreeList.CheckBoxAlign.Right:
                        gtlColumn.CheckBoxAlign = HorizontalAlignment.Right;
                        break;
                    case WidgetTreeList.CheckBoxAlign.Center:
                        gtlColumn.CheckBoxAlign = HorizontalAlignment.Center;
                        break;
                    default:
                        gtlColumn.CheckBoxAlign = HorizontalAlignment.Left;
                        break;
                }
            }
            else
            {
                gtlColumn.CheckBoxes = false;
            }

            m_TreeListControl.TheTreeList.BeginNodeListChange();
            m_TreeListControl.TheTreeList.Columns.Add( gtlColumn );
            m_TreeListControl.TheTreeList.EndNodeListChange();
        }

        /// <summary>
        /// Add Node
        /// </summary>
        /// <param name="node"></param>
        public void AddNode( TreeListNode node )
        {
            GTLTreeNode gtlTreeNode;
            if ( m_treeNodesByKey.TryGetValue( node.Key, out gtlTreeNode ) )
            {
                LogFactory.ApplicationLog.MessageCtx("WidgetTreeListVisible", "AddNode with key {0} already exists", node.Key);
                return;
            }

            gtlTreeNode = new GTLTreeNode();
            gtlTreeNode.Text = node.Name;
            gtlTreeNode.Tag = node.NodeData;

            if ( node.Expand )
            {
                gtlTreeNode.Expand();
            }
            else
            {
                gtlTreeNode.Collapse();
            }            

            m_TreeListControl.TheTreeList.BeginNodeListChange();
            
            GTLTreeNode gtlParentNode;
            if ( m_treeNodesByKey.TryGetValue( node.ParentKey, out gtlParentNode ) )
            {
                gtlParentNode.Nodes.Add( gtlTreeNode );
            }
            else
            {
                m_TreeListControl.TheTreeList.TreeList.Nodes.Add( gtlTreeNode );
            }
            
            m_TreeListControl.TheTreeList.EndNodeListChange();

            m_treeNodesByKey[node.Key] = gtlTreeNode;
        }

        /// <summary>
        /// Add Item
        /// </summary>
        /// <param name="item"></param>
        public void AddItem( TreeListItem item )
        {
            GTLTreeNode gtlTreeNode;
            if ( !m_treeNodesByKey.TryGetValue( item.NodeKey, out gtlTreeNode ) )
            {
                LogFactory.ApplicationLog.MessageCtx("WidgetTreeListVisible", "AddItem '{0}' key {1} to node key {2} before node was added", item.Val, item.Key, item.NodeKey);
                return;
            }

            if ( gtlTreeNode.SubItems.Count == m_TreeListControl.TheTreeList.Columns.Count )
            {
                LogFactory.ApplicationLog.MessageCtx("WidgetTreeListVisible", "AddItem key {0} to node {1} with too few columns", item.Key, gtlTreeNode.Text);
                return;
            }

            m_TreeListControl.TheTreeList.BeginNodeListChange();

            bool addSubItem = false;
            GTLSubItem gtlSubItem = null;
            int column;
            if ( (gtlTreeNode.SubItems.Count == 1) && (gtlTreeNode.SubItems[0].Tag == null) )
            {
                // first time adding an item
                gtlSubItem = gtlTreeNode.SubItems[0];
                column = 0;
            }
            else
            {
                gtlSubItem = new GTLSubItem();
                addSubItem = true;
                column = gtlTreeNode.SubItems.Count;
            }

            // initialize the sub item
            gtlSubItem.Text = item.Val;
            gtlSubItem.Tag = item.ItemData;
            gtlSubItem.DisableEmbeddedControl = item.ReadOnly;
            gtlSubItem.DisableCheckbox = item.NoCheckBox;

            // set the check box state
            if ( !gtlSubItem.DisableCheckbox )
            {
                switch ( item.CheckedState )
                {
                    case WidgetTreeList.CheckBoxState.Unchecked:
                        gtlSubItem.CheckState = System.Windows.Forms.CheckState.Unchecked;
                        break;
                    case WidgetTreeList.CheckBoxState.Indeterminate:
                        gtlSubItem.CheckState = System.Windows.Forms.CheckState.Indeterminate;
                        break;
                    case WidgetTreeList.CheckBoxState.Checked:
                        gtlSubItem.CheckState = System.Windows.Forms.CheckState.Checked;
                        break;
                }
            }

            // verify our sub item control type is appropriate for the column control type
            GTLColumn gtlColumn = m_TreeListControl.TheTreeList.TreeList.Columns[column];
            System.Type itemTemplateType = GetTypeFromItemControlType( item.ItemControlType );

            switch ( gtlColumn.EmbeddedControlType )
            {
                case GEmbeddedControlTypes.None:
                    gtlSubItem.DisableEmbeddedControl = true;
                    break;
                case GEmbeddedControlTypes.ColumnTemplate:
                    if ( itemTemplateType == null )
                    {
                        // either ItemControlType.None or unsupported control type
                        gtlSubItem.DisableEmbeddedControl = true;
                    }
                    else if ( itemTemplateType != gtlColumn.EmbeddedControlTemplateType )
                    {
                        rageMessageBox.ShowWarning(
                            String.Format( "Column-item control type mismatch:\n{0} vs {1}\nin node {2} column {3} item {4}\n\nDisabling the edit control for this item.", 
                            gtlColumn.EmbeddedControlTemplateType.ToString(), itemTemplateType.ToString(), gtlTreeNode.Text, column.ToString(), gtlSubItem.Text ), 
                            "Control Type Mismatch" );

                        gtlSubItem.DisableEmbeddedControl = true;
                    }
                    else if ( (item.ItemControlType == WidgetTreeList.ItemControlType.ComboBox) && (item.ComboBoxItems.Length == 0) )
                    {
                        // no items in the combo box drop-down
                        gtlSubItem.DisableEmbeddedControl = true;
                    }
                    break;
                case GEmbeddedControlTypes.InPlaceTemplate:
                    if ( itemTemplateType == null )
                    {
                        // either ItemControlType.None or unsupported control type
                        gtlSubItem.DisableEmbeddedControl = true;
                    }
                    else if ( (item.ItemControlType == WidgetTreeList.ItemControlType.ComboBox) && (item.ComboBoxItems.Length == 0) )
                    {
                        // no items in the combo box drop-down
                        gtlSubItem.DisableEmbeddedControl = true;
                    }
                    else
                    {
                        gtlSubItem.EmbeddedControlTemplateType = itemTemplateType;
                    }
                    break;
                case GEmbeddedControlTypes.TextBox:
                    if ( item.ItemControlType == WidgetTreeList.ItemControlType.None )
                    {
                        gtlSubItem.DisableEmbeddedControl = true;
                    }
                    else if ( item.ItemControlType != WidgetTreeList.ItemControlType.TextBox )
                    {
                        rageMessageBox.ShowWarning(
                            String.Format( "Column-item control type mismatch:\nTextBox vs {0}\nin node {1} column {2} item {3}\n\nDisabling the edit control for this item.",
                            itemTemplateType.ToString(), gtlTreeNode.Text, column.ToString(), gtlSubItem.Text ),
                            "Control Type Mismatch" );


                        gtlSubItem.DisableEmbeddedControl = true;
                    }
                    break;
                case GEmbeddedControlTypes.ComboBox:
                    if ( item.ItemControlType == WidgetTreeList.ItemControlType.None )
                    {
                        gtlSubItem.DisableEmbeddedControl = true;
                    }
                    else if ( item.ItemControlType != WidgetTreeList.ItemControlType.ComboBox )
                    {
                        rageMessageBox.ShowWarning(
                            String.Format( "Column-item control type mismatch:\nComboBox vs {0}\nin node {1} column {2} item {3}\n\nDisabling the edit control for this item.",
                            itemTemplateType.ToString(), gtlTreeNode.Text, column.ToString(), gtlSubItem.Text ),
                            "Control Type Mismatch" );

                        gtlSubItem.DisableEmbeddedControl = true;
                    }
                    else if ( item.ComboBoxItems.Length == 0 )
                    {
                        // no items in the combo box drop-down
                        gtlSubItem.DisableEmbeddedControl = true;
                    }
                    break;
                case GEmbeddedControlTypes.DateTimePicker:
                    if ( item.ItemControlType == WidgetTreeList.ItemControlType.None )
                    {
                        gtlSubItem.DisableEmbeddedControl = true;
                    }
                    else if ( item.ItemControlType != WidgetTreeList.ItemControlType.DateTimePicker )
                    {
                        rageMessageBox.ShowWarning(
                            String.Format( "Column-item control type mismatch:\nDateTimePicker vs {0}\nin node {1} column {2} item {3}\n\nDisabling the edit control for this item.",
                            itemTemplateType.ToString(), gtlTreeNode.Text, column.ToString(), gtlSubItem.Text ),
                            "Control Type Mismatch" );

                        gtlSubItem.DisableEmbeddedControl = true;
                    }
                    break;
            }

            if ( addSubItem )
            {
                gtlTreeNode.SubItems.Add( gtlSubItem );
            }

            m_subItemsByKey[item.Key] = gtlSubItem;

            if ( item.CheckBoxKey != -1 )
            {
                m_checkBoxesByKey[item.CheckBoxKey] = gtlSubItem;
            }

            UpdateParentNodeSubItem( gtlTreeNode.ParentNode, gtlTreeNode.SubItems.Count - 1 );

            m_TreeListControl.TheTreeList.EndNodeListChange();
        }

        public void RemoveNode( int key )
        {
            GTLTreeNode gtlTreeNode;
            if ( !m_treeNodesByKey.TryGetValue( key, out gtlTreeNode ) )
            {
                LogFactory.ApplicationLog.MessageCtx("WidgetTreeListVisible", "RemoveNode key {0} does not exist", key);
                return;
            }

            foreach ( GTLSubItem gtlSubItem in gtlTreeNode.SubItems )
            {
                TreeListItemData iData = gtlSubItem.Tag as TreeListItemData;
                if ( iData != null )
                {
                    m_alwaysVisibleEditControls.Remove( iData.Key );
                }
            }

            RemoveNodeKeys( gtlTreeNode );

            GTLTreeNode parentNode = gtlTreeNode.ParentNode;

            m_TreeListControl.TheTreeList.BeginNodeListChange();
            if ( gtlTreeNode.ParentNode == null )
            {
                m_TreeListControl.TheTreeList.TreeList.Nodes.Remove( gtlTreeNode );
            }
            else
            {
                gtlTreeNode.ParentNode.Nodes.Remove( gtlTreeNode );

                for ( int column = 0; column < parentNode.SubItems.Count; ++column )
                {
                    UpdateParentNodeSubItem( parentNode, column );
                }
            }
            m_TreeListControl.TheTreeList.EndNodeListChange();
        }

        public void RemoveItem( int key )
        {
            GTLSubItem gtlSubItem;
            if ( !m_subItemsByKey.TryGetValue( key, out gtlSubItem ) )
            {
                LogFactory.ApplicationLog.MessageCtx("WidgetTreeListVisible", "RemoveItem key {0} does not exist", key);
                return;
            }

            TreeListItemData iData = gtlSubItem.Tag as TreeListItemData;
            if ( iData != null )
            {
                m_alwaysVisibleEditControls.Remove( iData.Key );

                if ( iData.CheckBoxKey != -1 )
                {
                    m_checkBoxesByKey.Remove( iData.CheckBoxKey );
                }
            }

            GTLTreeNode parentNode = gtlSubItem.ParentNode;
            int column = parentNode.SubItems.IndexOf( gtlSubItem );

            m_TreeListControl.TheTreeList.BeginNodeListChange();
            parentNode.SubItems.Remove( gtlSubItem );
            m_TreeListControl.TheTreeList.EndNodeListChange();

            UpdateParentNodeSubItem( parentNode, column );

            m_subItemsByKey.Remove( key );
        }

        public void SetItemValue( int key, string newVal )
        {
            GTLSubItem gtlSubItem;
            if ( !m_subItemsByKey.TryGetValue( key, out gtlSubItem ) )
            {
                LogFactory.ApplicationLog.MessageCtx("WidgetTreeListVisible", "SetItemValue key {0} does not exist", key);
                return;
            }

            string prevVal = gtlSubItem.Text;

            gtlSubItem.Text = newVal;

            GTLTreeNode parentNode = gtlSubItem.ParentNode;
            int column = parentNode.SubItems.IndexOf( gtlSubItem );

            UpdateParentNodeSubItem( parentNode, column );

            TreeListItemData iData = gtlSubItem.Tag as TreeListItemData;
            if ( iData != null )
            {
                if ( iData.PreviousValue == null )
                {
                    iData.PreviousValue = gtlSubItem.Text;
                }
                else
                {
                    if ( gtlSubItem.Text != prevVal )
                    {
                        iData.PreviousValue = prevVal;
                    }
                }

                // update "always visible" edit controls
                Control theControl;
                if ( m_alwaysVisibleEditControls.TryGetValue( iData.Key, out theControl ) )
                {
                    if ( theControl is System.Windows.Forms.ProgressBar )
                    {
                        System.Windows.Forms.ProgressBar bar = theControl as System.Windows.Forms.ProgressBar;
                        if ( bar.Value != Convert.ToInt32( newVal ) )
                        {
                            SetupEditControl( parentNode, column, newVal, theControl );
                        }
                    }
                    else if ( theControl.Text != newVal )
                    {
                        SetupEditControl( parentNode, column, newVal, theControl );
                    }
                }
            }
        }

        public void SetItemCheckBox( int checkBoxKey, WidgetTreeList.CheckBoxState state )
        {
            GTLSubItem gtlSubItem;
            if ( !m_checkBoxesByKey.TryGetValue( checkBoxKey, out gtlSubItem ) )
            {
                LogFactory.ApplicationLog.MessageCtx("WidgetTreeListVisible", "SetItemValue checkBoxKey {0} does not exist", checkBoxKey);
                return;
            }

            switch ( state )
            {
                case WidgetTreeList.CheckBoxState.Indeterminate:
                    gtlSubItem.CheckState = System.Windows.Forms.CheckState.Indeterminate;
                    break;
                case WidgetTreeList.CheckBoxState.Unchecked:
                    gtlSubItem.CheckState = System.Windows.Forms.CheckState.Unchecked;
                    break;
                case WidgetTreeList.CheckBoxState.Checked:
                    gtlSubItem.CheckState = System.Windows.Forms.CheckState.Checked;
                    break;
            }
        }

        public void ValidateNodes()
        {
            foreach ( GTLTreeNode node in m_TreeListControl.TheTreeList.TreeList.Nodes )
            {
                ValidateNode( node );
            }
        }
        #endregion

        #region Private Functions
        private System.Type GetTypeFromItemControlType( WidgetTreeList.ItemControlType itemCtrlType )
        {
            switch ( itemCtrlType )
            {
                case WidgetTreeList.ItemControlType.None:
                    return null;
                case WidgetTreeList.ItemControlType.TextBox:
                    return typeof( GlacialComponents.Controls.GTLCommon.GTextBox );
                case WidgetTreeList.ItemControlType.ComboBox:
                    return typeof( GlacialComponents.Controls.GTLCommon.GComboBox );
                case WidgetTreeList.ItemControlType.DateTimePicker:
                    return typeof( GlacialComponents.Controls.GTLCommon.GDateTimePicker );
                case WidgetTreeList.ItemControlType.NumericUpDown:
                    return typeof( EmbeddedNumericUpDown );
                case WidgetTreeList.ItemControlType.Button:
                    return typeof( System.Windows.Forms.Button );
                case WidgetTreeList.ItemControlType.ProgressBar:
                    return typeof( System.Windows.Forms.ProgressBar );
                case WidgetTreeList.ItemControlType.Slider:
                    return typeof( EmbeddedTrackBar );
                default:
                    return null;
            }
        }

        private void ValidateNode( GTLTreeNode node )
        {
            if ( (node.SubItems.Count == 1) && (node.SubItems[0].Tag == null) )
            {
                TreeListNodeData nData = node.Tag as TreeListNodeData;
                TreeListItemData iData = new TreeListItemData( nData.Key, -1, TreeListItemData.DataType.String, false,
                    node.SubItems[0].Text.IndexOf( "\"" ) > -1 );
                node.SubItems[0].Tag = iData;

                // disable check boxes and controls
                node.SubItems[0].DisableCheckbox = true;
                node.SubItems[0].DisableEmbeddedControl = true;
            }

            foreach ( GTLTreeNode subNode in node.Nodes )
            {
                ValidateNode( subNode );
            }
        }

        private void RemoveNodeKeys( GTLTreeNode gtlTreeNode )
        {
            TreeListNodeData nData = gtlTreeNode.Tag as TreeListNodeData;
            if ( nData != null )
            {
                m_treeNodesByKey.Remove( nData.Key );
            }

            foreach ( GTLSubItem gtlSubItem in gtlTreeNode.SubItems )
            {
                TreeListItemData iData = gtlSubItem.Tag as TreeListItemData;
                if ( iData != null )
                {
                    m_alwaysVisibleEditControls.Remove( iData.Key );

                    m_subItemsByKey.Remove( iData.Key );

                    if ( iData.CheckBoxKey != -1 )
                    {
                        m_checkBoxesByKey.Remove( iData.CheckBoxKey );
                    }
                }
            }

            foreach ( GTLTreeNode subGtlTreeNode in gtlTreeNode.Nodes )
            {
                RemoveNodeKeys( subGtlTreeNode );
            }
        }

        /// <summary>
        /// Fills the text of the edit control appropriately
        /// </summary>
        /// <param name="node"></param>
        /// <param name="column"></param>
        /// <param name="theControl"></param>
        /// <param name="transferHandled"></param>
        /// <returns>false if we should cancel the edit, true if we handled the transfer</returns>
        private bool SetupEditControl( GTLTreeNode node, int column, string text, Control theControl )
        {
            if ( column >= node.SubItems.Count )
            {
                return false;
            }

            GTLSubItem gtlSubItem = node.SubItems[column];
            TreeListItemData iData = gtlSubItem.Tag as TreeListItemData;
            if ( iData == null )
            {
                return false;
            }

            TreeListNodeData nData = node.Tag as TreeListNodeData;
            if ( nData.Type != TreeListNodeData.DataType.Native )
            {
                LogFactory.ApplicationLog.MessageCtx("WidgetTreeListVisible", "TreeList_BeforeCellEdit node {0} subitem {1} cannot edit non-native types", node.Text, column);
                return false;
            }

            // the control should always know what subItem it is editing
            theControl.Tag = iData;

            // setup specific edit controls
            if ( (theControl is GComboBox) && (iData.ComboBoxItems != null) )
            {
                GComboBox comboBox = theControl as GComboBox;
                comboBox.Items.Clear();
                comboBox.Items.AddRange( iData.ComboBoxItems );
                comboBox.SelectedText = text;
            }
            else if ( (theControl is GDateTimePicker) && (iData.ComboBoxItems != null) )
            {
                GDateTimePicker gDateTimePicker = theControl as GDateTimePicker;
                WidgetTreeList.DateTimeFormat format = (WidgetTreeList.DateTimeFormat)Int32.Parse( iData.ComboBoxItems[0] );
                switch ( format )
                {
                    case WidgetTreeList.DateTimeFormat.Long:
                        gDateTimePicker.Format = DateTimePickerFormat.Long;
                        break;
                    case WidgetTreeList.DateTimeFormat.Short:
                        gDateTimePicker.Format = DateTimePickerFormat.Short;
                        break;
                    case WidgetTreeList.DateTimeFormat.Time:
                        gDateTimePicker.Format = DateTimePickerFormat.Time;
                        gDateTimePicker.ShowUpDown = true;
                        break;
                }

                gDateTimePicker.MinDate = DateTime.Parse( iData.ComboBoxItems[1] );
                gDateTimePicker.MaxDate = DateTime.Parse( iData.ComboBoxItems[2] );
            }
            else if ( (theControl is EmbeddedNumericUpDown) && (iData.ComboBoxItems != null) )
            {
                EmbeddedNumericUpDown numericControl = theControl as EmbeddedNumericUpDown;
                if ( iData.Type == TreeListItemData.DataType.Int )
                {
                    numericControl.DecimalPlaces = 0;
                }
                else
                {
                    numericControl.DecimalPlaces = 6;
                }

                numericControl.Minimum = Convert.ToDecimal( iData.ComboBoxItems[0] );
                numericControl.Maximum = Convert.ToDecimal( iData.ComboBoxItems[1] );
                numericControl.Increment = Convert.ToDecimal( iData.ComboBoxItems[2] );
                numericControl.Text = text;
            }
            else if ( theControl is System.Windows.Forms.Button )
            {
                System.Windows.Forms.Button button = theControl as System.Windows.Forms.Button;
                button.Text = text;
                button.Click += new EventHandler( EditControl_Click );
            }
            else if ( (theControl is System.Windows.Forms.ProgressBar) && (iData.ComboBoxItems != null) )
            {
                System.Windows.Forms.ProgressBar bar = theControl as System.Windows.Forms.ProgressBar;
                bar.Minimum = Convert.ToInt32( iData.ComboBoxItems[0] );
                bar.Maximum = Convert.ToInt32( iData.ComboBoxItems[1] );
                bar.Value = Convert.ToInt32( text );
            }
            else if ( (theControl is EmbeddedTrackBar) && (iData.ComboBoxItems != null) )
            {
                EmbeddedTrackBar trackBar = theControl as EmbeddedTrackBar;
                trackBar.IsInteger = iData.Type == TreeListItemData.DataType.Int;
                trackBar.Min = float.Parse( iData.ComboBoxItems[0] );
                trackBar.Max = float.Parse( iData.ComboBoxItems[1] );
                trackBar.Text = text;
            }
            else if ( iData.Type == TreeListItemData.DataType.String )
            {
                // remove outside quotes for string edit control
                if ( iData.HasQuotes && text.StartsWith( "\"" ) && text.EndsWith( "\"" ) )
                {
                    theControl.Text = text.Substring( 1, text.Length - 2 );
                }
                else
                {
                    theControl.Text = text;
                }
            }
            else
            {
                theControl.Text = text;
            }

            return true;
        }

        /// <summary>
        /// Fills the GLTSubItem after an edit has been made.  Also, messages the game when a value changes.
        /// </summary>
        /// <param name="node"></param>
        /// <param name="column"></param>
        /// <param name="theControl"></param>
        /// <param name="transferHandled"></param>
        /// <returns>true if we should change the display value</returns>
        private bool ReadEditControl( GTLTreeNode node, int column, string text, Control theControl )
        {
            if ( column >= node.SubItems.Count )
            {
                return false;
            }

            GTLSubItem gtlSubItem = node.SubItems[column];
            TreeListItemData iData = gtlSubItem.Tag as TreeListItemData;
            if ( iData == null )
            {
                return false;
            }

            bool success = false;

            string originalText = gtlSubItem.Text;
            bool modify = false;

            switch ( iData.Type )
            {
                case TreeListItemData.DataType.Float:
                    // see if we have a float.  make sure it has a decimal point.  remove trailing 'f' as in '5.46f'
                    success = GetFloatString( ref text );
                    if ( success )
                    {
                        if ( GetFloatString( ref originalText ) )
                        {
                            modify = text != originalText;
                        }
                    }

                    /* screw it.  nevermind
                    if ( success )
                    {
                        // if this is a float that is a child of a vector, we need to send the message to the vector instead
                        GTLTreeNode parentNode = node.ParentNode;
                        if ( (parentNode != null) && (column < parentNode.SubItems.Count) )
                        {
                            int index = parentNode.Nodes.IndexOf( node );

                            // only the first 3 children are part of the vector (even though we really shouldn't have any more beyond that,
                            // but we don't prevent the user from adding additional children).
                            if ( index < 3 )
                            {
                                GTLSubItem parentSubItem = parentNode.SubItems[column];
                                TreeListItemData parentIData = parentSubItem.Tag as TreeListItemData;
                                if ( (parentIData != null) && (parentIData.Type == TreeListItemData.DataType.Vector) )
                                {
                                    // build the vector string with our new coordinate value
                                    System.Text.StringBuilder vectorStr = new System.Text.StringBuilder( "<<" );
                                    for (int i = 0; i < 3; ++i)
                                    {
                                        if ( i == index )
                                        {
                                            vectorStr.Append( text );
                                        }
                                        else
                                        {
                                            vectorStr.Append( parentNode.Nodes[i].SubItems[column].Text );
                                        }

                                        vectorStr.Append( "," );
                                    }

                                    vectorStr.Remove( vectorStr.Length-1, 1 );	// take off the last comma
                                    vectorStr.Append( ">>" );
									
                                    // now setup for sending the message
                                    text = vectorStr.ToString();
                                    iData = parentIData;
                                }
                            }
                        }
                    }
                    */
                    break;

                case TreeListItemData.DataType.Int:
                    // see if we really have an integer
                    success = GetIntString( ref text );
                    if ( success )
                    {
                        if ( GetIntString( ref originalText ) )
                        {
                            modify = text != originalText;
                        }
                    }
                    break;

                case TreeListItemData.DataType.String:
                    {
                        if ( theControl is GlacialComponents.Controls.GTLCommon.GDateTimePicker )
                        {
                            //success = GetDateTimeString( ref text );
                            if ( iData.HasQuotes )
                            {
                                success = GetQuotedString( ref text );
                                if ( success )
                                {
                                    if ( GetQuotedString( ref originalText ) )
                                    {
                                        modify = text != originalText;
                                    }
                                }
                            }
                            else
                            {
                                success = true;
                                modify = text != originalText;
                            }
                        }
                        else
                        {
                            // put back quotes after edit
                            if ( iData.HasQuotes )
                            {
                                success = GetQuotedString( ref text );
                                if ( success )
                                {
                                    if ( GetQuotedString( ref originalText ) )
                                    {
                                        modify = text != originalText;
                                    }
                                }
                            }
                            else
                            {
                                success = true;
                                modify = text != originalText;
                            }
                        }
                        break;
                    }

                case TreeListItemData.DataType.Bool:
                    // see if we really have something that qualifies as a boolean
                    success = GetBoolString( ref text );
                    if ( success )
                    {
                        if ( GetBoolString( ref originalText ) )
                        {
                            modify = text != originalText;
                        }
                    }
                    break;

                case TreeListItemData.DataType.Vector:
                    success = GetVectorString( ref text );
                    if ( success )
                    {
                        if ( GetVectorString( ref originalText ) )
                        {
                            modify = text != originalText;
                        }
                    }
                    break;

                default:
                    break;
            }

            if ( modify && success && (m_Widget != null) )
            {
                // for always visible items, save the previous value here
                if ( m_alwaysVisibleEditControls.ContainsKey( iData.Key ) )
                {
                    iData.PreviousValue = originalText;
                }

                m_Widget.ModifyItem( iData.Key, iData.Type, text );
            }

            return success;
        }

        private void UpdateParentNodeSubItem( GTLTreeNode parentNode, int column )
        {
            while ( parentNode != null )
            {
                TreeListNodeData nData = parentNode.Tag as TreeListNodeData;
                System.Text.StringBuilder val = null;

                if ( nData.Type == TreeListNodeData.DataType.Struct )
                {
                    foreach ( GTLTreeNode gtlSubTreeNode in parentNode.Nodes )
                    {
                        if ( column >= gtlSubTreeNode.SubItems.Count )
                        {
                            continue;
                        }

                        GTLSubItem gtlSubNodeItem = gtlSubTreeNode.SubItems[column];
                        TreeListItemData subIData = gtlSubNodeItem.Tag as TreeListItemData;
                        if ( subIData == null )
                        {
                            continue;
                        }

                        if ( subIData.UpdateParentSubItemsAfterEdit )
                        {
                            if ( val == null )
                            {
                                val = new System.Text.StringBuilder( "{ " );
                            }

                            val.Append( gtlSubNodeItem.Text );
                            val.Append( " " );
                        }
                    }

                    if ( val != null )
                    {
                        val.Append( "}" );
                    }
                }
                else if ( nData.Type == TreeListNodeData.DataType.Vector )
                {
                    string[] vectorDelims = new string[] { "<<", ",", ",", ">>" };
                    if ( parentNode.Nodes.Count == 3 )
                    {
                        for ( int i = 0; i < 3; ++i )
                        {
                            if ( column >= parentNode.Nodes[i].SubItems.Count )
                            {
                                continue;
                            }

                            GTLSubItem gtlSubItem = parentNode.Nodes[i].SubItems[column];
                            TreeListItemData iData = gtlSubItem.Tag as TreeListItemData;
                            if ( iData == null )
                            {
                                continue;
                            }

                            if ( iData.UpdateParentSubItemsAfterEdit )
                            {
                                if ( val == null )
                                {
                                    val = new System.Text.StringBuilder();
                                }

                                val.Append( vectorDelims[i] );
                                val.Append( gtlSubItem.Text );
                            }
                        }

                        if ( val != null )
                        {
                            val.Append( vectorDelims[3] );
                        }
                    }
                }

                if ( (val != null) && (column < parentNode.SubItems.Count) )
                {
                    parentNode.SubItems[column].Text = val.ToString();
                }

                parentNode = parentNode.ParentNode;
            }
        }

        private bool GetFloatString( ref string t )
        {
            try
            {
                double val = Convert.ToDouble( t );
                t = val.ToString();

                if ( t.IndexOf( '.' ) == -1 )
                {
                    t += ".0";
                }

                return true;
            }
            catch
            {
                if ( t.EndsWith( "f" ) )
                {
                    t = t.Remove( t.IndexOf( "f" ), 1 );
                    return GetFloatString( ref t );
                }
                else
                {
                    return false;
                }
            }
        }

        private bool GetIntString( ref string t )
        {
            try
            {
                double val = Convert.ToInt32( t );
                t = val.ToString();

                return true;
            }
            catch
            {
                return false;
            }
        }

        private bool GetQuotedString( ref string t )
        {
            t = "\"" + t + "\"";
            return true;
        }

        private bool GetBoolString( ref string t )
        {
            if ( (t.ToLower() == "true") || (t.ToLower() == "t") || (t == "1") )
            {
                t = "true";
                return true;
            }
            else if ( (t.ToLower() == "false") || (t.ToLower() == "f") || (t == "0") )
            {
                t = "false";
                return true;
            }

            return false;
        }

        private bool GetVectorString( ref string t )
        {
            string[] split = t.Split( ',' );

            if ( split.Length != 3 )
            {
                return false;
            }

            if ( split[0].StartsWith( "<<" ) )
            {
                split[0] = split[0].Substring( 2, split[0].Length - 2 );
            }

            if ( split[2].EndsWith( ">>" ) )
            {
                split[2] = split[2].Substring( 0, split[2].Length - 2 );
            }

            for ( int i = 0; i < 3; ++i )
            {
                if ( !GetFloatString( ref split[i] ) )
                {
                    return false;
                }
            }

            t = "<<" + split[0] + "," + split[1] + "," + split[2] + ">>";
            return true;
        }

        private bool GetDateTimeString( ref string t, WidgetTreeList.DateTimeFormat format )
        {
            // FIXME
            return true;

            /*
            System.DateTime theDate = System.DateTime.Parse( t );
            System.Text.StringBuilder s = new System.Text.StringBuilder();
            switch ( format )
            {
                case DateTimeFormat.MMDDYYSlashes:
                {
                    break;
                }

                case DateTimeFormat.MMDDYYHyphens:
                    break;

                case DateTimeFormat.MMDDYYPeriods:
                    break;

                case DateTimeFormat.MMDDYYYYSlashes:
                    break;

                case DateTimeFormat.MMDDYYYYHyphens:
                    break;

                case DateTimeFormat.MMDDYYYYPeriods:
                    break;

                case DateTimeFormat.MonthDayYear:
                    break;

                case DateTimeFormat.DayOfWeekMonthDayYear:
                    return true;

                default:
                    return false;
            }
            */
        }

        private void UpdateAlwaysVisibleItem( Control theControl )
        {
            if ( theControl == null )
            {
                return;
            }

            TreeListItemData iData = theControl.Tag as TreeListItemData;
            if ( iData == null )
            {
                return;
            }

            GTLSubItem gtlSubItem;
            if ( !m_subItemsByKey.TryGetValue( iData.Key, out gtlSubItem ) )
            {
                return;
            }

            if ( !ReadEditControl( gtlSubItem.ParentNode, gtlSubItem.ParentNode.SubItems.IndexOf( gtlSubItem ), theControl.Text, theControl ) )
            {
                // reset the control's text because it's not valid
                if ( theControl is System.Windows.Forms.ProgressBar )
                {
                    System.Windows.Forms.ProgressBar bar = theControl as System.Windows.Forms.ProgressBar;
                    bar.Value = Convert.ToInt32( gtlSubItem.Text );
                }
                else
                {
                    theControl.Text = gtlSubItem.Text;
                }
            }
        }
        #endregion

        #region Event Handlers
        private void TreeList_BeforeCheck( object source, GTLCheckChangeEventArgs args )
        {
            if ( args.Column >= args.TreeNode.SubItems.Count )
            {
                args.Cancel = true;
                return;
            }

            GTLSubItem gtlSubItem = args.TreeNode.SubItems[args.Column];
            TreeListItemData iData = gtlSubItem.Tag as TreeListItemData;
            if ( iData == null )
            {
                args.Cancel = true;
                return;
            }

            if ( args.ControlBased )
            {
                // here we have to determine the resulting check state ourselves
                WidgetTreeList.CheckBoxState newState = WidgetTreeList.CheckBoxState.Indeterminate;
                if ( gtlSubItem.CheckState == System.Windows.Forms.CheckState.Unchecked )
                {
                    newState = WidgetTreeList.CheckBoxState.Checked;
                }
                else if ( gtlSubItem.CheckState == System.Windows.Forms.CheckState.Checked )
                {
                    GTLColumn gtlCol = m_TreeListControl.TheTreeList.Columns[args.Column];
                    if ( gtlCol.CheckBoxStyle == GTLCheckBoxTypes.TwoState )
                    {
                        newState = WidgetTreeList.CheckBoxState.Unchecked;
                    }
                    else
                    {
                        newState = WidgetTreeList.CheckBoxState.Indeterminate;
                    }
                }
                else
                {
                    newState = WidgetTreeList.CheckBoxState.Unchecked;
                }

                m_Widget.ItemChecked( iData.CheckBoxKey, newState );

                // We want game engine to send back the changes, so cancel
                args.Cancel = true;
            }
        }

        private void TreeList_BeforeCellEdit( object source, GlacialComponents.Controls.GlacialTreeList.GTLBeforeEditEventArgs args )
        {
            if ( !SetupEditControl( args.TreeNode, args.Column, args.Text, args.Control ) )
            {
                args.Cancel = true;
                return;
            }

            args.Control.KeyDown += new KeyEventHandler( EditControl_UndoKeyDown );
        }

        private void TreeList_AfterCellEdit( object source, GlacialComponents.Controls.GlacialTreeList.GTLEmbeddedControlEventArgs args )
        {
            if ( !args.Cancel )
            {
                string prevValue = args.TreeNode.SubItems[args.Column].Text;

                if ( ReadEditControl( args.TreeNode, args.Column, args.Text, args.Control ) )
                {
                    // remember the old value for undo
                    if ( (args.Control.Tag != null) && (args.Control.Tag is TreeListItemData) )
                    {
                        TreeListItemData d = args.Control.Tag as TreeListItemData;
                        d.PreviousValue = prevValue;
                    }
                }

                args.Cancel = true;
            }
        }

        private void TreeList_EmbeddedControlShow( object source, GlacialComponents.Controls.GlacialTreeList.GTLBeforeEditEventArgs args )
        {
            // set up the text
            if ( !SetupEditControl( args.TreeNode, args.Column, args.Text, args.Control ) )
            {
                args.Cancel = true;
                return;
            }

            args.Control.KeyDown += new KeyEventHandler( EditControl_UndoKeyDown );

            // set up the control's event handler(s)
            if ( args.Control is GlacialComponents.Controls.GTLCommon.GTextBox )
            {
                args.Control.KeyDown += new KeyEventHandler( EditControl_KeyDown );
                args.Control.Leave += new EventHandler( EditControl_Leave );
            }
            else if ( args.Control is GlacialComponents.Controls.GTLCommon.GComboBox )
            {
                GlacialComponents.Controls.GTLCommon.GComboBox gComboBox = args.Control as GlacialComponents.Controls.GTLCommon.GComboBox;
                gComboBox.SelectedIndexChanged += new EventHandler( EditControl_SelectedIndexChanged );
            }
            else if ( args.Control is GlacialComponents.Controls.GTLCommon.GDateTimePicker )
            {
                GlacialComponents.Controls.GTLCommon.GDateTimePicker gDateTimePicker = args.Control as GlacialComponents.Controls.GTLCommon.GDateTimePicker;
                gDateTimePicker.ValueChanged += new EventHandler( EditControl_ValueChanged );
            }
            else if ( args.Control is EmbeddedNumericUpDown )
            {
                EmbeddedNumericUpDown numeric = args.Control as EmbeddedNumericUpDown;
                numeric.ValueChanged += new EventHandler( EditControl_ValueChanged );
                numeric.Leave += new EventHandler( EditControl_Leave );
            }
            else if ( args.Control is EmbeddedTrackBar )
            {
                EmbeddedTrackBar trackBar = args.Control as EmbeddedTrackBar;
                trackBar.ValueChanged += new EventHandler( EditControl_ValueChanged );
            }

            // remember what controls are always visible
            GTLSubItem gtlSubItem = args.TreeNode.SubItems[args.Column];
            TreeListItemData iData = gtlSubItem.Tag as TreeListItemData;
            if ( (iData != null) && !m_alwaysVisibleEditControls.ContainsKey( iData.Key ) )
            {
                // initialize previous value for undo
                iData.PreviousValue = args.Control.Text;

                m_alwaysVisibleEditControls.Add( iData.Key, args.Control );
            }
        }

        private void TreeList_EmbeddedControlHide( object source, GlacialComponents.Controls.GlacialTreeList.GTLEmbeddedControlEventArgs args )
        {
            if ( !args.Cancel )
            {
                string prevValue = args.TreeNode.SubItems[args.Column].Text;

                // read back the text
                if ( ReadEditControl( args.TreeNode, args.Column, args.Text, args.Control ) )
                {
                    // remember the old value for undo
                    if ( (args.Control.Tag != null) && (args.Control.Tag is TreeListItemData) )
                    {
                        TreeListItemData d = args.Control.Tag as TreeListItemData;
                        d.PreviousValue = prevValue;
                    }
                }

                args.Cancel = true;

                GTLSubItem gtlSubItem = args.TreeNode.SubItems[args.Column];
                TreeListItemData iData = gtlSubItem.Tag as TreeListItemData;
                if ( iData != null )
                {
                    m_alwaysVisibleEditControls.Remove( iData.Key );
                }
            }
        }

        private void TreeList_NodeDrag( object source, GlacialComponents.Controls.GlacialTreeList.GTLEventArgs args )
        {
            GlacialTreeList treelist = source as GlacialTreeList;
            if ( treelist != null )
            {
                treelist.DoDragDrop( args, DragDropEffects.Move );
            }
        }

        private void TreeList_DragDrop( object sender, System.Windows.Forms.DragEventArgs e )
        {
            if ( e.Data.GetDataPresent( "GlacialComponents.Controls.GlacialTreeList.GTLEventArgs", true ) )
            {
                GlacialTreeList treelist = sender as GlacialTreeList;
                if ( treelist != null )
                {
                    GTLEventArgs args = (GTLEventArgs)e.Data.GetData( "GlacialComponents.Controls.GlacialTreeList.GTLEventArgs" );
                    System.Drawing.Point pt = treelist.PointToClient( new System.Drawing.Point( e.X, e.Y ) );
                    GTLTreeNode destinationNode = treelist.GetNodeAt( pt.X, pt.Y );

                    // only allow drops to happen at the root level
                    if ( destinationNode == null )
                    {
                        TreeListNodeData nData = args.TreeNode.Tag as TreeListNodeData;
                        if ( (nData != null) && (m_Widget != null) )
                        {
                            m_Widget.DragDropNotification( nData.Key );
                        }
                    }
                }
            }

            e.Effect = DragDropEffects.None;
        }

        private void TreeList_DragEnter( object sender, System.Windows.Forms.DragEventArgs e )
        {
            e.Effect = DragDropEffects.Move;
        }

        private void TreeList_DragOver( object sender, System.Windows.Forms.DragEventArgs e )
        {
            if ( e.Data.GetDataPresent( "GlacialComponents.Controls.GlacialTreeList.GTLEventArgs", true ) )
            {
                GlacialTreeList treelist = sender as GlacialComponents.Controls.GlacialTreeList.GlacialTreeList;
                if ( treelist != null )
                {
                    GTLEventArgs args = (GTLEventArgs)e.Data.GetData( "GlacialComponents.Controls.GlacialTreeList.GTLEventArgs" );
                    System.Drawing.Point pt = treelist.PointToClient( new System.Drawing.Point( e.X, e.Y ) );
                    GTLTreeNode destinationNode = treelist.GetNodeAt( pt.X, pt.Y );

                    // only allow drops to happen at the root level
                    if ( destinationNode == null )
                    {
                        e.Effect = DragDropEffects.Move;
                    }
                    else
                    {
                        e.Effect = DragDropEffects.None;
                    }
                }
            }
            else
            {
                e.Effect = DragDropEffects.None;
            }
        }

        private void TreeList_CopyKeyDown( object sender, KeyEventArgs e )
        {
            if ( e.Control && (e.KeyCode == System.Windows.Forms.Keys.C) )
            {
                if ( m_TreeListControl.TheTreeList.SelectedNodes.Count == 1 )
                {
                    GTLTreeNode node = m_TreeListControl.TheTreeList.SelectedNodes[0];
                    TreeListNodeData nData = node.Tag as TreeListNodeData;
                    if ( nData != null )
                    {
                        DataObject data = new DataObject( TreeListNodeData_DataFormat.Name, nData );
                        Clipboard.SetDataObject( data );
                    }
                }
            }
        }

        private void TreeList_PasteKeyDown( object sender, KeyEventArgs e )
        {
            if ( e.Control && (e.KeyCode == System.Windows.Forms.Keys.V) )
            {
                // Retrieves the data from the clipboard.
                IDataObject data = Clipboard.GetDataObject();
                if ( (data != null) && (data.GetData( TreeListNodeData_DataFormat.Name ) != null) )
                {
                    TreeListNodeData nData = data.GetData( TreeListNodeData_DataFormat.Name ) as TreeListNodeData;
                    if ( (nData != null) && (m_Widget != null) )
                    {
                        m_Widget.DragDropNotification( nData.Key );
                    }
                }
            }
        }

        private void TreeList_DeleteKeyDown( object sender, KeyEventArgs e )
        {
            if ( e.KeyCode == System.Windows.Forms.Keys.Delete )
            {
                if ( m_TreeListControl.TheTreeList.SelectedNodes.Count == 1 )
                {
                    GTLTreeNode node = m_TreeListControl.TheTreeList.SelectedNodes[0];
                    TreeListNodeData nData = node.Tag as TreeListNodeData;
                    if ( (nData != null) && (m_Widget != null) )
                    {
                        m_Widget.DeleteNode( nData.Key );
                    }
                }
            }
        }

        private void EditControl_KeyDown( object sender, KeyEventArgs e )
        {
            if ( e.KeyCode == Keys.Enter )
            {
                UpdateAlwaysVisibleItem( sender as Control );
            }
        }

        private void EditControl_UndoKeyDown( object sender, KeyEventArgs e )
        {
            if ( e.Control && (e.KeyCode == Keys.Z) )
            {
                // undo
                if ( sender is Control )
                {
                    Control ctrl = sender as Control;
                    if ( ctrl.Tag != null )
                    {
                        if ( ctrl.Tag is TreeListItemData )
                        {
                            TreeListItemData iData = ctrl.Tag as TreeListItemData;
                            if ( iData.PreviousValue != null )
                            {
                                if ( ctrl is System.Windows.Forms.ProgressBar )
                                {
                                    System.Windows.Forms.ProgressBar bar = ctrl as System.Windows.Forms.ProgressBar;

                                    int prevVal = Convert.ToInt32( iData.PreviousValue );

                                    iData.PreviousValue = bar.Value.ToString();

                                    bar.Value = prevVal;
                                }
                                else
                                {
                                    string prevVal = iData.PreviousValue;

                                    iData.PreviousValue = ctrl.Text;

                                    ctrl.Text = prevVal;
                                }
                            }
                        }
                    }
                }

                e.SuppressKeyPress = true;
            }
        }

        private void EditControl_Leave( object sender, EventArgs e )
        {
            UpdateAlwaysVisibleItem( sender as Control );
        }

        private void EditControl_SelectedIndexChanged( object sender, EventArgs e )
        {
            UpdateAlwaysVisibleItem( sender as Control );
        }

        private void EditControl_ValueChanged( object sender, EventArgs e )
        {
            if ( sender is EmbeddedNumericUpDown )
            {
                EmbeddedNumericUpDown numeric = sender as EmbeddedNumericUpDown;
                numeric.Text = numeric.Value.ToString();
            }

            UpdateAlwaysVisibleItem( sender as Control );
        }

        private void EditControl_Click( object sender, EventArgs e )
        {
            Control theControl = sender as Control;
            if ( theControl == null )
            {
                return;
            }

            TreeListItemData iData = theControl.Tag as TreeListItemData;
            if ( iData == null )
            {
                return;
            }

            if ( m_Widget != null )
            {
                m_Widget.ButtonClicked( iData.Key );
            }
        }
        #endregion
    }

}
