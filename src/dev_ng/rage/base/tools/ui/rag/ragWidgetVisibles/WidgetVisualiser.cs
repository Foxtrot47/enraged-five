﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows.Forms;
using System.Collections;
using ragCore;

namespace ragWidgets
{
    public delegate WidgetVisible ShowWidgetVisibleDelegate( Widget widget, IWidgetView widgetView, Control parent, string hashKey, bool forceOpen, ToolTip tooltip);
    public delegate void HideWidgetVisibleDelegate( Widget widget, string hashKey );

    public abstract class WidgetVisualiser
    {

        public static BankRemotePacketProcessor PacketProcessor { get; set; }

        private static Dictionary<Type, Type> m_WidgetToVisibleMap = new Dictionary<Type, Type>();
        private static Dictionary<Type, ShowWidgetVisibleDelegate> m_ShowHandlers = new Dictionary<Type, ShowWidgetVisibleDelegate>();
        private static Dictionary<Type, HideWidgetVisibleDelegate> m_HideHandlers = new Dictionary<Type, HideWidgetVisibleDelegate>();

        // This is a hashtable because it can contain both WidgetVisible and a List<>... ugh
        private static Dictionary<Widget, Hashtable> s_VisibleMap = new Dictionary<Widget, Hashtable>();

        ///////////////////////////////////////////////////////////////////////////////// 
        /// Show visible handlers
        /// 

        /// <summary>
        /// This function instantiates a visible instance of this widget.  It should always call base.MakeVisible(),
        /// and the argument to MakeVisible() should be a new instance of the visible.
        /// </summary>
        /// <param name="widgetView">The widget view that the visible will be displayed in</param>
        /// <param name="parent">The parent control that the visible gets attached to</param>
        /// <param name="hashKey">the unique hash key for this visible instance</param>
        /// <param name="forceOpen">specifies if a group should be forced open (only used for group type widgets)</param>
        /// <returns></returns>
        public static WidgetVisible ShowVisible( Widget widget, IWidgetView widgetView, Control parent, string hashKey, bool forceOpen, ToolTip tooltip )
        {
            ShowWidgetVisibleDelegate handler;
            if ( !m_ShowHandlers.TryGetValue( widget.GetType(), out handler ) )
            {
                string msg = string.Format( "ShowVisible handler for {0} ( type='{1}' ) not implemented and/or registered.", widget.Title, widget.GetType().ToString() );
                throw new NotImplementedException( msg );
            }

            WidgetVisible visible = handler( widget, widgetView, parent, hashKey, forceOpen, tooltip );
            return visible;
        }


        public static void AddHandlerShowVisible( Type widgetType, ShowWidgetVisibleDelegate handler )
        {
            m_ShowHandlers[widgetType] = handler;
        }

        public static void AddDefaultHandlerShowVisible( Type widgetType, Type widgetVisibleType )
        {
            m_ShowHandlers[widgetType] = WidgetDefaultShowVisible;
            m_WidgetToVisibleMap[widgetType] = widgetVisibleType;
        }


        private static WidgetVisible WidgetDefaultShowVisible( Widget widget, IWidgetView widgetView, Control parent, string hashKey, bool forceOpen, ToolTip tooltip )
        {
            Type widgetType = widget.GetType();
            Type visibleType = m_WidgetToVisibleMap[widgetType];

            object[] constructorParams = new object[] { widgetView, widget, parent, tooltip };
            WidgetVisible visible = (WidgetVisible)Activator.CreateInstance( visibleType, constructorParams );
            WidgetShowVisibleSetup( widget, visible, hashKey );
            return visible;
        }

        public static void WidgetShowVisibleSetup( Widget widget, WidgetVisible visible, string hashKey )
        {
            RegisterVisible( widget, visible, hashKey );
            if (!widget.Enabled)
            {
                visible.Control.Enabled = false;
            }
            visible.FillColor = widget.FillColor;
        }

        public static void RegisterVisible( Widget widget, object visible, string hashKey )
        {
            if ( !s_VisibleMap.ContainsKey( widget ) )
            {
                s_VisibleMap[widget] = new Hashtable();
            }
            s_VisibleMap[widget][hashKey] = visible;
        }

        public static void UnregisterVisible( Widget widget, string hashKey )
        {
            s_VisibleMap[widget].Remove( hashKey );
            if ( s_VisibleMap[widget].Count == 0 )
            {
                s_VisibleMap.Remove( widget );
            }
        }

        public static Hashtable GetVisibleHash( Widget widget )
        {
            // Need to create it if it's not there...
            if (!s_VisibleMap.ContainsKey(widget))
            {
                s_VisibleMap[widget] = new Hashtable();
            }

            return s_VisibleMap[widget];
        }

        ///////////////////////////////////////////////////////////////////////////////// 
        /// Hide visible handlers


        /// <summary>
        /// This function hides the widget's widgetvisible and removes references to it.
        /// </summary>
        /// <param name="hashKey">the unique hash key for this visible instance</param>
        /// <returns></returns>

        public static void HideVisible( Widget widget, string hashKey )
        {
            HideWidgetVisibleDelegate handler;
            if ( !m_HideHandlers.TryGetValue( widget.GetType(), out handler ) )
            {
                string msg = string.Format( "HideVisible handler for {0} ( type='{1}' ) not implemented and/or registered.", widget.Title, widget.GetType().ToString() );
                throw new NotImplementedException( msg );
            }

            handler( widget, hashKey );
        }


        public static void AddHandlerHideVisible( Type widgetType, HideWidgetVisibleDelegate handler )
        {
            m_HideHandlers[widgetType] = handler;
        }

        public static void AddDefaultHandlerHideVisible( Type widgetType )
        {
            m_HideHandlers[widgetType] = WidgetDefaultHideVisible;
        }


        protected static void WidgetDefaultHideVisible( Widget widget, string hashKey )
        {
            WidgetVisible visible = FindVisible(widget, hashKey);
            if ( visible != null )
            {
                visible.Remove();
                visible.Destroy();
                Debug.Assert( s_VisibleMap[widget].ContainsKey( hashKey ), "m_VisibleHash.Contains( " + hashKey + " )" );

                UnregisterVisible( widget, hashKey );
            }
        }
        /////////////////////////////////////////////////


        public static void RemoveHandlers( Type widgetType )
        {
            m_ShowHandlers.Remove( widgetType );
            m_HideHandlers.Remove( widgetType );
            if ( m_WidgetToVisibleMap.ContainsKey( widgetType ) )
            {
                m_WidgetToVisibleMap.Remove( widgetType );
            }
        }

        public static WidgetVisible FindVisible( Widget widget, string hashKey )
        {
            if (!s_VisibleMap.ContainsKey(widget))
            {
                return null;
            }
            
            if (!s_VisibleMap[widget].ContainsKey(hashKey))
            {
                return null;
            }

            return s_VisibleMap[widget][hashKey] as WidgetVisible;
        }

    } // class

}
