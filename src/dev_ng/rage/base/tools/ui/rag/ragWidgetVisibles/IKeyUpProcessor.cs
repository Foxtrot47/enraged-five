﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ragWidgets
{
    public delegate bool ProcessKeyUpDelegate( System.Windows.Forms.KeyEventArgs e );

    public interface IKeyUpProcessor
    {
        bool ProcessKeyUp( System.Windows.Forms.KeyEventArgs e );

        void AddProcessKeyUpEvent( object key, ProcessKeyUpDelegate theDelegate );
        void RemoveProcessKeyUpEvent( object key );
    }
}
