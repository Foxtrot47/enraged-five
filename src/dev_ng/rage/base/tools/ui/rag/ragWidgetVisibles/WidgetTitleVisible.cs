using System;
using System.Windows.Forms;

using ragCore;

namespace ragWidgets
{
	/// <summary>
	/// Summary description for WidgetTitleVisible.
	/// </summary>
	public class WidgetTitleVisible : WidgetVisible
	{
        public WidgetTitleVisible(IWidgetView widgetView, WidgetTitle title, Control parent, ToolTip tooltip) 
			: base(widgetView,parent,title)
		{
			m_Widget=title;

			m_Label = new System.Windows.Forms.Label();
			//m_Label.Dock = System.Windows.Forms.DockStyle.Bottom;
			m_Label.Location = new System.Drawing.Point(0, 0);
			m_Label.Name = title.String;
			m_Label.TabIndex = 0;
			m_Label.Text = title.String;
			m_Label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            m_Label.Enabled = !m_Widget.IsReadOnly && m_Widget.Enabled;

			parent.Controls.Add(m_Label);
			
			AddMouseEnter(m_Label);
			SetDragDropHandlers(m_Label);
		}

		public override Control Control 
		{
			get
			{
				return m_Label;
			}
		}

		public override Control DragDropControl
		{
			get {return m_Label;}
		}

		public override void Remove()
		{
			m_Parent.Controls.Remove(m_Label);
			base.Remove();
		}

		public override void Destroy()
		{
			base.Destroy();
            m_Widget = null;
            m_Label.Dispose();
			m_Label=null;
		}

		public override void UpdatePersist(bool persist) {}

		public override Widget Widget {get {return m_Widget;}}

		private System.Windows.Forms.Label m_Label;
		private WidgetTitle m_Widget;
	}

}
