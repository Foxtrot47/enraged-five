using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

using ragCore;
using ragUi;

namespace ragWidgets
{
    public class WidgetVectorVisible : WidgetControlBaseVisible
    {
        public WidgetVectorVisible( IWidgetView widgetView, WidgetVector widget, Control parent, ToolTip tooltip )
            : base( widgetView, parent, widget )
        {
            m_widgetVector = widget;

            m_controlVector = new ControlVector();

            m_controlVector.BeginInit();
            m_controlVector.Text = widget.Title;
            m_controlVector.NumComponents = widget.NumComponents;
            m_controlVector.SetRange( widget.Minimum, widget.Maximum );
            m_controlVector.SetStep( widget.Step );
            AddEventHandlers();
            m_controlVector.UpdateValueFromRemote( m_widgetVector.Value );
            m_controlVector.IsReadOnly = widget.IsReadOnly;
            m_controlVector.EndInit();

            FinishCreation(tooltip);
            if (Widget.IsModified())
            {
                UpdateModifiedIndication();
            }
        }

        #region Variables
        private WidgetVector m_widgetVector;
        private ControlVector m_controlVector;
        private List<EventHandler> m_eventHandlers = new List<EventHandler>();
        private List<ControlVector.ParameterizedOperationEventHandler> m_vectorEventHandlers = new List<ControlVector.ParameterizedOperationEventHandler>();
        private delegate void UpdateInvoke(float[] inValue);
        #endregion

        protected override void widget_ValueChanged( object sender, EventArgs e )
        {
            WidgetVector widget = (WidgetVector)sender;
            UpdateValueFromRemote( widget.Value );
        }

        public void control_ValueChanged( object sender, EventArgs e )
        {
            ControlVector ctrl = sender as ControlVector;

            float[] vector = new float[] { (float)ctrl.Vector[0], (float)ctrl.Vector[1], (float)ctrl.Vector[2], (float)ctrl.Vector[3] };
            m_widgetVector.Value = vector;
        }

        #region Overrides
        public override ragUi.ControlBase ControlBase
        {
            get
            {
                return m_controlVector;
            }
        }

        public override Widget Widget 
        { 
            get 
            {
                return m_widgetVector; 
            } 
        }

        public override void Remove()
        {
            base.Remove();

            RemoveEventHandlers();
        }

        public override void Destroy()
        {
            base.Destroy();
            m_widgetVector = null;
            m_controlVector.Dispose();
            m_controlVector = null;
        }
        #endregion

        #region Properties
        public float[] Value
        {
            get
            {
                return new float[]{ (float)m_controlVector.Vector[0], (float)m_controlVector.Vector[1], 
                    (float)m_controlVector.Vector[2], (float)m_controlVector.Vector[3] };
            }
        }
        #endregion

        #region Public Functions
        public void UpdateValueFromRemote( float[] value )
        {
            if (m_controlVector.InvokeRequired == true)
            {
                UpdateInvoke updateInvoke = new UpdateInvoke(m_controlVector.UpdateValueFromRemote);

                object[] args = new object[1];
                args[0] = value;
                m_controlVector.Invoke(updateInvoke, args);
            }
            else
            {
                m_controlVector.UpdateValueFromRemote(value);
            }

            UpdateModifiedIndication();
        }

        #endregion

        #region Private Functions
        private void AddEventHandlers()
        {
            m_eventHandlers.Add( new EventHandler( control_ValueChanged ) );
            m_eventHandlers.Add( new EventHandler( m_widgetVector.AbsClickEH ) );
            m_vectorEventHandlers.Add( new ControlVector.ParameterizedOperationEventHandler( AddClickEH ) );
            m_vectorEventHandlers.Add( new ControlVector.ParameterizedOperationEventHandler( AddNetClickEH ) );
            m_vectorEventHandlers.Add( new ControlVector.ParameterizedOperationEventHandler( AverageClickEH ) );
            m_vectorEventHandlers.Add( new ControlVector.ParameterizedOperationEventHandler( CrossClickEH ) );
            m_vectorEventHandlers.Add( new ControlVector.ParameterizedOperationEventHandler( Dot3VClickEH ) );
            m_vectorEventHandlers.Add( new ControlVector.ParameterizedOperationEventHandler( DotVClickEH ) );
            m_vectorEventHandlers.Add( new ControlVector.ParameterizedOperationEventHandler( ExtendClickEH ) );
            m_eventHandlers.Add( new EventHandler( m_widgetVector.InvertClickEH ) );
            m_eventHandlers.Add( new EventHandler( m_widgetVector.InvertSafeClickEH ) );
            m_vectorEventHandlers.Add( new ControlVector.ParameterizedOperationEventHandler( InvScaleClickEH ) );
            m_vectorEventHandlers.Add( new ControlVector.ParameterizedOperationEventHandler( LerpClickEH ) );
            m_eventHandlers.Add( new EventHandler( m_widgetVector.Log10ClickEH ) );
            m_eventHandlers.Add( new EventHandler( m_widgetVector.LogClickEH ) );
            m_vectorEventHandlers.Add( new ControlVector.ParameterizedOperationEventHandler( MultiplyClickEH ) );
            m_eventHandlers.Add( new EventHandler( m_widgetVector.NegateClickEH ) );
            m_eventHandlers.Add( new EventHandler( m_widgetVector.Normalize3ClickEH ) );
            m_eventHandlers.Add( new EventHandler( m_widgetVector.Normalize3VClickEH ) );
            m_eventHandlers.Add( new EventHandler( m_widgetVector.NormalizeClickEH ) );
            m_eventHandlers.Add( new EventHandler( m_widgetVector.NormalizeFast3ClickEH ) );
            m_eventHandlers.Add( new EventHandler( m_widgetVector.NormalizeFastClickEH ) );
            m_eventHandlers.Add( new EventHandler( m_widgetVector.NormalizeSafeClickEH ) );
            m_eventHandlers.Add( new EventHandler( m_widgetVector.NormalizeSafeVClickEH ) );
            m_vectorEventHandlers.Add( new ControlVector.ParameterizedOperationEventHandler( ReflectAboutClickEH ) );
            m_vectorEventHandlers.Add( new ControlVector.ParameterizedOperationEventHandler( ReflectAboutFastClickEH ) );
            m_vectorEventHandlers.Add( new ControlVector.ParameterizedOperationEventHandler( RotateAboutAxisClickEH ) );
            m_vectorEventHandlers.Add( new ControlVector.ParameterizedOperationEventHandler( RotateClickEH ) );
            m_vectorEventHandlers.Add( new ControlVector.ParameterizedOperationEventHandler( RotateXClickEH ) );
            m_vectorEventHandlers.Add( new ControlVector.ParameterizedOperationEventHandler( RotateYClickEH ) );
            m_vectorEventHandlers.Add( new ControlVector.ParameterizedOperationEventHandler( RotateZClickEH ) );
            m_vectorEventHandlers.Add( new ControlVector.ParameterizedOperationEventHandler( Scale3ClickEH ) );
            m_vectorEventHandlers.Add( new ControlVector.ParameterizedOperationEventHandler( ScaleClickEH ) );
            m_vectorEventHandlers.Add( new ControlVector.ParameterizedOperationEventHandler( SubtractClickEH ) );

            m_controlVector.ValueChanged += m_eventHandlers[0];
            m_controlVector.AbsClick += m_eventHandlers[1];
            m_controlVector.AddClick += m_vectorEventHandlers[0];
            m_controlVector.AddNetClick += m_vectorEventHandlers[1];
            m_controlVector.AverageClick += m_vectorEventHandlers[2];
            m_controlVector.CrossClick += m_vectorEventHandlers[3];
            m_controlVector.Dot3VClick += m_vectorEventHandlers[4];
            m_controlVector.DotVClick += m_vectorEventHandlers[5];
            m_controlVector.ExtendClick += m_vectorEventHandlers[6];
            m_controlVector.InvertClick += m_eventHandlers[2];
            m_controlVector.InvertSafeClick += m_eventHandlers[3];
            m_controlVector.InvScaleClick += m_vectorEventHandlers[7];
            m_controlVector.LerpClick += m_vectorEventHandlers[8];
            m_controlVector.Log10Click += m_eventHandlers[4];
            m_controlVector.LogClick += m_eventHandlers[5];
            m_controlVector.MultiplyClick += m_vectorEventHandlers[9];
            m_controlVector.NegateClick += m_eventHandlers[6];
            m_controlVector.Normalize3Click += m_eventHandlers[7];
            m_controlVector.Normalize3VClick += m_eventHandlers[8];
            m_controlVector.NormalizeClick += m_eventHandlers[9];
            m_controlVector.NormalizeFast3Click += m_eventHandlers[10];
            m_controlVector.NormalizeFastClick += m_eventHandlers[11];
            m_controlVector.NormalizeSafeClick += m_eventHandlers[12];
            m_controlVector.NormalizeSafeVClick += m_eventHandlers[13];
            m_controlVector.ReflectAboutClick += m_vectorEventHandlers[10];
            m_controlVector.ReflectAboutFastClick += m_vectorEventHandlers[11];
            m_controlVector.RotateAboutAxisClick += m_vectorEventHandlers[12];
            m_controlVector.RotateClick += m_vectorEventHandlers[13];
            m_controlVector.RotateXClick += m_vectorEventHandlers[14];
            m_controlVector.RotateYClick += m_vectorEventHandlers[15];
            m_controlVector.RotateZClick += m_vectorEventHandlers[16];
            m_controlVector.Scale3Click += m_vectorEventHandlers[17];
            m_controlVector.ScaleClick += m_vectorEventHandlers[18];
            m_controlVector.SubtractClick += m_vectorEventHandlers[19];
        }

        private void RemoveEventHandlers()
        {
            m_controlVector.ValueChanged -= m_eventHandlers[0];
            m_controlVector.AbsClick -= m_eventHandlers[1];
            m_controlVector.AddClick -= m_vectorEventHandlers[0];
            m_controlVector.AddNetClick -= m_vectorEventHandlers[1];
            m_controlVector.AverageClick -= m_vectorEventHandlers[2];
            m_controlVector.CrossClick -= m_vectorEventHandlers[3];
            m_controlVector.Dot3VClick -= m_vectorEventHandlers[4];
            m_controlVector.DotVClick -= m_vectorEventHandlers[5];
            m_controlVector.ExtendClick -= m_vectorEventHandlers[6];
            m_controlVector.InvertClick -= m_eventHandlers[2];
            m_controlVector.InvertSafeClick -= m_eventHandlers[3];
            m_controlVector.InvScaleClick -= m_vectorEventHandlers[7];
            m_controlVector.LerpClick -= m_vectorEventHandlers[8];
            m_controlVector.Log10Click -= m_eventHandlers[4];
            m_controlVector.LogClick -= m_eventHandlers[5];
            m_controlVector.MultiplyClick -= m_vectorEventHandlers[9];
            m_controlVector.NegateClick -= m_eventHandlers[6];
            m_controlVector.Normalize3Click -= m_eventHandlers[7];
            m_controlVector.Normalize3VClick -= m_eventHandlers[8];
            m_controlVector.NormalizeClick -= m_eventHandlers[9];
            m_controlVector.NormalizeFast3Click -= m_eventHandlers[10];
            m_controlVector.NormalizeFastClick -= m_eventHandlers[11];
            m_controlVector.NormalizeSafeClick -= m_eventHandlers[12];
            m_controlVector.NormalizeSafeVClick -= m_eventHandlers[13];
            m_controlVector.ReflectAboutClick -= m_vectorEventHandlers[10];
            m_controlVector.ReflectAboutFastClick -= m_vectorEventHandlers[11];
            m_controlVector.RotateAboutAxisClick -= m_vectorEventHandlers[12];
            m_controlVector.RotateClick -= m_vectorEventHandlers[13];
            m_controlVector.RotateXClick -= m_vectorEventHandlers[14];
            m_controlVector.RotateYClick -= m_vectorEventHandlers[15];
            m_controlVector.RotateZClick -= m_vectorEventHandlers[16];
            m_controlVector.Scale3Click -= m_vectorEventHandlers[17];
            m_controlVector.ScaleClick -= m_vectorEventHandlers[18];
            m_controlVector.SubtractClick -= m_vectorEventHandlers[19]; 
            
            m_eventHandlers.Clear();
            m_vectorEventHandlers.Clear();
        }
        #endregion

        #region EventHandlers


        public void ScaleClickEH( object sender, VectorOperationParameterEventArgs e )
        {
            m_widgetVector.UpdateRemoteOperation(
                BankRemotePacket.EnumPacketType.USER_1,
                WidgetVector.Operation.Scale,
                e.FloatValue );
        }

        public void InvScaleClickEH( object sender, VectorOperationParameterEventArgs e )
        {
            m_widgetVector.UpdateRemoteOperation(
                BankRemotePacket.EnumPacketType.USER_1,
                WidgetVector.Operation.InvScale,
                e.FloatValue );
        }

        public void ExtendClickEH( object sender, VectorOperationParameterEventArgs e )
        {
            m_widgetVector.UpdateRemoteOperation(
                BankRemotePacket.EnumPacketType.USER_1,
                WidgetVector.Operation.Extend,
                e.FloatValue );
        }

        public void Scale3ClickEH( object sender, VectorOperationParameterEventArgs e )
        {
            m_widgetVector.UpdateRemoteOperation(
                BankRemotePacket.EnumPacketType.USER_1,
                WidgetVector.Operation.Scale3,
                e.FloatValue );
        }


        public void AddClickEH( object sender, VectorOperationParameterEventArgs e )
        {
            m_widgetVector.UpdateRemoteOperation(
                BankRemotePacket.EnumPacketType.USER_2,
                WidgetVector.Operation.Add,
                e.Vector );
        }

        public void SubtractClickEH( object sender, VectorOperationParameterEventArgs e )
        {
            m_widgetVector.UpdateRemoteOperation(
                BankRemotePacket.EnumPacketType.USER_2,
                WidgetVector.Operation.Subtract,
                e.Vector );
        }

        public void MultiplyClickEH( object sender, VectorOperationParameterEventArgs e )
        {
            m_widgetVector.UpdateRemoteOperation(
                BankRemotePacket.EnumPacketType.USER_2,
                WidgetVector.Operation.Multiply,
                e.Vector );
        }

        public void AverageClickEH( object sender, VectorOperationParameterEventArgs e )
        {
            m_widgetVector.UpdateRemoteOperation(
                BankRemotePacket.EnumPacketType.USER_0,
                WidgetVector.Operation.Average,
                e.Vector );
        }



        public void RotateClickEH( object sender, VectorOperationParameterEventArgs e )
        {
            m_widgetVector.UpdateRemoteOperation(
                BankRemotePacket.EnumPacketType.USER_1,
                WidgetVector.Operation.Rotate,
                e.FloatValue );
        }

        public void RotateXClickEH( object sender, VectorOperationParameterEventArgs e )
        {
            m_widgetVector.UpdateRemoteOperation(
                BankRemotePacket.EnumPacketType.USER_1,
                WidgetVector.Operation.RotateX,
                e.FloatValue );
        }

        public void RotateYClickEH( object sender, VectorOperationParameterEventArgs e )
        {
            m_widgetVector.UpdateRemoteOperation(
                BankRemotePacket.EnumPacketType.USER_1,
                WidgetVector.Operation.RotateY,
                e.FloatValue );
        }

        public void RotateZClickEH( object sender, VectorOperationParameterEventArgs e )
        {
            m_widgetVector.UpdateRemoteOperation(
                BankRemotePacket.EnumPacketType.USER_1,
                WidgetVector.Operation.RotateZ,
                e.FloatValue);
        }

        public void RotateAboutAxisClickEH( object sender, VectorOperationParameterEventArgs e )
        {
            m_widgetVector.UpdateRemoteOperation(
                BankRemotePacket.EnumPacketType.USER_4,
                WidgetVector.Operation.RotateAboutAxis,
                e.FloatValue,
                e.IntValue );
        }

        public void ReflectAboutClickEH( object sender, VectorOperationParameterEventArgs e )
        {
            m_widgetVector.UpdateRemoteOperation( 
                BankRemotePacket.EnumPacketType.USER_2, 
                WidgetVector.Operation.ReflectAbout, 
                e.Vector );
        }

        public void ReflectAboutFastClickEH( object sender, VectorOperationParameterEventArgs e )
        {
            m_widgetVector.UpdateRemoteOperation(
                BankRemotePacket.EnumPacketType.USER_2,
                WidgetVector.Operation.ReflectAboutFast,
                e.Vector );
        }

        public void AddNetClickEH( object sender, VectorOperationParameterEventArgs e )
        {
            m_widgetVector.UpdateRemoteOperation(
                BankRemotePacket.EnumPacketType.USER_2,
                WidgetVector.Operation.AddNet,
                e.Vector );
        }

        public void LerpClickEH( object sender, VectorOperationParameterEventArgs e )
        {
            m_widgetVector.UpdateRemoteOperation(
                BankRemotePacket.EnumPacketType.USER_3,
                WidgetVector.Operation.Lerp,
                e.FloatValue,
                e.Vector);
        }

        public void CrossClickEH( object sender, VectorOperationParameterEventArgs e )
        {
            m_widgetVector.UpdateRemoteOperation(
                BankRemotePacket.EnumPacketType.USER_2,
                WidgetVector.Operation.Cross,
                e.Vector );
        }

        public void DotVClickEH( object sender, VectorOperationParameterEventArgs e )
        {
            m_widgetVector.UpdateRemoteOperation(
                BankRemotePacket.EnumPacketType.USER_2,
                WidgetVector.Operation.DotV,
                e.Vector );
        }

        public void Dot3VClickEH( object sender, VectorOperationParameterEventArgs e )
        {
            m_widgetVector.UpdateRemoteOperation(
                BankRemotePacket.EnumPacketType.USER_2,
                WidgetVector.Operation.Dot3V,
                e.Vector );
        }


        #endregion
    }
}
