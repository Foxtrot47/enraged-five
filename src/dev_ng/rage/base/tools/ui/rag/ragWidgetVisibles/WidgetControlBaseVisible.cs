using System;
using System.Windows.Forms;

using ragCore;

namespace ragWidgets
{
    /// <summary>
    /// Summary description for WidgetControlBaseVisible.
    /// </summary>
    public abstract class WidgetControlBaseVisible : WidgetVisible
    {
        public WidgetControlBaseVisible( IWidgetView widgetView, Control parent, Widget widget )
            : base( widgetView, parent, widget )
        {
        }

        #region Overrides
        public override bool CanCopy
        {
            get
            {
                return this.ControlBase.CanCopy;
            }
        }

        public override bool CanPaste
        {
            get
            {
                return this.ControlBase.CanPaste;
            }
        }

        public override bool CanUndo
        {
            get
            {
                return this.ControlBase.CanUndo;
            }
        }

        public override bool IsClipboardDataAvailable
        {
            get
            {
                return base.IsClipboardDataAvailable && this.ControlBase.IsClipboardDataAvailable;
            }
        }

        public override bool IsUndoable
        {
            get
            {
                return base.IsUndoable && this.ControlBase.IsUndoable;
            }
        }

        public override bool IsReadOnly
        {
            set { ControlBase.IsReadOnly = value; }
        }

        public override Control Control
        {
            get
            {
                return this.ControlBase;
            }
        }

        public override Control DragDropControl
        {
            get
            {
                return this.ControlBase;
            }
        }

        public override void CopyToClipboard()
        {
            this.ControlBase.CopyToClipboard();
        }

        public override void PasteFromClipboard()
        {
            this.ControlBase.PasteFromClipboard();
        }

        public override void Remove()
        {
            if ( this.ControlBase != null )
            {
                RemoveDragDropHandlers( ControlBase );
                ControlBase.RemoveMouseEnter( new EventHandler( ShowHelp ) );
                ControlBase.RemoveMouseDown( new MouseEventHandler( OnMouseDown ) );
                ControlBase.RemoveMouseUp( new MouseEventHandler( OnMouseUp ) );
            }

            if ( m_Parent != null )
            {
                m_Parent.Controls.Remove( ControlBase );
            }

            base.Remove();
        }

        public override void Undo()
        {
            this.ControlBase.Undo();
        }

        public override void UpdatePersist( bool persist )
        {
            this.ControlBase.Icon = persist ? SaveIcon : null;
        }
        #endregion

        #region Abstractions
        public abstract ragUi.ControlBase ControlBase
        {
            get;
        }
        #endregion

        #region Public Functions
        public void FinishCreation(ToolTip tooltip)
        {
            this.ControlBase.AddMouseEnter( new EventHandler( ShowHelp ) );
            this.ControlBase.AddMouseDown( new MouseEventHandler( OnMouseDown ) );
            this.ControlBase.AddMouseUp(new MouseEventHandler(OnMouseUp));
            SetDragDropHandlers(this.ControlBase);
            this.ControlBase.SetToolTip(tooltip, Widget.HelpTitle, Widget.Memo);
            UpdatePersist(Widget.Persist);

            // Add to parent last.
            m_Parent.Controls.Add( this.ControlBase );
        }

        delegate void UpdateInvoke(System.Drawing.Color ColorDepth);

        public void UpdateModifiedIndication()
        {
            if ( !ViewSettings.ShowModifiedColours )
            {
                return;
            }

            if (Widget == null)
            {
                return;
            }

            // Make sure parents are up to date and shows correct color/indication
            if ( WidgetGroupBase.WidgetModified != null ) // will be null if no tree views are open
            {
                Widget parent = Widget.Parent;
                while ( parent != null )
                {
                    WidgetGroupBase widgetGroup = parent as WidgetGroupBase;
                    if ( widgetGroup != null )
                    {
                        WidgetGroupBase.WidgetModified( widgetGroup );
                    }

                    parent = parent.Parent;
                }
            }


            if ( ControlBase == null )
            {
                return;
            }

            System.Drawing.Color newColor = new System.Drawing.Color();
            bool setColor = false;
            if (Widget.IsModified())
            {
                newColor = ViewSettings.ModificationColor;
                setColor = true;
            }
            else
            {
                if (ControlBase.Parent != null)
                {
                    newColor = FillColor;
                    setColor = true;
                }
            }

            if (setColor == true && ControlBase != null)
            {
                if (ControlBase.InvokeRequired == true)
                {
                    UpdateInvoke invoke = delegate(System.Drawing.Color color)
                    {
                        ControlBase.BackColor = color;
                    };

                    object[] args = new object[1];
                    args[0] = newColor;
                    ControlBase.Invoke(invoke, args);
                }
                else
                {
                    ControlBase.BackColor = newColor;
                }
            }
        }

        #endregion
    }
}
