using System;

/// These are required references when creating a widget visible:
using ragCore;
using ragUi;
using ragWidgets;
using System.Windows.Forms;

namespace ragWidgets
{
	/// <summary>
	/// WidgetImageViewerVisible is a visible instance of the browser that the user sees.
	/// It is possible that several visibles share the same widget since it is possible to 
	/// have several views of the same widget.
	/// 
	/// The standard model is to create a separate control from the visible, and that 
	/// control is contained with in the visible class.  This allows you to design the 
	/// control in the designer, and it allows you to also reuse that control.
	/// The control for this example is in SampleControl.cs.
	/// </summary>
	public class WidgetImageViewerVisible : ragWidgets.WidgetControlBaseVisible
	{
		/// <summary>
		/// This is the constructor the visible widget.
		/// </summary>
		/// <param name="widgetView">The widget view that the visible will be displayed in.  NOTE that this can be null!!!</param>
		/// <param name="imageViewer">The instance of the widget that we communicate with. </param>
		/// <param name="parent">The parent control that will handle displaying this visible.</param>
        public WidgetImageViewerVisible(IWidgetView widgetView, WidgetImageViewer imageViewer, Control parent, ToolTip tooltip) 
			: base(widgetView,parent,imageViewer)
		{
			// store a reference to the image viewer so we can communicate changes in the interface to the widget:
            m_Widget = imageViewer;

			// create instances of your control(s):
			m_ImageViewer = new ControlImageViewer();
            m_ImageViewer.Text = m_Widget.Title;
            m_ImageViewer.ValueChanged += new EventHandler( control_ValueChanged );
            m_ImageViewer.UpdateValueFromRemote( m_Widget.ImageFile );
            m_ImageViewer.IsReadOnly = m_Widget.IsReadOnly;


            FinishCreation(tooltip);
		}

        protected override void widget_ValueChanged( object sender, EventArgs e )
        {
            WidgetImageViewer widget = (WidgetImageViewer)sender;
            UpdateValueFromRemote( widget.ImageFile );
        }

        public void control_ValueChanged( object sender, EventArgs e )
        {
            ControlImageViewer ctrl = sender as ControlImageViewer;

            m_Widget.ImageFile = ctrl.CurrentImageFile;
        }

		#region Properties
		/// <summary>
		/// This property allows us to change the control's web page.
		/// </summary>
		public string CurrentImageFile
		{
            get
            {
                return m_ImageViewer.CurrentImageFile;
            }
			set
			{
				m_ImageViewer.CurrentImageFile=value;
			}
		}
		#endregion

		#region Variables
		/// <summary>
		/// A reference to the instance of the widget browser which we update to and from.
		/// </summary>
		private WidgetImageViewer m_Widget;
		
        /// <summary>
		/// A reference to the control that the user interfaces with.
		/// </summary>
		private ControlImageViewer m_ImageViewer;

        private delegate void UpdateInvoke(string inImageFile);

		#endregion

		#region Overrides
		/// <summary>
		/// This should return the same control that got added to the list of controls for the parent control. 
		/// </summary>
		public override Control Control
		{
			get
			{
				return m_ImageViewer;
			}
		}

		/// <summary>
		/// This is reserved for future use, so for now it just needs to return null.
		/// </summary>
		public override Control DragDropControl
		{
			get
			{
				return null;
			}
		}

        public override ragUi.ControlBase ControlBase
        {
            get
            {
                return m_ImageViewer;
            }
        }

		/// <summary>
		/// Function for return the height of the control.
		/// </summary>
		/// <returns>The height of this widget.</returns>
		public override int GetHeight()
		{
			return m_ImageViewer.Height;
		}

		/// <summary>
		/// This just returns a reference to widget that this visible communicates with.
		/// </summary>
		public override Widget Widget
		{
			get
			{
				return m_Widget;
			}
		}

        public override void Remove()
        {
            base.Remove();

            m_Parent.Controls.Remove( m_ImageViewer );
        }

        public override void Destroy()
        {
            base.Destroy();

            m_Widget = null;
            m_ImageViewer.Dispose();
            m_ImageViewer = null;
        }
		#endregion

        #region Public Functions
        public void UpdateValueFromRemote( string imageFile )
        {
            if (m_ImageViewer.InvokeRequired == true)
            {
                UpdateInvoke updateInvoke = new UpdateInvoke(UpdateValueFromRemote);

                object[] args = new object[1];
                args[0] = imageFile;
                m_ImageViewer.Invoke(updateInvoke, args);
            }
            else
            {
                m_ImageViewer.UpdateValueFromRemote(imageFile);
            }

        }
        #endregion
    }
}
