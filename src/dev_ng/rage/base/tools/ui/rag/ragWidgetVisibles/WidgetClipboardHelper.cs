﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ragCore;
using System.Windows.Forms;

namespace ragWidgets
{

    public class WidgetClipboardHelper
    {
        public WidgetClipboardHelper( Widget widget )
        {
            m_Widget = widget;
        }

        public void WidgetCopyHandler( Object obj, System.EventArgs e )
        {
            LitJson.JsonWriter writer = new LitJson.JsonWriter();
            writer.PrettyPrint = true;

            LitJson.JsonData data = new LitJson.JsonData();
            data["name"] = m_Widget.Title;
            data["type"] = m_Widget.GetType().ToString();
            m_Widget.SerializeToJSON( data );

            data.ToJson( writer );
            if ( writer.TextWriter.ToString().Length > 0 )
            {
                Clipboard.SetText( writer.TextWriter.ToString() );
            }
        }

        public void WidgetPasteHandler( Object obj, System.EventArgs e )
        {
            String clipboardData = Clipboard.GetText();
            LitJson.JsonData data;

            try
            {
                LitJson.JsonReader reader = new LitJson.JsonReader( clipboardData );
                data = LitJson.JsonMapper.ToObject( clipboardData );
            }
            catch ( System.Exception ex )
            {
                string msg = "Incorrectly formatted JSON data in clipboard." + Environment.NewLine + Environment.NewLine;
                msg += ex.Message + Environment.NewLine;
                msg += ex.StackTrace + Environment.NewLine;
                MessageBox.Show( msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation );
                return;
            }

            try
            {
                bool clipboardWidgetMatch = (string)data["name"] == m_Widget.Title &&
                    (string)data["type"] == m_Widget.GetType().ToString();

                if ( !clipboardWidgetMatch )
                {
                    string msg = String.Format( "Mismatched data: You tried to paste data from '{0}' into '{1}')", (string)data["name"], m_Widget.Title );
                    MessageBox.Show( msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation );
                    return;
                }
            }
            catch ( System.Exception ex )
            {
                string msg = "Data in clipboard doesn't match widget structure." + Environment.NewLine + Environment.NewLine;
                msg += ex.Message + Environment.NewLine;
                msg += ex.StackTrace + Environment.NewLine;
                MessageBox.Show( msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation );
                return;
            }

            try
            {
                m_Widget.DeSerializeFromJSON( data );
            }
            catch ( Exception ex )
            {
                string msg = "Data in clipboard doesn't match widget structure." + Environment.NewLine + Environment.NewLine;
                msg += ex.Message + Environment.NewLine;
                msg += ex.StackTrace + Environment.NewLine;
                MessageBox.Show( msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation );
            }
        }

        private Widget m_Widget;
    }
}
