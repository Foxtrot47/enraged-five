using System;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Windows.Forms;
using ragCore;

namespace ragWidgets
{
    /// <summary>
    /// Summary description for WidgetVisible.
    /// </summary>
    public abstract class WidgetVisible
    {
        public WidgetVisible( IWidgetView widgetView, Control parent, Widget widget )
        {
            // create save icon if not already created:
            sm_SaveIcon = SaveIcon;

            m_WidgetView = widgetView;
            m_Parent = parent;

            if ( this.DisplayPath && (widget != null) )
            {
                m_LabelPath = new System.Windows.Forms.Label();
                //m_Label.Dock = System.Windows.Forms.DockStyle.Bottom;
                m_LabelPath.Location = new System.Drawing.Point( 0, 0 );
                m_LabelPath.BackColor = System.Drawing.SystemColors.Info;
                m_LabelPath.ForeColor = System.Drawing.SystemColors.InfoText;
                m_LabelPath.Font = new Font( "Microsoft Sans Serif", 7, FontStyle.Regular );
                m_LabelPath.Name = widget.Path;
                m_LabelPath.TabIndex = 0;
                m_LabelPath.Text = widget.Path;
                m_LabelPath.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;

                parent.Controls.Add( m_LabelPath );
            }

            if ( widget != null )
            {
                m_WidgetValueChangedEH = widget_ValueChanged;
                widget.ValueChanged += m_WidgetValueChangedEH;

                m_WidgetFillColorChangedEH = new EventHandler( widget_FillColorChanged );
                widget.WidgetFillColorChanged += m_WidgetFillColorChangedEH;

                m_WidgetIsReadOnlyChangedEH = new EventHandler( widget_IsReadOnlyChanged );
                widget.IsReadOnlyChanged += m_WidgetIsReadOnlyChangedEH;

                m_WidgetEnabledChangedEH = new EventHandler( widget_EnabledChanged );
                widget.EnabledChanged += m_WidgetEnabledChangedEH;

                m_WidgetPersistChangedEH = new EventHandler( widget_PersistChanged );
                widget.PersistChanged += m_WidgetPersistChangedEH;

                m_WidgetDestroyedEH = new EventHandler( widget_Destroyed );
                widget.DestroyEvent += m_WidgetDestroyedEH;
            }
        }

        #region Constants
        private const int MINIMUM_WIDTH = 200;
        #endregion

        #region Static Variables
        static private Bitmap sm_SaveIcon;
        static private Stream sm_IconStream;
        static private AddContextMenuItemsDel sm_ContextMenuItemsDelegate;
        static private CreateWidgetViewSingleDel sm_CreateWidgetViewDelegate;
        #endregion

        #region Variables
        public delegate void ValueChangedCallback();
        private IWidgetView m_WidgetView;
        protected Control m_Parent;
        private System.Windows.Forms.Label m_LabelPath;
        protected Color m_fillColor = Color.Empty;

        private event EventHandler m_WidgetValueChangedEH;
        private EventHandler m_WidgetFillColorChangedEH;
        private EventHandler m_WidgetIsReadOnlyChangedEH;
        private EventHandler m_WidgetEnabledChangedEH;
        private EventHandler m_WidgetPersistChangedEH;
        private EventHandler m_WidgetDestroyedEH;
        #endregion

        #region Static Properties
        static public AddContextMenuItemsDel ContextMenuItemsDelegate
        {
            set { sm_ContextMenuItemsDelegate = value; }
        }

        static public CreateWidgetViewSingleDel CreateWidgetViewDelegate
        {
            set { sm_CreateWidgetViewDelegate = value; }
        }

        public static int MinimumWidth
        {
            get { return MINIMUM_WIDTH; }
        }
        #endregion

        #region Delegates
        public delegate void AddContextMenuItemsDel( IWidgetView view, ContextMenuStrip menu, Widget widget, WidgetVisible visible );
        public delegate void CreateWidgetViewSingleDel( Widget widget );
        #endregion

        #region Properties
        public Bitmap SaveIcon
        {
            get
            {
                if ( sm_SaveIcon == null )
                {
                    sm_SaveIcon = new Bitmap( IconStream );
                }
                return sm_SaveIcon;
            }
        }

        public Stream IconStream
        {
            get
            {
                if ( sm_IconStream == null )
                {
                    Assembly ragCoreAssembly = Assembly.GetAssembly( typeof( IBankManager ) );
                    sm_IconStream = ragCoreAssembly.GetManifestResourceStream( "ragCore.save.ico" );
                }
                if ( sm_IconStream == null )
                    sm_IconStream = Assembly.GetExecutingAssembly().GetManifestResourceStream( "ragCore.save.ico" );
                return sm_IconStream;
            }
        }

        public virtual bool DisplayPath
        {
            get
            {
                return this.WidgetView != null ? this.WidgetView.DisplayPath : false;
            }
        }

        public IWidgetView WidgetView
        {
            get
            {
                return m_WidgetView;
            }
        }

        protected int ParentWidth
        {
            get { return m_Parent.Width; }
        }

        protected int ParentHeight
        {
            get { return m_Parent.Height; }
        }

        public virtual Color FillColor
        {
            get
            {
                return m_fillColor;
            }
            set
            {
                m_fillColor = value;

                // Modification colouring has precedence
                bool colorOverriddenByModification = ViewSettings.ShowModifiedColours && Widget.IsModified();
                if ( !colorOverriddenByModification && this.Control != null )
                {
                    this.Control.BackColor = m_fillColor;
                }
            }
        }

        public virtual bool IsReadOnly
        {
            set { Control.Enabled = !value && Widget.Enabled; }
        }

        public virtual bool CanCopy
        {
            get
            {
                return false;
            }
        }

        public virtual bool CanPaste
        {
            get
            {
                return false;
            }
        }

        public virtual bool CanUndo
        {
            get
            {
                return false;
            }
        }

        public virtual bool IsClipboardDataAvailable
        {
            get
            {
                return this.CanUndo;
            }
        }

        public virtual bool IsUndoable
        {
            get
            {
                return this.CanUndo;
            }
        }
        #endregion

        #region Virtual Functions
        public virtual void Destroy()
        {
            if ( m_WidgetValueChangedEH != null )
            {
                Widget.ValueChanged -= m_WidgetValueChangedEH;
            }

            if ( m_WidgetFillColorChangedEH != null )
            {
                Widget.WidgetFillColorChanged -= m_WidgetFillColorChangedEH;
            }

            if (m_WidgetIsReadOnlyChangedEH != null)
            {
                Widget.IsReadOnlyChanged -= m_WidgetIsReadOnlyChangedEH;
            }

            if ( m_WidgetEnabledChangedEH != null )
            {
                Widget.EnabledChanged -= m_WidgetEnabledChangedEH;
            }

            if ( m_WidgetPersistChangedEH != null )
            {
                Widget.PersistChanged -= m_WidgetPersistChangedEH;
            }

            if ( m_WidgetDestroyedEH != null )
            {
                Widget.DestroyEvent -= m_WidgetDestroyedEH;
            }
            m_WidgetView = null;
            m_Parent = null;
        }
        
        protected virtual void OnMouseUp( object sender, MouseEventArgs e )
        {
            if ( e.Button == MouseButtons.Right )
            {
                ContextMenuStrip menu = new ContextMenuStrip();
                MakeMenu( menu );

                if ( this.WidgetView != null )
                {
                    this.WidgetView.AddContextMenuOptions( menu, this.Widget, this );

                    if ( this.Widget != null )
                    {
                        // only show option if widget type allows it:
                        if ( this.Widget.AllowPersist )
                        {
                            // separator:
                            if ( menu.Items.Count >= 1 )
                            {
                                menu.Items.Add( new ToolStripSeparator() );
                            }

                            ToolStripMenuItem item = new ToolStripMenuItem( "Save Widget" );
                            item.Checked = this.Widget.Persist;
                            item.CheckOnClick = true;
                            item.Click += new EventHandler( UpdatePersistEH );
                            menu.Items.Add( item );
                        }

                        string name = this.Widget.CategoryDescription;

                        ToolStripMenuItem itemView = new ToolStripMenuItem( "Show " + name + " In Its Own View" );
                        itemView.Click += new EventHandler( CreateWidgetSingleViewEH );
                        menu.Items.Add( itemView );

                        menu.Items.Add( new ToolStripSeparator() );

                        if ( this.CanUndo )
                        {
                            itemView = new ToolStripMenuItem( "Undo" );
                            itemView.Click += new EventHandler( UndoEH );
                            itemView.Enabled = this.IsUndoable;
                            menu.Items.Add( itemView );

                            menu.Items.Add( new ToolStripSeparator() );
                        }

                        itemView = new ToolStripMenuItem( "Copy " + name + " Path" );
                        itemView.Click += new EventHandler( CopyWidgetNameToClipboardEH );
                        menu.Items.Add( itemView );

                        if ( this.CanCopy )
                        {
                            itemView = new ToolStripMenuItem( "Copy Value" );
                            itemView.Click += new EventHandler( CopyWidgetValueToClipboardEH );
                            menu.Items.Add( itemView );
                        }

                        if ( this.CanPaste )
                        {
                            itemView = new ToolStripMenuItem( "Paste Value" );
                            itemView.Click += new EventHandler( PasteWidgetValueFromClipboardEH );
                            itemView.Enabled = this.IsClipboardDataAvailable;
                            menu.Items.Add( itemView );
                        }

                        WidgetResetter resetter = new WidgetResetter( this.Widget );
                        itemView = new ToolStripMenuItem( "Reset to Default Value(s)" );
                        if ( this.Widget == null || !this.Widget.IsModified() )
                        {
                            itemView.Enabled = false;
                        }

                        itemView.Click += new EventHandler( resetter.ResetWidgetHandler );
                        menu.Items.Add( itemView );


                        // Copy/pasting values
                        WidgetClipboardHelper clipboard = new WidgetClipboardHelper( this.Widget );
                        ToolStripMenuItem copyItem = new ToolStripMenuItem( "Copy All Values (JSON)" );
                        copyItem.Click += new EventHandler( clipboard.WidgetCopyHandler );
                        menu.Items.Add( copyItem );

                        ToolStripMenuItem pasteItem = new ToolStripMenuItem( "Paste All Values (JSON)" );
                        pasteItem.Click += new EventHandler( clipboard.WidgetPasteHandler );
                        menu.Items.Add( pasteItem );

                        if ( this.Widget == null )
                        {
                            copyItem.Enabled = false;
                            pasteItem.Enabled = false;
                        }

                        if ( !Clipboard.ContainsText() )
                        {
                            pasteItem.Enabled = false;
                        }

                    }

                    if ( menu.Items.Count > 0 )
                    {
                        menu.Show( (Control)sender, new Point( e.X, e.Y ) );
                    }
                }
            }
        }

        protected virtual void OnMouseDown( object sender, MouseEventArgs e )
        {
            if ( e.Button == MouseButtons.Left && this.WidgetView != null && this.WidgetView.AllowDragDrop )
            {
                DragDropInfo info = new DragDropInfo( this );
                DragDropControl.DoDragDrop( info, DragDropEffects.Move );
            }
        }

        public virtual void CopyToClipboard()
        {

        }

        public virtual void PasteFromClipboard()
        {

        }

        public virtual void Undo()
        {

        }

        public virtual void Remove()
        {
            if ( m_LabelPath != null )
            {
                m_Parent.Controls.Remove( m_LabelPath );
                m_LabelPath = null;
            }
        }

        public virtual void ResizeAndReposition( int yLocation )
        {
            if ( Control != null )
            {
                Control.SetBounds( 0, yLocation, ParentWidth, GetHeight(), BoundsSpecified.All );
            }
        }

        public virtual void UpdatePersist( bool visible )
        {
        }

        public virtual int GetHeight() { return Control.Height; }

        protected virtual Bitmap UpdateControlTextAndSaveIcon( Control control, bool isPersistant )
        {
            if ( isPersistant )
            {
                Graphics newGraphics = Graphics.FromHwnd( control.Handle );
                Size size = newGraphics.MeasureString( " ", control.Font ).ToSize();

                String newText = "";
                int i;
                int iconWidth = SaveIcon.Width;
                for ( i = 0; i < iconWidth; )
                {
                    newText += " ";
                    i += size.Width;
                }
                if ( i != iconWidth )
                    newText += " ";

                control.Text = newText + control.Text.TrimStart();
                newGraphics.Dispose();
                return SaveIcon;
            }
            else
            {
                control.Text = control.Text.TrimStart();
                return null;
            }
        }
        #endregion

        #region Public Functions
        public void AddMouseEvents( Control control )
        {
            AddMouseEnter( control );
            control.MouseUp += new MouseEventHandler( OnMouseUp );
            control.MouseDown += new MouseEventHandler( OnMouseDown );
            SetDragDropHandlers( control );
        }

        public void SetDragDropHandlers( Control control )
        {
            control.DragDrop += new DragEventHandler( DragDrop_EH );
            control.DragOver += new DragEventHandler( DragOver_EH );
            control.DragEnter += new DragEventHandler( DragEnter_EH );
            control.GiveFeedback += new GiveFeedbackEventHandler( GiveFeedback_EH );
            control.AllowDrop = true;
            control.Tag = this;
        }

        public void RemoveDragDropHandlers( Control control )
        {
            control.DragDrop -= new DragEventHandler( DragDrop_EH );
            control.DragOver -= new DragEventHandler( DragOver_EH );
            control.DragEnter -= new DragEventHandler( DragEnter_EH );
            control.GiveFeedback -= new GiveFeedbackEventHandler( GiveFeedback_EH );
            control.Tag = null;
            control.AllowDrop = false;
        }

        public int GetParentIndex()
        {
            int childIndex = m_Parent.Controls.GetChildIndex( this.Control );
            if ( (this.WidgetView != null) && this.WidgetView.DisplayPath && (m_LabelPath != null) )
            {
                childIndex--;
            }

            return childIndex;
        }

        public void MoveVisible( int newParentIndex )
        {
            int widgetViewIndex = newParentIndex;
            if ( m_LabelPath != null )
            {
                int oldIndex = m_Parent.Controls.GetChildIndex( Control );
                if ( oldIndex == newParentIndex )
                    return;
                else if ( oldIndex < newParentIndex )
                {
                    m_Parent.Controls.SetChildIndex( m_LabelPath, newParentIndex + 1 );
                    newParentIndex = m_Parent.Controls.GetChildIndex( m_LabelPath );
                    m_Parent.Controls.SetChildIndex( Control, newParentIndex );
                }
                else
                {
                    m_Parent.Controls.SetChildIndex( Control, newParentIndex );
                    newParentIndex = m_Parent.Controls.GetChildIndex( Control );
                    m_Parent.Controls.SetChildIndex( m_LabelPath, newParentIndex );
                }

                // adjust for widget view:
                widgetViewIndex = widgetViewIndex / 2;
            }
            else
            {
                m_Parent.Controls.SetChildIndex( Control, newParentIndex );
            }

            // update widget view:
            if ( this.WidgetView != null )
            {
                this.WidgetView.SetIndexForWidget( this, widgetViewIndex );
                this.WidgetView.SetDirty();
                this.WidgetView.ResizeAndReposition();
            }
        }
        #endregion

        #region Protected Functions
        protected void AddMouseEnter( Control ctl )
        {
            ctl.MouseEnter += new EventHandler( ShowHelp );
        }

        protected void AddContextMenu( Control ctl )
        {
            ctl.MouseUp += new MouseEventHandler( OnMouseUp );
        }

        protected void RemoveMouseEnter( Control ctl )
        {
            ctl.MouseEnter -= new EventHandler( ShowHelp );
        }

        protected void RemoveContextMenu( Control ctl )
        {
            ctl.MouseUp -= new MouseEventHandler( OnMouseUp );
        }

        protected void ShowHelp( object sender, EventArgs e )
        {
            if ( Widget != null && m_WidgetView != null )
            {
                m_WidgetView.LabelHelp = Widget.HelpTitle;
                m_WidgetView.LabelDescription = Widget.Memo;
            }
        }
        
        protected void MakeMenu( ContextMenuStrip menu )
        {
            if ( sm_ContextMenuItemsDelegate != null )
            {
                sm_ContextMenuItemsDelegate( m_WidgetView, menu, Widget, this );
            }
        }
        #endregion

        #region Event Handlers
        private void UpdatePersistEH( object sender, EventArgs e )
        {
            this.Widget.Persist = !this.Widget.Persist;
        }

        private void CreateWidgetSingleViewEH( object sender, EventArgs e )
        {
            if ( sm_CreateWidgetViewDelegate != null )
            {
                sm_CreateWidgetViewDelegate( this.Widget );
            }
        }

        private void CopyWidgetNameToClipboardEH( object sender, EventArgs e )
        {
            // copy full path to clipboard:
            if ( this.Widget != null )
            {
                Clipboard.SetDataObject( Widget.FindPath(), true );
            }
        }

        private void CopyWidgetValueToClipboardEH( object sender, EventArgs e )
        {
            // copy full path to clipboard:
            if ( this.CanCopy )
            {
                CopyToClipboard();
            }
        }

        private void PasteWidgetValueFromClipboardEH( object sender, EventArgs e )
        {
            // copy full path to clipboard:
            if ( this.IsClipboardDataAvailable )
            {
                PasteFromClipboard();
            }
        }

        private void UndoEH( object sender, EventArgs e )
        {
            if ( this.IsUndoable )
            {
                Undo();
            }
        }

        private void DragDrop_EH( object sender, System.Windows.Forms.DragEventArgs e )
        {
            if ( this.WidgetView != null )
            {
                DragDropInfo visibleMoving = e.Data.GetData( "ragCore.DragDropInfo" ) as DragDropInfo;
                if (visibleMoving != null)
                {
                    visibleMoving.m_Visible.MoveVisible( GetParentIndex() );
                }
            }
        }

        private void DragOver_EH( object sender, System.Windows.Forms.DragEventArgs e )
        {
            if ( this.WidgetView != null )
            {
                Control control = this.WidgetView.GetControlAtCursorPoint();

                WidgetVisible visibleMovingOnTo = control.Tag as WidgetVisible;

                DragDropInfo visibleMoving = e.Data.GetData( "ragCore.DragDropInfo" ) as DragDropInfo;
            }
        }

        private void DragEnter_EH( object sender, System.Windows.Forms.DragEventArgs e )
        {
            e.Effect = DragDropEffects.Move;
        }

        private void GiveFeedback_EH( object sender, System.Windows.Forms.GiveFeedbackEventArgs e )
        {
            //e.Effect = DragDropEffects.Move;
        }

        void widget_FillColorChanged( object sender, EventArgs e )
        {
            Widget widget = (Widget)sender;
            FillColor = widget.FillColor;
        }

        void widget_IsReadOnlyChanged(object sender, EventArgs e)
        {
            Widget widget = (Widget)sender;
            IsReadOnly = widget.IsReadOnly;
        }

        void widget_EnabledChanged( object sender, EventArgs e )
        {
            Widget widget = (Widget)sender;
            Control.Enabled = widget.Enabled && !widget.IsReadOnly;
        }

        void widget_PersistChanged( object sender, EventArgs e )
        {
            Widget widget = (Widget)sender;
            UpdatePersist( widget.Persist );
        }

        void widget_Destroyed( object sender, EventArgs e )
        {
            Remove();
            Destroy();
        }

        protected virtual void widget_ValueChanged( object sender, EventArgs e )
        {
            //throw new Exception( "Not implemented" );
        }
        #endregion

        #region Abstracts
        public abstract Control Control { get; }
        public abstract Control DragDropControl { get; }
        public abstract Widget Widget { get; }
        #endregion
    }

    class DragDropInfo
    {
        public DragDropInfo( WidgetVisible visible )
        {
            m_Visible = visible;
        }
        public WidgetVisible m_Visible;
    }
}
