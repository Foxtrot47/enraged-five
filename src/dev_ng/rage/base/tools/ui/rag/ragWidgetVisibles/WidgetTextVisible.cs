using System;
using System.Windows.Forms;
using System.Text;

using ragCore;

namespace ragWidgets
{
	/// <summary>
	/// Summary description for WidgetTextVisible.
	/// </summary>
	public class WidgetTextVisible : WidgetControlBaseVisible
	{
        public WidgetTextVisible(IWidgetView widgetView, WidgetText text, Control parent, ToolTip tooltip) 
			: base(widgetView,parent,text)
		{
            m_Widget = text;

			m_ControlText = new ragUi.ControlText();

            m_ControlText.IsReadOnly = text.IsReadOnly;
			m_ControlText.Text = text.Title;
			m_ControlText.UpdateValueFromRemote( text.String );

            if ( m_Widget.TypeOfData == WidgetText.DataType.STRING )
            {
                // The text control's max length is the max length of
                // the string - 1 for the null terminating character
                m_ControlText.MaxLength = (int)m_Widget.MaxStringLength - 1;
            }
			
			m_TextBoxEH = new EventHandler(control_ValueChanged);
			m_ControlText.ValueChanged += m_TextBoxEH;


            FinishCreation(tooltip);
            if (Widget.IsModified())
            {
                UpdateModifiedIndication();
            }
        }

        protected override void widget_ValueChanged( object sender, EventArgs e )
        {
            WidgetText widget = (WidgetText)sender;
            UpdateValueFromRemote( widget.String );
        }

        public void control_ValueChanged( object sender, System.EventArgs e )
        {
            // get our value from the visible:
            ragUi.ControlText textBox = sender as ragUi.ControlText;
            if ( (textBox != null) && (textBox.Value != m_Widget.String) )
            {
                m_Widget.String = textBox.Value;

                // something went wrong, so change the value back
                if ( m_Widget.String != textBox.Value )
                {
                    UpdateValueFromRemote( m_Widget.String );
                }
            }
        }

        #region Variables
        private ragUi.ControlText m_ControlText;

        private EventHandler m_TextBoxEH;
        private WidgetText m_Widget;
        private delegate void UpdateInvoke(String inValue);
        #endregion

        #region Overrides
        public override ragUi.ControlBase ControlBase
        {
            get
            {
                return m_ControlText;
            }
        }

        public override Widget Widget {get {return m_Widget;}}

        public override void Destroy()
        {
            base.Destroy();
            m_Widget = null;
            m_ControlText.Dispose();
            m_ControlText = null;
            m_TextBoxEH = null;
        }

		public override void UpdatePersist(bool persist)
		{
			m_ControlText.Icon = persist ? SaveIcon : null;
        }
        #endregion

        #region Public Functions
        /// <summary>
		/// disable event handler temporarily.
		/// </summary>
		public void UpdateValueFromRemote(String value) 
		{
            if (m_ControlText.InvokeRequired == true)
            {
                UpdateInvoke updateInvoke = new UpdateInvoke(m_ControlText.UpdateValueFromRemote);

                object[] args = new object[1];
                args[0] = value;
                m_ControlText.Invoke(updateInvoke, args);
            }
            else
            {
                m_ControlText.UpdateValueFromRemote(value);
            }

            UpdateModifiedIndication();
        }
        #endregion
    }
}
