using System;
using System.Windows.Forms;

using ragCore;
using ragUi;

namespace ragWidgets
{
    /// <summary>
    /// Summary description for WidgetColorVisible.
    /// </summary>
    public class WidgetColorVisible : WidgetControlBaseVisible
    {
        public WidgetColorVisible(IWidgetView widgetView, WidgetColor color, Control parent, ToolTip tooltip)
            : base( widgetView, parent, color )
        {
            m_Widget = color;

            m_ControlColor = new ControlColor();
            m_ControlColor.Text = color.Title;
            m_ControlColor.Alpha = m_Widget.Type != WidgetColor.ColorType.VECTOR3;
            m_ControlColor.ValueChanged += new EventHandler( control_ValueChanged );
            m_ControlColor.UpdateValueFromRemote( color.A, color.R, color.G, color.B );
            m_ControlColor.IsReadOnly = color.IsReadOnly;


            FinishCreation(tooltip);
            if (Widget.IsModified())
            {
                UpdateModifiedIndication();
            }
        }

        protected override void widget_ValueChanged( object sender, EventArgs e )
        {
            WidgetColor widget = (WidgetColor)sender;
            UpdateValueFromRemote( widget.A, widget.R, widget.G, widget.B );
        }

        public void control_ValueChanged( object obj, System.EventArgs e )
        {
            ragUi.ControlColor ctrl = obj as ragUi.ControlColor;
            if ( ctrl == null )
            {
                Control tempCtl = (Control)obj;
                ctrl = (ragUi.ControlColor)tempCtl.Parent;
            }

            if ( ctrl != null )
            {
                m_Widget.SetColor((float)ctrl.R / 255.0f,
                                  (float)ctrl.G / 255.0f,
                                  (float)ctrl.B / 255.0f,
                                  (float)ctrl.A / 255.0f);
            }
        }

        #region Variables
        private ControlColor m_ControlColor;
        private WidgetColor m_Widget;
        private delegate void UpdateInvoke(byte a, byte r, byte g, byte b);
        #endregion

        #region Properties
        public byte A { get { return m_ControlColor.A; } }
        public byte R { get { return m_ControlColor.R; } }
        public byte G { get { return m_ControlColor.G; } }
        public byte B { get { return m_ControlColor.B; } }
        #endregion

        #region Overrides
        public override Widget Widget
        {
            get
            {
                return m_Widget;
            }
        }

        public override ragUi.ControlBase ControlBase
        {
            get
            {
                return m_ControlColor;
            }
        }

        public override void Destroy()
        {
            base.Destroy();
            m_Widget = null;
            m_ControlColor.Dispose();
            m_ControlColor = null;
        }
        #endregion

        #region Public Functions
        /// <summary>
        /// disable event handler temporarily.
        /// </summary>
        public void UpdateValueFromRemote( byte a, byte r, byte g, byte b )
        {
            if (m_ControlColor.InvokeRequired == true)
            {
                UpdateInvoke updateInvoke = new UpdateInvoke(UpdateValueFromRemote);

                object[] args = new object[4];
                args[0] = a;
                args[1] = r;
                args[2] = g;
                args[3] = b;
                m_ControlColor.Invoke(updateInvoke, args);
            }
            else
            {
                m_ControlColor.UpdateValueFromRemote(a, r, g, b);
            }

            UpdateModifiedIndication();
        }
        #endregion
    }
}
