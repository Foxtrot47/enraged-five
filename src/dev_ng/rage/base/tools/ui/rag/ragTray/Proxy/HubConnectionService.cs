﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Net.Sockets;
using ragCore;
using System.Net;
using LitJson;
using RSG.Rag.Contracts.Services;
using System.ServiceModel;
using RSG.Rag.Services;
using RSG.Base.Logging;

namespace ragTray
{
    /// <summary>
    /// Service that listens for incoming requests for the current game connection list.
    /// TODO: This should be "owned" by the hub manager as it can't function correctly without it.
    /// </summary>
    internal class HubConnectionService
    {
        #region Constants
        /// <summary>
        /// Port that the proxy listens on for game connection information.
        /// </summary>
        internal const int CONNECTION_SERVICE_PORT = 10000;

        /// <summary>
        /// Log message context.
        /// </summary>
        private const String LogCtx = "Hub Connection Service";
        #endregion // Constants

        #region Member Data
        /// <summary>
        /// Reference to the hub manager.
        /// </summary>
        private HubManager HubManager { get; set; }

        /// <summary>
        /// TCP listener that is listening for incoming game connection list requests.
        /// </summary>
        private TcpListener Listener { get; set; }

        /// <summary>
        /// Flag indicating whether the hub connection service is active.
        /// </summary>
        private bool ListenerActive { get; set; }

        /// <summary>
        /// Private field for the <see cref="ConnectionServicePort"/> property.
        /// </summary>
        private static int _connectionServicePort = CONNECTION_SERVICE_PORT;

        /// <summary>
        /// 
        /// </summary>
        private IConnectionService _connectionService;

        /// <summary>
        /// 
        /// </summary>
        private ServiceHost _connectionServiceHost;

        /// <summary>
        /// Reference to the log object to make use of.
        /// </summary>
        private readonly ILog _log;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Main constructor.
        /// </summary>
        internal HubConnectionService(HubManager hubManager, ILog log)
        {
            _log = log;
            HubManager = hubManager;
            ListenerActive = true;

            _log.MessageCtx(LogCtx, "Starting up.");
            Listener = new TcpListener(IPAddress.Any, ConnectionServicePort);
            Listener.Start();
            Listener.BeginAcceptTcpClient(DoAcceptTcpClientCallback, Listener);

            // Start up the WCF service.
            _connectionService = new ConnectionService(hubManager);
            _connectionServiceHost = new ServiceHost(_connectionService);
            _connectionServiceHost.Open();
        }
        #endregion // Constructor(s)

        #region Properties
        /// <summary>
        /// The port that we're listening on for game list requests.
        /// </summary>
        public static int ConnectionServicePort
        {
            get { return _connectionServicePort; }
            set { _connectionServicePort = value; }
        }
        #endregion // Properties

        #region Public Methods
        /// <summary>
        /// Call this on application shutdown.
        /// </summary>
        public void Shutdown()
        {
            _log.MessageCtx(LogCtx, "Shutting down.");
            ListenerActive = false;
            Listener.Stop();

            // Close down the WCF service.
            if (_connectionServiceHost != null)
            {
                _connectionServiceHost.Close();
                _connectionServiceHost = null;
                _connectionService = null;
            }
        }
        #endregion // Public Methods

        #region Async Methods
        /// <summary>
        /// 
        /// </summary>
        private void DoAcceptTcpClientCallback(IAsyncResult ar)
        {
            // Get the listener that handles the client request.
            TcpListener listener = (TcpListener)ar.AsyncState;

            if (ListenerActive)
            {
                using (TcpClient client = listener.EndAcceptTcpClient(ar))
                {
                    // Start the listener again.
                    listener.BeginAcceptTcpClient(DoAcceptTcpClientCallback, listener);

                    // Respond to the clients request
                    using (NetworkStream stream = client.GetStream())
                    {
                        // Attempt to read in the Json content
                        JsonData jsonData = JsonNetUtil.GetJsonFromConnection(stream, 10000, _log);
                        if (jsonData != null)
                        {
                            // Check whether the request was for the game list.
                            String action = (String)jsonData["action"];
                            if (action == "getgamelist")
                            {
                                _log.MessageCtx(LogCtx, "Processing game list request.");

                                // Create a string containing JSON data that we will return to the user.
                                String json = GetJSONString();
                                _log.MessageCtx(LogCtx, "Sending JSON: {0}", json);
                                byte[] data = Encoding.ASCII.GetBytes(json);

                                // Make sure that awe can write to the network stream.
                                if (stream.CanWrite)
                                {
                                    stream.Write(data, 0, data.Length);
                                }
                            }
                            else
                            {
                                _log.MessageCtx(LogCtx, "Unsupported action '{0}' encountered.", action);
                            }
                        }
                        else
                        {
                            _log.MessageCtx(LogCtx, "Unable to retrieve JSON data from connection.");
                        }
                    }
                }
            }
        }
        #endregion // Async Methods

        #region Private Methods
        /// <summary>
        /// Creates the JSON string containing the list of game connections.
        /// </summary>
        /// <returns></returns>
        private String GetJSONString()
        {
            // Generate the hub list array.
            LitJson.JsonData hubList = new LitJson.JsonData();
            hubList.SetJsonType(LitJson.JsonType.Array);

            // Create a copy of the hub manager's hub list.
            IEnumerable<ProxyHub> hubs = HubManager.ActiveHubs;

            // Go over each hub creating a new list.
            _log.MessageCtx(LogCtx, "{0} hubs are present.", hubs.Count());

            int index = 1;
            foreach (ProxyHub hub in hubs)
            {
                IConnectionListenerService connectionService = hub.ServiceManager.ConnectionListenerService;
                IConsoleProxy consoleService = hub.ServiceManager.ConsoleService;
                IGameProxyService gameService = hub.ServiceManager.GameService;

                // If one of these are null, then that hub must be in shutdown mode already so ignore it.
                if (connectionService == null || gameService == null)
                {
                    _log.MessageCtx(LogCtx, "Hub {0}/{1}: shutting down.", index++, hubs.Count());
                    continue;
                }

                _log.MessageCtx(LogCtx, "Hub {0}/{1}: Port {2}, Platform {3}, IP {4}, Console Port {5}, Default {6}.",
                    index++, hubs.Count(), connectionService.ConnectionPort, gameService.GetPlatform(), gameService.GetGameIP(),
                    consoleService == null ? -1 : consoleService.ConnectionPort, (hub == HubManager.DefaultHub));

                // Create the json object to return.
                LitJson.JsonData hubData = new LitJson.JsonData();
                hubData["port"] = connectionService.ConnectionPort;
                hubData["gameplatform"] = gameService.GetPlatform();
                hubData["gameip"] = gameService.GetGameIP();
                hubData["consoleport"] = consoleService == null ? -1 : consoleService.ConnectionPort;
                hubData["default"] = (hub == HubManager.DefaultHub);
                hubList.Add(hubData);
            }
            
            // Convert the json object to a string.
            LitJson.JsonWriter writer = new LitJson.JsonWriter();
            LitJson.JsonData jsonRoot = new LitJson.JsonData();
            jsonRoot["hubs"] = hubList;
            jsonRoot.ToJson(writer);

            return writer.TextWriter.ToString();
        }
        #endregion // Private Methods
    } // HubConnectionService
}
