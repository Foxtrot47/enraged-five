﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ragTray
{
    public partial class CloseConnectionToGameForm : Form
    {
        private GameProxy m_GameProxy;

        private int m_AnimT = 0;

        public CloseConnectionToGameForm(GameProxy proxy)
        {
            m_GameProxy = proxy;

            InitializeComponent();

            if (InvokeRequired)
            {
                Invoke( (MethodInvoker)delegate
                {
                    headerLabel.Text = string.Format( headerLabel.Text, m_GameProxy.GetPlatform(), m_GameProxy.GetGameIP() );
                } );
            }
            else
            {
                headerLabel.Text = string.Format( headerLabel.Text, m_GameProxy.GetPlatform(), m_GameProxy.GetGameIP() );
            }

            // Remove the [X] Close button
            ControlBox = false;

            // To ensure that if there's more than one window, they don't overlap.
            ++RagTrayIcon.sm_NumOpenFormInstances;
            FormClosed += new FormClosedEventHandler( CloseConnectionToGameForm_FormClosed );
        }

        protected override bool ShowWithoutActivation
        {
            get { return true; }
        }

        private const int WS_EX_TOPMOST = 0x00000008;
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams value = base.CreateParams;
                value.ExStyle |= WS_EX_TOPMOST;
                return value;
            }
        }

        private void CloseConnectionToGameForm_Load(object sender, EventArgs e)
        {
            try
            {
                TaskbarPosition taskbarPosition = new TaskbarPosition();

                switch (taskbarPosition.Side)
                {
                    case TaskbarSide.Bottom:
                        Location = new Point(
                            taskbarPosition.Size.Width - taskbarPosition.Location.X - Width,
                            taskbarPosition.Location.Y - Height * RagTrayIcon.sm_NumOpenFormInstances);
                        break;
                    case TaskbarSide.Top:
                        Location = new Point(
                            taskbarPosition.Size.Width - taskbarPosition.Location.X - Width,
                            taskbarPosition.Location.Y + taskbarPosition.Size.Height * RagTrayIcon.sm_NumOpenFormInstances);
                        break;
                    case TaskbarSide.Left:
                        Location = new Point(
                            taskbarPosition.Location.X + taskbarPosition.Size.Width,
                            taskbarPosition.Size.Height - taskbarPosition.Location.Y - Height * RagTrayIcon.sm_NumOpenFormInstances);
                        break;
                    case TaskbarSide.Right:
                        Location = new Point(
                            taskbarPosition.Location.X - Width,
                            taskbarPosition.Size.Height - taskbarPosition.Location.Y - Height * RagTrayIcon.sm_NumOpenFormInstances);
                        break;
                }
            }
            catch (Exception) { }// If this fails then we're ok to create the form as usual
        }

        private void CloseConnectionToGameForm_FormClosed( object sender, FormClosedEventArgs e )
        {
            --RagTrayIcon.sm_NumOpenFormInstances;
        }




        private void buttonYes_Click(object sender, EventArgs e)
        {
            m_GameProxy.OnCloseConnectionFormResult( DialogResult.Yes );
            Close();
        }

        private void buttonNo_Click(object sender, EventArgs e)
        {
            m_GameProxy.OnCloseConnectionFormResult( DialogResult.No );
            Close();
        }

        private void buttonStopAskingMe_Click(object sender, EventArgs e)
        {
            m_GameProxy.OnCloseConnectionFormResult( DialogResult.Abort );
            Close();
        }


        private void animTimer_Tick( object sender, EventArgs e )
        {
            m_AnimT += animTimer.Interval;
            if (m_AnimT > 350)
            {
                this.Opacity *= 0.9;
                if ( this.Opacity < 0.05 )
                {
                    m_GameProxy.OnCloseConnectionFormResult( DialogResult.No );
                    Close();
                }
            }
        }


        public void ConnectionRestored()
        {
            animTimer.Start();
            headerLabel.BackColor = Color.GreenYellow;
            headerLabel.Text = "Connection restored";
            this.Enabled = false;
        }
    }
}
