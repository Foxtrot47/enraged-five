﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Rag.Services.Exceptions
{
    /// <summary>
    /// Base class for widget related exceptions.
    /// </summary>
    /// <remarks>
    /// Only one of <see cref="WidgetPath"/> and <see cref="WidgetId"/> will be set. It
    /// depends on how the user was trying to access the widget.
    /// </remarks>
    public class WidgetException : Exception
    {
        #region Fields
        /// <summary>
        /// Private field for the <see cref="WidgetPath"/> property.
        /// </summary>
        private readonly String _widgetPath;

        /// <summary>
        /// Private field for the <see cref="WidgetId"/> property.
        /// </summary>
        private readonly uint _widgetId;
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="WidgetException"/> class
        /// based off a widget path.
        /// </summary>
        /// <param name="widgetPath"></param>
        /// <param name="message"></param>
        public WidgetException(String widgetPath, String message = null)
            : base(message)
        {
            _widgetPath = widgetPath;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="WidgetException"/> class
        /// based off a widget id.
        /// </summary>
        /// <param name="widgetId"></param>
        /// <param name="message"></param>
        public WidgetException(uint widgetId, String message = null)
            : base(message)
        {
            _widgetId = widgetId;
        }
        #endregion

        #region Properties
        /// <summary>
        /// Path to the widget that wasn't found.
        /// </summary>
        public String WidgetPath
        {
            get { return _widgetPath; }
        }
        
        /// <summary>
        /// Id of the widget that wasn't found.
        /// </summary>
        public uint WidgetId
        {
            get { return _widgetId; }
        }
        #endregion
    }
}
