﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ragTray
{
    // Code found here:
    // http://blogs.msdn.com/b/smondal/archive/2010/09/08/current-thread-must-be-set-to-single-thread-apartment-sta-mode-before-ole-calls-can-be-made.aspx
    // and here (for topmost)
    // http://stackoverflow.com/questions/4666580/how-can-i-set-topmost-at-the-savefiledialog-using-c

    public class STAFileDialogState
    {
        public DialogResult result;
        public FileDialog dialog;
        public bool topmost;

        public void ThreadProcShowDialog()
        {
            if ( !topmost )
            {
                result = dialog.ShowDialog();
                return;
            }

            Form f = new Form();
            f.Load += ( a, e ) =>
                {
                    f.Activate();
                    f.BringToFront();
                    f.TopMost = true;
                    result = dialog.ShowDialog( f );
                };

            f.Text = "Ignore";
            f.Width = 1;
            f.Height = 1;
            f.Visible = true;
            f.TopMost = true;
            f.Show();
            f.Close();            
        } 
    }

    public class STAFileDialog
    {
        public static DialogResult ShowDialog( FileDialog dialog, bool topmost )
        {
            STAFileDialogState state = new STAFileDialogState();
            state.topmost = topmost;
            state.dialog = dialog;

            System.Threading.Thread t = new System.Threading.Thread( state.ThreadProcShowDialog );
            t.Name = "STAFileDialog Thread";
            t.SetApartmentState( System.Threading.ApartmentState.STA );

            t.Start();
            t.Join();

            return state.result;
        } 
    }
}
