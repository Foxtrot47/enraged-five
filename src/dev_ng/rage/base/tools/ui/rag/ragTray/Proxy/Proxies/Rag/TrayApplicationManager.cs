﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Forms;
using System.Diagnostics;
using ragCore;
using System.Threading;
using System.Net.Sockets;
using ragWidgets;
using System.Net.NetworkInformation;
using System.Net;
using System.IO;
using RSG.Base.IO;
using System.Net.Mail;
using System.Threading.Tasks;
using RSG.Base.Configuration;
using RSG.Base.Logging;

namespace ragTray
{
    /// <summary>
    /// 
    /// </summary>
    public class TrayApplicationManager
    {
        #region Constants
        /// <summary>
        /// Handshake value to help ensure that Rag code remains synchronized with Rage bank code.  Increment this value 
        /// whenever there's a change in Rag code that requires a change in Rage code.  Keep this in sync with 
        /// RAG_VERSION in rage/base/tools/rag/RageApplication.cs.
        /// </summary>
        private const float RAG_VERSION = 2.0f;

        /// <summary>
        /// 
        /// </summary>
        private const int c_defaultBasePortNumber = 60000;
        #endregion // Constants

        #region Member Data
        private int m_BasePortNumber = c_defaultBasePortNumber;

        /// <summary>
        /// Index of the main application in the list of all applications.
        /// </summary>
        private uint m_MasterIndex = 0;
        private TrayApplication[] m_Applications;
        public TrayApplication[] Applications { get { return m_Applications; } }

        private TrayApplication m_MainApplication;
        public TrayApplication MainApplication { get { return m_MainApplication; } }

        public bool AppsLoaded { get; private set; }
        private volatile bool m_Quitting = false;

        private Thread m_updatePipeThread = null;

        /// <summary>
        /// String holding information about the platform we are connected to.
        /// </summary>
        public String PlatformString { get; private set; }

        /// <summary>
        /// String holding information about the build we are connected to.
        /// </summary>
        public String BuildConfigString { get; private set; }

        /// <summary>
        /// Reference to the log object.
        /// </summary>
        private readonly ILog _log;
        #endregion // Member Data

        #region Events
        /// <summary>
        /// 
        /// </summary>
        public event EventHandler UpdatePipeDisconnected;
        #endregion // Events

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="TrayApplicationManager"/> class.
        /// </summary>
        public TrayApplicationManager()
        {
            _log = LogFactory.ApplicationLog;
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// 
        /// </summary>
        public bool ReadMainAppInfo(INamedPipe pipe, String logCtx)
        {
            _log.MessageCtx(logCtx, "Reading main app info from connection at {0}.", pipe.Socket.RemoteEndPoint);

            // look for handshake packet
            bool timedOut;
            if (!AppRemotePacket.ReadPackets(pipe, this, out timedOut, _log, logCtx))
            {
                if (timedOut)
                {
                    _log.ErrorCtx(logCtx, "Rag timed out waiting to handshake with the remote application.");
                    rageMessageBox.ShowError("Rag timed out waiting to handshake with the application.  Either an unauthorized application " +
                        "attempted to connect with Rag, or your application may not be using Rage bank code version " + RAG_VERSION + ".",
                        "Connection Timed Out");
                }
                else
                {
                    _log.ErrorCtx(logCtx, "Failed to read initial packets.");
                }
                return false;
            }

            // Only create the main application when we receive the initial handshake packet
            TrayBankManager bankManager = new TrayBankManager(this);
            m_MainApplication = new TrayApplication(bankManager, m_BasePortNumber, c_defaultBasePortNumber + 1000);
            m_MainApplication.IsMainApp = true;
            m_BasePortNumber = m_MainApplication.PortNumber + 50;

            // send the base socket and version numbers to the game.
            AppRemotePacket p = new AppRemotePacket(pipe);
            p.BeginWrite();
            p.Write_s32(m_MainApplication.PortNumber);
            p.Write_float(RAG_VERSION);
            p.Send();

            // look for additional packets
            lock (m_MainApplication.PacketProcessor)
            {
                if (!AppRemotePacket.ReadPackets(pipe, this, out timedOut, _log, logCtx))
                {
                    if (timedOut)
                    {
                        rageMessageBox.ShowError("Rag timed out waiting for the application startup data.", "Connection Timed Out");
                    }
                    else
                    {
                        _log.ErrorCtx(logCtx, "Failed to read packets.");
                    }
                    return false;
                }
            }

            // make sure we have something to run:
            if (m_Applications.Length < 1)
            {
                rageMessageBox.ShowError("No applications were specified to run!", "No Applications");
                return false;
            }

            _log.MessageCtx(logCtx, "Establishing connection with client: {0} {1}", m_MainApplication.ExeName, m_MainApplication.Args);
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        public bool PacketHandler(AppRemotePacket packet, ILog log, String logCtx)
        {
            uint index;
            bool success = true;

            // our initial handshake packet containing the game's rag version number
            if (packet.AppCommand == AppRemotePacket.EnumPacketType.HANDSHAKE)
            {
                log.MessageCtx(logCtx, "Received initial handshake packet.");
                packet.Begin();
                float ragVersion = ragVersion = packet.Read_float();
                if (packet.CanRead())
                {
                    packet.Read_const_char();   // Application name, required for protocol but not used in this version of rag
                }
                packet.End();
                _log.Message("RAG Version: {0}", ragVersion);

                if (ragVersion != RAG_VERSION)
                {
                    string title;
                    string msg;
                    if (ragVersion == 0.0f)
                    {
                        title = "Rag Version Number Not Received";
                        msg = "Rag and Rage bank code are out of sync.  Need " + RAG_VERSION + " from Bank.\n\nPlease update your bank code.";
                    }
                    else
                    {
                        title = "Rag Version Mismatch";
                        msg = "Rag and Rage bank code are out of sync.  Expected " + RAG_VERSION + " but received " + ragVersion + "from Bank.\n\nPlease update " + ((RAG_VERSION < ragVersion) ? "Rag." : "your bank code.");
                    }

                    rageMessageBox.ShowError(msg, title);
                    Debug.Fail(title);
                    return false;
                }
            }
            // # of applications to create:
            else if (packet.AppCommand == AppRemotePacket.EnumPacketType.NUM_APPS)
            {
                log.MessageCtx(logCtx, "Received number of applications packet.");
                packet.Begin();
                uint numApps = packet.Read_u32();
                m_MasterIndex = packet.Read_u32();

                Debug.Assert(m_MasterIndex < numApps, "m_MasterIndex < numApps");

                if (packet.Length > 8)
                {
                    rageMessageBox.ShowError("Rag and Rage bank code are out of sync.  Need " + RAG_VERSION + " from Bank.\n\nPlease update your bank code.", "Rag Version Mismatch");
                    Debug.Fail("Rag Version Mismatch");
                    return false;
                }

                packet.End();
                log.MessageCtx(logCtx, "Num Apps: {0}", numApps);

                m_Applications = new TrayApplication[numApps];
                for (int i = 0; i < numApps; i++)
                {
                    if (i == m_MasterIndex)
                    {
                        // we already created the master app, so add it at the correct index
                        m_Applications[i] = m_MainApplication;
                    }
                    else
                    {
                        TrayBankManager bankManager = new TrayBankManager(this);
                        m_Applications[i] = new TrayApplication(bankManager, m_BasePortNumber, c_defaultBasePortNumber + 1000);
                        m_BasePortNumber = m_Applications[i].PortNumber + 50;
                    }
                }

                // try to retrieve the ip address of the application we're connected to
                if (packet.Pipe is NamedPipeSocket)
                {
                    NamedPipeSocket namedPipe = packet.Pipe as NamedPipeSocket;
                    Sensor.MainApplicationIpAddress = namedPipe.GetRemoteIpAddress();
                }
            }
            // the path of an application:
            else if (packet.AppCommand == AppRemotePacket.EnumPacketType.APP_NAME)
            {
                log.MessageCtx(logCtx, "Received application name packet.");
                packet.Begin();
                index = packet.Read_u32();
                String appName = packet.Read_const_char();
                m_Applications[index].AppName += appName;
                packet.End();
                log.MessageCtx(logCtx, "App{0} Name: {1}", index, appName);
            }
            // the visible name of an application:
            else if (packet.AppCommand == AppRemotePacket.EnumPacketType.APP_VISIBLE_NAME)
            {
                log.MessageCtx(logCtx, "Received visible application name packet.");
                packet.Begin();
                index = packet.Read_u32();
                String visibleName = packet.Read_const_char();
                m_Applications[index].VisibleName += visibleName;
                packet.End();
                log.MessageCtx(logCtx, "Visible App{0} Name: {1}", index, visibleName);
            }
            // the application args:
            else if (packet.AppCommand == AppRemotePacket.EnumPacketType.APP_ARGS)
            {
                log.MessageCtx(logCtx, "Received application arguments packet.");
                packet.Begin();
                index = packet.Read_u32();
                String args = packet.Read_const_char();
                m_Applications[index].Args += args;
                packet.End();
                log.MessageCtx(logCtx, "App{0} Args: {1}", index, args);
            }
            // the name of the pipe for bank communications:
            else if (packet.AppCommand == AppRemotePacket.EnumPacketType.PIPE_NAME_BANK)
            {
                log.MessageCtx(logCtx, "Received bank pipe name packet.");
                packet.Begin();
                index = packet.Read_u32();
                String name = packet.Read_const_char();
                m_Applications[index].PipeNameBank.AddString(name);
                packet.End();
                log.MessageCtx(logCtx, "Bank pipe {0}: {1}", index, name);
            }
            // the name of the pipe for text output communications:
            else if (packet.AppCommand == AppRemotePacket.EnumPacketType.PIPE_NAME_OUTPUT)
            {
                log.MessageCtx(logCtx, "Received output pipe name packet.");
                packet.Begin();
                index = packet.Read_u32();
                String name = packet.Read_const_char();
                m_Applications[index].PipeNameOutput.AddString(name);
                packet.End();
                log.MessageCtx(logCtx, "Output pipe {0}: {1}", index, name);
            }
            // the name of the pipe for event handling communications:
            else if (packet.AppCommand == AppRemotePacket.EnumPacketType.PIPE_NAME_EVENTS)
            {
                log.MessageCtx(logCtx, "Received events pipe name packet.");
                packet.Begin();
                index = packet.Read_u32();
                String name = packet.Read_const_char();
                m_Applications[index].PipeNameEvents.AddString(name);
                packet.End();
                log.MessageCtx(logCtx, "Event pipe {0}: {1}", index, name);
            }
            else if (packet.AppCommand == AppRemotePacket.EnumPacketType.PLATFORM_INFO)
            {
                log.MessageCtx(logCtx, "Received platform info packet.");
                packet.Begin();
                index = packet.Read_u32();
                string platformString = packet.Read_const_char();
                PlatformString = platformString;
                packet.End();
                log.MessageCtx(logCtx, "Platform name {0}: {1}", index, platformString);
            }
            else if (packet.AppCommand == AppRemotePacket.EnumPacketType.PS3_TARGET_ADDRESS)
            {
                log.MessageCtx(logCtx, "Received ps3 target address packet.");
                packet.Begin();
                index = packet.Read_u32();
                string ps3TargetAddr = packet.Read_const_char();
                Sensor.PS3TargetIpAddress = ps3TargetAddr;
                packet.End();
                log.MessageCtx(logCtx, "PS3 target ip {0}: {1}", index, ps3TargetAddr);
            }
            else if (packet.AppCommand == AppRemotePacket.EnumPacketType.BUILD_CONFIG)
            {
                log.MessageCtx(logCtx, "Received build config packet.");
                packet.Begin();
                index = packet.Read_u32();
                string buildConfigString = packet.Read_const_char();
                BuildConfigString = buildConfigString;
                packet.End();
                log.MessageCtx(logCtx, "Build Config {0}: {1}", index, buildConfigString);
            }
            else
            {
                log.WarningCtx(logCtx, "Unknown handshake packet received {0}.", packet.AppCommand);
                success = false;
            }

            return success;
        }

        /// <summary>
        /// 
        /// </summary>
        public void ProcessPipes()
        {
            // Check for main application disconnect.
            bool stopAllApps = MainApplication.PipesDisconnected;
            if (stopAllApps)
            {
                if (!MainApplication.OutputPipe.IsConnected())
                {
                    _log.Message("Output pipe has disconnected");
                }
                else if (!MainApplication.BankPipe.IsConnected())
                {
                    _log.Message("Bank pipe has disconnected");
                }
                else if (!MainApplication.InputHandler.IsConnected())
                {
                    _log.Message("Input handler has disconnected");
                }
                else
                {
                    _log.Message("Other disconnect");
                }
            }

            foreach (TrayApplication app in m_Applications)
            {
                // make sure the main application has disconnected:
                if (!stopAllApps && !(app.PipesDisconnected && app.WasConnected))
                {
                    lock (app.PacketProcessor)
                    {
                        app.PacketProcessor.Process(app.BankPipe);
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void LoadApps()
        {
            if (m_Applications != null)
            {
                // start the applications:
                foreach (TrayApplication app in m_Applications)
                {
                    if (app != MainApplication)
                    {
                        app.ShouldStart = true;
                    }

                    // start it if we have to:
                    if (app.ShouldStart)
                    {
                        _log.Message("Starting app {0}", app.VisibleName);
                    }
                }

                // connect all pipes:
                _log.Message("Connecting all application pipes.");

                foreach (TrayApplication app in m_Applications)
                {
                    if (!app.ConnectPipes())
                    {
                        _log.Message("Unable to connect pipes to {0}.", app.VisibleName);
                    }
                }
            }

            if (m_updatePipeThread == null)
            {
                m_updatePipeThread = new Thread(new ThreadStart(UpdatePipes));
                m_updatePipeThread.Name = "AppMan: Update Pipe Thread";
                m_updatePipeThread.Priority = ThreadPriority.Highest;
            }

            if (!m_updatePipeThread.IsAlive)
            {
                _log.Message("AppMan: Starting update pipe thread.");
                m_updatePipeThread.Start();
            }

            AppsLoaded = true;
        }

        /// <summary>
        /// This is the thread function
        /// </summary>
        public void UpdatePipes()
        {
            _log.Message("Started " + m_updatePipeThread.Name);
            List<Socket> readSockets = new List<Socket>();

            while (!m_Quitting)
            {
                try
                {
                    bool stopAllApps = false;
                    if (MainApplication.PipesDisconnected)
                    {
                        stopAllApps = true;

                        if (!MainApplication.OutputPipe.IsConnected())
                        {
                            _log.Message("Output pipe has disconnected");
                        }
                        else if (!MainApplication.BankPipe.IsConnected())
                        {
                            _log.Message("Bank pipe has disconnected");
                        }
                        else if (!MainApplication.InputHandler.IsConnected())
                        {
                            _log.Message("Input handler has disconnected");
                        }
                        else if (Sensor.CurrentSensor == null)
                        {
                            _log.Message("Sensor is null");
                        }
                        else
                        {
                            _log.Message("Other disconnect");
                        }
                    }

                    readSockets.Clear();
                    foreach (TrayApplication app in m_Applications)
                    {
                        readSockets.Add(app.OutputPipe.Socket);
                        readSockets.Add(app.BankPipe.Socket);
                    }

                    try
                    {
                        // 10 seconds
                        Socket.Select(readSockets, null, null, 10 * 1000 * 1000);
                    }
                    catch (ThreadAbortException e)
                    {
                        _log.ToolException(e, "Appman: UpdatePipes() ThreadAbortException #1");
                        break;
                    }
                    catch (Exception e)
                    {
                        _log.ToolException(e, "Appman: UpdatePipes() Exception ");
                        if (!stopAllApps)
                        {
                            _log.Message(e.ToString());
                            continue;
                        }
                    }

                    foreach (TrayApplication app in m_Applications)
                    {
                        // make sure the main application has disconnected:
                        if (stopAllApps || (app.PipesDisconnected && app.WasConnected))
                        {
                        }
                        else
                        {
                            // Check whether we can process data from the output pipe.
                            if (app.OutputPipe.IsValid() && readSockets.Contains(app.OutputPipe.Socket))
                            {
                                app.ReadOutputPipe();
                            }

                            // Check whether we can process data from the bank pipe.
                            if (app.BankPipe.IsValid() && readSockets.Contains(app.BankPipe.Socket))
                            {
                                Stopwatch sw = Stopwatch.StartNew();
                                lock (app.PacketProcessor)
                                {
                                    TimeSpan elapsed = sw.Elapsed;
                                    if (elapsed.TotalSeconds > 1)
                                    {
                                        _log.Message("Waited {0} seconds for lock to be release.", elapsed.TotalSeconds);
                                    }

                                    if (app.PacketProcessor.PipeOwner == null)
                                    {
                                        app.PacketProcessor.PipeOwner = app.PacketProcessor;
                                    }

                                    if (app.PacketProcessor.PipeOwner == app.PacketProcessor)
                                    {
                                        app.PacketProcessor.Update(app.BankPipe);
                                        _lastBankPipeUpdate = DateTime.Now;
                                        _secondsSinceLastUpdate = 0;
                                    }
                                    else
                                    {
                                        DateTime now = DateTime.Now;
                                        TimeSpan timeSinceLastUpdate = now - _lastBankPipeUpdate;

                                        if ((int)(timeSinceLastUpdate.TotalSeconds) != _secondsSinceLastUpdate)
                                        {
                                            _secondsSinceLastUpdate = (int)timeSinceLastUpdate.TotalSeconds;
                                            _log.Message("Potential bank pipe processing stall.  {0} seconds since last update.", _secondsSinceLastUpdate);
                                        }
                                    }
                                }
                            }
                        }
                    }

                    if (stopAllApps)
                    {
                        _log.Message("Appman: Stop all apps.");
                        break;
                    }
                }
                catch (ThreadAbortException e)
                {
                    _log.ToolException(e, "Appman: UpdatePipes() ThreadAbortException #2");
                    break;
                }
            }

            _log.Message("Ended " + m_updatePipeThread.Name);
            m_updatePipeThread = null;
            if (UpdatePipeDisconnected != null)
            {
                UpdatePipeDisconnected(this, EventArgs.Empty);
            }
        }

        private DateTime _lastBankPipeUpdate = DateTime.Now;
        private int _secondsSinceLastUpdate = 0;

        /// <summary>
        /// Event handler for when a new bank is added.
        /// </summary>
        public void AddBank(BankPipe pipe, WidgetBank bank)
        {
            TrayApplication app = FindApplication(pipe);
            if (app != null)
            {
                (app.BankMgr as TrayBankManager).AddBank(bank);
            }
        }

        /// <summary>
        /// Event handler for when a bank is removed.
        /// </summary>
        public void RemoveBank(BankPipe pipe, WidgetBank bank)
        {
            TrayApplication app = FindApplication(pipe);
            if (app != null)
            {
                (app.BankMgr as TrayBankManager).RemoveBank(bank);
            }
        }

        /// <summary>
        /// Finds an application based on the bank pipe associated with it.
        /// </summary>
        public TrayApplication FindApplication(INamedPipe bankPipe)
        {
            if (m_Applications != null)
            {
                foreach (TrayApplication app in m_Applications)
                {
                    if (app.BankPipe == bankPipe)
                        return app;
                }
            }

            return null;
        }

        /// <summary>
        /// Called when the game has been stopped.
        /// </summary>
        public void Stop()
        {
            _log.Message("AppMan: Stop");
            m_Quitting = true;

            Thread.Sleep(500);
            if (m_updatePipeThread != null)
            {
                m_updatePipeThread.Abort();
            }


            if (m_Applications != null)
            {
                foreach (TrayApplication app in m_Applications)
                {
                    TrayBankManager bmgr = app.BankMgr as TrayBankManager;
                    if (bmgr != null)
                    {
                        bmgr.RemoveHandlers(m_MainApplication.PacketProcessor);
                    }
                    app.Dispose();
                }
            }
        }
        #endregion // Public Methods
    } // TrayApplicationManager
}
