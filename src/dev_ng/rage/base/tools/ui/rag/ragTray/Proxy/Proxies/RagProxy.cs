﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading;
using System.Net.Sockets;
using System.Diagnostics;
using ragCore;
using System.Net;
using ragCompression;
using RSG.Base.Logging;


namespace ragTray
{
    /*
     * 
     * 
     * 
     * 
     * 
     * Note: See this: https://devstar.rockstargames.com/wiki/index.php/Proxy_Dev_Docs
     * 
     * 
     * 
     * 
     * */

    public class RagProxy : AppProxy
    {
        #region Constants
        /// <summary>
        /// Name of this app proxy.
        /// </summary>
        private const String c_name = "Rag Proxy";
        #endregion // Constants

        private TcpClient m_RagPipeClient;
        private Thread m_RagPipeThread;
        private DateTime _ragPipeThreadTimeout = DateTime.MinValue;
        private BankPipe m_BankPipe;

        private List<NetworkData> m_AdditionalBankDataToForward = new List<NetworkData>();

        public int ConnectionPort { get; set; }

        Dictionary<ragCore.PipeID.EnumSocketOffset, Queue<NetworkData>> m_DataToForward = new Dictionary<ragCore.PipeID.EnumSocketOffset, Queue<NetworkData>>();
        TcpClient[] m_RagClients = new TcpClient[(int)ragCore.PipeID.EnumSocketOffset.SOCKET_OFFSET_USER5];
        Dictionary<TcpClient, InterProxyStream> m_TcpToStreamMap = new Dictionary<TcpClient, InterProxyStream>();
        private IPAddress ConnectionAddress { get; set; }
        byte[] _partialBankRawData = new byte[0] {};

        private bool _sentWindowHandle = false;
        public bool SentWindowHandle
        {
            get { return _sentWindowHandle; }
        }

        public static event EventHandler StartedForwardingDataToRag;
        public static event ProgressEventHandler ForwardingDataToRagProgress;
        public static event EventHandler EndedForwardingDataToRag;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="hub"></param>
        /// <param name="connectionAddress"></param>
        /// <param name="connectionPort"></param>
        /// <param name="serviceManager"></param>
        public RagProxy( ProxyHub hub, IPAddress connectionAddress, int connectionPort)
            : base(c_name, hub)
        {
            ConnectionAddress = connectionAddress;
            ConnectionPort = connectionPort;

            m_RagPipeThread = new Thread( new ThreadStart( RagePipeProc ) );
            m_RagPipeThread.Name = String.Format("RagProxy '{0}' connection thread", UID);
            m_RagPipeThread.IsBackground = true;
            m_RagPipeThread.Start();
        }

        public override void Stop()
        {
            lock ( m_RagClients )
            {
                foreach ( var item in m_RagClients )
                {
                    if ( item != null && item.Connected )
                    {
                        item.GetStream().Close();
                        item.Close();
                    }
                }
            }

            foreach ( var item in m_HubStreams )
            {
                item.Value.RemoveProxy( this );
            }

            base.Stop();
        }

        private void RagePipeProc()
        {
            Log("Started " + m_RagPipeThread.Name);

            try
            {
                // Wait until game has connected, 
                // Then connect to RagApp
                // Set up "fake" handshake between this proxy and Rag
                while (State == AppProxy.ProxyState.Initialising)
                {
                    while (m_ServiceManager.GameService == null)
                    {
                        // The GameProxy has been removed but a new one hasn't
                        // been added yet.
                        Thread.Sleep(100);
                    }

                    // Wait until the GameProxy has completed the initial handshake with the game.
                    IList<AppRemotePacket> handshake = m_ServiceManager.GameService.GetHandshakePackets();
                    if (handshake.Count == 0)
                    {
                        Thread.Sleep(500);
                        continue;
                    }

                    // Temporary ugly hack, but we need to sleep here for a short while in order for
                    // Rag to re-create the tcplistener (essentially steal it from the ProxyConnection)
                    // Todo fix this
                    Log("Sleeping 2000 ms");
                    Thread.Sleep(2000);

                    m_ServiceManager.GameService.GameDisconnected += new GameEventHandler(GameService_GameDisconnected);

                    Log("Attempting to connect to Rag on {0}:{1}", ConnectionAddress, ConnectionPort);
                    m_RagPipeClient = new TcpClient(ConnectionAddress.ToString(), ConnectionPort);
                    m_BankPipe = new StreamBankPipe(m_RagPipeClient.GetStream());

                    Log("Sending {0} handshake packets", handshake.Count);
                    foreach (AppRemotePacket appPacket in handshake)
                    {
                        AppRemotePacket copy = new AppRemotePacket(m_BankPipe, appPacket);
                        copy.Send();
                    }

                    break;
                }

                if (State != ProxyState.Initialising)
                {
                    Log("Aborting initialisation.");
                    return;
                }

                // Receive the base port for Rag's pipes.
                Log("Waiting for address packet from Rag...");
                RemotePacket packet = new RemotePacket(m_BankPipe);
                packet.ReceiveHeader(m_BankPipe);
                packet.ReceiveStorage(m_BankPipe);
                int port = packet.Read_s32();
                float version = packet.Read_float();

                Log("...received, base port={0}; version={1}", port, version);

                if (!m_RagPipeClient.Connected)
                {
                    Log("Not connected to Rag anymore, aborting initialisation.");
                    State = ProxyState.Stopping;
                    return;
                }

                Log("Creating threads to connect to Rag's pipes");
                // Attempt to connect to the pipes, then wait until they're all done.
                for (int i = 0; i < (int)ragCore.PipeID.EnumSocketOffset.SOCKET_OFFSET_PROFILE; ++i)
                {
                    var t = new Tuple<string, int, int>(IPAddress.Loopback.ToString(), port + i, i);
                    Thread pipeThread = new Thread(new ParameterizedThreadStart(ConnectToPipeThread));
                    pipeThread.Name = String.Format("Rag Proxy PipeThread {0} p={1}", UID, (port + i));
                    pipeThread.IsBackground = true;
                    pipeThread.Start(t);
                }

                int retryCount = 0;
                while (State == AppProxy.ProxyState.Initialising)
                {
                    if (++retryCount > 600) // wait 60 seconds
                    {
                        Log("Could not connect to pipes before the timeout.");
                        State = ProxyState.Stopping;
                        return;
                    }

                    Thread.Sleep(100);
                    lock (m_RagClients)
                    {
                        if (m_RagClients[0] == null || m_RagClients[1] == null || m_RagClients[2] == null)
                        {
                            continue;
                        }

                        if (!m_RagClients[0].Connected || !m_RagClients[1].Connected || !m_RagClients[2].Connected)
                        {
                            continue;
                        }
                    }

                    Log("All clients connected");
                    break;
                }


                if (State != ProxyState.Initialising)
                {
                    return;
                }

                // Acquire any packets creating/destroying widgets since the start of the game, and
                // pass them on to Rag.
                // We lock our game proxy to make sure that no packets are missed between acquiring
                // the initial packets and registering the streams.
                byte[] handleData = new byte[RemotePacket.HEADER_SIZE + 4];
                bool sendHandle = true;

                lock (m_ServiceManager.GameService.AcquireLockObject())
                {
                    TimeSpan elapsed = TimeAction(() =>
                        {
                            IList<NetworkData> initialWidgets = m_ServiceManager.GameService.GetAllWidgetPackets();

                            if (initialWidgets.Any())
                            {
                                int widgetCount = initialWidgets.Count;

                                // Threshold at which we start showing the progress bar.
                                const int c_progressThreshold = 500;

                                // How often we will report progress (every 100 packets).
                                const int c_progressInterval = 100;

                                if (StartedForwardingDataToRag != null && widgetCount > c_progressThreshold)
                                {
                                    StartedForwardingDataToRag(this, EventArgs.Empty);
                                }

                                TcpClient c = m_RagClients[(int)ragCore.PipeID.EnumSocketOffset.SOCKET_OFFSET_BANK];
                                for (int i = 0; i < widgetCount; i++)
                                {
                                    // Report progress every 100 packets.
                                    if (ForwardingDataToRagProgress != null && widgetCount > c_progressThreshold && i % c_progressInterval == 0)
                                    {
                                        ForwardingDataToRagProgress(this, new ProgressEventArgs { Current = i, UpperBound = widgetCount });
                                    }

                                    NetworkData nd = initialWidgets[i];
                                    c.GetStream().Write(nd.Data, 0, nd.Count);
                                }

                                if (EndedForwardingDataToRag != null && widgetCount > c_progressThreshold)
                                {
                                    EndedForwardingDataToRag(this, EventArgs.Empty);
                                }
                            }

                            Log("Subscribing to streams.");
                            SubscribeToStream("bankraw", m_ServiceManager);
                            SubscribeToStream("bankcompressed", m_ServiceManager);
                            SubscribeToStream("output", m_ServiceManager);
                            SubscribeToStream("event", m_ServiceManager);

                            m_TcpToStreamMap[m_RagClients[(int)ragCore.PipeID.EnumSocketOffset.SOCKET_OFFSET_BANK]] = m_HubStreams["bankraw"];
                            m_TcpToStreamMap[m_RagClients[(int)ragCore.PipeID.EnumSocketOffset.SOCKET_OFFSET_OUTPUT]] = m_HubStreams["output"];
                            m_TcpToStreamMap[m_RagClients[(int)ragCore.PipeID.EnumSocketOffset.SOCKET_OFFSET_EVENTS]] = m_HubStreams["event"];

                            m_DataToForward[ragCore.PipeID.EnumSocketOffset.SOCKET_OFFSET_BANK] = new Queue<NetworkData>();
                            m_DataToForward[ragCore.PipeID.EnumSocketOffset.SOCKET_OFFSET_OUTPUT] = new Queue<NetworkData>();
                            m_DataToForward[ragCore.PipeID.EnumSocketOffset.SOCKET_OFFSET_EVENTS] = new Queue<NetworkData>();


                            // Read fuzzy window handle
                            TcpClient bankClient = m_RagClients[(int)ragCore.PipeID.EnumSocketOffset.SOCKET_OFFSET_BANK];

                            int bytesRead = bankClient.GetStream().Read(handleData, 0, handleData.Length);

                            if (bytesRead == handleData.Length)
                            {
                                foreach (RagProxy proxy in m_HubStreams["bankraw"].Proxies.OfType<RagProxy>())
                                {
                                    if (proxy != this && proxy.SentWindowHandle)
                                    {
                                        // Already another Rag open
                                        sendHandle = false;
                                    }
                                }
                            }


                            if (State == AppProxy.ProxyState.Initialising)
                            {
                                State = AppProxy.ProxyState.ReadyWorking;
                            }
                        });

                    if (elapsed.TotalSeconds > 1)
                    {
                        Log("Rag proxy held packet processor lock for more {0} seconds while sending initial packets.", elapsed.TotalSeconds);
                    }
                }

                // Parse the handle data to get at what handle we were sent from the client.
                RemotePacket handlePacket = new RemotePacket(null);
                int read = 0;
                handlePacket.ReceiveHeader(handleData, ref read);
                handlePacket.ReceiveStorage(handleData, ref read);
                uint windowHandle = handlePacket.Read_u32();

                if (sendHandle && windowHandle != 0)
                {
                    // Can't do this inside the lock above. Shouldn't be necessary.
                    m_HubStreams["bankraw"].Write(this, handleData);
                    _sentWindowHandle = true;
                }
            }
            catch (SocketException)
            {
                Log("Encountered connection error during initialization, shutting down...");
                State = ProxyState.Stopping;
            }
            catch (IOException)
            {
                Log("Encountered connection error during initialization, shutting down...");
                State = ProxyState.Stopping;
            }
        }

        void GameService_GameDisconnected( object proxy, GameConnectedEventArgs args )
        {
            Log( "Game disconnected, stopping..." );
            if (State != ProxyState.Stopped)
            {
                State = ProxyState.Stopping;
            }
        }

        public void ConnectToPipeThread( object context )
        {
            var t = (Tuple<string, int, int>)context;
            string ip = t.Item1;
            int port = t.Item2;
            int pipeSlot = t.Item3;


            Log( "Connecting to pipe {2}@ {0}:{1}", ip, port, pipeSlot );

            TcpClient c = null;
            while ( State == ProxyState.Initialising )
            {
                try
                {
                    c = new TcpClient();
                    c.SendTimeout = 15 * 1000; // 15 sec
                    c.NoDelay = true;
                    c.Connect( ip, port );
                    Log( "Connected to pipe {2}@ {0}:{1} on local port {3}", ip, port, pipeSlot, ((IPEndPoint)c.Client.LocalEndPoint).Port );
                    break;
                }
                catch ( SocketException )
                {
                    Thread.Sleep( 500 );
                }
            }

            lock ( m_RagClients )
            {
                Debug.Assert( c != null );
                Debug.Assert( m_RagClients[pipeSlot] == null );
                m_RagClients[pipeSlot] = c;
            }
        }


        public override void Update()
        {
            if ( !IsRunning )
            {
                bool stillStopping = m_RagPipeThread.IsAlive;
                if (!stillStopping)
                {
                    Log( "All threads stopped." );
                    Stop();
                }

                if (_ragPipeThreadTimeout == DateTime.MinValue)
                {
                    // Give it 5 seconds to shutdown cleanly before killing it.
                    _ragPipeThreadTimeout = DateTime.Now.AddSeconds(5);
                }
                else if (DateTime.Now > _ragPipeThreadTimeout)
                {
                    Log("Rag Pipe Thread didn't stop cleanly. Killing it off.");
                    m_RagPipeThread.Abort();
                    Stop();
                }

                return;
            }

            State = AppProxy.ProxyState.ReadyIdle;

            // Take data that comes from the tcp clients that are connected to Rag
            // and pass it on to the hub stream.
            ProcessRagClients();

            // Check whether we are stopping
            if ( State == AppProxy.ProxyState.Stopping )
            {
                return;
            }

            // Take the data we've received from the game
            // and pass it on to Rag over the appropriate tcp client.
            ForwardData();

            // Check if there is any additional data to forward.
            if ( IsRunning && m_AdditionalBankDataToForward.Count != 0 )
            {
                ForwardAdditionalData();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void ProcessRagClients()
        {
            foreach (TcpClient tcpClient in m_RagClients)
            {
                if (tcpClient == null)
                {
                    continue;
                }

                try
                {
                    while (tcpClient.GetStream().DataAvailable)
                    {
                        byte[] dataRead = new byte[tcpClient.Available];
                        int bytesRead = tcpClient.GetStream().Read(dataRead, 0, dataRead.Length);

                        InterProxyStream s = m_TcpToStreamMap[tcpClient];

                        // Need to handle this specifically since Rag and the game sends
                        // bank packets in different ways. 
                        // See https://devstar.rockstargames.com/wiki/index.php/Proxy_Dev_Docs#Bank_Packets
                        if (s.Name == "bankraw")
                        {
                            byte[] data;
                            if (_partialBankRawData.Length == 0)
                            {
                                data = dataRead;
                            }
                            else
                            {
                                data = new byte[_partialBankRawData.Length + bytesRead];
                                Buffer.BlockCopy(_partialBankRawData, 0, data, 0, _partialBankRawData.Length);
                                Buffer.BlockCopy(dataRead, 0, data, _partialBankRawData.Length, bytesRead);
                            }
                            
                            // Determine the offset of only the complete packets.
                            int offset = 0;
                            while (offset < data.Length)
                            {
                                // Make sure we have enough data to read in the header.
                                if (offset + RemotePacket.HEADER_SIZE > data.Length)
                                {
                                    break;
                                }

                                BankRemotePacket p = new BankRemotePacket(null);
                                p.ReceiveHeader(data, ref offset);

                                // Make sure we have enough data to read the data.
                                if (offset + p.Length > data.Length)
                                {
                                    offset -= RemotePacket.HEADER_SIZE;
                                    break;
                                }

                                offset += p.Length;
                            }

                            // Forward only the complete data.
                            s.Write(this, data, 0, offset);

                            int totalLength = offset;
                            noCompressionData compresser = new noCompressionData();
                            compresser.CompressedSize = compresser.DecompressedSize = (uint)totalLength;
                            totalLength += compresser.GetHeaderSize();

                            byte[] compressedData = new byte[totalLength];
                            int dataOffset = compresser.BuildHeader(compressedData, 0);
                            Buffer.BlockCopy(data, 0, compressedData, dataOffset, offset);

                            NetworkData nd = new NetworkData(compressedData, 0, compressedData.Length);
                            m_AdditionalBankDataToForward.Add(nd);

                            // Check whether we read all the data or if we need to keep track of any excess/non compelte packets.
                            if (offset == data.Length)
                            {
                                _partialBankRawData = new byte[0] { };
                            }
                            else
                            {
                                int remainingData = data.Length - offset;
                                _partialBankRawData = new byte[remainingData];
                                Buffer.BlockCopy(data, offset, _partialBankRawData, 0, remainingData);
                            }
                        }
                        else
                        {
                            s.Write(this, dataRead, 0, bytesRead);
                        }

                        State = AppProxy.ProxyState.ReadyWorking;
                    }
                }
                catch (IOException)
                {
                    State = AppProxy.ProxyState.Stopping;
                    break;
                }
            }
        }

        /// <summary>
        /// Forwards any data that was detected on the pipes that we are monitoring.
        /// </summary>
        private void ForwardData()
        {
            // Iterate over all the data that needs to be forwarded on to the RAG UI.
            foreach (KeyValuePair<ragCore.PipeID.EnumSocketOffset, Queue<NetworkData>> kvp in m_DataToForward)
            {
                TcpClient client = m_RagClients[(int)kvp.Key];
                Queue<NetworkData> queue = kvp.Value;

                // Create a copy of the queue to work with
                IList<NetworkData> dataToForward = null;
				lock (queue)
				{
                    if (queue.Any())
                    {
                        dataToForward = queue.ToList();
                        queue.Clear();
                    }
				}
                
                // Is there any data to forward?
				if (dataToForward != null)
				{    
                    try
                    {
                        foreach (NetworkData data in dataToForward)
                        {
                            client.GetStream().Write(data.Data, data.Offset, data.Count);
                        }

                        State = ProxyState.ReadyWorking;
                    }
                    catch (IOException)
                    {
                        State = AppProxy.ProxyState.Stopping;
                        break;
                    }
                    catch (SocketException)
                    {
                        State = AppProxy.ProxyState.Stopping;
                        break;
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void ForwardAdditionalData()
        {
            // The game service can be null if the proxy is shutting down.
            if (m_ServiceManager == null || m_ServiceManager.GameService == null)
            {
                return;
            }

            // We need to make sure that we 
            // 1) have the lock on the game's bank pipe
            // 2) the game's bank pipe is clear, i.e. all the data that is on it
            //    has been processed. Otherwise we'd be potentially interjecting our data
            //    in the middle of another packet.
            lock (m_ServiceManager.GameService.AcquireLockObject())
            {
                TimeSpan elapsed = TimeAction(() =>
                    {
                        if (m_ServiceManager.GameService.IsPipeClear())
                        {
                            m_ServiceManager.GameService.SetPipeOwner(this);
                        }

                        if (m_ServiceManager.GameService.GetPipeOwner() == this)
                        {
                            foreach (NetworkData data in m_AdditionalBankDataToForward)
                            {
                                m_HubStreams["bankcompressed"].Write(this, data.Data, data.Offset, data.Count);
                            }

                            if (_partialBankRawData.Length == 0)
                            {
                                m_ServiceManager.GameService.SetPipeOwner(null);
                            }

                            m_AdditionalBankDataToForward.Clear();
                        }
                    });

                if (elapsed.TotalSeconds > 1)
                {
                    Log("Rag proxy held packet processor lock for more {0} seconds while forwarding additional data.", elapsed.TotalSeconds);
                }
            }
        }

        private TimeSpan TimeAction(Action blockingAction)
        {
            Stopwatch stopWatch = Stopwatch.StartNew();
            blockingAction();
            stopWatch.Stop();
            return stopWatch.Elapsed;
        }

        public override void Write( string streamName, byte[] data, int offset, int count )
        {
            Debug.Assert( State != ProxyState.Initialising );
            if ( !IsRunning )
            {
                return;
            }

            NetworkData nd = new NetworkData( data, offset, count );
            Queue<NetworkData> forwardingQueue = null;
            if ( streamName == "bankcompressed" )
            {
                // This data either comes from the game or from another RagProxy, and is "compressed", 
                // so forward it on to Rag
                forwardingQueue = m_DataToForward[ragCore.PipeID.EnumSocketOffset.SOCKET_OFFSET_BANK];

            }
            else if ( streamName == "bankraw" )
            {
                // We're receiving "raw" widget data, possibly from another Rag instance. Convert it
                // into compressed data and pass it on the regular bank tcp client.
                forwardingQueue = m_DataToForward[ragCore.PipeID.EnumSocketOffset.SOCKET_OFFSET_BANK];

                int totalLength = count;
                noCompressionData compresser = new noCompressionData();
                compresser.CompressedSize = compresser.DecompressedSize = (uint)totalLength;
                totalLength += compresser.GetHeaderSize();

                byte[] compressedData = new byte[totalLength];
                int dataOffset = compresser.BuildHeader(compressedData, 0);
                Buffer.BlockCopy(data, offset, compressedData, dataOffset, count);
                data = compressedData;
                offset = 0;
                count = data.Length;

                nd = new NetworkData(data, offset, count);
            }
            else if ( streamName == "output" )
            {
                // Forward any tty output on to RAG.
                forwardingQueue = m_DataToForward[ragCore.PipeID.EnumSocketOffset.SOCKET_OFFSET_OUTPUT];
            }
            else if ( streamName == "event" )
            {
                // This seems a bit odd to me, does Rag really *read* event data?
                forwardingQueue = m_DataToForward[ragCore.PipeID.EnumSocketOffset.SOCKET_OFFSET_EVENTS];
            }

            // Check if the data is on a stream we are interested in.
            if ( forwardingQueue != null )
            {
                lock ( forwardingQueue )
                {
                    forwardingQueue.Enqueue( nd );  
                }
            }
        }

        public override string GetShortDescription()
        {
            return "Rag@" + GetShortStateDescription();
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public delegate void ProgressEventHandler(Object sender, ProgressEventArgs e);

    /// <summary>
    /// 
    /// </summary>
    public class ProgressEventArgs : EventArgs
    {
        /// <summary>
        /// Gets/sets the current amount of work done by the operation. This is always less
        ///  than or equal to System.Management.ProgressEventArgs.UpperBound.
        /// </summary>
        public int Current { get; set; }
        
        /// <summary>
        /// Gets or sets optional additional information regarding the operation's progress.
        /// </summary>
        public string Message { get; set; }
        
        /// <summary>
        /// Gets/sets the total amount of work required to be done by the operation.
        /// </summary>
        public int UpperBound { get; set; }
    } // ProgressEventArgs
}
