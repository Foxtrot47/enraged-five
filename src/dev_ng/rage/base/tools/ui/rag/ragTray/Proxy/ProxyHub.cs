﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading;
using System.Net.Sockets;
using System.Diagnostics;
using System.Collections.Concurrent;
using System.Threading.Tasks;
using RSG.Base.Logging;


namespace ragTray
{
    public interface IUINotifyService
    {
        void Notify(string title, string msg);
    }

    public interface IServicesProxy
    {
        int Port { get; }
    }

    public class GameConnectedEventArgs
    {
        public string Platform;
        public String BuildConfig;

        public GameConnectedEventArgs()
        {
        }

        public GameConnectedEventArgs( string platform, String buildConfig )
        {
            Platform = platform;
            BuildConfig = buildConfig;
        }
    }

    public class ServiceManager
    {
        public Dictionary<string, InterProxyStream> HubStreams { get; set; }

        public IGameProxyService GameService { get; set; }
        public IUINotifyService NotifyService {get; set;}
        public IConnectionListenerService ConnectionListenerService { get; set; }
        public IConsoleProxy ConsoleService { get; set; }
        public IServicesProxy ServicesProxy { get; set; }

        public ServiceManager()
        {
            HubStreams = new Dictionary<string, InterProxyStream>();
        }
    }

    /// <summary>
    /// Hub which contains a number of proxies for routing communication between game/external apps like the RAG UI.
    /// </summary>
    public class ProxyHub : IDisposable
    {
        #region Fields
        /// <summary>
        /// Reference to the log object this proxy hub can use for logging messages.
        /// </summary>
        private readonly ILog _log;

        /// <summary>
        /// Flag to detect re-entrant calls 
        /// </summary>
        private bool _shuttingDown = false;
        #endregion

        #region Properties
        /// <summary>
        /// Service manager for this proxy hub.
        /// </summary>
        public ServiceManager ServiceManager { get; private set; }

        /// <summary>
        /// Unique identifier for this proxy hub.
        /// </summary>
        public uint UID { get; private set; }

        /// <summary>
        /// Accessor to get the number of app proxies that this hub contains.
        /// </summary>
        public uint AppProxyCount
        {
            get
            {
                lock (AppProxies)
                {
                    return (uint)AppProxies.Count;
                }
            }
        }

        /// <summary>
        /// List of proxies this hub contains.
        /// </summary>
        private IList<AppProxy> AppProxies { get; set; }

        /// <summary>
        /// Thread safe producer/consumer collection that blocks on removal methods until data gets added.
        /// </summary>
        private BlockingCollection<ProxyHubJob> JobQueue { get; set; }

        /// <summary>
        /// Context to use for any messages logged by this proxy hub.
        /// </summary>
        public String LogCtx
        {
            get { return String.Format("Hub {0}", UID); }
        }
        #endregion // Properties

        #region Events
        /// <summary>
        /// Event that gets called when all the app proxies have shutdown.
        /// </summary>
        public event EventHandler AllAppsShutDown;
        #endregion // Events

        #region Enums
        /// <summary>
        /// Type of job to perform.
        /// </summary>
        private enum JobType
        {
            ShutDownAll,
            ShutDownProxy,
            HandleAppProxyStopped
        }
        #endregion // Enums

        #region Private Classes
        /// <summary>
        /// A job that the proxy hub needs to perform.
        /// </summary>
        private class ProxyHubJob
        {
            #region Properties
            /// <summary>
            /// Type of job to perform.
            /// </summary>
            public JobType Type { get; set; }

            /// <summary>
            /// App proxy to perform the job on (can be null).
            /// </summary>
            public AppProxy Proxy { get; set; }
            #endregion // Properties

            #region Constructor(s)
            /// <summary>
            /// 
            /// </summary>
            public ProxyHubJob(JobType type)
                : this(type, null)
            {
            }

            /// <summary>
            /// 
            /// </summary>
            public ProxyHubJob(JobType type, AppProxy proxy)
            {
                Type = type;
                Proxy = proxy;
            }
            #endregion // Constructor(s)
        }
        #endregion // Private Classes

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public ProxyHub(uint uid)
        {
            _log = LogFactory.ApplicationLog;
            AppProxies = new List<AppProxy>();
            UID = uid;

            // Create the service manager.
            ServiceManager = new ServiceManager();

            // Create the job queue and start the thread that will be processing the jobs.
            JobQueue = new BlockingCollection<ProxyHubJob>();

            // Start a new long running task for processing the jobs.
            Task.Factory.StartNew(() => ProcessJobs(), TaskCreationOptions.LongRunning);
        }
        #endregion // Constructor(s)

        #region State
        /// <summary>
        /// Run a new app proxy.
        /// </summary>
        /// <returns>Value indicating whether the proxy was successfully started up.</returns>
        public bool RunAppProxy(AppProxy proxy)
        {
            _log.MessageCtx(LogCtx, "Starting app proxy {0}.", proxy.ToString());
            if (_shuttingDown)
            {
                _log.MessageCtx(LogCtx, "Aborting startup as hub is already shutting down.");
                return false;
            }

            lock (AppProxies)
            {
                AppProxies.Add(proxy);
            }

            // Hook up the proxy stopped event.
            proxy.AppProxyStopped += AppProxyStopped;

            // Start a new thread for this proxy.
            Thread t = new Thread(new ParameterizedThreadStart(UpdateAppProxy));
            t.Name = String.Format("ProxyHub '{0}' update thread", proxy.ToString());
            t.IsBackground = true;
            t.Start(proxy);
            return true;
        }

        /// <summary>
        /// Shutdown a proxy.
        /// </summary>
        /// <param name="proxy"></param>
        public void ShutDownProxy( AppProxy proxy )
        {
            _log.MessageCtx(LogCtx, "Shutting down proxy {0}.", proxy.ToString());
            proxy.ShutDown();
        }

        /// <summary>
        /// Shutdown all proxies.
        /// </summary>
        public void ShutDownAllProxies()
        {
            _log.MessageCtx(LogCtx, "Queuing ShutDownAll job.");
            if (JobQueue != null && !_shuttingDown)
            {
                _shuttingDown = true;
                JobQueue.Add(new ProxyHubJob(JobType.ShutDownAll));
            }
        }

        /// <summary>
        /// Event handler for when a proxy has stopped.
        /// </summary>
        public void AppProxyStopped( object sender, EventArgs args )
        {
            AppProxy proxy = (AppProxy)sender;
            _log.MessageCtx(LogCtx, "Queuing HandleAppProxyStopped job for {0}.", proxy.ToString());
            JobQueue.Add(new ProxyHubJob(JobType.HandleAppProxyStopped, proxy));
        }

        /// <summary>
        /// Logs the state of all the app proxies.
        /// </summary>
        public void LogState()
        {
            _log.MessageCtx(LogCtx, "====Proxy states====");
            lock (AppProxies)
            {
                foreach (AppProxy proxy in AppProxies)
                {
                    proxy.LogState();
                }
            }
            _log.MessageCtx(LogCtx, "===================");
        }

        /// <summary>
        /// Returns an array of all the proxy descriptions.
        /// </summary>
        /// <returns></returns>
        public String[] GetProxyDescriptions()
        {
            lock (AppProxies)
            {
                String[] descriptions = new String[AppProxies.Count];
                for (int i = 0; i < AppProxies.Count; ++i)
                {
                    descriptions[i] = AppProxies[i].GetShortDescription();
                }
                return descriptions;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="uid"></param>
        /// <returns></returns>
        public AppProxy FindAppProxy(string name, int uid)
        {
            lock (AppProxies)
            {
                foreach (AppProxy proxy in AppProxies)
                {
                    if (proxy.Name == name && uid == proxy.UID)
                    {
                        return proxy;
                    }
                }
            }

            _log.MessageCtx(LogCtx, "No proxy with name {0} and uid {1} found.", name, uid);
            return null;
        }

        /// <summary>
        /// Find an app proxy based on it's name.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public List<AppProxy> FindAppProxy(string name)
        {
            List<AppProxy> results = new List<AppProxy>();
            lock (AppProxies)
            {
                foreach (AppProxy proxy in AppProxies)
                {
                    if (proxy.Name == name)
                    {
                        results.Add(proxy);
                    }
                }
            }

            return results;
        }

        /// <summary>
        /// Retrieves the state of a particular app proxy.
        /// </summary>
        public String GetProxyStatus(int proxyUID)
        {
            lock (AppProxies)
            {
                AppProxy proxy = AppProxies.FirstOrDefault(item => item.UID == proxyUID);
                if (proxy != null)
                {
                    return proxy.State.ToString();
                }
            }

            _log.MessageCtx(LogCtx, "Tried to find proxy with ID={0} but failed.", proxyUID);
            return "Error: No such proxy exists";
        }
        #endregion // State

        #region Thread Methods
        /// <summary>
        /// Method that consumes items from the job queue.
        /// </summary>
        private void ProcessJobs()
        {
            // Try and read jobs from the queue (blocks until one becomes available).
            foreach (ProxyHubJob job in JobQueue.GetConsumingEnumerable())
            {
                if (job.Type == JobType.ShutDownAll)
                {
                    _log.MessageCtx(LogCtx, "Processing ShutDownAll job.");
                    List<AppProxy> proxies;
                    lock (AppProxies)
                    {
                        proxies = new List<AppProxy>(AppProxies);
                    }

                    foreach (AppProxy proxy in proxies)
                    {
                        ShutDownProxy(proxy);
                    }
                }
                else if (job.Type == JobType.HandleAppProxyStopped)
                {
                    AppProxy proxy = job.Proxy;
                    _log.MessageCtx(LogCtx, "Processing HandleAppProxyStopped for {0}.", proxy.ToString());
                    proxy.AppProxyStopped -= AppProxyStopped;

                    lock (AppProxies)
                    {
                        AppProxies.Remove(proxy);

                        // Have all the app proxies shutdown?
                        if (AppProxies.Count == 0)
                        {
                            break;
                        }
                    }
                }
            }

            // We only break out of the above loop when all app proxies have shutdown.
            if (AllAppsShutDown != null)
            {
                AllAppsShutDown(this, null);
            }
        }

        /// <summary>
        /// Updates a particular app proxy.
        /// </summary>
        /// <param name="context"></param>
        private void UpdateAppProxy(object context)
        {
            AppProxy proxy = context as AppProxy;
            Debug.Assert(proxy != null, "Context is not a proxy.");

            // Wait until the proxy has been initialised.
            while (proxy.State == AppProxy.ProxyState.Initialising)
            {
                Thread.Sleep(50);
            }
            _log.MessageCtx(LogCtx, "Proxy done with initialization {0}.", proxy.ToString());

            // Update the proxy until it has stopped.
            while (proxy.State != AppProxy.ProxyState.Stopped)
            {
                proxy.Update();

                // Update the proxy more frequently when it is actually doing work.
                Thread.Sleep(proxy.IsWorking ? 1 : (int)proxy.IdleUpdateFrequency);
            }

            _log.MessageCtx(LogCtx, "Proxy done stopped {0}.", proxy.ToString());
        }
        #endregion // Thread Methods

        #region Disposable Implementation
        /// <summary>
        /// 
        /// </summary>
        public void Dispose()
        {
            GC.SuppressFinalize(this);
            if (JobQueue != null)
            {
                JobQueue.Dispose();
                JobQueue = null;
            }
        }
        #endregion // Disposable Implementation
    } // ProxyHub
}
