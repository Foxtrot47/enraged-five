﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using ragTray;
using RSG.Base.Extensions;
using RSG.Rag.Contracts.Data;
using RSG.Rag.Contracts.Services;

namespace RSG.Rag.Services
{
    /// <summary>
    /// Service that provides information about the connected games.
    /// </summary>
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, ConcurrencyMode = ConcurrencyMode.Multiple)]
    public class ConnectionService : IConnectionService, IConnectionSubscriptionService
    {
        #region Fields
        /// <summary>
        /// Reference to the <see cref="HubManager"/> object.
        /// </summary>
        private readonly HubManager _hubManager;

        /// <summary>
        /// The machines local IP address.
        /// </summary>
        private readonly IPAddress _localIP;

        /// <summary>
        /// List of subscribers that are currently listening for game connection events.
        /// </summary>
        private readonly IList<IConnectionEvents> _subscribers = new List<IConnectionEvents>();

        /// <summary>
        /// Lock object for the subscribers list.
        /// </summary>
        private readonly static object _subscribersLock = new object();
        #endregion

        #region Constructor(s)
        /// <summary>
        /// Initialises a new instance of the <see cref="ConnectionService"/> class.
        /// </summary>
        /// <param name="manager"></param>
        public ConnectionService(HubManager manager)
        {
            _hubManager = manager;
            _hubManager.HubInitialised += HubManager_HubInitialised;
            _hubManager.HubShutdown += HubManager_HubShutdown;
            _localIP = IPAddressExtensions.GetLocalIPAddress();
        }
        #endregion

        #region IConnectionService Implementation
        /// <summary>
        /// Retrieves the list of game connections the proxy currently has.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<GameConnection> GetGameConnections()
        {
            IList<GameConnection> connections = new List<GameConnection>();

            // This gets a copy of the active hub list.
            IEnumerable<ProxyHub> hubs = _hubManager.ActiveHubs;
            
            int index = 1;
            foreach (ProxyHub hub in hubs)
            {
                GameConnection connection = CreateConnectionForHub(hub);
                if (connection != null)
                {
                    connections.Add(connection);
                }
            }
            return connections;
        }

        /// <summary>
        /// Creates a GameConnection object for the specified hub.
        /// </summary>
        /// <param name="hub"></param>
        /// <returns></returns>
        private GameConnection CreateConnectionForHub(ProxyHub hub)
        {
            IGameProxyService gameService = hub.ServiceManager.GameService;
            IServicesProxy widgetProxyService = hub.ServiceManager.ServicesProxy;

            // If one of these are null, then that hub must be in shutdown mode already so ignore it.
            if (gameService == null || widgetProxyService == null)
            {
                return null;
            }

            GameConnection connection = new GameConnection();
            connection.GameIP = gameService.GetGameIPAddress();
            connection.RagProxyIP = _localIP;
            connection.Platform = gameService.GetPlatform();
            connection.ServicesPort = widgetProxyService.Port;
            connection.ExecutableName = gameService.GetExeName();
            connection.DefaultConnection = (hub == _hubManager.DefaultHub);
            connection.ConnectedAt = gameService.GetConnectedAt();
            return connection;
        }
        #endregion

        #region IConnectionSubscriptionService Implementation
        /// <summary>
        /// Allows clients to subscribe to receive events.
        /// </summary>
        public void Subscribe()
        {
            IConnectionEvents subscriber =
                OperationContext.Current.GetCallbackChannel<IConnectionEvents>();

            lock (_subscribersLock)
            {
                if (!_subscribers.Contains(subscriber))
                {
                    _subscribers.Add(subscriber);
                }
            }
        }

        /// <summary>
        /// Allows clients to unsubscribe from any further events.
        /// </summary>
        public void Unsubscribe()
        {
            IConnectionEvents subscriber =
                OperationContext.Current.GetCallbackChannel<IConnectionEvents>();

            lock (_subscribersLock)
            {
                _subscribers.Remove(subscriber);
            }
        }
        #endregion

        #region Event Handlers
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        private void HubManager_HubInitialised(object sender, ProxyHubEventArgs args)
        {
            GameConnection connection = CreateConnectionForHub(args.Hub);
            if (connection != null)
            {
                // Work with a copy of the subscribers list.
                IList<IConnectionEvents> subscribersCopy;
                lock (_subscribersLock)
                {
                    subscribersCopy = new List<IConnectionEvents>(_subscribers);
                }

                foreach (IConnectionEvents subscriber in subscribersCopy)
                {
                    try
                    {
                        subscriber.OnGameConnected(connection);
                    }
                    catch (Exception ex)
                    {
                        // Remove the subscriber if something went wrong with the notification.
                        lock (_subscribersLock)
                        {
                            _subscribers.Remove(subscriber);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        private void HubManager_HubShutdown(object sender, ProxyHubEventArgs args)
        {
            GameConnection connection = CreateConnectionForHub(args.Hub);
            if (connection != null)
            {
                // Work with a copy of the subscribers list.
                IList<IConnectionEvents> subscribersCopy;
                lock (_subscribersLock)
                {
                    subscribersCopy = new List<IConnectionEvents>(_subscribers);
                }

                foreach (IConnectionEvents subscriber in subscribersCopy)
                {
                    try
                    {
                        subscriber.OnGameDisconnected(connection);
                    }
                    catch (Exception ex)
                    {
                        // Remove the subscriber if something went wrong with the notification.
                        lock (_subscribersLock)
                        {
                            _subscribers.Remove(subscriber);
                        }
                    }
                }
            }
        }
        #endregion
    }
}
