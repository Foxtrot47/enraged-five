﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Text;

namespace RSG.Rag.Services
{
    /// <summary>
    /// Custom service host for services hosted inside of Rag.
    /// </summary>
    public class RagServiceHost : ServiceHost
    {
        #region Constructor(s)
        /// <summary>
        /// Initialises a new instance of the <see cref="RagServiceHost" /> class based on
        /// the supplied singleton instance and base addresses.
        /// </summary>
        /// <param name="singletonInstance"></param>
        /// <param name="baseAddresses"></param>
        public RagServiceHost(object singletonInstance, params Uri[] baseAddresses)
            : base(singletonInstance, baseAddresses)
        {
        }
        #endregion

        #region Overrides
        /// <summary>
        /// Loads the service description information from the configuration file and
        /// applies it to the runtime being constructed.
        /// </summary>
        protected override void ApplyConfiguration()
        {
            base.ApplyConfiguration();

            // Override the port that the service is listening on.
            if (SingletonInstance != null)
            {
                IRagService service = (IRagService)SingletonInstance;

                // Check if there is an http or tcp base address.
                foreach (ServiceEndpoint endpoint in this.Description.Endpoints)
                {
                    if (endpoint.Binding is NetTcpBinding ||
                        endpoint.Binding is NetNamedPipeBinding)
                    {
                        String modifiedAddress = endpoint.Address.Uri.ToString();
                        modifiedAddress = modifiedAddress.Replace("9000", service.Port.ToString());
                        endpoint.Address = new EndpointAddress(modifiedAddress);
                    }
                }
            }
        }
        #endregion
    }
}
