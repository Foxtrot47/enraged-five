﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using RSG.Rag.Contracts.Services;
using RSG.Rag.Services;

namespace ragTray.Proxy.Proxies
{
    /// <summary>
    /// 
    /// </summary>
    internal sealed class ServicesProxy : AppProxy, IServicesProxy
    {
        #region Fields
        /// <summary>
        /// Service host for hosting the console service.
        /// </summary>
        private readonly RagServiceHost _consoleServiceHost;

        /// <summary>
        /// Service host for hosting the widget service.
        /// </summary>
        private readonly RagServiceHost _widgetServiceHost;

        /// <summary>
        /// Service host for hosting the input service.
        /// </summary>
        private readonly RagServiceHost _inputServiceHost;

        /// <summary>
        /// Private field for the <see cref="Port"/> property.
        /// </summary>
        private readonly int _port;
        #endregion

        #region Constructor(s)
        /// <summary>
        /// Initialises a new instance of the <see cref="ServicesProxy"/> class.
        /// </summary>
        /// <param name="hub"></param>
        public ServicesProxy(ProxyHub hub)
            : base("Service Proxy", hub)
        {
            _port = 15060 + ((int)hub.UID % 100);

            // Subscribe to the streams we're interested in.
            SubscribeToStream("bankraw", hub.ServiceManager);

            //
            hub.ServiceManager.ServicesProxy = this;
            if (hub.ServiceManager.GameService.IsConnected())
            {
                // Start up the WCF services.
                Log("Starting services on port {0}", _port);
                ConsoleService consoleService = new ConsoleService(_port, hub.ServiceManager.GameService, m_HubStreams[StreamNameConstants.BankRaw], this, hub.ServiceManager.ConsoleService);
                _consoleServiceHost = new RagServiceHost(consoleService);
                _consoleServiceHost.Open();

                WidgetService widgetService = new WidgetService(_port, hub.ServiceManager.GameService, m_HubStreams[StreamNameConstants.BankRaw], this);
                _widgetServiceHost = new RagServiceHost(widgetService);
                _widgetServiceHost.Open();

                InputService inputService = new InputService(this._port, hub.ServiceManager.GameService);
                _inputServiceHost = new RagServiceHost(inputService);
                _inputServiceHost.Open();

                State = AppProxy.ProxyState.ReadyIdle;
            }
            else
            {
                Log("Not connected to game, stopping...");
                State = ProxyState.Stopping;
            }
        }
        #endregion

        #region Properties
        /// <summary>
        /// Port that the services for this game connection is listening on.
        /// </summary>
        public int Port
        {
            get { return _port; }
        }
        #endregion

        #region Methods
        /// <summary>
        /// 
        /// </summary>
        public override void Stop()
        {
            Log("Stopping WCF Services");
            // Close down the WCF services.
            if (_consoleServiceHost != null)
            {
                _consoleServiceHost.Close();
            }
            if (_widgetServiceHost != null)
            {
                _widgetServiceHost.Close();
            }
            if (this._inputServiceHost != null)
            {
                this._inputServiceHost.Close();
            }

            base.Stop();
        }

        /// <summary>
        /// 
        /// </summary>
        public override void Update()
        {
            if (!IsRunning)
            {
                Stop();
                return;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string GetShortDescription()
        {
            return "Services@" + GetShortStateDescription() + ":" + _port;
        }
        #endregion
    }
}
