﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Rag.Services.Exceptions
{
    /// <summary>
    /// Custom exception that is thrown if multiple widgets exist that share the same widget path.
    /// </summary>
    public class MultipleWidgetsFoundException : WidgetException
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="MultipleWidgetsFoundException"/> class
        /// based off a widget path.
        /// </summary>
        /// <param name="widgetPath"></param>
        public MultipleWidgetsFoundException(String widgetPath)
            : base(widgetPath)
        {
        }
        #endregion
    }
}
