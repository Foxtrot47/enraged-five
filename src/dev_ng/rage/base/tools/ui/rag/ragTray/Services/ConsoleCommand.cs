﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Rag.Services
{
    /// <summary>
    /// Command that you can invoke when using the rag console.
    /// </summary>
    public class ConsoleCommand
    {
        #region Fields
        /// <summary>
        /// Private field for the <see cref="Handler"/> property.
        /// </summary>
        private readonly Func<string, string[], string> _handler;

        /// <summary>
        /// Private field for the <see cref="HelpInfo"/> property.
        /// </summary>
        private readonly string _helpInfo;

        /// <summary>
        /// Private field for the <see cref="Key"/> property.
        /// </summary>
        private readonly string _key;
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ConsoleCommand"/> class using
        /// the specified key, handler and help information.
        /// </summary>
        /// <param name="key">
        /// The key used to identify the command.
        /// </param>
        /// <param name="handler">
        /// The handler to invoke when the command is encountered.
        /// </param>
        /// <param name="helpInfo">
        /// The help information about the command.
        /// </param>
        public ConsoleCommand(string key, Func<string, string[], string> handler, string helpInfo)
        {
            this._key = key;
            this._handler = handler;
            this._helpInfo = helpInfo;
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets the handler to invoke when the command is encountered.
        /// </summary>
        public Func<string, string[], string> Handler
        {
            get { return this._handler; }
        }

        /// <summary>
        /// Gets the help information about the command.
        /// </summary>
        public string HelpInfo
        {
            get { return this._helpInfo; }
        }

        /// <summary>
        /// Gets the key used to identify the command.
        /// </summary>
        public string Key
        {
            get { return this._key; }
        }
        #endregion
    }
}
