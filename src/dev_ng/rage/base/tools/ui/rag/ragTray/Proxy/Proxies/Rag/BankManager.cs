using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Text;
using System.Threading;
using System.Windows.Forms;

using ragCore;
using RSG.Base.Forms;

namespace ragTray
{
	/// <summary>
	/// Summary description for BankManager.
	/// </summary>
	public class BankManager : IBankManager
	{
		public BankManager()
		{
		}

		public void AddBank(WidgetBank bank)
		{
			if (!m_Banks.Contains(bank))
				m_Banks.Add(bank);
		}

		public void RemoveBank(WidgetBank bank)
		{
			m_Banks.Remove(bank);
		}

        public List<Widget> AllBanks
		{
			get { return m_Banks; }
		}

        private List<Widget> m_Banks = new List<Widget>();


        public List<Widget> FindWidgetsFromPath( string path )
        {
            throw new NotImplementedException();
        }

        public Widget FindFirstWidgetFromPath( string path )
        {
            throw new NotImplementedException();
        }

        public bool ProcessKeyUp( KeyEventArgs e )
        {
            throw new NotImplementedException();
        }

        public void AddProcessKeyUpEvent( object key, ProcessKeyUpDelegate theDelegate )
        {
            throw new NotImplementedException();
        }

        public void RemoveProcessKeyUpEvent( object key )
        {
            throw new NotImplementedException();
        }

        public void SendDelayedPing( EventHandler callback, BankPipe pipe )
        {
            throw new NotImplementedException();
        }

        public event InitFinishedHandler InitFinished;
    }
}
