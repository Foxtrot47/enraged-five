using Microsoft.VisualBasic.ApplicationServices;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Runtime.InteropServices;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Tcp;
using System.Text;
using System.Threading;
using System.Windows.Forms;

using ragTray.Properties;
using ragCore;
using RSG.Base.Forms;
using RSG.Base.IO;
using RSG.Base.Logging;
using RSG.Base.Configuration;
using RSG.Base;
using System.Reflection;
using RSG.Base.Configuration.Ports;
using RSG.Base.Configuration.Rag;

namespace ragTray
{
    public class MultipleInstanceProgram : WindowsFormsApplicationBase
    {
        public MultipleInstanceProgram(ILog log, IRagConfig ragConfig)
        {
            _log = log;
            this.MainForm = new RagTrayIcon(log, ragConfig);
            this.Startup += new StartupEventHandler( MultipleInstanceProgram_Startup );

            // Assign the handle:
            m_copyData.AssignHandle( this.MainForm.Handle );

            IPHostEntry host = Dns.GetHostEntry( Dns.GetHostName() );
            if ( host.AddressList.Length > 0 )
            {
                m_ipAddress = host.AddressList[0].ToString();
            }

            m_copyData.Channels.Add( this.ChannelName );

            // Hook up event notifications whenever a message is received:
            m_copyData.DataReceived += new vbAccelerator.Components.Win32.DataReceivedEventHandler( copyData_DataReceived );
        }

        #region Variables
        private readonly ILog _log;
        private Mutex m_mutex;
        private bool m_mutexIsNew;
        private string m_executableName = "rag.exe";
        private string m_channelBaseName = "RagTrayIcon";
        private string m_ipAddress = string.Empty;
        private vbAccelerator.Components.Win32.CopyData m_copyData = new vbAccelerator.Components.Win32.CopyData();
        #endregion

        #region Properties
        public string ChannelName
        {
            get
            {
                return BuildChannelName( m_ipAddress );
            }
        }
        #endregion

        #region Event Handlers
        private void MultipleInstanceProgram_Startup( object sender, StartupEventArgs e )
        {
            bool killAndExit = false;

            // find args:
            string processIdToKillStr = null;
            string processIdToWatchStr = null;
            string ipAddress = null;
            bool restarting = false;
            string versionFile = null;

            foreach ( string arg in e.CommandLine )
            {
                if ( arg == "-exit" )
                {
                    killAndExit = true;
                }
                else if ( arg == "-restarting" )
                {
                    restarting = true;
                }
                else if ( arg == "-version" )
                {
                    versionFile = string.Empty;
                }
                else
                {
                    ragCore.Args.CheckArg( arg, "-processToKill", ref processIdToKillStr );
                    ragCore.Args.CheckArg( arg, "-processToWatch", ref processIdToWatchStr );
                    ragCore.Args.CheckArg( arg, "-version", ref versionFile );
                    ragCore.Args.CheckArg( arg, "-addr", ref ipAddress );
                }
            }

            // save the version number to a file
            if ( String.IsNullOrEmpty( versionFile ) == false )
            {
                try
                {
                    StreamWriter writer = File.CreateText( versionFile );
                    writer.WriteLine( "Version = " + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString() );
                    writer.Close();
                }
                catch ( Exception ex )
                {
                    rageMessageBox.ShowError( ex.Message, "Error Creating Version File" );
                }
            }

            if ( String.IsNullOrEmpty( ipAddress ) == false )
            {
                bool found = false;
                IPHostEntry host = Dns.GetHostEntry( Dns.GetHostName() );
                foreach ( IPAddress addr in host.AddressList )
                {
                    if ( addr.ToString() == ipAddress )
                    {
                        found = true;
                        break;
                    }
                }

                if ( !found )
                {
                    rageMessageBox.ShowWarning( String.Format( "The IP Address {0} does not exist.  It will be ignored.", ipAddress ), "Rag.exe" );
                    ipAddress = null;
                }
                else
                {
                    m_copyData.Channels.Remove( this.ChannelName );
                    m_ipAddress = ipAddress;
                    m_copyData.Channels.Add( this.ChannelName );
                }
            }

            m_mutex = new Mutex( true, this.ChannelName, out m_mutexIsNew );

            // just kill and go away:
            if ( killAndExit )
            {
                if ( !m_mutexIsNew )
                {
                    m_copyData.Channels[this.ChannelName].Send( "exit" );
                }

                e.Cancel = true;
            }
            // start process killer if we're in that mode:
            else if ( processIdToKillStr != null && processIdToWatchStr != null )
            {
                if ( !m_mutexIsNew )
                {
                    m_copyData.Channels[this.ChannelName].Send( "wait:" + processIdToKillStr + ":" + processIdToWatchStr );
                }

                e.Cancel = true;
            }
            // output the version number and exit
            else if ( versionFile == string.Empty )
            {
                MessageBox.Show( System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString(), "Rag Version" );
                e.Cancel = true;
            }
            // start rag if the process killer isn't started:
            else
            {
                if ( !m_mutexIsNew )
                {
                    if ( restarting )
                    {
                        try
                        {
                            m_mutex.WaitOne();
                        }
                        catch ( AbandonedMutexException )
                        {
                            // allow this to pass, the wait still completed and the
                            // mutex was abandoned by a prev. run of the process
                        }
                    }
                    else
                    {
                        // app is already running...
                        if ( (ipAddress == null) || (ipAddress == string.Empty) )
                        {
                            rageMessageBox.ShowExclamation( "Only one instance of rag.exe (AKA The Proxy) is allowed at one time.", m_executableName );
                        }
                        else
                        {
                            rageMessageBox.ShowExclamation( "Only one instance of rag.exe (AKA The Proxy) is allowed per IP Address.", m_executableName );
                        }

                        e.Cancel = true;
                    }
                }

                if ( !e.Cancel )
                {
                    RagTrayIcon tray = this.MainForm as RagTrayIcon;
                    string[] args = new string[e.CommandLine.Count];
                    e.CommandLine.CopyTo( args, 0 );

                    if ( !Debugger.IsAttached )
                    {
                        try
                        {
                            if ( !tray.Init( ipAddress, args ) )
                            {
                                e.Cancel = true;
                            }
                        }
                        catch ( Exception ex )
                        {
                            tray.RestartAndExit( ex, true );
                        }
                    }
                    else
                    {
                        if ( !tray.Init( ipAddress, args ) )
                        {
                            e.Cancel = true;
                        }
                    }
                }
            }
        }

        private void copyData_DataReceived( object sender, vbAccelerator.Components.Win32.DataReceivedEventArgs e )
        {
            _log.Message( "copyData_DataReceived" );
            if ( e.ChannelName.Equals( this.ChannelName ) )
            {
                string cmd = e.Data as string;
                if ( cmd == "exit" )
                {
                    RagTrayIcon tray = this.MainForm as RagTrayIcon;
                    tray.Shutdown();
                }
                else if ( cmd.StartsWith( "wait" ) )
                {
                    string[] cmdSplit = cmd.Split( new char[] { ':' } );
                    if ( cmdSplit.Length == 3 )
                    {
                        WaitForProcessToDie( cmdSplit[1], cmdSplit[2] );
                    }
                }
            }
        }
        #endregion

        #region Private Functions
        private string BuildChannelName( string ipAddress )
        {
            return m_channelBaseName + ipAddress;
        }

        private void WaitForProcessToDie( string processToKillStr, string processToWatchStr )
        {
            // process to kill:
            Process processToKill = null;
            try
            {
                processToKill = Process.GetProcessById( Convert.ToInt32( processToKillStr ) );
            }
            catch
            {
                rageMessageBox.ShowError( String.Format( "Couldn't find process to kill with process Id {0}", processToKillStr ), m_executableName );
            }

            // process to watch:
            Process processToWatch = null;
            try
            {
                processToWatch = Process.GetProcessById( Convert.ToInt32( processToWatchStr ) );
            }
            catch
            {
                rageMessageBox.ShowError( String.Format( "Couldn't find process to watch with process Id {0}", processToWatchStr ), m_executableName );
            }

            // wait until the process to watch is no longer running.  Once
            // it's no longer running, we kill the process:
            while ( true )
            {
                Thread.Sleep( 500 );
                try
                {
                    if ( processToKill.HasExited )
                    {
                        break;
                    }
                    else if ( processToWatch.HasExited )
                    {
                        processToKill.Kill();
                        break;
                    }
                }
                catch ( Exception e )
                {
                    _log.ToolException(e, "Unable to kill process.");
                }
            }
        }
        #endregion
    }

    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main( string[] args )
        {
            LogFactory.Initialize(Path.Combine(LogFactory.GetLogDirectory(), "RagProxy"), false);
            AppDomain.CurrentDomain.UnhandledException += new System.UnhandledExceptionEventHandler( CurrentDomain_UnhandledException );

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault( false );
            Application.ThreadException += new ThreadExceptionEventHandler( Application_ThreadException );

            if (CommandLineUtils.Verbose())
            {
                LogFactory.SetLogLevel(LogLevel.Debug);
            }

            IRagConfig ragConfig = null;

            if (CommandLineUtils.UseToolsConfig())
            {
                try
                {
                    IConfig config = ConfigFactory.CreateConfig();
                    rageEmailUtilities.UseSmtpAuth = false;
                    rageEmailUtilities.SmtpServer = config.Studios.ThisStudio.ExchangeServer;

                    IPortConfig portConfig = ConfigFactory.CreatePortConfig(config.Project);

                    int hubConnectionServicesPort;
                    if (portConfig.Ports.TryGetValue("RAGProxyHubConnectionService", out hubConnectionServicesPort))
                    {
                        GameConnection.ConnectionServicePort = hubConnectionServicesPort;
                        HubConnectionService.ConnectionServicePort = hubConnectionServicesPort;
                    }

                    ragConfig = ConfigFactory.CreateRagConfig(config.Project);
                }
                catch (ConfigurationVersionException ex)
                {
                    LogFactory.ApplicationLog.ToolException(ex, "Configuration version error: run tools installer.  Installed version: {0}, expected version: {1}.",
                        ex.ActualVersion, ex.ExpectedVersion);
                    rageMessageBox.ShowError(String.Format("An important update has been made to the tool chain.  Install version: {0}, expected version: {1}.{2}{2}Sync latest or labelled tools, run {3} and then restart the application.",
                        ex.ActualVersion, ex.ExpectedVersion, Environment.NewLine,
                        Path.Combine(Environment.GetEnvironmentVariable("RS_TOOLSROOT"), "install.bat")),
                        "Config version error.");
                    Environment.Exit(1);
                }
                catch (ConfigurationException ex)
                {
                    LogFactory.ApplicationLog.ToolException(ex, "Configuration parse error.");
                    rageMessageBox.ShowError(String.Format("There was an error initialising configuration data.{0}{0}Sync latest or labelled tools and restart the application.",
                        Environment.NewLine),
                        "Config parse error.");
                    Environment.Exit(1);
                }
            }
            else
            {
                rageEmailUtilities.SmtpServer = null;
            }

            Application.DoEvents();

            String exeName = Assembly.GetExecutingAssembly().GetName().Name;
            try
            {
                bool newInstanceCreated = ApplicationInstanceManager.CreateSingleInstance(exeName, InstanceStartupCallback, false);

                if (!newInstanceCreated)
                {
                    rageMessageBox.ShowInformation("The RAG proxy is already running.  If the tray icon is missing please kill rag.exe via the task manager before attempting to relaunch the application.", "RAG Proxy Already Running.");
                    Environment.Exit(1);
                }
            }
            catch (Exception)
            {
                rageMessageBox.ShowInformation("An error occurred while trying to determine if the RAG proxy is already running.  If the tray icon is missing please kill rag.exe via the task manager before attempting to relaunch the application.  Alternatively this can sometimes happen if the RAG proxy is started too quickly after shutting down a previous instance.", "RAG Proxy Already Running.");
                Environment.Exit(1);
            }

            UpgradeSettings(LogFactory.ApplicationLog);

            MultipleInstanceProgram program = null;
            try
            {
                program = new MultipleInstanceProgram(LogFactory.ApplicationLog, ragConfig);
            }
            catch ( System.Exception e )
            {
                Application_ThreadException( program, new ThreadExceptionEventArgs( e ) );
            }

            if (program != null)
            {
                try
                {
                    program.Run(args);
                }
                catch (System.Exception e)
                {
                    Application_ThreadException(program, new ThreadExceptionEventArgs(e));
                }
                finally
                {
                    LogFactory.ApplicationShutdown();
                }
            }
        }

        private static void InstanceStartupCallback(Object sender, InstanceCallbackEventArgs e)
        {
        }

        /// <summary>
        /// Upgrade our settings from the previous version.
        /// </summary>
        private static void UpgradeSettings(ILog log)
        {
            try
            {
                try
                {
                    if (Properties.Settings.Default.UpgradeNeeded == String.Empty)
                    {
                        Properties.Settings.Default.Upgrade();
                        Properties.Settings.Default.UpgradeNeeded = "no";
                        Properties.Settings.Default.Save();
                    }
                }
                catch (System.Configuration.ConfigurationException ex)
                {
                    rageMessageBox.ShowExclamation("Exception occurred while upgrading you last session's settings.\n\nThe application will continue but not all of your last session's settings can be restored.",
                        String.Format("Unable to Upgrade Settings File to Version {0}!", System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString()));
                    log.ToolException(ex, "Error while updating the config.");
                    Properties.Settings.Default.Reset();
                }
            }
            catch (Exception ex)
            {
                log.ToolException(ex, "Uncategorised exception during settings upgrade or reset; not resetting to default.");
            }
        }


        static void CurrentDomain_UnhandledException( object sender, System.UnhandledExceptionEventArgs e )
        {
            HandleException( (Exception)e.ExceptionObject );
        }

        static void Application_ThreadException( object sender, ThreadExceptionEventArgs e )
        {
            HandleException( e.Exception );
        }


        static void HandleException( Exception e )
        {
            try
            {
                LogFactory.ApplicationLog.ToolException(e, "HandleException");
                LogFactory.FlushApplicationLog();
                List<string> attachments = new List<string>();

                if ( RagTrayIcon.Instance != null
                    && File.Exists( LogFactory.ApplicationLogFilename ) )
                {
                    attachments.Add(LogFactory.ApplicationLogFilename);
                }

                string userConfigFilename = rageFileUtilities.GetRageApplicationUserConfigFilename( "rag.exe" );
                if ( !String.IsNullOrEmpty( userConfigFilename ) && File.Exists( userConfigFilename ) )
                {
                    attachments.Add( userConfigFilename );
                }

                UnhandledExceptionDialog dialog = new UnhandledExceptionDialog( "Rag", attachments,
                    Settings.Default.ExceptionToEmailAdress, e );

                dialog.ShowDialog();

                if (RagTrayIcon.Instance != null)
                {
                    RagTrayIcon.Instance.Shutdown();
                }

                Settings.Default.Save();
            }

            finally
            {
                try
                {
                    Application.Exit();
                }
                catch ( Exception ex )
                {
                    LogFactory.ApplicationLog.ToolException(ex, "Application.Exit() failed.");
                    Application.Exit();
                }
            }
        }
    }
}
