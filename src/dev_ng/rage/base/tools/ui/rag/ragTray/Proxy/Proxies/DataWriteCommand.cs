﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ragTray.Proxy.Proxies
{
    public class DataWriteCommand
    {
        public static char[] bkDataIdentifier = { 'b', 'k', 'd', 'w' };
        public const int HeaderOffset = 4;
        private static readonly ASCIIEncoding c_encoder = new ASCIIEncoding();

        public static bool CheckHeader(char[] header)
        {
            return (header[0] == bkDataIdentifier[0] && header[1] == bkDataIdentifier[1]
                        && header[2] == bkDataIdentifier[2] && header[3] == bkDataIdentifier[3]);
        }

        public DataWriteCommand(byte[] buffer, int bufferOffset)
        {
            int offset = HeaderOffset + bufferOffset;
            DataSize = BitConverter.ToInt32(buffer, offset);
            offset += sizeof(int);

            WidgetId = BitConverter.ToInt32(buffer, offset);
            offset += sizeof(int);
            HeaderDataSize = offset - bufferOffset;

            Data = new byte[DataSize];
            CurrentDataSize = 0;

            // Check whether the buffer contains the remaining amount of data we need.
            int remainingBytes = buffer.Length - offset;
            if (DataSize < remainingBytes)
            {
                // Buffer contains more data than we need. Only read what we require.
                AddData(buffer, offset, DataSize);
            }
            else
            {
                // Buffer doesn't contain enough data.  Read it all mwahahaha >:D
                AddData(buffer, offset, remainingBytes);
            }
        }

        public bool IsComplete()
        {
            return Data.Length == DataSize;
        }

        public int AddData(byte[] buffer, int offset)
        {
            int remainingBytes = buffer.Length - offset;
            int bytesToAdd;

            // Determine how much we should read from the buffer.
            if (DataSize < CurrentDataSize + remainingBytes)
            {
                bytesToAdd = DataSize - CurrentDataSize;
            }
            else
            {
                bytesToAdd = remainingBytes;
            }

            AddData(buffer, offset, bytesToAdd);
            return bytesToAdd;
        }

        public void AddData(byte[] buffer, int sourceOffset, int length)
        {
            Array.Copy(buffer, sourceOffset, Data, 0, length);
            CurrentDataSize += length;
        }

        public int WidgetId;
        public int DataSize;
        public int CurrentDataSize;
        public int HeaderDataSize;
        public byte[] Data;
    }
}
