using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Timers;

using ragCore;
using RSG.Base.Forms;
using RSG.Base.IO;
using RSG.Base.Logging;
using ragWidgets;
using System.Net.Sockets;
using ragTray.Properties;
using System.Reflection;
using RSG.Base.Configuration.Rag;


namespace ragTray
{
    /*
     * 
     * 
     * 
     * 
     * 
     * Note: See this: https://devstar.rockstargames.com/wiki/index.php/Proxy_Dev_Docs
     * 
     * 
     * 
     * 
     * */

    public partial class RagTrayIcon : Form
    {
        public static int sm_NumOpenFormInstances = 0;

        private readonly ILog _log;

        private String LogCtx
        {
            get { return "RagTrayIcon"; }
        }

        public RagTrayIcon(ILog log, IRagConfig ragConfig)
        {
            _log = log;
            InitializeComponent();

            Thread.CurrentThread.Name = "MainThread";

            int handle = Handle.ToInt32(); // ensures the window handle is created
            Debug.Assert( handle > 0, "Handle not created" );

            // find the version number of the main rag assembly
            string versionString = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
            _log.MessageCtx(LogCtx, "Rag Proxy Version: {0}.", versionString);

            BankRemotePacketProcessor.DisplayMessage += new BankRemotePacketProcessor.DisplayMessageEventHandler( BankRemotePacket_OnDisplayMessage );
            NamedPipeSocket.DisplayMessage += new BankRemotePacketProcessor.DisplayMessageEventHandler( BankRemotePacket_OnDisplayMessage );

            GameProxy.GameStoppedResponding += new EventHandler( GameProxy_GameStoppedResponding );
            GameProxy.GameResumedResponding += new EventHandler( GameProxy_GameResumedResponding );

            RagProxy.StartedForwardingDataToRag += new EventHandler(RagProxy_StartedForwardingDataToRag);
            RagProxy.EndedForwardingDataToRag += new EventHandler(RagProxy_EndedForwardingDataToRag);
            RagProxy.ForwardingDataToRagProgress += new ProgressEventHandler(RagProxy_ForwardingDataToRagProgress);

            m_LaunchRagAutomatically = Settings.Default.AutoLaunchRag;
            AutoLaunchRagtoolStripMenuItem.Checked = m_LaunchRagAutomatically;

            m_LaunchRagInterfaceAutomatically = Settings.Default.AutoLaunchRagInterface;
            AutoLaunchRagInterfaceToolStripMenuItem.Checked = this.m_LaunchRagInterfaceAutomatically;

            // Ensure that only one will win  -just in case.
            if (this.m_LaunchRagAutomatically && this.m_LaunchRagInterfaceAutomatically)
            {
                this.m_LaunchRagAutomatically = true;
                this.AutoLaunchRagtoolStripMenuItem.Checked = this.m_LaunchRagAutomatically;
                Settings.Default.AutoLaunchRag = true;

                this.m_LaunchRagInterfaceAutomatically = false;
                this.AutoLaunchRagInterfaceToolStripMenuItem.Checked = this.m_LaunchRagInterfaceAutomatically;
                Settings.Default.AutoLaunchRagInterface = false;

                Settings.Default.Save();
            }

            m_Notifier = new TaskBarBalloonNotifier(notifyIcon, _log);
            m_Notifier.Enabled = Settings.Default.ShowBalloons;


            m_HubManager = new HubManager(m_Notifier, Address, _log);
            m_HubManager.AllHubsShutDown += m_HubManager_AllHubsShutDown;
            m_HubManager.HubStarted += m_HubManager_HubStarted;
            m_HubManager.PendingHub.ServiceManager.GameService.GameConnected += new GameEventHandler(GameService_GameConnected);

            m_gameListener = new GameListener(m_HubManager, _log, ragConfig);
            m_connectionService = new HubConnectionService(m_HubManager, _log);

            m_HoverForm = new TrayIconHoverForm(m_HubManager);
            m_HoverForm.Show();
            m_HoverForm.Visible = false;

            AutoVerifyGameLinkMenuItem.Checked = Settings.Default.AutoVerifyGameLink;
            cacheNetworkPacketsToolStripMenuItem.Checked = Settings.Default.CacheNetworkPackets;

            _log.MessageCtx(LogCtx, "Construction complete.");
        }

        void RagProxy_StartedForwardingDataToRag(object sender, EventArgs e)
        {
            lock (m_ForwardProgressForms)
            {
                RagProxy proxy = sender as RagProxy;
                if (InvokeRequired)
                {
                    Invoke((MethodInvoker)delegate
                    {
                        ForwardDataToRagForm form = new ForwardDataToRagForm(proxy);
                        form.FormClosed += new FormClosedEventHandler(forwardDataToRagForm_FormClosed);
                        form.Show();
                        m_ForwardProgressForms[proxy] = form;
                    });
                }
                else
                {
                    ForwardDataToRagForm form = new ForwardDataToRagForm(proxy);
                    form.FormClosed += new FormClosedEventHandler(closeConnectionToGameForm_FormClosed);
                    form.Show();
                    m_ForwardProgressForms[proxy] = form;
                }                
            }
        }

        void RagProxy_EndedForwardingDataToRag(object sender, EventArgs e)
        {
            lock (m_ForwardProgressForms)
            {
                RagProxy proxy = sender as RagProxy;
                if (InvokeRequired)
                {
                    Invoke((MethodInvoker)delegate
                    {
                        if (m_ForwardProgressForms.ContainsKey(proxy))
                        {
                            var form = m_ForwardProgressForms[proxy];
                            form.Complete();
                            m_ForwardProgressForms.Remove(proxy);
                        }
                    });
                }
                else
                {
                    var form = m_ForwardProgressForms[proxy];
                    form.Complete();
                    m_ForwardProgressForms.Remove(proxy);
                }
            }
        }

        void RagProxy_ForwardingDataToRagProgress(object sender, ProgressEventArgs e)
        {
            lock (m_ForwardProgressForms)
            {
                RagProxy proxy = sender as RagProxy;
                if (InvokeRequired)
                {
                    Invoke((MethodInvoker)delegate
                    {
                        if (m_ForwardProgressForms.ContainsKey(proxy))
                        {
                            var form = m_ForwardProgressForms[proxy];
                            form.ReportProgress(e.Current, e.UpperBound);
                        }
                    });
                }
                else
                {
                    var form = m_ForwardProgressForms[proxy];
                    form.ReportProgress(e.Current, e.UpperBound);
                }
            }
        }

        void forwardDataToRagForm_FormClosed(Object sender, FormClosedEventArgs e)
        {
            lock (m_ForwardProgressForms)
            {
                foreach (var kvp in m_ForwardProgressForms)
                {
                    if (kvp.Value == sender)
                    {
                        m_ForwardProgressForms.Remove(kvp.Key);
                        break;
                    }
                }
            }
        }

        void GameProxy_GameStoppedResponding( object sender, EventArgs e )
        {
            lock ( m_CloseConnectionForms )
            {
                GameProxy proxy = sender as GameProxy;
                if ( InvokeRequired )
                {
                    Invoke( (MethodInvoker)delegate
                    {
                        var form = new CloseConnectionToGameForm( sender as GameProxy );
                        form.FormClosed += new FormClosedEventHandler( closeConnectionToGameForm_FormClosed );
                        form.Show();
                        m_CloseConnectionForms[proxy] = form;
                    } );
                }
                else
                {
                    var form = new CloseConnectionToGameForm( sender as GameProxy );
                    form.FormClosed += new FormClosedEventHandler( closeConnectionToGameForm_FormClosed );
                    form.Show();
                    m_CloseConnectionForms[proxy] = form;
                }                
            }
        }

        void GameProxy_GameResumedResponding( object sender, EventArgs e )
        {
            lock ( m_CloseConnectionForms )
            {
                GameProxy proxy = sender as GameProxy;
                if ( InvokeRequired )
                {
                    Invoke( (MethodInvoker)delegate
                    {
                        if (m_CloseConnectionForms.ContainsKey(proxy))
                        {
                            var form = m_CloseConnectionForms[proxy];
                            form.ConnectionRestored();
                            m_CloseConnectionForms.Remove( proxy );
                        }
                    } );
                }
                else
                {
                    var form = m_CloseConnectionForms[proxy];
                    form.ConnectionRestored();
                    m_CloseConnectionForms.Remove( proxy );
                }
            }
        }

        void closeConnectionToGameForm_FormClosed( object sender, FormClosedEventArgs e )
        {
            lock ( m_CloseConnectionForms )
            {
                foreach ( var kvp in m_CloseConnectionForms )
                {
                    if ( kvp.Value == sender )
                    {
                        m_CloseConnectionForms.Remove( kvp.Key );
                        break;
                    }
                }                
            }
        }

        void m_HubManager_HubStarted(object sender, ProxyHubEventArgs e)
        {
            Debug.Assert( e.Hub.ServiceManager.GameService != null );
            e.Hub.ServiceManager.GameService.GameConnected +=new GameEventHandler(GameService_GameConnected);
        }

        void GameService_GameConnected( object sender, GameConnectedEventArgs args )
        {
            _log.MessageCtx(LogCtx, "gameclient_GameConnected()");
            if ( m_LaunchRagAutomatically )
            {
                GameProxy proxy = sender as GameProxy;
                if ( proxy.Hub.FindAppProxy( "Rag Proxy" ).Count == 0 )
                {
                    LaunchRag(proxy.Hub.ServiceManager.ConnectionListenerService.ConnectionPort);
                }
            }

            if (this.m_LaunchRagInterfaceAutomatically)
            {
                GameProxy proxy = sender as GameProxy;
                if (proxy.Hub.FindAppProxy("Rag Proxy").Count == 0)
                {
                    LaunchRagInterface(proxy.Hub.ServiceManager.ConnectionListenerService.ConnectionPort);
                }
            }
        }


        void BankRemotePacket_OnDisplayMessage( object sender, DisplayMessageEventArgs e )
        {
            if (InvokeRequired)
            {
                var eh = new BankRemotePacketProcessor.DisplayMessageEventHandler( BankRemotePacket_OnDisplayMessage );
                lock ( this )
                {
                    this.Invoke( eh, new object[] { sender, e } );
                }
            }
            else
            {
                DialogResult result = rageMessageBox.ShowExclamation( this, e.Message, e.Caption, (e.OkOnly ? MessageBoxButtons.OK : MessageBoxButtons.YesNo ));
                e.ResultYesOrOK = (result == DialogResult.Yes || result == DialogResult.OK);
            }
        }
        #region Variables
        private static RagTrayIcon sm_Instance;

        private string m_ipAddress = string.Empty;
        private string m_ExeArgs;
        private bool m_SuppressExit = false;


        GameListener m_gameListener;
        HubConnectionService m_connectionService;
        HubManager m_HubManager;
        bool m_LaunchRagAutomatically;
        private bool m_LaunchRagInterfaceAutomatically;

        TaskBarBalloonNotifier m_Notifier;
        TrayIconHoverForm m_HoverForm;

        Dictionary<GameProxy, CloseConnectionToGameForm> m_CloseConnectionForms = new Dictionary<GameProxy, CloseConnectionToGameForm>();
        Dictionary<RagProxy, ForwardDataToRagForm> m_ForwardProgressForms = new Dictionary<RagProxy, ForwardDataToRagForm>();

        #endregion

        #region Properties
        public static RagTrayIcon Instance
        {
            get
            {
                return sm_Instance;
            }
        }


        public string IpAddress
        {
            get
            {
                return m_ipAddress;
            }
            set
            {
                m_ipAddress = value;
            }
        }

        public IPAddress Address
        {
            get
            {
                if ( (m_ipAddress != null) && (m_ipAddress != string.Empty) )
                {
                    string[] split = m_ipAddress.Split( new char[] { '.' } );
                    byte[] byteArray = new byte[split.Length];
                    for ( int i = 0; i < split.Length; ++i )
                    {
                        byteArray[i] = byte.Parse( split[i] );
                    }

                    return new IPAddress( byteArray );
                }

                return IPAddress.Any;
            }
        }
        #endregion

        #region Overrides
        // the code below waits for a quit message for an instance of rag executed with "-exit".
        // We have to do this since a close message just closes the current application with RAG 
        // instead of closing RAG itself.
        protected override void WndProc( ref Message message )
        {
            base.WndProc( ref message );
            ////filter the RF_TESTMESSAGE
            ////if ( message.Msg == MainWindow.MSG_REALLY_QUIT )
            //{
            //    exitToolStripMenuItem_Click( this, EventArgs.Empty );
            //}
            ////be sure to pass along all messages to the base also
            //base.WndProc( ref message );
        }
        #endregion

        #region Public Functions
        public bool Init( string ipAddress, string[] args )
        {
            this.notifyIcon.Visible = true;

            m_ipAddress = ipAddress;

            // find the version number of the main rag assembly
            string versionString = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();

            // get plugin path:
            string pluginPath = null;
            string admin = Boolean.FalseString;
            string noquitmsg = Boolean.FalseString;
            string pipeTimeout = String.Empty;
            bool hasNoQuitMsg = false;

            m_ExeArgs = null;
            foreach ( string arg in args )
            {
                if ( arg.ToLower() == "-admin" )
                {
                    admin = Boolean.TrueString;
                }
                else if ( arg.ToLower() == "-noquitmsg" )
                {
                    noquitmsg = Boolean.TrueString;
                    hasNoQuitMsg = true;
                }
                else
                {
                    ragCore.Args.CheckArg( arg, "-pluginPath", ref pluginPath );
                    ragCore.Args.CheckArg( arg, "-admin", ref admin );
                    ragCore.Args.CheckArg( arg, "-pipeTimeout", ref pipeTimeout );

                    if ( !hasNoQuitMsg )
                    {
                        hasNoQuitMsg = ragCore.Args.CheckArg( arg, "-noquitmsg", ref noquitmsg );
                    }
                }

                if ( arg.ToLower() != "-restarting" )
                {
                    m_ExeArgs += " " + arg;
                }

            }

            int timeoutValue;
            if ( Int32.TryParse( pipeTimeout, out timeoutValue ) )
            {
                NamedPipeSocket.PipeTimeoutSeconds = timeoutValue;
            }

            if ( hasNoQuitMsg )
            {
                m_SuppressExit = true;
            }

            // start our application:            
            sm_Instance = this;
            _log.MessageCtx(LogCtx, "Instance set.");

            return true;
        }

        public void Restart()
        {
            _log.MessageCtx(LogCtx, "Restart");

            if ( !String.IsNullOrEmpty( m_ExeArgs ) && m_ExeArgs.ToLower().Contains( "-restarting " ) )
            {
                Process.Start( Application.ExecutablePath, m_ExeArgs );
            }
            else
            {
                Process.Start( Application.ExecutablePath, "-restarting " + m_ExeArgs );
            }
        }

        public void RestartAndExit( Exception e, bool showMessage )
        {
            _log.MessageCtx(LogCtx, "RestartAndExit()");
            if ( showMessage )
            {
                List<string> attachments = new List<string>();

                if ( RagTrayIcon.Instance != null
                    && File.Exists(LogFactory.ApplicationLogFilename))
                {
                    attachments.Add(LogFactory.ApplicationLogFilename);
                }

                string userConfigFilename = rageFileUtilities.GetRageApplicationUserConfigFilename( "rag.exe" );
                if ( !String.IsNullOrEmpty( userConfigFilename ) && File.Exists( userConfigFilename ) )
                {
                    attachments.Add( userConfigFilename );
                }

                UnhandledExceptionDialog dialog = new UnhandledExceptionDialog( "Rag", attachments,
                    Settings.Default.ExceptionToEmailAdress, e );

                dialog.ShowDialog();
            }

            Shutdown();
            Restart();
        }

        public void Shutdown()
        {
            _log.MessageCtx(LogCtx, "Shutdown()");

            m_gameListener.Shutdown();
            m_connectionService.Shutdown();
            m_HubManager.ShutDown();
        }

        /// <summary>
        /// Attached to the hub manager's all hubs shutdown event, this gets called once
        /// all proxy hubs have been shutdown.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        private void m_HubManager_AllHubsShutDown( object sender, EventArgs args )
        {
            _log.MessageCtx(LogCtx, "m_HubManager_AllHubsShutDown()");
            if ( this.InvokeRequired )
            {
                EventHandler eh = new EventHandler( m_HubManager_AllHubsShutDown );
                lock ( this )
                {
                    this.BeginInvoke( eh, new object[] { sender, args } );
                }
            }
            else
            {
                lock ( this )
                {
                    this.Close();
                    Application.Exit();
                }
            }
        }
        #endregion

        #region Event Handlers
        private void RagTrayIcon_FormClosing( object sender, FormClosingEventArgs e )
        {
            _log.MessageCtx(LogCtx, "Rag Closing");

            this.notifyIcon.Visible = false;
        }

        private void RagTrayIcon_FormClosed( object sender, FormClosedEventArgs e )
        {
            _log.MessageCtx(LogCtx, "Rag Closed");
        }

        private void RagTrayIcon_VisibleChanged( object sender, EventArgs e )
        {
            // make sure the form always stays hidden
            this.Visible = false;
        }

        private void viewLogToolStripMenuItem_Click( object sender, EventArgs e )
        {
            _log.MessageCtx(LogCtx, "viewLogToolStripMenuItem_Click()");
            m_HubManager.LogState();

            LogFactory.FlushApplicationLog();
            if (File.Exists(LogFactory.ApplicationLogFilename))
            {
                Process.Start(LogFactory.ApplicationLogFilename);
            }
        }

        private void openLogFolderToolStripMenuItem_Click( object sender, EventArgs e )
        {

            string logFolder = Path.Combine( Environment.GetEnvironmentVariable( "TEMP" ), "rag" );
            if ( !Directory.Exists( logFolder ) )
            {
                MessageBox.Show( "Couldn't find log folder at \n" + logFolder );
                return;
            }


            Process.Start( logFolder );
        }


        private void launchRagToolStripMenuItem_Click( object sender, EventArgs e )
        {
            LaunchRag(-1);
        }

        private void launchRagInterfaceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LaunchRagInterface(-1);
        }

        private void LaunchRag(int connectionPort)
        {
            _log.MessageCtx(LogCtx, "Launching Rag UI");

            string currentPath = Path.GetDirectoryName( Application.ExecutablePath );
            string ragPath = Path.Combine( currentPath, "ragApp.exe" );

            if (File.Exists(ragPath) == false)
            {
                ragPath = "%RS_TOOLSROOT%\\bin\\rag\\ragApp.exe";
                ragPath = Environment.ExpandEnvironmentVariables(ragPath);

                if (File.Exists(ragPath) == false)
                {
                    MessageBox.Show("Unable to find ragApp.exe executable.  The user interface will not be launched.");
                    return;
                }
            }

            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.FileName = ragPath;
            startInfo.WorkingDirectory = currentPath;
            startInfo.UseShellExecute = false;

            if (m_SuppressExit)
            {
                startInfo.Arguments += " -noquitmsg";
            }
            if (connectionPort != -1)
            {
                startInfo.Arguments += " -proxyport=" + connectionPort;
            }
            if (!CommandLineUtils.UseToolsConfig())
            {
                startInfo.Arguments += " -noconfig";
            }

            _log.MessageCtx(LogCtx, "Starting {0} {1}", ragPath, startInfo.Arguments);

            Process ragProcess = new Process();
            ragProcess.StartInfo = startInfo;
            ragProcess.Start();
        }

        private void LaunchRagInterface(int connectionPort)
        {
            _log.MessageCtx(LogCtx, "Launching Rag Interface");

            string currentPath = Path.GetDirectoryName(Application.ExecutablePath);
            string ragPath = Path.Combine(currentPath, @"..\RagInterface\RagInterface.exe");

            if (!File.Exists(ragPath))
            {
                ragPath = Environment.ExpandEnvironmentVariables(@"%RS_TOOLSROOT%\bin\RagInterface\RagInterface.exe");

                if (!File.Exists(ragPath))
                {
                    MessageBox.Show("Unable to find ragInterface.exe executable.  The user interface will not be launched.");
                    return;
                }
            }

            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.FileName = ragPath;
            startInfo.WorkingDirectory = currentPath;
            startInfo.UseShellExecute = false;

            if (connectionPort != -1)
            {
                startInfo.Arguments += "-proxyport " + connectionPort;
            }
            // -noconfig prevents attempts to load an unsupported configuration - necessary as the RagInterface version used is from later projects 
            startInfo.Arguments += " -noconfig";

            _log.MessageCtx(LogCtx, "Starting {0} {1}", ragPath, startInfo.Arguments);

            Process ragProcess = new Process();
            ragProcess.StartInfo = startInfo;
            ragProcess.Start();
        }

        private void launchGameToolStripMenuItem_Click( object sender, EventArgs e )
        {
            string exePath = (string)((ToolStripMenuItem)sender).Tag;

            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.FileName = exePath;
            startInfo.WorkingDirectory = Path.GetDirectoryName( exePath );
            startInfo.UseShellExecute = false;
            Process gameProcess = new Process();
            gameProcess.StartInfo = startInfo;
            gameProcess.Start();
        }

        private void exitToolStripMenuItem_Click( object sender, EventArgs e )
        {
            _log.MessageCtx(LogCtx, "exitToolStripMenuItem_Click()");
            Shutdown();
        }
        #endregion

        private void helpToolStripMenuItem_Click( object sender, EventArgs e )
        {
            Process.Start( "https://devstar.rockstargames.com/wiki/index.php/RAG" );
        }

        private void resetConsoleToolStripMenuItem_Click( object sender, EventArgs e )
        {
            // Todo: individual console reset
            m_HubManager.ResetConsole( "NameOfHub" );
        }

        private void AutoLaunchRagtoolStripMenuItem_CheckedChanged( object sender, EventArgs e )
        {
            m_LaunchRagAutomatically = AutoLaunchRagtoolStripMenuItem.Checked;

            this.m_LaunchRagInterfaceAutomatically = !this.m_LaunchRagAutomatically;
            this.AutoLaunchRagInterfaceToolStripMenuItem.Checked = this.m_LaunchRagInterfaceAutomatically;

            Settings.Default.AutoLaunchRag = m_LaunchRagAutomatically;
            Settings.Default.Save();
        }
        
        private void AutoLaunchRagInterfaceToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            this.m_LaunchRagInterfaceAutomatically = this.AutoLaunchRagInterfaceToolStripMenuItem.Checked;

            this.m_LaunchRagAutomatically = !this.m_LaunchRagInterfaceAutomatically;
            this.AutoLaunchRagtoolStripMenuItem.Checked = this.m_LaunchRagAutomatically;

            Settings.Default.AutoLaunchRagInterface = this.m_LaunchRagInterfaceAutomatically;
            Settings.Default.Save();
        }

        private void showBalloons_toolStripMenuItem_CheckedChanged( object sender, EventArgs e )
        {
            m_Notifier.Enabled = showBalloons_toolStripMenuItem.Checked;
            Settings.Default.ShowBalloons = m_Notifier.Enabled;
            Settings.Default.Save();
        }

        private DateTime notifyUpdateTime = DateTime.Now;
        private void notifyIcon_MouseMove( object sender, MouseEventArgs e )
        {            
            if (!m_HoverForm.Visible)
            {
                m_HoverForm.Visible = true;
            }

            m_HoverForm.LastKnownMouseHoverPos = Cursor.Position;
        }

        private void AutoVerifyGameLinkMenuItem_Click(object sender, EventArgs e)
        {
            Settings.Default.AutoVerifyGameLink = AutoVerifyGameLinkMenuItem.Checked;
            Settings.Default.Save();
        }

        private void cacheNetworkPacketsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Settings.Default.CacheNetworkPackets = cacheNetworkPacketsToolStripMenuItem.Checked;
            Settings.Default.Save();
        }

        /// <summary>
        /// Called when the "Set Default Connection" dropdown menu is about to open.
        /// </summary>
        private void setDefaultConnectionToolStripMenuItem_DropDownOpening(object sender, EventArgs e)
        {
            // Create a copy of the hubs
            IEnumerable<ProxyHub> hubs = m_HubManager.ActiveHubs;
            this.defaultConnectionContextMenuStrip.Items.Clear();

            if (hubs.Any())
            {
                foreach (ProxyHub hub in hubs)
                {
                    // Create the connection name
                    String connectionName = "";
                    if (hub.ServiceManager.GameService == null)
                    {
                        connectionName = "<unconnected>" + Environment.NewLine;
                    }
                    else
                    {
                        connectionName = hub.ServiceManager.GameService.GetPlatform() + " @ " + hub.ServiceManager.GameService.GetGameIP();
                    }

                    ToolStripMenuItem item = new ToolStripMenuItem(connectionName);
                    if (hub == m_HubManager.DefaultHub)
                    {
                        item.Checked = true;
                    }
                    item.Tag = hub;
                    item.Click += new EventHandler(setDefaultConnectionToolStripMenuItem_Click);

                    this.defaultConnectionContextMenuStrip.Items.Add(item);
                }
            }
            else
            {
                ToolStripMenuItem item = new ToolStripMenuItem("<<no connections>>");
                item.Enabled = false;
                this.defaultConnectionContextMenuStrip.Items.Add(item);
            }
        }


        private void setDefaultConnectionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem item = sender as ToolStripMenuItem;
            if (item != null && item.Tag is ProxyHub)
            {
                ProxyHub hub = (ProxyHub)item.Tag;

                if (hub != m_HubManager.DefaultHub)
                {
                    m_HubManager.DefaultHub = hub;
                }
            }
        }
    }

    public class TaskBarBalloonNotifier : IUINotifyService
    {
        NotifyIcon _icon;
        private readonly ILog _log;

        public TaskBarBalloonNotifier( NotifyIcon icon, ILog log )
        {
            _icon = icon;
            _log = log;
        }

        public bool Enabled { get; set; }

        public void Notify( string title, string msg )
        {
            _log.Message( "TaskBarBalloonNotifier: {0} :: {1}", title, msg );

            if ( !Enabled )
                return;

            _icon.BalloonTipTitle = title;
            _icon.BalloonTipText = msg;
            _icon.ShowBalloonTip(2000);
            _icon.ShowBalloonTip(2000, title, msg, ToolTipIcon.None);
        }
    }
}
