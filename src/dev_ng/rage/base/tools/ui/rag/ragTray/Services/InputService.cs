﻿// ---------------------------------------------------------------------------------------------
// <copyright file="InputService.cs" company="Rockstar Games">
//      Copyright © Rockstar Games 2016. All rights reserved
// </copyright>
// ---------------------------------------------------------------------------------------------

namespace RSG.Rag.Services
{
    using System.Windows.Input;
    using Contracts.Data;
    using Contracts.Services;
    using Services;
    using System;
    using System.ServiceModel;
    using System.Windows.Forms;
    using ragTray;

    /// <summary>
    /// Service to handle input.
    /// </summary>
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, ConcurrencyMode = ConcurrencyMode.Multiple)]
    public class InputService : IInputService, IRagService
    {
        #region Fields
        private readonly IGameProxyService _gameProxyService;
        #endregion

        #region Constructors
        public InputService(int port, IGameProxyService gameProxyService)
        {
            this._gameProxyService = gameProxyService;
            this.Port = port;
        }
        #endregion

        #region Properties
        /// <inheritdoc />
        public int Port { get; }
        #endregion

        #region Methods
        /// <inheritdoc />
        public void QueueKeyboardEvent(KeyboardEvent evt, Key key)
        {
            if (!this._gameProxyService.IsConnected())
            {
                return;
            }

            IInputManager input = this._gameProxyService.GetInputManager();
            input?.SendKeyboardEvent((Input.CommandKeyboard)evt, ConvertToWinFormsKey(key));
        }

        /// <inheritdoc />
        public void QueueMouseEvent(MouseEvent evt, double x, double y, int wheelDelta, double width, double height)
        {
            if (!this._gameProxyService.IsConnected())
            {
                return;
            }

            IInputManager input = this._gameProxyService.GetInputManager();
            input?.SendMouseEvent(
                (Input.CommandMouse)evt,
                false,
                Convert.ToInt32(x),
                Convert.ToInt32(y),
                wheelDelta,
                Convert.ToInt32(width),
                Convert.ToInt32(height));
        }

        private static Keys ConvertToWinFormsKey(Key key)
        {
            var formsKey = (Keys)KeyInterop.VirtualKeyFromKey(key);
            return formsKey;
        }
        #endregion
    }
}