﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Rag.Services
{
    /// <summary>
    /// Interface for the various rag services that exposes information about the port
    /// that the service is currently hosted on.
    /// </summary>
    public interface IRagService
    {
        /// <summary>
        /// Port that the service should listen on.
        /// </summary>
        int Port { get; }
    }
}
