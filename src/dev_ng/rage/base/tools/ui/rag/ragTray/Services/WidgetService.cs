﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ragCore;
using ragTray;
using ragTray.Proxy.Proxies;
using ragWidgets;
using RSG.Base.Collections;
using RSG.Base.Math;
using RSG.Rag.Contracts.Data;
using RSG.Rag.Contracts.Services;
using RSG.Rag.Services.Exceptions;

namespace RSG.Rag.Services
{
    /// <summary>
    /// Service that provides widget information.
    /// </summary>
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, ConcurrencyMode = ConcurrencyMode.Multiple)]
    internal class WidgetService : IRagService, IWidgetService
    {
        #region Fields
        /// <summary>
        /// 
        /// </summary>
        private readonly IGameProxyService _gameProxyService;

        /// <summary>
        /// Reference to the guy you need to see about taking out a loan...
        /// </summary>
        private readonly IBankManager _bankManager;

        /// <summary>
        /// 
        /// </summary>
        private readonly ServicesProxy _appProxy;

        /// <summary>
        /// 
        /// </summary>
        private readonly InterProxyStream _bankRawStream;

        /// <summary>
        /// Private field for the <see cref="Port"/> property.
        /// </summary>
        private readonly int _port;
        #endregion

        #region Constructor(s)
        /// <summary>
        /// Initialises a new instance of the <see cref="WidgetService"/> class.
        /// </summary>
        /// <param name="port"></param>
        /// <param name="bankManager"></param>
        public WidgetService(int port, IGameProxyService gameProxyService, InterProxyStream bankRawStream, ServicesProxy appProxy)
        {
            _appProxy = appProxy;
            _gameProxyService = gameProxyService;
            _bankManager = gameProxyService.GetBankManager();
            _bankRawStream = bankRawStream;
            _port = port;
        }
        #endregion

        #region Properties
        /// <summary>
        /// Port that the service is hosted on.
        /// </summary>
        public int Port
        {
            get { return _port; }
        }
        #endregion // Properties

        #region IWidgetService Implementation
        #region General Operations
        /// <summary>
        /// Returns whether a particular widget exists.
        /// </summary>
        /// <param name="widgetPath"></param>
        /// <returns></returns>
        public bool WidgetExists(String widgetPath)
        {
            List<Widget> widgets = _bankManager.FindWidgetsFromPath(widgetPath);
            return (widgets != null && widgets.Any());
        }

        /// <summary>
        /// Returns whether a particular widget exists.
        /// </summary>
        /// <param name="widgetId"></param>
        /// <returns></returns>
        public bool WidgetExists(uint widgetId)
        {
            Widget widget = _bankManager.FindWidgetFromID(widgetId);
            return widget != null;
        }

        /// <summary>
        /// Retrieves a particular widgets id.
        /// </summary>
        /// <param name="widgetPath"></param>
        /// <returns></returns>
        public uint GetWidgetId(String widgetPath)
        {
            // Try and retrieve the widget.
            List<Widget> widgetList = _bankManager.FindWidgetsFromPath(widgetPath);

            if (widgetList.Count == 0)
            {
                throw new WidgetNotFoundException(widgetPath);
            }
            else if (widgetList.Count > 1)
            {
                throw new MultipleWidgetsFoundException(widgetPath);
            }

            // Return the only widget's id.
            Widget widget = widgetList[0];
            return widget.Pipe.LocalToRemote(widget);
        }

        /// <summary>
        /// Retrieves a particular widgets path.
        /// </summary>
        /// <param name="widgetId"></param>
        /// <returns></returns>
        public String GetWidgetPath(uint widgetId)
        {
            Widget widget = _bankManager.FindWidgetFromID(widgetId);
            if (widget == null)
            {
                throw new WidgetNotFoundException(widgetId);
            }
            return widget.Path;
        }

        /// <summary>
        /// Adds a reference to the specified bank/group widget indicating that it is
        /// actively in use and to ensure that we receive value updates for child widgets.
        /// </summary>
        /// <param name="bankPath"></param>
        /// <returns></returns>
        public void AddBankReference(String bankPath)
        {
            WidgetGroupBase group = GetAndCheckWidget<WidgetGroupBase>(bankPath);
            AddBankReference(group);
        }

        /// <summary>
        /// Adds a reference to the specified bank/group widget indicating that it is
        /// actively in use and to ensure that we receive value updates for child widgets.
        /// </summary>
        /// <param name="widgetId"></param>
        /// <returns></returns>
        public void AddBankReference(uint widgetId)
        {
            WidgetGroupBase group = GetAndCheckWidget<WidgetGroupBase>(widgetId);
            AddBankReference(group);
        }

        /// <summary>
        /// Adds a reference to the specified bank/group widget indicating that it is
        /// actively in use and to ensure that we receive value updates for child widgets.
        /// </summary>
        /// <param name="widget"></param>
        private void AddBankReference(WidgetGroupBase group)
        {
            lock (_gameProxyService.AcquireLockObject())
            {
                do
                {
                    InterceptAndForwardPackets(group, () => group.AddRefGroup());
                    group = group.Parent as WidgetGroupBase;
                }
                while (group != null);
            }
        }

        /// <summary>
        /// Removes a reference to the specified bank/group widget.
        /// </summary>
        /// <param name="bankPath"></param>
        /// <returns></returns>
        public void RemoveBankReference(String bankPath)
        {
            WidgetGroupBase group = GetAndCheckWidget<WidgetGroupBase>(bankPath);
            RemoveBankReference(group);
        }

        /// <summary>
        /// Removes a reference to the specified bank/group widget.
        /// </summary>
        /// <param name="widgetId"></param>
        /// <returns></returns>
        public void RemoveBankReference(uint widgetId)
        {
            WidgetGroupBase group = GetAndCheckWidget<WidgetGroupBase>(widgetId);
            RemoveBankReference(group);
        }

        /// <summary>
        /// Removes a reference to the specified bank/group widget.
        /// </summary>
        /// <param name="widget"></param>
        private void RemoveBankReference(WidgetGroupBase group)
        {
            lock (_gameProxyService.AcquireLockObject())
            {
                do
                {
                    InterceptAndForwardPackets(group, () => group.ReleaseGroup());
                    group = group.Parent as WidgetGroupBase;
                }
                while (group != null);
            }
        }

        /// <summary>
        /// A special command that will block until all currently pending bank packets sent
        /// from the game have been processed. This is extremely useful when many widgets
        /// are being changed that may be dependent on each other.
        /// </summary>
        /// <param name="timeout">
        /// Optional timeout (in ms) for how long we should wait before aborting the sync
        /// command.
        /// </param>
        public void SendSyncCommand(uint timeout = 0)
        {
            TimeSpan timeoutSpan = new TimeSpan(0, 0, 0, (int)timeout);

            // this is ugly - we need a better way to maintain the bankpipe:
            BankPipe p = _bankManager.AllBanks[0].Pipe;

            bool receivedResponse = false;
            EventHandler pingCallback =
                delegate(object owner, EventArgs e)
                {
                    receivedResponse = true;
                };

            // Send the delayed ping request and await it's response.
            DateTime startTime = DateTime.UtcNow;

            _bankManager.SendDelayedPing(pingCallback, p);
            while (!receivedResponse)
            {
                TimeSpan elapsedTime = DateTime.UtcNow - startTime;
                if (timeoutSpan != TimeSpan.Zero && elapsedTime > timeoutSpan)
                {
                    throw new TimeoutException(
                        String.Format("Sync command timed out after {0} ms.", elapsedTime.TotalMilliseconds));
                }

                Thread.Sleep(25);
            }
        }

        /// <summary>
        /// Presses the specified widget as a button.
        /// </summary>
        /// <param name="widgetPath"></param>
        /// <returns></returns>
        public void PressButton(String widgetPath)
        {
            WidgetButton button = GetAndCheckWidget<WidgetButton>(widgetPath);
            InterceptAndForwardPackets(button, () => button.Activate());
        }

        /// <summary>
        /// Presses the specified widget as a button.
        /// </summary>
        /// <param name="widgetId"></param>
        /// <returns></returns>
        public void PressButton(uint widgetId)
        {
            WidgetButton button = GetAndCheckWidget<WidgetButton>(widgetId);
            InterceptAndForwardPackets(button, () => button.Activate());
        }

        /// <summary>
        /// Presses the specified widget as a button.
        /// </summary>
        /// <param name="widgetPath"></param>
        /// <returns></returns>
        public void PressVCRButton(String widgetPath, VCRButton button)
        {
            WidgetVCR vcr = GetAndCheckWidget<WidgetVCR>(widgetPath);
            InterceptAndForwardPackets(vcr, () => PressVCRButton(vcr, button));
        }

        /// <summary>
        /// Presses the specified widget as a button.
        /// </summary>
        /// <param name="widgetId"></param>
        /// <returns></returns>
        public void PressVCRButton(uint widgetId, VCRButton button)
        {
            WidgetVCR vcr = GetAndCheckWidget<WidgetVCR>(widgetId);
            InterceptAndForwardPackets(vcr, () => PressVCRButton(vcr, button));
        }

        /// <summary>
        /// Presses the specified widget as a button.
        /// </summary>
        /// <param name="widget"></param>
        /// <param name="button"></param>
        private void PressVCRButton(WidgetVCR widget, VCRButton button)
        {
            switch (button)
            {
                case VCRButton.FastForward:
                    widget.FastForwardOrGoToEnd();
                    break;
                case VCRButton.Pause:
                    widget.Pause();
                    break;
                case VCRButton.PlayBackwards:
                    widget.PlayBackwards();
                    break;
                case VCRButton.PlayForwards:
                    widget.PlayForwards();
                    break;
                case VCRButton.Rewind:
                    widget.RewindOrGoToStart();
                    break;
                case VCRButton.StepBackwards:
                    widget.GoToPreviousOrStepBackward();
                    break;
                case VCRButton.StepForwards:
                    widget.GoToNextOrStepForward();
                    break;
                default:
                    throw new ArgumentException(String.Format("Unable to determine an appropriate action for the {0} button.", button.ToString()));
            }
        }
        #endregion

        #region Read Operations
        /// <summary>
        /// Reads a boolean value from the specified widget.
        /// </summary>
        /// <param name="widgetPath"></param>
        /// <returns></returns>
        public bool ReadBoolWidget(String widgetPath)
        {
            Widget widget = GetAndCheckWidget(widgetPath);
            return ReadBoolWidget(widget);
        }

        /// <summary>
        /// Reads a boolean value from the specified widget.
        /// </summary>
        /// <param name="widgetId"></param>
        /// <returns></returns>
        public bool ReadBoolWidget(uint widgetId)
        {
            Widget widget = GetAndCheckWidget(widgetId);
            return ReadBoolWidget(widget);
        }

        /// <summary>
        /// Reads a boolean value from the specified widget.
        /// </summary>
        /// <param name="widget"></param>
        /// <returns></returns>
        private bool ReadBoolWidget(Widget widget)
        {
            if (widget is WidgetToggle)
            {
                WidgetToggle toggle = (WidgetToggle)widget;
                return toggle.Checked;
            }
            else if (widget is WidgetText)
            {
                WidgetText text = (WidgetText)widget;
                if (text.TypeOfData == WidgetText.DataType.BOOL)
                {
                    return Boolean.Parse(text.String);
                }
                else
                {
                    throw new WidgetException(widget.Path, "Text widget is not of a boolean type.");
                }
            }
            else
            {
                throw new InvalidCastException(String.Format("The '{0}' widget can't provide a boolean value.", widget.Path));
            }
        }

        /// <summary>
        /// Reads a float value from the specified widget.
        /// </summary>
        /// <param name="widgetPath"></param>
        /// <returns></returns>
        public float ReadFloatWidget(String widgetPath)
        {
            Widget widget = GetAndCheckWidget(widgetPath);
            return ReadFloatWidget(widget);
        }

        /// <summary>
        /// Reads a float value from the specified widget.
        /// </summary>
        /// <param name="widgetId"></param>
        /// <returns></returns>
        public float ReadFloatWidget(uint widgetId)
        {
            Widget widget = GetAndCheckWidget(widgetId);
            return ReadFloatWidget(widget);
        }

        /// <summary>
        /// Reads a float value from the specified widget.
        /// </summary>
        /// <param name="widget"></param>
        /// <returns></returns>
        private float ReadFloatWidget(Widget widget)
        {
            if (widget is WidgetSliderFloat)
            {
                WidgetSliderFloat slider = (WidgetSliderFloat)widget;
                return slider.Value;
            }
            else if (widget is WidgetText)
            {
                WidgetText text = (WidgetText)widget;
                if (text.TypeOfData == WidgetText.DataType.FLOAT)
                {
                    return Single.Parse(text.String);
                }
                else
                {
                    throw new WidgetException(widget.Path, "Text widget is not of a floating point type.");
                }
            }
            else if (widget is WidgetAngle)
            {
                WidgetAngle angle = (WidgetAngle)widget;
                if (angle.DisplayType == WidgetAngle.AngleType.Vector2)
                {
                    throw new WidgetException(widget.Path, "Angle widget is not of a floating point type.");
                }
                else
                {
                    return angle.Value[0];
                }
            }
            else
            {
                throw new InvalidCastException(String.Format("The '{0}' widget can't provide a floating point value.", widget.Path));
            }
        }

        /// <summary>
        /// Retrieves a widgets floating point limits.
        /// </summary>
        /// <param name="widgetPath"></param>
        /// <returns></returns>
        public Range<float> ReadFloatWidgetLimits(String widgetPath)
        {
            WidgetSliderFloat widget = GetAndCheckWidget<WidgetSliderFloat>(widgetPath);
            return new Range<float>(widget.Minimum, widget.Maximum);
        }

        /// <summary>
        /// Retrieves a widgets floating point limits.
        /// </summary>
        /// <param name="widgetId"></param>
        /// <returns></returns>
        public Range<float> ReadFloatWidgetLimits(uint widgetId)
        {
            WidgetSliderFloat widget = GetAndCheckWidget<WidgetSliderFloat>(widgetId);
            return new Range<float>(widget.Minimum, widget.Maximum);
        }

        /// <summary>
        /// Reads an integer value from the specified widget.
        /// </summary>
        /// <param name="widgetPath"></param>
        /// <returns></returns>
        public int ReadIntWidget(String widgetPath)
        {
            Widget widget = GetAndCheckWidget(widgetPath);
            return ReadIntWidget(widget);
        }

        /// <summary>
        /// Reads an integer value from the specified widget.
        /// </summary>
        /// <param name="widgetId"></param>
        /// <returns></returns>
        public int ReadIntWidget(uint widgetId)
        {
            Widget widget = GetAndCheckWidget(widgetId);
            return ReadIntWidget(widget);
        }

        /// <summary>
        /// Reads an integer value from the specified widget.
        /// </summary>
        /// <param name="widget"></param>
        /// <returns></returns>
        private int ReadIntWidget(Widget widget)
        {
            if (widget is WidgetSliderInt)
            {
                WidgetSliderInt slider = (WidgetSliderInt)widget;
                return (int)slider.Value;
            }
            else if (widget is WidgetText)
            {
                WidgetText text = (WidgetText)widget;
                if (text.TypeOfData == WidgetText.DataType.INT)
                {
                    return Int32.Parse(text.String);
                }
                else
                {
                    throw new WidgetException(widget.Path, "Text widget is not of an integer type.");
                }
            }
            else
            {
                throw new InvalidCastException(String.Format("The '{0}' widget can't provide an integer value.", widget.Path));
            }
        }

        /// <summary>
        /// Reads a string value from the specified widget.
        /// </summary>
        /// <param name="widgetPath"></param>
        /// <returns></returns>
        public String ReadStringWidget(String widgetPath)
        {
            Widget widget = GetAndCheckWidget(widgetPath);
            return ReadStringWidget(widget);
        }

        /// <summary>
        /// Reads a string value from the specified widget.
        /// </summary>
        /// <param name="widgetId"></param>
        /// <returns></returns>
        public String ReadStringWidget(uint widgetId)
        {
            Widget widget = GetAndCheckWidget(widgetId);
            return ReadStringWidget(widget);
        }

        /// <summary>
        /// Reads a string value from the specified widget.
        /// </summary>
        /// <param name="widget"></param>
        /// <returns></returns>
        private String ReadStringWidget(Widget widget)
        {
            if (widget is WidgetCombo)
            {
                WidgetCombo combo = (WidgetCombo)widget;
                return combo.Items[(int)combo.Value];
            }
            else if (widget is WidgetText)
            {
                WidgetText text = (WidgetText)widget;
                if (text.TypeOfData == WidgetText.DataType.STRING)
                {
                    return text.String;
                }
                else
                {
                    throw new WidgetException(widget.Path, "Text widget is not of a string type.");
                }
            }
            else
            {
                throw new InvalidCastException(String.Format("The '{0}' widget can't provide a string value.", widget.Path));
            }
        }

        /// <summary>
        /// Reads a vector2 value from the specified widget.
        /// </summary>
        /// <param name="widgetPath"></param>
        /// <returns></returns>
        public Vector2f ReadVector2Widget(String widgetPath)
        {
            WidgetVector2 widget = GetAndCheckWidget<WidgetVector2>(widgetPath);
            return new Vector2f(widget.Value);
        }

        /// <summary>
        /// Reads a vector2 value from the specified widget.
        /// </summary>
        /// <param name="widgetId"></param>
        /// <returns></returns>
        public Vector2f ReadVector2Widget(uint widgetId)
        {
            WidgetVector2 widget = GetAndCheckWidget<WidgetVector2>(widgetId);
            return new Vector2f(widget.Value);
        }

        /// <summary>
        /// Reads a vector3 value from the specified widget.
        /// </summary>
        /// <param name="widgetPath"></param>
        /// <returns></returns>
        public Vector3f ReadVector3Widget(String widgetPath)
        {
            WidgetVector3 widget = GetAndCheckWidget<WidgetVector3>(widgetPath);
            return new Vector3f(widget.Value);
        }

        /// <summary>
        /// Reads a vector3 value from the specified widget.
        /// </summary>
        /// <param name="widgetPath"></param>
        /// <returns></returns>
        public Vector3f ReadVector3Widget(uint widgetId)
        {
            WidgetVector3 widget = GetAndCheckWidget<WidgetVector3>(widgetId);
            return new Vector3f(widget.Value);
        }

        /// <summary>
        /// Reads a vector4 value from the specified widget.
        /// </summary>
        /// <param name="widgetPath"></param>
        /// <returns></returns>
        public Vector4f ReadVector4Widget(String widgetPath)
        {
            WidgetVector4 widget = GetAndCheckWidget<WidgetVector4>(widgetPath);
            return new Vector4f(widget.Value);
        }

        /// <summary>
        /// Reads a vector4 value from the specified widget.
        /// </summary>
        /// <param name="widgetId"></param>
        /// <returns></returns>
        public Vector4f ReadVector4Widget(uint widgetId)
        {
            WidgetVector4 widget = GetAndCheckWidget<WidgetVector4>(widgetId);
            return new Vector4f(widget.Value);
        }

        /// <summary>
        /// Reads a matrix33 value from the specified widget.
        /// </summary>
        /// <param name="widgetPath"></param>
        /// <returns></returns>
        public Matrix33f ReadMatrix33Widget(String widgetPath)
        {
            WidgetMatrix33 widget = GetAndCheckWidget<WidgetMatrix33>(widgetPath);
            return new Matrix33f(widget.Value);
        }

        /// <summary>
        /// Reads a matrix33 value from the specified widget.
        /// </summary>
        /// <param name="widgetId"></param>
        /// <returns></returns>
        public Matrix33f ReadMatrix33Widget(uint widgetId)
        {
            WidgetMatrix33 widget = GetAndCheckWidget<WidgetMatrix33>(widgetId);
            return new Matrix33f(widget.Value);
        }

        /// <summary>
        /// Reads a matrix34 value from the specified widget.
        /// </summary>
        /// <param name="widgetPath"></param>
        /// <returns></returns>
        public Matrix34f ReadMatrix34Widget(String widgetPath)
        {
            WidgetMatrix34 widget = GetAndCheckWidget<WidgetMatrix34>(widgetPath);
            return new Matrix34f(widget.Value);
        }

        /// <summary>
        /// Reads a matrix34 value from the specified widget.
        /// </summary>
        /// <param name="widgetId"></param>
        /// <returns></returns>
        public Matrix34f ReadMatrix34Widget(uint widgetId)
        {
            WidgetMatrix34 widget = GetAndCheckWidget<WidgetMatrix34>(widgetId);
            return new Matrix34f(widget.Value);
        }

        /// <summary>
        /// Reads a matrix44 value from the specified widget.
        /// </summary>
        /// <param name="widgetPath"></param>
        /// <returns></returns>
        public Matrix44f ReadMatrix44Widget(String widgetPath)
        {
            WidgetMatrix44 widget = GetAndCheckWidget<WidgetMatrix44>(widgetPath);
            return new Matrix44f(widget.Value);
        }

        /// <summary>
        /// Reads a matrix44 value from the specified widget.
        /// </summary>
        /// <param name="widgetId"></param>
        /// <returns></returns>
        public Matrix44f ReadMatrix44Widget(uint widgetId)
        {
            WidgetMatrix44 widget = GetAndCheckWidget<WidgetMatrix44>(widgetId);
            return new Matrix44f(widget.Value);
        }

        /// <summary>
        /// Reads a byte array value from the specified widget.
        /// </summary>
        /// <param name="widgetPath"></param>
        /// <returns></returns>
        public byte[] ReadDataWidget(String widgetPath)
        {
            WidgetData widget = GetAndCheckWidget<WidgetData>(widgetPath);
            return widget.Data;
        }

        /// <summary>
        /// Reads a byte array value from the specified widget.
        /// </summary>
        /// <param name="widgetId"></param>
        /// <returns></returns>
        public byte[] ReadDataWidget(uint widgetId)
        {
            WidgetData widget = GetAndCheckWidget<WidgetData>(widgetId);
            return widget.Data;
        }

        /// <summary>
        /// Gets a list of all the available combo box items for the specified widget.
        /// </summary>
        /// <param name="widgetPath"></param>
        /// <returns></returns>
        public String[] GetComboBoxItems(String widgetPath)
        {
            WidgetCombo widget = GetAndCheckWidget<WidgetCombo>(widgetPath);
            return widget.Items.ToArray();
        }

        /// <summary>
        /// Gets a list of all the available combo box items for the specified widget.
        /// </summary>
        /// <param name="widgetId"></param>
        /// <returns></returns>
        public String[] GetComboBoxItems(uint widgetId)
        {
            WidgetCombo widget = GetAndCheckWidget<WidgetCombo>(widgetId);
            return widget.Items.ToArray();
        }
        #endregion

        #region Write Operations
        /// <summary>
        /// Writes a boolean value to the specified widget.
        /// </summary>
        /// <param name="widgetPath"></param>
        /// <param name="value"></param>
        public void WriteBoolWidget(String widgetPath, bool value)
        {
            Widget widget = GetAndCheckWidget(widgetPath);
            InterceptAndForwardPackets(widget, () => WriteBoolWidget(widget, value));
        }

        /// <summary>
        /// Writes a boolean value to the specified widget.
        /// </summary>
        /// <param name="widgetId"></param>
        /// <param name="value"></param>
        public void WriteBoolWidget(uint widgetId, bool value)
        {
            Widget widget = GetAndCheckWidget(widgetId);
            InterceptAndForwardPackets(widget, () => WriteBoolWidget(widget, value));
        }

        /// <summary>
        /// Writes a boolean value to the specified widget.
        /// </summary>
        /// <param name="widget"></param>
        /// <param name="value"></param>
        private void WriteBoolWidget(Widget widget, bool value)
        {
            if (widget is WidgetToggle)
            {
                WidgetToggle toggle = (WidgetToggle)widget;
                toggle.Checked = value;
            }
            else if (widget is WidgetText)
            {
                WidgetText text = (WidgetText)widget;
                if (text.TypeOfData == WidgetText.DataType.BOOL)
                {
                    text.String = value.ToString();
                }
                else
                {
                    throw new WidgetException(widget.Path, "Text widget is not of a boolean type.");
                }
            }
            else
            {
                throw new InvalidCastException(String.Format("The '{0}' widget can't be set from a boolean value.", widget.Path));
            }
        }

        /// <summary>
        /// Writes a float value to the specified widget.
        /// </summary>
        /// <param name="widgetPath"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public void WriteFloatWidget(String widgetPath, float value)
        {
            Widget widget = GetAndCheckWidget(widgetPath);
            InterceptAndForwardPackets(widget, () => WriteFloatWidget(widget, value));
        }

        /// <summary>
        /// Writes a float value to the specified widget.
        /// </summary>
        /// <param name="widgetId"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public void WriteFloatWidget(uint widgetId, float value)
        {
            Widget widget = GetAndCheckWidget(widgetId);
            InterceptAndForwardPackets(widget, () => WriteFloatWidget(widget, value));
        }

        /// <summary>
        /// Writes a float value to the specified widget.
        /// </summary>
        /// <param name="widget"></param>
        /// <param name="value"></param>
        private void WriteFloatWidget(Widget widget, float value)
        {
            if (widget is WidgetSliderFloat)
            {
                WidgetSliderFloat slider = (WidgetSliderFloat)widget;
                slider.Value = value;
            }
            else if (widget is WidgetText)
            {
                WidgetText text = (WidgetText)widget;
                if (text.TypeOfData == WidgetText.DataType.FLOAT)
                {
                    text.String = value.ToString();
                }
                else
                {
                    throw new WidgetException(widget.Path, "Text widget is not of a floating point type.");
                }
            }
            else if (widget is WidgetAngle)
            {
                WidgetAngle angle = (WidgetAngle)widget;
                if (angle.DisplayType == WidgetAngle.AngleType.Vector2)
                {
                    throw new WidgetException(widget.Path, "Angle widget is not of a floating point type.");
                }
                else
                {
                    float[] val = new float[2];
                    val[0] = value;
                    angle.Value = val;
                }
            }
            else
            {
                throw new InvalidCastException(String.Format("The '{0}' widget can't be set from a float value.", widget.Path));
            }
        }

        /// <summary>
        /// Writes an integer value to the specified widget.
        /// </summary>
        /// <param name="widgetPath"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public void WriteIntWidget(String widgetPath, int value)
        {
            Widget widget = GetAndCheckWidget(widgetPath);
            InterceptAndForwardPackets(widget, () => WriteIntWidget(widget, value));
        }

        /// <summary>
        /// Writes an integer value to the specified widget.
        /// </summary>
        /// <param name="widgetId"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public void WriteIntWidget(uint widgetId, int value)
        {
            Widget widget = GetAndCheckWidget(widgetId);
            InterceptAndForwardPackets(widget, () => WriteIntWidget(widget, value));
        }

        /// <summary>
        /// Writes an integer value to the specified widget.
        /// </summary>
        /// <param name="widget"></param>
        /// <param name="value"></param>
        private void WriteIntWidget(Widget widget, int value)
        {
            if (widget is WidgetSliderInt)
            {
                WidgetSliderInt slider = (WidgetSliderInt)widget;
                slider.Value = value;
            }
            else if (widget is WidgetText)
            {
                WidgetText text = (WidgetText)widget;
                if (text.TypeOfData == WidgetText.DataType.INT)
                {
                    text.String = value.ToString();
                }
                else
                {
                    throw new WidgetException(widget.Path, "Text widget is not of an integer type.");
                }
            }
            else
            {
                throw new InvalidCastException(String.Format("The '{0}' widget can't be set from an integer value.", widget.Path));
            }
        }

        /// <summary>
        /// Writes a string value to the specified widget.
        /// </summary>
        /// <param name="widgetPath"></param>
        /// <param name="value"></param>
        public void WriteStringWidget(String widgetPath, String value)
        {
            Widget widget = GetAndCheckWidget(widgetPath);
            InterceptAndForwardPackets(widget, () => WriteStringWidget(widget, value));
        }

        /// <summary>
        /// Writes a string value to the specified widget.
        /// </summary>
        /// <param name="widgetId"></param>
        /// <param name="value"></param>
        public void WriteStringWidget(uint widgetId, String value)
        {
            Widget widget = GetAndCheckWidget(widgetId);
            InterceptAndForwardPackets(widget, () => WriteStringWidget(widget, value));
        }

        /// <summary>
        /// Writes a string value to the specified widget.
        /// </summary>
        /// <param name="widget"></param>
        /// <param name="value"></param>
        private void WriteStringWidget(Widget widget, String value)
        {
            if (widget is WidgetCombo)
            {
                WidgetCombo combo = (WidgetCombo)widget;

                for (int i = 0; i < combo.Items.Count; ++i)
                {
                    if (String.Compare(combo.Items[i], value, true) == 0)
                    {
                        combo.Value = i - combo.Offset;
                        break;
                    }
                }
            }
            else if (widget is WidgetText)
            {
                WidgetText text = (WidgetText)widget;
                if (text.TypeOfData == WidgetText.DataType.STRING)
                {
                    text.String = value.ToString();
                }
                else
                {
                    throw new WidgetException(widget.Path, "Text widget is not of a string type.");
                }
            }
            else if (widget is WidgetVCR)
            {
                widget.ProcessCommand(new String[] { value });
            }
            else
            {
                throw new InvalidCastException(String.Format("The '{0}' widget can't be set from a string value.", widget.Path));
            }
        }

        /// <summary>
        /// Writes a vector2 value to the specified widget.
        /// </summary>
        /// <param name="widgetPath"></param>
        /// <param name="value"></param>
        public void WriteVector2Widget(String widgetPath, Vector2f value)
        {
            WidgetVector2 widget = GetAndCheckWidget<WidgetVector2>(widgetPath);
            InterceptAndForwardPackets(widget, () => widget.Value = (float[])value);
        }

        /// <summary>
        /// Writes a vector2 value to the specified widget.
        /// </summary>
        /// <param name="widgetId"></param>
        /// <param name="value"></param>
        public void WriteVector2Widget(uint widgetId, Vector2f value)
        {
            WidgetVector2 widget = GetAndCheckWidget<WidgetVector2>(widgetId);
            InterceptAndForwardPackets(widget, () => widget.Value = (float[])value);
        }

        /// <summary>
        /// Writes a vector3 value to the specified widget.
        /// </summary>
        /// <param name="widgetPath"></param>
        /// <param name="value"></param>
        public void WriteVector3Widget(String widgetPath, Vector3f value)
        {
            WidgetVector3 widget = GetAndCheckWidget<WidgetVector3>(widgetPath);
            InterceptAndForwardPackets(widget, () => widget.Value = (float[])value);
        }

        /// <summary>
        /// Writes a vector3 value to the specified widget.
        /// </summary>
        /// <param name="widgetId"></param>
        /// <param name="value"></param>
        public void WriteVector3Widget(uint widgetId, Vector3f value)
        {
            WidgetVector3 widget = GetAndCheckWidget<WidgetVector3>(widgetId);
            InterceptAndForwardPackets(widget, () => widget.Value = (float[])value);
        }

        /// <summary>
        /// Writes a vector4 value to the specified widget.
        /// </summary>
        /// <param name="widgetPath"></param>
        /// <param name="value"></param>
        public void WriteVector4Widget(String widgetPath, Vector4f value)
        {
            WidgetVector4 widget = GetAndCheckWidget<WidgetVector4>(widgetPath);
            InterceptAndForwardPackets(widget, () => widget.Value = (float[])value);
        }

        /// <summary>
        /// Writes a vector4 value to the specified widget.
        /// </summary>
        /// <param name="widgetId"></param>
        /// <param name="value"></param>
        public void WriteVector4Widget(uint widgetId, Vector4f value)
        {
            WidgetVector4 widget = GetAndCheckWidget<WidgetVector4>(widgetId);
            InterceptAndForwardPackets(widget, () => widget.Value = (float[])value);
        }

        /// <summary>
        /// Writes a matrix33 value to the specified widget.
        /// </summary>
        /// <param name="widgetPath"></param>
        /// <param name="value"></param>
        public void WriteMatrix33Widget(String widgetPath, Matrix33f value)
        {
            WidgetMatrix33 widget = GetAndCheckWidget<WidgetMatrix33>(widgetPath);
            InterceptAndForwardPackets(widget, () => widget.Value = (float[][])value);
        }

        /// <summary>
        /// Writes a matrix33 value to the specified widget.
        /// </summary>
        /// <param name="widgetId"></param>
        /// <param name="value"></param>
        public void WriteMatrix33Widget(uint widgetId, Matrix33f value)
        {
            WidgetMatrix33 widget = GetAndCheckWidget<WidgetMatrix33>(widgetId);
            InterceptAndForwardPackets(widget, () => widget.Value = (float[][])value);
        }

        /// <summary>
        /// Writes a matrix34 value to the specified widget.
        /// </summary>
        /// <param name="widgetPath"></param>
        /// <param name="value"></param>
        public void WriteMatrix34Widget(String widgetPath, Matrix34f value)
        {
            WidgetMatrix34 widget = GetAndCheckWidget<WidgetMatrix34>(widgetPath);
            InterceptAndForwardPackets(widget, () => widget.Value = (float[][])value);
        }

        /// <summary>
        /// Writes a matrix34 value to the specified widget.
        /// </summary>
        /// <param name="widgetId"></param>
        /// <param name="value"></param>
        public void WriteMatrix34Widget(uint widgetId, Matrix34f value)
        {
            WidgetMatrix34 widget = GetAndCheckWidget<WidgetMatrix34>(widgetId);
            InterceptAndForwardPackets(widget, () => widget.Value = (float[][])value);
        }

        /// <summary>
        /// Writes a matrix44 value to the specified widget.
        /// </summary>
        /// <param name="widgetPath"></param>
        /// <param name="value"></param>
        public void WriteMatrix44Widget(String widgetPath, Matrix44f value)
        {
            WidgetMatrix44 widget = GetAndCheckWidget<WidgetMatrix44>(widgetPath);
            InterceptAndForwardPackets(widget, () => widget.Value = (float[][])value);
        }

        /// <summary>
        /// Writes a matrix44 value to the specified widget.
        /// </summary>
        /// <param name="widgetId"></param>
        /// <param name="value"></param>
        public void WriteMatrix44Widget(uint widgetId, Matrix44f value)
        {
            WidgetMatrix44 widget = GetAndCheckWidget<WidgetMatrix44>(widgetId);
            InterceptAndForwardPackets(widget, () => widget.Value = (float[][])value);
        }

        /// <summary>
        /// Writes a byte array to the specified widget.
        /// </summary>
        /// <param name="widgetPath"></param>
        /// <param name="value"></param>
        public void WriteDataWidget(String widgetPath, byte[] value)
        {
            WidgetData widget = GetAndCheckWidget<WidgetData>(widgetPath);
            InterceptAndForwardPackets(widget, () => widget.SetValue(value));
        }

        /// <summary>
        /// Writes a byte array to the specified widget.
        /// </summary>
        /// <param name="widgetId"></param>
        /// <param name="value"></param>
        public void WriteDataWidget(uint widgetId, byte[] value)
        {
            WidgetData widget = GetAndCheckWidget<WidgetData>(widgetId);
            InterceptAndForwardPackets(widget, () => widget.SetValue(value));
        }
        #endregion
        #endregion

        #region Util Methods
        /// <summary>
        /// Attempts to retrieve a widget based on its path, throwing an exception if it
        /// doesn't exist.
        /// </summary>
        /// <param name="widgetPath"></param>
        /// <returns></returns>
        private Widget GetAndCheckWidget(String widgetPath)
        {
            Widget widget = _bankManager.FindFirstWidgetFromPath(widgetPath);
            if (widget == null)
            {
                throw new WidgetNotFoundException(widgetPath);
            }
            return widget;
        }

        /// <summary>
        /// Attempts to retrieve a widget of a certain type based on its path, throwing an
        /// exception if it doesn't exist or isn't of the right type.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="widgetPath"></param>
        /// <returns></returns>
        private T GetAndCheckWidget<T>(String widgetPath) where T : Widget
        {
            Widget widget = _bankManager.FindFirstWidgetFromPath(widgetPath);
            if (widget == null)
            {
                throw new WidgetNotFoundException(widgetPath);
            }

            T typedWidget = widget as T;
            if (typedWidget == null)
            {
                throw new InvalidCastException(String.Format("The '{0}' widget isn't of type {1}.", widgetPath, typeof(T).FullName));
            }

            return typedWidget;
        }

        /// <summary>
        /// Attempts to retrieve a widget based on its id, throwing an exception if it
        /// doesn't exist.
        /// </summary>
        /// <param name="widgetId"></param>
        /// <returns></returns>
        private Widget GetAndCheckWidget(uint widgetId)
        {
            Widget widget = _bankManager.FindWidgetFromID(widgetId);
            if (widget == null)
            {
                throw new WidgetNotFoundException(widgetId);
            }
            return widget;
        }

        /// <summary>
        /// Attempts to retrieve a widget of a certain type based on its id, throwing an
        /// exception if it doesn't exist or isn't of the right type.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="widgetId"></param>
        /// <returns></returns>
        private T GetAndCheckWidget<T>(uint widgetId) where T : Widget
        {
            Widget widget = _bankManager.FindWidgetFromID(widgetId);
            if (widget == null)
            {
                throw new WidgetNotFoundException(widgetId);
            }

            T typedWidget = widget as T;
            if (typedWidget == null)
            {
                throw new InvalidCastException(String.Format("Widget with id '{0}' isn't of type {1}.", widgetId, typeof(T).FullName));
            }

            return typedWidget;
        }

        /// <summary>
        /// Utility method which sets up a temporary pipe to capture any remote packets
        /// that get generated by the widget while performing an action and then
        /// forwarding them to all other app proxies for processing.
        /// </summary>
        /// <param name="widget"></param>
        /// <param name="action"></param>
        private void InterceptAndForwardPackets(Widget widget, Action action)
        {
            lock (_gameProxyService.AcquireLockObject())
            {
                List<NetworkData> updateDataList = new List<NetworkData>();
                HubStreamPipe pipe = new HubStreamPipe(updateDataList, _bankManager.AllBanks[0].Pipe);

                BankPipe tmpPipe = widget.Pipe;
                widget.Pipe = pipe;
                action.Invoke();
                widget.Pipe = tmpPipe;

                foreach (NetworkData data in updateDataList)
                {
                    _bankRawStream.Write(_appProxy, data.Data);
                }
            }
        }
        #endregion
    }
}
