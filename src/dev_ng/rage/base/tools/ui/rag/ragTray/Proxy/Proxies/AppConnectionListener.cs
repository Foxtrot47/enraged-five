﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading;
using System.Net.Sockets;
using System.Diagnostics;
using ragCore;
using System.Net;
using RSG.Base.Forms;
using System.Threading.Tasks;
using LitJson;

namespace ragTray
{
    /// <summary>
    /// 
    /// </summary>
    public interface IConnectionListenerService
    {
        int ConnectionPort { get; }
    } // IConnectionListenerService

    /// <summary>
    /// 
    /// </summary>
    internal sealed class AppConnectionListener : AppProxy, IConnectionListenerService
    {
        #region Constants
        /// <summary>
        /// Name of this app proxy.
        /// </summary>
        private const String c_name = "Connection Listener";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Listener for incoming connections.
        /// </summary>
        private TcpListener ConnectionListener { get; set; }

        /// <summary>
        /// Flag indicating whether the connection listener is active.
        /// </summary>
        private bool ListenerActive { get; set; }

        /// <summary>
        /// Port the listener is monitoring.
        /// </summary>
        public int ConnectionPort { get; private set; }

        /// <summary>
        /// Map that keeps track of the currently active proxies.
        /// </summary>
        private Dictionary<TcpClient, AppProxy> ProxyLookup { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        internal AppConnectionListener(ProxyHub hub)
            : base(c_name, hub, 100)
        {
            Hub.ServiceManager.ConnectionListenerService = this;
            ListenerActive = true;
            ConnectionPort = HubConnectionService.ConnectionServicePort + ((int)Hub.UID % 100) + 1;
            ProxyLookup = new Dictionary<TcpClient, AppProxy>();

            // Try and start up the app connection listener on the default port.
            Log("Starting AppConnectionListener Listener.");
            try
            {
                ConnectionListener = new TcpListener(IPAddress.Any, ConnectionPort);
                ConnectionListener.Start();
                ConnectionListener.BeginAcceptTcpClient(DoAcceptTcpClientCallback, ConnectionListener);
            }
            catch (SocketException)
            {
                Log("Default AppConnectionListener port {0} is in use. Trying to find next available port.", ConnectionPort);
                int startingPort = ConnectionPort;

                const int maxRetryAttempts = 20;
                bool portAcquired = false;

                for (int i = startingPort + 1; i < startingPort + maxRetryAttempts; ++i)
                {
                    ConnectionPort++;

                    try
                    {
                        ConnectionListener = new TcpListener(IPAddress.Any, ConnectionPort);
                        ConnectionListener.Start();
                        ConnectionListener.BeginAcceptTcpClient(DoAcceptTcpClientCallback, ConnectionListener);
                        portAcquired = true;
                        break;
                    }
                    catch (SocketException)
                    {
                        Log("Failed to use port {0}.", i);
                    }
                }

                // Check whether we successfully managed to acquire a port.
                if (!portAcquired)
                {
                    Log("Failed to acquire a port after trying {0} ports.", maxRetryAttempts);
                    throw new ApplicationException(String.Format("AppConnectionListener failed to acquire a port after {0} attempts.", maxRetryAttempts));
                }
            }

            State = AppProxy.ProxyState.ReadyIdle;
        }
        #endregion // Constructor(s)

        #region AppProxy Overrides
        /// <summary>
        /// 
        /// </summary>
        public override string GetShortDescription()
        {
            return "ACL@" + GetShortStateDescription();
        }

        /// <summary>
        /// Called to stop the connection listener.
        /// </summary>
        public override void Stop()
        {
            ListenerActive = false;
            ConnectionListener.Stop();

            // Close the connections to any external apps.
            lock (ProxyLookup)
            {
                foreach (TcpClient client in ProxyLookup.Keys)
                {
                    NetworkStream stream = client.GetStream();
                    stream.Close();
                    client.Close();
                }

                ProxyLookup.Clear();
            }

            base.Stop();
        }

        /// <summary>
        /// Called to shutdown the connection listener.
        /// </summary>
        public override void ShutDown()
        {
            Log( "Stopping..." );
            State = ProxyState.Stopping;
        }

        /// <summary>
        /// 
        /// </summary>
        public override void Update()
        {
            if (!IsRunning)
            {
                Stop();
                return;
            }

            // Create a copy to prevent a lock on the map.
            Dictionary<TcpClient, AppProxy> proxyMapCopy = null;
            lock (ProxyLookup)
            {
                proxyMapCopy = new Dictionary<TcpClient, AppProxy>(ProxyLookup);
            }

            // Loop over the tcp client's checking any for data that has been sent.
            foreach (KeyValuePair<TcpClient, AppProxy> pair in proxyMapCopy)
            {
                TcpClient client = pair.Key;
                AppProxy proxy = pair.Value;

                NetworkStream stream = client.GetStream();
                if (stream.CanRead && stream.DataAvailable)
                {
                    JsonData jsonData = JsonNetUtil.GetJsonFromConnection(stream, 10000, LogObj);
                    if (jsonData != null)
                    {
                        // Check whether the request was for the game list.
                        String action = (String)jsonData["action"];
                        Log("Received action: {0}", action);

                        if (action == "disconnect")
                        {
                            if (DisconnectProxy(jsonData, proxy))
                            {
                                JsonData jsonDataToSend = new JsonData();
                                jsonDataToSend["disconnect"] = "ok";
                                String jsonToSend = jsonDataToSend.ToJson();
                                byte[] dataToSend = Encoding.ASCII.GetBytes(jsonToSend);

                                Log("Sending disconnect ACK.");
                                if (stream.CanWrite)
                                {
                                    stream.Write(dataToSend, 0, dataToSend.Length);
                                }

                                lock (ProxyLookup)
                                {
                                    ProxyLookup.Remove(client);
                                }

                                // Close the stream and client as we are done with them.
                                stream.Close();
                                client.Close();
                            }
                        }
                        else
                        {
                            Log("Unsupported action '{0}' encountered.", action);
                        }
                    }
                    else
                    {
                        Log("Unable to retrieve JSON data from connection.");
                    }
                }
            }
        }
        #endregion // AppProxy Overrides

        #region Async Methods
        /// <summary>
        /// 
        /// </summary>
        private void DoAcceptTcpClientCallback(IAsyncResult ar)
        {
            // Get the listener that handles the client request.
            TcpListener listener = (TcpListener)ar.AsyncState;
            Log("DoAccept: {0}", ListenerActive);

            if (ListenerActive)
            {
                bool proxyInitialised = false;
                TcpClient client = listener.EndAcceptTcpClient(ar);

                // Start the listener again.
                listener.BeginAcceptTcpClient(DoAcceptTcpClientCallback, listener);

                // Respond to the clients request
                NetworkStream stream = client.GetStream();

                // Attempt to read in the Json content
                JsonData jsonData = JsonNetUtil.GetJsonFromConnection(stream, 10000, LogObj);
                if (jsonData != null)
                {
                    // Check whether the request was for the game list.
                    String action = (String)jsonData["action"];
                    Log("Received action: {0}", action);

                    if (action == "connect")
                    {
                        AppProxy proxy = SetupNewProxy(jsonData);
                        if (proxy != null)
                        {
                            // Pass back information to the client about the newly created proxy.
                            JsonData jsonDataToSend = new JsonData();
                            jsonDataToSend["connect"] = "ok";
                            jsonDataToSend["name"] = proxy.Name;
                            jsonDataToSend["uid"] = proxy.UID;
                            String jsonToSend = jsonDataToSend.ToJson();
                            byte[] dataToSend = Encoding.ASCII.GetBytes(jsonToSend);

                            // Make sure that awe can write to the network stream.
                            if (stream.CanWrite)
                            {
                                stream.Write(dataToSend, 0, dataToSend.Length);
                            }

                            lock (ProxyLookup)
                            {
                                ProxyLookup.Add(client, proxy);
                            }
                            proxyInitialised = true;
                            Log("Completed setting up new app proxy '{0}' ", proxy.ToString());
                        }
                    }
                    else
                    {
                        Log("Unsupported action '{0}' encountered.", action);
                    }
                }
                else
                {
                    Log("Unable to retrieve JSON data from connection.");
                }

                // If a proxy wasn't initialised, close the stream/client to prevent a resource leak.
                if (!proxyInitialised)
                {
                    stream.Close();
                    client.Close();
                }
            }
        }
        #endregion // Async Methods

        #region Private Methods
        /// <summary>
        /// Sets up a new proxy based on the passed in json data.
        /// </summary>
        private AppProxy SetupNewProxy(LitJson.JsonData jsonData)
        {
            // Extract the relevant data from the json object.
            String connectionName = null;
            int connectionPort = 0;
            IPAddress connectionAddress = null;

            try
            {
                connectionName = (String)jsonData["name"];
                connectionPort = (int)jsonData["port"];
                connectionAddress = IPAddress.Parse((String)jsonData["hostname"]);
            }
            catch ( Exception ex )
            {
                String msg = String.Format("Unable to setup up new RAG app proxy due to bad data in the json packet.\n\n" +
                    "{0}\n{1}", ex.Message, ex.StackTrace);
                rageMessageBox.ShowExclamation(msg, "Error");
                return null;
            }

            // Try and start the app proxy.
            AppProxy proxy = null;
            try
            {
                if (connectionName == "Rag Proxy")
                {
                    proxy = new RagProxy(Hub, connectionAddress, connectionPort);
                    Hub.RunAppProxy(proxy);
                }
                else
                {
                    throw new Exception("Unknown app: " + connectionName);
                }
            }
            catch (System.Exception ex)
            {
                String msg = String.Format("Exception caught creating app proxy for {0}.\n\n{1}\n{2}",
                    ex.Message, ex.StackTrace);
                rageMessageBox.ShowExclamation(msg, "Error");
            }

            return proxy;
        }

        /// <summary>
        /// Disconnect a particular proxy.
        /// </summary>
        private bool DisconnectProxy(LitJson.JsonData jsonData, AppProxy proxy)
        {
            String connectionName = null;
            int uid = 0;

            try
            {
                connectionName = (String)jsonData["name"];
                uid = (int)jsonData["uid"];
            }
            catch ( Exception ex )
            {
                String msg = String.Format("Unable to setup up new RAG app proxy due to bad data in the json packet.\n\n" +
                    "{0}\n{1}", ex.Message, ex.StackTrace);
                rageMessageBox.ShowExclamation(msg, "Error");
                return false;
            }

            // Make sure that the uid/connection name match the proxy.
            if (proxy.Name == connectionName && proxy.UID == uid)
            {
                Log("Shutting down '{0}'[{1}] ", connectionName, uid);
                Hub.ShutDownProxy(proxy);
                return true;
            }
            else
            {
                Log("Attempt to shutdown incorrect proxy encountered.");
                return false;
            }
        }
        #endregion // Private Methods
    } // AppConnectionListener
}
