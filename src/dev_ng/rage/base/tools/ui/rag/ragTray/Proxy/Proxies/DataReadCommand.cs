﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace ragTray.Proxy.Proxies
{
    public class DataReadCommand
    {
        public static char[] bkDataIdentifier = { 'b', 'k', 'd', 'r' };
        public const int HeaderOffset = 4;
        private static readonly ASCIIEncoding c_encoder = new ASCIIEncoding();

        public static bool CheckHeader(char[] header)
        {
            return (header[0] == bkDataIdentifier[0] && header[1] == bkDataIdentifier[1]
                        && header[2] == bkDataIdentifier[2] && header[3] == bkDataIdentifier[3]);
        }

        public DataReadCommand(byte[] buffer, int bufferOffset, Stream stream)
        {
            WidgetId = BitConverter.ToInt32(buffer, bufferOffset + HeaderOffset);
            DataSize = HeaderOffset + sizeof(int);

            Stream = stream;
        }

        public int WidgetId;
        public int DataSize;
        public Stream Stream;
    }
}
