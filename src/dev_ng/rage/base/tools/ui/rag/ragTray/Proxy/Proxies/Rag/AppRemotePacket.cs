﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ragCore;
using System.Threading;
using RSG.Base.Logging;

namespace ragTray
{


    /// <summary>
    /// Summary description for AppRemotePacket.
    /// </summary>
    public class AppRemotePacket : RemotePacket
    {
        public enum EnumPacketType
        {
            HANDSHAKE,
            NUM_APPS,
            APP_NAME,
            APP_VISIBLE_NAME,
            APP_ARGS,
            PIPE_NAME_BANK,
            PIPE_NAME_OUTPUT,
            PIPE_NAME_EVENTS,
            END_OUTPUT,
            WINDOW_HANDLE,
            PLATFORM_INFO,
            PS3_TARGET_ADDRESS,
            BUILD_CONFIG
        };

        public EnumPacketType AppCommand
        {
            get
            {
                return (EnumPacketType)m_Header.m_Command;
            }
        }

        public static event EventHandler HandShakePacketReceived;
        public static bool ReadPackets( INamedPipe pipe, TrayApplicationManager appManager, out bool timedOut, ILog log, String logCtx )
        {
            AppRemotePacket p = new AppRemotePacket( pipe );

            timedOut = false;
            int retries = 40;   // 4 seconds

            bool loop = true;
            while ( loop )
            {
                if ( !pipe.HasData() )
                {
                    --retries;
                    if ( retries == 0 )
                    {
                        timedOut = true;
                        return false;
                    }

                    Thread.Sleep( 100 );
                }
                else
                {
                    retries = 40;   // reset
                    while ( pipe.HasData() )
                    {
                        p.ReceiveHeader( pipe );
                        if ( p.m_Header.m_Length > 0 )
                        {
                            p.ReceiveStorage( pipe );
                        }

                        if ( HandShakePacketReceived != null )
                        {
                            HandShakePacketReceived( p, null );
                        }

                        // see if we're done parsing:
                        if ( p.AppCommand == EnumPacketType.END_OUTPUT )
                        {
                            log.MessageCtx(logCtx, "Finished processing handshake packets.");
                            loop = false;
                            break;
                        }
                        else if ( !appManager.PacketHandler( p, log, logCtx ) )
                        {
                            log.MessageCtx(logCtx, "Failed to process handshake packet {0}", p.AppCommand);
                            return false;
                        }
                        else if ( p.AppCommand == EnumPacketType.HANDSHAKE )
                        {
                            // I think we can only fall through to here if the RAG versions don't match in the handshake packets.
                            loop = false;
                            break;
                        }
                    }
                }
            }

            return true;
        }

        public AppRemotePacket( INamedPipe pipe )
            : base( pipe )
        {
        }

        public AppRemotePacket( INamedPipe pipe, AppRemotePacket src )
            : base( pipe )
        {
            ReadFromBuffer( src.CopyToBuffer() );
        }
    }
}
