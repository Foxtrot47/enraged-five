﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ragTray
{
    public partial class ForwardDataToRagForm : Form
    {
        private RagProxy m_RagProxy;
        private int m_AnimT = 0;

        public ForwardDataToRagForm(RagProxy proxy)
        {
            m_RagProxy = proxy;

            InitializeComponent();

            // Remove the [X] Close button
            ControlBox = false;

            // Make sure the buttons don't have focus.
            headerLabel.Focus();

            // To ensure that if there's more than one window, they don't overlap.
            ++RagTrayIcon.sm_NumOpenFormInstances;
            FormClosed += new FormClosedEventHandler(CloseConnectionToGameForm_FormClosed);
        }

        /// <summary>
        /// Called to increment the progress shown on this form.
        /// </summary>
        /// <param name="current"></param>
        /// <param name="upperBound"></param>
        public void ReportProgress(int current, int upperBound)
        {
            progressBar.Maximum = upperBound;
            progressBar.Value = current;
        }

        /// <summary>
        /// Called when we've finished forwarding the data.
        /// </summary>
        public void Complete()
        {
            // Make sure progress is complete
            progressBar.Value = progressBar.Maximum;

            // Start the fade out timer.
            animTimer.Start();
            headerLabel.BackColor = Color.GreenYellow;
            headerLabel.Text = "Completed Successfully";
        }

        private void animTimer_Tick(object sender, EventArgs e)
        {
            m_AnimT += animTimer.Interval;
            if (m_AnimT > 250)
            {
                this.Opacity *= 0.9;
                if (this.Opacity < 0.05)
                {
                    Close();
                }
            }
        }

        private void ForwardDataToRagForm_Load(object sender, EventArgs e)
        {
            try
            {
                TaskbarPosition taskbarPosition = new TaskbarPosition();

                switch (taskbarPosition.Side)
                {
                    case TaskbarSide.Bottom:
                        Location = new Point(
                            taskbarPosition.Size.Width - taskbarPosition.Location.X - Width,
                            taskbarPosition.Location.Y - Height * RagTrayIcon.sm_NumOpenFormInstances);
                        break;
                    case TaskbarSide.Top:
                        Location = new Point(
                            taskbarPosition.Size.Width - taskbarPosition.Location.X - Width,
                            taskbarPosition.Location.Y + taskbarPosition.Size.Height * RagTrayIcon.sm_NumOpenFormInstances);
                        break;
                    case TaskbarSide.Left:
                        Location = new Point(
                            taskbarPosition.Location.X + taskbarPosition.Size.Width,
                            taskbarPosition.Size.Height - taskbarPosition.Location.Y - Height * RagTrayIcon.sm_NumOpenFormInstances);
                        break;
                    case TaskbarSide.Right:
                        Location = new Point(
                            taskbarPosition.Location.X - Width,
                            taskbarPosition.Size.Height - taskbarPosition.Location.Y - Height * RagTrayIcon.sm_NumOpenFormInstances);
                        break;
                }
            }
            catch (Exception) { }// If this fails then we're ok to create the form as usual


            Activate();
            BringToFront();
        }

        private void CloseConnectionToGameForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            --RagTrayIcon.sm_NumOpenFormInstances;
        }

    }
}
