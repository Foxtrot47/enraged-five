﻿namespace ragTray
{
    partial class TrayIconHoverForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && (components != null) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.timer1 = new System.Windows.Forms.Timer( this.components );
            this.mouseTimer = new System.Windows.Forms.Timer( this.components );
            this.SuspendLayout();
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 5000;
            this.timer1.Tick += new System.EventHandler( this.timer1_Tick );
            // 
            // mouseTimer
            // 
            this.mouseTimer.Enabled = true;
            this.mouseTimer.Interval = 10;
            this.mouseTimer.Tick += new System.EventHandler( this.mouseTimer_Tick );
            // 
            // TrayIconHoverForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.HotTrack;
            this.ClientSize = new System.Drawing.Size( 284, 262 );
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "TrayIconHoverForm";
            this.Opacity = 0.75D;
            this.ShowInTaskbar = false;
            this.Text = "TrayIconHoverForm";
            this.TopMost = true;
            this.VisibleChanged += new System.EventHandler( this.TrayIconHoverForm_VisibleChanged );
            this.ResumeLayout( false );

        }

        #endregion

        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Timer mouseTimer;
    }
}