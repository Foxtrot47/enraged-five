﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace ragTray
{
    public class StreamNameConstants
    {
        public static readonly string BankCompressed = "bankcompressed";
        public static readonly string BankRaw = "bankraw";
    }


    /// <summary>
    /// This class is used for communication between proxies.
    /// 
    /// </summary>
    public class InterProxyStream
    {
        public string Name { get; set; }
        private List<AppProxy> m_Proxies;
        public List<AppProxy> Proxies
        {
            get
            {
                lock ( m_Proxies )
                {
                    return new List<AppProxy>( m_Proxies );
                }
            }
        }

        public InterProxyStream( string name )
        {
            Name = name;
            m_Proxies = new List<AppProxy>();
        }

        public void AddProxy( AppProxy proxy )
        {
            lock ( m_Proxies )
            {
                m_Proxies.Add( proxy );
            }
        }

        public void RemoveProxy( AppProxy proxy )
        {
            lock ( m_Proxies )
            {
                m_Proxies.Remove( proxy );
            }
        }

        public void Write( AppProxy sender, byte[] buffer )
        {
            Write( sender, buffer, 0, buffer.Length );
        }

        public void Write( AppProxy sender, byte[] buffer, int offset, int count )
        {
            // Work on a copy of the list instead to prevent a locking bug. GameProxy.Write 
            // performs a lock of its own, and as a result we can end up in a deadlock if
            // m_Proxies is locked.
            List<AppProxy> proxyList;
            lock ( m_Proxies )
            {
                proxyList = new List<AppProxy>( m_Proxies );
            }

            foreach ( var proxy in proxyList )
            {
                if ( sender != proxy )
                {
                    proxy.Write( Name, buffer, offset, count );
                }
            }
        }
    }
}
