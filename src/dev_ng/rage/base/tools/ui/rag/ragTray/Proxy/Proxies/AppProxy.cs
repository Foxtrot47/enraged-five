﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using RSG.Base.Logging;

namespace ragTray
{
    /// <summary>
    /// Base class for the various proxies.
    /// </summary>
    public abstract class AppProxy
    {
        #region Constants
        /// <summary>
        /// Update every 10ms by default.
        /// </summary>
        private const uint c_defaultIdleUpdateFrequency = 10;
        #endregion // Constants

        #region Enums
        /// <summary>
        /// State that the proxy is in.
        /// </summary>
        public enum ProxyState
        {
            Initialising,
            ReadyIdle,
            ReadyWorking,
            Stopping,
            Stopped
        } // ProxyState
        #endregion // Enums

        /// <summary>
        /// Name for the proxy.
        /// </summary>
        public String Name { get; private set; }

        /// <summary>
        /// Unique Id for this proxy.
        /// </summary>
        public int UID { get; private set; }
        
        /// <summary>
        /// Parent hub that this app proxy is a part of.
        /// </summary>
        public ProxyHub Hub { get; private set; }

        /// <summary>
        /// Update frequency when the proxy is idle.
        /// </summary>
        public uint IdleUpdateFrequency { get; private set; }

        /// <summary>
        /// State that the proxy is in.
        /// </summary>
        private volatile ProxyState m_State = ProxyState.Initialising;
        protected readonly object m_StateLock = new object();

        protected ServiceManager m_ServiceManager;
        protected Dictionary<string, InterProxyStream> m_HubStreams = new Dictionary<string, InterProxyStream>();

        /// <summary>
        /// Time when the proxy started to idle.
        /// </summary>
        private DateTime m_IdleStart = DateTime.Now;

        /// <summary>
        /// Reference to the log object that this proxy logs to.
        /// </summary>
        private readonly ILog _log;

        #region Static Member Data
        /// <summary>
        /// Static count that gets incremented with each newly created proxy.
        /// </summary>
        private static int s_UID = 0;

        /// <summary>
        /// Amount of time until the proxy is considered idle.
        /// </summary>
        private static readonly TimeSpan c_MaxIdleTime = TimeSpan.FromSeconds(1.0);
        #endregion // Static member data.

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        protected AppProxy(String name, ProxyHub hub)
            : this(name, hub, c_defaultIdleUpdateFrequency)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        protected AppProxy(String name, ProxyHub hub, uint idleUpdateFreq)
        {
            Name = name;
            Hub = hub;
            UID = ++s_UID;
            IdleUpdateFrequency = idleUpdateFreq;
            m_ServiceManager = hub.ServiceManager;
            _log = LogFactory.ApplicationLog;
        }
        #endregion // Constructor(s)

        #region Events
        public event EventHandler AppProxyStopped;

        protected void OnAppProxyStopped()
        {
            if ( AppProxyStopped != null )
            {
                AppProxyStopped( this, null );
            }
        }
        #endregion

        public abstract void Update();
        public virtual void Write(string streamName, byte[] data, int offset, int count) { }

        protected void SubscribeToStream( string name, ServiceManager serviceManager )
        {
            if ( !serviceManager.HubStreams.ContainsKey( name ) )
            {
                serviceManager.HubStreams[name] = new InterProxyStream( name );
            }

            m_HubStreams[name] = serviceManager.HubStreams[name];
            m_HubStreams[name].AddProxy( this );
        }

        #region State Methods
        public virtual void ShutDown()
        {
            Log( "Stopping..." );
            if ( State != ProxyState.Stopped )
            {
                State = ProxyState.Stopping;
            }
        }

        public virtual void Stop()
        {
            Log("Stopped.");
            State = ProxyState.Stopped;
            OnAppProxyStopped();
        }

        /// <summary>
        /// Proxy's current state.
        /// </summary>
        public ProxyState State
        {
            protected set
            {
                lock (m_StateLock)
                {
                    if (m_State != value)
                    {
                        //Log("State changing from {0} to {1}.", m_State, value);

                        // Check that the next state is valid
                        if (m_State == ProxyState.ReadyIdle || m_State == ProxyState.ReadyWorking)
                        {
                            Debug.Assert(value == ProxyState.ReadyIdle || value == ProxyState.ReadyWorking || value == ProxyState.Stopping);
                        }
                        else if (m_State == ProxyState.Stopping)
                        {
                            Debug.Assert(value == ProxyState.Stopping || value == ProxyState.Stopped);
                        }

                        if (value == ProxyState.Stopping)
                        {
                            Log("State => Stopping");
                        }

                        m_State = value;
                    }
                }
            }
            get
            {
                lock (m_StateLock)
                {
                    return m_State;
                }
            }
        }

        /// <summary>
        /// Indicates whether the app proxy is currently running.
        /// </summary>
        public bool IsRunning
        {
            get
            {
                lock (m_StateLock)
                {
                    return (m_State == ProxyState.ReadyIdle || m_State == ProxyState.ReadyWorking);
                }
            }
        }

        /// <summary>
        /// Indicates whether the proxy is doing any work. This is used by the proxy hub to determine how often it should be updated.
        /// </summary>
        public bool IsWorking
        {
            get
            {
                lock (m_StateLock)
                {
                    if (!IsRunning)
                    {
                        return false;
                    }

                    // App proxy is working, so update the idle start time and return.
                    if (m_State == ProxyState.ReadyWorking)
                    {
                        m_IdleStart = DateTime.Now;
                        return true;
                    }

                    // App proxy isn't actively doing any work, check to see if the idle time has elapsed.
                    return ((DateTime.Now - m_IdleStart) < c_MaxIdleTime);
                }
            }
        }
        #endregion

        /// <summary>
        /// Wrapper for logging messages which includes the app proxy name.
        /// </summary>
        protected void Log( string msg, params object[] args )
        {
            _log.MessageCtx(LogCtx, msg, args);
        }

        protected ILog LogObj
        {
            get { return _log; }
        }

        protected string LogCtx
        {
            get
            {
                return String.Format("Hub {0} - {1}#{2}", Hub.UID, Name, UID);
            }
        }

        public virtual void LogState()
        {
            Log( State.ToString() );
        }

        public abstract string GetShortDescription();

        protected string GetShortStateDescription()
        {
            switch (State)
            {
                case ProxyState.Initialising: return "Initialising";
                case ProxyState.ReadyIdle: return "Idle";
                case ProxyState.ReadyWorking: return "Working";
                case ProxyState.Stopping: return "Stopping";
                default: return "Stopped";
            }
        }

        public override string ToString()
        {
            return string.Format("{0}[{1}]@h={2}", Name, UID, Hub.UID);
        }
    }

}
