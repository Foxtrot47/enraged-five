﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading;
using System.Net.Sockets;
using System.Diagnostics;
using ragCore;
using System.Net;
using RSG.Base.Forms;
using System.Threading.Tasks;
using ragTray.Proxy.Proxies;
using RSG.Base.Logging;

namespace ragTray
{
    /*
     * 
     * 
     * 
     * 
     * 
     * Note: See this: https://devstar.rockstargames.com/wiki/index.php/Proxy_Dev_Docs
     * 
     * 
     * 
     * 
     * */

    public class HubManager
    {
        #region Static Members
        /// <summary>
        /// Counter for unique IDs.
        /// </summary>
        private static uint s_nextProxyUID = 0;

        /// <summary>
        /// Log message context.
        /// </summary>
        private const String LogCtx = "Hub Manager";
        #endregion // Static Members

        #region Member Data
        /// <summary>
        /// Flag indicating that the proxy is shutting down.
        /// </summary>
        private bool m_Quitting = false;

        /// <summary>
        /// Service for providing notifications to the user.
        /// </summary>
        private IUINotifyService m_NotifyService;

        /// <summary>
        /// ?
        /// </summary>
        private IPAddress m_Address;

        /// <summary>
        /// 
        /// </summary>
        private object m_pendingHubSyncContext = new object();

        /// <summary>
        /// List of all the hubs that the manager is currently tracking.
        /// </summary>
        private IList<ProxyHub> m_hubs;
        #endregion // Member Data

        #region Properties
        /// <summary>
        /// A temporary hub that has been created to allow new game connection to immediately connect to.
        /// </summary>
        public ProxyHub PendingHub { get; private set; }

        /// <summary>
        /// The default hub that rag cmd/remote console requests get sent to.
        /// </summary>
        public ProxyHub DefaultHub { get; set; }

        /// <summary>
        /// List of hubs that currently have valid game connections.
        /// </summary>
        public IEnumerable<ProxyHub> ActiveHubs
        {
            get
            {
                lock (m_hubs)
                {
                    return new List<ProxyHub>(m_hubs);
                }
            }
        }

        /// <summary>
        /// Reference to the log object to make use of.
        /// </summary>
        private readonly ILog _log;
        #endregion // Properties

        #region Events
        /// <summary>
        /// Event that gets fired when all hubs have shutdown.
        /// </summary>
        public event EventHandler AllHubsShutDown;

        /// <summary>
        /// Event that gets fired when a new hub has started.
        /// </summary>
        public event ProxyHubEventHandler HubStarted;

        /// <summary>
        /// Event that gets fired once a hub has been initialised.
        /// This is fired after a game has connected to the GameProxy.
        /// </summary>
        public event ProxyHubEventHandler HubInitialised;

        /// <summary>
        /// Event that gets fired when a hub is being shutdown.
        /// </summary>
        public event ProxyHubEventHandler HubShutdown;
        #endregion

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public HubManager( IUINotifyService notifyService, IPAddress address, ILog log)
        {
            _log = log;
            m_hubs = new List<ProxyHub>();
            m_NotifyService = notifyService;
            m_Address = address;

            CreateNewPendingHub();
        }
        #endregion // Constructor(s)

        #region Public Methods
        /// <summary>
        /// Shuts down the hub manager.
        /// </summary>
        public void ShutDown()
        {
            _log.MessageCtx(LogCtx, "ShutDown");
            m_Quitting = true;
            PendingHub.ShutDownAllProxies();

            lock (m_hubs)
            {
                foreach (ProxyHub hub in m_hubs)
                {
                    hub.LogState();
                    hub.ShutDownAllProxies();
                }
            }

            // Make sure the game proxy is properly cleaned up.
            GameProxy.ClearStaticsAndPlugins();

            // Give all proxies roughly 10 seconds to exit cleanly.
            for (int i = 0; i < 100; ++i)
            {
                bool done = true;
                lock (m_hubs)
                {
                    done = !m_hubs.Any(item => item.AppProxyCount != 0);
                }

                if (done)
                {
                    _log.MessageCtx(LogCtx, "All app proxies shut down in time.");
                    break;
                }

                Thread.Sleep(100);
            }

            lock (m_hubs)
            {
                // Log information about proxy hubs that still have active app proxies.
                foreach (ProxyHub hub in m_hubs.Where(item => item.AppProxyCount > 0))
                {
                    _log.MessageCtx(LogCtx, "Some app proxies did not exit, will get killed off.");
                    hub.LogState();
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void LogState()
        {
            lock (m_hubs)
            {
                foreach (var hub in m_hubs)
                {
                    hub.LogState();
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="hubName"></param>
        public void ResetConsole(string hubName)
        {
            lock (m_hubs)
            {
                foreach (ProxyHub hub in m_hubs)
                {
                    hub.ServiceManager.GameService.ResetConsole();
                }
            }
        }
        #endregion // Public Methods

        #region Private Methods
        /// <summary>
        /// Method called to start a new hub.
        /// </summary>
        private void CreateNewPendingHub()
        {
            _log.MessageCtx(LogCtx, "Starting proxy hub #{0}.", s_nextProxyUID);
            lock (m_pendingHubSyncContext)
            {
                // Create a new hub.
                ProxyHub hub = new ProxyHub(s_nextProxyUID++);
                hub.AllAppsShutDown += new EventHandler(ProxyHub_AllAppsShutDown);
                hub.ServiceManager.NotifyService = m_NotifyService;

                // We create the game proxy so that we can immediately connect it to the game.
                GameProxy gameclient = new GameProxy(hub, m_Address);
                gameclient.GameConnected += Gameclient_GameConnected;
                gameclient.GameDisconnected += Gameclient_GameDisconnected;
                hub.RunAppProxy(gameclient);

                PendingHub = hub;

                // Fire off an event informing that a new hub has started.
                if (HubStarted != null)
                {
                    HubStarted(this, new ProxyHubEventArgs(hub));
                }
            }
        }

        /// <summary>
        /// Callback for when all hubs associated with a particular proxyhub have shutdown.
        /// </summary>
        private void ProxyHub_AllAppsShutDown(object sender, EventArgs e)
        {
            ProxyHub hub = sender as ProxyHub;

            lock (m_hubs)
            {
                m_hubs.Remove(hub);

                // Update the default hub.
                if (hub == DefaultHub)
                {
                    DefaultHub = m_hubs.FirstOrDefault();
                }

                // Check whether we still have any hubs.
                if (m_Quitting && m_hubs.Count == 0 && AllHubsShutDown != null)
                {
                    _log.MessageCtx(LogCtx, "No more hubs.  Sending shutdown event.");
                    AllHubsShutDown(this, null);
                }    
            }

            // Make sure the hub is properly disposed of.
            hub.Dispose();
        }

        /// <summary>
        /// 
        /// </summary>
        private void Gameclient_GameConnected(object sender, GameConnectedEventArgs e)
        {
            _log.MessageCtx(LogCtx, "Game Connected.  Quitting: {0}", m_Quitting);

            // When the game connects, we should start up a console proxy. It can't be done
            // before the game is connected so this is why we do it here.
            // Also start a new hub for the next game to come along and connect.
            if (!m_Quitting)
            {
                GameProxy gp = sender as GameProxy;

                // Create a new app connection listener for the hub.
                AppConnectionListener connectionListener = new AppConnectionListener(gp.Hub);
                if (!gp.Hub.RunAppProxy(connectionListener))
                {
                    connectionListener.ShutDown();
                    return;
                }

                // Create a new console proxy for the hub.
                ConsoleProxy consoleClient = new ConsoleProxy(gp.Hub, gp.ConsolePipe);
                if (!gp.Hub.RunAppProxy(consoleClient))
                {
                    consoleClient.ShutDown();
                    return;
                }

                // Create a new widget service proxy.
                ServicesProxy serviceProxy = new ServicesProxy(gp.Hub);
                if (!gp.Hub.RunAppProxy(serviceProxy))
                {
                    serviceProxy.ShutDown();
                    return;
                }

                if (DefaultHub == null)
                {
                    DefaultHub = gp.Hub;
                }

                if (HubInitialised != null)
                {
                    HubInitialised(this, new ProxyHubEventArgs(gp.Hub));
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void Gameclient_GameDisconnected(object sender, GameConnectedEventArgs args)
        {
            _log.MessageCtx(LogCtx, "Game Disconnected.  Quitting: {0}", m_Quitting);

            if (!m_Quitting)
            {
                GameProxy gp = sender as GameProxy;
                gp.Hub.ShutDownAllProxies();

                if (HubShutdown != null)
                {
                    HubShutdown(this, new ProxyHubEventArgs(gp.Hub));
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pipe"></param>
        internal void AttachPipeToGameProxy(NamedPipeSocket pipe, DateTime connectedAt)
        {
            lock (m_pendingHubSyncContext)
            {
                PendingHub.ServiceManager.GameService.InitialiseFromPipe(pipe, connectedAt);

                lock (m_hubs)
                {
                    m_hubs.Add(PendingHub);
                }

                // Pending hub is no longer free so create a new one.
                PendingHub = null;
                CreateNewPendingHub();
            }
        }
        #endregion // Private Methods
    } // HubManager

    /// <summary>
    /// Delegate for use when a new hub is started.
    /// </summary>
    public delegate void ProxyHubEventHandler(object sender, ProxyHubEventArgs args);

    /// <summary>
    /// Event arguments that get sent when a new hub has been started.
    /// </summary>
    public class ProxyHubEventArgs : EventArgs
    {
        private readonly ProxyHub _hub;

        public ProxyHubEventArgs(ProxyHub hub)
        {
            _hub = hub;
        }

        public ProxyHub Hub
        {
            get { return _hub; }
        }
    } // HubStartedEventArgs
}
