﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ragTray
{
    public class NetworkData
    {
        public byte[] Data;
        public int Offset;
        public int Count;

        public NetworkData( byte[] data, int count )
        {
            Data = data;
            Offset = 0;
            Count = count;
        }

        public NetworkData( byte[] data, int offset, int count )
        {
            Data = data;
            Offset = offset;
            Count = count;
        }
    }
}
