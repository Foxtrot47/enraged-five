using System;
using System.Diagnostics;
namespace ragTray
{
    partial class RagTrayIcon
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            _log.MessageCtx(LogCtx, "Dispose(" + disposing.ToString() + ")");
            _log.MessageCtx(LogCtx, "DEBUG printing callstack:");
            StackTrace stackTrace = new StackTrace();           // get call stack
            StackFrame[] stackFrames = stackTrace.GetFrames();  // get method calls (frames)

            // write call stack method names
            foreach ( StackFrame stackFrame in stackFrames )
            {
                _log.MessageCtx(LogCtx, stackFrame.GetMethod().Name);   // write method name
            }

            if ( disposing && (components != null) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RagTrayIcon));
            this.notifyIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.notifyIconContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.showBalloons_toolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.AutoLaunchRagtoolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.AutoLaunchRagInterfaceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.AutoVerifyGameLinkMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cacheNetworkPacketsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.launchRagToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.launchRagInterfaceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.resetConsoleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.launchGameToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.setDefaultConnectionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.defaultConnectionContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.dummyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.viewLogToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openLogFolderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.notifyIconContextMenuStrip.SuspendLayout();
            this.defaultConnectionContextMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // notifyIcon
            // 
            this.notifyIcon.ContextMenuStrip = this.notifyIconContextMenuStrip;
            this.notifyIcon.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon.Icon")));
            this.notifyIcon.MouseMove += new System.Windows.Forms.MouseEventHandler(this.notifyIcon_MouseMove);
            // 
            // notifyIconContextMenuStrip
            // 
            this.notifyIconContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.showBalloons_toolStripMenuItem,
            this.AutoLaunchRagtoolStripMenuItem,
            this.AutoLaunchRagInterfaceToolStripMenuItem,
            this.AutoVerifyGameLinkMenuItem,
            this.cacheNetworkPacketsToolStripMenuItem,
            this.launchRagToolStripMenuItem,
            this.launchRagInterfaceToolStripMenuItem,
            this.toolStripSeparator3,
            this.resetConsoleToolStripMenuItem,
            this.toolStripMenuItem1,
            this.toolStripMenuItem3,
            this.toolStripMenuItem2,
            this.launchGameToolStripMenuItem,
            this.toolStripSeparator4,
            this.setDefaultConnectionToolStripMenuItem,
            this.toolStripSeparator1,
            this.viewLogToolStripMenuItem,
            this.openLogFolderToolStripMenuItem,
            this.toolStripSeparator2,
            this.helpToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.notifyIconContextMenuStrip.Name = "contextMenuStrip";
            this.notifyIconContextMenuStrip.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.notifyIconContextMenuStrip.ShowCheckMargin = true;
            this.notifyIconContextMenuStrip.ShowImageMargin = false;
            this.notifyIconContextMenuStrip.Size = new System.Drawing.Size(385, 424);
            // 
            // showBalloons_toolStripMenuItem
            // 
            this.showBalloons_toolStripMenuItem.Checked = true;
            this.showBalloons_toolStripMenuItem.CheckOnClick = true;
            this.showBalloons_toolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.showBalloons_toolStripMenuItem.Name = "showBalloons_toolStripMenuItem";
            this.showBalloons_toolStripMenuItem.Size = new System.Drawing.Size(384, 22);
            this.showBalloons_toolStripMenuItem.Text = "Show balloon notifications";
            this.showBalloons_toolStripMenuItem.CheckedChanged += new System.EventHandler(this.showBalloons_toolStripMenuItem_CheckedChanged);
            // 
            // AutoLaunchRagtoolStripMenuItem
            // 
            this.AutoLaunchRagtoolStripMenuItem.Checked = true;
            this.AutoLaunchRagtoolStripMenuItem.CheckOnClick = true;
            this.AutoLaunchRagtoolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.AutoLaunchRagtoolStripMenuItem.Name = "AutoLaunchRagtoolStripMenuItem";
            this.AutoLaunchRagtoolStripMenuItem.Size = new System.Drawing.Size(384, 22);
            this.AutoLaunchRagtoolStripMenuItem.Text = "Automatically launch Rag UI  on game start";
            this.AutoLaunchRagtoolStripMenuItem.ToolTipText = "Ensures that, when a game connection is acquired, Rag is automatically launched.";
            this.AutoLaunchRagtoolStripMenuItem.CheckedChanged += new System.EventHandler(this.AutoLaunchRagtoolStripMenuItem_CheckedChanged);
            // 
            // AutoLaunchRagInterfaceToolStripMenuItem
            // 
            this.AutoLaunchRagInterfaceToolStripMenuItem.CheckOnClick = true;
            this.AutoLaunchRagInterfaceToolStripMenuItem.Name = "AutoLaunchRagInterfaceToolStripMenuItem";
            this.AutoLaunchRagInterfaceToolStripMenuItem.Size = new System.Drawing.Size(384, 22);
            this.AutoLaunchRagInterfaceToolStripMenuItem.Text = "Automatically launch new Rag Interface on game start";
            this.AutoLaunchRagInterfaceToolStripMenuItem.ToolTipText = "Ensures that, when a game connection is acquired, the new Rag Interface is automa" +
    "tically launched.";
            this.AutoLaunchRagInterfaceToolStripMenuItem.CheckedChanged += new System.EventHandler(this.AutoLaunchRagInterfaceToolStripMenuItem_CheckedChanged);
            // 
            // AutoVerifyGameLinkMenuItem
            // 
            this.AutoVerifyGameLinkMenuItem.Checked = true;
            this.AutoVerifyGameLinkMenuItem.CheckOnClick = true;
            this.AutoVerifyGameLinkMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.AutoVerifyGameLinkMenuItem.Name = "AutoVerifyGameLinkMenuItem";
            this.AutoVerifyGameLinkMenuItem.Size = new System.Drawing.Size(384, 22);
            this.AutoVerifyGameLinkMenuItem.Text = "Automatically verify link to game";
            this.AutoVerifyGameLinkMenuItem.CheckedChanged += new System.EventHandler(this.AutoVerifyGameLinkMenuItem_Click);
            // 
            // cacheNetworkPacketsToolStripMenuItem
            // 
            this.cacheNetworkPacketsToolStripMenuItem.Checked = true;
            this.cacheNetworkPacketsToolStripMenuItem.CheckOnClick = true;
            this.cacheNetworkPacketsToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cacheNetworkPacketsToolStripMenuItem.Name = "cacheNetworkPacketsToolStripMenuItem";
            this.cacheNetworkPacketsToolStripMenuItem.Size = new System.Drawing.Size(384, 22);
            this.cacheNetworkPacketsToolStripMenuItem.Text = "Cache Network Packets";
            this.cacheNetworkPacketsToolStripMenuItem.CheckedChanged += new System.EventHandler(this.cacheNetworkPacketsToolStripMenuItem_Click);
            // 
            // launchRagToolStripMenuItem
            // 
            this.launchRagToolStripMenuItem.Name = "launchRagToolStripMenuItem";
            this.launchRagToolStripMenuItem.Size = new System.Drawing.Size(384, 22);
            this.launchRagToolStripMenuItem.Text = "Launch Rag UI";
            this.launchRagToolStripMenuItem.Click += new System.EventHandler(this.launchRagToolStripMenuItem_Click);
            // 
            // launchRagInterfaceToolStripMenuItem
            // 
            this.launchRagInterfaceToolStripMenuItem.Name = "launchRagInterfaceToolStripMenuItem";
            this.launchRagInterfaceToolStripMenuItem.Size = new System.Drawing.Size(384, 22);
            this.launchRagInterfaceToolStripMenuItem.Text = "Launch Rag Interface";
            this.launchRagInterfaceToolStripMenuItem.Click += new System.EventHandler(this.launchRagInterfaceToolStripMenuItem_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(381, 6);
            // 
            // resetConsoleToolStripMenuItem
            // 
            this.resetConsoleToolStripMenuItem.Name = "resetConsoleToolStripMenuItem";
            this.resetConsoleToolStripMenuItem.Size = new System.Drawing.Size(384, 22);
            this.resetConsoleToolStripMenuItem.Text = "Kill game";
            this.resetConsoleToolStripMenuItem.Click += new System.EventHandler(this.resetConsoleToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(384, 22);
            this.toolStripMenuItem1.Tag = "X:\\gta5\\build\\dev\\game_xenon_beta.bat";
            this.toolStripMenuItem1.Text = "Launch X:\\gta5\\build\\dev\\game_xenon_beta.bat";
            this.toolStripMenuItem1.Click += new System.EventHandler(this.launchGameToolStripMenuItem_Click);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(384, 22);
            this.toolStripMenuItem3.Tag = "x:\\gta5\\build\\dev\\game_xenon_bankrelease.bat";
            this.toolStripMenuItem3.Text = "Launch X:\\gta5\\build\\dev\\game_xenon_bankrelease.bat";
            this.toolStripMenuItem3.Click += new System.EventHandler(this.launchGameToolStripMenuItem_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(384, 22);
            this.toolStripMenuItem2.Tag = "X:\\gta5\\build\\dev\\game_psn_bankrelease_SNC.bat";
            this.toolStripMenuItem2.Text = "Launch X:\\gta5\\build\\dev\\game_psn_bankrelease_SNC.bat";
            this.toolStripMenuItem2.Click += new System.EventHandler(this.launchGameToolStripMenuItem_Click);
            // 
            // launchGameToolStripMenuItem
            // 
            this.launchGameToolStripMenuItem.Name = "launchGameToolStripMenuItem";
            this.launchGameToolStripMenuItem.Size = new System.Drawing.Size(384, 22);
            this.launchGameToolStripMenuItem.Tag = "X:\\gta5\\build\\dev\\game_psn_beta_SNC.bat";
            this.launchGameToolStripMenuItem.Text = "Launch X:\\gta5\\build\\dev\\game_psn_beta_SNC.bat";
            this.launchGameToolStripMenuItem.Click += new System.EventHandler(this.launchGameToolStripMenuItem_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(381, 6);
            // 
            // setDefaultConnectionToolStripMenuItem
            // 
            this.setDefaultConnectionToolStripMenuItem.DropDown = this.defaultConnectionContextMenuStrip;
            this.setDefaultConnectionToolStripMenuItem.Name = "setDefaultConnectionToolStripMenuItem";
            this.setDefaultConnectionToolStripMenuItem.Size = new System.Drawing.Size(384, 22);
            this.setDefaultConnectionToolStripMenuItem.Text = "Set Default Connection";
            this.setDefaultConnectionToolStripMenuItem.DropDownOpening += new System.EventHandler(this.setDefaultConnectionToolStripMenuItem_DropDownOpening);
            // 
            // defaultConnectionContextMenuStrip
            // 
            this.defaultConnectionContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dummyToolStripMenuItem});
            this.defaultConnectionContextMenuStrip.Name = "defaultConnectionContextMenuStrip";
            this.defaultConnectionContextMenuStrip.OwnerItem = this.setDefaultConnectionToolStripMenuItem;
            this.defaultConnectionContextMenuStrip.Size = new System.Drawing.Size(117, 26);
            // 
            // dummyToolStripMenuItem
            // 
            this.dummyToolStripMenuItem.Enabled = false;
            this.dummyToolStripMenuItem.Name = "dummyToolStripMenuItem";
            this.dummyToolStripMenuItem.Size = new System.Drawing.Size(116, 22);
            this.dummyToolStripMenuItem.Text = "dummy";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(381, 6);
            // 
            // viewLogToolStripMenuItem
            // 
            this.viewLogToolStripMenuItem.Name = "viewLogToolStripMenuItem";
            this.viewLogToolStripMenuItem.Size = new System.Drawing.Size(384, 22);
            this.viewLogToolStripMenuItem.Text = "View Log";
            this.viewLogToolStripMenuItem.Click += new System.EventHandler(this.viewLogToolStripMenuItem_Click);
            // 
            // openLogFolderToolStripMenuItem
            // 
            this.openLogFolderToolStripMenuItem.Name = "openLogFolderToolStripMenuItem";
            this.openLogFolderToolStripMenuItem.Size = new System.Drawing.Size(384, 22);
            this.openLogFolderToolStripMenuItem.Text = "Open Log Folder";
            this.openLogFolderToolStripMenuItem.Click += new System.EventHandler(this.openLogFolderToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(381, 6);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(384, 22);
            this.helpToolStripMenuItem.Text = "Help";
            this.helpToolStripMenuItem.Click += new System.EventHandler(this.helpToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(384, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // RagTrayIcon
            // 
            this.ClientSize = new System.Drawing.Size(292, 273);
            this.Name = "RagTrayIcon";
            this.ShowInTaskbar = false;
            this.Text = "RagTrayIcon";
            this.WindowState = System.Windows.Forms.FormWindowState.Minimized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.RagTrayIcon_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.RagTrayIcon_FormClosed);
            this.VisibleChanged += new System.EventHandler(this.RagTrayIcon_VisibleChanged);
            this.notifyIconContextMenuStrip.ResumeLayout(false);
            this.defaultConnectionContextMenuStrip.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.NotifyIcon notifyIcon;
        private System.Windows.Forms.ContextMenuStrip notifyIconContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem viewLogToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem launchRagToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem launchRagInterfaceToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem launchGameToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem resetConsoleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem AutoLaunchRagtoolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem showBalloons_toolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openLogFolderToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem AutoVerifyGameLinkMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem setDefaultConnectionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dummyToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip defaultConnectionContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem cacheNetworkPacketsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem AutoLaunchRagInterfaceToolStripMenuItem;
    }
}