﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading;
using System.Net.Sockets;
using System.Diagnostics;
using ragCore;
using System.Net;
using ragWidgets;
using ragCompression;
using System.Reflection;
using System.Windows.Forms;
using System.Net.NetworkInformation;
using System.Threading.Tasks;
using RSG.Base.Logging;

namespace ragTray
{
    using Proxy.Proxies;

    /*
     * 
     * 
     * 
     * 
     * 
     * Note: See this: https://devstar.rockstargames.com/wiki/index.php/Proxy_Dev_Docs
     * 
     * 
     * 
     * 
     * */

    public delegate void GameEventHandler( object proxy, GameConnectedEventArgs args );
    public interface IGameProxyService
    {
        IList<NetworkData> GetAllWidgetPackets();
        IList<AppRemotePacket> GetHandshakePackets();

        NetworkData CompressNetworkData( ref List<NetworkData> updateDataList );

        IBankManager GetBankManager();
        IInputManager GetInputManager();

        object AcquireLockObject();

        void ResetConsole();

        DateTime GetConnectedAt();
        String GetExeName();
        string GetGameIP();
        IPAddress GetGameIPAddress();
        string GetPlatform();
        string GetBuildConfig();
        bool IsConnected();

        /// <summary>
        /// This checks if there is any lingering unprocessed widget data
        /// on the bank pipe. It's necessary to make sure that when the RagProxy
        /// forwards data onto Rag, it isn't interrupting the data that's
        /// already sent. Essentially, it shouldn't send anything in the middle
        /// of another packet that is being sent.
        /// </summary>
        /// <returns></returns>
        bool IsPipeClear();
        object GetPipeOwner();
        void SetPipeOwner(object owner);

        event GameEventHandler GameConnected;
        event GameEventHandler GameDisconnected;

        /// <summary>
        /// The GameProxy will during initialisation sit and wait for a connected 
        /// pipe to be set using this function.
        /// </summary>
        /// <param name="pipe"></param>
        /// <param name="connectedAt"></param>
        void InitialiseFromPipe(NamedPipeSocket pipe, DateTime connectedAt);
    }

    /// <summary>
    /// 
    /// </summary>
    public class GameProxy : AppProxy, IGameProxyService
    {
        #region Constants
        /// <summary>
        /// Name of this app proxy.
        /// </summary>
        private const String c_name = "Game Proxy";

        /// <summary>
        /// 
        /// </summary>
        private const int c_ragConsolePortOffset = (int)PipeID.EnumSocketOffset.SOCKET_OFFSET_USER5 + 1;

        /// <summary>
        /// Flag indicating whether we should be using a sensor.
        /// </summary>
        private const bool c_useSensor = false;
        
        private const int c_bankManagerEarlyCommunicationPingPeriodicityMS = 10 * 1000; // Give the game a chance to come up and be stable
        private const int c_bankManagerPingPeriodicityMS = 2 * 1000;
        private const int c_maxNumUnreturnedBankManagerPings = 3;
        #endregion // Constants

        #region Static Members
        private static List<IWidgetPlugIn> m_WidgetPlugIns = new List<IWidgetPlugIn>();
        private static bool s_StaticsInitialised = false;

        public static event EventHandler GameStoppedResponding;
        public static event EventHandler GameResumedResponding;
        #endregion // Static Members

        #region Member Data
        private TrayApplicationManager m_AppManager = new TrayApplicationManager();

        private INamedPipe m_RagPipe;
        private readonly INamedPipe m_ConsolePipe = PipeID.CreateNamedPipe();
        private InternalBankPipe m_InternalBankPipe;

        private IPAddress m_Address;

        private Thread m_RagPipeThread;
        private Thread m_UpdatePipeThread;
        private Thread m_ConsolePipeThread;
        private Thread m_CheckConnectionThread;

        private BankRemotePacketProcessor m_InternalPacketProcessor = new BankRemotePacketProcessor();

        private DateTime m_ForceQuitTimeout = DateTime.MinValue;

        private List<NetworkData> m_BankDataToForward = new List<NetworkData>();
        private List<NetworkData> m_OutputDataToForward = new List<NetworkData>();

        /// <summary>
        /// List that keeps track of all the data that has ever been sent from the game.
        /// </summary>
        private List<NetworkData> m_allWidgetPackets = new List<NetworkData>();
        private List<AppRemotePacket> m_HandShakePackets = new List<AppRemotePacket>();

        private Dictionary<String, IWidgetPlugIn> m_InitializedWidgetPlugInsMap = new Dictionary<String, IWidgetPlugIn>();

        /// <summary>
        /// Flag indicating that the game connection is initialised.  This is set the first time we receive any data on the bank pipe.
        /// </summary>
        private bool m_GameInitialised = false;

        /// <summary>
        /// 
        /// </summary>
        private DateTime m_connectedAt;

        private long m_numBankManagerPingsSent = 0;
        private long m_numBankManagerPingResponsesReceived = 0;
        private bool m_userDismissedRequestToCloseConnectionToGame = false;
        private bool m_WaitingForResponseToCloseConnection = false;
        #endregion // Member Data

        #region Events
        /// <summary>
        /// Event fired when we have established a connection on the RAG pipe.
        /// </summary>
        public event GameEventHandler GameConnected;

        /// <summary>
        /// Event fired when we've detected that the connection to the game has gone down.
        /// </summary>
        public event GameEventHandler GameDisconnected;
        #endregion // Events

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public GameProxy(ProxyHub hub, IPAddress address)
            : base(c_name, hub)
        {
            m_Address = address;

            // Simply initialise the update pipe thread waiting for it to be started.
            m_UpdatePipeThread = new Thread( new ThreadStart( UpdatePipes ) );
            m_UpdatePipeThread.SetApartmentState(ApartmentState.STA);
            m_UpdatePipeThread.Name = String.Format("Game Proxy Main pipe update thread {0}", UID);
            m_UpdatePipeThread.IsBackground = true;

            WidgetBank.AddBankEvent += m_AppManager.AddBank;
            WidgetBank.RemoveBankEvent += m_AppManager.RemoveBank;

            ServiceManager serviceManager = hub.ServiceManager;
            serviceManager.GameService = this;
            SubscribeToStream( "bankraw", serviceManager );
            SubscribeToStream( "bankcompressed", serviceManager );
            SubscribeToStream( "output", serviceManager );
            SubscribeToStream( "event", serviceManager );

            Log("Adding widget handlers to internal packet processor.");
            WidgetAngle.AddHandlers( m_InternalPacketProcessor );
            WidgetBank.AddHandlers( m_InternalPacketProcessor );
            WidgetButton.AddHandlers( m_InternalPacketProcessor );
            WidgetColor.AddHandlers( m_InternalPacketProcessor );
            WidgetCombo.AddHandlers( m_InternalPacketProcessor );
            WidgetData.AddHandlers( m_InternalPacketProcessor );
            WidgetGroup.AddHandlers( m_InternalPacketProcessor );
            WidgetImageViewer.AddHandlers( m_InternalPacketProcessor );
            WidgetList.AddHandlers( m_InternalPacketProcessor );
            WidgetMatrix.AddHandlers( m_InternalPacketProcessor );
            WidgetSeparator.AddHandlers( m_InternalPacketProcessor );
            WidgetSliderFloat.AddHandlers( m_InternalPacketProcessor );
            WidgetSliderInt.AddHandlers( m_InternalPacketProcessor );
            WidgetText.AddHandlers( m_InternalPacketProcessor );
            WidgetTitle.AddHandlers( m_InternalPacketProcessor );
            WidgetToggle.AddHandlers( m_InternalPacketProcessor );
            WidgetTreeList.AddHandlers( m_InternalPacketProcessor );
            WidgetVector.AddHandlers( m_InternalPacketProcessor );
            WidgetVCR.AddHandlers( m_InternalPacketProcessor );

            if (!s_StaticsInitialised)
            {
                s_StaticsInitialised = true;
                LoadDlls(Path.Combine(Environment.CurrentDirectory, "Plugins"), LogObj, LogCtx);
            }

            Log("Adding plugin handlers.");
            foreach (IWidgetPlugIn plugin in m_WidgetPlugIns)
            {
                plugin.AddHandlers(m_InternalPacketProcessor);
            }

            Log("Starting rag pipe thread.");
            m_RagPipeThread = new Thread( new ThreadStart( RagePipeProc ) );
            m_RagPipeThread.Name = String.Format("GameProxy '{0}' connection thread", UID);
            m_RagPipeThread.IsBackground = true;
            m_RagPipeThread.Start();
        }
        #endregion // Constructor(s)

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public INamedPipe ConsolePipe
        {
            get { return m_ConsolePipe; }
        }
        #endregion

        public override void Stop()
        {
            foreach ( var item in m_HubStreams )
            {
                item.Value.RemoveProxy( this );
            }

            if (c_useSensor)
            {
                Sensor.ClearSensor();
            }

            m_ServiceManager.GameService = null;

            ShutdownPlugIns();

            m_AppManager.UpdatePipeDisconnected -= m_AppManager_UpdatePipeDisconnected;
            m_AppManager.Stop();           

            if ( m_AppManager.MainApplication != null )
            {
                m_AppManager.MainApplication.OutputReceived -= MainApplication_OutputReceived;

                m_AppManager.MainApplication.PacketProcessor.ResetTypes();
                m_AppManager.MainApplication.PacketProcessor.DataReceieved -= BankRemotePacket_DataReceieved;

                try { m_AppManager.MainApplication.BankPipe.Close(); }
                catch ( Exception ) { Log( "Could not close pipe bank" ); }

                try { m_AppManager.MainApplication.OutputPipe.Close(); }
                catch ( Exception ) { Log( "Could not close output bank" ); }

                try { m_AppManager.MainApplication.InputHandler.Close(); }
                catch ( Exception ) { Log( "Could not close input handler" ); }
            }

            WidgetBank.AddBankEvent -= m_AppManager.AddBank;
            WidgetBank.RemoveBankEvent -= m_AppManager.RemoveBank;

            //Join all threads.
            if ( m_ConsolePipeThread != null )
            {
                m_ConsolePipe.CancelCreate();
                m_ConsolePipeThread.Join(); // wait for thread to terminate
                m_ConsolePipeThread = null;
            }

            if ( m_CheckConnectionThread != null )
            {
                m_CheckConnectionThread.Abort();
                m_CheckConnectionThread = null;
            }

            base.Stop();
        }


        public static void ClearStaticsAndPlugins()
        {
            lock ( m_WidgetPlugIns )
            {
                m_WidgetPlugIns.Clear();
            }
        }



        public override void ShutDown()
        {
            Log("ShutDown");
            m_ConsolePipe.CancelCreate();
            m_ConsolePipe.Close();
            base.ShutDown();
        }

        /// <summary>
        /// Sets up the rag pipe that will be used for retrieving data from the game.
        /// </summary>
        public void InitialiseFromPipe(NamedPipeSocket pipe, DateTime connectedAt)
        {
            m_RagPipe = pipe;
            m_connectedAt = connectedAt;
        }

        /// <summary>
        /// Thread method that takes care of setting up the game connection.
        /// </summary>
        private void RagePipeProc()
        {
            String threadName = m_RagPipeThread.Name;
            Log("Started " + threadName);

            // We wait here until m_RagPipe gets "attached" by the GameListener/HubManager.
            Log("Waiting for game to connect to pipe 'pipe_rag@{0}:{1}'...", m_Address, 2000);
            while (m_RagPipe == null)
            {
                if (State != AppProxy.ProxyState.Initialising)
                {
                    Log("State changed to '{0}' before the rag pipe was connected.", State);
                    Log("Ended " + threadName);
                    return;
                }

                Thread.Sleep( 100 );
            }
            Log("Rag pipe created.");
            AppRemotePacket.HandShakePacketReceived += AppRemotePacket_HandShakePacketReceived;

            bool mainWindowStarted = m_AppManager.ReadMainAppInfo(m_RagPipe, LogCtx);
            m_RagPipe.Close();
            AppRemotePacket.HandShakePacketReceived -= AppRemotePacket_HandShakePacketReceived;

            if (!mainWindowStarted)
            {
                Log("Couldn't read main app info");
                OnGameDisconnected();
                Log("Ended " + threadName);
                return;
            }

            Log( "Set up a connection" );
            if (State == ProxyState.Initialising)
            {
                State = AppProxy.ProxyState.ReadyIdle;

                Log("Starting console pipe thread.");
                m_ConsolePipeThread = new Thread(new ThreadStart(ConsolePipeProc));
                m_ConsolePipeThread.Name = String.Format("Game Proxy Console Connection Thread {0}", UID);
                m_ConsolePipeThread.IsBackground = true;
                m_ConsolePipeThread.Start();

                Log("Adding widget handlers to main app's packet processor.");
                WidgetAngle.AddHandlers( m_AppManager.MainApplication.PacketProcessor );
                WidgetBank.AddHandlers( m_AppManager.MainApplication.PacketProcessor );
                WidgetButton.AddHandlers( m_AppManager.MainApplication.PacketProcessor );
                WidgetColor.AddHandlers( m_AppManager.MainApplication.PacketProcessor );
                WidgetCombo.AddHandlers( m_AppManager.MainApplication.PacketProcessor );
                WidgetData.AddHandlers( m_AppManager.MainApplication.PacketProcessor );
                WidgetGroup.AddHandlers( m_AppManager.MainApplication.PacketProcessor );
                WidgetImageViewer.AddHandlers( m_AppManager.MainApplication.PacketProcessor );
                WidgetList.AddHandlers( m_AppManager.MainApplication.PacketProcessor );
                WidgetMatrix.AddHandlers( m_AppManager.MainApplication.PacketProcessor );
                WidgetSeparator.AddHandlers( m_AppManager.MainApplication.PacketProcessor );
                WidgetSliderFloat.AddHandlers( m_AppManager.MainApplication.PacketProcessor );
                WidgetSliderInt.AddHandlers( m_AppManager.MainApplication.PacketProcessor );
                WidgetText.AddHandlers( m_AppManager.MainApplication.PacketProcessor );
                WidgetTitle.AddHandlers( m_AppManager.MainApplication.PacketProcessor );
                WidgetToggle.AddHandlers( m_AppManager.MainApplication.PacketProcessor );
                WidgetTreeList.AddHandlers( m_AppManager.MainApplication.PacketProcessor );
                WidgetVector.AddHandlers( m_AppManager.MainApplication.PacketProcessor );
                WidgetVCR.AddHandlers( m_AppManager.MainApplication.PacketProcessor );

                m_AppManager.MainApplication.OutputReceived += new OutputDataDelegate( MainApplication_OutputReceived );
                m_AppManager.MainApplication.PacketProcessor.DataReceieved += BankRemotePacket_DataReceieved;
                m_InternalBankPipe = new InternalBankPipe( m_AppManager.MainApplication.BankPipe );

                Log("Loading apps from the app manager.");
                m_AppManager.LoadApps();
                m_AppManager.UpdatePipeDisconnected += m_AppManager_UpdatePipeDisconnected;

                Log("Initialising plugins.");
                InitPlugIns();

                Log("Starting check connection thread.");
                m_CheckConnectionThread = new Thread( new ThreadStart( CheckConnectionToGame ) );
                m_CheckConnectionThread.Name = String.Format("Game Proxy Check Connection Thread {0}", UID);
                m_CheckConnectionThread.IsBackground = true;
                m_CheckConnectionThread.Start();

                m_AppManager.MainApplication.PacketProcessor.DestroyHandler = new BankRemotePacketProcessor.DestroyDelegate( Widget.DestroyHandler );

                Log("Starting update pipe thread.");
                m_UpdatePipeThread.Start();

                Log("Waiting until rag pipe is no longer connected.");
                while ( m_RagPipe.IsConnected() )
                {
                    Thread.Sleep( 100 );
                }
                Log("Rag pipe is no longer connected.");
                OnGameConnected();

                if (!String.IsNullOrEmpty(m_AppManager.PlatformString) && c_useSensor)
                {
                    Log( "Creating sensor for platform '{0}'", m_AppManager.PlatformString );
                    Sensor.CreateSensor( m_AppManager.PlatformString, LogObj );
                    // TODO add sensor on outputmessage handling
                }

                if ( m_ServiceManager.NotifyService != null )
                {
                    m_ServiceManager.NotifyService.Notify( "Connected!", "The proxy is connected to " + GetPlatform() + " @ " + GetGameIP() );
                }

                Log( "Connection complete." );
            }
            else
            {
                if ( m_ServiceManager.NotifyService != null )
                {
                    m_ServiceManager.NotifyService.Notify( "Failed!", "The proxy tried and failed to connect to the game." );
                }

                Log( "Failed/aborted connecting to game. Stopping." );
                State = AppProxy.ProxyState.Stopping;
            }

            Log( "Ended " + threadName );
        }

        void m_AppManager_UpdatePipeDisconnected( object sender, EventArgs e )
        {
            Log( "App manager disconnected." );
            OnGameDisconnected();
        }

        /// <summary>
        /// Task method that creates the console pipe.
        /// </summary>
        private void ConsolePipeProc()
        {
            PipeID consolePipeName = m_AppManager.MainApplication.GetRagPipeSocket(c_ragConsolePortOffset);

            if (!m_ConsolePipe.Create(consolePipeName, true))
            {
                Log("Could not connect to pipe '{0}'!", consolePipeName.ToString());
            }
            else
            {
                Log("Connected to pipe '{0}'.", consolePipeName.ToString());
            }
        }

        private void HandleBankManagerPingMessage( object owner, EventArgs e )
        {
            ++m_numBankManagerPingResponsesReceived;
            if ( GameResumedResponding != null && m_WaitingForResponseToCloseConnection)
            {
                m_WaitingForResponseToCloseConnection = false;
                GameResumedResponding( this, EventArgs.Empty );
            }
        }

        /// <summary>
        /// Thread method that monitors the connection to the game.
        /// </summary>
        private void CheckConnectionToGame()
        {
            while ( IsRunning )
            {
                if ( m_numBankManagerPingResponsesReceived > 1 )
                {
                    Thread.Sleep( c_bankManagerPingPeriodicityMS );
                }
                else
                {
                    Thread.Sleep( c_bankManagerEarlyCommunicationPingPeriodicityMS );
                }

                if ( !m_GameInitialised )
                {
                    continue;
                }

                if ( !IsRunning )
                {
                    Log( "Exiting CheckConnectionToGame thread." );
                    break;
                }

                long numUnreturnedBankManagerPings = m_numBankManagerPingsSent - m_numBankManagerPingResponsesReceived;
                if ( Properties.Settings.Default.AutoVerifyGameLink &&
                    !m_userDismissedRequestToCloseConnectionToGame &&
                    numUnreturnedBankManagerPings > c_maxNumUnreturnedBankManagerPings &&
                    !m_WaitingForResponseToCloseConnection )
                {
                    if ( GameStoppedResponding != null )
                    {
                        m_WaitingForResponseToCloseConnection = true;
                        GameStoppedResponding( this, EventArgs.Empty );
                    }
                }

                // hang around for the first response before we start checking the connection regularly
                if ( m_numBankManagerPingsSent == 1 && m_numBankManagerPingResponsesReceived == 0 )
                {
                    continue;
                }

                try
                {
                    GetBankManager().SendPing( HandleBankManagerPingMessage, m_AppManager.MainApplication.BankPipe );
                    ++m_numBankManagerPingsSent;
                }
                catch ( SocketException )
                {
                    Log( Environment.NewLine + "Application disconnected. Failed sending ping." + Environment.NewLine );
                    OnGameDisconnected();

                    GetBankManager().Reset();
                    break;
                }
            }
        }

        public void OnCloseConnectionFormResult( DialogResult result )
        {
            if ( result == DialogResult.Yes )
            {
                Log( Environment.NewLine + "Application disconnected.  The bank manager stopped responding to pings." + Environment.NewLine );
                OnGameDisconnected();

                GetBankManager().Reset();
            }
            else if ( result == DialogResult.Abort )
            {
                m_userDismissedRequestToCloseConnectionToGame = true;
            }
            else
            {
                m_numBankManagerPingResponsesReceived = m_numBankManagerPingsSent;
            }

            m_WaitingForResponseToCloseConnection = false;
        }


        private void UpdatePipes()
        {
            Log("Starting update pipe.");

            // update all pipes...
            while ( m_AppManager.AppsLoaded && IsRunning)
            {
                // Tell the app manager to update the game pipes
                m_AppManager.ProcessPipes();

                if ( m_AppManager.MainApplication.PipesDisconnected )
                {
                    Log( Environment.NewLine + "Application Disconnected." + Environment.NewLine );
                    OnGameDisconnected();

                    GetBankManager().Reset();

                    if ( m_ServiceManager.NotifyService != null )
                    {
                        m_ServiceManager.NotifyService.Notify( "Disconnected!", "The proxy was disconnected from the game." );
                    }

                    break;
                }

                // Update the internal pipes that communicate between proxy and ui.
                lock (m_AppManager.MainApplication.PacketProcessor)
                {
                    TimeSpan elapsed = TimeAction(() =>
                        {
                            m_InternalPacketProcessor.Update(m_InternalBankPipe);
                            m_InternalPacketProcessor.Process(m_InternalBankPipe);
                        });

                    if (elapsed.TotalSeconds > 1)
                    {
                        Log("Game proxy held packet processor lock for more {0} seconds.", elapsed.TotalSeconds);
                    }
                }

                Thread.Sleep( 1 );
            }

            Log("Update pipe ended.");
        }

        private TimeSpan TimeAction(Action blockingAction)
        {
            Stopwatch stopWatch = Stopwatch.StartNew();
            blockingAction();
            stopWatch.Stop();
            return stopWatch.Elapsed;
        }

        public override void Update()
        {
            if ( !IsRunning )
            {
                bool stillStopping = m_RagPipeThread.IsAlive
                    || m_UpdatePipeThread.IsAlive
                    || (m_CheckConnectionThread != null && m_CheckConnectionThread.IsAlive)
                    || m_RagPipe != null && m_RagPipe.IsConnected();

                if ( m_ForceQuitTimeout == DateTime.MinValue )
                {
                    m_ForceQuitTimeout = DateTime.Now;
                }

                if ( DateTime.Now.Subtract( m_ForceQuitTimeout ).TotalSeconds > 10 )
                {
                    Log( "Been in state Stopping for too long, forcing quit" );
                    LogState();
                    if ( m_UpdatePipeThread.IsAlive )
                    {
                        Log( "Killing m_UpdatePipeThread" );
                        m_UpdatePipeThread.Abort();
                    }

                    if ( m_CheckConnectionThread.IsAlive )
                    {
                        Log( "Killing m_CheckConnectionThread" );
                        m_CheckConnectionThread.Abort();
                    }

                    Stop();
                }

                if ( !stillStopping )
                {
                    Log( "All threads stopped." );
                    Stop();
                }
                else if ( m_RagPipeThread.IsAlive && m_RagPipe != null )
                {
                    m_RagPipe.CancelCreate();
                }

                return;
            }

            if ( Sensor.CurrentSensor != null )
            {
                if ( Sensor.CurrentSensor.HasQuit || Sensor.CurrentSensor.HasStopped )
                {
                    State = AppProxy.ProxyState.Stopping;
                    return;
                }
            }

            State = AppProxy.ProxyState.ReadyIdle;

            // Take the bank and output data received from the game
            // and pass on to the hub stream.


            bool gameOwnsPipe = m_AppManager.MainApplication.PacketProcessor.PipeOwner == m_AppManager.MainApplication.PacketProcessor;
            if ( m_BankDataToForward.Count != 0 && gameOwnsPipe)
            {
                NetworkData[] copy;
                lock (m_BankDataToForward)
                {
                    copy = m_BankDataToForward.ToArray();
                    m_BankDataToForward.Clear();
                }

                // We need to take every packet we get and store it, so that
                // we can give it to any RagProxies that are setting up later on.
                if (Properties.Settings.Default.CacheNetworkPackets)
                {
                    lock (m_allWidgetPackets)
                    {
                        m_allWidgetPackets.AddRange(copy);
                    }
                }

                int totalLength = copy.Sum(item => item.Count);
                byte[] allData = new byte[totalLength];

                int offset = 0;
                foreach (NetworkData data in copy)
                {
                    System.Buffer.BlockCopy( data.Data, 0, allData, offset, data.Count );
                    offset += data.Count;
                }

                m_HubStreams["bankcompressed"].Write( this, allData );
                State = AppProxy.ProxyState.ReadyWorking;
            }

            // See https://devstar.rockstargames.com/wiki/index.php/Proxy_Dev_Docs#Packet_sending_synchronization
            lock ( m_AppManager.MainApplication.PacketProcessor )
            {
                if (gameOwnsPipe &&
                    m_BankDataToForward.Count == 0 &&
                    m_AppManager.MainApplication.PacketProcessor.IsPipeClear &&
                    !m_AppManager.MainApplication.PacketProcessor.HasDataToProcess(m_AppManager.MainApplication.BankPipe))
                {
                    m_AppManager.MainApplication.PacketProcessor.PipeOwner = null;
                }
            }

            if ( m_OutputDataToForward.Count != 0 )
            {
                NetworkData[] copy;
                lock ( m_OutputDataToForward )
                {
                    copy = m_OutputDataToForward.ToArray();
                    m_OutputDataToForward.Clear();
                }

                int totalLength = 0;
                for ( int i = 0; i < copy.Length; ++i )
                {
                    totalLength += copy[i].Count;
                }

                byte[] allData = new byte[totalLength];

                int offset = 0;
                foreach ( var data in copy )
                {
                    System.Buffer.BlockCopy( data.Data, 0, allData, offset, data.Count );
                    offset += data.Count;
                }

                m_HubStreams["output"].Write( this, allData );

                State = AppProxy.ProxyState.ReadyWorking;
            }
        }

        public override void Write( string streamName, byte[] data, int offset, int count )
        {
            Debug.Assert( State != ProxyState.Initialising );
            if ( !IsRunning )
            {
                return;
            }

            try
            {
                if ( streamName == "bankraw" )
                {
                    // We got some data from another app (e.g. Rag), pass it on
                    // to the game network bank pipe,
                    // and store the widget data internally as well.
                    lock ( m_AppManager.MainApplication.PacketProcessor )
                    {
                        TimeSpan elapsed = TimeAction(() =>
                            {
                                m_AppManager.MainApplication.BankPipe.WriteData(data, count);
                                m_InternalBankPipe.WriteData(data, count);
                            });

                        if (elapsed.TotalSeconds > 1)
                        {
                            Log("Game proxy held packet processor lock for {0} seconds while write data to the bankraw stream.", elapsed.TotalSeconds);
                        }
                    }
                }
                if ( streamName == "bankcompressed" )
                {
                    // This will be bank data coming from Rag via a RagProxy. The only thing we want
                    // to do with it is store it in the m_InitialWidgetPackets list, so that if
                    // a future Rag connects, it'll receive the correct state for the widgets.
                    if (Properties.Settings.Default.CacheNetworkPackets)
                    {
                        lock (m_allWidgetPackets)
                        {
                            m_allWidgetPackets.Add(new NetworkData(data, offset, count));
                        }
                    }
                }
                else if ( streamName == "event" )
                {
                    // Event data, such as mouse input. Forward on to the correct pipe.
                    if ( offset != 0 || count != data.Length )
                    {
                        byte[] newData = new byte[count];
                        System.Buffer.BlockCopy( data, offset, newData, 0, count );
                        data = newData;
                    }

                    m_AppManager.MainApplication.InputHandler.WritePipe( data );
                }
            }
            catch ( SocketException )
            {
                Log( "Error while trying to write to pipe, aborting." );
                OnGameDisconnected();

                GetBankManager().Reset();
            }
        }

        /// <summary>
        /// Callback for when hand shake packets are received from the game.
        /// </summary>
        private void AppRemotePacket_HandShakePacketReceived(object sender, EventArgs e)
        {
            // We need to store these so that, when Rag connects to a RagProxy, we can
            // send this list of handshake packets on to Rag and "fool" it into thinking
            // the proxy is actually the game.
            AppRemotePacket packet = (AppRemotePacket)sender;
            AppRemotePacket copy = new AppRemotePacket(packet.Pipe, packet);
            lock (m_HandShakePackets)
            {
                m_HandShakePackets.Add(copy);
            }
        }

        /// <summary>
        /// Callback for when game output is received from the game.
        /// </summary>
        void MainApplication_OutputReceived(object sender, OutputDataEventArgs args)
        {
            NetworkData data = new NetworkData((byte[])args.Data.Clone(), args.Count);
            lock (m_OutputDataToForward)
            {
                m_OutputDataToForward.Add(data);
            }
        }

        void BankRemotePacket_DataReceieved(object sender, BankRemotePacketProcessor.DataPacketEventArgs args)
        {
            // Add the data to the list of data to forward on.
            lock (m_BankDataToForward)
            {
                m_BankDataToForward.Add(new NetworkData((byte[])args.Data.Clone(), args.Count));
            }

            // Set the game initialised flag when we've started to receive bank data from the game.
            if (!m_GameInitialised)
            {
                Log("Game is now initialised.");
                m_GameInitialised = true;
            }
        }


        /// <summary>
        /// Retrieves the list of all widget packets that were ever sent from the game to the rag proxy.
        /// </summary>
        public IList<NetworkData> GetAllWidgetPackets()
        {
            // We now store every single packet we've received in the m_InitialWidgetPackets
            // so just return a copy of that.
            List<NetworkData> data;
            lock (m_allWidgetPackets)
            {
                data = new List<NetworkData>(m_allWidgetPackets);
            }
            return data;
        }

        public NetworkData CompressNetworkData( ref List<NetworkData> updateDataList )
        {
            if ( updateDataList.Count == 0 )
            {
                throw new ArgumentException( "Won't compress zero data." );
            }

            int totalLength = 0;
            foreach ( NetworkData nData in updateDataList )
            {
                totalLength += nData.Count;
            }

            noCompressionData compresser = new noCompressionData();
            compresser.CompressedSize = compresser.DecompressedSize = (uint)totalLength;
            totalLength += compresser.GetHeaderSize();

            byte[] updateBytes = new byte[totalLength];
            int offset = compresser.BuildHeader( updateBytes, 0 );

            foreach ( NetworkData networkData in updateDataList )
            {
                Buffer.BlockCopy( networkData.Data, 0, updateBytes, offset, networkData.Count );
                offset += networkData.Count;
            }

            Debug.Assert( offset == updateBytes.Length );

            NetworkData updateData = new NetworkData( updateBytes, totalLength );
            return updateData;
        }

        /// <summary>
        /// Returns a copy of the handshake packets that were received.
        /// </summary>
        public IList<AppRemotePacket> GetHandshakePackets()
        {
            if ( State == AppProxy.ProxyState.Initialising )
            {
                return new List<AppRemotePacket>();
            }

            lock (m_HandShakePackets)
            {
                return new List<AppRemotePacket>(m_HandShakePackets);
            }
        }

        public IBankManager GetBankManager()
        {
            if ( m_AppManager.MainApplication != null )
                return m_AppManager.MainApplication.BankMgr;

            return null;
        }

        public IInputManager GetInputManager()
        {
            return this.m_AppManager.MainApplication?.InputHandler;
        }

        public bool IsConnectedToConsole()
        {
            if ( !IsRunning )
                return false;

            if ( m_ConsolePipe == null || m_ConsolePipe.Socket == null )
                return false;

            m_ConsolePipe.HasData(); //Polls the console and updates the connection status.
            return m_ConsolePipe.IsConnected();
        }

        public string GetPlatform()
        {
            if ( m_AppManager == null || m_AppManager.MainApplication == null )
                return "<no platform>";

            if (m_AppManager.PlatformString == null)
            {
                LogState();
                return "<no platform>";
            }

            return m_AppManager.PlatformString;
        }

        public String GetBuildConfig()
        {
            if (m_AppManager == null || m_AppManager.MainApplication == null)
                return "<unknown config>";

            if (m_AppManager.BuildConfigString == null)
            {
                LogState();
                return "<unknown config>";
            }

            return m_AppManager.BuildConfigString;
        }

        public object AcquireLockObject()
        {
            return m_AppManager.MainApplication.PacketProcessor;
        }


        #region Plugin Management
        /// <summary>
        /// Loads all dlls found at the given path
        /// </summary>
        private static void LoadDlls(String path, ILog log, String logCtx)
        {
            log.MessageCtx(logCtx, "Loading plugins from {0}", path);

            // load rag dlls:
            String[] dllNames = null;
            try
            {
                dllNames = Directory.GetFiles( path, "*.dll" );
            }
            catch ( System.IO.DirectoryNotFoundException )
            {
                return;
            }

            // Build a list of the loaded assemblies so that we don't load them again from the Plugins folder
            Assembly[] loadedAssemblies = AppDomain.CurrentDomain.GetAssemblies();
            List<string> ragDlls = new List<string>();
            foreach ( Assembly loadedAssembly in loadedAssemblies )
            {
                ragDlls.Add( Path.GetFileNameWithoutExtension( loadedAssembly.Location ).ToUpper() );
            }

            foreach ( String dll in dllNames )
            {
                string dllFile = Path.GetFileNameWithoutExtension( dll );
                if ( ragDlls.Contains( dllFile.ToUpper() ) )
                {
                    log.MessageCtx(logCtx, "Skipping {0} because it is already loaded.", dllFile);
                    continue;
                }

                bool alreadyLoaded = false;
                Assembly assembly = LoadDll( dll, out alreadyLoaded, log, logCtx );
                if ( (assembly != null) && !alreadyLoaded )
                {
                    // Check for special types in the DLL.
                    log.MessageCtx(logCtx, "Getting types from {0}.", assembly.FullName);
                    foreach (Type t in assembly.GetTypes().Where(item => !item.IsAbstract))
                    {
                        // register any IWidgetPlugIn types
                        if ( t.GetInterface( "ragCore.IWidgetPlugIn" ) != null )
                        {
                            ragCore.IWidgetPlugIn plugIn = t.GetConstructor( System.Type.EmptyTypes ).Invoke( null ) as ragCore.IWidgetPlugIn;
                            m_WidgetPlugIns.Add( plugIn );
                        }
                    }
                }
            }
        }

        private static Assembly LoadDll( string path, out bool alreadyLoaded, ILog log, String logCtx )
        {
            alreadyLoaded = false;

            log.MessageCtx(logCtx, "Loading assembly {0}.", path);

            Assembly assembly = null;
            try
            {
                assembly = Assembly.LoadFrom( path );
            }
            catch ( System.Exception )
            {
            }

            if ( assembly != null )
            {
                bool hasPluginType = false;
                foreach ( Type t in assembly.GetTypes() )
                {
                    if ( (t.GetInterface( "ragCore.IWidgetPlugIn" ) != null ||
                         t.GetInterface( "ragWidgets.IViewPlugIn" ) != null) &&
                         !t.IsAbstract )
                    {
                        hasPluginType = true;
                        break;
                    }
                }

                if ( !hasPluginType )
                {
                    return null;
                }

                // make sure it's a plugin DLL, otherwise unload it
                //sm_DllModules.Add( path, assembly );
                //sm_GlobalPrefs.LoadedDll( path );

                return assembly;
            }

            return null;
        }

        private void InitPlugIns()
        {
            lock ( m_WidgetPlugIns )
            {
                foreach ( IWidgetPlugIn plugin in m_WidgetPlugIns )
                {
                    string pluginName = (plugin.GetType().Assembly.GetName().Name) + ".dll";
                    if ( !m_InitializedWidgetPlugInsMap.ContainsKey( pluginName ) )
                    {
                        Log("Initializing plugin {0}", pluginName);

                        plugin.AddHandlers( m_AppManager.MainApplication.PacketProcessor );

                        m_InitializedWidgetPlugInsMap.Add( pluginName, plugin );
                    }
                }                
            }
        }

        private void ShutdownPlugIns()
        {
            lock ( m_WidgetPlugIns )
            {
                foreach ( IWidgetPlugIn plugin in m_WidgetPlugIns )
                {
                    try
                    {
                        string pluginName = (plugin.GetType().Assembly.GetName().Name) + ".dll";
                        Log("Shutting down plugin {0}", pluginName);

                        plugin.RemoveHandlers( m_AppManager.MainApplication.PacketProcessor );

                        m_InitializedWidgetPlugInsMap.Remove( pluginName );
                    }
                    catch ( Exception )
                    {
                        // do nothing?
                    }
                }                
            }

            m_InitializedWidgetPlugInsMap.Clear();
        }
        #endregion


        public void ResetConsole()
        {
            Log( "ResetConsole(): Shutting down game." );
            if ( m_AppManager == null || m_AppManager.MainApplication == null )
            {
                Log( "Couldn't shut down game, logging state..." );
                LogState();
                MessageBox.Show( "You tried to shut down the game, but the Proxy isn't connected to the game. The log might hold useful information." );
            }
            else
            {
                m_AppManager.MainApplication.BankPipe.SendQuitMessage();
            }
        }

        public DateTime GetConnectedAt()
        {
            return m_connectedAt;
        }

        public String GetExeName()
        {
            if (m_AppManager == null || m_AppManager.MainApplication == null)
                return "<unconnected>";

            if (m_AppManager.MainApplication.AppName == null)
            {
                LogState();
                return "<unconnected>";
            }

            return Path.GetFileName(m_AppManager.MainApplication.AppName);
        }

        public string GetGameIP()
        {
            if ( m_AppManager == null || m_AppManager.MainApplication == null )
                return "<unconnected>";

            string ip = m_AppManager.MainApplication.BankPipe.GetRemoteIpAddress();
            if (ip == null)
                return "<unconnected>";

            return ip;
        }

        public IPAddress GetGameIPAddress()
        {
            if (m_AppManager == null || m_AppManager.MainApplication == null)
                return null;

            return m_AppManager.MainApplication.BankPipe.GetRemoteIp();

        }

        public override void LogState()
        {
            base.LogState();
            Log( "m_RagPipeThread.IsAlive {0} :: " +
                "m_UpdatePipeThread.IsAlive {1} :: " +
                "m_CheckConnectionThread != null {2} && m_CheckConnectionThread.IsAlive {3} :: " +
                "m_RagPipe.IsConnected() {4} :: " +
                "m_ConsolePipe.IsConnected() {5}",
                m_RagPipeThread.IsAlive,
                m_UpdatePipeThread.IsAlive,
                m_CheckConnectionThread != null,
                m_CheckConnectionThread != null && m_CheckConnectionThread.IsAlive,
                m_RagPipe != null ? m_RagPipe.IsConnected().ToString() : "null",
                m_ConsolePipe.IsConnected()
                );
        }

        public override string GetShortDescription()
        {
            return "Game@" + GetShortStateDescription();
        }


        protected void OnGameConnected()
        {
            if (GameConnected != null)
            {
                GameConnected(this, new GameConnectedEventArgs(GetPlatform(), GetBuildConfig()));
            }
        }

        protected void OnGameDisconnected()
        {
            if (State == ProxyState.Stopping || State == ProxyState.Stopped)
            {
                return;
            }

            State = AppProxy.ProxyState.Stopping;

            if ( GameDisconnected != null )
            {
                GameDisconnected(this, new GameConnectedEventArgs());
            }
        }

        public bool IsConnected()
        {
            return !m_AppManager.MainApplication.PipesDisconnected;
        }



        public bool IsPipeClear()
        {
            return m_AppManager.MainApplication.PacketProcessor.PipeOwner == null;
        }

        public object GetPipeOwner()
        {
            return m_AppManager.MainApplication.PacketProcessor.PipeOwner;
        }

        public void SetPipeOwner(object owner)
        {
            m_AppManager.MainApplication.PacketProcessor.PipeOwner = owner;
        }
    }
}
