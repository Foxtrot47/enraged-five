﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ragCore;
using RSG.Base.Forms;
using System.Diagnostics;
using System.Net.Sockets;
using System.Threading;
using System.Drawing;
using RSG.Base.Logging;
using System.Net.NetworkInformation;
using System.Net;
using System.IO;

namespace ragTray
{
    /// <summary>
    /// Game output event args.
    /// </summary>
    internal class OutputDataEventArgs : EventArgs
    {
        public byte[] Data { get; private set; }
        public int Count { get; private set; }

        public OutputDataEventArgs(byte[] data, int count)
        {
            Data = data;
            Count = count;
        }
    } // OutputDataEventArgs

    /// <summary>
    /// Delegate for sending output events.
    /// </summary>
    internal delegate void OutputDataDelegate(object sender, OutputDataEventArgs args);

    /// <summary>
    /// 
    /// </summary>
    public class TrayApplication : IDisposable
    {
        #region Constants
        /// <summary>
        /// Number of sockets we need to reserve.
        /// </summary>
        private const int c_numSocketsToReserve = 50;

        /// <summary>
        /// Size of the buffer for reading data from the output pipe.
        /// </summary>
        private const int c_outputBufferSize = 4 * 1024;

        /// <summary>
        /// Log message context.
        /// </summary>
        private const String LogCtx = "Tray Application";
        #endregion // Constants

        #region Static Member Data
        /// <summary>
        /// Lock object to use when attempting to reserve ports.
        /// </summary>
        private static object s_portReservationLock = new object();
        #endregion // Static Member Data

        #region Properties
        /// <summary>
        /// Whether this is a main application.
        /// </summary>
        public bool IsMainApp { get; set; }

        public int PortNumber { get; private set; }
        private List<TcpListener> m_reservedSockets = new List<TcpListener>();

        public ragCore.PipeID PipeNameBank { get; private set; }
        public ragCore.PipeID PipeNameOutput { get; private set; }
        public ragCore.PipeID PipeNameEvents { get; private set; }

        public BankRemotePacketProcessor PacketProcessor { get; private set; }

        public String VisibleName { get; set; }

        public String AppName { get; set; }

        public String ExeName
        {
            get { return System.IO.Path.GetFileName(AppName); }
        }

        public String Args { get; set; }

        public bool ShouldStart { get; set; }

        public IBankManager BankMgr { get; private set; }

        public bool WasConnected { get; private set; }

        public bool PipesDisconnected
        {
            get
            {
                return !OutputPipe.IsConnected()
                    || !BankPipe.IsConnected()
                    || (IsMainApp && !InputHandler.IsConnected())
                    || Sensor.CurrentSensor == null
                    || Sensor.CurrentSensor.HasQuit;
            }
        }


        public BankPipe BankPipe { get; private set; }
        public INamedPipe OutputPipe { get; private set; }

        public Input InputHandler { get; private set; }

        /// <summary>
        /// Reference to the log object to make use of.
        /// </summary>
        private readonly ILog _log;
        #endregion // Properties

        #region Events
        /// <summary>
        /// Event that gets fired when output data has been received on the output pipe.
        /// </summary>
        internal event OutputDataDelegate OutputReceived;
        #endregion // Events

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public TrayApplication( TrayBankManager bankManager, int basePort, int maxPort )
        {
            _log = LogFactory.ApplicationLog;
            PacketProcessor = new BankRemotePacketProcessor(true);
            InputHandler = new Input();
            BankPipe = new BankPipe();
            OutputPipe = PipeID.CreateNamedPipe();

            bankManager.AddHandlers(PacketProcessor);
            BankMgr = bankManager;

            // We need to lock here in case multiple games start up at the same time
            // (otherwise they would be fighting over the ports and never manage to get their 50).
            lock (s_portReservationLock)
            {
                FindAvailableSockets(basePort, maxPort);
            }

            PipeNameBank = GetRagPipeSocket((int)PipeID.EnumSocketOffset.SOCKET_OFFSET_BANK);
            PipeNameOutput = GetRagPipeSocket((int)PipeID.EnumSocketOffset.SOCKET_OFFSET_OUTPUT);
            PipeNameEvents = GetRagPipeSocket((int)PipeID.EnumSocketOffset.SOCKET_OFFSET_EVENTS);
        }
        #endregion // Constructor(s)

        #region Public Methods
        /// <summary>
        /// 
        /// </summary>
        public PipeID GetRagPipeSocket(int offset)
        {
            _log.MessageCtx(LogCtx, "Freeing up port {0}", PortNumber + offset);
            Debug.Assert(offset < m_reservedSockets.Count, "Requesting port that hasn't been reserved by the TrayApplication.");
            if (offset >= m_reservedSockets.Count)
            {
                throw new ArgumentOutOfRangeException("offset", "Requesting port that hasn't been reserved by the TrayApplication.");
            }

            // Stop the listener that is running on that port and create a pipe for it instead.
            //m_reservedSockets[offset].Stop();
            if (m_reservedSockets[offset] == null)
            {
                throw new ArgumentNullException("offset", "Listener has already been claimed.");
            }

            PipeID pipeId = new PipeID(RagTrayIcon.Instance.Address, PortNumber + offset, m_reservedSockets[offset]);
            m_reservedSockets[offset] = null;
            return pipeId;
        }

        /// <summary>
        /// 
        /// </summary>
        public bool ConnectPipes()
        {
            if ( IsMainApp )
            {
                // start output pipe:
                if (!ConnectToPipe(PipeNameOutput, OutputPipe))
                {
                    rageMessageBox.ShowError(
                        String.Format("Could not connect to Rage application '{0}'.  Unable to connect to pipe {1}.   Rag will be terminated.", this.ExeName, OutputPipe.Name), "Connection Failed");
                    return false;
                }

                // start bank pipe:
                if (ConnectToPipe(PipeNameBank, BankPipe))
                {
                    BankPipe.NoDelay = true;
                }
                else
                {
                    rageMessageBox.ShowError(
                        String.Format("Could not connect to Rage application '{0}'.  Unable to connect to pipe {1}.  Rag will be terminated.", this.ExeName, BankPipe.Name), "Connection Failed");
                    return false;
                }

                // What is this message for?!?!?
                BankRemotePacket packet = new BankRemotePacket(BankPipe);
                packet.Begin(BankRemotePacket.EnumPacketType.USER_3, TrayBankManager.GetStaticGUID(), 0);
                packet.Write_u32(0u);
                packet.Send();

                // connect to the input handler:
                if (!InputHandler.Connect(PipeNameEvents))
                {
                    rageMessageBox.ShowError(
                        String.Format("Could not connect to Rage application '{0}'.  Unable to connect to pipe {1}. Rag will be terminated.", this.ExeName, PipeNameEvents.Name), "Connection Failed");
                    return false;
                }
            }

            WasConnected = true;
            return true;
        }

        /// <summary>
        /// This is the thread function
        /// </summary>
        public void ReadOutputPipe()
        {
            if (OutputPipe.IsValid())
            {
                String currentLine = String.Empty;
                while (OutputPipe.HasData())
                {
                    Byte[] chars = new Byte[c_outputBufferSize];
                    int numRead = OutputPipe.ReadData(chars, c_outputBufferSize, false);

                    OutputReceived(this, new OutputDataEventArgs(chars, numRead));
                }
            }
        }
        #endregion // Public Methods

        #region Private Methods
        /// <summary>
        /// Method that finds a consecutive batch of 50 free sockets.
        /// </summary>
        private void FindAvailableSockets(int basePort, int maxPort)
        {
            _log.MessageCtx(LogCtx, "Reserving {0} sockets between {1} and {2}...",
                c_numSocketsToReserve, basePort, maxPort);
            int firstPort = ReservePorts(basePort, maxPort);

            if (m_reservedSockets.Count != c_numSocketsToReserve)
            {
                _log.WarningCtx(LogCtx, "Only reserved {0} sockets.", m_reservedSockets.Count);

                try
                {
                    IPGlobalProperties ipProperties = IPGlobalProperties.GetIPGlobalProperties();
                    TcpConnectionInformation[] tcpConnections = ipProperties.GetActiveTcpConnections();

                    _log.WarningCtx(LogCtx, "Active TCP Connections");
                    foreach (TcpConnectionInformation connection in tcpConnections.OrderBy(item => item.LocalEndPoint.Port))
                    {
                        _log.WarningCtx(LogCtx, "local: {0} remote: {1} state: {2}",
                            connection.LocalEndPoint,
                            connection.RemoteEndPoint,
                            connection.State);
                    }

                    _log.WarningCtx(LogCtx, "TCP Listeners");
                    IPEndPoint[] tcpListeners = ipProperties.GetActiveTcpListeners();
                    foreach (IPEndPoint listener in tcpListeners.OrderBy(item => item.Port))
                    {
                        _log.WarningCtx(LogCtx, listener.ToString());
                    }

                    _log.WarningCtx(LogCtx, "UDP Listeners");
                    IPEndPoint[] udpListeners = ipProperties.GetActiveUdpListeners();
                    foreach (IPEndPoint listener in udpListeners.OrderBy(item => item.Port))
                    {
                        _log.WarningCtx(LogCtx, listener.ToString());
                    }

                    string args = String.Format("/c netstat.exe -abn > {0}\\netstat_{1}.txt",
                        Path.GetDirectoryName(LogFactory.ApplicationLogFilename),
                        DateTime.Now.ToString("yyyyMMddHHmmss"));
                    Process.Start("cmd.exe", args);
                }
                catch (Exception)
                {
                    // Ignore exceptions
                }

                throw new ArgumentOutOfRangeException(String.Format(
                    "Unable to reserve a consecutive block of 50 ports between ports {0} and {1}.",
                    basePort,
                    maxPort));
            }

            // We only break out of the loop once we have reserved 50 sockets.
            PortNumber = firstPort;
            _log.MessageCtx(LogCtx, "Done Reserving Sockets.  Base Port = {0}", firstPort);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="startPort"></param>
        /// <param name="maxPort"></param>
        private int ReservePorts(int startPort, int maxPort)
        {
            m_reservedSockets.Clear();

            // HACK! We might have received a new game connection prior to the application starting up fully
            // so wait here until the TrayIcon instance is set (for a max of 10 seconds).
            if (RagTrayIcon.Instance == null)
            {
                _log.Message(LogCtx, "Tray icon instance null, waiting for up to 10 seconds for it to be set.");

                Stopwatch sw = new Stopwatch();
                sw.Start();
                while (RagTrayIcon.Instance == null)
                {
                    Thread.Sleep(50);

                    if (sw.Elapsed.TotalSeconds > 10)
                    {
                        throw new TimeoutException("Timeout waiting for application instance to be initialised.");
                    }
                }
            }

            // Loop over ports starting at the base port.
            int port;
            for (port = startPort; m_reservedSockets.Count < c_numSocketsToReserve && port <= maxPort && port <= UInt16.MaxValue; ++port)
            {
                try
                {
                    if (RagTrayIcon.Instance == null)
                    {
                        _log.MessageCtx(LogCtx, "RagTrayIcon.Instance null");
                    }
                    else if (RagTrayIcon.Instance.Address == null)
                    {
                        _log.MessageCtx(LogCtx, "RagTrayIcon.Instance.Address null");
                    }

                    TcpListener listener = new TcpListener(RagTrayIcon.Instance.Address, port);

                    LingerOption lingerOption = new LingerOption(false, 0);

                    if (listener.Server == null)
                    {
                        _log.MessageCtx(LogCtx, "listener.Server null");
                    }
                    listener.Server.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.Linger, lingerOption);

                    listener.Start();
                    m_reservedSockets.Add(listener);
                }
                catch (SocketException e)
                {
                    _log.WarningCtx(LogCtx, "Could not reserve port {0}: {1}", port, e.ToString());

                    // Clear out any sockets we've already reserved.
                    StopListeners(m_reservedSockets);
                    m_reservedSockets.Clear();
                }
                catch (NullReferenceException e)
                {
                    _log.ToolExceptionCtx(LogCtx, e, "Null ref exception!");
                    throw e;
                }
            }

            return port - c_numSocketsToReserve;
        }

        /// <summary>
        /// Stops a list of listeners.
        /// </summary>
        private void StopListeners(IList<TcpListener> listeners)
        {
            foreach (TcpListener listener in listeners)
            {
                if (listener != null)
                {
                    listener.Stop();
                }
            }
        }

        /// <summary>
        /// Connects a particular pipe.
        /// </summary>
        private bool ConnectToPipe(PipeID pipeId, INamedPipe pipe)
        {
            if (pipe.IsConnected())
            {
                return true;
            }

            _log.MessageCtx(LogCtx, "Waiting for client '{0}' to connect to pipe '{1}' on port {2}...", this.ExeName, pipeId.ToString(), pipeId.Port);

            bool result = pipe.Create(pipeId, false);
            if (result)
            {
                _log.MessageCtx(LogCtx, "Connected to pipe '{0}'.", pipeId.ToString());
            }
            else
            {
                _log.MessageCtx(LogCtx, "Could not connect to pipe '{0}'!", pipeId.ToString());
            }

            return result;
        }
        #endregion // Private Methods

        #region IDisposable Implementation
        /// <summary>
        /// Make sure the reserved sockets are correctly disposed of.
        /// </summary>
        public void Dispose()
        {
            StopListeners(m_reservedSockets);
            m_reservedSockets.Clear();

            if (PacketProcessor.WidgetLogTarget != null)
            {
                PacketProcessor.WidgetLogTarget.Flush();
            }
        }
        #endregion // IDisposable Implementation
    } // TrayApplication
}
