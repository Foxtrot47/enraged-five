﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Rag.Services.Exceptions
{
    /// <summary>
    /// Custom exception that is thrown when a widget doesn't exist.
    /// </summary>
    public class WidgetNotFoundException : WidgetException
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="WidgetNotFoundException"/> class
        /// based off a widget path.
        /// </summary>
        /// <param name="widgetPath"></param>
        public WidgetNotFoundException(String widgetPath)
            : base(widgetPath)
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="WidgetNotFoundException"/> class
        /// based off a widget id.
        /// </summary>
        /// <param name="widgetId"></param>
        public WidgetNotFoundException(uint widgetId)
            : base(widgetId)
        {
        }
        #endregion
    }
}
