﻿namespace ragTray
{
    partial class CloseConnectionToGameForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CloseConnectionToGameForm));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.buttonYes = new System.Windows.Forms.Button();
            this.buttonNo = new System.Windows.Forms.Button();
            this.buttonStopAskingMe = new System.Windows.Forms.Button();
            this.headerLabel = new System.Windows.Forms.Label();
            this.animTimer = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(62, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(272, 29);
            this.label1.TabIndex = 0;
            this.label1.Text = "The game has stopped responding to the Proxy.  Do you want to close the Proxy\'s c" +
    "onnection to the game?";
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(65, 93);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(269, 26);
            this.label2.TabIndex = 1;
            this.label2.Text = "To prevent this message appearing, uncheck the \'Automatically verify link to game" +
    "\' option in the Proxy\'s context menu.";
            // 
            // buttonYes
            // 
            this.buttonYes.Location = new System.Drawing.Point(12, 67);
            this.buttonYes.Name = "buttonYes";
            this.buttonYes.Size = new System.Drawing.Size(91, 23);
            this.buttonYes.TabIndex = 2;
            this.buttonYes.Text = "Yes, close.";
            this.buttonYes.UseVisualStyleBackColor = true;
            this.buttonYes.Click += new System.EventHandler(this.buttonYes_Click);
            // 
            // buttonNo
            // 
            this.buttonNo.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonNo.Location = new System.Drawing.Point(109, 67);
            this.buttonNo.Name = "buttonNo";
            this.buttonNo.Size = new System.Drawing.Size(121, 23);
            this.buttonNo.TabIndex = 3;
            this.buttonNo.Text = "No, keep retrying.";
            this.buttonNo.UseVisualStyleBackColor = true;
            this.buttonNo.Click += new System.EventHandler(this.buttonNo_Click);
            // 
            // buttonStopAskingMe
            // 
            this.buttonStopAskingMe.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonStopAskingMe.Location = new System.Drawing.Point(236, 67);
            this.buttonStopAskingMe.Name = "buttonStopAskingMe";
            this.buttonStopAskingMe.Size = new System.Drawing.Size(143, 23);
            this.buttonStopAskingMe.TabIndex = 4;
            this.buttonStopAskingMe.Text = "No, and don\'t ask again.";
            this.buttonStopAskingMe.UseVisualStyleBackColor = true;
            this.buttonStopAskingMe.Click += new System.EventHandler(this.buttonStopAskingMe_Click);
            // 
            // headerLabel
            // 
            this.headerLabel.BackColor = System.Drawing.Color.Orange;
            this.headerLabel.Dock = System.Windows.Forms.DockStyle.Top;
            this.headerLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.headerLabel.Location = new System.Drawing.Point(0, 0);
            this.headerLabel.Margin = new System.Windows.Forms.Padding(0);
            this.headerLabel.Name = "headerLabel";
            this.headerLabel.Size = new System.Drawing.Size(391, 30);
            this.headerLabel.TabIndex = 5;
            this.headerLabel.Text = "{0} @ {1} has stopped responding.";
            this.headerLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // animTimer
            // 
            this.animTimer.Interval = 10;
            this.animTimer.Tick += new System.EventHandler(this.animTimer_Tick);
            // 
            // CloseConnectionToGameForm
            // 
            this.AcceptButton = this.buttonNo;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.buttonNo;
            this.ClientSize = new System.Drawing.Size(391, 125);
            this.Controls.Add(this.headerLabel);
            this.Controls.Add(this.buttonStopAskingMe);
            this.Controls.Add(this.buttonNo);
            this.Controls.Add(this.buttonYes);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CloseConnectionToGameForm";
            this.Text = "Proxy detected a problem with the link to game";
            this.Load += new System.EventHandler(this.CloseConnectionToGameForm_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button buttonYes;
        private System.Windows.Forms.Button buttonNo;
        private System.Windows.Forms.Button buttonStopAskingMe;
        private System.Windows.Forms.Label headerLabel;
        private System.Windows.Forms.Timer animTimer;

    }
}