using System;
using System.Collections.Generic;

using ragCore;
using System.IO;
using ragCompression;
using System.Diagnostics;

namespace ragTray
{
    /// <summary>
    /// Summary description for BankPipe.
    /// </summary>
    public class InternalBankPipe : BankPipe
    {
        private Queue<NetworkData> m_Queue;
        public InternalBankPipe( BankPipe pipe )
            : base( pipe )
        {
            m_Queue = new Queue<NetworkData>();
        }

        public override int ReadData( byte[] buffer, int numBytesToRead, bool readExact )
        {
            NetworkData data = null;
            lock ( m_Queue )
            {
                if ( m_Queue.Count < 0 )
                {
                    return 0;
                }

                data = m_Queue.Dequeue();
            }


            int totalLength = data.Count;

            noCompressionData compresser = new noCompressionData();
            compresser.CompressedSize = compresser.DecompressedSize = (uint)totalLength;
            totalLength += compresser.GetHeaderSize();

            //byte[] updateBytes = new byte[totalLength];
            int offset = compresser.BuildHeader( buffer, 0 );

            Buffer.BlockCopy( data.Data, 0, buffer, offset, data.Count );
            offset += data.Count;         

            if ( offset > buffer.Length )
            {
                throw new ArgumentException( "buffer not big enough and it should be" );
            }

            Debug.Assert( numBytesToRead == totalLength );

            //Buffer.BlockCopy( data.Data, 0, buffer, 0, numBytesToRead );
            return numBytesToRead;
        }

        public override int WriteData( byte[] buffer, int numBytesToWrite )
        {
            NetworkData data = new NetworkData( (byte[])buffer.Clone(), numBytesToWrite );
            lock ( m_Queue )
            {
                m_Queue.Enqueue( data );
            }

            return numBytesToWrite;
        }

        public override bool HasData()
        {
            return m_Queue.Count > 0;
        }

        public override int GetNumBytesAvailable()
        {
            lock ( m_Queue )
            {
                if ( m_Queue.Count < 0 )
                {
                    return 0;
                }

                NetworkData data = m_Queue.Peek();
                return data.Count + 9;
            }
        }
    }

    public class StreamBankPipe : BankPipe
    {
        private Stream m_Stream;
        public StreamBankPipe( Stream stream )
        {
            m_Stream = stream;
        }

        public override bool IsConnected()
        {
            return true;
        }

        public override int ReadData( byte[] buffer, int numBytesToRead, bool readExact )
        {
            int numBytesRead = 0;
            try
            {
                do
                {
                    int thisRead = m_Stream.Read( buffer, numBytesRead, numBytesToRead );
                    numBytesRead += thisRead;
                    numBytesToRead -= thisRead;
                } while ( readExact && numBytesToRead > 0 );
            }
            catch ( Exception e )
            {
                OnReadWriteFailure( this, e );

                m_IsConnected = false;
            }

            return numBytesRead;
        }

        public override int WriteData( byte[] buffer, int numBytesToWrite )
        {
            try
            {
                m_Stream.Write( buffer, 0, numBytesToWrite );
            }
            catch ( Exception e )
            {
                OnReadWriteFailure( this, e );

                m_IsConnected = false;
                return 0;
            }

            return numBytesToWrite;
        }

        public override int GetNumBytesAvailable()
        {
            return (int)m_Stream.Length;
        }
    }


    public class HubStreamPipe : BankPipe
    {
        private List<NetworkData> m_DestData;

        public HubStreamPipe( List<NetworkData> data, BankPipe pipe )
            : base( pipe )
        {
            m_DestData = data;
        }

        public override bool IsConnected()
        {
            return true;
        }

        public override int ReadData( byte[] buffer, int numBytesToRead, bool readExact )
        {
            throw new NotImplementedException();
        }

        public override int WriteData( byte[] buffer, int numBytesToWrite )
        {
            NetworkData data = new NetworkData( buffer, numBytesToWrite );
            m_DestData.Add( data );
            return numBytesToWrite;
        }

        public override int GetNumBytesAvailable()
        {
            throw new NotImplementedException();
        }
    }
}
