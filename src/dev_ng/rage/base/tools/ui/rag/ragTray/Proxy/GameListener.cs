﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Net.Sockets;
using ragCore;
using System.Net;
using RSG.Base.Logging;
using RSG.Base.Configuration.Rag;

namespace ragTray
{
    /// <summary>
    /// Service that listens for incoming game connections.
    /// TODO: This should be "owned" by the hub manager as it can't function correctly without it.
    /// </summary>
    internal class GameListener
    {
        #region Constants
        /// <summary>
        /// Port the listener listens on.
        /// </summary>
        private const int c_port = 2000;

        /// <summary>
        /// Log message context.
        /// </summary>
        private const String LogCtx = "Game Listener";
        #endregion // Constants

        #region Member Data
        /// <summary>
        /// Reference to the rag configuration object.
        /// </summary>
        private readonly IRagConfig _ragConfig;

        /// <summary>
        /// Reference to the hub manager.
        /// </summary
        private HubManager HubManager { get; set; }

        /// <summary>
        /// TCP listener that is listening for new game connections.
        /// </summary>
        private TcpListener Listener { get; set; }

        /// <summary>
        /// Flag indicating whether the game listener is active.
        /// </summary>
        private bool ListenerActive { get; set; }

        /// <summary>
        /// Reference to the log object to make use of.
        /// </summary>
        private readonly ILog _log;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Main constructor.
        /// </summary>
        /// <param name="hubManager"></param>
        internal GameListener(HubManager hubManager, ILog log, IRagConfig ragConfig)
        {
            _log = log;
            _ragConfig = ragConfig;
            HubManager = hubManager;
            ListenerActive = true;

            _log.MessageCtx(LogCtx, "Starting GameListener.");
            Listener = new TcpListener(IPAddress.Any, c_port);

            // Don't linger
            //LingerOption lingerOption = new LingerOption(true, 0);
            //Listener.Server.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.Linger, lingerOption);

            Listener.Start();
            Listener.BeginAcceptSocket(DoAcceptSocketCallback, Listener);
        }
        #endregion // Constructor(s)

        #region Public Methods
        /// <summary>
        /// Call this on application shutdown.
        /// </summary>
        public void Shutdown()
        {
            _log.MessageCtx(LogCtx, "Shutting down GameListener.");
            ListenerActive = false;
            Listener.Stop();
        }
        #endregion // Public Methods

        #region Async Methods
        /// <summary>
        /// Callback for when a socket was accepted.
        /// </summary>
        /// <param name="ar"></param>
        public void DoAcceptSocketCallback(IAsyncResult ar)
        {
            // Get the listener that handles the client request.
            TcpListener listener = (TcpListener)ar.AsyncState;

            if (ListenerActive)
            {
                Socket clientSocket = listener.EndAcceptSocket(ar);

                // Start the listener again.
                listener.BeginAcceptSocket(DoAcceptSocketCallback, listener);

                IPEndPoint remoteEndPoint = (IPEndPoint)clientSocket.RemoteEndPoint;
                if (_ragConfig != null && _ragConfig.ShouldIgnoreConnectionsFrom(remoteEndPoint.Address))
                {
                    _log.MessageCtx(LogCtx, "Ignoring new connection from {0}.", clientSocket.RemoteEndPoint);
                    clientSocket.Close();
                }
                else
                {
                    _log.MessageCtx(LogCtx, "Detected new connection from {0} to {1}.",
                        clientSocket.RemoteEndPoint, clientSocket.LocalEndPoint);
                    NamedPipeSocket pipe = new NamedPipeSocket(clientSocket);

                    // Tell the hub manager to connect to the game.
                    HubManager.AttachPipeToGameProxy(pipe, DateTime.UtcNow);
                }
            }
        }
        #endregion // Async Methods
    } // GameListener
}
