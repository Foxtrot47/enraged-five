using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Text;
using System.Threading;
using System.Windows.Forms;

using ragCore;
using RSG.Base.Forms;
using ragWidgets;

namespace ragTray
{
	/// <summary>
	/// Summary description for BankManager.
	/// </summary>
	public class TrayBankManager : IBankManager
	{
        private uint m_id;
        private TrayApplicationManager m_AppManager;
        public TrayBankManager(TrayApplicationManager appManager)
		{
            m_AppManager = appManager;
            m_PingEventHandlers = new List<EventHandler>();
            m_DelayedPingEventHandlers = new List<EventHandler>();
		}
        
		public delegate void TimeStringDelegate(String str);
		public event ragCore.InitFinishedHandler InitFinished;
		public static event TimeStringDelegate TimeStringHandler;

		public static int GetWidgetTypeGUID() {return GetStaticGUID();}
		public static int GetStaticGUID()		{return Widget.ComputeGUID('b','m','g','r');}

        private bool RemoteHandler( BankRemotePacket packet, out String errorMessage )
        {
            bool handled = false;
            errorMessage = null;

            switch ( packet.BankCommand )
            {
                // open file dialog support #1:
                case BankRemotePacket.EnumPacketType.USER_0:
                    {
                        packet.Begin();
                        string origDest = packet.Read_const_char();
                        uint maxDest = packet.Read_u32();
                        string mask = packet.Read_const_char();
                        bool save = packet.Read_bool();
                        string desc = packet.Read_const_char();
                        packet.End();

                        string filter;
                        filter = (desc != string.Empty ? desc : "(" + mask + ")") + "|" + mask;
                        filter += "|" + "All Files (*.*)|*.*";
                        if ( save )
                        {
                            SaveFileDialog saveDialog = new SaveFileDialog();
                            saveDialog.Filter = filter;
                            DialogResult result = STAFileDialog.ShowDialog( saveDialog, true );
                            BankRemotePacket p = new BankRemotePacket( packet.Pipe as BankPipe );
                            p.Begin(BankRemotePacket.EnumPacketType.USER_0, GetWidgetTypeGUID(), m_id);
                            p.Write_bool( result == DialogResult.OK );
                            if ( result == DialogResult.OK )
                                p.Write_const_char( saveDialog.FileName );
                            p.Send();
                        }
                        else
                        {
                            OpenFileDialog openDialog = new OpenFileDialog();
                            openDialog.CheckFileExists = true;
                            openDialog.Filter = filter;
                            openDialog.Multiselect = false;
                            DialogResult result = STAFileDialog.ShowDialog( openDialog, true );
                            BankRemotePacket p = new BankRemotePacket( packet.Pipe as BankPipe );
                            p.Begin(BankRemotePacket.EnumPacketType.USER_0, GetWidgetTypeGUID(), m_id);
                            p.Write_bool( result == DialogResult.OK );
                            if ( result == DialogResult.OK )
                                p.Write_const_char( openDialog.FileName );
                            p.Send();
                        }

                        handled = true;
                    }
                    break;

                // signal that the client has finished his initialization phase:
                case BankRemotePacket.EnumPacketType.USER_2:
                    {
                        TrayApplication app = m_AppManager.FindApplication( packet.Pipe );
                        TrayBankManager bankMgr = app.BankMgr as TrayBankManager;

                        bankMgr.OnInitFinished();

                        // read saved widget information:
                        bankMgr.DeSerializeBanks(app.ExeName);
                        handled = true;
                    }
                    break;

                // see if its timer information:
                case BankRemotePacket.EnumPacketType.USER_5:
                    {
                        packet.Begin();

                        String timeString = packet.Read_const_char();
                        if ( TimeStringHandler != null )
                            TimeStringHandler( timeString );

                        packet.End();
                        handled = true;
                    }
                    break;

                // see if it's a ping response
                case BankRemotePacket.EnumPacketType.USER_6:
                    {
                        packet.Begin();
                        uint pingId = packet.Read_u32();
                        bool delayed = packet.Read_bool();
                        packet.End();

                        // Call the ping response.
                        TrayApplication app = m_AppManager.FindApplication( packet.Pipe );
                        TrayBankManager bankMgr = app.BankMgr as TrayBankManager;

                        bankMgr.OnPingResponse(delayed);
                        handled = true;
                    }
                    break;

                // CLIPBOARD_COPY:
                case BankRemotePacket.EnumPacketType.USER_7:
                    {
                        packet.Begin();
                        string theString = packet.Read_const_char();
                        // update clipboard string:
                        TrayApplication app = m_AppManager.FindApplication( packet.Pipe );
                        TrayBankManager bankMgr = app.BankMgr as TrayBankManager;
                        bankMgr.m_ClipboardString += theString;
                        Clipboard.SetDataObject( bankMgr.m_ClipboardString, true );
                        packet.End();
                        handled = true;
                    }
                    break;

                // CLIPBOARD_CLEAR:
                case BankRemotePacket.EnumPacketType.USER_8:
                    {
                        packet.Begin();
                        TrayApplication app = m_AppManager.FindApplication( packet.Pipe );
                        TrayBankManager bankMgr = app.BankMgr as TrayBankManager;
                        bankMgr.m_ClipboardString = "";
                        Clipboard.SetDataObject( bankMgr.m_ClipboardString, true );
                        packet.End();
                        handled = true;
                    }
                    break;

                case BankRemotePacket.EnumPacketType.CREATE:
                    m_id = packet.Id;
                    handled = true;
                    break;

                case BankRemotePacket.EnumPacketType.CHANGED:
                // CREATE_OUTPUT_WINDOW:
                case BankRemotePacket.EnumPacketType.USER_14:
                // INFORM_SUB_CHANNEL:
                case BankRemotePacket.EnumPacketType.USER_15:
                    handled = true;
                    break;

                default:
                    handled = Widget.RemoteHandlerBase(packet, GetStaticGUID(), out errorMessage);
                    break;
            }

            return handled;
        }

		protected void OnPingResponse(bool delayed) 
		{
            if (delayed == false)
            {
                lock (m_PingEventHandlers)
                {
                    foreach ( EventHandler eventHandle in m_PingEventHandlers )
                    {
                        eventHandle(this, System.EventArgs.Empty);
                    }

                    m_PingEventHandlers.Clear();
                }
            }
            else
            {
                lock (m_DelayedPingEventHandlers)
                {
                    foreach( EventHandler eventHandle in m_DelayedPingEventHandlers )
                    {
                        eventHandle(this, System.EventArgs.Empty);
                    }

                    m_DelayedPingEventHandlers.Clear();
                }
            }
		}

        private List<EventHandler> m_PingEventHandlers;
        private List<EventHandler> m_DelayedPingEventHandlers;
        private uint m_pingId = 0;

		public void SendDelayedPing(EventHandler callback, BankPipe pipe)
		{
            lock (m_PingEventHandlers)
            {
                
                BankRemotePacket p = new BankRemotePacket(pipe);
                p.Begin(BankRemotePacket.EnumPacketType.USER_6, GetWidgetTypeGUID(), m_id);
                p.Write_u32(m_pingId++);
                p.Write_bool(true);
                p.Send();

                m_DelayedPingEventHandlers.Add(callback);
            }
		}

        public void SendPing(EventHandler callback, BankPipe pipe)
        {

            lock(m_PingEventHandlers)
            {
                BankRemotePacket p = new BankRemotePacket(pipe);
                p.Begin(BankRemotePacket.EnumPacketType.USER_6, GetWidgetTypeGUID(), m_id);
                p.Write_u32(m_pingId++);
                p.Write_bool(false);
                p.Send();

                m_PingEventHandlers.Add(callback);
            }
        }

        public void OnInitFinished()
        {
            if ( this.InitFinished != null )
            {
                this.InitFinished();

                // DO NOT CLEAR this.InitFinished = null; // clear out these events since we've hit the one time event
            }

            if ( !m_initFinished )
            {
                //throw new NotImplementedException();
                //Widget.WidgetAdded += new WidgetEventHandler( Widget_WidgetAdded );
            }

            m_initFinished = true;
        }
        public void AddHandlers( BankRemotePacketProcessor packetProcessor )
		{
            packetProcessor.AddType( GetStaticGUID(), new BankRemotePacketProcessor.HandlerDel( this.RemoteHandler ) );
		}

        public void RemoveHandlers( BankRemotePacketProcessor packetProcessor )
        {
            packetProcessor.RemoveType( GetStaticGUID() );
        }

		public void AddBank(WidgetBank bank)
		{
			if (!m_Banks.Contains(bank))
				m_Banks.Add(bank);
		}

		public void RemoveBank(WidgetBank bank)
		{
			m_Banks.Remove(bank);
		}

        public List<Widget> AllBanks
		{
			get { return m_Banks; }
		}

        public void SerializeBanks( String appName )
        {
            throw new NotImplementedException();
        }

		public void DeSerializeBanks(String appName)
		{
		}

		public Widget FindFirstWidgetFromPath(string path)
		{
			List<Widget> list = FindWidgetsFromPath(path);
			if (list == null || list.Count == 0) 
			{
				return null;
			}
			return list[0];
		}

		public List<Widget> FindWidgetsFromPath(string path)
		{
            if (path == "/")
            {
                return m_Banks;
            }
			return WidgetGroupBase.FindWidgetsFromPath(null, m_Banks, path);
		}

        public List<Widget> FindWidgetsFromPath( String[] path ) 
		{
            if (path.Length == 0)
            {
                return m_Banks;
            }
			return WidgetGroupBase.FindWidgetsFromPath(null, m_Banks, path, 0);
		}

        public void Reset()
        {
            //Force the ping response to any proxies since the program has disconnected from the game.
            //A normal response will not occur and proxies may be waiting for this signal.
            OnPingResponse(false);
            OnPingResponse(true);
        }

        private List<Widget> m_Banks = new List<Widget>();
		private string m_ClipboardString="";
        private bool m_initFinished = false;

        public bool IsInitFinished
        {
            get
            {
                return m_initFinished;
            }
        }


        public Widget FindWidgetFromID( uint id )
        {
            if (m_AppManager == null || m_AppManager.MainApplication == null)
            {
                return null;
            }

            Widget widget;
            String errorMessage;
            m_AppManager.MainApplication.BankPipe.RemoteToLocal(id, out widget, out errorMessage);
            return widget;
        }
    }
}
