﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ragTray
{
    public partial class TrayIconHoverForm : Form
    {
        private HubManager m_HubManager;

        public Point LastKnownMouseHoverPos { get; set; }

        public TrayIconHoverForm( HubManager hubManager )
        {
            InitializeComponent();

            this.Opacity = 0.01;
            m_HubManager = hubManager;
            mouseTimer.Start();
        }

        private void timer1_Tick( object sender, EventArgs e )
        {
            ResetUI();
            BuildUI();
        }

        private void UpdateVisibility()
        {

            int left = LastKnownMouseHoverPos.X - Width / 2;
            int top = LastKnownMouseHoverPos.Y - Height - 10;

            left = Math.Max( left, 20 );
            left = Math.Min( left, SystemInformation.VirtualScreen.Right - 100 );

            top = Math.Max( top, 20 );
            if ( top < 300 )
            {
                top = Math.Max( top, LastKnownMouseHoverPos.Y + 10 );
            }

            Left = left;
            Top = top;

            bool mouseCursorNearIcon = false;

            Point worldspaceMousePos = new Point(
                 Cursor.Position.X,
                 Cursor.Position.Y );

            if ( Math.Abs( LastKnownMouseHoverPos.X - worldspaceMousePos.X ) < 10 ||
                Math.Abs( LastKnownMouseHoverPos.Y - worldspaceMousePos.Y ) < 10 )
            {
                mouseCursorNearIcon = true;
            }

            if ( !mouseCursorNearIcon )
            {
                //Top = Math.Min( Top, SystemInformation.VirtualScreen.Right - 100 );

                this.Opacity *= 0.9;
                if ( Opacity < 0.0005 )
                {
                    this.Opacity = 0.0005;
                    this.Visible = false;
                }
            }
            else if ( this.Visible )
            {
                if ( Opacity >= 0.95 )
                {
                    Opacity = 0.95;
                }
                else
                {
                    this.Opacity *= 1.2;
                }

            }
        }

        private void BuildUI()
        {
            int maxX = 0;
            Label header = new Label();
            header.BackColor = Color.Orange;
            header.Top = 0;
            header.Width = 200;
            header.Text = "Proxy Game Connections";
            header.TextAlign = ContentAlignment.MiddleCenter;
            this.Controls.Add( header );

            int y = header.Height;

            IEnumerable<ProxyHub> hubs = m_HubManager.ActiveHubs;

            if (hubs.Any())
            {
                foreach (ProxyHub hub in hubs)
                {
                    Label hubHeader = new Label();
                    hubHeader.BackColor = Color.CornflowerBlue;
                    hubHeader.Top = y;
                    hubHeader.Width = 200;
                    hubHeader.TextAlign = ContentAlignment.MiddleLeft;

                    if (hub.ServiceManager.GameService == null)
                    {
                        hubHeader.Text = "<unconnected>" + Environment.NewLine;
                    }
                    else
                    {
                        hubHeader.Text = hub.ServiceManager.GameService.GetPlatform() + " @ " + hub.ServiceManager.GameService.GetGameIP();
                    }

                    if (hub == m_HubManager.DefaultHub)
                    {
                        int top = (hubHeader.Height - 16) / 2;

                        PictureBox defaultImage = new PictureBox();
                        defaultImage.Image = Properties.Resources._default;
                        defaultImage.BackColor = Color.CornflowerBlue;
                        defaultImage.Top = y + top;
                        defaultImage.Left = 200 - 16 - top;
                        defaultImage.Width = 16;
                        defaultImage.Height = 16;
                        this.Controls.Add(defaultImage);
                    }

                    y += hubHeader.Height;
                    maxX = Math.Max(maxX, hubHeader.Width);
                    this.Controls.Add(hubHeader);

                    foreach (String desc in hub.GetProxyDescriptions())
                    {
                        Label proxyLabel = new Label();
                        proxyLabel.Top = y;
                        proxyLabel.Left = 20;
                        proxyLabel.Width = 180;
                        proxyLabel.TextAlign = ContentAlignment.MiddleLeft;
                        proxyLabel.ForeColor = Color.White;
                        proxyLabel.Text = desc;

                        y += proxyLabel.Height;
                        maxX = Math.Max(maxX, proxyLabel.Width);
                        this.Controls.Add(proxyLabel);
                    }

                    y += 10;
                }
            }
            else
            {
                Label hubHeader = new Label();
                hubHeader.BackColor = Color.CornflowerBlue;
                hubHeader.Top = y;
                hubHeader.Width = 200;
                hubHeader.TextAlign = ContentAlignment.MiddleLeft;
                hubHeader.Text = "<<no connections>>";

                y += hubHeader.Height;
                maxX = Math.Max(maxX, hubHeader.Width);
                this.Controls.Add(hubHeader);
            }

            this.Height = y;
            this.Width = maxX;
        }

        private void ResetUI()
        {
            this.Controls.Clear();
        }

        private void TrayIconHoverForm_VisibleChanged( object sender, EventArgs e )
        {
            if ( Visible )
            {
                ResetUI();
                BuildUI();
                timer1.Start();
            }
            else
            {
                timer1.Stop();
            }
        }

        private void mouseTimer_Tick( object sender, EventArgs e )
        {
            UpdateVisibility();
        }
    }
}
