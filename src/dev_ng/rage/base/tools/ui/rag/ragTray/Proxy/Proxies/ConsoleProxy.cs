﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Net.Sockets;
using System.Diagnostics;
using ragCore;
using ragWidgets;
using RSG.Base.Extensions;
using System.Net;
using ragCompression;
using RSG.Base.Logging;
using ragTray.Proxy.Proxies;

namespace ragTray
{
    public class Command
    {
        public delegate object CommandDelegate(string[] args);
        public delegate void ProcessResultCallback(TcpClient client, object result, bool executedRemotely);

        public Command(CommandDelegate command, ProcessResultCallback resultCallback)
        {
            m_Command = command;
            m_ResultCallback = resultCallback;
        }

        public CommandDelegate m_Command;
        public ProcessResultCallback m_ResultCallback;
    }

    /// <summary>
    /// 
    /// </summary>
    public interface IConsoleProxy
    {
        int ConnectionPort { get; }

        INamedPipe ConsolePipe { get; }
        object ConsolePipeLock { get; }
    } // IConsoleService

    /// <summary>
    /// Proxy interface for speaking with the game via console commands.
    /// </summary>
    internal sealed class ConsoleProxy : AppProxy, IConsoleProxy
    {
        #region Constants
        /// <summary>
        /// Name of this app proxy.
        /// </summary>
        private const String c_name = "Console Proxy";

        /// <summary>
        /// Default listening port.
        /// </summary>
        private const int c_defaultListeningPort = 5105;

        /// <summary>
        /// Regex for finding \n's not preceeded by \r.
        /// </summary>
        private static readonly Regex c_findBareNL = new Regex(@"(?<!\r)\n");

        /// <summary>
        /// String to show when RAG isn't connected to the game.
        /// </summary>
        private const String c_BankManagerError = "Bank Manager is not loaded.  RAG is not connected to the game.";

        /// <summary>
        /// ASCII encoder to use for sending messages.
        /// </summary>
        private static readonly ASCIIEncoding c_encoder = new ASCIIEncoding();

        /// <summary>
        /// Flag indicating whether we should be using DOS style new lines.
        /// </summary>
        private const bool c_useDosNewLines = true;

        /// <summary>
        /// 
        /// </summary>
        private const String c_commandTimeoutMessage = "Command was unable to complete due to a timeout.";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Listener for incoming connections.
        /// </summary>
        private TcpListener RemoteConnectionListener { get; set; }

        /// <summary>
        /// Port the listener is monitoring for incoming console proxy connections.
        /// (-1 because hub UIDs start at 1)
        /// </summary>
        public int ConnectionPort { get; set; }

        /// <summary>
        /// Flag indicating whether the remote connection listener is running.
        /// </summary>
        private bool ListenerActive { get; set; }
        #endregion // Properties

        #region Member Data
        /// <summary>
        /// Mapping of tcp clients to threads that are handling requests for that thread.
        /// </summary>
        private Dictionary<TcpClient, Thread> m_ConnectedClients;
        private Dictionary<string, Command> m_CommandMaps = new Dictionary<string, Command>();
        private object m_CommandLock = new object();

        private bool m_ConnectedToGame;
        private string m_PlatformLoaded;
        private string m_BuildConfigLoaded;
        private IBankManager m_BankManager;

        /// <summary>
        /// Widgets that have get updates enabled on them.
        /// </summary>
        private ISet<Widget> m_GetUpdatesWidgets;

        /// <summary>
        /// Reference to the pipe used for communicating with the game's console pipe.
        /// </summary>
        private readonly INamedPipe _consolePipe;

        /// <summary>
        /// Lock object for sending/receiving commands on the console pipe.
        /// </summary>
        private readonly object _consolePipeLock = new object();
        #endregion // Member Data

        #region Properties
        /// <summary>
        /// Lock object for sending/receiving commands on the console pipe.
        /// </summary>
        public object ConsolePipeLock
        {
            get { return _consolePipeLock; }
        }

        /// <summary>
        /// 
        /// </summary>
        public INamedPipe ConsolePipe
        {
            get { return _consolePipe; }
        }
        #endregion

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public ConsoleProxy(ProxyHub hub, INamedPipe consolePipe)
            : base(c_name, hub)
        {
            ConnectionPort = c_defaultListeningPort;

            hub.ServiceManager.ConsoleService = this;
            _consolePipe = consolePipe;

            m_ConnectedClients = new Dictionary<TcpClient,Thread>();
            m_ConnectedToGame = false;

            m_GetUpdatesWidgets = new HashSet<Widget>();

            SubscribeToStream("bankraw", hub.ServiceManager);

            Command.ProcessResultCallback resultCb = delegate(TcpClient resultClient, object resultObject, bool executedRemotely)
            {
                string result = (string)resultObject;
                result = ConvertNewlines(result);
                result += "\0";
                byte[] resultData = c_encoder.GetBytes(result);
                resultClient.GetStream().Write(resultData, 0, resultData.Length);
            };

            Command.ProcessResultCallback nullResultCb = delegate(TcpClient resultClient, object resultObject, bool executedRemotely)
            { };

            Command.ProcessResultCallback widgetIdResultCb = delegate(TcpClient resultClient, object resultObject, bool executedRemotely)
            {
                int result = Convert.ToInt32(resultObject);
                byte[] resultBytes = BitConverter.GetBytes(result);
                resultClient.GetStream().Write(resultBytes, 0, resultBytes.Length);
            };

            // Register the commands that the console proxy can listen to.
            AddCommand("widget", WidgetCommand, resultCb);
            AddCommand("wgid", SendWidgetIdCommand, widgetIdResultCb);

            AddCommand("widget_id", AcquireLocalWidgetIDCommand, resultCb);
            AddCommand("sync", SynchronizeCommand, resultCb);
            AddCommand("rc_ping", PingCommand, resultCb);
            AddCommand("getupdates", GetUpdatesCommand, resultCb);
            AddCommand("gameconnected", IsGameConnected, resultCb);
            AddCommand("widget_exists", WidgetExistCommand, resultCb );
            AddCommand("widget_exists_callback", WidgetExistCallbackCommand, resultCb);
            AddCommand("getplatform", PlatformCommand, resultCb );
            AddCommand("getbuildconfig", BuildConfigCommand, resultCb);
            AddCommand("getgameip", GetGameIPCommand, resultCb );
            AddCommand("getproxystatus", GetProxyStatus, resultCb );
            AddCommand("getexecutablename", GetExecutableNameCommand, resultCb);

            hub.ServiceManager.GameService.GameConnected += OnGameConnected;
            hub.ServiceManager.GameService.GameDisconnected += OnGameDisconnected;

            m_ConnectedToGame = hub.ServiceManager.GameService.IsConnected();
            if ( m_ConnectedToGame )
            {
                m_BankManager = m_ServiceManager.GameService.GetBankManager();
                m_PlatformLoaded = m_ServiceManager.GameService.GetPlatform();
                m_BuildConfigLoaded = m_ServiceManager.GameService.GetBuildConfig();

                // Temporarily do this in a try catch.  The max remote connection stuff doesn't query the proxy for
                // the port that it is should use, just using the default one (5105).
                Log("Starting ConsoleProxy Listener.");
                try
                {
                    RemoteConnectionListener = new TcpListener(IPAddress.Any, ConnectionPort);
                    RemoteConnectionListener.Start();
                    RemoteConnectionListener.BeginAcceptTcpClient(DoAcceptTcpClientCallback, RemoteConnectionListener);
                    ListenerActive = true;
                }
                catch (SocketException)
                {
                    // If we enter this block, max will never be able to connect to the proxy because it only ever uses
                    // port 5105.
                    Log("Could not start listening on port: " + RemoteConnectionListener.LocalEndpoint);
                    Log("Trying alternative port...");
                    try
                    {
                        ConnectionPort = c_defaultListeningPort + (UID % 100) + 1;
                        RemoteConnectionListener = new TcpListener(IPAddress.Any, ConnectionPort);
                        RemoteConnectionListener.Start();
                        RemoteConnectionListener.BeginAcceptTcpClient(DoAcceptTcpClientCallback, RemoteConnectionListener);
                        ListenerActive = true;
                    }
                    catch (SocketException)
                    {
                        Log("Could not start listening on port: " + RemoteConnectionListener.LocalEndpoint);
                        State = ProxyState.Stopping;
                    }
                }
                State = AppProxy.ProxyState.ReadyIdle;
            }
            else
            {
                Log( "Not connected to game, stopping..." );
                State = ProxyState.Stopping;
            }
        }
        #endregion // Constructor(s)

        /// <summary>
        /// Safely adds a command to the console to be specially handled.
        /// </summary>
        private void AddCommand(String commandName, Command.CommandDelegate command, Command.ProcessResultCallback resultCallback)
        {
            commandName = commandName.ToLower();
            IEnumerable<KeyValuePair<String, Command>> duplicateCommands = m_CommandMaps.Where( x => (x.Key == commandName) );
            Debug.Assert(duplicateCommands.Count() == 0,
                String.Format("Command '{0}' already exists in the command map.  This will be ignored!", commandName));
            m_CommandMaps[commandName] = new Command(command, resultCallback);
        }

        //PURPOSE:  Remove all disconnected clients from the list.
        public override void Stop()
        {
            Log("Stop: Stopping listener and threads");
            if (ListenerActive)
            {
                ListenerActive = false;
                RemoteConnectionListener.Stop();
            }

            Log( "Stop: Closing clients: " + m_ConnectedClients.Count );
            lock (m_ConnectedClients)
            {
                foreach (KeyValuePair<TcpClient, Thread> item in m_ConnectedClients)
                {
                    TcpClient client = item.Key;
                    Thread thread = item.Value;

                    if (client != null && client.Connected == true)
                    {
                        Log("Closing client: " + client.Client.ToString());
                        client.Close();
                    }

                    if (thread != null)
                    {
                        Log( "Joining client thread: " + (client.Client == null ? "null" : client.Client.ToString()) );
                        if ( !thread.Join( 20 * 1000 ) )
                        {
                            Log( "Aborting client thread: " + (client.Client == null ? "null" : client.Client.ToString()) );
                            thread.Abort();
                        }
                    }
                }

                m_ConnectedClients.Clear();
            }

            foreach (var item in m_HubStreams)
            {
                item.Value.RemoveProxy(this);
            }

            base.Stop();
        }

        public override void ShutDown()
        {
            Log( "Stopping..." );
            State = ProxyState.Stopping;
        }

        /// <summary>
        /// 
        /// </summary>
        private void DoAcceptTcpClientCallback(IAsyncResult ar)
        {
            // Get the listener that handles the client request.
            TcpListener listener = (TcpListener)ar.AsyncState;

            if (ListenerActive)
            {
                // Grab the client and start the listener again.
                TcpClient client = listener.EndAcceptTcpClient(ar);
                listener.BeginAcceptTcpClient(DoAcceptTcpClientCallback, listener);

                // Make sure the nagle algorithm is switched off.
                client.NoDelay = true;

                Log(String.Format("Got a new connection {0}\n\n", ((IPEndPoint)client.Client.RemoteEndPoint)));
                State = AppProxy.ProxyState.ReadyIdle;

                //Launch the new client thread.
                lock (m_ConnectedClients)
                {
                    Thread clientThread = new Thread(ClientThread);
                    clientThread.IsBackground = true;
                    clientThread.Name = String.Format("Console Client Thread {0} @ {1}", UID, client.Client.RemoteEndPoint);
                    clientThread.Start(client);

                    m_ConnectedClients.Add(client, clientThread);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void ClientThread(object client)
        {
            TcpClient tcpClient = (TcpClient)client;
            IPEndPoint remoteEndPoint = (IPEndPoint)tcpClient.Client.RemoteEndPoint;
            Log("Console client connected from {0}", remoteEndPoint);

            char[] stringSeparators = new char[] { '\n' };
            string partialCommand = "";

            DataWriteCommand currentDataWriteCommand = null;            
            
            while (IsRunning)
            {
                try
                {
                    lock (tcpClient)
                    {
                        if (tcpClient.Connected == false)
                        {
                            Log("TCP Client Disconnected");
                            break;
                        }

                        int numBytes = tcpClient.Available;
                        if (numBytes == 0)
                        {
                            if (tcpClient.Client.Poll(0, SelectMode.SelectRead))
                            {
                                byte[] dummy = new byte[1];
                                Int32 data = tcpClient.Client.Receive(dummy, 1, SocketFlags.Peek);
                                if (data == 0)
                                {
                                    Log("Received 0 bytes breaking out of loop.");
                                    break;
                                }
                            }

							Thread.Sleep(10);
                            continue;
                        }

                        // Call this function for every command result
                        byte[] readBuffer = new byte[4096];
                        IList<byte> completeBuffer = new List<byte>();
                        int totalBytesRead = 0;

                        // Read in 4096 bytes of data at a time.
                        do 
                        {
                            int bytesRead = tcpClient.GetStream().Read(readBuffer, 0, readBuffer.Length);
                            completeBuffer.AddRange(readBuffer.Take(bytesRead));
                            totalBytesRead += bytesRead;
                        } while (tcpClient.GetStream().DataAvailable);

                        // Buffer to process.
                        byte[] buffer;
                        if (totalBytesRead <= 4096)
                        {
                            buffer = readBuffer;
                        }
                        else
                        {
                            buffer = completeBuffer.ToArray();
                        }
                        
                        int bufferOffset = 0;
                        while (bufferOffset < totalBytesRead)
                        {
                            char[] header = c_encoder.GetChars(buffer, bufferOffset, DataWriteCommand.HeaderOffset);

                            if (DataWriteCommand.CheckHeader(header))
                            {
                                // Request to write buffer to widget.
                                currentDataWriteCommand = new DataWriteCommand(buffer, bufferOffset);

                                if (currentDataWriteCommand.IsComplete())
                                {
                                    ExecuteWriteStreamCommand(currentDataWriteCommand);
                                    bufferOffset += currentDataWriteCommand.HeaderDataSize + currentDataWriteCommand.DataSize;
                                    currentDataWriteCommand = null;
                                }
                                else
                                {
                                    //Only part of the binary stream has been read in.
                                    bufferOffset += currentDataWriteCommand.HeaderDataSize + currentDataWriteCommand.CurrentDataSize;
                                }
                            }
                            else if (currentDataWriteCommand != null && currentDataWriteCommand.IsComplete() == false)
                            {
                                bufferOffset += currentDataWriteCommand.AddData(buffer, bufferOffset);

                                if (currentDataWriteCommand.IsComplete())
                                {
                                    ExecuteWriteStreamCommand(currentDataWriteCommand);
                                    currentDataWriteCommand = null;
                                }
                            }
                            else if (DataReadCommand.CheckHeader(header))
                            {
                                // Request to read widget buffer.
                                DataReadCommand readCommand = new DataReadCommand(buffer, bufferOffset, tcpClient.GetStream());
                                ExecuteReadStreamCommand(readCommand);
                                bufferOffset += readCommand.DataSize;
                            }
                            else
                            {
                                string bufferString = c_encoder.GetString(buffer, bufferOffset, totalBytesRead - bufferOffset);
                                string fullCommandStr = partialCommand + bufferString; // last bit of old string plus the new string
                                fullCommandStr.Replace("\r", ""); // get rid of any unnecessary \rs

                                int index = fullCommandStr.IndexOf('\n', 0); 
                                if (index == -1)
                                {
                                    partialCommand += fullCommandStr;
                                    bufferOffset += fullCommandStr.Length;
                                }
                                else
                                {
                                    string textCommand = fullCommandStr.Substring(0, index+1);
                                    ExecuteCommand(tcpClient, textCommand);

                                    bufferOffset += (index+1); //Include the newline.
                                    partialCommand = "";
                                }
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    ExceptionFormatter formatter = new ExceptionFormatter(e);
                    Log("ClientThread Exception");
                    Log(formatter.FormatHeading());
                    foreach (String line in formatter.Format())
                    {
                        Log(line);
                    }
                    break;
                }
            }

            Log("Console client disconnected from {0}", remoteEndPoint);
        }

        public override void Update()
        {
            if (!IsRunning)
            {
                Stop();
                return;
            }

            // Create a copy for threading reasons.
            Dictionary<TcpClient, Thread> connectedClients;
            lock (m_ConnectedClients)
            {
                connectedClients = new Dictionary<TcpClient, Thread>(m_ConnectedClients);
            }

            // Loop over all the connections making sure that both the thread and tcp clients are still operational.
            foreach (KeyValuePair<TcpClient, Thread> pair in connectedClients)
            {
                TcpClient tcpClient = pair.Key;
                Thread clientThread = pair.Value;

                if (clientThread.IsAlive == false || tcpClient.Connected == false)
                {
                    tcpClient.Close();
                    clientThread.Abort();
                    m_ConnectedClients.Remove(tcpClient);
                }
            }
        }

        public void ExecuteWriteStreamCommand(DataWriteCommand command)
        {
            Widget rawWidget = m_BankManager.FindWidgetFromID((uint)command.WidgetId);
            if (rawWidget != null)
            {
                if (rawWidget is WidgetData)
                {
                    WidgetData dataWidget = (WidgetData)rawWidget;
                    dataWidget.SetValue(command.Data);
                }
                else
                {
                    rawWidget.ReceiveData(command.Data);
                }
            }
        }

        public void ExecuteReadStreamCommand(DataReadCommand command)
        {
            Widget rawWidget = m_BankManager.FindWidgetFromID((uint)command.WidgetId);
            if (rawWidget != null)
            {
                if (rawWidget is WidgetData)
                {
                    WidgetData dataWidget = (WidgetData)rawWidget;
                    dataWidget.UpdateRemote();

                    byte[] lengthBytes = BitConverter.GetBytes(dataWidget.Data.Length);
                    command.Stream.Write(lengthBytes, 0, lengthBytes.Length);

                    byte[] value = dataWidget.Data;
                    command.Stream.Write(value, 0, value.Length);

                }
                else
                {
                    rawWidget.SendData(command.Stream);
                }
            }
        }

        public void ExecuteCommand(TcpClient client, string commandString)
        {
            string[] split = SplitArgs(commandString);
            ExecuteCommand(client, split);
        }

        public void ExecuteCommand(TcpClient client, string[] split)
        {
            if (split.Length > 0)
            {
                // check if it's a comment:
                if (split[0].Trim().StartsWith("//"))
                    return;

                // see if it's a rag side command:
                split[0] = split[0].Trim();
                Command theCommand;
                lock (m_CommandLock)
                {
                    if (m_ConnectedToGame == false)
                    {
                        //string result = "Unable to process command '" + split[0] + "'.  (The game is not currently connected to the console.)";
                        //return result;
                    }
                    else if (m_CommandMaps.TryGetValue(split[0].ToLower(), out theCommand))
                    {
                        string[] splitWithoutCommand = new string[split.Length - 1];
                        Array.Copy(split, 1, splitWithoutCommand, 0, split.Length - 1);
                        object result = theCommand.m_Command(splitWithoutCommand);
                        if (theCommand.m_ResultCallback != null)
                        {
                            theCommand.m_ResultCallback(client, result, false);
                        }
                    }
                    else
                    {
                        // Make sure the console pipe is connected.
                        String result;

                        if (IsConnectedToConsole())
                        {
                            // Send this command to the game for processing.
                            lock (_consolePipeLock)
                            {
                                SendCommand(split);
                                result = ReceiveResponse();
                            }
                        }
                        else
                        {
                            result = c_commandTimeoutMessage;
                        }

                        // Send the result back to the console client.
                        result += "\0";
                        byte[] resultData = c_encoder.GetBytes(result);
                        client.GetStream().Write(resultData, 0, resultData.Length);
                    }
                    
                }
            }
        }


        //PURPOSE: Sends a command using the array of string arguments to the game.
        //This method packs the command in a special format for the game to interpret.
        //Upon completion of the command, any output is written back to the console
        //stream to be read by any other attached proxies.
        private void SendCommand(string[] commandArray)
        {
            int commandLen = 0;

            // collect valid strings:
            List<string> strings = new List<string>();
            string commandName = null;
            foreach (string theStringIter in commandArray)
            {
                string theString = theStringIter.Trim();
                if (theString != null)
                {
                    if (commandName == null)
                    {
                        commandName = theString;
                    }
                    else
                    {
                        strings.Add(theString);
                        commandLen += theString.Length + 1; // +1 for NULL
                    }
                }
            }

            const int HEADER_SIZE = 2 * 3; // 2*3 == sizeof(unsigned short)*3
            const int OFFSET_SIZE = 4; // 4 == sizeof(unsigned int)
            int commandSize = commandLen + HEADER_SIZE + (OFFSET_SIZE * strings.Count) + commandName.Length + 1;

            byte[] buffer = new byte[commandSize];
            int index = 0;

            // add header:
            AddShort(buffer, ref index, (ushort)strings.Count);
            AddShort(buffer, ref index, (ushort)commandLen);
            AddShort(buffer, ref index, (ushort)(commandName.Length + 1));

            // add arguments:
            foreach (string theString in strings)
            {
                AddString(buffer, ref index, theString);
            }

            // add offset:
            int offset = 0;
            foreach (string theString in strings)
            {
                AddInt(buffer, ref index, offset);
                offset += theString.Length + 1;
            }

            // add command name:
            AddString(buffer, ref index, commandName);

            // send to the application:
            Debug.Assert(index == buffer.Length);
            _consolePipe.WriteData(buffer, buffer.Length);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private string ReceiveResponse()
        {
            string resultString = string.Empty;

            int loopCount = 0;
            int loopMax = 1000;
            if (Debugger.IsAttached == true)
            {
                loopMax = Int32.MaxValue;
            }

            while (loopCount < loopMax)
            {
                int available = _consolePipe.GetNumBytesAvailable();
                if (available > 0)
                {
                    byte[] readBytes = new byte[available];
                    int len = _consolePipe.ReadData(readBytes, readBytes.Length, true);

                    // see if we find the end of command signal:
                    if (readBytes[readBytes.Length - 1] == 1)
                    {
                        String theString = new string(Encoding.ASCII.GetChars(readBytes, 0, readBytes.Length - 1));
                        resultString += theString;
                        return resultString;
                    }
                    else if (readBytes[readBytes.Length - 1] == 0)
                    {
                        return "Command is not recognized as valid.";
                    }
                    else
                    {
                        String theString = new string(Encoding.ASCII.GetChars(readBytes));
                        resultString += theString;
                    }
                }
                else
                {
                    ++loopCount;

                    // if we just dispatched a command and it has timed out, prompt the user
                    if (loopCount >= loopMax || IsConnectedToConsole() == false)
                    {
                        break;
                    }
                    else
                    {
                        Thread.Sleep(10);
                    }
                }
            }

            return c_commandTimeoutMessage;
        }

        public bool IsConnectedToConsole()
        {
            if (!IsRunning)
                return false;

            if (_consolePipe == null || _consolePipe.Socket == null)
                return false;

            _consolePipe.HasData(); //Polls the console and updates the connection status.
            return _consolePipe.IsConnected();
        }

        //PURPOSE:  A special command that will block until all widgets have been evaluated.
        //This is extremely useful when many widgets are being changed that may be dependent on each other.
        private string SynchronizeCommand(string[] args)
        {
            // Sends a delayed ping message to the game. That way we're sure all previous commands have finished executing
            // and can read any widgets that changed as a result.

            if (m_BankManager == null)
                return c_BankManagerError;

            // this is ugly - we need a better way to maintain the bankpipe:
            BankPipe p = m_BankManager.AllBanks[0].Pipe;

            bool receivedResponse = false;
            EventHandler pingCallback = delegate(object owner, EventArgs e) 
            {
                receivedResponse = true;
            };

            m_BankManager.SendDelayedPing(pingCallback, p);

            while (receivedResponse == false && m_ConnectedToGame == true) { Thread.Sleep(1); }
            
            return "";
        }

        //PURPOSE:  A general ping between the client of the ConsoleProxy to the ConsoleProxy.
        //Sends back a ping message.
        private string PingCommand(string[] args)
        {
            return "rc_ping";
        }

        //PURPOSE:  Allows the console to make known what widgets the game should send data back to when changes occur.
        //Without this command, the game may change the contents of the RAG widgets but not notify the actual BankManager.
        private string GetUpdatesCommand(string[] args)
        {
            if (args.Length == 1 && args[0] == "-help")
            {
                return "Usage: getupdates <widgetname> [on|off]";
            }

            if (args.Length == 1)
            {
                Widget w = m_BankManager.FindFirstWidgetFromPath(args[0]);
                if (w != null && m_GetUpdatesWidgets.Contains(w))
                {
                    return "true";
                }

                return "false";
            }

            if (args.Length == 2)
            {
                Widget widget = m_BankManager.FindFirstWidgetFromPath(args[0]);

                if (widget == null)
                {
                    return "false";
                }
                
                // If the command causes the widget to update a value, it will send the new value off to the
                // game. However, unless we hijack the pipe, it'll get sent straight to the game, and won't
                // have a chance to go to any other proxies that might be interested in the data.
                // This code makes sure that any widget changes are distributed properly.
                lock (m_ServiceManager.GameService.AcquireLockObject())
                {
                    List<NetworkData> updateDataList = new List<NetworkData>();
                    HubStreamPipe pipe = new HubStreamPipe(updateDataList, m_ServiceManager.GameService.GetBankManager().AllBanks[0].Pipe);

                    if (args[1] == "on")
                    { // turn on updates
                        if (m_GetUpdatesWidgets.Contains(widget))
                        {
                            // already getting updates for this widget
                            return "already on";
                        }
                        else
                        {
                            WidgetGroupBase group = widget as WidgetGroupBase;
                            if (group == null)
                            {
                                return "Not a group widget!";
                            }
                            do
                            {
                                if (group != null)
                                {
                                    BankPipe tmpPipe = group.Pipe;
                                    group.Pipe = pipe;
                                    group.AddRefGroup();
                                    group.Pipe = tmpPipe;
                                }
                                group = group.Parent as WidgetGroupBase;
                            }
                            while (group != null);

                            m_GetUpdatesWidgets.Add(widget);
                        }
                    }
                    else if (args[1] == "off")
                    {
                        if (!m_GetUpdatesWidgets.Contains(widget))
                        {
                            return "This group is already off.";
                        }
                        else
                        {
                            WidgetGroupBase group = widget as WidgetGroupBase;
                            if (group == null)
                            {
                                return "Not a group widget!";
                            }
                            do
                            {
                                if (group != null)
                                {
                                    BankPipe tmpPipe = group.Pipe;
                                    group.Pipe = pipe;
                                    group.ReleaseGroup();
                                    group.Pipe = tmpPipe;
                                }

                                group = group.Parent as WidgetGroupBase;
                            }
                            while (group != null);

                            m_GetUpdatesWidgets.Remove(widget);
                        }
                    }


                    foreach (NetworkData data in updateDataList)
                    {
                        m_HubStreams[StreamNameConstants.BankRaw].Write(this, data.Data);
                    }
                }
            }

            return "";
        }

       
        //PURPOSE: The general purpose widget command allows user to force a change on a particular widget
        //based on the context of that widget.  For example, the
        //  widget "Create Widgets"
        //command will trigger the "Create Widgets" button.  
        //  widget "TimeCycle/Time" 1
        //will set the "Time" widget in the "TimeCycle" bank to 1.
        private string WidgetCommand(string[] args)
        {
            if (args.Length == 0)
            {
                return "no widget specified";
            }

            if (args[0] == "-help" || args.Length < 1)
            {
                string helpString = "Command that allows you to set widget values.\n\n" +
                    "WIDGET [-help] [widget name] [widget value1] ... [widget valueN]\n\n" +
                    "\t-help\t\t\tshows this message.  Put -help after the widget name if you wish information specific to that widget.\n" +
                    "\twidget name\t\t\tthe full path name to the widget to change or get help information.\n" +
                    "\twidget value1\t\tthe first value to pass to the widget (value is widget dependent).\n" +
                    "\twidget valueN\t\tthe Nth value to pass to the widget (value is widget dependent)\n";
                return helpString;
            }

            if (m_BankManager == null)
            {
                return c_BankManagerError;
            }

            ragCore.Widget theWidget = null;

            int remote_id = 0;
            if ( int.TryParse( args[0], out remote_id ) )
            {
                theWidget = m_BankManager.FindWidgetFromID( (uint)remote_id );
                if (theWidget == null)
                {
                    return "Couldn't find widget with id'" + args[0] + "'";
                }
            }
            else
            {
                List<ragCore.Widget> list = m_BankManager.FindWidgetsFromPath( args[0] );

                // none found...
                if ( list == null || list.Count == 0 )
                {
                    return "Couldn't find widget '" + args[0] + "'";
                }
                // ...more than one found...
                else if ( list.Count > 1 )
                {
                    string returnStr = "More than one widget with the name '" + args[0] + "':\n";
                    foreach ( ragCore.Widget aWidget in list )
                    {
                        returnStr += "\t" + aWidget.Title + "\n";
                    }
                    return returnStr;
                }

                // only one found!
                theWidget = list[0];
            }


            // If the command causes the widget to update a value, it will send the new value off to the
            // game. However, unless we hijack the pipe, it'll get sent straight to the game, and won't
            // have a chance to go to any other proxies that might be interested in the data.
            // This code makes sure that any widget changes are distributed properly.
            lock ( m_ServiceManager.GameService.AcquireLockObject() )
            {
                List<NetworkData> updateDataList = new List<NetworkData>();
                HubStreamPipe pipe = new HubStreamPipe( updateDataList, m_ServiceManager.GameService.GetBankManager().AllBanks[0].Pipe );

                BankPipe tmpPipe = theWidget.Pipe;
                theWidget.Pipe = pipe;

                string[] argsWithoutWidgetName = new string[args.Length - 1];
                Array.Copy( args, 1, argsWithoutWidgetName, 0, args.Length - 1 );
                string result = theWidget.ProcessCommand( argsWithoutWidgetName );

                theWidget.Pipe = tmpPipe;

                foreach (NetworkData data in updateDataList)
                {
                    m_HubStreams[StreamNameConstants.BankRaw].Write( this, data.Data );
                }

                return result;
            }

        }

        /// <summary>
        /// Returns whether a widget exists.
        /// </summary>
        /// <param name="widgetName"></param>
        /// <param name="theWidget"></param>
        /// <returns></returns>
        private bool WidgetExist(string widgetName, out Widget theWidget)
        {
            theWidget = null;
            if (m_BankManager == null)
            {
                return false;
            }

            int remote_id = 0;
            if (int.TryParse(widgetName, out remote_id))
            {
                theWidget = m_BankManager.FindWidgetFromID((uint)remote_id);
                if (theWidget == null)
                {
                    return false;
                }
            }
            else
            {
                List<ragCore.Widget> list = m_BankManager.FindWidgetsFromPath(widgetName);

                if (list == null || list.Count == 0)
                {
                    return false;
                }
                else if (list.Count > 1)
                {
                    //There are multiple widgets with this particular path.  
                    //Ambiguous -- assume that the widget does not exist.
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Helper function for determining the existence of a widget.
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        private string WidgetExistCommand(string[] args)
        {
            if (args[0] == "-help" || args.Length < 1)
            {
                string helpString = "Command that allows you to set widget values.\n\n" +
                    "WIDGET [-help] [widget name] [widget value1] ... [widget valueN]\n\n" +
                    "\t-help\t\t\tshows this message.  Put -help after the widget name if you wish information specific to that widget.\n" +
                    "\twidget name\t\t\tthe full path name to the widget to change or get help information.\n";
                return helpString;
            }

            Widget theWidget = null;
            return (WidgetExist(args[0], out theWidget) == true) ? "true" : "false";
        }

        /// <summary>
        /// Returns when a widget exists.
        /// </summary>
        /// <param name="widgetName"></param>
		/// <param name="timeout">Timeout in milliseconds.</param>
        /// <param name="theWidget"></param>
        /// <returns></returns>
        private bool WidgetExistCallback(string widgetName, int timeout, out Widget theWidget)
        {
            theWidget = null;
            if (m_BankManager == null)
            {
                return false;
            }

            DateTime startTime = DateTime.UtcNow;
            TimeSpan currentTime = new TimeSpan();
            TimeSpan timeoutSpan = new TimeSpan(0, 0, 0, 0, timeout);
            while ( currentTime.TotalMilliseconds < timeoutSpan.TotalMilliseconds )
            {
                List<ragCore.Widget> list = m_BankManager.FindWidgetsFromPath(widgetName);

                if (list.Count == 1)
                {
                    return true;
                }
                else if (list.Count > 1)
                {
                    //There are multiple widgets with this particular path.  
                    //Ambiguous -- assume that the widget does not exist.
                    return false;
                }

                currentTime = (DateTime.UtcNow - startTime);
            }

            //Exceeded the timeout.
            return false;
        }

        /// <summary>
        /// Returns when a widget exists, unless it hits a timeout.  
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        private string WidgetExistCallbackCommand(string[] args)
        {
            const string helpString = "Command that will return when a widget exists.\n\n" +
                    "WIDGET [-help] [widget name] [timeout]\n\n" +
                    "\t-help\t\t\tshows this message.  Put -help after the widget name if you wish information specific to that widget.\n" +
                    "\twidget name\t\t\tthe full path name to the widget.\n" +
                    "\ttimeout\t\t the amount of time in milliseconds until the widget will return.";

            if (args[0] == "-help" || args.Length < 1 || args.Length > 2)
            {
                return helpString;
            }

            int timeout = -1;
            if (Int32.TryParse(args[1], out timeout) == false)
            {
                return helpString;
            }

            Widget theWidget = null;
            if ( WidgetExistCallback(args[0], timeout, out theWidget) == true )
                return "true";
            else
                return "false";
        }


        /// <summary>
        /// The widget_id command allows user to get the ID of a particular widget.  
        /// It will return the id of the widget as a string, in this format: "id=123" (without the quotes)
        /// Returns the ID that the widget has.
        /// If more or less than one widget is found on the path, an error string will be returned instead.  
        /// </summary>
        /// <param name="args">Should contain a single string, the full path to the widget.</param>
        /// <returns></returns>
        private object AcquireLocalWidgetIDCommand( string[] args )
        {
            if ( args.Length == 0 )
            {
                return "no widget specified";
            }

            if ( args[0] == "-help" || args.Length < 1 )
            {
                string helpString = "Command that allows you to get widget IDs from a path.\n\n" +
                    "WIDGET_ID [-help] [widget name]\n\n" +
                    "\t-help\t\t\tshows this message. \n" +
                    "\twidget name\t\t\tthe full path name to the widget to change or get help information.\n";
                return helpString;
            }

            if ( m_BankManager == null )
            {
                return c_BankManagerError;
            }

            List<ragCore.Widget> list = m_BankManager.FindWidgetsFromPath( args[0] );

            // none found...
            if ( list == null || list.Count == 0 )
            {
                return "Couldn't find widget '" + args[0] + "'";
            }
            // ...more than one found...
            else if ( list.Count > 1 )
            {
                string returnStr = "More than one widget with the name '" + args[0] + "':\n";
                foreach ( ragCore.Widget aWidget in list )
                {
                    returnStr += "\t" + aWidget.Title + "\n";
                }
                return returnStr;
            }

            // only one found!
            ragCore.Widget theWidget = list[0];
            return string.Format("id={0}", theWidget.Pipe.LocalToRemote( theWidget ));
        }

        /// <summary>
        /// The widget_id command allows user to get the ID of a particular widget.  
        /// It will return the id of the widget as a string, in this format: "id=123" (without the quotes)
        /// Returns the ID that the widget has.
        /// If more or less than one widget is found on the path, an error string will be returned instead.  
        /// </summary>
        /// <param name="args">Should contain a single string, the full path to the widget.</param>
        /// <returns></returns>
        private object SendWidgetIdCommand(string[] args)
        {
            if (args.Length == 0 || args.Length < 1 || m_BankManager == null)
            {
                return 0;
            }

            List<ragCore.Widget> list = m_BankManager.FindWidgetsFromPath(args[0]);
            if (list == null || list.Count == 0 || list.Count > 1)
            {
                return 0;
            }

            ragCore.Widget theWidget = list[0];
            return theWidget.Pipe.LocalToRemote(theWidget);
        }

        private string IsGameConnected(string[] args)
        {
            if ( m_ConnectedToGame == true )
                return "true";
            else
                return "false";
        }

        private string PlatformCommand(string[] args)
        {
            if (m_ConnectedToGame == false)
                return "Not connected";

            return m_PlatformLoaded;
        }

        private String BuildConfigCommand(String[] args)
        {
            if (m_ConnectedToGame == false)
                return "Not connected";

            return m_BuildConfigLoaded;
        }

        private String GetExecutableNameCommand(String[] args)
        {
            if (m_ConnectedToGame == false)
                return "Not connected";

            return m_ServiceManager.GameService.GetExeName();
        }

        private string GetGameIPCommand( string[] args )
        {
            if (m_ConnectedToGame == false)
                return "";

            try
            {
                return m_ServiceManager.GameService.GetGameIP();
            }
            catch ( Exception )
            {
                Log( "Couldn't get game ip, returning empty string" );
                return "";
            }
        }

        private string GetProxyStatus( string[] args )
        {
            if ( args.Length == 0 )
                return "Error: Must send proxy UID as first argument.";

            int uid = 0;
            if (!int.TryParse(args[0], out uid))
                return "Error: Couldn't parse first argument as integer";

            return Hub.GetProxyStatus(uid);
        }

        void OnGameConnected(object sender, GameConnectedEventArgs args)
        {
            m_BankManager = m_ServiceManager.GameService.GetBankManager();
            m_ConnectedToGame = true;
            m_PlatformLoaded = args.Platform;
            m_BuildConfigLoaded = args.BuildConfig;
        }

        void OnGameDisconnected(object sender, GameConnectedEventArgs args)
        {
            m_BankManager = null;
            m_ConnectedToGame = false;
            m_PlatformLoaded = args.Platform;
            m_BuildConfigLoaded = args.BuildConfig;
        }

        #region Helper Functions

        private void AddInt(byte[] buffer, ref int index, int theInt)
        {
            buffer[index++] = (byte)(theInt);
            buffer[index++] = (byte)(theInt >> 8);
            buffer[index++] = (byte)(theInt >> 16);
            buffer[index++] = (byte)(theInt >> 24);
        }

        private void AddShort(byte[] buffer, ref int index, ushort theShort)
        {
            buffer[index++] = (byte)(theShort);
            buffer[index++] = (byte)(theShort >> 8);
        }

        private void AddString(byte[] buffer, ref int index, string theString)
        {
            if (theString == null)
                buffer[index++] = 0;
            else
            {
                int sl = theString.Length;
                for (int i = 0; i < sl; i++)
                    buffer[index++] = Convert.ToByte(theString[i]);
                buffer[index++] = 0x0; // null byte
            }
        }

        public string ConvertNewlines(string s)
        {
            if (c_useDosNewLines)
            {
                return c_findBareNL.Replace(s, "\r\n");
            }
            return s;
        }

        private string ConvertToString(List<char> chars)
        {
            char[] charArray;
            charArray = chars.ToArray();
            return new string(charArray);
        }

        private string[] SplitArgs(string theCommandString)
        {
            List<string> strings = new List<string>();
            List<char> argChars = new List<char>();
            bool lookingForEndQuote = false;

            // extract arguments ('"' keeps spaces together):
            for (int i = 0; i < theCommandString.Length; i++)
            {
                char theChar = theCommandString[i];
                if (theChar == '"')
                {
                    if (lookingForEndQuote)
                    {
                        strings.Add(ConvertToString(argChars));
                        argChars.Clear();
                        lookingForEndQuote = false;
                    }
                    else
                    {
                        lookingForEndQuote = true;
                    }
                }
                else if (Char.IsWhiteSpace(theChar))
                {
                    if (lookingForEndQuote)
                    {
                        argChars.Add(theChar);
                    }
                    else
                    {
                        if (argChars.Count > 0)
                        {
                            strings.Add(ConvertToString(argChars));
                            argChars.Clear();
                        }
                    }
                }
                else
                    argChars.Add(theChar);
            }

            // convert the final arg:
            if (argChars.Count > 0)
                strings.Add(ConvertToString(argChars));


            string[] returnValue;
            returnValue = strings.ToArray();

            return returnValue;
        }

        public override void LogState()
        {
            base.LogState();
            
            Log( "#Clients: " + m_ConnectedClients == null ? "null" : m_ConnectedClients.Count.ToString() );
            lock (m_ConnectedClients)
            {
                int index = 0;
                foreach (KeyValuePair<TcpClient, Thread> item in m_ConnectedClients)
                {
                    Log("m_ConnectedClients[{0}].IsAlive {1} :: ", index++, item.Value.IsAlive);
                }
            }
        }
        #endregion


        public override string GetShortDescription()
        {
            return "Console@" + GetShortStateDescription() + ":" + ConnectionPort;
        }
    }
}
