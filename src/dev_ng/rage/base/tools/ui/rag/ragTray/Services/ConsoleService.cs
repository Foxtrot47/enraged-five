﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using ragCore;
using RSG.Rag.Contracts.Data;
using RSG.Rag.Contracts.Services;
using RSG.Rag.Services.Exceptions;
using ragTray;
using ragWidgets;
using ragTray.Proxy.Proxies;
using System.Diagnostics;

namespace RSG.Rag.Services
{
    /// <summary>
    /// Service that allows external applications to invoke console based commands against widgets.
    /// </summary>
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, ConcurrencyMode = ConcurrencyMode.Multiple)]
    internal class ConsoleService : IRagService, IConsoleService
    {
        #region Fields
        /// <summary>
        /// 
        /// </summary>
        private readonly IGameProxyService _gameProxyService;

        /// <summary>
        /// Reference to the console proxy.
        /// </summary>
        private readonly IConsoleProxy _consoleProxy;

        /// <summary>
        /// Reference to the guy you need to see about taking out a loan...
        /// </summary>
        private readonly IBankManager _bankManager;

        /// <summary>
        /// 
        /// </summary>
        private readonly ServicesProxy _appProxy;

        /// <summary>
        /// 
        /// </summary>
        private readonly InterProxyStream _bankRawStream;

        /// <summary>
        /// Private field for the <see cref="Port"/> property.
        /// </summary>
        private readonly int _port;

        /// <summary>
        /// List of commands that this service supports.
        /// </summary>
        private readonly IDictionary<string, ConsoleCommand> _commands =
            new SortedDictionary<string, ConsoleCommand>();

        /// <summary>
        /// Set of widgets that are currently retrieving updates.
        /// </summary>
        private readonly ISet<Widget> _getUpdateWidgets = new HashSet<Widget>();
        #endregion

        #region Constructor(s)
        /// <summary>
        /// Initialises a new instance of the <see cref="ConsoleService"/> class.
        /// </summary>
        /// <param name="port"></param>
        /// <param name="gameProxyService"></param>
        public ConsoleService(int port, IGameProxyService gameProxyService, InterProxyStream bankRawStream, ServicesProxy appProxy, IConsoleProxy consoleProxy)
        {
            _appProxy = appProxy;
            _gameProxyService = gameProxyService;
            _consoleProxy = consoleProxy;
            _bankManager = gameProxyService.GetBankManager();
            _bankRawStream = bankRawStream;
            _port = port;

            // Register the list of console commands the service supports.
            RegisterConsoleCommand("sync", SynchronizeCommandHandler, "<TIMEOUT> Sends a delayed ping request to the game with an optional timeout (in milliseconds).");
            RegisterConsoleCommand("widget", WidgetCommandHandler, "<WIDGETPATH> <WIDGETVALUE> Command that allows you to get/set widget values.");
        }
        #endregion

        #region Properties
        /// <summary>
        /// Port that the service is hosted on.
        /// </summary>
        public int Port
        {
            get { return _port; }
        }
        #endregion

        #region IConsoleService Implementation
        /// <summary>
        /// Executes a console command.
        /// </summary>
        /// <param name="fullCommand">Command to execute.</param>
        /// <returns>Results of executing the command.</returns>
        public ConsoleCommandResponse ExecuteCommand(string fullCommand)
        {
            ConsoleCommandResponse response;

            string[] tokens = SplitCommandString(fullCommand);
            if (tokens.Length == 0)
            {
                response = ConsoleCommandResponse.IgnoredResponse;
            }
            else
            {
                string command = tokens[0].ToLower();
                string[] arguments = null;
                if (tokens.Length > 1)
                {
                    arguments = new string[tokens.Length - 1];
                    Array.Copy(tokens, 1, arguments, 0, tokens.Length - 1);
                }

                try
                {
                    response = ProcessCommand(command, arguments ?? new string[0]);
                }
                catch (Exception e)
                {
                    response = new ConsoleCommandResponse(e);
                }
            }

            return response;
        }
        #endregion

        #region Methods
        /// <summary>
        /// Registers a new console command with the specified arguments.
        /// </summary>
        /// <param name="key">
        /// The key used to identify the command.
        /// </param>
        /// <param name="handler">
        /// The handler to invoke when the command is encountered.
        /// </param>
        /// <param name="helpInfo">
        /// The help information about the command.
        /// </param>
        private void RegisterConsoleCommand(
            string key, Func<string, string[], String> handler, string helpInfo)
        {
            this._commands.Add(key, new ConsoleCommand(key, handler, helpInfo));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="commandString"></param>
        /// <returns></returns>
        private string[] SplitCommandString(string commandString)
        {
            IList<string> tokens = new List<string>();
            StringBuilder argBuilder = new StringBuilder();
            bool lookingForEndQuote = false;

            for (int i = 0; i < commandString.Length; ++i)
            {
                char theChar = commandString[i];
                char nextChar = i < commandString.Length - 1 ? commandString[i + 1] : default(char);

                if (theChar == '\\')
                {
                    if (nextChar == '"')
                    {
                        argBuilder.Append('"');
                    }
                    else if (nextChar == '\\')
                    {
                        argBuilder.Append('\\');
                    }
                    else
                    {
                        throw new NotSupportedException("Only escaped quotes and backslashes are supported.");
                    }
                    ++i;
                }
                else if (theChar == '"')
                {
                    if (lookingForEndQuote)
                    {
                        tokens.Add(argBuilder.ToString());
                        argBuilder.Clear();
                    }
                    lookingForEndQuote = !lookingForEndQuote;
                }
                else if (Char.IsWhiteSpace(theChar) && !lookingForEndQuote)
                {
                    if (argBuilder.Length > 0)
                    {
                        tokens.Add(argBuilder.ToString());
                        argBuilder.Clear();
                    }
                }
                else
                {
                    argBuilder.Append(theChar);
                }
            }

            // Keep track of the final argument.
            if (argBuilder.Length > 0)
            {
                tokens.Add(argBuilder.ToString());
            }

            // Return the tokens as an array.
            return tokens.ToArray();
        }

        /// <summary>
        /// Processes the specified command with the specified arguments.
        /// </summary>
        /// <param name="command">
        /// The command to process.
        /// </param>
        /// <param name="arguments">
        /// The arguments sent with the command.
        /// </param>
        /// <returns>
        /// The result from processing the command.
        /// </returns>
        private ConsoleCommandResponse ProcessCommand(string command, string[] arguments)
        {
            ConsoleCommandResponse response;

            ConsoleCommand cmd;
            if (this._commands.TryGetValue(command, out cmd))
            {
                String result = cmd.Handler.Invoke(command, arguments);
                response = new ConsoleCommandResponse(ConsoleCommandExecutionState.ProcessedByRag, result);
            }
            else
            {
                response = ForwardCommandToGame(command, arguments);
            }

            return response;
        }

        /// <summary>
        /// A special command that will block until all widgets have been evaluated.
        /// This is extremely useful when many widgets are being changed that may be dependent on each other.
        /// </summary>
        /// <param name="command"></param>
        /// <param name="arguments"></param>
        /// <returns></returns>
        private String SynchronizeCommandHandler(String command, String[] arguments)
        {
            if (arguments.Length > 1)
            {
                String message = String.Format(
                    "Invalid number of arguments provided.  Expected 0 or 1, got {0}: '{1}'.",
                    arguments.Length,
                    String.Join(" ", arguments));
                throw new ArgumentException(message);
            }

            TimeSpan timeout = TimeSpan.Zero;
            if (arguments.Length == 1)
            {
                timeout = new TimeSpan(0, 0, 0, Int32.Parse(arguments[0]));
            }

            // this is ugly - we need a better way to maintain the bankpipe:
            BankPipe p = _bankManager.AllBanks[0].Pipe;

            bool receivedResponse = false;
            EventHandler pingCallback =
                delegate(object owner, EventArgs e)
                {
                    receivedResponse = true;
                };

            // Send the delayed ping request and await it's response.
            DateTime startTime = DateTime.UtcNow;

            _bankManager.SendDelayedPing(pingCallback, p);
            while (!receivedResponse)
            {
                TimeSpan elapsedTime = DateTime.UtcNow - startTime;
                if (timeout != TimeSpan.Zero && elapsedTime > timeout )
                {
                    throw new TimeoutException(
                        String.Format("Sync command timed out after {0} ms.", elapsedTime.TotalMilliseconds));
                }

                Thread.Sleep(25);
            }

            return "";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="command"></param>
        /// <param name="arguments"></param>
        /// <returns></returns>
        private String WidgetCommandHandler(String command, String[] arguments)
        {
            if (arguments.Length < 1)
            {
                String message = String.Format(
                    "Invalid number of arguments provided.  Expected 1 or more, got {0}: '{1}'.",
                    arguments.Length,
                    String.Join(" ", arguments));
                throw new ArgumentException(message);
            }

            // Try and retrieve the widget.
            String widgetPath = arguments[0];
            Widget widget = null;

            uint widgetId;
            if (UInt32.TryParse(widgetPath, out widgetId))
            {
                widget = _bankManager.FindWidgetFromID(widgetId);

                if (widget == null)
                {
                    throw new WidgetNotFoundException(widgetId);
                }
            }
            else
            {
                widget = _bankManager.FindFirstWidgetFromPath(widgetPath);

                if (widget == null)
                {
                    throw new WidgetNotFoundException(widgetPath);
                }
            }

            // If the command causes the widget to update a value, it will send the new value off to the
            // game. However, unless we hijack the pipe, it'll get sent straight to the game, and won't
            // have a chance to go to any other proxies that might be interested in the data.
            // This code makes sure that any widget changes are distributed properly.
            lock (_gameProxyService.AcquireLockObject())
            {
                List<NetworkData> updateDataList = new List<NetworkData>();
                HubStreamPipe pipe = new HubStreamPipe(updateDataList, _bankManager.AllBanks[0].Pipe);

                BankPipe tmpPipe = widget.Pipe;
                widget.Pipe = pipe;

                string[] argsWithoutWidgetName = new string[arguments.Length - 1];
                Array.Copy(arguments, 1, argsWithoutWidgetName, 0, arguments.Length - 1);
                string result = widget.ProcessCommand(argsWithoutWidgetName);

                widget.Pipe = tmpPipe;

                foreach (NetworkData data in updateDataList)
                {
                    _bankRawStream.Write(_appProxy, data.Data);
                }

                return result;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="command"></param>
        /// <param name="arguments"></param>
        /// <returns></returns>
        private ConsoleCommandResponse ForwardCommandToGame(String command, String[] arguments)
        {
            ConsoleCommandResponse response;

            lock (_consoleProxy.ConsolePipeLock)
            {
                byte[] commandBuffer = CreateCommandBuffer(command, arguments);
                _consoleProxy.ConsolePipe.WriteData(commandBuffer, commandBuffer.Length);

                ConsoleCommandExecutionState executionState;
                String result = ReceiveResponse(out executionState);
                response = new ConsoleCommandResponse(executionState, result);
            }

            return response;
        }

        /// <summary>
        /// Creates the buffer that we'll be sending to the game for this command.
        /// </summary>
        /// <param name="command"></param>
        /// <param name="arguments"></param>
        /// <returns></returns>
        private byte[] CreateCommandBuffer(String command, String[] arguments)
        {
            const int HEADER_SIZE = 2 * 3;  // 2*3 == sizeof(unsigned short)*3
            const int OFFSET_SIZE = 4;      // 4 == sizeof(unsigned int)

            // +1 for null
            int argumentsSize = arguments.Sum(arg => arg.Length + 1);
            int commandSize = HEADER_SIZE + argumentsSize + (OFFSET_SIZE * arguments.Length) + (command.Length + 1);

            byte[] buffer = new byte[commandSize];
            int index = 0;

            // add header:
            AddShort(buffer, ref index, (ushort)arguments.Length);
            AddShort(buffer, ref index, (ushort)argumentsSize);
            AddShort(buffer, ref index, (ushort)(command.Length + 1));

            // add arguments:
            foreach (string arg in arguments)
            {
                AddString(buffer, ref index, arg);
            }

            // add offset:
            int offset = 0;
            foreach (string arg in arguments)
            {
                AddInt(buffer, ref index, offset);
                offset += arg.Length + 1;
            }

            // add command name:
            AddString(buffer, ref index, command);

            Debug.Assert(index == buffer.Length);
            return buffer;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="buffer"></param>
        /// <param name="index"></param>
        /// <param name="theShort"></param>
        private void AddShort(byte[] buffer, ref int index, ushort theShort)
        {
            buffer[index++] = (byte)(theShort);
            buffer[index++] = (byte)(theShort >> 8);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="buffer"></param>
        /// <param name="index"></param>
        /// <param name="theInt"></param>
        private void AddInt(byte[] buffer, ref int index, int theInt)
        {
            buffer[index++] = (byte)(theInt);
            buffer[index++] = (byte)(theInt >> 8);
            buffer[index++] = (byte)(theInt >> 16);
            buffer[index++] = (byte)(theInt >> 24);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="buffer"></param>
        /// <param name="index"></param>
        /// <param name="theString"></param>
        private void AddString(byte[] buffer, ref int index, string theString)
        {
            if (theString == null)
            {
                buffer[index++] = 0;
            }
            else
            {
                // Prefix string with length.
                int sl = theString.Length;
                for (int i = 0; i < sl; i++)
                {
                    buffer[index++] = Convert.ToByte(theString[i]);
                }
                buffer[index++] = 0x0; // null byte
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private String ReceiveResponse(out ConsoleCommandExecutionState returnCode)
        {
            String result = String.Empty;

            int loopCount = 0;
            int loopMax = 1000;
            if (Debugger.IsAttached == true)
            {
                loopMax = Int32.MaxValue;
            }

            while (loopCount < loopMax)
            {
                int available = _consoleProxy.ConsolePipe.GetNumBytesAvailable();
                if (available > 0)
                {
                    byte[] readBytes = new byte[available];
                    int len = _consoleProxy.ConsolePipe.ReadData(readBytes, readBytes.Length, true);

                    // see if we find the end of command signal:
                    if (readBytes[readBytes.Length - 1] == 1)
                    {
                        String theString = new string(Encoding.ASCII.GetChars(readBytes, 0, readBytes.Length - 1));
                        result += theString;
                        returnCode = ConsoleCommandExecutionState.ProcessedByGame;
                        return result;
                    }
                    else if (readBytes[readBytes.Length - 1] == 0)
                    {
                        result = "Command is not recognized as valid.";
                        returnCode = ConsoleCommandExecutionState.Ignored;
                        return result;
                    }
                    else
                    {
                        String theString = new string(Encoding.ASCII.GetChars(readBytes));
                        result += theString;
                    }
                }
                else
                {
                    ++loopCount;

                    // if we just dispatched a command and it has timed out, prompt the user
                    if (loopCount >= loopMax || !IsConnectedToConsole())
                    {
                        break;
                    }
                    else
                    {
                        Thread.Sleep(10);
                    }
                }
            }

            throw new TimeoutException("Game took longer than 10 seconds to respond to the command.");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private bool IsConnectedToConsole()
        {
            if (_consoleProxy.ConsolePipe == null || _consoleProxy.ConsolePipe.Socket == null)
                return false;

            _consoleProxy.ConsolePipe.HasData(); //Polls the console and updates the connection status.
            return _consoleProxy.ConsolePipe.IsConnected();
        }

        #endregion
    }
}
