using System;
using System.Windows.Forms;

using ragCore;
using System.Collections;
using ragWidgets;

namespace ragColorGradientEditor
{
    /// <summary>
    /// Summary description for WidgetColorGradientEditorVisible.
    /// </summary>
    public class WidgetColorGradientEditorVisible : WidgetVisible
    {
        public WidgetColorGradientEditorVisible(IWidgetView widgetView, WidgetColorGradientEditor editor, Control parent)
            : base( widgetView, parent, editor )
        {
            m_Widget = editor;
            m_Widget.RangeUpdated += widget_RangeUpdated;
            m_Widget.ReceivedKeyFrameData += receivedKeyframeData;

            m_Editor = new ColorGradientEditor();

            m_Editor.colorGradientControl1.OnPegColorChanged += control_PegChanged;
            m_Editor.colorGradientControl1.OnPegLocationChanged += control_PegChanged;
            m_Editor.AllowEditAlpha = m_Widget.UseAlpha;

            parent.Controls.Add( m_Editor );

            m_Editor.SetRange(m_Widget.Min, m_Widget.Max);
            receivedKeyframeData( null, EventArgs.Empty );
        }

        void widget_RangeUpdated( object sender, EventArgs e )
        {
            m_Editor.SetRange( m_Widget.Min, m_Widget.Max );
        }


        private void control_PegChanged( object sender, ColorGradientControl.ColorGradientEventArgs e )
        {
            ColorGradientControl control = sender as ColorGradientControl;
            m_Widget.KeyframeData.Clear();

            ArrayList sData = control.GetKeyframeData();
            foreach ( ColorGradientControl.Keyframe skey in sData )
            {
                ColorGradientControl.Keyframe key = new ColorGradientControl.Keyframe( skey.m_Time, skey.m_ColorRGB, skey.m_ColorHSB );
                m_Widget.KeyframeData.Add( key );
            }

            m_Widget.OnReceivedKeyFrameData( control );
            m_Widget.UpdateRemote();
        }


        private void receivedKeyframeData( object sender, EventArgs args )
        {
            ColorGradientControl control = (ColorGradientControl)sender;

            ColorGradientEditor editor = Control as ColorGradientEditor;
            if ( m_Editor.colorGradientControl1 != control )
            {
                if ( control == null )
                    editor.BuildFromData( m_Widget.KeyframeData );
                else
                    editor.BuildFromControl( control );
            }
        }

        public override Control Control
        {
            get
            {
                return m_Editor;
            }
        }

        public override Control DragDropControl
        {
            get
            {
                return null;
            }
        }

        public override Widget Widget
        {
            get
            {
                return m_Widget;
            }
        }

        public override void Remove()
        {
            base.Remove();

            m_Parent.Controls.Remove( m_Editor );
        }

        public override void Destroy()
        {
            base.Destroy();
            m_Editor.colorGradientControl1.OnPegColorChanged -= control_PegChanged;
            m_Editor.colorGradientControl1.OnPegLocationChanged -= control_PegChanged;
            m_Widget.ReceivedKeyFrameData -= receivedKeyframeData;
            m_Widget.RangeUpdated -= widget_RangeUpdated;
            m_Widget = null;
            m_Editor.Dispose();
            m_Editor = null;
        }

        private WidgetColorGradientEditor m_Widget;
        private ColorGradientEditor m_Editor;
    }
}
