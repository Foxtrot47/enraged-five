using System;
using ragCore;

namespace ragColorGradientEditor
{
	/// <summary>
	/// Summary description for WidgetPlugIn.
	/// </summary>
	public class WidgetPlugIn : ragCore.IWidgetPlugIn
	{
        public void AddHandlers( BankRemotePacketProcessor packetProcessor ) 
		{
			WidgetColorGradientEditor.AddHandlers(packetProcessor );
		}
        public void RemoveHandlers( BankRemotePacketProcessor packetProcessor ) 
		{
			WidgetColorGradientEditor.RemoveHandlers(packetProcessor );
		}
	}
}
