using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace ragColorGradientEditor
{
	/// <summary>
	/// Summary description for Help.
	/// </summary>
	public class Help : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Label LabelHelpText;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public Help(string text)
		{
			InitializeComponent();
			this.LabelHelpText.Text = text;
			this.Width = LabelHelpText.Width;
			this.Height = LabelHelpText.Height;
		}
		public Help()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.panel1 = new System.Windows.Forms.Panel();
			this.LabelHelpText = new System.Windows.Forms.Label();
			this.panel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// panel1
			// 
			this.panel1.BackColor = System.Drawing.SystemColors.Info;
			this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.panel1.Controls.Add(this.LabelHelpText);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel1.Location = new System.Drawing.Point(0, 0);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(292, 16);
			this.panel1.TabIndex = 0;
			this.panel1.Click += new System.EventHandler(this.panel1_Click);
			this.panel1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseMove);
			this.panel1.MouseLeave += new System.EventHandler(this.panel1_MouseLeave);
			// 
			// LabelHelpText
			// 
			this.LabelHelpText.AutoSize = true;
			this.LabelHelpText.Location = new System.Drawing.Point(0, 0);
			this.LabelHelpText.Name = "LabelHelpText";
			this.LabelHelpText.Size = new System.Drawing.Size(35, 16);
			this.LabelHelpText.TabIndex = 0;
			this.LabelHelpText.Text = "label1";
			this.LabelHelpText.MouseLeave += new System.EventHandler(this.LabelHelpText_MouseLeave);
			// 
			// Help
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(292, 16);
			this.Controls.Add(this.panel1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "Help";
			this.ShowInTaskbar = false;
			this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
			this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
			this.Text = "Help";
			this.TopMost = true;
			this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Help_MouseMove);
			this.panel1.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		private void panel1_MouseLeave(object sender, System.EventArgs e)
		{
			this.Close();
		}

		private void panel1_Click(object sender, System.EventArgs e)
		{
			this.Close();
		}

		private void panel1_MouseMove(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			this.Close();
		}

		private void Help_MouseMove(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			this.Close();
		}

		private void LabelHelpText_MouseLeave(object sender, System.EventArgs e)
		{
			this.Close();
		}
	}
}
