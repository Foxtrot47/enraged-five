using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using ragCore;
using ragWidgets;

namespace ragColorGradientEditor
{
	/// <summary>
	/// Summary description for WidgetColorGradientEditor.
	/// </summary>
	public class WidgetColorGradientEditor : Widget
    {
        public WidgetColorGradientEditor(BankPipe pipe, uint id, String title, String memo, Color fillColor, bool readOnly)
            : base(pipe, id, title, memo, fillColor, readOnly)
        {

        }

		public override int		GetWidgetTypeGUID() {return GetStaticGUID();}
		public static int		GetStaticGUID()		{return ComputeGUID('r','c','g','e');}
		public override bool	AllowPersist {get {return false;}}

        /// <summary>
        /// Thread synchronisation object.
        /// </summary>
        private static object s_syncObj = new object();

        /// <summary>
        /// Reference count for number of times this object has been initialised.
        /// </summary>
        private static int s_refCount = 0;

		public static void AddHandlers( BankRemotePacketProcessor packetProcessor )
		{
			packetProcessor.AddType(GetStaticGUID(),new BankRemotePacketProcessor.HandlerDel(RemoteHandler));

            lock (s_syncObj)
            {
                // Check whether the script editor is currently set up.
                if (s_refCount == 0)
                {
                    WidgetVisualiser.AddHandlerShowVisible(typeof(WidgetColorGradientEditor), MakeVisible);
                    WidgetVisualiser.AddDefaultHandlerHideVisible(typeof(WidgetColorGradientEditor));
                }

                ++s_refCount;
            }
		}

        public static void RemoveHandlers( BankRemotePacketProcessor packetProcessor )
		{
            packetProcessor.RemoveType( GetStaticGUID() );

            lock (s_syncObj)
            {
                --s_refCount;

                if (s_refCount == 0)
                {
                    WidgetVisualiser.RemoveHandlers(typeof(WidgetColorGradientEditor));
                }
            }
		}

        public static WidgetVisible MakeVisible(Widget widget, IWidgetView widgetView, Control parent, String hashKey, bool forceOpen, ToolTip tooltip)
        {
            WidgetColorGradientEditor widgetColorEditor = (WidgetColorGradientEditor)widget;
            WidgetColorGradientEditorVisible visible = new WidgetColorGradientEditorVisible( widgetView, widgetColorEditor, parent );
            WidgetVisualiser.RegisterVisible( widgetColorEditor, visible, hashKey );

            widgetColorEditor.OnRangeUpdated();
			return visible;
		}
        


		public override void UpdateRemote() 
		{
			if (m_KeyframeData.Count < 15) 
			{
				BankRemotePacket p = new BankRemotePacket(Pipe);
                p.Begin(BankRemotePacket.EnumPacketType.USER_0, GetStaticGUID(), Id);
				p.Write_u32((uint)m_KeyframeData.Count);
				for(int i = 0; i < m_KeyframeData.Count; i++) 
				{
					ColorGradientControl.Keyframe key = m_KeyframeData[i] as ColorGradientControl.Keyframe;
					p.Write_float(key.m_Time);
					p.Write_float(key.m_ColorRGB.x);
					p.Write_float(key.m_ColorRGB.y);
					p.Write_float(key.m_ColorRGB.z);
					p.Write_float(key.m_ColorRGB.w);
				}
				p.Send();
			}
			else 
			{
				BankRemotePacket begin = new BankRemotePacket(Pipe);
                begin.Begin(BankRemotePacket.EnumPacketType.USER_1, GetStaticGUID(), Id);
				begin.Write_u32((uint)m_KeyframeData.Count);
				begin.Send();

				int stepSize = 15;

				for(int group = 0; group < m_KeyframeData.Count; group += stepSize)
				{
					BankRemotePacket p = new BankRemotePacket(Pipe);
                    p.Begin(BankRemotePacket.EnumPacketType.USER_2, GetStaticGUID(), Id);
					p.Write_u32((uint)group);
					p.Write_u32((uint)System.Math.Min(group + stepSize, m_KeyframeData.Count));
					int end = System.Math.Min(m_KeyframeData.Count - group, stepSize);
					for(int i = 0; i < end; i++) 
					{
						ColorGradientControl.Keyframe key = m_KeyframeData[group + i] as ColorGradientControl.Keyframe;
						p.Write_float(key.m_Time);
						p.Write_float(key.m_ColorRGB.x);
						p.Write_float(key.m_ColorRGB.y);
						p.Write_float(key.m_ColorRGB.z);
						p.Write_float(key.m_ColorRGB.w);
					}
					p.Send();
				}

				BankRemotePacket endpacket = new BankRemotePacket(Pipe);
                endpacket.Begin(BankRemotePacket.EnumPacketType.USER_3, GetStaticGUID(), Id);
				endpacket.Send();
			}
			
		}

        private static bool RemoteHandler(BankRemotePacket packet, out String errorMessage)
        {
            // USER_0: Full set of keyframes
            // USER_1: Begin partial update
            // USER_2: Partial update contents
            // USER_3: End partial update
            bool handled = false;

            switch ( packet.BankCommand )
            {
                case BankRemotePacket.EnumPacketType.CREATE:
                    {

                        packet.Begin();
                        uint remoteParent = packet.Read_bkWidget();
                        string title = packet.Read_const_char();
                        string memo = packet.Read_const_char();
                        Color fillColor = Widget.DeserializeColor(packet.Read_const_char());
                        bool readOnly = packet.Read_bool();

                        Widget parent;
                        if (packet.BankPipe.RemoteToLocal(remoteParent, out parent, out errorMessage))
                        {
                            WidgetColorGradientEditor local = new WidgetColorGradientEditor(packet.BankPipe, packet.Id, title, memo, fillColor, readOnly);
                            packet.Read_const_char();
                            packet.Read_const_char();
                            packet.Read_const_char();
                            packet.Read_const_char();
                            packet.Read_float();
                            packet.Read_float();
                            uint type = packet.Read_u8();
                            if (type == 5) // rmPtfxKeyframe::KF_VECTOR3
                            {
                                local.useAlpha = false;
                            }
                            packet.End();

                            packet.BankPipe.Associate(packet.Id, local);
                            parent.AddChild(local);
                            handled = true;
                        }
                    }
                    break;
                case BankRemotePacket.EnumPacketType.USER_0: // full set of keyframes
                    {
                        packet.Begin();

				        // find the local version of the the remote widget:
				        WidgetColorGradientEditor editor;
                        if (packet.BankPipe.RemoteToLocal(packet.Id, out editor, out errorMessage))
                        {
                            // get the number of keyframes
                            uint newSize = packet.Read_u32();

                            editor.m_KeyframeData.Clear();

                            for (int i = 0; i < newSize; i++)
                            {
                                ColorGradientControl.Keyframe key = new ColorGradientControl.Keyframe();

                                float time = packet.Read_float();
                                float red = packet.Read_float();
                                float green = packet.Read_float();
                                float blue = packet.Read_float();
                                float alpha = packet.Read_float();
                                key.Set(time, new ColorGradientControl.Vector4(red, green, blue, alpha), new ColorGradientControl.Vector4());
                                editor.m_KeyframeData.Add(key);

                            }
                            packet.End();

                            if (editor.ReceivedKeyFrameData != null)
                            {
                                editor.ReceivedKeyFrameData(null, EventArgs.Empty);
                            }
                            handled = true;
                        }
                    }
                    break;

                case BankRemotePacket.EnumPacketType.USER_1: // begin partial update
                    {
                        packet.Begin();

				        // find the local version of the the remote widget:
				        WidgetColorGradientEditor editor;
                        if (packet.BankPipe.RemoteToLocal(packet.Id, out editor, out errorMessage))
                        {
                            // get the number of keyframes
                            uint newSize = packet.Read_u32();

                            editor.m_KeyframeData = new ArrayList((int)newSize);
                            for (int i = 0; i < newSize; i++)
                            {
                                editor.m_KeyframeData.Add(new ColorGradientControl.Keyframe());
                            }

                            packet.End();
                            handled = true;
                        }
                    }
                    break;

                case BankRemotePacket.EnumPacketType.USER_2: // partial update data
                    {
                        packet.Begin();

				        // find the local version of the the remote widget:
				        WidgetColorGradientEditor editor;
                        if (packet.BankPipe.RemoteToLocal(packet.Id, out editor, out errorMessage))
                        {
                            uint begin = packet.Read_u32();
                            uint end = packet.Read_u32();

                            for (uint i = begin; i < end; i++)
                            {
                                float time = packet.Read_float();
                                float red = packet.Read_float();
                                float green = packet.Read_float();
                                float blue = packet.Read_float();
                                float alpha = packet.Read_float();
                                ColorGradientControl.Keyframe key = new ColorGradientControl.Keyframe();
                                key.Set(time, new ColorGradientControl.Vector4(red, green, blue, alpha), new ColorGradientControl.Vector4());
                                editor.m_KeyframeData[(int)i] = key;
                            }

                            packet.End();
                            handled = true;
                        }
                    }
                    break;

                case BankRemotePacket.EnumPacketType.USER_3: // end partial update
                    {
                        packet.Begin();

				        // find the local version of the the remote widget:
				        WidgetColorGradientEditor editor;
                        if (packet.BankPipe.RemoteToLocal(packet.Id, out editor, out errorMessage))
                        {
                            packet.End();
                            editor.OnValueChanged();

                            if (editor.ReceivedKeyFrameData != null)
                            {
                                editor.ReceivedKeyFrameData(null, EventArgs.Empty);
                            }
                            handled = true;
                        }
                    }
                    break;

                case BankRemotePacket.EnumPacketType.USER_4: //Ui Data update
                    {
                        packet.Begin();

				        // find the local version of the the remote widget:
				        WidgetColorGradientEditor editor;
                        if (packet.BankPipe.RemoteToLocal(packet.Id, out editor, out errorMessage))
                        {
                            for (int i = 0; i < 4; i++)
                            {
                                packet.Read_const_char();
                                packet.Read_u8();
                                packet.Read_u8();
                                packet.Read_u8();
                                packet.Read_u8();

                            }
                            packet.Read_float();
                            packet.Read_float();
                            editor.m_xMin = packet.Read_float();
                            editor.m_xMax = packet.Read_float();
                            packet.Read_float();
                            packet.Read_float();
                            packet.End();
                            editor.OnRangeUpdated();
                            handled = true;
                        }
                    }
                    break;

                default:
                    handled = Widget.RemoteHandlerBase(packet, GetStaticGUID(), out errorMessage);
                    break;
            }

            return handled;
        }

        private void OnRangeUpdated()
        {
            if (RangeUpdated != null)
            {
                RangeUpdated( this, EventArgs.Empty );
            }
        }

        public void OnReceivedKeyFrameData(object sender)
        {
            if ( ReceivedKeyFrameData != null )
            {
                ReceivedKeyFrameData( sender, EventArgs.Empty );
            }
        }


        #region Variables
        bool useAlpha = true;
		float m_xMax=1.0f;
		float m_xMin=0.0f;
		ArrayList m_KeyframeData = new ArrayList();
        public event EventHandler RangeUpdated;
		#endregion
        
        public event EventHandler ReceivedKeyFrameData;

        public float Max
        {
            get { return m_xMax; }
        }

        public float Min
        {
            get { return m_xMin; }
        }

        public bool UseAlpha
        {
            get { return useAlpha; }
        }

        public System.Collections.ArrayList KeyframeData
        {
            get { return m_KeyframeData; }
            set { m_KeyframeData = value; }
        }
	}
}
