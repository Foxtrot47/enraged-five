using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;

namespace Sample_ColorGradient
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class Form1 : System.Windows.Forms.Form
	{
		private ragColorGradientEditor.ColorGradientEditor colorGradientEditor1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public Form1()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.colorGradientEditor1 = new ragColorGradientEditor.ColorGradientEditor();
			this.SuspendLayout();
			// 
			// colorGradientEditor1
			// 
			this.colorGradientEditor1.AllowEditAlpha = true;
			this.colorGradientEditor1.AllowEditColor = true;
			this.colorGradientEditor1.AllowHSBMode = true;
			this.colorGradientEditor1.LabelRes = 3;
			this.colorGradientEditor1.Location = new System.Drawing.Point(0, 0);
			this.colorGradientEditor1.MaxRange = 1F;
			this.colorGradientEditor1.MinRange = 0F;
			this.colorGradientEditor1.Name = "colorGradientEditor1";
			this.colorGradientEditor1.Size = new System.Drawing.Size(568, 344);
			this.colorGradientEditor1.TabIndex = 0;
			// 
			// Form1
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(568, 341);
			this.Controls.Add(this.colorGradientEditor1);
			this.Name = "Form1";
			this.Text = "Form1";
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new Form1());
		}
	}
}
