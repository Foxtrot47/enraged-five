using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;

namespace ragColorGradientEditor
{
	/// <summary>
	/// Summary description for ColorGradientEditor.
	/// </summary>
	public class ColorGradientEditor : System.Windows.Forms.UserControl
	{
		public ragColorGradientEditor.ColorGradientControl colorGradientControl1;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Panel Panel_CurColor;
		private System.Windows.Forms.NumericUpDown Numeric_AlphaValue;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.NumericUpDown Numeric_ColorLocation;
		private System.Windows.Forms.NumericUpDown Numeric_AlphaLocation;
		private System.Windows.Forms.Button Button_Delete;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.CheckBox Check_HSBMode;
		private System.Windows.Forms.CheckBox Check_MinMaxMode;
		private System.Windows.Forms.Button button1;
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public ColorGradientEditor()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			// TODO: Add any initialization after the InitializeComponent call
			this.Numeric_ColorLocation.Minimum = (decimal)this.colorGradientControl1.GetRange().x;
			this.Numeric_ColorLocation.Maximum = (decimal)this.colorGradientControl1.GetRange().y;
			this.Numeric_AlphaLocation.Minimum = (decimal)this.colorGradientControl1.GetRange().x;
			this.Numeric_AlphaLocation.Maximum = (decimal)this.colorGradientControl1.GetRange().y;
			this.colorGradientControl1.OnSelectedPeg += new ColorGradientControl.ColorGradientEventHandler(OnSelectedPeg);
			this.colorGradientControl1.OnPegColorChanged+= new ColorGradientControl.ColorGradientEventHandler(OnPegColorChanged);
			this.colorGradientControl1.OnPegLocationChanged+= new ColorGradientControl.ColorGradientEventHandler(OnPegLocationChanged);
			this.colorGradientControl1.OnRangeChanged+= new ColorGradientControl.ColorGradientRangeEventHandler(OnRangeChanged);

		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}

                this.colorGradientControl1.Dispose();
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.colorGradientControl1 = new ragColorGradientEditor.ColorGradientControl();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.button1 = new System.Windows.Forms.Button();
			this.Check_MinMaxMode = new System.Windows.Forms.CheckBox();
			this.Check_HSBMode = new System.Windows.Forms.CheckBox();
			this.Button_Delete = new System.Windows.Forms.Button();
			this.Numeric_AlphaLocation = new System.Windows.Forms.NumericUpDown();
			this.Numeric_ColorLocation = new System.Windows.Forms.NumericUpDown();
			this.label2 = new System.Windows.Forms.Label();
			this.Numeric_AlphaValue = new System.Windows.Forms.NumericUpDown();
			this.Panel_CurColor = new System.Windows.Forms.Panel();
			this.label1 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.groupBox1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Numeric_AlphaLocation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Numeric_ColorLocation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Numeric_AlphaValue)).BeginInit();
			this.SuspendLayout();
			// 
			// colorGradientControl1
			// 
			this.colorGradientControl1.AllowEditAlpha = true;
			this.colorGradientControl1.AllowEditColor = true;
			this.colorGradientControl1.AllowHSBMode = true;
			this.colorGradientControl1.LabelRes = 3;
			this.colorGradientControl1.Location = new System.Drawing.Point(0, 0);
			this.colorGradientControl1.MaxRange = 1F;
			this.colorGradientControl1.MinRange = 0F;
			this.colorGradientControl1.Name = "colorGradientControl1";
			this.colorGradientControl1.ShowLables = true;
			this.colorGradientControl1.Size = new System.Drawing.Size(560, 240);
			this.colorGradientControl1.TabIndex = 0;
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.button1);
			this.groupBox1.Controls.Add(this.Check_MinMaxMode);
			this.groupBox1.Controls.Add(this.Check_HSBMode);
			this.groupBox1.Controls.Add(this.Button_Delete);
			this.groupBox1.Controls.Add(this.Numeric_AlphaLocation);
			this.groupBox1.Controls.Add(this.Numeric_ColorLocation);
			this.groupBox1.Controls.Add(this.label2);
			this.groupBox1.Controls.Add(this.Numeric_AlphaValue);
			this.groupBox1.Controls.Add(this.Panel_CurColor);
			this.groupBox1.Controls.Add(this.label1);
			this.groupBox1.Controls.Add(this.label3);
			this.groupBox1.Controls.Add(this.label4);
			this.groupBox1.Location = new System.Drawing.Point(8, 240);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(552, 96);
			this.groupBox1.TabIndex = 1;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Keys";
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(312, 16);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(80, 32);
			this.button1.TabIndex = 12;
			this.button1.Text = "button1";
			this.button1.Visible = false;
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// Check_MinMaxMode
			// 
			this.Check_MinMaxMode.Location = new System.Drawing.Point(440, 56);
			this.Check_MinMaxMode.Name = "Check_MinMaxMode";
			this.Check_MinMaxMode.Size = new System.Drawing.Size(96, 24);
			this.Check_MinMaxMode.TabIndex = 11;
			this.Check_MinMaxMode.Text = "MinMaxMode";
			this.Check_MinMaxMode.CheckStateChanged += new System.EventHandler(this.checkBox1_CheckStateChanged_1);
			// 
			// Check_HSBMode
			// 
			this.Check_HSBMode.Location = new System.Drawing.Point(440, 24);
			this.Check_HSBMode.Name = "Check_HSBMode";
			this.Check_HSBMode.Size = new System.Drawing.Size(88, 24);
			this.Check_HSBMode.TabIndex = 10;
			this.Check_HSBMode.Text = "HSBMode";
			this.Check_HSBMode.CheckStateChanged += new System.EventHandler(this.checkBox1_CheckStateChanged);
			// 
			// Button_Delete
			// 
			this.Button_Delete.Enabled = false;
			this.Button_Delete.Location = new System.Drawing.Point(312, 52);
			this.Button_Delete.Name = "Button_Delete";
			this.Button_Delete.Size = new System.Drawing.Size(72, 24);
			this.Button_Delete.TabIndex = 7;
			this.Button_Delete.Text = "Delete";
			this.Button_Delete.Click += new System.EventHandler(this.Button_Delete_Click);
			// 
			// Numeric_AlphaLocation
			// 
			this.Numeric_AlphaLocation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.Numeric_AlphaLocation.DecimalPlaces = 3;
			this.Numeric_AlphaLocation.Enabled = false;
			this.Numeric_AlphaLocation.Increment = new System.Decimal(new int[] {
																					1,
																					0,
																					0,
																					131072});
			this.Numeric_AlphaLocation.Location = new System.Drawing.Point(192, 24);
			this.Numeric_AlphaLocation.Name = "Numeric_AlphaLocation";
			this.Numeric_AlphaLocation.Size = new System.Drawing.Size(80, 20);
			this.Numeric_AlphaLocation.TabIndex = 6;
			this.Numeric_AlphaLocation.ValueChanged += new System.EventHandler(this.Numeric_AlphaLocation_ValueChanged);
			// 
			// Numeric_ColorLocation
			// 
			this.Numeric_ColorLocation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.Numeric_ColorLocation.DecimalPlaces = 3;
			this.Numeric_ColorLocation.Enabled = false;
			this.Numeric_ColorLocation.Increment = new System.Decimal(new int[] {
																					1,
																					0,
																					0,
																					131072});
			this.Numeric_ColorLocation.Location = new System.Drawing.Point(192, 56);
			this.Numeric_ColorLocation.Name = "Numeric_ColorLocation";
			this.Numeric_ColorLocation.Size = new System.Drawing.Size(80, 20);
			this.Numeric_ColorLocation.TabIndex = 5;
			this.Numeric_ColorLocation.ValueChanged += new System.EventHandler(this.Numeric_ColorLocation_ValueChanged);
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(8, 26);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(48, 16);
			this.label2.TabIndex = 4;
			this.label2.Text = "Opacity:";
			// 
			// Numeric_AlphaValue
			// 
			this.Numeric_AlphaValue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.Numeric_AlphaValue.Enabled = false;
			this.Numeric_AlphaValue.Location = new System.Drawing.Point(56, 24);
			this.Numeric_AlphaValue.Maximum = new System.Decimal(new int[] {
																			   255,
																			   0,
																			   0,
																			   0});
			this.Numeric_AlphaValue.Name = "Numeric_AlphaValue";
			this.Numeric_AlphaValue.Size = new System.Drawing.Size(48, 20);
			this.Numeric_AlphaValue.TabIndex = 2;
			this.Numeric_AlphaValue.Value = new System.Decimal(new int[] {
																			 255,
																			 0,
																			 0,
																			 0});
			this.Numeric_AlphaValue.ValueChanged += new System.EventHandler(this.Numeric_AlphaValue_ValueChanged);
			// 
			// Panel_CurColor
			// 
			this.Panel_CurColor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.Panel_CurColor.Enabled = false;
			this.Panel_CurColor.Location = new System.Drawing.Point(56, 56);
			this.Panel_CurColor.Name = "Panel_CurColor";
			this.Panel_CurColor.Size = new System.Drawing.Size(48, 22);
			this.Panel_CurColor.TabIndex = 0;
			this.Panel_CurColor.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Panel_CurColor_MouseDown);
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(20, 60);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(38, 16);
			this.label1.TabIndex = 3;
			this.label1.Text = "Color:";
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(136, 60);
			this.label3.Name = "label3";
			this.label3.TabIndex = 8;
			this.label3.Text = "Location:";
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(136, 26);
			this.label4.Name = "label4";
			this.label4.TabIndex = 9;
			this.label4.Text = "Location:";
			// 
			// ColorGradientEditor
			// 
			this.Controls.Add(this.groupBox1);
			this.Controls.Add(this.colorGradientControl1);
			this.Name = "ColorGradientEditor";
			this.Size = new System.Drawing.Size(568, 344);
			this.groupBox1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.Numeric_AlphaLocation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Numeric_ColorLocation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Numeric_AlphaValue)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion


		#region Properties
		[Description("Minimum range of the control"),Category("Data")]
		public float MinRange
		{	
			get 
			{ 
				return colorGradientControl1.GetRange().x;
			}
			set 
			{
				colorGradientControl1.SetRange(value,colorGradientControl1.GetRange().y);
			}
		}
		[Description("Maximum range of the control"),Category("Data")]
		public float MaxRange
		{	
			get 
			{ 
				return colorGradientControl1.GetRange().y;
			}
			set 
			{
				colorGradientControl1.SetRange(colorGradientControl1.GetRange().x,value);
			}
		}
		[Description("Label Resolution"),Category("Data")]
		public int LabelRes
		{	
			get 
			{ 
				return colorGradientControl1.GetLabelResolution();
			}
			set 
			{
				colorGradientControl1.SetLabelResolution(value);
			}
		}


		[Description("Allow HSB Interpolation"),Category("Data")]
		public bool AllowHSBMode
		{
			get
			{
				return colorGradientControl1.AllowHSBMode;
			}
			set 
			{
				colorGradientControl1.AllowHSBMode=value;
				Check_HSBMode.Visible = value;
			}
		}

		[Description("Allow Color Editing"),Category("Data")]
		public bool AllowEditColor
		{
			get
			{
				return colorGradientControl1.AllowEditColor;
			}
			set 
			{
				colorGradientControl1.AllowEditColor=value;
			}
		}
		[Description("Allow Alpha Editing"),Category("Data")]
		public bool AllowEditAlpha
		{
			get
			{
				return colorGradientControl1.AllowEditAlpha;
			}
			set 
			{
				colorGradientControl1.AllowEditAlpha=value;
			}
		}

		#endregion

		#region Variables
		ColorGradientControl.KeyPeg m_CurSelectedPeg;
		#endregion

		public ArrayList GetKeyframeData()
		{
			return this.colorGradientControl1.GetKeyframeData();
		}
		public void SetRange(float min,float max)
		{
			colorGradientControl1.SetRange(min,max);
		}
		public void ClearKeyframes()
		{
			colorGradientControl1.ClearKeyframes();
		}
		
		public void BuildFromData(ArrayList keyframeData)
		{
			colorGradientControl1.BuildFromData(keyframeData);
		}
		public void BuildFromControl(ColorGradientControl cgc)
		{
			colorGradientControl1.BuildFromGradientControl(cgc);
		}
		public void AddAlphaKeyframe(ColorGradientControl.Keyframe key,ColorGradientControl.Keyframe midpoint)
		{
			colorGradientControl1.AddAlphaKey(key.m_Time,key.m_ColorRGB,midpoint);
		}
		public void AddColorKeyframe(ColorGradientControl.Keyframe key,ColorGradientControl.Keyframe midpoint)
		{
			colorGradientControl1.AddColorKey(key.m_Time,key.m_ColorRGB,midpoint);
		}
		private void OnSelectedPeg(object sender,ColorGradientControl.ColorGradientEventArgs e) 
		{
			m_CurSelectedPeg=e.CurPeg;
			if(e.CurPeg.m_IsMidKey)
			{
				Panel_CurColor.BackColor = Color.FromKnownColor(KnownColor.Control);
				Panel_CurColor.Enabled=false;
				Numeric_AlphaValue.Value=0;;
				Numeric_AlphaValue.Enabled=false;
				Numeric_ColorLocation.Value=Numeric_ColorLocation.Minimum;
				Numeric_ColorLocation.Enabled=false;
				Numeric_AlphaLocation.Value=Numeric_AlphaLocation.Minimum;
				Numeric_AlphaLocation.Enabled=false;
				Button_Delete.Enabled=false;
				return;
			}

			if(e.CurPeg.m_IsAlphaKey)
			{
				Panel_CurColor.Enabled=false;
				Panel_CurColor.BackColor = Color.FromKnownColor(KnownColor.Control);
				Numeric_ColorLocation.Enabled=false;

				if(e.CurPeg.m_IsLocked)
				{
					Numeric_AlphaLocation.Enabled=false;
					Button_Delete.Enabled=false;
				}
				else
				{
					Numeric_AlphaLocation.Enabled=true;
					Button_Delete.Enabled = true;
				}

				Numeric_AlphaValue.Enabled=true;
                int alpha = (int)System.Math.Round(e.CurPeg.m_KeyData.m_ColorRGB.w * 255.0f, MidpointRounding.AwayFromZero);
				Numeric_AlphaValue.Value = alpha;
				Numeric_AlphaLocation.Value = (decimal) e.CurPeg.m_KeyData.m_Time;
			}
			else
			{
				Numeric_AlphaValue.Value=0;
				Numeric_AlphaValue.Enabled=false;
				Numeric_AlphaLocation.Enabled=false;
				
				if(e.CurPeg.m_IsLocked)
				{
					Numeric_ColorLocation.Enabled=false;
					Button_Delete.Enabled=false;
				}
				else
				{
					Numeric_ColorLocation.Enabled=true;
					Button_Delete.Enabled=true;
				}
				Panel_CurColor.Enabled=true;
				int red   = (int)System.Math.Round(e.CurPeg.m_KeyData.m_ColorRGB.x*255.0f, MidpointRounding.AwayFromZero);
                int green = (int)System.Math.Round(e.CurPeg.m_KeyData.m_ColorRGB.y * 255.0f, MidpointRounding.AwayFromZero);
				int blue  = (int)System.Math.Round(e.CurPeg.m_KeyData.m_ColorRGB.z*255.0f, MidpointRounding.AwayFromZero);
				Panel_CurColor.BackColor = Color.FromArgb(255,red,green,blue);
				Numeric_ColorLocation.Value = (decimal)e.CurPeg.m_KeyData.m_Time;
			}
		}
		private void OnRangeChanged(object sender,ColorGradientControl.ColorGradientRangeEventArgs e) 
		{
			Numeric_ColorLocation.Minimum = (decimal)e.NewRange.x;
			Numeric_ColorLocation.Maximum = (decimal)e.NewRange.y;
			Numeric_AlphaLocation.Minimum = (decimal)e.NewRange.x;
			Numeric_AlphaLocation.Maximum = (decimal)e.NewRange.y;
		}

		private void OnPegLocationChanged(object sender,ColorGradientControl.ColorGradientEventArgs e) 
		{
			if( m_CurSelectedPeg!=e.CurPeg || m_CurSelectedPeg.m_IsMidKey)
				return;
			if(e.CurPeg.m_IsAlphaKey)
			{
				Numeric_AlphaLocation.Value = (decimal)e.CurPeg.m_KeyData.m_Time;
			}
			else
			{
				Numeric_ColorLocation.Value = (decimal)e.CurPeg.m_KeyData.m_Time;
			}
		}

		private void OnPegColorChanged(object sender,ColorGradientControl.ColorGradientEventArgs e) 
		{
			if(m_CurSelectedPeg!=e.CurPeg)
				return;

			if(e.CurPeg.m_IsAlphaKey)
			{
                int alpha = (int)System.Math.Round(e.CurPeg.m_KeyData.m_ColorRGB.w * 255.0f, MidpointRounding.AwayFromZero);
				Numeric_AlphaValue.Value = alpha;
			}
			else
			{
                int red = (int)System.Math.Round(e.CurPeg.m_KeyData.m_ColorRGB.x * 255.0f, MidpointRounding.AwayFromZero);
                int green = (int)System.Math.Round(e.CurPeg.m_KeyData.m_ColorRGB.y * 255.0f, MidpointRounding.AwayFromZero);
                int blue = (int)System.Math.Round(e.CurPeg.m_KeyData.m_ColorRGB.z * 255.0f, MidpointRounding.AwayFromZero);
				Panel_CurColor.BackColor = Color.FromArgb(255,red,green,blue);
			}
		}

		private void Panel_CurColor_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			if(e.Button == MouseButtons.Left)
			{
				if( (m_CurSelectedPeg!=null) && (!m_CurSelectedPeg.m_IsAlphaKey))
					this.colorGradientControl1.ShowColorPicker(m_CurSelectedPeg);
			}
		}

		private void Numeric_AlphaValue_ValueChanged(object sender, System.EventArgs e)
		{
			if( (m_CurSelectedPeg!=null) && (m_CurSelectedPeg.m_IsAlphaKey))
			{
				m_CurSelectedPeg.m_KeyData.m_ColorRGB.w = ((float)Numeric_AlphaValue.Value)/255.0f;
				m_CurSelectedPeg.m_KeyData.m_ColorHSB.w = ((float)Numeric_AlphaValue.Value)/255.0f;
				this.colorGradientControl1.UpdatePegData(m_CurSelectedPeg);
			}
		}

		private void Numeric_ColorLocation_ValueChanged(object sender, System.EventArgs e)
		{
			if( (m_CurSelectedPeg!=null) && (!m_CurSelectedPeg.m_IsMidKey))
			{
				m_CurSelectedPeg.m_KeyData.m_Time = (float) Numeric_ColorLocation.Value;
				this.colorGradientControl1.UpdatePegData(m_CurSelectedPeg);
			}
		
		}

		private void Numeric_AlphaLocation_ValueChanged(object sender, System.EventArgs e)
		{
			if( (m_CurSelectedPeg!=null) && (!m_CurSelectedPeg.m_IsMidKey))
			{
				m_CurSelectedPeg.m_KeyData.m_Time = (float) Numeric_AlphaLocation.Value;
				this.colorGradientControl1.UpdatePegData(m_CurSelectedPeg);
			}
		}

		private void Button_Delete_Click(object sender, System.EventArgs e)
		{
			if( (m_CurSelectedPeg!=null) && (!m_CurSelectedPeg.m_IsMidKey))
			{
				this.colorGradientControl1.UserDelKey(m_CurSelectedPeg);
			}
		}

		private void checkBox1_CheckStateChanged(object sender, System.EventArgs e)
		{
			this.colorGradientControl1.SetHSBMode(Check_HSBMode.Checked);
		}

		private void checkBox1_CheckStateChanged_1(object sender, System.EventArgs e)
		{
			this.colorGradientControl1.SetMinMaxMode(Check_MinMaxMode.Checked);
		}

		private void button1_Click(object sender, System.EventArgs e)
		{
			ArrayList keyData = new ArrayList();

			ColorGradientControl.Keyframe key = new ColorGradientControl.Keyframe();
			key.Set(0.0f,new ColorGradientControl.Vector4(1.0f,0.0f,0.0f,1.0f),new ColorGradientControl.Vector4());
			keyData.Add(key);
			key = new ColorGradientControl.Keyframe();
			key.Set(0.5f,new ColorGradientControl.Vector4(0.0f,0.0f,1.0f,1.0f),new ColorGradientControl.Vector4());
			keyData.Add(key);
			key = new ColorGradientControl.Keyframe();
			key.Set(0.10f,new ColorGradientControl.Vector4(0.5f,0.0f,0.5f,1.0f),new ColorGradientControl.Vector4());
			keyData.Add(key);
			this.colorGradientControl1.BuildFromData(keyData);
			/*
			ColorGradientControl.Keyframe midtest = new ColorGradientControl.Keyframe();
			midtest.m_Time=0.25f;
			midtest.m_ColorRGB = new ColorGradientControl.Vector4(0.0f,1.0f,1.0f,1.0f);
			this.colorGradientControl1.AddColorKey(1.0f,new ColorGradientControl.Vector4(0.0f,1.0f,0.0f,1.0f),midtest);
			*/
		}
	}
}
