using System;
using System.Collections.Generic;
using System.Text;

using ragCore;

namespace ragProfiler
{
    public abstract class ProfileStatsItem : ProfileItem
    {
        public ProfileStatsItem( string name, uint id, ProfileGroupItem group )
            : base( name, id )
        {
            m_group = group;
        }

        #region Variables
        private ProfileGroupItem m_group;
        #endregion

        #region Properties
        public ProfileGroupItem Group
        {
            get
            {
                return m_group;
            }
        }
        #endregion

        #region Abstract Functions
        public abstract float GetStat( int stat );
        public abstract void SetStat( int stat, float value );
        #endregion
    };

    public class ProfileTimeStatsItem : ProfileStatsItem
    {
        public ProfileTimeStatsItem( string name, uint id, ProfileGroupItem group )
            : base( name, id, group )
        {
            
        }

        #region Enums
        public enum StatType
        {
            FrameTime,
            AverageFrameTime,
            PeakFrameTime,
            FrameCalls,
            AverageCalls,
            PeakCalls
        }

        public enum TimeMode
        {
            ElapsedTime,
            Counter0,
            Counter1
        }
        #endregion

        #region Variables
        private static TimeMode sm_timeMode = TimeMode.ElapsedTime;

        private float m_elapsedFrameTime;
        private float m_counter0Value;
        private float m_counter1Value;

        private float m_averageElapsedFrameTime;
        private float m_averageCounter0Value;
        private float m_averageCounter1Value;

        private float m_peakElapsedFrameTime;
        private float m_peakCounter0Value;
        private float m_peakCounter1Value;

        private int m_frameCalls;
        private float m_averageFrameCalls;
        private int m_peakFrameCalls;
        #endregion

        #region Properties
        public static TimeMode Mode
        {
            get
            {
                return sm_timeMode;
            }
            set
            {
                sm_timeMode = value;
            }
        }

        public float FrameTime
        {
            get
            {
                switch ( ProfileTimeStatsItem.Mode )
                {
                    case TimeMode.Counter0:
                        return m_counter0Value;

                    case TimeMode.Counter1:
                        return m_counter1Value;

                    default:
                        return m_elapsedFrameTime;
                }
            }
            set
            {
                m_counter0Value = m_counter1Value = m_elapsedFrameTime = value;
            }
        }

        public float ElapsedFrameTime
        {
            set
            {
                m_elapsedFrameTime = value;
            }
        }

        public float Counter0Value
        {
            set
            {
                m_counter0Value = value;
            }
        }

        public float Counter1Value
        {
            set
            {
                m_counter1Value = value;
            }
        }
        
        public float AverageFrameTime
        {
            get
            {
                switch ( ProfileTimeStatsItem.Mode )
                {
                    case TimeMode.Counter0:
                        return m_averageCounter0Value;

                    case TimeMode.Counter1:
                        return m_averageCounter1Value;

                    default:
                        return m_averageElapsedFrameTime;
                }
            }
            set
            {
                m_averageCounter0Value = m_averageCounter1Value = m_averageElapsedFrameTime = value;
            }
        }

        public float AverageElapsedFrameTime
        {
            set
            {
                m_averageElapsedFrameTime = value;
            }
        }

        public float AverageCounter0Value
        {
            set
            {
                m_averageCounter0Value = value;
            }
        }

        public float AverageCounter1Value
        {
            set
            {
                m_averageCounter1Value = value;
            }
        }
        
        public float PeakFrameTime
        {
            get
            {
                switch ( ProfileTimeStatsItem.Mode )
                {
                    case TimeMode.Counter0:
                        return m_peakCounter0Value;

                    case TimeMode.Counter1:
                        return m_peakCounter1Value;

                    default:
                        return m_peakElapsedFrameTime;
                }
            }
            set
            {
                m_peakCounter0Value = m_peakCounter1Value = m_peakElapsedFrameTime = value;
            }
        }

        public float PeakElapsedFrameTime
        {
            set
            {
                m_peakElapsedFrameTime = value;
            }
        }

        public float PeakCounter0Value
        {
            set
            {
                m_peakCounter0Value = value;
            }
        }

        public float PeakCounter1Value
        {
            set
            {
                m_peakCounter1Value = value;
            }
        }
        
        public int FrameCalls
        {
            get
            {
                return m_frameCalls;
            }
            set
            {
                m_frameCalls = value;
            }
        }

        public float AverageCalls
        {
            get
            {
                return m_averageFrameCalls;
            }
            set
            {
                m_averageFrameCalls = value;
            }
        }

        public int PeakCalls
        {
            get
            {
                return m_peakFrameCalls;
            }
            set
            {
                m_peakFrameCalls = value;
            }
        }
        #endregion

        #region Overrides
        public override float GetStat( int stat )
        {
            switch ( (StatType)stat )
            {
                case StatType.AverageCalls:
                    return this.AverageCalls;

                case StatType.AverageFrameTime:
                    return this.AverageFrameTime;

                case StatType.FrameCalls:
                    return this.FrameCalls;

                case StatType.FrameTime:
                    return this.FrameTime;

                case StatType.PeakCalls:
                    return this.PeakCalls;

                case StatType.PeakFrameTime:
                    return this.PeakFrameTime;

                default:
                    return 0.0f;
            }
        }

        public override void SetStat( int stat, float value )
        {
            switch ( (StatType)stat )
            {
                case StatType.AverageCalls:
                    this.AverageCalls = (float)Convert.ToDouble( value );
                    break;
                case StatType.AverageFrameTime:
                    this.AverageFrameTime = (float)Convert.ToDouble( value );
                    break;
                case StatType.FrameCalls:
                    this.FrameCalls = Convert.ToInt32( value );
                    break;
                case StatType.FrameTime:
                    this.FrameTime = (float)Convert.ToDouble( value );
                    break;
                case StatType.PeakCalls:
                    this.PeakCalls = Convert.ToInt32( value );
                    break;
                case StatType.PeakFrameTime:
                    this.PeakFrameTime = (float)Convert.ToDouble( value );
                    break;
                default:
                    break;
            }
        }
        #endregion
    }

    public class ProfileValueStatsItem : ProfileStatsItem
    {
        public ProfileValueStatsItem( string name, uint id, ProfileGroupItem group )
            : base( name, id, group )
        {

        }

        public ProfileValueStatsItem( string name, uint id, ProfileGroupItem group, float value )
            : base( name, id, group )
        {
            m_currentValue = value;
        }

        #region Enums
        public enum StatType
        {
            Current,
            Average,
            Peak,
            ValueDefault
        };
        #endregion

        #region Variables
        private StatType m_defaultValueReturns = StatType.Current;
        private float m_currentValue;
        private float m_averageValue;
        private float m_peakValue;
        #endregion

        #region Properties
        public float CurrentValue
        {
            get
            {
                return m_currentValue;
            }
            set
            {
                m_currentValue = value;
            }
        }

        public float AverageValue
        {
            get
            {
                return m_averageValue;
            }
            set
            {
                m_averageValue = value;
            }
        }

        public float PeakValue
        {
            get
            {
                return m_peakValue;
            }
            set
            {
                m_peakValue = value;
            }
        }
        #endregion

        #region Overrides
        public override float GetStat( int stat )
        {
            if ( (StatType)stat == StatType.ValueDefault )
            {
                stat = (int)m_defaultValueReturns;
            }

            switch ( (StatType)stat )
            {
                case StatType.Average:
                    return this.AverageValue;

                case StatType.Peak:
                    return this.PeakValue;

                default:
                    return this.CurrentValue;
            }
        }

        public override void SetStat( int stat, float value )
        {
            if ( (StatType)stat == StatType.ValueDefault )
            {
                stat = (int)m_defaultValueReturns;
            }

            switch ( (StatType)stat )
            {
                case StatType.Average:
                    try
                    {
                        this.AverageValue = value;
                    }
                    catch
                    {
                        this.AverageValue = (value > 0.0f) ? float.MaxValue : float.MinValue;
                    }
                    break;
                case StatType.Peak:
                    try
                    {
                        this.PeakValue = value;
                    }
                    catch
                    {
                        this.PeakValue = (value > 0.0f) ? float.MaxValue : float.MinValue;
                    }
                    break;
                default:
                    try
                    {
                        this.CurrentValue = value;
                    }
                    catch
                    {
                        this.CurrentValue = (value > 0.0f) ? float.MaxValue : float.MinValue;
                    }
                    break;
            }
        }
        #endregion
    }

    public class ProfileArrayValueStatsItem : ProfileValueStatsItem
    {
        public ProfileArrayValueStatsItem( string name, uint id, ProfileValueStatsItemArray array )
            : base( name, id, array.Group )
        {
            m_array = array;
        }

        public ProfileArrayValueStatsItem( string name, uint id, ProfileValueStatsItemArray array, float value )
            : base( name, id, array.Group, value )
        {
            m_array = array;
        }

        #region Variables
        private ProfileValueStatsItemArray m_array;
        #endregion

        #region Properties
        public ProfileValueStatsItemArray Array
        {
            get
            {
                return m_array;
            }
        }
        #endregion
    }

    public class ProfileValueStatsItemArray : ProfileStatsItem
    {
        public ProfileValueStatsItemArray( string name, uint id, ProfileGroupItem group )
            : base( name, id, group )
        {

        }

        #region Variables
        private List<ProfileArrayValueStatsItem> m_values = new List<ProfileArrayValueStatsItem>();
        #endregion

        #region Properties
        public List<ProfileArrayValueStatsItem> Values
        {
            get
            {
                return m_values;
            }
        }
        #endregion

        #region Overrides
        public override float GetStat( int stat )
        {
            // do nothing
            return 0.0f;
        }

        public override void SetStat( int stat, float value )
        {
            // do nothing
        }
        #endregion
    }
}
