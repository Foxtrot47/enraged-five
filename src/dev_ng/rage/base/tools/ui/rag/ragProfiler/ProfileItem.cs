using System;
using System.Collections.Generic;
using System.Text;

namespace ragProfiler
{
    public abstract class ProfileItem
    {
        protected ProfileItem( string name, uint guid )
        {
            m_name = name;
            m_guid = guid;
        }

        #region Variables
        protected string m_name;
        protected uint m_guid;
        #endregion

        #region Properties
        public string Name
        {
            get
            {
                return m_name;
            }
        }

        public uint ID
        {
            get
            {
                return m_guid;
            }
        }
        #endregion

        #region Overrides
        public override bool Equals( object obj )
        {
            if ( obj == null )
            {
                return false;
            }

            ProfileItem s = obj as ProfileItem;
            if ( (object)s == null )
            {
                return false;
            }

            return this.ID == s.ID;
        }

        public override int GetHashCode()
        {
            return Convert.ToInt32( m_guid );
        }

        public override string ToString()
        {
            return m_name;
        }
        #endregion
    }
}
