using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Windows.Forms;

using ragCompression;
using ragCore;
using RSG.Base.Logging;

namespace ragProfiler
{
    public class ProfilePipe : NamedPipeSocket
    {
        public ProfilePipe( HandlePacketDelegate handlePacketDel )
            : base()
        {
            m_handlePacket = handlePacketDel;

            m_compressionFactory = new CompressionFactory();
            m_compressionFactory.RegisterCompressionData( new datCompressionData() );
            m_compressionFactory.RegisterCompressionData( new fiCompressionData() );
        }

        #region Delegates
        public delegate void HandlePacketDelegate( RemotePacket p );
        #endregion

        #region Variables
        private HandlePacketDelegate m_handlePacket = null;
        private CompressionFactory m_compressionFactory = null;
        #endregion

        #region Overrides
        public override bool Create( PipeID pipeId, bool wait )
        {
            m_IsConnected = false;

            if ( m_Listener == null )
            {
                m_Listener = new TcpListener( IPAddress.Any, pipeId.Port );
                m_Listener.Start();

                // wait until we have a connection:
                if ( wait )
                {
                    while ( !m_Listener.Pending() )
                    {
                        Thread.Sleep( 100 );
                    }

                    m_Socket = m_Listener.AcceptSocket();
                    if ( m_Socket != null )
                    {
                        m_IsConnected = true;
                        m_Socket.Blocking = false;
                        return true;
                    }
                }
            }

            return false;
        }
        #endregion

        #region Public Functions
        public int ReadPipe( Dictionary<uint, CompressionData> dataDictionary )
        {
            int dataCount = 0;

            if ( this.IsValid() )
            {
                RemotePacket p = new RemotePacket( this );
                if ( this.HasData() )
                {
                    // retrieve the data
                    byte[] bytes = new byte[this.GetNumBytesAvailable()];
                    int numRead = this.ReadData( bytes, bytes.Length, false );

                    // send the data to the factory for decompression
                    m_compressionFactory.DecompressData( bytes, (UInt32)numRead );

                    // parse just the count from every decompressed data set
                    foreach ( CompressionData cData in m_compressionFactory.Data )
                    {
                        int index = 0;
                        p.ReceiveHeader( cData.DecompressedData, ref index );

                        if ( p.Length > 0 )
                        {
                            if ( index >= cData.DecompressedData.Length )
                            {
                                LogFactory.ApplicationLog.Error("RemotePacket expected more data than was decompressed.");
                                break;
                            }

                            p.ReceiveStorage( cData.DecompressedData, ref index );
                        }

                        if ( p.Command == (ushort)ProfileRemotePacket.Commands.Count )
                        {
                            p.Begin();
                            uint count = p.Read_u32();
                            p.End();

                            if ( !dataDictionary.ContainsKey( count ) )
                            {
                                dataDictionary.Add( count, cData );
                            }
                            else
                            {
                                MessageBox.Show( PlugIn.Instance.ParentControl,
                                    "The number of the COUNT command is already in use.",
                                    "Profiler Error", MessageBoxButtons.OK, MessageBoxIcon.Error );
                                return -1;
                            }
                        }
                        else
                        {
                            MessageBox.Show( PlugIn.Instance.ParentControl,
                                "Decompressed data packet does not contain a COUNT command at the beginning.\n"
                                + "You may be using an older version of //rage/dev/rage/base/profile/pfpacket.h and .cpp",
                                "Profiler Error", MessageBoxButtons.OK, MessageBoxIcon.Error );
                            return -1;
                        }
                    }

                    // remove everything
                    dataCount = m_compressionFactory.Data.Count;
                    m_compressionFactory.Data.Clear();
                }
            }

            return dataCount;
        }

        public int HandlePackets( CompressionData cData )
        {
            int count = 0;
            int index = 0;
            while ( index < cData.DecompressedData.Length )
            {
                RemotePacket p = new RemotePacket( this );
                p.ReceiveHeader( cData.DecompressedData, ref index );

                if ( p.Length > 0 )
                {
                    if ( index >= cData.DecompressedData.Length )
                    {
                        LogFactory.ApplicationLog.Error("RemotePacket expected more data than was decompressed.");
                        return -1;
                    }

                    p.ReceiveStorage( cData.DecompressedData, ref index );
                }

                if ( m_handlePacket != null )
                {
                    m_handlePacket( p );
                    ++count;
                }
            }

            return count;
        }
        #endregion
    }
}
