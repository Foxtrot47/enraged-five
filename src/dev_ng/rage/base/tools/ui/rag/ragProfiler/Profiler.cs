//#define TEST_MODE

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Threading;
using System.Windows.Forms;

using GlacialComponents.Controls.GlacialTreeList;
using ragCompression;
using ragCore;
using ragWidgetPage;
using TD.SandDock;
using ragWidgets;
using RSG.Base.Logging;

namespace ragProfiler
{
    public partial class Profiler : WidgetPage
    {
        protected Profiler()
        {
            InitializeComponent();
        }

        public Profiler(string appName, IBankManager bankMgr, DockControl dockControl, ILog log, int viewNum)
            : base( appName, bankMgr, null, dockControl, log )
        {
            InitializeComponent();

            InitializeAdditionalComponents();

            m_numView = viewNum;
        }

        private void InitializeAdditionalComponents()
        {
            this.DockControl.CloseAction = DockControlCloseAction.Dispose;

            this.DockControl.Closed += new EventHandler( DockControl_Closed );

            // build the image list of colors
            foreach ( Color c in ProfileStatSamples.Colors )
            {
                Bitmap img = new Bitmap( 16, 16 );
                for ( int x = 0; x < 16; ++x )
                {
                    for ( int y = 0; y < 16; ++y )
                    {
                        img.SetPixel( x, y, c );
                    }
                }

                this.colorKeyImageList.Images.Add( img );
            }

            sm_profilerComponent.LockForUpdate = true;

            sm_profilerComponent.PageCreated += new ProfilePageEventHandler( profilerComponent_PageCreated );
            sm_profilerComponent.PageDestroyed += new ProfilePageEventHandler( profilerComponent_PageDestroyed );

            sm_profilerComponent.GroupLinkedToPage += new ProfileGroupEventHandler( profilerComponent_GroupEventHandler );
            sm_profilerComponent.GroupDestroyed += new ProfileGroupEventHandler( profilerComponent_GroupEventHandler );

            sm_profilerComponent.ArrayStatsCreated += new ProfileStatsEventHandler( profilerComponent_StatsEventHandler );
            sm_profilerComponent.ArrayStatsDestroyed += new ProfileStatsEventHandler( profilerComponent_StatsEventHandler );

            sm_profilerComponent.TimeStatsCreated += new ProfileStatsEventHandler( profilerComponent_StatsEventHandler );
            sm_profilerComponent.TimeStatsDestroyed += new ProfileStatsEventHandler( profilerComponent_StatsEventHandler );

            sm_profilerComponent.ValueStatsCreated += new ProfileStatsEventHandler( profilerComponent_StatsEventHandler );
            sm_profilerComponent.ValueStatsDestroyed += new ProfileStatsEventHandler( profilerComponent_StatsEventHandler );

            sm_profilerComponent.ArrayValueStatsDestroyed += new ProfileStatsEventHandler( profilerComponent_StatsEventHandler );

            foreach ( ProfilePageItem page in sm_profilerComponent.PageItems )
            {
                this.pageToolStripComboBox.Items.Add( page );
            }

            sm_profilerComponent.LockForUpdate = false;

            m_maxValue = c_maximum;
        }

        #region Constants
        private const int c_ProfilerPortOffset = (int)ragCore.PipeID.EnumSocketOffset.SOCKET_OFFSET_USER5 + 40;
        private const string c_myName = "Profiler";
        private const float c_maximum = 1.0f / 1000.0f;
        #endregion

        #region Variables
        private static int sm_numViews = 0;
        private static bool sm_isConnected = false;
        private static Thread sm_connectThread = null;
        private static Thread sm_readThread = null;
        private static uint sm_messageCount = 0;
        private static ProfilePipe sm_pipe;
        private static ProfilerComponent sm_profilerComponent = new ProfilerComponent();

        private static Mutex sm_mutex = new Mutex();
        private static Dictionary<uint, int> sm_displayedPageRefCountDictionary = new Dictionary<uint, int>();

#if TEST_MODE
        private static Random sm_randomNumberGenerator = new Random();  // TEMP
        private static uint sm_tempKey = 0;
        private static uint s_tempCounter = 0;
#endif

        private int m_numView = 0;
        private float m_maxValue = c_maximum;
        private bool m_rebuildOnNextUpdate = false;
        private string m_lastSelectedPageName = string.Empty;
        private bool m_loadLastSelectedPage = true;
        private uint m_currentPageID = 0;

        private Dictionary<uint, ProfileStatSamples> m_statSamples = new Dictionary<uint, ProfileStatSamples>();
        #endregion

        #region Properties
        public static string MyName
        {
            get
            {
                return c_myName;
            }
        }

        public static int UseViewNum = -1;

        public static bool IsConnected
        {
            get
            {
                return sm_isConnected;
            }
        }
        #endregion

        #region Overrides
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing )
            {
                if ( components != null )
                {
                    components.Dispose();
                }

                if ( sm_profilerComponent != null )
                {
                    sm_profilerComponent.PageCreated -= new ProfilePageEventHandler( profilerComponent_PageCreated );
                    sm_profilerComponent.PageDestroyed -= new ProfilePageEventHandler( profilerComponent_PageDestroyed );

                    sm_profilerComponent.GroupLinkedToPage -= new ProfileGroupEventHandler( profilerComponent_GroupEventHandler );
                    sm_profilerComponent.GroupDestroyed -= new ProfileGroupEventHandler( profilerComponent_GroupEventHandler );

                    sm_profilerComponent.ArrayStatsCreated -= new ProfileStatsEventHandler( profilerComponent_StatsEventHandler );
                    sm_profilerComponent.ArrayStatsDestroyed -= new ProfileStatsEventHandler( profilerComponent_StatsEventHandler );

                    sm_profilerComponent.TimeStatsCreated -= new ProfileStatsEventHandler( profilerComponent_StatsEventHandler );
                    sm_profilerComponent.TimeStatsDestroyed -= new ProfileStatsEventHandler( profilerComponent_StatsEventHandler );

                    sm_profilerComponent.ValueStatsCreated -= new ProfileStatsEventHandler( profilerComponent_StatsEventHandler );
                    sm_profilerComponent.ValueStatsDestroyed -= new ProfileStatsEventHandler( profilerComponent_StatsEventHandler );

                    sm_profilerComponent.ArrayValueStatsDestroyed -= new ProfileStatsEventHandler( profilerComponent_StatsEventHandler );
                }
            }
            base.Dispose( disposing );
        }

        public override string GetMyName()
        {
            return GetTabText();
        }

        public override string GetTabText()
        {
            return Profiler.MyName + " " + m_numView;
        }

        public override void SaveWidgetPageData( Dictionary<string, string> data )
        {
            base.SaveWidgetPageData( data );

            data[GetTabText() + ".SplitterDistance"] = this.splitContainer1.SplitterDistance.ToString();

            if ( !String.IsNullOrEmpty( m_lastSelectedPageName ) )
            {
                data[GetTabText() + ".SelectedItem"] = m_lastSelectedPageName;
            }
            else
            {
                data.Remove( GetTabText() + ".SelectedItem" );
            }

            data[GetTabText() + ".ProfileTimeStatsItem.StatType"] = this.ekgPanel.TimeStatType.ToString();
            data[GetTabText() + ".ProfileTimeStatsItem.TimeMode"] = this.ekgPanel.TimeDisplayMode.ToString();
            data[GetTabText() + ".ProfileValueStatsItem.StatType"] = this.ekgPanel.ValueStatType.ToString();
            data[GetTabText() + ".MinimumValue"] = this.ekgPanel.MinimumValue.ToString();
            data[GetTabText() + ".MaximumValue"] = this.ekgPanel.MaximumValue.ToString();
            data[GetTabText() + ".AutoScale"] = this.ekgPanel.AutoScale.ToString();

            if ( !this.ekgPanel.AutoScale )
            {
                data[GetTabText() + ".VerticalScrollOffset"] = this.ekgPanel.VerticalScrollOffset.ToString();
            }
            else

            {
                data.Remove( GetTabText() + ".VerticalScrollOffset" );
            }
        }

        public override void LoadWidgetPageData( Dictionary<string, string> data )
        {
            base.LoadWidgetPageData( data );

            string val;
            if ( data.TryGetValue( GetTabText() + ".SelectedItem", out val ) )
            {
                m_lastSelectedPageName = val;            
            }
            else
            {
                m_lastSelectedPageName = string.Empty;
            }

            if ( data.TryGetValue( GetTabText() + ".SplitterDistance", out val ) )
            {
                try
                {
                    this.splitContainer1.SplitterDistance = int.Parse( val );
                }
                catch
                {

                }
            }

            if ( data.TryGetValue( GetTabText() + ".ProfileTimeStatsItem.StatType", out val ) )
            {
                this.ekgPanel.TimeStatType = (ProfileTimeStatsItem.StatType)Enum.Parse( typeof( ProfileTimeStatsItem.StatType ), val );
            }

            if ( data.TryGetValue( GetTabText() + ".ProfileTimeStatsItem.TimeMode", out val ) )
            {
                this.ekgPanel.TimeDisplayMode = (ProfileTimeStatsItem.TimeMode)Enum.Parse( typeof( ProfileTimeStatsItem.TimeMode ), val );
            }

            if ( data.TryGetValue( GetTabText() + ".ProfileValueStatsItem.StatType", out val ) )
            {
                this.ekgPanel.ValueStatType = (ProfileValueStatsItem.StatType)Enum.Parse( typeof( ProfileValueStatsItem.StatType ), val );
            }

            if ( data.TryGetValue( GetTabText() + ".MinimumValue", out val ) )
            {
                this.ekgPanel.MinimumValue = float.Parse( val );
            }

            if ( data.TryGetValue( GetTabText() + ".MaximumValue", out val ) )
            {
                this.ekgPanel.MaximumValue = m_maxValue = float.Parse( val );
            }

            if ( data.TryGetValue( GetTabText() + ".AutoScale", out val ) )
            {
                this.ekgPanel.AutoScale = bool.Parse( val );
            }

            if ( data.TryGetValue( GetTabText() + ".VerticalScrollOffset", out val ) )
            {
                int offset = int.Parse( val );
                if ( !this.ekgPanel.AutoScale )
                {
                    this.ekgPanel.VerticalScrollOffset = offset;
                }
            }
        }
        #endregion

        #region Static Functions
        public static void Connect()
        {
#if TEST_MODE
            // init test data
            for ( int i = 0; i < 4; ++i )
            {
                ProfilePageItem page = new ProfilePageItem( "Page " + i, (uint)i );
                sm_pageItems.Add( page );

                for ( int j = 0; j < 2; ++j )
                {
                    ProfileGroupItem group = new ProfileGroupItem( "Page " + i + " Group " + j, (uint)(i * j) );
                    page.Groups.Add( group );

                    switch ( i )
                    {
                        case 0:
                            {
                                ProfileValueStatsItem intStats
                                    = new ProfileValueStatsItem( "value - Int", sm_tempKey++, sm_randomNumberGenerator.Next( 100 ) );
                                group.Stats.Add( intStats );

                                ProfileValueStatsItem floatStats
                                    = new ProfileValueStatsItem( "value - Float", sm_tempKey++, (float)sm_randomNumberGenerator.NextDouble() * 50.0f );
                                group.Stats.Add( floatStats );
                            }
                            break;
                        case 1:
                            {
                                ProfileTimeStatsItem timeStats = new ProfileTimeStatsItem( "timer - 0", sm_tempKey++ );
                                group.Stats.Add( timeStats );

                                timeStats = new ProfileTimeStatsItem( "timer - 1", sm_tempKey++ );
                                group.Stats.Add( timeStats );
                            }
                            break;
                        case 2:
                            {
                                ProfileValueStatsItem uintStats
                                    = new ProfileValueStatsItem( "value - Int", sm_tempKey++, (float)sm_randomNumberGenerator.NextDouble() * 100.0f );
                                group.Stats.Add( uintStats );

                                ProfileTimeStatsItem timeStats = new ProfileTimeStatsItem( "timer - 0", sm_tempKey++ );
                                group.Stats.Add( timeStats );
                            }
                            break;
                        case 3:
                            {
                                ProfileValueStatsItemArray statsArray
                                    = new ProfileValueStatsItemArray( "value - Float Array", sm_tempKey++ );

                                for ( int k = 0; k < 3; ++k )
                                {
                                    ProfileValueStatsItem floatStats = new ProfileValueStatsItem( "value - Float Array_" + k, sm_tempKey++ );
                                    statsArray.Values.Add( floatStats );
                                }
                                
                                group.Stats.Add( statsArray );
                            }
                            break;
                    }
                }
            }

            sm_isConnected = true;
#else
            sm_pipe = new ProfilePipe( new ProfilePipe.HandlePacketDelegate( HandlePacket ) );

            sm_connectThread = new Thread( new ThreadStart( ConnectProc ) );
            sm_connectThread.Name = "Profiler Connect Thread";
            sm_connectThread.Start();
#endif
        }

        public static void Disconnect()
        {
            if ( sm_connectThread != null )
            {
                sm_connectThread.Abort();
                sm_connectThread = null;
            }

            if ( sm_readThread != null )
            {
                sm_readThread.Abort();
                sm_readThread = null;
            }

            if ( sm_pipe != null )
            {
                sm_pipe.Close();
            }

            sm_isConnected = false;
        }

        public static ragWidgetPage.WidgetPage CreatePage(string appName, IBankManager bankMgr, IKeyUpProcessor keyProcessor, DockControl dockControl, ILog log)
        {
            int viewNum;
            if ( Profiler.UseViewNum > -1 )
            {
                viewNum = Profiler.UseViewNum;
                Profiler.UseViewNum = -1;

                if ( viewNum >= sm_numViews )
                {
                    sm_numViews = viewNum + 1;
                }
            }
            else
            {
                viewNum = sm_numViews;
                ++sm_numViews;
            }

            return new Profiler( appName, bankMgr, dockControl, log, viewNum );
        }
        
        private static void ConnectProc()
        {
            try
            {
                PipeID pipeID = PlugIn.Instance.HostData.MainRageApplication.GetRagPipeSocket( c_ProfilerPortOffset );
                if ( sm_pipe.Create( pipeID, true ) )
                {
                    sm_isConnected = true;

                    sm_readThread = new Thread( new ThreadStart( ReadProc ) );
                    sm_readThread.Name = "Profiler read thread";
                    sm_readThread.Start();
                }
            }
            catch ( ThreadAbortException )
            {
                return;
            }
        }

        private static void ReadProc()
        {
            Dictionary<uint, CompressionData> dataDictionary = new Dictionary<uint, CompressionData>();

            while ( true )
            {
                try
                {
                    
#if TEST_MODE
                    sm_valuesMutex.WaitOne();

                    // modify test data
                    List<ProfileStatsItem> stats = new List<ProfileStatsItem>();
                    foreach ( ProfilePageItem page in sm_pageItems )
                    {
                        foreach ( ProfileGroupItem group in page.Groups )
                        {
                            stats.AddRange( group.Stats );
                        }
                    }

                    for ( int i = 0; i < 500; ++i )
                    {
                        int index = sm_randomNumberGenerator.Next( stats.Count * stats.Count );
                        index = (int)Math.Sqrt( (float)index );

                        float incr = (float)sm_randomNumberGenerator.NextDouble() - 0.5f;

                        if ( stats[index] is ProfileTimeStatsItem )
                        {
                            ProfileTimeStatsItem timeStats = stats[index] as ProfileTimeStatsItem;

                            timeStats.FrameTime += incr;

                            if ( timeStats.FrameTime > timeStats.PeakFrameTime )
                            {
                                timeStats.PeakFrameTime = timeStats.FrameTime;
                            }
                        }
                        else if ( stats[index] is ProfileValueStatsItem )
                        {
                            ProfileValueStatsItem valueStats = stats[index] as ProfileValueStatsItem;
                            valueStats.SetStat( (int)ProfileValueStatsItem.StatType.Current,
                                valueStats.GetStat( (int)ProfileValueStatsItem.StatType.Current ) + (incr * 5.0f) );

                            if ( valueStats.GetStat( (int)ProfileValueStatsItem.StatType.Current ) 
                                > valueStats.GetStat( (int)ProfileValueStatsItem.StatType.Peak ) )
                            {
                                valueStats.SetStat( (int)ProfileValueStatsItem.StatType.Peak, 
                                     valueStats.GetStat( (int)ProfileValueStatsItem.StatType.Current ) );
                            }
                        }
                        else if ( stats[index] is ProfileValueStatsItemArray )
                        {
                            ProfileValueStatsItemArray arrayStats = stats[index] as ProfileValueStatsItemArray;
                            int valueIndex = sm_randomNumberGenerator.Next( arrayStats.Values.Count );

                            arrayStats.Values[valueIndex].SetStat( (int)ProfileValueStatsItem.StatType.Current,
                                arrayStats.Values[valueIndex].GetStat( (int)ProfileValueStatsItem.StatType.Current ) + (incr * 5.0f) );

                            if ( arrayStats.Values[valueIndex].GetStat( (int)ProfileValueStatsItem.StatType.Current ) 
                                > arrayStats.Values[valueIndex].GetStat( (int)ProfileValueStatsItem.StatType.Peak ) )
                            {
                                arrayStats.Values[valueIndex].SetStat( (int)ProfileValueStatsItem.StatType.Peak,
                                     arrayStats.Values[valueIndex].GetStat( (int)ProfileValueStatsItem.StatType.Current ) );
                            }
                        }
                    }

                    ++s_tempCounter;

                    sm_valuesMutex.ReleaseMutex();
#else
                    bool error = false;

                    // put each CompressionData in a dictionary according to the Count packet that should be at the front of each data buffer
                    if ( sm_pipe.ReadPipe( dataDictionary ) == -1 )
                    {
                        error = true;
                    }

                    if ( !error && (dataDictionary.Count > 0) )
                    {
                        // find the CompressionData corresponding to the next message count we're looking for
                        CompressionData cData;
                        while ( dataDictionary.TryGetValue( sm_messageCount, out cData ) )
                        {
                            if ( sm_pipe.HandlePackets( cData ) == -1 )
                            {
                                error = true;
                                break;
                            }

                            dataDictionary.Remove( sm_messageCount );

                            ++sm_messageCount;
                        }
                    }
#endif
                    Thread.Sleep( 0 );
                }
                catch ( ThreadAbortException )
                {
                    break;
                }
            }
        }

        private static void HandlePacket( RemotePacket p )
        {
            try
            {
                switch ( (ProfileRemotePacket.Commands)p.Command )
                {
                    case ProfileRemotePacket.Commands.CreateArray:
                        {
                            p.Begin();
                            uint id = p.Read_u32();
                            uint groupId = p.Read_u32();
                            string name = p.Read_const_char();
                            p.End();

                            sm_profilerComponent.CreateArray( name, id, groupId );
                        }
                        break;
                    case ProfileRemotePacket.Commands.CreateArrayValue:
                        {
                            p.Begin();
                            uint id = p.Read_u32();
                            uint arrayId = p.Read_u32();
                            string name = p.Read_const_char();
                            p.End();

                            sm_profilerComponent.CreateArrayValue( name, id, arrayId );
                        }
                        break;
                    case ProfileRemotePacket.Commands.CreateGroup:
                        {
                            p.Begin();
                            uint id = p.Read_u32();
                            string name = p.Read_const_char();
                            p.End();

                            sm_profilerComponent.CreateGroup( name, id );
                        }
                        break;
                    case ProfileRemotePacket.Commands.CreatePage:
                        {
                            p.Begin();
                            uint id = p.Read_u32();
                            string name = p.Read_const_char();
                            p.End();

                            sm_profilerComponent.CreatePage( name, id );
                        }
                        break;
                    case ProfileRemotePacket.Commands.CreateTimer:
                        {
                            p.Begin();
                            uint id = p.Read_u32();
                            uint groupId = p.Read_u32();
                            string name = p.Read_const_char();
                            p.End();

                            sm_profilerComponent.CreateTimer( name, id, groupId );
                        }
                        break;
                    case ProfileRemotePacket.Commands.CreateValue:
                        {
                            p.Begin();
                            uint id = p.Read_u32();
                            uint groupId = p.Read_u32();
                            string name = p.Read_const_char();
                            p.End();

                            sm_profilerComponent.CreateValue( name, id, groupId );
                        }
                        break;
                    case ProfileRemotePacket.Commands.DestroyArray:
                        {
                            p.Begin();
                            uint id = p.Read_u32();
                            p.End();

                            sm_profilerComponent.DestroyArray( id );
                        }
                        break;
                    case ProfileRemotePacket.Commands.DestroyArrayValue:
                        {
                            p.Begin();
                            uint id = p.Read_u32();
                            p.End();

                            sm_profilerComponent.DestroyArrayValue( id );
                        }
                        break;
                    case ProfileRemotePacket.Commands.DestroyGroup:
                        {
                            p.Begin();
                            uint id = p.Read_u32();
                            p.End();

                            sm_profilerComponent.DestroyGroup( id );
                        }
                        break;
                    case ProfileRemotePacket.Commands.DestroyPage:
                        {
                            p.Begin();
                            uint id = p.Read_u32();
                            p.End();

                            sm_profilerComponent.DestroyPage( id );
                        }
                        break;
                    case ProfileRemotePacket.Commands.DestroyTimer:
                        {
                            p.Begin();
                            uint id = p.Read_u32();
                            p.End();

                            sm_profilerComponent.DestroyTimer( id );
                        }
                        break;
                    case ProfileRemotePacket.Commands.DestroyValue:
                        {
                            p.Begin();
                            uint id = p.Read_u32();
                            p.End();

                            sm_profilerComponent.DestroyValue( id );
                        }
                        break;
                    case ProfileRemotePacket.Commands.LinkGroupToPage:
                        {
                            p.Begin();
                            uint groupId = p.Read_u32();
                            uint pageId = p.Read_u32();
                            p.End();

                            sm_profilerComponent.LinkGroupToPage( groupId, pageId );
                        }
                        break;
                    case ProfileRemotePacket.Commands.UpdateTimer:
                        {
                            p.Begin();
                            uint id = p.Read_u32();
                            
                            float elapsedFrameTime = p.Read_float();
                            float counter0Value = p.Read_float();
                            float counter1Value = p.Read_float();

                            float averageElapsedFrameTime = p.Read_float();
                            float averageCounter0Value = p.Read_float();
                            float averageCounter1Value = p.Read_float();

                            float peakElapsedFrameTime = p.Read_float();
                            float peakCounter0Value = p.Read_float();
                            float peakCounter1Value = p.Read_float();

                            int calls = p.Read_s32();
                            float averageCalls = p.Read_float();
                            int peakCalls = p.Read_s32();
                            p.End();

                            sm_profilerComponent.UpdateTimer( id, 
                                elapsedFrameTime, counter0Value, counter1Value,
                                averageElapsedFrameTime, averageCounter0Value, averageCounter1Value,
                                peakElapsedFrameTime, peakCounter0Value, peakCounter1Value,
                                calls, averageCalls, peakCalls );
                        }
                        break;
                    case ProfileRemotePacket.Commands.UpdateValue:
                        {
                            p.Begin();
                            uint id = p.Read_u32();
                            float value = p.Read_float();
                            float averageValue = p.Read_float();
                            float peakValue = p.Read_float();
                            p.End();

                            sm_profilerComponent.UpdateValue( id, value, averageValue, peakValue );
                        }
                        break;
                }                  
            }
            catch
            {
            }
        }
        #endregion

        #region Event Handlers
        private void Profiler_Load( object sender, EventArgs e )
        {
            this.ekgPanel.SamplesPerSecond = 1000 / this.timer.Interval;

            this.ekgPanel.MinimumValue = 0.0f;
            this.ekgPanel.MaximumValue = m_maxValue;

            this.colorKeyGlacialTreeList.Columns[1].Text
                = this.ekgPanel.TimeStatType.ToString() + " / " + this.ekgPanel.ValueStatType.ToString();            

            this.timer.Start();
        }

        private void DockControl_Closed( object sender, EventArgs e )
        {
            TD.SandDock.DockControl dockControl = sender as TD.SandDock.DockControl;
            if (dockControl != null && dockControl.CloseAction == DockControlCloseAction.Dispose)
            {
                if ( m_currentPageID != 0 )
                {
                    PageHidden( m_currentPageID );
                }
            }
        }

        private void timer_Tick( object sender, EventArgs e )
        {
            sm_profilerComponent.LockForUpdate = true;

            if ( m_rebuildOnNextUpdate )
            {
                // toggle so the lists and such get rebuilt.
                object selectedItem = this.pageToolStripComboBox.SelectedItem;                
                this.pageToolStripComboBox.SelectedItem = null;
                this.pageToolStripComboBox.SelectedItem = selectedItem;

                m_rebuildOnNextUpdate = false;
            }

            this.colorKeyGlacialTreeList.BeginUpdate();

            // take a sample
            ProfilePageItem page = this.pageToolStripComboBox.SelectedItem as ProfilePageItem;
            if ( page != null )
            {
                foreach ( ProfileGroupItem group in page.Groups )
                {
                    foreach ( ProfileStatsItem stats in group.Stats )
                    {
                        TakeStatsSample( stats );
                    }
                }
            }
           
            this.ekgPanel.MinimumValue = 0;

            if ( this.ekgPanel.AutoScale )
            {
                this.ekgPanel.MaximumValue = m_maxValue;
            }

            this.colorKeyGlacialTreeList.EndUpdate();

            sm_profilerComponent.LockForUpdate = false;

            this.ekgPanel.Invalidate();
        }

        private void pageToolStripComboBox_SelectedIndexChanged( object sender, EventArgs e )
        {
            ProfilePageItem page = this.pageToolStripComboBox.SelectedItem as ProfilePageItem;
            if ( page == null )
            {
                return;
            }

            sm_profilerComponent.LockForUpdate = true;

            if ( m_currentPageID != 0 )
            {
                PageHidden( m_currentPageID );
            }

            m_statSamples.Clear();

            this.ekgPanel.LockForUpdate = true;

            this.ekgPanel.Samples.Clear();

            this.colorKeyGlacialTreeList.BeginUpdate();

            this.colorKeyGlacialTreeList.Nodes.Clear();

            int imageIndex = 0;
            foreach ( ProfileGroupItem group in page.Groups )
            {
                GTLTreeNode groupNode = new GTLTreeNode( group.Name );
                groupNode.Tag = group;
                groupNode.Checked = true;   // FIXME: remember values

                this.colorKeyGlacialTreeList.Nodes.Add( groupNode );

                foreach ( ProfileStatsItem stats in group.Stats )
                {
                    AddStatsTreeNode( stats, groupNode, ref imageIndex );
                }
            }

            m_lastSelectedPageName = page.ToString();

            this.colorKeyGlacialTreeList.EndUpdate();

            this.ekgPanel.LockForUpdate = false;

            m_currentPageID = page.ID;

            PageShown( m_currentPageID );

            sm_profilerComponent.LockForUpdate = false;

            m_maxValue = c_maximum;
        }

        private void optionsToolStripDropDownButton_DropDownOpening( object sender, EventArgs e )
        {
            this.autoScaleToolStripMenuItem.Checked = this.ekgPanel.AutoScale;
        }

        private void nextPageToolStripMenuItem_Click( object sender, EventArgs e )
        {
            sm_profilerComponent.LockForUpdate = true;
            
            this.pageToolStripComboBox.SelectedIndex = (this.pageToolStripComboBox.SelectedIndex < this.pageToolStripComboBox.Items.Count - 1) ? this.pageToolStripComboBox.SelectedIndex + 1 : 0;
            
            sm_profilerComponent.LockForUpdate = false;
        }

        private void previousPageToolStripMenuItem_Click( object sender, EventArgs e )
        {
            sm_profilerComponent.LockForUpdate = true;

            this.pageToolStripComboBox.SelectedIndex = (this.pageToolStripComboBox.SelectedIndex > 0) ? this.pageToolStripComboBox.SelectedIndex - 1 : this.pageToolStripComboBox.Items.Count - 1;

            sm_profilerComponent.LockForUpdate = false;
        }

        private void showTimeTypeToolStripMenuItem_DropDownOpening( object sender, EventArgs e )
        {
            this.frameTimeToolStripMenuItem.Checked = this.ekgPanel.TimeStatType == ProfileTimeStatsItem.StatType.FrameTime;
            this.averageTimeToolStripMenuItem.Checked = this.ekgPanel.TimeStatType == ProfileTimeStatsItem.StatType.AverageFrameTime;
            this.peakTimeToolStripMenuItem.Checked = this.ekgPanel.TimeStatType == ProfileTimeStatsItem.StatType.PeakFrameTime;
            this.frameCallsToolStripMenuItem.Checked = this.ekgPanel.TimeStatType == ProfileTimeStatsItem.StatType.FrameCalls;
            this.averageCallsToolStripMenuItem.Checked = this.ekgPanel.TimeStatType == ProfileTimeStatsItem.StatType.AverageCalls;
            this.peakCallsToolStripMenuItem.Checked = this.ekgPanel.TimeStatType == ProfileTimeStatsItem.StatType.PeakCalls;
        }

        private void showTimeTypeToolStripSubMenuItem_Click( object sender, EventArgs e )
        {
            ToolStripMenuItem item = sender as ToolStripMenuItem;
            this.ekgPanel.TimeStatType = (ProfileTimeStatsItem.StatType)Enum.Parse( typeof( ProfileTimeStatsItem.StatType ), item.Tag as string );

            this.colorKeyGlacialTreeList.Columns[1].Text = this.ekgPanel.TimeStatType.ToString() + " / " + this.ekgPanel.ValueStatType.ToString();
        }

        private void timeDisplayModeToolStripMenuItem_DropDownOpening( object sender, EventArgs e )
        {
            this.elapsedTimeToolStripMenuItem.Checked = this.ekgPanel.TimeDisplayMode == ProfileTimeStatsItem.TimeMode.ElapsedTime;
            this.counter0ToolStripMenuItem.Checked = this.ekgPanel.TimeDisplayMode == ProfileTimeStatsItem.TimeMode.Counter0;
            this.counter1ToolStripMenuItem.Checked = this.ekgPanel.TimeDisplayMode == ProfileTimeStatsItem.TimeMode.Counter1;
        }

        private void timeDisplayModeToolStripSubMenuItem_Click( object sender, EventArgs e )
        {
            ToolStripMenuItem item = sender as ToolStripMenuItem;
            this.ekgPanel.TimeDisplayMode = (ProfileTimeStatsItem.TimeMode)Enum.Parse( typeof( ProfileTimeStatsItem.TimeMode ), item.Tag as string );
        }

        private void showValueTypeToolStripMenuItem_DropDownOpening( object sender, EventArgs e )
        {
            this.currentToolStripMenuItem.Checked = this.ekgPanel.ValueStatType == ProfileValueStatsItem.StatType.Current;
            this.averageToolStripMenuItem.Checked = this.ekgPanel.ValueStatType == ProfileValueStatsItem.StatType.Average;
            this.peakToolStripMenuItem.Checked = this.ekgPanel.ValueStatType == ProfileValueStatsItem.StatType.Peak;
            this.valueDefaultToolStripMenuItem.Checked = this.ekgPanel.ValueStatType == ProfileValueStatsItem.StatType.ValueDefault;
        }

        private void showValueTypeToolStripSubMenuItem_Click( object sender, EventArgs e )
        {
            ToolStripMenuItem item = sender as ToolStripMenuItem;
            this.ekgPanel.ValueStatType = (ProfileValueStatsItem.StatType)Enum.Parse( typeof( ProfileValueStatsItem.StatType ), item.Tag as string );

            this.colorKeyGlacialTreeList.Columns[1].Text = this.ekgPanel.TimeStatType.ToString() + " / " + this.ekgPanel.ValueStatType.ToString();
        }

        private void autoScaleToolStripMenuItem_Click( object sender, EventArgs e )
        {
            ToolStripMenuItem item = sender as ToolStripMenuItem;
            this.ekgPanel.AutoScale = item.Checked;
        }

        private void autoScaleCheckBox_CheckedChanged( object sender, EventArgs e )
        {
            CheckBox box = sender as CheckBox;
            this.ekgPanel.AutoScale = box.Checked;
        }

        private void scaleUpToolStripMenuItem_Click( object sender, EventArgs e )
        {
            this.ekgPanel.ScaleUp( 1.2f );
        }

        private void scaleDownToolStripMenuItem_Click( object sender, EventArgs e )
        {
            this.ekgPanel.ScaleDown( 1.2f );
        }

        private void scrollUpToolStripMenuItem_Click( object sender, EventArgs e )
        {
            ++this.ekgPanel.VerticalScrollOffset;
        }

        private void scrollDownToolStripMenuItem_Click( object sender, EventArgs e )
        {
            --this.ekgPanel.VerticalScrollOffset;
        }

        private void profilerComponent_PageCreated( object sender, ProfilePageEventArgs e )
        {
            PageDelegate del = new PageDelegate( AddPage );
            this.Invoke( del, new object[] { e.Page } );
        }

        private void profilerComponent_PageDestroyed( object sender, ProfilePageEventArgs e )
        {
            PageDelegate del = new PageDelegate( RemovePage );
            this.Invoke( del, new object[] { e.Page } );
        }

        private void profilerComponent_GroupEventHandler( object sender, ProfileGroupEventArgs e )
        {
            UpdateGroupsAndValuesDelegate del = new UpdateGroupsAndValuesDelegate( UpdateGroupsAndValues );
            this.Invoke( del, new object[] { e.Group.Pages } );
        }

        private void profilerComponent_StatsEventHandler( object sender, ProfileStatsEventArgs e )
        {
            UpdateGroupsAndValuesDelegate del = new UpdateGroupsAndValuesDelegate( UpdateGroupsAndValues );
            this.Invoke( del, new object[] { e.Stats.Group.Pages } );
        }

        private void ekgPanel_AutoScaleChanged( object sender, EventArgs e )
        {
            this.autoScaleCheckBox.Checked = this.ekgPanel.AutoScale;
        }
        #endregion

        #region Private Functions
        private delegate void PageDelegate( ProfilePageItem page );
        private void AddPage( ProfilePageItem page )
        {
            this.pageToolStripComboBox.Items.Add( page );

            if ( m_loadLastSelectedPage && (page.ToString() == m_lastSelectedPageName) )
            {
                m_loadLastSelectedPage = false;

                this.pageToolStripComboBox.SelectedItem = page;
            }
        }

        private void RemovePage( ProfilePageItem page )
        {
            this.pageToolStripComboBox.Items.Remove( page );

            if ( page.ID == m_currentPageID )
            {
                m_currentPageID = 0;
            }
        }

        private delegate void UpdateGroupsAndValuesDelegate( List<ProfilePageItem> pages );
        private void UpdateGroupsAndValues( List<ProfilePageItem> pages )
        {
            if ( this.pageToolStripComboBox.SelectedItem != null )
            {
                // check if we need to update what is currently being displayed
                ProfilePageItem page = this.pageToolStripComboBox.SelectedItem as ProfilePageItem;
                if ( pages.Contains( page ) )
                {
                    m_rebuildOnNextUpdate = true;                    
                }
            }
        }

        /// <summary>
        /// Creates a GTLTreeNode for the ProfileStatsItem as a child of parentNode, and with the given imageIndex.
        /// </summary>
        /// <param name="stats"></param>
        /// <param name="parentNode"></param>
        /// <param name="imageIndex"></param>
        private void AddStatsTreeNode( ProfileStatsItem stats, GTLTreeNode parentNode, ref int imageIndex )
        {
            if ( stats is ProfileValueStatsItemArray )
            {
                GTLTreeNode arrayNode = new GTLTreeNode( stats.Name );
                arrayNode.Tag = stats;
                arrayNode.Checked = true;   // FIXME: remember values

                parentNode.Nodes.Add( arrayNode );

                ProfileValueStatsItemArray statsArray = stats as ProfileValueStatsItemArray;
                foreach ( ProfileValueStatsItem statsItem in statsArray.Values )
                {
                    AddStatsTreeNode( statsItem, arrayNode, ref imageIndex );
                }
            }
            else
            {
                GTLTreeNode statsNode = new GTLTreeNode( stats.Name );

                statsNode.Checked = true;   // FIXME: remember values
                statsNode.ImageIndex = imageIndex++;
                statsNode.Tag = stats;

                // the timer tick will fill in the value of this subItem
                statsNode.SubItems.Add( string.Empty );

                parentNode.Nodes.Add( statsNode );

                ProfileStatSamples samples = new ProfileStatSamples( statsNode );
                this.ekgPanel.Samples.Add( samples );

                m_statSamples.Add( stats.ID, samples );
            }
        }

        /// <summary>
        /// Takes a sample of the ProfileStatsItem's current value for the selected Show types.
        /// </summary>
        /// <param name="stats"></param>
        private void TakeStatsSample( ProfileStatsItem stats )
        {
            if ( stats is ProfileValueStatsItemArray )
            {
                ProfileValueStatsItemArray statsArray = stats as ProfileValueStatsItemArray;
                foreach ( ProfileValueStatsItem statsItem in statsArray.Values )
                {
                    TakeStatsSample( statsItem );
                }
            }
            else
            {
                bool isTimeStat = stats is ProfileTimeStatsItem;
                int statType = isTimeStat ? (int)this.ekgPanel.TimeStatType : (int)this.ekgPanel.ValueStatType;

                float value = stats.GetStat( statType );

                ProfileStatSamples samples;
                if ( m_statSamples.TryGetValue( stats.ID, out samples ) )
                {
                    samples.AddSample( value );

                    if ( isTimeStat )
                    {
                        switch ( this.ekgPanel.TimeStatType )
                        {
                            case ProfileTimeStatsItem.StatType.AverageFrameTime:
                            case ProfileTimeStatsItem.StatType.FrameTime:
                            case ProfileTimeStatsItem.StatType.PeakFrameTime:
                                {
                                    switch ( this.ekgPanel.TimeDisplayMode )
                                    {
                                        case ProfileTimeStatsItem.TimeMode.ElapsedTime:
                                            samples.TreeNode.SubItems[1].Text = (value * this.ekgPanel.GraphScaleMillisecondsAdjust).ToString();
                                            break;
                                        default:
                                            samples.TreeNode.SubItems[1].Text = value.ToString();
                                            break;
                                    }
                                }
                                break;
                            default:
                                samples.TreeNode.SubItems[1].Text = value.ToString();
                                break;
                        }
                    }
                    else
                    {
                        samples.TreeNode.SubItems[1].Text = value.ToString();
                    }

                    if ( this.ekgPanel.AutoScale )
                    {
                        if ( value > m_maxValue )
                        {
                            m_maxValue = value;
                        }
                    }
                }
            }
        }

        private void PageShown( uint id )
        {
            sm_mutex.WaitOne();

            int count;
            if ( sm_displayedPageRefCountDictionary.TryGetValue( id, out count ) )
            {
                ++count;
            }
            else
            {
                count = 1;
            }

            sm_displayedPageRefCountDictionary[id] = count;

            ProfileRemotePacket packet = new ProfileRemotePacket( sm_pipe );
            packet.Begin( ProfileRemotePacket.Commands.PageRefCount, id );
            packet.Write_s32( count );
            packet.Send();

            sm_mutex.ReleaseMutex();
        }

        private void PageHidden( uint id )
        {
            sm_mutex.WaitOne();

            int count;
            if ( sm_displayedPageRefCountDictionary.TryGetValue( id, out count ) )
            {
                --count;
            }

            if ( count == 0 )
            {
                sm_displayedPageRefCountDictionary.Remove( id );
            }

            ProfileRemotePacket packet = new ProfileRemotePacket( sm_pipe );
            packet.Begin( ProfileRemotePacket.Commands.PageRefCount, id );
            packet.Write_s32( count );
            packet.Send();

            sm_mutex.ReleaseMutex();
        }
        #endregion
    }
}