using System;
using System.Collections.Generic;
using System.Text;

namespace ragProfiler
{
    public class ProfilePageItem : ProfileItem
    {
        public ProfilePageItem( string name, uint guid )
            : base( name, guid )
        {

        }

        #region Constants
        public const int MaxGroups = 16;
        #endregion

        #region Variables
        private List<ProfileGroupItem> m_groups = new List<ProfileGroupItem>();
        #endregion

        #region Properties
        public List<ProfileGroupItem> Groups
        {
            get
            {
                return m_groups;
            }
        }
        #endregion
    }
}
