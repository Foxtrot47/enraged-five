using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using GlacialComponents.Controls.GlacialTreeList;

namespace ragProfiler
{
    public class ProfileStatSamples
    {
        public ProfileStatSamples( GTLTreeNode treeNode )
        {
            m_treeNode = treeNode;
        }

        #region Enums
        public enum SampleType
        {
            Timer,
            Value
        }
        #endregion

        #region Constants
        public const int MaxSamples = 300;

        public static List<Color> Colors = GenerateColors();

        private static List<Color> GenerateColors()
        {
            List<List<Color>> lists = new List<List<Color>>();

            lists.Add( GenerateColors( 255, 0, 0 ) );
            lists.Add( GenerateColors( 255, 255, 0 ) );
            lists.Add( GenerateColors( 0, 255, 0 ) );
            lists.Add( GenerateColors( 0, 255, 255 ) );
            lists.Add( GenerateColors( 0, 0, 255 ) );
            lists.Add( GenerateColors( 255, 0, 255 ) );

            List<Color> finalList = new List<Color>();

            // put them in "rainbow" order
            int count = lists[0].Count;
            for ( int i = 0; i < count; ++i )
            {
                for ( int j = 0; j < lists.Count; ++j )
                {
                    finalList.Add( lists[j][i] );
                }
            }

            finalList.Add( Color.DarkGray );

            return finalList;
        }

        private static List<Color> GenerateColors( int origRed, int origGreen, int origBlue )
        {
            List<Color> colors = new List<Color>();

            int addRed = origRed == 0 ? 1 : 0;
            int addGreen = origGreen == 0 ? 1 : 0;
            int addBlue = origBlue == 0 ? 1 : 0;

            int subtractRed = origRed == 0 ? 0 : 1;
            int subtractGreen = origGreen == 0 ? 0 : 1;
            int subtractBlue = origBlue == 0 ? 0 : 1;

            int[] subtractValues = new int[] { 0, 100, 50, 150, 25, 75, 125, 175 };
            int[] addValues = new int[] { 0, 100, 50, 125, 25, 75 };

            for ( int i = 0; i < 6; ++i )
            {
                int red, green, blue;
                red = origRed + (addRed * addValues[i]);
                green = origGreen + (addGreen * addValues[i]);
                blue = origBlue + (addBlue * addValues[i]);

                for ( int j = 0; j < 8; ++j )
                {
                    int r, g, b;
                    r = red - (subtractRed * subtractValues[j]);
                    g = green - (subtractGreen * subtractValues[j]);
                    b = blue - (subtractBlue * subtractValues[j]);

                    if ( subtractRed == 1 )
                    {
                        if ( addGreen == 1 )
                        {
                            if ( g > r - 30 )
                            {
                                break;
                            }
                        }
                        else if ( addBlue == 1 )
                        {
                            if ( b > r - 30 )
                            {
                                break;
                            }
                        }
                    }
                    else if ( subtractGreen == 1 )
                    {
                        if ( r > g - 30 )
                        {
                            break;
                        }
                    }
                    else if ( subtractBlue == 1 )
                    {
                        if ( r > b - 30 )
                        {
                            break;
                        }
                    }

                    colors.Add( Color.FromArgb( r, g, b ) );
                }
            }

            return colors;
        }
        #endregion

        #region Variables
        private GTLTreeNode m_treeNode;
        private float[] m_samples = new float[ProfileStatSamples.MaxSamples];
        private int m_sampleStart = 0;
        private int m_numSamples = 0;
        #endregion

        #region Properties
        public GTLTreeNode TreeNode
        {
            get
            {
                return m_treeNode;
            }
        }

        public bool Enabled
        {
            get
            {
                if ( m_treeNode == null )
                {
                    return false;
                }

                GTLTreeNode parentNode = m_treeNode.ParentNode;
                while ( parentNode != null )
                {
                    if ( !parentNode.Checked )
                    {
                        return false;
                    }

                    parentNode = parentNode.ParentNode;
                }

                return m_treeNode.Checked;
            }
            set
            {
                if ( m_treeNode != null )
                {
                    m_treeNode.Checked = value;
                }
            }
        }

        public SampleType Type
        {
            get
            {
                if ( m_treeNode == null )
                {
                    return SampleType.Value;
                }

                return (m_treeNode.Tag is ProfileTimeStatsItem) ? SampleType.Timer : SampleType.Value;
            }
        }

        public float[] Samples
        {
            get
            {
                return m_samples;
            }
        }

        public int SampleStart
        {
            get
            {
                return m_sampleStart;
            }
        }

        public int NumSamples
        {
            get
            {
                return m_numSamples;
            }
        }

        public float CurrentValue
        {
            get
            {
                return m_samples[m_sampleStart];
            }
        }
        #endregion

        #region Public Functions
        public void AddSample( float f )
        {
            m_sampleStart = (m_sampleStart + ProfileStatSamples.MaxSamples - 1) % ProfileStatSamples.MaxSamples;

            m_samples[m_sampleStart] = f;

            m_numSamples = Math.Min( m_numSamples + 1, ProfileStatSamples.MaxSamples );
        }
        #endregion
    }
}
