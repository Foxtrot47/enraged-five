using System;
using System.Collections.Generic;
using System.Text;

using ragCore;

namespace ragProfiler
{
    public class ProfileRemotePacket : RemotePacket
    {
        public ProfileRemotePacket( ProfilePipe pipe )
            : base( pipe )
        {

        }

        #region Enums
        public enum Commands
        {
            Count,
            CreatePage,
            DestroyPage,
            CreateGroup,
            DestroyGroup,
            CreateTimer,
            DestroyTimer,
            CreateValue,
            DestroyValue,
            CreateArray,
            DestroyArray,
            CreateArrayValue,
            DestroyArrayValue,
            LinkGroupToPage,
            UpdateTimer,
            UpdateValue,
            PageRefCount
        }
        #endregion

        #region Public Functions
        public void Begin( Commands command, uint guid )
        {
            m_Header.m_Length = 0;
            m_Header.m_Command = (ushort)command;
            m_Header.m_Guid = (int)guid;

            m_Offset = -1;
        }
        #endregion
    }
}
