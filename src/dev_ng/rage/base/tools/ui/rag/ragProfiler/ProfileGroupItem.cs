using System;
using System.Collections.Generic;
using System.Text;

namespace ragProfiler
{
    public class ProfileGroupItem : ProfileItem
    {
        public ProfileGroupItem( string name, uint guid )
            : base( name, guid )
        {

        }

        #region Constants
        public const int MaxStats = 24;
        #endregion

        #region Variables
        private List<ProfileStatsItem> m_statsItems = new List<ProfileStatsItem>();
        private List<ProfilePageItem> m_pages = new List<ProfilePageItem>();
        #endregion

        #region Properties
        public List<ProfileStatsItem> Stats
        {
            get
            {
                return m_statsItems;
            }
        }

        public List<ProfilePageItem> Pages
        {
            get
            {
                return m_pages;
            }
        }
        #endregion
    }
}
