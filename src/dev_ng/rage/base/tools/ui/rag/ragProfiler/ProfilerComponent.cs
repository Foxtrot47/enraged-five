using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading;

namespace ragProfiler
{
    public partial class ProfilerComponent : Component
    {
        public ProfilerComponent()
        {
            InitializeComponent();
        }

        public ProfilerComponent( IContainer container )
        {
            container.Add( this );

            InitializeComponent();
        }

        #region Variables
        private Mutex m_mutex = new Mutex();

        private List<ProfilePageItem> m_pageItems = new List<ProfilePageItem>();
        private Dictionary<uint, ProfilePageItem> m_pageDictionary = new Dictionary<uint, ProfilePageItem>();
        private Dictionary<uint, ProfileGroupItem> m_groupDictionary = new Dictionary<uint, ProfileGroupItem>();
        private Dictionary<uint, ProfileTimeStatsItem> m_timerDictionary = new Dictionary<uint, ProfileTimeStatsItem>();
        private Dictionary<uint, ProfileValueStatsItem> m_valueDictionary = new Dictionary<uint, ProfileValueStatsItem>();
        private Dictionary<uint, ProfileValueStatsItemArray> m_arrayDictionary = new Dictionary<uint, ProfileValueStatsItemArray>();        
        #endregion

        #region Properties
        public bool LockForUpdate
        {
            set
            {
                if ( value )
                {
                    m_mutex.WaitOne();
                }
                else
                {
                    m_mutex.ReleaseMutex();
                }
            }
        }

        public List<ProfilePageItem> PageItems
        {
            get
            {
                return m_pageItems;
            }
        }
        #endregion

        #region Events
        public event ProfilePageEventHandler PageCreated;
        public event ProfilePageEventHandler PageDestroyed;

        public event ProfileGroupEventHandler GroupCreated;
        public event ProfileGroupEventHandler GroupLinkedToPage;
        public event ProfileGroupEventHandler GroupDestroyed;

        public event ProfileStatsEventHandler TimeStatsCreated;
        public event ProfileStatsEventHandler TimeStatsDestroyed;
        public event ProfileStatsEventHandler ValueStatsCreated;
        public event ProfileStatsEventHandler ValueStatsDestroyed;
        public event ProfileStatsEventHandler ArrayStatsCreated;
        public event ProfileStatsEventHandler ArrayStatsDestroyed;
        public event ProfileStatsEventHandler ArrayValueStatsCreated;
        public event ProfileStatsEventHandler ArrayValueStatsDestroyed;

        public event ProfileStatsEventHandler TimeStatsUpdated;
        public event ProfileStatsEventHandler ValueStatsUpdated;
        #endregion

        #region Event Dispatchers
        private void OnPageCreated( ProfilePageItem page )
        {
            if ( this.PageCreated != null )
            {
                this.PageCreated( this, new ProfilePageEventArgs( page ) );
            }
        }

        private void OnPageDestroyed( ProfilePageItem page )
        {
            if ( this.PageDestroyed != null )
            {
                this.PageDestroyed( this, new ProfilePageEventArgs( page ) );
            }
        }

        private void OnGroupCreated( ProfileGroupItem group )
        {
            if ( this.GroupCreated != null )
            {
                this.GroupCreated( this, new ProfileGroupEventArgs( group ) );
            }
        }

        private void OnGroupLinkedToPage( ProfileGroupItem group )
        {
            if ( this.GroupLinkedToPage != null )
            {
                this.GroupLinkedToPage( this, new ProfileGroupEventArgs( group ) );
            }
        }

        private void OnGroupDestroyed( ProfileGroupItem group )
        {
            if ( this.GroupDestroyed != null )
            {
                this.GroupDestroyed( this, new ProfileGroupEventArgs( group ) );
            }
        }

        private void OnTimeStatsCreated( ProfileTimeStatsItem timer )
        {
            if ( this.TimeStatsCreated != null )
            {
                this.TimeStatsCreated( this, new ProfileStatsEventArgs( timer ) );
            }
        }

        private void OnTimeStatsDestroyed( ProfileTimeStatsItem timer )
        {
            if ( this.TimeStatsDestroyed != null )
            {
                this.TimeStatsDestroyed( this, new ProfileStatsEventArgs( timer ) );
            }
        }

        private void OnValueStatsCreated( ProfileValueStatsItem value )
        {
            if ( this.ValueStatsCreated != null )
            {
                this.ValueStatsCreated( this, new ProfileStatsEventArgs( value ) );
            }
        }

        private void OnValueStatsDestroyed( ProfileValueStatsItem value )
        {
            if ( this.ValueStatsDestroyed != null )
            {
                this.ValueStatsDestroyed( this, new ProfileStatsEventArgs( value ) );
            }
        }

        private void OnArrayStatsCreated( ProfileValueStatsItemArray array )
        {
            if ( this.ArrayStatsCreated != null )
            {
                this.ArrayStatsCreated( this, new ProfileStatsEventArgs( array ) );
            }
        }

        private void OnArrayStatsDestroyed( ProfileValueStatsItemArray array )
        {
            if ( this.ArrayStatsDestroyed != null )
            {
                this.ArrayStatsDestroyed( this, new ProfileStatsEventArgs( array ) );
            }
        }

        private void OnArrayValueStatsCreated( ProfileArrayValueStatsItem arrayValue )
        {
            if ( this.ArrayValueStatsCreated != null )
            {
                this.ArrayValueStatsCreated( this, new ProfileStatsEventArgs( arrayValue ) );
            }
        }

        private void OnArrayValueStatsDestroyed( ProfileArrayValueStatsItem arrayValue )
        {
            if ( this.ArrayValueStatsDestroyed != null )
            {
                this.ArrayValueStatsDestroyed( this, new ProfileStatsEventArgs( arrayValue ) );
            }
        }

        private void OnTimeStatsUpdated( ProfileTimeStatsItem timer )
        {
            if ( this.TimeStatsUpdated != null )
            {
                this.TimeStatsUpdated( this, new ProfileStatsEventArgs( timer ) );
            }
        }

        private void OnValueStatsUpdated( ProfileValueStatsItem value )
        {
            if ( this.ValueStatsUpdated != null )
            {
                this.ValueStatsUpdated( this, new ProfileStatsEventArgs( value ) );
            }
        }
        #endregion

        #region Public Functions
        public void CreateArray( string name, uint id, uint groupId )
        {
            m_mutex.WaitOne();

            ProfileValueStatsItemArray array = null;

            if ( !m_arrayDictionary.ContainsKey( id ) )
            {
                ProfileGroupItem group;
                if ( m_groupDictionary.TryGetValue( groupId, out group ) )
                {
                    array = new ProfileValueStatsItemArray( name, id, group );

                    group.Stats.Add( array );

                    m_arrayDictionary.Add( id, array );                    
                }
            }

            m_mutex.ReleaseMutex();

            if ( array != null )
            {
                OnArrayStatsCreated( array );
            }
        }

        public void CreateArrayValue( string name, uint id, uint arrayId )
        {
            m_mutex.WaitOne();

            ProfileArrayValueStatsItem arrayValue = null;

            if ( !m_valueDictionary.ContainsKey( id ) )
            {
                ProfileValueStatsItemArray array;
                if ( m_arrayDictionary.TryGetValue( arrayId, out array ) )
                {
                    arrayValue = new ProfileArrayValueStatsItem( name, id, array );

                    array.Values.Add( arrayValue );

                    m_valueDictionary.Add( id, arrayValue );                    
                }
            }

            m_mutex.ReleaseMutex();

            if ( arrayValue != null )
            {
                OnArrayValueStatsCreated( arrayValue );
            }
        }

        public void CreateGroup( string name, uint id )
        {
            m_mutex.WaitOne();

            ProfileGroupItem group = null;

            if ( !m_groupDictionary.ContainsKey( id ) )
            {
                group = new ProfileGroupItem( name, id );

                m_groupDictionary.Add( id, group );                
            }

            m_mutex.ReleaseMutex();

            if ( group != null )
            {
                OnGroupCreated( group );
            }
        }

        public void CreatePage( string name, uint id )
        {
            m_mutex.WaitOne();

            ProfilePageItem page = null;

            if ( !m_pageDictionary.ContainsKey( id ) )
            {
                page = new ProfilePageItem( name, id );

                m_pageItems.Add( page );
                m_pageDictionary.Add( id, page );                
            }

            m_mutex.ReleaseMutex();

            if ( page != null )
            {
                OnPageCreated( page );
            }
        }

        public void CreateTimer( string name, uint id, uint groupId )
        {
            m_mutex.WaitOne();

            ProfileTimeStatsItem timer = null;

            if ( !m_timerDictionary.ContainsKey( id ) )
            {
                ProfileGroupItem group;
                if ( m_groupDictionary.TryGetValue( groupId, out group ) )
                {
                    timer = new ProfileTimeStatsItem( name, id, group );

                    group.Stats.Add( timer );

                    m_timerDictionary.Add( id, timer );                    
                }
            }

            m_mutex.ReleaseMutex();

            if ( timer != null )
            {
                OnTimeStatsCreated( timer );
            }
        }

        public void CreateValue( string name, uint id, uint groupId )
        {
            m_mutex.WaitOne();

            ProfileValueStatsItem value = null;

            if ( !m_valueDictionary.ContainsKey( id ) )
            {
                ProfileGroupItem group;
                if ( m_groupDictionary.TryGetValue( groupId, out group ) )
                {
                    value = new ProfileValueStatsItem( name, id, group );

                    group.Stats.Add( value );

                    m_valueDictionary.Add( id, value );                    
                }
            }

            m_mutex.ReleaseMutex();

            if ( value != null )
            {
                OnValueStatsCreated( value );
            }
        }

        public void DestroyArray( uint id )
        {
            m_mutex.WaitOne();

            ProfileValueStatsItemArray array = null;
            if ( m_arrayDictionary.TryGetValue( id, out array ) )
            {
                List<ProfileArrayValueStatsItem> values = new List<ProfileArrayValueStatsItem>( array.Values );
                foreach ( ProfileValueStatsItem value in values )
                {
                    DestroyArrayValue( value.ID );
                }

                array.Group.Stats.Remove( array );

                m_arrayDictionary.Remove( id );                
            }

            m_mutex.ReleaseMutex();

            if ( array != null )
            {
                OnArrayStatsDestroyed( array );
            }
        }

        public void DestroyArrayValue( uint id )
        {
            m_mutex.WaitOne();

            ProfileArrayValueStatsItem arrayValue = null;

            ProfileValueStatsItem value;
            if ( m_valueDictionary.TryGetValue( id, out value ) )
            {
                if ( value is ProfileArrayValueStatsItem )
                {
                    arrayValue = value as ProfileArrayValueStatsItem;

                    arrayValue.Array.Values.Remove( arrayValue );

                    m_valueDictionary.Remove( id );                    

                    if ( arrayValue.Array.Values.Count == 0 )
                    {
                        // we'll do our own clean up of the array because in this instance, the game doesn't do it for us
                        DestroyArray( arrayValue.Array.ID );
                    }
                }
            }

            m_mutex.ReleaseMutex();

            if ( arrayValue != null )
            {
                OnArrayValueStatsDestroyed( arrayValue );
            }
        }

        public void DestroyGroup( uint id )
        {
            m_mutex.WaitOne();

            ProfileGroupItem group = null;
            if ( m_groupDictionary.TryGetValue( id, out group ) )
            {
                foreach ( ProfilePageItem page in group.Pages )
                {
                    page.Groups.Remove( group );
                }

                List<ProfileStatsItem> stats = new List<ProfileStatsItem>( group.Stats );
                foreach ( ProfileStatsItem stat in stats )
                {
                    if ( stat is ProfileValueStatsItemArray )
                    {
                        DestroyArray( stat.ID );
                    }
                    else if ( stat is ProfileTimeStatsItem )
                    {
                        DestroyTimer( stat.ID );
                    }
                    else
                    {
                        DestroyValue( stat.ID );
                    }
                }

                m_groupDictionary.Remove( id );                
            }

            m_mutex.ReleaseMutex();

            if ( group != null )
            {
                OnGroupDestroyed( group );
            }
        }

        public void DestroyPage( uint id )
        {
            m_mutex.WaitOne();

            ProfilePageItem page = null;
            if ( m_pageDictionary.TryGetValue( id, out page ) )
            {
                List<ProfileGroupItem> groups = new List<ProfileGroupItem>( page.Groups );
                foreach ( ProfileGroupItem group in groups )
                {
                    if ( (group.Pages.Count == 1) && (group.Pages[0].ID == id) )
                    {
                        // last reference to the group, so destroy it
                        DestroyGroup( group.ID );
                    }
                    else
                    {
                        // remove the reference
                        group.Pages.Remove( page );
                    }
                }

                m_pageDictionary.Remove( id );
                m_pageItems.Remove( page );                
            }

            m_mutex.ReleaseMutex();

            if ( page != null )
            {
                OnPageDestroyed( page );
            }
        }

        public void DestroyTimer( uint id )
        {
            m_mutex.WaitOne();

            ProfileTimeStatsItem timer = null;
            if ( m_timerDictionary.TryGetValue( id, out timer ) )
            {
                timer.Group.Stats.Remove( timer );

                m_timerDictionary.Remove( id );                
            }

            m_mutex.ReleaseMutex();

            if ( timer != null )
            {
                OnTimeStatsDestroyed( timer );
            }
        }

        public void DestroyValue( uint id )
        {
            m_mutex.WaitOne();
            
            ProfileValueStatsItem value = null;
            if ( m_valueDictionary.TryGetValue( id, out value ) )
            {
                value.Group.Stats.Remove( value );

                m_valueDictionary.Remove( id );                
            }

            m_mutex.ReleaseMutex();

            if ( value != null )
            {
                OnValueStatsDestroyed( value );
            }
        }

        public void LinkGroupToPage( uint groupId, uint pageId )
        {
            m_mutex.WaitOne();

            ProfileGroupItem group;
            ProfilePageItem page;
            if ( m_groupDictionary.TryGetValue( groupId, out group )
                && m_pageDictionary.TryGetValue( pageId, out page ) )
            {
                page.Groups.Add( group );
                group.Pages.Add( page );                
            }

            m_mutex.ReleaseMutex();

            if ( group != null )
            {
                OnGroupLinkedToPage( group );
            }
        }

        public void UpdateTimer( uint id, 
                                float elapsedFrameTime, float counter0Value, float counter1Value,
                                float averageElapsedFrameTime, float averageCounter0Value, float averageCounter1Value,
                                float peakElapsedFrameTime, float peakCounter0Value, float peakCounter1Value,
                                int calls, float averageCalls, int peakCalls )
        {
            m_mutex.WaitOne();

            ProfileTimeStatsItem timer = null;
            if ( m_timerDictionary.TryGetValue( id, out timer ) )
            {
                timer.ElapsedFrameTime = elapsedFrameTime;
                timer.Counter0Value = counter0Value;
                timer.Counter1Value = counter1Value;
                
                timer.AverageElapsedFrameTime = averageElapsedFrameTime;
                timer.AverageCounter0Value = averageCounter0Value;
                timer.AverageCounter1Value = averageCounter1Value;

                timer.PeakElapsedFrameTime = peakElapsedFrameTime;
                timer.PeakCounter0Value = peakCounter0Value;
                timer.PeakCounter1Value = peakCounter1Value;

                timer.FrameCalls = calls;
                timer.AverageCalls = averageCalls;
                timer.PeakCalls = peakCalls;                
            }

            m_mutex.ReleaseMutex();

            if ( timer != null )
            {
                OnTimeStatsUpdated( timer );
            }
        }

        public void UpdateValue( uint id, float currentValue, float average, float peak )
        {
            m_mutex.WaitOne();

            ProfileValueStatsItem value = null;
            if ( m_valueDictionary.TryGetValue( id, out value ) )
            {
                value.CurrentValue = currentValue;
                value.AverageValue = average;
                value.PeakValue = peak;                
            }

            m_mutex.ReleaseMutex();

            if ( value != null )
            {
                OnValueStatsUpdated( value );
            }
        }
        #endregion
    }

    #region EventArgs
    public class ProfilePageEventArgs : EventArgs
    {
        public ProfilePageEventArgs( ProfilePageItem page )
        {
            m_page = page;
        }

        #region Variables
        private ProfilePageItem m_page;
        #endregion

        #region Properties
        public ProfilePageItem Page
        {
            get
            {
                return m_page;
            }
        }
        #endregion
    }

    public class ProfileGroupEventArgs : EventArgs
    {
        public ProfileGroupEventArgs( ProfileGroupItem group )
        {
            m_group = group;
        }

        #region Variables
        private ProfileGroupItem m_group;
        #endregion

        #region Properties
        public ProfileGroupItem Group
        {
            get
            {
                return m_group;
            }
        }
        #endregion
    }

    public class ProfileStatsEventArgs : EventArgs
    {
        public ProfileStatsEventArgs( ProfileStatsItem stats )
        {
            m_stats = stats;
        }

        #region Variables
        private ProfileStatsItem m_stats;
        #endregion

        #region Properties
        public ProfileStatsItem Stats
        {
            get
            {
                return m_stats;
            }
        }
        #endregion
    }
    #endregion

    #region EventHandler Delegates
    public delegate void ProfilePageEventHandler( object sender, ProfilePageEventArgs e );
    public delegate void ProfileGroupEventHandler( object sender, ProfileGroupEventArgs e );
    public delegate void ProfileStatsEventHandler( object sender, ProfileStatsEventArgs e );
    #endregion
}
