using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace ragProfiler
{
    public partial class EkgPanel : Panel
    {
        public EkgPanel()
        {
            InitializeComponent();

            SetStyle( ControlStyles.AllPaintingInWmPaint, true );
            SetStyle( ControlStyles.OptimizedDoubleBuffer, true );
            SetStyle( ControlStyles.UserPaint, true );

            RecalculateScaleText();
        }

        #region Constants
        private const int c_topLineOffsetY = 2;
        private const int c_bottomLineOffsetY = 5;
        #endregion

        #region Variables
        private float m_minValue = 0.0f;
        private float m_maxValue = 1.0e-6f;
        private float m_minimumMaxValue = 1.0e-6f;
        private float m_deltaYvalue = 1.0e-6f;
        private float m_pixelsPerValue = 0.0f;

        private int m_verticalScrollOffset = 0;
        private bool m_autoScale = true;
        private string m_graphScaleFormat = string.Empty;
        private float m_graphScaleMillisecondsAdjust = 1.0f;
        private float[] m_graphXpixels = new float[ProfileStatSamples.MaxSamples + 1];

        private int m_samplesPerSecond;
        private int m_counter = 0;
        private ProfileTimeStatsItem.StatType m_timerStatType;
        private ProfileTimeStatsItem.TimeMode m_timerDisplayMode;
        private ProfileValueStatsItem.StatType m_valueStatType;
        private Mutex m_mutex = new Mutex();

        private List<ProfileStatSamples> m_samples = new List<ProfileStatSamples>();
        #endregion

        #region Properties
        public bool LockForUpdate
        {
            set
            {
                if ( value )
                {
                    m_mutex.WaitOne();
                }
                else
                {
                    m_mutex.ReleaseMutex();

                    RecalculateScaleText();
                }
            }
        }

        public float MinimumValue
        {
            get
            {
                return m_minValue;
            }
            set
            {
                if ( m_minValue != value )
                {
                    m_minValue = value;

                    RecalculateDeltaYValue();

                    RecalculateScaleText();
                }
            }
        }

        public float MaximumValue
        {
            get
            {
                return m_maxValue;
            }
            set
            {                
                if ( m_maxValue != value )
                {
                    m_maxValue = value;

                    if ( m_maxValue < m_minValue )
                    {
                        m_maxValue = m_minValue;
                    }

                    RecalculateMinimumMaxValue();

                    RecalculateDeltaYValue();

                    RecalculateScaleText();
                }
            }
        }

        public bool AutoScale
        {
            get
            {
                return m_autoScale;
            }
            set
            {
                if ( m_autoScale != value )
                {
                    m_autoScale = value;

                    if ( !m_autoScale )
                    {
                        m_pixelsPerValue = (this.Height - (c_bottomLineOffsetY + c_topLineOffsetY + this.Font.Height)) / (m_maxValue - m_minValue);
                    }
                    else
                    {
                        if ( m_verticalScrollOffset != 0 )
                        {
                            m_verticalScrollOffset = 0;
                        }
                    }

                    OnAutoScaleChanged();
                }
            }
        }

        public int VerticalScrollOffset
        {
            get
            {
                return m_verticalScrollOffset;
            }
            set
            {
                int minOffset = (int)(m_minValue / m_deltaYvalue);
                if ( (value >= minOffset) && (value != m_verticalScrollOffset) )
                {
                    // toggle
                    this.AutoScale = true;
                    this.AutoScale = false;

                    m_verticalScrollOffset = value;
                }
            }
        }

        public float GraphScaleMillisecondsAdjust
        {
            get
            {
                return m_graphScaleMillisecondsAdjust;
            }
        }

        public int SamplesPerSecond
        {
            get
            {
                return m_samplesPerSecond;
            }
            set
            {
                m_samplesPerSecond = value;
            }
        }

        public List<ProfileStatSamples> Samples
        {
            get
            {
                return m_samples;
            }
        }

        public ProfileTimeStatsItem.StatType TimeStatType
        {
            get
            {
                return m_timerStatType;
            }
            set
            {
                m_timerStatType = value;

                RecalculateMinimumMaxValue();

                RecalculateScaleText();
            }
        }

        public ProfileTimeStatsItem.TimeMode TimeDisplayMode
        {
            get
            {
                return m_timerDisplayMode;
            }
            set
            {
                m_timerDisplayMode = value;

                ProfileTimeStatsItem.Mode = value;

                RecalculateMinimumMaxValue();

                RecalculateScaleText();
            }
        }

        public ProfileValueStatsItem.StatType ValueStatType
        {
            get
            {
                return m_valueStatType;
            }
            set
            {
                m_valueStatType = value;

                RecalculateMinimumMaxValue();

                RecalculateScaleText();
            }
        }
        #endregion

        #region Events
        public event EventHandler AutoScaleChanged;
        #endregion

        #region Event Dispatchers
        private void OnAutoScaleChanged()
        {
            if ( this.AutoScaleChanged != null )
            {
                this.AutoScaleChanged( this, EventArgs.Empty );
            }
        }
        #endregion

        #region Overrides
        protected override void OnPaintBackground( PaintEventArgs e )
        {
            base.OnPaintBackground( e );

            if ( this.DesignMode )
            {
                return;
            }

            Pen p = new Pen( Color.FromArgb( 128, Color.LightGray ) );

            // draw the vertical grid lines
            for ( int i = 0; i < ProfileStatSamples.MaxSamples; i += m_samplesPerSecond )
            {
                int v = i + m_counter;
                if ( v > ProfileStatSamples.MaxSamples )
                {
                    v -= ProfileStatSamples.MaxSamples;
                }

                try
                {
                    e.Graphics.DrawLine( p, m_graphXpixels[v], 0.0f, m_graphXpixels[v], this.Height );
                }
                catch
                {

                }
            }

            ++m_counter;
            if ( m_counter > ProfileStatSamples.MaxSamples )
            {
                m_counter = 0;
            }
           
            if ( m_autoScale )
            {
                m_pixelsPerValue = (this.Height - (c_bottomLineOffsetY + c_topLineOffsetY + this.Font.Height)) / (m_maxValue - m_minValue);
            }

            // draw the horizontal lines
            float deltaYpos = m_deltaYvalue * m_pixelsPerValue;
            float halfFontHeight = this.Font.Height / 2.0f;

            for ( float yPos = this.Height - c_bottomLineOffsetY; yPos >= -halfFontHeight; yPos -= deltaYpos )
            {
                try
                {
                    e.Graphics.DrawLine( p, 0, yPos, this.Width, yPos );
                }
                catch
                {

                }
            }
        }

        protected override void OnPaint( PaintEventArgs e )
        {
            if ( this.DesignMode )
            {
                return;
            }

            m_mutex.WaitOne();

            float height = this.Height - (c_bottomLineOffsetY + c_topLineOffsetY + this.Font.Height);

            if ( m_autoScale )
            {
                m_pixelsPerValue = height / (m_maxValue - m_minValue);
            }

            float valueOffset = m_minValue + (m_verticalScrollOffset * m_deltaYvalue);
            float bottomY = this.Height - c_bottomLineOffsetY;
            foreach ( ProfileStatSamples samples in m_samples )
            {
                if ( !samples.Enabled )
                {
                    continue;
                }

                Pen p = new Pen( ProfileStatSamples.Colors[samples.TreeNode.ImageIndex] );

                for ( int j = 0; j < samples.NumSamples; ++j )
                {
                    float sample0 = samples.Samples[(samples.SampleStart + j) % ProfileStatSamples.MaxSamples];
                    float sample1 = samples.Samples[(samples.SampleStart + j + 1) % ProfileStatSamples.MaxSamples];

                    float y0 = bottomY - ((sample0 - valueOffset) * m_pixelsPerValue);
                    float y1 = bottomY - ((sample1 - valueOffset) * m_pixelsPerValue);

                    float x0 = m_graphXpixels[j];
                    float x1 = m_graphXpixels[j + 1];

                    if ( ((y0 < 0.0f) && (y1 < 0.0f )) || ((y0 > this.Height) && (y1 > this.Height)) )
                    {
                        continue;
                    }

                    try
                    {
                        e.Graphics.DrawLine( p, x0, Math.Max( Int16.MinValue, Math.Min( Int16.MaxValue, y0 ) ),
                            x1, Math.Max( Int16.MinValue, Math.Min( Int16.MaxValue, y1 ) ) );
                    }
                    catch
                    {

                    }
                }
            }

            // draw the horizontal lines' values
            Brush b = new SolidBrush( Color.LightGray );
            float deltaYpos = m_deltaYvalue * m_pixelsPerValue;

            for ( float yPos = this.Height - c_bottomLineOffsetY - this.Font.Height, 
                yValue = m_minValue + (m_verticalScrollOffset * m_deltaYvalue);
                yPos >= -this.Font.Height; yPos -= deltaYpos, yValue += m_deltaYvalue )
            {
                try
                {
                    // draw the value
                    string strValue = String.Format( m_graphScaleFormat, yValue * m_graphScaleMillisecondsAdjust );
                    e.Graphics.DrawString( strValue, this.Font, b, 0, yPos );
                }
                catch
                {

                }
            }

            m_mutex.ReleaseMutex();
        }

        protected override void OnSizeChanged( EventArgs e )
        {
            base.OnSizeChanged( e );

            for ( int i = 0; i < m_graphXpixels.Length; ++i )
            {
                m_graphXpixels[i] = this.Width - (((float)i / (float)(ProfileStatSamples.MaxSamples - 1)) * this.Width);
            }
        }
        #endregion

        #region Public Functions
        public void ScaleUp( float scaleFactor )
        {
            float bottomValue = m_minValue + (m_verticalScrollOffset * m_deltaYvalue);
            
            this.MaximumValue *= scaleFactor;            

            // toggle
            this.AutoScale = true;
            this.AutoScale = false;

            // restore
            m_verticalScrollOffset = (int)((bottomValue - m_minValue) / m_deltaYvalue);
            
            // fix rounding errors
            float newBottomValue = m_minValue + (m_verticalScrollOffset * m_deltaYvalue);
            if ( newBottomValue < bottomValue )
            {
                ++m_verticalScrollOffset;
            }
            else if ( newBottomValue > bottomValue )
            {
                --m_verticalScrollOffset;
            }
        }

        public void ScaleDown( float scaleFactor )
        {
            float bottomValue = m_minValue + (m_verticalScrollOffset * m_deltaYvalue);
            
            this.MaximumValue /= scaleFactor;

            // toggle so that we 
            this.AutoScale = true;
            this.AutoScale = false;

            // restore
            m_verticalScrollOffset = (int)((bottomValue - m_minValue) / m_deltaYvalue);

            // fix rounding errors
            float newBottomValue = m_minValue + (m_verticalScrollOffset * m_deltaYvalue);
            if ( newBottomValue < bottomValue )
            {
                ++m_verticalScrollOffset;
            }
            else if ( newBottomValue > bottomValue )
            {
                --m_verticalScrollOffset;
            }
        }
        #endregion

        #region Private Functions
        private void RecalculateMinimumMaxValue()
        {
            bool allTimers = true;
            foreach ( ProfileStatSamples samples in m_samples )
            {
                if ( samples.Type != ProfileStatSamples.SampleType.Timer )
                {
                    allTimers = false;
                    break;
                }
            }

            m_minimumMaxValue = 1.0f;
            if ( allTimers )
            {
                switch ( m_timerStatType )
                {
                    case ProfileTimeStatsItem.StatType.AverageFrameTime:
                    case ProfileTimeStatsItem.StatType.FrameTime:
                    case ProfileTimeStatsItem.StatType.PeakFrameTime:
                        {
                            switch ( m_timerDisplayMode )
                            {
                                case ProfileTimeStatsItem.TimeMode.ElapsedTime:
                                    m_minimumMaxValue = 1.0e-6f;
                                    break;
                                default:
                                    m_minimumMaxValue = 1.0f;
                                    break;
                            }
                        }
                        break;
                    default:
                        break;
                }
            }

            if ( m_maxValue < m_minimumMaxValue )
            {
                this.MaximumValue = m_minimumMaxValue;
            }
        }

        private void RecalculateDeltaYValue()
        {
            float tickBase = 10.0f;
            m_deltaYvalue = 1.0e-6f;

            float yHeight = m_maxValue - m_minValue;
            while ( m_deltaYvalue * tickBase < yHeight )
            {
                m_deltaYvalue *= tickBase;
            }
        }

        private void RecalculateScaleText()
        {
            bool allTimers = true;
            foreach ( ProfileStatSamples samples in m_samples )
            {
                if ( samples.Type != ProfileStatSamples.SampleType.Timer )
                {
                    allTimers = false;
                    break;
                }
            }

            m_graphScaleFormat = "{0,4:F0}";
            m_graphScaleMillisecondsAdjust = 1.0f;
            if ( allTimers )
            {
                switch ( m_timerStatType )
                {
                    case ProfileTimeStatsItem.StatType.AverageFrameTime:
                    case ProfileTimeStatsItem.StatType.FrameTime:
                    case ProfileTimeStatsItem.StatType.PeakFrameTime:
                        {
                            if ( m_maxValue > 0.001f )
                            {
                                m_graphScaleMillisecondsAdjust = 1.0e3f;

                                switch ( m_timerDisplayMode )
                                {
                                    case ProfileTimeStatsItem.TimeMode.Counter0:
                                        m_graphScaleFormat = "{0,3:F0}KI$";
                                        break;
                                    case ProfileTimeStatsItem.TimeMode.Counter1:
                                        m_graphScaleFormat = "{0,3:F0}KD$";
                                        break;
                                    default:
                                        m_graphScaleFormat += "ms";
                                        break;
                                }
                            }
                            else
                            {
                                m_graphScaleMillisecondsAdjust = 1.0e6f;

                                switch ( m_timerDisplayMode )
                                {
                                    case ProfileTimeStatsItem.TimeMode.Counter0:
                                        m_graphScaleFormat += "I$";
                                        break;
                                    case ProfileTimeStatsItem.TimeMode.Counter1:
                                        m_graphScaleFormat += "D$";
                                        break;
                                    default:
                                        m_graphScaleFormat += "us";
                                        break;
                                }
                            }
                        }
                        break;
                    default:
                        break;
                }
            }
        }
        #endregion
    }
}

