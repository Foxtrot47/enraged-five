using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

using ragCore;
using ragWidgetPage;
using ragWidgets;
using RSG.Base.Logging;

namespace ragProfiler
{
    public class PlugIn : WidgetPagePlugIn
    {
        #region Variables
        private static PlugIn sm_Instance;
        
        private Control m_parentControl;
        private PlugInHostData m_hostData;
        #endregion

        #region Properties
        public static PlugIn Instance
        {
            get
            {
                return sm_Instance;
            }
        }

        public Control ParentControl
        {
            get
            {
                return m_parentControl;
            }
        }

        public PlugInHostData HostData
        {
            get
            {
                return m_hostData;
            }
        }
        #endregion

        #region Overrides
        public override string Name
        {
            get
            {
                return "Profiler View";
            }
        }

        public override void InitPlugIn( PlugInHostData hostData )
        {
            m_hostData = hostData;

            base.InitPlugIn( hostData );
            sm_Instance = this;

            // find the top-level form
            Control parent = hostData.MenuToolStrip.Parent;
            while ( (parent != null) && !(parent is Form) )
            {
                parent = parent.Parent;
            }
            m_parentControl = parent;

            // register our WidgetPage with its CreatePageDel
            base.m_pageNameToCreateDelMap[Profiler.MyName] = new WidgetPage.CreatePageDel( Profiler.CreatePage );

            if ( !String.IsNullOrEmpty( hostData.MainRageApplication.Args ) )
            {
                string[] splitArgs = hostData.MainRageApplication.Args.Split( new char[] { '-' }, StringSplitOptions.RemoveEmptyEntries );
                foreach ( string arg in splitArgs )
                {
                    string lowerArg = arg.Trim().ToLower();
                    if ( !lowerArg.StartsWith( "!" ) && lowerArg.Contains( "profilerrag" ) )
                    {
                        bool connect = true;

                        string[] splitArg = lowerArg.Split( new char[] { ':' }, StringSplitOptions.RemoveEmptyEntries );
                        if ( (splitArg.Length > 1) && (splitArg[1] == "profilerrag") )
                        {
                            switch ( hostData.Platform )
                            {
                                case "Win32":
                                    connect = splitArg[0] == "pc";
                                    break;
                                case "Xbox360":
                                    connect = splitArg[0] == "xe";
                                    break;
                                case "PlayStation3":
                                    connect = splitArg[0] == "p3";
                                    break;
                                default:
                                    connect = false;
                                    break;
                            }
                        }
                        else if ( splitArg.Length > 0 )
                        {
                            connect = splitArg[0] == "profilerrag";
                        }
                        else
                        {
                            connect = false;
                        }

                        if ( connect )
                        {
                            Profiler.Connect();
                            break;
                        }
                    }
                }
            }            
        }

        public override void RemovePlugIn()
        {
            Profiler.Disconnect();

            base.RemovePlugIn();

            sm_Instance = null;
        }

        // PURPOSE: Override so that we can have one CreagePageDel create multiple instances of MultilineControl
        public override TD.SandDock.DockControl ResolveDockControl(String guid, ILog log)
        {
            if ( base.DllInfo.GuidToNameHash.ContainsKey( guid ) )
            {
                String pageName = base.DllInfo.GuidToNameHash[guid];
                if ( pageName.StartsWith( Profiler.MyName ) )
                {
                    // a bit of hack, but this will specify which InstanceNumber to use in the Tab Text and subsequently our hash tables
                    String num = pageName.Substring( Profiler.MyName.Length ).Trim();
                    Profiler.UseViewNum = int.Parse( num );

                    IDockableView view = CreateView( m_pageNameToCreateDelMap[Profiler.MyName], log );

                    // we can go ahead and call PostOpenView because SandDockManager is going to do the opening for us
                    PostOpenView( view.DockControl, view );
                    return view.DockControl;
                }
            }

            return null;
        }

        public override bool CheckPluginStatus()
        {
            return Profiler.IsConnected;
        }

        public override void ActivatePlugInMenuItem( object sender, EventArgs args )
        {
            base.CreateAndOpenView(base.m_pageNameToCreateDelMap[Profiler.MyName], LogFactory.ApplicationLog);
        }

        public override bool ShowOnActivation
        {
            get
            {
                return true;
            }
        }
        #endregion
    }
}
