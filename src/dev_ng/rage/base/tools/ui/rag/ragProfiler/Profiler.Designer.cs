namespace ragProfiler
{
    partial class Profiler
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            GlacialComponents.Controls.GlacialTreeList.GTLColumn gtlColumn1 = new GlacialComponents.Controls.GlacialTreeList.GTLColumn();
            GlacialComponents.Controls.GlacialTreeList.GTLColumn gtlColumn2 = new GlacialComponents.Controls.GlacialTreeList.GTLColumn();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager( typeof( Profiler ) );
            this.timer = new System.Windows.Forms.Timer( this.components );
            this.colorKeyImageList = new System.Windows.Forms.ImageList( this.components );
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.ekgGroupBox = new System.Windows.Forms.GroupBox();
            this.autoScaleCheckBox = new System.Windows.Forms.CheckBox();
            this.scrollDownButton = new System.Windows.Forms.Button();
            this.scrollUpButton = new System.Windows.Forms.Button();
            this.scrollLabel = new System.Windows.Forms.Label();
            this.scaleLabel = new System.Windows.Forms.Label();
            this.scaleDownButton = new System.Windows.Forms.Button();
            this.scaleUpButton = new System.Windows.Forms.Button();
            this.ekgPanel = new ragProfiler.EkgPanel();
            this.colorKeyGroupBox = new System.Windows.Forms.GroupBox();
            this.colorKeyGlacialTreeList = new GlacialComponents.Controls.GlacialTreeList.GlacialTreeList();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.pageToolStripLabel = new System.Windows.Forms.ToolStripLabel();
            this.pageToolStripComboBox = new System.Windows.Forms.ToolStripComboBox();
            this.optionsToolStripDropDownButton = new System.Windows.Forms.ToolStripDropDownButton();
            this.nextPageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.previousPageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.showTimeTypeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.frameTimeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.averageTimeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.peakTimeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.frameCallsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.averageCallsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.peakCallsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.timeDisplayModeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.elapsedTimeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.counter0ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.counter1ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showValueTypeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.currentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.averageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.peakToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.valueDefaultToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.autoScaleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.scaleUpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.scaleDownToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.scrollUpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.scrollDownToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.ekgGroupBox.SuspendLayout();
            this.colorKeyGroupBox.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // timer
            // 
            this.timer.Interval = 10;
            this.timer.Tick += new System.EventHandler( this.timer_Tick );
            // 
            // colorKeyImageList
            // 
            this.colorKeyImageList.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.colorKeyImageList.ImageSize = new System.Drawing.Size( 16, 16 );
            this.colorKeyImageList.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.Location = new System.Drawing.Point( 0, 25 );
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add( this.ekgGroupBox );
            this.splitContainer1.Panel1MinSize = 187;
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add( this.colorKeyGroupBox );
            this.splitContainer1.Panel2MinSize = 50;
            this.splitContainer1.Size = new System.Drawing.Size( 803, 455 );
            this.splitContainer1.SplitterDistance = 187;
            this.splitContainer1.TabIndex = 1;
            // 
            // ekgGroupBox
            // 
            this.ekgGroupBox.BackColor = System.Drawing.SystemColors.Control;
            this.ekgGroupBox.Controls.Add( this.autoScaleCheckBox );
            this.ekgGroupBox.Controls.Add( this.scrollDownButton );
            this.ekgGroupBox.Controls.Add( this.scrollUpButton );
            this.ekgGroupBox.Controls.Add( this.scrollLabel );
            this.ekgGroupBox.Controls.Add( this.scaleLabel );
            this.ekgGroupBox.Controls.Add( this.scaleDownButton );
            this.ekgGroupBox.Controls.Add( this.scaleUpButton );
            this.ekgGroupBox.Controls.Add( this.ekgPanel );
            this.ekgGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ekgGroupBox.Location = new System.Drawing.Point( 0, 0 );
            this.ekgGroupBox.Name = "ekgGroupBox";
            this.ekgGroupBox.Size = new System.Drawing.Size( 803, 187 );
            this.ekgGroupBox.TabIndex = 0;
            this.ekgGroupBox.TabStop = false;
            this.ekgGroupBox.Text = "EKG";
            // 
            // autoScaleCheckBox
            // 
            this.autoScaleCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.autoScaleCheckBox.AutoSize = true;
            this.autoScaleCheckBox.Checked = true;
            this.autoScaleCheckBox.Location = new System.Drawing.Point( 749, 93 );
            this.autoScaleCheckBox.Name = "autoScaleCheckBox";
            this.autoScaleCheckBox.Size = new System.Drawing.Size( 48, 17 );
            this.autoScaleCheckBox.TabIndex = 0;
            this.autoScaleCheckBox.Text = "Auto";
            this.autoScaleCheckBox.UseVisualStyleBackColor = true;
            this.autoScaleCheckBox.CheckedChanged += new System.EventHandler( this.autoScaleCheckBox_CheckedChanged );
            // 
            // scrollDownButton
            // 
            this.scrollDownButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.scrollDownButton.Location = new System.Drawing.Point( 749, 158 );
            this.scrollDownButton.Name = "scrollDownButton";
            this.scrollDownButton.Size = new System.Drawing.Size( 48, 23 );
            this.scrollDownButton.TabIndex = 8;
            this.scrollDownButton.Text = "-";
            this.scrollDownButton.UseVisualStyleBackColor = true;
            this.scrollDownButton.Click += new System.EventHandler( this.scrollDownToolStripMenuItem_Click );
            // 
            // scrollUpButton
            // 
            this.scrollUpButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.scrollUpButton.Location = new System.Drawing.Point( 749, 129 );
            this.scrollUpButton.Name = "scrollUpButton";
            this.scrollUpButton.Size = new System.Drawing.Size( 48, 23 );
            this.scrollUpButton.TabIndex = 7;
            this.scrollUpButton.Text = "+";
            this.scrollUpButton.UseVisualStyleBackColor = true;
            this.scrollUpButton.Click += new System.EventHandler( this.scrollUpToolStripMenuItem_Click );
            // 
            // scrollLabel
            // 
            this.scrollLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.scrollLabel.AutoSize = true;
            this.scrollLabel.Location = new System.Drawing.Point( 755, 113 );
            this.scrollLabel.Name = "scrollLabel";
            this.scrollLabel.Size = new System.Drawing.Size( 33, 13 );
            this.scrollLabel.TabIndex = 6;
            this.scrollLabel.Text = "Scroll";
            // 
            // scaleLabel
            // 
            this.scaleLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.scaleLabel.AutoSize = true;
            this.scaleLabel.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)) );
            this.scaleLabel.Location = new System.Drawing.Point( 754, 19 );
            this.scaleLabel.Name = "scaleLabel";
            this.scaleLabel.Size = new System.Drawing.Size( 34, 13 );
            this.scaleLabel.TabIndex = 5;
            this.scaleLabel.Text = "Scale";
            // 
            // scaleDownButton
            // 
            this.scaleDownButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.scaleDownButton.Location = new System.Drawing.Point( 749, 64 );
            this.scaleDownButton.Name = "scaleDownButton";
            this.scaleDownButton.Size = new System.Drawing.Size( 48, 23 );
            this.scaleDownButton.TabIndex = 4;
            this.scaleDownButton.Text = "-";
            this.scaleDownButton.UseVisualStyleBackColor = true;
            this.scaleDownButton.Click += new System.EventHandler( this.scaleDownToolStripMenuItem_Click );
            // 
            // scaleUpButton
            // 
            this.scaleUpButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.scaleUpButton.Location = new System.Drawing.Point( 749, 35 );
            this.scaleUpButton.Name = "scaleUpButton";
            this.scaleUpButton.Size = new System.Drawing.Size( 48, 23 );
            this.scaleUpButton.TabIndex = 3;
            this.scaleUpButton.Text = "+";
            this.scaleUpButton.UseVisualStyleBackColor = true;
            this.scaleUpButton.Click += new System.EventHandler( this.scaleUpToolStripMenuItem_Click );
            // 
            // ekgPanel
            // 
            this.ekgPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.ekgPanel.AutoScale = true;
            this.ekgPanel.BackColor = System.Drawing.Color.FromArgb( ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))) );
            this.ekgPanel.Location = new System.Drawing.Point( 6, 19 );
            this.ekgPanel.MaximumValue = 1E-06F;
            this.ekgPanel.MinimumValue = 0F;
            this.ekgPanel.Name = "ekgPanel";
            this.ekgPanel.SamplesPerSecond = 0;
            this.ekgPanel.Size = new System.Drawing.Size( 737, 162 );
            this.ekgPanel.TabIndex = 0;
            this.ekgPanel.TimeDisplayMode = ragProfiler.ProfileTimeStatsItem.TimeMode.ElapsedTime;
            this.ekgPanel.TimeStatType = ragProfiler.ProfileTimeStatsItem.StatType.FrameTime;
            this.ekgPanel.ValueStatType = ragProfiler.ProfileValueStatsItem.StatType.Current;
            this.ekgPanel.VerticalScrollOffset = 0;
            this.ekgPanel.AutoScaleChanged += new System.EventHandler( this.ekgPanel_AutoScaleChanged );
            // 
            // colorKeyGroupBox
            // 
            this.colorKeyGroupBox.Controls.Add( this.colorKeyGlacialTreeList );
            this.colorKeyGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.colorKeyGroupBox.Location = new System.Drawing.Point( 0, 0 );
            this.colorKeyGroupBox.Name = "colorKeyGroupBox";
            this.colorKeyGroupBox.Size = new System.Drawing.Size( 803, 264 );
            this.colorKeyGroupBox.TabIndex = 0;
            this.colorKeyGroupBox.TabStop = false;
            this.colorKeyGroupBox.Text = "Color Key";
            // 
            // colorKeyGlacialTreeList
            // 
            gtlColumn1.CheckBoxes = true;
            gtlColumn1.ImageIndex = -1;
            gtlColumn1.Name = "nameColumn";
            gtlColumn1.Text = "Name";
            gtlColumn1.Width = 250;
            gtlColumn2.ImageIndex = -1;
            gtlColumn2.Name = "valueColumn";
            gtlColumn2.Text = "Value";
            gtlColumn2.Width = 200;
            this.colorKeyGlacialTreeList.Columns.AddRange( new GlacialComponents.Controls.GlacialTreeList.GTLColumn[] {
            gtlColumn1,
            gtlColumn2} );
            this.colorKeyGlacialTreeList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.colorKeyGlacialTreeList.HotTrackingColor = System.Drawing.SystemColors.HotTrack;
            this.colorKeyGlacialTreeList.ImageList = this.colorKeyImageList;
            this.colorKeyGlacialTreeList.Location = new System.Drawing.Point( 3, 16 );
            this.colorKeyGlacialTreeList.MultiSelect = true;
            this.colorKeyGlacialTreeList.Name = "colorKeyGlacialTreeList";
            this.colorKeyGlacialTreeList.SelectedTextColor = System.Drawing.SystemColors.HighlightText;
            this.colorKeyGlacialTreeList.SelectionColor = System.Drawing.SystemColors.Highlight;
            this.colorKeyGlacialTreeList.ShadedColumnMode = GlacialComponents.Controls.GlacialTreeList.GTLShadedColumnModes.None;
            this.colorKeyGlacialTreeList.Size = new System.Drawing.Size( 797, 245 );
            this.colorKeyGlacialTreeList.TabIndex = 0;
            this.colorKeyGlacialTreeList.Text = "glacialTreeList1";
            this.colorKeyGlacialTreeList.UnfocusedSelectionColor = System.Drawing.SystemColors.Control;
            this.colorKeyGlacialTreeList.UnfocusedSelectionTextColor = System.Drawing.SystemColors.ControlText;
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.pageToolStripLabel,
            this.pageToolStripComboBox,
            this.optionsToolStripDropDownButton} );
            this.toolStrip1.Location = new System.Drawing.Point( 0, 0 );
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size( 803, 25 );
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // pageToolStripLabel
            // 
            this.pageToolStripLabel.Name = "pageToolStripLabel";
            this.pageToolStripLabel.Size = new System.Drawing.Size( 31, 22 );
            this.pageToolStripLabel.Text = "Page";
            // 
            // pageToolStripComboBox
            // 
            this.pageToolStripComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.pageToolStripComboBox.Name = "pageToolStripComboBox";
            this.pageToolStripComboBox.Size = new System.Drawing.Size( 250, 25 );
            this.pageToolStripComboBox.SelectedIndexChanged += new System.EventHandler( this.pageToolStripComboBox_SelectedIndexChanged );
            // 
            // optionsToolStripDropDownButton
            // 
            this.optionsToolStripDropDownButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.optionsToolStripDropDownButton.DropDownItems.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.nextPageToolStripMenuItem,
            this.previousPageToolStripMenuItem,
            this.toolStripSeparator2,
            this.showTimeTypeToolStripMenuItem,
            this.timeDisplayModeToolStripMenuItem,
            this.showValueTypeToolStripMenuItem,
            this.toolStripSeparator1,
            this.autoScaleToolStripMenuItem,
            this.scaleUpToolStripMenuItem,
            this.scaleDownToolStripMenuItem,
            this.scrollUpToolStripMenuItem,
            this.scrollDownToolStripMenuItem} );
            this.optionsToolStripDropDownButton.Image = ((System.Drawing.Image)(resources.GetObject( "optionsToolStripDropDownButton.Image" )));
            this.optionsToolStripDropDownButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.optionsToolStripDropDownButton.Name = "optionsToolStripDropDownButton";
            this.optionsToolStripDropDownButton.Size = new System.Drawing.Size( 57, 22 );
            this.optionsToolStripDropDownButton.Text = "Options";
            this.optionsToolStripDropDownButton.DropDownOpening += new System.EventHandler( this.optionsToolStripDropDownButton_DropDownOpening );
            // 
            // nextPageToolStripMenuItem
            // 
            this.nextPageToolStripMenuItem.Name = "nextPageToolStripMenuItem";
            this.nextPageToolStripMenuItem.Size = new System.Drawing.Size( 173, 22 );
            this.nextPageToolStripMenuItem.Text = "&Next Page";
            this.nextPageToolStripMenuItem.Click += new System.EventHandler( this.nextPageToolStripMenuItem_Click );
            // 
            // previousPageToolStripMenuItem
            // 
            this.previousPageToolStripMenuItem.Name = "previousPageToolStripMenuItem";
            this.previousPageToolStripMenuItem.Size = new System.Drawing.Size( 173, 22 );
            this.previousPageToolStripMenuItem.Text = "&Previous Page";
            this.previousPageToolStripMenuItem.Click += new System.EventHandler( this.previousPageToolStripMenuItem_Click );
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size( 170, 6 );
            // 
            // showTimeTypeToolStripMenuItem
            // 
            this.showTimeTypeToolStripMenuItem.DropDownItems.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.frameTimeToolStripMenuItem,
            this.averageTimeToolStripMenuItem,
            this.peakTimeToolStripMenuItem,
            this.frameCallsToolStripMenuItem,
            this.averageCallsToolStripMenuItem,
            this.peakCallsToolStripMenuItem} );
            this.showTimeTypeToolStripMenuItem.Name = "showTimeTypeToolStripMenuItem";
            this.showTimeTypeToolStripMenuItem.Size = new System.Drawing.Size( 173, 22 );
            this.showTimeTypeToolStripMenuItem.Text = "Show &Time Type";
            this.showTimeTypeToolStripMenuItem.DropDownOpening += new System.EventHandler( this.showTimeTypeToolStripMenuItem_DropDownOpening );
            // 
            // frameTimeToolStripMenuItem
            // 
            this.frameTimeToolStripMenuItem.CheckOnClick = true;
            this.frameTimeToolStripMenuItem.Name = "frameTimeToolStripMenuItem";
            this.frameTimeToolStripMenuItem.Size = new System.Drawing.Size( 151, 22 );
            this.frameTimeToolStripMenuItem.Tag = "FrameTime";
            this.frameTimeToolStripMenuItem.Text = "&Frame Time";
            this.frameTimeToolStripMenuItem.Click += new System.EventHandler( this.showTimeTypeToolStripSubMenuItem_Click );
            // 
            // averageTimeToolStripMenuItem
            // 
            this.averageTimeToolStripMenuItem.CheckOnClick = true;
            this.averageTimeToolStripMenuItem.Name = "averageTimeToolStripMenuItem";
            this.averageTimeToolStripMenuItem.Size = new System.Drawing.Size( 151, 22 );
            this.averageTimeToolStripMenuItem.Tag = "AverageFrameTime";
            this.averageTimeToolStripMenuItem.Text = "&Average Time";
            this.averageTimeToolStripMenuItem.Click += new System.EventHandler( this.showTimeTypeToolStripSubMenuItem_Click );
            // 
            // peakTimeToolStripMenuItem
            // 
            this.peakTimeToolStripMenuItem.CheckOnClick = true;
            this.peakTimeToolStripMenuItem.Name = "peakTimeToolStripMenuItem";
            this.peakTimeToolStripMenuItem.Size = new System.Drawing.Size( 151, 22 );
            this.peakTimeToolStripMenuItem.Tag = "PeakFrameTime";
            this.peakTimeToolStripMenuItem.Text = "&Peak Time";
            this.peakTimeToolStripMenuItem.Click += new System.EventHandler( this.showTimeTypeToolStripSubMenuItem_Click );
            // 
            // frameCallsToolStripMenuItem
            // 
            this.frameCallsToolStripMenuItem.CheckOnClick = true;
            this.frameCallsToolStripMenuItem.Name = "frameCallsToolStripMenuItem";
            this.frameCallsToolStripMenuItem.Size = new System.Drawing.Size( 151, 22 );
            this.frameCallsToolStripMenuItem.Tag = "FrameCalls";
            this.frameCallsToolStripMenuItem.Text = "Frame &Calls";
            this.frameCallsToolStripMenuItem.Click += new System.EventHandler( this.showTimeTypeToolStripSubMenuItem_Click );
            // 
            // averageCallsToolStripMenuItem
            // 
            this.averageCallsToolStripMenuItem.CheckOnClick = true;
            this.averageCallsToolStripMenuItem.Name = "averageCallsToolStripMenuItem";
            this.averageCallsToolStripMenuItem.Size = new System.Drawing.Size( 151, 22 );
            this.averageCallsToolStripMenuItem.Tag = "AverageCalls";
            this.averageCallsToolStripMenuItem.Text = "A&verage Calls";
            this.averageCallsToolStripMenuItem.Click += new System.EventHandler( this.showTimeTypeToolStripSubMenuItem_Click );
            // 
            // peakCallsToolStripMenuItem
            // 
            this.peakCallsToolStripMenuItem.CheckOnClick = true;
            this.peakCallsToolStripMenuItem.Name = "peakCallsToolStripMenuItem";
            this.peakCallsToolStripMenuItem.Size = new System.Drawing.Size( 151, 22 );
            this.peakCallsToolStripMenuItem.Tag = "PeakCalls";
            this.peakCallsToolStripMenuItem.Text = "P&eak Calls";
            this.peakCallsToolStripMenuItem.Click += new System.EventHandler( this.showTimeTypeToolStripSubMenuItem_Click );
            // 
            // timeDisplayModeToolStripMenuItem
            // 
            this.timeDisplayModeToolStripMenuItem.DropDownItems.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.elapsedTimeToolStripMenuItem,
            this.counter0ToolStripMenuItem,
            this.counter1ToolStripMenuItem} );
            this.timeDisplayModeToolStripMenuItem.Name = "timeDisplayModeToolStripMenuItem";
            this.timeDisplayModeToolStripMenuItem.Size = new System.Drawing.Size( 173, 22 );
            this.timeDisplayModeToolStripMenuItem.Text = "Time Display &Mode";
            this.timeDisplayModeToolStripMenuItem.DropDownOpening += new System.EventHandler( this.timeDisplayModeToolStripMenuItem_DropDownOpening );
            // 
            // elapsedTimeToolStripMenuItem
            // 
            this.elapsedTimeToolStripMenuItem.Checked = true;
            this.elapsedTimeToolStripMenuItem.CheckOnClick = true;
            this.elapsedTimeToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.elapsedTimeToolStripMenuItem.Name = "elapsedTimeToolStripMenuItem";
            this.elapsedTimeToolStripMenuItem.Size = new System.Drawing.Size( 147, 22 );
            this.elapsedTimeToolStripMenuItem.Tag = "ElapsedTime";
            this.elapsedTimeToolStripMenuItem.Text = "&Elapsed Time";
            this.elapsedTimeToolStripMenuItem.Click += new System.EventHandler( this.timeDisplayModeToolStripSubMenuItem_Click );
            // 
            // counter0ToolStripMenuItem
            // 
            this.counter0ToolStripMenuItem.CheckOnClick = true;
            this.counter0ToolStripMenuItem.Name = "counter0ToolStripMenuItem";
            this.counter0ToolStripMenuItem.Size = new System.Drawing.Size( 147, 22 );
            this.counter0ToolStripMenuItem.Tag = "Counter0";
            this.counter0ToolStripMenuItem.Text = "Counter&0";
            this.counter0ToolStripMenuItem.Click += new System.EventHandler( this.timeDisplayModeToolStripSubMenuItem_Click );
            // 
            // counter1ToolStripMenuItem
            // 
            this.counter1ToolStripMenuItem.CheckOnClick = true;
            this.counter1ToolStripMenuItem.Name = "counter1ToolStripMenuItem";
            this.counter1ToolStripMenuItem.Size = new System.Drawing.Size( 147, 22 );
            this.counter1ToolStripMenuItem.Tag = "Counter1";
            this.counter1ToolStripMenuItem.Text = "Counter&1";
            this.counter1ToolStripMenuItem.Click += new System.EventHandler( this.timeDisplayModeToolStripSubMenuItem_Click );
            // 
            // showValueTypeToolStripMenuItem
            // 
            this.showValueTypeToolStripMenuItem.DropDownItems.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.currentToolStripMenuItem,
            this.averageToolStripMenuItem,
            this.peakToolStripMenuItem,
            this.valueDefaultToolStripMenuItem} );
            this.showValueTypeToolStripMenuItem.Name = "showValueTypeToolStripMenuItem";
            this.showValueTypeToolStripMenuItem.Size = new System.Drawing.Size( 173, 22 );
            this.showValueTypeToolStripMenuItem.Text = "Show &Value Type";
            this.showValueTypeToolStripMenuItem.DropDownOpening += new System.EventHandler( this.showValueTypeToolStripMenuItem_DropDownOpening );
            // 
            // currentToolStripMenuItem
            // 
            this.currentToolStripMenuItem.CheckOnClick = true;
            this.currentToolStripMenuItem.Name = "currentToolStripMenuItem";
            this.currentToolStripMenuItem.Size = new System.Drawing.Size( 149, 22 );
            this.currentToolStripMenuItem.Tag = "Current";
            this.currentToolStripMenuItem.Text = "&Current";
            this.currentToolStripMenuItem.Click += new System.EventHandler( this.showValueTypeToolStripSubMenuItem_Click );
            // 
            // averageToolStripMenuItem
            // 
            this.averageToolStripMenuItem.CheckOnClick = true;
            this.averageToolStripMenuItem.Name = "averageToolStripMenuItem";
            this.averageToolStripMenuItem.Size = new System.Drawing.Size( 149, 22 );
            this.averageToolStripMenuItem.Tag = "Average";
            this.averageToolStripMenuItem.Text = "&Average";
            this.averageToolStripMenuItem.Click += new System.EventHandler( this.showValueTypeToolStripSubMenuItem_Click );
            // 
            // peakToolStripMenuItem
            // 
            this.peakToolStripMenuItem.CheckOnClick = true;
            this.peakToolStripMenuItem.Name = "peakToolStripMenuItem";
            this.peakToolStripMenuItem.Size = new System.Drawing.Size( 149, 22 );
            this.peakToolStripMenuItem.Tag = "Peak";
            this.peakToolStripMenuItem.Text = "&Peak";
            this.peakToolStripMenuItem.Click += new System.EventHandler( this.showValueTypeToolStripSubMenuItem_Click );
            // 
            // valueDefaultToolStripMenuItem
            // 
            this.valueDefaultToolStripMenuItem.CheckOnClick = true;
            this.valueDefaultToolStripMenuItem.Name = "valueDefaultToolStripMenuItem";
            this.valueDefaultToolStripMenuItem.Size = new System.Drawing.Size( 149, 22 );
            this.valueDefaultToolStripMenuItem.Tag = "ValueDefault";
            this.valueDefaultToolStripMenuItem.Text = "Value &Default";
            this.valueDefaultToolStripMenuItem.Click += new System.EventHandler( this.showValueTypeToolStripSubMenuItem_Click );
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size( 170, 6 );
            // 
            // autoScaleToolStripMenuItem
            // 
            this.autoScaleToolStripMenuItem.Checked = true;
            this.autoScaleToolStripMenuItem.CheckOnClick = true;
            this.autoScaleToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.autoScaleToolStripMenuItem.Name = "autoScaleToolStripMenuItem";
            this.autoScaleToolStripMenuItem.Size = new System.Drawing.Size( 173, 22 );
            this.autoScaleToolStripMenuItem.Text = "&Auto Scale";
            this.autoScaleToolStripMenuItem.Click += new System.EventHandler( this.autoScaleToolStripMenuItem_Click );
            // 
            // scaleUpToolStripMenuItem
            // 
            this.scaleUpToolStripMenuItem.Name = "scaleUpToolStripMenuItem";
            this.scaleUpToolStripMenuItem.Size = new System.Drawing.Size( 173, 22 );
            this.scaleUpToolStripMenuItem.Text = "Scale &Up";
            this.scaleUpToolStripMenuItem.Click += new System.EventHandler( this.scaleUpToolStripMenuItem_Click );
            // 
            // scaleDownToolStripMenuItem
            // 
            this.scaleDownToolStripMenuItem.Name = "scaleDownToolStripMenuItem";
            this.scaleDownToolStripMenuItem.Size = new System.Drawing.Size( 173, 22 );
            this.scaleDownToolStripMenuItem.Text = "Scale &Down";
            this.scaleDownToolStripMenuItem.Click += new System.EventHandler( this.scaleDownToolStripMenuItem_Click );
            // 
            // scrollUpToolStripMenuItem
            // 
            this.scrollUpToolStripMenuItem.Name = "scrollUpToolStripMenuItem";
            this.scrollUpToolStripMenuItem.Size = new System.Drawing.Size( 173, 22 );
            this.scrollUpToolStripMenuItem.Text = "Scroll U&p";
            this.scrollUpToolStripMenuItem.Click += new System.EventHandler( this.scrollUpToolStripMenuItem_Click );
            // 
            // scrollDownToolStripMenuItem
            // 
            this.scrollDownToolStripMenuItem.Name = "scrollDownToolStripMenuItem";
            this.scrollDownToolStripMenuItem.Size = new System.Drawing.Size( 173, 22 );
            this.scrollDownToolStripMenuItem.Text = "Scroll D&own";
            this.scrollDownToolStripMenuItem.Click += new System.EventHandler( this.scrollDownToolStripMenuItem_Click );
            // 
            // Profiler
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.Controls.Add( this.splitContainer1 );
            this.Controls.Add( this.toolStrip1 );
            this.Name = "Profiler";
            this.Size = new System.Drawing.Size( 803, 480 );
            this.Load += new System.EventHandler( this.Profiler_Load );
            this.splitContainer1.Panel1.ResumeLayout( false );
            this.splitContainer1.Panel2.ResumeLayout( false );
            this.splitContainer1.ResumeLayout( false );
            this.ekgGroupBox.ResumeLayout( false );
            this.ekgGroupBox.PerformLayout();
            this.colorKeyGroupBox.ResumeLayout( false );
            this.toolStrip1.ResumeLayout( false );
            this.toolStrip1.PerformLayout();
            this.ResumeLayout( false );
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripLabel pageToolStripLabel;
        private System.Windows.Forms.ToolStripComboBox pageToolStripComboBox;
        private System.Windows.Forms.ToolStripDropDownButton optionsToolStripDropDownButton;
        private System.Windows.Forms.ToolStripMenuItem autoScaleToolStripMenuItem;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.GroupBox ekgGroupBox;
        private System.Windows.Forms.GroupBox colorKeyGroupBox;
        private System.Windows.Forms.ImageList colorKeyImageList;
        private System.Windows.Forms.ToolStripMenuItem showTimeTypeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem showValueTypeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem frameTimeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem averageTimeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem peakTimeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem frameCallsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem averageCallsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem peakCallsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem currentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem averageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem peakToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem valueDefaultToolStripMenuItem;
        private EkgPanel ekgPanel;
        private GlacialComponents.Controls.GlacialTreeList.GlacialTreeList colorKeyGlacialTreeList;
        private System.Windows.Forms.ToolStripMenuItem timeDisplayModeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem elapsedTimeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem counter0ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem counter1ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem scaleUpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem scaleDownToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nextPageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem previousPageToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem scrollUpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem scrollDownToolStripMenuItem;
        private System.Windows.Forms.Button scrollDownButton;
        private System.Windows.Forms.Button scrollUpButton;
        private System.Windows.Forms.Label scrollLabel;
        private System.Windows.Forms.Label scaleLabel;
        private System.Windows.Forms.Button scaleDownButton;
        private System.Windows.Forms.Button scaleUpButton;
        private System.Windows.Forms.CheckBox autoScaleCheckBox;
    }
}
