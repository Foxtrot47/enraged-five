﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using ragCore;
using ragWidgets;
using RSG.Base.Collections;
using RSG.Base.Logging;
using RSG.Base.Net;

namespace ragClient
{
    /// <summary>
    /// 
    /// </summary>
    public static class Client
    {
        public class CommandContext
        {
            public delegate void PipeReadFunction();
            //public delegate void CompleteCallback(Result result);

            public ProxyConnection ProxyConnection { get; set; }
            public cRemoteConsole Console { get; set; }
            public BankPipe PipeBank { get; set; }
            public INamedPipe PipeOutput { get; set; }
            public INamedPipe PipeEvents { get; set; }
            public PipeReadFunction ReadBankPipe { get; set; }
            public PipeReadFunction ReadOutputPipe { get; set; }
            public bool ProxyInitialised { get; set; }
            public ObservableCollection<WidgetBank> Banks { get; set; }
        }

        private static CommandContext m_Context = new CommandContext();
        private static PipeID m_RagPipeName;
        private static bool m_StoppingPipe;
        private static INamedPipe m_RagPipe = PipeID.CreateNamedPipe();
        private static Thread m_PipeUpdateThread;
        private static Thread m_OutputUpdateThread;
        private static BankRemotePacketProcessor m_PacketProcessor = new BankRemotePacketProcessor();
        private static BankManager m_Bm = new BankManager();
        private static bool m_ConsoleOnly;

        public static BankManager BM
        {
            get { return m_Bm; }
        }

        public static CommandContext Context
        {
            get { return m_Context; }
        }

        public static bool ConsoleOnly
        {
            get
            {
                return m_ConsoleOnly;
            }
            set
            {
                m_ConsoleOnly = value;
            }
        }

        static void WidgetBank_AddBankEvent(BankPipe pipe, WidgetBank bank)
        {
            m_Bm.AddBank(bank);
        }

        [Obsolete("Use the version that passes in an ILog object.")]
        public static void CloseConnections()
        {
            CloseConnections(LogFactory.ApplicationLog);
        }

        public static void CloseConnections(ILog log)
        {
            log.Message("Client closing connections");
            m_StoppingPipe = true;

            if (m_RagPipe != null)
            {
                m_RagPipe.CancelCreate();
            }

            if (m_Context.ProxyConnection != null)
            {
                m_Context.ProxyConnection.DisconnectFromProxy();

                // Allow proxy to shut it down peacefully before closing pipes
                bool isConsoleConnected = m_Context.Console.IsConnected();
                if (!isConsoleConnected)
                {
                    isConsoleConnected = m_Context.Console.Connect();
                }

                if (isConsoleConnected)
                {
                    string command = "getProxyStatus " + m_Context.ProxyConnection.ProxyUID;
                    for (int i = 0; i < 20; i++)
                    {
                        Thread.Sleep(200);
                        string proxyStatus = m_Context.Console.SendCommand(command);
                        if (proxyStatus.Contains("Error"))
                        {
                            break;
                        }
                    }

                    m_Context.Console.DisconnectConsole();
                }
                else
                {
                    Thread.Sleep(1000);
                }
            }

            if (!m_ConsoleOnly)
            {
                m_Context.PipeBank.Close();
                m_Context.PipeOutput.Close();
                m_Context.PipeEvents.Close();
            }

            m_StoppingPipe = false;
        }

        [Obsolete("Use the version that passes in an ILog object.")]
        public static void SetupConnection()
        {
            SetupConnection(LogFactory.ApplicationLog);
        }

        public static void SetupConnection(ILog log)
        {
            m_Context.Console = new cRemoteConsole();
            m_Context.ProxyConnection = new ProxyConnection("Rag Proxy", LogFactory.ApplicationLog);

            if (!m_ConsoleOnly)
            {
                m_Context.PipeBank = new BankPipe();
                m_Context.PipeOutput = PipeID.CreateNamedPipe();
                m_Context.PipeEvents = PipeID.CreateNamedPipe();

                BankManager.AddHandlers(m_PacketProcessor);

                WidgetAngle.AddHandlers(m_PacketProcessor);
                WidgetBank.AddHandlers(m_PacketProcessor);
                WidgetButton.AddHandlers(m_PacketProcessor);
                WidgetColor.AddHandlers(m_PacketProcessor);
                WidgetCombo.AddHandlers(m_PacketProcessor);
                WidgetData.AddHandlers(m_PacketProcessor);
                WidgetGroup.AddHandlers(m_PacketProcessor);
                WidgetImageViewer.AddHandlers(m_PacketProcessor);
                ragWidgets.WidgetList.AddHandlers(m_PacketProcessor);
                WidgetMatrix.AddHandlers(m_PacketProcessor);
                WidgetSeparator.AddHandlers(m_PacketProcessor);
                WidgetSliderFloat.AddHandlers(m_PacketProcessor);
                WidgetSliderInt.AddHandlers(m_PacketProcessor);
                WidgetText.AddHandlers(m_PacketProcessor);
                WidgetTitle.AddHandlers(m_PacketProcessor);
                WidgetToggle.AddHandlers(m_PacketProcessor);
                WidgetTreeList.AddHandlers(m_PacketProcessor);
                WidgetVector.AddHandlers(m_PacketProcessor);
                WidgetVCR.AddHandlers(m_PacketProcessor);

                WidgetBank.AddBankEvent += new WidgetBank.BankDelegate(WidgetBank_AddBankEvent);
                m_PacketProcessor.DestroyHandler = new BankRemotePacketProcessor.DestroyDelegate(Widget.DestroyHandler);
            }

            IList<GameConnection> games = GameConnection.AcquireGameList(LogFactory.ApplicationLog);
            if (!games.Any())
            {
                log.Message("Proxy not connected to any games");
                return;
            }

            GameConnection defaultGame = games.FirstOrDefault(item => item.Default);
            if (defaultGame == null)
            {
                defaultGame = games[0];
            }

            if (!m_Context.Console.Connect(IPAddress.Loopback.ToString(), defaultGame.ConsolePort))
            {
                log.Error("Failed connecting to the proxy console");
                return;
            }

            if (m_Context.Console.SendCommand("gameconnected") != "true")
            {
                log.Message("Proxy isn't connected to the game yet");
                return;
            }

            if (!m_ConsoleOnly)
            {
                // temp hack
                m_Context.ProxyConnection.RemotePort = defaultGame.Port;

                ProxyLock proxyLock = null;
                try
                {
                    proxyLock = m_Context.ProxyConnection.ConnectToProxy(60000);
                    log.Message("Connected to proxy");
                }
                catch (Exception ex)
                {
                    log.ToolException(ex, "Exception received when trying to connect to proxy.");
                    m_Context.ProxyConnection.DisconnectFromProxy();
                    return;
                }

                m_RagPipeName = new PipeID("pipe_rag", IPAddress.Loopback, proxyLock.Port);
                proxyLock.Release();

                bool success = true;

                while (!m_StoppingPipe)
                {
                    bool result = m_RagPipe.Create(m_RagPipeName, true);
                    if (!result)
                    {
                        log.Error("Could not connect to pipe '{0}'!", m_RagPipeName.ToString());
                        success = false;
                        break;
                    }
                    else if (!m_StoppingPipe)
                    {
                        log.Message("Connected to proxy (again)");
                        ProxyHandshake.HandshakeResult handshake = PerformProxyHandshake(m_RagPipe, log);
                        if (handshake != null)
                        {
                            log.Message("Proxy handshake completed successfully");

                            // Handshake is done, but wait until Proxy is completely initialised
                            // before creating assets out of banks/widgets.
                            string command = "getProxyStatus " + m_Context.ProxyConnection.ProxyUID;
                            while (!m_StoppingPipe)
                            {
                                string proxyStatus = m_Context.Console.SendCommand(command);
                                if (proxyStatus == "ReadyIdle" || proxyStatus == "ReadyWorking")
                                {
                                    log.Message("Proxy initialised, setting up widget tree...");
                                    break;
                                }
                                else if (proxyStatus.Contains("Error"))
                                {
                                    log.Error("Proxy status returned error: {0}", proxyStatus);
                                    success = false;
                                    break;
                                }
                                else if (proxyStatus != "Initialising")
                                {
                                    log.Error("Proxy status returned unexpected value: {0}", proxyStatus);
                                    success = false;
                                    break;
                                }

                                Thread.Sleep(100);
                            }

                            break;
                        }

                        break;
                    }
                }

                m_RagPipe.Close();

                if (!success)
                {
                    log.Message("An error occurred while connecting to the Proxy, resetting.");
                    m_Context.Console.DisconnectConsole();
                }
            }
        }

        public static ProxyHandshake.HandshakeResult PerformProxyHandshake(INamedPipe pipe, ILog log)
        {
            IPAddress address = IPAddress.Parse(pipe.Socket.LocalEndPoint.ToString().Split(':')[0]);

            bool timedOut;
            ProxyHandshake.HandshakeResult result = new ProxyHandshake.HandshakeResult();


            int basePort = 60000;
            result.ReservedSockets = new List<TcpListener>();
            result.PortNumber = RagUtils.FindAvailableSockets(basePort, 10, result.ReservedSockets, LogFactory.ApplicationLog);

            result.PipeNameBank = RagUtils.GetRagPipeSocket(result.PortNumber, (int)PipeID.EnumSocketOffset.SOCKET_OFFSET_BANK, result.ReservedSockets);
            result.PipeNameOutput = RagUtils.GetRagPipeSocket(result.PortNumber, (int)PipeID.EnumSocketOffset.SOCKET_OFFSET_OUTPUT, result.ReservedSockets);
            result.PipeNameEvents = RagUtils.GetRagPipeSocket(result.PortNumber, (int)PipeID.EnumSocketOffset.SOCKET_OFFSET_EVENTS, result.ReservedSockets);

            log.Message("Connecting bank and output pipes...");
            log.Message("Bank: {0}", result.PipeNameBank.ToString());
            log.Message("Output: {0}", result.PipeNameOutput.ToString());

            RagUtils.ConnectToPipe(result.PipeNameBank, m_Context.PipeBank, LogFactory.ApplicationLog);
            RagUtils.ConnectToPipe(result.PipeNameOutput, m_Context.PipeOutput, LogFactory.ApplicationLog);
            RagUtils.ConnectToPipe(result.PipeNameEvents, m_Context.PipeEvents, LogFactory.ApplicationLog);

            log.Message("Waiting for pipes to start...");
            for (int i = 0; i < 1000; ++i)
            {
                Thread.Sleep(10);
                if (((NamedPipeSocket)m_Context.PipeBank).IsListeningForConnections() &&
                     ((NamedPipeSocket)m_Context.PipeOutput).IsListeningForConnections())
                {
                    log.Message("Pipes listening");
                    break;
                }
            }

            log.Message("Performing handshake...");
            for (int i = 0; i < 1; i++)
            {
                if (!ProxyHandshake.DoHandshake(pipe, log, out timedOut, ref result))
                {
                    log.Error("Handshake with Proxy failed");
                    if (timedOut)
                    {
                        log.Error("Rag timed out waiting for the application startup data.", "Connection Timed Out");
                    }
                    return null;
                }

            }

            // send the base socket and version numbers
            log.Message("Sending port number...");
            RemotePacket p = new RemotePacket(pipe);
            p.BeginWrite();
            p.Write_s32(result.PortNumber);
            p.Write_float(ProxyHandshake.RAG_VERSION);
            p.Send();

            // Wait for Rag Proxy to connect to the pipes
            while (true)
            {
                if (m_Context.PipeBank.IsConnected() &&
                    m_Context.PipeOutput.IsConnected())
                {
                    log.Message("Pipes connected");
                    break;
                }

                if (!((NamedPipeSocket)m_Context.PipeBank).IsListeningForConnections() ||
                     !((NamedPipeSocket)m_Context.PipeOutput).IsListeningForConnections())
                {
                    log.Message("Pipes stopped");
                    m_Context.PipeBank.CancelCreate();
                    m_Context.PipeOutput.CancelCreate();
                    return null;
                }
            }

            m_PipeUpdateThread = new Thread(() => ReadBankPipe(log));
            m_PipeUpdateThread.Name = "WidgetBrowser BankPipe thread";
            m_PipeUpdateThread.IsBackground = true;
            m_PipeUpdateThread.Start();

            m_OutputUpdateThread = new Thread(() => ReadOutputPipe(log));
            m_OutputUpdateThread.Name = "WidgetBrowser OutputPipe thread";
            m_OutputUpdateThread.IsBackground = true;
            m_OutputUpdateThread.Start();

            // Send input window handle
            BankRemotePacket packet = new BankRemotePacket(m_Context.PipeBank);
            packet.Begin(BankRemotePacket.EnumPacketType.USER_3, Widget.ComputeGUID('b', 'm', 'g', 'r'), 0);
            packet.Write_u32(12345); // todo send an actual window's handle
            packet.Send();

            log.Message("Handshake on pipe {0} done.", pipe.Name);
            log.Message("Establishing connection with client: {0} {1}", result.ExeName, result.Args);

            return result;
        }

        private static void ReadBankPipe(ILog log)
        {
            while (m_Context.PipeBank.IsConnected())
            {
                m_PacketProcessor.Update(m_Context.PipeBank);
                m_PacketProcessor.Process(m_Context.PipeBank);
            }

            log.Debug("Listening to bank pipe END");
        }

        private static void ReadOutputPipe(ILog log)
        {
            while (m_Context.PipeOutput.IsConnected())
            {
                string currentLine = string.Empty;
                while (m_Context.PipeOutput.HasData())
                {
                    const int BUFFER_SIZE = 4 * 1024;
                    Byte[] chars = new Byte[BUFFER_SIZE];
                    int numRead = m_Context.PipeOutput.ReadData(chars, BUFFER_SIZE, false);

                    for (int i = 0; i < numRead; i++)
                    {
                        Char charRead = Convert.ToChar(chars[i]);
                        if (charRead == 0)
                        {
                            currentLine = string.Empty;
                        }
                        else
                        {
                            currentLine += charRead;
                        }
                    }
                }
            }

            log.Debug("Listening to output pipe END");
        }

    }
}
