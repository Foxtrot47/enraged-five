using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Text;
using System.Threading;

using ragCore;

using ragWidgets;
using RSG.Base.Logging;

namespace ragClient
{

	/// <summary>
	/// Summary description for BankManager.
	/// </summary>
	public class BankManager : IBankManager
	{
		public BankManager()
		{
            if (InitFinished != null) InitFinished();  //HACK: To avoid unused InitFinished variable warning, but necessary to implement the IBankManager interface.
		}

        public static uint m_id;
		public delegate void TimeStringDelegate(String str);
		public event ragCore.InitFinishedHandler InitFinished;
		public static event TimeStringDelegate TimeStringHandler;

		public static int GetWidgetTypeGUID() {return GetStaticGUID();}
		public static int GetStaticGUID()		{return Widget.ComputeGUID('b','m','g','r');}


        public static void AddHandlers(BankRemotePacketProcessor packetProcessor)
        {
            packetProcessor.AddType( GetStaticGUID(), new BankRemotePacketProcessor.HandlerDel( RemoteHandler ) );
        }

        private static bool RemoteHandler( BankRemotePacket packet, out String errorMessage )
        {
            bool handled = false;
            switch ( packet.BankCommand )
            {
                // see if we got a confirmation that the application got the quit message:
                case BankRemotePacket.EnumPacketType.USER_4:
                    {
                        LogFactory.ApplicationLog.MessageCtx("RAG", "Got quit message from game");
                        handled = true;
                    }
                    break;

                // see if its timer information:
                case BankRemotePacket.EnumPacketType.USER_5:
                    {
                        packet.Begin();

                        // This string contains current frame times and stuff for the game
                        String timeString = packet.Read_const_char();
                        if ( TimeStringHandler != null )
                            TimeStringHandler( timeString );

                        packet.End();
                        handled = true;
                    }
                    break;

                case BankRemotePacket.EnumPacketType.CREATE:
                    m_id = packet.Id;
                    handled = true;
                    break;
            }

            errorMessage = null;
            return handled;
        }

        public void Reset()
        {
            OnPingResponse();
        }

		protected void OnPingResponse() 
		{
			if (m_PingResponse != null) 
			{
				// m_PingResponse (the member variable) needs to be null before the
				// event is signaled, so that we can add a new ping request from that event.
				EventHandler pingEvent = m_PingResponse;
				m_PingResponse = null;
				pingEvent(this, System.EventArgs.Empty);
			}
		}

		private event EventHandler m_PingResponse;

        public void SendDelayedPing(EventHandler callback, BankPipe pipe)
		{
			// What we should really do here is see if there is already a ping response handler. If so we should
			// add a new handler that sends this as a second (or Nth) ping and then responds.
			// For now we just punt and have the new handler called for the old message.

			if (m_PingResponse == null) 
			{
				BankRemotePacket p = new BankRemotePacket(pipe);
                p.Begin(BankRemotePacket.EnumPacketType.USER_6, GetWidgetTypeGUID(), m_id);
				p.Write_u32(0);
				p.Write_bool(true);
				p.Send();
			}
			m_PingResponse += callback;
		}

        public void SendPing(EventHandler callback, BankPipe pipe)
        {
            // What we should really do here is see if there is already a ping response handler. If so we should
            // add a new handler that sends this as a second (or Nth) ping and then responds.
            // For now we just punt and have the new handler called for the old message.

            if (m_PingResponse == null)
            {
                BankRemotePacket p = new BankRemotePacket(pipe);
                p.Begin(BankRemotePacket.EnumPacketType.USER_6, GetWidgetTypeGUID(), m_id);
                p.Write_u32(0);
                p.Write_bool(false);
                p.Send();
            }
            m_PingResponse += callback;
        }

		public void AddBank(WidgetBank bank)
		{
			if (!m_Banks.Contains(bank))
				m_Banks.Add(bank);
		}

		public void RemoveBank(WidgetBank bank)
		{
			m_Banks.Remove(bank);
		}

        public List<Widget> AllBanks
		{
			get { return m_Banks; }
		}


		public Widget FindFirstWidgetFromPath(string path)
		{
			List<Widget> list = FindWidgetsFromPath(path);
			if (list == null || list.Count == 0) 
			{
				return null;
			}
			return list[0];
		}

		public List<Widget> FindWidgetsFromPath(string path)
		{
            if (path == "/")
            {
                return m_Banks;
            }
			return WidgetGroupBase.FindWidgetsFromPath(null, m_Banks, path);
		}

        public List<Widget> FindWidgetsFromPath( String[] path ) 
		{
            if (path.Length == 0)
            {
                return m_Banks;
            }
			return WidgetGroupBase.FindWidgetsFromPath(null, m_Banks, path, 0);
		}


        private List<Widget> m_Banks = new List<Widget>();
        private bool m_initFinished = false;

        public bool IsInitFinished
        {
            get
            {
                return m_initFinished;
            }
        }


        public Widget FindWidgetFromID( uint id )
        {
            throw new NotImplementedException();
        }
    }
}
