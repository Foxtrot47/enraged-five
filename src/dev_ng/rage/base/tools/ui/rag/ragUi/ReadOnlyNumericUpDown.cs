﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ragUi
{
    /// <summary>
    /// 
    /// </summary>
    public class ReadOnlyNumericUpDown : NumericUpDown
    {
        /// <summary>
        /// 
        /// </summary>
        private decimal _originalIncrement;

        /// <summary>
        /// Initialises a new instance of the <see cref="ReadOnlyNumericUpDown"/> class.
        /// </summary>
        public ReadOnlyNumericUpDown()
            : base()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        public new decimal Increment
        {
            get
            {
                return base.Increment;
            }
            set
            {
                if (base.ReadOnly)
                {
                    _originalIncrement = value;
                }
                else
                {
                    _originalIncrement = value;
                    base.Increment = value;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public new bool ReadOnly
        {
            get { return base.ReadOnly; }
            set
            {
                base.ReadOnly = value;
                if (value)
                {
                    base.Increment = 0;
                }
                else
                {
                    base.Increment = _originalIncrement;
                }
            }
        }
    }
}
