namespace ragUi
{
    partial class PromptMatrixForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && (components != null) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.okButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.valueControlMatrix = new ragUi.ControlMatrix();
            ((System.ComponentModel.ISupportInitialize)(this.valueControlMatrix)).BeginInit();
            this.SuspendLayout();
            // 
            // okButton
            // 
            this.okButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.okButton.Location = new System.Drawing.Point( 413, 168 );
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size( 75, 23 );
            this.okButton.TabIndex = 1;
            this.okButton.Text = "OK";
            this.okButton.UseVisualStyleBackColor = true;
            // 
            // cancelButton
            // 
            this.cancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.Location = new System.Drawing.Point( 494, 168 );
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size( 75, 23 );
            this.cancelButton.TabIndex = 2;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            // 
            // valueControlMatrix
            // 
            this.valueControlMatrix.AllowDropDownOptions = false;
            this.valueControlMatrix.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.valueControlMatrix.Location = new System.Drawing.Point( 12, 12 );
            this.valueControlMatrix.Matrix = new decimal[][] {
        new decimal[] {
                0,
                0,
                0,
                0},
        new decimal[] {
                0,
                0,
                0,
                0},
        new decimal[] {
                0,
                0,
                0,
                0},
        new decimal[] {
                0,
                0,
                0,
                0}};
            this.valueControlMatrix.Maximum = 100M;
            this.valueControlMatrix.Minimum = 0M;
            this.valueControlMatrix.Name = "valueControlMatrix";
            this.valueControlMatrix.NumColumns = 4;
            this.valueControlMatrix.NumRows = 4;
            this.valueControlMatrix.Size = new System.Drawing.Size( 562, 150 );
            this.valueControlMatrix.Step = 1M;
            this.valueControlMatrix.TabIndex = 3;
            this.valueControlMatrix.ValueXPath = null;
            // 
            // PromptMatrixForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size( 581, 203 );
            this.Controls.Add( this.valueControlMatrix );
            this.Controls.Add( this.cancelButton );
            this.Controls.Add( this.okButton );
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PromptMatrixForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Enter Matrix Value";
            ((System.ComponentModel.ISupportInitialize)(this.valueControlMatrix)).EndInit();
            this.ResumeLayout( false );

        }

        #endregion

        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.Button cancelButton;
        private ControlMatrix valueControlMatrix;
    }
}