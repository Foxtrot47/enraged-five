using System;

using GlacialComponents.Controls.GTLCommon;

namespace ragUi
{
	/// <summary>
	/// Summary description for EmbeddedTrackBar.
	/// </summary>
	public class EmbeddedTrackBar : System.Windows.Forms.TrackBar, GlacialComponents.Controls.GTLCommon.IGEmbeddedControl
	{
		public EmbeddedTrackBar()
		{
			//
			// TODO: Add constructor logic here
			//
			
			base.AutoSize = false;
			base.Minimum = 0;
			base.Maximum = 100;
			base.TickFrequency = 0;
			base.TickStyle = System.Windows.Forms.TickStyle.BottomRight;
		}

		private object m_Owner;
		private object m_UserData;

		public object Owner
		{ 
			get
			{
				return m_Owner;
			}
		}

		public object UserData
		{ 
			get
			{
				return m_UserData;
			}
		}

		public override string Text
		{ 
			get
			{
				float f = ((Max - Min) * (float)base.Value / 100.0f) + Min;
				
				if ( IsInteger )
				{
					int i = Convert.ToInt32( f );
					return i.ToString();
				}
				else
				{
					return f.ToString();
				}
			}
			set
			{
				float f = float.Parse( value );
				base.Value = Convert.ToInt32( (100.0f * (f - Min)) / (Max - Min) );
			}
		}

		public float Min;
		public float Max;
		public bool IsInteger;

		public bool Load( string text, object userData, object owner )
		{
			this.Text = text;
			m_UserData = userData;
			m_Owner = owner;

			return true;
		}
		
		public string Unload()
		{
			return this.Text;
		}
	}
}
