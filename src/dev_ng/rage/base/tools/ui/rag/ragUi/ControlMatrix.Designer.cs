namespace ragUi
{
    partial class ControlMatrix
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager( typeof( ControlMatrix ) );
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripDropDownButton1 = new System.Windows.Forms.ToolStripDropDownButton();
            this.basicOperationsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.identityToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.identity3x3ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.zeroToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.zero3x3ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.setDiagonalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.addToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.add3x3ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.subtractToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.subtract3x3ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.negateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.negate3x3ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.absToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.scaleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.scaleFullToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.makeScaleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.makeScaleFullToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.translateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.makeTranslateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.makePosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.trigonometryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rotationsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rotateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rotateXToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rotateYToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rotateZToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rotateUnitAxisToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rotateToToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.rotateLocalXToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rotateLocalYToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rotateLocalZToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rotateLocalAxisToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.makeRotateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.makeRotateXToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.makeRotXToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.makeRotateYToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.makeRotYToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.makeRotateZToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.makeRotZToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.makeRotateUnitAxisToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.makeRotateToToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.rotateFullToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rotateFullXToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rotateFullYToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rotateFullZToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rotateFullUnitAxisToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.normalizeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.normalizeSafeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.dotToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dot3x3ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dotFromLeftToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dot3x3FromLeftToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dotTransposeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dot3x3TransposeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.crossProductToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.outerProductToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.inverseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.inverse3x3ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fastInverseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.transposeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.transpose3x4ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.advancedOperationsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.makeUprightToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.makeDoubleCrossMatrixToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.makePosRotYToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.makeReflectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator9 = new System.Windows.Forms.ToolStripSeparator();
            this.dotCrossProdMtxToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dot3x3CrossProdMtxToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dotCrossProdTransposeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dot3x3CrossProdTransposeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator10 = new System.Windows.Forms.ToolStripSeparator();
            this.lookDownToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lookAtToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.polarViewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator11 = new System.Windows.Forms.ToolStripSeparator();
            this.coordinateInverseSafeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mirrorOnPlaneToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.interpolateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.row1ControlVector = new ragUi.ControlVector();
            this.row2ControlVector = new ragUi.ControlVector();
            this.row3ControlVector = new ragUi.ControlVector();
            this.row4ControlVector = new ragUi.ControlVector();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.row1ControlVector)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.row2ControlVector)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.row3ControlVector)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.row4ControlVector)).BeginInit();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.Items.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.toolStripDropDownButton1} );
            this.toolStrip1.Location = new System.Drawing.Point( 0, 0 );
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size( 562, 25 );
            this.toolStrip1.TabIndex = 4;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripDropDownButton1
            // 
            this.toolStripDropDownButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripDropDownButton1.DropDownItems.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.basicOperationsToolStripMenuItem,
            this.trigonometryToolStripMenuItem,
            this.advancedOperationsToolStripMenuItem} );
            this.toolStripDropDownButton1.Image = ((System.Drawing.Image)(resources.GetObject( "toolStripDropDownButton1.Image" )));
            this.toolStripDropDownButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton1.Name = "toolStripDropDownButton1";
            this.toolStripDropDownButton1.ShowDropDownArrow = false;
            this.toolStripDropDownButton1.Size = new System.Drawing.Size( 139, 22 );
            this.toolStripDropDownButton1.Text = "toolStripDropDownButton1";
            // 
            // basicOperationsToolStripMenuItem
            // 
            this.basicOperationsToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.basicOperationsToolStripMenuItem.DropDownItems.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.identityToolStripMenuItem,
            this.identity3x3ToolStripMenuItem,
            this.zeroToolStripMenuItem,
            this.zero3x3ToolStripMenuItem,
            this.setDiagonalToolStripMenuItem,
            this.toolStripSeparator1,
            this.addToolStripMenuItem,
            this.add3x3ToolStripMenuItem,
            this.subtractToolStripMenuItem,
            this.subtract3x3ToolStripMenuItem,
            this.negateToolStripMenuItem,
            this.negate3x3ToolStripMenuItem,
            this.toolStripSeparator2,
            this.absToolStripMenuItem,
            this.scaleToolStripMenuItem,
            this.scaleFullToolStripMenuItem,
            this.makeScaleToolStripMenuItem,
            this.makeScaleFullToolStripMenuItem,
            this.translateToolStripMenuItem,
            this.makeTranslateToolStripMenuItem,
            this.makePosToolStripMenuItem} );
            this.basicOperationsToolStripMenuItem.Name = "basicOperationsToolStripMenuItem";
            this.basicOperationsToolStripMenuItem.Size = new System.Drawing.Size( 189, 22 );
            this.basicOperationsToolStripMenuItem.Text = "&Basic Operations";
            // 
            // identityToolStripMenuItem
            // 
            this.identityToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.identityToolStripMenuItem.Name = "identityToolStripMenuItem";
            this.identityToolStripMenuItem.Size = new System.Drawing.Size( 155, 22 );
            this.identityToolStripMenuItem.Text = "&Identity";
            // 
            // identity3x3ToolStripMenuItem
            // 
            this.identity3x3ToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.identity3x3ToolStripMenuItem.Name = "identity3x3ToolStripMenuItem";
            this.identity3x3ToolStripMenuItem.Size = new System.Drawing.Size( 155, 22 );
            this.identity3x3ToolStripMenuItem.Text = "Identity&3x3";
            // 
            // zeroToolStripMenuItem
            // 
            this.zeroToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.zeroToolStripMenuItem.Name = "zeroToolStripMenuItem";
            this.zeroToolStripMenuItem.Size = new System.Drawing.Size( 155, 22 );
            this.zeroToolStripMenuItem.Text = "&Zero";
            // 
            // zero3x3ToolStripMenuItem
            // 
            this.zero3x3ToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.zero3x3ToolStripMenuItem.Name = "zero3x3ToolStripMenuItem";
            this.zero3x3ToolStripMenuItem.Size = new System.Drawing.Size( 155, 22 );
            this.zero3x3ToolStripMenuItem.Text = "Zer&o3x3";
            // 
            // setDiagonalToolStripMenuItem
            // 
            this.setDiagonalToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.setDiagonalToolStripMenuItem.Name = "setDiagonalToolStripMenuItem";
            this.setDiagonalToolStripMenuItem.Size = new System.Drawing.Size( 155, 22 );
            this.setDiagonalToolStripMenuItem.Text = "Set &Diagonal";
            this.setDiagonalToolStripMenuItem.Click += new System.EventHandler( this.PromptVector_ToolStripMenuItem_Click );
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size( 152, 6 );
            // 
            // addToolStripMenuItem
            // 
            this.addToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.addToolStripMenuItem.Name = "addToolStripMenuItem";
            this.addToolStripMenuItem.Size = new System.Drawing.Size( 155, 22 );
            this.addToolStripMenuItem.Text = "&Add";
            this.addToolStripMenuItem.Click += new System.EventHandler( this.PromptMatrix_ToolStripMenuItem_Click );
            // 
            // add3x3ToolStripMenuItem
            // 
            this.add3x3ToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.add3x3ToolStripMenuItem.Name = "add3x3ToolStripMenuItem";
            this.add3x3ToolStripMenuItem.Size = new System.Drawing.Size( 155, 22 );
            this.add3x3ToolStripMenuItem.Text = "A&dd3&x3";
            this.add3x3ToolStripMenuItem.Click += new System.EventHandler( this.PromptMatrix_ToolStripMenuItem_Click );
            // 
            // subtractToolStripMenuItem
            // 
            this.subtractToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.subtractToolStripMenuItem.Name = "subtractToolStripMenuItem";
            this.subtractToolStripMenuItem.Size = new System.Drawing.Size( 155, 22 );
            this.subtractToolStripMenuItem.Text = "&Subtract";
            this.subtractToolStripMenuItem.Click += new System.EventHandler( this.PromptMatrix_ToolStripMenuItem_Click );
            // 
            // subtract3x3ToolStripMenuItem
            // 
            this.subtract3x3ToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.subtract3x3ToolStripMenuItem.Name = "subtract3x3ToolStripMenuItem";
            this.subtract3x3ToolStripMenuItem.Size = new System.Drawing.Size( 155, 22 );
            this.subtract3x3ToolStripMenuItem.Text = "Subt&ract3x3";
            this.subtract3x3ToolStripMenuItem.Click += new System.EventHandler( this.PromptMatrix_ToolStripMenuItem_Click );
            // 
            // negateToolStripMenuItem
            // 
            this.negateToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.negateToolStripMenuItem.Name = "negateToolStripMenuItem";
            this.negateToolStripMenuItem.Size = new System.Drawing.Size( 155, 22 );
            this.negateToolStripMenuItem.Text = "&Negate";
            // 
            // negate3x3ToolStripMenuItem
            // 
            this.negate3x3ToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.negate3x3ToolStripMenuItem.Name = "negate3x3ToolStripMenuItem";
            this.negate3x3ToolStripMenuItem.Size = new System.Drawing.Size( 155, 22 );
            this.negate3x3ToolStripMenuItem.Text = "N&egate3x3";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size( 152, 6 );
            // 
            // absToolStripMenuItem
            // 
            this.absToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.absToolStripMenuItem.Name = "absToolStripMenuItem";
            this.absToolStripMenuItem.Size = new System.Drawing.Size( 155, 22 );
            this.absToolStripMenuItem.Text = "A&bs";
            // 
            // scaleToolStripMenuItem
            // 
            this.scaleToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.scaleToolStripMenuItem.Name = "scaleToolStripMenuItem";
            this.scaleToolStripMenuItem.Size = new System.Drawing.Size( 155, 22 );
            this.scaleToolStripMenuItem.Text = "S&cale";
            this.scaleToolStripMenuItem.Click += new System.EventHandler( this.PromptFloat_ToolStripMenuItem_Click );
            // 
            // scaleFullToolStripMenuItem
            // 
            this.scaleFullToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.scaleFullToolStripMenuItem.Name = "scaleFullToolStripMenuItem";
            this.scaleFullToolStripMenuItem.Size = new System.Drawing.Size( 155, 22 );
            this.scaleFullToolStripMenuItem.Text = "Sca&leFull";
            this.scaleFullToolStripMenuItem.Click += new System.EventHandler( this.PromptFloat_ToolStripMenuItem_Click );
            // 
            // makeScaleToolStripMenuItem
            // 
            this.makeScaleToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.makeScaleToolStripMenuItem.Name = "makeScaleToolStripMenuItem";
            this.makeScaleToolStripMenuItem.Size = new System.Drawing.Size( 155, 22 );
            this.makeScaleToolStripMenuItem.Text = "&MakeScale";
            this.makeScaleToolStripMenuItem.Click += new System.EventHandler( this.PromptFloat_ToolStripMenuItem_Click );
            // 
            // makeScaleFullToolStripMenuItem
            // 
            this.makeScaleFullToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.makeScaleFullToolStripMenuItem.Name = "makeScaleFullToolStripMenuItem";
            this.makeScaleFullToolStripMenuItem.Size = new System.Drawing.Size( 155, 22 );
            this.makeScaleFullToolStripMenuItem.Text = "MakeScaleF&ull";
            this.makeScaleFullToolStripMenuItem.Click += new System.EventHandler( this.PromptFloat_ToolStripMenuItem_Click );
            // 
            // translateToolStripMenuItem
            // 
            this.translateToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.translateToolStripMenuItem.Name = "translateToolStripMenuItem";
            this.translateToolStripMenuItem.Size = new System.Drawing.Size( 155, 22 );
            this.translateToolStripMenuItem.Text = "&Translate";
            this.translateToolStripMenuItem.Click += new System.EventHandler( this.PromptVector_ToolStripMenuItem_Click );
            // 
            // makeTranslateToolStripMenuItem
            // 
            this.makeTranslateToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.makeTranslateToolStripMenuItem.Name = "makeTranslateToolStripMenuItem";
            this.makeTranslateToolStripMenuItem.Size = new System.Drawing.Size( 155, 22 );
            this.makeTranslateToolStripMenuItem.Text = "Ma&keTranslate";
            this.makeTranslateToolStripMenuItem.Click += new System.EventHandler( this.PromptVector_ToolStripMenuItem_Click );
            // 
            // makePosToolStripMenuItem
            // 
            this.makePosToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.makePosToolStripMenuItem.Name = "makePosToolStripMenuItem";
            this.makePosToolStripMenuItem.Size = new System.Drawing.Size( 155, 22 );
            this.makePosToolStripMenuItem.Text = "Make&Pos";
            this.makePosToolStripMenuItem.Click += new System.EventHandler( this.PromptFloatFloatFloat_ToolStripMenuItem_Click );
            // 
            // trigonometryToolStripMenuItem
            // 
            this.trigonometryToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.trigonometryToolStripMenuItem.DropDownItems.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.rotationsToolStripMenuItem,
            this.toolStripSeparator3,
            this.normalizeToolStripMenuItem,
            this.normalizeSafeToolStripMenuItem,
            this.toolStripSeparator4,
            this.dotToolStripMenuItem,
            this.dot3x3ToolStripMenuItem,
            this.dotFromLeftToolStripMenuItem,
            this.dot3x3FromLeftToolStripMenuItem,
            this.dotTransposeToolStripMenuItem,
            this.dot3x3TransposeToolStripMenuItem,
            this.crossProductToolStripMenuItem,
            this.outerProductToolStripMenuItem,
            this.toolStripSeparator5,
            this.inverseToolStripMenuItem,
            this.inverse3x3ToolStripMenuItem,
            this.fastInverseToolStripMenuItem,
            this.transposeToolStripMenuItem,
            this.transpose3x4ToolStripMenuItem} );
            this.trigonometryToolStripMenuItem.Name = "trigonometryToolStripMenuItem";
            this.trigonometryToolStripMenuItem.Size = new System.Drawing.Size( 189, 22 );
            this.trigonometryToolStripMenuItem.Text = "&Trigonometry";
            // 
            // rotationsToolStripMenuItem
            // 
            this.rotationsToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.rotationsToolStripMenuItem.DropDownItems.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.rotateToolStripMenuItem,
            this.rotateXToolStripMenuItem,
            this.rotateYToolStripMenuItem,
            this.rotateZToolStripMenuItem,
            this.rotateUnitAxisToolStripMenuItem,
            this.rotateToToolStripMenuItem,
            this.toolStripSeparator6,
            this.rotateLocalXToolStripMenuItem,
            this.rotateLocalYToolStripMenuItem,
            this.rotateLocalZToolStripMenuItem,
            this.rotateLocalAxisToolStripMenuItem,
            this.toolStripSeparator7,
            this.makeRotateToolStripMenuItem,
            this.makeRotateXToolStripMenuItem,
            this.makeRotXToolStripMenuItem,
            this.makeRotateYToolStripMenuItem,
            this.makeRotYToolStripMenuItem,
            this.makeRotateZToolStripMenuItem,
            this.makeRotZToolStripMenuItem,
            this.makeRotateUnitAxisToolStripMenuItem,
            this.makeRotateToToolStripMenuItem,
            this.toolStripSeparator8,
            this.rotateFullToolStripMenuItem,
            this.rotateFullXToolStripMenuItem,
            this.rotateFullYToolStripMenuItem,
            this.rotateFullZToolStripMenuItem,
            this.rotateFullUnitAxisToolStripMenuItem} );
            this.rotationsToolStripMenuItem.Name = "rotationsToolStripMenuItem";
            this.rotationsToolStripMenuItem.Size = new System.Drawing.Size( 170, 22 );
            this.rotationsToolStripMenuItem.Text = "&Rotations";
            // 
            // rotateToolStripMenuItem
            // 
            this.rotateToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.rotateToolStripMenuItem.Name = "rotateToolStripMenuItem";
            this.rotateToolStripMenuItem.Size = new System.Drawing.Size( 182, 22 );
            this.rotateToolStripMenuItem.Text = "&Rotate";
            this.rotateToolStripMenuItem.Click += new System.EventHandler( this.PromptVectorFloat_ToolStripMenuItem_Click );
            // 
            // rotateXToolStripMenuItem
            // 
            this.rotateXToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.rotateXToolStripMenuItem.Name = "rotateXToolStripMenuItem";
            this.rotateXToolStripMenuItem.Size = new System.Drawing.Size( 182, 22 );
            this.rotateXToolStripMenuItem.Text = "Rotate&X";
            this.rotateXToolStripMenuItem.Click += new System.EventHandler( this.PromptFloat_ToolStripMenuItem_Click );
            // 
            // rotateYToolStripMenuItem
            // 
            this.rotateYToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.rotateYToolStripMenuItem.Name = "rotateYToolStripMenuItem";
            this.rotateYToolStripMenuItem.Size = new System.Drawing.Size( 182, 22 );
            this.rotateYToolStripMenuItem.Text = "Rotate&Y";
            this.rotateYToolStripMenuItem.Click += new System.EventHandler( this.PromptFloat_ToolStripMenuItem_Click );
            // 
            // rotateZToolStripMenuItem
            // 
            this.rotateZToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.rotateZToolStripMenuItem.Name = "rotateZToolStripMenuItem";
            this.rotateZToolStripMenuItem.Size = new System.Drawing.Size( 182, 22 );
            this.rotateZToolStripMenuItem.Text = "Rotate&Z";
            this.rotateZToolStripMenuItem.Click += new System.EventHandler( this.PromptFloat_ToolStripMenuItem_Click );
            // 
            // rotateUnitAxisToolStripMenuItem
            // 
            this.rotateUnitAxisToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.rotateUnitAxisToolStripMenuItem.Name = "rotateUnitAxisToolStripMenuItem";
            this.rotateUnitAxisToolStripMenuItem.Size = new System.Drawing.Size( 182, 22 );
            this.rotateUnitAxisToolStripMenuItem.Text = "Rotate&UnitAxis";
            this.rotateUnitAxisToolStripMenuItem.Click += new System.EventHandler( this.PromptVectorFloat_ToolStripMenuItem_Click );
            // 
            // rotateToToolStripMenuItem
            // 
            this.rotateToToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.rotateToToolStripMenuItem.Name = "rotateToToolStripMenuItem";
            this.rotateToToolStripMenuItem.Size = new System.Drawing.Size( 182, 22 );
            this.rotateToToolStripMenuItem.Text = "Rotate&To";
            this.rotateToToolStripMenuItem.Click += new System.EventHandler( this.PromptVectorVectorFloat_ToolStripMenuItem_Click );
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size( 179, 6 );
            // 
            // rotateLocalXToolStripMenuItem
            // 
            this.rotateLocalXToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.rotateLocalXToolStripMenuItem.Name = "rotateLocalXToolStripMenuItem";
            this.rotateLocalXToolStripMenuItem.Size = new System.Drawing.Size( 182, 22 );
            this.rotateLocalXToolStripMenuItem.Text = "Rotate&LocalX";
            this.rotateLocalXToolStripMenuItem.Click += new System.EventHandler( this.PromptFloat_ToolStripMenuItem_Click );
            // 
            // rotateLocalYToolStripMenuItem
            // 
            this.rotateLocalYToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.rotateLocalYToolStripMenuItem.Name = "rotateLocalYToolStripMenuItem";
            this.rotateLocalYToolStripMenuItem.Size = new System.Drawing.Size( 182, 22 );
            this.rotateLocalYToolStripMenuItem.Text = "RotateL&ocalY";
            this.rotateLocalYToolStripMenuItem.Click += new System.EventHandler( this.PromptFloat_ToolStripMenuItem_Click );
            // 
            // rotateLocalZToolStripMenuItem
            // 
            this.rotateLocalZToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.rotateLocalZToolStripMenuItem.Name = "rotateLocalZToolStripMenuItem";
            this.rotateLocalZToolStripMenuItem.Size = new System.Drawing.Size( 182, 22 );
            this.rotateLocalZToolStripMenuItem.Text = "RotateLo&calZ";
            this.rotateLocalZToolStripMenuItem.Click += new System.EventHandler( this.PromptFloat_ToolStripMenuItem_Click );
            // 
            // rotateLocalAxisToolStripMenuItem
            // 
            this.rotateLocalAxisToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.rotateLocalAxisToolStripMenuItem.Name = "rotateLocalAxisToolStripMenuItem";
            this.rotateLocalAxisToolStripMenuItem.Size = new System.Drawing.Size( 182, 22 );
            this.rotateLocalAxisToolStripMenuItem.Text = "RotateLoc&alAxis";
            this.rotateLocalAxisToolStripMenuItem.Click += new System.EventHandler( this.PromptFloatInt_ToolStripMenuItem_Click );
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size( 179, 6 );
            // 
            // makeRotateToolStripMenuItem
            // 
            this.makeRotateToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.makeRotateToolStripMenuItem.Name = "makeRotateToolStripMenuItem";
            this.makeRotateToolStripMenuItem.Size = new System.Drawing.Size( 182, 22 );
            this.makeRotateToolStripMenuItem.Text = "&MakeRotate";
            this.makeRotateToolStripMenuItem.Click += new System.EventHandler( this.PromptVectorFloat_ToolStripMenuItem_Click );
            // 
            // makeRotateXToolStripMenuItem
            // 
            this.makeRotateXToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.makeRotateXToolStripMenuItem.Name = "makeRotateXToolStripMenuItem";
            this.makeRotateXToolStripMenuItem.Size = new System.Drawing.Size( 182, 22 );
            this.makeRotateXToolStripMenuItem.Text = "Ma&keRotateX";
            this.makeRotateXToolStripMenuItem.Click += new System.EventHandler( this.PromptFloat_ToolStripMenuItem_Click );
            // 
            // makeRotXToolStripMenuItem
            // 
            this.makeRotXToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.makeRotXToolStripMenuItem.Name = "makeRotXToolStripMenuItem";
            this.makeRotXToolStripMenuItem.Size = new System.Drawing.Size( 182, 22 );
            this.makeRotXToolStripMenuItem.Text = "MakeRotX";
            this.makeRotXToolStripMenuItem.Click += new System.EventHandler( this.PromptFloat_ToolStripMenuItem_Click );
            // 
            // makeRotateYToolStripMenuItem
            // 
            this.makeRotateYToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.makeRotateYToolStripMenuItem.Name = "makeRotateYToolStripMenuItem";
            this.makeRotateYToolStripMenuItem.Size = new System.Drawing.Size( 182, 22 );
            this.makeRotateYToolStripMenuItem.Text = "Mak&eRotateY";
            this.makeRotateYToolStripMenuItem.Click += new System.EventHandler( this.PromptFloat_ToolStripMenuItem_Click );
            // 
            // makeRotYToolStripMenuItem
            // 
            this.makeRotYToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.makeRotYToolStripMenuItem.Name = "makeRotYToolStripMenuItem";
            this.makeRotYToolStripMenuItem.Size = new System.Drawing.Size( 182, 22 );
            this.makeRotYToolStripMenuItem.Text = "MakeRotY";
            this.makeRotYToolStripMenuItem.Click += new System.EventHandler( this.PromptFloat_ToolStripMenuItem_Click );
            // 
            // makeRotateZToolStripMenuItem
            // 
            this.makeRotateZToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.makeRotateZToolStripMenuItem.Name = "makeRotateZToolStripMenuItem";
            this.makeRotateZToolStripMenuItem.Size = new System.Drawing.Size( 182, 22 );
            this.makeRotateZToolStripMenuItem.Text = "MakeR&otateZ";
            this.makeRotateZToolStripMenuItem.Click += new System.EventHandler( this.PromptFloat_ToolStripMenuItem_Click );
            // 
            // makeRotZToolStripMenuItem
            // 
            this.makeRotZToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.makeRotZToolStripMenuItem.Name = "makeRotZToolStripMenuItem";
            this.makeRotZToolStripMenuItem.Size = new System.Drawing.Size( 182, 22 );
            this.makeRotZToolStripMenuItem.Text = "MakeRotZ";
            this.makeRotZToolStripMenuItem.Click += new System.EventHandler( this.PromptFloat_ToolStripMenuItem_Click );
            // 
            // makeRotateUnitAxisToolStripMenuItem
            // 
            this.makeRotateUnitAxisToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.makeRotateUnitAxisToolStripMenuItem.Name = "makeRotateUnitAxisToolStripMenuItem";
            this.makeRotateUnitAxisToolStripMenuItem.Size = new System.Drawing.Size( 182, 22 );
            this.makeRotateUnitAxisToolStripMenuItem.Text = "MakeRotateU&nitAxis";
            this.makeRotateUnitAxisToolStripMenuItem.Click += new System.EventHandler( this.PromptVectorFloat_ToolStripMenuItem_Click );
            // 
            // makeRotateToToolStripMenuItem
            // 
            this.makeRotateToToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.makeRotateToToolStripMenuItem.Name = "makeRotateToToolStripMenuItem";
            this.makeRotateToToolStripMenuItem.Size = new System.Drawing.Size( 182, 22 );
            this.makeRotateToToolStripMenuItem.Text = "MakeRotateTo";
            this.makeRotateToToolStripMenuItem.Click += new System.EventHandler( this.PromptVectorVector_ToolStripMenuItem_Click );
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size( 179, 6 );
            // 
            // rotateFullToolStripMenuItem
            // 
            this.rotateFullToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.rotateFullToolStripMenuItem.Name = "rotateFullToolStripMenuItem";
            this.rotateFullToolStripMenuItem.Size = new System.Drawing.Size( 182, 22 );
            this.rotateFullToolStripMenuItem.Text = "Rotate&Full";
            this.rotateFullToolStripMenuItem.Click += new System.EventHandler( this.PromptVectorFloat_ToolStripMenuItem_Click );
            // 
            // rotateFullXToolStripMenuItem
            // 
            this.rotateFullXToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.rotateFullXToolStripMenuItem.Name = "rotateFullXToolStripMenuItem";
            this.rotateFullXToolStripMenuItem.Size = new System.Drawing.Size( 182, 22 );
            this.rotateFullXToolStripMenuItem.Text = "RotateFullX";
            this.rotateFullXToolStripMenuItem.Click += new System.EventHandler( this.PromptFloat_ToolStripMenuItem_Click );
            // 
            // rotateFullYToolStripMenuItem
            // 
            this.rotateFullYToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.rotateFullYToolStripMenuItem.Name = "rotateFullYToolStripMenuItem";
            this.rotateFullYToolStripMenuItem.Size = new System.Drawing.Size( 182, 22 );
            this.rotateFullYToolStripMenuItem.Text = "RotateFullY";
            this.rotateFullYToolStripMenuItem.Click += new System.EventHandler( this.PromptFloat_ToolStripMenuItem_Click );
            // 
            // rotateFullZToolStripMenuItem
            // 
            this.rotateFullZToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.rotateFullZToolStripMenuItem.Name = "rotateFullZToolStripMenuItem";
            this.rotateFullZToolStripMenuItem.Size = new System.Drawing.Size( 182, 22 );
            this.rotateFullZToolStripMenuItem.Text = "RotateFullZ";
            this.rotateFullZToolStripMenuItem.Click += new System.EventHandler( this.PromptFloat_ToolStripMenuItem_Click );
            // 
            // rotateFullUnitAxisToolStripMenuItem
            // 
            this.rotateFullUnitAxisToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.rotateFullUnitAxisToolStripMenuItem.Name = "rotateFullUnitAxisToolStripMenuItem";
            this.rotateFullUnitAxisToolStripMenuItem.Size = new System.Drawing.Size( 182, 22 );
            this.rotateFullUnitAxisToolStripMenuItem.Text = "RotateFullUnitAxis";
            this.rotateFullUnitAxisToolStripMenuItem.Click += new System.EventHandler( this.PromptVectorFloat_ToolStripMenuItem_Click );
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size( 167, 6 );
            // 
            // normalizeToolStripMenuItem
            // 
            this.normalizeToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.normalizeToolStripMenuItem.Name = "normalizeToolStripMenuItem";
            this.normalizeToolStripMenuItem.Size = new System.Drawing.Size( 170, 22 );
            this.normalizeToolStripMenuItem.Text = "&Normalize";
            // 
            // normalizeSafeToolStripMenuItem
            // 
            this.normalizeSafeToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.normalizeSafeToolStripMenuItem.Name = "normalizeSafeToolStripMenuItem";
            this.normalizeSafeToolStripMenuItem.Size = new System.Drawing.Size( 170, 22 );
            this.normalizeSafeToolStripMenuItem.Text = "Normalize&Safe";
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size( 167, 6 );
            // 
            // dotToolStripMenuItem
            // 
            this.dotToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.dotToolStripMenuItem.Name = "dotToolStripMenuItem";
            this.dotToolStripMenuItem.Size = new System.Drawing.Size( 170, 22 );
            this.dotToolStripMenuItem.Text = "&Dot";
            this.dotToolStripMenuItem.Click += new System.EventHandler( this.PromptMatrix_ToolStripMenuItem_Click );
            // 
            // dot3x3ToolStripMenuItem
            // 
            this.dot3x3ToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.dot3x3ToolStripMenuItem.Name = "dot3x3ToolStripMenuItem";
            this.dot3x3ToolStripMenuItem.Size = new System.Drawing.Size( 170, 22 );
            this.dot3x3ToolStripMenuItem.Text = "Dot&3x3";
            this.dot3x3ToolStripMenuItem.Click += new System.EventHandler( this.PromptMatrix_ToolStripMenuItem_Click );
            // 
            // dotFromLeftToolStripMenuItem
            // 
            this.dotFromLeftToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.dotFromLeftToolStripMenuItem.Name = "dotFromLeftToolStripMenuItem";
            this.dotFromLeftToolStripMenuItem.Size = new System.Drawing.Size( 170, 22 );
            this.dotFromLeftToolStripMenuItem.Text = "DotFrom&Left";
            this.dotFromLeftToolStripMenuItem.Click += new System.EventHandler( this.PromptMatrix_ToolStripMenuItem_Click );
            // 
            // dot3x3FromLeftToolStripMenuItem
            // 
            this.dot3x3FromLeftToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.dot3x3FromLeftToolStripMenuItem.Name = "dot3x3FromLeftToolStripMenuItem";
            this.dot3x3FromLeftToolStripMenuItem.Size = new System.Drawing.Size( 170, 22 );
            this.dot3x3FromLeftToolStripMenuItem.Text = "Dot3x3FromL&eft";
            this.dot3x3FromLeftToolStripMenuItem.Click += new System.EventHandler( this.PromptMatrix_ToolStripMenuItem_Click );
            // 
            // dotTransposeToolStripMenuItem
            // 
            this.dotTransposeToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.dotTransposeToolStripMenuItem.Name = "dotTransposeToolStripMenuItem";
            this.dotTransposeToolStripMenuItem.Size = new System.Drawing.Size( 170, 22 );
            this.dotTransposeToolStripMenuItem.Text = "DotTr&anspose";
            // 
            // dot3x3TransposeToolStripMenuItem
            // 
            this.dot3x3TransposeToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.dot3x3TransposeToolStripMenuItem.Name = "dot3x3TransposeToolStripMenuItem";
            this.dot3x3TransposeToolStripMenuItem.Size = new System.Drawing.Size( 170, 22 );
            this.dot3x3TransposeToolStripMenuItem.Text = "Dot3x3Trans&pose";
            this.dot3x3TransposeToolStripMenuItem.Click += new System.EventHandler( this.PromptMatrix_ToolStripMenuItem_Click );
            // 
            // crossProductToolStripMenuItem
            // 
            this.crossProductToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.crossProductToolStripMenuItem.Name = "crossProductToolStripMenuItem";
            this.crossProductToolStripMenuItem.Size = new System.Drawing.Size( 170, 22 );
            this.crossProductToolStripMenuItem.Text = "&CrossProduct";
            this.crossProductToolStripMenuItem.Click += new System.EventHandler( this.PromptVector_ToolStripMenuItem_Click );
            // 
            // outerProductToolStripMenuItem
            // 
            this.outerProductToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.outerProductToolStripMenuItem.Name = "outerProductToolStripMenuItem";
            this.outerProductToolStripMenuItem.Size = new System.Drawing.Size( 170, 22 );
            this.outerProductToolStripMenuItem.Text = "&OuterProduct";
            this.outerProductToolStripMenuItem.Click += new System.EventHandler( this.PromptVectorVector_ToolStripMenuItem_Click );
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size( 167, 6 );
            // 
            // inverseToolStripMenuItem
            // 
            this.inverseToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.inverseToolStripMenuItem.Name = "inverseToolStripMenuItem";
            this.inverseToolStripMenuItem.Size = new System.Drawing.Size( 170, 22 );
            this.inverseToolStripMenuItem.Text = "&Inverse";
            // 
            // inverse3x3ToolStripMenuItem
            // 
            this.inverse3x3ToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.inverse3x3ToolStripMenuItem.Name = "inverse3x3ToolStripMenuItem";
            this.inverse3x3ToolStripMenuItem.Size = new System.Drawing.Size( 170, 22 );
            this.inverse3x3ToolStripMenuItem.Text = "Inverse3&x3";
            // 
            // fastInverseToolStripMenuItem
            // 
            this.fastInverseToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.fastInverseToolStripMenuItem.Name = "fastInverseToolStripMenuItem";
            this.fastInverseToolStripMenuItem.Size = new System.Drawing.Size( 170, 22 );
            this.fastInverseToolStripMenuItem.Text = "&FastInverse";
            // 
            // transposeToolStripMenuItem
            // 
            this.transposeToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.transposeToolStripMenuItem.Name = "transposeToolStripMenuItem";
            this.transposeToolStripMenuItem.Size = new System.Drawing.Size( 170, 22 );
            this.transposeToolStripMenuItem.Text = "&Transpose";
            // 
            // transpose3x4ToolStripMenuItem
            // 
            this.transpose3x4ToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.transpose3x4ToolStripMenuItem.Name = "transpose3x4ToolStripMenuItem";
            this.transpose3x4ToolStripMenuItem.Size = new System.Drawing.Size( 170, 22 );
            this.transpose3x4ToolStripMenuItem.Text = "Transpose3x&4";
            // 
            // advancedOperationsToolStripMenuItem
            // 
            this.advancedOperationsToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.advancedOperationsToolStripMenuItem.DropDownItems.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.makeUprightToolStripMenuItem,
            this.makeDoubleCrossMatrixToolStripMenuItem,
            this.makePosRotYToolStripMenuItem,
            this.makeReflectToolStripMenuItem,
            this.toolStripSeparator9,
            this.dotCrossProdMtxToolStripMenuItem,
            this.dot3x3CrossProdMtxToolStripMenuItem,
            this.dotCrossProdTransposeToolStripMenuItem,
            this.dot3x3CrossProdTransposeToolStripMenuItem,
            this.toolStripSeparator10,
            this.lookDownToolStripMenuItem,
            this.lookAtToolStripMenuItem,
            this.polarViewToolStripMenuItem,
            this.toolStripSeparator11,
            this.coordinateInverseSafeToolStripMenuItem,
            this.mirrorOnPlaneToolStripMenuItem,
            this.interpolateToolStripMenuItem} );
            this.advancedOperationsToolStripMenuItem.Name = "advancedOperationsToolStripMenuItem";
            this.advancedOperationsToolStripMenuItem.Size = new System.Drawing.Size( 189, 22 );
            this.advancedOperationsToolStripMenuItem.Text = "&Advanced Operations";
            // 
            // makeUprightToolStripMenuItem
            // 
            this.makeUprightToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.makeUprightToolStripMenuItem.Name = "makeUprightToolStripMenuItem";
            this.makeUprightToolStripMenuItem.Size = new System.Drawing.Size( 219, 22 );
            this.makeUprightToolStripMenuItem.Text = "Make&Upright";
            // 
            // makeDoubleCrossMatrixToolStripMenuItem
            // 
            this.makeDoubleCrossMatrixToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.makeDoubleCrossMatrixToolStripMenuItem.Name = "makeDoubleCrossMatrixToolStripMenuItem";
            this.makeDoubleCrossMatrixToolStripMenuItem.Size = new System.Drawing.Size( 219, 22 );
            this.makeDoubleCrossMatrixToolStripMenuItem.Text = "Make&DoubleCrossMatrix";
            this.makeDoubleCrossMatrixToolStripMenuItem.Click += new System.EventHandler( this.PromptVectorVector_ToolStripMenuItem_Click );
            // 
            // makePosRotYToolStripMenuItem
            // 
            this.makePosRotYToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.makePosRotYToolStripMenuItem.Name = "makePosRotYToolStripMenuItem";
            this.makePosRotYToolStripMenuItem.Size = new System.Drawing.Size( 219, 22 );
            this.makePosRotYToolStripMenuItem.Text = "Ma&kePosRotY";
            this.makePosRotYToolStripMenuItem.Click += new System.EventHandler( this.PromptVectorFloatFloat_ToolStripMenuItem_Click );
            // 
            // makeReflectToolStripMenuItem
            // 
            this.makeReflectToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.makeReflectToolStripMenuItem.Name = "makeReflectToolStripMenuItem";
            this.makeReflectToolStripMenuItem.Size = new System.Drawing.Size( 219, 22 );
            this.makeReflectToolStripMenuItem.Text = "Make&Reflect";
            this.makeReflectToolStripMenuItem.Click += new System.EventHandler( this.PromptVector_ToolStripMenuItem_Click );
            // 
            // toolStripSeparator9
            // 
            this.toolStripSeparator9.Name = "toolStripSeparator9";
            this.toolStripSeparator9.Size = new System.Drawing.Size( 216, 6 );
            // 
            // dotCrossProdMtxToolStripMenuItem
            // 
            this.dotCrossProdMtxToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.dotCrossProdMtxToolStripMenuItem.Name = "dotCrossProdMtxToolStripMenuItem";
            this.dotCrossProdMtxToolStripMenuItem.Size = new System.Drawing.Size( 219, 22 );
            this.dotCrossProdMtxToolStripMenuItem.Text = "DotCrossProd&Mtx";
            this.dotCrossProdMtxToolStripMenuItem.Click += new System.EventHandler( this.PromptVector_ToolStripMenuItem_Click );
            // 
            // dot3x3CrossProdMtxToolStripMenuItem
            // 
            this.dot3x3CrossProdMtxToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.dot3x3CrossProdMtxToolStripMenuItem.Name = "dot3x3CrossProdMtxToolStripMenuItem";
            this.dot3x3CrossProdMtxToolStripMenuItem.Size = new System.Drawing.Size( 219, 22 );
            this.dot3x3CrossProdMtxToolStripMenuItem.Text = "Dot&3x3CrossProdMtx";
            this.dot3x3CrossProdMtxToolStripMenuItem.Click += new System.EventHandler( this.PromptVector_ToolStripMenuItem_Click );
            // 
            // dotCrossProdTransposeToolStripMenuItem
            // 
            this.dotCrossProdTransposeToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.dotCrossProdTransposeToolStripMenuItem.Name = "dotCrossProdTransposeToolStripMenuItem";
            this.dotCrossProdTransposeToolStripMenuItem.Size = new System.Drawing.Size( 219, 22 );
            this.dotCrossProdTransposeToolStripMenuItem.Text = "DotCrossProd&Transpose";
            this.dotCrossProdTransposeToolStripMenuItem.Click += new System.EventHandler( this.PromptVector_ToolStripMenuItem_Click );
            // 
            // dot3x3CrossProdTransposeToolStripMenuItem
            // 
            this.dot3x3CrossProdTransposeToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.dot3x3CrossProdTransposeToolStripMenuItem.Name = "dot3x3CrossProdTransposeToolStripMenuItem";
            this.dot3x3CrossProdTransposeToolStripMenuItem.Size = new System.Drawing.Size( 219, 22 );
            this.dot3x3CrossProdTransposeToolStripMenuItem.Text = "Dot3&x3CrossProdTranspose";
            this.dot3x3CrossProdTransposeToolStripMenuItem.Click += new System.EventHandler( this.PromptVector_ToolStripMenuItem_Click );
            // 
            // toolStripSeparator10
            // 
            this.toolStripSeparator10.Name = "toolStripSeparator10";
            this.toolStripSeparator10.Size = new System.Drawing.Size( 216, 6 );
            // 
            // lookDownToolStripMenuItem
            // 
            this.lookDownToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.lookDownToolStripMenuItem.Name = "lookDownToolStripMenuItem";
            this.lookDownToolStripMenuItem.Size = new System.Drawing.Size( 219, 22 );
            this.lookDownToolStripMenuItem.Text = "&LookDown";
            this.lookDownToolStripMenuItem.Click += new System.EventHandler( this.PromptVectorVector_ToolStripMenuItem_Click );
            // 
            // lookAtToolStripMenuItem
            // 
            this.lookAtToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.lookAtToolStripMenuItem.Name = "lookAtToolStripMenuItem";
            this.lookAtToolStripMenuItem.Size = new System.Drawing.Size( 219, 22 );
            this.lookAtToolStripMenuItem.Text = "&Look&At";
            this.lookAtToolStripMenuItem.Click += new System.EventHandler( this.PromptVectorVector_ToolStripMenuItem_Click );
            // 
            // polarViewToolStripMenuItem
            // 
            this.polarViewToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.polarViewToolStripMenuItem.Name = "polarViewToolStripMenuItem";
            this.polarViewToolStripMenuItem.Size = new System.Drawing.Size( 219, 22 );
            this.polarViewToolStripMenuItem.Text = "&PolarView";
            this.polarViewToolStripMenuItem.Click += new System.EventHandler( this.PromptFloatFloatFloatFloat_ToolStripMenuItem_Click );
            // 
            // toolStripSeparator11
            // 
            this.toolStripSeparator11.Name = "toolStripSeparator11";
            this.toolStripSeparator11.Size = new System.Drawing.Size( 216, 6 );
            // 
            // coordinateInverseSafeToolStripMenuItem
            // 
            this.coordinateInverseSafeToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.coordinateInverseSafeToolStripMenuItem.Name = "coordinateInverseSafeToolStripMenuItem";
            this.coordinateInverseSafeToolStripMenuItem.Size = new System.Drawing.Size( 219, 22 );
            this.coordinateInverseSafeToolStripMenuItem.Text = "&CoordinateInverseSafe";
            // 
            // mirrorOnPlaneToolStripMenuItem
            // 
            this.mirrorOnPlaneToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.mirrorOnPlaneToolStripMenuItem.Name = "mirrorOnPlaneToolStripMenuItem";
            this.mirrorOnPlaneToolStripMenuItem.Size = new System.Drawing.Size( 219, 22 );
            this.mirrorOnPlaneToolStripMenuItem.Text = "&MirrorOnPlane";
            this.mirrorOnPlaneToolStripMenuItem.Click += new System.EventHandler( this.PromptVector_ToolStripMenuItem_Click );
            // 
            // interpolateToolStripMenuItem
            // 
            this.interpolateToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.interpolateToolStripMenuItem.Name = "interpolateToolStripMenuItem";
            this.interpolateToolStripMenuItem.Size = new System.Drawing.Size( 219, 22 );
            this.interpolateToolStripMenuItem.Text = "&Interpolate";
            this.interpolateToolStripMenuItem.Click += new System.EventHandler( this.PromptMatrixFloat_ToolStripMenuItem_Click );
            // 
            // row1ControlVector
            // 
            this.row1ControlVector.AllowDropDownOptions = false;
            this.row1ControlVector.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.row1ControlVector.Location = new System.Drawing.Point( 3, 28 );
            this.row1ControlVector.Maximum = 100M;
            this.row1ControlVector.Minimum = 0M;
            this.row1ControlVector.Name = "row1ControlVector";
            this.row1ControlVector.NumComponents = 4;
            this.row1ControlVector.Size = new System.Drawing.Size( 556, 25 );
            this.row1ControlVector.Step = 1M;
            this.row1ControlVector.TabIndex = 5;
            this.row1ControlVector.ValueXPath = null;
            this.row1ControlVector.Vector = new decimal[] {
        new decimal(new int[] {
                    0,
                    0,
                    0,
                    0}),
        new decimal(new int[] {
                    0,
                    0,
                    0,
                    0}),
        new decimal(new int[] {
                    0,
                    0,
                    0,
                    0}),
        new decimal(new int[] {
                    0,
                    0,
                    0,
                    0})};
            this.row1ControlVector.W = 0M;
            this.row1ControlVector.X = 0M;
            this.row1ControlVector.Y = 0M;
            this.row1ControlVector.Z = 0M;
            this.row1ControlVector.ValueChanged += new System.EventHandler( this.ControlVector_ValueChanged );
            // 
            // row2ControlVector
            // 
            this.row2ControlVector.AllowDropDownOptions = false;
            this.row2ControlVector.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.row2ControlVector.Location = new System.Drawing.Point( 3, 59 );
            this.row2ControlVector.Maximum = 100M;
            this.row2ControlVector.Minimum = 0M;
            this.row2ControlVector.Name = "row2ControlVector";
            this.row2ControlVector.NumComponents = 4;
            this.row2ControlVector.Size = new System.Drawing.Size( 556, 25 );
            this.row2ControlVector.Step = 1M;
            this.row2ControlVector.TabIndex = 6;
            this.row2ControlVector.ValueXPath = null;
            this.row2ControlVector.Vector = new decimal[] {
        new decimal(new int[] {
                    0,
                    0,
                    0,
                    0}),
        new decimal(new int[] {
                    0,
                    0,
                    0,
                    0}),
        new decimal(new int[] {
                    0,
                    0,
                    0,
                    0}),
        new decimal(new int[] {
                    0,
                    0,
                    0,
                    0})};
            this.row2ControlVector.W = 0M;
            this.row2ControlVector.X = 0M;
            this.row2ControlVector.Y = 0M;
            this.row2ControlVector.Z = 0M;
            this.row2ControlVector.ValueChanged += new System.EventHandler( this.ControlVector_ValueChanged );
            // 
            // row3ControlVector
            // 
            this.row3ControlVector.AllowDropDownOptions = false;
            this.row3ControlVector.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.row3ControlVector.Location = new System.Drawing.Point( 3, 90 );
            this.row3ControlVector.Maximum = 100M;
            this.row3ControlVector.Minimum = 0M;
            this.row3ControlVector.Name = "row3ControlVector";
            this.row3ControlVector.NumComponents = 4;
            this.row3ControlVector.Size = new System.Drawing.Size( 556, 25 );
            this.row3ControlVector.Step = 1M;
            this.row3ControlVector.TabIndex = 7;
            this.row3ControlVector.ValueXPath = null;
            this.row3ControlVector.Vector = new decimal[] {
        new decimal(new int[] {
                    0,
                    0,
                    0,
                    0}),
        new decimal(new int[] {
                    0,
                    0,
                    0,
                    0}),
        new decimal(new int[] {
                    0,
                    0,
                    0,
                    0}),
        new decimal(new int[] {
                    0,
                    0,
                    0,
                    0})};
            this.row3ControlVector.W = 0M;
            this.row3ControlVector.X = 0M;
            this.row3ControlVector.Y = 0M;
            this.row3ControlVector.Z = 0M;
            this.row3ControlVector.ValueChanged += new System.EventHandler( this.ControlVector_ValueChanged );
            // 
            // row4ControlVector
            // 
            this.row4ControlVector.AllowDropDownOptions = false;
            this.row4ControlVector.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.row4ControlVector.Location = new System.Drawing.Point( 3, 121 );
            this.row4ControlVector.Maximum = 100M;
            this.row4ControlVector.Minimum = 0M;
            this.row4ControlVector.Name = "row4ControlVector";
            this.row4ControlVector.NumComponents = 4;
            this.row4ControlVector.Size = new System.Drawing.Size( 556, 25 );
            this.row4ControlVector.Step = 1M;
            this.row4ControlVector.TabIndex = 8;
            this.row4ControlVector.ValueXPath = null;
            this.row4ControlVector.Vector = new decimal[] {
        new decimal(new int[] {
                    0,
                    0,
                    0,
                    0}),
        new decimal(new int[] {
                    0,
                    0,
                    0,
                    0}),
        new decimal(new int[] {
                    0,
                    0,
                    0,
                    0}),
        new decimal(new int[] {
                    0,
                    0,
                    0,
                    0})};
            this.row4ControlVector.W = 0M;
            this.row4ControlVector.X = 0M;
            this.row4ControlVector.Y = 0M;
            this.row4ControlVector.Z = 0M;
            this.row4ControlVector.ValueChanged += new System.EventHandler( this.ControlVector_ValueChanged );
            // 
            // ControlMatrix
            // 
            this.Controls.Add( this.row4ControlVector );
            this.Controls.Add( this.row3ControlVector );
            this.Controls.Add( this.row2ControlVector );
            this.Controls.Add( this.row1ControlVector );
            this.Controls.Add( this.toolStrip1 );
            this.Name = "ControlMatrix";
            this.Size = new System.Drawing.Size( 562, 150 );
            this.toolStrip1.ResumeLayout( false );
            this.toolStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.row1ControlVector)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.row2ControlVector)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.row3ControlVector)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.row4ControlVector)).EndInit();
            this.ResumeLayout( false );
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton1;
        private System.Windows.Forms.ToolStripMenuItem basicOperationsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem trigonometryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem advancedOperationsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem identityToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem zeroToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem setDiagonalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem subtractToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem absToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem negateToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem scaleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem makeScaleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem identity3x3ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem zero3x3ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem add3x3ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem subtract3x3ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem negate3x3ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem scaleFullToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem translateToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem makeTranslateToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem makePosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem makeScaleFullToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem dotToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dot3x3ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dotFromLeftToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dotTransposeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem crossProductToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem outerProductToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rotationsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem inverseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fastInverseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem transposeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem normalizeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem normalizeSafeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dot3x3FromLeftToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dot3x3TransposeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem inverse3x3ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem transpose3x4ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripMenuItem rotateXToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rotateYToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rotateZToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rotateUnitAxisToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rotateToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rotateLocalXToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rotateLocalYToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rotateLocalZToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rotateLocalAxisToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem makeRotateToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem makeRotateXToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem makeRotateYToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem makeRotateZToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem makeRotateUnitAxisToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rotateToToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem makeRotateToToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rotateFullToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rotateFullXToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rotateFullYToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rotateFullZToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rotateFullUnitAxisToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem makeRotXToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem makeRotYToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem makeRotZToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripMenuItem makeUprightToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem coordinateInverseSafeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dotCrossProdMtxToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dotCrossProdTransposeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem makeDoubleCrossMatrixToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mirrorOnPlaneToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem lookDownToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem lookAtToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem polarViewToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dot3x3CrossProdMtxToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dot3x3CrossProdTransposeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem interpolateToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem makePosRotYToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem makeReflectToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator9;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator10;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator11;
        private ControlVector row1ControlVector;
        private ControlVector row2ControlVector;
        private ControlVector row3ControlVector;
        private ControlVector row4ControlVector;
    }
}
