namespace ragUi
{
    partial class ControlAngleVector2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && (components != null) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager( typeof( ControlAngleVector2 ) );
            this.controlVector = new ragUi.ControlVector();
            ((System.ComponentModel.ISupportInitialize)(this.controlVector)).BeginInit();
            this.SuspendLayout();
            // 
            // controlVector
            // 
            this.controlVector.AllowDropDownOptions = false;
            this.controlVector.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.controlVector.ClipboardData = ((object)(resources.GetObject( "controlVector.ClipboardData" )));
            this.controlVector.Location = new System.Drawing.Point( 4, 4 );
            this.controlVector.Maximum = 1000M;
            this.controlVector.Minimum = -1000M;
            this.controlVector.Name = "controlVector";
            this.controlVector.NumComponents = 2;
            this.controlVector.Size = new System.Drawing.Size( 556, 25 );
            this.controlVector.Step = 1M;
            this.controlVector.TabIndex = 1;
            this.controlVector.ValueXPath = null;
            this.controlVector.Vector = new decimal[] {
        new decimal(new int[] {
                    0,
                    0,
                    0,
                    0}),
        new decimal(new int[] {
                    0,
                    0,
                    0,
                    0}),
        new decimal(new int[] {
                    0,
                    0,
                    0,
                    0}),
        new decimal(new int[] {
                    0,
                    0,
                    0,
                    0})};
            this.controlVector.W = 0M;
            this.controlVector.X = 0M;
            this.controlVector.Y = 0M;
            this.controlVector.Z = 0M;
            this.controlVector.ValueChanged += new System.EventHandler( this.controlVector_ValueChanged );
            // 
            // ControlAngleVector2
            // 
            this.Controls.Add( this.controlVector );
            this.Name = "ControlAngleVector2";
            this.Controls.SetChildIndex( this.anglePanel, 0 );
            this.Controls.SetChildIndex( this.controlVector, 0 );
            ((System.ComponentModel.ISupportInitialize)(this.controlVector)).EndInit();
            this.ResumeLayout( false );

        }

        #endregion

        private ControlVector controlVector;

    }
}
