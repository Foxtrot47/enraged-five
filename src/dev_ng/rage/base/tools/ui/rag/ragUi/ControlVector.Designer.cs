namespace ragUi
{
    partial class ControlVector
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager( typeof( ControlVector ) );
            this.XnumericUpDown = new ReadOnlyNumericUpDown();
            this.YnumericUpDown = new ReadOnlyNumericUpDown();
            this.ZnumericUpDown = new ReadOnlyNumericUpDown();
            this.WnumericUpDown = new ReadOnlyNumericUpDown();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripDropDownButton1 = new System.Windows.Forms.ToolStripDropDownButton();
            this.basicToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.scaleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.invScaleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.extendToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.scale3ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.addToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.subtractToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.multiplyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.invertToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.invertSafeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.negateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.absToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.averageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.trigToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.normalizeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.normalizeSafeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.normalizeSafeVToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.normalizeFastToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.normalize3ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.normalize3VToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.normalizeFast3ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.rotateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rotateXToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rotateYToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rotateZToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rotateAboutAxisToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.dotVToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dot3VToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.crossToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.advancedToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lerpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.logToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.log10ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.reflectAboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reflectAboutFastToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.addNetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.XcheckBox = new System.Windows.Forms.CheckBox();
            this.YcheckBox = new System.Windows.Forms.CheckBox();
            this.ZcheckBox = new System.Windows.Forms.CheckBox();
            this.WcheckBox = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.XnumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.YnumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ZnumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WnumericUpDown)).BeginInit();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // XnumericUpDown
            // 
            this.XnumericUpDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.XnumericUpDown.DecimalPlaces = 3;
            this.XnumericUpDown.Font = new System.Drawing.Font( "Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)) );
            this.XnumericUpDown.Location = new System.Drawing.Point( 173, 2 );
            this.XnumericUpDown.Name = "XnumericUpDown";
            this.XnumericUpDown.Size = new System.Drawing.Size( 67, 20 );
            this.XnumericUpDown.TabIndex = 2;
            this.XnumericUpDown.ValueChanged += new System.EventHandler( this.NumericUpdateDown_ValueChanged );
            this.XnumericUpDown.Enter += new System.EventHandler( this.NumericUpDown_Enter );
            // 
            // YnumericUpDown
            // 
            this.YnumericUpDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.YnumericUpDown.DecimalPlaces = 3;
            this.YnumericUpDown.Font = new System.Drawing.Font( "Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)) );
            this.YnumericUpDown.Location = new System.Drawing.Point( 276, 2 );
            this.YnumericUpDown.Name = "YnumericUpDown";
            this.YnumericUpDown.Size = new System.Drawing.Size( 67, 20 );
            this.YnumericUpDown.TabIndex = 4;
            this.YnumericUpDown.ValueChanged += new System.EventHandler( this.NumericUpdateDown_ValueChanged );
            this.YnumericUpDown.Enter += new System.EventHandler( this.NumericUpDown_Enter );
            // 
            // ZnumericUpDown
            // 
            this.ZnumericUpDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ZnumericUpDown.DecimalPlaces = 3;
            this.ZnumericUpDown.Font = new System.Drawing.Font( "Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)) );
            this.ZnumericUpDown.Location = new System.Drawing.Point( 379, 2 );
            this.ZnumericUpDown.Name = "ZnumericUpDown";
            this.ZnumericUpDown.Size = new System.Drawing.Size( 67, 20 );
            this.ZnumericUpDown.TabIndex = 6;
            this.ZnumericUpDown.ValueChanged += new System.EventHandler( this.NumericUpdateDown_ValueChanged );
            this.ZnumericUpDown.Enter += new System.EventHandler( this.NumericUpDown_Enter );
            // 
            // WnumericUpDown
            // 
            this.WnumericUpDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.WnumericUpDown.DecimalPlaces = 3;
            this.WnumericUpDown.Font = new System.Drawing.Font( "Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)) );
            this.WnumericUpDown.Location = new System.Drawing.Point( 486, 2 );
            this.WnumericUpDown.Name = "WnumericUpDown";
            this.WnumericUpDown.Size = new System.Drawing.Size( 67, 20 );
            this.WnumericUpDown.TabIndex = 8;
            this.WnumericUpDown.ValueChanged += new System.EventHandler( this.NumericUpdateDown_ValueChanged );
            this.WnumericUpDown.Enter += new System.EventHandler( this.NumericUpDown_Enter );
            // 
            // toolStrip1
            // 
            this.toolStrip1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.toolStrip1.AutoSize = false;
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.Items.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.toolStripDropDownButton1} );
            this.toolStrip1.Location = new System.Drawing.Point( 0, 0 );
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.ShowItemToolTips = false;
            this.toolStrip1.Size = new System.Drawing.Size( 140, 25 );
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripDropDownButton1
            // 
            this.toolStripDropDownButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripDropDownButton1.DropDownItems.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.basicToolStripMenuItem,
            this.trigToolStripMenuItem,
            this.advancedToolStripMenuItem} );
            this.toolStripDropDownButton1.Image = ((System.Drawing.Image)(resources.GetObject( "toolStripDropDownButton1.Image" )));
            this.toolStripDropDownButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton1.Name = "toolStripDropDownButton1";
            this.toolStripDropDownButton1.ShowDropDownArrow = false;
            this.toolStripDropDownButton1.Size = new System.Drawing.Size( 155, 19 );
            this.toolStripDropDownButton1.Text = "toolStripDropDownButton1";
            // 
            // basicToolStripMenuItem
            // 
            this.basicToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.basicToolStripMenuItem.DropDownItems.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.scaleToolStripMenuItem,
            this.invScaleToolStripMenuItem,
            this.extendToolStripMenuItem,
            this.scale3ToolStripMenuItem,
            this.toolStripSeparator1,
            this.addToolStripMenuItem,
            this.subtractToolStripMenuItem,
            this.multiplyToolStripMenuItem,
            this.invertToolStripMenuItem,
            this.invertSafeToolStripMenuItem,
            this.negateToolStripMenuItem,
            this.toolStripSeparator2,
            this.absToolStripMenuItem,
            this.averageToolStripMenuItem} );
            this.basicToolStripMenuItem.Name = "basicToolStripMenuItem";
            this.basicToolStripMenuItem.Size = new System.Drawing.Size( 188, 22 );
            this.basicToolStripMenuItem.Text = "&Basic Operations";
            // 
            // scaleToolStripMenuItem
            // 
            this.scaleToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.scaleToolStripMenuItem.Name = "scaleToolStripMenuItem";
            this.scaleToolStripMenuItem.Size = new System.Drawing.Size( 126, 22 );
            this.scaleToolStripMenuItem.Text = "S&cale";
            this.scaleToolStripMenuItem.Click += new System.EventHandler( this.PromptFloat_ToolStripMenuItem_Click );
            // 
            // invScaleToolStripMenuItem
            // 
            this.invScaleToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.invScaleToolStripMenuItem.Name = "invScaleToolStripMenuItem";
            this.invScaleToolStripMenuItem.Size = new System.Drawing.Size( 126, 22 );
            this.invScaleToolStripMenuItem.Text = "InvSca&le";
            this.invScaleToolStripMenuItem.Click += new System.EventHandler( this.PromptFloat_ToolStripMenuItem_Click );
            // 
            // extendToolStripMenuItem
            // 
            this.extendToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.extendToolStripMenuItem.Name = "extendToolStripMenuItem";
            this.extendToolStripMenuItem.Size = new System.Drawing.Size( 126, 22 );
            this.extendToolStripMenuItem.Text = "&Extend";
            this.extendToolStripMenuItem.Click += new System.EventHandler( this.PromptFloat_ToolStripMenuItem_Click );
            // 
            // scale3ToolStripMenuItem
            // 
            this.scale3ToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.scale3ToolStripMenuItem.Name = "scale3ToolStripMenuItem";
            this.scale3ToolStripMenuItem.Size = new System.Drawing.Size( 126, 22 );
            this.scale3ToolStripMenuItem.Text = "Scale&3";
            this.scale3ToolStripMenuItem.Click += new System.EventHandler( this.PromptFloat_ToolStripMenuItem_Click );
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size( 123, 6 );
            // 
            // addToolStripMenuItem
            // 
            this.addToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.addToolStripMenuItem.Name = "addToolStripMenuItem";
            this.addToolStripMenuItem.Size = new System.Drawing.Size( 126, 22 );
            this.addToolStripMenuItem.Text = "&Add";
            this.addToolStripMenuItem.Click += new System.EventHandler( this.PromptVector_ToolStripMenuItem_Click );
            // 
            // subtractToolStripMenuItem
            // 
            this.subtractToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.subtractToolStripMenuItem.Name = "subtractToolStripMenuItem";
            this.subtractToolStripMenuItem.Size = new System.Drawing.Size( 126, 22 );
            this.subtractToolStripMenuItem.Text = "&Subtract";
            this.subtractToolStripMenuItem.Click += new System.EventHandler( this.PromptVector_ToolStripMenuItem_Click );
            // 
            // multiplyToolStripMenuItem
            // 
            this.multiplyToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.multiplyToolStripMenuItem.Name = "multiplyToolStripMenuItem";
            this.multiplyToolStripMenuItem.Size = new System.Drawing.Size( 126, 22 );
            this.multiplyToolStripMenuItem.Text = "&Multiply";
            this.multiplyToolStripMenuItem.Click += new System.EventHandler( this.PromptVector_ToolStripMenuItem_Click );
            // 
            // invertToolStripMenuItem
            // 
            this.invertToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.invertToolStripMenuItem.Name = "invertToolStripMenuItem";
            this.invertToolStripMenuItem.Size = new System.Drawing.Size( 126, 22 );
            this.invertToolStripMenuItem.Text = "&Invert";
            // 
            // invertSafeToolStripMenuItem
            // 
            this.invertSafeToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.invertSafeToolStripMenuItem.Name = "invertSafeToolStripMenuItem";
            this.invertSafeToolStripMenuItem.Size = new System.Drawing.Size( 126, 22 );
            this.invertSafeToolStripMenuItem.Text = "In&vertSafe";
            // 
            // negateToolStripMenuItem
            // 
            this.negateToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.negateToolStripMenuItem.Name = "negateToolStripMenuItem";
            this.negateToolStripMenuItem.Size = new System.Drawing.Size( 126, 22 );
            this.negateToolStripMenuItem.Text = "&Negate";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size( 123, 6 );
            // 
            // absToolStripMenuItem
            // 
            this.absToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.absToolStripMenuItem.Name = "absToolStripMenuItem";
            this.absToolStripMenuItem.Size = new System.Drawing.Size( 126, 22 );
            this.absToolStripMenuItem.Text = "A&bs";
            // 
            // averageToolStripMenuItem
            // 
            this.averageToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.averageToolStripMenuItem.Name = "averageToolStripMenuItem";
            this.averageToolStripMenuItem.Size = new System.Drawing.Size( 126, 22 );
            this.averageToolStripMenuItem.Text = "Av&erage";
            this.averageToolStripMenuItem.Click += new System.EventHandler( this.PromptVector_ToolStripMenuItem_Click );
            // 
            // trigToolStripMenuItem
            // 
            this.trigToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.trigToolStripMenuItem.DropDownItems.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.normalizeToolStripMenuItem,
            this.normalizeSafeToolStripMenuItem,
            this.normalizeSafeVToolStripMenuItem,
            this.normalizeFastToolStripMenuItem,
            this.normalize3ToolStripMenuItem,
            this.normalize3VToolStripMenuItem,
            this.normalizeFast3ToolStripMenuItem,
            this.toolStripSeparator3,
            this.rotateToolStripMenuItem,
            this.rotateXToolStripMenuItem,
            this.rotateYToolStripMenuItem,
            this.rotateZToolStripMenuItem,
            this.rotateAboutAxisToolStripMenuItem,
            this.toolStripSeparator4,
            this.dotVToolStripMenuItem,
            this.dot3VToolStripMenuItem,
            this.crossToolStripMenuItem} );
            this.trigToolStripMenuItem.Name = "trigToolStripMenuItem";
            this.trigToolStripMenuItem.Size = new System.Drawing.Size( 188, 22 );
            this.trigToolStripMenuItem.Text = "&Trigonometry";
            // 
            // normalizeToolStripMenuItem
            // 
            this.normalizeToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.normalizeToolStripMenuItem.Name = "normalizeToolStripMenuItem";
            this.normalizeToolStripMenuItem.Size = new System.Drawing.Size( 162, 22 );
            this.normalizeToolStripMenuItem.Text = "&Normalize";
            // 
            // normalizeSafeToolStripMenuItem
            // 
            this.normalizeSafeToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.normalizeSafeToolStripMenuItem.Name = "normalizeSafeToolStripMenuItem";
            this.normalizeSafeToolStripMenuItem.Size = new System.Drawing.Size( 162, 22 );
            this.normalizeSafeToolStripMenuItem.Text = "Normalize&Safe";
            // 
            // normalizeSafeVToolStripMenuItem
            // 
            this.normalizeSafeVToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.normalizeSafeVToolStripMenuItem.Name = "normalizeSafeVToolStripMenuItem";
            this.normalizeSafeVToolStripMenuItem.Size = new System.Drawing.Size( 162, 22 );
            this.normalizeSafeVToolStripMenuItem.Text = "NormalizeSafe&V";
            // 
            // normalizeFastToolStripMenuItem
            // 
            this.normalizeFastToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.normalizeFastToolStripMenuItem.Name = "normalizeFastToolStripMenuItem";
            this.normalizeFastToolStripMenuItem.Size = new System.Drawing.Size( 162, 22 );
            this.normalizeFastToolStripMenuItem.Text = "Normalize&Fast";
            // 
            // normalize3ToolStripMenuItem
            // 
            this.normalize3ToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.normalize3ToolStripMenuItem.Name = "normalize3ToolStripMenuItem";
            this.normalize3ToolStripMenuItem.Size = new System.Drawing.Size( 162, 22 );
            this.normalize3ToolStripMenuItem.Text = "Normalize&3";
            // 
            // normalize3VToolStripMenuItem
            // 
            this.normalize3VToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.normalize3VToolStripMenuItem.Name = "normalize3VToolStripMenuItem";
            this.normalize3VToolStripMenuItem.Size = new System.Drawing.Size( 162, 22 );
            this.normalize3VToolStripMenuItem.Text = "Nor&malize3V";
            // 
            // normalizeFast3ToolStripMenuItem
            // 
            this.normalizeFast3ToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.normalizeFast3ToolStripMenuItem.Name = "normalizeFast3ToolStripMenuItem";
            this.normalizeFast3ToolStripMenuItem.Size = new System.Drawing.Size( 162, 22 );
            this.normalizeFast3ToolStripMenuItem.Text = "Norma&lizeFast3";
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size( 159, 6 );
            // 
            // rotateToolStripMenuItem
            // 
            this.rotateToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.rotateToolStripMenuItem.Name = "rotateToolStripMenuItem";
            this.rotateToolStripMenuItem.Size = new System.Drawing.Size( 162, 22 );
            this.rotateToolStripMenuItem.Text = "&Rotate";
            this.rotateToolStripMenuItem.Click += new System.EventHandler( this.PromptFloat_ToolStripMenuItem_Click );
            // 
            // rotateXToolStripMenuItem
            // 
            this.rotateXToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.rotateXToolStripMenuItem.Name = "rotateXToolStripMenuItem";
            this.rotateXToolStripMenuItem.Size = new System.Drawing.Size( 162, 22 );
            this.rotateXToolStripMenuItem.Text = "Rotate&X";
            this.rotateXToolStripMenuItem.Click += new System.EventHandler( this.PromptFloat_ToolStripMenuItem_Click );
            // 
            // rotateYToolStripMenuItem
            // 
            this.rotateYToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.rotateYToolStripMenuItem.Name = "rotateYToolStripMenuItem";
            this.rotateYToolStripMenuItem.Size = new System.Drawing.Size( 162, 22 );
            this.rotateYToolStripMenuItem.Text = "Rotate&Y";
            this.rotateYToolStripMenuItem.Click += new System.EventHandler( this.PromptFloat_ToolStripMenuItem_Click );
            // 
            // rotateZToolStripMenuItem
            // 
            this.rotateZToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.rotateZToolStripMenuItem.Name = "rotateZToolStripMenuItem";
            this.rotateZToolStripMenuItem.Size = new System.Drawing.Size( 162, 22 );
            this.rotateZToolStripMenuItem.Text = "Rotate&Z";
            this.rotateZToolStripMenuItem.Click += new System.EventHandler( this.PromptFloat_ToolStripMenuItem_Click );
            // 
            // rotateAboutAxisToolStripMenuItem
            // 
            this.rotateAboutAxisToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.rotateAboutAxisToolStripMenuItem.Name = "rotateAboutAxisToolStripMenuItem";
            this.rotateAboutAxisToolStripMenuItem.Size = new System.Drawing.Size( 162, 22 );
            this.rotateAboutAxisToolStripMenuItem.Text = "Rotate&AboutAxis";
            this.rotateAboutAxisToolStripMenuItem.Click += new System.EventHandler( this.PromptFloatInt_ToolStripMenuItem_Click );
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size( 159, 6 );
            // 
            // dotVToolStripMenuItem
            // 
            this.dotVToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.dotVToolStripMenuItem.Name = "dotVToolStripMenuItem";
            this.dotVToolStripMenuItem.Size = new System.Drawing.Size( 162, 22 );
            this.dotVToolStripMenuItem.Text = "&DotV";
            this.dotVToolStripMenuItem.Click += new System.EventHandler( this.PromptVector_ToolStripMenuItem_Click );
            // 
            // dot3VToolStripMenuItem
            // 
            this.dot3VToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.dot3VToolStripMenuItem.Name = "dot3VToolStripMenuItem";
            this.dot3VToolStripMenuItem.Size = new System.Drawing.Size( 162, 22 );
            this.dot3VToolStripMenuItem.Text = "Do&t3V";
            this.dot3VToolStripMenuItem.Click += new System.EventHandler( this.PromptVector_ToolStripMenuItem_Click );
            // 
            // crossToolStripMenuItem
            // 
            this.crossToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.crossToolStripMenuItem.Name = "crossToolStripMenuItem";
            this.crossToolStripMenuItem.Size = new System.Drawing.Size( 162, 22 );
            this.crossToolStripMenuItem.Text = "&Cross";
            this.crossToolStripMenuItem.Click += new System.EventHandler( this.PromptVector_ToolStripMenuItem_Click );
            // 
            // advancedToolStripMenuItem
            // 
            this.advancedToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.advancedToolStripMenuItem.DropDownItems.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.lerpToolStripMenuItem,
            this.toolStripSeparator5,
            this.logToolStripMenuItem,
            this.log10ToolStripMenuItem,
            this.toolStripSeparator6,
            this.reflectAboutToolStripMenuItem,
            this.reflectAboutFastToolStripMenuItem,
            this.toolStripSeparator7,
            this.addNetToolStripMenuItem} );
            this.advancedToolStripMenuItem.Name = "advancedToolStripMenuItem";
            this.advancedToolStripMenuItem.Size = new System.Drawing.Size( 188, 22 );
            this.advancedToolStripMenuItem.Text = "&Advanced Operations";
            // 
            // lerpToolStripMenuItem
            // 
            this.lerpToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.lerpToolStripMenuItem.Name = "lerpToolStripMenuItem";
            this.lerpToolStripMenuItem.Size = new System.Drawing.Size( 164, 22 );
            this.lerpToolStripMenuItem.Text = "&Lerp";
            this.lerpToolStripMenuItem.Click += new System.EventHandler( this.PromptFloatVector_ToolStripMenuItem_Click );
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size( 161, 6 );
            // 
            // logToolStripMenuItem
            // 
            this.logToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.logToolStripMenuItem.Name = "logToolStripMenuItem";
            this.logToolStripMenuItem.Size = new System.Drawing.Size( 164, 22 );
            this.logToolStripMenuItem.Text = "L&og";
            // 
            // log10ToolStripMenuItem
            // 
            this.log10ToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.log10ToolStripMenuItem.Name = "log10ToolStripMenuItem";
            this.log10ToolStripMenuItem.Size = new System.Drawing.Size( 164, 22 );
            this.log10ToolStripMenuItem.Text = "Lo&g10";
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size( 161, 6 );
            // 
            // reflectAboutToolStripMenuItem
            // 
            this.reflectAboutToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.reflectAboutToolStripMenuItem.Name = "reflectAboutToolStripMenuItem";
            this.reflectAboutToolStripMenuItem.Size = new System.Drawing.Size( 164, 22 );
            this.reflectAboutToolStripMenuItem.Text = "&ReflectAbout";
            this.reflectAboutToolStripMenuItem.Click += new System.EventHandler( this.PromptVector_ToolStripMenuItem_Click );
            // 
            // reflectAboutFastToolStripMenuItem
            // 
            this.reflectAboutFastToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.reflectAboutFastToolStripMenuItem.Name = "reflectAboutFastToolStripMenuItem";
            this.reflectAboutFastToolStripMenuItem.Size = new System.Drawing.Size( 164, 22 );
            this.reflectAboutFastToolStripMenuItem.Text = "ReflectAbout&Fast";
            this.reflectAboutFastToolStripMenuItem.Click += new System.EventHandler( this.PromptVector_ToolStripMenuItem_Click );
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size( 161, 6 );
            // 
            // addNetToolStripMenuItem
            // 
            this.addNetToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.addNetToolStripMenuItem.Name = "addNetToolStripMenuItem";
            this.addNetToolStripMenuItem.Size = new System.Drawing.Size( 164, 22 );
            this.addNetToolStripMenuItem.Text = "&AddNet";
            this.addNetToolStripMenuItem.Click += new System.EventHandler( this.PromptVector_ToolStripMenuItem_Click );
            // 
            // XcheckBox
            // 
            this.XcheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.XcheckBox.Appearance = System.Windows.Forms.Appearance.Button;
            this.XcheckBox.AutoSize = true;
            this.XcheckBox.Location = new System.Drawing.Point( 143, 0 );
            this.XcheckBox.Name = "XcheckBox";
            this.XcheckBox.Size = new System.Drawing.Size( 24, 23 );
            this.XcheckBox.TabIndex = 1;
            this.XcheckBox.TabStop = false;
            this.XcheckBox.Text = "X";
            this.XcheckBox.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.XcheckBox.UseVisualStyleBackColor = true;
            this.XcheckBox.KeyDown += new System.Windows.Forms.KeyEventHandler( this.CheckBox_KeyDown );
            this.XcheckBox.MouseDown += new System.Windows.Forms.MouseEventHandler( this.CheckBox_MouseDown );
            this.XcheckBox.MouseMove += new System.Windows.Forms.MouseEventHandler( this.CheckBox_MouseMove );
            this.XcheckBox.MouseUp += new System.Windows.Forms.MouseEventHandler( this.CheckBox_MouseUp );
            // 
            // YcheckBox
            // 
            this.YcheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.YcheckBox.Appearance = System.Windows.Forms.Appearance.Button;
            this.YcheckBox.AutoSize = true;
            this.YcheckBox.Location = new System.Drawing.Point( 246, 0 );
            this.YcheckBox.Name = "YcheckBox";
            this.YcheckBox.Size = new System.Drawing.Size( 24, 23 );
            this.YcheckBox.TabIndex = 3;
            this.YcheckBox.TabStop = false;
            this.YcheckBox.Text = "Y";
            this.YcheckBox.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.YcheckBox.UseVisualStyleBackColor = true;
            this.YcheckBox.KeyDown += new System.Windows.Forms.KeyEventHandler( this.CheckBox_KeyDown );
            this.YcheckBox.MouseDown += new System.Windows.Forms.MouseEventHandler( this.CheckBox_MouseDown );
            this.YcheckBox.MouseMove += new System.Windows.Forms.MouseEventHandler( this.CheckBox_MouseMove );
            this.YcheckBox.MouseUp += new System.Windows.Forms.MouseEventHandler( this.CheckBox_MouseUp );
            // 
            // ZcheckBox
            // 
            this.ZcheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ZcheckBox.Appearance = System.Windows.Forms.Appearance.Button;
            this.ZcheckBox.AutoSize = true;
            this.ZcheckBox.Location = new System.Drawing.Point( 349, 0 );
            this.ZcheckBox.Name = "ZcheckBox";
            this.ZcheckBox.Size = new System.Drawing.Size( 24, 23 );
            this.ZcheckBox.TabIndex = 5;
            this.ZcheckBox.TabStop = false;
            this.ZcheckBox.Text = "Z";
            this.ZcheckBox.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.ZcheckBox.UseVisualStyleBackColor = true;
            this.ZcheckBox.KeyDown += new System.Windows.Forms.KeyEventHandler( this.CheckBox_KeyDown );
            this.ZcheckBox.MouseDown += new System.Windows.Forms.MouseEventHandler( this.CheckBox_MouseDown );
            this.ZcheckBox.MouseMove += new System.Windows.Forms.MouseEventHandler( this.CheckBox_MouseMove );
            this.ZcheckBox.MouseUp += new System.Windows.Forms.MouseEventHandler( this.CheckBox_MouseUp );
            // 
            // WcheckBox
            // 
            this.WcheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.WcheckBox.Appearance = System.Windows.Forms.Appearance.Button;
            this.WcheckBox.AutoSize = true;
            this.WcheckBox.Location = new System.Drawing.Point( 452, 0 );
            this.WcheckBox.Name = "WcheckBox";
            this.WcheckBox.Size = new System.Drawing.Size( 28, 23 );
            this.WcheckBox.TabIndex = 7;
            this.WcheckBox.TabStop = false;
            this.WcheckBox.Text = "W";
            this.WcheckBox.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.WcheckBox.UseVisualStyleBackColor = true;
            this.WcheckBox.KeyDown += new System.Windows.Forms.KeyEventHandler( this.CheckBox_KeyDown );
            this.WcheckBox.MouseDown += new System.Windows.Forms.MouseEventHandler( this.CheckBox_MouseDown );
            this.WcheckBox.MouseMove += new System.Windows.Forms.MouseEventHandler( this.CheckBox_MouseMove );
            this.WcheckBox.MouseUp += new System.Windows.Forms.MouseEventHandler( this.CheckBox_MouseUp );
            // 
            // ControlVector
            // 
            this.Controls.Add( this.WcheckBox );
            this.Controls.Add( this.ZcheckBox );
            this.Controls.Add( this.YcheckBox );
            this.Controls.Add( this.XcheckBox );
            this.Controls.Add( this.toolStrip1 );
            this.Controls.Add( this.WnumericUpDown );
            this.Controls.Add( this.ZnumericUpDown );
            this.Controls.Add( this.YnumericUpDown );
            this.Controls.Add( this.XnumericUpDown );
            this.Name = "ControlVector";
            this.Size = new System.Drawing.Size( 556, 25 );
            ((System.ComponentModel.ISupportInitialize)(this.XnumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.YnumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ZnumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WnumericUpDown)).EndInit();
            this.toolStrip1.ResumeLayout( false );
            this.toolStrip1.PerformLayout();
            this.ResumeLayout( false );
            this.PerformLayout();

        }

        #endregion

        private ReadOnlyNumericUpDown XnumericUpDown;
        private ReadOnlyNumericUpDown YnumericUpDown;
        private ReadOnlyNumericUpDown ZnumericUpDown;
        private ReadOnlyNumericUpDown WnumericUpDown;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton1;
        private System.Windows.Forms.ToolStripMenuItem basicToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem trigToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem advancedToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem scaleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem invScaleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem extendToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem scale3ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem addToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem subtractToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem multiplyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem invertToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem invertSafeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem negateToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem absToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem averageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem normalizeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem normalizeSafeToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem rotateToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem normalizeSafeVToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem normalize3ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem normalize3VToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem normalizeFast3ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rotateXToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rotateYToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rotateZToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem crossToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dotVToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rotateAboutAxisToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dot3VToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem lerpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem logToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem log10ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reflectAboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reflectAboutFastToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addNetToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripMenuItem normalizeFastToolStripMenuItem;
        private System.Windows.Forms.CheckBox XcheckBox;
        private System.Windows.Forms.CheckBox YcheckBox;
        private System.Windows.Forms.CheckBox ZcheckBox;
        private System.Windows.Forms.CheckBox WcheckBox;
    }
}
