using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace ragUi
{
	public class ControlToggle : ragUi.ControlBase
	{
		private System.Windows.Forms.CheckBox m_CheckBox;
		private System.ComponentModel.IContainer components = null;
        private delegate void UpdateInvoke(bool value);

		public ControlToggle()
		{
			// This call is required by the Windows Form Designer.
			InitializeComponent();

			// TODO: Add any initialization after the InitializeComponent call
        }

        #region Properties
        public bool Value
        {
            get
            {
                return m_CheckBox.Checked;
            }
            set
            {
                if (m_CheckBox.InvokeRequired == true)
                {
                    UpdateInvoke invoke = delegate(bool invokeValue)
                    {
                        m_CheckBox.Checked = invokeValue;
                        OnValueChanged();
                    };
                }
                else
                {
                    m_CheckBox.Checked = value;
                    OnValueChanged();
                }
            }
        }

        public override bool IsReadOnly
        {
            get { return !m_CheckBox.Enabled; }
            set { m_CheckBox.Enabled = !value; }
        }
        #endregion

        #region Overrides
        public override bool CanCopy
        {
            get
            {
                return true;
            }
        }

        public override bool CanPaste
        {
            get
            {
                return true;
            }
        }

        public override object ClipboardData
        {
            get
            {
				return this.Value.ToString();
            }
            set
            {
				if ((value != null) && (value is string))
				{
					// Ok it is a string.  Are there any numbers in the string?
					string strClipboardString = value as string;
					string[] astrStringParts = strClipboardString.ToLower().Split();
					foreach (string strPossibleNumber in astrStringParts)
					{
						if (strPossibleNumber == "true")
						{
							this.Value = true;
							return;
						}
						else if (strPossibleNumber == "false")
						{
							this.Value = false;
							return;
						}
					}
				}
			}
        }

        [Browsable( true ), Category( "Misc" ), DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
        public override string Text
        {
            get
            {
                return m_CheckBox.Text;
            }
            set
            {
                m_CheckBox.Text = value;
            }
        }

        /// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
        }

        protected override void OnIconChanged()
        {
            PadControlTextForIcon( m_CheckBox, m_Icon );
            m_CheckBox.Image = m_Icon;
        }
        #endregion

        #region Event Handlers
        private void OnCheckedChanged( object sender, System.EventArgs e )
        {
            OnValueChanged();
        }
        #endregion

        #region Designer generated code
        /// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.m_CheckBox = new System.Windows.Forms.CheckBox();
			this.SuspendLayout();
			// 
			// m_CheckBox
			// 
			this.m_CheckBox.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.m_CheckBox.Dock = System.Windows.Forms.DockStyle.Fill;
			this.m_CheckBox.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.m_CheckBox.Location = new System.Drawing.Point(0, 0);
			this.m_CheckBox.Name = "m_CheckBox";
			this.m_CheckBox.Size = new System.Drawing.Size(296, 24);
			this.m_CheckBox.TabIndex = 0;
			this.m_CheckBox.Text = "Checkbox";
			this.m_CheckBox.CheckedChanged += new System.EventHandler(this.OnCheckedChanged);
			// 
			// ControlToggle
			// 
			this.Controls.Add(this.m_CheckBox);
			this.Name = "ControlToggle";
			this.Size = new System.Drawing.Size(296, 24);
			this.ResumeLayout(false);

		}
		#endregion
	}

    #region Helper Classes
    [Serializable]
    public class ToggleClipboardData : Object
    {
        public ToggleClipboardData( bool value )
        {
            m_value = value;
        }

        #region Variables
        private bool m_value;
        #endregion

        #region Properties
        public bool Value
        {
            get
            {
                return m_value;
            }
        }
        #endregion
    };
    #endregion
}

