using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace ragUi
{
	public class ControlCombo : ragUi.ControlBase
	{
		private System.Windows.Forms.Label m_Label;
        private ComboBoxEx m_ComboBox;
		private System.ComponentModel.IContainer components = null;

		public ControlCombo()
		{
			// This call is required by the Windows Form Designer.
			InitializeComponent();

			// TODO: Add any initialization after the InitializeComponent call
        }

        #region Variables
        private int m_selectedIndex = -1;
        private int m_prevSelectedIndex = -1;
        private bool m_firstTime = true;
        #endregion

        #region Properties
        public String Value
        {
            get
            {
                return m_ComboBox.SelectedItem as string;
            }
            set
            {
                m_ComboBox.SelectedItem = value;
            }
        }

        public int SelectedIndex
        {
            get
            {
                return m_ComboBox.SelectedIndex;
            }
            set
            {
                SetSelectedIndex( value, true );
            }
        }

        [Browsable( true ), Category( "Misc" ), DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
        public override string Text
        {
            get
            {
                return m_Label.Text;
            }
            set
            {
                m_Label.Text = value;
            }
        }

        public override bool IsReadOnly
        {
            get { return !m_ComboBox.Enabled; }
            set { m_ComboBox.Enabled = !value; }
        }
        #endregion

        #region Overrides
        public override bool CanCopy
        {
            get
            {
                return true;
            }
        }

        public override bool CanPaste
        {
            get
            {
                return true;
            }
        }

        public override bool CanUndo
        {
            get
            {
                return true;
            }
        }

        public override object ClipboardData
        {
            get
            {
				return this.Value;
            }
            set
            {
				if ((value != null) && (value is string))
				{
					// Ok it is a string.  Are there any numbers in the string?
					string strClipboardString = value as string;
					string[] astrStringParts = strClipboardString.ToLower().Split();
					foreach (string strPossibleValue in astrStringParts)
					{
						// Is this value in the list of values?
						for (int v = 0; v < m_ComboBox.Items.Count; v++)
						{
							string strComboBoxItem = m_ComboBox.Items[v] as string;
							strComboBoxItem = strComboBoxItem.ToLower();
							if (strPossibleValue == strComboBoxItem)
							{
								// Found a match!
								int selectedIndex = this.SelectedIndex;
								this.Value = (m_ComboBox.Items[v] as string);
								if (this.SelectedIndex != selectedIndex)
								{
									m_prevSelectedIndex = selectedIndex;
								}
								return;
							}
						}
					}
				}
            }
        }

        public override bool IsUndoable
        {
            get
            {
                return base.IsUndoable && (m_prevSelectedIndex != -1) && (m_prevSelectedIndex != m_selectedIndex);
            }
        }

        /// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
        }

        protected override void OnIconChanged()
        {
            PadControlTextForIcon( m_Label, m_Icon );
            m_Label.Image = m_Icon;
        }

        public override void Undo()
        {
            int newIndex = m_prevSelectedIndex;

            m_prevSelectedIndex = m_selectedIndex;

            SetSelectedIndex( newIndex, true );
        }
        #endregion

        #region Event Handlers
        private void ComboBox_SelectedIndexChanged( object sender, EventArgs e )
        {
            m_prevSelectedIndex = m_selectedIndex;

            SetSelectedIndex( m_ComboBox.SelectedIndex, true );
        }

        private void ComboBox_KeyDown( object sender, KeyEventArgs e )
        {
            if ( e.Control )
            {
                switch ( e.KeyCode )
                {
                    case Keys.C:
                        CopyToClipboard();
                        e.SuppressKeyPress = true;
                        break;
                    case Keys.V:
                        PasteFromClipboard();
                        e.SuppressKeyPress = true;
                        break;
                    case Keys.Z:
                        Undo();
                        e.SuppressKeyPress = true;
                        break;
                    default:
                        break;
                }
            }
        }
        #endregion

        #region Public Functions
        public void UpdateValueFromRemote( int index )
        {
            int prevIndex = this.SelectedIndex;

            SetSelectedIndex( index, false );

            if ( m_firstTime )
            {
                // since this is the first time, previous value will be the initial value.
                m_prevSelectedIndex = this.SelectedIndex;
                m_firstTime = false;
            }
            else
            {
                if ( this.SelectedIndex != prevIndex )
                {
                    m_prevSelectedIndex = prevIndex;
                }
            }
        }

        public void SetItems( List<string> items )
        {
            m_ComboBox.BeginUpdate();
            
            m_ComboBox.Items.Clear();
            
            foreach ( string str in items )
            {
                m_ComboBox.Items.Add( str != null ? str : "<no name>" );
            }
            
            m_ComboBox.EndUpdate();

            m_selectedIndex = m_ComboBox.SelectedIndex;
        }

        public void SetItem( int index, string item )
        {
            while ( m_ComboBox.Items.Count < index + 1 )
            {
                m_ComboBox.Items.Add( "<no name>" );
            }

            if ( item != null )
            {
                m_ComboBox.Items[index] = item;
            }
        }

        public void ResizeItems( int numItems )
        {
            while ( m_ComboBox.Items.Count < numItems )
            {
                m_ComboBox.Items.Add( "<no name>" );
            }

            while ( m_ComboBox.Items.Count > numItems )
            {
                m_ComboBox.Items.RemoveAt( m_ComboBox.Items.Count - 1 );
            }
        }
        #endregion

        #region Private Functions
        private void SetSelectedIndex( int index, bool triggerEvent )
        {
            if ( m_selectedIndex == index || index >= m_ComboBox.Items.Count || index < 0)
            {
                return;
            }

            m_selectedIndex = index;

            EventHandler eh = new EventHandler( ComboBox_SelectedIndexChanged );
            m_ComboBox.SelectedIndexChanged -= eh;
            m_ComboBox.SelectedIndex = m_selectedIndex;
            m_ComboBox.SelectedIndexChanged += eh;

            if ( triggerEvent )
            {
                OnValueChanged();
            }
        }
        #endregion

        #region Designer generated code
        /// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.m_Label = new System.Windows.Forms.Label();
            this.m_ComboBox = new ComboBoxEx();
            this.SuspendLayout();
            // 
            // m_Label
            // 
            this.m_Label.Dock = System.Windows.Forms.DockStyle.Left;
            this.m_Label.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.m_Label.Location = new System.Drawing.Point( 0, 0 );
            this.m_Label.Name = "m_Label";
            this.m_Label.Size = new System.Drawing.Size( 157, 24 );
            this.m_Label.TabIndex = 0;
            this.m_Label.Text = "label1";
            this.m_Label.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // m_ComboBox
            // 
            this.m_ComboBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_ComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.m_ComboBox.Location = new System.Drawing.Point( 157, 0 );
            this.m_ComboBox.Name = "m_ComboBox";
            this.m_ComboBox.Size = new System.Drawing.Size( 282, 21 );
            this.m_ComboBox.TabIndex = 1;
            this.m_ComboBox.SelectedIndexChanged += new System.EventHandler( this.ComboBox_SelectedIndexChanged );
            this.m_ComboBox.KeyDown += new System.Windows.Forms.KeyEventHandler( this.ComboBox_KeyDown );
            // 
            // ControlCombo
            // 
            this.Controls.Add( this.m_ComboBox );
            this.Controls.Add( this.m_Label );
            this.Name = "ControlCombo";
            this.Size = new System.Drawing.Size( 439, 24 );
            this.ResumeLayout( false );

		}
		#endregion
	}

    #region Helper Classes
    [Serializable]
    public class ComboClipboardData : Object
    {
        public ComboClipboardData( string value )
        {
            m_value = value;
        }

        #region Variables
        private string m_value;
        #endregion

        #region Properties
        public string Value
        {
            get
            {
                return m_value;
            }
        }
        #endregion
    };
    #endregion
}

