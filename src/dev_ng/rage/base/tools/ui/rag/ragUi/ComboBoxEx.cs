﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ragUi
{
    /// <summary>
    /// Custom combo box that fixes problem with the contents being cleared on a background thread.
    /// </summary>
    public class ComboBoxEx : ComboBox
    {
        /// <summary>
        /// 
        /// </summary>
        public override int SelectedIndex
        {
            get { return Items.Count > 0 ? base.SelectedIndex : -1; }
            set
            {
                if (Items.Count > 0)
                {
                    base.SelectedIndex = value;
                }
            }
        }
    }
}
