using System;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Windows.Forms;
using Microsoft.Win32;

namespace ragUi
{
    /// <summary>
    /// Summary description for ControlColor.
    /// </summary>
    public class ControlColor : ControlBase
    {
        private System.Windows.Forms.Panel m_PanelColor;
        private System.Windows.Forms.Label m_Label;
        private ReadOnlyNumericUpDown m_NumericUpDownAlpha;
        private ReadOnlyNumericUpDown m_NumericUpDownRed;
        private ReadOnlyNumericUpDown m_NumericUpDownGreen;
        private ReadOnlyNumericUpDown m_NumericUpDownBlue;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TrackBar m_TrackBarV;
        private System.Windows.Forms.TrackBar m_TrackBarBlue;
        private System.Windows.Forms.TrackBar m_TrackBarGreen;
        private System.Windows.Forms.TrackBar m_TrackBarH;
        private System.Windows.Forms.TrackBar m_TrackBarS;
        private System.Windows.Forms.TrackBar m_TrackBarRed;
        private System.Windows.Forms.PictureBox m_PictureBoxColorRamp;
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.Container components = null;

        public ControlColor()
            : base()
        {
            // This call is required by the Windows.Forms Form Designer.
            InitializeComponent();

            // TODO: Add any initialization after the InitializeComponent call
            if ( sm_BitmapColorRamp == null )
            {
                sm_BitmapColorRamp = new Bitmap( m_PictureBoxColorRamp.Image );
            }

            m_PrevWidth = this.Size.Width;

            // this forces the value to change, so it forces the Extended property
            // to update the size
            m_Extended = !sm_DefaultToExtended;
            this.Extended = sm_DefaultToExtended;
        }

        #region Variables
        private static Bitmap sm_BitmapColorRamp = null;
        private static bool sm_DefaultToExtended = false;
        private static bool sm_TriedToLoadColorPicker = false;
        private static Assembly sm_ColorPickerDll;
        private static Type sm_ColorPickerType;

        private bool m_MouseDown = false;
        private int m_PrevWidth;
        private bool m_Extended = false;
        private bool m_trackbarToggle = false;

        private Color m_value;
        private Color m_prevValue;
        private bool m_firstTime = true;
        #endregion

        #region Properties
        public bool Alpha
        {
            set
            {
                m_NumericUpDownAlpha.Visible = value;

                if ( !value )
                {
                    EventHandler eh = new EventHandler( NumericUpdateDown_ValueChanged );
                    m_NumericUpDownAlpha.ValueChanged -= eh;
                    m_NumericUpDownAlpha.Value = 255;
                    m_NumericUpDownAlpha.ValueChanged += eh;

                    m_PanelColor.BackColor = Color.FromArgb( 255, m_PanelColor.BackColor.R, m_PanelColor.BackColor.G, m_PanelColor.BackColor.B );
                }
            }
            get
            {
                return m_NumericUpDownAlpha.Visible;
            }
        }

        public byte A
        {
            get
            {
                return m_value.A;
            }
            set
            {
                SetARGB( value, this.R, this.G, this.B, true );
            }
        }
        
        public byte R
        {
            get
            {
                return m_value.R;
            }
            set
            {
                SetARGB( this.A, value, this.G, this.B, true );
            }
        }
        
        public byte G
        {
            get
            {
                return m_value.G;
            }
            set
            {
                SetARGB( this.A, this.R, value, this.B, true );
            }
        }
        
        public byte B
        {
            get
            {
                return m_value.B;
            }
            set
            {
                SetARGB( this.A, this.R, this.G, value, true );
            }
        }

        [DefaultValue( false )]
        public bool Extended
        {
            get
            {
                return m_Extended;
            }
            set
            {
                if ( m_Extended != value )
                {
                    m_Extended = value;
                    ToggleExtended( m_Extended, m_Extended ? 96 : 30 );
                }
            }
        }

        public static bool DefaultToExtended
        {
            set
            {
                sm_DefaultToExtended = value;
            }
        }

        public override bool IsReadOnly
        {
            get { return m_NumericUpDownAlpha.ReadOnly; }
            set
            {
                m_PanelColor.Enabled = !value;
                m_NumericUpDownAlpha.ReadOnly = value;
                m_NumericUpDownRed.ReadOnly = value;
                m_NumericUpDownGreen.ReadOnly = value;
                m_NumericUpDownBlue.ReadOnly = value;
                m_TrackBarH.Enabled = !value;
                m_TrackBarS.Enabled = !value;
                m_TrackBarV.Enabled = !value;
                m_TrackBarRed.Enabled = !value;
                m_TrackBarGreen.Enabled = !value;
                m_TrackBarBlue.Enabled = !value;
                m_PictureBoxColorRamp.Enabled = !value;
            }
        }
        #endregion

        #region Overrides
        public override bool CanCopy
        {
            get
            {
                return true;
            }
        }

        public override bool CanPaste
        {
            get
            {
                return true;
            }
        }

        public override bool CanUndo
        {
            get
            {
                return true;
            }
        }

        public override object ClipboardData
        {
            get
            {
				return m_value.R +" "+ m_value.G +" "+ m_value.B +" "+ m_value.A;
            }
            set
            {
				if ((value != null) && (value is string))
				{
					// Ok it is a string.  Are there any numbers in the string?
					string strClipboardString = value as string;
					string[] astrStringParts = strClipboardString.Split();
					int i = 0;
					byte iR = m_value.R;
					byte iG = m_value.G;
					byte iB = m_value.B;
					byte iA = m_value.A;
					foreach (string strPossibleNumber in astrStringParts)
					{
						byte iResult;
						if (byte.TryParse(strPossibleNumber, out iResult))
						{
							switch (i)
							{
								case 0: iR = iResult; break;
								case 1: iG = iResult; break;
								case 2: iB = iResult; break;
								case 3: iA = iResult; break;
								default: break;
							}
							i++;
							if (i == 4) break;
						}
					}
					SetARGB(iA, iR, iG, iB, true);
				}
			}
        }

        public override bool IsUndoable
        {
            get
            {
                return base.IsUndoable && (m_prevValue != m_value);
            }
        }

        [Browsable( true ), Category( "Misc" ), DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
        public override string Text
        {
            set
            {
                m_Label.Text = value;
                PadControlTextForIcon( m_Label, m_Icon );
            }
            get
            {
                return m_Label.Text.TrimStart();
            }
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose( bool disposing )
        {
            if ( disposing )
            {
                if ( components != null )
                {
                    components.Dispose();
                }

                DisposeUpDown( m_NumericUpDownAlpha );
                DisposeUpDown( m_NumericUpDownRed );
                DisposeUpDown( m_NumericUpDownGreen );
                DisposeUpDown( m_NumericUpDownBlue );
            }
            base.Dispose( disposing );
        }

        public override void ConnectControlToXmlNode( System.Xml.XmlNode root )
        {
            DataBindings.Add( "R", FindXPathNode( root, ValueXPath + "/@x" ), "Value" );
            DataBindings.Add( "G", FindXPathNode( root, ValueXPath + "/@y" ), "Value" );
            DataBindings.Add( "B", FindXPathNode( root, ValueXPath + "/@z" ), "Value" );
            if ( Alpha )
            {
                DataBindings.Add( "A", FindXPathNode( root, ValueXPath + "/@w" ), "Value" );
            }
        }

        protected override void OnIconChanged()
        {
            PadControlTextForIcon( m_Label, m_Icon );
            m_Label.Image = m_Icon;
        }

        public override void Undo()
        {
            byte newA = m_prevValue.A;
            byte newR = m_prevValue.R;
            byte newG = m_prevValue.G;
            byte newB = m_prevValue.B;

            m_prevValue = m_value;

            SetARGB( newA, newR, newG, newB, true );
        }
        #endregion

        #region Event Handlers
        private void NumericUpdateDown_ValueChanged( object sender, EventArgs e )
        {
            m_prevValue = m_value;

            SetARGB( Convert.ToByte( this.m_NumericUpDownAlpha.Value ), Convert.ToByte( this.m_NumericUpDownRed.Value ),
                Convert.ToByte( this.m_NumericUpDownGreen.Value ), Convert.ToByte( this.m_NumericUpDownBlue.Value ), true );
        }

        private void NumericUpDown_Enter( object sender, EventArgs e )
        {
            if ( sender is NumericUpDown )
            {
                NumericUpDown numericUpDown = (NumericUpDown)sender;
                numericUpDown.Select( 0, numericUpDown.Text.Length );
            }
        }

        private void PictureBoxColorRamp_MouseDown( object sender, System.Windows.Forms.MouseEventArgs e )
        {
            if ( e.Button == MouseButtons.Left )
            {
                m_prevValue = m_value;
                m_MouseDown = true;

                // pass data on to MouseMove so we update the color right away
                PictureBoxColorRamp_MouseMove( sender, e );
            }
        }

        private void PictureBoxColorRamp_MouseUp( object sender, System.Windows.Forms.MouseEventArgs e )
        {
            if ( e.Button == MouseButtons.Left )
            {
                m_MouseDown = false;
            }
        }

        private void PictureBoxColorRamp_MouseMove( object sender, System.Windows.Forms.MouseEventArgs e )
        {
            if ( (e.Button == MouseButtons.Left) && m_MouseDown )
            {
                // clamp x:
                Int32 x = e.X;
                if ( x < 0 )
                {
                    x = 0;
                }
                else if ( x >= sm_BitmapColorRamp.Width )
                {
                    x = sm_BitmapColorRamp.Width - 1;
                }

                // clamp y:
                Int32 y = e.Y;
                if ( y < 0 )
                {
                    y = 0;
                }
                else if ( y >= sm_BitmapColorRamp.Height )
                {
                    y = sm_BitmapColorRamp.Height - 1;
                }

                Color color = sm_BitmapColorRamp.GetPixel( x, y );
                SetARGB( this.A, color.R, color.G, color.B, true );
            }
        }

        private void RGB_Scroll( object sender, System.EventArgs e )
        {
            if ( m_trackbarToggle )
            {
                // save this off when we first move the track bar so that we have something to Undo to.
                m_prevValue = m_value;
                m_trackbarToggle = false;
            }

            SetARGB( this.A, Convert.ToByte( m_TrackBarRed.Value ), Convert.ToByte( m_TrackBarGreen.Value ),
                Convert.ToByte( m_TrackBarBlue.Value ), true );
        }

        private void HSV_Scroll( object sender, System.EventArgs e )
        {
            if ( m_trackbarToggle )
            {
                // save this off when we first move the track bar so that we have something to Undo to.
                m_prevValue = m_value;
                m_trackbarToggle = false;
            }

            Byte r = 0, g = 0, b = 0;
            HSVtoRGB( (Byte)m_TrackBarH.Value, (Byte)m_TrackBarS.Value, (Byte)m_TrackBarV.Value, ref r, ref g, ref b );

            Byte h = 0, s = 0, v = 0;
            RGBtoHSV( r, g, b, ref h, ref s, ref v );

            if ( (byte)m_TrackBarH.Value != h )
            {
                throw (new Exception( "Hue should be the same.  Expected " + (byte)m_TrackBarH.Value + " but have " + h + "." ));
            }

            if ( (byte)m_TrackBarS.Value != s )
            {
                throw (new Exception( "Saturation should be the same.  Expected " + (byte)m_TrackBarS.Value + " but have " + s + "." ));
            }

            if ( (byte)m_TrackBarV.Value != v )
            {
                throw (new Exception( "Luminance should be the same.  Expected " + (byte)m_TrackBarV.Value + " but have " + v + "." ));
            }

            SetARGB( this.A, r, g, b, true );
        }

        private void TrackBar_KeyDown( object sender, KeyEventArgs e )
        {
            if ( e.Control )
            {
                switch ( e.KeyCode )
                {
                    case Keys.C:
                        CopyToClipboard();
                        e.SuppressKeyPress = true;
                        break;
                    case Keys.V:
                        PasteFromClipboard();
                        e.SuppressKeyPress = true;
                        break;
                    case Keys.Z:
                        Undo();
                        e.SuppressKeyPress = true;
                        break;
                    default:
                        break;
                }
            }
        }

        private void TrackBar_MouseDown( object sender, MouseEventArgs e )
        {
            if ( e.Button == MouseButtons.Left )
            {
                m_trackbarToggle = true;
            }
        }

        private void TrackBar_MouseUp( object sender, MouseEventArgs e )
        {
            if ( e.Button == MouseButtons.Left )
            {
                m_trackbarToggle = false;
            }
        }

        private void TrackBar_Leave( object sender, EventArgs e )
        {
            m_trackbarToggle = false;
        }

        private void ControlColor_SizeChanged( object sender, System.EventArgs e )
        {
            int newXOffset = this.Size.Width - m_PrevWidth;
            m_NumericUpDownAlpha.Left += newXOffset;
            m_NumericUpDownRed.Left += newXOffset;
            m_NumericUpDownGreen.Left += newXOffset;
            m_NumericUpDownBlue.Left += newXOffset;
            m_PanelColor.Left += newXOffset;
            m_PictureBoxColorRamp.Left += newXOffset;
            m_PrevWidth = this.Size.Width;
        }

        private void ControlColor_Enter( object sender, System.EventArgs e )
        {
            //ShowExtended();
        }

        private void ControlColor_Leave( object sender, System.EventArgs e )
        {
            //HideExtended();
        }

        private void OnColorChanged( object sender, EventArgs e )
        {
            PropertyInfo prop = sm_ColorPickerType.GetProperty( "CurrentColor" );
            Color c = (Color)prop.GetValue( sender, null );
            SetARGB( c.A, c.R, c.G, c.B, true );
        }

        private void OnChooseColor( object sender, System.Windows.Forms.MouseEventArgs e )
        {
            if ( (e.Button != MouseButtons.Left) || (e.Clicks != 1) )
            {
                return;
            }

            // This is the basic effect we're trying to achieve:
            //	Sano.PersonalProjects.ColorPicker.Controls.ColorPickerDialog dlg = new Sano.PersonalProjects.ColorPicker.Controls.ColorPickerDialog();
            //	dlg.SimpleColorChanged += new EventHandler(OnColorChanged);
            //	dlg.CurrentColor = Color.FromArgb(A, R, G, B);
            //	dlg.ShowDialog();

            // Check to see if a color picker dll is installed. If so, use that one, otherwise
            if ( (sm_ColorPickerDll == null) && (sm_TriedToLoadColorPicker == false) )
            {
                // try and load it
                try
                {
                    sm_ColorPickerDll = Assembly.LoadFrom( Path.GetDirectoryName( Path.GetFullPath( Application.ExecutablePath ) ) + @"\plugins\ColorPicker.dll" );
                }
                catch ( System.IO.FileNotFoundException )
                {
                    // do nothing
                }
            }

            sm_TriedToLoadColorPicker = true;

            bool showedPlugInPicker = false;
            if ( sm_ColorPickerDll != null )
            {
                // Create a new color picker, hook events up, and show it.

                // Could use Type.GetType, but this way we ignore namespaces
                foreach ( Type t in sm_ColorPickerDll.GetTypes() )
                {
                    if ( t.Name == "ColorPickerDialog" )
                    {
                        sm_ColorPickerType = t;
                        break;
                    }
                }

                if ( sm_ColorPickerType != null )
                {
                    try
                    {
                        Form dialog = sm_ColorPickerDll.CreateInstance( sm_ColorPickerType.FullName ) as Form;

                        PropertyInfo startPosition = sm_ColorPickerType.GetProperty( "StartPosition" );
                        startPosition.SetValue( dialog, FormStartPosition.CenterParent, null );

                        PropertyInfo useAlpha = sm_ColorPickerType.GetProperty( "UseAlpha" );
                        useAlpha.SetValue( dialog, this.Alpha, null );

                        PropertyInfo currColor = sm_ColorPickerType.GetProperty( "CurrentColor" );
                        currColor.SetValue( dialog, m_value, null );

                        EventInfo changedEvent = sm_ColorPickerType.GetEvent( "SimpleColorChanged" );
                        changedEvent.AddEventHandler( dialog, new EventHandler( OnColorChanged ) );

                        dialog.Text = Text;

                        m_prevValue = m_value;

                        dialog.ShowDialog( this );

                        showedPlugInPicker = true;
                    }
                    catch ( Exception )
                    {
                        // failed for some reason, so show the normal color picker
                    }
                }
            }

            if ( !showedPlugInPicker )
            {
                // fall back on windows color picker
                ColorDialog dlg = new ColorDialog();
                dlg.Color = m_value;
                dlg.FullOpen = true;

                m_prevValue = m_value;

                if ( dlg.ShowDialog( this ) == DialogResult.OK )
                {
                    // Leave alpha alone (since windows will set it to 255)
                    SetARGB( this.A, dlg.Color.R, dlg.Color.G, dlg.Color.B, true );
                }
            }
        }

        private void Label_DoubleClick( object sender, System.EventArgs e )
        {
            this.Extended = !this.Extended;
        }
        #endregion

        #region Public Functions
        public void UpdateValueFromRemote( byte a, byte r, byte g, byte b )
        {
            Color prevValue = m_value;

            SetARGB( a, r, g, b, false );

            if ( m_firstTime )
            {
                // since this is the first time, previous value will be the initial value.
                m_prevValue = m_value;
                m_firstTime = false;
            }
            else
            {
                if ( m_value != prevValue )
                {
                    m_prevValue = prevValue;
                }
            }
        }
        #endregion

        #region Private Functions
        private void RGBtoHSV( Byte rInt, Byte gInt, Byte bInt, ref Byte hInt, ref Byte sInt, ref Byte vInt )
        {
#if CRAP
			float r, g, b;
			float x,y,z;
			float dx;
			float m;
			float h;

			r = ((float)rInt)/255.0f;
			g = ((float)gInt)/255.0f;
			b = ((float)bInt)/255.0f;

			z = r; if (z < g) z = g; if (z < b) z = b;
			m = r; if (m > g) m = g; if (m > b) m = b;

			dx = z - m;

			if (dx == 0.0f)
			{
				x = 0.0f;
				y = 0.0f;
			}
			else
			{
				y = dx / z;
				if (r == z)
				{
					h = (z - b) / dx - (z - g) / dx;
				}
				else if (g == z)
				{
					h = (z - r) / dx - (z - b) / dx + 2.0f;
				}
				else
				{
					h = (z - g) / dx - (z - r) / dx + 4.0f;
				}

				if (h < 0.0f)
				{	
					h += 6.0f;
				}

				x = h / 6.0f;
			}

			hInt = (Byte)(x*255.0f + 0.5f);
			sInt = (Byte)(y*255.0f + 0.5f);
			vInt = (Byte)(z*255.0f + 0.5f);
#endif
			float r, g, b;
            float min, max;
            float h, s, v;

            r = ((float)rInt) / 255.0f;
            g = ((float)gInt) / 255.0f;
            b = ((float)bInt) / 255.0f;

            min = r; if ( min < g ) min = g; if ( min < b ) min = b;
            max = r; if ( max > g ) max = g; if ( max > b ) max = b;

            v = max;				// v
            float delta = max - min;
            if ( max != 0.0f )
            {
                s = delta / max;		// s
            }
            else
            {
                // r = g = b = 0		// s = 0, v is undefined
                s = 0.0f;
                h = 0.0f;
                return;
            }

            if ( r == max )
            {
                h = (g - b) / delta;		// between yellow & magenta
            }
            else if ( g == max )
            {
                h = 2.0f + (b - r) / delta;	// between cyan & yellow
            }
            else
            {
                h = 4 + (r - g) / delta;	// between magenta & cyan
            }

            h *= 60;				// degrees
            if ( h < 0 )
            {
                h += 360;
            }

            h = h / 360.0f;

            hInt = (Byte)(h * 255.0f + 0.5f);
			sInt = (Byte)(s * 255.0f + 0.5f);
			vInt = (Byte)(v * 255.0f + 0.5f);
        }

        private void HSVtoRGB( Byte hInt, Byte sInt, Byte vInt, ref Byte rInt, ref Byte gInt, ref Byte bInt )
        {
            float p;
            float q;
            float t;
            float h;
            float f;
            float x, y, z;
            int i;

            x = ((float)hInt) / 255.0f;
            y = ((float)sInt) / 255.0f;
            z = ((float)vInt) / 255.0f;

            if ( y == 0.0f )
            {
                x = y = z;
            }
            else
            {
                h = ((float)Math.IEEERemainder( x, 1.0f )) * 60.0f;
                i = (int)h;
                f = h - (float)i;
                p = z * (1.0f - y);
                q = z * (1.0f - (y * f));
                t = z * (1.0f - (y * (1.0f - f)));
                switch ( i )
                {
                    case 0:
                        x = z;
                        y = t;
                        z = p;
                        break;
                    case 1:
                        x = q;
                        y = z;
                        z = p;
                        break;
                    case 2:
                        x = p;
                        y = z;
                        z = t;
                        break;
                    case 3:
                        x = p;
                        y = q;
                        break;
                    case 4:
                        x = t;
                        y = p;
                        break;
                    case 5:
                        x = z;
                        y = p;
                        z = q;
                        break;
                    default:
                        break;
                }
            }

			rInt = (Byte)(x * 255.0f + 0.5f);
			gInt = (Byte)(y * 255.0f + 0.5f);
			bInt = (Byte)(z * 255.0f + 0.5f);
        }

        private void SetARGB( byte a, byte r, byte g, byte b, bool triggerEvent )
        {
            Color color = Color.FromArgb( a, r, g, b );
            if ( m_value == color )
            {
                return;
            }

            m_value = color;

            EventHandler eh;
            eh = new EventHandler( NumericUpdateDown_ValueChanged );
            m_NumericUpDownAlpha.ValueChanged -= eh;
            m_NumericUpDownRed.ValueChanged -= eh;
            m_NumericUpDownGreen.ValueChanged -= eh;
            m_NumericUpDownBlue.ValueChanged -= eh;
            m_NumericUpDownAlpha.Value = m_value.A;
            m_NumericUpDownRed.Value = m_value.R;
            m_NumericUpDownGreen.Value = m_value.G;
            m_NumericUpDownBlue.Value = m_value.B;
            m_NumericUpDownAlpha.ValueChanged += eh;
            m_NumericUpDownRed.ValueChanged += eh;
            m_NumericUpDownGreen.ValueChanged += eh;
            m_NumericUpDownBlue.ValueChanged += eh;

            eh = new EventHandler( RGB_Scroll );
            m_TrackBarRed.ValueChanged -= eh;
            m_TrackBarGreen.ValueChanged -= eh;
            m_TrackBarBlue.ValueChanged -= eh;
            m_TrackBarRed.Value = m_value.R;
            m_TrackBarGreen.Value = m_value.G;
            m_TrackBarBlue.Value = m_value.B;
            m_TrackBarRed.ValueChanged += eh;
            m_TrackBarGreen.ValueChanged += eh;
            m_TrackBarBlue.ValueChanged += eh;

            byte h = 0, s = 0, v = 0;
            RGBtoHSV( m_value.R, m_value.G, m_value.B, ref h, ref s, ref v );

            eh = new EventHandler( HSV_Scroll );
            m_TrackBarH.ValueChanged -= eh;
            m_TrackBarS.ValueChanged -= eh;
            m_TrackBarV.ValueChanged -= eh;
            m_TrackBarH.Value = h;
            m_TrackBarS.Value = s;
            m_TrackBarV.Value = v;
            m_TrackBarH.ValueChanged += eh;
            m_TrackBarS.ValueChanged += eh;
            m_TrackBarV.ValueChanged += eh;

            m_PanelColor.BackColor = m_value;

            if ( triggerEvent )
            {
                OnValueChanged();
            }
        }

        private void ToggleExtended( bool show, int height )
        {
            m_PictureBoxColorRamp.Visible = show;
            m_TrackBarRed.Visible = show;
            m_TrackBarGreen.Visible = show;
            m_TrackBarBlue.Visible = show;
            this.Height = height;
        }

        private void HideExtended()
        {
            this.Extended = false;
        }

        private void ShowExtended()
        {
            this.Extended = true;
        }
        #endregion

        #region Static Functions
        static private void DisposeUpDown( UpDownBase obj )
        {
            // this is lame - this event is added to a global event, so it's 
            // not properly garbage collected!
            SystemEvents.UserPreferenceChanged -=
                (UserPreferenceChangedEventHandler)Delegate.CreateDelegate(
                typeof( UserPreferenceChangedEventHandler ), obj, "UserPreferenceChanged" );
        }
        #endregion

        #region Component Designer generated code
        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager( typeof( ControlColor ) );
            this.m_PanelColor = new System.Windows.Forms.Panel();
            this.m_Label = new System.Windows.Forms.Label();
            this.m_NumericUpDownAlpha = new ReadOnlyNumericUpDown();
            this.m_NumericUpDownRed = new ReadOnlyNumericUpDown();
            this.m_NumericUpDownGreen = new ReadOnlyNumericUpDown();
            this.m_NumericUpDownBlue = new ReadOnlyNumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.m_TrackBarV = new System.Windows.Forms.TrackBar();
            this.m_TrackBarBlue = new System.Windows.Forms.TrackBar();
            this.m_TrackBarGreen = new System.Windows.Forms.TrackBar();
            this.m_TrackBarH = new System.Windows.Forms.TrackBar();
            this.m_TrackBarS = new System.Windows.Forms.TrackBar();
            this.m_TrackBarRed = new System.Windows.Forms.TrackBar();
            this.m_PictureBoxColorRamp = new System.Windows.Forms.PictureBox();
            ( (System.ComponentModel.ISupportInitialize)( this.m_NumericUpDownAlpha ) ).BeginInit();
            ( (System.ComponentModel.ISupportInitialize)( this.m_NumericUpDownRed ) ).BeginInit();
            ( (System.ComponentModel.ISupportInitialize)( this.m_NumericUpDownGreen ) ).BeginInit();
            ( (System.ComponentModel.ISupportInitialize)( this.m_NumericUpDownBlue ) ).BeginInit();
            ( (System.ComponentModel.ISupportInitialize)( this.m_TrackBarV ) ).BeginInit();
            ( (System.ComponentModel.ISupportInitialize)( this.m_TrackBarBlue ) ).BeginInit();
            ( (System.ComponentModel.ISupportInitialize)( this.m_TrackBarGreen ) ).BeginInit();
            ( (System.ComponentModel.ISupportInitialize)( this.m_TrackBarH ) ).BeginInit();
            ( (System.ComponentModel.ISupportInitialize)( this.m_TrackBarS ) ).BeginInit();
            ( (System.ComponentModel.ISupportInitialize)( this.m_TrackBarRed ) ).BeginInit();
            ( (System.ComponentModel.ISupportInitialize)( this.m_PictureBoxColorRamp ) ).BeginInit();
            this.SuspendLayout();
            // 
            // m_PanelColor
            // 
            this.m_PanelColor.Anchor = ( (System.Windows.Forms.AnchorStyles)( ( System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right ) ) );
            this.m_PanelColor.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.m_PanelColor.Location = new System.Drawing.Point( 136, 8 );
            this.m_PanelColor.Name = "m_PanelColor";
            this.m_PanelColor.Size = new System.Drawing.Size( 20, 20 );
            this.m_PanelColor.TabIndex = 3;
            this.m_PanelColor.MouseUp += new System.Windows.Forms.MouseEventHandler( this.OnChooseColor );
            // 
            // m_Label
            // 
            this.m_Label.Anchor = ( (System.Windows.Forms.AnchorStyles)( ( ( System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left )
                        | System.Windows.Forms.AnchorStyles.Right ) ) );
            this.m_Label.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.m_Label.Location = new System.Drawing.Point( 0, 8 );
            this.m_Label.Name = "m_Label";
            this.m_Label.Size = new System.Drawing.Size( 120, 16 );
            this.m_Label.TabIndex = 4;
            this.m_Label.DoubleClick += new System.EventHandler( this.Label_DoubleClick );
            // 
            // m_NumericUpDownAlpha
            // 
            this.m_NumericUpDownAlpha.Anchor = ( (System.Windows.Forms.AnchorStyles)( ( System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right ) ) );
            this.m_NumericUpDownAlpha.BackColor = System.Drawing.Color.White;
            this.m_NumericUpDownAlpha.Font = new System.Drawing.Font( "Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( (byte)( 0 ) ) );
            this.m_NumericUpDownAlpha.ForeColor = System.Drawing.Color.Black;
            this.m_NumericUpDownAlpha.Location = new System.Drawing.Point(304, 8);
            this.m_NumericUpDownAlpha.Maximum = new decimal( new int[] {
            255,
            0,
            0,
            0} );
            this.m_NumericUpDownAlpha.Name = "m_NumericUpDownAlpha";
            this.m_NumericUpDownAlpha.Size = new System.Drawing.Size( 48, 20 );
            this.m_NumericUpDownAlpha.TabIndex = 5;
            this.m_NumericUpDownAlpha.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.m_NumericUpDownAlpha.Value = new decimal( new int[] {
            255,
            0,
            0,
            0} );
            this.m_NumericUpDownAlpha.ValueChanged += new System.EventHandler( this.NumericUpdateDown_ValueChanged );
            this.m_NumericUpDownAlpha.Enter += new System.EventHandler( this.NumericUpDown_Enter );
            // 
            // m_NumericUpDownRed
            // 
            this.m_NumericUpDownRed.Anchor = ( (System.Windows.Forms.AnchorStyles)( ( System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right ) ) );
            this.m_NumericUpDownRed.BackColor = System.Drawing.Color.White;
            this.m_NumericUpDownRed.Font = new System.Drawing.Font( "Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( (byte)( 0 ) ) );
            this.m_NumericUpDownRed.ForeColor = System.Drawing.Color.Red;
            this.m_NumericUpDownRed.Location = new System.Drawing.Point(160, 8);
            this.m_NumericUpDownRed.Maximum = new decimal( new int[] {
            255,
            0,
            0,
            0} );
            this.m_NumericUpDownRed.Name = "m_NumericUpDownRed";
            this.m_NumericUpDownRed.Size = new System.Drawing.Size( 48, 20 );
            this.m_NumericUpDownRed.TabIndex = 5;
            this.m_NumericUpDownRed.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.m_NumericUpDownRed.ValueChanged += new System.EventHandler( this.NumericUpdateDown_ValueChanged );
            this.m_NumericUpDownRed.Enter += new System.EventHandler( this.NumericUpDown_Enter );
            // 
            // m_NumericUpDownGreen
            // 
            this.m_NumericUpDownGreen.Anchor = ( (System.Windows.Forms.AnchorStyles)( ( System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right ) ) );
            this.m_NumericUpDownGreen.BackColor = System.Drawing.Color.White;
            this.m_NumericUpDownGreen.Font = new System.Drawing.Font( "Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( (byte)( 0 ) ) );
            this.m_NumericUpDownGreen.ForeColor = System.Drawing.Color.Green;
            this.m_NumericUpDownGreen.Location = new System.Drawing.Point(208, 8);
            this.m_NumericUpDownGreen.Maximum = new decimal( new int[] {
            255,
            0,
            0,
            0} );
            this.m_NumericUpDownGreen.Name = "m_NumericUpDownGreen";
            this.m_NumericUpDownGreen.Size = new System.Drawing.Size( 48, 20 );
            this.m_NumericUpDownGreen.TabIndex = 5;
            this.m_NumericUpDownGreen.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.m_NumericUpDownGreen.ValueChanged += new System.EventHandler( this.NumericUpdateDown_ValueChanged );
            this.m_NumericUpDownGreen.Enter += new System.EventHandler( this.NumericUpDown_Enter );
            // 
            // m_NumericUpDownBlue
            // 
            this.m_NumericUpDownBlue.Anchor = ( (System.Windows.Forms.AnchorStyles)( ( System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right ) ) );
            this.m_NumericUpDownBlue.BackColor = System.Drawing.Color.White;
            this.m_NumericUpDownBlue.Font = new System.Drawing.Font( "Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( (byte)( 0 ) ) );
            this.m_NumericUpDownBlue.ForeColor = System.Drawing.Color.Blue;
            this.m_NumericUpDownBlue.Location = new System.Drawing.Point(256, 8);
            this.m_NumericUpDownBlue.Maximum = new decimal( new int[] {
            255,
            0,
            0,
            0} );
            this.m_NumericUpDownBlue.Name = "m_NumericUpDownBlue";
            this.m_NumericUpDownBlue.Size = new System.Drawing.Size( 48, 20 );
            this.m_NumericUpDownBlue.TabIndex = 5;
            this.m_NumericUpDownBlue.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.m_NumericUpDownBlue.ValueChanged += new System.EventHandler( this.NumericUpdateDown_ValueChanged );
            this.m_NumericUpDownBlue.Enter += new System.EventHandler( this.NumericUpDown_Enter );
            // 
            // label1
            // 
            this.label1.Anchor = ( (System.Windows.Forms.AnchorStyles)( ( System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left ) ) );
            this.label1.Location = new System.Drawing.Point( 8, 32 );
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size( 112, 16 );
            this.label1.TabIndex = 6;
            this.label1.Text = "R   G   B                 ";
            // 
            // m_TrackBarV
            // 
            this.m_TrackBarV.Anchor = ( (System.Windows.Forms.AnchorStyles)( ( System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left ) ) );
            this.m_TrackBarV.AutoSize = false;
            this.m_TrackBarV.Cursor = System.Windows.Forms.Cursors.Hand;
            this.m_TrackBarV.Location = new System.Drawing.Point( 104, 48 );
            this.m_TrackBarV.Maximum = 255;
            this.m_TrackBarV.Name = "m_TrackBarV";
            this.m_TrackBarV.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.m_TrackBarV.Size = new System.Drawing.Size( 16, 48 );
            this.m_TrackBarV.TabIndex = 0;
            this.m_TrackBarV.TickFrequency = 0;
            this.m_TrackBarV.Visible = false;
            this.m_TrackBarV.Leave += new System.EventHandler( this.TrackBar_Leave );
            this.m_TrackBarV.Scroll += new System.EventHandler( this.HSV_Scroll );
            this.m_TrackBarV.MouseDown += new System.Windows.Forms.MouseEventHandler( this.TrackBar_MouseDown );
            this.m_TrackBarV.MouseUp += new System.Windows.Forms.MouseEventHandler( this.TrackBar_MouseUp );
            this.m_TrackBarV.KeyDown += new System.Windows.Forms.KeyEventHandler( this.TrackBar_KeyDown );
            // 
            // m_TrackBarBlue
            // 
            this.m_TrackBarBlue.Anchor = ( (System.Windows.Forms.AnchorStyles)( ( System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left ) ) );
            this.m_TrackBarBlue.AutoSize = false;
            this.m_TrackBarBlue.Cursor = System.Windows.Forms.Cursors.Hand;
            this.m_TrackBarBlue.Location = new System.Drawing.Point( 40, 48 );
            this.m_TrackBarBlue.Maximum = 255;
            this.m_TrackBarBlue.Name = "m_TrackBarBlue";
            this.m_TrackBarBlue.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.m_TrackBarBlue.Size = new System.Drawing.Size( 16, 48 );
            this.m_TrackBarBlue.TabIndex = 0;
            this.m_TrackBarBlue.TickFrequency = 0;
            this.m_TrackBarBlue.Leave += new System.EventHandler( this.TrackBar_Leave );
            this.m_TrackBarBlue.Scroll += new System.EventHandler( this.RGB_Scroll );
            this.m_TrackBarBlue.MouseDown += new System.Windows.Forms.MouseEventHandler( this.TrackBar_MouseDown );
            this.m_TrackBarBlue.MouseUp += new System.Windows.Forms.MouseEventHandler( this.TrackBar_MouseUp );
            this.m_TrackBarBlue.KeyDown += new System.Windows.Forms.KeyEventHandler( this.TrackBar_KeyDown );
            // 
            // m_TrackBarGreen
            // 
            this.m_TrackBarGreen.Anchor = ( (System.Windows.Forms.AnchorStyles)( ( System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left ) ) );
            this.m_TrackBarGreen.AutoSize = false;
            this.m_TrackBarGreen.Cursor = System.Windows.Forms.Cursors.Hand;
            this.m_TrackBarGreen.Location = new System.Drawing.Point( 24, 48 );
            this.m_TrackBarGreen.Maximum = 255;
            this.m_TrackBarGreen.Name = "m_TrackBarGreen";
            this.m_TrackBarGreen.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.m_TrackBarGreen.Size = new System.Drawing.Size( 16, 48 );
            this.m_TrackBarGreen.TabIndex = 0;
            this.m_TrackBarGreen.TickFrequency = 0;
            this.m_TrackBarGreen.Leave += new System.EventHandler( this.TrackBar_Leave );
            this.m_TrackBarGreen.Scroll += new System.EventHandler( this.RGB_Scroll );
            this.m_TrackBarGreen.MouseDown += new System.Windows.Forms.MouseEventHandler( this.TrackBar_MouseDown );
            this.m_TrackBarGreen.MouseUp += new System.Windows.Forms.MouseEventHandler( this.TrackBar_MouseUp );
            this.m_TrackBarGreen.KeyDown += new System.Windows.Forms.KeyEventHandler( this.TrackBar_KeyDown );
            // 
            // m_TrackBarH
            // 
            this.m_TrackBarH.Anchor = ( (System.Windows.Forms.AnchorStyles)( ( System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left ) ) );
            this.m_TrackBarH.AutoSize = false;
            this.m_TrackBarH.Cursor = System.Windows.Forms.Cursors.Hand;
            this.m_TrackBarH.Location = new System.Drawing.Point( 72, 48 );
            this.m_TrackBarH.Maximum = 255;
            this.m_TrackBarH.Name = "m_TrackBarH";
            this.m_TrackBarH.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.m_TrackBarH.Size = new System.Drawing.Size( 16, 48 );
            this.m_TrackBarH.TabIndex = 0;
            this.m_TrackBarH.TickFrequency = 0;
            this.m_TrackBarH.Visible = false;
            this.m_TrackBarH.Leave += new System.EventHandler( this.TrackBar_Leave );
            this.m_TrackBarH.Scroll += new System.EventHandler( this.HSV_Scroll );
            this.m_TrackBarH.MouseDown += new System.Windows.Forms.MouseEventHandler( this.TrackBar_MouseDown );
            this.m_TrackBarH.MouseUp += new System.Windows.Forms.MouseEventHandler( this.TrackBar_MouseUp );
            this.m_TrackBarH.KeyDown += new System.Windows.Forms.KeyEventHandler( this.TrackBar_KeyDown );
            // 
            // m_TrackBarS
            // 
            this.m_TrackBarS.Anchor = ( (System.Windows.Forms.AnchorStyles)( ( System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left ) ) );
            this.m_TrackBarS.AutoSize = false;
            this.m_TrackBarS.Cursor = System.Windows.Forms.Cursors.Hand;
            this.m_TrackBarS.Location = new System.Drawing.Point( 88, 48 );
            this.m_TrackBarS.Maximum = 255;
            this.m_TrackBarS.Name = "m_TrackBarS";
            this.m_TrackBarS.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.m_TrackBarS.Size = new System.Drawing.Size( 16, 48 );
            this.m_TrackBarS.TabIndex = 0;
            this.m_TrackBarS.TickFrequency = 0;
            this.m_TrackBarS.Visible = false;
            this.m_TrackBarS.Leave += new System.EventHandler( this.TrackBar_Leave );
            this.m_TrackBarS.Scroll += new System.EventHandler( this.HSV_Scroll );
            this.m_TrackBarS.MouseDown += new System.Windows.Forms.MouseEventHandler( this.TrackBar_MouseDown );
            this.m_TrackBarS.MouseUp += new System.Windows.Forms.MouseEventHandler( this.TrackBar_MouseUp );
            this.m_TrackBarS.KeyDown += new System.Windows.Forms.KeyEventHandler( this.TrackBar_KeyDown );
            // 
            // m_TrackBarRed
            // 
            this.m_TrackBarRed.Anchor = ( (System.Windows.Forms.AnchorStyles)( ( System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left ) ) );
            this.m_TrackBarRed.AutoSize = false;
            this.m_TrackBarRed.Cursor = System.Windows.Forms.Cursors.Hand;
            this.m_TrackBarRed.Location = new System.Drawing.Point( 8, 48 );
            this.m_TrackBarRed.Maximum = 255;
            this.m_TrackBarRed.Name = "m_TrackBarRed";
            this.m_TrackBarRed.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.m_TrackBarRed.Size = new System.Drawing.Size( 16, 48 );
            this.m_TrackBarRed.TabIndex = 0;
            this.m_TrackBarRed.TickFrequency = 0;
            this.m_TrackBarRed.Leave += new System.EventHandler( this.TrackBar_Leave );
            this.m_TrackBarRed.Scroll += new System.EventHandler( this.RGB_Scroll );
            this.m_TrackBarRed.MouseDown += new System.Windows.Forms.MouseEventHandler( this.TrackBar_MouseDown );
            this.m_TrackBarRed.MouseUp += new System.Windows.Forms.MouseEventHandler( this.TrackBar_MouseUp );
            this.m_TrackBarRed.KeyDown += new System.Windows.Forms.KeyEventHandler( this.TrackBar_KeyDown );
            // 
            // m_PictureBoxColorRamp
            // 
            this.m_PictureBoxColorRamp.Anchor = ( (System.Windows.Forms.AnchorStyles)( ( System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right ) ) );
            this.m_PictureBoxColorRamp.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.m_PictureBoxColorRamp.Cursor = System.Windows.Forms.Cursors.Hand;
            this.m_PictureBoxColorRamp.Image = ( (System.Drawing.Image)( resources.GetObject( "m_PictureBoxColorRamp.Image" ) ) );
            this.m_PictureBoxColorRamp.Location = new System.Drawing.Point( 170, 40 );
            this.m_PictureBoxColorRamp.Name = "m_PictureBoxColorRamp";
            this.m_PictureBoxColorRamp.Size = new System.Drawing.Size( 185, 49 );
            this.m_PictureBoxColorRamp.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.m_PictureBoxColorRamp.TabIndex = 1;
            this.m_PictureBoxColorRamp.TabStop = false;
            this.m_PictureBoxColorRamp.MouseMove += new System.Windows.Forms.MouseEventHandler( this.PictureBoxColorRamp_MouseMove );
            this.m_PictureBoxColorRamp.MouseDown += new System.Windows.Forms.MouseEventHandler( this.PictureBoxColorRamp_MouseDown );
            this.m_PictureBoxColorRamp.MouseUp += new System.Windows.Forms.MouseEventHandler( this.PictureBoxColorRamp_MouseUp );
            // 
            // ControlColor
            // 
            this.Controls.Add( this.m_PanelColor );
            this.Controls.Add( this.label1 );
            this.Controls.Add( this.m_NumericUpDownAlpha );
            this.Controls.Add( this.m_Label );
            this.Controls.Add( this.m_PictureBoxColorRamp );
            this.Controls.Add( this.m_TrackBarRed );
            this.Controls.Add( this.m_TrackBarGreen );
            this.Controls.Add( this.m_TrackBarBlue );
            this.Controls.Add( this.m_NumericUpDownRed );
            this.Controls.Add( this.m_NumericUpDownGreen );
            this.Controls.Add( this.m_NumericUpDownBlue );
            this.Controls.Add( this.m_TrackBarH );
            this.Controls.Add( this.m_TrackBarS );
            this.Controls.Add( this.m_TrackBarV );
            this.Name = "ControlColor";
            this.Size = new System.Drawing.Size( 352, 96 );
            this.MouseLeave += new System.EventHandler( this.ControlColor_Leave );
            this.MouseEnter += new System.EventHandler( this.ControlColor_Enter );
            ( (System.ComponentModel.ISupportInitialize)( this.m_NumericUpDownAlpha ) ).EndInit();
            ( (System.ComponentModel.ISupportInitialize)( this.m_NumericUpDownRed ) ).EndInit();
            ( (System.ComponentModel.ISupportInitialize)( this.m_NumericUpDownGreen ) ).EndInit();
            ( (System.ComponentModel.ISupportInitialize)( this.m_NumericUpDownBlue ) ).EndInit();
            ( (System.ComponentModel.ISupportInitialize)( this.m_TrackBarV ) ).EndInit();
            ( (System.ComponentModel.ISupportInitialize)( this.m_TrackBarBlue ) ).EndInit();
            ( (System.ComponentModel.ISupportInitialize)( this.m_TrackBarGreen ) ).EndInit();
            ( (System.ComponentModel.ISupportInitialize)( this.m_TrackBarH ) ).EndInit();
            ( (System.ComponentModel.ISupportInitialize)( this.m_TrackBarS ) ).EndInit();
            ( (System.ComponentModel.ISupportInitialize)( this.m_TrackBarRed ) ).EndInit();
            ( (System.ComponentModel.ISupportInitialize)( this.m_PictureBoxColorRamp ) ).EndInit();
            this.ResumeLayout( false );
            this.PerformLayout();

        }
        #endregion
    }

    #region Helper Classes
    [Serializable]
    public class ColorClipboardData : Object
    {
        public ColorClipboardData( Color c )
        {
            m_color = c;
        }

        #region Variables
        private Color m_color;
        #endregion

        #region Properties
        public Color Color
        {
            get
            {
                return m_color;
            }
        }
        #endregion
    };
    #endregion
}
