namespace ragCore
{
    partial class XPander
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && (components != null) )
            {
                //m_CaptionFont.Dispose();
                //m_TriangleFont.Dispose();
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // XPander
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.Name = "XPander";
            this.ControlRemoved += new System.Windows.Forms.ControlEventHandler( this.XPander_ControlRemoved );
            this.ControlAdded += new System.Windows.Forms.ControlEventHandler( this.XPander_ControlAdded );
            this.ResumeLayout( false );

        }

        #endregion
    }
}
