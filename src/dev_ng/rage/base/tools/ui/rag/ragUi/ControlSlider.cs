using System;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using Microsoft.Win32;

namespace ragUi
{
	/// <summary>
	/// Summary description for ControlSlider.
	/// </summary>
    public class ControlSlider : ControlBase
    {
        private System.Windows.Forms.CheckBox m_Button;
        private ReadOnlyNumericUpDown m_NumericUpDown;
        private System.Windows.Forms.TrackBar m_TrackBar;
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.Container components = null;

        public ControlSlider()
        {
            // This call is required by the Windows.Forms Form Designer.
            InitializeComponent();

            //this.AllowDrop = true;
            //this.DragDrop += new DragEventHandler(this.DragDrop);
            //this.DragEnter += new DragEventHandler(this.DragEnterEH);
            //m_Button.DragEnter += new DragEventHandler(this.DragEnterEH);
            this.m_TrackBar.Maximum = (int)TRACK_BAR_SCALE_FACTOR;

            m_NumericUpDown.Minimum = Decimal.MinValue;
            m_NumericUpDown.Maximum = Decimal.MaxValue;
        }

        public ControlSlider( System.Xml.XmlNode schema )
        {
            InitializeComponent();

            this.Text = schema.Attributes["name"].Value;
            this.Maximum = System.Decimal.Parse( schema.Attributes["max"].Value );
            this.Minimum = System.Decimal.Parse( schema.Attributes["min"].Value );

            IsFloat = schema.Name == "float";
        }

        #region Constants
        private const decimal TRACK_BAR_SCALE_FACTOR = 1000.0M;
        #endregion

        #region Variables
        private delegate void ValueChangedDelegate();
        private bool m_FirstTime = true;
        private decimal m_Value;
        private decimal m_prevValue;

        private bool m_IsFloat;
        private bool m_UseTrackbar = true;
        private int m_LastX;
        private bool m_trackbarToggle = false;
        #endregion

        #region Properties
        [Browsable( true ), Category( "Misc" ), DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
        public override string Text
        {
            get
            {
                return m_Button.Text;
            }
            set
            {
                m_Button.Text = value;
            }
        }

        public decimal Minimum
        {
            get
            {
                return m_NumericUpDown.Minimum;
            }
            set
            {
                m_NumericUpDown.Minimum = value;
            }
        }

        public decimal Maximum
        {
            get
            {
                return m_NumericUpDown.Maximum;
            }
            set
            {
                m_NumericUpDown.Maximum = value;
            }
        }

        public bool IsFloat
        {
            get { return m_IsFloat; }
            set
            {
                m_IsFloat = value;
                if ( m_IsFloat )
                {
                    m_NumericUpDown.DecimalPlaces = 3;
                }
                else
                {
                    m_NumericUpDown.DecimalPlaces = 0;
                }
            }
        }

        public decimal Step
        {
            get
            {
                return m_NumericUpDown.Increment;
            }
            set
            {
                if ( value > 0 )
                {
                    m_NumericUpDown.Increment = value;
                }
                else
                {
                    m_NumericUpDown.Increment = 0M;
                }
            }
        }

        public bool UseTrackbar
        {
            get
            {
                return m_UseTrackbar;
            }
            set
            {
                if ( m_UseTrackbar != value )
                {
                    m_UseTrackbar = value;
                    this.m_TrackBar.Visible = m_UseTrackbar;
                    this.PerformLayout();
                }
            }
        }

        public decimal Value
        {
            get
            {
                return m_Value;
            }
            set
            {
                SetValueAndClamp( value, true );
            }
        }

        [Browsable( false )]
        public System.Windows.Forms.CheckBox ButtonControl
        {
            get
            {
                return m_Button;
            }
        }

        public override bool IsReadOnly
        {
            get { return this.m_NumericUpDown.ReadOnly; }
            set
            {
                this.m_Button.Enabled = !value;
                this.m_TrackBar.Enabled = !value;
                this.m_NumericUpDown.ReadOnly = value;
                if (value)
                {
                }
            }
        }
        #endregion

        #region Overrides
        public override bool CanCopy
        {
            get
            {
                return true;
            }
        }

        public override bool CanPaste
        {
            get
            {
                return true;
            }
        }

        public override bool CanUndo
        {
            get
            {
                return true;
            }
        }

        public override object ClipboardData
        {
            get
            {
				return this.Value.ToString();
            }
            set
            {
				if ((value != null) && (value is string))
				{
					// Ok it is a string.  Are there any numbers in the string?
					string strClipboardString = value as string;
					string[] astrStringParts = strClipboardString.Split();
					foreach (string strPossibleNumber in astrStringParts)
					{
						decimal fResult;
						if (decimal.TryParse(strPossibleNumber, out fResult))
						{
							decimal val = this.Value;
							this.Value = fResult;
							if (val != this.Value)
							{
								m_prevValue = val;
							}
						}
					}
				}
			}
        }

        public override bool IsClipboardDataAvailable
        {
            get
            {
                return base.IsClipboardDataAvailable;
            }
        }

        public override bool IsUndoable
        {
            get
            {
                return base.IsUndoable && (m_prevValue != m_Value);
            }
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose( bool disposing )
        {
            if ( disposing )
            {
                if ( components != null )
                {
                    components.Dispose();
                }

                DisposeUpDown( m_NumericUpDown );
            }
            base.Dispose( disposing );
        }

        protected override void OnIconChanged()
        {
            PadControlTextForIcon( m_Button, m_Icon );
            m_Button.Image = m_Icon;
            m_Button.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
        }

        protected override void OnLayout( LayoutEventArgs levent )
        {
            base.OnLayout( levent );

            bool useTrackbar = m_UseTrackbar && this.Width > 230;

            int controlPadding = 4;

            m_TrackBar.Visible = useTrackbar;
            if ( useTrackbar )
            {
                // Control looks like:
                // [ BTN | pad | Slider | pad | numeric ]

                int resizeWidth = Width - controlPadding * 2 - m_NumericUpDown.Width;

                int x = 0;
                m_Button.SetBounds( x, -1, resizeWidth / 2, -1, BoundsSpecified.Width | BoundsSpecified.X );

                x += resizeWidth / 2;
                x += controlPadding;

                m_TrackBar.SetBounds( x, -1, resizeWidth / 2, -1, BoundsSpecified.Width | BoundsSpecified.X );

                x += resizeWidth / 2;
                x += controlPadding;

                m_NumericUpDown.SetBounds( x, -1, -1, -1, BoundsSpecified.X );
            }
            else
            {
                // Control looks like:
                // [ BTN | pad | numeric ]

                int resizeWidth = Width - controlPadding - m_NumericUpDown.Width;

                m_Button.Width = resizeWidth;
                m_NumericUpDown.SetBounds( resizeWidth + controlPadding, -1, -1, -1, BoundsSpecified.X );
            }
        }

        public override void Undo()
        {
            decimal newValue = m_prevValue;

            m_prevValue = this.Value;

            this.Value = newValue;
        }
        #endregion

        #region Event Handlers
        public void DragEnterEH( object sender, DragEventArgs e )
        {
        }

        private void Button_KeyDown( object sender, KeyEventArgs e )
        {
            if ( e.Control )
            {
                switch ( e.KeyCode )
                {
                    case Keys.Z:
                        Undo();
                        e.SuppressKeyPress = true;
                        break;
                    case Keys.C:
                        CopyToClipboard();
                        e.SuppressKeyPress = true;
                        break;
                    case Keys.V:
                        PasteFromClipboard();
                        e.SuppressKeyPress = true;
                        break;
                    default:
                        break;
                }
            }
        }

        private void Button_MouseDown( object sender, System.Windows.Forms.MouseEventArgs e )
        {
            if ( e.Button == MouseButtons.Left )
            {
                if ( !this.IsReadOnly )
                {
                    m_Button.CheckState = CheckState.Checked;
                    m_Button.Capture = true;
                    m_Button.Cursor = Cursors.SizeWE;
                    m_LastX = e.X;
                    m_prevValue = this.Value;
                }
                else
                {
                    m_Button.CheckState = CheckState.Unchecked;
                }
            }
        }

        private void Button_MouseUp( object sender, System.Windows.Forms.MouseEventArgs e )
        {
            if ( e.Button == MouseButtons.Left )
            {
                if (!this.IsReadOnly)
                {
                    m_Button.CheckState = CheckState.Unchecked;
                    m_Button.Capture = false;
                    m_Button.Cursor = Cursors.Default;
                }
                else
                {
                    m_Button.CheckState = CheckState.Unchecked;
                }
            }
        }

        private void Button_MouseMove( object sender, System.Windows.Forms.MouseEventArgs e )
        {
            if (!this.IsReadOnly && m_Button.Capture && (e.Button == MouseButtons.Left))
            {
                if ( m_LastX > e.X )
                {
                    m_Button.Cursor = Cursors.PanWest;
                }
                else if ( m_LastX < e.X )
                {
                    m_Button.Cursor = Cursors.PanEast;
                }

                decimal scale = 0.25M;
                if ( Control.ModifierKeys != Keys.Control )
                {
                    scale *= 4.0M;
                }

                if ( Control.ModifierKeys != Keys.Shift )
                {
                    scale = 0.25M;
                }

                // update the value:
                decimal addition = (this.Step * (scale * Convert.ToDecimal( e.X - m_LastX )));
                if ( IsFloat || (Math.Abs( addition ) >= 1.0M) )
                {
                    SetValueAndClamp( this.Value + (decimal)addition, true );

                    // make sure we keep around the remainder:
                    if ( !IsFloat )
                    {
                        m_LastX = (int)(e.X - Math.IEEERemainder( Convert.ToDouble( addition ), Convert.ToDouble( Math.Floor( addition ) ) ) / Convert.ToDouble( this.Step * scale ));
                    }
                    else
                    {
                        m_LastX = e.X;
                    }
                }
            }
        }

        private void TrackBar_KeyDown( object sender, KeyEventArgs e )
        {
            if ( e.Control )
            {
                switch ( e.KeyCode )
                {
                    case Keys.C:
                        CopyToClipboard();
                        e.SuppressKeyPress = true;
                        break;
                    case Keys.V:
                        PasteFromClipboard();
                        e.SuppressKeyPress = true;
                        break;
                    case Keys.Z:
                        Undo();
                        e.SuppressKeyPress = true;
                        break;
                    default:
                        break;
                }
            }
        }

        private void TrackBar_MouseDown( object sender, MouseEventArgs e )
        {
            if ( e.Button == MouseButtons.Left )
            {
                m_trackbarToggle = true;
            }
        }

        private void TrackBar_MouseUp( object sender, MouseEventArgs e )
        {
            if ( e.Button == MouseButtons.Left )
            {
                m_trackbarToggle = false;
            }
        }

        private void TrackBar_Leave( object sender, EventArgs e )
        {
            m_trackbarToggle = false;
        }

        private void TrackBar_ValueChanged( object sender, EventArgs e )
        {
            try
            {
                if ( m_trackbarToggle )
                {
                    // save this off when we first move the track bar so that we have something to Undo to.
                    m_prevValue = this.Value;
                    m_trackbarToggle = false;
                }

                SetValueAndClamp( (m_TrackBar.Value / TRACK_BAR_SCALE_FACTOR) * (this.Maximum - this.Minimum) + this.Minimum, true );
            }
            catch ( System.OverflowException )
            {
                // no change to value
            }
        }

        private void NumericUpDown_ValueChanged( object sender, EventArgs e )
        {
            m_prevValue = this.Value;
            
            SetValueAndClamp( m_NumericUpDown.Value, true );
        }

        private void NumericUpDown_Enter( object sender, EventArgs e )
        {
            m_NumericUpDown.Select( 0, m_NumericUpDown.Text.Length );
        }
        #endregion

        #region Public Functions
        public void SetRange( float min, float max )
        {
            try
            {
                this.Minimum = Convert.ToDecimal( min );
            }
            catch 
            {
                if ( min < 0.0f )
                {
                    this.Minimum = decimal.MinValue;
                }
                else
                {
                    this.Minimum = decimal.MaxValue;
                }
            }

            try
            {
                this.Maximum = Convert.ToDecimal( max );
            }
            catch
            {
                if ( max < 0.0f )
                {
                    this.Maximum = decimal.MinValue;
                }
                else
                {
                    this.Maximum = decimal.MaxValue;
                }
            }
        }

        public void SetStep( float step )
        {
            try
            {
                this.Step = Convert.ToDecimal( step );
            }
            catch
            {
                if ( step < 0.0f )
                {
                    this.Step = decimal.MinValue;
                }
                else
                {
                    this.Step = decimal.MaxValue;
                }
            }
        }

        public void UpdateValueFromRemote(int value)
        {
            decimal prevValue = this.Value;
            decimal decimalValue = Convert.ToDecimal(value);

            SetValueAndClamp(decimalValue, false);

            if (m_FirstTime)
            {
                // since this is the first time, previous value will be the initial value.
                m_prevValue = this.Value;
                m_FirstTime = false;
            }
            else
            {
                if (this.Value != prevValue)
                {
                    m_prevValue = prevValue;
                }
            }
        }


        public void UpdateValueFromRemote( float value )
        {
            decimal prevValue = this.Value;

            decimal decimalValue = Convert.ToDecimal( value );
            SetValueAndClamp(decimalValue, false, m_FirstTime);

            if ( m_FirstTime )
            {
                // since this is the first time, previous value will be the initial value.
                m_prevValue = this.Value;
                m_FirstTime = false;
            }
            else
            {
                if ( this.Value != prevValue )
                {
                    m_prevValue = prevValue;
                }
            }
        }
        #endregion

        #region Private Functions
        private void DisposeUpDown( UpDownBase obj )
        {
            // Manual fix to a memory leak in .NET with NumericUpDown controls
            SystemEvents.UserPreferenceChanged -=
                (UserPreferenceChangedEventHandler)Delegate.CreateDelegate(
                typeof( UserPreferenceChangedEventHandler ), obj, "UserPreferenceChanged" );
        }

        private void SetValueAndClamp( decimal newValue, bool triggerEvent, bool forceSet = false )
        {
            if (!this.IsReadOnly)
            {
                newValue = Math.Min( Math.Max( newValue, this.Minimum ), this.Maximum );
            }
            
            if ( !this.IsFloat )
            {
                newValue = Math.Round( newValue );
            }

            if ( forceSet == false && this.Value == newValue )
            {
                return;
            }

            m_Value = newValue;

            if ( this.IsReadOnly )
            {
                if ( m_Value < this.Minimum )
                {
                    this.Minimum = m_Value;
                }

                if ( m_Value > this.Maximum )
                {
                    this.Maximum = m_Value;
                }
            }

            EventHandler eh = NumericUpDown_ValueChanged;
            m_NumericUpDown.ValueChanged -= eh;

            if (m_NumericUpDown.InvokeRequired == true)
            {
                ValueChangedDelegate valueChangedCallback = delegate()
                {
                    m_NumericUpDown.Value = m_Value;
                };

                m_NumericUpDown.Invoke( valueChangedCallback ); 
            }
            else
            {
                m_NumericUpDown.Value = m_Value;
            }
            m_NumericUpDown.ValueChanged += eh;

            eh = TrackBar_ValueChanged;
            m_TrackBar.ValueChanged -= eh;
            if ( this.Maximum == this.Minimum )
            {
                m_TrackBar.Value = Convert.ToInt32( TRACK_BAR_SCALE_FACTOR * 0.5M );
            }
            else
            {
                try
                {
                    int val = Convert.ToInt32( ((this.Value - this.Minimum) * TRACK_BAR_SCALE_FACTOR) / (this.Maximum - this.Minimum) );
                    if ( val > m_TrackBar.Maximum )
                    {
                        val = m_TrackBar.Maximum;
                    }
                    else if ( val < m_TrackBar.Minimum )
                    {
                        val = m_TrackBar.Minimum;
                    }

                    if (m_TrackBar.InvokeRequired == true)
                    {
                           ValueChangedDelegate valueChangedCallback = delegate()
                            {
                                m_TrackBar.Value = val;
                            };

                            m_TrackBar.Invoke( valueChangedCallback ); 
                    }
                    else
                    {
                        m_TrackBar.Value = val;
                    }
                }
                catch ( System.OverflowException )
                {
                    m_TrackBar.Value = 0;
                }
                catch ( System.ArgumentException )
                {
                    if ( this.Value < this.Minimum )
                    {
                        m_TrackBar.Value = 0;
                    }
                    else if ( this.Value > this.Maximum )
                    {
                        m_TrackBar.Value = (int)TRACK_BAR_SCALE_FACTOR;
                    }
                }
            }
            m_TrackBar.ValueChanged += eh;

            if ( triggerEvent )
            {
                OnValueChanged();
            }
        }
        #endregion

        #region Component Designer generated code
        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.m_Button = new System.Windows.Forms.CheckBox();
            this.m_TrackBar = new System.Windows.Forms.TrackBar();
            this.m_NumericUpDown = new ReadOnlyNumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.m_TrackBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_NumericUpDown)).BeginInit();
            this.SuspendLayout();
            // 
            // m_Button
            // 
            this.m_Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.m_Button.Appearance = System.Windows.Forms.Appearance.Button;
            this.m_Button.Location = new System.Drawing.Point(0, 0);
            this.m_Button.Name = "m_Button";
            this.m_Button.Size = new System.Drawing.Size(82, 23);
            this.m_Button.TabIndex = 0;
            this.m_Button.TabStop = false;
            this.m_Button.Text = "label1";
            this.m_Button.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Button_KeyDown);
            this.m_Button.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Button_MouseDown);
            this.m_Button.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Button_MouseMove);
            this.m_Button.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Button_MouseUp);
            // 
            // m_TrackBar
            // 
            this.m_TrackBar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.m_TrackBar.AutoSize = false;
            this.m_TrackBar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.m_TrackBar.Location = new System.Drawing.Point(90, -1);
            this.m_TrackBar.Maximum = 1000;
            this.m_TrackBar.Name = "m_TrackBar";
            this.m_TrackBar.Size = new System.Drawing.Size(198, 24);
            this.m_TrackBar.TabIndex = 1;
            this.m_TrackBar.TabStop = false;
            this.m_TrackBar.TickFrequency = 0;
            this.m_TrackBar.TickStyle = System.Windows.Forms.TickStyle.TopLeft;
            this.m_TrackBar.ValueChanged += new System.EventHandler(this.TrackBar_ValueChanged);
            this.m_TrackBar.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TrackBar_KeyDown);
            this.m_TrackBar.Leave += new System.EventHandler(this.TrackBar_Leave);
            this.m_TrackBar.MouseDown += new System.Windows.Forms.MouseEventHandler(this.TrackBar_MouseDown);
            this.m_TrackBar.MouseUp += new System.Windows.Forms.MouseEventHandler(this.TrackBar_MouseUp);
            // 
            // m_NumericUpDown
            // 
            this.m_NumericUpDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.m_NumericUpDown.Location = new System.Drawing.Point(296, 0);
            this.m_NumericUpDown.Name = "m_NumericUpDown";
            this.m_NumericUpDown.Size = new System.Drawing.Size(88, 20);
            this.m_NumericUpDown.TabIndex = 3;
            this.m_NumericUpDown.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.m_NumericUpDown.ValueChanged += new System.EventHandler(this.NumericUpDown_ValueChanged);
            this.m_NumericUpDown.Enter += new System.EventHandler(this.NumericUpDown_Enter);
            this.m_NumericUpDown.Leave += new System.EventHandler(this.NumericUpDown_ValueChanged);
            // 
            // ControlSlider
            // 
            this.Controls.Add(this.m_NumericUpDown);
            this.Controls.Add(this.m_TrackBar);
            this.Controls.Add(this.m_Button);
            this.Name = "ControlSlider";
            this.Size = new System.Drawing.Size(384, 24);
            ((System.ComponentModel.ISupportInitialize)(this.m_TrackBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_NumericUpDown)).EndInit();
            this.ResumeLayout(false);

        }
        #endregion       
    }


    #region Helper Classes
    [Serializable]
    public class SliderClipboardData : Object
    {
        public SliderClipboardData( decimal value )
        {
            m_value = value;
        }

        #region Variables
        private decimal m_value;
        #endregion

        #region Properties
        public decimal Value
        {
            get
            {
                return m_value;
            }
        }
        #endregion
    };
    #endregion
}
