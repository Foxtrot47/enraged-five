using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;

namespace ragUi
{
	/// <summary>
	/// A control panel is a simple list of rag controls. No hierarchies or anything. Use XPanderList for that.
	/// </summary>
	[Designer("System.Windows.Forms.Design.ParentControlDesigner,System.Design", typeof(System.ComponentModel.Design.IDesigner))]
	public class ControlPanel : System.Windows.Forms.UserControl
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public ControlPanel()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			// TODO: Add any initialization after the InitializeComponent call
			HScroll = false;
		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			// 
			// ControlPanel
			// 
			this.AutoScroll = true;
			this.Name = "ControlPanel";
			this.Layout += new System.Windows.Forms.LayoutEventHandler(this.ControlPanel_Layout);

		}
		#endregion

		private void ControlPanel_Layout(object sender, System.Windows.Forms.LayoutEventArgs e)
		{
			int yLocation = 0;

			// sort by the requested y positions
            List<Control> controls = new List<Control>();
            foreach ( Control c in this.Controls )
            {
                controls.Add( c );
            }
			controls.Sort( SortByY );

			foreach (Control c in controls) 
			{
				c.SetBounds(0, yLocation, this.Width, c.Height, System.Windows.Forms.BoundsSpecified.All);
				yLocation += c.Height;
			}
		}

        private static int SortByY( Control c1, Control c2 )
        {
            if ( c1.Top < c2.Top )
            {
                return -1;
            }
            else if ( c1.Top > c2.Top )
            {
                return 1;
            }
            return 0;
        }
	}
}
