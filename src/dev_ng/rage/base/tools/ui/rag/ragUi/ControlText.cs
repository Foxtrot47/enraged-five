using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace ragUi
{
    public class ControlText : ragUi.ControlBase
    {
        private System.Windows.Forms.TextBox m_TextBox;
        private System.Windows.Forms.Label m_Label;
        private System.ComponentModel.IContainer components = null;

        public ControlText()
        {
            // This call is required by the Windows Form Designer.
            InitializeComponent();

            // TODO: Add any initialization after the InitializeComponent call
        }

        #region Variables
        protected string m_Value;
        protected string m_prevValue;
        protected bool m_firstTime = true;
        #endregion

        #region Properties
        public string Value
        {
            get
            {
                return m_Value;
            }
            set
            {
                SetValue( value, true );
            }
        }

        public override bool IsReadOnly
        {
            get
            {
                return m_TextBox.ReadOnly;
            }
            set
            {
                m_TextBox.ReadOnly = value;
            }
        }

        public int MaxLength
        {
            set
            {
                m_TextBox.MaxLength = value;
            }
        }
        #endregion

        #region Overrides
        public override bool CanCopy
        {
            get
            {
                return true;
            }
        }

        public override bool CanPaste
        {
            get
            {
                return true;
            }
        }

        public override bool CanUndo
        {
            get
            {
                return true;
            }
        }

        public override void CopyToClipboard()
        {
            int selStartIndex = m_TextBox.SelectionStart;
            int selEndIndex = m_TextBox.SelectionStart + m_TextBox.SelectionLength;
            if (selStartIndex >= 0 && selEndIndex <= m_TextBox.Text.Length)
            {
                this.ClipboardData = m_TextBox.Text.Substring( selStartIndex, selEndIndex - selStartIndex );
            }
            base.CopyToClipboard();
        }

        public override void PasteFromClipboard()
        {
            Object data = this.ClipboardData;
            if ( data != null )
            {
                Type type = data.GetType();

                IDataObject iData = Clipboard.GetDataObject();
                if ( (iData != null) && iData.GetDataPresent( type ) )
                {
                    data = iData.GetData( type );

                    int selStartIndex = m_TextBox.SelectionStart;
                    int selEndIndex = m_TextBox.SelectionStart + m_TextBox.SelectionLength;

                    if ( selStartIndex >= 0 && selEndIndex <= m_TextBox.Text.Length )
                    {
                        string value =(string)data;
                        string oldStart = m_TextBox.Text.Substring( 0, selStartIndex );
                        string oldEnd = m_TextBox.Text.Substring( selEndIndex, m_TextBox.Text.Length - selEndIndex );
                        string newValue = oldStart + value + oldEnd;

                        m_TextBox.Text = newValue;
                        m_TextBox.SelectionStart = selEndIndex + value.Length;
                        m_TextBox.ForeColor = System.Drawing.Color.DarkRed;
                    }
                }
            }
        }

        public string m_ClipboardData = "";
        public override object ClipboardData
        {
            get
            {
                return m_ClipboardData;
            }
            set
            {
                if ( (value != null) && (value is string) )
                {
                    m_ClipboardData = (string)value;
                }
            }
        }

        public override bool IsClipboardDataAvailable
        {
            get
            {
                return base.IsClipboardDataAvailable && !this.IsReadOnly;
            }
        }

        public override bool IsUndoable
        {
            get
            {
                return base.IsUndoable && (m_prevValue != null) && (m_prevValue != m_Value);
            }
        }

        [Browsable( true ), Category( "Misc" ), DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
        public override string Text
        {
            get
            {
                return m_Label.Text.TrimStart();
            }
            set
            {
                m_Label.Text = value;
                PadControlTextForIcon( m_Label, m_Icon );
            }
        }

        public override void ConnectControlToXmlNode( System.Xml.XmlNode root )
        {
            DataBindings.Add( "Value", FindXPathNode( root, ValueXPath + "/text()" ), "Value" );
        }

        protected override void OnIconChanged()
        {
            PadControlTextForIcon( m_Label, m_Icon );
            m_Label.Image = m_Icon;
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose( bool disposing )
        {
            if ( disposing )
            {
                if ( components != null )
                {
                    components.Dispose();
                }
            }
            base.Dispose( disposing );
        }

        public override void Undo()
        {
            string prevValue = m_prevValue;

            m_prevValue = this.Value;

            this.Value = m_prevValue;
        }
        #endregion

        #region Event Handlers
        private void TextBox_Leave( object sender, EventArgs e )
        {
            CommitValue();
        }

        private void TextBox_KeyDown( object sender, KeyEventArgs e )
        {
            if ( e.KeyCode == Keys.Enter )
            {
                CommitValue();
                e.SuppressKeyPress = true;
            }
            else if ( e.KeyCode == Keys.Escape )
            {
                m_TextBox.Text = m_Value;
                m_TextBox.ForeColor = System.Drawing.Color.Black;
                e.SuppressKeyPress = true;
            }
            else if ( e.Control )
            {
                switch ( e.KeyCode )
                {
                    case Keys.C:
                        CopyToClipboard();
                        e.SuppressKeyPress = true;
                        break;
                    case Keys.V:
                        PasteFromClipboard();
                        e.SuppressKeyPress = true;
                        break;
                    case Keys.Z:
                        Undo();
                        e.SuppressKeyPress = true;
                        break;
                    default:
                        break;
                }
            }
            else
            {
                // Would need to check for specific keys so that we don't
                // indicate the string as dirty when you're just pressing arrow or something
                //                m_TextBox.Font = new Font(m_TextBox.Font, FontStyle.Italic);
                char c = (char)e.KeyCode;
                if (Char.IsLetterOrDigit(c))
                {
                    m_TextBox.ForeColor = System.Drawing.Color.DarkRed;
                }
            }
        }

        private void TextBox_PreviewKeyDown( object sender, PreviewKeyDownEventArgs e )
        {
            if ( e.KeyCode == Keys.Escape )
            {
                e.IsInputKey = true;
            }
        }
        #endregion

        #region Public Functions
        public void UpdateValueFromRemote( string value )
        {
            string prevValue = this.Value;

            SetValue( value, false );

            if ( m_firstTime )
            {
                m_prevValue = this.Value;
                m_firstTime = false;
            }
            else
            {
                if ( this.Value != prevValue )
                {
                    m_prevValue = prevValue;
                }
            }
        }
        #endregion

        #region Protected Functions
        protected void SetValue( string value, bool triggerEvent )
        {
            if ( m_Value == value )
            {
                return;
            }

            m_TextBox.ForeColor = System.Drawing.Color.Black;
            m_Value = value;

            m_TextBox.Text = value;

            if ( triggerEvent )
            {
                OnValueChanged();
            }
        }

        protected void CommitValue()
        {
            m_prevValue = this.Value;

            SetValue( m_TextBox.Text, true );
        }
        #endregion

        #region Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.m_TextBox = new System.Windows.Forms.TextBox();
            this.m_Label = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // m_TextBox
            // 
            this.m_TextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.m_TextBox.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)) );
            this.m_TextBox.Location = new System.Drawing.Point( 135, 2 );
            this.m_TextBox.Name = "m_TextBox";
            this.m_TextBox.Size = new System.Drawing.Size( 232, 20 );
            this.m_TextBox.TabIndex = 0;
            this.m_TextBox.Text = "textBox1";
            this.m_TextBox.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler( this.TextBox_PreviewKeyDown );
            this.m_TextBox.Leave += new System.EventHandler( this.TextBox_Leave );
            this.m_TextBox.KeyDown += new System.Windows.Forms.KeyEventHandler( this.TextBox_KeyDown );
            // 
            // m_Label
            // 
            this.m_Label.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.m_Label.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.m_Label.Location = new System.Drawing.Point( 0, 0 );
            this.m_Label.Name = "m_Label";
            this.m_Label.Size = new System.Drawing.Size( 136, 24 );
            this.m_Label.TabIndex = 1;
            this.m_Label.Text = "label1";
            this.m_Label.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ControlText
            // 
            this.Controls.Add( this.m_Label );
            this.Controls.Add( this.m_TextBox );
            this.Name = "ControlText";
            this.Size = new System.Drawing.Size( 368, 24 );
            this.ResumeLayout( false );
            this.PerformLayout();

        }
        #endregion
    }

    #region Helper Classes
    [Serializable]
    public class TextClipboardData : Object
    {
        public TextClipboardData( string text )
        {
            m_text = text;
        }

        #region Variables
        private string m_text;
        #endregion

        #region Properties
        public string Text
        {
            get
            {
                return m_text;
            }
        }
        #endregion
    };
    #endregion
}

