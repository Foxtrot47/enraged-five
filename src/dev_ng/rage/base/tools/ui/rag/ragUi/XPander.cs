using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Data;
using System.Text;
using System.Windows.Forms;
using ragUi;

namespace ragCore
{
    public partial class XPander : UserControl
    {
        public XPander()
        {
            InitializeComponent();

            InitializeAdditionalComponents();
        }

        public XPander( bool isExpanded )
        {
            InitializeComponent();

            InitializeAdditionalComponents();

            m_Expanded = isExpanded;
        }

        private void InitializeAdditionalComponents()
        {
            this.SuspendLayout();

            //Add any initialization after the InitializeComponent() call
            // We draw everything, and repaint when resized.
            SetStyle( ControlStyles.ResizeRedraw, true );
            SetStyle( ControlStyles.AllPaintingInWmPaint, true );
            SetStyle( ControlStyles.UserPaint, true );
            SetStyle( ControlStyles.DoubleBuffer, true );
            SetStyle( ControlStyles.SupportsTransparentBackColor, true );
            SetStyle( ControlStyles.ContainerControl, true );

            m_Height = m_CaptionHeight;

            this.ResumeLayout( false );
        }

        #region Constants
        private const int c_ExpandCollapseHeight = 1;
        private const int c_ExpandCollapseAlpha = 2;
        #endregion

        #region Variables
        private int m_CaptionHeight = 18;
        private string m_CaptionText = "";
        private Color m_CaptionTextColor = Color.FromArgb( 33, 93, 198 );
        private Color m_CaptionTextHighlightColor = Color.FromArgb( 66, 142, 255 );
        private static readonly Font m_CaptionFont = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
        private static readonly Font m_TriangleFont = new Font( "Marlett", 12 );
        private Color m_CaptionColor = Color.FromArgb( 198, 210, 248 );
        private Color m_borderFillColor = Color.FromKnownColor( KnownColor.Control );
        private bool m_Expanded = true;
        private bool m_IsCaptionHighlighted = false;
        private int m_Height;

        private List<Control> m_originalContrlOrder = new List<Control>();
        #endregion

        #region Properties
        [Description( "Border fill color." ),
        DefaultValue( "SystemColors.Control" ),
        Category( "Appearance" )]
        public Color BorderFillColor
        {
            get
            {                
                return m_borderFillColor;
            }

            set
            {
                m_borderFillColor = value;
                Invalidate();
            }
        }

        [Description( "Height of caption." ),
        DefaultValue( 25 ),
        Category( "Appearance" )]
        public int CaptionHeight
        {
            get
            {
                return m_CaptionHeight;
            }

            set
            {
                m_CaptionHeight = value;
                Invalidate();
            }
        }

        [Description( "Caption text." ),
        DefaultValue( "" ),
        Category( "Appearance" ),
        Localizable( true )]
        public string CaptionText
        {
            get
            {
                return m_CaptionText;
            }

            set
            {
                m_CaptionText = value;
                Invalidate();
            }
        }

        [Description( "Caption color." ),
        DefaultValue( "180,190,240" ),
        Category( "Appearance" )]
        public Color CaptionColor
        {
            get
            {
                return m_CaptionColor;
            }

            set
            {
                m_CaptionColor = value;
                Invalidate();
            }
        }

        [Description( "Caption text color." ),
        DefaultValue( "33,93,198" ),
        Category( "Appearance" )]
        public Color CaptionTextColor
        {
            get
            {
                return m_CaptionTextColor;
            }

            set
            {
                m_CaptionTextColor = value;
                Invalidate();
            }
        }

        [Description( "Caption text color when the mouse is hovering over it." ),
        DefaultValue( "66, 142, 255" ),
        Category( "Appearance" )]
        public Color CaptionTextHighlightColor
        {
            get
            {
                return m_CaptionTextHighlightColor;
            }

            set
            {
                m_CaptionTextHighlightColor = value;
                Invalidate();
            }
        }
        /*
        [Description( "Caption font." ),
        Category( "Appearance" )]
        public Font CaptionFont
        {
            get
            {
                return m_CaptionFont;
            }

            set
            {
                if (m_CaptionFont != null)
                {
                    m_CaptionFont.Dispose();
                }
                m_CaptionFont = value;
                Invalidate();
            }
        }
        */

        public bool Expanded
        {
            get
            {
                return m_Expanded;
            }
            set
            {
                if ( m_Expanded == value )
                {
                    return;
                }

                m_Expanded = value;

                if ( !m_Expanded )
                {
                    BeginUpdate();

                    if ( OnXPanderCollapsing() )
                    {
                        ResumeLayout();
                        return;
                    }

                    foreach ( Control cntrl in Controls )
                    {
                        cntrl.Visible = false;
                    }

                    OnXPanderCollapsed();

                    OnReleaseGroup();

                    EndUpdate();
                }
                else
                {
                    BeginUpdate();
                    
                    if ( OnXPanderExpanding() )
                    {
                        ResumeLayout();
                        return;
                    }

                    foreach ( Control cntrl in Controls )
                    {
                        cntrl.Visible = true;
                    }

                    OnXPanderExpanded();

                    OnAddRefGroup();

                    EndUpdate();

                    // NOTE: Expanding this XPander might cause its parent to create
                    // a vertical scroll bar, which changes the available width of this
                    // XPander, causing its sub-controls to be partially hidden by the scroll
                    // bar. So, we end the update, allowing for the scroll bar to be created,
                    // and then we make sure each sub-control's width is correct.
                    BeginUpdate();
                    foreach (Control ctrl in this.Controls)
                    {
                        ctrl.Width = this.ClientSize.Width;
                    }
                    EndUpdate();
                }
            }
        }

        [Browsable( false ),
        DesignOnly( true )]
        public int ExpandedHeight
        {
            get
            {
                return m_Height;
            }
            set
            {
                m_Height = value;
            }
        }

        [Browsable( false )]
        public XPanderList ParentXPanderList
        {
            get
            {
                if (m_parentXPanderList == null)
                {
                    m_parentXPanderList = GetParentXPanderList();
                }
                return m_parentXPanderList;
            }
        }
        private XPanderList m_parentXPanderList;
        #endregion

        #region Events
        [Description( "Fired when the XPander begins to collapse."),
        Category( "Behavior" )]
        public event CancelEventHandler XPanderCollapsing;

        [Description( "Fired when the XPander is completely collapsed." ),
        Category( "Behavior" )]
        public event EventHandler XPanderCollapsed;

        [Description( "Fired when the XPander begins to expand." ),
        Category( "Behavior" )]
        public event CancelEventHandler XPanderExpanding;

        [Description( "Fired when the XPander is completely expanded." ),
        Category( "Behavior" )]
        public event EventHandler XPanderExpanded;

        [Description( "Add handler for reference counting between RAG and the remote app." ),
        Category( "Behavior" )]
        public event EventHandler AddRefGroup;

        [Description( "Release handler for reference counting between RAG and the remote app." ),
        Category( "Behavior" )]
        public event EventHandler ReleaseGroup;

        [Description( "Fired when the XPander is sorted." ),
        Category( "Behavior" )]
        public event EventHandler Sorted;

        [Description( "Fired when the XPander is unsorted." ),
        Category( "Behavior" )]
        public event EventHandler Unsorted;
        #endregion

        #region Event Dispatchers
        private bool OnXPanderCollapsing()
        {
            if ( this.XPanderCollapsing != null )
            {
                CancelEventArgs e = new CancelEventArgs( false );
                this.XPanderCollapsing( this, e );

                return e.Cancel;
            }

            return false;
        }

        private void OnXPanderCollapsed()
        {
            if ( this.XPanderCollapsed != null )
            {
                this.XPanderCollapsed( this, EventArgs.Empty );
            }
        }

        private bool OnXPanderExpanding()
        {
            if ( this.XPanderExpanding != null )
            {
                CancelEventArgs e = new CancelEventArgs( false );
                this.XPanderExpanding( this, e );

                return e.Cancel;
            }

            return false;
        }

        private void OnXPanderExpanded()
        {
            if ( this.XPanderExpanded != null )
            {
                this.XPanderExpanded( this, EventArgs.Empty );
            }
        }

        private void OnAddRefGroup()
        {
            if ( this.AddRefGroup != null )
            {
                this.AddRefGroup( this, EventArgs.Empty );
            }
        }

        private void OnReleaseGroup()
        {
            if ( this.ReleaseGroup != null )
            {
                this.ReleaseGroup( this, EventArgs.Empty );
            }
        }

        private void OnSorted()
        {
            if ( this.Sorted != null )
            {
                this.Sorted( this, EventArgs.Empty );
            }
        }

        private void OnUnsorted()
        {
            if ( this.Unsorted != null )
            {
                this.Unsorted( this, EventArgs.Empty );
            }
        }
        #endregion

        #region Overrides
        /// <summary>
        /// 
        /// </summary>
        /// <param name="levent"></param>
        protected override void OnLayout(LayoutEventArgs levent)
        {
            XPanderList xPanderList = ParentXPanderList;
            if ((xPanderList != null) && xPanderList.Updating)
            {
                return;
            }

            base.OnLayout(levent);

            int inset = 0;
            int bottomPad = 0;

            if (xPanderList != null)
            {
                inset = xPanderList.NestingIndent;
                bottomPad = xPanderList.NestingBottomPad;
            }

            int yPos = m_CaptionHeight + 1;
            if (m_Expanded)
            {
                foreach (Control ctrl in this.Controls)
                {
                    ctrl.SetBounds(inset, yPos, this.ClientSize.Width - inset, ctrl.Height, BoundsSpecified.All);
                    yPos += ctrl.Height;
                }

                yPos += bottomPad;
            }

            this.Height = yPos;
        }

        protected override void OnPaint( PaintEventArgs e )
        {
            Rectangle rc = new Rectangle( 0, 0, this.Width, CaptionHeight );
            LinearGradientBrush b;
            if ( m_IsCaptionHighlighted )
            {
                b = new LinearGradientBrush( rc, CaptionColor, Color.White,
                    LinearGradientMode.Vertical );
            }
            else
            {
                b = new LinearGradientBrush( rc, Color.White, CaptionColor,
                    LinearGradientMode.Vertical );
            }

            Size size = e.Graphics.MeasureString(CaptionText, m_CaptionFont).ToSize();

            // Now draw the caption areas with the rounded corners at the top
            e.Graphics.FillRectangle( b, rc );

            b.Dispose();

            using (Pen pen = new Pen(m_CaptionTextColor))
            {
                e.Graphics.DrawLine(pen, 0, m_CaptionHeight - 1, this.Width, m_CaptionHeight - 1);
            }

            // Draw the outline around the work area
            if ( this.Height > m_CaptionHeight )
            {
                // fill it in
                e.Graphics.FillRectangle( new SolidBrush( m_borderFillColor ),
                    new Rectangle( 0, this.CaptionHeight, this.Width - 1, this.Height - 1 ) );
                
                using (Pen pen = new Pen(Color.FromKnownColor(KnownColor.HighlightText)))
                {
                    e.Graphics.DrawLine(pen, 0, this.CaptionHeight, 0, this.Height - 1);
                    e.Graphics.DrawLine(pen, this.Width - 1, this.CaptionHeight, this.Width - 1, this.Height - 1);
                    e.Graphics.DrawLine(pen, 0, this.Height - 1, this.Width - 1, this.Height - 1);
                }
            }

            // Caption text.
            using (SolidBrush brush = new SolidBrush(m_CaptionTextColor))
            {
                e.Graphics.DrawString(CaptionText, m_CaptionFont, brush, (float)10, (float)((this.CaptionHeight - 2 - size.Height) / 2));

                // Expand / Collapse Icon
                e.Graphics.DrawString((m_Expanded ? "6" : "4"), m_TriangleFont, brush, this.Width - 27, 0);
            }
        }

        protected override void OnMouseMove( System.Windows.Forms.MouseEventArgs e )
        {
            // Change cursor to hand when over caption area.
            if ( e.Y <= this.CaptionHeight )
            {
                Cursor.Current = Cursors.Hand;
                m_IsCaptionHighlighted = true;
            }
            else
            {
                Cursor.Current = Cursors.Default;
                m_IsCaptionHighlighted = false;
            }

            Invalidate();
        }

        protected override void OnMouseDown( System.Windows.Forms.MouseEventArgs e )
        {
            if ( e.Button == MouseButtons.Left )
            {
                if ( e.Y <= this.CaptionHeight )
                {
                    this.Expanded = !this.Expanded;

                    this.Focus();

                    this.Refresh();
                }
                else
                {
                    Control ctrl = this.GetChildAtPoint( new Point( this.Width / 2, e.Y ) );
                    if ( ctrl != null )
                    {
                        ctrl.Focus();
                    }
                }
            }
        }

        protected override void OnMouseLeave( System.EventArgs e )
        {
            if ( m_IsCaptionHighlighted )
            {
                m_IsCaptionHighlighted = false;
                Invalidate();
            }
        }

        protected override void OnMouseUp( MouseEventArgs e )
        {
            // Don't do anything if did not click on caption.
            if ( e.Y > this.CaptionHeight )
            {
                return;
            }

            base.OnMouseUp( e );
        }
        #endregion

        #region Public Functions
        public void Expand()
        {
            this.Expanded = true;
        }

        public void Collapse()
        {
            this.Expanded = false;
        }

        public void Sort()
        {
            this.SuspendLayout();

            this.ControlAdded -= new ControlEventHandler( XPander_ControlAdded );
            this.ControlRemoved -= new ControlEventHandler( XPander_ControlRemoved );

            Control[] controls = new Control[this.Controls.Count];
            this.Controls.CopyTo( controls, 0 );

            List<Control> controlList = new List<Control>( controls );
            controlList.Sort( CompareControls );

            this.Controls.Clear();
            this.Controls.AddRange( controlList.ToArray() );

            foreach ( Control ctrl in this.Controls )
            {
                if ( ctrl is XPander )
                {
                    (ctrl as XPander).Sort();
                }
                else if ( ctrl is XPanderList )
                {
                    (ctrl as XPanderList).Sort();
                }
            }

            this.ControlAdded += new ControlEventHandler( XPander_ControlAdded );
            this.ControlRemoved += new ControlEventHandler( XPander_ControlRemoved );

            this.ResumeLayout();

            OnSorted();
        }

        public void Unsort()
        {
            this.SuspendLayout();

            this.ControlAdded -= new ControlEventHandler( XPander_ControlAdded );
            this.ControlRemoved -= new ControlEventHandler( XPander_ControlRemoved );

            this.Controls.Clear();
            this.Controls.AddRange( m_originalContrlOrder.ToArray() );

            foreach ( Control ctrl in this.Controls )
            {
                if ( ctrl is XPander )
                {
                    (ctrl as XPander).Unsort();
                }
                else if ( ctrl is XPanderList )
                {
                    (ctrl as XPanderList).Unsort();
                }
            }

            this.ControlAdded += new ControlEventHandler( XPander_ControlAdded );
            this.ControlRemoved += new ControlEventHandler( XPander_ControlRemoved );

            this.ResumeLayout();

            OnUnsorted();
        }

        public void ForceRelayout()
        {
            this.PerformLayout( null, null );

            if ( this.Parent is XPander )
            {
                (this.Parent as XPander).ForceRelayout();
            }
            else if ( this.Parent is XPanderList )
            {
                (this.Parent as XPanderList).PerformLayout();
            }
        }

        public void BeginUpdate()
        {
            SuspendLayout();

            foreach ( Control ctrl in this.Controls )
            {
                if ( ctrl is XPander )
                {
                    (ctrl as XPander).BeginUpdate();
                }
                else
                {
                    ctrl.SuspendLayout();
                }
            }
        }

        public void EndUpdate()
        {
            foreach ( Control ctrl in this.Controls )
            {
                if ( ctrl is XPander )
                {
                    (ctrl as XPander).EndUpdate();
                }
                else
                {
                    ctrl.ResumeLayout();
                }
            }
            
            ResumeLayout();
        }

        public XPanderList GetParentXPanderList()
        {
            Control xp = this;
            while ( xp.Parent is XPander )
            {
                xp = xp.Parent;
            }
            if ( xp.Parent is XPanderList )
            {
                return xp.Parent as XPanderList;
            }
            return null;
        }
        #endregion

        #region Event Handlers
        private void XPander_ControlAdded( object sender, ControlEventArgs e )
        {
            m_originalContrlOrder.Add( e.Control );
        }

        private void XPander_ControlRemoved( object sender, ControlEventArgs e )
        {
            m_originalContrlOrder.Remove(e.Control);
        }
        #endregion

        #region Private Functions
        private int CompareControls( Control a, Control b )
        {
            if ( a == null )
            {
                if ( b == null )
                {
                    // If a is null and b is null, they're equal. 
                    return 0;
                }
                else
                {
                    // If a is null and b is not null, b is greater. 
                    return -1;
                }
            }
            else
            {
                // If a is not null...
                //
                if ( b == null )
                // ...and b is null, a is greater.
                {
                    return 1;
                }
                else
                {
                    // sort them with ordinary string comparison.
                    return string.Compare( a.Text, b.Text );
                }
            }
        }
        #endregion
    }
}
