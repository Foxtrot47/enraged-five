using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ragUi
{
    public partial class ControlMatrix : ragUi.ControlBase, ISupportInitialize
    {
        public ControlMatrix()
        {
            InitializeComponent();
            InitializeAdditionalComponents( null );
        }

        public ControlMatrix( System.Xml.XmlNode schema )
        {
            InitializeComponent();
            InitializeAdditionalComponents( schema );
        }

        #region Delegates
        public delegate void ParameterizedOperationEventHandler( object sender, MatrixOperationParameterEventArgs e );
        #endregion

        #region Variables
        private int m_numRows = 4;

        private Dictionary<string, ParameterizedOperationEventHandler> m_eventDictionary = new Dictionary<string, ParameterizedOperationEventHandler>();
        private Dictionary<string, string> m_eventTextDictionary = new Dictionary<string, string>();

        private ControlVector m_lastControlVectorChanged = null;
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the Text displayed for the control, typically it's name.
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        Description( "Gets or sets the Text displayed for the control, typically it's name." )]
        public override string Text
        {
            get
            {
                return this.toolStripDropDownButton1.Text;
            }
            set
            {
                this.toolStripDropDownButton1.Text = value;
            }
        }

        public override bool IsReadOnly
        {
            get { return row1ControlVector.IsReadOnly; }
            set
            {
                row1ControlVector.IsReadOnly = value;
                row2ControlVector.IsReadOnly = value;
                row3ControlVector.IsReadOnly = value;
                row4ControlVector.IsReadOnly = value;
                basicOperationsToolStripMenuItem.Enabled = !value;
                trigonometryToolStripMenuItem.Enabled = !value;
                advancedOperationsToolStripMenuItem.Enabled = !value;
            }
        }

        /// <summary>
        /// Gets or sets whether the drop down options are available.
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        Description( "Gets or sets whether the drop down options are available." )]
        public bool AllowDropDownOptions
        {
            get
            {
                return this.toolStripDropDownButton1.DropDownItems.Count > 0;
            }
            set
            {
                if ( value )
                {
                    if ( !this.AllowDropDownOptions )
                    {
                        this.toolStripDropDownButton1.DropDownItems.Add( this.basicOperationsToolStripMenuItem );
                        this.toolStripDropDownButton1.DropDownItems.Add( this.trigonometryToolStripMenuItem );
                        this.toolStripDropDownButton1.DropDownItems.Add( this.advancedOperationsToolStripMenuItem );
                    }
                }
                else
                {
                    this.toolStripDropDownButton1.DropDownItems.Clear();
                }
            }
        }

        /// <summary>
        /// Gets or sets the entire matrix.  Assumed to be 4 x 4.
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        Description( "Gets or sets the entire matrix.  Assumed to be 4 x 4." ),
        DefaultValue( null )]
        public decimal[][] Matrix
        {
            get
            {
                decimal[][] m = new decimal[][] {
                    this.row1ControlVector.Vector,
                    this.row2ControlVector.Vector,
                    this.row3ControlVector.Vector,
                    this.row4ControlVector.Vector
                };

                return m;
            }
            set
            {
                if ( value != null )
                {
                    SetValue( value, true );
                }
                else
                {
                    SetValue( new decimal[][] {
                        new decimal[]{ 0, 0, 0, 0 },
                        new decimal[]{ 0, 0, 0, 0 },
                        new decimal[]{ 0, 0, 0, 0 },
                        new decimal[]{ 0, 0, 0, 0 } }, true );
                }
            }
        }

        /// <summary>
        /// Gets or sets the number of rows in the matrix (typically 3 or 4).
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        Description( "Gets or sets the number of rows in the matrix (typically 3 or 4)." )]
        public int NumRows
        {
            get
            {
                return m_numRows;
            }
            set
            {
                m_numRows = value;

                this.row1ControlVector.Visible = m_numRows >= 1;
                this.row2ControlVector.Visible = m_numRows >= 2;
                this.row3ControlVector.Visible = m_numRows >= 3;
                this.row4ControlVector.Visible = m_numRows >= 4;

                this.Size = new Size( this.Size.Width, 30 * (m_numRows + 1) );

                EnableDropDownMenuItems();
            }
        }

        /// <summary>
        /// Gets or sets the number of columns in the matrix (typically 3 or 4).
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        Description( "Gets or sets the number of columns in the matrix (typically 3 or 4)." )]
        public int NumColumns
        {
            get
            {
                return this.row1ControlVector.NumComponents;
            }
            set
            {
                this.row1ControlVector.NumComponents = value;
                this.row2ControlVector.NumComponents = value;
                this.row3ControlVector.NumComponents = value;
                this.row4ControlVector.NumComponents = value;

                EnableDropDownMenuItems();
            }
        }

        public decimal Minimum
        {
            get
            {
                return this.row1ControlVector.Minimum;
            }
            set
            {
                this.row1ControlVector.Minimum = value;
                this.row2ControlVector.Minimum = value;
                this.row3ControlVector.Minimum = value;
                this.row4ControlVector.Minimum = value;
            }
        }

        public decimal Maximum
        {
            get
            {
                return this.row1ControlVector.Maximum;
            }
            set
            {
                this.row1ControlVector.Maximum = value;
                this.row2ControlVector.Maximum = value;
                this.row3ControlVector.Maximum = value;
                this.row4ControlVector.Maximum = value;
            }
        }

        public decimal Step
        {
            get
            {
                return this.row1ControlVector.Step;
            }
            set
            {
                this.row1ControlVector.Step = value;
                this.row2ControlVector.Step = value;
                this.row3ControlVector.Step = value;
                this.row4ControlVector.Step = value;
            }
        }

        public event ParameterizedOperationEventHandler SetDiagonalClick
        {
            add
            {
                m_eventDictionary[this.setDiagonalToolStripMenuItem.Name] = m_eventDictionary[this.setDiagonalToolStripMenuItem.Name] + value;
            }
            remove
            {
                m_eventDictionary[this.setDiagonalToolStripMenuItem.Name] = m_eventDictionary[this.setDiagonalToolStripMenuItem.Name] - value;
            }
        }

        public event ParameterizedOperationEventHandler AddClick
        {
            add
            {
                m_eventDictionary[this.addToolStripMenuItem.Name] = m_eventDictionary[this.addToolStripMenuItem.Name] + value;
            }
            remove
            {
                m_eventDictionary[this.addToolStripMenuItem.Name] = m_eventDictionary[this.addToolStripMenuItem.Name] - value;
            }
        }

        public event ParameterizedOperationEventHandler SubtractClick
        {
            add
            {
                m_eventDictionary[this.subtractToolStripMenuItem.Name] = m_eventDictionary[this.subtractToolStripMenuItem.Name] + value;
            }
            remove
            {
                m_eventDictionary[this.subtractToolStripMenuItem.Name] = m_eventDictionary[this.subtractToolStripMenuItem.Name] - value;
            }
        }

        public event ParameterizedOperationEventHandler ScaleClick
        {
            add
            {
                m_eventDictionary[this.scaleToolStripMenuItem.Name] = m_eventDictionary[this.scaleToolStripMenuItem.Name] + value;
            }
            remove
            {
                m_eventDictionary[this.scaleToolStripMenuItem.Name] = m_eventDictionary[this.scaleToolStripMenuItem.Name] - value;
            }
        }

        public event ParameterizedOperationEventHandler MakeScaleClick
        {
            add
            {
                m_eventDictionary[this.makeScaleToolStripMenuItem.Name] = m_eventDictionary[this.makeScaleToolStripMenuItem.Name] + value;
            }
            remove
            {
                m_eventDictionary[this.makeScaleToolStripMenuItem.Name] = m_eventDictionary[this.makeScaleToolStripMenuItem.Name] - value;
            }
        }

        public event ParameterizedOperationEventHandler Add3x3Click
        {
            add
            {
                m_eventDictionary[this.add3x3ToolStripMenuItem.Name] = m_eventDictionary[this.add3x3ToolStripMenuItem.Name] + value;
            }
            remove
            {
                m_eventDictionary[this.add3x3ToolStripMenuItem.Name] = m_eventDictionary[this.add3x3ToolStripMenuItem.Name] - value;
            }
        }

        public event ParameterizedOperationEventHandler Subtract3x3Click
        {
            add
            {
                m_eventDictionary[this.subtract3x3ToolStripMenuItem.Name] = m_eventDictionary[this.subtract3x3ToolStripMenuItem.Name] + value;
            }
            remove
            {
                m_eventDictionary[this.subtract3x3ToolStripMenuItem.Name] = m_eventDictionary[this.subtract3x3ToolStripMenuItem.Name] - value;
            }
        }

        public event ParameterizedOperationEventHandler ScaleFullClick
        {
            add
            {
                m_eventDictionary[this.scaleFullToolStripMenuItem.Name] = m_eventDictionary[this.scaleFullToolStripMenuItem.Name] + value;
            }
            remove
            {
                m_eventDictionary[this.scaleFullToolStripMenuItem.Name] = m_eventDictionary[this.scaleFullToolStripMenuItem.Name] - value;
            }
        }

        public event ParameterizedOperationEventHandler TranslateClick
        {
            add
            {
                m_eventDictionary[this.translateToolStripMenuItem.Name] = m_eventDictionary[this.translateToolStripMenuItem.Name] + value;
            }
            remove
            {
                m_eventDictionary[this.translateToolStripMenuItem.Name] = m_eventDictionary[this.translateToolStripMenuItem.Name] - value;
            }
        }

        public event ParameterizedOperationEventHandler MakeTranslateClick
        {
            add
            {
                m_eventDictionary[this.makeTranslateToolStripMenuItem.Name] = m_eventDictionary[this.makeTranslateToolStripMenuItem.Name] + value;
            }
            remove
            {
                m_eventDictionary[this.makeTranslateToolStripMenuItem.Name] = m_eventDictionary[this.makeTranslateToolStripMenuItem.Name] - value;
            }
        }

        public event ParameterizedOperationEventHandler MakePosClick
        {
            add
            {
                m_eventDictionary[this.makePosToolStripMenuItem.Name] = m_eventDictionary[this.makePosToolStripMenuItem.Name] + value;
            }
            remove
            {
                m_eventDictionary[this.makePosToolStripMenuItem.Name] = m_eventDictionary[this.makePosToolStripMenuItem.Name] - value;
            }
        }

        public event ParameterizedOperationEventHandler MakeScaleFullClick
        {
            add
            {
                m_eventDictionary[this.makeScaleFullToolStripMenuItem.Name] = m_eventDictionary[this.makeScaleFullToolStripMenuItem.Name] + value;
            }
            remove
            {
                m_eventDictionary[this.makeScaleFullToolStripMenuItem.Name] = m_eventDictionary[this.makeScaleFullToolStripMenuItem.Name] - value;
            }
        }

        public event ParameterizedOperationEventHandler DotClick
        {
            add
            {
                m_eventDictionary[this.dotToolStripMenuItem.Name] = m_eventDictionary[this.dotToolStripMenuItem.Name] + value;
            }
            remove
            {
                m_eventDictionary[this.dotToolStripMenuItem.Name] = m_eventDictionary[this.dotToolStripMenuItem.Name] - value;
            }
        }

        public event ParameterizedOperationEventHandler Dot3x3Click
        {
            add
            {
                m_eventDictionary[this.dot3x3ToolStripMenuItem.Name] = m_eventDictionary[this.dot3x3ToolStripMenuItem.Name] + value;
            }
            remove
            {
                m_eventDictionary[this.dot3x3ToolStripMenuItem.Name] = m_eventDictionary[this.dot3x3ToolStripMenuItem.Name] - value;
            }
        }

        public event ParameterizedOperationEventHandler DotFromLeftClick
        {
            add
            {
                m_eventDictionary[this.dotFromLeftToolStripMenuItem.Name] = m_eventDictionary[this.dotFromLeftToolStripMenuItem.Name] + value;
            }
            remove
            {
                m_eventDictionary[this.dotFromLeftToolStripMenuItem.Name] = m_eventDictionary[this.dotFromLeftToolStripMenuItem.Name] - value;
            }
        }

        public event ParameterizedOperationEventHandler DotTransposeClick
        {
            add
            {
                m_eventDictionary[this.dotTransposeToolStripMenuItem.Name] = m_eventDictionary[this.dotTransposeToolStripMenuItem.Name] + value;
            }
            remove
            {
                m_eventDictionary[this.dotTransposeToolStripMenuItem.Name] = m_eventDictionary[this.dotTransposeToolStripMenuItem.Name] - value;
            }
        }

        public event ParameterizedOperationEventHandler CrossProductClick
        {
            add
            {
                m_eventDictionary[this.crossProductToolStripMenuItem.Name] = m_eventDictionary[this.crossProductToolStripMenuItem.Name] + value;
            }
            remove
            {
                m_eventDictionary[this.crossProductToolStripMenuItem.Name] = m_eventDictionary[this.crossProductToolStripMenuItem.Name] - value;
            }
        }

        public event ParameterizedOperationEventHandler OuterProductClick
        {
            add
            {
                m_eventDictionary[this.outerProductToolStripMenuItem.Name] = m_eventDictionary[this.outerProductToolStripMenuItem.Name] + value;
            }
            remove
            {
                m_eventDictionary[this.outerProductToolStripMenuItem.Name] = m_eventDictionary[this.outerProductToolStripMenuItem.Name] - value;
            }
        }

        public event ParameterizedOperationEventHandler Dot3x3FromLeftClick
        {
            add
            {
                m_eventDictionary[this.dot3x3FromLeftToolStripMenuItem.Name] = m_eventDictionary[this.dot3x3FromLeftToolStripMenuItem.Name] + value;
            }
            remove
            {
                m_eventDictionary[this.dot3x3FromLeftToolStripMenuItem.Name] = m_eventDictionary[this.dot3x3FromLeftToolStripMenuItem.Name] - value;
            }
        }

        public event ParameterizedOperationEventHandler Dot3x3TransposeClick
        {
            add
            {
                m_eventDictionary[this.dot3x3TransposeToolStripMenuItem.Name] = m_eventDictionary[this.dot3x3TransposeToolStripMenuItem.Name] + value;
            }
            remove
            {
                m_eventDictionary[this.dot3x3TransposeToolStripMenuItem.Name] = m_eventDictionary[this.dot3x3TransposeToolStripMenuItem.Name] - value;
            }
        }

        public event ParameterizedOperationEventHandler RotateXClick
        {
            add
            {
                m_eventDictionary[this.rotateXToolStripMenuItem.Name] = m_eventDictionary[this.rotateXToolStripMenuItem.Name] + value;
            }
            remove
            {
                m_eventDictionary[this.rotateXToolStripMenuItem.Name] = m_eventDictionary[this.rotateXToolStripMenuItem.Name] - value;
            }
        }

        public event ParameterizedOperationEventHandler RotateYClick
        {
            add
            {
                m_eventDictionary[this.rotateYToolStripMenuItem.Name] = m_eventDictionary[this.rotateYToolStripMenuItem.Name] + value;
            }
            remove
            {
                m_eventDictionary[this.rotateYToolStripMenuItem.Name] = m_eventDictionary[this.rotateYToolStripMenuItem.Name] - value;
            }
        }

        public event ParameterizedOperationEventHandler RotateZClick
        {
            add
            {
                m_eventDictionary[this.rotateZToolStripMenuItem.Name] = m_eventDictionary[this.rotateZToolStripMenuItem.Name] + value;
            }
            remove
            {
                m_eventDictionary[this.rotateZToolStripMenuItem.Name] = m_eventDictionary[this.rotateZToolStripMenuItem.Name] - value;
            }
        }

        public event ParameterizedOperationEventHandler RotateUnitAxisClick
        {
            add
            {
                m_eventDictionary[this.rotateUnitAxisToolStripMenuItem.Name] = m_eventDictionary[this.rotateUnitAxisToolStripMenuItem.Name] + value;
            }
            remove
            {
                m_eventDictionary[this.rotateUnitAxisToolStripMenuItem.Name] = m_eventDictionary[this.rotateUnitAxisToolStripMenuItem.Name] - value;
            }
        }

        public event ParameterizedOperationEventHandler RotateClick
        {
            add
            {
                m_eventDictionary[this.rotateToolStripMenuItem.Name] = m_eventDictionary[this.rotateToolStripMenuItem.Name] + value;
            }
            remove
            {
                m_eventDictionary[this.rotateToolStripMenuItem.Name] = m_eventDictionary[this.rotateToolStripMenuItem.Name] - value;
            }
        }

        public event ParameterizedOperationEventHandler RotateLocalXClick
        {
            add
            {
                m_eventDictionary[this.rotateLocalXToolStripMenuItem.Name] = m_eventDictionary[this.rotateLocalXToolStripMenuItem.Name] + value;
            }
            remove
            {
                m_eventDictionary[this.rotateLocalXToolStripMenuItem.Name] = m_eventDictionary[this.rotateLocalXToolStripMenuItem.Name] - value;
            }
        }

        public event ParameterizedOperationEventHandler RotateLocalYClick
        {
            add
            {
                m_eventDictionary[this.rotateLocalYToolStripMenuItem.Name] = m_eventDictionary[this.rotateLocalYToolStripMenuItem.Name] + value;
            }
            remove
            {
                m_eventDictionary[this.rotateLocalYToolStripMenuItem.Name] = m_eventDictionary[this.rotateLocalYToolStripMenuItem.Name] - value;
            }
        }

        public event ParameterizedOperationEventHandler RotateLocalZClick
        {
            add
            {
                m_eventDictionary[this.rotateLocalZToolStripMenuItem.Name] = m_eventDictionary[this.rotateLocalZToolStripMenuItem.Name] + value;
            }
            remove
            {
                m_eventDictionary[this.rotateLocalZToolStripMenuItem.Name] = m_eventDictionary[this.rotateLocalZToolStripMenuItem.Name] - value;
            }
        }

        public event ParameterizedOperationEventHandler RotateLocalAxisClick
        {
            add
            {
                m_eventDictionary[this.rotateLocalAxisToolStripMenuItem.Name] = m_eventDictionary[this.rotateLocalAxisToolStripMenuItem.Name] + value;
            }
            remove
            {
                m_eventDictionary[this.rotateLocalAxisToolStripMenuItem.Name] = m_eventDictionary[this.rotateLocalAxisToolStripMenuItem.Name] - value;
            }
        }

        public event ParameterizedOperationEventHandler MakeRotateClick
        {
            add
            {
                m_eventDictionary[this.makeRotateToolStripMenuItem.Name] = m_eventDictionary[this.makeRotateToolStripMenuItem.Name] + value;
            }
            remove
            {
                m_eventDictionary[this.makeRotateToolStripMenuItem.Name] = m_eventDictionary[this.makeRotateToolStripMenuItem.Name] - value;
            }
        }

        public event ParameterizedOperationEventHandler MakeRotateXClick
        {
            add
            {
                m_eventDictionary[this.makeRotateXToolStripMenuItem.Name] = m_eventDictionary[this.makeRotateXToolStripMenuItem.Name] + value;
            }
            remove
            {
                m_eventDictionary[this.makeRotateXToolStripMenuItem.Name] = m_eventDictionary[this.makeRotateXToolStripMenuItem.Name] - value;
            }
        }

        public event ParameterizedOperationEventHandler MakeRotateYClick
        {
            add
            {
                m_eventDictionary[this.makeRotateYToolStripMenuItem.Name] = m_eventDictionary[this.makeRotateYToolStripMenuItem.Name] + value;
            }
            remove
            {
                m_eventDictionary[this.makeRotateYToolStripMenuItem.Name] = m_eventDictionary[this.makeRotateYToolStripMenuItem.Name] - value;
            }
        }

        public event ParameterizedOperationEventHandler MakeRotateZClick
        {
            add
            {
                m_eventDictionary[this.makeRotateZToolStripMenuItem.Name] = m_eventDictionary[this.makeRotateZToolStripMenuItem.Name] + value;
            }
            remove
            {
                m_eventDictionary[this.makeRotateZToolStripMenuItem.Name] = m_eventDictionary[this.makeRotateZToolStripMenuItem.Name] - value;
            }
        }

        public event ParameterizedOperationEventHandler MakeRotateUnitAxisClick
        {
            add
            {
                m_eventDictionary[this.makeRotateUnitAxisToolStripMenuItem.Name] = m_eventDictionary[this.makeRotateUnitAxisToolStripMenuItem.Name] + value;
            }
            remove
            {
                m_eventDictionary[this.makeRotateUnitAxisToolStripMenuItem.Name] = m_eventDictionary[this.makeRotateUnitAxisToolStripMenuItem.Name] - value;
            }
        }

        public event ParameterizedOperationEventHandler RotateToClick
        {
            add
            {
                m_eventDictionary[this.rotateToToolStripMenuItem.Name] = m_eventDictionary[this.rotateToToolStripMenuItem.Name] + value;
            }
            remove
            {
                m_eventDictionary[this.rotateToToolStripMenuItem.Name] = m_eventDictionary[this.rotateToToolStripMenuItem.Name] - value;
            }
        }

        public event ParameterizedOperationEventHandler MakeRotateToClick
        {
            add
            {
                m_eventDictionary[this.makeRotateToToolStripMenuItem.Name] = m_eventDictionary[this.makeRotateToToolStripMenuItem.Name] + value;
            }
            remove
            {
                m_eventDictionary[this.makeRotateToToolStripMenuItem.Name] = m_eventDictionary[this.makeRotateToToolStripMenuItem.Name] - value;
            }
        }

        public event ParameterizedOperationEventHandler RotateFullClick
        {
            add
            {
                m_eventDictionary[this.rotateFullToolStripMenuItem.Name] = m_eventDictionary[this.rotateFullToolStripMenuItem.Name] + value;
            }
            remove
            {
                m_eventDictionary[this.rotateFullToolStripMenuItem.Name] = m_eventDictionary[this.rotateFullToolStripMenuItem.Name] - value;
            }
        }

        public event ParameterizedOperationEventHandler RotateFullXClick
        {
            add
            {
                m_eventDictionary[this.rotateFullXToolStripMenuItem.Name] = m_eventDictionary[this.rotateFullXToolStripMenuItem.Name] + value;
            }
            remove
            {
                m_eventDictionary[this.rotateFullXToolStripMenuItem.Name] = m_eventDictionary[this.rotateFullXToolStripMenuItem.Name] - value;
            }
        }

        public event ParameterizedOperationEventHandler RotateFullYClick
        {
            add
            {
                m_eventDictionary[this.rotateFullYToolStripMenuItem.Name] = m_eventDictionary[this.rotateFullYToolStripMenuItem.Name] + value;
            }
            remove
            {
                m_eventDictionary[this.rotateFullYToolStripMenuItem.Name] = m_eventDictionary[this.rotateFullYToolStripMenuItem.Name] - value;
            }
        }

        public event ParameterizedOperationEventHandler RotateFullZClick
        {
            add
            {
                m_eventDictionary[this.rotateFullZToolStripMenuItem.Name] = m_eventDictionary[this.rotateFullZToolStripMenuItem.Name] + value;
            }
            remove
            {
                m_eventDictionary[this.rotateFullZToolStripMenuItem.Name] = m_eventDictionary[this.rotateFullZToolStripMenuItem.Name] - value;
            }
        }

        public event ParameterizedOperationEventHandler RotateFullUnitAxisClick
        {
            add
            {
                m_eventDictionary[this.rotateFullUnitAxisToolStripMenuItem.Name] = m_eventDictionary[this.rotateFullUnitAxisToolStripMenuItem.Name] + value;
            }
            remove
            {
                m_eventDictionary[this.rotateFullUnitAxisToolStripMenuItem.Name] = m_eventDictionary[this.rotateFullUnitAxisToolStripMenuItem.Name] - value;
            }
        }

        public event ParameterizedOperationEventHandler MakeRotXClick
        {
            add
            {
                m_eventDictionary[this.makeRotXToolStripMenuItem.Name] = m_eventDictionary[this.makeRotXToolStripMenuItem.Name] + value;
            }
            remove
            {
                m_eventDictionary[this.makeRotXToolStripMenuItem.Name] = m_eventDictionary[this.makeRotXToolStripMenuItem.Name] - value;
            }
        }

        public event ParameterizedOperationEventHandler MakeRotYClick
        {
            add
            {
                m_eventDictionary[this.makeRotYToolStripMenuItem.Name] = m_eventDictionary[this.makeRotYToolStripMenuItem.Name] + value;
            }
            remove
            {
                m_eventDictionary[this.makeRotYToolStripMenuItem.Name] = m_eventDictionary[this.makeRotYToolStripMenuItem.Name] - value;
            }
        }

        public event ParameterizedOperationEventHandler MakeRotZClick
        {
            add
            {
                m_eventDictionary[this.makeRotZToolStripMenuItem.Name] = m_eventDictionary[this.makeRotZToolStripMenuItem.Name] + value;
            }
            remove
            {
                m_eventDictionary[this.makeRotZToolStripMenuItem.Name] = m_eventDictionary[this.makeRotZToolStripMenuItem.Name] - value;
            }
        }

        public event ParameterizedOperationEventHandler DotCrossProdMtxClick
        {
            add
            {
                m_eventDictionary[this.dotCrossProdMtxToolStripMenuItem.Name] = m_eventDictionary[this.dotCrossProdMtxToolStripMenuItem.Name] + value;
            }
            remove
            {
                m_eventDictionary[this.dotCrossProdMtxToolStripMenuItem.Name] = m_eventDictionary[this.dotCrossProdMtxToolStripMenuItem.Name] - value;
            }
        }

        public event ParameterizedOperationEventHandler DotCrossProdTransposeClick
        {
            add
            {
                m_eventDictionary[this.dotCrossProdTransposeToolStripMenuItem.Name] = m_eventDictionary[this.dotCrossProdTransposeToolStripMenuItem.Name] + value;
            }
            remove
            {
                m_eventDictionary[this.dotCrossProdTransposeToolStripMenuItem.Name] = m_eventDictionary[this.dotCrossProdTransposeToolStripMenuItem.Name] - value;
            }
        }

        public event ParameterizedOperationEventHandler MakeDoubleCrossMatrixClick
        {
            add
            {
                m_eventDictionary[this.makeDoubleCrossMatrixToolStripMenuItem.Name] = m_eventDictionary[this.makeDoubleCrossMatrixToolStripMenuItem.Name] + value;
            }
            remove
            {
                m_eventDictionary[this.makeDoubleCrossMatrixToolStripMenuItem.Name] = m_eventDictionary[this.makeDoubleCrossMatrixToolStripMenuItem.Name] - value;
            }
        }

        public event ParameterizedOperationEventHandler MirrorOnPlaneClick
        {
            add
            {
                m_eventDictionary[this.mirrorOnPlaneToolStripMenuItem.Name] = m_eventDictionary[this.mirrorOnPlaneToolStripMenuItem.Name] + value;
            }
            remove
            {
                m_eventDictionary[this.mirrorOnPlaneToolStripMenuItem.Name] = m_eventDictionary[this.mirrorOnPlaneToolStripMenuItem.Name] - value;
            }
        }

        public event ParameterizedOperationEventHandler LookDownClick
        {
            add
            {
                m_eventDictionary[this.lookDownToolStripMenuItem.Name] = m_eventDictionary[this.lookDownToolStripMenuItem.Name] + value;
            }
            remove
            {
                m_eventDictionary[this.lookDownToolStripMenuItem.Name] = m_eventDictionary[this.lookDownToolStripMenuItem.Name] - value;
            }
        }

        public event ParameterizedOperationEventHandler LookAtClick
        {
            add
            {
                m_eventDictionary[this.lookAtToolStripMenuItem.Name] = m_eventDictionary[this.lookAtToolStripMenuItem.Name] + value;
            }
            remove
            {
                m_eventDictionary[this.lookAtToolStripMenuItem.Name] = m_eventDictionary[this.lookAtToolStripMenuItem.Name] - value;
            }
        }

        public event ParameterizedOperationEventHandler PolarViewClick
        {
            add
            {
                m_eventDictionary[this.polarViewToolStripMenuItem.Name] = m_eventDictionary[this.polarViewToolStripMenuItem.Name] + value;
            }
            remove
            {
                m_eventDictionary[this.polarViewToolStripMenuItem.Name] = m_eventDictionary[this.polarViewToolStripMenuItem.Name] - value;
            }
        }

        public event ParameterizedOperationEventHandler Dot3x3CrossProdMtxClick
        {
            add
            {
                m_eventDictionary[this.dot3x3CrossProdMtxToolStripMenuItem.Name] = m_eventDictionary[this.dot3x3CrossProdMtxToolStripMenuItem.Name] + value;
            }
            remove
            {
                m_eventDictionary[this.dot3x3CrossProdMtxToolStripMenuItem.Name] = m_eventDictionary[this.dot3x3CrossProdMtxToolStripMenuItem.Name] - value;
            }
        }

        public event ParameterizedOperationEventHandler Dot3x3CrossProdTransposeClick
        {
            add
            {
                m_eventDictionary[this.dot3x3CrossProdTransposeToolStripMenuItem.Name] = m_eventDictionary[this.dot3x3CrossProdTransposeToolStripMenuItem.Name] + value;
            }
            remove
            {
                m_eventDictionary[this.dot3x3CrossProdTransposeToolStripMenuItem.Name] = m_eventDictionary[this.dot3x3CrossProdTransposeToolStripMenuItem.Name] - value;
            }
        }

        public event ParameterizedOperationEventHandler InterpolateClick
        {
            add
            {
                m_eventDictionary[this.interpolateToolStripMenuItem.Name] = m_eventDictionary[this.interpolateToolStripMenuItem.Name] + value;
            }
            remove
            {
                m_eventDictionary[this.interpolateToolStripMenuItem.Name] = m_eventDictionary[this.interpolateToolStripMenuItem.Name] - value;
            }
        }

        public event ParameterizedOperationEventHandler MakePosRotYClick
        {
            add
            {
                m_eventDictionary[this.makePosRotYToolStripMenuItem.Name] = m_eventDictionary[this.makePosRotYToolStripMenuItem.Name] + value;
            }
            remove
            {
                m_eventDictionary[this.makePosRotYToolStripMenuItem.Name] = m_eventDictionary[this.makePosRotYToolStripMenuItem.Name] - value;
            }
        }

        public event ParameterizedOperationEventHandler MakeReflectClick
        {
            add
            {
                m_eventDictionary[this.makeReflectToolStripMenuItem.Name] = m_eventDictionary[this.makeReflectToolStripMenuItem.Name] + value;
            }
            remove
            {
                m_eventDictionary[this.makeReflectToolStripMenuItem.Name] = m_eventDictionary[this.makeReflectToolStripMenuItem.Name] - value;
            }
        }

        public event EventHandler IdentityClick
        {
            add
            {
                this.identityToolStripMenuItem.Click += value;
            }
            remove
            {
                this.identityToolStripMenuItem.Click -= value;
            }
        }

        public event EventHandler ZeroClick
        {
            add
            {
                this.zeroToolStripMenuItem.Click += value;
            }
            remove
            {
                this.zeroToolStripMenuItem.Click -= value;
            }
        }

        public event EventHandler AbsClick
        {
            add
            {
                this.absToolStripMenuItem.Click += value;
            }
            remove
            {
                this.absToolStripMenuItem.Click -= value;
            }
        }

        public event EventHandler NegateClick
        {
            add
            {
                this.negateToolStripMenuItem.Click += value;
            }
            remove
            {
                this.negateToolStripMenuItem.Click -= value;
            }
        }

        public event EventHandler InverseClick
        {
            add
            {
                this.inverseToolStripMenuItem.Click += value;
            }
            remove
            {
                this.inverseToolStripMenuItem.Click -= value;
            }
        }

        public event EventHandler FastInverseClick
        {
            add
            {
                this.fastInverseToolStripMenuItem.Click += value;
            }
            remove
            {
                this.fastInverseToolStripMenuItem.Click -= value;
            }
        }

        public event EventHandler TransposeClick
        {
            add
            {
                this.transposeToolStripMenuItem.Click += value;
            }
            remove
            {
                this.transposeToolStripMenuItem.Click -= value;
            }
        }

        public event EventHandler CoordinateInverseSafeClick
        {
            add
            {
                this.coordinateInverseSafeToolStripMenuItem.Click += value;
            }
            remove
            {
                this.coordinateInverseSafeToolStripMenuItem.Click -= value;
            }
        }

        public event EventHandler NormalizeClick
        {
            add
            {
                this.normalizeToolStripMenuItem.Click += value;
            }
            remove
            {
                this.normalizeToolStripMenuItem.Click -= value;
            }
        }

        public event EventHandler NormalizeSafeClick
        {
            add
            {
                this.normalizeSafeToolStripMenuItem.Click += value;
            }
            remove
            {
                this.normalizeSafeToolStripMenuItem.Click -= value;
            }
        }

        public event EventHandler Identity3x3Click
        {
            add
            {
                this.identity3x3ToolStripMenuItem.Click += value;
            }
            remove
            {
                this.identity3x3ToolStripMenuItem.Click -= value;
            }
        }

        public event EventHandler Zero3x3Click
        {
            add
            {
                this.zero3x3ToolStripMenuItem.Click += value;
            }
            remove
            {
                this.zero3x3ToolStripMenuItem.Click -= value;
            }
        }

        public event EventHandler Negate3x3Click
        {
            add
            {
                this.negate3x3ToolStripMenuItem.Click += value;
            }
            remove
            {
                this.negate3x3ToolStripMenuItem.Click -= value;
            }
        }

        public event EventHandler Inverse3x3Click
        {
            add
            {
                this.inverse3x3ToolStripMenuItem.Click += value;
            }
            remove
            {
                this.inverse3x3ToolStripMenuItem.Click -= value;
            }
        }

        public event EventHandler Transpose3x4Click
        {
            add
            {
                this.transpose3x4ToolStripMenuItem.Click += value;
            }
            remove
            {
                this.transpose3x4ToolStripMenuItem.Click -= value;
            }
        }

        public event EventHandler MakeUprightClick
        {
            add
            {
                this.makeUprightToolStripMenuItem.Click += value;
            }
            remove
            {
                this.makeUprightToolStripMenuItem.Click -= value;
            }
        }
        #endregion

        #region ISupportInitialize
        public void BeginInit()
        {
            foreach ( Control c in this.Controls )
            {
                c.SuspendLayout();
            }

            this.SuspendLayout();
        }

        public void EndInit()
        {
            foreach ( Control c in this.Controls )
            {
                c.ResumeLayout( false );
            }

            this.toolStrip1.PerformLayout();
            this.ResumeLayout( false );
            this.PerformLayout();
        }
        #endregion

        #region Private Functions
        private void InitializeAdditionalComponents( System.Xml.XmlNode schema )
        {
            if ( schema != null )
            {
                this.Text = schema.Attributes["name"].Value;
                this.Minimum = decimal.Parse( schema.Attributes["Minimum"].Value );
                this.Maximum = decimal.Parse( schema.Attributes["Maximum"].Value );
                this.NumRows = int.Parse( schema.Attributes["NumRows"].Value );
                this.NumColumns = int.Parse( schema.Attributes["NumColumns"].Value );
                this.Step = decimal.Parse( schema.Attributes["Step"].Value );
            }

            this.row1ControlVector.Text = "Row 1";
            this.row2ControlVector.Text = "Row 2";
            this.row3ControlVector.Text = "Row 3";
            this.row4ControlVector.Text = "Row 4";

            m_eventDictionary.Add( setDiagonalToolStripMenuItem.Name, null );
            m_eventDictionary.Add( addToolStripMenuItem.Name, null );
            m_eventDictionary.Add( subtractToolStripMenuItem.Name, null );
            m_eventDictionary.Add( scaleToolStripMenuItem.Name, null );
            m_eventDictionary.Add( makeScaleToolStripMenuItem.Name, null );
            m_eventDictionary.Add( add3x3ToolStripMenuItem.Name, null );
            m_eventDictionary.Add( subtract3x3ToolStripMenuItem.Name, null );
            m_eventDictionary.Add( scaleFullToolStripMenuItem.Name, null );
            m_eventDictionary.Add( translateToolStripMenuItem.Name, null );
            m_eventDictionary.Add( makeTranslateToolStripMenuItem.Name, null );
            m_eventDictionary.Add( makePosToolStripMenuItem.Name, null );
            m_eventDictionary.Add( makeScaleFullToolStripMenuItem.Name, null );
            m_eventDictionary.Add( dotToolStripMenuItem.Name, null );
            m_eventDictionary.Add( dot3x3ToolStripMenuItem.Name, null );
            m_eventDictionary.Add( dotFromLeftToolStripMenuItem.Name, null );
            m_eventDictionary.Add( dotTransposeToolStripMenuItem.Name, null );
            m_eventDictionary.Add( crossProductToolStripMenuItem.Name, null );
            m_eventDictionary.Add( outerProductToolStripMenuItem.Name, null );
            m_eventDictionary.Add( dot3x3FromLeftToolStripMenuItem.Name, null );
            m_eventDictionary.Add( dot3x3TransposeToolStripMenuItem.Name, null );
            m_eventDictionary.Add( rotateXToolStripMenuItem.Name, null );
            m_eventDictionary.Add( rotateYToolStripMenuItem.Name, null );
            m_eventDictionary.Add( rotateZToolStripMenuItem.Name, null );
            m_eventDictionary.Add( rotateUnitAxisToolStripMenuItem.Name, null );
            m_eventDictionary.Add( rotateToolStripMenuItem.Name, null );
            m_eventDictionary.Add( rotateLocalXToolStripMenuItem.Name, null );
            m_eventDictionary.Add( rotateLocalYToolStripMenuItem.Name, null );
            m_eventDictionary.Add( rotateLocalZToolStripMenuItem.Name, null );
            m_eventDictionary.Add( rotateLocalAxisToolStripMenuItem.Name, null );
            m_eventDictionary.Add( makeRotateToolStripMenuItem.Name, null );
            m_eventDictionary.Add( makeRotateXToolStripMenuItem.Name, null );
            m_eventDictionary.Add( makeRotateYToolStripMenuItem.Name, null );
            m_eventDictionary.Add( makeRotateZToolStripMenuItem.Name, null );
            m_eventDictionary.Add( makeRotateUnitAxisToolStripMenuItem.Name, null );
            m_eventDictionary.Add( rotateToToolStripMenuItem.Name, null );
            m_eventDictionary.Add( makeRotateToToolStripMenuItem.Name, null );
            m_eventDictionary.Add( rotateFullToolStripMenuItem.Name, null );
            m_eventDictionary.Add( rotateFullXToolStripMenuItem.Name, null );
            m_eventDictionary.Add( rotateFullYToolStripMenuItem.Name, null );
            m_eventDictionary.Add( rotateFullZToolStripMenuItem.Name, null );
            m_eventDictionary.Add( rotateFullUnitAxisToolStripMenuItem.Name, null );
            m_eventDictionary.Add( makeRotXToolStripMenuItem.Name, null );
            m_eventDictionary.Add( makeRotYToolStripMenuItem.Name, null );
            m_eventDictionary.Add( makeRotZToolStripMenuItem.Name, null );
            m_eventDictionary.Add( coordinateInverseSafeToolStripMenuItem.Name, null );
            m_eventDictionary.Add( dotCrossProdMtxToolStripMenuItem.Name, null );
            m_eventDictionary.Add( dotCrossProdTransposeToolStripMenuItem.Name, null );
            m_eventDictionary.Add( makeDoubleCrossMatrixToolStripMenuItem.Name, null );
            m_eventDictionary.Add( mirrorOnPlaneToolStripMenuItem.Name, null );
            m_eventDictionary.Add( lookDownToolStripMenuItem.Name, null );
            m_eventDictionary.Add( lookAtToolStripMenuItem.Name, null );
            m_eventDictionary.Add( polarViewToolStripMenuItem.Name, null );
            m_eventDictionary.Add( dot3x3CrossProdMtxToolStripMenuItem.Name, null );
            m_eventDictionary.Add( dot3x3CrossProdTransposeToolStripMenuItem.Name, null );
            m_eventDictionary.Add( interpolateToolStripMenuItem.Name, null );
            m_eventDictionary.Add( makePosRotYToolStripMenuItem.Name, null );
            m_eventDictionary.Add( makeReflectToolStripMenuItem.Name, null );

            m_eventTextDictionary.Add( setDiagonalToolStripMenuItem.Name, "Vector of the Diagonal" );
            m_eventTextDictionary.Add( addToolStripMenuItem.Name, "Matrix to Add" );
            m_eventTextDictionary.Add( subtractToolStripMenuItem.Name, "Matrix to Subtract" );
            m_eventTextDictionary.Add( scaleToolStripMenuItem.Name, "Scale Factor" );
            m_eventTextDictionary.Add( makeScaleToolStripMenuItem.Name, "Scale Factor" );
            m_eventTextDictionary.Add( add3x3ToolStripMenuItem.Name, "Matrix to Add" );
            m_eventTextDictionary.Add( subtract3x3ToolStripMenuItem.Name, "Matrix to Subtract" );
            m_eventTextDictionary.Add( scaleFullToolStripMenuItem.Name, "Scale Factor" );
            m_eventTextDictionary.Add( translateToolStripMenuItem.Name, "Translation Vector" );
            m_eventTextDictionary.Add( makeTranslateToolStripMenuItem.Name, "Translation Vector" );
            m_eventTextDictionary.Add( makePosToolStripMenuItem.Name, "Position Vector" );
            m_eventTextDictionary.Add( makeScaleFullToolStripMenuItem.Name, "Scale Factor" );
            m_eventTextDictionary.Add( dotToolStripMenuItem.Name, "Second Matrix in the Dot Product" );
            m_eventTextDictionary.Add( dot3x3ToolStripMenuItem.Name, "Second Matrix in the Dot Product" );
            m_eventTextDictionary.Add( dotFromLeftToolStripMenuItem.Name, "Second Matrix in the Dot Product" );
            m_eventTextDictionary.Add( dotTransposeToolStripMenuItem.Name, "Second Matrix in the Dot Product" );
            m_eventTextDictionary.Add( crossProductToolStripMenuItem.Name, "Vector in the Cross Product" );
            m_eventTextDictionary.Add( outerProductToolStripMenuItem.Name, "First Vector,Second Vector" );
            m_eventTextDictionary.Add( dot3x3FromLeftToolStripMenuItem.Name, "Second Matrix in the Dot Product" );
            m_eventTextDictionary.Add( dot3x3TransposeToolStripMenuItem.Name, "Second Matrix in the Dot Product" );
            m_eventTextDictionary.Add( rotateXToolStripMenuItem.Name, "Radians About the X Axis" );
            m_eventTextDictionary.Add( rotateYToolStripMenuItem.Name, "Radians About the Y Axis" );
            m_eventTextDictionary.Add( rotateZToolStripMenuItem.Name, "Radians About the Z Axis" );
            m_eventTextDictionary.Add( rotateUnitAxisToolStripMenuItem.Name, "Radians About the Axis,Vector to Rotate Around" );
            m_eventTextDictionary.Add( rotateToolStripMenuItem.Name, "" );
            m_eventTextDictionary.Add( rotateLocalXToolStripMenuItem.Name, "Radians About the X Axis" );
            m_eventTextDictionary.Add( rotateLocalYToolStripMenuItem.Name, "Radians About the Y Axis" );
            m_eventTextDictionary.Add( rotateLocalZToolStripMenuItem.Name, "Radians About the Z Axis" );
            m_eventTextDictionary.Add( rotateLocalAxisToolStripMenuItem.Name, "Radians About the Axis,Axis to Rotate Around" );
            m_eventTextDictionary.Add( makeRotateToolStripMenuItem.Name, "Radians Around the Axis,Vector to Rotate Around" );
            m_eventTextDictionary.Add( makeRotateXToolStripMenuItem.Name, "Radians About the X Axis" );
            m_eventTextDictionary.Add( makeRotateYToolStripMenuItem.Name, "Radians About the Y Axis" );
            m_eventTextDictionary.Add( makeRotateZToolStripMenuItem.Name, "Radians About the Z Axis" );
            m_eventTextDictionary.Add( makeRotateUnitAxisToolStripMenuItem.Name, "Radians About the Axis,Vector to Rotate Around" );
            m_eventTextDictionary.Add( rotateToToolStripMenuItem.Name, "From Vector,To Vector,Scale Factor" );
            m_eventTextDictionary.Add( makeRotateToToolStripMenuItem.Name, "Radians About the Axis,Vector to Rotate Around" );
            m_eventTextDictionary.Add( rotateFullToolStripMenuItem.Name, "Radians About the Axis,Vector to Rotate Around" );
            m_eventTextDictionary.Add( rotateFullXToolStripMenuItem.Name, "Radians About the X Axis" );
            m_eventTextDictionary.Add( rotateFullYToolStripMenuItem.Name, "Radians About the Y Axis" );
            m_eventTextDictionary.Add( rotateFullZToolStripMenuItem.Name, "Radians About the Z Axis" );
            m_eventTextDictionary.Add( rotateFullUnitAxisToolStripMenuItem.Name, "Radians About the Axis,Vector to Rotate Around" );
            m_eventTextDictionary.Add( makeRotXToolStripMenuItem.Name, "Radians About the X Axis" );
            m_eventTextDictionary.Add( makeRotYToolStripMenuItem.Name, "Radians About the Y Axis" );
            m_eventTextDictionary.Add( makeRotZToolStripMenuItem.Name, "Radians About the Z Axis" );
            m_eventTextDictionary.Add( dotCrossProdMtxToolStripMenuItem.Name, "Vector in the Cross Product" );
            m_eventTextDictionary.Add( dotCrossProdTransposeToolStripMenuItem.Name, "Vector in the Cross Product" );
            m_eventTextDictionary.Add( makeDoubleCrossMatrixToolStripMenuItem.Name, "First Vector,Second Vector" );
            m_eventTextDictionary.Add( mirrorOnPlaneToolStripMenuItem.Name, "Vector Representing the Plane" );
            m_eventTextDictionary.Add( lookDownToolStripMenuItem.Name, "Look Direction Vector,Up Vector" );
            m_eventTextDictionary.Add( lookAtToolStripMenuItem.Name, "Look At Position,Up Vector" );
            m_eventTextDictionary.Add( polarViewToolStripMenuItem.Name, "Distance,Azimuth (Radians),Inclination (Radians),Twist or Roll (Radians)" );
            m_eventTextDictionary.Add( dot3x3CrossProdMtxToolStripMenuItem.Name, "Vector in the Cross Product" );
            m_eventTextDictionary.Add( dot3x3CrossProdTransposeToolStripMenuItem.Name, "Vector in the Cross Product" );
            m_eventTextDictionary.Add( interpolateToolStripMenuItem.Name, "Matrix to Interpolate To,Interpolation Value" );
            m_eventTextDictionary.Add( makePosRotYToolStripMenuItem.Name, "Position,Cosine Theta,Sin Theta" );
            m_eventTextDictionary.Add( makeReflectToolStripMenuItem.Name, "Vector Representing the Plane" );
        }

        private void EnableDropDownMenuItems()
        {
            bool is33 = (this.NumColumns == 3) && (this.NumRows == 3);
            bool is34 = (this.NumColumns == 3) && (this.NumRows == 4);
            bool is44 = (this.NumColumns == 4) && (this.NumRows == 4);
            bool is33or34 = is33 || is34;

            this.setDiagonalToolStripMenuItem.Enabled = is33or34;
            this.absToolStripMenuItem.Enabled = is33or34;
            this.negateToolStripMenuItem.Enabled = is33or34;
            this.scaleToolStripMenuItem.Enabled = is33or34;
            this.identity3x3ToolStripMenuItem.Enabled = is34;
            this.zero3x3ToolStripMenuItem.Enabled = is34;
            this.add3x3ToolStripMenuItem.Enabled = is34;
            this.subtract3x3ToolStripMenuItem.Enabled = is34;
            this.negate3x3ToolStripMenuItem.Enabled = is34;
            this.scaleFullToolStripMenuItem.Enabled = is34;
            this.translateToolStripMenuItem.Enabled = is34;
            this.makeTranslateToolStripMenuItem.Enabled = is34;
            this.makePosToolStripMenuItem.Enabled = is44;
            this.dot3x3ToolStripMenuItem.Enabled = is33or34;
            this.dotFromLeftToolStripMenuItem.Enabled = is33or34;
            this.dotTransposeToolStripMenuItem.Enabled = is33or34;
            this.crossProductToolStripMenuItem.Enabled = is33or34;
            this.outerProductToolStripMenuItem.Enabled = is33or34;
            this.normalizeToolStripMenuItem.Enabled = is33or34;
            this.normalizeSafeToolStripMenuItem.Enabled = is33or34;
            this.dot3x3FromLeftToolStripMenuItem.Enabled = is34;
            this.dot3x3TransposeToolStripMenuItem.Enabled = is34;
            this.inverse3x3ToolStripMenuItem.Enabled = is34;
            this.transpose3x4ToolStripMenuItem.Enabled = is34;
            this.rotateXToolStripMenuItem.Enabled = is33or34;
            this.rotateYToolStripMenuItem.Enabled = is33or34;
            this.rotateZToolStripMenuItem.Enabled = is33or34;
            this.rotateUnitAxisToolStripMenuItem.Enabled = is33or34;
            this.rotateToolStripMenuItem.Enabled = is33or34;
            this.rotateLocalXToolStripMenuItem.Enabled = is33or34;
            this.rotateLocalYToolStripMenuItem.Enabled = is33or34;
            this.rotateLocalZToolStripMenuItem.Enabled = is33or34;
            this.rotateLocalAxisToolStripMenuItem.Enabled = is33or34;
            this.makeRotateToolStripMenuItem.Enabled = is33or34;
            this.makeRotateXToolStripMenuItem.Enabled = is33or34;
            this.makeRotateYToolStripMenuItem.Enabled = is33or34;
            this.makeRotateZToolStripMenuItem.Enabled = is33or34;
            this.makeRotateUnitAxisToolStripMenuItem.Enabled = is33or34;
            this.rotateToToolStripMenuItem.Enabled = is33or34;
            this.makeRotateToToolStripMenuItem.Enabled = is33or34;
            this.rotateFullToolStripMenuItem.Enabled = is34;
            this.rotateFullXToolStripMenuItem.Enabled = is34;
            this.rotateFullYToolStripMenuItem.Enabled = is34;
            this.rotateFullZToolStripMenuItem.Enabled = is34;
            this.rotateFullUnitAxisToolStripMenuItem.Enabled = is34;
            this.makeRotXToolStripMenuItem.Enabled = is44;
            this.makeRotYToolStripMenuItem.Enabled = is44;
            this.makeRotZToolStripMenuItem.Enabled = is44;
            this.makeUprightToolStripMenuItem.Enabled = is33or34;
            this.coordinateInverseSafeToolStripMenuItem.Enabled = is33or34;
            this.dotCrossProdMtxToolStripMenuItem.Enabled = is33;
            this.dotCrossProdTransposeToolStripMenuItem.Enabled = is33;
            this.makeDoubleCrossMatrixToolStripMenuItem.Enabled = is33or34;
            this.mirrorOnPlaneToolStripMenuItem.Enabled = is33or34;
            this.lookDownToolStripMenuItem.Enabled = is34;
            this.lookAtToolStripMenuItem.Enabled = is34;
            this.polarViewToolStripMenuItem.Enabled = is34;
            this.dot3x3CrossProdMtxToolStripMenuItem.Enabled = is34;
            this.dot3x3CrossProdTransposeToolStripMenuItem.Enabled = is34;
            this.interpolateToolStripMenuItem.Enabled = is34;
            this.makePosRotYToolStripMenuItem.Enabled = is44;
            this.makeReflectToolStripMenuItem.Enabled = is44;
        }

        private void SetValue( decimal[][] value, bool triggerEvent )
        {
            bool changed = false;
            ControlVector[] vectors = new ControlVector[] { this.row1ControlVector, this.row2ControlVector, 
                this.row3ControlVector, this.row4ControlVector };
            for ( int i = 0; i < 4; ++i )
            {
                for ( int j = 0; j < 4; ++j )
                {
                    if ( Convert.ToDecimal( vectors[i].Vector[j] ) != value[i][j] )
                    {
                        EventHandler eh = new EventHandler( ControlVector_ValueChanged );
                        vectors[i].ValueChanged -= eh;
                        vectors[i].UpdateValueFromRemote( new float[] { (float)value[i][0], (float)value[i][1], 
                            (float)value[i][2], (float)value[i][3] } );
                        vectors[i].ValueChanged += eh;

                        changed = true;
                        break;
                    }
                }
            }

            if ( !changed )
            {
                return;
            }

            if ( triggerEvent )
            {
                OnValueChanged();
            }
        }
        #endregion

        #region Public Functions
        public void SetRange( float min, float max )
        {
            try
            {
                this.Minimum = Convert.ToDecimal( min );
            }
            catch
            {
                if ( min < 0.0f )
                {
                    this.Minimum = decimal.MinValue;
                }
                else
                {
                    this.Minimum = decimal.MaxValue;
                }
            }

            try
            {
                this.Maximum = Convert.ToDecimal( max );
            }
            catch
            {
                if ( max < 0.0f )
                {
                    this.Maximum = decimal.MinValue;
                }
                else
                {
                    this.Maximum = decimal.MaxValue;
                }
            }
        }

        public void SetStep( float step )
        {
            try
            {
                this.Step = Convert.ToDecimal( step );
            }
            catch
            {
                if ( step < 0.0f )
                {
                    this.Step = decimal.MinValue;
                }
                else
                {
                    this.Step = decimal.MaxValue;
                }
            }
        }

        public void UpdateValueFromRemote( float[][] value )
        {
            decimal[][] values = new decimal[4][];
            for ( int i = 0; i < 4; ++i )
            {
                values[i] = new decimal[4];
                for ( int j = 0; j < 4; ++j )
                {
                    values[i][j] = Convert.ToDecimal( value[i][j] );
                }
            }

            SetValue( values, false );
        }
        #endregion

        #region Overrides
        public override bool CanCopy
        {
            get
            {
                return true;
            }
        }

        public override bool CanPaste
        {
            get
            {
                return true;
            }
        }

        public override bool CanUndo
        {
            get
            {
                return true;
            }
        }

        public override object ClipboardData
        {
            get
            {
				string strMatrixAsString = "";
				for (int i = 0; i < 4; ++i)
				{
					for (int j = 0; j < 4; ++j)
					{
						strMatrixAsString += this.Matrix[i][j];
						strMatrixAsString += " ";
					}
					strMatrixAsString += "\n";
				}
				return strMatrixAsString;
            }
            set
            {
				if((value != null) && (value is string) )
				{
					// Ok it is a string.  Are there any numbers in the string?
					string strClipboardString = value as string;
					string[] astrStringParts = strClipboardString.Split();
					List<decimal> afNumbersFromClipboard = new List<decimal>();
					foreach(string strPossibleNumber in astrStringParts)
					{
						decimal fResult;
						if (decimal.TryParse(strPossibleNumber, out fResult))
						{
							afNumbersFromClipboard.Add(fResult);
						}
					}

					// Got all the strings I could, now enter them
					decimal[][] matrix = this.Matrix;
					int i = 0;
					int j = 0;
					int iNoColumns = 4;
					if (afNumbersFromClipboard.Count == 12)
					{
						iNoColumns = 3;
					}

					foreach(decimal d in afNumbersFromClipboard)
					{
						matrix[i][j] = d;
						j++;
						if (j == iNoColumns)
						{
							i++;
							j = 0;
						}
						if (i == 4) break;
					}
					this.Matrix = matrix;
				}
            }
        }

        public override bool IsClipboardDataAvailable
        {
            get
            {
                return base.IsClipboardDataAvailable && !this.IsReadOnly;
            }
        }

        public override bool IsUndoable
        {
            get
            {
                return base.IsUndoable && (m_lastControlVectorChanged != null) && m_lastControlVectorChanged.IsUndoable;
            }
        }

        protected override void OnIconChanged()
        {
            //PadControlTextForIcon( this.toolStripDropDownButton1, m_Icon );
            this.toolStripDropDownButton1.Image = m_Icon;
        }

        public override void ConnectControlToXmlNode( System.Xml.XmlNode root )
        {
            DataBindings.Add( "Text", FindXPathNode( root, ValueXPath + "/@name" ), "Value" );
            DataBindings.Add( "Minimum", FindXPathNode( root, ValueXPath + "/@Minimum" ), "Value" );
            DataBindings.Add( "Maximum", FindXPathNode( root, ValueXPath + "/@Maximum" ), "Value" );
            DataBindings.Add( "NumRows", FindXPathNode( root, ValueXPath + "/@NumRows" ), "Value" );
            DataBindings.Add( "NumColumns", FindXPathNode( root, ValueXPath + "/@NumColumns" ), "Value" );
            DataBindings.Add( "Step", FindXPathNode( root, ValueXPath + "/@Step" ), "Value" );
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing )
            {
                if ( components != null )
                {
                    components.Dispose();
                }

                this.row1ControlVector.Dispose();
                this.row2ControlVector.Dispose();
                this.row3ControlVector.Dispose();
                this.row4ControlVector.Dispose();

                m_eventDictionary.Clear();
            }

            base.Dispose( disposing );
        }

        public override void Undo()
        {
            if ( m_lastControlVectorChanged != null )
            {
                m_lastControlVectorChanged.Undo();
            }
        }
        #endregion

        #region Event Handlers
        private void ControlVector_ValueChanged( object sender, EventArgs e )
        {
            if ( sender is ControlVector )
            {
                m_lastControlVectorChanged = sender as ControlVector;
            }

            OnValueChanged();
        }

        private void PromptFloat_ToolStripMenuItem_Click( object sender, EventArgs e )
        {
            ToolStripMenuItem item = sender as ToolStripMenuItem;

            float f = PromptFloatForm.ShowIt( this, m_eventTextDictionary[item.Name] );
            if ( f != float.MinValue )
            {
                OnParameterizedOperationEvent( item, new MatrixOperationParameterEventArgs( f ) );
            }
        }

        private void PromptFloatFloatFloat_ToolStripMenuItem_Click( object sender, EventArgs e )
        {
            ToolStripMenuItem item = sender as ToolStripMenuItem;
            string[] split = m_eventTextDictionary[item.Name].Split( new char[] { ',' } );

            float f1 = PromptFloatForm.ShowIt( this, split[0] );
            if ( f1 != float.MinValue )
            {
                float f2 = PromptFloatForm.ShowIt( this, split[1] );
                if ( f2 != float.MinValue )
                {
                    float f3 = PromptFloatForm.ShowIt( this, split[2] );
                    if ( f3 != float.MinValue )
                    {
                        OnParameterizedOperationEvent( item, new MatrixOperationParameterEventArgs( f1, f2, f3 ) );
                    }
                }
            }
        }

        private void PromptFloatFloatFloatFloat_ToolStripMenuItem_Click( object sender, EventArgs e )
        {
            ToolStripMenuItem item = sender as ToolStripMenuItem;
            string[] split = m_eventTextDictionary[item.Name].Split( new char[] { ',' } );

            float f1 = PromptFloatForm.ShowIt( this, split[0] );
            if ( f1 != float.MinValue )
            {
                float f2 = PromptFloatForm.ShowIt( this, split[1] );
                if ( f2 != float.MinValue )
                {
                    float f3 = PromptFloatForm.ShowIt( this, split[2] );
                    if ( f3 != float.MinValue )
                    {
                        float f4 = PromptFloatForm.ShowIt( this, split[3] );
                        if ( f4 != float.MinValue )
                        {
                            OnParameterizedOperationEvent( item, new MatrixOperationParameterEventArgs( f1, f2, f3, f4 ) );
                        }
                    }
                }
            }
        }

        private void PromptVector_ToolStripMenuItem_Click( object sender, EventArgs e )
        {
            ToolStripMenuItem item = sender as ToolStripMenuItem;

            // special case
            int numComponents = (item == this.mirrorOnPlaneToolStripMenuItem) ? 4 : this.NumColumns;

            decimal[] v = PromptVectorForm.ShowIt( this, m_eventTextDictionary[item.Name],
                numComponents, this.Minimum, this.Maximum, this.Step );
            if ( v != null )
            {
                float[] val = new float[] { (float)v[0], (float)v[1], (float)v[2], (float)v[3] };
                OnParameterizedOperationEvent( item, new MatrixOperationParameterEventArgs( val ) );
            }
        }

        private void PromptVectorVector_ToolStripMenuItem_Click( object sender, EventArgs e )
        {
            ToolStripMenuItem item = sender as ToolStripMenuItem;
            string[] split = m_eventTextDictionary[item.Name].Split( new char[] { ',' } );

            decimal[] v1 = PromptVectorForm.ShowIt( this, split[0], this.NumColumns, this.Minimum, this.Maximum, this.Step );
            if ( v1 != null )
            {
                float[] val1 = new float[] { (float)v1[0], (float)v1[1], (float)v1[2], (float)v1[3] };
                
                decimal[] v2 = PromptVectorForm.ShowIt( this, split[1], this.NumColumns, this.Minimum, this.Maximum, this.Step );
                if ( v2 != null )
                {
                    float[] val2 = new float[] { (float)v2[0], (float)v2[1], (float)v2[2], (float)v2[3] };
                    OnParameterizedOperationEvent( item, new MatrixOperationParameterEventArgs( val1, val2 ) );
                }
            }
        }

        private void PromptVectorVectorFloat_ToolStripMenuItem_Click( object sender, EventArgs e )
        {
            ToolStripMenuItem item = sender as ToolStripMenuItem;
            string[] split = m_eventTextDictionary[item.Name].Split( new char[] { ',' } );

            decimal[] v1 = PromptVectorForm.ShowIt( this, split[0], this.NumColumns, this.Minimum, this.Maximum, this.Step );
            if ( v1 != null )
            {
                float[] val1 = new float[] { (float)v1[0], (float)v1[1], (float)v1[2], (float)v1[3] };

                decimal[] v2 = PromptVectorForm.ShowIt( this, split[1], this.NumColumns, this.Minimum, this.Maximum, this.Step );
                if ( v2 != null )
                {
                    float[] val2 = new float[] { (float)v2[0], (float)v2[1], (float)v2[2], (float)v2[3] };

                    float f = PromptFloatForm.ShowIt( this, split[2] );
                    if ( f != float.MinValue )
                    {
                        OnParameterizedOperationEvent( item, new MatrixOperationParameterEventArgs( val1, val2, f ) );
                    }
                }
            }
        }

        private void PromptVectorFloat_ToolStripMenuItem_Click( object sender, EventArgs e )
        {
            ToolStripMenuItem item = sender as ToolStripMenuItem;
            string[] split = m_eventTextDictionary[item.Name].Split( new char[] { ',' } );

            decimal[] v = PromptVectorForm.ShowIt( this, split[0], this.NumColumns, this.Minimum, this.Maximum, this.Step );
            if ( v != null )
            {
                float[] val = new float[] { (float)v[0], (float)v[1], (float)v[2], (float)v[3] };

                float f = PromptFloatForm.ShowIt( this, split[1] );
                if ( f != float.MinValue )
                {
                    OnParameterizedOperationEvent( item, new MatrixOperationParameterEventArgs( f, val ) );
                }
            }
        }

        private void PromptVectorFloatFloat_ToolStripMenuItem_Click( object sender, EventArgs e )
        {
            ToolStripMenuItem item = sender as ToolStripMenuItem;
            string[] split = m_eventTextDictionary[item.Name].Split( new char[] { ',' } );

            int numComponents = (item == this.makePosRotYToolStripMenuItem) ? 3 : this.NumColumns;

            decimal[] v = PromptVectorForm.ShowIt( this, split[0], numComponents, this.Minimum, this.Maximum, this.Step );
            if ( v != null )
            {
                float[] val = new float[] { (float)v[0], (float)v[1], (float)v[2], (float)v[3] };

                float f1 = PromptFloatForm.ShowIt( this, split[1] );
                if ( f1 != float.MinValue )
                {
                    float f2 = PromptFloatForm.ShowIt( this, split[2] );
                    if ( f2 != float.MinValue )
                    {
                        OnParameterizedOperationEvent( item, new MatrixOperationParameterEventArgs( f1, f2, val ) );
                    }
                }
            }
        }

        private void PromptMatrix_ToolStripMenuItem_Click( object sender, EventArgs e )
        {
            ToolStripMenuItem item = sender as ToolStripMenuItem;

            int rows = (item == this.dot3x3ToolStripMenuItem) ? 4 : this.NumRows;
            int cols = (item == this.dot3x3ToolStripMenuItem) ? 3 : this.NumColumns;

            decimal[][] m = PromptMatrixForm.ShowIt( this, m_eventTextDictionary[item.Name],
                rows, cols, this.Minimum, this.Maximum, this.Step );
            if ( m != null )
            {
                float[][] mVal = new float[4][];
                for ( int i = 0; i < 4; ++i )
                {
                    mVal[i] = new float[4];
                    for ( int j = 0; j < 4; ++j )
                    {
                        mVal[i][j] = (float)m[i][j];
                    }
                }

                OnParameterizedOperationEvent( item, new MatrixOperationParameterEventArgs( mVal ) );
            }
        }

        private void PromptMatrixFloat_ToolStripMenuItem_Click( object sender, EventArgs e )
        {
            ToolStripMenuItem item = sender as ToolStripMenuItem;
            string[] split = m_eventTextDictionary[item.Name].Split( new char[] { ',' } );

            decimal[][] m = PromptMatrixForm.ShowIt( this, split[0], this.NumRows, this.NumColumns, this.Minimum, this.Maximum, this.Step );
            if ( m != null )
            {
                float[][] mVal = new float[4][];
                for ( int i = 0; i < 4; ++i )
                {
                    mVal[i] = new float[4];
                    for ( int j = 0; j < 4; ++j )
                    {
                        mVal[i][j] = (float)m[i][j];
                    }
                }

                float f = PromptFloatForm.ShowIt( this, split[1] );
                if ( f != float.MinValue )
                {
                    OnParameterizedOperationEvent( item, new MatrixOperationParameterEventArgs( mVal, f ) );
                }
            }
        }

        private void PromptFloatInt_ToolStripMenuItem_Click( object sender, EventArgs e )
        {
            ToolStripMenuItem item = sender as ToolStripMenuItem;
            string[] split = m_eventTextDictionary[item.Name].Split( new char[] { ',' } );

            float f = PromptFloatForm.ShowIt( this, split[0] );
            if ( f != float.MinValue )
            {
                int i = PromptAxisForm.ShowIt( this, split[1] );
                if ( i != -1 )
                {
                    OnParameterizedOperationEvent( item, new MatrixOperationParameterEventArgs( f, i ) );
                }
            }
        }

        private void OnParameterizedOperationEvent( ToolStripMenuItem item, MatrixOperationParameterEventArgs e )
        {
            ParameterizedOperationEventHandler handler;
            if ( m_eventDictionary.TryGetValue( item.Name, out handler ) && (handler != null) )
            {
                handler( item, e );
            }
        }
        #endregion
    }

    #region Helper Classes
    [Serializable]
    public class MatrixClipboardData : Object
    {
        public MatrixClipboardData( decimal[][] matrix, int numRows, int numColumns )
        {
            m_matrix = matrix;
            m_numRows = numRows;
            m_numColumns = numColumns;
        }

        #region Variables
        private decimal[][] m_matrix;
        private int m_numRows;
        private int m_numColumns;
        #endregion

        #region Properties
        public decimal[][] Matrix
        {
            get
            {
                return m_matrix;
            }
        }

        public int NumRows
        {
            get
            {
                return m_numRows;
            }
        }

        public int NumColumns
        {
            get
            {
                return m_numColumns;
            }
        }
        #endregion
    };
    #endregion

    public class MatrixOperationParameterEventArgs : EventArgs
    {
        public MatrixOperationParameterEventArgs( float f )
        {
            m_float1 = f;
        }

        public MatrixOperationParameterEventArgs( float f1, float f2, float f3 )
        {
            m_float1 = f1;
            m_float2 = f2;
            m_float3 = f3;
        }

        public MatrixOperationParameterEventArgs( float f1, float f2, float f3, float f4 )
        {
            m_float1 = f1;
            m_float2 = f2;
            m_float3 = f3;
            m_float4 = f4;
        }

        public MatrixOperationParameterEventArgs( float[] v )
        {
            m_vector1 = v;
        }

        public MatrixOperationParameterEventArgs( float[] v1, float[] v2 )
        {
            m_vector1 = v1;
            m_vector2 = v2;
        }

        public MatrixOperationParameterEventArgs( float[] v1, float[] v2, float f )
        {
            m_vector1 = v1;
            m_vector2 = v2;
            m_float1 = f;
        }

        public MatrixOperationParameterEventArgs( float[] v1, float[] v2, float[] v3 )
        {
            m_vector1 = v1;
            m_vector2 = v2;
            m_vector3 = v3;
        }

        public MatrixOperationParameterEventArgs( float[][] m )
        {
            m_matrix = m;
        }

        public MatrixOperationParameterEventArgs( float[][] m, float f )
        {
            m_matrix = m;
            m_float1 = f;
        }

        public MatrixOperationParameterEventArgs( float f, float[] v )
        {
            m_float1 = f;
            m_vector1 = v;
        }

        public MatrixOperationParameterEventArgs( float f1, float f2, float[] v )
        {
            m_float1 = f1;
            m_float2 = f2;
            m_vector1 = v;
        }

        public MatrixOperationParameterEventArgs( float f, int i )
        {
            m_float1 = f;
            m_int = i;
        }

        public MatrixOperationParameterEventArgs( string namedOperation )
        {
            m_namedOperation = namedOperation;
        }

        public MatrixOperationParameterEventArgs( string namedOperation, MatrixOperationParameterEventArgs e )
        {
            m_namedOperation = namedOperation;
            m_namedOperationParameters = e;
        }

        #region Variables
        private int m_int = int.MinValue;
        private float m_float1 = float.MinValue;
        private float m_float2 = float.MinValue;
        private float m_float3 = float.MinValue;
        private float m_float4 = float.MinValue;
        private float[] m_vector1 = null;
        private float[] m_vector2 = null;
        private float[] m_vector3 = null;
        private float[][] m_matrix = null;
        private string m_namedOperation = null;
        private MatrixOperationParameterEventArgs m_namedOperationParameters = null;
        #endregion

        #region Properties
        public int IntValue
        {
            get
            {
                return m_int;
            }
        }

        public float FloatValue1
        {
            get
            {
                return m_float1;
            }
        }

        public float FloatValue2
        {
            get
            {
                return m_float2;
            }
        }

        public float FloatValue3
        {
            get
            {
                return m_float3;
            }
        }

        public float FloatValue4
        {
            get
            {
                return m_float4;
            }
        }

        public float[] Vector1
        {
            get
            {
                return m_vector1;
            }
        }

        public float[] Vector2
        {
            get
            {
                return m_vector2;
            }
        }

        public float[] Vector3
        {
            get
            {
                return m_vector3;
            }
        }

        public float[][] Matrix
        {
            get
            {
                return m_matrix;
            }
        }

        public string NamedOperation
        {
            get
            {
                return m_namedOperation;
            }
        }

        public MatrixOperationParameterEventArgs NamedOperationParamters
        {
            get
            {
                return m_namedOperationParameters;
            }
        }
        #endregion
    }
}

