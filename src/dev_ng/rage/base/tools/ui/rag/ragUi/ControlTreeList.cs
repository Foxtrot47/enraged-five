using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;

using GlacialComponents.Controls.GlacialTreeList;

namespace ragUi
{
	/// <summary>
	/// Summary description for ControlTreeList.
	/// </summary>
	public class ControlTreeList : System.Windows.Forms.UserControl
	{
		private System.Windows.Forms.Label label1;
        private GlacialTreeList glacialTreeList1;
        private IContainer components;

		public ControlTreeList() : base()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			// TODO: Add any initialization after the InitializeComponent call
		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.glacialTreeList1 = new GlacialComponents.Controls.GlacialTreeList.GlacialTreeList("6D19053BCC89C84DA86A60EA66B3E62D");
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Location = new System.Drawing.Point( 0, 0 );
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size( 176, 23 );
            this.label1.TabIndex = 2;
            this.label1.Text = "label1";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // glacialTreeList1
            // 
            this.glacialTreeList1.AllowColumnReorder = true;
            this.glacialTreeList1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.glacialTreeList1.BackColor = System.Drawing.SystemColors.Window;
            this.glacialTreeList1.ControlStyle = GlacialComponents.Controls.GlacialTreeList.GTLControlStyles.Normal;
            this.glacialTreeList1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.glacialTreeList1.GridColor = System.Drawing.SystemColors.ActiveBorder;
            this.glacialTreeList1.HotTrackingColor = System.Drawing.SystemColors.HotTrack;
            this.glacialTreeList1.Location = new System.Drawing.Point( 0, 24 );
            this.glacialTreeList1.Name = "glacialTreeList1";
            this.glacialTreeList1.SelectedTextColor = System.Drawing.SystemColors.HighlightText;
            this.glacialTreeList1.SelectionColor = System.Drawing.SystemColors.Highlight;
            this.glacialTreeList1.ShadedColumnMode = GlacialComponents.Controls.GlacialTreeList.GTLShadedColumnModes.None;
            this.glacialTreeList1.ShowRootLines = false;
            this.glacialTreeList1.Size = new System.Drawing.Size( 176, 160 );
            this.glacialTreeList1.TabIndex = 3;
            this.glacialTreeList1.Text = "glacialTreeList1";
            this.glacialTreeList1.UnfocusedSelectionColor = System.Drawing.SystemColors.Control;
            this.glacialTreeList1.UnfocusedSelectionTextColor = System.Drawing.SystemColors.ControlText;
            // 
            // ControlTreeList
            // 
            this.Controls.Add( this.glacialTreeList1 );
            this.Controls.Add( this.label1 );
            this.Name = "ControlTreeList";
            this.Size = new System.Drawing.Size( 176, 184 );
            this.ResumeLayout( false );

		}
		#endregion

		#region Properties
		public GlacialComponents.Controls.GlacialTreeList.GlacialTreeList TheTreeList
		{
			get
			{
				return glacialTreeList1;
			}
		}

		public string Title
		{
			get
			{
				return label1.Text;
			}

			set
			{
				label1.Text = value;
			}
		}

		public Label TheLabel
		{
			get
			{
				return label1;
			}
		}
		#endregion
	}
}
