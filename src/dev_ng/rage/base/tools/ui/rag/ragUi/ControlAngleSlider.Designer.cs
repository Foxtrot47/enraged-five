namespace ragUi
{
    partial class ControlAngleSlider
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && (components != null) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager( typeof( ControlAngleSlider ) );
            this.controlSlider = new ragUi.ControlSlider();
            this.SuspendLayout();
            // 
            // anglePanel
            // 
            this.anglePanel.Location = new System.Drawing.Point( 398, 0 );
            // 
            // controlSlider
            // 
            this.controlSlider.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.controlSlider.ClipboardData = ((object)(resources.GetObject( "controlSlider.ClipboardData" )));
            this.controlSlider.IsFloat = false;
            this.controlSlider.Location = new System.Drawing.Point( 3, 3 );
            this.controlSlider.Maximum = 0;
            this.controlSlider.Minimum = 0;
            this.controlSlider.Name = "controlSlider";
            this.controlSlider.Size = new System.Drawing.Size( 389, 24 );
            this.controlSlider.Step = 1M;
            this.controlSlider.TabIndex = 0;
            this.controlSlider.Text = "controlSlider1";
            this.controlSlider.UseTrackbar = true;
            this.controlSlider.Value = new decimal( new int[] {
            0,
            0,
            0,
            0} );
            this.controlSlider.ValueXPath = null;
            this.controlSlider.ValueChanged += new System.EventHandler( this.controlSlider_ValueChanged );
            // 
            // ControlAngleSlider
            // 
            this.Controls.Add( this.controlSlider );
            this.Name = "ControlAngleSlider";
            this.Size = new System.Drawing.Size( 448, 50 );
            this.Controls.SetChildIndex( this.anglePanel, 0 );
            this.Controls.SetChildIndex( this.controlSlider, 0 );
            this.ResumeLayout( false );

        }

        #endregion

        private ControlSlider controlSlider;
    }
}
