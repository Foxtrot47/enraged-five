using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Data;
using System.Windows.Forms;

using EV.Windows.Forms;
using ListViewEx;

namespace ragUi
{
	/// <summary>
	/// Summary description for ControlList.
	/// </summary>
    public class ControlList : ControlBase
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.Container components = null;

        public ControlList()
        {
            // This call is required by the Windows.Forms Form Designer.
            InitializeComponent();

            // TODO: Add any initialization after the InitializeComponent call
            m_ListView.SubItemClicked += new ListViewEx.ListViewEx.SubItemEventHandler( listViewEx1_SubItemClicked );
            m_ListView.SubItemEndEditing += new ListViewEx.ListViewEx.SubItemEndEditingEventHandler( listViewEx1_SubItemEndEditing );
            m_ListView.DoubleClick += new EventHandler(listView_DoubleClick);
            m_ListView.Click += new EventHandler(listView_SingleClick);
        }

        #region Variables
        private System.Windows.Forms.Label label1;
        private string m_PreviousText;

        protected ListViewSortManager m_SortMgr;
        protected ListViewEx.ListViewEx m_ListView;
        protected System.Windows.Forms.TextBox textBox1;
        protected Dictionary<object, ListViewItem> m_KeyToListItem = new Dictionary<object, ListViewItem>();
        protected Dictionary<ListViewItem, object> m_ListItemToKey = new Dictionary<ListViewItem, object>();
        protected Control[] m_Editors;
        #endregion

        #region Delegates
        public delegate void EditCompleteDelegate( ListViewEx.ListViewEx.SubItemEndEditingEventArgs e, int key );
        public delegate void DoubleClickDelegate(int row);
        public delegate void SingleClickDelegate(int row); 
        #endregion

        #region Events
        public event EditCompleteDelegate EditComplete;
        public event DoubleClickDelegate ControlListDoubleClick;
        public event SingleClickDelegate ControlListSingleClick;
        #endregion

        #region Properties
        [Browsable( true ), Category( "Misc" ), DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
        public override bool IsReadOnly
        {
            get { return !m_ListView.AllowEdit; }
            set { m_ListView.AllowEdit = !value; }
        }

        [Browsable( true ), Category( "Misc" ), DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
        public ListView ListView
        {
            get
            {
                return m_ListView;
            }
        }
        #endregion

        #region Overrides
        [Browsable( true ), Category( "Misc" ), DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
        public override string Text
        {
            get
            {
                return label1.Text.TrimStart();
            }
            set
            {
                label1.Text = value;
                PadControlTextForIcon( label1, m_Icon );
            }
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose( bool disposing )
        {
            if ( disposing )
            {
                if ( components != null )
                {
                    components.Dispose();
                }

                if ( m_SortMgr != null )
                {
                    m_SortMgr.Dispose();
                    m_SortMgr = null;
                }
            }
            base.Dispose( disposing );
        }

        protected override void OnIconChanged()
        {
            PadControlTextForIcon( label1, m_Icon );
            label1.Image = m_Icon;
        }
        #endregion

        #region Event Handlers
        private void SizeChangedEH( object sender, System.EventArgs e )
        {
            m_ListView.Width = this.Width;
            label1.Width = this.Width;

            m_ListView.Height = this.Height - label1.Height;
        }

        private void listViewEx1_SubItemClicked( object sender, ListViewEx.ListViewEx.SubItemEventArgs e )
        {
            m_PreviousText = e.Item.SubItems[e.SubItem].Text;
            m_ListView.StartEditing( m_Editors[e.SubItem], e.Item, e.SubItem );
        }

        private void listViewEx1_SubItemEndEditing( object sender, ListViewEx.ListViewEx.SubItemEndEditingEventArgs e )
        {
            if ( !e.Cancel )
            {
                EditComplete( e, (int)m_ListItemToKey[e.Item] );

                OnValueChanged();

                // reset the text - the remote game will update the value if needed:
                e.DisplayText = m_PreviousText;
            }
        }

        private void listView_DoubleClick(object sender, EventArgs e)
        {
            if (m_ListView.SelectedItems.Count > 0)
            {
                int key = (int)m_ListItemToKey[m_ListView.SelectedItems[0]];
                ControlListDoubleClick(key);
            }
                
        }

        private void listView_SingleClick(object sender, EventArgs e)
        {
            if (m_ListView.SelectedItems.Count > 0)
            {
                int key = (int)m_ListItemToKey[m_ListView.SelectedItems[0]];
                ControlListSingleClick(key);
            }

        }

        #endregion

        #region Public Functions
        public void Rebuild( ColumnData[] columnData, RowData[] rows )
        {
            Rebuild( columnData, rows, 120 );
        }

        public void Rebuild( ColumnData[] columnData, RowData[] rows, int columnWidth )
        {
            m_ListView.BeginUpdate();

            Type[] sortTypes = new Type[columnData.Length];

            m_Editors = new Control[columnData.Length];

            m_ListView.Columns.Clear();

            // setup column headers:
            for ( int i = 0; i < columnData.Length; i++ )
            {
                m_ListView.Columns.Add( columnData[i].Name, columnWidth, HorizontalAlignment.Left );
                switch ( columnData[i].DataType )
                {
                    case ColumnData.Type.FLOAT:
                        sortTypes[i] = typeof( ListViewDoubleSort );
                        break;
                    case ColumnData.Type.INT:
                        sortTypes[i] = typeof( ListViewInt32Sort );
                        break;
                    case ColumnData.Type.STRING:
                        sortTypes[i] = typeof( ListViewTextCaseInsensitiveSort );
                        break;
                }
                m_Editors[i] = textBox1;
            }

            m_KeyToListItem.Clear();
            m_ListItemToKey.Clear();

            m_ListView.Items.Clear();

            foreach ( RowData row in rows )
            {
                m_KeyToListItem.Add( row.Key, row.Item );
                m_ListItemToKey.Add( row.Item, row.Key );
                m_ListView.Items.Add( row.Item );
            }

            int sortColumn = 0;
            SortOrder sortOrder = SortOrder.Ascending;
            if ( m_SortMgr != null )
            {
                sortColumn = m_SortMgr.Column >= 0 ? m_SortMgr.Column : 0;
                sortOrder = m_SortMgr.SortOrder;

                m_SortMgr.Dispose();
                m_SortMgr = null;
            }
            
            if ( columnData.Length > 0 ) // Need at least 1 column to do the sorting
            {
                m_SortMgr = new ListViewSortManager( m_ListView, sortTypes, 0, SortOrder.Ascending );
            }

            m_ListView.EndUpdate();
        }

        public void Add( int key, int column, String val )
        {
            // find the row:
            ListViewItem item = null;
            if ( m_KeyToListItem.ContainsKey( key ) == false )
            {
                item = new ListViewItem( val, 0 );
                item.Tag = val; // this is the previous value which is initialized to val.
                m_KeyToListItem.Add( key, item );
                m_ListView.Items.Add( item );
                m_ListItemToKey.Add( item, key );

                if ( column != 0 )
                {
                    throw (new Exception( "Expected column index to be 0." ));
                }

                return;
            }
            else
            {
                item = m_KeyToListItem[key];
            }

            ListViewItem.ListViewSubItem subItem = null;

            // add the column
            if ( item.SubItems.Count <= column )
            {
                int numToAdd = column - item.SubItems.Count + 1;
                for ( int i = 0; i <= numToAdd; i++ )
                {
                    item.SubItems.Add( " " );
                }

                subItem = new ListViewItem.ListViewSubItem();
                item.SubItems[column] = subItem;
            }
            else
            {
                subItem = item.SubItems[column];
            }

            string prevValue = subItem.Text;

            subItem.Text = val;

            if ( subItem.Tag == null )
            {
                subItem.Tag = subItem.Text; // initialize
            }
            else
            {
                if ( subItem.Text != prevValue )
                {
                    subItem.Tag = prevValue; // remember previous value
                }
            }

            // sort it:
            if ( m_SortMgr.Column == column )
            {
                m_SortMgr.Sort( m_SortMgr.Column );
            }
        }

        public void Remove( int key )
        {
            ListViewItem item;
            if ( m_KeyToListItem.TryGetValue( key, out item ) )
            {
                m_ListView.Items.Remove( item );
                m_KeyToListItem.Remove( key );
                m_ListItemToKey.Remove( item );
            }
        }
        #endregion

        #region Helper Classes
        public class ColumnData
        {
            public enum Type
            {
                STRING,
                INT,
                FLOAT
            };

            public string Name;
            public Type DataType;
        }

        public class RowData
        {
            public object Key;
            public ListViewItem Item;
        }
        #endregion

        #region Component Designer generated code
        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.m_ListView = new ListViewEx.ListViewEx();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Location = new System.Drawing.Point( 0, 0 );
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size( 200, 23 );
            this.label1.TabIndex = 1;
            this.label1.Text = "label1";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // m_ListView
            // 
            this.m_ListView.AllowColumnReorder = true;
            this.m_ListView.DoubleClickActivation = false;
            this.m_ListView.FullRowSelect = true;
            this.m_ListView.Location = new System.Drawing.Point( 0, 24 );
            this.m_ListView.Name = "m_ListView";
            this.m_ListView.Size = new System.Drawing.Size( 200, 160 );
            this.m_ListView.TabIndex = 0;
            this.m_ListView.View = System.Windows.Forms.View.Details;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point( 56, 80 );
            this.textBox1.Name = "textBox1";
            this.textBox1.TabIndex = 2;
            this.textBox1.Text = "textBox1";
            this.textBox1.Visible = false;
            // 
            // ControlList
            // 
            this.Controls.Add( this.textBox1 );
            this.Controls.Add( this.label1 );
            this.Controls.Add( this.m_ListView );
            this.Name = "ControlList";
            this.Size = new System.Drawing.Size( 200, 184 );
            this.SizeChanged += new System.EventHandler( this.SizeChangedEH );
            this.ResumeLayout( false );

        }
        #endregion
    }
}
