using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ragUi
{
    public partial class PromptFloatForm : Form
    {
        public PromptFloatForm()
        {
            InitializeComponent();

            this.valueNumericUpDown.Minimum = decimal.MinValue;
            this.valueNumericUpDown.Maximum = decimal.MaxValue;
        }

        public static float ShowIt( Control parentControl, string title )
        {
            PromptFloatForm form = new PromptFloatForm();
            form.Text = title;

            if ( form.ShowDialog( parentControl ) == DialogResult.OK )
            {
                return (float)form.valueNumericUpDown.Value;
            }

            return float.MinValue;
        }
    }
}