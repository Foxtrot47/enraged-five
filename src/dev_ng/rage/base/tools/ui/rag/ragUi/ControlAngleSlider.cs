using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ragUi
{
    public partial class ControlAngleSlider : ControlAngleBase
    {
        protected ControlAngleSlider()
            : base()
        {
            InitializeComponent();
        }

        public ControlAngleSlider( Mode mode, float min, float max )
            : base()
        {
            InitializeComponent();

            this.controlSlider.UseTrackbar = false;
            this.controlSlider.IsFloat = true;
            this.controlSlider.SetRange( min, max );

            this.DisplayMode = mode;
        }

        public ControlAngleSlider( Mode mode, decimal min, decimal max )
            : base()
        {
            InitializeComponent();

            this.controlSlider.UseTrackbar = false;
            this.controlSlider.IsFloat = true;
            this.controlSlider.Minimum = min;
            this.controlSlider.Maximum = max;

            this.DisplayMode = mode;
        }

        #region Enum
        public enum Mode
        {
            Degrees,
            Fraction,
            Radians,
        }
        #endregion

        #region Variables
        private Mode m_mode;
        private delegate void UpdateInvoke(float value);
        #endregion

        #region Properties
        public decimal Value
        {
            get
            {
                return this.controlSlider.Value;
            }
        }

        public Mode DisplayMode
        {
            get
            {
                return m_mode;
            }
            set
            {
                m_mode = value;

                switch ( m_mode )
                {
                    case Mode.Degrees:
                        this.controlSlider.Step = 1.0M;
                        break;
                    case Mode.Fraction:
                        this.controlSlider.Step = 0.01M;
                        break;
                    case Mode.Radians:
                        this.controlSlider.Step = 0.01M;
                        break;
                }
            }
        }

        public override bool IsReadOnly
        {
            get { return controlSlider.IsReadOnly; }
            set
            {
                anglePanel.Enabled = !value;
                controlSlider.IsReadOnly = value;
            }
        }
        #endregion

        #region Overrides
        public override double Angle
        {
            get
            {
                double angleRadians = 0.0;
                switch ( m_mode )
                {
                    case Mode.Degrees:
                        {
                            angleRadians = Convert.ToDouble( this.controlSlider.Value ) * Math.PI / 180.0;
                        }
                        break;
                    case Mode.Fraction:
                        {
                            angleRadians = Convert.ToDouble( this.controlSlider.Value ) * Math.PI * 2.0;
                        }
                        break;
                    default:
                        {
                            angleRadians = Convert.ToDouble( this.controlSlider.Value );
                        }
                        break;
                }

                // rotate for Up as 0.0f
                angleRadians -= Math.PI / 2.0;

                return angleRadians;
            }
            set
            {
                try
                {
                    switch ( m_mode )
                    {
                        case Mode.Degrees:
                            {
                                decimal angleDegrees = Convert.ToDecimal( 180.0 * value / Math.PI );

                                if ((this.controlSlider.Minimum == 0) && (this.controlSlider.Maximum == 360))
                                {
                                    if (angleDegrees < this.controlSlider.Minimum)
                                    {
                                        angleDegrees += 360M;
                                    }
                                    else if (angleDegrees > this.controlSlider.Maximum)
                                    {
                                        angleDegrees -= 360M;
                                    }
                                }
                                else
                                {
                                    if (angleDegrees < this.controlSlider.Minimum)
                                    {
                                        angleDegrees = this.controlSlider.Minimum;
                                    }
                                    else if (angleDegrees > this.controlSlider.Maximum)
                                    {
                                        if (angleDegrees > 180M)
                                        {
                                            angleDegrees -= 360M;

                                            if (angleDegrees < this.controlSlider.Minimum)
                                                angleDegrees = this.controlSlider.Minimum;
                                        }
                                        else
                                        {
                                            angleDegrees = this.controlSlider.Maximum;
                                        }
                                    }
                                }

                                this.controlSlider.Value = angleDegrees;
                            }
                            break;
                        case Mode.Fraction:
                            {
                                decimal angleFraction = Convert.ToDecimal( value / (Math.PI * 2.0) );
                                if ( angleFraction < this.controlSlider.Minimum )
                                {
                                    angleFraction += 1M;
                                }
                                else if ( angleFraction > this.controlSlider.Maximum )
                                {
                                    angleFraction -= 1M;
                                }

                                this.controlSlider.Value = angleFraction;
                            }
                            break;
                        default:
                            {
                                decimal angleRadians = Convert.ToDecimal( value );
                                if ( angleRadians < this.controlSlider.Minimum )
                                {
                                    angleRadians += Convert.ToDecimal( Math.PI * 2.0 );
                                }
                                else if ( angleRadians > this.controlSlider.Maximum )
                                {
                                    angleRadians -= Convert.ToDecimal( Math.PI * 2.0 );
                                }

                                this.controlSlider.Value = angleRadians;
                            }
                            break;
                    }
                }
                catch
                {

                }
            }
        }

        public override bool CanCopy
        {
            get
            {
                return this.controlSlider.CanCopy;
            }
        }

        public override bool CanPaste
        {
            get
            {
                return this.controlSlider.CanPaste;
            }
        }

        public override bool CanUndo
        {
            get
            {
                return this.controlSlider.CanUndo;
            }
        }

        public override object ClipboardData
        {
            get
            {
				return this.controlSlider.Value.ToString();
            }
            set
            {
				if ((value != null) && (value is string))
				{
					// Ok it is a string.  Are there any numbers in the string?
					string strClipboardString = value as string;
					string[] astrStringParts = strClipboardString.Split();
					foreach (string strPossibleNumber in astrStringParts)
					{
						decimal fResult;
						if (decimal.TryParse(strPossibleNumber, out fResult))
						{
							this.controlSlider.Value = fResult;
						}
					}
				}
            }
        }

        public override bool IsUndoable
        {
            get
            {
                return this.controlSlider.IsUndoable;
            }
        }

        public override string Text
        {
            get
            {
                return this.controlSlider.Text;
            }
            set
            {
                this.controlSlider.Text = value;
            }
        }

        protected override void OnIconChanged()
        {
            PadControlTextForIcon( this.controlSlider, m_Icon );
        }

        public override void Undo()
        {
            this.controlSlider.Undo();
        }
        #endregion

        #region Event Handlers
        private void controlSlider_ValueChanged( object sender, EventArgs e )
        {
            SetAnglePanel( this.Angle, true );
        }
        #endregion

        #region Public Functions
        public void UpdateValueFromRemote( float value )
        {
            if (this.InvokeRequired == true)
            {
                UpdateInvoke updateInvoke = new UpdateInvoke(controlSlider.UpdateValueFromRemote);

                object[] args = new object[1];
                args[0] = value;
                this.Invoke(updateInvoke, args);
            }
            else
            {
                this.controlSlider.UpdateValueFromRemote(value);
            }
            
            SetAnglePanel( this.Angle, false );
        }
        #endregion
    }
}

