using System;

using GlacialComponents.Controls.GTLCommon;

namespace ragUi
{
	/// <summary>
	/// Summary description for EmbeddedNumericUpDown.
	/// </summary>
	public class EmbeddedNumericUpDown : System.Windows.Forms.NumericUpDown, GlacialComponents.Controls.GTLCommon.IGEmbeddedControl
	{
		public EmbeddedNumericUpDown()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		private object m_Owner;
		private object m_UserData;

		public object Owner
		{ 
			get
			{
				return m_Owner;
			}
		}

		public object UserData
		{ 
			get
			{
				return m_UserData;
			}
		}

		public override string Text
		{ 
			get
			{
				return base.Text;
			}
			set
			{
				if ( this.DecimalPlaces == 0 )
				{
					base.Text = value;
                    base.Value = Convert.ToDecimal( value );
				}
				else
				{
					decimal val = Convert.ToDecimal( value );
					val = Decimal.Round( val, this.DecimalPlaces );
					base.Text = val.ToString();
                    base.Value = val;
				}
			}
		}

		public bool Load( string text, object userData, object owner )
		{
			this.Text = text;
			m_UserData = userData;
			m_Owner = owner;

			return true;
		}
		
		public string Unload()
		{
			return this.Text;
		}
	}
}
