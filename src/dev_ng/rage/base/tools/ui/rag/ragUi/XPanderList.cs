using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace ragCore
{
    public partial class XPanderList : UserControl
    {
        public XPanderList()
        {
            InitializeComponent();

            InitializeAdditionalComponents();
        }

        private void InitializeAdditionalComponents()
        {
            this.SuspendLayout();

            //Add any initialization after the InitializeComponent() call
            SetStyle( ControlStyles.DoubleBuffer, true );
            SetStyle( ControlStyles.ResizeRedraw, true );
            SetStyle( ControlStyles.UserPaint, true );
            SetStyle( ControlStyles.AllPaintingInWmPaint, true );

            this.BackColor = m_BgColorDark;

            this.ResumeLayout( false );
        }

        #region Constants
        private const int c_InitialVerticalSpacing = 10;
        private const int c_HorizontalSpacing = 2;
        private const int c_VerticalSpacing = 2;      // Y gap between XPander2 controls

        private static Color c_LightBackColor = Color.FromArgb( 128, 128, 128 );
        private static Color c_DarkBackColor = Color.FromArgb( 128, 128, 128 );
        #endregion

        #region Variables
        private Color m_BgColorLight = c_LightBackColor;
        private Color m_BgColorDark = c_DarkBackColor;
        private int m_NestingIndent = 10;
        private int m_NestingBottomPad = 10;
        private int m_MinimumWidth = 0;
        private bool m_IsUpdating;

        private List<Control> m_originalContrlOrder = new List<Control>();
        #endregion

        #region Properties
        [Description( "Light color used in gradient for background." ),
        DefaultValue( "123,162,239" ),
        Category( "Appearance" )]
        public Color BackColorLight
        {
            get
            {
                return m_BgColorLight;
            }
            set
            {
                if ( m_BgColorLight != value )
                {
                    m_BgColorLight = value;
                    Invalidate();
                }
            }
        }

        [Description( "Dark color used in gradient for background." ),
        DefaultValue( "99,117,222" ),
        Category( "Appearance" )]
        public Color BackColorDark
        {
            get
            {
                return m_BgColorDark;
            }
            set
            {
                if ( m_BgColorDark != value )
                {
                    m_BgColorDark = value;
                    Invalidate();
                }
            }
        }

        [Description( "Indent for nested XPander2" ),
        Category( "Appearance" )]
        public int NestingIndent
        {
            get
            {
                return m_NestingIndent;
            }
            set
            {
                if ( m_NestingIndent != value )
                {
                    m_NestingIndent = value;
                    Invalidate();
                }
            }
        }

        [Description( "Padding at bottom of nested XPander2" ),
        Category( "Appearance" )]
        public int NestingBottomPad
        {
            get
            {
                return m_NestingBottomPad;
            }
            set
            {
                if ( m_NestingBottomPad != value )
                {
                    m_NestingBottomPad = value;
                    Invalidate();
                }
            }
        }

        [Description( "Minimum width for contained controls" ),
        DefaultValue( 350 ),
        Category( "Appearance" )]
        public int MinimumWidth
        {
            get
            {
                return m_MinimumWidth;
            }
            set
            {
                if ( m_MinimumWidth != value )
                {
                    m_MinimumWidth = value;
                    Invalidate();
                }
            }
        }

        [Browsable( false )]
        public bool Updating
        {
            get 
            { 
                return m_IsUpdating; 
            }
        }
        #endregion

        #region Events
        [Description( "Fired when the XPanderList is sorted." ),
        Category( "Behavior" )]
        public event EventHandler Sorted;

        [Description( "Fired when the XPanderList is unsorted." ),
        Category( "Behavior" )]
        public event EventHandler Unsorted;
        #endregion

        #region Event Dispatchers
        private void OnSorted()
        {
            if ( this.Sorted != null )
            {
                this.Sorted( this, EventArgs.Empty );
            }
        }

        private void OnUnsorted()
        {
            if ( this.Unsorted != null )
            {
                this.Unsorted( this, EventArgs.Empty );
            }
        }
        #endregion

        #region Public Functions
        public void Sort()
        {
            this.SuspendLayout();

            this.ControlAdded -= new ControlEventHandler( XPanderList_ControlAdded );
            this.ControlRemoved -= new ControlEventHandler( XPanderList_ControlRemoved );

            Control[] controls = new Control[this.Controls.Count];
            this.Controls.CopyTo( controls, 0 );

            List<Control> controlList = new List<Control>( controls );
            controlList.Sort( CompareControls );

            this.Controls.Clear();
            this.Controls.AddRange( controlList.ToArray() );

            foreach ( Control ctrl in this.Controls )
            {
                if ( ctrl is XPander )
                {
                    (ctrl as XPander).Sort();
                }
                else if ( ctrl is XPanderList )
                {
                    (ctrl as XPanderList).Sort();
                }
            }

            this.ControlAdded += new ControlEventHandler( XPanderList_ControlAdded );
            this.ControlRemoved += new ControlEventHandler( XPanderList_ControlRemoved );

            this.ResumeLayout();

            OnSorted();
        }

        public void Unsort()
        {
            this.SuspendLayout();

            this.ControlAdded -= new ControlEventHandler( XPanderList_ControlAdded );
            this.ControlRemoved -= new ControlEventHandler( XPanderList_ControlRemoved );

            this.Controls.Clear();
            this.Controls.AddRange( m_originalContrlOrder.ToArray() );

            foreach ( Control ctrl in this.Controls )
            {
                if ( ctrl is XPander )
                {
                    (ctrl as XPander).Unsort();
                }
                else if ( ctrl is XPanderList )
                {
                    (ctrl as XPanderList).Unsort();
                }
            }

            this.ControlAdded += new ControlEventHandler( XPanderList_ControlAdded );
            this.ControlRemoved += new ControlEventHandler( XPanderList_ControlRemoved );

            this.ResumeLayout();

            OnUnsorted();
        }

        public void BeginUpdate()
        {
            SuspendLayout();

            foreach ( Control ctrl in this.Controls )
            {
                if ( ctrl is XPander )
                {
                    (ctrl as XPander).BeginUpdate();
                }
                else
                {
                    ctrl.SuspendLayout();
                }
            }

            m_IsUpdating = true;
        }

        public void EndUpdate()
        {
            m_IsUpdating = false;

            foreach ( Control ctrl in this.Controls )
            {
                if ( ctrl is XPander )
                {
                    (ctrl as XPander).EndUpdate();
                }
                else
                {
                    ctrl.ResumeLayout();
                }
            }
            
            ResumeLayout();
        }
        #endregion

        #region Overrides
        protected override void OnLayout( LayoutEventArgs levent )
        {
            base.OnLayout( levent );

            int yPos = this.AutoScrollPosition.Y;
            foreach ( Control ctrl in Controls )
            {
                ctrl.SetBounds( this.AutoScrollPosition.X, yPos, this.ClientSize.Width, ctrl.Height, BoundsSpecified.All );

                yPos += ctrl.Height;
            }
        }

        protected override void OnPaint( System.Windows.Forms.PaintEventArgs e )
        {
            // Draw gradient background
            Rectangle rc = new Rectangle( 0, 0, this.Width, this.Height );
            LinearGradientBrush b = new LinearGradientBrush( rc, m_BgColorLight, m_BgColorDark, LinearGradientMode.Vertical );
            e.Graphics.FillRectangle( b, rc );
        }

        protected override Point ScrollToControl( Control activeControl )
        {
            // this disables the crappy logic that almost arbitrarily sets the scroll position when the height of this control changes
            // or when a child control gets focus.
            return this.AutoScrollPosition;
        }
        #endregion

        #region Event Handlers
        private void XPanderList_ControlAdded( object sender, ControlEventArgs e )
        {
            m_originalContrlOrder.Add( e.Control );
        }

        private void XPanderList_ControlRemoved( object sender, ControlEventArgs e )
        {
            m_originalContrlOrder.Remove( e.Control );
        }
        #endregion

        #region Private Functions
        private int CompareControls( Control a, Control b )
        {
            if ( a == null )
            {
                if ( b == null )
                {
                    // If a is null and b is null, they're equal. 
                    return 0;
                }
                else
                {
                    // If a is null and b is not null, b is greater. 
                    return -1;
                }
            }
            else
            {
                // If a is not null...
                //
                if ( b == null )
                // ...and b is null, a is greater.
                {
                    return 1;
                }
                else
                {
                    // sort them with ordinary string comparison.
                    return string.Compare( a.Text, b.Text );
                }
            }
        }
        #endregion
    }
}
