using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ragUi
{
    public partial class PromptAxisForm : Form
    {
        public PromptAxisForm()
        {
            InitializeComponent();
        }

        public static int ShowIt( Control parentControl, string title )
        {
            PromptAxisForm form = new PromptAxisForm();
            form.Text = title;

            if ( form.ShowDialog( parentControl ) == DialogResult.OK )
            {
                return form.axisComboBox.SelectedIndex;
            }

            return -1;
        }
    }
}