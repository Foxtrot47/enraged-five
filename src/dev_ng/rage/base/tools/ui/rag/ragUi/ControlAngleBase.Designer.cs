namespace ragUi
{
    partial class ControlAngleBase
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && (components != null) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.anglePanel = new System.Windows.Forms.Panel();
            this.SuspendLayout();
            // 
            // anglePanel
            // 
            this.anglePanel.Dock = System.Windows.Forms.DockStyle.Right;
            this.anglePanel.Location = new System.Drawing.Point( 387, 0 );
            this.anglePanel.Name = "anglePanel";
            this.anglePanel.Size = new System.Drawing.Size( 50, 50 );
            this.anglePanel.TabIndex = 0;
            this.anglePanel.MouseLeave += new System.EventHandler( this.anglePanel_MouseLeave );
            this.anglePanel.MouseDown += new System.Windows.Forms.MouseEventHandler( this.anglePanel_MouseDown );
            this.anglePanel.MouseMove += new System.Windows.Forms.MouseEventHandler( this.anglePanel_MouseMove );
            this.anglePanel.Paint += new System.Windows.Forms.PaintEventHandler( this.anglePanel_Paint );
            this.anglePanel.MouseUp += new System.Windows.Forms.MouseEventHandler( this.anglePanel_MouseUp );
            // 
            // ControlAngleBase
            // 
            this.Controls.Add( this.anglePanel );
            this.Name = "ControlAngleBase";
            this.Size = new System.Drawing.Size( 437, 50 );
            this.ResumeLayout( false );

        }

        #endregion

        protected System.Windows.Forms.Panel anglePanel;

    }
}
