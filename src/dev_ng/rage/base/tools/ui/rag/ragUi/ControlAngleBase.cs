using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ragUi
{
    public abstract partial class ControlAngleBase : ragUi.ControlBase
    {
        protected ControlAngleBase()
        {
            InitializeComponent();
        }

        #region Variables
        protected Point m_anglePoint;
        protected bool m_mouseCapture = false;
        #endregion

        #region Virtual Functions
        public virtual double Angle
        {
            get
            {
                return 0.0f;
            }
            set
            {
                
            }
        }
        #endregion

        #region Event Handlers
        private void anglePanel_MouseDown( object sender, MouseEventArgs e )
        {
            if ( e.Button == MouseButtons.Left )
            {
                Cursor.Position = this.anglePanel.PointToScreen( m_anglePoint );
                m_mouseCapture = true;
            }
        }

        private void anglePanel_MouseUp( object sender, MouseEventArgs e )
        {
            if ( e.Button == MouseButtons.Left )
            {
                m_mouseCapture = false;
            }
        }

        private void anglePanel_MouseMove( object sender, MouseEventArgs e )
        {
            try
            {
                if ( m_mouseCapture )
                {
                    PointF v1 = new PointF( 0, -1 );

                    PointF v2 = this.anglePanel.PointToClient( Cursor.Position );
                    v2 = new PointF( v2.X - (this.anglePanel.Width / 2), v2.Y - (this.anglePanel.Height / 2) );
                    NormalizeVector( ref v2 );

                    if ( float.IsNaN( v2.X ) || float.IsNaN( v2.Y ) )
                    {
                        return;
                    }

                    double angleRadians = Math.Atan2( v2.Y, v2.X ) - Math.Atan2( v1.Y, v1.X );
                    if ( angleRadians < 0.0 )
                    {
                        angleRadians += Math.PI * 2.0;
                    }

                    this.Angle = angleRadians;
                }
            }
            catch
            {

            }
        }

        private void anglePanel_MouseLeave( object sender, EventArgs e )
        {
            m_mouseCapture = false;
        }

        private void anglePanel_Paint( object sender, PaintEventArgs e )
        {
            Point center = new Point( this.anglePanel.Width / 2, this.anglePanel.Height / 2 );

            // draw a background dial
            e.Graphics.FillEllipse( Brushes.White, 0, 0, this.anglePanel.Width, this.anglePanel.Height );

            // draw a border around the dial
            Pen p = new Pen( Color.Black );
            e.Graphics.DrawEllipse( p, 0, 0, this.anglePanel.Width, this.anglePanel.Height );            

            // draw a 0 line from the center to the top
            e.Graphics.DrawLine( p, center, new Point( center.X, 0 ) );

            if ( m_anglePoint != null )
            {
                // draw the angle line
                p = new Pen( Color.Red );
                e.Graphics.DrawLine( p, center, m_anglePoint );
            }
        }
        #endregion

        #region Protected Functions
        protected void SetAnglePanel( double angleRadians, bool triggerEvent )
        {
            Point center = new Point( this.anglePanel.Width / 2, this.anglePanel.Height / 2 );
            m_anglePoint = new Point( Convert.ToInt32( center.X + (center.X * Math.Cos( angleRadians )) ),
                Convert.ToInt32( center.Y + (center.Y * Math.Sin( angleRadians )) ) );

            if ( triggerEvent )
            {
                OnValueChanged();
            }

            this.anglePanel.Invalidate();
        }

        protected void NormalizeVector( ref PointF v )
        {
            float mag = (float)Math.Sqrt( (v.X * v.X) + (v.Y * v.Y) );
            v.X /= mag;
            v.Y /= mag;
        }
        #endregion

        #region Helper Classes
        [Serializable]
        protected class AngleClipboardData : Object
        {
            public AngleClipboardData( double angle )
            {
                m_angle = angle;
            }

            #region Variables
            private double m_angle;
            #endregion

            #region Properties
            public double Angle
            {
                get
                {
                    return m_angle;
                }
            }
            #endregion
        };
        #endregion
    }
}

