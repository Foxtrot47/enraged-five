using System;
using System.Drawing;
using System.Windows.Forms;
using System.Windows.Forms.Design;
using System.ComponentModel;

namespace ragUi
{
	// Checklist for ControlBase subclasses:
	//
	// 1. Call OnValueChanged whenever the value changes
	// 2. Define Value property if it exists
	// 3. In the value property (or somewhere else), if m_XmlNode exists, set the node value
	// 4. Define OnIconChanged(). Generally it calls PadControlTextForIcon and sets someControl.Image
	// 5. Define Text property. Get, set methods should be away of any potential padding because of icons

	/// <summary>
	/// Summary description for ControlBase.
	/// </summary>
	public abstract class ControlBase : System.Windows.Forms.UserControl
	{
		public ControlBase() 
        {
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			m_XPath = null;
        }

        #region Variables
        protected string m_XPath;
        protected System.Drawing.Bitmap m_Icon;
		private IContainer components;
        #endregion

        #region Properties
        public string ValueXPath
        {
            get { return m_XPath; }
            set { m_XPath = value; }
        }
        
        [DefaultValue( null )]
        public System.Drawing.Bitmap Icon
        {
            get { return m_Icon; }
            set
            {
                m_Icon = value;
                OnIconChanged();
            }
        }

        /// <summary>
        /// Returns true if we can copy data from this control at any time.
        /// </summary>
        public virtual bool CanCopy
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Returns true if we can paste data to this control at certain times.
        /// </summary>
        public virtual bool CanPaste
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Returns true if we can undo the last edit to this control at certain times.
        /// </summary>
        public virtual bool CanUndo
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Used by the default <see cref="IsClipboardDataAvailable"/>, <see cref="CopyToClipboard"/> and <see cref="PasteFromClipboard"/> to
        /// copy and paste the value of this Control to and from the <see cref="Clipboard"/>.
        /// </summary>
        [Browsable( false )]
        public virtual Object ClipboardData
        {
            get
            {
                return null;
            }
            set
            {

            }
        }

        /// <summary>
        /// Returns true if <see cref="CanPaste"/> returns true and there is data that matches the type of <see cref="ClipboardData"/> on
        /// the <see cref="Clipboard"/>.
        /// </summary>
        public virtual bool IsClipboardDataAvailable
        {
            get
            {
                if ( this.CanPaste )
                {
                    Object obj = this.ClipboardData;
                    if ( obj != null )
                    {
                        IDataObject iData = Clipboard.GetDataObject();
                        if ( (iData != null) && iData.GetDataPresent( obj.GetType() ))
                        {
                            return true;
                        }
                    }
                }

                return false;
            }
        }

        /// <summary>
        /// Returns true if this.CanUndo and there is data to roll back to.
        /// </summary>
        public virtual bool IsUndoable
        {
            get
            {
                return this.CanUndo;
            }
        }

        public abstract bool IsReadOnly { get; set; }
        #endregion

        #region Events
        public event EventHandler ValueChanged;
        #endregion

        #region Virtual Functions
        protected virtual void OnIconChanged() 
		{
			throw(new NotImplementedException());
		}

		public virtual void ConnectControlToXmlNode(System.Xml.XmlNode root) 
		{
			DataBindings.Add("Value", FindXPathNode(root, ValueXPath + "/@value"), "Value");
        }

        public virtual void AddMouseUp( MouseEventHandler eh )
        {
            this.MouseUp += eh;
            foreach ( Control control in this.Controls )
            {
                if ( control is ControlBase )
                {
                    (control as ControlBase).AddMouseUp( eh );
                }
                else
                {
                    control.MouseUp += eh;
                }
            }
        }

        public virtual void RemoveMouseUp( MouseEventHandler eh )
        {
            this.MouseUp -= eh;
            foreach ( Control control in this.Controls )
            {
                if ( control is ControlBase )
                {
                    (control as ControlBase).RemoveMouseUp( eh );
                }
                else
                {
                    control.MouseUp -= eh;
                }
            }
        }

        public virtual void AddMouseDown( MouseEventHandler eh )
        {
            this.MouseDown += eh;
            foreach ( Control control in this.Controls )
            {
                if ( control is ControlBase )
                {
                    (control as ControlBase).AddMouseDown( eh );
                }
                else
                {
                    control.MouseDown += eh;
                }
            }
        }

        public virtual void RemoveMouseDown( MouseEventHandler eh )
        {
            this.MouseDown -= eh;
            foreach ( Control control in this.Controls )
            {
                if ( control is ControlBase )
                {
                    (control as ControlBase).RemoveMouseDown( eh );
                }
                else
                {
                    control.MouseDown -= eh;
                }
            }
        }

        public virtual void AddMouseEnter( EventHandler eh )
        {
            this.MouseEnter += eh;
            foreach ( Control control in this.Controls )
            {
                if ( control is ControlBase )
                {
                    (control as ControlBase).AddMouseEnter( eh );
                }
                else
                {
                    control.MouseEnter += eh;
                }
            }
        }

        public virtual void RemoveMouseEnter( EventHandler eh )
        {
            this.MouseEnter -= eh;
            foreach ( Control control in this.Controls )
            {
                if ( control is ControlBase )
                {
                    (control as ControlBase).RemoveMouseEnter( eh );
                }
                else
                {
                    control.MouseEnter -= eh;
                }
            }
        }

        protected virtual void OnValueChanged()
        {
            if ( ValueChanged != null )
            {
                System.EventArgs e = new System.EventArgs();
                ValueChanged( this, e );
            }
        }

        public virtual void CopyToClipboard()
        {
            Object data = this.ClipboardData;
            if ( data != null )
            {
                Clipboard.SetDataObject( data );
            }
        }

        public virtual void PasteFromClipboard()
        {
            Object data = this.ClipboardData;
            if ( data != null )
            {
                Type type = data.GetType();

                IDataObject iData = Clipboard.GetDataObject();
                if ( (iData != null) && iData.GetDataPresent( type ) )
                {
                    data = iData.GetData( type );
                    this.ClipboardData = data;
                }
            }
        }

        public virtual void Undo()
        {

        }
        #endregion

        #region Public Functions
        public object FindXPathValue( System.Xml.XmlNode root, string path )
        {
            return root.CreateNavigator().Evaluate( path );
        }

		public void SetToolTip(ToolTip tooltip, string strTitle, string strMemo)
		{
			// Add tool tips to controls
			if (!String.IsNullOrEmpty(strMemo))
            {
                String tooltipValue = String.Format("{0}\r\n{1}", strTitle, strMemo);
			    foreach (Control control in this.Controls)
			    {
				    tooltip.SetToolTip(control, tooltipValue);
			    }
            }
		}

		#endregion

        #region Static Functions
        public static System.Xml.XmlNode FindXPathNode(System.Xml.XmlNode root, string path) 
		{
			System.Xml.XPath.XPathNavigator nav = root.CreateNavigator();
			System.Xml.XPath.XPathNodeIterator iter = nav.Select(path);			
		
			if (iter.MoveNext()) 
			{ // get the first match
				return ((System.Xml.IHasXmlNode)iter.Current).GetNode();
			}
			else {
				return null;
			}
        }

        static protected void PadControlTextForIcon( Control control, Bitmap icon )
        {
            if ( icon != null )
            {
                Graphics newGraphics = Graphics.FromHwnd( control.Handle );
                Size size = newGraphics.MeasureString( " ", control.Font ).ToSize();

                String newText = "";
                int i;
                int iconWidth = icon.Width;
                for ( i = 0; i < iconWidth; )
                {
                    newText += " ";
                    i += size.Width;
                }
                if ( i != iconWidth )
                {
                    newText += " ";
                }

                control.Text = newText + control.Text.TrimStart();

                newGraphics.Dispose();
            }
            else
            {
                control.Text = control.Text.TrimStart();
            }
        }
        #endregion



		private void InitializeComponent()
        {
			this.components = new System.ComponentModel.Container();
			this.SuspendLayout();
			// 
			// ControlBase
			// 
			this.Name = "ControlBase";
			this.ResumeLayout(false);
        }
	}
}
