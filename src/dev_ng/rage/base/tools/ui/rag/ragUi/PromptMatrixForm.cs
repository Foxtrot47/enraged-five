using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ragUi
{
    public partial class PromptMatrixForm : Form
    {
        public PromptMatrixForm()
        {
            InitializeComponent();

            this.valueControlMatrix.Text = "Matrix";
        }

        public static decimal[][] ShowIt( Control parentControl, string title, int numRows, int numColumns, decimal min, decimal max, decimal step )
        {
            PromptMatrixForm form = new PromptMatrixForm();
            form.Text = title;

            form.valueControlMatrix.NumRows = numRows;
            form.valueControlMatrix.NumColumns = numColumns;
            form.valueControlMatrix.Minimum = min;
            form.valueControlMatrix.Maximum = max;
            form.valueControlMatrix.Step = step;

            if ( form.ShowDialog( parentControl ) == DialogResult.OK )
            {
                return form.valueControlMatrix.Matrix;
            }

            return null;
        }
    }
}