using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

using PlaybackControls;

namespace ragUi
{
    public partial class ControlVCR : ControlBase
    {
        public ControlVCR()
        {
            InitializeComponent();
        }

        #region Overrides
        public override string Text
        {
            get
            {
                return this.label.Text;
            }
            set
            {
                this.label.Text = value;
            }
        }

        public override bool IsReadOnly
        {
            get { return !vcrControl.Enabled; }
            set { vcrControl.Enabled = !value; }
        }

        protected override void OnIconChanged()
        {
            // do nothing
        }
        #endregion

        #region Properties
        public VCRControl VCR
        {
            get
            {
                return this.vcrControl;
            }
        }
        #endregion
    }
}