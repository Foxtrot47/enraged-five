using System;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Data;
using System.IO;
using System.Windows.Forms;

namespace ragUi
{
	/// <summary>
	/// This is the control that the user actually sees.  It is separated from the widget visible
	/// so that the we can reuse the control and so that we can use the Developer Studio designer to 
	/// create the control.
	/// </summary>
	public class ControlImageViewer : ragUi.ControlBase
    {
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public ControlImageViewer()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();
        }

        #region Variables
        private string m_imageFile;
        private string m_previousImageFile;
        private SplitContainer splitContainer1;
        private Label label1;
        private LinkLabel linkLabel1;
        private PictureBox pictureBox1;
        private bool m_firstTime = true;
        private FileStream m_imageFileStream = null;
        #endregion

        #region Properties
        /// <summary>
        /// A property for setting the current web page.
        /// </summary>
        public string CurrentImageFile
        {
            get
            {
                return m_imageFile;
            }
            set
            {
                SetImageFile( value, true );
            }
        }
        #endregion

        #region Overrides
        public override bool CanCopy
        {
            get
            {
                return true;
            }
        }

        public override bool CanPaste
        {
            get
            {
                return true;
            }
        }

        public override bool CanUndo
        {
            get
            {
                return true;
            }
        }

        public override object ClipboardData
        {
            get
            {
                return new ImageViewerClipboardData( this.CurrentImageFile );
            }
            set
            {
                if ( (value != null) && (value is ImageViewerClipboardData) )
                {
                    ImageViewerClipboardData data = value as ImageViewerClipboardData;
                    this.CurrentImageFile = data.ImageFile;
                }
            }
        }

        public override bool IsUndoable
        {
            get
            {
                return base.IsUndoable && (m_previousImageFile != null) && (this.CurrentImageFile != m_previousImageFile);
            }
        }


        public override bool IsReadOnly
        {
            get { return false; }
            set { }
        }

        public override void AddMouseUp( MouseEventHandler eh )
        {
            base.AddMouseUp( eh );

            this.label1.MouseUp += eh;
            this.pictureBox1.MouseUp += eh;
            this.splitContainer1.Panel1.MouseUp += eh;
        }

        public override void RemoveMouseUp( MouseEventHandler eh )
        {
            base.RemoveMouseUp( eh );

            this.label1.MouseUp -= eh;
            this.pictureBox1.MouseUp -= eh;
            this.splitContainer1.Panel1.MouseUp -= eh;
        }

        public override void AddMouseDown( MouseEventHandler eh )
        {
            base.AddMouseDown( eh );

            this.label1.MouseUp += eh;
            this.pictureBox1.MouseUp += eh;
            this.splitContainer1.Panel1.MouseUp += eh;
        }

        public override void RemoveMouseDown( MouseEventHandler eh )
        {
            base.RemoveMouseDown( eh );

            this.label1.MouseDown -= eh;
            this.pictureBox1.MouseDown -= eh;
            this.splitContainer1.Panel1.MouseDown -= eh;
        }

        public override void AddMouseEnter( EventHandler eh )
        {
            base.AddMouseEnter( eh );

            this.label1.MouseEnter += eh;
            this.pictureBox1.MouseEnter += eh;
            this.splitContainer1.Panel1.MouseEnter += eh;
        }

        public override void RemoveMouseEnter( EventHandler eh )
        {
            base.RemoveMouseEnter( eh );

            this.label1.MouseEnter -= eh;
            this.pictureBox1.MouseEnter -= eh;
            this.splitContainer1.Panel1.MouseEnter -= eh;
        }

        /// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
                if ( components != null )
                {
                    components.Dispose();
                }

                if ( m_imageFileStream != null )
                {
                    m_imageFileStream.Close();
                    m_imageFileStream.Dispose();
                }
			}
			base.Dispose( disposing );
        }

        protected override void OnIconChanged()
        {            
            PadControlTextForIcon( this.linkLabel1, m_Icon );
            this.linkLabel1.Image = m_Icon;
        }

        public override void Undo()
        {
            string prevFile = m_previousImageFile;

            m_previousImageFile = this.CurrentImageFile;

            SetImageFile( prevFile, true );
        }

        public override string Text
        {
            get
            {
                return base.Text;
            }
            set
            {
                base.Text = value;
                this.label1.Text = value;
            }
        }
        #endregion

        #region Event Handlers
        private void linkLabel1_LinkClicked( object sender, System.Windows.Forms.LinkLabelLinkClickedEventArgs e )
        {
            try
            {
                Process.Start( this.linkLabel1.Text );
            }
            catch
            {
            }
        }
        #endregion

        #region Public Functions
        public void UpdateValueFromRemote( string imageFile )
        {
            string prevFile = m_imageFile;

            SetImageFile( imageFile, false );

            if ( m_firstTime )
            {
                // since this is the first time, previous value will be the initial value.
                m_previousImageFile = m_imageFile;
                m_firstTime = false;
            }
            else
            {
                if ( m_imageFile != prevFile )
                {
                    m_previousImageFile = prevFile;
                }
            }
        }
        #endregion

        #region Private Functions
        private void SetImageFile( string imageFile, bool triggerEvent )
        {
            // always reload
            try
            {
                if ( imageFile != null )
                {
                    if ( m_imageFileStream != null )
                    {
                        m_imageFileStream.Close();
                    }

                    m_imageFileStream = new FileStream( imageFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite );

                    this.pictureBox1.Image = Image.FromStream( m_imageFileStream );
                }
            }
            catch
            {
            }

            if ( imageFile == this.CurrentImageFile )
            {
                return;
            }

            m_imageFile = imageFile;

            this.linkLabel1.Text = imageFile;

            if ( triggerEvent )
            {
                OnValueChanged();
            }
        }
        #endregion

        #region Component Designer generated code
        /// <summary>
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point( 0, 0 );
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add( this.linkLabel1 );
            this.splitContainer1.Panel1.Controls.Add( this.label1 );
            this.splitContainer1.Panel1MinSize = 45;
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add( this.pictureBox1 );
            this.splitContainer1.Size = new System.Drawing.Size( 350, 300 );
            this.splitContainer1.SplitterDistance = 45;
            this.splitContainer1.TabIndex = 0;
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Location = new System.Drawing.Point( 3, 25 );
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size( 0, 13 );
            this.linkLabel1.TabIndex = 1;
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler( this.linkLabel1_LinkClicked );
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point( 3, 6 );
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size( 35, 13 );
            this.label1.TabIndex = 0;
            this.label1.Text = "label1";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox1.Location = new System.Drawing.Point( 0, 0 );
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size( 350, 251 );
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // ControlImageViewer
            // 
            this.Controls.Add( this.splitContainer1 );
            this.Name = "ControlImageViewer";
            this.Size = new System.Drawing.Size( 350, 300 );
            this.splitContainer1.Panel1.ResumeLayout( false );
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout( false );
            this.splitContainer1.ResumeLayout( false );
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout( false );

		}
		#endregion
	}

    #region Helper Classes
    public class ImageViewerClipboardData : Object
    {
        public ImageViewerClipboardData( string imageFile )
        {
            m_imageFile = imageFile;
        }

        #region Variables
        private string m_imageFile;
        #endregion

        #region Properties
        public string ImageFile
        {
            get
            {
                return m_imageFile;
            }
        }
        #endregion
    };
    #endregion
}
