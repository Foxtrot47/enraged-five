using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ragUi
{
    public partial class ControlAngleVector2 : ragUi.ControlAngleBase
    {
        protected ControlAngleVector2()
        {
            InitializeComponent();
        }

        public ControlAngleVector2( float min, float max )
        {
            InitializeComponent();

            m_minValue = min;
            m_maxValue = max;
            this.controlVector.Step = 0.01M;

            // HACK:  get this on top because the ControlVector sticks out over top of it
            this.Controls.SetChildIndex( this.anglePanel, 0 );
        }

        #region Variables
        private float m_minValue;
        private float m_maxValue;
        private delegate void UpdateInvoke(float[] value);
        #endregion

        #region Properties
        public decimal[] Value
        {
            get
            {
                decimal[] vec = this.controlVector.Vector;
                return new decimal[] { vec[0], vec[1] };
            }
        }

        public override bool IsReadOnly
        {
            get { return controlVector.IsReadOnly; }
            set
            {
                anglePanel.Enabled = !value;
                controlVector.IsReadOnly = value;
            }
        }
        #endregion

        #region Overrides
        public override double Angle
        {
            get
            {
                return Vector2ToRadians( this.controlVector.X, this.controlVector.Y );
            }
            set
            {
                double angleRadians = value;
                if ( angleRadians < m_minValue )
                {
                    angleRadians += Math.PI * 2.0;
                }
                else if ( angleRadians > m_maxValue )
                {
                    angleRadians -= Math.PI * 2.0;
                }

                this.controlVector.Vector = RadiansToVector2( angleRadians );
            }
        }

        public override bool CanCopy
        {
            get
            {
                return this.controlVector.CanCopy;
            }
        }

        public override bool CanPaste
        {
            get
            {
                return this.controlVector.CanPaste;
            }
        }

        public override bool CanUndo
        {
            get
            {
                return this.controlVector.CanUndo;
            }
        }

        public override object ClipboardData
        {
            get
            {
				return this.controlVector.ClipboardData;
            }
            set
            {
				if ((value != null) && (value is string))
				{
					this.controlVector.ClipboardData = value;
				}
            }
        }

        public override bool IsUndoable
        {
            get
            {
                return this.controlVector.IsUndoable;
            }
        }

        public override string Text
        {
            get
            {
                return this.controlVector.Text;
            }
            set
            {
                this.controlVector.Text = value;
            }
        }

        protected override void OnIconChanged()
        {
            PadControlTextForIcon( this.controlVector, m_Icon );
        }

        public override void Undo()
        {
            this.controlVector.Undo();
        }
        #endregion

        #region Event Handlers
        private void controlVector_ValueChanged( object sender, EventArgs e )
        {
            SetAnglePanel( this.Angle, true );
        }
        #endregion

        #region Public Functions
        public void UpdateValueFromRemote( float[] value )
        {
            if (this.InvokeRequired == true)
            {
                UpdateInvoke updateInvoke = new UpdateInvoke(controlVector.UpdateValueFromRemote);
                object[] args = new object[1];
                args[0] = value;
                this.Invoke(updateInvoke, args);
            }
            else
            {
                this.controlVector.UpdateValueFromRemote(value);
            }

            SetAnglePanel( this.Angle, false );
        }
        #endregion

        #region Private Functions
        private double Vector2ToRadians( decimal x, decimal y )
        {
            PointF v1 = new PointF( 0, -1 );

            float fx;
            try
            {
                fx = (float)Convert.ToDouble( x );
            }
            catch
            {
                if ( x < 0 )
                {
                    fx = (float)double.MinValue;
                }
                else
                {
                    fx = (float)double.MaxValue;
                }
            }

            float fy;
            try
            {
                fy = (float)Convert.ToDouble( y );
            }
            catch
            {
                if ( y < 0 )
                {
                    fy = (float)double.MinValue;
                }
                else
                {
                    fy = (float)double.MaxValue;
                }
            }


            PointF v2 = new PointF( fx, fy );
            NormalizeVector( ref v2 );

            double angleRadians = Math.Atan2( v2.Y, v2.X ) - Math.Atan2( v1.Y, v1.X );
            if ( angleRadians < 0.0 )
            {
                angleRadians += Math.PI * 2.0;
            }

            angleRadians = -angleRadians;
            angleRadians += Math.PI / 2.0;

            return angleRadians;
        }

        private decimal[] RadiansToVector2( double angleRadians )
        {
            return new decimal[] { Convert.ToDecimal( Math.Sin( angleRadians ) ), Convert.ToDecimal( Math.Cos( angleRadians ) ), 
                        0M, 0M };
        }
        #endregion
    }
}

