using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Microsoft.Win32;

namespace ragUi
{
    public partial class ControlVector : ragUi.ControlBase, ISupportInitialize
    {
        public ControlVector()
            : base()
        {
            InitializeComponent();
            InitializeAdditionalComponents( null );
        }

        public ControlVector( System.Xml.XmlNode schema )
            : base()
        {
            InitializeComponent();
            InitializeAdditionalComponents( schema );
        }

        #region Delegates
        public delegate void ParameterizedOperationEventHandler( object sender, VectorOperationParameterEventArgs e );
        #endregion

        #region Variables
        private int m_numComponents = 4;
        private int m_lastX = 0;
        private Dictionary<string, ParameterizedOperationEventHandler> m_eventDictionary = new Dictionary<string, ParameterizedOperationEventHandler>();
        private Dictionary<string, string> m_eventTextDictionary = new Dictionary<string, string>();

        private decimal[] m_value = new decimal[4];
        private decimal[] m_prevValue = new decimal[4];
        private bool m_firstTime = true;
        private delegate void ControlVectorInvoke(decimal[] values);

        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the Text displayed for the control, typically it's name.
        /// </summary>
        [Browsable( true ), 
        Category( "Misc" ), 
        Description( "Gets or sets the Text displayed for the control, typically it's name." )]
        public override string Text
        {
            get
            {
                return this.toolStripDropDownButton1.Text;
            }
            set
            {
                this.toolStripDropDownButton1.Text = value;
            }
        }

        public override bool IsReadOnly
        {
            get { return XnumericUpDown.ReadOnly; }
            set
            {
                this.XnumericUpDown.ReadOnly = value;
                this.YnumericUpDown.ReadOnly = value;
                this.ZnumericUpDown.ReadOnly = value;
                this.WnumericUpDown.ReadOnly = value;

                this.XcheckBox.Enabled = !value;
                this.YcheckBox.Enabled = !value;
                this.ZcheckBox.Enabled = !value;
                this.WcheckBox.Enabled = !value;

                this.basicToolStripMenuItem.Enabled = !value;
                this.trigToolStripMenuItem.Enabled = !value;
                this.advancedToolStripMenuItem.Enabled = !value;
            }
        }

        /// <summary>
        /// Gets or sets whether the drop down options are available.
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        Description( "Gets or sets whether the drop down options are available." )]
        public bool AllowDropDownOptions
        {
            get
            {
                return this.toolStripDropDownButton1.DropDownItems.Count > 0;
            }
            set
            {
                if ( value )
                {
                    if ( !this.AllowDropDownOptions )
                    {
                        this.toolStripDropDownButton1.DropDownItems.Add( this.basicToolStripMenuItem );
                        this.toolStripDropDownButton1.DropDownItems.Add( this.trigToolStripMenuItem );
                        this.toolStripDropDownButton1.DropDownItems.Add( this.advancedToolStripMenuItem );
                    }
                }
                else
                {
                    this.toolStripDropDownButton1.DropDownItems.Clear();
                }
            }
        }

        /// <summary>
        /// Gets or sets the entire vector.  Assumed to be length of 4.
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        Description( "Gets or sets the entire vector.  Assumed to be length of 4." ),
        DefaultValue( null )]
        public decimal[] Vector
        {
            get
            {
                decimal[] v = new decimal[4];
                m_value.CopyTo( v, 0 );

                return v;
            }
            set
            {
                if ( value != null )
                {
                    SetValueAndClamp( value, true );
                }
                else
                {
                    SetValueAndClamp( new decimal[] { 0, 0, 0, 0 }, true );
                }
            }
        }

        /// <summary>
        /// Gets or sets the X component of the vector.
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        Description( "Gets or sets the X component of the vector." )]
        public decimal X
        {
            get
            {
                return m_value[0];
            }
            set
            {
                SetValueAndClamp( new decimal[] { value, m_value[1], m_value[2], m_value[3] }, true );
            }
        }

        /// <summary>
        /// Gets or sets the Y component of the vector.
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        Description( "Gets or sets the Y component of the vector" )]
        public decimal Y
        {
            get
            {
                return m_value[1];
            }
            set
            {
                SetValueAndClamp( new decimal[] { m_value[0], value, m_value[2], m_value[3] }, true );
            }
        }

        /// <summary>
        /// Gets or sets the Z component of the vector.
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        Description( "Gets or sets the Z component of the vector." )]
        public decimal Z
        {
            get
            {
                return m_value[2];
            }
            set
            {
                SetValueAndClamp( new decimal[] { m_value[0], m_value[1], value, m_value[3] }, true );
            }
        }

        /// <summary>
        /// Gets or sets the W component of the vector.
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        Description( "Gets or sets the W component of the vector." )]
        public decimal W
        {
            get
            {
                return m_value[3];
            }
            set
            {
                SetValueAndClamp( new decimal[] { m_value[0], m_value[1], m_value[2], value }, true );
            }
        }

        /// <summary>
        /// Gets or sets the number of components in the vector (typically 3 or 4).
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        Description( "Gets or sets the number of components in the vector (typically 3 or 4)." )]
        public int NumComponents
        {
            get
            {
                return m_numComponents;
            }
            set
            {
                m_numComponents = value;

                this.ZnumericUpDown.Visible = m_numComponents >= 3;
                this.ZcheckBox.Visible = m_numComponents >= 3;
                this.WnumericUpDown.Visible = m_numComponents >= 4;
                this.WcheckBox.Visible = m_numComponents >= 4;

                bool twoOnly = m_numComponents == 2;
                bool threeOnly = m_numComponents == 3;
                bool fourOnly = m_numComponents == 4;
                bool twoOrThree = twoOnly || threeOnly;
                bool threeOrFour = threeOnly || fourOnly;

                this.normalizeSafeToolStripMenuItem.Enabled = twoOrThree;
                this.rotateToolStripMenuItem.Enabled = twoOnly;
                this.rotateYToolStripMenuItem.Enabled = twoOrThree;
                this.absToolStripMenuItem.Enabled = threeOrFour;
                this.invertToolStripMenuItem.Enabled = threeOrFour;
                this.invertSafeToolStripMenuItem.Enabled = threeOrFour;
                this.normalizeSafeVToolStripMenuItem.Enabled = threeOnly;
                this.normalizeFastToolStripMenuItem.Enabled = threeOnly;
                this.crossToolStripMenuItem.Enabled = threeOrFour;
                this.dotVToolStripMenuItem.Enabled = threeOrFour;
                this.averageToolStripMenuItem.Enabled = threeOrFour;
                this.lerpToolStripMenuItem.Enabled = threeOrFour;
                this.rotateAboutAxisToolStripMenuItem.Enabled = threeOnly;
                this.logToolStripMenuItem.Enabled = threeOrFour;
                this.log10ToolStripMenuItem.Enabled = threeOrFour;
                this.extendToolStripMenuItem.Enabled = threeOnly;
                this.reflectAboutToolStripMenuItem.Enabled = threeOnly;
                this.reflectAboutFastToolStripMenuItem.Enabled = threeOnly;
                this.addNetToolStripMenuItem.Enabled = threeOnly;
                this.rotateXToolStripMenuItem.Enabled = threeOnly;
                this.rotateYToolStripMenuItem.Enabled = threeOnly;
                this.rotateZToolStripMenuItem.Enabled = threeOnly;
                this.scale3ToolStripMenuItem.Enabled = fourOnly;
                this.normalize3ToolStripMenuItem.Enabled = fourOnly;
                this.normalizeFast3ToolStripMenuItem.Enabled = fourOnly;
                this.dot3VToolStripMenuItem.Enabled = fourOnly;
            }
        }

        public decimal Minimum
        {
            get
            {
                return this.XnumericUpDown.Minimum;
            }
            set
            {
                this.XnumericUpDown.Minimum = value;
                this.YnumericUpDown.Minimum = value;
                this.ZnumericUpDown.Minimum = value;
                this.WnumericUpDown.Minimum = value;
            }
        }

        public decimal Maximum
        {
            get
            {
                return this.XnumericUpDown.Maximum;
            }
            set
            {
                this.XnumericUpDown.Maximum = value;
                this.YnumericUpDown.Maximum = value;
                this.ZnumericUpDown.Maximum = value;
                this.WnumericUpDown.Maximum = value;
            }
        }

        public decimal Step
        {
            get
            {
                return this.XnumericUpDown.Increment;
            }
            set
            {
                this.XnumericUpDown.Increment = value;
                this.YnumericUpDown.Increment = value;
                this.ZnumericUpDown.Increment = value;
                this.WnumericUpDown.Increment = value;
            }
        }

        public event ParameterizedOperationEventHandler ScaleClick
        {
            add
            {
                m_eventDictionary[this.scaleToolStripMenuItem.Name] = m_eventDictionary[this.scaleToolStripMenuItem.Name] + value;
            }
            remove
            {
                m_eventDictionary[this.scaleToolStripMenuItem.Name] = m_eventDictionary[this.scaleToolStripMenuItem.Name] - value;
            }
        }

        public event ParameterizedOperationEventHandler InvScaleClick
        {
            add
            {
                m_eventDictionary[this.invScaleToolStripMenuItem.Name] = m_eventDictionary[this.invScaleToolStripMenuItem.Name] + value;
            }
            remove
            {
                m_eventDictionary[this.invScaleToolStripMenuItem.Name] = m_eventDictionary[this.invScaleToolStripMenuItem.Name] - value;
            }
        }

        public event ParameterizedOperationEventHandler ExtendClick
        {
            add
            {
                m_eventDictionary[this.extendToolStripMenuItem.Name] = m_eventDictionary[this.extendToolStripMenuItem.Name] + value;
            }
            remove
            {
                m_eventDictionary[this.extendToolStripMenuItem.Name] = m_eventDictionary[this.extendToolStripMenuItem.Name] - value;
            }
        }

        public event ParameterizedOperationEventHandler Scale3Click
        {
            add
            {
                m_eventDictionary[this.scale3ToolStripMenuItem.Name] = m_eventDictionary[this.scale3ToolStripMenuItem.Name] + value;
            }
            remove
            {
                m_eventDictionary[this.scale3ToolStripMenuItem.Name] = m_eventDictionary[this.scale3ToolStripMenuItem.Name] - value;
            }
        }

        public event ParameterizedOperationEventHandler AddClick
        {
            add
            {
                m_eventDictionary[this.addToolStripMenuItem.Name] = m_eventDictionary[this.addToolStripMenuItem.Name] + value;
            }
            remove
            {
                m_eventDictionary[this.addToolStripMenuItem.Name] = m_eventDictionary[this.addToolStripMenuItem.Name] - value;
            }
        }

        public event ParameterizedOperationEventHandler SubtractClick
        {
            add
            {
                m_eventDictionary[this.subtractToolStripMenuItem.Name] = m_eventDictionary[this.subtractToolStripMenuItem.Name] + value;
            }
            remove
            {
                m_eventDictionary[this.subtractToolStripMenuItem.Name] = m_eventDictionary[this.subtractToolStripMenuItem.Name] - value;
            }
        }

        public event ParameterizedOperationEventHandler MultiplyClick
        {
            add
            {
                m_eventDictionary[this.multiplyToolStripMenuItem.Name] = m_eventDictionary[this.multiplyToolStripMenuItem.Name] + value;
            }
            remove
            {
                m_eventDictionary[this.multiplyToolStripMenuItem.Name] = m_eventDictionary[this.multiplyToolStripMenuItem.Name] - value;
            }
        }

        public event EventHandler InvertClick
        {
            add
            {
                this.invertToolStripMenuItem.Click += value;
            }
            remove
            {
                this.invertToolStripMenuItem.Click -= value;
            }
        }

        public event EventHandler InvertSafeClick
        {
            add
            {
                this.invertSafeToolStripMenuItem.Click += value;
            }
            remove
            {
                this.invertSafeToolStripMenuItem.Click -= value;
            }
        }

        public event EventHandler NegateClick
        {
            add
            {
                this.negateToolStripMenuItem.Click += value;
            }
            remove
            {
                this.negateToolStripMenuItem.Click -= value;
            }
        }

        public event EventHandler AbsClick
        {
            add
            {
                this.absToolStripMenuItem.Click += value;
            }
            remove
            {
                this.absToolStripMenuItem.Click -= value;
            }
        }

        public event ParameterizedOperationEventHandler AverageClick
        {
            add
            {
                m_eventDictionary[this.averageToolStripMenuItem.Name] = m_eventDictionary[this.averageToolStripMenuItem.Name] + value;
            }
            remove
            {
                m_eventDictionary[this.averageToolStripMenuItem.Name] = m_eventDictionary[this.averageToolStripMenuItem.Name] - value;
            }
        }

        public event EventHandler NormalizeClick
        {
            add
            {
                this.normalizeToolStripMenuItem.Click += value;
            }
            remove
            {
                this.normalizeToolStripMenuItem.Click -= value;
            }
        }

        public event EventHandler NormalizeSafeClick
        {
            add
            {
                this.normalizeSafeToolStripMenuItem.Click += value;
            }
            remove
            {
                this.normalizeSafeToolStripMenuItem.Click -= value;
            }
        }

        public event EventHandler NormalizeSafeVClick
        {
            add
            {
                this.normalizeSafeVToolStripMenuItem.Click += value;
            }
            remove
            {
                this.normalizeSafeVToolStripMenuItem.Click -= value;
            }
        }

        public event EventHandler NormalizeFastClick
        {
            add
            {
                this.normalizeFastToolStripMenuItem.Click += value;
            }
            remove
            {
                this.normalizeFastToolStripMenuItem.Click -= value;
            }
        }

        public event EventHandler Normalize3Click
        {
            add
            {
                this.normalize3ToolStripMenuItem.Click += value;
            }
            remove
            {
                this.normalize3ToolStripMenuItem.Click -= value;
            }
        }

        public event EventHandler Normalize3VClick
        {
            add
            {
                this.normalize3VToolStripMenuItem.Click += value;
            }
            remove
            {
                this.normalize3VToolStripMenuItem.Click -= value;
            }
        }

        public event EventHandler NormalizeFast3Click
        {
            add
            {
                this.normalizeFast3ToolStripMenuItem.Click += value;
            }
            remove
            {
                this.normalizeFast3ToolStripMenuItem.Click -= value;
            }
        }

        public event ParameterizedOperationEventHandler RotateClick
        {
            add
            {
                m_eventDictionary[this.rotateToolStripMenuItem.Name] = m_eventDictionary[this.rotateToolStripMenuItem.Name] + value;
            }
            remove
            {
                m_eventDictionary[this.rotateToolStripMenuItem.Name] = m_eventDictionary[this.rotateToolStripMenuItem.Name] - value;
            }
        }

        public event ParameterizedOperationEventHandler RotateXClick
        {
            add
            {
                m_eventDictionary[this.rotateXToolStripMenuItem.Name] = m_eventDictionary[this.rotateXToolStripMenuItem.Name] + value;
            }
            remove
            {
                m_eventDictionary[this.rotateXToolStripMenuItem.Name] = m_eventDictionary[this.rotateXToolStripMenuItem.Name] - value;
            }
        }
        
        public event ParameterizedOperationEventHandler RotateYClick
        {
            add
            {
                m_eventDictionary[this.rotateYToolStripMenuItem.Name] = m_eventDictionary[this.rotateYToolStripMenuItem.Name] + value;
            }
            remove
            {
                m_eventDictionary[this.rotateYToolStripMenuItem.Name] = m_eventDictionary[this.rotateYToolStripMenuItem.Name] - value;
            }
        }
        
        public event ParameterizedOperationEventHandler RotateZClick
        {
            add
            {
                m_eventDictionary[this.rotateZToolStripMenuItem.Name] = m_eventDictionary[this.rotateZToolStripMenuItem.Name] + value;
            }
            remove
            {
                m_eventDictionary[this.rotateZToolStripMenuItem.Name] = m_eventDictionary[this.rotateZToolStripMenuItem.Name] - value;
            }
        }

        public event ParameterizedOperationEventHandler RotateAboutAxisClick
        {
            add
            {
                m_eventDictionary[this.rotateAboutAxisToolStripMenuItem.Name] = m_eventDictionary[this.rotateAboutAxisToolStripMenuItem.Name] + value;
            }
            remove
            {
                m_eventDictionary[this.rotateAboutAxisToolStripMenuItem.Name] = m_eventDictionary[this.rotateAboutAxisToolStripMenuItem.Name] - value;
            }
        }

        public event ParameterizedOperationEventHandler CrossClick
        {
            add
            {
                m_eventDictionary[this.crossToolStripMenuItem.Name] = m_eventDictionary[this.crossToolStripMenuItem.Name] + value;
            }
            remove
            {
                m_eventDictionary[this.crossToolStripMenuItem.Name] = m_eventDictionary[this.crossToolStripMenuItem.Name] - value;
            }
        }

        public event ParameterizedOperationEventHandler DotVClick
        {
            add
            {
                m_eventDictionary[this.dotVToolStripMenuItem.Name] = m_eventDictionary[this.dotVToolStripMenuItem.Name] + value;
            }
            remove
            {
                m_eventDictionary[this.dotVToolStripMenuItem.Name] = m_eventDictionary[this.dotVToolStripMenuItem.Name] - value;
            }
        }

        public event ParameterizedOperationEventHandler Dot3VClick
        {
            add
            {
                m_eventDictionary[this.dot3VToolStripMenuItem.Name] = m_eventDictionary[this.dot3VToolStripMenuItem.Name] + value;
            }
            remove
            {
                m_eventDictionary[this.dot3VToolStripMenuItem.Name] = m_eventDictionary[this.dot3VToolStripMenuItem.Name] - value;
            }
        }

        public event ParameterizedOperationEventHandler LerpClick
        {
            add
            {
                m_eventDictionary[this.lerpToolStripMenuItem.Name] = m_eventDictionary[this.lerpToolStripMenuItem.Name] + value;
            }
            remove
            {
                m_eventDictionary[this.lerpToolStripMenuItem.Name] = m_eventDictionary[this.lerpToolStripMenuItem.Name] - value;
            }
        }

        public event EventHandler LogClick
        {
            add
            {
                this.logToolStripMenuItem.Click += value;
            }
            remove
            {
                this.logToolStripMenuItem.Click -= value;
            }
        }

        public event EventHandler Log10Click
        {
            add
            {
                this.log10ToolStripMenuItem.Click += value;
            }
            remove
            {
                this.log10ToolStripMenuItem.Click -= value;
            }
        }

        public event ParameterizedOperationEventHandler ReflectAboutClick
        {
            add
            {
                m_eventDictionary[this.reflectAboutToolStripMenuItem.Name] = m_eventDictionary[this.reflectAboutToolStripMenuItem.Name] + value;
            }
            remove
            {
                m_eventDictionary[this.reflectAboutToolStripMenuItem.Name] = m_eventDictionary[this.reflectAboutToolStripMenuItem.Name] - value;
            }
        }

        public event ParameterizedOperationEventHandler ReflectAboutFastClick
        {
            add
            {
                m_eventDictionary[this.reflectAboutFastToolStripMenuItem.Name] = m_eventDictionary[this.reflectAboutFastToolStripMenuItem.Name] + value;
            }
            remove
            {
                m_eventDictionary[this.reflectAboutFastToolStripMenuItem.Name] = m_eventDictionary[this.reflectAboutFastToolStripMenuItem.Name] - value;
            }
        }

        public event ParameterizedOperationEventHandler AddNetClick
        {
            add
            {
                m_eventDictionary[this.addNetToolStripMenuItem.Name] = m_eventDictionary[this.addNetToolStripMenuItem.Name] + value;
            }
            remove
            {
                m_eventDictionary[this.addNetToolStripMenuItem.Name] = m_eventDictionary[this.addNetToolStripMenuItem.Name] - value;
            }
        }
        #endregion

        #region ISupportInitialize
        public void BeginInit()
        {
            foreach ( Control c in this.Controls )
            {
                c.SuspendLayout();
            }

            this.SuspendLayout();
        }

        public void EndInit()
        {
            foreach ( Control c in this.Controls )
            {
                c.ResumeLayout( false );
            }

            this.toolStrip1.PerformLayout();
            this.ResumeLayout( false );
            this.PerformLayout();
        }
        #endregion

        #region Public Functions
        public void SetRange( float min, float max )
        {
            try
            {
                this.Minimum = Convert.ToDecimal( min );
            }
            catch
            {
                if ( min < 0.0f )
                {
                    this.Minimum = decimal.MinValue;
                }
                else
                {
                    this.Minimum = decimal.MaxValue;
                }
            }

            try
            {
                this.Maximum = Convert.ToDecimal( max );
            }
            catch
            {
                if ( max < 0.0f )
                {
                    this.Maximum = decimal.MinValue;
                }
                else
                {
                    this.Maximum = decimal.MaxValue;
                }
            }
        }

        public void SetStep( float step )
        {
            try
            {
                this.Step = Convert.ToDecimal( step );
            }
            catch
            {
                if ( step < 0.0f )
                {
                    this.Step = decimal.MinValue;
                }
                else
                {
                    this.Step = decimal.MaxValue;
                }
            }
        }

        public void UpdateValueFromRemote( float[] value )
        {
            decimal[] prevValue = new decimal[4];
            m_value.CopyTo( prevValue, 0 );

            decimal[] values = new decimal[4];
            if ( value.Length >= 1 )
            {
                values[0] = Convert.ToDecimal( value[0] );
            }

            if ( value.Length >= 2 )
            {
                values[1] = Convert.ToDecimal( value[1] );
            }

            if ( value.Length >= 3 )
            {
                values[2] = Convert.ToDecimal( value[2] );
            }

            if ( value.Length >= 4 )
            {
                values[3] = Convert.ToDecimal( value[3] );
            }

            SetValueAndClamp( values, false );

            if ( m_firstTime )
            {
                m_value.CopyTo( m_prevValue, 0 );
                m_firstTime = false;
            }
            else
            {
                int i;
                for ( i = 0; i < 4; ++i )
                {
                    if ( m_value[i] != prevValue[i] )
                    {
                        break;
                    }
                }

                if ( i < 4 )
                {
                    prevValue.CopyTo( m_prevValue, 0 );
                }
            }
        }
        #endregion

        #region Private Functions
        private void InitializeAdditionalComponents( System.Xml.XmlNode schema )
        {
            if ( schema != null )
            {
                this.Text = schema.Attributes["name"].Value;
                this.XnumericUpDown.Value = decimal.Parse( schema.Attributes["X"].Value );
                this.YnumericUpDown.Value = decimal.Parse( schema.Attributes["Y"].Value );
                this.ZnumericUpDown.Value = decimal.Parse( schema.Attributes["Z"].Value );
                this.WnumericUpDown.Value = decimal.Parse( schema.Attributes["W"].Value );
                this.Minimum = decimal.Parse( schema.Attributes["Minimum"].Value );
                this.Maximum = decimal.Parse( schema.Attributes["Maximum"].Value );
                this.NumComponents = int.Parse( schema.Attributes["NumComponents"].Value );
                this.Step = decimal.Parse( schema.Attributes["Step"].Value );
            }

            m_eventDictionary.Add( this.scaleToolStripMenuItem.Name, null );
            m_eventDictionary.Add( this.invScaleToolStripMenuItem.Name, null );
            m_eventDictionary.Add( this.extendToolStripMenuItem.Name, null );
            m_eventDictionary.Add( this.scale3ToolStripMenuItem.Name, null );
            m_eventDictionary.Add( this.addToolStripMenuItem.Name, null );
            m_eventDictionary.Add( this.subtractToolStripMenuItem.Name, null );
            m_eventDictionary.Add( this.multiplyToolStripMenuItem.Name, null );
            m_eventDictionary.Add( this.rotateToolStripMenuItem.Name, null );
            m_eventDictionary.Add( this.rotateXToolStripMenuItem.Name, null );
            m_eventDictionary.Add( this.rotateYToolStripMenuItem.Name, null );
            m_eventDictionary.Add( this.rotateZToolStripMenuItem.Name, null );
            m_eventDictionary.Add( this.rotateAboutAxisToolStripMenuItem.Name, null );
            m_eventDictionary.Add( this.crossToolStripMenuItem.Name, null );
            m_eventDictionary.Add( this.dotVToolStripMenuItem.Name, null );
            m_eventDictionary.Add( this.dot3VToolStripMenuItem.Name, null );
            m_eventDictionary.Add( this.averageToolStripMenuItem.Name, null );
            m_eventDictionary.Add( this.lerpToolStripMenuItem.Name, null );
            m_eventDictionary.Add( this.reflectAboutToolStripMenuItem.Name, null );
            m_eventDictionary.Add( this.reflectAboutFastToolStripMenuItem.Name, null );
            m_eventDictionary.Add( this.addNetToolStripMenuItem.Name, null );

            m_eventTextDictionary.Add( this.scaleToolStripMenuItem.Name, "Scale Factor" );
            m_eventTextDictionary.Add( this.invScaleToolStripMenuItem.Name, "Inverse Scale Factor" );
            m_eventTextDictionary.Add( this.extendToolStripMenuItem.Name, "Distance to Extend" );
            m_eventTextDictionary.Add( this.scale3ToolStripMenuItem.Name, "Scale Factor" );
            m_eventTextDictionary.Add( this.addToolStripMenuItem.Name, "Vector to Add" );
            m_eventTextDictionary.Add( this.subtractToolStripMenuItem.Name, "Vector to Subtract" );
            m_eventTextDictionary.Add( this.multiplyToolStripMenuItem.Name, "Vector to Multiply" );
            m_eventTextDictionary.Add( this.rotateToolStripMenuItem.Name, "Radians About the Z Axis" );
            m_eventTextDictionary.Add( this.rotateXToolStripMenuItem.Name, "Radians About the X Axis" );
            m_eventTextDictionary.Add( this.rotateYToolStripMenuItem.Name, "Radians About the Y Axis" );
            m_eventTextDictionary.Add( this.rotateZToolStripMenuItem.Name, "Radians About the Z Axis" );
            m_eventTextDictionary.Add( this.rotateAboutAxisToolStripMenuItem.Name, "Radians Around the Axis,Axis to Rotate Around" );
            m_eventTextDictionary.Add( this.crossToolStripMenuItem.Name, "Second Vector in the Cross Product" );
            m_eventTextDictionary.Add( this.dotVToolStripMenuItem.Name, "Second Vector in the Dot Product" );
            m_eventTextDictionary.Add( this.dot3VToolStripMenuItem.Name, "Second Vector in the Dot Product" );
            m_eventTextDictionary.Add( this.averageToolStripMenuItem.Name, "Vector to Average With" );
            m_eventTextDictionary.Add( this.lerpToolStripMenuItem.Name, "Interpolation Value,Vector Representing an Interpolation Value of 1" );
            m_eventTextDictionary.Add( this.reflectAboutToolStripMenuItem.Name, "Normal Vector" );
            m_eventTextDictionary.Add( this.reflectAboutFastToolStripMenuItem.Name, "Normal Vector" );
            m_eventTextDictionary.Add( this.addNetToolStripMenuItem.Name, "Vector to Add" );
        }

        private void DisposeUpDown( UpDownBase obj )
        {
            // Manual fix to a memory leak in .NET with NumericUpDown controls
            SystemEvents.UserPreferenceChanged -=
                (UserPreferenceChangedEventHandler)Delegate.CreateDelegate(
                typeof( UserPreferenceChangedEventHandler ), obj, "UserPreferenceChanged" );
        }

        private void SetValueAndClamp( decimal[] value, bool triggerEvent )
        {
            int i;

            if (!this.IsReadOnly)
            {
                for ( i = 0; i < 4; ++i )
                {
                    value[i] = Math.Min( Math.Max( value[i], this.XnumericUpDown.Minimum ), this.XnumericUpDown.Maximum );
                }
            }

            for ( i = 0; i < 4; ++i )
            {
                if ( m_value[i] != value[i] )
                {
                    break;
                }
            }

            if ( i == 4 )
            {
                return;
            }

            value.CopyTo( m_value, 0 );

            if ( !this.IsReadOnly )
            {
                for ( i = 0; i < 4; ++i )
                {
                    if ( m_value[i] < this.Minimum )
                    {
                        this.Minimum = m_value[i];
                    }

                    if ( m_value[i] > this.Maximum )
                    {
                        this.Maximum = m_value[i];
                    }
                }
            }

            EventHandler eh = NumericUpdateDown_ValueChanged;
            if (this.InvokeRequired == true)
            {
                ControlVectorInvoke vectorInvoke = delegate(decimal[] values)
                {
                    this.XnumericUpDown.ValueChanged -= eh;
                    this.YnumericUpDown.ValueChanged -= eh;
                    this.ZnumericUpDown.ValueChanged -= eh;
                    this.WnumericUpDown.ValueChanged -= eh;
                    this.XnumericUpDown.Value = values[0];
                    this.YnumericUpDown.Value = values[1];
                    this.ZnumericUpDown.Value = values[2];
                    this.WnumericUpDown.Value = values[3];
                    this.XnumericUpDown.ValueChanged += eh;
                    this.YnumericUpDown.ValueChanged += eh;
                    this.ZnumericUpDown.ValueChanged += eh;
                    this.WnumericUpDown.ValueChanged += eh;
                };

                object[] args = new object[1];
                args[0] = m_value;
                this.Invoke(vectorInvoke, args);
            }
            else
            {
                this.XnumericUpDown.ValueChanged -= eh;
                this.YnumericUpDown.ValueChanged -= eh;
                this.ZnumericUpDown.ValueChanged -= eh;
                this.WnumericUpDown.ValueChanged -= eh;
                this.XnumericUpDown.Value = m_value[0];
                this.YnumericUpDown.Value = m_value[1];
                this.ZnumericUpDown.Value = m_value[2];
                this.WnumericUpDown.Value = m_value[3];
                this.XnumericUpDown.ValueChanged += eh;
                this.YnumericUpDown.ValueChanged += eh;
                this.ZnumericUpDown.ValueChanged += eh;
                this.WnumericUpDown.ValueChanged += eh;
            }

            if ( triggerEvent )
            {
                OnValueChanged();
            }
        }
        #endregion

        #region Overrides
        public override bool CanCopy
        {
            get
            {
                return true;
            }
        }

        public override bool CanPaste
        {
            get
            {
                return true;
            }
        }

        public override bool CanUndo
        {
            get
            {
                return true;
            }
        }

        public override object ClipboardData
        {
            get
            {
				string strVectorAsString = "";
				for (int i = 0; i < this.Vector.Length; ++i)
				{
					strVectorAsString += this.Vector[i];
					strVectorAsString += " ";
				}
				return strVectorAsString;
            }
            set
            {
				if ((value != null) && (value is string))
				{
					// Ok it is a string.  Are there any numbers in the string?
					string strClipboardString = value as string;
					string[] astrStringParts = strClipboardString.Split();
					decimal[] v = this.Vector;
					int i = 0;
					foreach (string strPossibleNumber in astrStringParts)
					{
						decimal fResult;
						if (decimal.TryParse(strPossibleNumber, out fResult))
						{
							v[i] = fResult;
							i++;
							if (i == v.Length) break;
						}
					}

					this.Vector = v;
				}
			}
        }

        public override bool IsClipboardDataAvailable
        {
            get
            {
                return base.IsClipboardDataAvailable && !this.IsReadOnly;
            }
        }

        public override bool IsUndoable
        {
            get
            {
                if ( base.IsUndoable )
                {
                    int i;
                    for ( i = 0; i < 4; ++i )
                    {
                        if ( m_value[i] != m_prevValue[i] )
                        {
                            break;
                        }
                    }

                    if ( i == 4 )
                    {
                        return false;
                    }

                    return true;
                }

                return false;
            }
        }

        protected override void OnIconChanged()
        {
            this.toolStripDropDownButton1.Image = m_Icon;
        }

        public override void ConnectControlToXmlNode( System.Xml.XmlNode root )
        {
            DataBindings.Add( "Text", FindXPathNode( root, ValueXPath + "/@name" ), "Value" );
            DataBindings.Add( "X", FindXPathNode( root, ValueXPath + "/@X" ), "Value" );
            DataBindings.Add( "Y", FindXPathNode( root, ValueXPath + "/@Y" ), "Value" );
            DataBindings.Add( "Z", FindXPathNode( root, ValueXPath + "/@Z" ), "Value" );
            DataBindings.Add( "W", FindXPathNode( root, ValueXPath + "/@W" ), "Value" );
            DataBindings.Add( "Minimum", FindXPathNode( root, ValueXPath + "/@Minimum" ), "Value" );
            DataBindings.Add( "Maximum", FindXPathNode( root, ValueXPath + "/@Maximum" ), "Value" );
            DataBindings.Add( "NumComponents", FindXPathNode( root, ValueXPath + "/@NumComponents" ), "Value" );
            DataBindings.Add( "Step", FindXPathNode( root, ValueXPath + "/@Step" ), "Value" );
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing )
            {
                if ( components != null )
                {
                    components.Dispose();
                }

                DisposeUpDown( this.XnumericUpDown );
                DisposeUpDown( this.YnumericUpDown );
                DisposeUpDown( this.ZnumericUpDown );
                DisposeUpDown( this.WnumericUpDown );

                m_eventDictionary.Clear();
            }
            base.Dispose( disposing );
        }

        public override void Undo()
        {
            decimal[] newValue = new decimal[m_prevValue.Length];
            m_prevValue.CopyTo( newValue, 0 );

            m_value.CopyTo( m_prevValue, 0 );

            SetValueAndClamp( newValue, true );
        }
        #endregion

        #region Event Handlers
        private void CheckBox_KeyDown( object sender, KeyEventArgs e )
        {
            if ( e.Control )
            {
                switch ( e.KeyCode )
                {
                    case Keys.C:
                        CopyToClipboard();
                        e.SuppressKeyPress = true;
                        break;
                    case Keys.V:
                        PasteFromClipboard();
                        e.SuppressKeyPress = true;
                        break;
                    case Keys.Z:
                        Undo();
                        e.SuppressKeyPress = true;
                        break;
                    default:
                        break;
                }
            }
        }

        private void CheckBox_MouseDown( object sender, MouseEventArgs e )
        {
            if ( e.Button == MouseButtons.Left )
            {
                CheckBox checkBox = sender as CheckBox;
                if ( checkBox != null )
                {
                    if (!this.IsReadOnly)
                    {
                        if ( ((checkBox == this.XcheckBox) && (this.NumComponents < 1))
                        || ((checkBox == this.YcheckBox) && (this.NumComponents < 2))
                        || ((checkBox == this.ZcheckBox) && (this.NumComponents < 3))
                        || ((checkBox == this.WcheckBox) && (this.NumComponents < 4)) )
                        {
                            return;
                        }

                        checkBox.Capture = true;
                        checkBox.CheckState = CheckState.Checked;
                        checkBox.Cursor = Cursors.SizeWE;
                        m_lastX = e.X;

                        m_value.CopyTo( m_prevValue, 0 );
                    }
                    else
                    {
                        checkBox.CheckState = CheckState.Unchecked;
                    }
                }
            }
        }

        private void CheckBox_MouseMove( object sender, MouseEventArgs e )
        {
            CheckBox checkBox = sender as CheckBox;
            if (!this.IsReadOnly && (checkBox != null) && checkBox.Capture && (e.Button == MouseButtons.Left))
            {
                if ( m_lastX > e.X )
                {
                    checkBox.Cursor = Cursors.PanWest;
                }
                else if ( m_lastX < e.X )
                {
                    checkBox.Cursor = Cursors.PanEast;
                }

                decimal scale = 0.25M;
                if ( Control.ModifierKeys != Keys.Control )
                {
                    scale *= 4.0M;
                }

                if ( Control.ModifierKeys != Keys.Shift )
                {
                    scale = 0.25M;
                }

                // update the value:

                decimal addition = (this.Step * (scale * Convert.ToDecimal( e.X - m_lastX )));
                if ( checkBox == this.XcheckBox )
                {
                    this.X += addition;
                }
                else if ( checkBox == this.YcheckBox )
                {
                    this.Y += addition;                    
                }
                else if ( checkBox == this.ZcheckBox )
                {
                    this.Z += addition;                    
                }
                else if ( checkBox == this.WcheckBox )
                {
                    this.W += addition;                    
                }

                m_lastX = e.X;
            }
        }

        private void CheckBox_MouseUp( object sender, MouseEventArgs e )
        {
            if ( e.Button == MouseButtons.Left )
            {
                CheckBox checkBox = sender as CheckBox;
                if ( checkBox != null )
                {
                    if (!this.IsReadOnly)
                    {
                        checkBox.Capture = false;
                        checkBox.CheckState = CheckState.Unchecked;
                        checkBox.Cursor = Cursors.Default;
                    }
                    else
                    {
                        checkBox.CheckState = CheckState.Unchecked;
                    }
                }
            }
        }

        private void NumericUpdateDown_ValueChanged( object sender, EventArgs e )
        {
            m_value.CopyTo( m_prevValue, 0 );

            SetValueAndClamp( new decimal[] { this.XnumericUpDown.Value, this.YnumericUpDown.Value, 
                this.ZnumericUpDown.Value, this.WnumericUpDown.Value }, true );
        }        

        private void NumericUpDown_Enter( object sender, EventArgs e )
        {
            if ( sender is NumericUpDown )
            {
                NumericUpDown numericUpDown = (NumericUpDown)sender;
                numericUpDown.Select( 0, numericUpDown.Text.Length );
            }
        }

        private void PromptFloat_ToolStripMenuItem_Click( object sender, EventArgs e )
        {
            ToolStripMenuItem item = sender as ToolStripMenuItem;
            
            float f = PromptFloatForm.ShowIt( this, m_eventTextDictionary[item.Name] );
            if ( f != float.MinValue )
            {
                OnParameterizedOperationEvent( item, new VectorOperationParameterEventArgs( f ) );
            }
        }

        private void PromptVector_ToolStripMenuItem_Click( object sender, EventArgs e )
        {
            ToolStripMenuItem item = sender as ToolStripMenuItem;
            
            decimal[] v = PromptVectorForm.ShowIt( this, m_eventTextDictionary[item.Name], 
                this.NumComponents, this.Minimum, this.Maximum, this.Step );
            if ( v != null )
            {
                float[] val = new float[] { (float)v[0], (float)v[1], (float)v[2], (float)v[3] };

                OnParameterizedOperationEvent( item, new VectorOperationParameterEventArgs( val ) );
            }
        }

        private void PromptFloatInt_ToolStripMenuItem_Click( object sender, EventArgs e )
        {
            ToolStripMenuItem item = sender as ToolStripMenuItem;
            string[] split = m_eventTextDictionary[item.Name].Split( new char[] { ',' } );

            float f = PromptFloatForm.ShowIt( this, split[0] );
            if ( f != float.MinValue )
            {
                int i = PromptAxisForm.ShowIt( this, split[1] );
                if ( i != -1 )
                {
                    OnParameterizedOperationEvent( item, new VectorOperationParameterEventArgs( f, i ) );
                }
            }
        }

        private void PromptFloatVector_ToolStripMenuItem_Click( object sender, EventArgs e )
        {
            ToolStripMenuItem item = sender as ToolStripMenuItem;
            string[] split = m_eventTextDictionary[item.Name].Split( new char[] { ',' } );

            float f = PromptFloatForm.ShowIt( this, split[0] );
            if ( f != float.MinValue )
            {
                decimal[] v = PromptVectorForm.ShowIt( this, split[1], this.NumComponents, this.Minimum, this.Maximum, this.Step );
                if ( v != null )
                {
                    float[] val = new float[] { (float)v[0], (float)v[1], (float)v[2], (float)v[3] };

                    OnParameterizedOperationEvent( item, new VectorOperationParameterEventArgs( f, val ) );
                }
            }
        }

        private void OnParameterizedOperationEvent( ToolStripMenuItem item, VectorOperationParameterEventArgs e )
        {
            ParameterizedOperationEventHandler handler;
            if ( m_eventDictionary.TryGetValue( item.Name, out handler ) && (handler != null) )
            {
                handler( item, e );
            }
        }
        #endregion
    }

    #region Helper Classes
    [Serializable]
    public class VectorClipboardData : Object
    {
        public VectorClipboardData( decimal[] vector, int numComponents )
        {
            m_vector = vector;
            m_numComponents = numComponents;
        }

        #region Variables
        private decimal[] m_vector;
        private int m_numComponents;
        #endregion

        #region Properties
        public decimal[] Vector
        {
            get
            {
                return m_vector;
            }
        }

        public int NumComponents
        {
            get
            {
                return m_numComponents;
            }
        }
        #endregion
    };
    #endregion

    public class VectorOperationParameterEventArgs : EventArgs
    {
        public VectorOperationParameterEventArgs( float f )
        {
            m_float = f;
        }

        public VectorOperationParameterEventArgs( float[] v )
        {
            m_vector = v;
        }

        public VectorOperationParameterEventArgs( float f, float[] v )
        {
            m_float = f;
            m_vector = v;
        }

        public VectorOperationParameterEventArgs( float f, int i )
        {
            m_float = f;
            m_int = i;
        }

        public VectorOperationParameterEventArgs( string namedOperation )
        {
            m_namedOperation = namedOperation;
        }

        public VectorOperationParameterEventArgs( string namedOperation, VectorOperationParameterEventArgs e )
        {
            m_namedOperation = namedOperation;
            m_namedOperationParameters = e;
        }

        #region Variables
        private int m_int = int.MinValue;
        private float m_float = float.MinValue;
        private float[] m_vector = null;
        private string m_namedOperation = null;
        private VectorOperationParameterEventArgs m_namedOperationParameters = null;
        #endregion

        #region Properties
        public int IntValue
        {
            get
            {
                return m_int;
            }
        }

        public float FloatValue
        {
            get
            {
                return m_float;
            }
        }

        public float[] Vector
        {
            get
            {
                return m_vector;
            }
        }

        public string NamedOperation
        {
            get
            {
                return m_namedOperation;
            }
        }

        public VectorOperationParameterEventArgs NamedOperationParamters
        {
            get
            {
                return m_namedOperationParameters;
            }
        }
        #endregion
    }
}

