using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace ragUi
{
    public partial class ControlDataView : ragUi.ControlBase
    {
        public ControlDataView()
        {
            InitializeComponent();
        }

        #region Properties
        public int MaxLength
        {
            set
            {
                this.dataRichTextBox.MaxLength = value;
            }
        }

        public string TheText
        {
            get
            {
                return this.dataRichTextBox.Text;
            }
            set
            {
                this.dataRichTextBox.Text = value;
            }
        }

        public override bool IsReadOnly
        {
            get { return dataRichTextBox.ReadOnly; }
            set
            {
                dataRichTextBox.ReadOnly = value;
                sendButton.Enabled = !value;
            }
        }
        #endregion

        #region Events
        public event EventHandler SendButtonClick;
        #endregion

        #region Event Handlers
        private void sendButton_Click( object sender, EventArgs e )
        {
            if ( SendButtonClick != null )
            {
                SendButtonClick( sender, e );
            }
        }
        #endregion

        #region Overrides
        [Browsable( true ), Category( "Misc" ), DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
        public override string Text
        {
            set
            {
                this.nameLabel.Text = value;
                PadControlTextForIcon( this.nameLabel, m_Icon );
            }
            get
            {
                return this.nameLabel.Text.TrimStart();
            }
        }

        protected override void OnIconChanged()
        {
            PadControlTextForIcon( this.nameLabel, m_Icon );
            this.nameLabel.Image = m_Icon;
        }
        #endregion
    }
}
