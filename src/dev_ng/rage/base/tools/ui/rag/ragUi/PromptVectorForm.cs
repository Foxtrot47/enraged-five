using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ragUi
{
    public partial class PromptVectorForm : Form
    {
        public PromptVectorForm()
        {
            InitializeComponent();

            this.valueControlVector.Text = "Vector";
        }

        public static decimal[] ShowIt( Control parentControl, string title, int numComponents, decimal min, decimal max, decimal step )
        {
            PromptVectorForm form = new PromptVectorForm();
            form.Text = title;

            form.valueControlVector.NumComponents = numComponents;
            form.valueControlVector.Minimum = min;
            form.valueControlVector.Maximum = max;
            form.valueControlVector.Step = step;

            if ( form.ShowDialog( parentControl ) == DialogResult.OK )
            {
                return form.valueControlVector.Vector;
            }

            return null;
        }
    }
}