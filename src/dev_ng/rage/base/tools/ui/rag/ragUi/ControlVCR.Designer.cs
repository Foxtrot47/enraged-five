namespace ragUi
{
    partial class ControlVCR
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && (components != null) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label = new System.Windows.Forms.Label();
            this.vcrControl = new PlaybackControls.VCRControl();
            this.SuspendLayout();
            // 
            // label
            // 
            this.label.AutoSize = true;
            this.label.Location = new System.Drawing.Point( 3, 9 );
            this.label.Name = "label";
            this.label.Size = new System.Drawing.Size( 35, 13 );
            this.label.TabIndex = 0;
            this.label.Text = "label";
            // 
            // vcrControl
            // 
            this.vcrControl.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.vcrControl.Location = new System.Drawing.Point( 256, 3 );
            this.vcrControl.Name = "vcrControl";
            this.vcrControl.Size = new System.Drawing.Size( 205, 24 );
            this.vcrControl.TabIndex = 1;
            // 
            // UserControl1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add( this.vcrControl );
            this.Controls.Add( this.label );
            this.Name = "UserControl1";
            this.Size = new System.Drawing.Size( 464, 32 );
            this.ResumeLayout( false );
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label;
        private PlaybackControls.VCRControl vcrControl;
    }
}
