using System;
using System.Net.Sockets;
using System.Net;

namespace ragCore
{
	/// <summary>
	/// Summary description for INamedPipe.
	/// </summary>
	public interface INamedPipe
	{
		bool IsValid();
		bool IsConnected();

		bool Create(PipeID id,bool wait);
        void CancelCreate();
		void Close();
		bool HasData();
        int GetNumBytesAvailable();
	
		int ReadData(byte[] buffer,int numBytesToRead,bool exact);
		int WriteData(byte[] buffer,int numBytesToWrite);

        bool NoDelay { get; set; }

        Socket Socket { get; }

        string Name { get; }

        IPAddress GetLocalIP();
	}
}
