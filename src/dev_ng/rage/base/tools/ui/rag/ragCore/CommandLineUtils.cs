﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ragCore
{
    /// <summary>
    /// Util methods for parsing RAG's commandline parameters.
    /// </summary>
    public static class CommandLineUtils
    {
        /// <summary>
        /// Checks whether the -noconfig commandline parameter was passed in.
        /// </summary>
        /// <returns></returns>
        public static bool UseToolsConfig()
        {
            bool useConfig = true;

            string[] args = Environment.GetCommandLineArgs();
            foreach (String str in args)
            {
                if (str == "-noconfig")
                {
                    useConfig = false;
                    break;
                }
            }
            return useConfig;
        }

        /// <summary>
        /// Checks whether we are connecting against the proxy.
        /// </summary>
        /// <returns></returns>
        public static bool UseProxy()
        {
            bool useProxy = true;

            string[] args = Environment.GetCommandLineArgs();
            foreach (String str in args)
            {
                if (str == "-noproxy")
                {
                    useProxy = false;
                    break;
                }
            }
            return useProxy;
        }

        public static bool Verbose()
        {
            bool verbose = false;

            string[] args = Environment.GetCommandLineArgs();
            foreach (String str in args)
            {
                if (str == "-verbose")
                {
                    verbose = true;
                    break;
                }
            }
            return verbose;
        }
    }
}
