using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Security.Permissions;
using System.Text;
using System.Threading;
using System.Net.NetworkInformation;
using RSG.Base.Logging;




namespace ragCore
{
    /// <summary>
    /// Summary description for NamedPipeSocket.
    /// </summary>
    public class NamedPipeSocket : INamedPipe
    {
        public NamedPipeSocket()
        {
        }

        public bool IsValid()
        {
            return IsConnected();
        }

        public virtual bool IsConnected()
        {
            CheckConnectionStatus();
            return m_IsConnected;
        }

        public string Name
        {
            get
            {
                return m_name;
            }
        }

        public override string ToString()
        {
            return this.Name;
        }

        protected delegate Socket AcceptSocketDel();

        static int sm_pipeTimeout = 15 * 1000;   // 5 second default timeout

        protected bool m_IsConnected;
        protected DateTime m_LastConnectedTime = DateTime.Now;

        protected TcpListener m_Listener;
        protected Socket m_Socket;
        protected string m_name = string.Empty;
        private ManualResetEvent m_CreateResetEvent = new ManualResetEvent( false );

        public static event ragCore.BankRemotePacketProcessor.DisplayMessageEventHandler DisplayMessage;

        public NamedPipeSocket( Socket socket )
        {
            m_Socket = socket;
            m_IsConnected = m_Socket.Connected;
        }


        public virtual bool Create( PipeID pipeId, bool wait )
        {
            LogFactory.ApplicationLog.MessageCtx("NamedPipeSocket", "Creating pipe {0}.", pipeId.ToString());
            m_IsConnected = false;
            m_name = pipeId.ToString();

            // try to get a connection;
            if ( m_Listener == null )
            {
                if (pipeId.Listener != null)
                {
                    m_Listener = pipeId.Listener;
                }
                else
                {
                    m_Listener = new TcpListener(pipeId.Address, pipeId.Port);

                    try
                    {
                        m_Listener.Start();
                    }
                    catch (SocketException e)
                    {
                        LogFactory.ApplicationLog.ToolExceptionCtx("NamedPipeSocket", e, "Socket Exception: {0}, Error Code: {1}", e.Message, e.SocketErrorCode);
                        

                        OnDisplayMessage(new DisplayMessageEventArgs(
                            String.Format("{0}\n\nAnother service is already running on port {1} so RAG is unable to start.  Rebooting may fix this problem.",
                                e.Message,
                                pipeId.Port),
                            "RAG port already in use!",
                            true,
                            true));
                        return false;
                    }
                    catch (Exception e)
                    {
                        OnDisplayMessage(new DisplayMessageEventArgs(
                            String.Format("{0}\n\nAnother service is already running on port {1} so RAG is unable to start.  Rebooting may fix this problem.",
                                e.Message,
                                pipeId.Port),
                            "RAG port already in use!",
                            true,
                            true));
                    }
                }

                AcceptSocketDel del = new AcceptSocketDel( m_Listener.AcceptSocket );
                IAsyncResult result = del.BeginInvoke( null, null );

                // Either wait indefinitely or for a given timeout period to accept a socket
                // or until another thread asks to cancel.
                if (!result.CompletedSynchronously)
                {
                    WaitHandle[] handles = new WaitHandle[] { m_CreateResetEvent, result.AsyncWaitHandle };
                    int handleIndex = 0;
                    if (wait)
                    {
                        handleIndex = WaitHandle.WaitAny(handles);
                    }
                    else
                    {
                        handleIndex = WaitHandle.WaitAny(handles, sm_pipeTimeout);
                    }

                    if (handleIndex == WaitHandle.WaitTimeout)
                    {
                        LogFactory.ApplicationLog.MessageCtx("NamedPipeSocket", "Timed out while waiting for connection to port {0}.", pipeId.Port.ToString());
                        return false;
                    }
                    else if (handleIndex == 0)
                    {
                        LogFactory.ApplicationLog.MessageCtx("NamedPipeSocket", "Reset event received while waiting for connection to port {0}.", pipeId.Port.ToString());
                        //m_CreateResetEvent.Reset();
                        return false;
                    }
                }
                else
                {
                    LogFactory.ApplicationLog.MessageCtx("NamedPipeSocket", "AcceptSocket completed asynchronously.");
                }

                m_Socket = del.EndInvoke( result );
                if ( m_Socket != null )
                {
                    // Won't use this yet but I'll leave it in commented out for future use if necessary.
                    LingerOption lingerOption = new LingerOption( false, 0 );
                    m_Socket.SetSocketOption( SocketOptionLevel.Socket, SocketOptionName.Linger, lingerOption );

                    //m_Socket.SetSocketOption( SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true );
                    m_Listener.Server.Close();
                    m_Listener.Stop();
                    m_IsConnected = true;

                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Raises the <see cref="DisplayMessage"/> event.
        /// </summary>
        /// <param name="e">A <see cref="DisplayMessageEventArgs"/> that contains the event data.</param>
        protected void OnDisplayMessage(DisplayMessageEventArgs e)
        {
            if (DisplayMessage != null)
            {
                DisplayMessage(this, e);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        /// <param name="caption"></param>
        protected void PrintErrorQuitMessage( string message, string caption )
        {
            // Yeah, aware that this is a bit ugly.
            // But it's better than a network class showing a dialog box directly..

            if (DisplayMessage != null )
            {
                DisplayMessageEventArgs args = new DisplayMessageEventArgs(message, caption, false);
                DisplayMessage(null, args);

                if ( args.ResultYesOrOK )
                {
                    Process.Start( "taskmgr.exe" );
                }                  
            }    
        }

        public void SendQuitMessage()
        {
            // if the bank pipe isn't connected, we don't want to send the quit message since we'll get no response!
            if ( Sensor.CurrentSensor.HasStopped || Sensor.CurrentSensor.HasQuit || !IsConnected() )
                return;

            try
            {
                // this really only works with one instance of rag running - we probably want rag to specify a unique port:
                PipeID pipeId = new PipeID( "quit", 0xDEAD );

                LogFactory.ApplicationLog.MessageCtx("NamedPipeSocket", "SendQuitMessage on BankPipe '{0}' to '{1}'.", ToString(), pipeId.Port);

                TcpClient client = null;
                try
                {
                    client = Sensor.CurrentSensor.GetDebugIpAddress( pipeId.Port );
                }
                catch ( Exception e )
                {
                    // it seems the application has already terminated, so just return
                    LogFactory.ApplicationLog.ToolExceptionCtx("NamedPipeSocket", e, e.ToString());
                    return;
                }

                byte[] buffer = new byte[4];
                int numMsToWait = 7000;
                while ( true )
                {
                    NetworkStream stream = client.GetStream();

                    if ( stream.DataAvailable )
                    {
                        int count = stream.Read( buffer, 0, 4 );
                        if ( count == 4 )
                        {
                            string theString = Encoding.ASCII.GetString( buffer, 0, count );
                            if ( theString == "L8R\0" )
                            {
                                LogFactory.ApplicationLog.MessageCtx("NamedPipeSocket", "SendQuitMessage ack received.");
                                return;
                            }
                        }
                    }
                    else if ( numMsToWait <= 0 )
                    {
                        PrintErrorQuitMessage( "Quit message not received from the RAGE application.  This could be because:\n\n" +
                            "1.) The application has already stopped on its own or it was killed.\n" +
                            "2.) The application is stopped in a debugger.\n\n" +
                            "\n\nPress 'Yes' to quit RAG and launch the task manager" +
                            "\n'Press No' to just quit RAG\n",
                            "Did Not Receive Quit Message" );
                        return;
                    }
                    else
                    {
                        Thread.Sleep( 250 );
                        numMsToWait -= 250;
                    }
                }
            }
            catch ( ThreadAbortException )
            {
                // return gracefully
                return;
            }
            catch ( Exception e )
            {
                PrintErrorQuitMessage( "Error trying to connect to the application: " + e +
                    "\n\nYou may have to kill the application yourself." +
                    "\n\nPress 'Yes' to quit RAG and launch the task manager" +
                    "\n'Press No' to just quit RAG\n",
                    "Did Not Receive Quit Message" );
            }
        }

        public void Close()
        {
            LogFactory.ApplicationLog.MessageCtx("NamedPipeSocket", "Closing '{0}'", Name );
            m_IsConnected = false;

            if ( m_Socket != null )
            {
                try
                {
                    if (m_Socket.Connected)
                    {
                        m_Socket.Shutdown( SocketShutdown.Both );
                    }
                }
                catch (System.Exception ex)
                {
                    LogFactory.ApplicationLog.ToolExceptionCtx("NamedPipeSocket", ex, "Caught an exception while trying to shutdown socket: " + ex.ToString());                	
                }
                finally
                {
                    m_Socket.Close();
                }
            }

            if ( m_Listener != null )
            {
                try
                {
                    if (m_Listener.Server.Connected)
                    {
                        m_Listener.Server.Shutdown(SocketShutdown.Both);
                    }
                }
                catch ( System.Exception ex )
                {
                    LogFactory.ApplicationLog.ToolExceptionCtx("NamedPipeSocket", ex, "Caught an exception while trying to shutdown socket: " + ex.ToString());
                }
                finally
                {
                    m_Listener.Server.Close();
                    m_Listener.Stop();
                    m_Listener = null;
                }
            }

            LogFactory.ApplicationLog.MessageCtx("NamedPipeSocket", "Closed '{0}'", Name );
        }

        public bool CheckConnectionStatus()
        {
            if ( m_IsConnected == false )
                return false;

            // Don't "spam "the connection unnecessarily: If it was connected one second ago, it
            // probably still is. May return true even though it's actually false, but 
            // I *think* that shouldn't be a big issue. 
            lock ( m_Socket )
            {
                TimeSpan timeSinceLastConnectionCheck = DateTime.Now - m_LastConnectedTime;
                if ( timeSinceLastConnectionCheck > TimeSpan.FromSeconds(1))
                {
                    m_LastConnectedTime = DateTime.Now;
                }
                else
                {
                    return true;
                }
            }

            try
            {
                // checks to see if we're disconnected:
                if ( m_Socket.Connected == false )
                {
                    m_IsConnected = false;
                    return false;
                }

                // according to the net, this is the best way to check for a disconnect - we poll and then peek:
                else
                {
                    // Need to lock the socket, otherwise if there's data on the socket, Poll will return true. 
                    // If another thread then manages to read that data before the call to Receive here, then
                    // the socket will be empty, and it'll look as though it's been disconnected.
                    lock ( m_Socket )
                    {
                        if ( m_Socket.Poll( 0, SelectMode.SelectRead ) )
                        {
                            byte[] dummy = new byte[1];
                            m_Socket.ReceiveTimeout = 5 * 1000; // will cause a SocketException if timed out

                            try
                            {
                                Int32 data = m_Socket.Receive( dummy, 1, SocketFlags.Peek );
                                if ( data == 0 )
                                {
                                    m_IsConnected = false;
                                    return false;
                                }
                            }
                            catch (SocketException ex)
                            {
                                LogFactory.ApplicationLog.ToolExceptionCtx("NamedPipeSocket", ex, "Named pipe '{0}' timed out during Socket.Receive test. Connected={1}", Name, m_Socket.Connected);
                                LogFactory.ApplicationLog.ErrorCtx("NamedPipeSocket", "{0}", ex.ErrorCode);
                            }
                        }
                    }
                }
            }
            catch ( SocketException e)
            {
                LogFactory.ApplicationLog.ToolExceptionCtx("NamedPipeSocket", e, "Named pipe '{0}' disconnected", Name);
                m_IsConnected = false;
                return false;
            }

            return true;
        }

        public virtual bool HasData()
        {
            if ( CheckConnectionStatus() == false )
                return false;

            if ( m_Socket.Available != 0 )
                return true;

            return false;
        }

        public virtual int GetNumBytesAvailable()
        {
            if ( m_IsConnected && (m_Socket != null) )
            {
                return m_Socket.Available;
            }

            return 0;
        }

        public static event UnhandledExceptionEventHandler ReadWriteFailure;

        protected static void OnReadWriteFailure( NamedPipeSocket pipe, Exception e )
        {
            if ( NamedPipeSocket.ReadWriteFailure != null )
            {
                NamedPipeSocket.ReadWriteFailure( pipe, new UnhandledExceptionEventArgs( e, false ) );
            }
        }

        public virtual int ReadData( byte[] buffer, int numBytesToRead, bool readExact )
        {
            int numBytesRead = 0;
            try
            {
                lock ( m_Socket )
                {
                    // If Receive returns 0 bytes it means that the remote end has closed the socket.
                    int thisRead;
                    do
                    {
                        thisRead = m_Socket.Receive( buffer, numBytesRead, numBytesToRead, SocketFlags.None );
                        numBytesRead += thisRead;
                        numBytesToRead -= thisRead;
                    } while (readExact && numBytesToRead > 0 && thisRead > 0);
                }
            }
            catch ( Exception e )
            {
                OnReadWriteFailure( this, e );

                m_IsConnected = false;
            }

            return numBytesRead;
        }

        public virtual int WriteData( byte[] buffer, int numBytesToWrite )
        {
            try
            {
                lock ( m_Socket )
                {
                    m_Socket.Send( buffer, numBytesToWrite, SocketFlags.None );
                }
            }
            catch ( Exception e )
            {
                OnReadWriteFailure( this, e );

                m_IsConnected = false;
                return 0;
            }

            return numBytesToWrite;
        }

        public string GetRemoteIpAddress()
        {
            if ( m_Socket != null )
            {
                EndPoint pt = m_Socket.RemoteEndPoint;
                string ipAddress = pt.ToString();
                int indexOf = ipAddress.IndexOf( ':' );
                if ( indexOf > -1 )
                {
                    return ipAddress.Substring( 0, indexOf );
                }

                return ipAddress;
            }

            return null;
        }

        public IPAddress GetRemoteIp()
        {
            if (m_Socket == null)
            {
                return null;
            }

            IPEndPoint ipEndpoint = (IPEndPoint)m_Socket.RemoteEndPoint;
            return ipEndpoint.Address;
        }

        public bool NoDelay
        {
            get
            {
                if ( m_Socket != null )
                {
                    return m_Socket.NoDelay;
                }
                else
                {
                    return false;   // Socket's default
                }
            }
            set
            {
                if ( m_Socket != null )
                {
                    m_Socket.NoDelay = value;
                }
            }
        }

        public Socket Socket
        {
            get
            {
                return m_Socket;
            }
        }

        static public int PipeTimeoutSeconds
        {
            get
            {
                if ( sm_pipeTimeout == -1 )
                {
                    // No timeout
                    return 0;
                }
                else
                {
                    // Convert from milliseconds to seconds
                    return sm_pipeTimeout / 1000;
                }
            }
            set
            {
                if ( value > 0 )
                {
                    // Convert timeout from seconds to milliseconds
                    sm_pipeTimeout = value * 1000;
                }
                else
                {
                    // No timeout
                    sm_pipeTimeout = -1;
                }
            }
        }


        public void CancelCreate()
        {
            if ( m_Listener == null )
            {
                return;
            }

            if ( !m_IsConnected )
            {
                LogFactory.ApplicationLog.MessageCtx("NamedPipeSocket", "Named pipe '{0}' cancelling Create()", Name);
                m_CreateResetEvent.Set();                
            }
        }


        public IPAddress GetLocalIP()
        {
            if ( m_Socket != null )
            {
                return IPAddress.Parse( m_Socket.LocalEndPoint.ToString().Split( ':' )[0] );
            }

            return IPAddress.Any;
        }

        public bool IsListeningForConnections()
        {
            return m_Listener != null;
        }
    }
}
