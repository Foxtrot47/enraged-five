using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace ragCore
{
    public interface IRageApplication
    {
        /// <summary>
        /// The display name of the Application.  Typically displayed as the Application Window's title.
        /// </summary>
        string VisibleName { get; }

        /// <summary>
        /// A unique identifier for this Application.
        /// </summary>
        string Guid { get; }

        /// <summary>
        /// The path of the Application.
        /// </summary>
        string ExePath { get; }

        /// <summary>
        /// The name of the Application
        /// </summary>
        string ExeName { get; }

        /// <summary>
        /// The arguments pass to the Application.
        /// </summary>
        string Args { get; }

        /// <summary>
        /// Whether the Application (one that is not the Main Application) should start when the Main Application starts.
        /// </summary>
        bool ShouldStart { set; }

        /// <summary>
        /// Whether the Application is running or not.
        /// </summary>
        bool IsStarted { get; }

        /// <summary>
        /// Whether the Application was stopped or not.
        /// </summary>
        bool IsStopped { get; }

        /// <summary>
        /// The IBankManager associated with the Application.
        /// </summary>
        IBankManager BankMgr { get; }

        /// <summary>
        /// Indicates whether any of the Pipes were connected at one time.
        /// </summary>
        bool WasConnected { get; }

        /// <summary>
        /// Indicates if any pipes has lost connection with its Application.
        /// </summary>
        bool PipesDisconnected { get; }

        /// <summary>
        /// The BankPipe through which the Application communicates.
        /// </summary>
        BankPipe BankPipe { get; }

        /// <summary>
        /// The TTY Output Pipe
        /// </summary>
        INamedPipe TheOutputPipe { get; }

        /// <summary>
        /// The Bank (Widget) Pipe.
        /// </summary>
        INamedPipe TheBankPipe { get; }

        /// <summary>
        /// Get or set the image in the Application Window's RenderWindow for this application.  Typically used for non-Win32 Applications.
        /// </summary>
        string RenderWindowImageFilename { get; set; }

        /// <summary>
        /// Get or set the resolution of the Application Window's RenderWindow for this application.
        /// </summary>
        Size RenderWindowResolution { get; set; }

        /// <summary>
        /// Retrieves a PipeID with an IP address and an appropriate port number based on the offset provided.
        /// Typically used by IViewPlugIns to connect to an Application through auxiliary ports.
        /// </summary>
        /// <param name="offset">The port offset desired.</param>
        /// <returns>A PipeID.</returns>
        PipeID GetRagPipeSocket( int offset );
    }
}
