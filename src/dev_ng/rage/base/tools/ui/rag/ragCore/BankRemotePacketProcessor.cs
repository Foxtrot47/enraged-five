using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading;

using ragCompression;
using System.Collections.Concurrent;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;


namespace ragCore
{
    /// <summary>
    /// Summary description for BankRemotePacket.
    /// </summary>
    public class BankRemotePacketProcessor
    {
        #region Fields
        /// <summary>
        /// Reference to the <see cref="BankRemotePacketProcessor"/>'s log object.
        /// </summary>
        private readonly ILog _log;

        /// <summary>
        /// Flag indicating whether we should log create and destroy calls.
        /// </summary>
        private readonly bool _logWidgetCreateDestroy;

        /// <summary>
        /// Private field for the <see cref="WidgetLog"/> property.
        /// </summary>
        private readonly ILog _widgetLog;

        /// <summary>
        /// Private field for the <see cref="WidgetLogTarget"/> property.
        /// </summary>
        private readonly IUniversalLogTarget _widgetLogTarget;
        #endregion

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public BankRemotePacketProcessor(bool logWidgetPackets = false)
        {
            _log = LogFactory.ApplicationLog;

            _logWidgetCreateDestroy = logWidgetPackets;
            if (_logWidgetCreateDestroy)
            {
                string logName = String.Format("WidgetCreateDestroy_{0:yyyyMMddHHmmss}", DateTime.Now);

                _widgetLog = LogFactory.CreateUniversalLog(logName, false);
                _widgetLogTarget = LogFactory.CreateUniversalLogFile((IUniversalLog)_widgetLog);
                _widgetLogTarget.Reconnect(LogFactory.GetLogLevel());
            }
        }
        #endregion // Constructor(s)

        #region Properties
        /// <summary>
        /// Log object to use for logging widget create/destroy calls.
        /// </summary>
        public ILog WidgetLog
        {
            get { return _widgetLog; }
        }

        /// <summary>
        /// Log target object.
        /// </summary>
        public IUniversalLogTarget WidgetLogTarget
        {
            get { return _widgetLogTarget; }
        }
        #endregion

        #region Delegates
        public delegate bool HandlerDel(BankRemotePacket p, out String errorMessage);
        public delegate bool DestroyDelegate( BankRemotePacket packet, BankRemotePacketProcessor packetProcessor, out String errorMessage );

        public delegate void BeginBankUpdateDelegate();
        public delegate void EndBankUpdateDelegate();
        
        public delegate void DisplayMessageEventHandler( object sender, DisplayMessageEventArgs e );

        public delegate void DataReceivedDelegate(object sender, DataPacketEventArgs args);

        #endregion

        #region Public Class
        /// <summary>
        /// 
        /// </summary>
        public class DataPacketEventArgs : EventArgs
        {
            public DataPacketEventArgs(byte[] data, int count)
            {
                Data = data;
                Count = count;
            }

            #region Variables
            public byte[] Data;
            public int Count;
            #endregion
        }
        #endregion // Private Class

        #region Events
        /// <summary>
        /// 
        /// </summary>
        public static event DisplayMessageEventHandler DisplayMessage;

        /// <summary>
        /// 
        /// </summary>
        public event DataReceivedDelegate DataReceieved;
        #endregion

        #region Static Properties
        public static System.Threading.Mutex MakingVisibleMutex
        {
            get
            {
                return sm_MakingVisibleMutex;
            }
        }

        public DestroyDelegate DestroyHandler
        {
            set
            {
                sm_DestroyHandler = value;
            }
        }

        public List<Widget> DisabledWidgets
        {
            get
            {
                return sm_disabledWidgets;
            }
        }

        // todo make obsolete
        public static BeginBankUpdateDelegate BeginBankUpdate
        {
            set
            {
                sm_beginBankUpdateHandler = value;
            }
        }

        public static EndBankUpdateDelegate EndBankUpdate
        {
            set
            {
                sm_endBankUpdateHandler = value;
            }
        }

        public bool IsPipeClear { get; set; }
        public bool HasDataToProcess(BankPipe pipe)
        {
            return _packetQueues[pipe].Count > 0;
        }

        public object PipeOwner { get; set; }

        #endregion

        #region Static Variables
        private Dictionary<int, HandlerDel> _widgetTypes = new Dictionary<int, HandlerDel>();
        private Dictionary<INamedPipe, CompressionFactory> _compressionFactory = new Dictionary<INamedPipe, CompressionFactory>();
        private DestroyDelegate sm_DestroyHandler;
        private static BeginBankUpdateDelegate sm_beginBankUpdateHandler;
        private static EndBankUpdateDelegate sm_endBankUpdateHandler;

        /// <summary>
        /// One packet queue per bank pipe.
        /// </summary>
        private Dictionary<BankPipe, ConcurrentQueue<BankRemotePacket>> _packetQueues = new Dictionary<BankPipe, ConcurrentQueue<BankRemotePacket>>();
        
        private static System.Threading.Mutex sm_MakingVisibleMutex = new System.Threading.Mutex();

        private List<Widget> sm_disabledWidgets = new List<Widget>();
        #endregion



        #region Public Functions
        /// <summary>
        /// Attempts to read data of the specified BankPipe converting the data into a BankRemotePacket via
        /// the (de)compression factory.  Once decompressed it adds the packets to a queue for processing
        /// by a consumer thread.
        /// This is the thread function.
        /// </summary>
        /// <param name="pipe">The pipe to read data from.</param>
        public void Update( BankPipe pipe )
        {
            if ( pipe.HasData() )
            {
                IsPipeClear = false;

                // retrieve the data
                byte[] bytes = new byte[pipe.GetNumBytesAvailable()];
                int numRead = pipe.ReadData( bytes, bytes.Length, false );

                CompressionFactory factory = GetOrCreateCompressionFactory(pipe);
                try
                {
                    // send the data to the factory for decompression
                    factory.DecompressData(bytes, (UInt32)numRead);
                }
                catch (System.Data.DataException ex)
                {
                    OnDisplayMessage(new DisplayMessageEventArgs(
                            "Unable to decompress bank data received from the game due to it being badly formed.\n" +
                            "This is most usually due to an out of memory problem on the console.  This can be confirmed by looking " +
                            "for a message that looks like the following in the console logs:\n" +
                            "[XNET:1] Warning: send failed: 10055\n" +
                            "^[XNET:1] Warning: Out of memory allocating a CPacket of size 1448 bytes\n\n" +
                            "RAG is no longer in a valid state and will now close.",
                            "Bank Data Decompression Error",
                            true));

                    // rethrow the exception so the main exception handler can catch it
                    throw ex;
                }

                // parse every decompressed data set
                foreach ( CompressionData cData in factory.Data )
                {
                    int index = 0;
                    while ( index < cData.DecompressedData.Length )
                    {
                        int startIndex = index;
                        BankRemotePacket p = new BankRemotePacket( pipe );
                        p.ReceiveHeader( cData.DecompressedData, ref index );

                        if ( p.Length > 0 )
                        {
                            if ( index >= cData.DecompressedData.Length )
                            {
                                _log.Error("BankRemotePacket expected more data than was decompressed.");
                                break;
                            }

                            p.ReceiveStorage( cData.DecompressedData, ref index );
                        }

                        if ( !_widgetTypes.ContainsKey( p.Guid ) )
                        {
                            // skip packet:
                            p.SkipRead();

                            char a, b, c, d;
                            Widget.ReverseGUID( p.Guid, out a, out b, out c, out d );
                            _log.Error("Unknown guid '{0}{1}{2}{3}'", a, b, c, d);
                        }
                        else
                        {
                            ConcurrentQueue<BankRemotePacket> q;
                            if (!_packetQueues.TryGetValue(pipe, out q))
                            {
                                q = new ConcurrentQueue<BankRemotePacket>();
                                _packetQueues.Add( pipe, q );
                            }

                            q.Enqueue(p);
                        }
                    }

                    byte[] compressedData = new byte[cData.GetHeaderSize() + cData.CompressedSize];
                    int dataOffset = cData.BuildHeader(compressedData, 0);
                    Buffer.BlockCopy(cData.CompressedData, 0, compressedData, dataOffset, (int)cData.CompressedSize);
                    OnDataReceived(compressedData, (int)(cData.GetHeaderSize() + cData.CompressedSize));
                }

                // clear the data
                _compressionFactory[pipe].Data.Clear();
                if ( !_compressionFactory[pipe].HasDataLeftToProcess())
                {
                    IsPipeClear = true;
                }
            }
        }

        /// <summary>
        /// This is called to update the UI.
        /// </summary>
        public void Process(BankPipe pipe)
        {
            ConcurrentQueue<BankRemotePacket> q;
            if (_packetQueues.TryGetValue(pipe, out q))
            {
                bool resumeLayoutNeeded = false;

                BankRemotePacket p;
                while (q.TryDequeue(out p))
                {
                    // Logging
                    if (_logWidgetCreateDestroy)
                    {
                        if (p.Command == (ushort)(BankRemotePacket.EnumPacketType.DESTROY))
                        {
                            string errorMessage;
                            Widget widget;
                            if (p.BankPipe.RemoteToLocal(p.Id, out widget, out errorMessage))
                            {
                                _widgetLog.DebugCtx("DESTROY", widget.Path);
                            }
                        }
                    }

                    if (p.Command == (ushort)(BankRemotePacket.EnumPacketType.DESTROY))
                    {
                        if (!resumeLayoutNeeded && (sm_beginBankUpdateHandler != null))
                        {
                            sm_beginBankUpdateHandler();
                            resumeLayoutNeeded = true;
                        }

                        try
                        {
                            // surrounded by a mutex to prevent a race condition since this function is usually on a timer: 
                            sm_MakingVisibleMutex.WaitOne();

                            String errorMessage;
                            if (!sm_DestroyHandler(p, this, out errorMessage))
                            {
                                _log.Error(errorMessage);

                                DisplayMessageEventArgs args = new DisplayMessageEventArgs(
                                    String.Format("{0}\n\nWould you like to attempt to continue?", errorMessage),
                                    "Widget Destroy Error",
                                    false);
                                if (OnDisplayMessage(args) && !args.ResultYesOrOK)
                                {
                                    // Throw an exception so that the main handler can catch it.
                                    // Kinda nasty...
                                    throw new Exception(errorMessage);
                                }
                            }
                            sm_MakingVisibleMutex.ReleaseMutex();
                        }
                        catch (System.Exception e)
                        {
                            DisplayMessageEventArgs args = new DisplayMessageEventArgs(
                                String.Format("{0}\n\nWould you like to attempt to continue?", e.ToString()),
                                "Widget Destroy Error", false);

                            if (OnDisplayMessage(args) && !args.ResultYesOrOK)
                            {
                                // rethrow the exception so the main exception handler can catch it
                                throw e;
                            }
                        }
                    }
                    else
                    {
                        HandlerDel handler;

                        // execute handler for command:
                        if (_widgetTypes.TryGetValue(p.Guid, out handler))
                        {
                            if (!resumeLayoutNeeded && (sm_beginBankUpdateHandler != null)
                                && (p.Guid != Widget.ComputeGUID('b', 'm', 'g', 'r')))
                            {
                                sm_beginBankUpdateHandler();
                                resumeLayoutNeeded = true;
                            }

                            try
                            {
                                String errorMessage;
                                if (!handler(p, out errorMessage))
                                {
                                    _log.Error(errorMessage);

                                    if ((p.BankPipe.LastWidgetRetrieved != null) && p.BankPipe.LastWidgetRetrieved.Enabled)
                                    {
                                        DisplayMessageEventArgs args = new DisplayMessageEventArgs(
                                            String.Format("There was an error with the Widget {0}:\n\n{1}\nWould you like to disable the widget?",
                                                p.BankPipe.LastWidgetRetrieved.Path, errorMessage),
                                                "Widget Error", false);
                                        OnDisplayMessage(args);

                                        if (args.ResultYesOrOK)
                                        {
                                            p.BankPipe.LastWidgetRetrieved.Enabled = false;

                                            if (!sm_disabledWidgets.Contains(p.BankPipe.LastWidgetRetrieved))
                                            {
                                                sm_disabledWidgets.Add(p.BankPipe.LastWidgetRetrieved);
                                            }
                                        }
                                    }
                                }
                                
                                // Logging
                                if (_logWidgetCreateDestroy)
                                {
                                    if (p.Command == (ushort)(BankRemotePacket.EnumPacketType.CREATE))
                                    {
                                        Widget widget = p.BankPipe.LastWidgetAssociated;
                                        if (widget != null)
                                        {
                                            _widgetLog.DebugCtx("CREATE", widget.Path);
                                        }
                                    }
                                }
                            }
                            catch (Exception e)
                            {
                                if ((p.BankPipe.LastWidgetRetrieved != null) && p.BankPipe.LastWidgetRetrieved.Enabled)
                                {
                                    DisplayMessageEventArgs args = new DisplayMessageEventArgs(
                                        String.Format("There was an error with the Widget {0}:\n\n{1}\nWould you like to disable the widget?",
                                            p.BankPipe.LastWidgetRetrieved.Path, e.ToString()),
                                            "Widget Error", false);
                                    OnDisplayMessage(args);

                                    if (args.ResultYesOrOK)
                                    {
                                        p.BankPipe.LastWidgetRetrieved.Enabled = false;

                                        if (!sm_disabledWidgets.Contains(p.BankPipe.LastWidgetRetrieved))
                                        {
                                            sm_disabledWidgets.Add(p.BankPipe.LastWidgetRetrieved);
                                        }
                                    }
                                }
                                else
                                {
                                    _log.ToolException(e, "Caught exception");
                                }
                            }
                        }
                    }
                }

                if (resumeLayoutNeeded && (sm_endBankUpdateHandler != null))
                {
                    sm_endBankUpdateHandler();
                }
            }
        }

        // Call on widget server
        public void ResetTypes() 
        {
            _widgetTypes.Clear();
        }

        public void AddType( int guid, HandlerDel handler )
        {
            // make sure this is initialized when needed:
            if ( _widgetTypes == null )
                _widgetTypes = new Dictionary<int, HandlerDel>();

            if ( _widgetTypes.ContainsKey( guid ) == false )
                _widgetTypes.Add( guid, handler );
            else
            {
                char a, b, c, d;
                Widget.ReverseGUID( guid, out a, out b, out c, out d );
                Debug.Fail( "duplicate handlers for guid " + a + b + c + d + "!" );
            }
        }

        public void RemoveType( int guid )
        {
            if ( _widgetTypes != null )
            {
                _widgetTypes.Remove( guid );
            }
        }
        #endregion

        #region Private Functions
        /// <summary>
        /// 
        /// </summary>
        /// <param name="pipe"></param>
        /// <returns></returns>
        private CompressionFactory GetOrCreateCompressionFactory(INamedPipe pipe)
        {
            CompressionFactory factory;
            if (!_compressionFactory.TryGetValue(pipe, out factory))
            {
                factory = new CompressionFactory();
                factory.RegisterCompressionData(new fiCompressionData());
                factory.RegisterCompressionData(new datCompressionData());
                factory.RegisterCompressionData(new noCompressionData());
                _compressionFactory[pipe] = factory;
            }

            return factory;
        }

        /// <summary>
        /// Raises the <see cref="DisplayMessage"/> event.
        /// </summary>
        /// <param name="e">A <see cref="DisplayMessageEventArgs"/> that contains the event data.</param>
        protected bool OnDisplayMessage(DisplayMessageEventArgs e)
        {
            if (DisplayMessage != null)
            {
                DisplayMessage(this, e);
                return true;
            }

            return false;
        }

        protected void OnDataReceived(byte[] data, int count)
        {
            if (DataReceieved != null)
            {
                DataReceieved(this, new DataPacketEventArgs(data, count));
            }
        }
        #endregion
    }

}
