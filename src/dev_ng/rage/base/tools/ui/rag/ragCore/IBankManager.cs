using System;
using System.Collections.Generic;

namespace ragCore
{
	/// <summary>
	/// Summary description for IBankManager.
	/// </summary>
	
	public delegate void InitFinishedHandler();

	public interface IBankManager
	{
		List<Widget> AllBanks {get;}

		List<Widget> FindWidgetsFromPath( string path );
		Widget FindFirstWidgetFromPath(string path);
		Widget FindWidgetFromID( uint id );

        void SendDelayedPing(EventHandler callback, BankPipe pipe);
        void SendPing(EventHandler callback, BankPipe pipe);
        void Reset();

		event InitFinishedHandler InitFinished;
	}


	public class Args
	{
		public static string[] SplitArgs(string fullArgString)
		{
            if ( fullArgString == null )
            {
                return null;
            }

            List<string> args = new List<string>();

			string[] argsSplit=fullArgString.Split();
            if ( argsSplit == null )
            {
                return null;
            }

            for ( int i = 0; i < argsSplit.Length; i++ )
            {
                if ( argsSplit[i].StartsWith( "-" ) )
                {
                    if ( argsSplit[i].IndexOf( "=" ) >= 0 )
                    {
                        args.Add( argsSplit[i] );
                    }
                    else
                    {
                        string argString = argsSplit[i];
                        // copy additional arguments:
                        while ( i < argsSplit.Length - 1 && !argsSplit[i + 1].StartsWith( "-" ) )
                        {
                            i++;
                            argString += " " + argsSplit[i];
                        }
                        args.Add( argString );
                    }
                }
            }

            // create final array:
            string[] result = new string[args.Count];
            args.CopyTo( result );
            return result;
		}

		public static bool CheckArg(string arg,string argKey,ref string argValue)
		{
			arg=arg.ToLower();
			argKey=argKey.ToLower();
			
            if (arg.StartsWith(argKey))
			{
				try 
				{
					argValue=arg.Substring(argKey.Length+1);
					return true;
				}
				catch
				{
					argValue=null;
				}
			}

			return false;
		}
	}
}
