using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using RSG.Base.Logging;


namespace ragCore
{
	/// <summary>
	/// ISensor is used to derive platform specific status checking to determine if an application has quit.
	/// </summary>
	public class Sensor : IDisposable
	{
        #region Delegates
        public delegate Sensor CreateSensorDel();
        public delegate void OutputMessageDel( string message );
        public delegate void MessageEventHandler( object sender, MessageEventArgs e );
        #endregion

        public static event MessageEventHandler OnOutputMessage;


		#region Properties
		public virtual bool HasStopped 
		{
			get 
			{
				return false;
			}
		}

		public virtual bool HasQuit 
		{
			get
			{
				return false;
			}
		}

		public static Dictionary<string,Sensor.CreateSensorDel> Map
		{
			get
			{
				return sm_Map;
			}
		}

		public virtual string Platform 
		{
			get
			{
				return "PC"; 
			}
		}

        public virtual bool CanTakeScreenShot
        {
            get
            {
                return false;
            }
        }
        #endregion

        #region Static Properties
        public static Sensor CurrentSensor
		{
			get
			{
				return sm_CurrentSensor;
			}

			set 
			{
				sm_CurrentSensor=value;
			}
		}

		public static OutputMessageDel OutputMessageHandler
		{
			set
			{
				sm_OutputMessageHandler=value;
			}
		}

        /// <summary>
        /// Gets or sets the IP Address of the Main Application that this instance of Rag is connected to.
        /// </summary>
        public static string MainApplicationIpAddress
        {
            get
            {
                return sm_mainAppIpAddress;
            }
            set
            {
                sm_mainAppIpAddress = value;
            }
        }

        /// <summary>
        /// Gets or sets the IP Address of the PS3 Target that this instance of Rag is connected to. 
        /// The XDevkit API is able to retrieve this, but PS3TM API is not so it must be specified by the game.
        /// </summary>
        public static string PS3TargetIpAddress
        {
            get
            {
                return sm_ps3TargetIpAddress;
            }
            set
            {
                sm_ps3TargetIpAddress = value;
            }
        }

        public static Mutex sm_Mutex = new Mutex();
		#endregion

        #region Public Functions
        public virtual TcpClient GetDebugIpAddress(int port)
		{

            return new TcpClient( MainApplicationIpAddress, port );
		}

		public virtual void Quit()
		{
        }

        public virtual bool SaveScreenShot( string filename )
        {
            return false;
        }
        #endregion

        #region IDisposable interface
        public virtual void Dispose()
        {

        }
        #endregion

        #region Static Functions
        public static void CreateSensor( String platformName, ILog log )
		{            
            if ( (sm_CurrentSensor != null) && (sm_CurrentSensor.Platform == platformName) )
            {
                // don't create the same sensor more than once
                return;
            }

            Sensor.CreateSensorDel sensorCb;
            if ( sm_Map.TryGetValue( platformName, out sensorCb ) )
            {
                sm_CurrentSensor = sensorCb();
            }
            else
            {
                sm_CurrentSensor = new Sensor();
                log.Warning("sensor for platform '{0}' not found.", platformName);
            }
		}

        public static void ClearSensor()
        {
            sm_CurrentSensor = null;
        }

        public static void OutputMessage( string msg )
        {
            if ( OnOutputMessage != null )
            {
                OnOutputMessage( sm_CurrentSensor, new MessageEventArgs( msg ) );
            }
        }

		public static void EmptyOutputMessageHandler()
		{
			sm_Mutex.WaitOne();
			sm_OutputMessageHandler=null;
			sm_Mutex.ReleaseMutex();
        }
        #endregion

        #region Static Variables
        private static Sensor			sm_CurrentSensor=new Sensor();
        private static Dictionary<string, Sensor.CreateSensorDel> sm_Map = new Dictionary<string, Sensor.CreateSensorDel>();
		private static OutputMessageDel sm_OutputMessageHandler;
        private static string sm_mainAppIpAddress;
        private static string sm_ps3TargetIpAddress;
		#endregion
	}



    public class MessageEventArgs : EventArgs
    {
        public MessageEventArgs( string msg )
        {
            m_Msg = msg;
        }

        #region Variables
        private string m_Msg;
        #endregion

        #region Properties
        public string Message
        {
            get
            {
                return m_Msg;
            }
        }
        #endregion
    };
}
