using System;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;

namespace ragCore
{
	/// <summary>
	/// Summary description for DllInfo.
	/// </summary>
	
	[XmlRoot(ElementName = "DllInfo")]
	public class DllInfo
	{
		public DllInfo()
		{
			//
			// TODO: Add constructor logic here
			//
		}

        [XmlType(TypeName="Item")]
		public class NameValuePair
		{            
            [XmlAttribute]
			public string Name, Value;
		}

		[XmlAttribute]
		public string Name;

		[XmlAttribute]
		public bool ShouldLoad;

		[XmlIgnore]
		public bool Loaded;

		[XmlIgnore]
		public string FullName;

		public NameValuePair[] PluginData
		{
			get 
            {
				NameValuePair[] infos = new NameValuePair[m_PluginDataHash.Count];
				int i=0;
                foreach ( KeyValuePair<String,String> pair in m_PluginDataHash )
                {
                    infos[i] = new NameValuePair();
                    infos[i].Name = pair.Key;
                    infos[i].Value = pair.Value;
                    i++;
                }
				return infos;
			}
			set 
            {
				m_PluginDataHash.Clear();
				foreach(NameValuePair d in value) {
					m_PluginDataHash.Add(d.Name, d.Value);
				}
			}
		}

		public override string ToString()
		{
			if (FullName != null) 
			{
				return FullName;
			}
			return Name;
		}

		[XmlIgnore]
        private Dictionary<string, string> m_PluginDataHash = new Dictionary<string, string>();

        [XmlIgnore]
        public Dictionary<string,string> PluginDataHash
        {
            get
            {
                return m_PluginDataHash;
            }
        }

        [XmlIgnore]
        private Dictionary<string, string> m_nameToGuidHash = new Dictionary<string, string>();

        [XmlIgnore]
        private Dictionary<string, string> m_guidToNameHash = new Dictionary<string, string>();        

        [XmlIgnore]
        public Dictionary<string,string> NameToGuidHash
        {
            get
            {
                return m_nameToGuidHash;
            }
        }
        
        [XmlIgnore]
        public Dictionary<string, string> GuidToNameHash
        {
            get
            {
                return m_guidToNameHash;
            }
        }
    }
}
