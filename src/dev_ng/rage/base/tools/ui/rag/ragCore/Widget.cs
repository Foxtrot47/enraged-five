using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Xml;
using System.Drawing;

namespace ragCore
{
    /// <summary>
    /// Summary description for Class1.
    /// </summary>
    public abstract class Widget
    {
        public Widget( BankPipe pipe, uint id, String title, String memo, Color fillColor, bool readOnly )
        {
            m_id = id;
            m_NamedPipe = pipe;
            m_Title = title;
            m_Memo = memo;
            m_readOnly = readOnly;

            m_fillColor = fillColor;
        }

        #region Enums
        private enum ColorFormat
        {
            NamedColor,
            ARGBColor
        }
        #endregion

        #region Abstractions
        public abstract void UpdateRemote();
        public abstract int GetWidgetTypeGUID();
        public abstract bool AllowPersist { get; }
        #endregion

        #region Event
        public event EventHandler DestroyEvent;

        public static event WidgetEventHandler WidgetAdded;
        public static event WidgetEventHandler WidgetRemoved;
        public event WidgetEventHandler WidgetAddedInstance;
        public event WidgetEventHandler WidgetRemovedInstance;

        public event EventHandler ValueChanged;

        public event EventHandler WidgetFillColorChanged;
        public event EventHandler IsReadOnlyChanged;

        public event EventHandler EnabledChanged;

        public event EventHandler PersistChanged;
        #endregion;

        #region Private Variables
        private bool m_Persist = false;
        private string m_PersistKey = "";
        private bool m_isDestroyed = false;
        #endregion

        #region Protected Variables
        protected uint m_id;
        protected String m_Title;
        protected String m_Memo;
        protected bool m_readOnly;
        protected bool m_enabled = true;
        protected BankPipe m_NamedPipe;
        protected Widget m_Parent;
        protected Color m_fillColor = Color.Empty;

        #endregion

        #region Properties
        public uint Id
        {
            get { return m_id; }
        }

        public string Path
        {
            get
            {
                if ( Parent != null )
                    return Parent.Path + @"\" + Title;
                else
                    return Title;
            }
        }

        public Widget Parent
        {
            get
            {
                return m_Parent;
            }

            set
            {
                m_Parent = value;
            }
        }

        public bool Persist
        {
            get
            {
                return m_Persist;
            }

            set
            {
                m_Persist = value;

                if ( PersistChanged != null )
                {
                    PersistChanged( this, System.EventArgs.Empty );
                }
            }
        }

        public string PersistKey
        {
            get
            {
                return m_PersistKey;
            }

            set
            {
                m_PersistKey = value;
            }
        }

        public virtual String HelpTitle
        {
            get { return m_Title; }
        }

        public String Title
        {
            get { return m_Title; }
        }

        public virtual String CategoryDescription
        {
            get { return "Widget"; }
        }

        public String Memo
        {
            get { return m_Memo; }
        }

        public BankPipe Pipe
        {
            get { return m_NamedPipe; }
            set { m_NamedPipe = value; }
        }

        public bool Enabled
        {
            get
            {
                return m_enabled;
            }
            set
            {
                m_enabled = value;

                if ( EnabledChanged != null )
                {
                    EnabledChanged( this, System.EventArgs.Empty );
                }
            }
        }

        public bool IsDestroyed
        {
            get
            {
                return m_isDestroyed;
            }
        }


        public virtual Color FillColor
        {
            get
            {
                return m_fillColor;
            }
            set
            {
                m_fillColor = value;

                if ( WidgetFillColorChanged != null )
                {
                    WidgetFillColorChanged( this, System.EventArgs.Empty );
                }
            }
        }

        /// <summary>
        /// Flag indicating whether this widget can be edited.
        /// </summary>
        public bool IsReadOnly
        {
            get { return m_readOnly; }
            set
            {
                if (m_readOnly != value)
                {
                    m_readOnly = value;
                    if (IsReadOnlyChanged != null)
                    {
                        IsReadOnlyChanged(this, EventArgs.Empty);
                    }
                }
            }
        }
        #endregion

        #region Public Virtual Functions
        public virtual void Destroy()
        {
            uint remote = m_NamedPipe.LocalToRemote( this );
            m_NamedPipe.RemoveRemote( remote );
            m_NamedPipe.RemoveLocal( this );

            if ( Parent != null )
                Parent.RemoveChild( this );

            m_isDestroyed = true;

            if ( DestroyEvent != null )
            {
                DestroyEvent( this, System.EventArgs.Empty );
            }
        }

        // Not really correct. Once FindFirstWidgetFromPath can handle getting the Nth widget
        // of a certain name, FindPath should check for similarly named siblings and
        // return a path that specifies the correct widget.
        public virtual string FindPath()
        {
            string path = "";
            if ( m_Parent != null )
            {
                path = m_Parent.FindPath() + "/";
            }
            return path + m_Title;
        }

        public virtual void Serialize( XmlTextWriter writer, string path )
        {
        }

        public virtual void DeSerialize( XmlTextReader reader )
        {
        }

        // Returns false if it isn't serializable
        public virtual bool SerializeToJSON( LitJson.JsonData data )
        {
            return false;
        }

        public virtual void DeSerializeFromJSON( LitJson.JsonData data )
        {
        }

        public virtual Widget ComparePath( String[] path, ref int index )
        {
            if ( index < path.Length && path[index] == Title )
                return this;
            else
                return null;
        }

        public virtual List<Widget> FindWidgetsFromPath( String[] path, int index )
        {
            if ( path.Length == index + 1 )
            {
                List<Widget> list = new List<Widget>();
                list.Add( this );
                return list;
            }
            return null;
        }


        public virtual void FindWidgetWithMatchingString( bool includeGroup,
                                                         bool matchCase,
                                                         bool matchWholeWord,
                                                         bool exactMatch,
                                                         bool checkHelpDesc,
                                                         String text,
                                                         ref List<Widget> list,
                                                         int depthToSearch )
        {
            if ( !MatchStringWithWidgetText( text, Title, matchCase, matchWholeWord, exactMatch ) )
            {
                if ( !checkHelpDesc || !MatchStringWithWidgetText( text, Memo, matchCase, matchWholeWord, exactMatch ) )
                    return;
            }

            // passed all tests, add to our list:
            list.Add( this );
        }

        public virtual bool IsBank()
        {
            return false;
        }

        public virtual bool IsGroup()
        {
            return false;
        }

        public virtual void AddChild( Widget widget )
        {
            OnWidgetAdded( widget );
        }

        public virtual void RemoveChild( Widget widget )
        {
            OnWidgetRemoved( widget );
        }

        public virtual string ProcessCommand( string[] args )
        {
            return "Nothing to do for widget '" + m_Title + "'";
        }

        public virtual void ReceiveData(byte[] bytes)
        {
            //Nothing to do.
            return;
        }

        public virtual void SendData(Stream stream)
        {
            //Nothing to do.
            return;
        }

        public virtual bool IsModified()
        {
            return false;
        }

        #endregion

        #region Public Functions
        public bool SerializeStart( XmlTextWriter writer, ref String path )
        {
            path += @"\" + Title;

            if ( !Persist )
                return false;

            writer.WriteStartElement( "Widget" );
            writer.WriteAttributeString( "Type", GetType().ToString() );
            writer.WriteAttributeString( "Path", path );

            return true;
        }


        #endregion

        #region Protected Functions
        protected void DeSerializeShared()
        {
            UpdateRemote();
            Persist = true;
        }

        protected bool MatchStringWithWidgetText( string text, string widgetText, bool matchCase, bool matchWholeWord, bool exactMatch )
        {
            if ( widgetText == null )
            {
                return false;
            }

            if ( exactMatch )
            {
                if ( matchCase )
                    return (text == widgetText);
                else
                    return (text.ToLower() == widgetText.ToLower());
            }

            int foundAtIndex = -1;
            if ( !matchCase )
            {
                text = text.ToLower();
                foundAtIndex = widgetText.ToLower().IndexOf( text );
            }
            else
            {
                foundAtIndex = widgetText.IndexOf( text );
            }

            if ( (foundAtIndex >= 0) && matchWholeWord )
            {
                // Check character before and after the substring.
                // If either one is an alpha-numeric character, then don't count this as a match.

                int indexBefore = foundAtIndex - 1;
                int indexAfter = foundAtIndex + text.Length;

                if ( ((indexBefore >= 0)
                    && ((char.IsLetterOrDigit( widgetText, indexBefore ) || char.IsSymbol( widgetText, indexBefore )) || (widgetText[indexBefore] == '_')))
                    || ((indexAfter < widgetText.Length)
                    && ((char.IsLetterOrDigit( widgetText, indexAfter ) || char.IsSymbol( widgetText, indexAfter )) || (widgetText[indexAfter] == '_'))) )
                {
                    foundAtIndex = -1;
                }
            }

            return foundAtIndex >= 0;
        }

        #endregion


        #region Overrides
        public override string ToString()
        {
            return Title;
        }
        #endregion

        #region Public Static Functions
        public static int ComputeGUID( char a, char b, char c, char d )
        {
            return (int)(Convert.ToByte( a ) | (Convert.ToByte( b ) << 8) | (Convert.ToByte( c ) << 16) | (Convert.ToByte( d ) << 24));
        }

        public static void ReverseGUID( int guid, out char a, out char b, out char c, out char d )
        {
            a = Convert.ToChar( guid & 0xff );
            b = Convert.ToChar( (guid >> 8) & 0xff );
            c = Convert.ToChar( (guid >> 16) & 0xff );
            d = Convert.ToChar( (guid >> 24) & 0xff );
        }

        public static bool RemoteHandlerBase(BankRemotePacket packet, int guid, out String errorMessage)
        {
            bool handled = false;

            if ( packet.BankCommand == BankRemotePacket.EnumPacketType.FILL_COLOR )
            {
                packet.Begin();
                Color fillColor = Widget.DeserializeColor(packet.Read_const_char());
                packet.End();

                Widget widget;
                if (packet.BankPipe.RemoteToLocal(packet.Id, out widget, out errorMessage))
                {
                    widget.FillColor = fillColor;
                    handled = true;
                }
            }
            else if (packet.BankCommand == BankRemotePacket.EnumPacketType.READ_ONLY)
            {
                packet.Begin();
                bool readOnly = packet.Read_bool();
                packet.End();

                Widget widget;
                if (packet.BankPipe.RemoteToLocal(packet.Id, out widget, out errorMessage))
                {
                    widget.IsReadOnly = readOnly;
                    handled = true;
                }
            }
            else
            {
                char a, b, c, d;
                ReverseGUID(guid, out a, out b, out c, out d);
                errorMessage = String.Format("Unsupported bank command '{0}' for widget type '{1}{2}{3}{4}'.", packet.BankCommand, a, b, c, d);
            }

            return handled;
        }

        public static bool DestroyHandler( BankRemotePacket packet, BankRemotePacketProcessor packetProcessor, out String errorMessage)
        {
            // Destroy is already virtual for all widgets
            packet.Begin();
            packet.End();

            Widget local;
            if (packet.BankPipe.RemoteToLocal(packet.Id, out local, out errorMessage))
            {
                packetProcessor.DisabledWidgets.Remove( local );
                local.Destroy();
                return true;
            }

            return false;
        }


        public static string SerializeColor( Color color )
        {
            if ( color.IsNamedColor )
            {
                return string.Format( "{0}:{1}", ColorFormat.NamedColor, color.Name );
            }
            else
            {
                return string.Format( "{0}:{1}:{2}:{3}:{4}", ColorFormat.ARGBColor, color.A, color.R, color.G, color.B );
            }
        }

        public static Color DeserializeColor( string color )
        {
            if ( String.IsNullOrEmpty( color ) )
            {
                return Color.Empty;
            }

            byte a, r, g, b;

            string[] pieces = color.Split( new char[] { ':' } );
            if ( pieces.Length > 0 )
            {
                ColorFormat colorType = (ColorFormat)Enum.Parse( typeof( ColorFormat ), pieces[0], true );

                switch ( colorType )
                {
                    case ColorFormat.NamedColor:
                        if ( pieces.Length >= 2 )
                        {
                            return Color.FromName( pieces[1] );
                        }
                        break;

                    case ColorFormat.ARGBColor:
                        if ( pieces.Length >= 5 )
                        {
                            a = byte.Parse( pieces[1] );
                            r = byte.Parse( pieces[2] );
                            g = byte.Parse( pieces[3] );
                            b = byte.Parse( pieces[4] );

                            if ( (a == 0) && (r == 0) && (g == 0) && (b == 0) )
                            {
                                return Color.Empty;
                            }
                            else
                            {
                                return Color.FromArgb( a, r, g, b );
                            }
                        }
                        break;
                }
            }

            return Color.Empty;
        }
        #endregion

        #region Event Dispatchers
        private void OnWidgetAdded( Widget widget )
        {
            if ( Widget.WidgetAdded != null )
            {
                Widget.WidgetAdded( this, new WidgetEventArgs( widget ) );
            }
            if ( WidgetAddedInstance != null )
            {
                WidgetAddedInstance( this, new WidgetEventArgs( widget ) );
            }
        }

        private void OnWidgetRemoved( Widget widget )
        {
            if ( Widget.WidgetRemoved != null )
            {
                Widget.WidgetRemoved( this, new WidgetEventArgs( widget ) );
            }
            if ( WidgetRemovedInstance != null )
            {
                WidgetRemovedInstance( this, new WidgetEventArgs( widget ) );
            }
        }
        #endregion

        public virtual void ResetToDefault()
        {
            //throw new NotImplementedException();
        }

        protected virtual void OnValueChanged()
        {
            if ( this.ValueChanged != null )
            {
                this.ValueChanged( this, EventArgs.Empty );
            }
        }
    }


    public class WidgetEventArgs : EventArgs
    {
        public WidgetEventArgs( Widget widget )
        {
            m_widget = widget;
        }

        #region Variables
        private Widget m_widget;
        #endregion

        #region Properties
        public Widget Widget
        {
            get
            {
                return m_widget;
            }
        }
        #endregion
    };


    #region Helper Classes
    public class WidgetResetter
    {
        public WidgetResetter( Widget widget )
        {
            m_Widget = widget;
        }

        public void ResetWidgetHandler( Object obj, System.EventArgs e )
        {
            m_Widget.ResetToDefault();
        }

        private Widget m_Widget;
    }


    #endregion


    public delegate void GenericValueEventHandler( object sender, GenericValueArgs args );
    public delegate void WidgetEventHandler( object sender, WidgetEventArgs e );

    public class GenericValueArgs : EventArgs
    {
        public GenericValueArgs( object value )
        {
            Value = value;
        }

        #region Variables
        public object Value;
        #endregion
    };
}
