using System;
using System.Net;
using System.Net.Sockets;

namespace ragCore
{
	/// <summary>
	/// Summary description for PipeID.
	/// </summary>
	public class PipeID
	{
		public static INamedPipe CreateNamedPipe()
		{
			return new NamedPipeSocket();
		}

        #region Enums
        /// <summary>
        /// This list mirrors bkRemotePacket::EnumSocketOffset in rage/base/src/bank/packet.h
        /// Access additional offsets with SOCKET_OFFSET_USER5+1 through SOCKET_OFFSET_USER5+40.
        /// </summary>
		public enum EnumSocketOffset
		{
			SOCKET_OFFSET_BANK,
			SOCKET_OFFSET_OUTPUT,
			SOCKET_OFFSET_EVENTS,
            SOCKET_OFFSET_PROFILE,
			SOCKET_OFFSET_USER0,
            SOCKET_OFFSET_USER1,
            SOCKET_OFFSET_USER2,
            SOCKET_OFFSET_USER3,
            SOCKET_OFFSET_USER4,
            SOCKET_OFFSET_USER5,
		}
        #endregion // Enums

        #region Properties
        /// <summary>
        /// Name of the pipe.
        /// </summary>
        public String Name { get; set; }

        /// <summary>
        /// Address to connect to.
        /// </summary>
        public IPAddress Address { get; private set; }

        /// <summary>
        /// Port to connect on.
        /// </summary>
        public int Port { get; private set; }

        /// <summary>
        /// Optional listener that this pipe is already using.
        /// </summary>
        public TcpListener Listener { get; private set; }
        #endregion // Properties

        #region Constructor(s)
        public PipeID(int port)
            : this(null, IPAddress.Any, port, null)
        {
        }

        public PipeID(String name, int port)
            : this(name, IPAddress.Any, port, null)
        {
        }

        public PipeID(IPAddress addr, int port)
            : this(null, addr, port, null)
        {
        }

        public PipeID(IPAddress addr, int port, TcpListener listener)
            : this(null, addr, port, listener)
        {
        }

        public PipeID(String name, IPAddress addr, int port)
            : this(name, addr, port, null)
        {
        }

        public PipeID(String name, IPAddress addr, int port, TcpListener listener)
        {
            Name = name;
            Address = addr;
            Port = port;
            Listener = listener;
        }
        #endregion // Constructor(s)

        #region Public Methods
        /// <summary>
        /// Adds a postfix to the pipes name.
        /// </summary>
        /// <param name="str"></param>
        public void AddString(String str)
		{
            if (Name == null)
                Name = str;
			else
                Name += str;
		}
        #endregion // Public Methods

        #region Object Overrides
        /// <summary>
        /// 
        /// </summary>
        public override String ToString()
        {
            if (Name != null)
            {
                return String.Format("{0}@{1}:{2}", Name, Address, Port);
            }
            else
            {
                return String.Format("{0}:{1}", Address, Port);
            }
        }
        #endregion // Object Overrides
    } // PipeID
}
