using System;
using System.Collections.Generic;

using ragCore;
using RSG.Base.Logging;

namespace ragCore
{
    /// <summary>
    /// Summary description for BankPipe.
    /// </summary>
    public class BankPipe : NamedPipeSocket
    {
        #region Constants
        /// <summary>
        /// Invalid widget id.
        /// </summary>
        public const uint INVALID_ID = 0;
        #endregion // Constants

        #region Member Data
        /// <summary>
        /// 
        /// </summary>
        private IDictionary<uint, Widget> _remoteToLocal = new Dictionary<uint, Widget>();

        /// <summary>
        /// 
        /// </summary>
        private IDictionary<Widget, uint> _LocalToRemote = new Dictionary<Widget, uint>();

        /// <summary>
        /// 
        /// </summary>
        private IDictionary<uint, string> _widgetIdHistory = new Dictionary<uint, string>();

        /// <summary>
        /// 
        /// </summary>
        private Widget _lastWidgetAssociated = null;

        /// <summary>
        /// 
        /// </summary>
        private Widget _lastWidgetRetrieved = null;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public BankPipe()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="other"></param>
        public BankPipe(BankPipe other)
        {
            _remoteToLocal = other._remoteToLocal;
            _LocalToRemote = other._LocalToRemote;
            _widgetIdHistory = other._widgetIdHistory;
        }
        #endregion // Constructor(s)

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public Widget LastWidgetAssociated
        {
            get { return _lastWidgetAssociated; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Widget LastWidgetRetrieved
        {
            get { return _lastWidgetRetrieved; }
        }
        #endregion // Properties

        #region Public Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="remote"></param>
        /// <param name="local"></param>
        public void Associate( uint remote, Widget local )
        {
            _remoteToLocal[remote] = local;
            _LocalToRemote[local] = remote;

            _widgetIdHistory[remote] = local.Path;
            _lastWidgetAssociated = local;
        }

        /// <summary>
        /// 
        /// </summary>
        public void DeleteAssociations()
        {
            _remoteToLocal.Clear();
            _LocalToRemote.Clear();
        }

        /// <summary>
        /// Attempts to resolve a widget based on it's ID. 
        /// </summary>
        /// <param name="remoteId"></param>
        /// <param name="local"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public bool RemoteToLocal<T>(uint remoteId, out T local, out String errorMessage) where T : Widget
        {
            bool success = false;
            Widget baseWidget;

            // Check whether we are asking for the invalid id.
            if (remoteId == INVALID_ID)
            {
                errorMessage = String.Format("A packet was received that references the invalid widget ID '{0}'.  " +
                    "This generally indicates that the game has attempted to modify a widget prior to it being created.", INVALID_ID);
                local = null;
            }
            else if (!_remoteToLocal.TryGetValue(remoteId, out baseWidget))
            {
                String path;
                if (_widgetIdHistory.TryGetValue(remoteId, out path))
                {
                    errorMessage = String.Format("A widget with ID {0} does not exist anymore.  It was associated with '{1}'.", remoteId, path);
                }
                else
                {
                    errorMessage = String.Format("A widget with ID {0} does not exist.  It was never associated with a widget.", remoteId);
                }

                local = null;
            }
            else if (!(baseWidget is T))
            {
                errorMessage = String.Format("Widget with ID {0} ('{1}') is of the wrong type.  Expected {2} but widget is actually {3}.",
                    remoteId, baseWidget.Path, typeof(T).Name, baseWidget.GetType().Name);
                local = null;
            }
            else
            {
                local = (T)baseWidget;
                errorMessage = null;
                success = true;
            }

            // Update the last widget that was retrieved and return the success of the operation.
            _lastWidgetRetrieved = local;
            return success;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="local"></param>
        /// <returns></returns>
        public uint LocalToRemote( Widget local )
        {
            uint remote;
            if (_LocalToRemote.TryGetValue(local, out remote))
            {
                return remote;
            }
            else
            {
                LogFactory.ApplicationLog.Error("bkRemotePacket::LocalToRemote - Unknown local widget " + local);
                return INVALID_ID;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="remote"></param>
        public void RemoveRemote( uint remote )
        {
            _remoteToLocal.Remove( remote );
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="local"></param>
        public void RemoveLocal( Widget local )
        {
            _LocalToRemote.Remove( local );
        }
        #endregion // Public Methods
    } // BankPipe
}
