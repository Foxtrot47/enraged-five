using System;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using RSG.Base.Logging;

namespace ragCore
{
    /// <summary>
    /// Summary description for RemotePacket.
    /// </summary>
    public class RemotePacket
    {
        [StructLayout( LayoutKind.Explicit )]
        public struct FloatToInt
        {
            [FieldOffset( 0 )]
            public float f;
            [FieldOffset( 0 )]
            public uint u;
        }

        [StructLayout( LayoutKind.Sequential )]
        protected struct Header
        {
            public ushort m_Length;		// Length, *not* including eight-byte header
            public ushort m_Command;
            public int m_Guid;
            public uint m_Id;

            public string ConvertGuidToString()
            {
                char[] guidString = new char[4];
                guidString[0] = Convert.ToChar( m_Guid & 0x000000ff );
                guidString[1] = Convert.ToChar( (m_Guid & 0x0000ff00) >> 8 );
                guidString[2] = Convert.ToChar( (m_Guid & 0x00ff0000) >> 16 );
                guidString[3] = Convert.ToChar( (m_Guid & 0xff000000) >> 24 );

                return new string( guidString );
            }
        }

        public const int HEADER_SIZE = 12;
        public const int STORAGE_SIZE = 512 - HEADER_SIZE;

        public RemotePacket( INamedPipe pipe )
        {
            m_Pipe = pipe;

            //
            // TODO: Add constructor logic here
            //
            m_Storage = new byte[STORAGE_SIZE];
        }


        protected Header m_Header;

        public ushort Length
        {
            get { return m_Header.m_Length; }
        }

        public ushort Command
        {
            get { return m_Header.m_Command; }
        }

        public int Guid
        {
            get { return m_Header.m_Guid; }
        }

        public uint Id
        {
            get { return m_Header.m_Id; }
        }

        private byte[] m_Storage;
        public byte[] Storage { get { return m_Storage; } }


        public class RemotePacketEventArgs : EventArgs
        {
            public RemotePacketEventArgs( byte[] data )
            {
                Data = data;
            }

            public RemotePacketEventArgs( byte[] data, int guid )
            {
                Data = data;
            }

            #region Variables
            public byte[] Data;
            #endregion
        };

        // these aren't read from the pipe:
        protected int m_Offset;
        protected INamedPipe m_Pipe;

        public INamedPipe Pipe
        {
            get { return m_Pipe; }
        }

        public void Begin()
        {
            m_Offset = 0;
        }

        public void BeginWrite()
        {
            m_Offset = -1;
        }

        void CheckForBufferOverwrite()
        {
            Debug.Assert( m_Header.m_Length <= m_Storage.Length, "m_Header.m_Length <= m_Storage.Length" );
        }

        public void Write_u8( byte value )
        {
            Debug.Assert( m_Offset == -1, "m_Offset == -1" );
            m_Storage[m_Header.m_Length++] = value;
            CheckForBufferOverwrite();
        }

        public void Write_s8( sbyte value ) { Write_u8( (byte)value ); }
        public void Write_bool( bool value ) { Write_u8( (byte)((value == true) ? 1 : 0) ); }

        public void Write_u16( ushort value )
        {
            Debug.Assert( m_Offset == -1, "m_Offset == -1" );
            m_Storage[m_Header.m_Length++] = (byte)(value);
            m_Storage[m_Header.m_Length++] = (byte)(value >> 8);
            CheckForBufferOverwrite();
        }

        public void Write_s16( short value ) { Write_u16( (ushort)value ); }

        public void Write_u32( uint value )
        {
            Debug.Assert( m_Offset == -1, "m_Offset == -1" );
            m_Storage[m_Header.m_Length++] = (byte)(value);
            m_Storage[m_Header.m_Length++] = (byte)(value >> 8);
            m_Storage[m_Header.m_Length++] = (byte)(value >> 16);
            m_Storage[m_Header.m_Length++] = (byte)(value >> 24);
            CheckForBufferOverwrite();
        }

        public void Write_s32( int value ) { Write_u32( (uint)value ); }

        public void Write_u64(ulong value)
        {
            Debug.Assert(m_Offset == -1, "m_Offset == -1");
            m_Storage[m_Header.m_Length++] = (byte)(value);
            m_Storage[m_Header.m_Length++] = (byte)(value >> 8);
            m_Storage[m_Header.m_Length++] = (byte)(value >> 16);
            m_Storage[m_Header.m_Length++] = (byte)(value >> 24);
            m_Storage[m_Header.m_Length++] = (byte)(value >> 32);
            m_Storage[m_Header.m_Length++] = (byte)(value >> 40);
            m_Storage[m_Header.m_Length++] = (byte)(value >> 48);
            m_Storage[m_Header.m_Length++] = (byte)(value >> 56);
            CheckForBufferOverwrite();
        }

        public void Write_s64(long value)
        {
            Write_u64((ulong)value);
        }

        public void Write_float( float value )
        {
            FloatToInt x;
            x.u = 0;
            x.f = value;
            Write_u32( x.u );
        }

        public void Write_const_char( String value )
        {
            Debug.Assert( m_Offset == -1, "m_Offset == -1" );
            if ( value == null )
                m_Storage[m_Header.m_Length++] = 0;
            else
            {
                int sl = value.Length;
                Debug.Assert( sl < 256, "sl < 256" );
                m_Storage[m_Header.m_Length++] = (byte)(sl + 1);
                for ( int i = 0; i < sl; i++ )
                    m_Storage[m_Header.m_Length++] = Convert.ToByte( value[i] );
                m_Storage[m_Header.m_Length++] = 0x0; // null byte
            }
            CheckForBufferOverwrite();
        }

        public byte Read_u8()
        {
            Debug.Assert( m_Offset >= 0, "m_Offset >= 0" );
            return m_Storage[m_Offset++];
        }

        public sbyte Read_s8()
        {
            return (sbyte)Read_u8();
        }

        public bool Read_bool() { return Read_u8() != 0; }
        public ushort Read_u16()
        {
            Debug.Assert( m_Offset >= 0, "m_Offset >= 0" );
            ushort result = m_Storage[m_Offset++];
            return (ushort)(result | (m_Storage[m_Offset++] << 8));
        }
        public short Read_s16() { return (short)Read_u16(); }

        public uint Read_u32()
        {
            Debug.Assert( m_Offset >= 0, "m_Offset >= 0" );
            uint result = m_Storage[m_Offset++];
            result |= (uint)(m_Storage[m_Offset++] << 8);
            result |= (uint)(m_Storage[m_Offset++] << 16);
            return result | (uint)(m_Storage[m_Offset++] << 24);
        }

        public int Read_s32() { return (int)Read_u32(); }

        public ulong Read_u64()
        {
            Debug.Assert(m_Offset >= 0, "m_Offset >= 0");
            ulong result = m_Storage[m_Offset++];
            result |= (ulong)m_Storage[m_Offset++] << 8;
            result |= (ulong)m_Storage[m_Offset++] << 16;
            result |= (ulong)m_Storage[m_Offset++] << 24;
            result |= (ulong)m_Storage[m_Offset++] << 32;
            result |= (ulong)m_Storage[m_Offset++] << 40;
            result |= (ulong)m_Storage[m_Offset++] << 48;
            result |= (ulong)m_Storage[m_Offset++] << 56;
            return result;
        }

        public long Read_s64()
        {
            return (long)Read_u64();
        }

        public float Read_float()
        {
            FloatToInt x;
            x.f = 0.0f;
            x.u = Read_u32();
            return x.f;
        }

        public String Read_const_char()
        {
            Debug.Assert( m_Offset >= 0, "m_Offset >= 0" );
            if ( m_Storage[m_Offset] > 0 )
            {
                int sl = Read_u8();
                char[] array = new char[sl - 1];
                for ( int i = 0; i < sl - 1; i++ )
                    array[i] = Convert.ToChar( Read_u8() );
                Read_u8(); //read null character
                String result = new String( array );
                return result;
            }
            else
            {
                ++m_Offset;
                return null;
            }
        }

        public void SkipRead()
        {
            Begin();
            for ( int i = 0; i < m_Header.m_Length; i++ )
                Read_u8();
            End();
        }

        string[] cmds = new string[] { "CREATE", "DESTROY", "CHANGED", "USER0", "USER1", "USER2", "USER3", "USER4", "USER5", "USER6", "USER7", "USER8", "USER9", "USER10" };

        public void Send()
        {
            // make sure we're connected:
            if ( !m_Pipe.IsValid() )
                return;

#if DEBUG_PACKETS
			char a, b, c, d;
			Widget.ReverseGUID( this.m_Header.m_Guid, out a, out b, out c, out d );
			LogFactory.ApplicationLog.Message( "SEND Command {0} GUID {1}{2}{3}{4}", cmds[this.m_Header.m_Command], a, b, c, d );
#endif
            // write to the stream:
            try
            {
                byte[] data = CopyToBuffer();
                lock (m_Pipe)
                {
                    m_Pipe.WriteData(data, data.Length);
                }
                //RawSerialize( m_Pipe, m_Header );
                //m_Pipe.WriteData( m_Storage, m_Header.m_Length );
            }
            catch
            {
                LogFactory.ApplicationLog.Error( "BankRemotePacket::Send() failed!" );
            }
        }

        public byte[] CopyToBuffer()
        {
            // Serialise the header data
            byte[] headerData = RawSerialize( m_Header );

            // Cache the lengths
            int headerLength = headerData.Length;
            int dataLength = m_Header.m_Length;

            // Create new buffer of the required size and copy the header and data into it
            byte[] buffer = new byte[headerLength + dataLength];

            try
            {
                System.Buffer.BlockCopy(headerData, 0, buffer, 0, headerLength);
                System.Buffer.BlockCopy(m_Storage, 0, buffer, headerLength, dataLength);
            }
            catch (System.Exception ex)
            {
                LogFactory.ApplicationLog.ToolException(ex, "headerLength: {0}, dataLength: {1}, m_storage.Length: {2}", headerLength, dataLength, m_Storage.Length);
                throw ex;
            }
            return buffer;
        }

        public void ReadFromBuffer( byte[] buffer )
        {
            int headerIndex = 0;
            ReceiveHeader( buffer, ref headerIndex );
            System.Buffer.BlockCopy( buffer, headerIndex, m_Storage, 0, buffer.Length - headerIndex);
        }

        public void ReceiveHeader( INamedPipe reader )
        {
            m_Header = (Header)RawDeSerialize(reader, m_Header.GetType());
            if (m_Header.m_Length > STORAGE_SIZE)
            {
                m_Storage = new byte[m_Header.m_Length];
            }
        }

        public void ReceiveHeader( byte[] data, ref int index )
        {
            int rawsize = Marshal.SizeOf( m_Header.GetType() );
            byte[] rawdatas = new byte[rawsize];

            for ( int i = 0; i < rawsize; ++i )
            {
                rawdatas[i] = data[i + index];
            }

            m_Header = (Header)RawDeSerialize(rawdatas, m_Header.GetType());
            if (m_Header.m_Length > STORAGE_SIZE)
            {
                m_Storage = new byte[m_Header.m_Length];
            }

            index += rawsize;
        }

        public void ReceiveStorage( INamedPipe reader )
        {
            int bytesRead = reader.ReadData( m_Storage, m_Header.m_Length, true );
            if ( bytesRead != m_Header.m_Length )
            {
                Debug.WriteLine( "Length mismatch 1!" );
            }
        }

        public void ReceiveStorage( byte[] data, ref int index )
        {
            for ( int i = 0; i < m_Header.m_Length; ++i )
            {
                m_Storage[i] = data[i + index];
            }

            index += m_Header.m_Length;
        }

        public void End()
        {
            if ( m_Offset != m_Header.m_Length )
            {
                Debug.WriteLine( "Length mismatch 2!" );
            }
            Debug.Assert( m_Offset == m_Header.m_Length, "m_Offset == m_Header.m_Length" );
            m_Offset = -1;
        }

        public bool CanRead()
        {
            return (m_Offset < m_Header.m_Length);
        }

        private static void RawSerialize( INamedPipe writer, object anything )
        {
            int rawsize = Marshal.SizeOf( anything );
            IntPtr buffer = Marshal.AllocHGlobal( rawsize );
            Marshal.StructureToPtr( anything, buffer, false );
            byte[] rawdatas = new byte[rawsize];
            Marshal.Copy( buffer, rawdatas, 0, rawsize );
            Marshal.FreeHGlobal( buffer );

            writer.WriteData( rawdatas, rawsize );
        }

        private static byte[] RawSerialize( object anything )
        {
            int rawsize = Marshal.SizeOf( anything );
            IntPtr buffer = Marshal.AllocHGlobal( rawsize );
            Marshal.StructureToPtr( anything, buffer, false );
            byte[] rawdatas = new byte[rawsize];
            Marshal.Copy( buffer, rawdatas, 0, rawsize );
            Marshal.FreeHGlobal( buffer );

            return rawdatas;
        }

        private static object RawDeSerialize( INamedPipe reader, Type anytype )
        {
            int rawsize = Marshal.SizeOf( anytype );
            byte[] rawdatas = new byte[rawsize];

            // read in header:
            reader.ReadData( rawdatas, rawsize, true );

            return RawDeSerialize( rawdatas, anytype );
        }

        private static object RawDeSerialize( byte[] rawdatas, Type anytype )
        {
            int rawsize = Marshal.SizeOf( anytype );
            IntPtr buffer = Marshal.AllocHGlobal( rawsize );
            Marshal.Copy( rawdatas, 0, buffer, rawsize );
            object retobj = Marshal.PtrToStructure( buffer, anytype );
            Marshal.FreeHGlobal( buffer );
            return retobj;
        }
    }


}
