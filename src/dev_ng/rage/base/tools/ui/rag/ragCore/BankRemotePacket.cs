using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading;

using ragCompression;

namespace ragCore
{
    /// <summary>
    /// Summary description for BankRemotePacket.
    /// </summary>
    public class BankRemotePacket : RemotePacket
    {
        public BankRemotePacket( BankPipe pipe )
            : base( pipe )
        {
        }

        #region Enums
        public enum EnumPacketType
        {
            CREATE,
            DESTROY,
            CHANGED,
            FILL_COLOR,
            READ_ONLY,
            USER_0,
            USER_1,
            USER_2,
            USER_3,
            USER_4,
            USER_5,
            USER_6,
            USER_7,
            USER_8,
            USER_9,
            USER_10,
            USER_11,
            USER_12,
            USER_13,
            USER_14,
            USER_15,
            USER_16,
            USER_17,
            USER_18,
            USER_19
        };
        #endregion

        #region Properties
        public EnumPacketType BankCommand
        {
            get
            {
                return (EnumPacketType)m_Header.m_Command;
            }
        }

        public BankPipe BankPipe
        {
            get { return m_Pipe as BankPipe; }
        }

        #endregion



        #region Public Functions
        public void Begin( EnumPacketType command, int guid, uint id )
        {
            m_Header.m_Length = 0;
            m_Header.m_Command = (ushort)command;
            m_Header.m_Guid = guid;
            m_Header.m_Id = id;

            m_Offset = -1;
        }

        public void Write_bkWidget( uint value ) { Write_ptr( value ); }
        public uint Read_bkWidget() { return Read_ptr(); }

        public void Write_ptr( uint value ) { Write_u32( (uint)value ); }
        public uint Read_ptr() { return (uint)Read_u32(); }
        #endregion


    }

}
