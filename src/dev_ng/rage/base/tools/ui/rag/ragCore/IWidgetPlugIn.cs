using System;

namespace ragCore
{
	/// <summary>
	/// Summary description for IWidgetPlugIn.
	/// </summary>
	public interface IWidgetPlugIn
	{
        void AddHandlers( BankRemotePacketProcessor packetProcessor );
        void RemoveHandlers( BankRemotePacketProcessor packetProcessor );
	}
}
