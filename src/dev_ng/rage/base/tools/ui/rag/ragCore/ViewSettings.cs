﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace ragCore
{
    public static class ViewSettings
    {
        private static bool m_ShowModifiedColours = true;
        public static System.Drawing.Color ModificationColor = System.Drawing.Color.Khaki;

        public static bool ShowModifiedColours
        {
            get { return m_ShowModifiedColours; }
            set
            {
                m_ShowModifiedColours = value;
                if ( SettingsChanged != null )
                {
                    SettingsChanged( null, EventArgs.Empty );
                }
            }
        }


        public static event EventHandler SettingsChanged;
    }

}
