﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ragCore
{
    /// <summary>
    /// 
    /// </summary>
    public class DisplayMessageEventArgs : EventArgs
    {
        public DisplayMessageEventArgs( string msg, string caption, bool error, bool okOnly = false )
        {
            m_Msg = msg;
            m_Caption = caption;
            m_Error = error;
            OkOnly = okOnly;
        }
    
        #region Variables
        private string m_Msg;
        private string m_Caption;
        private bool m_Error;
        #endregion
    
        #region Properties
        public string Message
        {
            get
            {
                return m_Msg;
            }
        }
        public string Caption
        {
            get
            {
                return m_Caption;
            }
        }
        public bool Error
        {
            get
            {
                return m_Error;
            }
        }
        public bool OkOnly { get; private set; }
        public bool ResultYesOrOK { get; set; }
        #endregion
    }
}
