﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Net;
using System.ComponentModel;
using RSG.Base.Logging;

namespace ragCore
{
    public class RagUtils
    {
        public delegate void Callback( PipeID pipeId );

        public static int FindAvailableSockets( int basePort,  int socketsToReserve, List<TcpListener> reservedSockets, ILog log )
        {
            log.Message( "Reserving Sockets..." );

            reservedSockets.Clear();
            int i;
            for (i = 0; i < socketsToReserve && (basePort + i) < UInt16.MaxValue; ++i)
            {
                try
                {
                    TcpListener listener = new TcpListener( IPAddress.Loopback, basePort + i );
                    listener.Start();
                    reservedSockets.Add( listener );
                }
                catch (SocketException)
                {
                    log.Message("Could not reserve port {0}", basePort + i);

                    // Wtf.. Does this code make sense?
                    basePort += i + 1;
                    i = -1;
                    reservedSockets.Clear();
                }
            }

            if (i == UInt16.MaxValue)
            {
                throw new ArgumentOutOfRangeException("Unable to reserver port as couldn't reserve a consecutive block of 50 ports.");
            }

            log.Message("Done Reserving Sockets.  Port = {0}", basePort);

            return basePort;
        }

        public static PipeID GetRagPipeSocket( int basePort, int offset, List<TcpListener> reservedSockets )
        {
            if ( offset < reservedSockets.Count )
            {
                reservedSockets[offset].Stop();
                return new PipeID( IPAddress.Loopback, basePort + offset );
            }

            return null;
        }


        public static void ConnectToPipe( PipeID pipeId, INamedPipe pipe, Callback onComplete, Callback onFail, ILog log )
        {
            if ( pipe.IsConnected() )
            {
                log.Message("Already connected to pipe '{0}' on port {1}...", pipeId.ToString(), pipeId.Port);
                return;
            }

            log.Message("Waiting for client to connect to pipe '{0}' on port {1}...", pipeId.ToString(), pipeId.Port);

            BackgroundWorker worker = new BackgroundWorker();
            worker.DoWork += delegate( object s, DoWorkEventArgs args )
                {
                    args.Result = pipe.Create( pipeId, true );
                };
            worker.RunWorkerCompleted += delegate(object s, RunWorkerCompletedEventArgs args)
                {
                    bool result = (bool)args.Result;
                    if ( result )
                    {
                        log.Message("Connected pipe '{0}' to port {1}.", pipeId.ToString(), ((IPEndPoint)pipe.Socket.RemoteEndPoint).Port);
                        onComplete( pipeId );
                    }
                    else
                    {
                        log.Message("Could not connect pipe '{0}'!", pipeId.ToString());
                        onFail( pipeId );
                    } 
                };

            worker.RunWorkerAsync();
        }

        public static void ConnectToPipe( PipeID pipeId, INamedPipe pipe, ILog log)
        {
            if ( pipe.IsConnected() )
            {
                log.Message("Already connected to pipe '{0}' on port {1}...", pipeId.ToString(), pipeId.Port);
                return;
            }

            log.Message("Waiting for client to connect to pipe '{0}' on port {1}...", pipeId.ToString(), pipeId.Port);

            BackgroundWorker worker = new BackgroundWorker();
            worker.DoWork += delegate( object s, DoWorkEventArgs args )
            {
                args.Result = pipe.Create( pipeId, true );
            };
            worker.RunWorkerCompleted += delegate( object s, RunWorkerCompletedEventArgs args )
            {
                bool result = (bool)args.Result;
                if ( result )
                {
                    log.Message("Connected pipe '{0}' to port {1}.", pipeId.ToString(), ((IPEndPoint)pipe.Socket.RemoteEndPoint).Port);
                }
                else
                {
                    log.Message("Could not connect pipe '{0}'!", pipeId.ToString());
                    pipe.Close();
                }
            };

            worker.RunWorkerAsync();
        }

    }
}
