using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using ragWidgets;
using RSG.Base.Logging;

namespace ragDebugUtils
{
	/// <summary>
	/// Summary description for DebugPanel.
	/// </summary>
	public class DebugPanel : ragWidgetPage.WidgetPage
	{
		private System.Windows.Forms.Button GCCollect;
		private System.Windows.Forms.Button GCAndBreakBtn;
		private System.Windows.Forms.Button TotalMemBtn;
		private System.Windows.Forms.Label TotalMemLabel;
        private Button ThrowExceptionBtn;
        private Button FailAssert1Btn;
        private Label label1;
        private Button FailAssert2Btn;
        private Button ForceQuitBtn;
        private Button CrashTheGameBtn;
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public DebugPanel()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			// TODO: Add any initialization after the InitializeComponent call
		}

        public DebugPanel(string appName, ragCore.IBankManager bankMgr, IKeyUpProcessor keyProcessor, TD.SandDock.DockControl dockControl, ILog log)
			: base(appName, bankMgr, keyProcessor, dockControl, log)
		{
			InitializeComponent();
		}

        public static string MyName = "Debug Utils";

        public override string GetMyName()
        {
            return MyName;
        }

        public static ragWidgetPage.WidgetPage CreateView(String appName, ragCore.IBankManager bankMgr, IKeyUpProcessor keyProcessor, TD.SandDock.DockControl dockControl, ILog log)
		{
			sm_Instance = new DebugPanel(appName, bankMgr, keyProcessor, dockControl, log);
			
			return sm_Instance;
		}

        public static void Shutdown()
        {
            if ( sm_Instance != null )
            {
                sm_Instance.ClearView();
                sm_Instance = null;
            }
        }

		protected static DebugPanel sm_Instance = null;

        public static DebugPanel Instance
        {
            get
            {
                return sm_Instance;
            }
        }

        public override string GetTabText()
        {
            return MyName;
        }

        public override bool IsSingleInstance
        {
            get
            {
                return true;
            }
        }

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.GCCollect = new System.Windows.Forms.Button();
            this.GCAndBreakBtn = new System.Windows.Forms.Button();
            this.TotalMemBtn = new System.Windows.Forms.Button();
            this.TotalMemLabel = new System.Windows.Forms.Label();
            this.ThrowExceptionBtn = new System.Windows.Forms.Button();
            this.FailAssert1Btn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.FailAssert2Btn = new System.Windows.Forms.Button();
            this.ForceQuitBtn = new System.Windows.Forms.Button();
            this.CrashTheGameBtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // GCCollect
            // 
            this.GCCollect.Location = new System.Drawing.Point(8, 8);
            this.GCCollect.Name = "GCCollect";
            this.GCCollect.Size = new System.Drawing.Size(80, 23);
            this.GCCollect.TabIndex = 0;
            this.GCCollect.Text = "GC.Collect()";
            this.GCCollect.Click += new System.EventHandler(this.GCCollect_Click);
            // 
            // GCAndBreakBtn
            // 
            this.GCAndBreakBtn.Location = new System.Drawing.Point(8, 40);
            this.GCAndBreakBtn.Name = "GCAndBreakBtn";
            this.GCAndBreakBtn.Size = new System.Drawing.Size(96, 23);
            this.GCAndBreakBtn.TabIndex = 1;
            this.GCAndBreakBtn.Text = "GC And Break";
            this.GCAndBreakBtn.Click += new System.EventHandler(this.GCAndBreakBtn_Click);
            // 
            // TotalMemBtn
            // 
            this.TotalMemBtn.Location = new System.Drawing.Point(208, 67);
            this.TotalMemBtn.Name = "TotalMemBtn";
            this.TotalMemBtn.Size = new System.Drawing.Size(56, 23);
            this.TotalMemBtn.TabIndex = 2;
            this.TotalMemBtn.Text = "Refresh";
            this.TotalMemBtn.Click += new System.EventHandler(this.TotalMemBtn_Click);
            // 
            // TotalMemLabel
            // 
            this.TotalMemLabel.Location = new System.Drawing.Point(8, 72);
            this.TotalMemLabel.Name = "TotalMemLabel";
            this.TotalMemLabel.Size = new System.Drawing.Size(192, 16);
            this.TotalMemLabel.TabIndex = 3;
            this.TotalMemLabel.Text = "Total Memory: ?";
            // 
            // ThrowExceptionBtn
            // 
            this.ThrowExceptionBtn.BackColor = System.Drawing.Color.Red;
            this.ThrowExceptionBtn.Location = new System.Drawing.Point(11, 133);
            this.ThrowExceptionBtn.Name = "ThrowExceptionBtn";
            this.ThrowExceptionBtn.Size = new System.Drawing.Size(96, 23);
            this.ThrowExceptionBtn.TabIndex = 4;
            this.ThrowExceptionBtn.Text = "Throw Exception";
            this.ThrowExceptionBtn.UseVisualStyleBackColor = false;
            this.ThrowExceptionBtn.Click += new System.EventHandler(this.ThrowExceptionBtn_Click);
            // 
            // FailAssert1Btn
            // 
            this.FailAssert1Btn.BackColor = System.Drawing.Color.Red;
            this.FailAssert1Btn.Location = new System.Drawing.Point(11, 162);
            this.FailAssert1Btn.Name = "FailAssert1Btn";
            this.FailAssert1Btn.Size = new System.Drawing.Size(96, 23);
            this.FailAssert1Btn.TabIndex = 5;
            this.FailAssert1Btn.Text = "Fail Assert 1";
            this.FailAssert1Btn.UseVisualStyleBackColor = false;
            this.FailAssert1Btn.Click += new System.EventHandler(this.FailAssert1Btn_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(7, 110);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(140, 20);
            this.label1.TabIndex = 6;
            this.label1.Text = "DO NOT PRESS";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // FailAssert2Btn
            // 
            this.FailAssert2Btn.BackColor = System.Drawing.Color.Red;
            this.FailAssert2Btn.Location = new System.Drawing.Point(11, 191);
            this.FailAssert2Btn.Name = "FailAssert2Btn";
            this.FailAssert2Btn.Size = new System.Drawing.Size(96, 23);
            this.FailAssert2Btn.TabIndex = 7;
            this.FailAssert2Btn.Text = "Fail Assert 2";
            this.FailAssert2Btn.UseVisualStyleBackColor = false;
            this.FailAssert2Btn.Click += new System.EventHandler(this.FailAssert2Btn_Click);
            // 
            // ForceQuitBtn
            // 
            this.ForceQuitBtn.BackColor = System.Drawing.Color.Red;
            this.ForceQuitBtn.Location = new System.Drawing.Point(11, 220);
            this.ForceQuitBtn.Name = "ForceQuitBtn";
            this.ForceQuitBtn.Size = new System.Drawing.Size(96, 23);
            this.ForceQuitBtn.TabIndex = 8;
            this.ForceQuitBtn.Text = "Force Quit";
            this.ForceQuitBtn.UseVisualStyleBackColor = false;
            this.ForceQuitBtn.Click += new System.EventHandler(this.ForceQuitBtn_Click);
            // 
            // CrashTheGameBtn
            // 
            this.CrashTheGameBtn.BackColor = System.Drawing.Color.Red;
            this.CrashTheGameBtn.Location = new System.Drawing.Point(11, 249);
            this.CrashTheGameBtn.Name = "CrashTheGameBtn";
            this.CrashTheGameBtn.Size = new System.Drawing.Size(96, 23);
            this.CrashTheGameBtn.TabIndex = 9;
            this.CrashTheGameBtn.Text = "Crash the Game";
            this.CrashTheGameBtn.UseVisualStyleBackColor = false;
            this.CrashTheGameBtn.Click += new System.EventHandler(this.CrashTheGameBtn_Click);
            // 
            // DebugPanel
            // 
            this.Controls.Add(this.CrashTheGameBtn);
            this.Controls.Add(this.ForceQuitBtn);
            this.Controls.Add(this.FailAssert2Btn);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.FailAssert1Btn);
            this.Controls.Add(this.ThrowExceptionBtn);
            this.Controls.Add(this.TotalMemLabel);
            this.Controls.Add(this.TotalMemBtn);
            this.Controls.Add(this.GCAndBreakBtn);
            this.Controls.Add(this.GCCollect);
            this.Name = "DebugPanel";
            this.Size = new System.Drawing.Size(280, 352);
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion

        public string Label
        {
            get
            {
                return this.label1.Text;
            }
            set
            {
                this.label1.Text = value;
            }
        }

		private void GCCollect_Click(object sender, System.EventArgs e)
		{
			System.GC.Collect();
		}

		private void GCAndBreakBtn_Click(object sender, System.EventArgs e)
		{
			System.GC.Collect();
			if (System.Diagnostics.Debugger.IsAttached)
			{
				System.Diagnostics.Debugger.Break();
			}
		}

		private void TotalMemBtn_Click(object sender, System.EventArgs e)
		{
			TotalMemLabel.Text = String.Format("Total Memory: {0}", System.GC.GetTotalMemory(true));
		}

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void ThrowExceptionBtn_Click(object sender, EventArgs e)
        {
            NotImplementedException ex = new NotImplementedException("What did you think it would do?");
            throw ex;
        }

        private void SendDebugMessage(ragCore.BankRemotePacket.EnumPacketType type)
        {
            ragCore.IBankManager bankMgr = ViewPlugIn.sm_HostData.MainRageApplication.BankMgr;
            WidgetBank bank = bankMgr.AllBanks[0] as WidgetBank;
            // send a bogus message
            ragCore.BankRemotePacket msg = new ragCore.BankRemotePacket(bank.Pipe);

            int guid = ragCore.Widget.ComputeGUID('b', 'm', 'g', 'r');

            msg.Begin(type, guid, 0);
            msg.Send();
        }

        private void FailAssert1Btn_Click(object sender, EventArgs e)
        {

            SendDebugMessage(ragCore.BankRemotePacket.EnumPacketType.USER_10);
        }

        private void FailAssert2Btn_Click(object sender, EventArgs e)
        {

            SendDebugMessage(ragCore.BankRemotePacket.EnumPacketType.USER_11);
        }

        private void ForceQuitBtn_Click(object sender, EventArgs e)
        {

            SendDebugMessage(ragCore.BankRemotePacket.EnumPacketType.USER_12);
        }

        private void CrashTheGameBtn_Click(object sender, EventArgs e)
        {
            SendDebugMessage(ragCore.BankRemotePacket.EnumPacketType.USER_13);
        }

	}
}
