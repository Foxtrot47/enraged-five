using System;
using ragWidgets;
using RSG.Base.Logging;

namespace ragDebugUtils
{
	/// <summary>
	/// Summary description for ViewPlugIn.
	/// </summary>
	public class ViewPlugIn : ragWidgetPage.WidgetPagePlugIn
	{
		public ViewPlugIn()
		{
			//
			// TODO: Add constructor logic here
			//
        }

        #region WidgetPagePlugIn overrides
        public override string Name
        {
            get
            {
                return "Debug Utils";
            }
        }

        public override void InitPlugIn( PlugInHostData hostData )
        {
            base.InitPlugIn( hostData );

            base.m_pageNameToCreateDelMap[DebugPanel.MyName] = new ragWidgetPage.WidgetPage.CreatePageDel( DebugPanel.CreateView );
        }

        public override void RemovePlugIn()
        {
            base.RemovePlugIn();

            DebugPanel.Shutdown();
        }

        public override void ActivatePlugInMenuItem(object sender, EventArgs args)
		{
            if ( DebugPanel.Instance == null || DebugPanel.Instance.DockControl == null)
            {
                base.CreateAndOpenView( base.m_pageNameToCreateDelMap[DebugPanel.MyName], LogFactory.ApplicationLog);
            }
            else if ( !DebugPanel.Instance.DockControl.IsOpen )
            {
                DebugPanel.Instance.DockControl.Open( TD.SandDock.WindowOpenMethod.OnScreenActivate );
            }
		}

		public override bool CheckPluginStatus()
		{
            return true;
		}

		public override bool ShowOnActivation
		{
			get
			{
				return true;
			}
        }
        #endregion
    }
}
