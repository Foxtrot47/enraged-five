using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Xml;

using ragCore;
using EventEditorControls;
using ragWidgets;
using RSG.Base.Logging;


namespace ragEventEditor
{
    public class WidgetEventEditor : Widget
    {
        public WidgetEventEditor(BankPipe pipe, uint id, String title, String memo, Color fillColor, bool readOnly)
            : base(pipe, id, title, memo, fillColor, readOnly)
        {
            // useful for debugging this plug-in:
            // Debugger.Break();
        }

        #region Enums
        protected enum EnumType
        {
            EVENT_EDITOR,
            CLIP_TAG_EDITOR,
        }
        #endregion

        #region Variables
        private EnumType m_type;
        private TimeLineComponent m_timeLineComponent = new TimeLineComponent();
        
        private List<string> m_eventTypeNames = new List<string>();
        private List<TrackInst> m_trackInsts = new List<TrackInst>();

        private Dictionary<uint, EventInst> m_remoteToEventInst = new Dictionary<uint, EventInst>();
        private Dictionary<int, EventInst> m_localToEventInst = new Dictionary<int, EventInst>();

        private bool m_prevWasSelected = false;
        private uint m_prevSelectedObject = 0;

        public event EventHandler PhaseAndDisplayUpdated;
        public event EventHandler DisplayViewUpdated;

        public event GenericValueEventHandler TimeUpdated;
        public event GenericValueEventHandler EventMoved;
        public event GenericValueEventHandler TrackReplaced;
        public event GenericValueEventHandler TrackAdded;
        public event GenericValueEventHandler EventAdded;
        public event GenericValueEventHandler EventRemoved;
        public event GenericValueEventHandler EventTypeAdded;
        public event GenericValueEventHandler EventChanged;

        /// <summary>
        /// Thread synchronisation object.
        /// </summary>
        private static object s_syncObj = new object();

        /// <summary>
        /// Reference count for number of times this object has been initialised.
        /// </summary>
        private static int s_refCount = 0;
        #endregion

        #region Properties
        public List<string> EventTypeNames
        {
            get
            {
                return m_eventTypeNames;
            }
        }

        public override bool AllowPersist
        {
            get
            {
                return false;
            }
        }

        public TimeLineComponent TimeLineComponent
        {
            get { return m_timeLineComponent; }
        }

        internal List<TrackInst> TrackInsts
        {
            get { return m_trackInsts; }
        }

        #endregion

        #region Overrides
        public override int GetWidgetTypeGUID()
        {
            switch ( m_type )
            {
                case EnumType.EVENT_EDITOR:
                    return GetEventEditorGUID();
                case EnumType.CLIP_TAG_EDITOR:
                    return GetClipTagEditorGUID();
            }

            throw new InvalidOperationException( String.Format( "Invalid type {0} for WidgetEventEditor", m_type ) );
        }

        public override void Serialize( XmlTextWriter writer, string path )
        {
        
        }
        
        public override void DeSerialize( XmlTextReader reader )
        {
        
        }


        public static WidgetVisible ShowVisible(Widget widget, IWidgetView widgetView, Control parent, String hashKey, bool forceOpen, ToolTip tooltip)
        {
            WidgetEventEditor widgetEditor = (WidgetEventEditor)widget;
            WidgetEventEditorVisible visible = new WidgetEventEditorVisible( widgetView, widgetEditor, parent, tooltip );

            WidgetVisualiser.WidgetShowVisibleSetup( widgetEditor, visible, hashKey );

            visible.SetPhaseAndDisplayRange( widgetEditor.TimeLineComponent.PhaseMin,
                widgetEditor.TimeLineComponent.PhaseMax,
                widgetEditor.TimeLineComponent.DisplayMin,
                widgetEditor.TimeLineComponent.DisplayMax );
            visible.SetDisplayView( widgetEditor.TimeLineComponent.DisplayMin,
                widgetEditor.TimeLineComponent.DisplayMax );
            visible.SetCurrentTime( widgetEditor.TimeLineComponent.DisplayMin );

            foreach ( string typeName in widgetEditor.EventTypeNames )
            {
                visible.AddEventType( typeName );
            }

            foreach ( TrackInst inst in widgetEditor.m_trackInsts )
            {
                visible.CreateAndAddTrack( inst.Text, inst.TypeName );
            }

            foreach ( KeyValuePair<uint, EventInst> pair in widgetEditor.m_remoteToEventInst )
            {
                EventControl eventCtrl
                    = visible.CreateAndAddEvent( pair.Value.Text, pair.Value.TypeName, pair.Value.MinT, pair.Value.MaxT, pair.Value.TrackIndex );
                if ( eventCtrl != null )
                {
                    eventCtrl.Tag = pair.Key;
                }
            }

            return visible;
        }
        

        public override void UpdateRemote()
        {
        
        }
        #endregion

        #region Public Functions
        /// <summary>
        /// Sends a message to the game to create a new track.
        /// </summary>
        /// <param name="text"></param>
        /// <param name="typeName"></param>
        /// <param name="trackIndex"></param>
        public void CreateTrack( string text, string typeName, int trackIndex )
        {
            // associate an event track with an event type on the remote program:
            BankRemotePacket p = new BankRemotePacket( Pipe );
            p.Begin(BankRemotePacket.EnumPacketType.USER_7, GetWidgetTypeGUID(), Id);
            p.Write_const_char( typeName );
            p.Write_s32( trackIndex );
            p.Send();
        }

        /// <summary>
        /// Sends a message to the game to create a new event.
        /// </summary>
        /// <param name="text"></param>
        /// <param name="typeName"></param>
        /// <param name="minT"></param>
        /// <param name="maxT"></param>
        /// <param name="trackIndex"></param>
        public void CreateEventInst( string text, string typeName, float minT, float maxT, int trackIndex )
        {
            EventInst inst = new EventInst( text, typeName, minT, maxT, trackIndex, 0 );

            m_localToEventInst.Add( inst.GetHashCode(), inst );

            BankRemotePacket p = new BankRemotePacket( Pipe );
            p.Begin(BankRemotePacket.EnumPacketType.USER_0, GetWidgetTypeGUID(), Id);
            p.Write_s32( inst.GetHashCode() ); // write our hash code (game side should send us a local pointer)
            p.Write_float( inst.MinT );
            p.Write_float( inst.MaxT );
            p.Write_s32( inst.TrackIndex );
            p.Send();
        }

        /// <summary>
        /// Sends a message to the game to delete an event.
        /// </summary>
        /// <param name="remote"></param>
        public void DeleteEvent( uint remote )
        {
            BankRemotePacket p = new BankRemotePacket( Pipe );
            p.Begin(BankRemotePacket.EnumPacketType.USER_1, GetWidgetTypeGUID(), Id);
            p.Write_ptr( remote );
            p.Send();
        }

        public void SendCurrentTime(float time)
        {
            SetCurrentTime( time );

            BankRemotePacket p = new BankRemotePacket(Pipe);
            p.Begin(BankRemotePacket.EnumPacketType.USER_3, GetWidgetTypeGUID(), Id);
            p.Write_float(time);
            p.Send();
        }

        /// <summary>
        /// Retrieves the WidgetGroup for the event.
        /// </summary>
        /// <param name="remote"></param>
        /// <returns></returns>
        public WidgetGroup GetRemoteWidgetGroup( uint remote )
        {
            EventInst inst;
            if ( m_remoteToEventInst.TryGetValue( remote, out inst ) )
            {
                WidgetGroup group;
                String errorMessage;
                if (!Pipe.RemoteToLocal(inst.RemoteGroup, out group, out errorMessage))
                {
                    LogFactory.ApplicationLog.MessageCtx("WidgetEventEditor", "GetRemoteWidgetGroup() - {0}", errorMessage);
                }
                return group;
            }

            return null;
        }

        /// <summary>
        /// Sends a message to the game with updated event properties.  Go ahead and update all visibles.
        /// </summary>
        /// <param name="remote"></param>
        /// <param name="minT"></param>
        /// <param name="maxT"></param>
        public void MoveEvent( uint remote, float minT, float maxT )
        {
            EventInst inst;
            if ( m_remoteToEventInst.TryGetValue( remote, out inst ) )
            {
                inst.MinT = minT;
                inst.MaxT = maxT;
                
                if ( EventMoved != null )
                {
                    EventMoved( this, new GenericValueArgs( Tuple.Create( remote, inst ) ) );
                }
            }

            BankRemotePacket p = new BankRemotePacket( Pipe );
            p.Begin(BankRemotePacket.EnumPacketType.USER_6, GetWidgetTypeGUID(), Id);
            p.Write_ptr( remote );
            p.Write_float( minT );
            p.Write_float( maxT );
            p.Send();
        }

        /// <summary>
        /// Sends a message to inform the game that the currently selected item has changed
        /// </summary>
        /// <param name="selected">Was the item selected or deselected</param>
        /// <param name="remote">GUID of the item, if selected. 0 if deselected</param>
        public void SelectionChanged(bool selected, uint remote)
        {
            // try to reduce network traffic
            if (m_prevWasSelected == selected && m_prevSelectedObject == remote)
            {
                return;
            }

            BankRemotePacket p = new BankRemotePacket(Pipe);
            p.Begin(BankRemotePacket.EnumPacketType.USER_9, GetWidgetTypeGUID(), Id);
            p.Write_ptr( selected ? remote : 0 );
            p.Send();

            m_prevSelectedObject = remote;
            m_prevWasSelected = selected;
        }

        /// <summary>
        /// Sends a message to the game requesting the game copies the specified instance to its internal clipboard.
        /// </summary>
        /// <param name="remote"></param>
        public void CopyInstance(uint remote)
        {
            BankRemotePacket p = new BankRemotePacket(Pipe);
            p.Begin(BankRemotePacket.EnumPacketType.USER_10, GetWidgetTypeGUID(), Id);
            p.Write_ptr(remote);
            p.Send();
        }

        /// <summary>
        /// Sends a message to the game, requesting the game pastes the current selected event and offsets it to
        /// the specified time
        /// </summary>
        /// <param name="pasteTime"></param>
        public void PasteInstance(float pasteTime)
        {
            BankRemotePacket p = new BankRemotePacket(Pipe);
            p.Begin(BankRemotePacket.EnumPacketType.USER_11, GetWidgetTypeGUID(), Id);
            p.Write_float(pasteTime);
            p.Send();
        }
        #endregion

        #region Private Functions
        /// <summary>
        /// Associates the EventInst with the remote.
        /// </summary>
        /// <param name="remote"></param>
        /// <param name="inst"></param>
        private void Associate( uint remote, EventInst inst )
        {
            if ( m_remoteToEventInst.ContainsKey( remote ) )
            {
                throw (new Exception( String.Format( "RemoteToLocal already contains remote ID {0}.", remote ) ));
            }

            m_remoteToEventInst.Add( remote, inst );

            if ( !m_localToEventInst.ContainsKey( inst.GetHashCode() ) )
            {
                throw (new Exception( String.Format( "LocalToRemote does not contain EventInst {0}.", inst.GetHashCode() ) ));
            }

            m_localToEventInst[inst.GetHashCode()] = inst;
        }

        /// <summary>
        /// Adds a new track to all visibles.
        /// </summary>
        /// <param name="text"></param>
        /// <param name="typeName"></param>
        /// <param name="trackIndex"></param>
        private void AddTrack( string text, string typeName, int trackIndex )
        {
            TrackInst inst = new TrackInst( text, typeName );

            bool createTrack = true;
            if ( trackIndex < m_trackInsts.Count )
            {
                m_trackInsts[trackIndex] = inst;

                createTrack = false;

                if ( TrackReplaced != null )
                {
                    TrackReplaced( this, new GenericValueArgs( Tuple.Create( trackIndex, inst ) ) );
                }
            }
            else if ( trackIndex > m_trackInsts.Count )
            {
                while ( m_trackInsts.Count < trackIndex - 1 )
                {
                    AddTrack( string.Empty, string.Empty, m_trackInsts.Count );
                }                
            }

            if ( createTrack )
            {
                m_trackInsts.Add( inst );

                if ( TrackAdded != null )
                {
                    TrackAdded( this, new GenericValueArgs( inst ) );
                }
            }
        }

        /// <summary>
        /// Associates the EventInst given by localHash with the remote and remoteGroup.  Adds a new event to all tracks. 
        /// This is called after the user has created a event and the game has returned with the remote and remoteGroup association.
        /// </summary>
        /// <param name="remote"></param>
        /// <param name="localHash"></param>
        /// <param name="remoteGroup"></param>
        private void AddEvent( uint remote, int localHash, uint remoteGroup )
        {
            EventInst inst;
            if ( !m_localToEventInst.TryGetValue( localHash, out inst ) )
            {
                throw (new Exception( "EventInst is null." ));
            }

            inst.RemoteGroup = remoteGroup;

            AddEvent( remote, inst );
        }

        /// <summary>
        /// Adds a new event to all tracks.
        /// </summary>
        /// <param name="remote"></param>
        /// <param name="inst"></param>
        private void AddEvent( uint remote, EventInst inst )
        {
            if ( EventAdded != null )
            {
                EventAdded( this, new GenericValueArgs( Tuple.Create( remote, inst) ) );
            }

            m_localToEventInst[inst.GetHashCode()] = inst;

            Associate( remote, inst );
        }

        /// <summary>
        /// Removes the event from all tracks
        /// </summary>
        /// <param name="remote"></param>
        private void RemoveEvent( uint remote )
        {
            if ( EventRemoved!= null )
            {
                EventRemoved( this, new GenericValueArgs(  remote ) );
            }

            EventInst inst;
            if ( m_remoteToEventInst.TryGetValue( remote, out inst ) )
            {
                m_remoteToEventInst.Remove( remote );
                m_localToEventInst.Remove( inst.GetHashCode() );
            }
        }

        /// <summary>
        /// Sets the current time (scrub bar position) for a all visibles
        /// </summary>
        /// <param name="time"></param>
        private void SetCurrentTime( float time )
        {
            if (TimeUpdated != null)
            {
                TimeUpdated( this, new GenericValueArgs( time ) );
            }
        }

        /// <summary>
        /// Sets the phase and display ranges for all visibles
        /// </summary>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <param name="displayMin"></param>
        /// <param name="displayMax"></param>
        private void SetPhaseAndDisplayRange( float min, float max, float displayMin, float displayMax )
        {
            m_timeLineComponent.SetPhase( min, max );
            m_timeLineComponent.DisplayMin = displayMin;
            m_timeLineComponent.DisplayMax = displayMax;

            if (PhaseAndDisplayUpdated != null)
            {
                PhaseAndDisplayUpdated( this, EventArgs.Empty );
            }
        }

        /// <summary>
        /// Set the current visible area for all visibles
        /// </summary>
        /// <param name="min"></param>
        /// <param name="max"></param>
        private void SetDisplayView( float min, float max )
        {
            m_timeLineComponent.DisplayViewMin = min;
            m_timeLineComponent.DisplayViewMax = max;

            if ( DisplayViewUpdated != null )
            {
                DisplayViewUpdated( this, EventArgs.Empty );
            }
        }

        /// <summary>
        /// Adds a new even type
        /// </summary>
        /// <param name="typeName"></param>
        private void AddEventType( string typeName )
        {
            m_eventTypeNames.Add( typeName );
            
            if ( EventTypeAdded != null )
            {
                EventTypeAdded( this, new GenericValueArgs( typeName ) );
            }
        }

        /// <summary>
        /// Updates the properties of an event
        /// </summary>
        /// <param name="remote"></param>
        /// <param name="text"></param>
        /// <param name="minT"></param>
        /// <param name="maxT"></param>
        private void SetEvent( uint remote, string text, float minT, float maxT )
        {
            EventInst inst;
            if ( m_remoteToEventInst.TryGetValue( remote, out inst ) )
            {
                inst.Text = text;
                inst.MinT = minT;
                inst.MaxT = maxT;

                if ( EventChanged != null )
                {
                    EventChanged( this, new GenericValueArgs( Tuple.Create( remote, inst ) ) );
                }
            }
        }
        #endregion

        #region Public Static Functions
        public static int GetEventEditorGUID()
        {
            return ComputeGUID( 'e', 'v', 'n', 't' );
        }
        
        public static int GetClipTagEditorGUID()
        {
            return ComputeGUID( 'c', 't', 'a', 'g' );
        }

        private static bool RemoteHandler( BankRemotePacket packet, out String errorMessage, int guid, EnumType type )
        {
            // USER_0 == added EventBar
            // USER_1 == removed EventBar
            // USER_2 == new remote pointer (game-to-rag direction only)
            // USER_3 == current playing time
            // USER_4 == max # of frames
            // USER_5 == declare event type
            // USER_6 == event instance moved
            // USER_7 == declare an event track
            // USER_8 == set time window
            // USER_9 == set current instance
            // USER_10 == copy current instance
            // USER_11 == paste instance
            bool handled = false;
           
            switch ( packet.BankCommand )
            {
                case BankRemotePacket.EnumPacketType.CREATE:
                    {
                        packet.Begin();
                        uint remoteParent = packet.Read_bkWidget();
                        String title = packet.Read_const_char();
                        String memo = packet.Read_const_char();
                        Color fillColor = Widget.DeserializeColor(packet.Read_const_char());
                        bool readOnly = packet.Read_bool();
                        packet.End();
                        
                        Widget parent;
                        if (packet.BankPipe.RemoteToLocal(remoteParent, out parent, out errorMessage))
                        {
                            WidgetEventEditor local = new WidgetEventEditor(packet.BankPipe, packet.Id, title, memo, fillColor, readOnly);
                            local.m_type = type;

                            packet.BankPipe.Associate(packet.Id, local);
                            parent.AddChild(local);
                            handled = true;
                        }
                    }
                    break;

                // create and associate a locally created event bar with an already created remote instance:
                case BankRemotePacket.EnumPacketType.USER_0:
                    {
                        packet.Begin();

                        WidgetEventEditor editor;
                        if (packet.BankPipe.RemoteToLocal(packet.Id, out editor, out errorMessage))
                        {
                            // find the local editor instance:
                            uint ptrRemote = packet.Read_ptr();
                            string typeStr = packet.Read_const_char();
                            string labelString = packet.Read_const_char();
                            float minT = packet.Read_float();
                            float maxT = packet.Read_float();
                            int trackNumber = packet.Read_s32();
                            uint remoteGroup = packet.Read_bkWidget();

                            packet.End();

                            editor.AddEvent(packet.Id, new EventInst(labelString, typeStr, minT, maxT, trackNumber, remoteGroup));
                            handled = true;
                        }
                    }
                    break;

                // remove a local event instance that has been already been removed remotely:
                case BankRemotePacket.EnumPacketType.USER_1:
                    {
                        packet.Begin();

                        WidgetEventEditor editor;
                        if (packet.BankPipe.RemoteToLocal(packet.Id, out editor, out errorMessage))
                        {
                            uint ptrRemote = packet.Read_ptr();

                            packet.End();

                            editor.RemoveEvent(packet.Id);
                            handled = true;
                        }
                    }
                    break;

                // associate a locally created event bar with the remote instance:
                case BankRemotePacket.EnumPacketType.USER_2:
                    {
                        packet.Begin();

                        WidgetEventEditor editor;
                        if (packet.BankPipe.RemoteToLocal(packet.Id, out editor, out errorMessage))
                        {
                            uint ptrRemote = packet.Read_ptr();
                            int hashLocal = packet.Read_s32();
                            uint remoteGroup = packet.Read_bkWidget();

                            packet.End();

                            editor.AddEvent(ptrRemote, hashLocal, remoteGroup);
                            handled = true;
                        }
                    }
                    break;

                // setting the current time:
                case BankRemotePacket.EnumPacketType.USER_3:
                    {
                        packet.Begin();

                        WidgetEventEditor editor;
                        if (packet.BankPipe.RemoteToLocal(packet.Id, out editor, out errorMessage))
                        {
                            float time = packet.Read_float();

                            packet.End();

                            editor.SetCurrentTime(time);
                            handled = true;
                        }
                    }
                    break;

                // getting the maximum time of the time line:
                case BankRemotePacket.EnumPacketType.USER_4:
                    {
                        packet.Begin();

                        WidgetEventEditor editor;
                        if (packet.BankPipe.RemoteToLocal(packet.Id, out editor, out errorMessage))
                        {
                            float min = packet.Read_float();
                            float max = packet.Read_float();
                            float displayMin = packet.Read_float();
                            float displayMax = packet.Read_float();

                            packet.End();

                            editor.SetPhaseAndDisplayRange(min, max, displayMin, displayMax);
                            handled = true;
                        }
                    }
                    break;

                // make a new available type:
                case BankRemotePacket.EnumPacketType.USER_5:
                    {
                        packet.Begin();

                        WidgetEventEditor editor;
                        if (packet.BankPipe.RemoteToLocal(packet.Id, out editor, out errorMessage))
                        {
                            string newType = packet.Read_const_char();

                            packet.End();

                            editor.AddEventType(newType);
                            handled = true;
                        }
                    }
                    break;

                // set an event's properties
                case BankRemotePacket.EnumPacketType.USER_6:
                    {
                        packet.Begin();

                        WidgetEventEditor editor;
                        if (packet.BankPipe.RemoteToLocal(packet.Id, out editor, out errorMessage))
                        {
                            uint ptrRemote = packet.Read_ptr();
                            string text = packet.Read_const_char();
                            float minT = packet.Read_float();
                            float maxT = packet.Read_float();

                            packet.End();

                            editor.SetEvent(packet.Id, text, minT, maxT);
                            handled = true;
                        }
                    }
                    break;

                case BankRemotePacket.EnumPacketType.USER_7:
                    {
                        packet.Begin();

                        WidgetEventEditor editor;
                        if (packet.BankPipe.RemoteToLocal(packet.Id, out editor, out errorMessage))
                        {
                            string typeName = packet.Read_const_char();
                            int trackIndex = packet.Read_s32();

                            packet.End();

                            editor.AddTrack(typeName, typeName, trackIndex);
                            handled = true;
                        }
                    }
                    break;

                case BankRemotePacket.EnumPacketType.USER_8:
                    {
                        packet.Begin();

                        WidgetEventEditor editor;
                        if (packet.BankPipe.RemoteToLocal(packet.Id, out editor, out errorMessage))
                        {
                            float min = packet.Read_float();
                            float max = packet.Read_float();

                            packet.End();

                            editor.SetDisplayView(min, max);
                            handled = true;
                        }
                    }
                    break;

                case BankRemotePacket.EnumPacketType.USER_9:
                case BankRemotePacket.EnumPacketType.USER_10:
                case BankRemotePacket.EnumPacketType.USER_11:
                    handled = true;
                    errorMessage = null;
                    break;

                default:
                    handled = Widget.RemoteHandlerBase(packet, guid, out errorMessage);
                    break;
            }

            return handled;
        }

        public static void AddHandlers( BankRemotePacketProcessor packetProcessor )
        {
            packetProcessor.AddType( GetEventEditorGUID(),
                new BankRemotePacketProcessor.HandlerDel(
                    delegate( BankRemotePacket p, out String errorMessage)
                    {
                            return RemoteHandler( p, out errorMessage, GetEventEditorGUID(), EnumType.EVENT_EDITOR );
                    }));
            packetProcessor.AddType( GetClipTagEditorGUID(),
                new BankRemotePacketProcessor.HandlerDel(
                    delegate( BankRemotePacket p, out String errorMessage)
                    {
                            return RemoteHandler( p, out errorMessage, GetClipTagEditorGUID(), EnumType.CLIP_TAG_EDITOR );
                    }));

            lock (s_syncObj)
            {
                // Check whether the script editor is currently set up.
                if (s_refCount == 0)
                {
                    WidgetVisualiser.AddHandlerShowVisible(typeof(WidgetEventEditor), ShowVisible);
                    WidgetVisualiser.AddDefaultHandlerHideVisible(typeof(WidgetEventEditor));
                }

                ++s_refCount;
            }
        }

        public static void RemoveHandlers( BankRemotePacketProcessor packetProcessor )
        {
            packetProcessor.RemoveType( GetEventEditorGUID() );
            packetProcessor.RemoveType( GetClipTagEditorGUID() );

            lock (s_syncObj)
            {
                --s_refCount;

                if (s_refCount == 0)
                {
                    WidgetVisualiser.RemoveHandlers(typeof(WidgetEventEditor));
                }
            }
        }
        #endregion
    }

    sealed class TrackInst
    {
        public TrackInst( string text, string typeName )
        {
            m_text = text;
            m_typeName = typeName;
        }

        #region Variables
        private string m_text;
        private string m_typeName;
        #endregion

        #region Properties
        public string Text
        {
            get
            {
                return m_text;
            }
        }

        public string TypeName
        {
            get
            {
                return m_typeName;
            }
        }
        #endregion
    }

    sealed class EventInst
    {
        public EventInst( string text, string typeName, float minT, float maxT, int trackIndex, uint remoteGroup )
        {
            m_text = text;
            m_typeName = typeName;
            m_minT = minT;
            m_maxT = maxT;
            m_trackIndex = trackIndex;
            m_remoteGroup = remoteGroup;
        }

        #region Variables
        private string m_text;
        private string m_typeName;
        private float m_minT;
        private float m_maxT;
        private float m_midT = -1.0f;
        private int m_trackIndex;
        private uint m_remoteGroup = 0;
        #endregion

        #region Properties
        public string Text
        {
            get
            {
                return m_text;
            }
            set
            {
                m_text = value;
            }
        }

        public string TypeName
        {
            get
            {
                return m_typeName;
            }
            set
            {
                m_typeName = value;
            }
        }

        public float MinT
        {
            get
            {
                return m_minT;
            }
            set
            {
                m_minT = value;
            }
        }

        public float MaxT
        {
            get
            {
                return m_maxT;
            }
            set
            {
                m_maxT = value;
            }
        }

        public float MidT
        {
            get
            {
                return m_midT;
            }
            set
            {
                m_midT = value;
            }
        }
        
        public int TrackIndex
        {
            get
            {
                return m_trackIndex;
            }
            set
            {
                m_trackIndex = value;
            }
        }

        public uint RemoteGroup
        {
            get
            {
                return m_remoteGroup;
            }
            set
            {
                m_remoteGroup = value;
            }
        }
        #endregion
    }
}
