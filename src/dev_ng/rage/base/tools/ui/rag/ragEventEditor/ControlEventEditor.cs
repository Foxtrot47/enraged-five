using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

using EventEditorControls;

namespace ragEventEditor
{
    public partial class ControlEventEditor : UserControl
    {
        public ControlEventEditor()
        {
            InitializeComponent();
        }

        #region Properties
        public float CurrentTime
        {
            get
            {
                return this.eventEditorControl.CurrentTime;
            }
            set
            {
                this.eventEditorControl.CurrentTime = value;
            }
        }

        public float CurrentPhase
        {
            get
            {
                return this.eventEditorControl.CurrentPhase;
            }
            set
            {
                this.eventEditorControl.CurrentPhase = value;
            }
        }

        public List<Control> MouseEventControls
        {
            get
            {
                List<Control> ctrls = new List<Control>();
                ctrls.Add( this.panel1 );
                ctrls.Add( this.label1 );
                return ctrls;
            }
        }

        public List<TrackControl> TrackControls
        {
            get
            {
                return this.eventEditorControl.TrackControls;
            }
        }

        public override string Text
        {
            get
            {
                return base.Text;
            }
            set
            {
                base.Text = value;
                this.label1.Text = value;
            }
        }
        #endregion

        #region Events
        public event TrackCreateEventHandler TrackCreate
        {
            add
            {
                this.eventEditorControl.TrackCreate += value;
            }
            remove
            {
                this.eventEditorControl.TrackCreate -= value;
            }
        }

        public event EventCreateOnTrackEventHandler EventCreate
        {
            add
            {
                this.eventEditorControl.EventCreate += value;
            }
            remove
            {
                this.eventEditorControl.EventCreate -= value;
            }
        }

        public event EventOnTrackWithCancelEventHandler EventDelete
        {
            add
            {
                this.eventEditorControl.EventDelete += value;
            }
            remove
            {
                this.eventEditorControl.EventDelete -= value;
            }
        }

        public event EventOnTrackEventHandler EventEdit
        {
            add
            {
                this.eventEditorControl.EventEdit += value;
            }
            remove
            {
                this.eventEditorControl.EventEdit -= value;
            }
        }

        public event EventOnTrackEventHandler EventMoved
        {
            add
            {
                this.eventEditorControl.EventMoved += value;
            }
            remove
            {
                this.eventEditorControl.EventMoved -= value;
            }
        }

        public event EventOnTrackEventHandler EventMoving
        {
            add
            {
                this.eventEditorControl.EventMoving += value;
            }
            remove
            {
                this.eventEditorControl.EventMoving -= value;
            }
        }

        public event EventOnTrackEventHandler SelectionChanged
        {
            add
            {
                this.eventEditorControl.SelectionChanged += value;
            }
            remove
            {
                this.eventEditorControl.SelectionChanged -= value;
            }
        }

        public event EventHandler CutPressed;
        public event EventHandler CopyPressed;
        public event EventHandler PastePressed;
        #endregion

        #region Public Functions
        public void SetPhaseRange( float min, float max )
        {
            this.eventEditorControl.SetPhaseRange( min, max );
        }

        public void SetDisplayRange( float min, float max )
        {
            this.eventEditorControl.SetDisplayRange( min, max );
        }

        public void SetDisplayView( float min, float max )
        {
            this.eventEditorControl.SetDisplayView( min, max );
        }

        public void AddTrackType( string typeName )
        {
            this.eventEditorControl.AddTrackType( typeName );
        }

        public TrackControl CreateTrack( string text, string typeName )
        {
            return this.eventEditorControl.CreateTrack( text, typeName );
        }

        public void AddTrack( TrackControl trackCtrl )
        {
            this.eventEditorControl.AddTrack( trackCtrl );
        }

        public void RemoveTrack( int trackIndex )
        {
            this.eventEditorControl.RemoveTrack( trackIndex );
        }

        public EventControl CreateEvent( string text, string typeName, float minT, float maxT, float midT, int trackIndex )
        {
            return this.eventEditorControl.CreateEvent( text, typeName, minT, maxT, midT, trackIndex );
        }

        public void AddEvent( EventControl eventCtrl, int trackIndex )
        {
            this.eventEditorControl.AddEvent( eventCtrl, trackIndex );
        }

        public void RemoveEvent( EventControl eventCtrl )
        {
            this.eventEditorControl.RemoveEvent( eventCtrl );
        }

        public EventControl FindEvent( object tag )
        {
            return this.eventEditorControl.FindEvent( tag );
        }

        public List<EventControl> FindSelectedEvents()
        {
            return this.eventEditorControl.FindSelectedEvents();
        }
        #endregion

        #region Overrides
        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == (Keys.Control | Keys.C))
            {
                if (CopyPressed != null)
                {
                    CopyPressed(this, new EventArgs());
                }
                return true;
            }
            else if (keyData == (Keys.Control | Keys.V))
            {
                if (PastePressed != null)
                {
                    PastePressed(this, new EventArgs());
                }
                return true;
            }
            else if (keyData == (Keys.Control | Keys.X))
            {
                if (CutPressed != null)
                {
                    CutPressed(this, new EventArgs());
                }
                return true;
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }
        #endregion
    }
}
