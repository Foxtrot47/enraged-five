using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using ragCore;
using ragPopup;
using EventEditorControls;
using RSG.Base;
using System.Diagnostics;
using ragWidgets;


namespace ragEventEditor
{
    public class WidgetEventEditorVisible : WidgetVisible
    {
        public WidgetEventEditorVisible(IWidgetView widgetView, WidgetEventEditor eventEditor, Control parent, ToolTip tooltip)
            : base( widgetView, parent, eventEditor )
        {
            m_tooltip = tooltip;

            m_Widget = eventEditor;
            m_Widget.PhaseAndDisplayUpdated += widget_PhaseAndDisplayUpdated;
            m_Widget.DisplayViewUpdated += widget_DisplayViewUpdated;
            m_Widget.TimeUpdated += widget_TimeUpdated;
            m_Widget.EventMoved += widget_EventMoved;
            m_Widget.TrackReplaced += widget_TrackReplaced;
            m_Widget.TrackAdded += widget_TrackAdded;
            m_Widget.EventAdded += widget_EventAdded;
            m_Widget.EventRemoved += widget_EventRemoved;
            m_Widget.EventTypeAdded += widget_EventTypeAdded;
            m_Widget.EventChanged += widget_EventChanged;

            m_controlEventEditor = new ControlEventEditor();
            m_controlEventEditor.Text = eventEditor.Title;
            m_controlEventEditor.Enabled = !m_Widget.IsReadOnly && m_Widget.Enabled;
            m_controlEventEditor.EventCreate += new EventCreateOnTrackEventHandler( eventEditorControl_EventCreate );
            m_controlEventEditor.EventDelete += new EventOnTrackWithCancelEventHandler( eventEditorControl_EventDelete );
            m_controlEventEditor.EventEdit += new EventOnTrackEventHandler( eventEditorControl_EventEdit );
            m_controlEventEditor.EventMoved += new EventOnTrackEventHandler( eventEditorControl_EventMoved );
            m_controlEventEditor.EventMoving += new EventOnTrackEventHandler(eventEditorControl_EventMoving);
            m_controlEventEditor.TrackCreate += new TrackCreateEventHandler( eventEditorControl_TrackCreate );
            m_controlEventEditor.SelectionChanged += new EventOnTrackEventHandler(eventEditorControl_SelectionChanged);

            m_controlEventEditor.CopyPressed += delegate(object s, EventArgs e) {
                CopyToClipboard();
            };

            m_controlEventEditor.PastePressed += delegate(object s, EventArgs e)
            {
                PasteFromClipboard();
            };

            m_controlEventEditor.CutPressed += delegate(object s, EventArgs e)
            {
                CutToClipboard();
            };

            foreach ( string eventType in eventEditor.EventTypeNames )
            {
                m_controlEventEditor.AddTrackType( eventType );
            }

            parent.Controls.Add( m_controlEventEditor );

            List<Control> ctrls = m_controlEventEditor.MouseEventControls;
            foreach ( Control ctrl in ctrls )
            {
                AddMouseEvents( ctrl );
            }
        }

        #region Variables
        private static int sm_eventBarPost = 0;

        private ControlEventEditor m_controlEventEditor;
        private WidgetEventEditor m_Widget;

        /// <summary>
        /// Reference to the view's shared tooltip.
        /// </summary>
        private ToolTip m_tooltip;
        #endregion

        #region Properties
        public override Control Control
        {
            get
            {
                return m_controlEventEditor;
            }
        }

        public override Control DragDropControl
        {
            get
            {
                return m_controlEventEditor;
            }
        }

        public override Widget Widget
        {
            get
            {
                return m_Widget;
            }
        }

        public override bool CanCopy
        {
            get
            {
                return true;
            }
        }

        public override bool CanPaste
        {
            get
            {
                return true;
            }
        }

        public override bool IsClipboardDataAvailable
        {
            get
            {
                return true;
            }
        }
        #endregion

        #region Event Handlers
        private void widget_PhaseAndDisplayUpdated( object sender, EventArgs args )
        {
            Debug.Assert( sender == m_Widget );
            SetPhaseAndDisplayRange( 
                m_Widget.TimeLineComponent.PhaseMin,
                m_Widget.TimeLineComponent.PhaseMax,
                m_Widget.TimeLineComponent.DisplayMin,
                m_Widget.TimeLineComponent.DisplayMax);
        }

        private void widget_DisplayViewUpdated( object sender, EventArgs args )
        {
            Debug.Assert( sender == m_Widget );
            SetDisplayView(
                m_Widget.TimeLineComponent.DisplayViewMin,
                m_Widget.TimeLineComponent.DisplayViewMax );
        }

        private void widget_TimeUpdated( object sender, GenericValueArgs args )
        {
            Debug.Assert( sender == m_Widget );
            SetCurrentTime( (float)args.Value );
        }

        private void widget_EventMoved( object sender, GenericValueArgs args )
        {
            Debug.Assert( sender == m_Widget );

            uint remote = ((Tuple<uint, EventInst>)args.Value).Item1;
            EventInst inst = ((Tuple<uint, EventInst>)args.Value).Item2;

            EventControl eventCtrl = FindEvent( remote );
            if ( eventCtrl != null )
            {
                eventCtrl.SetMinMax( inst.MinT, inst.MaxT );
            }
        }

        private void widget_TrackReplaced( object sender, GenericValueArgs args )
        {
            Debug.Assert( sender == m_Widget );

            int trackIndex = ((Tuple<int, EventInst>)args.Value).Item1;
            EventInst inst = ((Tuple<int, EventInst>)args.Value).Item2;

            TrackControl trackCtrl = GetTrack( trackIndex );
            if ( trackCtrl != null )
            {
                trackCtrl.Text = inst.Text;
                trackCtrl.TypeName = inst.TypeName;
            }
        }

        private void widget_TrackAdded( object sender, GenericValueArgs args )
        {
            Debug.Assert( sender == m_Widget );

            EventInst inst = (EventInst)args.Value;
            CreateAndAddTrack( inst.Text, inst.TypeName );
        }

        private void widget_EventAdded( object sender, GenericValueArgs args )
        {
            Debug.Assert( sender == m_Widget );

            uint remote = ((Tuple<uint, EventInst>)args.Value).Item1;
            EventInst inst = ((Tuple<uint, EventInst>)args.Value).Item2;

            EventControl eventCtrl = CreateAndAddEvent( inst.Text, inst.TypeName, inst.MinT, inst.MaxT, inst.TrackIndex );
            if ( eventCtrl != null )
            {
                eventCtrl.Tag = remote;
            }
        }

        private void widget_EventRemoved( object sender, GenericValueArgs args )
        {
            Debug.Assert( sender == m_Widget );

            uint remote = (uint)args.Value;
            RemoveEvent( remote );
        }

        private void widget_EventTypeAdded( object sender, GenericValueArgs args )
        {
            Debug.Assert( sender == m_Widget );

            string typeName = (string)args.Value;
            AddEventType( typeName );
        }

        private void widget_EventChanged( object sender, GenericValueArgs args )
        {
            Debug.Assert( sender == m_Widget );

            uint remote = ((Tuple<uint, EventInst>)args.Value).Item1;
            EventInst inst = ((Tuple<uint, EventInst>)args.Value).Item2;

            EventControl eventCtrl = FindEvent( remote );
            if ( eventCtrl != null )
            {
                eventCtrl.Text = inst.Text;
                eventCtrl.SetMinMax( inst.MinT, inst.MaxT );
            }
        }

        private void eventEditorControl_EventCreate( object sender, EventCreateOnTrackEventArgs e )
        {
            // do not assign to e.EventControl.  We will dispatch a message to the game and create the event
            // when/if it tells us.
            m_Widget.CreateEventInst( e.TrackControl.Text, e.TrackControl.TypeName, e.MinT, e.MaxT, e.TrackControl.TrackNumber );
        }

        private void eventEditorControl_EventDelete( object sender, EventOnTrackWithCancelEventArgs e )
        {
            if ( e.EventControl.Tag != null )
            {
                // do not actually delete the event yet.  We will dispatch a message to the game and it will 
                // tell us when/if to delete the event.
                e.Cancel = true;

                m_Widget.DeleteEvent( (uint)e.EventControl.Tag );
            }
        }

        private void eventEditorControl_EventEdit( object sender, EventOnTrackEventArgs e )
        {
            if ( e.EventControl.Tag != null )
            {
                WidgetGroup group = m_Widget.GetRemoteWidgetGroup( (uint)e.EventControl.Tag );
                if ( group != null )
                {
                    string eventBarName = "EventBarPost" + (sm_eventBarPost++);

                    XPanderList xpanderList = new XPanderList();
                    xpanderList.Dock = DockStyle.Fill;

                    WidgetVisualiser.ShowVisible( group, null, xpanderList, eventBarName, true, m_tooltip );
                    xpanderList.Size = new Size( 350, 450 );
                    
                    Popup thePopup = new Popup( xpanderList, e.EventControl );
                    thePopup.Resizable = true;
                    thePopup.AnimationSpeed = 0;
                    thePopup.ShowShadow = true;
                    thePopup.HorizontalPlacement = Popup.ePlacement.BottomLeft;

                    thePopup.Show();
                }
            }
        }

        private void eventEditorControl_EventMoved( object sender, EventOnTrackEventArgs e )
        {
            if ( e.EventControl.Tag != null )
            {
                m_Widget.MoveEvent( (uint)e.EventControl.Tag, e.EventControl.MinT, e.EventControl.MaxT );
            }
        }

        private void eventEditorControl_EventMoving(object sender, EventOnTrackEventArgs e)
        {
            if (e.EventControl.Tag != null)
            {
                float eventMinDisplay = e.EventControl.TimeLine.PhaseToDisplay(e.EventControl.MinT);

                if (e.EventControl.IsPhaseScrubOn)
                {
                    m_controlEventEditor.CurrentTime = eventMinDisplay;
                    m_Widget.SendCurrentTime(eventMinDisplay);
                }

                m_Widget.MoveEvent( (uint)e.EventControl.Tag, e.EventControl.MinT, e.EventControl.MaxT );
            }
        }

        private void eventEditorControl_SelectionChanged(object sender, EventOnTrackEventArgs e)
        {
            bool wasSelected = (e.EventControl != null);
            m_Widget.SelectionChanged(wasSelected, wasSelected ? (uint)e.EventControl.Tag : 0);
        }

        private void eventEditorControl_TrackCreate( object sender, TrackCreateEventArgs e )
        {
            // do not assign to e.TrackControl.  We will dispatch a message to the game and create the track
            // when/if it tells us.
            m_Widget.CreateTrack( e.Text, e.TypeName, m_controlEventEditor.TrackControls.Count );
        }
        #endregion

        #region Overrides
        public override void CopyToClipboard()
        {
            List<EventControl> selection = m_controlEventEditor.FindSelectedEvents();
            if (selection.Count == 1)
            {
                m_Widget.CopyInstance((uint)selection[0].Tag);
            }
        }

        public override void PasteFromClipboard()
        {
            m_Widget.PasteInstance(m_controlEventEditor.CurrentPhase);   
        }
        #endregion

        #region Public Functions

        public void AddEventType( string typeName )
        {
            m_controlEventEditor.AddTrackType( typeName );
        }

        public TrackControl CreateAndAddTrack( string text, string typeName )
        {
            TrackControl trackCtrl = m_controlEventEditor.CreateTrack( text, typeName );
            if ( trackCtrl != null )
            {
                m_controlEventEditor.AddTrack( trackCtrl );
            }

            return trackCtrl;
        }

        public void RemoveTrack( int trackIndex )
        {
            m_controlEventEditor.RemoveTrack( trackIndex );
        }

        public TrackControl GetTrack( int trackIndex )
        {
            List<TrackControl> trackCtrls = m_controlEventEditor.TrackControls;
            if ( (trackIndex >= 0) && (trackIndex < trackCtrls.Count) )
            {
                return trackCtrls[trackIndex];
            }

            return null;
        }

        public EventControl CreateAndAddEvent( string text, string typeName, float minT, float maxT, int trackIndex )
        {
            EventControl eventCtrl = m_controlEventEditor.CreateEvent( text, typeName, minT, maxT, -1.0f, trackIndex );
            if ( eventCtrl != null )
            {
                m_controlEventEditor.AddEvent( eventCtrl, trackIndex );
            }

            return eventCtrl;
        }

        public void RemoveEvent( uint remote )
        {
            EventControl eventCtrl = m_controlEventEditor.FindEvent( remote );
            if ( eventCtrl != null )
            {
                m_controlEventEditor.RemoveEvent( eventCtrl );
            }
        }

        public EventControl FindEvent( uint remote )
        {
            return m_controlEventEditor.FindEvent( remote );
        }

        public EventControl FindEvent( int localHash )
        {
            Guid guid = new Guid( localHash, 0, 0, new byte[8] );
            return m_controlEventEditor.FindEvent( guid );
        }

        public void SetCurrentTime( float time )
        {
            m_controlEventEditor.CurrentTime = time;
        }

        public void SetPhaseAndDisplayRange( float min, float max, float displayMin, float displayMax )
        {
            m_controlEventEditor.SetPhaseRange( min, max );
            m_controlEventEditor.SetDisplayRange( displayMin, displayMax );
        }

        public void SetDisplayView( float min, float max )
        {
            m_controlEventEditor.SetDisplayView( min, max );
        }
        #endregion

        #region Protected Functions
        protected void CutToClipboard()
        {
            List<EventControl> selection = m_controlEventEditor.FindSelectedEvents();
            if (selection.Count == 1)
            {
                m_Widget.CopyInstance((uint)selection[0].Tag);
                m_Widget.DeleteEvent((uint)selection[0].Tag);
            }
        }

        public override void Destroy()
        {
            m_Widget.PhaseAndDisplayUpdated -= widget_PhaseAndDisplayUpdated;
            m_Widget.DisplayViewUpdated -= widget_DisplayViewUpdated;
            m_Widget.TimeUpdated -= widget_TimeUpdated;
            m_Widget.EventMoved -= widget_EventMoved;
            m_Widget.TrackReplaced -= widget_TrackReplaced;
            m_Widget.TrackAdded -= widget_TrackAdded;
            m_Widget.EventAdded -= widget_EventAdded;
            m_Widget.EventRemoved -= widget_EventRemoved;
            m_Widget.EventTypeAdded -= widget_EventTypeAdded;
            m_Widget.EventChanged -= widget_EventChanged;
            m_Widget = null;

            base.Destroy();
        }
        #endregion
    }
}
