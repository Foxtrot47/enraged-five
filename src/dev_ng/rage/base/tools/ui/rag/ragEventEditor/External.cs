using System;
using System.Collections.Generic;
using System.Text;

using ragCore;

namespace ragEventEditor
{
    public class External : IWidgetPlugIn
    {
        public External()
        {

        }

        public void AddHandlers( BankRemotePacketProcessor packetProcessor )
        {
            WidgetEventEditor.AddHandlers(packetProcessor );
        }

        public void RemoveHandlers( BankRemotePacketProcessor packetProcessor )
        {
            WidgetEventEditor.RemoveHandlers(packetProcessor );
        }
    }
}
