// MainDlg.h : interface of the CMainDlg class
//
/////////////////////////////////////////////////////////////////////////////

#pragma once

#include "vcproj.h"
#include "resource.h"

class CMainDlg : public CDialogImpl<CMainDlg>, public CUpdateUI<CMainDlg>,
		public CMessageFilter, public CIdleHandler
{
public:
	enum { IDD = IDD_MAINDLG };

    CMainDlg(LPTSTR lpCmdLine);

	virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual BOOL OnIdle();

    void PopulateTree();

	BEGIN_UPDATE_UI_MAP(CMainDlg)
	END_UPDATE_UI_MAP()

	BEGIN_MSG_MAP(CMainDlg)
		MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog)
		MESSAGE_HANDLER(WM_DESTROY, OnDestroy)
		COMMAND_ID_HANDLER(IDOK, OnOK)
		COMMAND_ID_HANDLER(IDCANCEL, OnCancel)
        COMMAND_ID_HANDLER(IDC_APPLY, OnApply)
        COMMAND_ID_HANDLER(IDC_NEWGROUP, OnNewGroup)
        COMMAND_ID_HANDLER(IDC_DELETEGROUP, OnDeleteGroup)
        COMMAND_HANDLER(IDC_EDIT1, EN_CHANGE, OnEnChangeEdit1)
        NOTIFY_HANDLER(IDC_TREE1, NM_CLICK, OnNMClickTree1)
        NOTIFY_HANDLER(IDC_GROUPS, LVN_BEGINLABELEDIT, OnLvnBeginlabeleditGroups)
        NOTIFY_HANDLER(IDC_GROUPS, LVN_ENDLABELEDIT, OnLvnEndlabeleditGroups)
        NOTIFY_HANDLER(IDC_GROUPS, LVN_ITEMCHANGED, OnLvnItemchangedGroups)
    END_MSG_MAP()

	LRESULT OnInitDialog(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);
	LRESULT OnDestroy(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);
	LRESULT OnOK(WORD /*wNotifyCode*/, WORD wID, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
    LRESULT OnApply(WORD /*wNotifyCode*/, WORD wID, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
	LRESULT OnCancel(WORD /*wNotifyCode*/, WORD wID, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
    LRESULT OnNewGroup(WORD /*wNotifyCode*/, WORD wID, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
    LRESULT OnDeleteGroup(WORD /*wNotifyCode*/, WORD wID, HWND /*hWndCtl*/, BOOL& /*bHandled*/);

	void CloseDialog(int nVal);

    bool            m_inited;
    bool            m_groupsDirty, m_iniDirty;
    CEdit           m_edit;
    CTreeViewCtrl   m_tree;
    CImageList      m_stateImages;
    cSln            m_sln;
    CComboBox       m_configs;
    CListViewCtrl   m_groups;
    int             m_curGroup;
    string          m_filter;
	string			m_CommandLine;

    LRESULT OnEnChangeEdit1(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
    LRESULT OnNMClickTree1(int /*idCtrl*/, LPNMHDR pNMHDR, BOOL& /*bHandled*/);
    LRESULT OnLvnBeginlabeleditGroups(int /*idCtrl*/, LPNMHDR pNMHDR, BOOL& /*bHandled*/);
    LRESULT OnLvnEndlabeleditGroups(int /*idCtrl*/, LPNMHDR pNMHDR, BOOL& /*bHandled*/);
    LRESULT OnLvnItemchangedGroups(int /*idCtrl*/, LPNMHDR pNMHDR, BOOL& /*bHandled*/);
};
