#include "stdafx.h"
#include "vcproj.h"
#include "../../../3rdParty/GrinningLizard/TinyXML/tinyxml.h"
#include <boost/algorithm/string.hpp>
#include "vcproj.h"
#include "MainDlg.h"
#include <map>

using namespace boost;

cSln::cSln()
:   mpRoot(new TiXmlElement("Filter"))
{
    mpRoot->SetAttribute("Name", "All");
}

cSln::~cSln()
{
    delete mpRoot;
}

bool cSln::ParseProjectLine(const char* line)
{
	//Ensure that the Project("{ token is at the start of the line.
	if( strstr(line, "Project(\"{") == line)
	{
		//Project("{8BC9CEB8-8B4A-11D0-8D11-00A0C91BC942}") = "sample_multidrawable", "sample_multidrawable_2008.vcproj", "{826F3544-AA3E-4E5D-AE79-718485C7EC6A}"
		//NOTE: The outputted GUID contains brackets.

		string lineStr(line);
		size_t guidIndex = lineStr.find_first_of("{");
		size_t guidEndIndex = lineStr.find_first_of("}");
		string m_SolutionGUID = lineStr.substr(guidIndex, guidEndIndex - guidIndex + 1);
		
		string afterGUIDToken("\") = \"");
		size_t projectIndex = lineStr.find_first_of(afterGUIDToken, guidEndIndex + afterGUIDToken.length());  //NOTE: The extra space and equals are meant to find the first quotation after the GUID.
		size_t projectEndIndex = lineStr.find_first_of("\"", projectIndex+1);
		string m_ProjectName = lineStr.substr(projectIndex + 1, projectEndIndex - projectIndex -1 );  //NOTE:  Eliminate the quotations at the front and back of the string.

		size_t relativePathIndex = lineStr.find_first_of("\"", projectEndIndex+1);
		size_t relativePathEndIndex = lineStr.find_first_of("\"", relativePathIndex+1);
		string m_RelativePath = lineStr.substr(relativePathIndex + 1, relativePathEndIndex - relativePathIndex - 1 );  //NOTE:  Eliminate the quotations at the front and back of the string.

		size_t projectGUIDIndex = lineStr.find_first_of("\"", relativePathEndIndex+1);
		size_t projectGUIDEndIndex = lineStr.find_first_of("\"", projectGUIDIndex+1);
		string m_ProjectGUID = lineStr.substr(projectGUIDIndex + 1, projectGUIDEndIndex - projectGUIDIndex -1 );  //NOTE:  Eliminate the quotations at the front and back of the string.

		if (m_SolutionGUID.compare("{2150E333-8FDC-42A3-9474-1A3956D46DE8}") == 0)
		{
			// Project is really a solution folder - skip!
			return false;
		}

		mProjects[m_ProjectGUID].name = string(m_ProjectName);
		mProjects[m_ProjectGUID].path = JoinPath(mSlnDir, string(m_RelativePath));            

		return true;
	}

	return false;
}

bool cSln::ParseConfigurationLine(const char* line)
{
	if ( strstr(line, ".ActiveCfg = ") != NULL)
	{
		// strip line endings... 
		size_t length = strlen(line);
		if (line[length-1] == 0x0A)
			length--;

		string lineStr(line, length);  //{AFE1A518-0637-45B3-8044-3F4D023E7B8B}.BankRelease|Win32.ActiveCfg = ToolReleaseMax9|Win32

		size_t guidStartIndex = lineStr.find_first_of("{");
		size_t periodIndex = lineStr.find_first_of('.');
		string m_ProjectGUID = lineStr.substr(guidStartIndex, periodIndex - guidStartIndex);

		size_t slashIndex = lineStr.find_first_of('|');
		string m_SolutionConfig = lineStr.substr( periodIndex+1, slashIndex - periodIndex - 1 );

		string activeConfigStr(".ActiveCfg = ");
		size_t activeConfigIndex = lineStr.find(".ActiveCfg = ");
		string m_SolutionPlatform = lineStr.substr(slashIndex+1, activeConfigIndex - slashIndex - 1);

		size_t projectConfigStart = activeConfigIndex+activeConfigStr.length();
		size_t projectConfigEnd = lineStr.length() - projectConfigStart;
		string m_ProjectConfig = lineStr.substr(projectConfigStart, projectConfigEnd);

		if (m_SolutionPlatform.find("PS3") != string::npos || m_SolutionPlatform.find("PS3SNC") != string::npos ) // only PS3 configs!
		{
			if (find(mConfigs.begin(), mConfigs.end(), m_SolutionConfig) == mConfigs.end())
				mConfigs.push_back(m_SolutionConfig);
			mProjects[ m_ProjectGUID ].projConfigDirs[ m_ProjectConfig ] = "";
			mProjects[ m_ProjectGUID ].slnToProjConfig[ m_SolutionConfig ] = m_ProjectConfig;
			return true;
		}
	}

	return false;
}

void cSln::LoadSln(std::string slnpath)
{    
	mPath = trim_copy_if(slnpath, is_any_of(" \t\n\""));
    mSlnDir = RemoveFilenameFromPath(mPath);

    char buf[512];
    FILE* f = 0;
    fopen_s(&f, mPath.c_str(), "r");
    if (!f)
        return;

    while (fgets(buf,512,f))
    {
        ParseProjectLine(buf);
		ParseConfigurationLine(buf);
    }
    fclose(f);

    for(map<string,cProj>::iterator it = mProjects.begin(); it != mProjects.end(); ++it)
    {
        cProj& proj = it->second;
        TiXmlElement* pproj = LoadVcProj(proj.name.c_str(), proj.path.c_str());

		const char* projectName = pproj->Attribute("Name");

		const char *keyword = pproj->Attribute("Keyword");
		if (keyword)
		{
			if (!strcmp(keyword,"MakeFileProj"))
				continue;
		}

        // store the output directory for each configuration in the project
        string projdir = RemoveFilenameFromPath(proj.path);
        TiXmlElement* configs = pproj->FirstChild("Configurations")->ToElement();
        for(TiXmlElement* config = configs->FirstChildElement("Configuration"); 
            config; config = config->NextSiblingElement("Configuration"))
        {
            const char* name = config->Attribute("Name");

            if (proj.projConfigDirs.find(string(name)) != proj.projConfigDirs.end())
            {
                const char* outputfile = 0;
                for(TiXmlElement* tool = config->FirstChildElement("Tool"); 
                    tool; tool = tool->NextSiblingElement("Tool"))
                {
                    outputfile = tool->Attribute("OutputFile");
                    if (outputfile)
                        break;
                }

                if(outputfile)
                {
                    string configName(name, strchr(name,'|'));
                    string platformName(strchr(name,'|')+1);
                    string outputDir = JoinPath(projdir, config->Attribute("OutputDirectory"));
                    string outputPath(outputfile);
                    replace_all(outputPath, "$(OutDir)", outputDir);
                    replace_all(outputPath, "$(ConfigurationName)", configName);
                    replace_all(outputPath, "$(PlatformName)", platformName);
					replace_all(outputPath, "$(ProjectName)", projectName);
                    proj.projConfigDirs[name] = outputDir;
                    proj.projConfigOutputs[name] = outputPath;
                }
            }
        }
    }

    // load any config groups
    const char* ext = strrchr(mPath.c_str(), '.');
    string slnnoext(mPath.c_str(), ext);
    string dbgpath = slnnoext + "_debuggroups.txt";
    if (LoadDebugGroups(dbgpath.c_str()))
	{
		string dbgenable = slnnoext + "_debuggroups.ini";
		LoadDebugEnable(dbgenable.c_str());
	}
	else
	{
		mGroups.push_back(cDebugGroup(string("Default")));
	}
}

TiXmlElement* cSln::LoadVcProj(const char* name, const char* filename)
{
    TiXmlDocument xml(filename);
    xml.LoadFile();
    TiXmlElement* pProject = mpRoot->InsertEndChild(*xml.FirstChild("VisualStudioProject"))->ToElement();
    pProject->SetAttribute("Name", name);
    return pProject;
}

bool cSln::LoadDebugGroups(const char* filename)
{
    mDebugGroupsFilename = filename;
    FILE* f = 0;
    fopen_s(&f, filename, "r");
    if (!f)
        return false;
    cDebugGroup* curgroup = 0;
    char buf[2048];
    while (fgets(buf, 2048, f))
    {
        string s = trim_copy(string(buf));
        if (s[0] == '[' && s[s.length()-1] == ']')
        {
            mGroups.push_back(cDebugGroup(s.substr(1, s.length() - 2)));
            curgroup = &mGroups.back();
        }
        else if (curgroup && !s.empty())
        {
            curgroup->mFiles.insert(s);
        }
    }
    fclose(f);
	return true;
}

void cSln::LoadDebugEnable(const char* filename)
{
    mDebugEnableFilename = filename;
    FILE* f = 0;
    fopen_s(&f, filename, "r");
    if (!f)
        return;
   
    char buf[2048];
    while (fgets(buf, 2048, f))
    {
        char* eq = strchr(buf,'=');
        if (eq)
        {
            *eq = 0;
            bool val = eq[1] == '1';
            for(size_t i=0; i<mGroups.size(); ++i)
                if (mGroups[i].Name() == buf)
                    mGroups[i].mEnabled = val;
        }
    } 
    fclose(f);
}

void cSln::SaveDebugInfo(bool saveGroups,bool saveIni)
{
    FILE* f = 0;
	if (saveGroups)
	{
		fopen_s(&f, mDebugGroupsFilename.c_str(), "w");
		if (f)
		{
			for(size_t i=0; i<mGroups.size(); ++i)
			{
				fprintf(f, "[%s]\n", mGroups[i].Name().c_str());
				vector<string> g(mGroups[i].mFiles.begin(), mGroups[i].mFiles.end());
				sort(g.begin(), g.end());
				for(vector<string>::iterator j=g.begin(); j!=g.end(); ++j)
					fprintf(f, "%s\n", j->c_str());
				fprintf(f, "\n");
			}
			fclose(f);
			f = 0;
		}
		else
		{
			char msg[2048];
			sprintf_s(msg, 2048, "Error writing to debuggroups file '%s'", mDebugGroupsFilename.c_str());
			MessageBox(0, msg, "PS3 Debug Info", MB_OK);
		}
	}

	if (saveIni)
	{
		fopen_s(&f, mDebugEnableFilename.c_str(), "w");
		if (f)
		{
			for(size_t i=0; i<mGroups.size(); ++i)
				fprintf(f, "%s=%i\n", mGroups[i].Name().c_str(), mGroups[i].mEnabled);
			fclose(f);
			f = 0;
		}
		else
		{
			char msg[2048];
			sprintf_s(msg, 2048, "Error writing to ini file '%s' (did you forget to hit Apply first?)", mDebugEnableFilename.c_str());
			MessageBox(0, msg, "PS3 Debug Info", MB_OK);
		}
	}

	set<string> files;
	for(size_t i=0; i<mGroups.size(); ++i)
		if (mGroups[i].mEnabled)
			files.insert(mGroups[i].mFiles.begin(), mGroups[i].mFiles.end());    

    // save a debuginfo.txt for each vcproj
    for(map<string, cProj>::iterator it = mProjects.begin(); it != mProjects.end(); ++it)
    {
        string name = it->second.name+"|";
        int l = name.length();
        string path = it->second.path;
        string debuginfo = RemoveFilenameFromPath(path) + "\\debuginfo.txt";
        vector<string> oldfiles;
        fopen_s(&f, debuginfo.c_str(), "r");
        if (f)
        {
            char buf[2048];
            while (fgets(buf, 2048, f))
            {
                string name(buf);
                trim(name);
                oldfiles.push_back(name);
            }
            fclose(f);
            f = 0;
        }
        vector<string> newfiles;
        for(set<string>::iterator j = files.begin(); j != files.end(); ++j)
        {
            if (starts_with(*j, name))
            {
                string n = to_lower_copy(j->substr(l, j->find_last_of('.') - l));                              
                newfiles.push_back(n);
            }
        }
        sort(newfiles.begin(), newfiles.end());
        sort(oldfiles.begin(), oldfiles.end());
        if (newfiles != oldfiles)
        {
            // file list has changed!
            fopen_s(&f, debuginfo.c_str(), "w");
            if (f)
            {
                for(vector<string>::iterator j = newfiles.begin(); j != newfiles.end(); ++j)
                    fprintf(f, "%s\n", j->c_str());
                fclose(f);
                f = 0;

                // delete the LIB files to force a rebuild
                for(map<string,string>::iterator j = it->second.projConfigOutputs.begin();
                    j != it->second.projConfigOutputs.end(); ++j)
                    _unlink(j->second.c_str());
            }
            else
            {
                char msg[2048];
                sprintf_s(msg, 2048, "Error writing to file library debuginfo file '%s'", debuginfo.c_str());
                MessageBox(0, msg, "PS3 Debug Info", MB_OK);
            }
        }
    }
}

int cSln::PopulateTree(CTreeViewCtrl& tree, const char* filter, HTREEITEM parent, TiXmlElement* pElement)
{
    if (!pElement)
        pElement = mpRoot;

    const char* path = 0;
    const char* name = 0;
    if (strcmp(pElement->Value(), "VisualStudioProject") == 0)
    {
        name = pElement->Attribute("Name");
        pElement = pElement->FirstChild("Files")->ToElement();
    }
    else if (strcmp(pElement->Value(), "Filter") == 0)
    {
        name = pElement->Attribute("Name");
    }
    else if (strcmp(pElement->Value(), "File") == 0)
    {
        const char* ext = strrchr(pElement->Attribute("RelativePath"), '.');
        if (ext && (_stricmp(ext, ".c")==0 || _stricmp(ext, ".cpp")==0 || _stricmp(ext, ".job")==0 || _stricmp(ext, ".task")==0))
        {
            path = pElement->Attribute("RelativePath");
            name = GetNameFromPath(path);
        }
    }
    HTREEITEM item = 0;
    if (name)
        item = parent = tree.InsertItem(name, parent, TVI_LAST);

    int state = 0;
    if (name && icontains(name, filter))
    {
        tree.SetItemData(item, (DWORD_PTR)pElement);
        if (path)
        {
            pElement->Attribute("Debug", &state);
            ++state;
        }
        filter = "";
    }
    for(TiXmlElement* pChild = pElement->FirstChildElement(); pChild; pChild = pChild->NextSiblingElement()) 
    {
        int childState = PopulateTree(tree, filter, parent, pChild);
        if (childState)
        {
            if (!state)
                state = childState;
            else if (state != childState)
                state = 3;
        }
    }
    if (item)
    {
        if (!state)
        {
            tree.DeleteItem(item);
        }
        else
        {
            tree.SortChildren(item);
            tree.SetItemState(item, INDEXTOSTATEIMAGEMASK(state), TVIS_STATEIMAGEMASK);
            if (strlen(filter) || pElement == mpRoot)
                tree.Expand(item);
        }
    }
    return state;
}

bool cDebugGroup::Contains(const string& name)
{
    return mFiles.find(name) != mFiles.end();
}

void cSln::ApplyDebugGroup(int id, const char* projectname, TiXmlElement* pElement)
{
    if (!pElement)
        pElement = mpRoot;
    if (!strcmp(pElement->Value(),"VisualStudioProject"))
        projectname = pElement->Attribute("Name");
    const char* path = pElement->Attribute("RelativePath");
    if (path)
    {
        int state = 0;
        string name = string(projectname) + "|" + GetNameFromPath(path);
        if (id == -1)
        {
            for(size_t i=0; !state && i<mGroups.size(); ++i)
                if (mGroups[i].mEnabled)
                    state = mGroups[i].Contains(name);
        }
        else
        {
            state = mGroups[id].Contains(name);                
        }
        pElement->SetAttribute("Debug", state);
    }
    for(TiXmlElement* pChild = pElement->FirstChildElement(); pChild; pChild = pChild->NextSiblingElement()) 
        ApplyDebugGroup(id, projectname, pChild);
}

void cSln::SetDebugInfo(int groupid, TiXmlElement* pElement, int state)
{
    pElement->SetAttribute("Debug", state);
    TiXmlElement* pProject = pElement;
    while (pProject && strcmp(pProject->Value(),"VisualStudioProject"))
        pProject = pProject->Parent()->ToElement();    
    string name = string(pProject->Attribute("Name")) + "|" + GetNameFromPath(pElement->Attribute("RelativePath"));

    set<string>& g = mGroups[groupid].mFiles;
    if (!state)
    {
        if (g.find(name) != g.end())
            g.erase(g.find(name));
    }
    else
    {
        g.insert(name);
    }
}

const char* GetNameFromPath(const char* path)
{
    const char* name = path;
    const char* lastslash = strrchr(path, '/');
    const char* lastbslash = strrchr(path, '\\');
    if (!lastslash || (lastbslash && lastbslash < lastslash))
        lastslash = lastbslash;
    if (lastslash)
        name = lastslash + 1;
    return name;
}

string RemoveFilenameFromPath(string filepath)
{
    char drive[_MAX_DRIVE];
    char dir[_MAX_DIR];
    char filename[_MAX_FNAME];
    char ext[_MAX_EXT];
    _splitpath_s(filepath.c_str(), drive, dir, filename, ext);
    char path[_MAX_PATH];
    _makepath_s(path, drive, dir, "", "");
    return path;
}

string JoinPath(string path1, string path2)
{
    char buf[2048];
    GetFullPathName((path1+"/"+path2).c_str(), 2048, buf, 0);
    return buf;
}
