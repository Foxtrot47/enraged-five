#ifndef VCPROJ_H
#define VCPROJ_H

using namespace std;

class TiXmlElement;

struct cProj
{
    string name;
    string path;
    map<string, string> slnToProjConfig;
    map<string, string> projConfigDirs;
    map<string, string> projConfigOutputs;
};

struct cDebugGroup
{
    cDebugGroup(string name) : mEnabled(false), mName(name) {}
    void    Name(string name) {mName = name;}
    string  Name() const {return mName;}
    bool    Contains(const string& name);
    bool            mEnabled;
    string          mName;
    set<string>     mFiles;
};

class cSln
{
public:
    cSln();
    ~cSln();

	void                    LoadSln(std::string slnpath);
    TiXmlElement*           LoadVcProj(const char* name, const char* filename);
    void                    SaveDebugInfo(bool saveGroups,bool saveIni);
    const vector<string>&   Configs() const {return mConfigs;}
    vector<cDebugGroup>&    Groups() {return mGroups;}

    int                     PopulateTree(CTreeViewCtrl& tree, const char* filter = "", 
                                        HTREEITEM parent = TVI_ROOT, TiXmlElement* pElement = 0);
    void                    ApplyDebugGroup(int groupid, const char* rootpath = "", TiXmlElement* pElement = 0);
    void                    SetDebugInfo(int groupid, TiXmlElement* pElement, int state);
    const char*             Path() {return mPath.c_str();}

private:
    bool					LoadDebugGroups(const char* filename);
    void                    LoadDebugEnable(const char* filename);

	bool					ParseProjectLine(const char* line);
	bool					ParseConfigurationLine(const char* line);

    cSln(const cSln&);
    cSln& operator= (const cSln& rhs);

    string              mPath;
    string              mSlnDir;
    string              mDebugGroupsFilename;
    string              mDebugEnableFilename;
    TiXmlElement*       mpRoot;
    vector<string>      mConfigs;
    vector<cDebugGroup> mGroups;
    map<string, cProj>  mProjects;   
};

const char* GetNameFromPath(const char* path);
string RemoveFilenameFromPath(string filepath);
string JoinPath(string path1, string path2);

#endif // VCPROJ_H
