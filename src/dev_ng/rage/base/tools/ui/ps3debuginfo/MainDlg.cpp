// MainDlg.cpp : implementation of the CMainDlg class
//
/////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "MainDlg.h"
#include "vcproj.h"
#include "../../../3rdParty/GrinningLizard/TinyXML/tinyxml.h"

CMainDlg::CMainDlg(LPTSTR lpCmdLine)
{
	if (lpCmdLine == NULL)
		m_CommandLine = "";
	else
		m_CommandLine = lpCmdLine;

	m_inited = false;
}

BOOL CMainDlg::PreTranslateMessage(MSG* pMsg)
{
	return CWindow::IsDialogMessage(pMsg);
}

BOOL CMainDlg::OnIdle()
{
	return FALSE;
}

LRESULT CMainDlg::OnInitDialog(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
	if (m_CommandLine.empty())
	{
		MessageBox("Usage: ps3debuginfo path-to-sln-file.sln", "ps3debuginfo", MB_OK | MB_ICONERROR);
		ExitProcess(1);
	}

	m_sln.LoadSln(m_CommandLine);

	// center the dialog on the screen
	CenterWindow();

	SetWindowText((std::string("ps3debuginfo - ") + m_sln.Path()).c_str());

	// set icons
	HICON hIcon = (HICON)::LoadImage(_Module.GetResourceInstance(), MAKEINTRESOURCE(IDR_MAINFRAME), 
		IMAGE_ICON, ::GetSystemMetrics(SM_CXICON), ::GetSystemMetrics(SM_CYICON), LR_DEFAULTCOLOR);
	SetIcon(hIcon, TRUE);
	HICON hIconSmall = (HICON)::LoadImage(_Module.GetResourceInstance(), MAKEINTRESOURCE(IDR_MAINFRAME), 
		IMAGE_ICON, ::GetSystemMetrics(SM_CXSMICON), ::GetSystemMetrics(SM_CYSMICON), LR_DEFAULTCOLOR);
	SetIcon(hIconSmall, FALSE);

	// register object for message filtering and idle updates
	CMessageLoop* pLoop = _Module.GetMessageLoop();
	ATLASSERT(pLoop != NULL);
	pLoop->AddMessageFilter(this);
	pLoop->AddIdleHandler(this);

	UIAddChildWindowContainer(m_hWnd);

    m_edit.Attach(GetDlgItem(IDC_EDIT1));
    m_configs.Attach(GetDlgItem(IDC_CONFIGS));

    m_stateImages.Create(IDB_TREESTATES, 16, 0, RGB(255,0,0));
    m_tree.Attach(GetDlgItem(IDC_TREE1));
    m_tree.SetImageList(m_stateImages, TVSIL_STATE);

    RECT rect;
    m_groups.Attach(GetDlgItem(IDC_GROUPS));
    m_groups.SetExtendedListViewStyle(LVS_EX_CHECKBOXES|LVS_EX_FULLROWSELECT);
    m_groups.AddColumn("Name", 0);
    m_groups.GetClientRect(&rect);
    m_groups.SetColumnWidth(0, rect.right);    

    for(vector<string>::const_iterator it = m_sln.Configs().begin(); it != m_sln.Configs().end(); ++it)
        m_configs.AddString(it->c_str());
    if (!m_sln.Configs().empty())
        m_configs.SelectString(0, m_sln.Configs()[0].c_str());
    for(size_t i=0; i<m_sln.Groups().size(); ++i)
    {
        int state = m_sln.Groups()[i].mEnabled;
        m_groups.AddItem(i, 0, m_sln.Groups()[i].Name().c_str());
        m_groups.SetCheckState(i, state);
    }
	if (m_sln.Groups().size() > 0)
	{
		m_groups.SelectItem(0);
		m_curGroup = 0;
	}
	else
	{
		m_curGroup = -1;
	}
    m_inited = true;
    m_groupsDirty = false;
	m_iniDirty = false;

    m_sln.ApplyDebugGroup(m_curGroup);
    PopulateTree();

	return TRUE;
}

LRESULT CMainDlg::OnDestroy(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
	// unregister message filtering and idle updates
	CMessageLoop* pLoop = _Module.GetMessageLoop();
	ATLASSERT(pLoop != NULL);
	pLoop->RemoveMessageFilter(this);
	pLoop->RemoveIdleHandler(this);

	return 0;
}

LRESULT CMainDlg::OnApply(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& /*bHandled*/)
{
    if (hWndCtl == GetDlgItem(IDC_APPLY))
    {
        m_sln.SaveDebugInfo(m_groupsDirty,m_iniDirty);
        m_groupsDirty = m_iniDirty = false;
    }
	return 0;
}

LRESULT CMainDlg::OnOK(WORD /*wNotifyCode*/, WORD wID, HWND hWndCtl, BOOL& /*bHandled*/)
{
    if (hWndCtl == GetDlgItem(IDOK))
    {
        m_sln.SaveDebugInfo(m_groupsDirty,m_iniDirty);
	    CloseDialog(wID);
    }
	return 0;
}

LRESULT CMainDlg::OnCancel(WORD /*wNotifyCode*/, WORD wID, HWND hWndCtl, BOOL& /*bHandled*/)
{
    if (hWndCtl == GetDlgItem(IDCANCEL))
    {
        if (m_groupsDirty || m_iniDirty)
        {
            int result = MessageBox("Save changes?", "PS3 Debug Info", MB_YESNOCANCEL);
            if (result == IDCANCEL)
                return 0;
            if (result == IDYES)
                m_sln.SaveDebugInfo(m_groupsDirty,m_iniDirty);
        }
        CloseDialog(wID);
    }
    return 0;
}

void CMainDlg::CloseDialog(int nVal)
{
	DestroyWindow();
	::PostQuitMessage(nVal);
}

void CMainDlg::PopulateTree()
{
    m_tree.SetRedraw(FALSE);
    m_tree.DeleteAllItems();
    m_sln.PopulateTree(m_tree, m_filter.c_str());
    m_tree.SetRedraw();
}

LRESULT CMainDlg::OnEnChangeEdit1(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
    char buf[256];
    m_edit.GetWindowText(buf, 256);
    m_filter = buf;
    PopulateTree();
    return 0;
}

void UpdateParentCheckStates(CTreeViewCtrl& tree, HTREEITEM i)
{
    if (!i)
        return;
    HTREEITEM child = tree.GetChildItem(i);
    int state = tree.GetItemState(child, TVIS_STATEIMAGEMASK)>>12;
    while ((child = tree.GetNextSiblingItem(child)) != 0 && state != 3)
        if (state != tree.GetItemState(child, TVIS_STATEIMAGEMASK)>>12)
            state = 3;
    tree.SetItemState(i, INDEXTOSTATEIMAGEMASK(state), TVIS_STATEIMAGEMASK);
    UpdateParentCheckStates(tree, tree.GetParentItem(i));
}

HTREEITEM NextTreeItem(CTreeViewCtrl& tree, HTREEITEM i, HTREEITEM root = 0)
{
    HTREEITEM next = tree.GetChildItem(i);
    while (!next)
    {
        if (i == root)
            break;
        next = tree.GetNextSiblingItem(i);
        if (!next)
            i = tree.GetParentItem(i);
    }
    return next;
}

LRESULT CMainDlg::OnNMClickTree1(int /*idCtrl*/, LPNMHDR pNMHDR, BOOL& /*bHandled*/)
{
    if (pNMHDR->hwndFrom != m_tree)
        return 0;

    CPoint pt;
    GetCursorPos(&pt);
    CPoint ptClient = pt;
    ::ScreenToClient(pNMHDR->hwndFrom, &ptClient);

    TVHITTESTINFO tvhti = { 0 };
    tvhti.pt = ptClient;
    m_tree.HitTest(&tvhti);
    if (tvhti.flags & TVHT_ONITEMSTATEICON)
    {
        HTREEITEM i = tvhti.hItem;
        bool state = !m_tree.GetCheckState(i);        
        m_tree.SelectItem(i);

        if (m_curGroup >= 0)
        {
            m_groupsDirty = true;
            // update item & children
            do
            {
                TiXmlElement* pElement = (TiXmlElement*)m_tree.GetItemData(i);            
                if (pElement && pElement->Attribute("RelativePath"))
                    m_sln.SetDebugInfo(m_curGroup, pElement, state);

                m_tree.SetCheckState(i, state);
                i = NextTreeItem(m_tree, i, tvhti.hItem);
            } while (i);
            UpdateParentCheckStates(m_tree, m_tree.GetParentItem(tvhti.hItem));
        }
        return 1;
    }
    return 0;
}

LRESULT CMainDlg::OnNewGroup(WORD /*wNotifyCode*/, WORD wID, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
    m_groups.SetFocus();    
    char name[16];
    for(int n=1;;++n)
    {
        sprintf_s(name, 16, "Group%i", n);
        size_t i;
        for(i=0; i<m_sln.Groups().size(); ++i)
            if (m_sln.Groups()[i].Name() == name)
                break;
        if (i == m_sln.Groups().size())
            break;
    }        
    m_groupsDirty = true;
    m_sln.Groups().push_back(cDebugGroup(name));
    int i = m_groups.GetItemCount();
    m_groups.AddItem(i, 0, name, 0);
    m_groups.SelectItem(i);
    m_groups.EditLabel(i);
    return 0;
}

LRESULT CMainDlg::OnDeleteGroup(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
    int item = m_groups.GetSelectedIndex();
    if (item >= 0 && item < m_groups.GetItemCount())
    {
        m_groupsDirty = true;
        m_groups.DeleteItem(item);
        m_sln.Groups().erase(m_sln.Groups().begin() + item);
    }
    return 0;
}

LRESULT CMainDlg::OnLvnBeginlabeleditGroups(int /*idCtrl*/, LPNMHDR pNMHDR, BOOL& /*bHandled*/)
{
    return 0;
}
LRESULT CMainDlg::OnLvnEndlabeleditGroups(int /*idCtrl*/, LPNMHDR pNMHDR, BOOL& /*bHandled*/)
{
    NMLVDISPINFO *pDispInfo = reinterpret_cast<NMLVDISPINFO*>(pNMHDR);
    if ((pDispInfo->item.mask & TVIF_TEXT) && pDispInfo->item.pszText && *pDispInfo->item.pszText)
    {
        for(size_t i=0; i<m_sln.Groups().size(); ++i)
            if (m_sln.Groups()[i].Name() == pDispInfo->item.pszText)
                return 0;
        m_groupsDirty = true;
        m_sln.Groups()[pDispInfo->item.iItem].Name(pDispInfo->item.pszText);
        return 1;
    }
    return 0;
}
LRESULT CMainDlg::OnLvnItemchangedGroups(int /*idCtrl*/, LPNMHDR pNMHDR, BOOL& /*bHandled*/)
{
    if (!m_inited)
        return 0;

    NMLISTVIEW* pDispInfo = (NMLISTVIEW*)pNMHDR;
    bool update = false;
    int item = pDispInfo->iItem;
    if (item >= 0 && item < m_groups.GetItemCount())
    {
        bool state = m_groups.GetCheckState(item) != 0;
        bool& istate = m_sln.Groups()[item].mEnabled;
        if (istate != state)
        {
            istate = state;
            update = true;
        }
    }

    item = m_groups.GetSelectedIndex();
    if (item < 0 || item >= m_groups.GetItemCount())
        item = -1;
    if (m_curGroup != item)
    {
        m_curGroup = item;
        update = true;
    }

    if (update)
    {
        m_iniDirty = true;
        m_sln.ApplyDebugGroup(m_curGroup);
        PopulateTree();
    }

    return 0;
}
