//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by ps3debuginfo.rc
//
#define IDR_MAINFRAME                   128
#define IDD_MAINDLG                     129
#define IDB_TREESTATES                  202
#define IDC_TREE1                       1000
#define IDC_EDIT1                       1001
#define IDC_CONFIGS                     1002
#define IDC_GROUPS                      1005
#define IDC_NEWGROUP                    1006
#define IDC_DELETEGROUP                 1007
#define IDC_APPLY                       1008
#define ID_ACCELERATOR32776             32776

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        204
#define _APS_NEXT_COMMAND_VALUE         32779
#define _APS_NEXT_CONTROL_VALUE         1009
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
