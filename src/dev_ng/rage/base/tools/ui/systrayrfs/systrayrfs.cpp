#undef _WIN32_IE
#define _WIN32_IE 0x0500

#define USE_STOCK_ALLOCATOR	1

#define __WINMAIN 1

#include "system/xtl.h"
#include "system/main.h"

#ifndef STRICT
#define STRICT
#endif

#include "file/remote.h"
#include "file/winsock.h"
#include "string/string.h"
#include "system/ipc.h"
#include "system/memory.h"
#include "paging/streamer.h"
#include "parser/manager.h"
#include "parser/tree.h"

#include "configParser/configIgnoredConnectionSources.h"
#include "configParser/configPortView.h"

#include "XMessageBox.h"
#define IS_SERVER
#undef GetClassName
#undef MessageBox
int MessageBox(HWND hWnd, LPCSTR lpText, LPCSTR lpCaption, UINT uType);

// We are recompiling a different version of remote.cpp here, with IS_SERVER
// defined.  This is intended to override the version of remote.obj that is in
// the RageCore library.  If you get linker errors about duplicate sysmbols,
// that means that the linker needed to pull in both object files.  That happens
// when there is a symbol missing from the version we are compiling here with
// IS_SERVER defined.  The simplest way to track down the missing symbol is to
// wrap the entire remote.cpp file in #ifdef IS_SERVER link again and see what
// the missing symbols are.  Thanks to Dave Etherton for this tip!
#include "file/remote.cpp"

#include <stdio.h>
// #include <iostream.h>
#include "resource.h"

#define ICONIDENT		102
#define WM_TRAYMESSAGE	(WM_USER+1)		//user defined message for icon
//#define RESERVE			4002
//#define MENUITEM3			4003
#define MENUITEM_EXIT		4004
#define MENUITEM_RESET		4005
#define MENUITEM_DUMP		4006
//#define RESERVEMENU		5000

#define UPDATE_TIMER_ID	1
#define GetClassName  GetClassNameA

static int sPort = 9001;


NOTIFYICONDATA Nicon;	//contains information about icon to add/modify/delete
HWND			hWnd;
HMENU			phmenu, hmenu;
HINSTANCE		hInst;
POINT			pxy;

#if (_WIN32_IE < 0x0500)
#define TOOL_TIP_LENGTH 64
#else
#define TOOL_TIP_LENGTH 128
#endif
char ToolTip[TOOL_TIP_LENGTH] = {"sysTrayRfs v1.62 proto 4/5/6"};	//can't be over TOOL_TIP_LENGTH char length!

char List[20][32];
int ListCount(0);

bool g_NoFocus = false;

const char* sWindowName	="SysTrayRfs";

static configParser::ConfigIgnoredConnectionSources* sIgnoredConnectionSources;

int AssertMessageBox(HWND hWnd, LPCSTR lpText, LPCSTR lpCaption, UINT uType);



namespace rage {
	void serverDumpAllServerFiles();
	
	u32 GetStrIndexFromHandle(const pgStreamer::Handle &)
	{
		return 0;
	}
}

void BuildReservedMenu()
{
//	RemoveMenu(hmenu, RESERVEMENU+i, MF_BYCOMMAND);
//	AppendMenu(hmenu, MF_DISABLED, RESERVEMENU + (i-1), (LPCTSTR) List[i]);
}

int MessageBox(HWND hWnd, LPCSTR lpText, LPCSTR lpCaption, UINT uType)
{
	if (g_NoFocus)
	{
		char name[32];
		GetClassName(GetForegroundWindow(), name, ARRAYSIZE(name));
		if (strcmp(name, "Shell_TrayWnd") != 0) // windows task bar
		{
			uType |= MB_NOFOCUS;
			uType &= ~MB_SETFOREGROUND;
		}
	}

	return XMessageBox(hWnd, lpText, lpCaption, uType);	
}

int AssertMessageBox(HWND hWnd, LPCSTR lpText, LPCSTR lpCaption, UINT uType)
{
	if (g_NoFocus)
	{
		char name[32];
		GetClassName(GetForegroundWindow(), name, ARRAYSIZE(name));
		if (strcmp(name, "Shell_TrayWnd") != 0) // windows task bar
		{
			uType |= MB_NOFOCUS;
			uType &= ~MB_SETFOREGROUND;
		}
	}

	int rc = 0;
	XMSGBOXPARAMS xmb;

	if ((uType & MB_ABORTRETRYIGNORE) != 0)
	{
		// Set up the custom buttons
		_tcscpy(xmb.szCustomButtons, _T("Abort\nRetry\nIgnore\nEdit Bug")); 
		uType -= MB_ABORTRETRYIGNORE;

		rc = XMessageBox(hWnd, lpText, lpCaption, uType, &xmb) & 0xFF;

		// Patch up the return code
		if ((rc & 0xFF) == IDCUSTOM1)
		{
			rc = IDABORT;
		}
		else if ((rc & 0xFF) == IDCUSTOM2)
		{
			rc = IDRETRY;
		}
		else if ((rc & 0xFF) == IDCUSTOM3)
		{
			rc = IDIGNORE;
		}
	}
	else if ((uType & MB_OK) != 0)
	{
		_tcscpy(xmb.szCustomButtons, _T("OK\nEdit Bug"));
		uType -= MB_OK;
		rc = XMessageBox(hWnd, lpText, lpCaption, uType, &xmb) & 0xFF;

		// Patch up the return code
		if ((rc & 0xFF) == IDCUSTOM1)
		{
			rc = IDOK;
		}
		else if ((rc & 0xFF) == IDCUSTOM2)
		{
			rc = IDCUSTOM4;
		}
	}
	else
	{
		rc = XMessageBox(hWnd, lpText, lpCaption, uType, &xmb) & 0xFF;
	}

	return rc;
}

LRESULT CALLBACK WndProc(HWND hWnd,UINT iMsg,WPARAM wParam,LPARAM lParam)
{
	switch (iMsg)
	{
	case WM_CREATE:
		hInst = (HINSTANCE)GetWindowLong(hWnd,GWL_HINSTANCE);
		phmenu = LoadMenu(hInst,MAKEINTRESOURCE(IDC_POPUP));	//load menu from resource
		hmenu = GetSubMenu(phmenu,0);	//load submenu for icon
		////////////////////////////////////////////////////////////////////////////
			Nicon.cbSize=sizeof(NOTIFYICONDATA);	//tell it the size of this structure
			Nicon.hIcon = LoadIcon(hInst,MAKEINTRESOURCE(IDR_AVAILABLE));	//tell it which icon
			Nicon.hWnd = hWnd;						//messages will be sent to this window
			Nicon.uFlags = NIF_ICON|NIF_MESSAGE|NIF_TIP;	//this says that we are
															//using an icon tooltip and a
															//message for the struct
			Nicon.uCallbackMessage = WM_TRAYMESSAGE;		//for notification
			Nicon.uID = ICONIDENT;			//ID of the icon
			rage::safecpy( Nicon.szTip, ToolTip, sizeof(Nicon.szTip) );
			Shell_NotifyIcon(NIM_ADD,&Nicon);	//add icon to systray
		////////////////////////////////////////////////////////////////////////////
		break;
	case WM_TRAYMESSAGE:
		switch(wParam)
		{
		case ICONIDENT:		//make sure which icon it was, you can add more
			switch(lParam)	//look for mouse message
			{
			case WM_RBUTTONUP:
					GetCursorPos(&pxy);	//Since the mouse will be on the icon use it
					//Pop the menu up
					SetForegroundWindow(hWnd);
					TrackPopupMenu(hmenu,TPM_LEFTALIGN|TPM_TOPALIGN,(pxy.x)+1,pxy.y,0,hWnd,NULL);
					PostMessageA(hWnd, WM_NULL, 0, 0);
					break;
			case WM_LBUTTONUP:
			{
				// do something here
				break;
			}
			}
			break;
		}
		break;

	case WM_TIMER:
	{
		switch (wParam)
		{
			case UPDATE_TIMER_ID:
			{
				// check cookie
				break;
			 }
		}
	}
	break;

	case WM_COMMAND:
	{
		switch(LOWORD(wParam))
		{
/*
			case RESERVE:	// release cookie
//				AddReservation(hWnd,cookieFile,userName,owner);
				break;

			case MENUITEM3:	// check cookie
//				CheckCookie(hWnd,cookieFile,userName,owner);
				break;
*/
			case MENUITEM_EXIT: // exit
				DestroyWindow(hWnd);
				break;

			case MENUITEM_RESET:
				rage::fiRemoteResetFiles();
                break;

			case MENUITEM_DUMP:
				rage::serverDumpAllServerFiles();
				break;
		}
	}
		break;

	case WM_CLOSE:
		DestroyWindow(hWnd);
		break;
	case WM_DESTROY:
			rage::fiRemoteResetFiles();
			Shell_NotifyIcon(NIM_DELETE,&Nicon);	//get rid of the icon cause were ending
			PostQuitMessage(0);
			return 0;
		break;
	}
	return DefWindowProc(hWnd,iMsg,wParam,lParam);
}

extern WINBASEAPI
BOOL
WINAPI
SwitchToThread(
    VOID
    );


//static HANDLE sRFSThread;
//static DWORD sThreadId;
static char sIp[32];

#pragma warning(disable: 4702)

static void sRFSThreadFunc(LPVOID) 
{
	rage::fiRemoteServer(sPort,sIp);
}


namespace rage {
extern bool RestrictedAccess;
extern bool g_fiCaseSensitive;
extern bool g_RfsNeverTimeOut;
}

#include "diag/channel.h"


PARAM(addr, "Address to connect to" );
PARAM(port, "Port to connect on" );
PARAM(reset, "Resets files if already running" );
PARAM(exit, "Exits the program if already running" );
PARAM(trusted, "Whether the application should run in trusted mode" );
PARAM(nofocus, "Message box shouldn't take focus" );
PARAM(casesensitive, "Initialises rage to be case-sensitive" );
PARAM(log, "Sends all output to the c:\\systrayrfs.log file" );
PARAM(debuglevel, "Specify the verbosity of the debug output, 0-3" );
PARAM(nevertimeout, "Sets rage not to timeout" );

PARAM(file, "script parameter - here just to stop rage complaining" );
PARAM(project, "script parameter - here just to stop rage complaining" );


bool ShouldAcceptConnection(const char* ip)
{
	return !sIgnoredConnectionSources->ShouldIgnoreAddress(ip);
}

int Main()
{
	// do this so that we can read command line arguments
	//extern int __argc;
	//extern char **__argv;

	INIT_PARSER;
	PARSER.Settings().SetFlag(rage::parSettings::USE_TEMPORARY_HEAP, true);

	InitWinSock();
	
	sIgnoredConnectionSources = new configParser::ConfigIgnoredConnectionSources();
	rage::fiRemoteSetAcceptConnectionDelegate(rage::ShouldAcceptRemoteConnectionFunc(ShouldAcceptConnection));

	char myname[64];
	if (PARAM_addr.Get())
	{
		const char* address;
		PARAM_addr.Get(address);
		rage::safecpy( myname, address, sizeof(myname) );
	}
	else
	{
		gethostname( myname, 63 );
	}

	// Check whether the port config file contains a port for systray.
	configParser::ConfigPortView* portview = new configParser::ConfigPortView();
	int configPort;
	if (portview->GetPortForId("systrayrfs", configPort))
	{
		sPort = configPort;
	}
	delete portview;

	if (PARAM_port.Get())
	{
		int port;
		if(PARAM_port.Get(port))
		{
			sPort = port;
		}
	}

	struct hostent *hp = gethostbyname( myname );
	if ( hp == NULL )
	{
		return 0;
	}

	unsigned char *ip = (unsigned char*)hp->h_addr;
    sprintf(sIp,"%d.%d.%d.%d",ip[0],ip[1],ip[2],ip[3]);
	char rfs_dat[256];
	for (int pass=0; pass<2; pass++) {
		sprintf(rfs_dat,"%s\\rfs.dat",pass==1?"c:":getenv("TEMP"));
		FILE *f = fopen(rfs_dat,"w");
		if (f) {
			printf("creating [%s]\n",rfs_dat);
			fprintf(f,sIp);
			fprintf(f,"\n");
			fclose(f);
		}
	} 

	char windowTitle[128];
    sprintf(windowTitle,"SysTrayRfs (port %d on %s)",sPort,sIp);

	HWND alreadyRunning = ::FindWindow(NULL,windowTitle);
	bool reset = PARAM_reset.Get();
	bool exit = PARAM_exit.Get();

	g_NoFocus = PARAM_nofocus.Get();
	rage::g_fiCaseSensitive = PARAM_casesensitive.Get();

	if (alreadyRunning) {
		if (reset) {
			::SendMessage(alreadyRunning,WM_COMMAND,MENUITEM_RESET,0);
		}
		else if (exit) {
			::SendMessage(alreadyRunning,WM_COMMAND,MENUITEM_EXIT,0);
		}
		return 0;
	}
	else if (reset || exit)	// ignore these if we were not already running
		return 0;

	if (PARAM_trusted.Get()) {
		rage::RestrictedAccess = false;
		rage::safecat( ToolTip, " [TRUSTED]", TOOL_TIP_LENGTH );
	}

	HINSTANCE hInstance = GetModuleHandle(NULL);

	const char *MainClassName = "SysTrayRfs";
	WNDCLASSEX wclass;
	MSG msg;
	wclass.cbSize = sizeof(wclass);
	wclass.style = CS_HREDRAW | CS_VREDRAW;
	wclass.lpfnWndProc = WndProc;
	wclass.cbClsExtra = 0;
	wclass.cbWndExtra = 0;
	wclass.hInstance = hInstance;
	wclass.hIcon = LoadIcon(hInstance,MAKEINTRESOURCE(IDR_AVAILABLE));
	wclass.hCursor = LoadCursor(NULL,IDC_ARROW);
	wclass.hbrBackground = (HBRUSH)GetStockObject(LTGRAY_BRUSH);
	wclass.lpszMenuName = NULL;
	wclass.lpszClassName = MainClassName;
	wclass.hIconSm = LoadIcon(hInstance,MAKEINTRESOURCE(IDR_AVAILABLE)); // reserved is the other option

	RegisterClassEx(&wclass);

	char tmp[32];		
    sprintf(tmp,"\n    (ip:port %s:%d)",sIp,sPort);
	rage::safecat( ToolTip, tmp, TOOL_TIP_LENGTH );

	hWnd = CreateWindow(MainClassName, windowTitle,
						WS_OVERLAPPEDWINDOW, CW_USEDEFAULT, CW_USEDEFAULT, 300, 100,NULL, NULL, hInstance, NULL);

//	SetTimer (hWnd, UPDATE_TIMER_ID, 5000, NULL) ; // milli-seconds
	
//	MessageBox(hWnd,"To use the cookie monster you need to provide a -path command line argument that tells the location of the cookie.txt file, eg as -path M:\\agent\\doc\\soft\\cookiemonster.",sWindowName,MB_OK);
	if (PARAM_log.Get()) 
	{
		rage::diagChannel::ALL.OpenLogFile("c:/systrayrfs.log");
		rage::diagChannel::SetOutput(true);
		rage::g_RfsDebug = 1;
	}

	PARAM_debuglevel.Get(rage::g_RfsDebug);

	rage::g_RfsNeverTimeOut = PARAM_nevertimeout.Get();
	
	sysIpcCreateThread(sRFSThreadFunc,NULL,rage::sysIpcMinThreadStackSize,rage::PRIO_NORMAL,"Main Server Thread");

	while(GetMessage(&msg,NULL,0,0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	delete sIgnoredConnectionSources;

	SHUTDOWN_PARSER;
	return msg.wParam;
}
