#include <winsock2.h>
#include <stdio.h>

#pragma comment(lib,"wsock32.lib")


void main() {
	WORD wVersionRequested=MAKEWORD(2,0);
 	WSADATA wsaData;
	if (WSAStartup(wVersionRequested,&wsaData) != 0)
		printf("InitWinSock - Couldn't find a usable WinSock DLL\n");

	if (LOBYTE(wsaData.wVersion)!=2 || HIBYTE(wsaData.wVersion)!=0) {
		WSACleanup();
		printf("InitWinSock - Couldn't find a usable WinSock DLL (version=%x)\n",wsaData.wVersion);
	}

	char myname[64];
	gethostname(myname,63);

	struct hostent *hp = gethostbyname(myname);

	unsigned char *ip = (unsigned char*)hp->h_addr;

	char rfs_dat[256];
	sprintf(rfs_dat,"%s\\rfs.dat",getenv("TEMP"));
	FILE *f = fopen(rfs_dat,"w");
	if (f) {
		printf("creating [%s]\n",rfs_dat);
		fprintf(f,"%d.%d.%d.%d\n",ip[0],ip[1],ip[2],ip[3]);
		fclose(f);
	}
	else
		printf("unable to create %s\n",rfs_dat);
}
