//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by cookie.rc
//
#define IDR_AVAILABLE                   103
#define IDC_POPUP                       4000
#define ID_SYSTRAY_SHOWTTY              40002
#define ID_SYSTRAY_DUMPOPENFILESTOC     40003

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        104
#define _APS_NEXT_COMMAND_VALUE         40004
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
