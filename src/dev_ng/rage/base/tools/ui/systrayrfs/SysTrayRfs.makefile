Project SysTrayRfs

ConfigurationType exe
TitleId 0x54540840

RootDirectory .

IncludePath $(RAGE_DIR)\base\src
IncludePath $(RAGE_DIR)\base\tools\libs
IncludePath $(RAGE_3RDPARTY)\cli\boost_1_43_0
IncludePath $(RAGE_3RDPARTY)\cli\libxml2-2.7.6\include

Define SYSTRAY

Files {
	cookie.rc
	resource.h
	systrayrfs.cpp
	XMessageBox.h
	XMessageBox.cpp
}

Libraries {
	%RAGE_DIR%\base\src\vcproj\RageCore\RageCore
	%RAGE_DIR%\base\tools\libs\configUtil\configUtil
	%RAGE_DIR%\base\tools\libs\configParser\configParser
	%RAGE_3RDPARTY%\cli\libxslt-1.1.26\vcproj\libxslt
	%RAGE_3RDPARTY%\cli\libxml2-2.7.6\vcproj\libxml2
}