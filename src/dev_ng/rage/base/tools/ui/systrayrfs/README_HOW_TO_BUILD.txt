Requirements:
1. Visual Studio 2012
2. Lots of patience

If you need to regenerate the solution:
1. Run x:\gta5\tools_ng\script\util\ProjGen\projGen.bat from the current folder
2. Open SysTrayRfs_2012.sln
3. Select Release - Win32 configuration (x64 doesn't work as SysTrayRfs passes windows HANDLE over the network casted to int)
4. Double check that the following configuration is set:
	- configParser: ToolRelease-DLL
	- configUtil: ToolRelease-DLL
	- libxml2: Release-DLL
	- libxslt: Release-DLL
	- RageCore: ToolRelease
	- SysTrayRfs: ToolRelease
5. Verify that all projects have the following platform toolset: Visual Studio 2012 (v110)