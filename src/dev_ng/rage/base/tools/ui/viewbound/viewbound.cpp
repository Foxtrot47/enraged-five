////////////////////
// viewbound.cpp  //
////////////////////


#include "bank/bank.h"
#include "bank/bkmgr.h"
#include "devcam/freecam.h"
#include "grcore/im.h"
#include "input/keyboard.h"
#include "input/keys.h"
#include "input/pad.h"
#include "phbound/bound.h"
#include "phbound/boundgrid.h"
#include "phcore/materialmgrimpl.h"
#include "grprofile/drawmanager.h"
#include "system/main.h"
#include "system/param.h"
#include "system/timer.h"
#include "vector/colors.h"

#include "sample_grcore/sample_grcore.h"

namespace rage {

PFD_DECLARE_GROUP_ON(Polygons);
PFD_DECLARE_ITEM_ON(All,Color_white,Polygons);
PFD_DECLARE_ITEM(Thin,Color_red,Polygons);
PFD_DECLARE_ITEM(BadNormal,Color_red,Polygons);

EXT_PFD_DECLARE_GROUP(Physics);
EXT_PFD_DECLARE_GROUP(Bounds);
EXT_PFD_DECLARE_ITEM(DrawBoundMaterials);
EXT_PFD_DECLARE_ITEM_SLIDER(BoundDrawDistance);

EXT_PFD_DECLARE_ITEM(Solid);
EXT_PFD_DECLARE_ITEM_SLIDER(NormalLength);
EXT_PFD_DECLARE_ITEM(Face);
EXT_PFD_DECLARE_ITEM(Edge);
EXT_PFD_DECLARE_ITEM_SLIDER_INT(HighlightFlags);

} // namespace rage 

namespace ragesamples {

using namespace rage;

////////////////////////////////////////////////////////////////
// 

#define VIEWBOUND_DEFAULT_FILE	"T:/rage/assets/physics/crate/bound.bnd"

PARAM(file,"the file containing the bound (.bnd)");
FPARAM(1, filename, "the file containing the bound (.bnd)");
PARAM(posx,"camera target x position");
PARAM(posy,"camera target y position");
PARAM(posz,"camera target z position");

class viewBoundManager : public grcSampleManager
{
public:
	viewBoundManager()
	  : m_Bound(NULL)
	  , m_ShowNormals(POLY_NORMALS)
	  , m_CameraTarget(ORIGIN)
	  , m_DrawEntireGrid(true)
	  , m_GridCellX(0)
	  ,	m_GridCellZ(0)
	{
	}

	virtual void InitCamera()
	{
		Vector3 cameraPos(3.0f,3.0f,3.0f);
		cameraPos.Add(m_CameraTarget);
		grcSampleManager::InitSampleCamera(cameraPos, m_CameraTarget);
	}

	virtual const char* GetSampleName() const
	{
		static char name[256] = "viewbound ";

		const char* filename = VIEWBOUND_DEFAULT_FILE;
		if (!PARAM_file.Get(filename))
		{
			PARAM_filename.Get(filename);
		}
		strcat(name, filename);
		return name;
	}

	// UNFINISHED - a little work on reading Google Sketchup files:
	bool CheckConvertKmlToBnd (const char* filename)
	{
		// See if the file with the given name is a Google Sketchup file (*.kml).
		int nameLength = 0;
		while (filename[nameLength++]) {}
		char extension[4];
		formatf(extension,sizeof(extension),&filename[nameLength-4]);
		if (!strcmp(extension,"kml"))
		{
			// This is a Google Sketchup file (the name has extension kml).
			return true;
		}

		// This is not a Google Sketchup file.
		return false;
	}

	virtual void InitClient()
	{
		sysTimer timer;
		const char* filename = VIEWBOUND_DEFAULT_FILE;

		PFD_GROUP_ENABLE(Physics, true);
		PFD_GROUP_ENABLE(Bounds, true);
		PFD_ITEM_SLIDER_SET_VALUE(BoundDrawDistance, 1000.0f);

		////////////////////////////////////////////////////////////
		// get bound filename
		if (!PARAM_file.Get(filename))
		{
			PARAM_filename.Get(filename);
		}

		////////////////////////////////////////////////////////////
		// get optional vector elements for the camera target position
		float position;
		m_PositionSpecified = false;
		if (PARAM_posx.Get(position))
		{
			m_CameraTarget.x = position;
			m_PositionSpecified = true;
		}
		if (PARAM_posy.Get(position))
		{
			m_CameraTarget.y = position;
			m_PositionSpecified = true;
		}
		if (PARAM_posz.Get(position))
		{
			m_CameraTarget.z = position;
			m_PositionSpecified = true;
		}

		timer.Reset();

		phBound::SetBoundFlag(phBound::DELETE_BAD_POLYGONS,false);
		phBound::SetBoundFlag(phBound::ASSERT_MATH_ERRORS,false);

		phBound::DisableMessages();

		// Make the material manager with a large maximum number of materials to handle grid bounds.
		phMaterialMgrImpl<phMaterial>::Create();
		char mtlPath[1024];
		sprintf(mtlPath,"%s/%s",GetFullAssetPath(),"tune/materials");
		ASSET.PushFolder(mtlPath);
		MATERIALMGR.Load();
		ASSET.PopFolder();

		// UNFINISHED - a little work on reading Google Sketchup files:
		// See if the given file is a Google Sketchup file (*.kml), and load it and resave as a bound file if so.
		CheckConvertKmlToBnd(filename);

		// Load the bnd file.
		m_Bound = phBound::Load(filename);
		if (!m_Bound)
		{
			Quitf("Unable to find file '%s'.",filename);
		}

		////////////////////////////////////////////////////////////
		// display some useful bound information
		Displayf("Load time: %f ms\n",timer.GetMsTime());
		DisplayBoundInfo();

		////////////////////////////////////////////////////////////
		// display type specific stats
		if (m_Bound->GetType() == phBound::OCTREEGRID)
		{
			DisplayGridInfo();
#if __BANK
			bkBank& bank = BANKMGR.CreateBank("grid");
			bank.AddToggle("Draw Entire Grid",&m_DrawEntireGrid,datCallback(MFA(viewBoundManager::ToggleDrawEntireGrid), this));
			m_GridCellX = static_cast<phBoundGrid*>(m_Bound)->GetMinFirstAxis();
			bank.AddSlider("Grid Cell X",
						   &m_GridCellX,
						   m_GridCellX,
						   static_cast<phBoundGrid*>(m_Bound)->GetMaxFirstAxis(),
						   1,
						   datCallback(MFA(viewBoundManager::SetGridCell), this));
			m_GridCellZ = static_cast<phBoundGrid*>(m_Bound)->GetMinSecondAxis();
			bank.AddSlider("Grid Cell Z",
				           &m_GridCellZ,
						   m_GridCellZ,
						   static_cast<phBoundGrid*>(m_Bound)->GetMaxSecondAxis(),
						   1,
						   datCallback(MFA(viewBoundManager::SetGridCell), this));
#endif
#if __PFDRAW
			if (m_PositionSpecified)
			{
				// A position was given in arguments, so set the grid to draw only the occupied cell.
				viewBoundManager::ToggleDrawEntireGrid();
				static_cast<phBoundGrid*>(m_Bound)->SetDrawCell(m_CameraTarget);
			}
#endif
		}

		m_Mapper.Reset();
		m_Mapper.Map(IOMS_KEYBOARD, KEY_N, m_NormalsIoValue);
		m_Mapper.Map(IOMS_KEYBOARD, KEY_ADD, m_NextCell);
		m_Mapper.Map(IOMS_KEYBOARD, KEY_SUBTRACT, m_PrevCell);
		m_Mapper.Map(IOMS_KEYBOARD, KEY_MULTIPLY, m_ClearCell);

#if __PFDRAW
		GetRageProfileDraw().Init(20000000, true, grcBatcher::BUF_FULL_ONSCREEN_WARNING);
		GetRageProfileDraw().SetEnabled(true);
#endif
	}

	void InitLights()
	{
		m_Lights.Reset();
		m_Lights.SetColor(0, 1.0f,1.0f,1.0f);
		m_Lights.SetColor(1, 0.0f,0.0f,0.0f);
		m_Lights.SetColor(2, 0.0f,0.0f,0.0f);
		m_Lights.SetPosition(0, Vec3V(1.0f,2.0f,3.0f));
		m_Lights.SetPosition(1, Vec3V(-1.27f,-0.5f,-1.0f));
		m_Lights.SetPosition(2, Vec3V(1.2f,-0.575f,-2.15f));
		m_Lights.SetIntensity(0, 1.0f);
		m_Lights.SetIntensity(1, 0.0f);
		m_Lights.SetIntensity(2, 0.0f);
		m_Lights.SetAmbient(0.0f, 0.0f, 0.2f);

		//Initialize the manipulators for the lights
		//InitLightManipulators(m_Lights);
		// disabled because it crashes, and viewbound doesn't need it (NC 1 March 06)
	}

	virtual void ShutdownClient()
	{
#if __PFDRAW
		GetRageProfileDraw().Shutdown();
#endif

		delete m_Bound;

		MATERIALMGR.Destroy();
	}

	virtual void Update()
	{
		m_Mapper.Update();

		if (m_NormalsIoValue.IsPressed())
		{
			m_ShowNormals = (m_ShowNormals+1)%NUM_SHOW_NORMALS;

			PFD_ITEM_ENABLE(Face, m_ShowNormals == POLY_NORMALS || m_ShowNormals == POLY_EDGE_NORMALS);
			PFD_ITEM_ENABLE(Edge, m_ShowNormals == EDGE_NORMALS || m_ShowNormals == POLY_EDGE_NORMALS);
		}

		if (m_Bound->GetType()==phBound::OCTREEGRID)
		{
#if __PFDRAW
			if (m_ClearCell.IsPressed())
			{
				// select all components
				static_cast<phBoundGrid*>(m_Bound)->ClearDrawCell();
				m_DrawEntireGrid = true;
			}

			bool newCell = false;

			if (m_NextCell.IsPressed())
			{
				// select next component
				static_cast<phBoundGrid*>(m_Bound)->NextDrawCell();
				m_DrawEntireGrid = false;
				newCell = true;
			}

			if (m_PrevCell.IsPressed())
			{
				// select next component
				static_cast<phBoundGrid*>(m_Bound)->PrevDrawCell();
				m_DrawEntireGrid = false;
				newCell = true;
			}

			if (newCell)
			{
				phBoundGrid* grid = static_cast<phBoundGrid*>(m_Bound);
				int drawCell = grid->GetDrawCell();
				int minX = grid->GetMinFirstAxis();
				int maxX = grid->GetMaxFirstAxis();
				int minZ = grid->GetMinSecondAxis();
				m_GridCellX = (drawCell % (maxX - minX + 1)) + minX;
				m_GridCellZ = (drawCell / (maxX - minX + 1)) + minZ;
			}
#endif
		}

		grcSampleManager::Update();
	}

	virtual void DrawClient()
	{
#if __PFDRAW
		grcBindTexture(NULL);
		grcLightState::SetEnabled(true);

		Matrix34 drawMatrix(M34_IDENTITY);
		if (!m_PositionSpecified)
		{
			drawMatrix.d = VEC3V_TO_VECTOR3(m_Bound->GetBoundingBoxMax() + m_Bound->GetBoundingBoxMin());
			drawMatrix.d.Scale(-0.5f);
		}

		if (PFD_All.Begin())
		{
			grcColor(PFD_All.GetBaseColor());
			m_Bound->Draw(RCC_MAT34V(drawMatrix), PFD_DrawBoundMaterials.GetEnabled(), PFD_Solid.GetEnabled(), phBound::ALL_POLYS, PFD_HighlightFlags.GetValue());
			PFD_All.End();

			if (PFD_Face.Begin())
			{
				grcColor(PFD_Face.GetBaseColor());
				m_Bound->DrawNormals(RCC_MAT34V(drawMatrix), phBound::FACE_NORMALS, phBound::ALL_POLYS, PFD_NormalLength.GetValue());
				PFD_Face.End();
			}

			if (PFD_Edge.Begin())
			{
				grcColor(Color_green);
				m_Bound->DrawNormals(RCC_MAT34V(drawMatrix), phBound::EDGE_NORMALS, phBound::ALL_POLYS, PFD_NormalLength.GetValue());
				PFD_Edge.End();
			}
		}

		if (PFD_Thin.Begin())
		{
			grcColor(Color_DeepPink);
			m_Bound->Draw(RCC_MAT34V(drawMatrix), PFD_DrawBoundMaterials.GetEnabled(), true, phBound::RENDER_THIN_POLYS, PFD_HighlightFlags.GetValue());
			PFD_Thin.End();

			if (!PFD_All.GetEnabled() && PFD_Face.Begin())
			{
				grcColor(PFD_Face.GetBaseColor());
				m_Bound->DrawNormals(RCC_MAT34V(drawMatrix), phBound::FACE_NORMALS, phBound::RENDER_THIN_POLYS, PFD_NormalLength.GetValue());
				PFD_Face.End();
			}

			if (!PFD_All.GetEnabled() && PFD_Edge.Begin())
			{
				grcColor(PFD_Edge.GetBaseColor());
				m_Bound->DrawNormals(RCC_MAT34V(drawMatrix), phBound::EDGE_NORMALS, phBound::RENDER_THIN_POLYS, PFD_NormalLength.GetValue());
				PFD_Edge.End();
			}
		}

		if (PFD_BadNormal.Begin())
		{
			grcColor(Color_DeepPink);
			m_Bound->Draw(RCC_MAT34V(drawMatrix), PFD_DrawBoundMaterials.GetEnabled(), true, phBound::RENDER_BAD_NORMAL_POLYS, PFD_HighlightFlags.GetValue());
			PFD_BadNormal.End();

			if (!PFD_All.GetEnabled() && PFD_Face.Begin())
			{
				grcColor(PFD_Face.GetBaseColor());
				m_Bound->DrawNormals(RCC_MAT34V(drawMatrix), phBound::FACE_NORMALS, phBound::RENDER_BAD_NORMAL_POLYS, PFD_NormalLength.GetValue());
				PFD_Face.End();
			}

			if (!PFD_All.GetEnabled() && PFD_Edge.Begin())
			{
				grcColor(PFD_Edge.GetBaseColor());
				m_Bound->DrawNormals(RCC_MAT34V(drawMatrix), phBound::EDGE_NORMALS, phBound::RENDER_BAD_NORMAL_POLYS, PFD_NormalLength.GetValue());
				PFD_Edge.End();
			}
		}

		GetRageProfileDraw().Render();
#endif

		grcColor(Color_white);
		const grcFont& font=grcFont::GetCurrent();
		float screenY = 400.0f;
		font.DrawScaled(50.0f, screenY, 0.0f, Color_white, 1.0f, 1.0f, "Press N to cycle normals display");
		screenY += font.GetHeight() + 2.0f;
		if (m_Bound->GetType()==phBound::OCTREEGRID)
		{
			font.DrawScaled(50.0f, screenY, 0.0f, Color_white, 1.0f, 1.0f, "Press +/- to select grid cell, * to clear selection");
		}
	}

private:
	void DisplayBoundInfo()
	{
		Assert(m_Bound);
		Displayf("Bound info");
		Displayf("----------");
		#if __DEV || __TOOL
			Displayf("- Type:          %d (%s)",m_Bound->GetType(),m_Bound->GetTypeString());
		#else
			Displayf("- Type:          %d",m_Bound->GetType());
		#endif
		Displayf("- NumMaterials:  %d",m_Bound->GetNumMaterials());
		Vector3 centroidOffset = VEC3V_TO_VECTOR3(m_Bound->GetCentroidOffset());
		Displayf("- Offset:        %s, %f %f %f",m_Bound->IsCentroidOffset()?"true":"false",centroidOffset.x,centroidOffset.y,centroidOffset.z);
		Vector3 cgOffset = VEC3V_TO_VECTOR3(m_Bound->GetCGOffset());
		Displayf("- CG Offset:     %s, %f %f %f",m_Bound->IsCGOffset()?"true":"false",cgOffset.x,cgOffset.y,cgOffset.z);
		Displayf("- Actual radius: %f",m_Bound->GetRadiusAroundCentroid());
	}

	void DisplayGridInfo()
	{
		const phBoundGrid* grid = static_cast<phBoundGrid*>(m_Bound);
		Assert(grid);

		Displayf(" ");
		Displayf("grid info:");
		Displayf("-----------------");
		Displayf("MinX %d, MaxX %d",grid->GetMinFirstAxis(),grid->GetMaxFirstAxis());
		Displayf("MinZ %d, MaxZ %d",grid->GetMinSecondAxis(),grid->GetMaxSecondAxis());
		Displayf("NumCells: %d",(grid->GetMaxFirstAxis()-grid->GetMinFirstAxis()+1)*(grid->GetMaxFirstAxis()-grid->GetMinFirstAxis()+1));

#if 0//__DEV || __TOOL
		// count the number of cells
		int totalCells = 0;
		int totalPolys = 0;
		int totalSourceVerts = 0;
		int totalSourcePolys = 0;
		for (int x=grid->GetMinFirstAxis(); x<=grid->GetMaxFirstAxis(); x++)
		{
			for (int z=grid->GetMinSecondAxis(); z<=grid->GetMaxSecondAxis(); z++)
			{
				const phStaticOctree * octree = otgrid->GetOctree(x,z)->GetOctree();
				const phBoundGeometry * geom = octree->GetGeom();
				if (octree)
				{
					totalOctCells += octree->CountNumCells();
					totalOctPolys += octree->CountNumPolys();
					if (geom)
					{
						totalSourcePolys += geom->GetNumPolygons();
						totalSourceVerts += geom->GetNumVertices();
					}
				}
			}
		}
		Displayf("total cells: %d",totalCells);
		Displayf("total polys: %d",totalPolys);
		Displayf("total source polys: %d",totalSourcePolys);
		Displayf("total source verts: %d",totalSourceVerts);


		// display memory usage
		int octCellMem = sizeof(phOctreeCell)*totalOctCells;
		int octPolyMem = sizeof(u16)*totalOctPolys;
		int sourcePolyMem = sizeof(phPolygon)*totalSourcePolys;
		int sourceVertMem = sizeof(Vector3)*totalSourceVerts;
		Displayf("\nMemory usage");
		Displayf("-- octrees --");
		Displayf("octree cell memory: %d",octCellMem);
		Displayf("octree poly memory: %d",octPolyMem);
		Displayf("-- source geometry --");
		Displayf("source poly memory: %d",sourcePolyMem);
		Displayf("source vert memory: %d",sourceVertMem);
		Displayf("---------------------");
		Displayf("total: >%.2fKB",(float)(octCellMem+octPolyMem+sourcePolyMem+sourceVertMem)/1024);
#endif
	}

	void ToggleDrawEntireGrid()
	{
		if (m_Bound->GetType()==phBound::OCTREEGRID)
		{
			if (m_DrawEntireGrid)
			{
#if __PFDRAW
				static_cast<phBoundGrid*>(m_Bound)->ClearDrawCell();
#endif
			}
			else
			{
				SetGridCell();
			}
		}
	}

	void SetGridCell()
	{
		m_DrawEntireGrid = false;
#if __PFDRAW
		phBoundGrid* grid = static_cast<phBoundGrid*>(m_Bound);
		int minX = grid->GetMinFirstAxis();
		int maxX = grid->GetMaxFirstAxis();
		int minZ = grid->GetMinSecondAxis();
		grid->SetDrawCell((m_GridCellZ - minZ) * (maxX - minX + 1) + (m_GridCellX - minX));
#endif
	}

	enum { NO_NORMALS, POLY_NORMALS, EDGE_NORMALS, POLY_EDGE_NORMALS, NUM_SHOW_NORMALS };	// for showNormals

	phBound* m_Bound;
	int m_ShowNormals;
	ioMapper m_Mapper;
	ioValue m_NormalsIoValue;
	Vector3 m_CameraTarget;

	ioValue m_NextCell;
	ioValue m_PrevCell;
	ioValue m_ClearCell;
	bool m_DrawEntireGrid;
	int m_GridCellX;
	int m_GridCellZ;

	bool m_PositionSpecified;
};

} // namespace ragesamples

// main application
int Main()
{
	ragesamples::viewBoundManager viewbound;
	viewbound.Init();

	viewbound.UpdateLoop();

	viewbound.Shutdown();

	return 0;
}
