﻿/*******************************************\
| ContentStar : Rockstar Games © 2013       |
| ------------------------------------------|
| Author: Thomas Woodhead (R* London)       |
| Email: thomas.woodhead@rockstarlondon.com |
\*******************************************/

#region Usings

using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

#endregion

namespace contentStar
{
    static class Program
    {
        #region Variables

        private static frmMain m_mainForm = null;
        private delegate void reloadDelegate();

        #endregion

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            m_mainForm = new frmMain();

            if (m_mainForm.Initialise())
                Application.Run(m_mainForm);

            m_mainForm.Shutdown();
        }

        #region Reloads

        public static void reloadGlobalFields()
        {
            if (getIsInvokedRequired())
            {
                reloadDelegate reloadDel = new reloadDelegate(m_mainForm.initialiseBankList);

                doInvoke(reloadDel);
            }
            else
                m_mainForm.initialiseBankList();
        }

        public static void reloadSolution()
        {
            if (getIsInvokedRequired())
            {
                reloadDelegate reloadDel = new reloadDelegate(m_mainForm.loadSolution);

                doInvoke(reloadDel);
            }
            else
                m_mainForm.loadSolution();
        }

        public static void reloadSettings()
        {
            
        }

        public static void reloadTypeMappings()
        {
            
        }

        #endregion

        #region UI Updates

        public static void onSelectedProjectChanged(Project previous, Project current)
        {
            m_mainForm.onSelectedProjectChanged(previous, current);
        }

        public static void updateUIFromDirtyState()
        {
            m_mainForm.updateUIFromDirtyState();
        }

        public static void decipherNodeImage(contentTreeNode node)
        {
            m_mainForm.decipherNodeImage(node);
        }

        #endregion

        #region Invokes

        public static void doInvoke(Delegate del, object obj)
        {
            m_mainForm.Invoke(del, obj);
        }

        public static void doInvoke(Delegate del)
        {
            m_mainForm.Invoke(del);
        }

        #endregion

        #region Gets

        public static Form getForm()
        {
            return m_mainForm;
        }

        public static bool getIsInvokedRequired()
        {
            return m_mainForm.InvokeRequired;
        }

        #endregion
    }
}
