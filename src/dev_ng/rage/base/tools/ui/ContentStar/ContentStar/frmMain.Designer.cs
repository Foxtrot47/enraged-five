﻿/*******************************************\
| ContentStar : Rockstar Games © 2013       |
| ------------------------------------------|
| Author: Thomas Woodhead (R* London)       |
| Email: thomas.woodhead@rockstarlondon.com |
\*******************************************/

namespace contentStar
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.spContMainHorz = new System.Windows.Forms.SplitContainer();
            this.spContTopVert = new System.Windows.Forms.SplitContainer();
            this.spContentTreeHorz = new System.Windows.Forms.SplitContainer();
            this.tvContentTree = new System.Windows.Forms.TreeView();
            this.ilContentTree = new System.Windows.Forms.ImageList(this.components);
            this.btnLogFilter = new System.Windows.Forms.Button();
            this.cboLogFilter = new System.Windows.Forms.ComboBox();
            this.cboSearchWord = new System.Windows.Forms.ComboBox();
            this.btnExpandAll = new System.Windows.Forms.Button();
            this.btnCollapseAll = new System.Windows.Forms.Button();
            this.cboSearchType = new System.Windows.Forms.ComboBox();
            this.tcAttributes = new System.Windows.Forms.TabControl();
            this.tabpbContent = new System.Windows.Forms.TabPage();
            this.txtBuildingProjs = new System.Windows.Forms.TextBox();
            this.lblBuildingProjs = new System.Windows.Forms.Label();
            this.txtProjRelPath = new System.Windows.Forms.TextBox();
            this.lblProjRelPath = new System.Windows.Forms.Label();
            this.txtBuildDependents = new System.Windows.Forms.TextBox();
            this.lblBuildDependents = new System.Windows.Forms.Label();
            this.txtBuildDeps = new System.Windows.Forms.TextBox();
            this.lblBuildDeps = new System.Windows.Forms.Label();
            this.txtBuildInclusions = new System.Windows.Forms.TextBox();
            this.lblBuilds = new System.Windows.Forms.Label();
            this.txtMBSize = new System.Windows.Forms.TextBox();
            this.txtKBSize = new System.Windows.Forms.TextBox();
            this.txtBSize = new System.Windows.Forms.TextBox();
            this.lblSize = new System.Windows.Forms.Label();
            this.txtContentName = new System.Windows.Forms.TextBox();
            this.lblContentName = new System.Windows.Forms.Label();
            this.txtFullPath = new System.Windows.Forms.TextBox();
            this.lblFullPath = new System.Windows.Forms.Label();
            this.tabpgGlobalFields = new System.Windows.Forms.TabPage();
            this.grpEditBank = new System.Windows.Forms.GroupBox();
            this.gbPS4GlobalFields = new System.Windows.Forms.GroupBox();
            this.txtPS4GFJPNContIDSecond = new System.Windows.Forms.TextBox();
            this.lblPS4GFJPNContIDHyp = new System.Windows.Forms.Label();
            this.txtPS4GFEntitlement = new System.Windows.Forms.TextBox();
            this.txtPS4GFJPNContIDFirst = new System.Windows.Forms.TextBox();
            this.lblPS4GFJNContID = new System.Windows.Forms.Label();
            this.txtPS4GFUSContIDFirst = new System.Windows.Forms.TextBox();
            this.lblPS4GFUSContID = new System.Windows.Forms.Label();
            this.txtPS4GFEUContIDSecond = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.lblPS4GFEUContIDHyp = new System.Windows.Forms.Label();
            this.txtPS4GFEUContIDFirst = new System.Windows.Forms.TextBox();
            this.lblPS4GFEUContID = new System.Windows.Forms.Label();
            this.txtPS4GFUSContIDSecond = new System.Windows.Forms.TextBox();
            this.lblPS4GFUSContIDHyp = new System.Windows.Forms.Label();
            this.grpGlobalsXbox = new System.Windows.Forms.GroupBox();
            this.lblGlobFieldTitleID = new System.Windows.Forms.Label();
            this.lblGlobFieldTitle = new System.Windows.Forms.Label();
            this.txtGlobalFieldGameTitle = new System.Windows.Forms.TextBox();
            this.txtGlobalFieldTitleID = new System.Windows.Forms.TextBox();
            this.grpGlobalsPs3 = new System.Windows.Forms.GroupBox();
            this.txtGlobalFieldJPNContentIDSecond = new System.Windows.Forms.TextBox();
            this.lblPS3GFJPNContIDHyp = new System.Windows.Forms.Label();
            this.txtGlobalFieldJPNContentIDFirst = new System.Windows.Forms.TextBox();
            this.lblGlobFieldJPNContID = new System.Windows.Forms.Label();
            this.txtGlobalFieldUSContentIDFirst = new System.Windows.Forms.TextBox();
            this.lblGlobFieldUSContID = new System.Windows.Forms.Label();
            this.txtGlobalFieldEUContentIDSecond = new System.Windows.Forms.TextBox();
            this.lblGlobFieldKLicensee = new System.Windows.Forms.Label();
            this.lblPS3GFEUContIDHyp = new System.Windows.Forms.Label();
            this.txtGlobalFieldKLicense = new System.Windows.Forms.TextBox();
            this.txtGlobalFieldEUContentIDFirst = new System.Windows.Forms.TextBox();
            this.lblGlobFieldEUContID = new System.Windows.Forms.Label();
            this.txtGlobalFieldUSContentIDSecond = new System.Windows.Forms.TextBox();
            this.lblPS3GFUSContIDHyp = new System.Windows.Forms.Label();
            this.btnSetCurrBank = new System.Windows.Forms.Button();
            this.btnDeleteBank = new System.Windows.Forms.Button();
            this.lblGlobalBank = new System.Windows.Forms.Label();
            this.btnAddNewBank = new System.Windows.Forms.Button();
            this.cboBankName = new System.Windows.Forms.ComboBox();
            this.grpGlobalBanks = new System.Windows.Forms.GroupBox();
            this.lblCurrentBank = new System.Windows.Forms.Label();
            this.txtCurrGlobalBank = new System.Windows.Forms.TextBox();
            this.tabpgXbox = new System.Windows.Forms.TabPage();
            this.pbXboxTitleImg = new System.Windows.Forms.PictureBox();
            this.chkConsumableOffer = new System.Windows.Forms.CheckBox();
            this.chkRestrictedLicense = new System.Windows.Forms.CheckBox();
            this.cboVisibility = new System.Windows.Forms.ComboBox();
            this.lblVisibility = new System.Windows.Forms.Label();
            this.txtOfferId = new System.Windows.Forms.TextBox();
            this.lblOfferId = new System.Windows.Forms.Label();
            this.txtPrevOfferId = new System.Windows.Forms.TextBox();
            this.lblPrevOfferId = new System.Windows.Forms.Label();
            this.txtContentCat = new System.Windows.Forms.TextBox();
            this.lblContentCat = new System.Windows.Forms.Label();
            this.txtLicenseMask = new System.Windows.Forms.TextBox();
            this.lblLicenseMask = new System.Windows.Forms.Label();
            this.chkDevTrans = new System.Windows.Forms.CheckBox();
            this.chkProfTrans = new System.Windows.Forms.CheckBox();
            this.chkIntForMktPlc = new System.Windows.Forms.CheckBox();
            this.chkHasCost = new System.Windows.Forms.CheckBox();
            this.dtPickerXboxActDate = new System.Windows.Forms.DateTimePicker();
            this.txtXboxPubFlags = new System.Windows.Forms.TextBox();
            this.btnBrowseXboxDBIcon = new System.Windows.Forms.Button();
            this.btnBrowseXboxTitleImg = new System.Windows.Forms.Button();
            this.btnBrowseXboxOfferBan = new System.Windows.Forms.Button();
            this.pbXboxDBIcon = new System.Windows.Forms.PictureBox();
            this.pbXboxOffBan = new System.Windows.Forms.PictureBox();
            this.lblXboxOffBan = new System.Windows.Forms.Label();
            this.txtXboxOffBan = new System.Windows.Forms.TextBox();
            this.lblXboxPkgName = new System.Windows.Forms.Label();
            this.txtXboxPkgName = new System.Windows.Forms.TextBox();
            this.lblXboxTitleImg = new System.Windows.Forms.Label();
            this.txtXboxTitleID = new System.Windows.Forms.TextBox();
            this.txtXboxTitleImage = new System.Windows.Forms.TextBox();
            this.lblXboxTitleID = new System.Windows.Forms.Label();
            this.lblXboxDBIcon = new System.Windows.Forms.Label();
            this.txtXboxGameTitle = new System.Windows.Forms.TextBox();
            this.txtXboxDBIcon = new System.Windows.Forms.TextBox();
            this.lblXboxGameTitle = new System.Windows.Forms.Label();
            this.lblXboxActDate = new System.Windows.Forms.Label();
            this.txtXboxOffID = new System.Windows.Forms.TextBox();
            this.lblXboxOffID = new System.Windows.Forms.Label();
            this.lblXboxPubFlags = new System.Windows.Forms.Label();
            this.tabpgPS3 = new System.Windows.Forms.TabPage();
            this.txtPs3JNVer = new System.Windows.Forms.TextBox();
            this.lblPs3JNVer = new System.Windows.Forms.Label();
            this.txtPs3EUVer = new System.Windows.Forms.TextBox();
            this.lblPs3EUVer = new System.Windows.Forms.Label();
            this.txtPs3USVer = new System.Windows.Forms.TextBox();
            this.lblUSVer = new System.Windows.Forms.Label();
            this.btnBrowsePs3JPNParamSfo = new System.Windows.Forms.Button();
            this.txtPs3JPNParamSfo = new System.Windows.Forms.TextBox();
            this.lblPs3JPNParamSfo = new System.Windows.Forms.Label();
            this.btnBrowsePs3EUParamSfo = new System.Windows.Forms.Button();
            this.txtPs3EUParamSfo = new System.Windows.Forms.TextBox();
            this.lblPs3EUParamSfo = new System.Windows.Forms.Label();
            this.lblPs3JPNContIDZero = new System.Windows.Forms.Label();
            this.txtPs3JPNCID_Third = new System.Windows.Forms.TextBox();
            this.lblPs3JPNContIDHyp = new System.Windows.Forms.Label();
            this.txtPs3JPNCID_Second = new System.Windows.Forms.TextBox();
            this.lblPs3JPNContIDSep = new System.Windows.Forms.Label();
            this.txtPs3JPNCID_First = new System.Windows.Forms.TextBox();
            this.lblPs3EUContIDZero = new System.Windows.Forms.Label();
            this.txtPs3EUCID_Third = new System.Windows.Forms.TextBox();
            this.lblPs3EUContIDHyp = new System.Windows.Forms.Label();
            this.txtPs3EUCID_Second = new System.Windows.Forms.TextBox();
            this.lblPs3EUContIDSep = new System.Windows.Forms.Label();
            this.txtPs3EUCID_First = new System.Windows.Forms.TextBox();
            this.lblPs3USContIDZero = new System.Windows.Forms.Label();
            this.txtPs3USCID_Third = new System.Windows.Forms.TextBox();
            this.lblPs3USContIDHyp = new System.Windows.Forms.Label();
            this.txtPs3USCID_Second = new System.Windows.Forms.TextBox();
            this.lblPs3USContIDSep = new System.Windows.Forms.Label();
            this.txtPs3USCID_First = new System.Windows.Forms.TextBox();
            this.lblPs3JPNContID = new System.Windows.Forms.Label();
            this.lblPs3EUContID = new System.Windows.Forms.Label();
            this.btnBrowsePs3USParamSfo = new System.Windows.Forms.Button();
            this.btnBrowsePs3Icon0 = new System.Windows.Forms.Button();
            this.txtPs3Icon0 = new System.Windows.Forms.TextBox();
            this.lblPs3Icon0 = new System.Windows.Forms.Label();
            this.txtPs3USParamSfo = new System.Windows.Forms.TextBox();
            this.lblPs3USParamSfo = new System.Windows.Forms.Label();
            this.pbPs3Banner = new System.Windows.Forms.PictureBox();
            this.txtPs3ContType = new System.Windows.Forms.TextBox();
            this.lblPs3ContType = new System.Windows.Forms.Label();
            this.txtPs3DRMType = new System.Windows.Forms.TextBox();
            this.lblPs3DRMType = new System.Windows.Forms.Label();
            this.txtPs3KLicensee = new System.Windows.Forms.TextBox();
            this.lblKLicensee = new System.Windows.Forms.Label();
            this.lblPs3USContID = new System.Windows.Forms.Label();
            this.tabpgPC = new System.Windows.Forms.TabPage();
            this.tbPS4 = new System.Windows.Forms.TabPage();
            this.txtPS4VolType = new System.Windows.Forms.TextBox();
            this.lblPS4VolType = new System.Windows.Forms.Label();
            this.btnPS4CodeGen = new System.Windows.Forms.Button();
            this.txtPS4VerJPN = new System.Windows.Forms.TextBox();
            this.lblPS4JNVer = new System.Windows.Forms.Label();
            this.txtPS4VerEU = new System.Windows.Forms.TextBox();
            this.lblPS4EUVer = new System.Windows.Forms.Label();
            this.txtPS4VerUS = new System.Windows.Forms.TextBox();
            this.lblPS4USVer = new System.Windows.Forms.Label();
            this.btnPS4JPNParamSfo = new System.Windows.Forms.Button();
            this.txtPS4JPNParamSfo = new System.Windows.Forms.TextBox();
            this.lblPS4JNParam = new System.Windows.Forms.Label();
            this.btnPS4EUParamSfo = new System.Windows.Forms.Button();
            this.txtPS4EUParamSfo = new System.Windows.Forms.TextBox();
            this.lblPS4EUParam = new System.Windows.Forms.Label();
            this.lblPS4JNContIDZero = new System.Windows.Forms.Label();
            this.txtPS4JPNContID_Third = new System.Windows.Forms.TextBox();
            this.lblPS4JPNContIDHyp = new System.Windows.Forms.Label();
            this.txtPS4JPNContID_Second = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtPS4JPNContID_First = new System.Windows.Forms.TextBox();
            this.lblPS4EUContIDZero = new System.Windows.Forms.Label();
            this.txtPS4EUContID_Third = new System.Windows.Forms.TextBox();
            this.lblPS4EUContIDHyp = new System.Windows.Forms.Label();
            this.txtPS4EUContID_Second = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtPS4EUContID_First = new System.Windows.Forms.TextBox();
            this.lblPS4USContIDZero = new System.Windows.Forms.Label();
            this.txtPS4USContID_Third = new System.Windows.Forms.TextBox();
            this.lblPS4USContIDHyp = new System.Windows.Forms.Label();
            this.txtPS4USContID_Second = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtPS4USContID_First = new System.Windows.Forms.TextBox();
            this.lblPS4JPNContID = new System.Windows.Forms.Label();
            this.lblPS4EUContID = new System.Windows.Forms.Label();
            this.btnPS4USParamSfo = new System.Windows.Forms.Button();
            this.txtPS4Icon0Browse = new System.Windows.Forms.Button();
            this.txtPS4Icon0 = new System.Windows.Forms.TextBox();
            this.lblPS4Icon = new System.Windows.Forms.Label();
            this.txtPS4USParamSfo = new System.Windows.Forms.TextBox();
            this.lblPS4USParam = new System.Windows.Forms.Label();
            this.pbPS4Icon0 = new System.Windows.Forms.PictureBox();
            this.txtPS4Code = new System.Windows.Forms.TextBox();
            this.lblPS4Passcode = new System.Windows.Forms.Label();
            this.txtPS4Entitlement = new System.Windows.Forms.TextBox();
            this.lblPS4Entitlement = new System.Windows.Forms.Label();
            this.lblPS4USContID = new System.Windows.Forms.Label();
            this.tbXboxOne = new System.Windows.Forms.TabPage();
            this.grpXB1AppManifest = new System.Windows.Forms.GroupBox();
            this.grpXB1ProductID = new System.Windows.Forms.GroupBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtXB1AllowedProductIDJPN = new System.Windows.Forms.TextBox();
            this.rbXB1Release = new System.Windows.Forms.RadioButton();
            this.rbXB1Dev = new System.Windows.Forms.RadioButton();
            this.txtXB1AllowedProductID = new System.Windows.Forms.TextBox();
            this.pbXB1WideLogo = new System.Windows.Forms.PictureBox();
            this.pbXB1SmallLogo = new System.Windows.Forms.PictureBox();
            this.pbXB1LiveLogo = new System.Windows.Forms.PictureBox();
            this.pbXB1StoreLogo = new System.Windows.Forms.PictureBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.grpVisualElements = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtXB1VWideLogo = new System.Windows.Forms.TextBox();
            this.btnXB1WideLogo = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.txtXB1VSmallLogo = new System.Windows.Forms.TextBox();
            this.btnXB1SmallLogo = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.txtXB1VLiveLogo = new System.Windows.Forms.TextBox();
            this.btnXB1LiveLogo = new System.Windows.Forms.Button();
            this.txtXB1VDisplayName = new System.Windows.Forms.TextBox();
            this.lbXB1VDisplayName = new System.Windows.Forms.Label();
            this.lbXB1VDesc = new System.Windows.Forms.Label();
            this.txtXB1VDescription = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.grpXB1Properties = new System.Windows.Forms.GroupBox();
            this.lbXB1StoreLogo = new System.Windows.Forms.Label();
            this.txtXB1StoreLogo = new System.Windows.Forms.TextBox();
            this.btnXB1StoreLogo = new System.Windows.Forms.Button();
            this.txtXB1PDescription = new System.Windows.Forms.TextBox();
            this.lbXB1PDescription = new System.Windows.Forms.Label();
            this.txtXB1PPublisherName = new System.Windows.Forms.TextBox();
            this.lbXB1PublisherDname = new System.Windows.Forms.Label();
            this.txtXB1PDisplayName = new System.Windows.Forms.TextBox();
            this.lbXB1PDisplayName = new System.Windows.Forms.Label();
            this.grpChunks = new System.Windows.Forms.GroupBox();
            this.btnXB1SaveChunksFile = new System.Windows.Forms.Button();
            this.btXB1EditChunkFile = new System.Windows.Forms.Button();
            this.btnXB1LoadChunkFile = new System.Windows.Forms.Button();
            this.btnXB1GenerateChunk = new System.Windows.Forms.Button();
            this.grpXB1Identity = new System.Windows.Forms.GroupBox();
            this.txtXB1IdentityName = new System.Windows.Forms.TextBox();
            this.txtXB1IdentityVersion = new System.Windows.Forms.TextBox();
            this.lbXB1IName = new System.Windows.Forms.Label();
            this.lbXB1IVersion = new System.Windows.Forms.Label();
            this.toolStripMain = new System.Windows.Forms.ToolStrip();
            this.tsbBuildAll = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbBuildXbox = new System.Windows.Forms.ToolStripButton();
            this.tsbBuildPs3 = new System.Windows.Forms.ToolStripButton();
            this.tsbBuildPC = new System.Windows.Forms.ToolStripButton();
            this.tsbBuildPS4 = new System.Windows.Forms.ToolStripButton();
            this.tsbBuildXbone = new System.Windows.Forms.ToolStripButton();
            this.tsbBuildShippablePack = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbBuildCloud = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbOpenContentFolder = new System.Windows.Forms.ToolStripButton();
            this.tsbGotoConfigFiles = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbBuildSetupAndContent = new System.Windows.Forms.ToolStripButton();
            this.tsbBuildXMLs = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbOptions = new System.Windows.Forms.ToolStripButton();
            this.rtbOutput = new System.Windows.Forms.RichTextBox();
            this.ofdAddFile = new System.Windows.Forms.OpenFileDialog();
            this.fbdAddFolder = new System.Windows.Forms.FolderBrowserDialog();
            this.ofdOpenSolution = new System.Windows.Forms.OpenFileDialog();
            this.sfdSolution = new System.Windows.Forms.SaveFileDialog();
            this.ctxtmnuOutputWnd = new System.Windows.Forms.ContextMenu();
            this.mnuOutWndClearCur = new System.Windows.Forms.MenuItem();
            this.menuItem7 = new System.Windows.Forms.MenuItem();
            this.mnuOutWndClearAll = new System.Windows.Forms.MenuItem();
            this.ofdContentData = new System.Windows.Forms.OpenFileDialog();
            this.ttGeneric = new System.Windows.Forms.ToolTip(this.components);
            this.sfdProject = new System.Windows.Forms.SaveFileDialog();
            this.ofdProject = new System.Windows.Forms.OpenFileDialog();
            this.ctxtmnuTvNode = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.ctxtToggleBuildStatus = new System.Windows.Forms.ToolStripMenuItem();
            this.ctxtBuildAll = new System.Windows.Forms.ToolStripMenuItem();
            this.ctxtBuildNone = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator9 = new System.Windows.Forms.ToolStripSeparator();
            this.ctxtToggleThisOnly = new System.Windows.Forms.ToolStripMenuItem();
            this.ctxtBuildThisOnly = new System.Windows.Forms.ToolStripMenuItem();
            this.ctxtBuildAllExceptThis = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.ctxtOpenExpHere = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuMainBar = new System.Windows.Forms.MainMenu(this.components);
            this.mnuFileOpt = new System.Windows.Forms.MenuItem();
            this.mnuRefresh = new System.Windows.Forms.MenuItem();
            this.menuItem2 = new System.Windows.Forms.MenuItem();
            this.mnuSaveOpt = new System.Windows.Forms.MenuItem();
            this.menuItem5 = new System.Windows.Forms.MenuItem();
            this.mnuExitOpt = new System.Windows.Forms.MenuItem();
            this.mnuBuildOpt = new System.Windows.Forms.MenuItem();
            this.mnuBuildAllOpt = new System.Windows.Forms.MenuItem();
            this.menuItem3 = new System.Windows.Forms.MenuItem();
            this.mnuBuildXBOXOpt = new System.Windows.Forms.MenuItem();
            this.mnuBuildPS3Opt = new System.Windows.Forms.MenuItem();
            this.mnuBuildPCOpt = new System.Windows.Forms.MenuItem();
            this.menuItem4 = new System.Windows.Forms.MenuItem();
            this.mnuBuildCloudOpt = new System.Windows.Forms.MenuItem();
            this.menuItem6 = new System.Windows.Forms.MenuItem();
            this.mnuClean = new System.Windows.Forms.MenuItem();
            this.mnuHelpOpt = new System.Windows.Forms.MenuItem();
            this.mnuWikiOpt = new System.Windows.Forms.MenuItem();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.chkReleaseBuild = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.spContMainHorz)).BeginInit();
            this.spContMainHorz.Panel1.SuspendLayout();
            this.spContMainHorz.Panel2.SuspendLayout();
            this.spContMainHorz.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spContTopVert)).BeginInit();
            this.spContTopVert.Panel1.SuspendLayout();
            this.spContTopVert.Panel2.SuspendLayout();
            this.spContTopVert.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spContentTreeHorz)).BeginInit();
            this.spContentTreeHorz.Panel1.SuspendLayout();
            this.spContentTreeHorz.Panel2.SuspendLayout();
            this.spContentTreeHorz.SuspendLayout();
            this.tcAttributes.SuspendLayout();
            this.tabpbContent.SuspendLayout();
            this.tabpgGlobalFields.SuspendLayout();
            this.grpEditBank.SuspendLayout();
            this.gbPS4GlobalFields.SuspendLayout();
            this.grpGlobalsXbox.SuspendLayout();
            this.grpGlobalsPs3.SuspendLayout();
            this.grpGlobalBanks.SuspendLayout();
            this.tabpgXbox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbXboxTitleImg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbXboxDBIcon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbXboxOffBan)).BeginInit();
            this.tabpgPS3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbPs3Banner)).BeginInit();
            this.tbPS4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbPS4Icon0)).BeginInit();
            this.tbXboxOne.SuspendLayout();
            this.grpXB1AppManifest.SuspendLayout();
            this.grpXB1ProductID.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbXB1WideLogo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbXB1SmallLogo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbXB1LiveLogo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbXB1StoreLogo)).BeginInit();
            this.grpVisualElements.SuspendLayout();
            this.grpXB1Properties.SuspendLayout();
            this.grpChunks.SuspendLayout();
            this.grpXB1Identity.SuspendLayout();
            this.toolStripMain.SuspendLayout();
            this.ctxtmnuTvNode.SuspendLayout();
            this.SuspendLayout();
            // 
            // spContMainHorz
            // 
            this.spContMainHorz.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.spContMainHorz.Dock = System.Windows.Forms.DockStyle.Fill;
            this.spContMainHorz.Location = new System.Drawing.Point(0, 0);
            this.spContMainHorz.Name = "spContMainHorz";
            this.spContMainHorz.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // spContMainHorz.Panel1
            // 
            this.spContMainHorz.Panel1.Controls.Add(this.spContTopVert);
            // 
            // spContMainHorz.Panel2
            // 
            this.spContMainHorz.Panel2.Controls.Add(this.rtbOutput);
            this.spContMainHorz.Size = new System.Drawing.Size(1264, 682);
            this.spContMainHorz.SplitterDistance = 502;
            this.spContMainHorz.TabIndex = 0;
            this.spContMainHorz.TabStop = false;
            this.spContMainHorz.SplitterMoved += new System.Windows.Forms.SplitterEventHandler(this.spContMainHorz_SplitterMoved);
            // 
            // spContTopVert
            // 
            this.spContTopVert.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.spContTopVert.Dock = System.Windows.Forms.DockStyle.Fill;
            this.spContTopVert.Location = new System.Drawing.Point(0, 0);
            this.spContTopVert.Name = "spContTopVert";
            // 
            // spContTopVert.Panel1
            // 
            this.spContTopVert.Panel1.Controls.Add(this.spContentTreeHorz);
            // 
            // spContTopVert.Panel2
            // 
            this.spContTopVert.Panel2.Controls.Add(this.tcAttributes);
            this.spContTopVert.Panel2.Controls.Add(this.toolStripMain);
            this.spContTopVert.Size = new System.Drawing.Size(1264, 502);
            this.spContTopVert.SplitterDistance = 338;
            this.spContTopVert.TabIndex = 0;
            this.spContTopVert.TabStop = false;
            this.spContTopVert.SplitterMoved += new System.Windows.Forms.SplitterEventHandler(this.spContTopVert_SplitterMoved);
            // 
            // spContentTreeHorz
            // 
            this.spContentTreeHorz.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.spContentTreeHorz.Dock = System.Windows.Forms.DockStyle.Fill;
            this.spContentTreeHorz.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.spContentTreeHorz.IsSplitterFixed = true;
            this.spContentTreeHorz.Location = new System.Drawing.Point(0, 0);
            this.spContentTreeHorz.Name = "spContentTreeHorz";
            this.spContentTreeHorz.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // spContentTreeHorz.Panel1
            // 
            this.spContentTreeHorz.Panel1.Controls.Add(this.tvContentTree);
            // 
            // spContentTreeHorz.Panel2
            // 
            this.spContentTreeHorz.Panel2.Controls.Add(this.btnLogFilter);
            this.spContentTreeHorz.Panel2.Controls.Add(this.cboLogFilter);
            this.spContentTreeHorz.Panel2.Controls.Add(this.cboSearchWord);
            this.spContentTreeHorz.Panel2.Controls.Add(this.btnExpandAll);
            this.spContentTreeHorz.Panel2.Controls.Add(this.btnCollapseAll);
            this.spContentTreeHorz.Panel2.Controls.Add(this.cboSearchType);
            this.spContentTreeHorz.Panel2MinSize = 60;
            this.spContentTreeHorz.Size = new System.Drawing.Size(338, 502);
            this.spContentTreeHorz.SplitterDistance = 409;
            this.spContentTreeHorz.TabIndex = 1;
            this.spContentTreeHorz.TabStop = false;
            // 
            // tvContentTree
            // 
            this.tvContentTree.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tvContentTree.HideSelection = false;
            this.tvContentTree.ImageIndex = 0;
            this.tvContentTree.ImageList = this.ilContentTree;
            this.tvContentTree.Location = new System.Drawing.Point(0, 0);
            this.tvContentTree.Name = "tvContentTree";
            this.tvContentTree.SelectedImageIndex = 0;
            this.tvContentTree.ShowNodeToolTips = true;
            this.tvContentTree.Size = new System.Drawing.Size(334, 405);
            this.tvContentTree.TabIndex = 0;
            this.tvContentTree.TabStop = false;
            this.tvContentTree.AfterExpand += new System.Windows.Forms.TreeViewEventHandler(this.tvContentTree_AfterExpand);
            this.tvContentTree.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.tvContentTree_AfterSelect);
            this.tvContentTree.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.tvContentTree_NodeMouseClick);
            this.tvContentTree.DoubleClick += new System.EventHandler(this.tvContentTree_DoubleClick);
            // 
            // ilContentTree
            // 
            this.ilContentTree.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ilContentTree.ImageStream")));
            this.ilContentTree.TransparentColor = System.Drawing.Color.Transparent;
            this.ilContentTree.Images.SetKeyName(0, "inBuild");
            this.ilContentTree.Images.SetKeyName(1, "notInBuild");
            this.ilContentTree.Images.SetKeyName(2, "dbRoot");
            // 
            // btnLogFilter
            // 
            this.btnLogFilter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLogFilter.Location = new System.Drawing.Point(252, 59);
            this.btnLogFilter.Name = "btnLogFilter";
            this.btnLogFilter.Size = new System.Drawing.Size(79, 23);
            this.btnLogFilter.TabIndex = 6;
            this.btnLogFilter.Text = "Filter Log";
            this.ttGeneric.SetToolTip(this.btnLogFilter, "Expand all nodes");
            this.btnLogFilter.UseVisualStyleBackColor = true;
            this.btnLogFilter.Click += new System.EventHandler(this.btnLogFilter_Click);
            // 
            // cboLogFilter
            // 
            this.cboLogFilter.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cboLogFilter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboLogFilter.FormattingEnabled = true;
            this.cboLogFilter.Location = new System.Drawing.Point(11, 62);
            this.cboLogFilter.Name = "cboLogFilter";
            this.cboLogFilter.Size = new System.Drawing.Size(236, 21);
            this.cboLogFilter.TabIndex = 5;
            // 
            // cboSearchWord
            // 
            this.cboSearchWord.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cboSearchWord.DropDownWidth = 213;
            this.cboSearchWord.FormattingEnabled = true;
            this.cboSearchWord.Location = new System.Drawing.Point(118, 6);
            this.cboSearchWord.Name = "cboSearchWord";
            this.cboSearchWord.Size = new System.Drawing.Size(213, 21);
            this.cboSearchWord.TabIndex = 2;
            this.cboSearchWord.SelectionChangeCommitted += new System.EventHandler(this.cboSearchWord_SelectionChangeCommitted);
            this.cboSearchWord.TextChanged += new System.EventHandler(this.cboSearchWord_TextChanged);
            // 
            // btnExpandAll
            // 
            this.btnExpandAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExpandAll.Location = new System.Drawing.Point(252, 32);
            this.btnExpandAll.Name = "btnExpandAll";
            this.btnExpandAll.Size = new System.Drawing.Size(79, 23);
            this.btnExpandAll.TabIndex = 4;
            this.btnExpandAll.Text = "Expand All";
            this.ttGeneric.SetToolTip(this.btnExpandAll, "Expand all nodes");
            this.btnExpandAll.UseVisualStyleBackColor = true;
            this.btnExpandAll.Click += new System.EventHandler(this.btnExpandAll_Click);
            // 
            // btnCollapseAll
            // 
            this.btnCollapseAll.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCollapseAll.Location = new System.Drawing.Point(10, 32);
            this.btnCollapseAll.Name = "btnCollapseAll";
            this.btnCollapseAll.Size = new System.Drawing.Size(236, 23);
            this.btnCollapseAll.TabIndex = 3;
            this.btnCollapseAll.Text = "Collapse All";
            this.ttGeneric.SetToolTip(this.btnCollapseAll, "Collapse all project nodes");
            this.btnCollapseAll.UseVisualStyleBackColor = true;
            this.btnCollapseAll.Click += new System.EventHandler(this.btnCollapseAll_Click);
            // 
            // cboSearchType
            // 
            this.cboSearchType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboSearchType.FormattingEnabled = true;
            this.cboSearchType.Items.AddRange(new object[] {
            "Projects",
            "Folders",
            "Files"});
            this.cboSearchType.Location = new System.Drawing.Point(10, 6);
            this.cboSearchType.Name = "cboSearchType";
            this.cboSearchType.Size = new System.Drawing.Size(102, 21);
            this.cboSearchType.TabIndex = 1;
            this.ttGeneric.SetToolTip(this.cboSearchType, "Select search filter");
            this.cboSearchType.SelectionChangeCommitted += new System.EventHandler(this.cboSearchType_SelectionChangeCommitted);
            // 
            // tcAttributes
            // 
            this.tcAttributes.Controls.Add(this.tabpbContent);
            this.tcAttributes.Controls.Add(this.tabpgGlobalFields);
            this.tcAttributes.Controls.Add(this.tabpgXbox);
            this.tcAttributes.Controls.Add(this.tabpgPS3);
            this.tcAttributes.Controls.Add(this.tabpgPC);
            this.tcAttributes.Controls.Add(this.tbPS4);
            this.tcAttributes.Controls.Add(this.tbXboxOne);
            this.tcAttributes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tcAttributes.Location = new System.Drawing.Point(0, 25);
            this.tcAttributes.Name = "tcAttributes";
            this.tcAttributes.SelectedIndex = 0;
            this.tcAttributes.Size = new System.Drawing.Size(918, 473);
            this.tcAttributes.TabIndex = 3;
            // 
            // tabpbContent
            // 
            this.tabpbContent.Controls.Add(this.chkReleaseBuild);
            this.tabpbContent.Controls.Add(this.txtBuildingProjs);
            this.tabpbContent.Controls.Add(this.lblBuildingProjs);
            this.tabpbContent.Controls.Add(this.txtProjRelPath);
            this.tabpbContent.Controls.Add(this.lblProjRelPath);
            this.tabpbContent.Controls.Add(this.txtBuildDependents);
            this.tabpbContent.Controls.Add(this.lblBuildDependents);
            this.tabpbContent.Controls.Add(this.txtBuildDeps);
            this.tabpbContent.Controls.Add(this.lblBuildDeps);
            this.tabpbContent.Controls.Add(this.txtBuildInclusions);
            this.tabpbContent.Controls.Add(this.lblBuilds);
            this.tabpbContent.Controls.Add(this.txtMBSize);
            this.tabpbContent.Controls.Add(this.txtKBSize);
            this.tabpbContent.Controls.Add(this.txtBSize);
            this.tabpbContent.Controls.Add(this.lblSize);
            this.tabpbContent.Controls.Add(this.txtContentName);
            this.tabpbContent.Controls.Add(this.lblContentName);
            this.tabpbContent.Controls.Add(this.txtFullPath);
            this.tabpbContent.Controls.Add(this.lblFullPath);
            this.tabpbContent.Location = new System.Drawing.Point(4, 22);
            this.tabpbContent.Name = "tabpbContent";
            this.tabpbContent.Padding = new System.Windows.Forms.Padding(3);
            this.tabpbContent.Size = new System.Drawing.Size(910, 447);
            this.tabpbContent.TabIndex = 1;
            this.tabpbContent.Text = "Content Attributes";
            this.tabpbContent.UseVisualStyleBackColor = true;
            // 
            // txtBuildingProjs
            // 
            this.txtBuildingProjs.Location = new System.Drawing.Point(85, 188);
            this.txtBuildingProjs.Multiline = true;
            this.txtBuildingProjs.Name = "txtBuildingProjs";
            this.txtBuildingProjs.ReadOnly = true;
            this.txtBuildingProjs.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtBuildingProjs.Size = new System.Drawing.Size(185, 234);
            this.txtBuildingProjs.TabIndex = 23;
            this.ttGeneric.SetToolTip(this.txtBuildingProjs, "Projects that are included in the build");
            // 
            // lblBuildingProjs
            // 
            this.lblBuildingProjs.AutoSize = true;
            this.lblBuildingProjs.Location = new System.Drawing.Point(32, 191);
            this.lblBuildingProjs.Name = "lblBuildingProjs";
            this.lblBuildingProjs.Size = new System.Drawing.Size(47, 13);
            this.lblBuildingProjs.TabIndex = 24;
            this.lblBuildingProjs.Text = "Building:";
            this.ttGeneric.SetToolTip(this.lblBuildingProjs, "Builds included in");
            // 
            // txtProjRelPath
            // 
            this.txtProjRelPath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtProjRelPath.Location = new System.Drawing.Point(85, 58);
            this.txtProjRelPath.Name = "txtProjRelPath";
            this.txtProjRelPath.ReadOnly = true;
            this.txtProjRelPath.Size = new System.Drawing.Size(818, 20);
            this.txtProjRelPath.TabIndex = 21;
            this.ttGeneric.SetToolTip(this.txtProjRelPath, "Full path of file/folder");
            // 
            // lblProjRelPath
            // 
            this.lblProjRelPath.AutoSize = true;
            this.lblProjRelPath.Location = new System.Drawing.Point(11, 61);
            this.lblProjRelPath.Name = "lblProjRelPath";
            this.lblProjRelPath.Size = new System.Drawing.Size(68, 13);
            this.lblProjRelPath.TabIndex = 22;
            this.lblProjRelPath.Text = "Project Path:";
            this.ttGeneric.SetToolTip(this.lblProjRelPath, "Full path of file/folder");
            // 
            // txtBuildDependents
            // 
            this.txtBuildDependents.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtBuildDependents.Location = new System.Drawing.Point(85, 136);
            this.txtBuildDependents.Name = "txtBuildDependents";
            this.txtBuildDependents.ReadOnly = true;
            this.txtBuildDependents.Size = new System.Drawing.Size(449, 20);
            this.txtBuildDependents.TabIndex = 19;
            this.ttGeneric.SetToolTip(this.txtBuildDependents, "Full path of file/folder");
            // 
            // lblBuildDependents
            // 
            this.lblBuildDependents.AutoSize = true;
            this.lblBuildDependents.Location = new System.Drawing.Point(11, 139);
            this.lblBuildDependents.Name = "lblBuildDependents";
            this.lblBuildDependents.Size = new System.Drawing.Size(68, 13);
            this.lblBuildDependents.TabIndex = 20;
            this.lblBuildDependents.Text = "Dependents:";
            this.ttGeneric.SetToolTip(this.lblBuildDependents, "Builds included in");
            // 
            // txtBuildDeps
            // 
            this.txtBuildDeps.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtBuildDeps.Location = new System.Drawing.Point(85, 110);
            this.txtBuildDeps.Name = "txtBuildDeps";
            this.txtBuildDeps.ReadOnly = true;
            this.txtBuildDeps.Size = new System.Drawing.Size(449, 20);
            this.txtBuildDeps.TabIndex = 7;
            this.ttGeneric.SetToolTip(this.txtBuildDeps, "Full path of file/folder");
            // 
            // lblBuildDeps
            // 
            this.lblBuildDeps.AutoSize = true;
            this.lblBuildDeps.Location = new System.Drawing.Point(0, 113);
            this.lblBuildDeps.Name = "lblBuildDeps";
            this.lblBuildDeps.Size = new System.Drawing.Size(79, 13);
            this.lblBuildDeps.TabIndex = 18;
            this.lblBuildDeps.Text = "Dependencies:";
            this.ttGeneric.SetToolTip(this.lblBuildDeps, "Builds included in");
            // 
            // txtBuildInclusions
            // 
            this.txtBuildInclusions.Location = new System.Drawing.Point(85, 162);
            this.txtBuildInclusions.Name = "txtBuildInclusions";
            this.txtBuildInclusions.ReadOnly = true;
            this.txtBuildInclusions.Size = new System.Drawing.Size(185, 20);
            this.txtBuildInclusions.TabIndex = 6;
            this.ttGeneric.SetToolTip(this.txtBuildInclusions, "Builds included in");
            // 
            // lblBuilds
            // 
            this.lblBuilds.AutoSize = true;
            this.lblBuilds.Location = new System.Drawing.Point(41, 165);
            this.lblBuilds.Name = "lblBuilds";
            this.lblBuilds.Size = new System.Drawing.Size(38, 13);
            this.lblBuilds.TabIndex = 14;
            this.lblBuilds.Text = "Builds:";
            this.ttGeneric.SetToolTip(this.lblBuilds, "Builds included in");
            // 
            // txtMBSize
            // 
            this.txtMBSize.Location = new System.Drawing.Point(409, 84);
            this.txtMBSize.Name = "txtMBSize";
            this.txtMBSize.ReadOnly = true;
            this.txtMBSize.Size = new System.Drawing.Size(125, 20);
            this.txtMBSize.TabIndex = 5;
            // 
            // txtKBSize
            // 
            this.txtKBSize.Location = new System.Drawing.Point(276, 84);
            this.txtKBSize.Name = "txtKBSize";
            this.txtKBSize.ReadOnly = true;
            this.txtKBSize.Size = new System.Drawing.Size(127, 20);
            this.txtKBSize.TabIndex = 4;
            // 
            // txtBSize
            // 
            this.txtBSize.Location = new System.Drawing.Point(85, 84);
            this.txtBSize.Name = "txtBSize";
            this.txtBSize.ReadOnly = true;
            this.txtBSize.Size = new System.Drawing.Size(185, 20);
            this.txtBSize.TabIndex = 3;
            // 
            // lblSize
            // 
            this.lblSize.AutoSize = true;
            this.lblSize.Location = new System.Drawing.Point(49, 87);
            this.lblSize.Name = "lblSize";
            this.lblSize.Size = new System.Drawing.Size(30, 13);
            this.lblSize.TabIndex = 10;
            this.lblSize.Text = "Size:";
            // 
            // txtContentName
            // 
            this.txtContentName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtContentName.Location = new System.Drawing.Point(85, 6);
            this.txtContentName.Name = "txtContentName";
            this.txtContentName.ReadOnly = true;
            this.txtContentName.Size = new System.Drawing.Size(819, 20);
            this.txtContentName.TabIndex = 1;
            this.ttGeneric.SetToolTip(this.txtContentName, "Target name of file/folder");
            // 
            // lblContentName
            // 
            this.lblContentName.AutoSize = true;
            this.lblContentName.Location = new System.Drawing.Point(41, 9);
            this.lblContentName.Name = "lblContentName";
            this.lblContentName.Size = new System.Drawing.Size(38, 13);
            this.lblContentName.TabIndex = 5;
            this.lblContentName.Text = "Name:";
            this.ttGeneric.SetToolTip(this.lblContentName, "Target name of file/folder");
            // 
            // txtFullPath
            // 
            this.txtFullPath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFullPath.Location = new System.Drawing.Point(85, 32);
            this.txtFullPath.Name = "txtFullPath";
            this.txtFullPath.ReadOnly = true;
            this.txtFullPath.Size = new System.Drawing.Size(818, 20);
            this.txtFullPath.TabIndex = 2;
            this.ttGeneric.SetToolTip(this.txtFullPath, "Full path of file/folder");
            // 
            // lblFullPath
            // 
            this.lblFullPath.AutoSize = true;
            this.lblFullPath.Location = new System.Drawing.Point(29, 35);
            this.lblFullPath.Name = "lblFullPath";
            this.lblFullPath.Size = new System.Drawing.Size(50, 13);
            this.lblFullPath.TabIndex = 3;
            this.lblFullPath.Text = "Full path:";
            this.ttGeneric.SetToolTip(this.lblFullPath, "Full path of file/folder");
            // 
            // tabpgGlobalFields
            // 
            this.tabpgGlobalFields.Controls.Add(this.grpEditBank);
            this.tabpgGlobalFields.Controls.Add(this.grpGlobalBanks);
            this.tabpgGlobalFields.Location = new System.Drawing.Point(4, 22);
            this.tabpgGlobalFields.Name = "tabpgGlobalFields";
            this.tabpgGlobalFields.Padding = new System.Windows.Forms.Padding(3);
            this.tabpgGlobalFields.Size = new System.Drawing.Size(910, 447);
            this.tabpgGlobalFields.TabIndex = 4;
            this.tabpgGlobalFields.Text = "Global Fields";
            this.tabpgGlobalFields.UseVisualStyleBackColor = true;
            // 
            // grpEditBank
            // 
            this.grpEditBank.Controls.Add(this.gbPS4GlobalFields);
            this.grpEditBank.Controls.Add(this.grpGlobalsXbox);
            this.grpEditBank.Controls.Add(this.grpGlobalsPs3);
            this.grpEditBank.Controls.Add(this.btnSetCurrBank);
            this.grpEditBank.Controls.Add(this.btnDeleteBank);
            this.grpEditBank.Controls.Add(this.lblGlobalBank);
            this.grpEditBank.Controls.Add(this.btnAddNewBank);
            this.grpEditBank.Controls.Add(this.cboBankName);
            this.grpEditBank.Location = new System.Drawing.Point(6, 62);
            this.grpEditBank.Name = "grpEditBank";
            this.grpEditBank.Size = new System.Drawing.Size(741, 265);
            this.grpEditBank.TabIndex = 96;
            this.grpEditBank.TabStop = false;
            this.grpEditBank.Text = "Edit Bank";
            // 
            // gbPS4GlobalFields
            // 
            this.gbPS4GlobalFields.Controls.Add(this.txtPS4GFJPNContIDSecond);
            this.gbPS4GlobalFields.Controls.Add(this.lblPS4GFJPNContIDHyp);
            this.gbPS4GlobalFields.Controls.Add(this.txtPS4GFEntitlement);
            this.gbPS4GlobalFields.Controls.Add(this.txtPS4GFJPNContIDFirst);
            this.gbPS4GlobalFields.Controls.Add(this.lblPS4GFJNContID);
            this.gbPS4GlobalFields.Controls.Add(this.txtPS4GFUSContIDFirst);
            this.gbPS4GlobalFields.Controls.Add(this.lblPS4GFUSContID);
            this.gbPS4GlobalFields.Controls.Add(this.txtPS4GFEUContIDSecond);
            this.gbPS4GlobalFields.Controls.Add(this.label7);
            this.gbPS4GlobalFields.Controls.Add(this.lblPS4GFEUContIDHyp);
            this.gbPS4GlobalFields.Controls.Add(this.txtPS4GFEUContIDFirst);
            this.gbPS4GlobalFields.Controls.Add(this.lblPS4GFEUContID);
            this.gbPS4GlobalFields.Controls.Add(this.txtPS4GFUSContIDSecond);
            this.gbPS4GlobalFields.Controls.Add(this.lblPS4GFUSContIDHyp);
            this.gbPS4GlobalFields.Location = new System.Drawing.Point(375, 128);
            this.gbPS4GlobalFields.Name = "gbPS4GlobalFields";
            this.gbPS4GlobalFields.Size = new System.Drawing.Size(359, 132);
            this.gbPS4GlobalFields.TabIndex = 95;
            this.gbPS4GlobalFields.TabStop = false;
            this.gbPS4GlobalFields.Text = "PS4";
            // 
            // txtPS4GFJPNContIDSecond
            // 
            this.txtPS4GFJPNContIDSecond.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPS4GFJPNContIDSecond.Location = new System.Drawing.Point(174, 73);
            this.txtPS4GFJPNContIDSecond.MaxLength = 9;
            this.txtPS4GFJPNContIDSecond.Name = "txtPS4GFJPNContIDSecond";
            this.txtPS4GFJPNContIDSecond.Size = new System.Drawing.Size(72, 21);
            this.txtPS4GFJPNContIDSecond.TabIndex = 13;
            // 
            // lblPS4GFJPNContIDHyp
            // 
            this.lblPS4GFJPNContIDHyp.AutoSize = true;
            this.lblPS4GFJPNContIDHyp.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPS4GFJPNContIDHyp.Location = new System.Drawing.Point(154, 76);
            this.lblPS4GFJPNContIDHyp.Name = "lblPS4GFJPNContIDHyp";
            this.lblPS4GFJPNContIDHyp.Size = new System.Drawing.Size(14, 15);
            this.lblPS4GFJPNContIDHyp.TabIndex = 96;
            this.lblPS4GFJPNContIDHyp.Text = "-";
            // 
            // txtPS4GFEntitlement
            // 
            this.txtPS4GFEntitlement.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPS4GFEntitlement.Location = new System.Drawing.Point(96, 99);
            this.txtPS4GFEntitlement.MaxLength = 32;
            this.txtPS4GFEntitlement.Name = "txtPS4GFEntitlement";
            this.txtPS4GFEntitlement.Size = new System.Drawing.Size(256, 21);
            this.txtPS4GFEntitlement.TabIndex = 14;
            // 
            // txtPS4GFJPNContIDFirst
            // 
            this.txtPS4GFJPNContIDFirst.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPS4GFJPNContIDFirst.Location = new System.Drawing.Point(96, 73);
            this.txtPS4GFJPNContIDFirst.MaxLength = 6;
            this.txtPS4GFJPNContIDFirst.Name = "txtPS4GFJPNContIDFirst";
            this.txtPS4GFJPNContIDFirst.Size = new System.Drawing.Size(52, 21);
            this.txtPS4GFJPNContIDFirst.TabIndex = 12;
            // 
            // lblPS4GFJNContID
            // 
            this.lblPS4GFJNContID.AutoSize = true;
            this.lblPS4GFJNContID.Location = new System.Drawing.Point(13, 77);
            this.lblPS4GFJNContID.Name = "lblPS4GFJNContID";
            this.lblPS4GFJNContID.Size = new System.Drawing.Size(77, 13);
            this.lblPS4GFJNContID.TabIndex = 95;
            this.lblPS4GFJNContID.Text = "JN Content ID:";
            // 
            // txtPS4GFUSContIDFirst
            // 
            this.txtPS4GFUSContIDFirst.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPS4GFUSContIDFirst.Location = new System.Drawing.Point(96, 19);
            this.txtPS4GFUSContIDFirst.MaxLength = 6;
            this.txtPS4GFUSContIDFirst.Name = "txtPS4GFUSContIDFirst";
            this.txtPS4GFUSContIDFirst.Size = new System.Drawing.Size(52, 21);
            this.txtPS4GFUSContIDFirst.TabIndex = 8;
            // 
            // lblPS4GFUSContID
            // 
            this.lblPS4GFUSContID.AutoSize = true;
            this.lblPS4GFUSContID.Location = new System.Drawing.Point(11, 23);
            this.lblPS4GFUSContID.Name = "lblPS4GFUSContID";
            this.lblPS4GFUSContID.Size = new System.Drawing.Size(79, 13);
            this.lblPS4GFUSContID.TabIndex = 84;
            this.lblPS4GFUSContID.Text = "US Content ID:";
            // 
            // txtPS4GFEUContIDSecond
            // 
            this.txtPS4GFEUContIDSecond.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPS4GFEUContIDSecond.Location = new System.Drawing.Point(174, 46);
            this.txtPS4GFEUContIDSecond.MaxLength = 9;
            this.txtPS4GFEUContIDSecond.Name = "txtPS4GFEUContIDSecond";
            this.txtPS4GFEUContIDSecond.Size = new System.Drawing.Size(72, 21);
            this.txtPS4GFEUContIDSecond.TabIndex = 11;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(28, 104);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(62, 13);
            this.label7.TabIndex = 89;
            this.label7.Text = "Entitlement:";
            // 
            // lblPS4GFEUContIDHyp
            // 
            this.lblPS4GFEUContIDHyp.AutoSize = true;
            this.lblPS4GFEUContIDHyp.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPS4GFEUContIDHyp.Location = new System.Drawing.Point(154, 49);
            this.lblPS4GFEUContIDHyp.Name = "lblPS4GFEUContIDHyp";
            this.lblPS4GFEUContIDHyp.Size = new System.Drawing.Size(14, 15);
            this.lblPS4GFEUContIDHyp.TabIndex = 92;
            this.lblPS4GFEUContIDHyp.Text = "-";
            // 
            // txtPS4GFEUContIDFirst
            // 
            this.txtPS4GFEUContIDFirst.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPS4GFEUContIDFirst.Location = new System.Drawing.Point(96, 46);
            this.txtPS4GFEUContIDFirst.MaxLength = 6;
            this.txtPS4GFEUContIDFirst.Name = "txtPS4GFEUContIDFirst";
            this.txtPS4GFEUContIDFirst.Size = new System.Drawing.Size(52, 21);
            this.txtPS4GFEUContIDFirst.TabIndex = 10;
            // 
            // lblPS4GFEUContID
            // 
            this.lblPS4GFEUContID.AutoSize = true;
            this.lblPS4GFEUContID.Location = new System.Drawing.Point(11, 50);
            this.lblPS4GFEUContID.Name = "lblPS4GFEUContID";
            this.lblPS4GFEUContID.Size = new System.Drawing.Size(79, 13);
            this.lblPS4GFEUContID.TabIndex = 90;
            this.lblPS4GFEUContID.Text = "EU Content ID:";
            // 
            // txtPS4GFUSContIDSecond
            // 
            this.txtPS4GFUSContIDSecond.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPS4GFUSContIDSecond.Location = new System.Drawing.Point(174, 19);
            this.txtPS4GFUSContIDSecond.MaxLength = 9;
            this.txtPS4GFUSContIDSecond.Name = "txtPS4GFUSContIDSecond";
            this.txtPS4GFUSContIDSecond.Size = new System.Drawing.Size(72, 21);
            this.txtPS4GFUSContIDSecond.TabIndex = 9;
            // 
            // lblPS4GFUSContIDHyp
            // 
            this.lblPS4GFUSContIDHyp.AutoSize = true;
            this.lblPS4GFUSContIDHyp.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPS4GFUSContIDHyp.Location = new System.Drawing.Point(154, 22);
            this.lblPS4GFUSContIDHyp.Name = "lblPS4GFUSContIDHyp";
            this.lblPS4GFUSContIDHyp.Size = new System.Drawing.Size(14, 15);
            this.lblPS4GFUSContIDHyp.TabIndex = 91;
            this.lblPS4GFUSContIDHyp.Text = "-";
            // 
            // grpGlobalsXbox
            // 
            this.grpGlobalsXbox.Controls.Add(this.lblGlobFieldTitleID);
            this.grpGlobalsXbox.Controls.Add(this.lblGlobFieldTitle);
            this.grpGlobalsXbox.Controls.Add(this.txtGlobalFieldGameTitle);
            this.grpGlobalsXbox.Controls.Add(this.txtGlobalFieldTitleID);
            this.grpGlobalsXbox.Location = new System.Drawing.Point(6, 46);
            this.grpGlobalsXbox.Name = "grpGlobalsXbox";
            this.grpGlobalsXbox.Size = new System.Drawing.Size(363, 76);
            this.grpGlobalsXbox.TabIndex = 93;
            this.grpGlobalsXbox.TabStop = false;
            this.grpGlobalsXbox.Text = "Xbox";
            // 
            // lblGlobFieldTitleID
            // 
            this.lblGlobFieldTitleID.AutoSize = true;
            this.lblGlobFieldTitleID.Location = new System.Drawing.Point(46, 22);
            this.lblGlobFieldTitleID.Name = "lblGlobFieldTitleID";
            this.lblGlobFieldTitleID.Size = new System.Drawing.Size(44, 13);
            this.lblGlobFieldTitleID.TabIndex = 56;
            this.lblGlobFieldTitleID.Text = "Title ID:";
            // 
            // lblGlobFieldTitle
            // 
            this.lblGlobFieldTitle.AutoSize = true;
            this.lblGlobFieldTitle.Location = new System.Drawing.Point(29, 48);
            this.lblGlobFieldTitle.Name = "lblGlobFieldTitle";
            this.lblGlobFieldTitle.Size = new System.Drawing.Size(61, 13);
            this.lblGlobFieldTitle.TabIndex = 53;
            this.lblGlobFieldTitle.Text = "Game Title:";
            // 
            // txtGlobalFieldGameTitle
            // 
            this.txtGlobalFieldGameTitle.Location = new System.Drawing.Point(96, 45);
            this.txtGlobalFieldGameTitle.Name = "txtGlobalFieldGameTitle";
            this.txtGlobalFieldGameTitle.Size = new System.Drawing.Size(256, 20);
            this.txtGlobalFieldGameTitle.TabIndex = 7;
            this.txtGlobalFieldGameTitle.TextChanged += new System.EventHandler(this.setSolutionDirty);
            // 
            // txtGlobalFieldTitleID
            // 
            this.txtGlobalFieldTitleID.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGlobalFieldTitleID.Location = new System.Drawing.Point(96, 19);
            this.txtGlobalFieldTitleID.MaxLength = 8;
            this.txtGlobalFieldTitleID.Name = "txtGlobalFieldTitleID";
            this.txtGlobalFieldTitleID.Size = new System.Drawing.Size(64, 20);
            this.txtGlobalFieldTitleID.TabIndex = 6;
            this.txtGlobalFieldTitleID.TextChanged += new System.EventHandler(this.setSolutionDirty);
            // 
            // grpGlobalsPs3
            // 
            this.grpGlobalsPs3.Controls.Add(this.txtGlobalFieldJPNContentIDSecond);
            this.grpGlobalsPs3.Controls.Add(this.lblPS3GFJPNContIDHyp);
            this.grpGlobalsPs3.Controls.Add(this.txtGlobalFieldJPNContentIDFirst);
            this.grpGlobalsPs3.Controls.Add(this.lblGlobFieldJPNContID);
            this.grpGlobalsPs3.Controls.Add(this.txtGlobalFieldUSContentIDFirst);
            this.grpGlobalsPs3.Controls.Add(this.lblGlobFieldUSContID);
            this.grpGlobalsPs3.Controls.Add(this.txtGlobalFieldEUContentIDSecond);
            this.grpGlobalsPs3.Controls.Add(this.lblGlobFieldKLicensee);
            this.grpGlobalsPs3.Controls.Add(this.lblPS3GFEUContIDHyp);
            this.grpGlobalsPs3.Controls.Add(this.txtGlobalFieldKLicense);
            this.grpGlobalsPs3.Controls.Add(this.txtGlobalFieldEUContentIDFirst);
            this.grpGlobalsPs3.Controls.Add(this.lblGlobFieldEUContID);
            this.grpGlobalsPs3.Controls.Add(this.txtGlobalFieldUSContentIDSecond);
            this.grpGlobalsPs3.Controls.Add(this.lblPS3GFUSContIDHyp);
            this.grpGlobalsPs3.Location = new System.Drawing.Point(6, 128);
            this.grpGlobalsPs3.Name = "grpGlobalsPs3";
            this.grpGlobalsPs3.Size = new System.Drawing.Size(363, 132);
            this.grpGlobalsPs3.TabIndex = 94;
            this.grpGlobalsPs3.TabStop = false;
            this.grpGlobalsPs3.Text = "PS3";
            // 
            // txtGlobalFieldJPNContentIDSecond
            // 
            this.txtGlobalFieldJPNContentIDSecond.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGlobalFieldJPNContentIDSecond.Location = new System.Drawing.Point(174, 73);
            this.txtGlobalFieldJPNContentIDSecond.MaxLength = 9;
            this.txtGlobalFieldJPNContentIDSecond.Name = "txtGlobalFieldJPNContentIDSecond";
            this.txtGlobalFieldJPNContentIDSecond.Size = new System.Drawing.Size(72, 21);
            this.txtGlobalFieldJPNContentIDSecond.TabIndex = 13;
            this.txtGlobalFieldJPNContentIDSecond.TextChanged += new System.EventHandler(this.setSolutionDirty);
            // 
            // lblPS3GFJPNContIDHyp
            // 
            this.lblPS3GFJPNContIDHyp.AutoSize = true;
            this.lblPS3GFJPNContIDHyp.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPS3GFJPNContIDHyp.Location = new System.Drawing.Point(154, 76);
            this.lblPS3GFJPNContIDHyp.Name = "lblPS3GFJPNContIDHyp";
            this.lblPS3GFJPNContIDHyp.Size = new System.Drawing.Size(14, 15);
            this.lblPS3GFJPNContIDHyp.TabIndex = 96;
            this.lblPS3GFJPNContIDHyp.Text = "-";
            // 
            // txtGlobalFieldJPNContentIDFirst
            // 
            this.txtGlobalFieldJPNContentIDFirst.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGlobalFieldJPNContentIDFirst.Location = new System.Drawing.Point(96, 73);
            this.txtGlobalFieldJPNContentIDFirst.MaxLength = 6;
            this.txtGlobalFieldJPNContentIDFirst.Name = "txtGlobalFieldJPNContentIDFirst";
            this.txtGlobalFieldJPNContentIDFirst.Size = new System.Drawing.Size(52, 21);
            this.txtGlobalFieldJPNContentIDFirst.TabIndex = 12;
            this.txtGlobalFieldJPNContentIDFirst.TextChanged += new System.EventHandler(this.setSolutionDirty);
            // 
            // lblGlobFieldJPNContID
            // 
            this.lblGlobFieldJPNContID.AutoSize = true;
            this.lblGlobFieldJPNContID.Location = new System.Drawing.Point(13, 77);
            this.lblGlobFieldJPNContID.Name = "lblGlobFieldJPNContID";
            this.lblGlobFieldJPNContID.Size = new System.Drawing.Size(77, 13);
            this.lblGlobFieldJPNContID.TabIndex = 95;
            this.lblGlobFieldJPNContID.Text = "JN Content ID:";
            // 
            // txtGlobalFieldUSContentIDFirst
            // 
            this.txtGlobalFieldUSContentIDFirst.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGlobalFieldUSContentIDFirst.Location = new System.Drawing.Point(96, 19);
            this.txtGlobalFieldUSContentIDFirst.MaxLength = 6;
            this.txtGlobalFieldUSContentIDFirst.Name = "txtGlobalFieldUSContentIDFirst";
            this.txtGlobalFieldUSContentIDFirst.Size = new System.Drawing.Size(52, 21);
            this.txtGlobalFieldUSContentIDFirst.TabIndex = 8;
            this.txtGlobalFieldUSContentIDFirst.TextChanged += new System.EventHandler(this.setSolutionDirty);
            // 
            // lblGlobFieldUSContID
            // 
            this.lblGlobFieldUSContID.AutoSize = true;
            this.lblGlobFieldUSContID.Location = new System.Drawing.Point(11, 23);
            this.lblGlobFieldUSContID.Name = "lblGlobFieldUSContID";
            this.lblGlobFieldUSContID.Size = new System.Drawing.Size(79, 13);
            this.lblGlobFieldUSContID.TabIndex = 84;
            this.lblGlobFieldUSContID.Text = "US Content ID:";
            // 
            // txtGlobalFieldEUContentIDSecond
            // 
            this.txtGlobalFieldEUContentIDSecond.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGlobalFieldEUContentIDSecond.Location = new System.Drawing.Point(174, 46);
            this.txtGlobalFieldEUContentIDSecond.MaxLength = 9;
            this.txtGlobalFieldEUContentIDSecond.Name = "txtGlobalFieldEUContentIDSecond";
            this.txtGlobalFieldEUContentIDSecond.Size = new System.Drawing.Size(72, 21);
            this.txtGlobalFieldEUContentIDSecond.TabIndex = 11;
            this.txtGlobalFieldEUContentIDSecond.TextChanged += new System.EventHandler(this.setSolutionDirty);
            // 
            // lblGlobFieldKLicensee
            // 
            this.lblGlobFieldKLicensee.AutoSize = true;
            this.lblGlobFieldKLicensee.Location = new System.Drawing.Point(34, 103);
            this.lblGlobFieldKLicensee.Name = "lblGlobFieldKLicensee";
            this.lblGlobFieldKLicensee.Size = new System.Drawing.Size(56, 13);
            this.lblGlobFieldKLicensee.TabIndex = 89;
            this.lblGlobFieldKLicensee.Text = "Klicensee:";
            // 
            // lblPS3GFEUContIDHyp
            // 
            this.lblPS3GFEUContIDHyp.AutoSize = true;
            this.lblPS3GFEUContIDHyp.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPS3GFEUContIDHyp.Location = new System.Drawing.Point(154, 49);
            this.lblPS3GFEUContIDHyp.Name = "lblPS3GFEUContIDHyp";
            this.lblPS3GFEUContIDHyp.Size = new System.Drawing.Size(14, 15);
            this.lblPS3GFEUContIDHyp.TabIndex = 92;
            this.lblPS3GFEUContIDHyp.Text = "-";
            // 
            // txtGlobalFieldKLicense
            // 
            this.txtGlobalFieldKLicense.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGlobalFieldKLicense.Location = new System.Drawing.Point(96, 100);
            this.txtGlobalFieldKLicense.MaxLength = 34;
            this.txtGlobalFieldKLicense.Name = "txtGlobalFieldKLicense";
            this.txtGlobalFieldKLicense.Size = new System.Drawing.Size(256, 21);
            this.txtGlobalFieldKLicense.TabIndex = 14;
            this.txtGlobalFieldKLicense.TextChanged += new System.EventHandler(this.setSolutionDirty);
            // 
            // txtGlobalFieldEUContentIDFirst
            // 
            this.txtGlobalFieldEUContentIDFirst.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGlobalFieldEUContentIDFirst.Location = new System.Drawing.Point(96, 46);
            this.txtGlobalFieldEUContentIDFirst.MaxLength = 6;
            this.txtGlobalFieldEUContentIDFirst.Name = "txtGlobalFieldEUContentIDFirst";
            this.txtGlobalFieldEUContentIDFirst.Size = new System.Drawing.Size(52, 21);
            this.txtGlobalFieldEUContentIDFirst.TabIndex = 10;
            this.txtGlobalFieldEUContentIDFirst.TextChanged += new System.EventHandler(this.setSolutionDirty);
            // 
            // lblGlobFieldEUContID
            // 
            this.lblGlobFieldEUContID.AutoSize = true;
            this.lblGlobFieldEUContID.Location = new System.Drawing.Point(11, 50);
            this.lblGlobFieldEUContID.Name = "lblGlobFieldEUContID";
            this.lblGlobFieldEUContID.Size = new System.Drawing.Size(79, 13);
            this.lblGlobFieldEUContID.TabIndex = 90;
            this.lblGlobFieldEUContID.Text = "EU Content ID:";
            // 
            // txtGlobalFieldUSContentIDSecond
            // 
            this.txtGlobalFieldUSContentIDSecond.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGlobalFieldUSContentIDSecond.Location = new System.Drawing.Point(174, 19);
            this.txtGlobalFieldUSContentIDSecond.MaxLength = 9;
            this.txtGlobalFieldUSContentIDSecond.Name = "txtGlobalFieldUSContentIDSecond";
            this.txtGlobalFieldUSContentIDSecond.Size = new System.Drawing.Size(72, 21);
            this.txtGlobalFieldUSContentIDSecond.TabIndex = 9;
            this.txtGlobalFieldUSContentIDSecond.TextChanged += new System.EventHandler(this.setSolutionDirty);
            // 
            // lblPS3GFUSContIDHyp
            // 
            this.lblPS3GFUSContIDHyp.AutoSize = true;
            this.lblPS3GFUSContIDHyp.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPS3GFUSContIDHyp.Location = new System.Drawing.Point(154, 22);
            this.lblPS3GFUSContIDHyp.Name = "lblPS3GFUSContIDHyp";
            this.lblPS3GFUSContIDHyp.Size = new System.Drawing.Size(14, 15);
            this.lblPS3GFUSContIDHyp.TabIndex = 91;
            this.lblPS3GFUSContIDHyp.Text = "-";
            // 
            // btnSetCurrBank
            // 
            this.btnSetCurrBank.Location = new System.Drawing.Point(460, 17);
            this.btnSetCurrBank.Name = "btnSetCurrBank";
            this.btnSetCurrBank.Size = new System.Drawing.Size(79, 23);
            this.btnSetCurrBank.TabIndex = 4;
            this.btnSetCurrBank.Text = "Set Current";
            this.ttGeneric.SetToolTip(this.btnSetCurrBank, "Set the currently selected bank as the project bank");
            this.btnSetCurrBank.UseVisualStyleBackColor = true;
            this.btnSetCurrBank.Click += new System.EventHandler(this.btnSetCurrBank_Click);
            // 
            // btnDeleteBank
            // 
            this.btnDeleteBank.Location = new System.Drawing.Point(545, 17);
            this.btnDeleteBank.Name = "btnDeleteBank";
            this.btnDeleteBank.Size = new System.Drawing.Size(79, 23);
            this.btnDeleteBank.TabIndex = 5;
            this.btnDeleteBank.Text = "Delete";
            this.ttGeneric.SetToolTip(this.btnDeleteBank, "Delete the current bank");
            this.btnDeleteBank.UseVisualStyleBackColor = true;
            this.btnDeleteBank.Click += new System.EventHandler(this.btnDeleteBank_Click);
            // 
            // lblGlobalBank
            // 
            this.lblGlobalBank.AutoSize = true;
            this.lblGlobalBank.Location = new System.Drawing.Point(61, 22);
            this.lblGlobalBank.Name = "lblGlobalBank";
            this.lblGlobalBank.Size = new System.Drawing.Size(35, 13);
            this.lblGlobalBank.TabIndex = 56;
            this.lblGlobalBank.Text = "Bank:";
            // 
            // btnAddNewBank
            // 
            this.btnAddNewBank.Location = new System.Drawing.Point(375, 17);
            this.btnAddNewBank.Name = "btnAddNewBank";
            this.btnAddNewBank.Size = new System.Drawing.Size(79, 23);
            this.btnAddNewBank.TabIndex = 3;
            this.btnAddNewBank.Text = "Add/Update";
            this.ttGeneric.SetToolTip(this.btnAddNewBank, "Add a new bank to the collection");
            this.btnAddNewBank.UseVisualStyleBackColor = true;
            this.btnAddNewBank.Click += new System.EventHandler(this.btnAddNewBank_Click);
            // 
            // cboBankName
            // 
            this.cboBankName.FormattingEnabled = true;
            this.cboBankName.Location = new System.Drawing.Point(102, 19);
            this.cboBankName.Name = "cboBankName";
            this.cboBankName.Size = new System.Drawing.Size(267, 21);
            this.cboBankName.Sorted = true;
            this.cboBankName.TabIndex = 2;
            this.cboBankName.SelectedIndexChanged += new System.EventHandler(this.cboBankName_SelectedIndexChanged);
            // 
            // grpGlobalBanks
            // 
            this.grpGlobalBanks.Controls.Add(this.lblCurrentBank);
            this.grpGlobalBanks.Controls.Add(this.txtCurrGlobalBank);
            this.grpGlobalBanks.Location = new System.Drawing.Point(6, 6);
            this.grpGlobalBanks.Name = "grpGlobalBanks";
            this.grpGlobalBanks.Size = new System.Drawing.Size(741, 50);
            this.grpGlobalBanks.TabIndex = 95;
            this.grpGlobalBanks.TabStop = false;
            this.grpGlobalBanks.Text = "Project Bank";
            // 
            // lblCurrentBank
            // 
            this.lblCurrentBank.AutoSize = true;
            this.lblCurrentBank.Location = new System.Drawing.Point(24, 22);
            this.lblCurrentBank.Name = "lblCurrentBank";
            this.lblCurrentBank.Size = new System.Drawing.Size(72, 13);
            this.lblCurrentBank.TabIndex = 99;
            this.lblCurrentBank.Text = "Current Bank:";
            // 
            // txtCurrGlobalBank
            // 
            this.txtCurrGlobalBank.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.txtCurrGlobalBank.Location = new System.Drawing.Point(102, 19);
            this.txtCurrGlobalBank.MaxLength = 8;
            this.txtCurrGlobalBank.Name = "txtCurrGlobalBank";
            this.txtCurrGlobalBank.ReadOnly = true;
            this.txtCurrGlobalBank.Size = new System.Drawing.Size(267, 20);
            this.txtCurrGlobalBank.TabIndex = 1;
            // 
            // tabpgXbox
            // 
            this.tabpgXbox.Controls.Add(this.pbXboxTitleImg);
            this.tabpgXbox.Controls.Add(this.chkConsumableOffer);
            this.tabpgXbox.Controls.Add(this.chkRestrictedLicense);
            this.tabpgXbox.Controls.Add(this.cboVisibility);
            this.tabpgXbox.Controls.Add(this.lblVisibility);
            this.tabpgXbox.Controls.Add(this.txtOfferId);
            this.tabpgXbox.Controls.Add(this.lblOfferId);
            this.tabpgXbox.Controls.Add(this.txtPrevOfferId);
            this.tabpgXbox.Controls.Add(this.lblPrevOfferId);
            this.tabpgXbox.Controls.Add(this.txtContentCat);
            this.tabpgXbox.Controls.Add(this.lblContentCat);
            this.tabpgXbox.Controls.Add(this.txtLicenseMask);
            this.tabpgXbox.Controls.Add(this.lblLicenseMask);
            this.tabpgXbox.Controls.Add(this.chkDevTrans);
            this.tabpgXbox.Controls.Add(this.chkProfTrans);
            this.tabpgXbox.Controls.Add(this.chkIntForMktPlc);
            this.tabpgXbox.Controls.Add(this.chkHasCost);
            this.tabpgXbox.Controls.Add(this.dtPickerXboxActDate);
            this.tabpgXbox.Controls.Add(this.txtXboxPubFlags);
            this.tabpgXbox.Controls.Add(this.btnBrowseXboxDBIcon);
            this.tabpgXbox.Controls.Add(this.btnBrowseXboxTitleImg);
            this.tabpgXbox.Controls.Add(this.btnBrowseXboxOfferBan);
            this.tabpgXbox.Controls.Add(this.pbXboxDBIcon);
            this.tabpgXbox.Controls.Add(this.pbXboxOffBan);
            this.tabpgXbox.Controls.Add(this.lblXboxOffBan);
            this.tabpgXbox.Controls.Add(this.txtXboxOffBan);
            this.tabpgXbox.Controls.Add(this.lblXboxPkgName);
            this.tabpgXbox.Controls.Add(this.txtXboxPkgName);
            this.tabpgXbox.Controls.Add(this.lblXboxTitleImg);
            this.tabpgXbox.Controls.Add(this.txtXboxTitleID);
            this.tabpgXbox.Controls.Add(this.txtXboxTitleImage);
            this.tabpgXbox.Controls.Add(this.lblXboxTitleID);
            this.tabpgXbox.Controls.Add(this.lblXboxDBIcon);
            this.tabpgXbox.Controls.Add(this.txtXboxGameTitle);
            this.tabpgXbox.Controls.Add(this.txtXboxDBIcon);
            this.tabpgXbox.Controls.Add(this.lblXboxGameTitle);
            this.tabpgXbox.Controls.Add(this.lblXboxActDate);
            this.tabpgXbox.Controls.Add(this.txtXboxOffID);
            this.tabpgXbox.Controls.Add(this.lblXboxOffID);
            this.tabpgXbox.Controls.Add(this.lblXboxPubFlags);
            this.tabpgXbox.Location = new System.Drawing.Point(4, 22);
            this.tabpgXbox.Name = "tabpgXbox";
            this.tabpgXbox.Padding = new System.Windows.Forms.Padding(3);
            this.tabpgXbox.Size = new System.Drawing.Size(910, 447);
            this.tabpgXbox.TabIndex = 0;
            this.tabpgXbox.Text = "XBOX";
            this.tabpgXbox.UseVisualStyleBackColor = true;
            // 
            // pbXboxTitleImg
            // 
            this.pbXboxTitleImg.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pbXboxTitleImg.Location = new System.Drawing.Point(522, 247);
            this.pbXboxTitleImg.MaximumSize = new System.Drawing.Size(64, 64);
            this.pbXboxTitleImg.Name = "pbXboxTitleImg";
            this.pbXboxTitleImg.Size = new System.Drawing.Size(64, 64);
            this.pbXboxTitleImg.TabIndex = 81;
            this.pbXboxTitleImg.TabStop = false;
            // 
            // chkConsumableOffer
            // 
            this.chkConsumableOffer.AutoSize = true;
            this.chkConsumableOffer.Location = new System.Drawing.Point(362, 188);
            this.chkConsumableOffer.Name = "chkConsumableOffer";
            this.chkConsumableOffer.Size = new System.Drawing.Size(116, 17);
            this.chkConsumableOffer.TabIndex = 16;
            this.chkConsumableOffer.Text = "Consumable Offer?";
            this.chkConsumableOffer.UseVisualStyleBackColor = true;
            // 
            // chkRestrictedLicense
            // 
            this.chkRestrictedLicense.AutoSize = true;
            this.chkRestrictedLicense.Location = new System.Drawing.Point(362, 164);
            this.chkRestrictedLicense.Name = "chkRestrictedLicense";
            this.chkRestrictedLicense.Size = new System.Drawing.Size(120, 17);
            this.chkRestrictedLicense.TabIndex = 13;
            this.chkRestrictedLicense.Text = "Restricted License?";
            this.chkRestrictedLicense.UseVisualStyleBackColor = true;
            // 
            // cboVisibility
            // 
            this.cboVisibility.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboVisibility.DropDownWidth = 213;
            this.cboVisibility.FormattingEnabled = true;
            this.cboVisibility.Items.AddRange(new object[] {
            "Visible in Marketplace",
            "Visible in Marketplace (Except Partnernet)",
            "Not visible in Marketplace"});
            this.cboVisibility.Location = new System.Drawing.Point(95, 214);
            this.cboVisibility.Name = "cboVisibility";
            this.cboVisibility.Size = new System.Drawing.Size(261, 21);
            this.cboVisibility.TabIndex = 17;
            // 
            // lblVisibility
            // 
            this.lblVisibility.AutoSize = true;
            this.lblVisibility.Location = new System.Drawing.Point(43, 217);
            this.lblVisibility.Name = "lblVisibility";
            this.lblVisibility.Size = new System.Drawing.Size(46, 13);
            this.lblVisibility.TabIndex = 80;
            this.lblVisibility.Text = "Visibility:";
            // 
            // txtOfferId
            // 
            this.txtOfferId.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtOfferId.Location = new System.Drawing.Point(276, 188);
            this.txtOfferId.MaxLength = 10;
            this.txtOfferId.Name = "txtOfferId";
            this.txtOfferId.Size = new System.Drawing.Size(80, 20);
            this.txtOfferId.TabIndex = 15;
            // 
            // lblOfferId
            // 
            this.lblOfferId.AutoSize = true;
            this.lblOfferId.Location = new System.Drawing.Point(223, 191);
            this.lblOfferId.Name = "lblOfferId";
            this.lblOfferId.Size = new System.Drawing.Size(47, 13);
            this.lblOfferId.TabIndex = 79;
            this.lblOfferId.Text = "Offer ID:";
            // 
            // txtPrevOfferId
            // 
            this.txtPrevOfferId.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPrevOfferId.Location = new System.Drawing.Point(276, 162);
            this.txtPrevOfferId.MaxLength = 10;
            this.txtPrevOfferId.Name = "txtPrevOfferId";
            this.txtPrevOfferId.Size = new System.Drawing.Size(80, 20);
            this.txtPrevOfferId.TabIndex = 12;
            // 
            // lblPrevOfferId
            // 
            this.lblPrevOfferId.AutoSize = true;
            this.lblPrevOfferId.Location = new System.Drawing.Point(182, 165);
            this.lblPrevOfferId.Name = "lblPrevOfferId";
            this.lblPrevOfferId.Size = new System.Drawing.Size(88, 13);
            this.lblPrevOfferId.TabIndex = 77;
            this.lblPrevOfferId.Text = "Preview Offer ID:";
            // 
            // txtContentCat
            // 
            this.txtContentCat.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtContentCat.Location = new System.Drawing.Point(96, 188);
            this.txtContentCat.MaxLength = 10;
            this.txtContentCat.Name = "txtContentCat";
            this.txtContentCat.Size = new System.Drawing.Size(80, 20);
            this.txtContentCat.TabIndex = 14;
            // 
            // lblContentCat
            // 
            this.lblContentCat.AutoSize = true;
            this.lblContentCat.Location = new System.Drawing.Point(-1, 191);
            this.lblContentCat.Name = "lblContentCat";
            this.lblContentCat.Size = new System.Drawing.Size(92, 13);
            this.lblContentCat.TabIndex = 75;
            this.lblContentCat.Text = "Content Category:";
            // 
            // txtLicenseMask
            // 
            this.txtLicenseMask.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLicenseMask.Location = new System.Drawing.Point(96, 162);
            this.txtLicenseMask.MaxLength = 10;
            this.txtLicenseMask.Name = "txtLicenseMask";
            this.txtLicenseMask.Size = new System.Drawing.Size(80, 20);
            this.txtLicenseMask.TabIndex = 11;
            // 
            // lblLicenseMask
            // 
            this.lblLicenseMask.AutoSize = true;
            this.lblLicenseMask.Location = new System.Drawing.Point(14, 165);
            this.lblLicenseMask.Name = "lblLicenseMask";
            this.lblLicenseMask.Size = new System.Drawing.Size(76, 13);
            this.lblLicenseMask.TabIndex = 73;
            this.lblLicenseMask.Text = "License Mask:";
            // 
            // chkDevTrans
            // 
            this.chkDevTrans.AutoSize = true;
            this.chkDevTrans.Enabled = false;
            this.chkDevTrans.Location = new System.Drawing.Point(357, 135);
            this.chkDevTrans.Name = "chkDevTrans";
            this.chkDevTrans.Size = new System.Drawing.Size(104, 17);
            this.chkDevTrans.TabIndex = 10;
            this.chkDevTrans.Text = "Device transfer?";
            this.chkDevTrans.UseVisualStyleBackColor = true;
            // 
            // chkProfTrans
            // 
            this.chkProfTrans.AutoSize = true;
            this.chkProfTrans.Enabled = false;
            this.chkProfTrans.Location = new System.Drawing.Point(252, 135);
            this.chkProfTrans.Name = "chkProfTrans";
            this.chkProfTrans.Size = new System.Drawing.Size(99, 17);
            this.chkProfTrans.TabIndex = 9;
            this.chkProfTrans.Text = "Profile transfer?";
            this.chkProfTrans.UseVisualStyleBackColor = true;
            // 
            // chkIntForMktPlc
            // 
            this.chkIntForMktPlc.AutoSize = true;
            this.chkIntForMktPlc.Location = new System.Drawing.Point(232, 112);
            this.chkIntForMktPlc.Name = "chkIntForMktPlc";
            this.chkIntForMktPlc.Size = new System.Drawing.Size(153, 17);
            this.chkIntForMktPlc.TabIndex = 7;
            this.chkIntForMktPlc.Text = "Intended for market place?";
            this.chkIntForMktPlc.UseVisualStyleBackColor = true;
            this.chkIntForMktPlc.CheckedChanged += new System.EventHandler(this.chkIntForMktPlc_CheckedChanged);
            // 
            // chkHasCost
            // 
            this.chkHasCost.AutoSize = true;
            this.chkHasCost.Location = new System.Drawing.Point(232, 89);
            this.chkHasCost.Name = "chkHasCost";
            this.chkHasCost.Size = new System.Drawing.Size(68, 17);
            this.chkHasCost.TabIndex = 5;
            this.chkHasCost.Text = "Has cost";
            this.chkHasCost.UseVisualStyleBackColor = true;
            // 
            // dtPickerXboxActDate
            // 
            this.dtPickerXboxActDate.CustomFormat = "MM/dd/yyyy";
            this.dtPickerXboxActDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtPickerXboxActDate.Location = new System.Drawing.Point(96, 136);
            this.dtPickerXboxActDate.Name = "dtPickerXboxActDate";
            this.dtPickerXboxActDate.Size = new System.Drawing.Size(130, 20);
            this.dtPickerXboxActDate.TabIndex = 8;
            this.dtPickerXboxActDate.ValueChanged += new System.EventHandler(this.setSolutionDirty);
            // 
            // txtXboxPubFlags
            // 
            this.txtXboxPubFlags.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtXboxPubFlags.Location = new System.Drawing.Point(96, 84);
            this.txtXboxPubFlags.MaxLength = 8;
            this.txtXboxPubFlags.Name = "txtXboxPubFlags";
            this.txtXboxPubFlags.Size = new System.Drawing.Size(64, 20);
            this.txtXboxPubFlags.TabIndex = 4;
            this.txtXboxPubFlags.TextChanged += new System.EventHandler(this.setSolutionDirty);
            // 
            // btnBrowseXboxDBIcon
            // 
            this.btnBrowseXboxDBIcon.Location = new System.Drawing.Point(485, 267);
            this.btnBrowseXboxDBIcon.Name = "btnBrowseXboxDBIcon";
            this.btnBrowseXboxDBIcon.Size = new System.Drawing.Size(31, 23);
            this.btnBrowseXboxDBIcon.TabIndex = 21;
            this.btnBrowseXboxDBIcon.Text = "...";
            this.ttGeneric.SetToolTip(this.btnBrowseXboxDBIcon, "Browse for files, use the \'Delete\' key to clear this field.");
            this.btnBrowseXboxDBIcon.UseVisualStyleBackColor = true;
            this.btnBrowseXboxDBIcon.Click += new System.EventHandler(this.btnBrowseXboxDBIcon_Click);
            // 
            // btnBrowseXboxTitleImg
            // 
            this.btnBrowseXboxTitleImg.Location = new System.Drawing.Point(485, 241);
            this.btnBrowseXboxTitleImg.Name = "btnBrowseXboxTitleImg";
            this.btnBrowseXboxTitleImg.Size = new System.Drawing.Size(31, 23);
            this.btnBrowseXboxTitleImg.TabIndex = 19;
            this.btnBrowseXboxTitleImg.Text = "...";
            this.ttGeneric.SetToolTip(this.btnBrowseXboxTitleImg, "Browse for files, use the \'Delete\' key to clear this field.");
            this.btnBrowseXboxTitleImg.UseVisualStyleBackColor = true;
            this.btnBrowseXboxTitleImg.Click += new System.EventHandler(this.btnBrowseXboxTitleImg_Click);
            // 
            // btnBrowseXboxOfferBan
            // 
            this.btnBrowseXboxOfferBan.Location = new System.Drawing.Point(485, 293);
            this.btnBrowseXboxOfferBan.Name = "btnBrowseXboxOfferBan";
            this.btnBrowseXboxOfferBan.Size = new System.Drawing.Size(31, 23);
            this.btnBrowseXboxOfferBan.TabIndex = 23;
            this.btnBrowseXboxOfferBan.Text = "...";
            this.ttGeneric.SetToolTip(this.btnBrowseXboxOfferBan, "Browse for files, use the \'Delete\' key to clear this field.");
            this.btnBrowseXboxOfferBan.UseVisualStyleBackColor = true;
            this.btnBrowseXboxOfferBan.Click += new System.EventHandler(this.btnBrowseXboxOfferBan_Click);
            // 
            // pbXboxDBIcon
            // 
            this.pbXboxDBIcon.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pbXboxDBIcon.Location = new System.Drawing.Point(592, 247);
            this.pbXboxDBIcon.MaximumSize = new System.Drawing.Size(64, 64);
            this.pbXboxDBIcon.Name = "pbXboxDBIcon";
            this.pbXboxDBIcon.Size = new System.Drawing.Size(64, 64);
            this.pbXboxDBIcon.TabIndex = 66;
            this.pbXboxDBIcon.TabStop = false;
            // 
            // pbXboxOffBan
            // 
            this.pbXboxOffBan.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pbXboxOffBan.Location = new System.Drawing.Point(96, 321);
            this.pbXboxOffBan.MaximumSize = new System.Drawing.Size(420, 95);
            this.pbXboxOffBan.Name = "pbXboxOffBan";
            this.pbXboxOffBan.Size = new System.Drawing.Size(420, 95);
            this.pbXboxOffBan.TabIndex = 65;
            this.pbXboxOffBan.TabStop = false;
            this.ttGeneric.SetToolTip(this.pbXboxOffBan, "Offer banner");
            // 
            // lblXboxOffBan
            // 
            this.lblXboxOffBan.AutoSize = true;
            this.lblXboxOffBan.Location = new System.Drawing.Point(20, 298);
            this.lblXboxOffBan.Name = "lblXboxOffBan";
            this.lblXboxOffBan.Size = new System.Drawing.Size(70, 13);
            this.lblXboxOffBan.TabIndex = 64;
            this.lblXboxOffBan.Text = "Offer Banner:";
            // 
            // txtXboxOffBan
            // 
            this.txtXboxOffBan.Location = new System.Drawing.Point(96, 295);
            this.txtXboxOffBan.Name = "txtXboxOffBan";
            this.txtXboxOffBan.ReadOnly = true;
            this.txtXboxOffBan.Size = new System.Drawing.Size(383, 20);
            this.txtXboxOffBan.TabIndex = 22;
            this.txtXboxOffBan.TabStop = false;
            this.ttGeneric.SetToolTip(this.txtXboxOffBan, "To clear field, highlight and press \'Delete\'");
            this.txtXboxOffBan.TextChanged += new System.EventHandler(this.txtXboxOffBan_TextChanged);
            this.txtXboxOffBan.KeyUp += new System.Windows.Forms.KeyEventHandler(this.clearTxtField);
            // 
            // lblXboxPkgName
            // 
            this.lblXboxPkgName.AutoSize = true;
            this.lblXboxPkgName.Location = new System.Drawing.Point(6, 61);
            this.lblXboxPkgName.Name = "lblXboxPkgName";
            this.lblXboxPkgName.Size = new System.Drawing.Size(84, 13);
            this.lblXboxPkgName.TabIndex = 50;
            this.lblXboxPkgName.Text = "Package Name:";
            // 
            // txtXboxPkgName
            // 
            this.txtXboxPkgName.Location = new System.Drawing.Point(96, 58);
            this.txtXboxPkgName.Name = "txtXboxPkgName";
            this.txtXboxPkgName.Size = new System.Drawing.Size(383, 20);
            this.txtXboxPkgName.TabIndex = 3;
            this.txtXboxPkgName.TextChanged += new System.EventHandler(this.setSolutionDirty);
            // 
            // lblXboxTitleImg
            // 
            this.lblXboxTitleImg.AutoSize = true;
            this.lblXboxTitleImg.Location = new System.Drawing.Point(28, 246);
            this.lblXboxTitleImg.Name = "lblXboxTitleImg";
            this.lblXboxTitleImg.Size = new System.Drawing.Size(62, 13);
            this.lblXboxTitleImg.TabIndex = 62;
            this.lblXboxTitleImg.Text = "Title Image:";
            // 
            // txtXboxTitleID
            // 
            this.txtXboxTitleID.BackColor = System.Drawing.Color.Gold;
            this.txtXboxTitleID.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtXboxTitleID.Location = new System.Drawing.Point(96, 6);
            this.txtXboxTitleID.MaxLength = 8;
            this.txtXboxTitleID.Name = "txtXboxTitleID";
            this.txtXboxTitleID.ReadOnly = true;
            this.txtXboxTitleID.Size = new System.Drawing.Size(64, 20);
            this.txtXboxTitleID.TabIndex = 1;
            this.txtXboxTitleID.TextChanged += new System.EventHandler(this.setSolutionDirty);
            // 
            // txtXboxTitleImage
            // 
            this.txtXboxTitleImage.Location = new System.Drawing.Point(96, 243);
            this.txtXboxTitleImage.Name = "txtXboxTitleImage";
            this.txtXboxTitleImage.ReadOnly = true;
            this.txtXboxTitleImage.Size = new System.Drawing.Size(383, 20);
            this.txtXboxTitleImage.TabIndex = 18;
            this.txtXboxTitleImage.TabStop = false;
            this.ttGeneric.SetToolTip(this.txtXboxTitleImage, "To clear field, highlight and press \'Delete\'");
            this.txtXboxTitleImage.TextChanged += new System.EventHandler(this.txtXboxTitleImage_TextChanged);
            this.txtXboxTitleImage.KeyUp += new System.Windows.Forms.KeyEventHandler(this.clearTxtField);
            // 
            // lblXboxTitleID
            // 
            this.lblXboxTitleID.AutoSize = true;
            this.lblXboxTitleID.Location = new System.Drawing.Point(46, 9);
            this.lblXboxTitleID.Name = "lblXboxTitleID";
            this.lblXboxTitleID.Size = new System.Drawing.Size(44, 13);
            this.lblXboxTitleID.TabIndex = 52;
            this.lblXboxTitleID.Text = "Title ID:";
            // 
            // lblXboxDBIcon
            // 
            this.lblXboxDBIcon.AutoSize = true;
            this.lblXboxDBIcon.Location = new System.Drawing.Point(6, 272);
            this.lblXboxDBIcon.Name = "lblXboxDBIcon";
            this.lblXboxDBIcon.Size = new System.Drawing.Size(86, 13);
            this.lblXboxDBIcon.TabIndex = 60;
            this.lblXboxDBIcon.Text = "Dashboard Icon:";
            // 
            // txtXboxGameTitle
            // 
            this.txtXboxGameTitle.BackColor = System.Drawing.Color.Gold;
            this.txtXboxGameTitle.Location = new System.Drawing.Point(96, 32);
            this.txtXboxGameTitle.Name = "txtXboxGameTitle";
            this.txtXboxGameTitle.ReadOnly = true;
            this.txtXboxGameTitle.Size = new System.Drawing.Size(383, 20);
            this.txtXboxGameTitle.TabIndex = 2;
            this.txtXboxGameTitle.TextChanged += new System.EventHandler(this.setSolutionDirty);
            // 
            // txtXboxDBIcon
            // 
            this.txtXboxDBIcon.Location = new System.Drawing.Point(96, 269);
            this.txtXboxDBIcon.Name = "txtXboxDBIcon";
            this.txtXboxDBIcon.ReadOnly = true;
            this.txtXboxDBIcon.Size = new System.Drawing.Size(383, 20);
            this.txtXboxDBIcon.TabIndex = 20;
            this.txtXboxDBIcon.TabStop = false;
            this.ttGeneric.SetToolTip(this.txtXboxDBIcon, "To clear field, highlight and press \'Delete\'");
            this.txtXboxDBIcon.TextChanged += new System.EventHandler(this.txtXboxDBIcon_TextChanged);
            this.txtXboxDBIcon.KeyUp += new System.Windows.Forms.KeyEventHandler(this.clearTxtField);
            // 
            // lblXboxGameTitle
            // 
            this.lblXboxGameTitle.AutoSize = true;
            this.lblXboxGameTitle.Location = new System.Drawing.Point(31, 35);
            this.lblXboxGameTitle.Name = "lblXboxGameTitle";
            this.lblXboxGameTitle.Size = new System.Drawing.Size(61, 13);
            this.lblXboxGameTitle.TabIndex = 1;
            this.lblXboxGameTitle.Text = "Game Title:";
            // 
            // lblXboxActDate
            // 
            this.lblXboxActDate.AutoSize = true;
            this.lblXboxActDate.Location = new System.Drawing.Point(7, 142);
            this.lblXboxActDate.Name = "lblXboxActDate";
            this.lblXboxActDate.Size = new System.Drawing.Size(83, 13);
            this.lblXboxActDate.TabIndex = 58;
            this.lblXboxActDate.Text = "Activation Date:";
            // 
            // txtXboxOffID
            // 
            this.txtXboxOffID.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtXboxOffID.Location = new System.Drawing.Point(96, 110);
            this.txtXboxOffID.MaxLength = 7;
            this.txtXboxOffID.Name = "txtXboxOffID";
            this.txtXboxOffID.Size = new System.Drawing.Size(64, 20);
            this.txtXboxOffID.TabIndex = 6;
            this.txtXboxOffID.TextChanged += new System.EventHandler(this.setSolutionDirty);
            // 
            // lblXboxOffID
            // 
            this.lblXboxOffID.AutoSize = true;
            this.lblXboxOffID.Location = new System.Drawing.Point(29, 113);
            this.lblXboxOffID.Name = "lblXboxOffID";
            this.lblXboxOffID.Size = new System.Drawing.Size(61, 13);
            this.lblXboxOffID.TabIndex = 56;
            this.lblXboxOffID.Text = "Offering ID:";
            // 
            // lblXboxPubFlags
            // 
            this.lblXboxPubFlags.AutoSize = true;
            this.lblXboxPubFlags.Location = new System.Drawing.Point(9, 87);
            this.lblXboxPubFlags.Name = "lblXboxPubFlags";
            this.lblXboxPubFlags.Size = new System.Drawing.Size(81, 13);
            this.lblXboxPubFlags.TabIndex = 57;
            this.lblXboxPubFlags.Text = "Publisher Flags:";
            // 
            // tabpgPS3
            // 
            this.tabpgPS3.Controls.Add(this.txtPs3JNVer);
            this.tabpgPS3.Controls.Add(this.lblPs3JNVer);
            this.tabpgPS3.Controls.Add(this.txtPs3EUVer);
            this.tabpgPS3.Controls.Add(this.lblPs3EUVer);
            this.tabpgPS3.Controls.Add(this.txtPs3USVer);
            this.tabpgPS3.Controls.Add(this.lblUSVer);
            this.tabpgPS3.Controls.Add(this.btnBrowsePs3JPNParamSfo);
            this.tabpgPS3.Controls.Add(this.txtPs3JPNParamSfo);
            this.tabpgPS3.Controls.Add(this.lblPs3JPNParamSfo);
            this.tabpgPS3.Controls.Add(this.btnBrowsePs3EUParamSfo);
            this.tabpgPS3.Controls.Add(this.txtPs3EUParamSfo);
            this.tabpgPS3.Controls.Add(this.lblPs3EUParamSfo);
            this.tabpgPS3.Controls.Add(this.lblPs3JPNContIDZero);
            this.tabpgPS3.Controls.Add(this.txtPs3JPNCID_Third);
            this.tabpgPS3.Controls.Add(this.lblPs3JPNContIDHyp);
            this.tabpgPS3.Controls.Add(this.txtPs3JPNCID_Second);
            this.tabpgPS3.Controls.Add(this.lblPs3JPNContIDSep);
            this.tabpgPS3.Controls.Add(this.txtPs3JPNCID_First);
            this.tabpgPS3.Controls.Add(this.lblPs3EUContIDZero);
            this.tabpgPS3.Controls.Add(this.txtPs3EUCID_Third);
            this.tabpgPS3.Controls.Add(this.lblPs3EUContIDHyp);
            this.tabpgPS3.Controls.Add(this.txtPs3EUCID_Second);
            this.tabpgPS3.Controls.Add(this.lblPs3EUContIDSep);
            this.tabpgPS3.Controls.Add(this.txtPs3EUCID_First);
            this.tabpgPS3.Controls.Add(this.lblPs3USContIDZero);
            this.tabpgPS3.Controls.Add(this.txtPs3USCID_Third);
            this.tabpgPS3.Controls.Add(this.lblPs3USContIDHyp);
            this.tabpgPS3.Controls.Add(this.txtPs3USCID_Second);
            this.tabpgPS3.Controls.Add(this.lblPs3USContIDSep);
            this.tabpgPS3.Controls.Add(this.txtPs3USCID_First);
            this.tabpgPS3.Controls.Add(this.lblPs3JPNContID);
            this.tabpgPS3.Controls.Add(this.lblPs3EUContID);
            this.tabpgPS3.Controls.Add(this.btnBrowsePs3USParamSfo);
            this.tabpgPS3.Controls.Add(this.btnBrowsePs3Icon0);
            this.tabpgPS3.Controls.Add(this.txtPs3Icon0);
            this.tabpgPS3.Controls.Add(this.lblPs3Icon0);
            this.tabpgPS3.Controls.Add(this.txtPs3USParamSfo);
            this.tabpgPS3.Controls.Add(this.lblPs3USParamSfo);
            this.tabpgPS3.Controls.Add(this.pbPs3Banner);
            this.tabpgPS3.Controls.Add(this.txtPs3ContType);
            this.tabpgPS3.Controls.Add(this.lblPs3ContType);
            this.tabpgPS3.Controls.Add(this.txtPs3DRMType);
            this.tabpgPS3.Controls.Add(this.lblPs3DRMType);
            this.tabpgPS3.Controls.Add(this.txtPs3KLicensee);
            this.tabpgPS3.Controls.Add(this.lblKLicensee);
            this.tabpgPS3.Controls.Add(this.lblPs3USContID);
            this.tabpgPS3.Location = new System.Drawing.Point(4, 22);
            this.tabpgPS3.Name = "tabpgPS3";
            this.tabpgPS3.Padding = new System.Windows.Forms.Padding(3);
            this.tabpgPS3.Size = new System.Drawing.Size(910, 447);
            this.tabpgPS3.TabIndex = 2;
            this.tabpgPS3.Text = "PS3";
            this.tabpgPS3.UseVisualStyleBackColor = true;
            // 
            // txtPs3JNVer
            // 
            this.txtPs3JNVer.Location = new System.Drawing.Point(482, 62);
            this.txtPs3JNVer.Name = "txtPs3JNVer";
            this.txtPs3JNVer.Size = new System.Drawing.Size(51, 20);
            this.txtPs3JNVer.TabIndex = 16;
            // 
            // lblPs3JNVer
            // 
            this.lblPs3JNVer.AutoSize = true;
            this.lblPs3JNVer.Location = new System.Drawing.Point(434, 64);
            this.lblPs3JNVer.Name = "lblPs3JNVer";
            this.lblPs3JNVer.Size = new System.Drawing.Size(42, 13);
            this.lblPs3JNVer.TabIndex = 104;
            this.lblPs3JNVer.Text = "JN Ver:";
            // 
            // txtPs3EUVer
            // 
            this.txtPs3EUVer.Location = new System.Drawing.Point(482, 35);
            this.txtPs3EUVer.Name = "txtPs3EUVer";
            this.txtPs3EUVer.Size = new System.Drawing.Size(51, 20);
            this.txtPs3EUVer.TabIndex = 10;
            // 
            // lblPs3EUVer
            // 
            this.lblPs3EUVer.AutoSize = true;
            this.lblPs3EUVer.Location = new System.Drawing.Point(432, 39);
            this.lblPs3EUVer.Name = "lblPs3EUVer";
            this.lblPs3EUVer.Size = new System.Drawing.Size(44, 13);
            this.lblPs3EUVer.TabIndex = 102;
            this.lblPs3EUVer.Text = "EU Ver:";
            // 
            // txtPs3USVer
            // 
            this.txtPs3USVer.Location = new System.Drawing.Point(482, 7);
            this.txtPs3USVer.Name = "txtPs3USVer";
            this.txtPs3USVer.Size = new System.Drawing.Size(51, 20);
            this.txtPs3USVer.TabIndex = 4;
            // 
            // lblUSVer
            // 
            this.lblUSVer.AutoSize = true;
            this.lblUSVer.Location = new System.Drawing.Point(434, 11);
            this.lblUSVer.Name = "lblUSVer";
            this.lblUSVer.Size = new System.Drawing.Size(44, 13);
            this.lblUSVer.TabIndex = 100;
            this.lblUSVer.Text = "US Ver:";
            // 
            // btnBrowsePs3JPNParamSfo
            // 
            this.btnBrowsePs3JPNParamSfo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBrowsePs3JPNParamSfo.Location = new System.Drawing.Point(867, 60);
            this.btnBrowsePs3JPNParamSfo.Name = "btnBrowsePs3JPNParamSfo";
            this.btnBrowsePs3JPNParamSfo.Size = new System.Drawing.Size(31, 23);
            this.btnBrowsePs3JPNParamSfo.TabIndex = 18;
            this.btnBrowsePs3JPNParamSfo.Text = "...";
            this.ttGeneric.SetToolTip(this.btnBrowsePs3JPNParamSfo, "Browse for files, use the \'Delete\' key to clear this field.");
            this.btnBrowsePs3JPNParamSfo.UseVisualStyleBackColor = true;
            this.btnBrowsePs3JPNParamSfo.Click += new System.EventHandler(this.btnBrowsePs3JPNParamSfo_Click);
            // 
            // txtPs3JPNParamSfo
            // 
            this.txtPs3JPNParamSfo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPs3JPNParamSfo.Location = new System.Drawing.Point(604, 62);
            this.txtPs3JPNParamSfo.Name = "txtPs3JPNParamSfo";
            this.txtPs3JPNParamSfo.ReadOnly = true;
            this.txtPs3JPNParamSfo.Size = new System.Drawing.Size(257, 20);
            this.txtPs3JPNParamSfo.TabIndex = 17;
            this.txtPs3JPNParamSfo.TabStop = false;
            this.ttGeneric.SetToolTip(this.txtPs3JPNParamSfo, "To clear field, highlight and press \'Delete\'");
            this.txtPs3JPNParamSfo.TextChanged += new System.EventHandler(this.setSolutionDirty);
            this.txtPs3JPNParamSfo.KeyUp += new System.Windows.Forms.KeyEventHandler(this.clearTxtField);
            // 
            // lblPs3JPNParamSfo
            // 
            this.lblPs3JPNParamSfo.AutoSize = true;
            this.lblPs3JPNParamSfo.Location = new System.Drawing.Point(542, 65);
            this.lblPs3JPNParamSfo.Name = "lblPs3JPNParamSfo";
            this.lblPs3JPNParamSfo.Size = new System.Drawing.Size(56, 13);
            this.lblPs3JPNParamSfo.TabIndex = 98;
            this.lblPs3JPNParamSfo.Text = "JN Param:";
            this.ttGeneric.SetToolTip(this.lblPs3JPNParamSfo, "Select the relevant SFO file for this region");
            // 
            // btnBrowsePs3EUParamSfo
            // 
            this.btnBrowsePs3EUParamSfo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBrowsePs3EUParamSfo.Location = new System.Drawing.Point(867, 32);
            this.btnBrowsePs3EUParamSfo.Name = "btnBrowsePs3EUParamSfo";
            this.btnBrowsePs3EUParamSfo.Size = new System.Drawing.Size(31, 23);
            this.btnBrowsePs3EUParamSfo.TabIndex = 12;
            this.btnBrowsePs3EUParamSfo.Text = "...";
            this.ttGeneric.SetToolTip(this.btnBrowsePs3EUParamSfo, "Browse for files, use the \'Delete\' key to clear this field.");
            this.btnBrowsePs3EUParamSfo.UseVisualStyleBackColor = true;
            this.btnBrowsePs3EUParamSfo.Click += new System.EventHandler(this.btnBrowsePs3EUParamSfo_Click);
            // 
            // txtPs3EUParamSfo
            // 
            this.txtPs3EUParamSfo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPs3EUParamSfo.Location = new System.Drawing.Point(604, 36);
            this.txtPs3EUParamSfo.Name = "txtPs3EUParamSfo";
            this.txtPs3EUParamSfo.ReadOnly = true;
            this.txtPs3EUParamSfo.Size = new System.Drawing.Size(257, 20);
            this.txtPs3EUParamSfo.TabIndex = 11;
            this.txtPs3EUParamSfo.TabStop = false;
            this.ttGeneric.SetToolTip(this.txtPs3EUParamSfo, "To clear field, highlight and press \'Delete\'");
            this.txtPs3EUParamSfo.TextChanged += new System.EventHandler(this.setSolutionDirty);
            this.txtPs3EUParamSfo.KeyUp += new System.Windows.Forms.KeyEventHandler(this.clearTxtField);
            // 
            // lblPs3EUParamSfo
            // 
            this.lblPs3EUParamSfo.AutoSize = true;
            this.lblPs3EUParamSfo.Location = new System.Drawing.Point(540, 39);
            this.lblPs3EUParamSfo.Name = "lblPs3EUParamSfo";
            this.lblPs3EUParamSfo.Size = new System.Drawing.Size(58, 13);
            this.lblPs3EUParamSfo.TabIndex = 95;
            this.lblPs3EUParamSfo.Text = "EU Param:";
            this.ttGeneric.SetToolTip(this.lblPs3EUParamSfo, "Select the relevant SFO file for this region");
            // 
            // lblPs3JPNContIDZero
            // 
            this.lblPs3JPNContIDZero.AutoSize = true;
            this.lblPs3JPNContIDZero.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPs3JPNContIDZero.Location = new System.Drawing.Point(246, 64);
            this.lblPs3JPNContIDZero.Name = "lblPs3JPNContIDZero";
            this.lblPs3JPNContIDZero.Size = new System.Drawing.Size(28, 15);
            this.lblPs3JPNContIDZero.TabIndex = 92;
            this.lblPs3JPNContIDZero.Text = "_00";
            // 
            // txtPs3JPNCID_Third
            // 
            this.txtPs3JPNCID_Third.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPs3JPNCID_Third.Location = new System.Drawing.Point(300, 61);
            this.txtPs3JPNCID_Third.MaxLength = 16;
            this.txtPs3JPNCID_Third.Name = "txtPs3JPNCID_Third";
            this.txtPs3JPNCID_Third.Size = new System.Drawing.Size(126, 21);
            this.txtPs3JPNCID_Third.TabIndex = 15;
            this.txtPs3JPNCID_Third.TextChanged += new System.EventHandler(this.setSolutionDirty);
            // 
            // lblPs3JPNContIDHyp
            // 
            this.lblPs3JPNContIDHyp.AutoSize = true;
            this.lblPs3JPNContIDHyp.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPs3JPNContIDHyp.Location = new System.Drawing.Point(280, 64);
            this.lblPs3JPNContIDHyp.Name = "lblPs3JPNContIDHyp";
            this.lblPs3JPNContIDHyp.Size = new System.Drawing.Size(14, 15);
            this.lblPs3JPNContIDHyp.TabIndex = 90;
            this.lblPs3JPNContIDHyp.Text = "-";
            // 
            // txtPs3JPNCID_Second
            // 
            this.txtPs3JPNCID_Second.BackColor = System.Drawing.Color.Gold;
            this.txtPs3JPNCID_Second.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPs3JPNCID_Second.Location = new System.Drawing.Point(168, 61);
            this.txtPs3JPNCID_Second.MaxLength = 9;
            this.txtPs3JPNCID_Second.Name = "txtPs3JPNCID_Second";
            this.txtPs3JPNCID_Second.ReadOnly = true;
            this.txtPs3JPNCID_Second.Size = new System.Drawing.Size(72, 21);
            this.txtPs3JPNCID_Second.TabIndex = 14;
            this.txtPs3JPNCID_Second.TextChanged += new System.EventHandler(this.setSolutionDirty);
            // 
            // lblPs3JPNContIDSep
            // 
            this.lblPs3JPNContIDSep.AutoSize = true;
            this.lblPs3JPNContIDSep.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPs3JPNContIDSep.Location = new System.Drawing.Point(148, 64);
            this.lblPs3JPNContIDSep.Name = "lblPs3JPNContIDSep";
            this.lblPs3JPNContIDSep.Size = new System.Drawing.Size(14, 15);
            this.lblPs3JPNContIDSep.TabIndex = 88;
            this.lblPs3JPNContIDSep.Text = "-";
            // 
            // txtPs3JPNCID_First
            // 
            this.txtPs3JPNCID_First.BackColor = System.Drawing.Color.Gold;
            this.txtPs3JPNCID_First.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPs3JPNCID_First.Location = new System.Drawing.Point(90, 61);
            this.txtPs3JPNCID_First.MaxLength = 6;
            this.txtPs3JPNCID_First.Name = "txtPs3JPNCID_First";
            this.txtPs3JPNCID_First.ReadOnly = true;
            this.txtPs3JPNCID_First.Size = new System.Drawing.Size(52, 21);
            this.txtPs3JPNCID_First.TabIndex = 13;
            this.txtPs3JPNCID_First.TextChanged += new System.EventHandler(this.setSolutionDirty);
            // 
            // lblPs3EUContIDZero
            // 
            this.lblPs3EUContIDZero.AutoSize = true;
            this.lblPs3EUContIDZero.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPs3EUContIDZero.Location = new System.Drawing.Point(246, 37);
            this.lblPs3EUContIDZero.Name = "lblPs3EUContIDZero";
            this.lblPs3EUContIDZero.Size = new System.Drawing.Size(28, 15);
            this.lblPs3EUContIDZero.TabIndex = 86;
            this.lblPs3EUContIDZero.Text = "_00";
            // 
            // txtPs3EUCID_Third
            // 
            this.txtPs3EUCID_Third.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPs3EUCID_Third.Location = new System.Drawing.Point(300, 34);
            this.txtPs3EUCID_Third.MaxLength = 16;
            this.txtPs3EUCID_Third.Name = "txtPs3EUCID_Third";
            this.txtPs3EUCID_Third.Size = new System.Drawing.Size(126, 21);
            this.txtPs3EUCID_Third.TabIndex = 9;
            this.txtPs3EUCID_Third.TextChanged += new System.EventHandler(this.setSolutionDirty);
            // 
            // lblPs3EUContIDHyp
            // 
            this.lblPs3EUContIDHyp.AutoSize = true;
            this.lblPs3EUContIDHyp.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPs3EUContIDHyp.Location = new System.Drawing.Point(280, 37);
            this.lblPs3EUContIDHyp.Name = "lblPs3EUContIDHyp";
            this.lblPs3EUContIDHyp.Size = new System.Drawing.Size(14, 15);
            this.lblPs3EUContIDHyp.TabIndex = 84;
            this.lblPs3EUContIDHyp.Text = "-";
            // 
            // txtPs3EUCID_Second
            // 
            this.txtPs3EUCID_Second.BackColor = System.Drawing.Color.Gold;
            this.txtPs3EUCID_Second.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPs3EUCID_Second.Location = new System.Drawing.Point(168, 34);
            this.txtPs3EUCID_Second.MaxLength = 9;
            this.txtPs3EUCID_Second.Name = "txtPs3EUCID_Second";
            this.txtPs3EUCID_Second.ReadOnly = true;
            this.txtPs3EUCID_Second.Size = new System.Drawing.Size(72, 21);
            this.txtPs3EUCID_Second.TabIndex = 8;
            this.txtPs3EUCID_Second.TextChanged += new System.EventHandler(this.setSolutionDirty);
            // 
            // lblPs3EUContIDSep
            // 
            this.lblPs3EUContIDSep.AutoSize = true;
            this.lblPs3EUContIDSep.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPs3EUContIDSep.Location = new System.Drawing.Point(148, 37);
            this.lblPs3EUContIDSep.Name = "lblPs3EUContIDSep";
            this.lblPs3EUContIDSep.Size = new System.Drawing.Size(14, 15);
            this.lblPs3EUContIDSep.TabIndex = 82;
            this.lblPs3EUContIDSep.Text = "-";
            // 
            // txtPs3EUCID_First
            // 
            this.txtPs3EUCID_First.BackColor = System.Drawing.Color.Gold;
            this.txtPs3EUCID_First.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPs3EUCID_First.Location = new System.Drawing.Point(90, 34);
            this.txtPs3EUCID_First.MaxLength = 6;
            this.txtPs3EUCID_First.Name = "txtPs3EUCID_First";
            this.txtPs3EUCID_First.ReadOnly = true;
            this.txtPs3EUCID_First.Size = new System.Drawing.Size(52, 21);
            this.txtPs3EUCID_First.TabIndex = 7;
            this.txtPs3EUCID_First.TextChanged += new System.EventHandler(this.setSolutionDirty);
            // 
            // lblPs3USContIDZero
            // 
            this.lblPs3USContIDZero.AutoSize = true;
            this.lblPs3USContIDZero.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPs3USContIDZero.Location = new System.Drawing.Point(246, 10);
            this.lblPs3USContIDZero.Name = "lblPs3USContIDZero";
            this.lblPs3USContIDZero.Size = new System.Drawing.Size(28, 15);
            this.lblPs3USContIDZero.TabIndex = 80;
            this.lblPs3USContIDZero.Text = "_00";
            // 
            // txtPs3USCID_Third
            // 
            this.txtPs3USCID_Third.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPs3USCID_Third.Location = new System.Drawing.Point(300, 7);
            this.txtPs3USCID_Third.MaxLength = 16;
            this.txtPs3USCID_Third.Name = "txtPs3USCID_Third";
            this.txtPs3USCID_Third.Size = new System.Drawing.Size(126, 21);
            this.txtPs3USCID_Third.TabIndex = 3;
            this.txtPs3USCID_Third.TextChanged += new System.EventHandler(this.setSolutionDirty);
            // 
            // lblPs3USContIDHyp
            // 
            this.lblPs3USContIDHyp.AutoSize = true;
            this.lblPs3USContIDHyp.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPs3USContIDHyp.Location = new System.Drawing.Point(280, 10);
            this.lblPs3USContIDHyp.Name = "lblPs3USContIDHyp";
            this.lblPs3USContIDHyp.Size = new System.Drawing.Size(14, 15);
            this.lblPs3USContIDHyp.TabIndex = 78;
            this.lblPs3USContIDHyp.Text = "-";
            // 
            // txtPs3USCID_Second
            // 
            this.txtPs3USCID_Second.BackColor = System.Drawing.Color.Gold;
            this.txtPs3USCID_Second.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPs3USCID_Second.Location = new System.Drawing.Point(168, 7);
            this.txtPs3USCID_Second.MaxLength = 9;
            this.txtPs3USCID_Second.Name = "txtPs3USCID_Second";
            this.txtPs3USCID_Second.ReadOnly = true;
            this.txtPs3USCID_Second.Size = new System.Drawing.Size(72, 21);
            this.txtPs3USCID_Second.TabIndex = 2;
            this.txtPs3USCID_Second.TextChanged += new System.EventHandler(this.setSolutionDirty);
            // 
            // lblPs3USContIDSep
            // 
            this.lblPs3USContIDSep.AutoSize = true;
            this.lblPs3USContIDSep.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPs3USContIDSep.Location = new System.Drawing.Point(148, 10);
            this.lblPs3USContIDSep.Name = "lblPs3USContIDSep";
            this.lblPs3USContIDSep.Size = new System.Drawing.Size(14, 15);
            this.lblPs3USContIDSep.TabIndex = 76;
            this.lblPs3USContIDSep.Text = "-";
            // 
            // txtPs3USCID_First
            // 
            this.txtPs3USCID_First.BackColor = System.Drawing.Color.Gold;
            this.txtPs3USCID_First.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPs3USCID_First.Location = new System.Drawing.Point(90, 6);
            this.txtPs3USCID_First.MaxLength = 6;
            this.txtPs3USCID_First.Name = "txtPs3USCID_First";
            this.txtPs3USCID_First.ReadOnly = true;
            this.txtPs3USCID_First.Size = new System.Drawing.Size(52, 21);
            this.txtPs3USCID_First.TabIndex = 1;
            this.txtPs3USCID_First.TextChanged += new System.EventHandler(this.setSolutionDirty);
            // 
            // lblPs3JPNContID
            // 
            this.lblPs3JPNContID.AutoSize = true;
            this.lblPs3JPNContID.Location = new System.Drawing.Point(7, 65);
            this.lblPs3JPNContID.Name = "lblPs3JPNContID";
            this.lblPs3JPNContID.Size = new System.Drawing.Size(77, 13);
            this.lblPs3JPNContID.TabIndex = 73;
            this.lblPs3JPNContID.Text = "JN Content ID:";
            // 
            // lblPs3EUContID
            // 
            this.lblPs3EUContID.AutoSize = true;
            this.lblPs3EUContID.Location = new System.Drawing.Point(5, 39);
            this.lblPs3EUContID.Name = "lblPs3EUContID";
            this.lblPs3EUContID.Size = new System.Drawing.Size(79, 13);
            this.lblPs3EUContID.TabIndex = 71;
            this.lblPs3EUContID.Text = "EU Content ID:";
            // 
            // btnBrowsePs3USParamSfo
            // 
            this.btnBrowsePs3USParamSfo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBrowsePs3USParamSfo.Location = new System.Drawing.Point(867, 6);
            this.btnBrowsePs3USParamSfo.Name = "btnBrowsePs3USParamSfo";
            this.btnBrowsePs3USParamSfo.Size = new System.Drawing.Size(31, 23);
            this.btnBrowsePs3USParamSfo.TabIndex = 6;
            this.btnBrowsePs3USParamSfo.Text = "...";
            this.ttGeneric.SetToolTip(this.btnBrowsePs3USParamSfo, "Browse for files, use the \'Delete\' key to clear this field.");
            this.btnBrowsePs3USParamSfo.UseVisualStyleBackColor = true;
            this.btnBrowsePs3USParamSfo.Click += new System.EventHandler(this.btnBrowsePs3ParamSfo_Click);
            // 
            // btnBrowsePs3Icon0
            // 
            this.btnBrowsePs3Icon0.Location = new System.Drawing.Point(432, 165);
            this.btnBrowsePs3Icon0.Name = "btnBrowsePs3Icon0";
            this.btnBrowsePs3Icon0.Size = new System.Drawing.Size(31, 23);
            this.btnBrowsePs3Icon0.TabIndex = 23;
            this.btnBrowsePs3Icon0.Text = "...";
            this.ttGeneric.SetToolTip(this.btnBrowsePs3Icon0, "Browse for files, use the \'Delete\' key to clear this field.");
            this.btnBrowsePs3Icon0.UseVisualStyleBackColor = true;
            this.btnBrowsePs3Icon0.Click += new System.EventHandler(this.btnBrowsePs3Icon0_Click);
            // 
            // txtPs3Icon0
            // 
            this.txtPs3Icon0.Location = new System.Drawing.Point(90, 167);
            this.txtPs3Icon0.Name = "txtPs3Icon0";
            this.txtPs3Icon0.ReadOnly = true;
            this.txtPs3Icon0.Size = new System.Drawing.Size(336, 20);
            this.txtPs3Icon0.TabIndex = 22;
            this.txtPs3Icon0.TabStop = false;
            this.ttGeneric.SetToolTip(this.txtPs3Icon0, "To clear field, highlight and press \'Delete\'");
            this.txtPs3Icon0.TextChanged += new System.EventHandler(this.txtPs3Icon0_TextChanged);
            this.txtPs3Icon0.KeyUp += new System.Windows.Forms.KeyEventHandler(this.clearTxtField);
            // 
            // lblPs3Icon0
            // 
            this.lblPs3Icon0.AutoSize = true;
            this.lblPs3Icon0.Location = new System.Drawing.Point(47, 170);
            this.lblPs3Icon0.Name = "lblPs3Icon0";
            this.lblPs3Icon0.Size = new System.Drawing.Size(37, 13);
            this.lblPs3Icon0.TabIndex = 70;
            this.lblPs3Icon0.Text = "Icon0:";
            // 
            // txtPs3USParamSfo
            // 
            this.txtPs3USParamSfo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPs3USParamSfo.Location = new System.Drawing.Point(604, 8);
            this.txtPs3USParamSfo.Name = "txtPs3USParamSfo";
            this.txtPs3USParamSfo.ReadOnly = true;
            this.txtPs3USParamSfo.Size = new System.Drawing.Size(257, 20);
            this.txtPs3USParamSfo.TabIndex = 5;
            this.txtPs3USParamSfo.TabStop = false;
            this.ttGeneric.SetToolTip(this.txtPs3USParamSfo, "To clear field, highlight and press \'Delete\'");
            this.txtPs3USParamSfo.TextChanged += new System.EventHandler(this.setSolutionDirty);
            this.txtPs3USParamSfo.KeyUp += new System.Windows.Forms.KeyEventHandler(this.clearTxtField);
            // 
            // lblPs3USParamSfo
            // 
            this.lblPs3USParamSfo.AutoSize = true;
            this.lblPs3USParamSfo.Location = new System.Drawing.Point(540, 11);
            this.lblPs3USParamSfo.Name = "lblPs3USParamSfo";
            this.lblPs3USParamSfo.Size = new System.Drawing.Size(58, 13);
            this.lblPs3USParamSfo.TabIndex = 68;
            this.lblPs3USParamSfo.Text = "US Param:";
            this.ttGeneric.SetToolTip(this.lblPs3USParamSfo, "Select the relevant SFO file for this region");
            // 
            // pbPs3Banner
            // 
            this.pbPs3Banner.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pbPs3Banner.Location = new System.Drawing.Point(90, 193);
            this.pbPs3Banner.Name = "pbPs3Banner";
            this.pbPs3Banner.Size = new System.Drawing.Size(420, 95);
            this.pbPs3Banner.TabIndex = 66;
            this.pbPs3Banner.TabStop = false;
            this.ttGeneric.SetToolTip(this.pbPs3Banner, "Icon0");
            // 
            // txtPs3ContType
            // 
            this.txtPs3ContType.Location = new System.Drawing.Point(90, 114);
            this.txtPs3ContType.Name = "txtPs3ContType";
            this.txtPs3ContType.Size = new System.Drawing.Size(336, 20);
            this.txtPs3ContType.TabIndex = 20;
            this.txtPs3ContType.TextChanged += new System.EventHandler(this.setSolutionDirty);
            // 
            // lblPs3ContType
            // 
            this.lblPs3ContType.AutoSize = true;
            this.lblPs3ContType.Location = new System.Drawing.Point(49, 116);
            this.lblPs3ContType.Name = "lblPs3ContType";
            this.lblPs3ContType.Size = new System.Drawing.Size(34, 13);
            this.lblPs3ContType.TabIndex = 62;
            this.lblPs3ContType.Text = "Type:";
            // 
            // txtPs3DRMType
            // 
            this.txtPs3DRMType.Location = new System.Drawing.Point(90, 88);
            this.txtPs3DRMType.Name = "txtPs3DRMType";
            this.txtPs3DRMType.Size = new System.Drawing.Size(336, 20);
            this.txtPs3DRMType.TabIndex = 19;
            this.txtPs3DRMType.TextChanged += new System.EventHandler(this.setSolutionDirty);
            // 
            // lblPs3DRMType
            // 
            this.lblPs3DRMType.AutoSize = true;
            this.lblPs3DRMType.Location = new System.Drawing.Point(49, 90);
            this.lblPs3DRMType.Name = "lblPs3DRMType";
            this.lblPs3DRMType.Size = new System.Drawing.Size(35, 13);
            this.lblPs3DRMType.TabIndex = 60;
            this.lblPs3DRMType.Text = "DRM:";
            // 
            // txtPs3KLicensee
            // 
            this.txtPs3KLicensee.BackColor = System.Drawing.Color.Gold;
            this.txtPs3KLicensee.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPs3KLicensee.Location = new System.Drawing.Point(90, 140);
            this.txtPs3KLicensee.MaxLength = 34;
            this.txtPs3KLicensee.Name = "txtPs3KLicensee";
            this.txtPs3KLicensee.ReadOnly = true;
            this.txtPs3KLicensee.Size = new System.Drawing.Size(336, 21);
            this.txtPs3KLicensee.TabIndex = 21;
            this.txtPs3KLicensee.TextChanged += new System.EventHandler(this.setSolutionDirty);
            // 
            // lblKLicensee
            // 
            this.lblKLicensee.AutoSize = true;
            this.lblKLicensee.Location = new System.Drawing.Point(28, 143);
            this.lblKLicensee.Name = "lblKLicensee";
            this.lblKLicensee.Size = new System.Drawing.Size(56, 13);
            this.lblKLicensee.TabIndex = 58;
            this.lblKLicensee.Text = "Klicensee:";
            // 
            // lblPs3USContID
            // 
            this.lblPs3USContID.AutoSize = true;
            this.lblPs3USContID.Location = new System.Drawing.Point(5, 11);
            this.lblPs3USContID.Name = "lblPs3USContID";
            this.lblPs3USContID.Size = new System.Drawing.Size(79, 13);
            this.lblPs3USContID.TabIndex = 1;
            this.lblPs3USContID.Text = "US Content ID:";
            // 
            // tabpgPC
            // 
            this.tabpgPC.Location = new System.Drawing.Point(4, 22);
            this.tabpgPC.Name = "tabpgPC";
            this.tabpgPC.Padding = new System.Windows.Forms.Padding(3);
            this.tabpgPC.Size = new System.Drawing.Size(910, 447);
            this.tabpgPC.TabIndex = 3;
            this.tabpgPC.Text = "PC";
            this.tabpgPC.UseVisualStyleBackColor = true;
            // 
            // tbPS4
            // 
            this.tbPS4.Controls.Add(this.txtPS4VolType);
            this.tbPS4.Controls.Add(this.lblPS4VolType);
            this.tbPS4.Controls.Add(this.btnPS4CodeGen);
            this.tbPS4.Controls.Add(this.txtPS4VerJPN);
            this.tbPS4.Controls.Add(this.lblPS4JNVer);
            this.tbPS4.Controls.Add(this.txtPS4VerEU);
            this.tbPS4.Controls.Add(this.lblPS4EUVer);
            this.tbPS4.Controls.Add(this.txtPS4VerUS);
            this.tbPS4.Controls.Add(this.lblPS4USVer);
            this.tbPS4.Controls.Add(this.btnPS4JPNParamSfo);
            this.tbPS4.Controls.Add(this.txtPS4JPNParamSfo);
            this.tbPS4.Controls.Add(this.lblPS4JNParam);
            this.tbPS4.Controls.Add(this.btnPS4EUParamSfo);
            this.tbPS4.Controls.Add(this.txtPS4EUParamSfo);
            this.tbPS4.Controls.Add(this.lblPS4EUParam);
            this.tbPS4.Controls.Add(this.lblPS4JNContIDZero);
            this.tbPS4.Controls.Add(this.txtPS4JPNContID_Third);
            this.tbPS4.Controls.Add(this.lblPS4JPNContIDHyp);
            this.tbPS4.Controls.Add(this.txtPS4JPNContID_Second);
            this.tbPS4.Controls.Add(this.label11);
            this.tbPS4.Controls.Add(this.txtPS4JPNContID_First);
            this.tbPS4.Controls.Add(this.lblPS4EUContIDZero);
            this.tbPS4.Controls.Add(this.txtPS4EUContID_Third);
            this.tbPS4.Controls.Add(this.lblPS4EUContIDHyp);
            this.tbPS4.Controls.Add(this.txtPS4EUContID_Second);
            this.tbPS4.Controls.Add(this.label14);
            this.tbPS4.Controls.Add(this.txtPS4EUContID_First);
            this.tbPS4.Controls.Add(this.lblPS4USContIDZero);
            this.tbPS4.Controls.Add(this.txtPS4USContID_Third);
            this.tbPS4.Controls.Add(this.lblPS4USContIDHyp);
            this.tbPS4.Controls.Add(this.txtPS4USContID_Second);
            this.tbPS4.Controls.Add(this.label17);
            this.tbPS4.Controls.Add(this.txtPS4USContID_First);
            this.tbPS4.Controls.Add(this.lblPS4JPNContID);
            this.tbPS4.Controls.Add(this.lblPS4EUContID);
            this.tbPS4.Controls.Add(this.btnPS4USParamSfo);
            this.tbPS4.Controls.Add(this.txtPS4Icon0Browse);
            this.tbPS4.Controls.Add(this.txtPS4Icon0);
            this.tbPS4.Controls.Add(this.lblPS4Icon);
            this.tbPS4.Controls.Add(this.txtPS4USParamSfo);
            this.tbPS4.Controls.Add(this.lblPS4USParam);
            this.tbPS4.Controls.Add(this.pbPS4Icon0);
            this.tbPS4.Controls.Add(this.txtPS4Code);
            this.tbPS4.Controls.Add(this.lblPS4Passcode);
            this.tbPS4.Controls.Add(this.txtPS4Entitlement);
            this.tbPS4.Controls.Add(this.lblPS4Entitlement);
            this.tbPS4.Controls.Add(this.lblPS4USContID);
            this.tbPS4.Location = new System.Drawing.Point(4, 22);
            this.tbPS4.Name = "tbPS4";
            this.tbPS4.Padding = new System.Windows.Forms.Padding(3);
            this.tbPS4.Size = new System.Drawing.Size(910, 447);
            this.tbPS4.TabIndex = 5;
            this.tbPS4.Text = "PS4";
            this.tbPS4.UseVisualStyleBackColor = true;
            // 
            // txtPS4VolType
            // 
            this.txtPS4VolType.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.txtPS4VolType.Location = new System.Drawing.Point(605, 141);
            this.txtPS4VolType.MaxLength = 32;
            this.txtPS4VolType.Name = "txtPS4VolType";
            this.txtPS4VolType.Size = new System.Drawing.Size(256, 20);
            this.txtPS4VolType.TabIndex = 152;
            // 
            // lblPS4VolType
            // 
            this.lblPS4VolType.AutoSize = true;
            this.lblPS4VolType.Location = new System.Drawing.Point(565, 144);
            this.lblPS4VolType.Name = "lblPS4VolType";
            this.lblPS4VolType.Size = new System.Drawing.Size(34, 13);
            this.lblPS4VolType.TabIndex = 153;
            this.lblPS4VolType.Text = "Type:";
            // 
            // btnPS4CodeGen
            // 
            this.btnPS4CodeGen.Location = new System.Drawing.Point(868, 86);
            this.btnPS4CodeGen.Name = "btnPS4CodeGen";
            this.btnPS4CodeGen.Size = new System.Drawing.Size(31, 23);
            this.btnPS4CodeGen.TabIndex = 151;
            this.btnPS4CodeGen.Text = "?";
            this.ttGeneric.SetToolTip(this.btnPS4CodeGen, "Mystery button where do you go? If I press you what secrets will I know? Will you" +
                    " prove P != NP in a single move? I suppose I have nothing left to lose...");
            this.btnPS4CodeGen.UseVisualStyleBackColor = true;
            this.btnPS4CodeGen.Click += new System.EventHandler(this.btnPS4CodeGen_Click);
            // 
            // txtPS4VerJPN
            // 
            this.txtPS4VerJPN.Location = new System.Drawing.Point(483, 62);
            this.txtPS4VerJPN.Name = "txtPS4VerJPN";
            this.txtPS4VerJPN.Size = new System.Drawing.Size(51, 20);
            this.txtPS4VerJPN.TabIndex = 121;
            this.txtPS4VerJPN.TextChanged += new System.EventHandler(this.setSolutionDirty);
            // 
            // lblPS4JNVer
            // 
            this.lblPS4JNVer.AutoSize = true;
            this.lblPS4JNVer.Location = new System.Drawing.Point(435, 64);
            this.lblPS4JNVer.Name = "lblPS4JNVer";
            this.lblPS4JNVer.Size = new System.Drawing.Size(42, 13);
            this.lblPS4JNVer.TabIndex = 150;
            this.lblPS4JNVer.Text = "JN Ver:";
            // 
            // txtPS4VerEU
            // 
            this.txtPS4VerEU.Location = new System.Drawing.Point(483, 35);
            this.txtPS4VerEU.Name = "txtPS4VerEU";
            this.txtPS4VerEU.Size = new System.Drawing.Size(51, 20);
            this.txtPS4VerEU.TabIndex = 115;
            this.txtPS4VerEU.TextChanged += new System.EventHandler(this.setSolutionDirty);
            // 
            // lblPS4EUVer
            // 
            this.lblPS4EUVer.AutoSize = true;
            this.lblPS4EUVer.Location = new System.Drawing.Point(433, 39);
            this.lblPS4EUVer.Name = "lblPS4EUVer";
            this.lblPS4EUVer.Size = new System.Drawing.Size(44, 13);
            this.lblPS4EUVer.TabIndex = 149;
            this.lblPS4EUVer.Text = "EU Ver:";
            // 
            // txtPS4VerUS
            // 
            this.txtPS4VerUS.Location = new System.Drawing.Point(483, 7);
            this.txtPS4VerUS.Name = "txtPS4VerUS";
            this.txtPS4VerUS.Size = new System.Drawing.Size(51, 20);
            this.txtPS4VerUS.TabIndex = 109;
            this.txtPS4VerUS.TextChanged += new System.EventHandler(this.setSolutionDirty);
            // 
            // lblPS4USVer
            // 
            this.lblPS4USVer.AutoSize = true;
            this.lblPS4USVer.Location = new System.Drawing.Point(435, 11);
            this.lblPS4USVer.Name = "lblPS4USVer";
            this.lblPS4USVer.Size = new System.Drawing.Size(44, 13);
            this.lblPS4USVer.TabIndex = 148;
            this.lblPS4USVer.Text = "US Ver:";
            // 
            // btnPS4JPNParamSfo
            // 
            this.btnPS4JPNParamSfo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPS4JPNParamSfo.Location = new System.Drawing.Point(868, 60);
            this.btnPS4JPNParamSfo.Name = "btnPS4JPNParamSfo";
            this.btnPS4JPNParamSfo.Size = new System.Drawing.Size(31, 23);
            this.btnPS4JPNParamSfo.TabIndex = 123;
            this.btnPS4JPNParamSfo.Text = "...";
            this.ttGeneric.SetToolTip(this.btnPS4JPNParamSfo, "Browse for files, use the \'Delete\' key to clear this field.");
            this.btnPS4JPNParamSfo.UseVisualStyleBackColor = true;
            this.btnPS4JPNParamSfo.Click += new System.EventHandler(this.btnBrowsePS4JPNParamSfo_Click);
            // 
            // txtPS4JPNParamSfo
            // 
            this.txtPS4JPNParamSfo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPS4JPNParamSfo.Location = new System.Drawing.Point(605, 62);
            this.txtPS4JPNParamSfo.Name = "txtPS4JPNParamSfo";
            this.txtPS4JPNParamSfo.ReadOnly = true;
            this.txtPS4JPNParamSfo.Size = new System.Drawing.Size(257, 20);
            this.txtPS4JPNParamSfo.TabIndex = 122;
            this.txtPS4JPNParamSfo.TabStop = false;
            this.ttGeneric.SetToolTip(this.txtPS4JPNParamSfo, "To clear field, highlight and press \'Delete\'");
            this.txtPS4JPNParamSfo.TextChanged += new System.EventHandler(this.setSolutionDirty);
            this.txtPS4JPNParamSfo.KeyUp += new System.Windows.Forms.KeyEventHandler(this.clearTxtField);
            // 
            // lblPS4JNParam
            // 
            this.lblPS4JNParam.AutoSize = true;
            this.lblPS4JNParam.Location = new System.Drawing.Point(543, 65);
            this.lblPS4JNParam.Name = "lblPS4JNParam";
            this.lblPS4JNParam.Size = new System.Drawing.Size(56, 13);
            this.lblPS4JNParam.TabIndex = 147;
            this.lblPS4JNParam.Text = "JN Param:";
            this.ttGeneric.SetToolTip(this.lblPS4JNParam, "Select the relevant SFO file for this region");
            // 
            // btnPS4EUParamSfo
            // 
            this.btnPS4EUParamSfo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPS4EUParamSfo.Location = new System.Drawing.Point(868, 32);
            this.btnPS4EUParamSfo.Name = "btnPS4EUParamSfo";
            this.btnPS4EUParamSfo.Size = new System.Drawing.Size(31, 23);
            this.btnPS4EUParamSfo.TabIndex = 117;
            this.btnPS4EUParamSfo.Text = "...";
            this.ttGeneric.SetToolTip(this.btnPS4EUParamSfo, "Browse for files, use the \'Delete\' key to clear this field.");
            this.btnPS4EUParamSfo.UseVisualStyleBackColor = true;
            this.btnPS4EUParamSfo.Click += new System.EventHandler(this.btnBrowsePS4EUParamSfo_Click);
            // 
            // txtPS4EUParamSfo
            // 
            this.txtPS4EUParamSfo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPS4EUParamSfo.Location = new System.Drawing.Point(605, 36);
            this.txtPS4EUParamSfo.Name = "txtPS4EUParamSfo";
            this.txtPS4EUParamSfo.ReadOnly = true;
            this.txtPS4EUParamSfo.Size = new System.Drawing.Size(257, 20);
            this.txtPS4EUParamSfo.TabIndex = 116;
            this.txtPS4EUParamSfo.TabStop = false;
            this.ttGeneric.SetToolTip(this.txtPS4EUParamSfo, "To clear field, highlight and press \'Delete\'");
            this.txtPS4EUParamSfo.TextChanged += new System.EventHandler(this.setSolutionDirty);
            this.txtPS4EUParamSfo.KeyUp += new System.Windows.Forms.KeyEventHandler(this.clearTxtField);
            // 
            // lblPS4EUParam
            // 
            this.lblPS4EUParam.AutoSize = true;
            this.lblPS4EUParam.Location = new System.Drawing.Point(541, 39);
            this.lblPS4EUParam.Name = "lblPS4EUParam";
            this.lblPS4EUParam.Size = new System.Drawing.Size(58, 13);
            this.lblPS4EUParam.TabIndex = 146;
            this.lblPS4EUParam.Text = "EU Param:";
            this.ttGeneric.SetToolTip(this.lblPS4EUParam, "Select the relevant SFO file for this region");
            // 
            // lblPS4JNContIDZero
            // 
            this.lblPS4JNContIDZero.AutoSize = true;
            this.lblPS4JNContIDZero.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPS4JNContIDZero.Location = new System.Drawing.Point(247, 64);
            this.lblPS4JNContIDZero.Name = "lblPS4JNContIDZero";
            this.lblPS4JNContIDZero.Size = new System.Drawing.Size(28, 15);
            this.lblPS4JNContIDZero.TabIndex = 145;
            this.lblPS4JNContIDZero.Text = "_00";
            // 
            // txtPS4JPNContID_Third
            // 
            this.txtPS4JPNContID_Third.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPS4JPNContID_Third.Location = new System.Drawing.Point(301, 61);
            this.txtPS4JPNContID_Third.MaxLength = 16;
            this.txtPS4JPNContID_Third.Name = "txtPS4JPNContID_Third";
            this.txtPS4JPNContID_Third.Size = new System.Drawing.Size(126, 21);
            this.txtPS4JPNContID_Third.TabIndex = 120;
            this.txtPS4JPNContID_Third.TextChanged += new System.EventHandler(this.setSolutionDirty);
            // 
            // lblPS4JPNContIDHyp
            // 
            this.lblPS4JPNContIDHyp.AutoSize = true;
            this.lblPS4JPNContIDHyp.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPS4JPNContIDHyp.Location = new System.Drawing.Point(281, 64);
            this.lblPS4JPNContIDHyp.Name = "lblPS4JPNContIDHyp";
            this.lblPS4JPNContIDHyp.Size = new System.Drawing.Size(14, 15);
            this.lblPS4JPNContIDHyp.TabIndex = 144;
            this.lblPS4JPNContIDHyp.Text = "-";
            // 
            // txtPS4JPNContID_Second
            // 
            this.txtPS4JPNContID_Second.BackColor = System.Drawing.Color.Gold;
            this.txtPS4JPNContID_Second.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPS4JPNContID_Second.Location = new System.Drawing.Point(169, 61);
            this.txtPS4JPNContID_Second.MaxLength = 9;
            this.txtPS4JPNContID_Second.Name = "txtPS4JPNContID_Second";
            this.txtPS4JPNContID_Second.ReadOnly = true;
            this.txtPS4JPNContID_Second.Size = new System.Drawing.Size(72, 21);
            this.txtPS4JPNContID_Second.TabIndex = 119;
            this.txtPS4JPNContID_Second.TextChanged += new System.EventHandler(this.setSolutionDirty);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(149, 64);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(14, 15);
            this.label11.TabIndex = 143;
            this.label11.Text = "-";
            // 
            // txtPS4JPNContID_First
            // 
            this.txtPS4JPNContID_First.BackColor = System.Drawing.Color.Gold;
            this.txtPS4JPNContID_First.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPS4JPNContID_First.Location = new System.Drawing.Point(91, 61);
            this.txtPS4JPNContID_First.MaxLength = 6;
            this.txtPS4JPNContID_First.Name = "txtPS4JPNContID_First";
            this.txtPS4JPNContID_First.ReadOnly = true;
            this.txtPS4JPNContID_First.Size = new System.Drawing.Size(52, 21);
            this.txtPS4JPNContID_First.TabIndex = 118;
            this.txtPS4JPNContID_First.TextChanged += new System.EventHandler(this.setSolutionDirty);
            // 
            // lblPS4EUContIDZero
            // 
            this.lblPS4EUContIDZero.AutoSize = true;
            this.lblPS4EUContIDZero.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPS4EUContIDZero.Location = new System.Drawing.Point(247, 37);
            this.lblPS4EUContIDZero.Name = "lblPS4EUContIDZero";
            this.lblPS4EUContIDZero.Size = new System.Drawing.Size(28, 15);
            this.lblPS4EUContIDZero.TabIndex = 142;
            this.lblPS4EUContIDZero.Text = "_00";
            // 
            // txtPS4EUContID_Third
            // 
            this.txtPS4EUContID_Third.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPS4EUContID_Third.Location = new System.Drawing.Point(301, 34);
            this.txtPS4EUContID_Third.MaxLength = 16;
            this.txtPS4EUContID_Third.Name = "txtPS4EUContID_Third";
            this.txtPS4EUContID_Third.Size = new System.Drawing.Size(126, 21);
            this.txtPS4EUContID_Third.TabIndex = 114;
            this.txtPS4EUContID_Third.TextChanged += new System.EventHandler(this.setSolutionDirty);
            // 
            // lblPS4EUContIDHyp
            // 
            this.lblPS4EUContIDHyp.AutoSize = true;
            this.lblPS4EUContIDHyp.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPS4EUContIDHyp.Location = new System.Drawing.Point(281, 37);
            this.lblPS4EUContIDHyp.Name = "lblPS4EUContIDHyp";
            this.lblPS4EUContIDHyp.Size = new System.Drawing.Size(14, 15);
            this.lblPS4EUContIDHyp.TabIndex = 141;
            this.lblPS4EUContIDHyp.Text = "-";
            // 
            // txtPS4EUContID_Second
            // 
            this.txtPS4EUContID_Second.BackColor = System.Drawing.Color.Gold;
            this.txtPS4EUContID_Second.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPS4EUContID_Second.Location = new System.Drawing.Point(169, 34);
            this.txtPS4EUContID_Second.MaxLength = 9;
            this.txtPS4EUContID_Second.Name = "txtPS4EUContID_Second";
            this.txtPS4EUContID_Second.ReadOnly = true;
            this.txtPS4EUContID_Second.Size = new System.Drawing.Size(72, 21);
            this.txtPS4EUContID_Second.TabIndex = 113;
            this.txtPS4EUContID_Second.TextChanged += new System.EventHandler(this.setSolutionDirty);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(149, 37);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(14, 15);
            this.label14.TabIndex = 140;
            this.label14.Text = "-";
            // 
            // txtPS4EUContID_First
            // 
            this.txtPS4EUContID_First.BackColor = System.Drawing.Color.Gold;
            this.txtPS4EUContID_First.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPS4EUContID_First.Location = new System.Drawing.Point(91, 34);
            this.txtPS4EUContID_First.MaxLength = 6;
            this.txtPS4EUContID_First.Name = "txtPS4EUContID_First";
            this.txtPS4EUContID_First.ReadOnly = true;
            this.txtPS4EUContID_First.Size = new System.Drawing.Size(52, 21);
            this.txtPS4EUContID_First.TabIndex = 112;
            this.txtPS4EUContID_First.TextChanged += new System.EventHandler(this.setSolutionDirty);
            // 
            // lblPS4USContIDZero
            // 
            this.lblPS4USContIDZero.AutoSize = true;
            this.lblPS4USContIDZero.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPS4USContIDZero.Location = new System.Drawing.Point(247, 10);
            this.lblPS4USContIDZero.Name = "lblPS4USContIDZero";
            this.lblPS4USContIDZero.Size = new System.Drawing.Size(28, 15);
            this.lblPS4USContIDZero.TabIndex = 139;
            this.lblPS4USContIDZero.Text = "_00";
            // 
            // txtPS4USContID_Third
            // 
            this.txtPS4USContID_Third.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPS4USContID_Third.Location = new System.Drawing.Point(301, 7);
            this.txtPS4USContID_Third.MaxLength = 16;
            this.txtPS4USContID_Third.Name = "txtPS4USContID_Third";
            this.txtPS4USContID_Third.Size = new System.Drawing.Size(126, 21);
            this.txtPS4USContID_Third.TabIndex = 108;
            this.txtPS4USContID_Third.TextChanged += new System.EventHandler(this.setSolutionDirty);
            // 
            // lblPS4USContIDHyp
            // 
            this.lblPS4USContIDHyp.AutoSize = true;
            this.lblPS4USContIDHyp.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPS4USContIDHyp.Location = new System.Drawing.Point(281, 10);
            this.lblPS4USContIDHyp.Name = "lblPS4USContIDHyp";
            this.lblPS4USContIDHyp.Size = new System.Drawing.Size(14, 15);
            this.lblPS4USContIDHyp.TabIndex = 138;
            this.lblPS4USContIDHyp.Text = "-";
            // 
            // txtPS4USContID_Second
            // 
            this.txtPS4USContID_Second.BackColor = System.Drawing.Color.Gold;
            this.txtPS4USContID_Second.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPS4USContID_Second.Location = new System.Drawing.Point(169, 7);
            this.txtPS4USContID_Second.MaxLength = 9;
            this.txtPS4USContID_Second.Name = "txtPS4USContID_Second";
            this.txtPS4USContID_Second.ReadOnly = true;
            this.txtPS4USContID_Second.Size = new System.Drawing.Size(72, 21);
            this.txtPS4USContID_Second.TabIndex = 107;
            this.txtPS4USContID_Second.TextChanged += new System.EventHandler(this.setSolutionDirty);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(149, 10);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(14, 15);
            this.label17.TabIndex = 137;
            this.label17.Text = "-";
            // 
            // txtPS4USContID_First
            // 
            this.txtPS4USContID_First.BackColor = System.Drawing.Color.Gold;
            this.txtPS4USContID_First.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPS4USContID_First.Location = new System.Drawing.Point(91, 6);
            this.txtPS4USContID_First.MaxLength = 6;
            this.txtPS4USContID_First.Name = "txtPS4USContID_First";
            this.txtPS4USContID_First.ReadOnly = true;
            this.txtPS4USContID_First.Size = new System.Drawing.Size(52, 21);
            this.txtPS4USContID_First.TabIndex = 105;
            this.txtPS4USContID_First.TextChanged += new System.EventHandler(this.setSolutionDirty);
            // 
            // lblPS4JPNContID
            // 
            this.lblPS4JPNContID.AutoSize = true;
            this.lblPS4JPNContID.Location = new System.Drawing.Point(8, 65);
            this.lblPS4JPNContID.Name = "lblPS4JPNContID";
            this.lblPS4JPNContID.Size = new System.Drawing.Size(77, 13);
            this.lblPS4JPNContID.TabIndex = 136;
            this.lblPS4JPNContID.Text = "JN Content ID:";
            // 
            // lblPS4EUContID
            // 
            this.lblPS4EUContID.AutoSize = true;
            this.lblPS4EUContID.Location = new System.Drawing.Point(6, 39);
            this.lblPS4EUContID.Name = "lblPS4EUContID";
            this.lblPS4EUContID.Size = new System.Drawing.Size(79, 13);
            this.lblPS4EUContID.TabIndex = 135;
            this.lblPS4EUContID.Text = "EU Content ID:";
            // 
            // btnPS4USParamSfo
            // 
            this.btnPS4USParamSfo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPS4USParamSfo.Location = new System.Drawing.Point(868, 6);
            this.btnPS4USParamSfo.Name = "btnPS4USParamSfo";
            this.btnPS4USParamSfo.Size = new System.Drawing.Size(31, 23);
            this.btnPS4USParamSfo.TabIndex = 111;
            this.btnPS4USParamSfo.Text = "...";
            this.ttGeneric.SetToolTip(this.btnPS4USParamSfo, "Browse for files, use the \'Delete\' key to clear this field.");
            this.btnPS4USParamSfo.UseVisualStyleBackColor = true;
            this.btnPS4USParamSfo.Click += new System.EventHandler(this.btnBrowsePS4USParamSfo_Click);
            // 
            // txtPS4Icon0Browse
            // 
            this.txtPS4Icon0Browse.Location = new System.Drawing.Point(433, 86);
            this.txtPS4Icon0Browse.Name = "txtPS4Icon0Browse";
            this.txtPS4Icon0Browse.Size = new System.Drawing.Size(31, 23);
            this.txtPS4Icon0Browse.TabIndex = 128;
            this.txtPS4Icon0Browse.Text = "...";
            this.ttGeneric.SetToolTip(this.txtPS4Icon0Browse, "Browse for files, use the \'Delete\' key to clear this field.");
            this.txtPS4Icon0Browse.UseVisualStyleBackColor = true;
            this.txtPS4Icon0Browse.Click += new System.EventHandler(this.btnBrowsePS4Icon0_Click);
            // 
            // txtPS4Icon0
            // 
            this.txtPS4Icon0.Location = new System.Drawing.Point(91, 88);
            this.txtPS4Icon0.Name = "txtPS4Icon0";
            this.txtPS4Icon0.ReadOnly = true;
            this.txtPS4Icon0.Size = new System.Drawing.Size(336, 20);
            this.txtPS4Icon0.TabIndex = 127;
            this.txtPS4Icon0.TabStop = false;
            this.ttGeneric.SetToolTip(this.txtPS4Icon0, "To clear field, highlight and press \'Delete\'");
            this.txtPS4Icon0.TextChanged += new System.EventHandler(this.txtPS4Icon0_TextChanged);
            this.txtPS4Icon0.KeyUp += new System.Windows.Forms.KeyEventHandler(this.clearTxtField);
            // 
            // lblPS4Icon
            // 
            this.lblPS4Icon.AutoSize = true;
            this.lblPS4Icon.Location = new System.Drawing.Point(48, 91);
            this.lblPS4Icon.Name = "lblPS4Icon";
            this.lblPS4Icon.Size = new System.Drawing.Size(37, 13);
            this.lblPS4Icon.TabIndex = 134;
            this.lblPS4Icon.Text = "Icon0:";
            // 
            // txtPS4USParamSfo
            // 
            this.txtPS4USParamSfo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPS4USParamSfo.Location = new System.Drawing.Point(605, 8);
            this.txtPS4USParamSfo.Name = "txtPS4USParamSfo";
            this.txtPS4USParamSfo.ReadOnly = true;
            this.txtPS4USParamSfo.Size = new System.Drawing.Size(257, 20);
            this.txtPS4USParamSfo.TabIndex = 110;
            this.txtPS4USParamSfo.TabStop = false;
            this.ttGeneric.SetToolTip(this.txtPS4USParamSfo, "To clear field, highlight and press \'Delete\'");
            this.txtPS4USParamSfo.TextChanged += new System.EventHandler(this.setSolutionDirty);
            this.txtPS4USParamSfo.KeyUp += new System.Windows.Forms.KeyEventHandler(this.clearTxtField);
            // 
            // lblPS4USParam
            // 
            this.lblPS4USParam.AutoSize = true;
            this.lblPS4USParam.Location = new System.Drawing.Point(541, 11);
            this.lblPS4USParam.Name = "lblPS4USParam";
            this.lblPS4USParam.Size = new System.Drawing.Size(58, 13);
            this.lblPS4USParam.TabIndex = 133;
            this.lblPS4USParam.Text = "US Param:";
            this.ttGeneric.SetToolTip(this.lblPS4USParam, "Select the relevant SFO file for this region");
            // 
            // pbPS4Icon0
            // 
            this.pbPS4Icon0.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pbPS4Icon0.Location = new System.Drawing.Point(22, 114);
            this.pbPS4Icon0.MaximumSize = new System.Drawing.Size(512, 512);
            this.pbPS4Icon0.Name = "pbPS4Icon0";
            this.pbPS4Icon0.Size = new System.Drawing.Size(512, 512);
            this.pbPS4Icon0.TabIndex = 132;
            this.pbPS4Icon0.TabStop = false;
            this.ttGeneric.SetToolTip(this.pbPS4Icon0, "Icon0");
            // 
            // txtPS4Code
            // 
            this.txtPS4Code.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPS4Code.Location = new System.Drawing.Point(606, 88);
            this.txtPS4Code.MaxLength = 32;
            this.txtPS4Code.Name = "txtPS4Code";
            this.txtPS4Code.Size = new System.Drawing.Size(256, 20);
            this.txtPS4Code.TabIndex = 125;
            this.txtPS4Code.TextChanged += new System.EventHandler(this.setSolutionDirty);
            // 
            // lblPS4Passcode
            // 
            this.lblPS4Passcode.AutoSize = true;
            this.lblPS4Passcode.Location = new System.Drawing.Point(543, 91);
            this.lblPS4Passcode.Name = "lblPS4Passcode";
            this.lblPS4Passcode.Size = new System.Drawing.Size(57, 13);
            this.lblPS4Passcode.TabIndex = 131;
            this.lblPS4Passcode.Text = "Passcode:";
            // 
            // txtPS4Entitlement
            // 
            this.txtPS4Entitlement.BackColor = System.Drawing.Color.Gold;
            this.txtPS4Entitlement.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPS4Entitlement.Location = new System.Drawing.Point(605, 114);
            this.txtPS4Entitlement.MaxLength = 32;
            this.txtPS4Entitlement.Name = "txtPS4Entitlement";
            this.txtPS4Entitlement.ReadOnly = true;
            this.txtPS4Entitlement.Size = new System.Drawing.Size(256, 21);
            this.txtPS4Entitlement.TabIndex = 126;
            this.txtPS4Entitlement.TextChanged += new System.EventHandler(this.setSolutionDirty);
            // 
            // lblPS4Entitlement
            // 
            this.lblPS4Entitlement.AutoSize = true;
            this.lblPS4Entitlement.Location = new System.Drawing.Point(538, 118);
            this.lblPS4Entitlement.Name = "lblPS4Entitlement";
            this.lblPS4Entitlement.Size = new System.Drawing.Size(62, 13);
            this.lblPS4Entitlement.TabIndex = 129;
            this.lblPS4Entitlement.Text = "Entitlement:";
            // 
            // lblPS4USContID
            // 
            this.lblPS4USContID.AutoSize = true;
            this.lblPS4USContID.Location = new System.Drawing.Point(6, 11);
            this.lblPS4USContID.Name = "lblPS4USContID";
            this.lblPS4USContID.Size = new System.Drawing.Size(79, 13);
            this.lblPS4USContID.TabIndex = 106;
            this.lblPS4USContID.Text = "US Content ID:";
            // 
            // tbXboxOne
            // 
            this.tbXboxOne.Controls.Add(this.grpXB1AppManifest);
            this.tbXboxOne.Location = new System.Drawing.Point(4, 22);
            this.tbXboxOne.Name = "tbXboxOne";
            this.tbXboxOne.Padding = new System.Windows.Forms.Padding(3);
            this.tbXboxOne.Size = new System.Drawing.Size(910, 447);
            this.tbXboxOne.TabIndex = 6;
            this.tbXboxOne.Text = "XBOX 1";
            this.tbXboxOne.UseVisualStyleBackColor = true;
            // 
            // grpXB1AppManifest
            // 
            this.grpXB1AppManifest.Controls.Add(this.grpXB1ProductID);
            this.grpXB1AppManifest.Controls.Add(this.pbXB1WideLogo);
            this.grpXB1AppManifest.Controls.Add(this.pbXB1SmallLogo);
            this.grpXB1AppManifest.Controls.Add(this.pbXB1LiveLogo);
            this.grpXB1AppManifest.Controls.Add(this.pbXB1StoreLogo);
            this.grpXB1AppManifest.Controls.Add(this.label3);
            this.grpXB1AppManifest.Controls.Add(this.label4);
            this.grpXB1AppManifest.Controls.Add(this.label2);
            this.grpXB1AppManifest.Controls.Add(this.grpVisualElements);
            this.grpXB1AppManifest.Controls.Add(this.label1);
            this.grpXB1AppManifest.Controls.Add(this.grpXB1Properties);
            this.grpXB1AppManifest.Controls.Add(this.grpChunks);
            this.grpXB1AppManifest.Controls.Add(this.grpXB1Identity);
            this.grpXB1AppManifest.Location = new System.Drawing.Point(4, 6);
            this.grpXB1AppManifest.Name = "grpXB1AppManifest";
            this.grpXB1AppManifest.Size = new System.Drawing.Size(882, 438);
            this.grpXB1AppManifest.TabIndex = 1;
            this.grpXB1AppManifest.TabStop = false;
            this.grpXB1AppManifest.Text = "AppManifest";
            // 
            // grpXB1ProductID
            // 
            this.grpXB1ProductID.Controls.Add(this.label10);
            this.grpXB1ProductID.Controls.Add(this.label9);
            this.grpXB1ProductID.Controls.Add(this.txtXB1AllowedProductIDJPN);
            this.grpXB1ProductID.Controls.Add(this.rbXB1Release);
            this.grpXB1ProductID.Controls.Add(this.rbXB1Dev);
            this.grpXB1ProductID.Controls.Add(this.txtXB1AllowedProductID);
            this.grpXB1ProductID.Location = new System.Drawing.Point(9, 179);
            this.grpXB1ProductID.Name = "grpXB1ProductID";
            this.grpXB1ProductID.Size = new System.Drawing.Size(374, 89);
            this.grpXB1ProductID.TabIndex = 74;
            this.grpXB1ProductID.TabStop = false;
            this.grpXB1ProductID.Text = "Product ID";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(12, 53);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(27, 13);
            this.label10.TabIndex = 5;
            this.label10.Text = "JPN";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(12, 29);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(29, 13);
            this.label9.TabIndex = 4;
            this.label9.Text = "WW";
            // 
            // txtXB1AllowedProductIDJPN
            // 
            this.txtXB1AllowedProductIDJPN.Location = new System.Drawing.Point(62, 50);
            this.txtXB1AllowedProductIDJPN.Name = "txtXB1AllowedProductIDJPN";
            this.txtXB1AllowedProductIDJPN.Size = new System.Drawing.Size(213, 20);
            this.txtXB1AllowedProductIDJPN.TabIndex = 3;
            // 
            // rbXB1Release
            // 
            this.rbXB1Release.AutoSize = true;
            this.rbXB1Release.Location = new System.Drawing.Point(304, 50);
            this.rbXB1Release.Name = "rbXB1Release";
            this.rbXB1Release.Size = new System.Drawing.Size(64, 17);
            this.rbXB1Release.TabIndex = 2;
            this.rbXB1Release.TabStop = true;
            this.rbXB1Release.Text = "Release";
            this.rbXB1Release.UseVisualStyleBackColor = true;
            // 
            // rbXB1Dev
            // 
            this.rbXB1Dev.AutoSize = true;
            this.rbXB1Dev.Checked = true;
            this.rbXB1Dev.Location = new System.Drawing.Point(304, 27);
            this.rbXB1Dev.Name = "rbXB1Dev";
            this.rbXB1Dev.Size = new System.Drawing.Size(45, 17);
            this.rbXB1Dev.TabIndex = 1;
            this.rbXB1Dev.TabStop = true;
            this.rbXB1Dev.Text = "Dev";
            this.rbXB1Dev.UseVisualStyleBackColor = true;
            // 
            // txtXB1AllowedProductID
            // 
            this.txtXB1AllowedProductID.Location = new System.Drawing.Point(62, 26);
            this.txtXB1AllowedProductID.Name = "txtXB1AllowedProductID";
            this.txtXB1AllowedProductID.Size = new System.Drawing.Size(213, 20);
            this.txtXB1AllowedProductID.TabIndex = 0;
            // 
            // pbXB1WideLogo
            // 
            this.pbXB1WideLogo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbXB1WideLogo.Location = new System.Drawing.Point(401, 187);
            this.pbXB1WideLogo.Name = "pbXB1WideLogo";
            this.pbXB1WideLogo.Size = new System.Drawing.Size(373, 100);
            this.pbXB1WideLogo.TabIndex = 73;
            this.pbXB1WideLogo.TabStop = false;
            // 
            // pbXB1SmallLogo
            // 
            this.pbXB1SmallLogo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbXB1SmallLogo.Location = new System.Drawing.Point(674, 49);
            this.pbXB1SmallLogo.Name = "pbXB1SmallLogo";
            this.pbXB1SmallLogo.Size = new System.Drawing.Size(100, 100);
            this.pbXB1SmallLogo.TabIndex = 72;
            this.pbXB1SmallLogo.TabStop = false;
            // 
            // pbXB1LiveLogo
            // 
            this.pbXB1LiveLogo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbXB1LiveLogo.Location = new System.Drawing.Point(536, 49);
            this.pbXB1LiveLogo.Name = "pbXB1LiveLogo";
            this.pbXB1LiveLogo.Size = new System.Drawing.Size(100, 100);
            this.pbXB1LiveLogo.TabIndex = 71;
            this.pbXB1LiveLogo.TabStop = false;
            // 
            // pbXB1StoreLogo
            // 
            this.pbXB1StoreLogo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbXB1StoreLogo.Location = new System.Drawing.Point(401, 49);
            this.pbXB1StoreLogo.Name = "pbXB1StoreLogo";
            this.pbXB1StoreLogo.Size = new System.Drawing.Size(100, 100);
            this.pbXB1StoreLogo.TabIndex = 70;
            this.pbXB1StoreLogo.TabStop = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(398, 171);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 13);
            this.label3.TabIndex = 69;
            this.label3.Text = "Wide Logo";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(533, 32);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(54, 13);
            this.label4.TabIndex = 68;
            this.label4.Text = "Live Logo";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(671, 32);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 13);
            this.label2.TabIndex = 67;
            this.label2.Text = "Small Logo";
            // 
            // grpVisualElements
            // 
            this.grpVisualElements.Controls.Add(this.label8);
            this.grpVisualElements.Controls.Add(this.txtXB1VWideLogo);
            this.grpVisualElements.Controls.Add(this.btnXB1WideLogo);
            this.grpVisualElements.Controls.Add(this.label6);
            this.grpVisualElements.Controls.Add(this.txtXB1VSmallLogo);
            this.grpVisualElements.Controls.Add(this.btnXB1SmallLogo);
            this.grpVisualElements.Controls.Add(this.label5);
            this.grpVisualElements.Controls.Add(this.txtXB1VLiveLogo);
            this.grpVisualElements.Controls.Add(this.btnXB1LiveLogo);
            this.grpVisualElements.Controls.Add(this.txtXB1VDisplayName);
            this.grpVisualElements.Controls.Add(this.lbXB1VDisplayName);
            this.grpVisualElements.Controls.Add(this.lbXB1VDesc);
            this.grpVisualElements.Controls.Add(this.txtXB1VDescription);
            this.grpVisualElements.Location = new System.Drawing.Point(9, 274);
            this.grpVisualElements.Name = "grpVisualElements";
            this.grpVisualElements.Size = new System.Drawing.Size(374, 158);
            this.grpVisualElements.TabIndex = 66;
            this.grpVisualElements.TabStop = false;
            this.grpVisualElements.Text = "Visual Elements";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(7, 131);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(59, 13);
            this.label8.TabIndex = 74;
            this.label8.Text = "Wide Logo";
            // 
            // txtXB1VWideLogo
            // 
            this.txtXB1VWideLogo.Location = new System.Drawing.Point(72, 128);
            this.txtXB1VWideLogo.Name = "txtXB1VWideLogo";
            this.txtXB1VWideLogo.ReadOnly = true;
            this.txtXB1VWideLogo.Size = new System.Drawing.Size(235, 20);
            this.txtXB1VWideLogo.TabIndex = 72;
            this.txtXB1VWideLogo.TabStop = false;
            this.ttGeneric.SetToolTip(this.txtXB1VWideLogo, "To clear field, highlight and press \'Delete\'");
            this.txtXB1VWideLogo.TextChanged += new System.EventHandler(this.txtXB1WideLogo_TextChanged);
            // 
            // btnXB1WideLogo
            // 
            this.btnXB1WideLogo.Location = new System.Drawing.Point(313, 126);
            this.btnXB1WideLogo.Name = "btnXB1WideLogo";
            this.btnXB1WideLogo.Size = new System.Drawing.Size(31, 23);
            this.btnXB1WideLogo.TabIndex = 73;
            this.btnXB1WideLogo.Text = "...";
            this.ttGeneric.SetToolTip(this.btnXB1WideLogo, "Browse for files, use the \'Delete\' key to clear this field.");
            this.btnXB1WideLogo.UseVisualStyleBackColor = true;
            this.btnXB1WideLogo.Click += new System.EventHandler(this.btnBrowseXboxOneWideImg_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(7, 105);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(59, 13);
            this.label6.TabIndex = 71;
            this.label6.Text = "Small Logo";
            // 
            // txtXB1VSmallLogo
            // 
            this.txtXB1VSmallLogo.Location = new System.Drawing.Point(72, 102);
            this.txtXB1VSmallLogo.Name = "txtXB1VSmallLogo";
            this.txtXB1VSmallLogo.ReadOnly = true;
            this.txtXB1VSmallLogo.Size = new System.Drawing.Size(235, 20);
            this.txtXB1VSmallLogo.TabIndex = 69;
            this.txtXB1VSmallLogo.TabStop = false;
            this.ttGeneric.SetToolTip(this.txtXB1VSmallLogo, "To clear field, highlight and press \'Delete\'");
            this.txtXB1VSmallLogo.TextChanged += new System.EventHandler(this.txtXB1SmallLogo_TextChanged);
            // 
            // btnXB1SmallLogo
            // 
            this.btnXB1SmallLogo.Location = new System.Drawing.Point(313, 100);
            this.btnXB1SmallLogo.Name = "btnXB1SmallLogo";
            this.btnXB1SmallLogo.Size = new System.Drawing.Size(31, 23);
            this.btnXB1SmallLogo.TabIndex = 70;
            this.btnXB1SmallLogo.Text = "...";
            this.ttGeneric.SetToolTip(this.btnXB1SmallLogo, "Browse for files, use the \'Delete\' key to clear this field.");
            this.btnXB1SmallLogo.UseVisualStyleBackColor = true;
            this.btnXB1SmallLogo.Click += new System.EventHandler(this.btnBrowseXboxOneSmallImg_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(7, 79);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(54, 13);
            this.label5.TabIndex = 68;
            this.label5.Text = "Live Logo";
            // 
            // txtXB1VLiveLogo
            // 
            this.txtXB1VLiveLogo.Location = new System.Drawing.Point(72, 76);
            this.txtXB1VLiveLogo.Name = "txtXB1VLiveLogo";
            this.txtXB1VLiveLogo.ReadOnly = true;
            this.txtXB1VLiveLogo.Size = new System.Drawing.Size(235, 20);
            this.txtXB1VLiveLogo.TabIndex = 66;
            this.txtXB1VLiveLogo.TabStop = false;
            this.ttGeneric.SetToolTip(this.txtXB1VLiveLogo, "To clear field, highlight and press \'Delete\'");
            this.txtXB1VLiveLogo.TextChanged += new System.EventHandler(this.txtXB1LiveLogo_TextChanged);
            // 
            // btnXB1LiveLogo
            // 
            this.btnXB1LiveLogo.Location = new System.Drawing.Point(313, 74);
            this.btnXB1LiveLogo.Name = "btnXB1LiveLogo";
            this.btnXB1LiveLogo.Size = new System.Drawing.Size(31, 23);
            this.btnXB1LiveLogo.TabIndex = 67;
            this.btnXB1LiveLogo.Text = "...";
            this.ttGeneric.SetToolTip(this.btnXB1LiveLogo, "Browse for files, use the \'Delete\' key to clear this field.");
            this.btnXB1LiveLogo.UseVisualStyleBackColor = true;
            this.btnXB1LiveLogo.Click += new System.EventHandler(this.btnBrowseXboxOneLiveImg_Click);
            // 
            // txtXB1VDisplayName
            // 
            this.txtXB1VDisplayName.Location = new System.Drawing.Point(84, 50);
            this.txtXB1VDisplayName.Name = "txtXB1VDisplayName";
            this.txtXB1VDisplayName.Size = new System.Drawing.Size(260, 20);
            this.txtXB1VDisplayName.TabIndex = 19;
            // 
            // lbXB1VDisplayName
            // 
            this.lbXB1VDisplayName.AutoSize = true;
            this.lbXB1VDisplayName.Location = new System.Drawing.Point(6, 53);
            this.lbXB1VDisplayName.Name = "lbXB1VDisplayName";
            this.lbXB1VDisplayName.Size = new System.Drawing.Size(72, 13);
            this.lbXB1VDisplayName.TabIndex = 18;
            this.lbXB1VDisplayName.Text = "Display Name";
            // 
            // lbXB1VDesc
            // 
            this.lbXB1VDesc.AutoSize = true;
            this.lbXB1VDesc.Location = new System.Drawing.Point(6, 22);
            this.lbXB1VDesc.Name = "lbXB1VDesc";
            this.lbXB1VDesc.Size = new System.Drawing.Size(60, 13);
            this.lbXB1VDesc.TabIndex = 17;
            this.lbXB1VDesc.Text = "Description";
            // 
            // txtXB1VDescription
            // 
            this.txtXB1VDescription.Location = new System.Drawing.Point(72, 19);
            this.txtXB1VDescription.Name = "txtXB1VDescription";
            this.txtXB1VDescription.Size = new System.Drawing.Size(272, 20);
            this.txtXB1VDescription.TabIndex = 17;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(398, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 13);
            this.label1.TabIndex = 65;
            this.label1.Text = "Store Logo";
            // 
            // grpXB1Properties
            // 
            this.grpXB1Properties.Controls.Add(this.lbXB1StoreLogo);
            this.grpXB1Properties.Controls.Add(this.txtXB1StoreLogo);
            this.grpXB1Properties.Controls.Add(this.btnXB1StoreLogo);
            this.grpXB1Properties.Controls.Add(this.txtXB1PDescription);
            this.grpXB1Properties.Controls.Add(this.lbXB1PDescription);
            this.grpXB1Properties.Controls.Add(this.txtXB1PPublisherName);
            this.grpXB1Properties.Controls.Add(this.lbXB1PublisherDname);
            this.grpXB1Properties.Controls.Add(this.txtXB1PDisplayName);
            this.grpXB1Properties.Controls.Add(this.lbXB1PDisplayName);
            this.grpXB1Properties.Location = new System.Drawing.Point(9, 68);
            this.grpXB1Properties.Name = "grpXB1Properties";
            this.grpXB1Properties.Size = new System.Drawing.Size(374, 108);
            this.grpXB1Properties.TabIndex = 0;
            this.grpXB1Properties.TabStop = false;
            this.grpXB1Properties.Text = "Properties";
            this.grpXB1Properties.Enter += new System.EventHandler(this.grpXB1Properties_Enter);
            // 
            // lbXB1StoreLogo
            // 
            this.lbXB1StoreLogo.AutoSize = true;
            this.lbXB1StoreLogo.Location = new System.Drawing.Point(7, 87);
            this.lbXB1StoreLogo.Name = "lbXB1StoreLogo";
            this.lbXB1StoreLogo.Size = new System.Drawing.Size(59, 13);
            this.lbXB1StoreLogo.TabIndex = 65;
            this.lbXB1StoreLogo.Text = "Store Logo";
            // 
            // txtXB1StoreLogo
            // 
            this.txtXB1StoreLogo.Location = new System.Drawing.Point(72, 84);
            this.txtXB1StoreLogo.Name = "txtXB1StoreLogo";
            this.txtXB1StoreLogo.ReadOnly = true;
            this.txtXB1StoreLogo.Size = new System.Drawing.Size(235, 20);
            this.txtXB1StoreLogo.TabIndex = 63;
            this.txtXB1StoreLogo.TabStop = false;
            this.ttGeneric.SetToolTip(this.txtXB1StoreLogo, "To clear field, highlight and press \'Delete\'");
            this.txtXB1StoreLogo.TextChanged += new System.EventHandler(this.txtXB1StoreLogo_TextChanged);
            // 
            // btnXB1StoreLogo
            // 
            this.btnXB1StoreLogo.Location = new System.Drawing.Point(313, 82);
            this.btnXB1StoreLogo.Name = "btnXB1StoreLogo";
            this.btnXB1StoreLogo.Size = new System.Drawing.Size(31, 23);
            this.btnXB1StoreLogo.TabIndex = 64;
            this.btnXB1StoreLogo.Text = "...";
            this.ttGeneric.SetToolTip(this.btnXB1StoreLogo, "Browse for files, use the \'Delete\' key to clear this field.");
            this.btnXB1StoreLogo.UseVisualStyleBackColor = true;
            this.btnXB1StoreLogo.Click += new System.EventHandler(this.btnBrowseXboxOneStoreImg_Click);
            // 
            // txtXB1PDescription
            // 
            this.txtXB1PDescription.Location = new System.Drawing.Point(72, 58);
            this.txtXB1PDescription.Name = "txtXB1PDescription";
            this.txtXB1PDescription.Size = new System.Drawing.Size(272, 20);
            this.txtXB1PDescription.TabIndex = 16;
            // 
            // lbXB1PDescription
            // 
            this.lbXB1PDescription.AutoSize = true;
            this.lbXB1PDescription.Location = new System.Drawing.Point(6, 61);
            this.lbXB1PDescription.Name = "lbXB1PDescription";
            this.lbXB1PDescription.Size = new System.Drawing.Size(60, 13);
            this.lbXB1PDescription.TabIndex = 15;
            this.lbXB1PDescription.Text = "Description";
            // 
            // txtXB1PPublisherName
            // 
            this.txtXB1PPublisherName.Location = new System.Drawing.Point(181, 32);
            this.txtXB1PPublisherName.Name = "txtXB1PPublisherName";
            this.txtXB1PPublisherName.Size = new System.Drawing.Size(163, 20);
            this.txtXB1PPublisherName.TabIndex = 14;
            // 
            // lbXB1PublisherDname
            // 
            this.lbXB1PublisherDname.AutoSize = true;
            this.lbXB1PublisherDname.Location = new System.Drawing.Point(178, 16);
            this.lbXB1PublisherDname.Name = "lbXB1PublisherDname";
            this.lbXB1PublisherDname.Size = new System.Drawing.Size(118, 13);
            this.lbXB1PublisherDname.TabIndex = 13;
            this.lbXB1PublisherDname.Text = "Publisher Display Name";
            // 
            // txtXB1PDisplayName
            // 
            this.txtXB1PDisplayName.Location = new System.Drawing.Point(9, 32);
            this.txtXB1PDisplayName.Name = "txtXB1PDisplayName";
            this.txtXB1PDisplayName.Size = new System.Drawing.Size(163, 20);
            this.txtXB1PDisplayName.TabIndex = 12;
            // 
            // lbXB1PDisplayName
            // 
            this.lbXB1PDisplayName.AutoSize = true;
            this.lbXB1PDisplayName.Location = new System.Drawing.Point(6, 16);
            this.lbXB1PDisplayName.Name = "lbXB1PDisplayName";
            this.lbXB1PDisplayName.Size = new System.Drawing.Size(72, 13);
            this.lbXB1PDisplayName.TabIndex = 10;
            this.lbXB1PDisplayName.Text = "Display Name";
            // 
            // grpChunks
            // 
            this.grpChunks.Controls.Add(this.btnXB1SaveChunksFile);
            this.grpChunks.Controls.Add(this.btXB1EditChunkFile);
            this.grpChunks.Controls.Add(this.btnXB1LoadChunkFile);
            this.grpChunks.Controls.Add(this.btnXB1GenerateChunk);
            this.grpChunks.Location = new System.Drawing.Point(401, 359);
            this.grpChunks.Name = "grpChunks";
            this.grpChunks.Size = new System.Drawing.Size(374, 73);
            this.grpChunks.TabIndex = 0;
            this.grpChunks.TabStop = false;
            this.grpChunks.Text = "Chunks";
            // 
            // btnXB1SaveChunksFile
            // 
            this.btnXB1SaveChunksFile.Location = new System.Drawing.Point(273, 44);
            this.btnXB1SaveChunksFile.Name = "btnXB1SaveChunksFile";
            this.btnXB1SaveChunksFile.Size = new System.Drawing.Size(75, 23);
            this.btnXB1SaveChunksFile.TabIndex = 9;
            this.btnXB1SaveChunksFile.Text = "Save";
            this.btnXB1SaveChunksFile.UseVisualStyleBackColor = true;
            this.btnXB1SaveChunksFile.Click += new System.EventHandler(this.btnXB1SaveChunksFile_Click);
            // 
            // btXB1EditChunkFile
            // 
            this.btXB1EditChunkFile.Location = new System.Drawing.Point(192, 17);
            this.btXB1EditChunkFile.Name = "btXB1EditChunkFile";
            this.btXB1EditChunkFile.Size = new System.Drawing.Size(75, 23);
            this.btXB1EditChunkFile.TabIndex = 2;
            this.btXB1EditChunkFile.Text = "Edit";
            this.btXB1EditChunkFile.UseVisualStyleBackColor = true;
            // 
            // btnXB1LoadChunkFile
            // 
            this.btnXB1LoadChunkFile.Location = new System.Drawing.Point(192, 44);
            this.btnXB1LoadChunkFile.Name = "btnXB1LoadChunkFile";
            this.btnXB1LoadChunkFile.Size = new System.Drawing.Size(75, 23);
            this.btnXB1LoadChunkFile.TabIndex = 1;
            this.btnXB1LoadChunkFile.Text = "Load";
            this.btnXB1LoadChunkFile.UseVisualStyleBackColor = true;
            // 
            // btnXB1GenerateChunk
            // 
            this.btnXB1GenerateChunk.Location = new System.Drawing.Point(273, 17);
            this.btnXB1GenerateChunk.Name = "btnXB1GenerateChunk";
            this.btnXB1GenerateChunk.Size = new System.Drawing.Size(75, 23);
            this.btnXB1GenerateChunk.TabIndex = 0;
            this.btnXB1GenerateChunk.Text = "Generate";
            this.btnXB1GenerateChunk.UseVisualStyleBackColor = true;
            this.btnXB1GenerateChunk.Click += new System.EventHandler(this.btnXB1GenerateChunk_Click);
            // 
            // grpXB1Identity
            // 
            this.grpXB1Identity.Controls.Add(this.txtXB1IdentityName);
            this.grpXB1Identity.Controls.Add(this.txtXB1IdentityVersion);
            this.grpXB1Identity.Controls.Add(this.lbXB1IName);
            this.grpXB1Identity.Controls.Add(this.lbXB1IVersion);
            this.grpXB1Identity.Location = new System.Drawing.Point(9, 19);
            this.grpXB1Identity.Name = "grpXB1Identity";
            this.grpXB1Identity.Size = new System.Drawing.Size(374, 43);
            this.grpXB1Identity.TabIndex = 0;
            this.grpXB1Identity.TabStop = false;
            this.grpXB1Identity.Text = "Identity";
            // 
            // txtXB1IdentityName
            // 
            this.txtXB1IdentityName.Location = new System.Drawing.Point(47, 13);
            this.txtXB1IdentityName.Name = "txtXB1IdentityName";
            this.txtXB1IdentityName.Size = new System.Drawing.Size(128, 20);
            this.txtXB1IdentityName.TabIndex = 8;
            // 
            // txtXB1IdentityVersion
            // 
            this.txtXB1IdentityVersion.Location = new System.Drawing.Point(229, 13);
            this.txtXB1IdentityVersion.Name = "txtXB1IdentityVersion";
            this.txtXB1IdentityVersion.Size = new System.Drawing.Size(140, 20);
            this.txtXB1IdentityVersion.TabIndex = 10;
            // 
            // lbXB1IName
            // 
            this.lbXB1IName.AutoSize = true;
            this.lbXB1IName.Location = new System.Drawing.Point(6, 16);
            this.lbXB1IName.Name = "lbXB1IName";
            this.lbXB1IName.Size = new System.Drawing.Size(35, 13);
            this.lbXB1IName.TabIndex = 9;
            this.lbXB1IName.Text = "Name";
            // 
            // lbXB1IVersion
            // 
            this.lbXB1IVersion.AutoSize = true;
            this.lbXB1IVersion.Location = new System.Drawing.Point(181, 16);
            this.lbXB1IVersion.Name = "lbXB1IVersion";
            this.lbXB1IVersion.Size = new System.Drawing.Size(42, 13);
            this.lbXB1IVersion.TabIndex = 11;
            this.lbXB1IVersion.Text = "Version";
            // 
            // toolStripMain
            // 
            this.toolStripMain.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStripMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbBuildAll,
            this.toolStripSeparator1,
            this.tsbBuildXbox,
            this.tsbBuildPs3,
            this.tsbBuildPC,
            this.tsbBuildPS4,
            this.tsbBuildXbone,
            this.tsbBuildShippablePack,
            this.toolStripSeparator4,
            this.tsbBuildCloud,
            this.toolStripSeparator3,
            this.tsbOpenContentFolder,
            this.tsbGotoConfigFiles,
            this.toolStripSeparator2,
            this.tsbBuildSetupAndContent,
            this.tsbBuildXMLs,
            this.toolStripSeparator5,
            this.tsbOptions});
            this.toolStripMain.Location = new System.Drawing.Point(0, 0);
            this.toolStripMain.Name = "toolStripMain";
            this.toolStripMain.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.toolStripMain.Size = new System.Drawing.Size(918, 25);
            this.toolStripMain.TabIndex = 2;
            this.toolStripMain.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.toolStripMain_ItemClicked);
            // 
            // tsbBuildAll
            // 
            this.tsbBuildAll.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbBuildAll.Image = ((System.Drawing.Image)(resources.GetObject("tsbBuildAll.Image")));
            this.tsbBuildAll.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbBuildAll.Name = "tsbBuildAll";
            this.tsbBuildAll.Size = new System.Drawing.Size(23, 22);
            this.tsbBuildAll.Text = "Build All";
            this.tsbBuildAll.Click += new System.EventHandler(this.tsbBuildAll_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // tsbBuildXbox
            // 
            this.tsbBuildXbox.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbBuildXbox.Image = ((System.Drawing.Image)(resources.GetObject("tsbBuildXbox.Image")));
            this.tsbBuildXbox.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbBuildXbox.Name = "tsbBuildXbox";
            this.tsbBuildXbox.Size = new System.Drawing.Size(23, 22);
            this.tsbBuildXbox.Text = "Build XBOX";
            this.tsbBuildXbox.Click += new System.EventHandler(this.tsbBuildXbox_Click);
            // 
            // tsbBuildPs3
            // 
            this.tsbBuildPs3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbBuildPs3.Image = ((System.Drawing.Image)(resources.GetObject("tsbBuildPs3.Image")));
            this.tsbBuildPs3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbBuildPs3.Name = "tsbBuildPs3";
            this.tsbBuildPs3.Size = new System.Drawing.Size(23, 22);
            this.tsbBuildPs3.Text = "Build PS3";
            this.tsbBuildPs3.Click += new System.EventHandler(this.tsbBuildPs3_Click);
            // 
            // tsbBuildPC
            // 
            this.tsbBuildPC.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbBuildPC.Image = ((System.Drawing.Image)(resources.GetObject("tsbBuildPC.Image")));
            this.tsbBuildPC.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbBuildPC.Name = "tsbBuildPC";
            this.tsbBuildPC.Size = new System.Drawing.Size(23, 22);
            this.tsbBuildPC.Text = "Build PC";
            this.tsbBuildPC.Click += new System.EventHandler(this.tsbBuildPC_Click);
            // 
            // tsbBuildPS4
            // 
            this.tsbBuildPS4.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbBuildPS4.Image = ((System.Drawing.Image)(resources.GetObject("tsbBuildPS4.Image")));
            this.tsbBuildPS4.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbBuildPS4.Name = "tsbBuildPS4";
            this.tsbBuildPS4.Size = new System.Drawing.Size(23, 22);
            this.tsbBuildPS4.Text = "Build PS4";
            this.tsbBuildPS4.Click += new System.EventHandler(this.tsbBuildPS4_Click);
            // 
            // tsbBuildXbone
            // 
            this.tsbBuildXbone.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbBuildXbone.Image = ((System.Drawing.Image)(resources.GetObject("tsbBuildXbone.Image")));
            this.tsbBuildXbone.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbBuildXbone.Name = "tsbBuildXbone";
            this.tsbBuildXbone.Size = new System.Drawing.Size(23, 22);
            this.tsbBuildXbone.Text = "Build XBONE";
            this.tsbBuildXbone.Click += new System.EventHandler(this.tsbBuildXbone_Click);
            // 
            // tsbBuildShippablePack
            // 
            this.tsbBuildShippablePack.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbBuildShippablePack.Image = ((System.Drawing.Image)(resources.GetObject("tsbBuildShippablePack.Image")));
            this.tsbBuildShippablePack.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbBuildShippablePack.Name = "tsbBuildShippablePack";
            this.tsbBuildShippablePack.Size = new System.Drawing.Size(23, 22);
            this.tsbBuildShippablePack.Text = "Build Shippable Packs";
            this.tsbBuildShippablePack.Click += new System.EventHandler(this.tsbBuildShippablePack_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 25);
            // 
            // tsbBuildCloud
            // 
            this.tsbBuildCloud.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbBuildCloud.Image = ((System.Drawing.Image)(resources.GetObject("tsbBuildCloud.Image")));
            this.tsbBuildCloud.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbBuildCloud.Name = "tsbBuildCloud";
            this.tsbBuildCloud.Size = new System.Drawing.Size(23, 22);
            this.tsbBuildCloud.Text = "Build Cloud (All platforms)";
            this.tsbBuildCloud.Click += new System.EventHandler(this.tsbBuildCloud_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // tsbOpenContentFolder
            // 
            this.tsbOpenContentFolder.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbOpenContentFolder.Image = ((System.Drawing.Image)(resources.GetObject("tsbOpenContentFolder.Image")));
            this.tsbOpenContentFolder.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbOpenContentFolder.Name = "tsbOpenContentFolder";
            this.tsbOpenContentFolder.Size = new System.Drawing.Size(23, 22);
            this.tsbOpenContentFolder.Text = "Open output directory";
            this.tsbOpenContentFolder.Click += new System.EventHandler(this.tsbOpenContentFolder_Click);
            // 
            // tsbGotoConfigFiles
            // 
            this.tsbGotoConfigFiles.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbGotoConfigFiles.Image = ((System.Drawing.Image)(resources.GetObject("tsbGotoConfigFiles.Image")));
            this.tsbGotoConfigFiles.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbGotoConfigFiles.Name = "tsbGotoConfigFiles";
            this.tsbGotoConfigFiles.Size = new System.Drawing.Size(23, 22);
            this.tsbGotoConfigFiles.Text = "toolStripButton1";
            this.tsbGotoConfigFiles.ToolTipText = "Go to ContentStar config files";
            this.tsbGotoConfigFiles.Click += new System.EventHandler(this.tsbGotoConfigFiles_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // tsbBuildSetupAndContent
            // 
            this.tsbBuildSetupAndContent.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbBuildSetupAndContent.Image = ((System.Drawing.Image)(resources.GetObject("tsbBuildSetupAndContent.Image")));
            this.tsbBuildSetupAndContent.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbBuildSetupAndContent.Name = "tsbBuildSetupAndContent";
            this.tsbBuildSetupAndContent.Size = new System.Drawing.Size(23, 22);
            this.tsbBuildSetupAndContent.Text = "Automatically build setup2.xml and content.xml";
            this.tsbBuildSetupAndContent.Click += new System.EventHandler(this.tsbBuildSetupAndContent_Click);
            // 
            // tsbBuildXMLs
            // 
            this.tsbBuildXMLs.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbBuildXMLs.Image = ((System.Drawing.Image)(resources.GetObject("tsbBuildXMLs.Image")));
            this.tsbBuildXMLs.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbBuildXMLs.Name = "tsbBuildXMLs";
            this.tsbBuildXMLs.Size = new System.Drawing.Size(23, 22);
            this.tsbBuildXMLs.Text = "Build Setup2 and Content XML only";
            this.tsbBuildXMLs.Click += new System.EventHandler(this.tsbBuildXMLs_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 25);
            // 
            // tsbOptions
            // 
            this.tsbOptions.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbOptions.Image = ((System.Drawing.Image)(resources.GetObject("tsbOptions.Image")));
            this.tsbOptions.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbOptions.Name = "tsbOptions";
            this.tsbOptions.Size = new System.Drawing.Size(23, 22);
            this.tsbOptions.Text = "Options";
            this.tsbOptions.Click += new System.EventHandler(this.tsbOptions_Click);
            // 
            // rtbOutput
            // 
            this.rtbOutput.BackColor = System.Drawing.Color.Silver;
            this.rtbOutput.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtbOutput.Font = new System.Drawing.Font("Courier New", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtbOutput.Location = new System.Drawing.Point(0, 0);
            this.rtbOutput.Name = "rtbOutput";
            this.rtbOutput.ReadOnly = true;
            this.rtbOutput.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.rtbOutput.Size = new System.Drawing.Size(1260, 172);
            this.rtbOutput.TabIndex = 0;
            this.rtbOutput.TabStop = false;
            this.rtbOutput.Text = "";
            // 
            // ofdAddFile
            // 
            this.ofdAddFile.Multiselect = true;
            this.ofdAddFile.Title = "Select a file to add...";
            // 
            // ofdOpenSolution
            // 
            this.ofdOpenSolution.Filter = "ContentStar solution|*.cstarsol";
            this.ofdOpenSolution.Title = "Select solution to open...";
            // 
            // sfdSolution
            // 
            this.sfdSolution.DefaultExt = "cstarsol";
            this.sfdSolution.Filter = "ContentStar solution|*.cstarsol";
            this.sfdSolution.Title = "Save solution...";
            // 
            // ctxtmnuOutputWnd
            // 
            this.ctxtmnuOutputWnd.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnuOutWndClearCur,
            this.menuItem7,
            this.mnuOutWndClearAll});
            // 
            // mnuOutWndClearCur
            // 
            this.mnuOutWndClearCur.Index = 0;
            this.mnuOutWndClearCur.Text = "Clear";
            this.mnuOutWndClearCur.Click += new System.EventHandler(this.mnuClearOutputWnd_Click);
            // 
            // menuItem7
            // 
            this.menuItem7.Index = 1;
            this.menuItem7.Text = "-";
            // 
            // mnuOutWndClearAll
            // 
            this.mnuOutWndClearAll.Index = 2;
            this.mnuOutWndClearAll.Text = "Clear All";
            this.mnuOutWndClearAll.Click += new System.EventHandler(this.mnuOutWndClearAll_Click);
            // 
            // ofdContentData
            // 
            this.ofdContentData.Title = "Select file...";
            // 
            // sfdProject
            // 
            this.sfdProject.DefaultExt = "cstarproj";
            this.sfdProject.Filter = "ContentStar project|*.cstarproj";
            this.sfdProject.Title = "Save project...";
            // 
            // ofdProject
            // 
            this.ofdProject.DefaultExt = "cstarproj";
            this.ofdProject.Filter = "ContentStar project|*.cstarproj";
            this.ofdProject.Multiselect = true;
            this.ofdProject.Title = "Select project to open...";
            // 
            // ctxtmnuTvNode
            // 
            this.ctxtmnuTvNode.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ctxtToggleBuildStatus,
            this.toolStripSeparator6,
            this.ctxtOpenExpHere});
            this.ctxtmnuTvNode.Name = "ctxtmnuTvNode";
            this.ctxtmnuTvNode.Size = new System.Drawing.Size(175, 54);
            // 
            // ctxtToggleBuildStatus
            // 
            this.ctxtToggleBuildStatus.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ctxtBuildAll,
            this.ctxtBuildNone,
            this.toolStripSeparator9,
            this.ctxtToggleThisOnly,
            this.ctxtBuildThisOnly,
            this.ctxtBuildAllExceptThis});
            this.ctxtToggleBuildStatus.Name = "ctxtToggleBuildStatus";
            this.ctxtToggleBuildStatus.Size = new System.Drawing.Size(174, 22);
            this.ctxtToggleBuildStatus.Text = "Build status";
            // 
            // ctxtBuildAll
            // 
            this.ctxtBuildAll.Name = "ctxtBuildAll";
            this.ctxtBuildAll.Size = new System.Drawing.Size(175, 22);
            this.ctxtBuildAll.Text = "Build all";
            this.ctxtBuildAll.Click += new System.EventHandler(this.ctxtBuildAll_Click);
            // 
            // ctxtBuildNone
            // 
            this.ctxtBuildNone.Name = "ctxtBuildNone";
            this.ctxtBuildNone.Size = new System.Drawing.Size(175, 22);
            this.ctxtBuildNone.Text = "Build none";
            this.ctxtBuildNone.Click += new System.EventHandler(this.ctxtBuildNone_Click);
            // 
            // toolStripSeparator9
            // 
            this.toolStripSeparator9.Name = "toolStripSeparator9";
            this.toolStripSeparator9.Size = new System.Drawing.Size(172, 6);
            // 
            // ctxtToggleThisOnly
            // 
            this.ctxtToggleThisOnly.Name = "ctxtToggleThisOnly";
            this.ctxtToggleThisOnly.Size = new System.Drawing.Size(175, 22);
            this.ctxtToggleThisOnly.Text = "Toggle this only";
            this.ctxtToggleThisOnly.Click += new System.EventHandler(this.ctxtToggleThisOnly_Click);
            // 
            // ctxtBuildThisOnly
            // 
            this.ctxtBuildThisOnly.Name = "ctxtBuildThisOnly";
            this.ctxtBuildThisOnly.Size = new System.Drawing.Size(175, 22);
            this.ctxtBuildThisOnly.Text = "Build this only";
            this.ctxtBuildThisOnly.Click += new System.EventHandler(this.ctxtToggleBuildOnlyThis_Click);
            // 
            // ctxtBuildAllExceptThis
            // 
            this.ctxtBuildAllExceptThis.Name = "ctxtBuildAllExceptThis";
            this.ctxtBuildAllExceptThis.Size = new System.Drawing.Size(175, 22);
            this.ctxtBuildAllExceptThis.Text = "Build all except this";
            this.ctxtBuildAllExceptThis.Click += new System.EventHandler(this.ctxtToggleBuildAllExceptThis_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(171, 6);
            // 
            // ctxtOpenExpHere
            // 
            this.ctxtOpenExpHere.Name = "ctxtOpenExpHere";
            this.ctxtOpenExpHere.Size = new System.Drawing.Size(174, 22);
            this.ctxtOpenExpHere.Text = "Open explorer here";
            this.ctxtOpenExpHere.Click += new System.EventHandler(this.ctxtOpenExpHere_Click);
            // 
            // mnuMainBar
            // 
            this.mnuMainBar.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnuFileOpt,
            this.mnuBuildOpt,
            this.mnuHelpOpt});
            // 
            // mnuFileOpt
            // 
            this.mnuFileOpt.Index = 0;
            this.mnuFileOpt.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnuRefresh,
            this.menuItem2,
            this.mnuSaveOpt,
            this.menuItem5,
            this.mnuExitOpt});
            this.mnuFileOpt.Text = "File";
            // 
            // mnuRefresh
            // 
            this.mnuRefresh.Index = 0;
            this.mnuRefresh.Shortcut = System.Windows.Forms.Shortcut.F5;
            this.mnuRefresh.Text = "Refresh";
            this.mnuRefresh.Click += new System.EventHandler(this.mnuRefresh_Click);
            // 
            // menuItem2
            // 
            this.menuItem2.Index = 1;
            this.menuItem2.Text = "-";
            // 
            // mnuSaveOpt
            // 
            this.mnuSaveOpt.Index = 2;
            this.mnuSaveOpt.Shortcut = System.Windows.Forms.Shortcut.CtrlS;
            this.mnuSaveOpt.Text = "Save";
            this.mnuSaveOpt.Click += new System.EventHandler(this.mnuSaveOpt_Click);
            // 
            // menuItem5
            // 
            this.menuItem5.Index = 3;
            this.menuItem5.Text = "-";
            // 
            // mnuExitOpt
            // 
            this.mnuExitOpt.Index = 4;
            this.mnuExitOpt.Shortcut = System.Windows.Forms.Shortcut.AltF4;
            this.mnuExitOpt.Text = "Exit";
            this.mnuExitOpt.Click += new System.EventHandler(this.mnuExitOpt_Click);
            // 
            // mnuBuildOpt
            // 
            this.mnuBuildOpt.Index = 1;
            this.mnuBuildOpt.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnuBuildAllOpt,
            this.menuItem3,
            this.mnuBuildXBOXOpt,
            this.mnuBuildPS3Opt,
            this.mnuBuildPCOpt,
            this.menuItem4,
            this.mnuBuildCloudOpt,
            this.menuItem6,
            this.mnuClean});
            this.mnuBuildOpt.Text = "Build";
            // 
            // mnuBuildAllOpt
            // 
            this.mnuBuildAllOpt.Index = 0;
            this.mnuBuildAllOpt.Shortcut = System.Windows.Forms.Shortcut.CtrlB;
            this.mnuBuildAllOpt.Text = "Build All";
            this.mnuBuildAllOpt.Click += new System.EventHandler(this.mnuBuildAllOpt_Click);
            // 
            // menuItem3
            // 
            this.menuItem3.Index = 1;
            this.menuItem3.Text = "-";
            // 
            // mnuBuildXBOXOpt
            // 
            this.mnuBuildXBOXOpt.Index = 2;
            this.mnuBuildXBOXOpt.Shortcut = System.Windows.Forms.Shortcut.CtrlM;
            this.mnuBuildXBOXOpt.Text = "Build XBOX";
            this.mnuBuildXBOXOpt.Click += new System.EventHandler(this.mnuBuildXBOXOpt_Click);
            // 
            // mnuBuildPS3Opt
            // 
            this.mnuBuildPS3Opt.Index = 3;
            this.mnuBuildPS3Opt.Shortcut = System.Windows.Forms.Shortcut.CtrlP;
            this.mnuBuildPS3Opt.Text = "Build PS3";
            this.mnuBuildPS3Opt.Click += new System.EventHandler(this.mnuBuildPS3Opt_Click);
            // 
            // mnuBuildPCOpt
            // 
            this.mnuBuildPCOpt.Index = 4;
            this.mnuBuildPCOpt.Shortcut = System.Windows.Forms.Shortcut.CtrlW;
            this.mnuBuildPCOpt.Text = "Build PC";
            this.mnuBuildPCOpt.Click += new System.EventHandler(this.mnuBuildPCOpt_Click);
            // 
            // menuItem4
            // 
            this.menuItem4.Index = 5;
            this.menuItem4.Text = "-";
            // 
            // mnuBuildCloudOpt
            // 
            this.mnuBuildCloudOpt.Index = 6;
            this.mnuBuildCloudOpt.Shortcut = System.Windows.Forms.Shortcut.CtrlR;
            this.mnuBuildCloudOpt.Text = "Build Cloud";
            this.mnuBuildCloudOpt.Click += new System.EventHandler(this.mnuBuildCloudOpt_Click);
            // 
            // menuItem6
            // 
            this.menuItem6.Index = 7;
            this.menuItem6.Text = "-";
            // 
            // mnuClean
            // 
            this.mnuClean.Index = 8;
            this.mnuClean.Shortcut = System.Windows.Forms.Shortcut.CtrlShiftT;
            this.mnuClean.Text = "Clean";
            this.mnuClean.Click += new System.EventHandler(this.mnuClean_Click);
            // 
            // mnuHelpOpt
            // 
            this.mnuHelpOpt.Index = 2;
            this.mnuHelpOpt.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnuWikiOpt});
            this.mnuHelpOpt.Text = "Help";
            // 
            // mnuWikiOpt
            // 
            this.mnuWikiOpt.Index = 0;
            this.mnuWikiOpt.Text = "Wiki";
            this.mnuWikiOpt.Click += new System.EventHandler(this.mnuWikiOpt_Click);
            // 
            // chkReleaseBuild
            // 
            this.chkReleaseBuild.AutoSize = true;
            this.chkReleaseBuild.Location = new System.Drawing.Point(277, 164);
            this.chkReleaseBuild.Name = "chkReleaseBuild";
            this.chkReleaseBuild.Size = new System.Drawing.Size(65, 17);
            this.chkReleaseBuild.TabIndex = 25;
            this.chkReleaseBuild.Text = "Release";
            this.chkReleaseBuild.UseVisualStyleBackColor = true;
            this.chkReleaseBuild.CheckedChanged += new System.EventHandler(this.chkReleaseBuild_CheckedChanged);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1264, 682);
            this.Controls.Add(this.spContMainHorz);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Menu = this.mnuMainBar;
            this.MinimumSize = new System.Drawing.Size(1280, 720);
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ContentStar";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMain_FormClosing);
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.Shown += new System.EventHandler(this.frmMain_Shown);
            this.Resize += new System.EventHandler(this.frmMain_Resize);
            this.spContMainHorz.Panel1.ResumeLayout(false);
            this.spContMainHorz.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spContMainHorz)).EndInit();
            this.spContMainHorz.ResumeLayout(false);
            this.spContTopVert.Panel1.ResumeLayout(false);
            this.spContTopVert.Panel2.ResumeLayout(false);
            this.spContTopVert.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spContTopVert)).EndInit();
            this.spContTopVert.ResumeLayout(false);
            this.spContentTreeHorz.Panel1.ResumeLayout(false);
            this.spContentTreeHorz.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spContentTreeHorz)).EndInit();
            this.spContentTreeHorz.ResumeLayout(false);
            this.tcAttributes.ResumeLayout(false);
            this.tabpbContent.ResumeLayout(false);
            this.tabpbContent.PerformLayout();
            this.tabpgGlobalFields.ResumeLayout(false);
            this.grpEditBank.ResumeLayout(false);
            this.grpEditBank.PerformLayout();
            this.gbPS4GlobalFields.ResumeLayout(false);
            this.gbPS4GlobalFields.PerformLayout();
            this.grpGlobalsXbox.ResumeLayout(false);
            this.grpGlobalsXbox.PerformLayout();
            this.grpGlobalsPs3.ResumeLayout(false);
            this.grpGlobalsPs3.PerformLayout();
            this.grpGlobalBanks.ResumeLayout(false);
            this.grpGlobalBanks.PerformLayout();
            this.tabpgXbox.ResumeLayout(false);
            this.tabpgXbox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbXboxTitleImg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbXboxDBIcon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbXboxOffBan)).EndInit();
            this.tabpgPS3.ResumeLayout(false);
            this.tabpgPS3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbPs3Banner)).EndInit();
            this.tbPS4.ResumeLayout(false);
            this.tbPS4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbPS4Icon0)).EndInit();
            this.tbXboxOne.ResumeLayout(false);
            this.grpXB1AppManifest.ResumeLayout(false);
            this.grpXB1AppManifest.PerformLayout();
            this.grpXB1ProductID.ResumeLayout(false);
            this.grpXB1ProductID.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbXB1WideLogo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbXB1SmallLogo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbXB1LiveLogo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbXB1StoreLogo)).EndInit();
            this.grpVisualElements.ResumeLayout(false);
            this.grpVisualElements.PerformLayout();
            this.grpXB1Properties.ResumeLayout(false);
            this.grpXB1Properties.PerformLayout();
            this.grpChunks.ResumeLayout(false);
            this.grpXB1Identity.ResumeLayout(false);
            this.grpXB1Identity.PerformLayout();
            this.toolStripMain.ResumeLayout(false);
            this.toolStripMain.PerformLayout();
            this.ctxtmnuTvNode.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer spContMainHorz;
        private System.Windows.Forms.SplitContainer spContTopVert;
        private System.Windows.Forms.TreeView tvContentTree;
        private System.Windows.Forms.OpenFileDialog ofdAddFile;
        private System.Windows.Forms.FolderBrowserDialog fbdAddFolder;
        private System.Windows.Forms.ImageList ilContentTree;
        private System.Windows.Forms.ToolStrip toolStripMain;
        private System.Windows.Forms.ToolStripButton tsbBuildAll;
        private System.Windows.Forms.ToolStripButton tsbBuildXbox;
        private System.Windows.Forms.ToolStripButton tsbBuildPs3;
        private System.Windows.Forms.TabControl tcAttributes;
        private System.Windows.Forms.TabPage tabpgXbox;
        private System.Windows.Forms.TabPage tabpbContent;
        private System.Windows.Forms.Label lblFullPath;
        private System.Windows.Forms.TextBox txtFullPath;
        private System.Windows.Forms.Label lblContentName;
        private System.Windows.Forms.TextBox txtContentName;
        private System.Windows.Forms.OpenFileDialog ofdOpenSolution;
        private System.Windows.Forms.RichTextBox rtbOutput;
        private System.Windows.Forms.SaveFileDialog sfdSolution;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton tsbBuildPC;
        private System.Windows.Forms.ContextMenu ctxtmnuOutputWnd;
        private System.Windows.Forms.MenuItem mnuOutWndClearCur;
        private System.Windows.Forms.TabPage tabpgPS3;
        private System.Windows.Forms.TabPage tabpgPC;
        private System.Windows.Forms.TextBox txtXboxPubFlags;
        private System.Windows.Forms.Button btnBrowseXboxDBIcon;
        private System.Windows.Forms.Button btnBrowseXboxTitleImg;
        private System.Windows.Forms.Button btnBrowseXboxOfferBan;
        private System.Windows.Forms.PictureBox pbXboxDBIcon;
        private System.Windows.Forms.PictureBox pbXboxOffBan;
        private System.Windows.Forms.Label lblXboxOffBan;
        private System.Windows.Forms.TextBox txtXboxOffBan;
        private System.Windows.Forms.Label lblXboxPkgName;
        private System.Windows.Forms.TextBox txtXboxPkgName;
        private System.Windows.Forms.Label lblXboxTitleImg;
        private System.Windows.Forms.TextBox txtXboxTitleID;
        private System.Windows.Forms.TextBox txtXboxTitleImage;
        private System.Windows.Forms.Label lblXboxTitleID;
        private System.Windows.Forms.Label lblXboxDBIcon;
        private System.Windows.Forms.TextBox txtXboxGameTitle;
        private System.Windows.Forms.TextBox txtXboxDBIcon;
        private System.Windows.Forms.Label lblXboxGameTitle;
        private System.Windows.Forms.Label lblXboxActDate;
        private System.Windows.Forms.TextBox txtXboxOffID;
        private System.Windows.Forms.Label lblXboxOffID;
        private System.Windows.Forms.Label lblXboxPubFlags;
        private System.Windows.Forms.Label lblPs3USContID;
        private System.Windows.Forms.TextBox txtPs3ContType;
        private System.Windows.Forms.Label lblPs3ContType;
        private System.Windows.Forms.TextBox txtPs3DRMType;
        private System.Windows.Forms.Label lblPs3DRMType;
        private System.Windows.Forms.TextBox txtPs3KLicensee;
        private System.Windows.Forms.Label lblKLicensee;
        private System.Windows.Forms.Button btnBrowsePs3USParamSfo;
        private System.Windows.Forms.Button btnBrowsePs3Icon0;
        private System.Windows.Forms.TextBox txtPs3Icon0;
        private System.Windows.Forms.Label lblPs3Icon0;
        private System.Windows.Forms.TextBox txtPs3USParamSfo;
        private System.Windows.Forms.Label lblPs3USParamSfo;
        private System.Windows.Forms.PictureBox pbPs3Banner;
        private System.Windows.Forms.ToolStripButton tsbOptions;
        private System.Windows.Forms.DateTimePicker dtPickerXboxActDate;
        private System.Windows.Forms.OpenFileDialog ofdContentData;
        private System.Windows.Forms.Label lblPs3JPNContID;
        private System.Windows.Forms.Label lblPs3EUContID;
        private System.Windows.Forms.Label lblPs3USContIDZero;
        private System.Windows.Forms.TextBox txtPs3USCID_Third;
        private System.Windows.Forms.Label lblPs3USContIDHyp;
        private System.Windows.Forms.TextBox txtPs3USCID_Second;
        private System.Windows.Forms.Label lblPs3USContIDSep;
        private System.Windows.Forms.TextBox txtPs3USCID_First;
        private System.Windows.Forms.Label lblPs3EUContIDZero;
        private System.Windows.Forms.TextBox txtPs3EUCID_Third;
        private System.Windows.Forms.Label lblPs3EUContIDHyp;
        private System.Windows.Forms.TextBox txtPs3EUCID_Second;
        private System.Windows.Forms.Label lblPs3EUContIDSep;
        private System.Windows.Forms.TextBox txtPs3EUCID_First;
        private System.Windows.Forms.Label lblPs3JPNContIDZero;
        private System.Windows.Forms.TextBox txtPs3JPNCID_Third;
        private System.Windows.Forms.Label lblPs3JPNContIDHyp;
        private System.Windows.Forms.TextBox txtPs3JPNCID_Second;
        private System.Windows.Forms.Label lblPs3JPNContIDSep;
        private System.Windows.Forms.TextBox txtPs3JPNCID_First;
        private System.Windows.Forms.Button btnBrowsePs3JPNParamSfo;
        private System.Windows.Forms.TextBox txtPs3JPNParamSfo;
        private System.Windows.Forms.Label lblPs3JPNParamSfo;
        private System.Windows.Forms.Button btnBrowsePs3EUParamSfo;
        private System.Windows.Forms.TextBox txtPs3EUParamSfo;
        private System.Windows.Forms.Label lblPs3EUParamSfo;
        private System.Windows.Forms.ToolTip ttGeneric;
        public System.Windows.Forms.SaveFileDialog sfdProject;
        private System.Windows.Forms.OpenFileDialog ofdProject;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton tsbOpenContentFolder;
        private System.Windows.Forms.TextBox txtBSize;
        private System.Windows.Forms.Label lblSize;
        private System.Windows.Forms.TextBox txtMBSize;
        private System.Windows.Forms.TextBox txtKBSize;
        private System.Windows.Forms.SplitContainer spContentTreeHorz;
        private System.Windows.Forms.ComboBox cboSearchType;
        private System.Windows.Forms.Button btnExpandAll;
        private System.Windows.Forms.Button btnCollapseAll;
        private System.Windows.Forms.ComboBox cboSearchWord;
        private System.Windows.Forms.TextBox txtBuildInclusions;
        private System.Windows.Forms.Label lblBuilds;
        private System.Windows.Forms.CheckBox chkHasCost;
        private System.Windows.Forms.CheckBox chkDevTrans;
        private System.Windows.Forms.CheckBox chkProfTrans;
        private System.Windows.Forms.CheckBox chkIntForMktPlc;
        private System.Windows.Forms.TextBox txtPrevOfferId;
        private System.Windows.Forms.Label lblPrevOfferId;
        private System.Windows.Forms.TextBox txtContentCat;
        private System.Windows.Forms.Label lblContentCat;
        private System.Windows.Forms.TextBox txtLicenseMask;
        private System.Windows.Forms.Label lblLicenseMask;
        private System.Windows.Forms.TextBox txtOfferId;
        private System.Windows.Forms.Label lblOfferId;
        private System.Windows.Forms.ComboBox cboVisibility;
        private System.Windows.Forms.Label lblVisibility;
        private System.Windows.Forms.CheckBox chkConsumableOffer;
        private System.Windows.Forms.CheckBox chkRestrictedLicense;
        private System.Windows.Forms.ContextMenuStrip ctxtmnuTvNode;
        private System.Windows.Forms.ToolStripMenuItem ctxtToggleBuildStatus;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripMenuItem ctxtOpenExpHere;
        private System.Windows.Forms.MainMenu mnuMainBar;
        private System.Windows.Forms.MenuItem mnuFileOpt;
        private System.Windows.Forms.MenuItem mnuSaveOpt;
        private System.Windows.Forms.MenuItem menuItem5;
        private System.Windows.Forms.MenuItem mnuExitOpt;
        private System.Windows.Forms.MenuItem mnuBuildOpt;
        private System.Windows.Forms.MenuItem mnuBuildAllOpt;
        private System.Windows.Forms.MenuItem menuItem3;
        private System.Windows.Forms.MenuItem mnuBuildXBOXOpt;
        private System.Windows.Forms.MenuItem mnuBuildPS3Opt;
        private System.Windows.Forms.MenuItem mnuBuildPCOpt;
        private System.Windows.Forms.MenuItem mnuHelpOpt;
        private System.Windows.Forms.MenuItem mnuWikiOpt;
        private System.Windows.Forms.MenuItem mnuClean;
        private System.Windows.Forms.MenuItem menuItem4;
        private System.Windows.Forms.TabPage tabpgGlobalFields;
        private System.Windows.Forms.TextBox txtGlobalFieldEUContentIDSecond;
        private System.Windows.Forms.Label lblPS3GFEUContIDHyp;
        private System.Windows.Forms.TextBox txtGlobalFieldEUContentIDFirst;
        private System.Windows.Forms.TextBox txtGlobalFieldUSContentIDSecond;
        private System.Windows.Forms.Label lblPS3GFUSContIDHyp;
        private System.Windows.Forms.TextBox txtGlobalFieldUSContentIDFirst;
        private System.Windows.Forms.Label lblGlobFieldEUContID;
        private System.Windows.Forms.TextBox txtGlobalFieldKLicense;
        private System.Windows.Forms.Label lblGlobFieldKLicensee;
        private System.Windows.Forms.Label lblGlobFieldUSContID;
        private System.Windows.Forms.TextBox txtGlobalFieldTitleID;
        private System.Windows.Forms.Label lblGlobFieldTitleID;
        private System.Windows.Forms.TextBox txtGlobalFieldGameTitle;
        private System.Windows.Forms.Label lblGlobFieldTitle;
        private System.Windows.Forms.GroupBox grpGlobalsPs3;
        private System.Windows.Forms.GroupBox grpGlobalsXbox;
        private System.Windows.Forms.GroupBox grpGlobalBanks;
        private System.Windows.Forms.Label lblGlobalBank;
        private System.Windows.Forms.ComboBox cboBankName;
        private System.Windows.Forms.Button btnAddNewBank;
        private System.Windows.Forms.Button btnDeleteBank;
        private System.Windows.Forms.Button btnSetCurrBank;
        private System.Windows.Forms.TextBox txtGlobalFieldJPNContentIDSecond;
        private System.Windows.Forms.Label lblPS3GFJPNContIDHyp;
        private System.Windows.Forms.TextBox txtGlobalFieldJPNContentIDFirst;
        private System.Windows.Forms.Label lblGlobFieldJPNContID;
        private System.Windows.Forms.Label lblCurrentBank;
        private System.Windows.Forms.TextBox txtCurrGlobalBank;
        private System.Windows.Forms.GroupBox grpEditBank;
        private System.Windows.Forms.TextBox txtPs3JNVer;
        private System.Windows.Forms.Label lblPs3JNVer;
        private System.Windows.Forms.TextBox txtPs3EUVer;
        private System.Windows.Forms.Label lblPs3EUVer;
        private System.Windows.Forms.TextBox txtPs3USVer;
        private System.Windows.Forms.Label lblUSVer;
        private System.Windows.Forms.PictureBox pbXboxTitleImg;
        private System.Windows.Forms.ComboBox cboLogFilter;
        private System.Windows.Forms.Button btnLogFilter;
        private System.Windows.Forms.MenuItem menuItem7;
        private System.Windows.Forms.MenuItem mnuOutWndClearAll;
        private System.Windows.Forms.ToolStripMenuItem ctxtBuildThisOnly;
        private System.Windows.Forms.ToolStripMenuItem ctxtBuildAllExceptThis;
        private System.Windows.Forms.ToolStripMenuItem ctxtToggleThisOnly;
        private System.Windows.Forms.ToolStripMenuItem ctxtBuildAll;
        private System.Windows.Forms.ToolStripMenuItem ctxtBuildNone;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator9;
        private System.Windows.Forms.ToolStripButton tsbGotoConfigFiles;
        private System.Windows.Forms.ToolStripButton tsbBuildCloud;
        private System.Windows.Forms.MenuItem mnuRefresh;
        private System.Windows.Forms.MenuItem menuItem2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.MenuItem mnuBuildCloudOpt;
        private System.Windows.Forms.MenuItem menuItem6;
        private System.Windows.Forms.TextBox txtBuildDeps;
        private System.Windows.Forms.Label lblBuildDeps;
        private System.Windows.Forms.TextBox txtBuildDependents;
        private System.Windows.Forms.Label lblBuildDependents;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton tsbBuildSetupAndContent;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.TextBox txtProjRelPath;
        private System.Windows.Forms.Label lblProjRelPath;
        private System.Windows.Forms.TextBox txtBuildingProjs;
        private System.Windows.Forms.Label lblBuildingProjs;
        private System.Windows.Forms.ToolStripButton tsbBuildPS4;
        private System.Windows.Forms.ToolStripButton tsbBuildXbone;
        private System.Windows.Forms.TabPage tbPS4;
        private System.Windows.Forms.Button btnPS4CodeGen;
        private System.Windows.Forms.TextBox txtPS4VerJPN;
        private System.Windows.Forms.Label lblPS4JNVer;
        private System.Windows.Forms.TextBox txtPS4VerEU;
        private System.Windows.Forms.Label lblPS4EUVer;
        private System.Windows.Forms.TextBox txtPS4VerUS;
        private System.Windows.Forms.Label lblPS4USVer;
        private System.Windows.Forms.Button btnPS4JPNParamSfo;
        private System.Windows.Forms.TextBox txtPS4JPNParamSfo;
        private System.Windows.Forms.Label lblPS4JNParam;
        private System.Windows.Forms.Button btnPS4EUParamSfo;
        private System.Windows.Forms.TextBox txtPS4EUParamSfo;
        private System.Windows.Forms.Label lblPS4EUParam;
        private System.Windows.Forms.Label lblPS4JNContIDZero;
        private System.Windows.Forms.TextBox txtPS4JPNContID_Third;
        private System.Windows.Forms.Label lblPS4JPNContIDHyp;
        private System.Windows.Forms.TextBox txtPS4JPNContID_Second;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtPS4JPNContID_First;
        private System.Windows.Forms.Label lblPS4EUContIDZero;
        private System.Windows.Forms.TextBox txtPS4EUContID_Third;
        private System.Windows.Forms.Label lblPS4EUContIDHyp;
        private System.Windows.Forms.TextBox txtPS4EUContID_Second;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtPS4EUContID_First;
        private System.Windows.Forms.Label lblPS4USContIDZero;
        private System.Windows.Forms.TextBox txtPS4USContID_Third;
        private System.Windows.Forms.Label lblPS4USContIDHyp;
        private System.Windows.Forms.TextBox txtPS4USContID_Second;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtPS4USContID_First;
        private System.Windows.Forms.Label lblPS4JPNContID;
        private System.Windows.Forms.Label lblPS4EUContID;
        private System.Windows.Forms.Button btnPS4USParamSfo;
        private System.Windows.Forms.Button txtPS4Icon0Browse;
        private System.Windows.Forms.TextBox txtPS4Icon0;
        private System.Windows.Forms.Label lblPS4Icon;
        private System.Windows.Forms.TextBox txtPS4USParamSfo;
        private System.Windows.Forms.Label lblPS4USParam;
        private System.Windows.Forms.PictureBox pbPS4Icon0;
        private System.Windows.Forms.TextBox txtPS4Code;
        private System.Windows.Forms.Label lblPS4Passcode;
        private System.Windows.Forms.TextBox txtPS4Entitlement;
        private System.Windows.Forms.Label lblPS4Entitlement;
        private System.Windows.Forms.Label lblPS4USContID;
        private System.Windows.Forms.GroupBox gbPS4GlobalFields;
        private System.Windows.Forms.TextBox txtPS4GFJPNContIDSecond;
        private System.Windows.Forms.Label lblPS4GFJPNContIDHyp;
        private System.Windows.Forms.TextBox txtPS4GFJPNContIDFirst;
        private System.Windows.Forms.Label lblPS4GFJNContID;
        private System.Windows.Forms.TextBox txtPS4GFUSContIDFirst;
        private System.Windows.Forms.Label lblPS4GFUSContID;
        private System.Windows.Forms.TextBox txtPS4GFEUContIDSecond;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lblPS4GFEUContIDHyp;
        private System.Windows.Forms.TextBox txtPS4GFEntitlement;
        private System.Windows.Forms.TextBox txtPS4GFEUContIDFirst;
        private System.Windows.Forms.Label lblPS4GFEUContID;
        private System.Windows.Forms.TextBox txtPS4GFUSContIDSecond;
        private System.Windows.Forms.Label lblPS4GFUSContIDHyp;
        private System.Windows.Forms.TextBox txtPS4VolType;
        private System.Windows.Forms.Label lblPS4VolType;
		private System.Windows.Forms.TabPage tbXboxOne;
		private System.Windows.Forms.GroupBox grpChunks;
		private System.Windows.Forms.Button btXB1EditChunkFile;
		private System.Windows.Forms.Button btnXB1LoadChunkFile;
		private System.Windows.Forms.Button btnXB1GenerateChunk;
		private System.Windows.Forms.Button btnXB1SaveChunksFile;
		private System.Windows.Forms.GroupBox grpXB1AppManifest;
		private System.Windows.Forms.GroupBox grpXB1Properties;
		private System.Windows.Forms.GroupBox grpXB1Identity;
		private System.Windows.Forms.TextBox txtXB1IdentityVersion;
		private System.Windows.Forms.Label lbXB1IVersion;
		private System.Windows.Forms.TextBox txtXB1IdentityName;
		private System.Windows.Forms.Label lbXB1IName;
		private System.Windows.Forms.Label lbXB1PDisplayName;
		private System.Windows.Forms.TextBox txtXB1PPublisherName;
		private System.Windows.Forms.Label lbXB1PublisherDname;
		private System.Windows.Forms.TextBox txtXB1PDisplayName;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox txtXB1PDescription;
		private System.Windows.Forms.Label lbXB1PDescription;
		private System.Windows.Forms.GroupBox grpVisualElements;
		private System.Windows.Forms.TextBox txtXB1VDisplayName;
		private System.Windows.Forms.Label lbXB1VDisplayName;
		private System.Windows.Forms.Label lbXB1VDesc;
		private System.Windows.Forms.TextBox txtXB1VDescription;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.PictureBox pbXB1SmallLogo;
		private System.Windows.Forms.PictureBox pbXB1LiveLogo;
		private System.Windows.Forms.PictureBox pbXB1StoreLogo;
		private System.Windows.Forms.PictureBox pbXB1WideLogo;
		private System.Windows.Forms.Label lbXB1StoreLogo;
		private System.Windows.Forms.TextBox txtXB1StoreLogo;
		private System.Windows.Forms.Button btnXB1StoreLogo;
		private System.Windows.Forms.ColorDialog colorDialog1;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.TextBox txtXB1VWideLogo;
		private System.Windows.Forms.Button btnXB1WideLogo;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.TextBox txtXB1VSmallLogo;
		private System.Windows.Forms.Button btnXB1SmallLogo;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.TextBox txtXB1VLiveLogo;
		private System.Windows.Forms.Button btnXB1LiveLogo;
		private System.Windows.Forms.GroupBox grpXB1ProductID;
		private System.Windows.Forms.RadioButton rbXB1Release;
		private System.Windows.Forms.RadioButton rbXB1Dev;
		private System.Windows.Forms.TextBox txtXB1AllowedProductID;
		private System.Windows.Forms.ToolStripButton tsbBuildShippablePack;
		private System.Windows.Forms.TextBox txtXB1AllowedProductIDJPN;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ToolStripButton tsbBuildXMLs;
        private System.Windows.Forms.CheckBox chkReleaseBuild;

    }
}

