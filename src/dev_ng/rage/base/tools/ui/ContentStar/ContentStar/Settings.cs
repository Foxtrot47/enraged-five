﻿/*******************************************\
| ContentStar : Rockstar Games © 2013       |
| ------------------------------------------|
| Author: Thomas Woodhead (R* London)       |
| Email: thomas.woodhead@rockstarlondon.com |
\*******************************************/

#region Usings

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Collections;
using System.Xml;
using System.Xml.Linq;
using System.Diagnostics;
using System.Xml.Serialization;
using ContentStar;
using System.Threading;
using contentStar;

#endregion

namespace ContentStar
{
    #region DeployTarget

    [Serializable]
    public class DeployTarget
    {
        [XmlAttribute("ipAddress")]
        public string m_ipAddress;

        [XmlAttribute("deploy")]
        public bool m_deploy;

        public DeployTarget()
        {
            m_ipAddress = "";
            m_deploy = false;
        }
    }

    #endregion

    #region Settings

    [Serializable]
    [XmlRoot("CStarSettings")]
    public class SettingsManager
    {
        [XmlArray("XboxTargets")]
        [XmlArrayItem("Target", typeof(DeployTarget), IsNullable = false)]
        public List<DeployTarget> m_xboxTargets;

        [XmlArray("PS3Targets")]
        [XmlArrayItem("Target", typeof(DeployTarget), IsNullable = false)]
        public List<DeployTarget> m_ps3Targets;

        [XmlArray("PS4Targets")]
        [XmlArrayItem("Target", typeof(DeployTarget), IsNullable = false)]
		public List<DeployTarget> m_PS4Targets;

		[XmlArray("XB1Targets")]
		[XmlArrayItem("Target", typeof(DeployTarget), IsNullable = false)]
		public List<DeployTarget> m_XB1Targets;

        [XmlElement("BlastPath")]
        public string m_blastPath;

        [XmlElement("MakePackagePath")]
        public string m_makePackagePath;

        [XmlElement("PS4MakePackagePath")]
        public string m_PS4MakePackagePath;
		
		[XmlElement("XB1MakePackagePath")]
        public string m_XB1MakePackagePath;

		[XmlElement("XB1xbConnectPath")]
        public string m_XB1xbconnectPath;

		[XmlElement("XB1xbAppPath")]
		public string m_XB1xbappPath;

        [XmlElement("MakeEdatPath")]
        public string m_makeEdatPath;

        [XmlElement("PS3CtrlPath")]
        public string m_ps3CtrlPath;

        [XmlElement("PS4CtrlPath")]
        public string m_PS4CtrlPath;

        [XmlElement("GenerateSetupAndContent")]
        public bool m_generateSetupAndContent;

        [XmlElement("Ps3DeployUS")]
        public bool m_ps3DeployUS;

        [XmlElement("Ps3DeployEU")]
        public bool m_ps3DeployEU;

        [XmlElement("Ps3DeployJN")]
        public bool m_ps3DeployJN;

        [XmlElement("Ps4DeployUS")]
        public bool m_ps4DeployUS;

        [XmlElement("Ps4DeployEU")]
        public bool m_ps4DeployEU;

        [XmlElement("Ps4DeployJN")]
        public bool m_ps4DeployJN;

        [XmlIgnoreAttribute()]
        private static FileSystemWatcher m_fileWatcher;

        [XmlIgnoreAttribute()]
        private static SettingsManager m_settingsMgrInst = null;

        public static void initialise()
        {
            m_settingsMgrInst = new SettingsManager();
        }

        public SettingsManager()
        {
            m_xboxTargets = new List<DeployTarget>();
            m_ps3Targets = new List<DeployTarget>();
            m_PS4Targets = new List<DeployTarget>();
			m_XB1Targets = new List<DeployTarget>();
            m_blastPath = "";
            m_makePackagePath = "";
            m_makeEdatPath = "";
            m_ps3CtrlPath = "";
            m_PS4MakePackagePath = "";
			m_XB1MakePackagePath = "";
			m_XB1xbconnectPath = "";
            m_generateSetupAndContent = false;
            m_ps3DeployUS = false;
            m_ps3DeployEU = false;
            m_ps3DeployJN = false;
            m_ps4DeployUS = false;
            m_ps4DeployEU = false;
            m_ps4DeployJN = false;
            m_fileWatcher = null;

            validateExePath(ref m_ps3CtrlPath, Globals.DEFAULT_PS3_CTRL_32, Globals.DEFAULT_PS3_CTRL_64, Globals.DEFAULT_PS3_CTRL_32);
            validateExePath(ref m_blastPath, Globals.DEFAULT_BLAST_32, Globals.DEFAULT_BLAST_64, Globals.DEFAULT_BLAST_32);
            validateExePath(ref m_makePackagePath, Globals.DEFAULT_MAKE_PS3_PACKAGE_EXE, null, Globals.DEFAULT_MAKE_PS3_PACKAGE_EXE);
            validateExePath(ref m_makeEdatPath, Globals.DEFAULT_MAKE_PS3_EDAT_EXE, null, Globals.DEFAULT_MAKE_PS3_EDAT_EXE);
            validateExePath(ref m_PS4CtrlPath, Globals.DEFAULT_PS4_CTRL_32, Globals.DEFAULT_PS4_CTRL_64, Globals.DEFAULT_PS4_CTRL_32);
			validateExePath(ref m_PS4MakePackagePath, Globals.DEFAULT_MAKE_PS4_PACKAGE_EXE_32, Globals.DEFAULT_MAKE_PS4_PACKAGE_EXE_64, Globals.DEFAULT_MAKE_PS4_PACKAGE_EXE_32);
			validateExePath(ref m_XB1MakePackagePath, Globals.DEFAULT_MAKE_XB1_PACKAGE_EXE, Globals.DEFAULT_MAKE_XB1_PACKAGE_EXE, Globals.DEFAULT_MAKE_XB1_PACKAGE_EXE);
			validateExePath(ref m_XB1xbconnectPath, Globals.DEFAULT_XB1_CONNECT_EXE, Globals.DEFAULT_XB1_CONNECT_EXE, Globals.DEFAULT_XB1_CONNECT_EXE);
			validateExePath(ref m_XB1xbappPath, Globals.DEFAULT_XB1_XBAPP_EXE, Globals.DEFAULT_XB1_XBAPP_EXE, Globals.DEFAULT_XB1_XBAPP_EXE);
        }

        public void postDeseralise()
        {
            validateExePath(ref m_ps3CtrlPath, m_ps3CtrlPath, null, Globals.DEFAULT_PS3_CTRL_32);
            validateExePath(ref m_blastPath, m_blastPath, null, Globals.DEFAULT_BLAST_32);
            validateExePath(ref m_makePackagePath, m_makePackagePath, null, Globals.DEFAULT_MAKE_PS3_PACKAGE_EXE);
            validateExePath(ref m_makeEdatPath, m_makeEdatPath, null, Globals.DEFAULT_MAKE_PS3_EDAT_EXE);
            validateExePath(ref m_PS4CtrlPath, Globals.DEFAULT_PS4_CTRL_32, Globals.DEFAULT_PS4_CTRL_64, Globals.DEFAULT_PS4_CTRL_32);
			validateExePath(ref m_PS4MakePackagePath, Globals.DEFAULT_MAKE_PS4_PACKAGE_EXE_32, Globals.DEFAULT_MAKE_PS4_PACKAGE_EXE_64, Globals.DEFAULT_MAKE_PS4_PACKAGE_EXE_32);
			validateExePath(ref m_XB1MakePackagePath, Globals.DEFAULT_MAKE_XB1_PACKAGE_EXE, Globals.DEFAULT_MAKE_XB1_PACKAGE_EXE, Globals.DEFAULT_MAKE_XB1_PACKAGE_EXE);
			validateExePath(ref m_XB1xbconnectPath, Globals.DEFAULT_XB1_CONNECT_EXE, Globals.DEFAULT_XB1_CONNECT_EXE, Globals.DEFAULT_XB1_CONNECT_EXE);
			validateExePath(ref m_XB1xbappPath, Globals.DEFAULT_XB1_XBAPP_EXE, Globals.DEFAULT_XB1_XBAPP_EXE, Globals.DEFAULT_XB1_XBAPP_EXE);
        }

        private void validateExePath(ref string outputPath, string mainPath, string altPath, string fallBack)
        {
            outputPath = fallBack;

            if (mainPath != null && File.Exists(mainPath))
                outputPath = mainPath;
            else if (altPath != null && File.Exists(altPath))
                outputPath = altPath;
            else
            {
                OutputWindow.appendOutput("Error validating executable path!",OutputWindow.MessageType.ERROR);
                OutputWindow.appendOutput("mainPath = " + mainPath,OutputWindow.MessageType.ERROR);
                OutputWindow.appendOutput("altPath = " + altPath,OutputWindow.MessageType.ERROR);
                OutputWindow.appendOutput("fallBack = " + fallBack,OutputWindow.MessageType.ERROR);
            }
        }

        public static void setupFileWatcher(string fullPath)
        {
            if (m_fileWatcher == null)
            {
                m_fileWatcher = new FileSystemWatcher();
                Globals.setupFileWatcher(m_fileWatcher, fullPath);
            }
        }

        public static void readSettings()
        {
            try
            {
                string settingsFilePath = Application.StartupPath + "\\" + Globals.SETTINGS_FILE;

                if (File.Exists(settingsFilePath))
                {
                    OutputWindow.appendOutput("Reading " + settingsFilePath, OutputWindow.MessageType.IMPORTANT);

                    XmlSerializer settingsDeserialiser = new XmlSerializer(typeof(SettingsManager));
                    using (TextReader readStream = new StreamReader(settingsFilePath))
                    {
                        m_settingsMgrInst = (SettingsManager)settingsDeserialiser.Deserialize(readStream);

                        readStream.Close();
                    }

                    SettingsManager.GetInstance.postDeseralise();

                    setupFileWatcher(settingsFilePath);
                }
            }
            catch (Exception ex)
            {
                OutputWindow.appendOutput(ex.Message, OutputWindow.MessageType.ERROR);
            }
        }

        public static void saveSettings()
        {
            try
            {
                string settingsFilePath = Application.StartupPath + "\\" + Globals.SETTINGS_FILE;

                OutputWindow.appendOutput("Saving " + settingsFilePath, OutputWindow.MessageType.IMPORTANT);

                if (m_fileWatcher != null)
                    m_fileWatcher.EnableRaisingEvents = false; // Don't raise events about ourselves

                XmlSerializer settingsSerialiser = new XmlSerializer(typeof(SettingsManager));
                using (TextWriter writeStream = new StreamWriter(settingsFilePath))
                {
                    settingsSerialiser.Serialize(writeStream, SettingsManager.GetInstance);

                    writeStream.Close();
                }

                if (m_fileWatcher != null)
                    m_fileWatcher.EnableRaisingEvents = true;

                setupFileWatcher(settingsFilePath);
            }
            catch (Exception ex)
            {
                OutputWindow.appendOutput(ex.Message, OutputWindow.MessageType.ERROR);
            }
        }

        [XmlIgnoreAttribute()]
        public static SettingsManager GetInstance
        {
            get { return m_settingsMgrInst; }
        }
    }

    #endregion
}
