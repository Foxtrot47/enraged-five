﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ContentStar
{
	public partial class ShippablePackSelector : Form
	{
		public delegate void BuildFunction(List<string> targets);
		BuildFunction m_buildFunction;
		bool m_isClosed =false;
		public ShippablePackSelector()
		{
			InitializeComponent();
		}
		public ShippablePackSelector(BuildFunction function)
		{
			m_buildFunction = function;
			InitializeComponent();
		}
		public void SetBuildFunction(BuildFunction function)
		{
			m_buildFunction = function;
		}

		private void btnBuildShippablePacks_Click(object sender, EventArgs e)
		{
			if (m_buildFunction != null)
			{
				List<string> targets = new List<string>();
				if (cbBuildFolder.Checked)
				{
					targets.Add("build");
				}
				if (cbTUdevNG.Checked)
				{
					targets.Add("dev_ng");
				}
				if (cbTUdevTemp.Checked)
				{
					targets.Add("dev_temp");
				}
				if (cbJapaneseTU.Checked)
				{
					targets.Add("japanese");
				}
				if (cbDevNGLive.Checked)
				{
					targets.Add("dev_ng_live");
				}
				if (cbPatchesFolder.Checked)
				{
					targets.Add("patch");
				}
				if (cbJPN_NG_LIVE.Checked)
				{
					targets.Add("japanese_ng_live");
				}
				m_buildFunction(targets);
			}
		}

		private void cbDefaultCheckedChanged(object sender, EventArgs e)
		{
			CheckBox cb = sender as CheckBox;
			if (cb.Checked)
			{
				cbJapaneseTU.Checked = false;
				cbJPN_NG_LIVE.Checked = false;
			}
		}
		private void cbJapaneseTU_CheckedChanged(object sender, EventArgs e)
		{
			CheckBox cb = sender as CheckBox;
			if (cb.Checked)
			{
				cbBuildFolder.Checked = false;
				cbTUdevNG.Checked = false;
				cbTUdevTemp.Checked = false;
				cbPatchesFolder.Checked = false;
				cbDevNGLive.Checked = false;
			}
		}
	}
}
