﻿/*******************************************\
| ContentStar : Rockstar Games © 2013       |
| ------------------------------------------|
| Author: Thomas Woodhead (R* London)       |
| Email: thomas.woodhead@rockstarlondon.com |
\*******************************************/

#region Usings

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Collections;
using System.Xml;
using System.Xml.Linq;
using System.Diagnostics;
using System.Threading;
using System.Xml.Serialization;
using ContentStar;
using contentStar;
using RSG.Base.Editor;
using RSG.Metadata;
using RSG.Metadata.Model;
using RSG.Metadata.Parser;
using RSG.Base.Configuration;
using RSG.Metadata.Data;

#endregion

namespace ContentStar
{
    public class MetaFileLoader
    {
        protected string m_filePath = null;
        protected MetaFile m_metaFile = null;
        protected string m_rootTag = string.Empty;

        public bool read(string filePath)
        {
            try
            {
                m_filePath = filePath;
                m_metaFile = new MetaFile(filePath, Globals.META_DICTIONARY);
            }
            catch (System.Exception ex)
            {
                OutputWindow.appendOutput(ex.Message, OutputWindow.MessageType.ERROR);
                reset();

                return false;
            }

            return true;
        }

        public void create(string filePath)
        {
            try
            {
                m_filePath = filePath;
                m_metaFile = new MetaFile(Globals.META_DICTIONARY[m_rootTag]);
            }
            catch (System.Exception ex)
            {
                OutputWindow.appendOutput(ex.Message, OutputWindow.MessageType.ERROR);
                reset();
            }
        }

        private void reset()
        {
            m_filePath = null;
            m_metaFile = null;
        }

        public void save()
        {
            try
            {
                OutputWindow.appendOutput("Saving " + m_filePath, OutputWindow.MessageType.IMPORTANT);

                if (m_metaFile != null)
                    m_metaFile.Serialise(m_filePath);
            }
            catch (System.Exception ex)
            {
                OutputWindow.appendOutput(ex.Message, OutputWindow.MessageType.ERROR);
            }
        }

        protected StructureTunable findStructTunableInMetaFile(string definitionName)
        {
            ITunable retStruct = null;

            m_metaFile.Members.TryGetValue(definitionName, out retStruct);

            return (StructureTunable)retStruct;
        }

        protected StructureTunable findOrCreateStructTunableInMetaFile(string definitionName)
        {
            Structure targetStruct = null;
            ITunable retStruct = null;

            if (!m_metaFile.Members.TryGetValue(definitionName, out retStruct))
            {
                if (Globals.META_DICTIONARY.TryGetValue(definitionName, out targetStruct))
                    retStruct = new StructureTunable(new StructMember(targetStruct, targetStruct), m_metaFile.Members);
            }

            if (retStruct == null)
                OutputWindow.appendOutput("Cannot create tunable! " + definitionName, OutputWindow.MessageType.IMPORTANT);

            return (StructureTunable)retStruct;
        }

        public string FilePath
        {
            get { return m_filePath; }
        }
    }
}
