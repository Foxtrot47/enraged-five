﻿/*******************************************\
| ContentStar : Rockstar Games © 2013       |
| ------------------------------------------|
| Author: Thomas Woodhead (R* London)       |
| Email: thomas.woodhead@rockstarlondon.com |
\*******************************************/

#region Usings

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Collections;
using System.Xml;
using System.Xml.Linq;
using System.Diagnostics;
using System.Threading;
using System.Xml.Serialization;
using ContentStar;
using contentStar;
using RSG.Base.Editor;
using RSG.Metadata;
using RSG.Metadata.Model;
using RSG.Metadata.Parser;
using RSG.Base.Configuration;
using RSG.Metadata.Data;

#endregion

namespace ContentStar
{
    public class ContentXML : MetaFileLoader
    {
        #region Variables

        private static string[] m_dataFileOmmisions = { "registerAs", "locked", "loadCompletely", "enforceLsnSorting", "installPartition" };
        private static string[] m_persistentExtensions = { ".rpf" };
        private Dictionary<int, string> m_dataFilesData;
        private Dictionary<int, string> m_addedFilesToEnable;
        private Dictionary<int, StructureTunable> m_oldDataFiles;
        private List<int> m_oldDataFilesOrder;
        private List<int> m_oldFilesToEnableOrder;

        #endregion

        #region Constructor

        public ContentXML()
        {
            m_rootTag = "CDataFileMgr::ContentsOfDataFileXml";
            m_dataFilesData = new Dictionary<int, string>();
            m_addedFilesToEnable = new Dictionary<int, string>();
            m_oldDataFiles = new Dictionary<int, StructureTunable>();
            m_oldDataFilesOrder = new List<int>();
            m_oldFilesToEnableOrder = new List<int>();
        }

        #endregion

        #region PreSerialise

        public void preSerialise(Project project)
        {
            try
            {
                StructureTunable contentData = findOrCreateStructTunableInMetaFile(m_rootTag);

                OutputWindow.appendOutput("Preparing content file for writing " + m_filePath, OutputWindow.MessageType.IMPORTANT);

                m_dataFilesData.Clear();
                m_addedFilesToEnable.Clear();
                m_oldDataFiles.Clear();
                m_oldDataFilesOrder.Clear();
                m_oldFilesToEnableOrder.Clear();

                if (contentData != null)
                {
                    ArrayTunable contentChangeSets = getContentChangeSets();
                    ArrayTunable dataFiles = contentData["dataFiles"] as ArrayTunable;

                    // 1. Store all data files that currently exist in the file so we can look them up by filename...
                    foreach (StructureTunable dataFile in dataFiles.OfType<StructureTunable>())
                    {
                        StringTunable fileName = dataFile["filename"] as StringTunable;

                        if (fileName != null && fileName.Value != null)
                        {
                            int fileNameHash = Globals.getStringHashCode(fileName.Value);

                            if (!m_oldDataFiles.ContainsKey(fileNameHash))
                            {
                                m_oldDataFiles.Add(fileNameHash, dataFile);
                            }
							else
							{
								OutputWindow.appendOutput("Duplicated filename " + 
                                    fileName.Value + " in content.xml of " + project.m_toolsProject.Name, OutputWindow.MessageType.ERROR);
							}

                            m_oldDataFilesOrder.Add(fileNameHash);
                        }
                    }

                    // 2. Clear the list of existing data files as we will populate them from the content tree...
                    if (dataFiles != null)
                        dataFiles.Items.Clear();

                    if (contentChangeSets != null)
                    {
                        List<string> changeSetsToAdd = new List<string>();
                        Structure changesetDefinition = Globals.META_DICTIONARY["CDataFileMgr::ContentChangeSet"];

                        changeSetsToAdd.Add(project.m_title.ToUpper() + Globals.PROJECT_CS_SUFFIX);
                        changeSetsToAdd.Add(project.m_title.ToUpper() + Globals.UNLOCKS_CS_SUFFIX);

                        // 3. Go through any change sets we aren't adding ourselves and store what files they reference
                        foreach (StructureTunable contentChangeSet in contentChangeSets.Items.OfType<StructureTunable>())
                        {
                            StringTunable changeSetName = contentChangeSet["changeSetName"] as StringTunable;

                            if (!changeSetsToAdd.Contains(changeSetName.Value))
                            {
                                List<ArrayTunable> filesToEnableList = new List<ArrayTunable>();
                                ArrayTunable mapsChangeSetData = contentChangeSet["mapChangeSetData"] as ArrayTunable;

                                filesToEnableList.Add(contentChangeSet["filesToEnable"] as ArrayTunable);

                                foreach (StructureTunable mapChangeSetData in mapsChangeSetData.OfType<StructureTunable>())
                                {
                                    ArrayTunable mapChangeSetFilesToEnable = mapChangeSetData["filesToEnable"] as ArrayTunable;

                                    if (mapChangeSetFilesToEnable != null)
                                        filesToEnableList.Add(mapChangeSetFilesToEnable);
                                }

                                foreach (ArrayTunable filesToEnable in filesToEnableList)
                                {
                                    foreach (StringTunable fileToEnable in filesToEnable.OfType<StringTunable>())
                                    {
                                        int fileToEnablePathHash = Globals.getStringHashCode(fileToEnable.Value);

                                        if (!m_addedFilesToEnable.ContainsKey(fileToEnablePathHash))
                                            m_addedFilesToEnable.Add(fileToEnablePathHash, fileToEnable.Value);
                                        else
                                            OutputWindow.appendOutput("Check for duplicate file reference " + fileToEnable.Value, OutputWindow.MessageType.WARNING);
                                    }
                                }
                            }
                        }

                        foreach (string changeSetToAdd in changeSetsToAdd)
                        {
                            bool changeSetExists = false;

                            // 4. Check to see if the project change sets already exist...
                            foreach (StructureTunable contentChangeSet in contentChangeSets.Items.OfType<StructureTunable>())
                            {
                                StringTunable changeSetName = contentChangeSet["changeSetName"] as StringTunable;

                                if (changeSetName != null && String.Compare(changeSetName.Value, changeSetToAdd, true) == 0)
                                {
                                    ArrayTunable filesToEnable = contentChangeSet["filesToEnable"] as ArrayTunable;

                                    if (filesToEnable != null)
                                    {
                                        // Store the order of referenced files
                                        foreach (StringTunable currFileToEnable in filesToEnable.OfType<StringTunable>())
                                            m_oldFilesToEnableOrder.Add(Globals.getStringHashCode(currFileToEnable.Value));

                                        filesToEnable.Items.Clear();
                                    }

                                    changeSetExists = true;
                                    break;
                                }
                            }

                            // 5. Create them if we can't find them...
                            if (!changeSetExists)
                            {
                                StructureTunable newChangeSet = new StructureTunable(new StructMember(changesetDefinition, changesetDefinition), contentChangeSets);
                                StringTunable changeSetName = newChangeSet["changeSetName"] as StringTunable;

                                changeSetName.Value = changeSetToAdd;
                                contentChangeSets.Add(newChangeSet);
                            }
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                OutputWindow.appendOutput(ex.Message, OutputWindow.MessageType.ERROR);
            }
        }

        #endregion

        #region WriteProject

        private void prepareAudioFileName(ref string filePath)
        {
            // Audio files can have their binary schema number in their filename but the game does the fix up so content.xml shouldn't mention the schema version
            if (filePath.Contains("\\audio\\") || filePath.Contains("/audio/"))
            {
                string stringToFind = ".DAT";
                int datExtensionIndex = filePath.IndexOf(stringToFind, StringComparison.OrdinalIgnoreCase);

				// Kills anything after the .dat as that should be handled by the game code
                if (datExtensionIndex != -1)
                    filePath = filePath.Remove(datExtensionIndex + stringToFind.Length);
            }
        }

        public void writeProject(Project project, List<contentTreeNode> filesInProject, string deviceName)
        {
            try
            {
                StructureTunable contentData = findOrCreateStructTunableInMetaFile(m_rootTag);
                ArrayTunable changeSets = getContentChangeSets();
                ArrayTunable dataFiles = (contentData != null) ? contentData["dataFiles"] as ArrayTunable : null;

                if (contentData == null || changeSets == null || dataFiles == null)
                {
                    OutputWindow.appendOutput("Unable to find or create content data!", OutputWindow.MessageType.IMPORTANT);
                    return;
                }

                ArrayTunable filesToEnable = null;
                ArrayTunable unlockFilesToEnable = null;

                // 1. Find the files to enable for this project...
                foreach (StructureTunable changeSet in changeSets.Items.OfType<StructureTunable>())
                {
                    StringTunable changeSetName = changeSet["changeSetName"] as StringTunable;

                    if (String.Compare(changeSetName.Value, project.m_title.ToUpper() + Globals.PROJECT_CS_SUFFIX, true) == 0)
                        filesToEnable = changeSet["filesToEnable"] as ArrayTunable;

                    if (String.Compare(changeSetName.Value, project.m_title.ToUpper() + Globals.UNLOCKS_CS_SUFFIX, true) == 0)
                        unlockFilesToEnable = changeSet["filesToEnable"] as ArrayTunable;
                }

                if (filesToEnable == null || unlockFilesToEnable == null)
                    return;

                // 2. Go over all of the files included in this project, this comes from the content tree...
                Structure dataFileDefinition = Globals.META_DICTIONARY["CDataFileMgr::DataFile"];
                StructureTunable newDataFile = null;
                StringTunable fileName = null;
                EnumTunable fileType = null;
                EnumTunable contents = null;
                BoolTunable disabled = null;
                BoolTunable persistent = null;
                StringTunable newString = null;
                string filePathToWrite = string.Empty;
                string fileTypeString = string.Empty;
                int pathToWriteHashCode = 0;
                bool isNewFile = false;

                // Look for .ityp references in the existing data, they don't exist as standalone files
                // Plus let any folders slip the net since they also don't exist as standalone files
                foreach (KeyValuePair<int, StructureTunable> oldDataFilePair in m_oldDataFiles)
                {
                    StructureTunable oldDataFile = oldDataFilePair.Value;
                    StringTunable oldDataFileName = oldDataFile["filename"] as StringTunable;

                    filePathToWrite = oldDataFileName.Value;
                    pathToWriteHashCode = Globals.getStringHashCode(filePathToWrite);

                    if (String.Compare(Path.GetExtension(filePathToWrite), ".ityp", true) == 0 || 
                        Path.HasExtension(filePathToWrite) == false)
                    {
                        contents = oldDataFile["contents"] as EnumTunable;

                        if (String.Compare(contents.ValueAsString, "CONTENTS_DEFAULT") == 0)
                            oldDataFile.Remove("contents");

                        // Remove any members we wish to omit from the file
                        foreach (string omission in m_dataFileOmmisions)
                            oldDataFile.Remove(omission);

                        // Add this data file as active to the actual change set data and to the cache
                        dataFiles.Items.Add(oldDataFile);

                        if (!m_dataFilesData.ContainsKey(pathToWriteHashCode))
                            m_dataFilesData.Add(pathToWriteHashCode, filePathToWrite);
						else
						{
							OutputWindow.appendOutput("Duplicated filename " + 
                                    oldDataFileName.Value + " in content.xml of " + project.m_toolsProject.Name, OutputWindow.MessageType.ERROR);
						}

                        if (!m_addedFilesToEnable.ContainsKey(pathToWriteHashCode))
                        {
                            newString = (StringTunable)TunableFactory.Create(filesToEnable, (filesToEnable.Definition as ArrayMember).ElementType);
                            newString.Value = filePathToWrite;
                            m_addedFilesToEnable.Add(pathToWriteHashCode, filePathToWrite);

                            filesToEnable.Add(newString);
                        }
                    }
                }

                foreach (contentTreeNode fileNode in filesInProject)
                {
                    fileTypeString = FileTypeMappingMgr.GetInstance.getFileType(fileNode.FullFilePath);
                    filePathToWrite = deviceName + fileNode.ProjectRelativeFilePath;
                    filePathToWrite = filePathToWrite.Replace(deviceName + "\\", deviceName);
                    filePathToWrite = filePathToWrite.Replace("\\", "/");
                    prepareAudioFileName(ref filePathToWrite);
                    BuildManager.contractFilePath(ref filePathToWrite);
                    pathToWriteHashCode = Globals.getStringHashCode(filePathToWrite);
                    newDataFile = null;
                    isNewFile = false;

                    if (m_dataFilesData.ContainsKey(pathToWriteHashCode))
                        continue;

                    if (m_oldDataFiles.ContainsKey(pathToWriteHashCode))
                        newDataFile = m_oldDataFiles[pathToWriteHashCode];

                    if (newDataFile == null)
                    {
                        // The file didn't exist previously, ask the user to give us the enum value as a string
                        if (fileTypeString == null || fileTypeString.Length <= 0)
                        {
                            if (Globals.InputBox("Type mapping missing",
                                "Enter type mapping enum value for " + fileNode.FullFilePath + " (e.g. RPF_FILE):", ref fileTypeString, Program.getForm()) == DialogResult.OK)
                            {
                                fileTypeString = FileTypeMappingMgr.GetInstance.addTypeMapping(fileNode.FullFilePath, fileTypeString);
                            }
                        }

                        if (fileTypeString == null || fileTypeString.Length <= 0)
                        {
                            OutputWindow.appendOutput("File does not have a type mapping! " + fileNode.FullFilePath, OutputWindow.MessageType.WARNING);
                            continue;
                        }

                        newDataFile = new StructureTunable(new StructMember(dataFileDefinition, dataFileDefinition), dataFiles);
                        isNewFile = true;
                    }
                    else
                    {
                        // The file existed previously, but we don't have a mapping for it! Try to add one
                        if (fileTypeString == null || fileTypeString.Length <= 0)
                            fileTypeString = FileTypeMappingMgr.GetInstance.addTypeMapping(fileNode.FullFilePath, (newDataFile["fileType"] as EnumTunable).ValueAsString);

                        if (fileTypeString == null || fileTypeString.Length <= 0)
                        {
                            OutputWindow.appendOutput("File does not have a type mapping! " + fileNode.FullFilePath, OutputWindow.MessageType.WARNING);
                            continue;
                        }
                    }

                    fileName = newDataFile["filename"] as StringTunable;
                    fileType = newDataFile["fileType"] as EnumTunable;
                    disabled = newDataFile["disabled"] as BoolTunable;
                    persistent = newDataFile["persistent"] as BoolTunable;
                    contents = newDataFile["contents"] as EnumTunable;

                    if (String.Compare(contents.ValueAsString, "CONTENTS_DEFAULT") == 0)
                        newDataFile.Remove("contents");

                    // 4. Remove any members we wish to omit from the file
                    foreach (string omission in m_dataFileOmmisions)
                        newDataFile.Remove(omission);

                    // 5. Brand new files get populated by us, anything existing just persists
                    if (isNewFile)
                    {
                        fileName.Value = filePathToWrite;
                        disabled.Value = true;
                        persistent.Value = m_persistentExtensions.Contains(Path.GetExtension(filePathToWrite));
                        fileType.ValueAsString = fileTypeString;
                    }

                    // 6. Add this data file as active to the actual change set data and to the cache
                    dataFiles.Items.Add(newDataFile);

                    if (!m_dataFilesData.ContainsKey(pathToWriteHashCode))
                        m_dataFilesData.Add(pathToWriteHashCode, filePathToWrite);
					else
					{
						OutputWindow.appendOutput("Duplicated filename " + 
                                filePathToWrite + " in content.xml of " + project.m_toolsProject.Name, OutputWindow.MessageType.ERROR);
					}

                    // 7. Only add internal files to the auto generated change lists, external changeSets will be merged later
                    if (fileNode.Internal && !m_addedFilesToEnable.ContainsKey(pathToWriteHashCode))
                    {
                        newString = (StringTunable)TunableFactory.Create(filesToEnable, (filesToEnable.Definition as ArrayMember).ElementType);
                        newString.Value = filePathToWrite;
                        m_addedFilesToEnable.Add(pathToWriteHashCode, filePathToWrite);

                        // 7.1 Put content locks into a separate change set
                        if (String.Compare(fileTypeString, "CONTENT_UNLOCKING_META_FILE", true) == 0)
                            unlockFilesToEnable.Add(newString);
                        else
                            filesToEnable.Add(newString);
                    }

                    OutputWindow.appendOutput("Adding data file " + filePathToWrite + " persistent [" +
                        persistent.Value.ToString() + "] disabled [" + disabled.Value.ToString() + "]", OutputWindow.MessageType.IMPORTANT);
                }

                OutputWindow.appendOutput("Sorting files", OutputWindow.MessageType.IMPORTANT);

                // Sort the data files so they are close to the order they came in
                {
                    List<ITunable> tempDataFilesArray = new List<ITunable>();

                    for (int i = 0; i < m_oldDataFilesOrder.Count; i++)
                    {
                        int currOldFilePathHash = m_oldDataFilesOrder[i];

                        // Add any files that were already referenced back in their place
                        for (int j = 0; j < dataFiles.Items.Count; j++)
                        {
                            StructureTunable currDataFile = dataFiles[j] as StructureTunable;
                            StringTunable currFileName = currDataFile["filename"] as StringTunable;

                            if (currOldFilePathHash == Globals.getStringHashCode(currFileName.Value))
                            {
                                tempDataFilesArray.Add(currDataFile);
                                break;
                            }
                        }
                    }

                    // Add any newly referenced files at the end
                    for (int j = 0; j < dataFiles.Items.Count; j++)
                    {
                        if (!tempDataFilesArray.Contains(dataFiles[j]))
                            tempDataFilesArray.Add(dataFiles[j]);
                    }

                    dataFiles.Clear();

                    // Re add the newly sorted data files
                    foreach (ITunable currTunable in tempDataFilesArray)
                        dataFiles.Add(currTunable);
                }

                // Sort the files to enable so they are close to the order they came in
                {
                    List<ITunable> tempFilesToEnableArray = new List<ITunable>();

                    // Add any files that were already referenced back in their place
                    for (int i = 0; i < m_oldFilesToEnableOrder.Count; i++)
                    {
                        int currOldFilePathHash = m_oldFilesToEnableOrder[i];

                        for (int j = 0; j < filesToEnable.Items.Count; j++)
                        {
                            StringTunable currFileName = filesToEnable[j] as StringTunable;

                            if (currOldFilePathHash == Globals.getStringHashCode(currFileName.Value))
                            {
                                tempFilesToEnableArray.Add(currFileName);
                                break;
                            }
                        }
                    }

                    // Add any newly referenced files at the end
                    for (int j = 0; j < filesToEnable.Items.Count; j++)
                    {
                        if (!tempFilesToEnableArray.Contains(filesToEnable[j]))
                            tempFilesToEnableArray.Add(filesToEnable[j]);
                    }

                    filesToEnable.Clear();

                    // Re add the newly sorted files to enable
                    foreach (ITunable currTunable in tempFilesToEnableArray)
                        filesToEnable.Add(currTunable);     
                }           
            }
            catch (System.Exception ex)
            {
                OutputWindow.appendOutput(ex.Message, OutputWindow.MessageType.ERROR);
            }
        }

        #endregion

        #region MergeProjects

        public void mergeProjects(Project project, List<string> contentsToMerge, List<contentTreeNode> filesInProject, string deviceName)
        {
            // 1. Write project data first, then attempt to merge after we have this in place
            writeProject(project, filesInProject, deviceName);

            try
            {
                StructureTunable contentData = findOrCreateStructTunableInMetaFile(m_rootTag);

                if (contentData != null)
                {
                    ArrayTunable mergeTargetChangeSets = getContentChangeSets();
                    ContentXML mergingContentXML = new ContentXML();
                    StructureTunable mergingContentData = null;
                    string deviceNameDelim = ":/";

                    // 2. Go over all the content XMLs we need to merge...
                    foreach (string mergingContentXMLPath in contentsToMerge)
                    {
                        if (!mergingContentXML.read(mergingContentXMLPath))
                            continue;

                        mergingContentData = mergingContentXML.findStructTunableInMetaFile(m_rootTag);

                        if (mergingContentData == null)
                            continue;

                        ArrayTunable mergingChangeSets = mergingContentData["contentChangeSets"] as ArrayTunable; // TOMW: should really do a deep copy here of mergingContentData

                        if (mergingChangeSets == null)
                            continue;

                        // 3. Go over all the merging change sets...
                        foreach (StructureTunable mergingChangeSet in mergingChangeSets.OfType<StructureTunable>())
                        {
                            StringTunable mergingChangeSetName = mergingChangeSet["changeSetName"] as StringTunable;
                            ArrayTunable mergingFilesToEnable = mergingChangeSet["filesToEnable"] as ArrayTunable;
                            ArrayTunable existingMergeTargetFilesToEnable = null;

                            // 4. Check to see if this change set already exists...
                            foreach (StructureTunable currMergeTargetChangeSet in mergeTargetChangeSets.OfType<StructureTunable>())
                            {
                                StringTunable mergeTargetChangeSetName = currMergeTargetChangeSet["changeSetName"] as StringTunable;

                                if (String.Compare(mergingChangeSetName.Value, mergeTargetChangeSetName.Value, true) == 0)
                                {
                                    existingMergeTargetFilesToEnable = currMergeTargetChangeSet["filesToEnable"] as ArrayTunable;

                                    // 4.1 Remove the added files to enable for this change set so that we overwrite the files later
                                    for (int i = 0; i < existingMergeTargetFilesToEnable.Length; i++)
                                    {
                                        StringTunable existingMergingFileToEnable = existingMergeTargetFilesToEnable[i] as StringTunable;

                                        if (existingMergingFileToEnable != null)
                                            m_addedFilesToEnable.Remove(Globals.getStringHashCode(existingMergingFileToEnable.Value));
                                    }

                                    break;
                                }
                            }

                            // 5. Fixup the file path and perform final checks...
                            for (int i = 0; i < mergingFilesToEnable.Length; i++)
                            {
                                StringTunable mergingFileToEnable = mergingFilesToEnable[i] as StringTunable;

                                if (mergingFileToEnable != null)
                                {
                                    string mergedFileName = mergingFileToEnable.Value;
                                    int deviceNameDelimIndex = mergedFileName.IndexOf(deviceNameDelim);

                                    mergedFileName = mergedFileName.Substring(deviceNameDelimIndex + deviceNameDelim.Length);
                                    mergedFileName = deviceName + mergedFileName;

                                    int mergedFileNameHashCode = Globals.getStringHashCode(mergedFileName);

                                    // 5.1 If this file is not present in dataFiles or if it's already present in a different changeSet, remove
                                    if (!m_dataFilesData.ContainsKey(mergedFileNameHashCode) || m_addedFilesToEnable.ContainsKey(mergedFileNameHashCode))
                                    {
                                        mergingFilesToEnable.Remove(mergingFileToEnable);
                                        i--;
                                    }
                                    else
                                    {
                                        mergingFileToEnable.Value = mergedFileName;
                                        m_addedFilesToEnable.Add(mergedFileNameHashCode, mergedFileName);
                                    }
                                }
                            }

                            // 6. If there are any merging files to enable...
                            if (mergingFilesToEnable.Length > 0)
                            {
                                //6.1 If this changeset already exists simply copy in the files from the merging one to the merge target
                                if (existingMergeTargetFilesToEnable != null)
                                    existingMergeTargetFilesToEnable.Items = mergingFilesToEnable.Items;
                                else
                                    mergeTargetChangeSets.Items.Add(mergingChangeSet);
                            }
                        }
                    }
                }
                else
                    OutputWindow.appendOutput("Unable to find or create content data!", OutputWindow.MessageType.IMPORTANT);
            }
            catch (System.Exception ex)
            {
                OutputWindow.appendOutput(ex.Message, OutputWindow.MessageType.ERROR);
            }
        }

        #endregion

        #region Gets

        public ArrayTunable getContentChangeSets()
        {
            try
            {
                StructureTunable contentData = findStructTunableInMetaFile(m_rootTag);

                if (contentData != null)
                    return contentData["contentChangeSets"] as ArrayTunable;
            }
            catch (System.Exception ex)
            {
                OutputWindow.appendOutput(ex.Message, OutputWindow.MessageType.ERROR);
            }

            return null;
        }

        #endregion
    }
}
