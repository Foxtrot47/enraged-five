﻿namespace ContentStar
{
	partial class ShippablePackSelector
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ShippablePackSelector));
			this.cbBuildFolder = new System.Windows.Forms.CheckBox();
			this.cbTUdevTemp = new System.Windows.Forms.CheckBox();
			this.cbTUdevNG = new System.Windows.Forms.CheckBox();
			this.cbPatchesFolder = new System.Windows.Forms.CheckBox();
			this.btnBuildShippablePacks = new System.Windows.Forms.Button();
			this.cbJapaneseTU = new System.Windows.Forms.CheckBox();
			this.cbDevNGLive = new System.Windows.Forms.CheckBox();
			this.cbJPN_NG_LIVE = new System.Windows.Forms.CheckBox();
			this.SuspendLayout();
			// 
			// cbBuildFolder
			// 
			this.cbBuildFolder.AutoSize = true;
			this.cbBuildFolder.Location = new System.Drawing.Point(12, 12);
			this.cbBuildFolder.Name = "cbBuildFolder";
			this.cbBuildFolder.Size = new System.Drawing.Size(86, 17);
			this.cbBuildFolder.TabIndex = 0;
			this.cbBuildFolder.Text = "Build Folders";
			this.cbBuildFolder.UseVisualStyleBackColor = true;
			this.cbBuildFolder.CheckedChanged += new System.EventHandler(this.cbDefaultCheckedChanged);
			// 
			// cbTUdevTemp
			// 
			this.cbTUdevTemp.AutoSize = true;
			this.cbTUdevTemp.Checked = true;
			this.cbTUdevTemp.CheckState = System.Windows.Forms.CheckState.Checked;
			this.cbTUdevTemp.Location = new System.Drawing.Point(12, 36);
			this.cbTUdevTemp.Name = "cbTUdevTemp";
			this.cbTUdevTemp.Size = new System.Drawing.Size(137, 17);
			this.cbTUdevTemp.TabIndex = 1;
			this.cbTUdevTemp.Text = "TitleUpdate (dev_temp)";
			this.cbTUdevTemp.UseVisualStyleBackColor = true;
			this.cbTUdevTemp.CheckedChanged += new System.EventHandler(this.cbDefaultCheckedChanged);
			// 
			// cbTUdevNG
			// 
			this.cbTUdevNG.AutoSize = true;
			this.cbTUdevNG.Location = new System.Drawing.Point(12, 59);
			this.cbTUdevNG.Name = "cbTUdevNG";
			this.cbTUdevNG.Size = new System.Drawing.Size(126, 17);
			this.cbTUdevNG.TabIndex = 2;
			this.cbTUdevNG.Text = "TitleUpdate (dev_ng)";
			this.cbTUdevNG.UseVisualStyleBackColor = true;
			this.cbTUdevNG.CheckedChanged += new System.EventHandler(this.cbDefaultCheckedChanged);
			// 
			// cbPatchesFolder
			// 
			this.cbPatchesFolder.AutoSize = true;
			this.cbPatchesFolder.Location = new System.Drawing.Point(12, 179);
			this.cbPatchesFolder.Name = "cbPatchesFolder";
			this.cbPatchesFolder.Size = new System.Drawing.Size(102, 17);
			this.cbPatchesFolder.TabIndex = 3;
			this.cbPatchesFolder.Text = "Patches Folders";
			this.cbPatchesFolder.UseVisualStyleBackColor = true;
			this.cbPatchesFolder.CheckedChanged += new System.EventHandler(this.cbDefaultCheckedChanged);
			// 
			// btnBuildShippablePacks
			// 
			this.btnBuildShippablePacks.Location = new System.Drawing.Point(12, 202);
			this.btnBuildShippablePacks.Name = "btnBuildShippablePacks";
			this.btnBuildShippablePacks.Size = new System.Drawing.Size(147, 28);
			this.btnBuildShippablePacks.TabIndex = 4;
			this.btnBuildShippablePacks.Text = "Build Packs";
			this.btnBuildShippablePacks.UseVisualStyleBackColor = true;
			this.btnBuildShippablePacks.Click += new System.EventHandler(this.btnBuildShippablePacks_Click);
			// 
			// cbJapaneseTU
			// 
			this.cbJapaneseTU.AutoSize = true;
			this.cbJapaneseTU.Location = new System.Drawing.Point(12, 82);
			this.cbJapaneseTU.Name = "cbJapaneseTU";
			this.cbJapaneseTU.Size = new System.Drawing.Size(157, 17);
			this.cbJapaneseTU.TabIndex = 5;
			this.cbJapaneseTU.Text = "TitleUpdate (dev_japanese)";
			this.cbJapaneseTU.UseVisualStyleBackColor = true;
			this.cbJapaneseTU.CheckedChanged += new System.EventHandler(this.cbJapaneseTU_CheckedChanged);
			// 
			// cbDevNGLive
			// 
			this.cbDevNGLive.AutoSize = true;
			this.cbDevNGLive.Location = new System.Drawing.Point(12, 105);
			this.cbDevNGLive.Name = "cbDevNGLive";
			this.cbDevNGLive.Size = new System.Drawing.Size(148, 17);
			this.cbDevNGLive.TabIndex = 6;
			this.cbDevNGLive.Text = "TitleUpdate (dev_ng_live)";
			this.cbDevNGLive.UseVisualStyleBackColor = true;
			this.cbDevNGLive.CheckedChanged += new System.EventHandler(this.cbDefaultCheckedChanged);
			// 
			// cbJPN_NG_LIVE
			// 
			this.cbJPN_NG_LIVE.AutoSize = true;
			this.cbJPN_NG_LIVE.Location = new System.Drawing.Point(12, 128);
			this.cbJPN_NG_LIVE.Name = "cbJPN_NG_LIVE";
			this.cbJPN_NG_LIVE.Size = new System.Drawing.Size(173, 17);
			this.cbJPN_NG_LIVE.TabIndex = 7;
			this.cbJPN_NG_LIVE.Text = "TitleUpdate (japanese_ng_live)";
			this.cbJPN_NG_LIVE.UseVisualStyleBackColor = true;
			this.cbJPN_NG_LIVE.CheckedChanged += new System.EventHandler(this.cbJapaneseTU_CheckedChanged);
			// 
			// ShippablePackSelector
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(191, 242);
			this.Controls.Add(this.cbJPN_NG_LIVE);
			this.Controls.Add(this.cbDevNGLive);
			this.Controls.Add(this.cbJapaneseTU);
			this.Controls.Add(this.btnBuildShippablePacks);
			this.Controls.Add(this.cbPatchesFolder);
			this.Controls.Add(this.cbTUdevNG);
			this.Controls.Add(this.cbTUdevTemp);
			this.Controls.Add(this.cbBuildFolder);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "ShippablePackSelector";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Build to";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.CheckBox cbBuildFolder;
		private System.Windows.Forms.CheckBox cbTUdevTemp;
		private System.Windows.Forms.CheckBox cbTUdevNG;
		private System.Windows.Forms.CheckBox cbPatchesFolder;
		private System.Windows.Forms.Button btnBuildShippablePacks;
		private System.Windows.Forms.CheckBox cbJapaneseTU;
		private System.Windows.Forms.CheckBox cbDevNGLive;
		private System.Windows.Forms.CheckBox cbJPN_NG_LIVE;
	}
}