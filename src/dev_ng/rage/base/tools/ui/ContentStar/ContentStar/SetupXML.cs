﻿/*******************************************\
| ContentStar : Rockstar Games © 2013       |
| ------------------------------------------|
| Author: Thomas Woodhead (R* London)       |
| Email: thomas.woodhead@rockstarlondon.com |
\*******************************************/

#region Usings

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Collections;
using System.Xml;
using System.Xml.Linq;
using System.Diagnostics;
using System.Threading;
using System.Xml.Serialization;
using ContentStar;
using contentStar;
using RSG.Base.Editor;
using RSG.Metadata;
using RSG.Metadata.Model;
using RSG.Metadata.Parser;
using RSG.Base.Configuration;
using RSG.Metadata.Data;

#endregion

namespace ContentStar
{
    public class SetupXML : MetaFileLoader
    {
        #region Variables

        private const string GROUP_STARTUP = "GROUP_STARTUP";
        private static string[] m_globalOmmisions = { "contentChangeSets", "dependencyPackHash", "startupScript", "scriptCallstackSize" };

        #endregion

        #region Constructor

        public SetupXML()
        {
            m_rootTag = "SSetupData";
        }

        #endregion

        #region PreSerialise

        public void preSerialise(Project project)
        {
            try
            {
                StructureTunable setupData = findOrCreateStructTunableInMetaFile(m_rootTag);

                if (setupData != null)
                {
                    ArrayTunable changeSetGroups = getContentChangeSetGroups();
                    ArrayTunable legacyChangeSets = getLegacyContentChangeSets();
                    List<string> startupChangeSetsToAdd = new List<string>();

                    // Collect any legacy change sets, we can put these into GROUP_STARTUP
                    if (legacyChangeSets != null)
                    {
                        foreach (StringTunable legacyChangeSet in legacyChangeSets.Items.OfType<StringTunable>())
                            startupChangeSetsToAdd.Add(legacyChangeSet.Value);

                        legacyChangeSets.Items.Clear();
                    }

                    if (changeSetGroups != null)
                    {
                        StructureTunable startupGroup = null;

                        // 1. Try to find the startup group...
                        foreach (StructureTunable changeSetGroup in changeSetGroups.Items.OfType<StructureTunable>())
                        {
                            StringTunable nameHash = changeSetGroup["NameHash"] as StringTunable;

                            if (nameHash != null && String.Compare(nameHash.Value, GROUP_STARTUP, true) == 0)
                            {
                                startupGroup = changeSetGroup;
                                break;
                            }
                        }

                        // 2. Create a new startup group if we couldn't find one...
                        if (startupGroup == null)
                        {
                            IMember changeSetGroupType = (changeSetGroups.Definition as ArrayMember).ElementType;
                            StructureTunable newStartupGroup = (StructureTunable)TunableFactory.Create(changeSetGroups, changeSetGroupType);
                            StringTunable groupNameHash = newStartupGroup["NameHash"] as StringTunable;

                            groupNameHash.Value = GROUP_STARTUP;
                            startupGroup = newStartupGroup;
                            changeSetGroups.Add(newStartupGroup);
                        }

                        if (startupGroup != null)
                        {
                            ArrayTunable startupChangeSets = startupGroup["ContentChangeSets"] as ArrayTunable;
                            IMember startupChangeSetType = (startupChangeSets.Definition as ArrayMember).ElementType;

                            startupChangeSetsToAdd.Add(project.m_title.ToUpper() + Globals.PROJECT_CS_SUFFIX);
                            startupChangeSetsToAdd.Add(project.m_title.ToUpper() + Globals.UNLOCKS_CS_SUFFIX);

                            // 3. Add any startup change sets that we have collected, if they don't already exist...
                            foreach (string startupChangeSetToAdd in startupChangeSetsToAdd)
                            {
                                bool startupChangeSetExists = false;

                                foreach (StringTunable startupChangeSet in startupChangeSets.Items.OfType<StringTunable>())
                                {
                                    if (String.Compare(startupChangeSet.Value, startupChangeSetToAdd, true) == 0)
                                    {
                                        startupChangeSetExists = true;
                                        break;
                                    }
                                }

                                if (!startupChangeSetExists)
                                {
                                    StringTunable newStartupChangeSet = (StringTunable)TunableFactory.Create(startupChangeSets, startupChangeSetType);

                                    newStartupChangeSet.Value = startupChangeSetToAdd;
                                    startupChangeSets.Items.Add(newStartupChangeSet);
                                }
                            }
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                OutputWindow.appendOutput(ex.Message, OutputWindow.MessageType.ERROR);
            }
        }

        #endregion

        #region WriteProject

        public void modifySetupXML(Project project)
        {
            StructureTunable setupData = findOrCreateStructTunableInMetaFile(m_rootTag);

            if (setupData == null)
                return;

            S32Tunable subPackCount = setupData.ContainsKey("subPackCount") ? setupData["subPackCount"] as S32Tunable : null;
            if (project.m_subPackCount > 0)
            {
                try
                {
                    subPackCount.Value = project.m_subPackCount;
                }
                catch (System.Exception ex)
                {
                    OutputWindow.appendOutput(ex.Message + "Parser file not setup correctly, try updating //depot/gta5/assets_ng/metadata/definitions/game/scene/extracontent.psc", OutputWindow.MessageType.ERROR);
                }
            }
        }

        public void writeProject(Project project, string deviceName)
        {
            try
            {
                StructureTunable setupData = findOrCreateStructTunableInMetaFile(m_rootTag);

                if (setupData != null)
                {
                    StringTunable deviceNameTunable = setupData.ContainsKey("deviceName") ? setupData["deviceName"] as StringTunable : null;
                    StringTunable nameHash = setupData.ContainsKey("nameHash") ? setupData["nameHash"] as StringTunable : null;
                    StringTunable datFile = setupData.ContainsKey("datFile") ? setupData["datFile"] as StringTunable : null;
                    StringTunable timeStamp = setupData.ContainsKey("timeStamp") ? setupData["timeStamp"] as StringTunable : null;
                    S32Tunable subPackCount = setupData.ContainsKey("subPackCount") ? setupData["subPackCount"] as S32Tunable : null;
                    EnumTunable type = setupData.ContainsKey("type") ? setupData["type"] as EnumTunable : null;

                    // 1. Remove any tags we don't want for compat and paidPacks...
                    foreach (string ommision in m_globalOmmisions)
                        setupData.Remove(ommision);

                    // We don't build the TU here, default to paid
                    if (String.Compare(type.ValueAsString, "EXTRACONTENT_TU_PACK", true) == 0)
                    {
                        type.ValueAsString = "EXTRACONTENT_PAID_PACK";
                        setupData.Remove("order");
                    }
                    else if (String.Compare(type.ValueAsString, "EXTRACONTENT_PAID_PACK", true) == 0)
                    {
                        setupData.Remove("order");
                    }

                    // 2. Fill in any variables that are missing...
                    if (deviceNameTunable != null)
                        deviceNameTunable.Value = deviceName.Replace(":/", "");

                    if (datFile != null && (datFile.Value == null || datFile.Value.Length <= 0))
                        datFile.Value = Globals.CONTENT_XML;

                    if (nameHash != null && (nameHash.Value == null || nameHash.Value.Length <= 0))
                        nameHash.Value = project.m_title;

                    if (timeStamp != null)
                        timeStamp.Value = DateTime.Now.ToString();

                    if (subPackCount != null)
                        subPackCount.Value = project.m_subPackCount;

                    OutputWindow.appendOutput("Writing " + m_filePath + " deviceName [" + deviceNameTunable.Value + "] nameHash [" + nameHash.Value + "]", OutputWindow.MessageType.IMPORTANT);
                }
                else
                    OutputWindow.appendOutput("Unable to find or create setup data!", OutputWindow.MessageType.IMPORTANT);
            }
            catch (System.Exception ex)
            {
                OutputWindow.appendOutput(ex.Message, OutputWindow.MessageType.ERROR);
            }
        }

        #endregion

        #region MergeProjects

        public void mergeProjects(Project project, List<string> setupsToMerge, ArrayTunable includedChangeSets, string deviceName)
        {
            // 1. Write the current project data, so we can merge the other setups into this...
            writeProject(project, deviceName);

            try
            {
                SetupXML mergingSetupXML = new SetupXML();
                ArrayTunable mergeTargetChangeSetGroups = getContentChangeSetGroups();

                // 2. Go over all the setups we need to merge...
                foreach (string setupToMerge in setupsToMerge)
                {
                    if (!mergingSetupXML.read(setupToMerge))
                        continue;

                    OutputWindow.appendOutput("Merging setup data from " + setupToMerge, OutputWindow.MessageType.IMPORTANT);

                    ArrayTunable mergingChangeSetGroups = mergingSetupXML.getContentChangeSetGroups();

                    if (mergingChangeSetGroups == null)
                        continue;

                    // 3. Go over all the merging change set groups...
                    foreach (StructureTunable mergingChangeSetGroup in mergingChangeSetGroups.Items.OfType<StructureTunable>())
                    {
                        bool needToAddGroup = true;
                        StringTunable mergingNameHash = mergingChangeSetGroup["NameHash"] as StringTunable;
                        ArrayTunable mergingCCS = mergingChangeSetGroup["ContentChangeSets"] as ArrayTunable;

                        // 4. Does the merging changeset group already exist in the merge target?
                        foreach (StructureTunable mergeTargetCCSGroup in mergeTargetChangeSetGroups.Items.OfType<StructureTunable>())
                        {
                            StringTunable mergeTargetNameHash = mergeTargetCCSGroup["NameHash"] as StringTunable;
                            ArrayTunable mergeTargetCCS = mergeTargetCCSGroup["ContentChangeSets"] as ArrayTunable;

                            if (mergingNameHash.Value == mergeTargetNameHash.Value)
                            {
                                needToAddGroup = false;

                                // 4.1 Merge in any change sets that aren't already included and are not ommitted...
                                foreach (StringTunable mergingCCSName in mergingCCS.Items.OfType<StringTunable>())
                                {
                                    if (!getHasStringValue(mergeTargetCCS, mergingCCSName) && getIsChangeSetIncluded(includedChangeSets, mergingCCSName))
                                        mergeTargetCCS.Add(mergingCCSName);
                                }
                            }
                        }

                        // 5. Add a new group if required...
                        if (needToAddGroup)
                        {
                            IMember ccsGroupType = (mergeTargetChangeSetGroups.Definition as ArrayMember).ElementType;
                            StructureTunable newCCSGroup = (StructureTunable)TunableFactory.Create(mergeTargetChangeSetGroups, ccsGroupType);
                            StringTunable groupNameHash = newCCSGroup["NameHash"] as StringTunable;
                            ArrayTunable changeSets = newCCSGroup["ContentChangeSets"] as ArrayTunable;

                            groupNameHash.Value = mergingNameHash.Value;

                            // 5.1 Add any change sets to the new group that are meant to be included...
                            foreach (StringTunable mergingCCSName in mergingCCS.Items.OfType<StringTunable>())
                            {
                                if (getIsChangeSetIncluded(includedChangeSets, mergingCCSName))
                                    changeSets.Add(mergingCCSName);
                            }

                            mergeTargetChangeSetGroups.Add(newCCSGroup);
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                OutputWindow.appendOutput(ex.Message, OutputWindow.MessageType.ERROR);
            }
        }

        #endregion

        #region Gets

        private bool getIsChangeSetIncluded(ArrayTunable changeSets, StringTunable changeSetToCheck)
        {
            foreach (StructureTunable changeSet in changeSets.OfType<StructureTunable>())
            {
                StringTunable changeSetName = changeSet["changeSetName"] as StringTunable;

                if (String.Compare(changeSetName.Value, changeSetToCheck.Value, true) == 0)
                    return true;
            }

            return false;
        }

        private bool getHasStringValue(ArrayTunable stringArray, StringTunable stringToCheck)
        {
            foreach (StringTunable stringTunable in stringArray.OfType<StringTunable>())
            {
                if (String.Compare(stringTunable.Value, stringToCheck.Value, true) == 0)
                    return true;
            }

            return false;
        }

        public string getDeviceName(Project project)
        {
            string devicePath = Globals.prefixDeviceName(project.m_title);
            StructureTunable setupData = findOrCreateStructTunableInMetaFile(m_rootTag);

            if (setupData != null)
            {
                StringTunable deviceName = setupData["deviceName"] as StringTunable;

                if (deviceName != null && (deviceName.Value != null && deviceName.Value.Length > 0))
                    devicePath = deviceName.Value;
            }

            return devicePath + ":/";
        }

        public ArrayTunable getContentChangeSetGroups()
        {
            try
            {
                StructureTunable setupData = findStructTunableInMetaFile(m_rootTag);

                if (setupData != null)
                    return setupData["contentChangeSetGroups"] as ArrayTunable;
            }
            catch (System.Exception ex)
            {
                OutputWindow.appendOutput(ex.Message, OutputWindow.MessageType.ERROR);
            }

            return null;
        }

        public ArrayTunable getLegacyContentChangeSets()
        {
            try
            {
                StructureTunable setupData = findStructTunableInMetaFile(m_rootTag);

                if (setupData != null)
                    return setupData["contentChangeSets"] as ArrayTunable;
            }
            catch (System.Exception ex)
            {
                OutputWindow.appendOutput(ex.Message, OutputWindow.MessageType.ERROR);
            }

            return null;
        }

        #endregion
    }
}
