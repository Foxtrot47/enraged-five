﻿/*******************************************\
| ContentStar : Rockstar Games © 2013       |
| ------------------------------------------|
| Author: Thomas Woodhead (R* London)       |
| Email: thomas.woodhead@rockstarlondon.com |
\*******************************************/

#region Usings

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Collections;
using System.Xml;
using System.Xml.Linq;
using System.Diagnostics;
using System.Threading;
using System.Xml.Serialization;
using ContentStar;
using contentStar;

#endregion

namespace ContentStar
{
    // Manages the output window, output is separated into filterable channels
    public class OutputWindow
    {
        #region Magic

        // Magic for imitating BeginUpdate() and EndUpdate() on TextBoxes
        [System.Runtime.InteropServices.DllImport("user32.dll")]
        public static extern bool LockWindowUpdate(IntPtr hWndLock);

        #endregion

        #region OutputMessage

        public enum MessageType { NORMAL, WARNING, ERROR, IMPORTANT }

        public class OutputMessage
        {
            public string m_message;
            public MessageType m_type;

            public OutputMessage(string message, MessageType msgType)
            {
                m_message = message;
                m_type = msgType;
            }
        }

        #endregion

        #region Channel

        // Stores messages for a particular channel
        private class Channel
        {
            private int m_Id;
            private string m_name;
            private List<OutputMessage> m_messages = new List<OutputMessage>();

            public void addMessage(string message, MessageType msgType)
            {
                m_messages.Add(new OutputMessage(message, msgType));
            }

            public void clear()
            {
                m_messages.Clear();
            }

            public void scrollToEnd(RichTextBox outputWindow)
            {
                outputWindow.SelectionStart = m_rtbOutput.TextLength;
                outputWindow.ScrollToCaret();
                outputWindow.Refresh();
            }

            public void appendLastMessage(RichTextBox outputWindow)
            {
                if (m_messages.Count > 0)
                {
                    writeToWindow(outputWindow, m_messages[m_messages.Count - 1]);
                    scrollToEnd(outputWindow);
                }
            }

            public void displayMessages(RichTextBox outputWindow)
            {
                try
                {
                    LockWindowUpdate(outputWindow.Handle);

                    outputWindow.Clear();

                    foreach (OutputMessage msg in m_messages)
                        writeToWindow(outputWindow, msg);

                    scrollToEnd(outputWindow);
                }
                finally
                {
                    LockWindowUpdate(IntPtr.Zero);
                }
            }

            private void writeToWindow(RichTextBox outputWindow, OutputMessage msg)
            {
                Font msgFont = Globals.OUTPUT_REGULAR_FONT;

                outputWindow.SelectionStart = outputWindow.TextLength;
                outputWindow.SelectionLength = 0;

                switch (msg.m_type)
                {
                    case MessageType.NORMAL:
                    {
                        outputWindow.SelectionColor = Color.Black;
                    }
                    break;

                    case MessageType.WARNING:
                    {
                        outputWindow.SelectionColor = Color.Yellow;
                    }
                    break;

                    case MessageType.ERROR:
                    {
                        outputWindow.SelectionColor = Color.Crimson;
                        msgFont = Globals.OUTPUT_BOLD_FONT;
                    }
                    break;

                    case MessageType.IMPORTANT:
                    {
                        outputWindow.SelectionColor = Color.Green;
                        msgFont = Globals.OUTPUT_BOLD_FONT;
                    }
                    break;
                }

                outputWindow.SelectionFont = msgFont;
                outputWindow.AppendText("<" + m_name + "> <" + DateTime.Now.ToShortTimeString() + "> " + msg.m_message + "\n");
                outputWindow.SelectionColor = outputWindow.ForeColor;
            }

            public override string ToString()
            {
                return m_name;
            }

            public List<OutputMessage> Messages
            {
                get { return m_messages; }
            }

            public int Id
            {
                get { return m_Id; }
                set { m_Id = value; }
            }

            public string Name
            {
                get { return m_name; }
                set { m_name = value; }
            }
        }

        #endregion

        #region OutputWindow Management

        private static uint m_totalChannelCount = 0;
        private static int m_activeChannel = -1;
        private static int m_masterChannel = -1;
        private static int m_selectedChannel = -1;
        private static bool m_recordingFails = false;
        private static List<string> m_recordedWarnings = new List<string>();
        private static List<string> m_recordedErrors = new List<string>();
        private static RichTextBox m_rtbOutput = null;
        private static ComboBox m_cboChannelList = null;
        private static List<OutputMessage> m_bufferedMessages = new List<OutputMessage>();
        private static Dictionary<int, Channel> m_channels = new Dictionary<int, Channel>();

        // Create the class and add the master channel "All"
        public static void initialise(RichTextBox textBox, ComboBox cboChannelList)
        {
            m_rtbOutput = textBox;
            m_cboChannelList = cboChannelList;

            reset();
        }

        // Reset, but preserve the master channel data
        public static void reset()
        {
            Channel masterChannel = null;
            OutputMessage[] tempArray = null;

            m_recordingFails = false;
            m_recordedErrors.Clear();
            m_recordedWarnings.Clear();

            m_channels.TryGetValue(m_masterChannel, out masterChannel);

            if (masterChannel != null)
            {
                tempArray = new OutputMessage[masterChannel.Messages.Count];
                masterChannel.Messages.CopyTo(tempArray);
            }

            m_rtbOutput.Clear();
            m_channels.Clear();
            m_cboChannelList.Items.Clear();
            m_masterChannel = addChannel("All");

            m_channels.TryGetValue(m_masterChannel, out masterChannel);

            if (masterChannel != null && tempArray != null)
            {
                foreach (OutputMessage msg in tempArray)
                    masterChannel.addMessage(msg.m_message, msg.m_type);
            }

            selectChannel(m_masterChannel);
        }

        public static void startRecording()
        {
            m_recordingFails = true;
            m_recordedErrors.Clear();
            m_recordedWarnings.Clear();
        }

        public static void stopRecording()
        {
            m_recordingFails = false;

            foreach (string message in m_recordedWarnings)
                appendOutput(message, MessageType.WARNING);

            foreach (string message in m_recordedErrors)
                appendOutput(message, MessageType.ERROR, false);

            appendOutput("Recorded " + m_recordedErrors.Count + " error(s)", MessageType.ERROR, false);
            appendOutput("Recorded " + m_recordedWarnings.Count + " warning(s)", MessageType.WARNING);

            m_recordedErrors.Clear();
            m_recordedWarnings.Clear();
        }

        public static int addChannel(string name)
        {
            Channel newChannel = new Channel();
            string strId = "newChannel" + m_totalChannelCount;
            int id = Globals.getStringHashCode(strId);

            newChannel.Name = name;
            newChannel.Id = id;
            m_channels.Add(id, newChannel);
            m_totalChannelCount++;
            populateChannelList();

            return id;
        }

        public static void removeChannel(int id)
        {
            m_channels.Remove(id);
            populateChannelList();
        }

        public static void pushActiveChannel(int id)
        {
            m_activeChannel = id;
        }

        delegate void appendLastMessageDelegate(RichTextBox rtb);

        public static void appendOutput(string inMessage, MessageType errorLevel, bool showStackFrame=true)
        {
            Channel targetChannel = null;
            string message = inMessage;

            switch (errorLevel)
            {
                case MessageType.NORMAL:
                {
                    try { RSG.Base.Logging.Log.Log__Message(message); }
                    catch (System.Exception /*ex*/) { }
                }
                break;

                case MessageType.WARNING:
                {
                    if (m_recordingFails)
                        m_recordedWarnings.Add(message);

                    try { RSG.Base.Logging.Log.Log__Warning(message); }
                    catch (System.Exception /*ex*/) { }
                }
                break;

                case MessageType.ERROR:
                {
                    if (showStackFrame)
                    {
                        StackFrame currSF = new StackFrame(1);
                        message = "[" + currSF.GetMethod().Name + "] " + inMessage;
                    }

                    if (m_recordingFails)
                        m_recordedErrors.Add(message);

                    try { RSG.Base.Logging.Log.Log__Error(message); }
                    catch (System.Exception /*ex*/) { }
                }
                break;

                case MessageType.IMPORTANT:
                {
                    try { RSG.Base.Logging.Log.Log__Message(message); }
                    catch (System.Exception /*ex*/) { }
                }
                break;
            }

            RSG.Base.Logging.LogFactory.FlushApplicationLog();

            m_channels.TryGetValue(m_masterChannel, out targetChannel);

            if (targetChannel != null)
                targetChannel.addMessage(message, errorLevel);

            if (m_activeChannel != -1)
            {
                m_channels.TryGetValue(m_activeChannel, out targetChannel);

                if (targetChannel != null)
                    targetChannel.addMessage(message, errorLevel);
            }

            Channel selectedChannel = null;

            m_channels.TryGetValue(m_selectedChannel, out selectedChannel);

            if (selectedChannel != null)
            {
                if (Program.getIsInvokedRequired())
                {
                    appendLastMessageDelegate appendLast = new appendLastMessageDelegate(selectedChannel.appendLastMessage);

                    Program.doInvoke(appendLast, m_rtbOutput);
                }
                else
                    selectedChannel.appendLastMessage(m_rtbOutput);
            }
        }

        public static void popActiveChannel()
        {
            m_activeChannel = -1;
        }

        public static void renameChannel(int id, string newName)
        {
            Channel selectedChannel = null;

            m_channels.TryGetValue(id, out selectedChannel);

            if (selectedChannel != null)
                selectedChannel.Name = newName;

            populateChannelList();
        }

        private static void refreshDisplay()
        {
            Channel selectedChannel = null;

            m_channels.TryGetValue(m_selectedChannel, out selectedChannel);

            if (selectedChannel != null)
                selectedChannel.displayMessages(m_rtbOutput);
        }

        private static void selectChannel(int channelId)
        {
            int index = 0;
            var chanIter = m_channels.GetEnumerator();

            while (chanIter.MoveNext())
            {
                if (chanIter.Current.Key == channelId)
                {
                    m_cboChannelList.SelectedIndex = index;
                    break;
                }

                index++;
            }

            m_selectedChannel = channelId;
            refreshDisplay();
        }

        public static void clearCurrent()
        {
            Channel selectedChannel = null;

            m_channels.TryGetValue(m_selectedChannel, out selectedChannel);

            if (selectedChannel != null)
                selectedChannel.clear();

            refreshDisplay();
        }

        public static void clearAll()
        {
            var chanIter = m_channels.GetEnumerator();

            while (chanIter.MoveNext())
                chanIter.Current.Value.clear();
            
            refreshDisplay();
        }

        public static void populateChannelList()
        {
            m_cboChannelList.Items.Clear();

            var chanIter = m_channels.GetEnumerator();

            while (chanIter.MoveNext())
                m_cboChannelList.Items.Add(chanIter.Current.Value);

            selectChannel(m_selectedChannel);
        }

        public static void filterLog()
        {
            Channel selectedChannel = (Channel)m_cboChannelList.SelectedItem;

            selectChannel(selectedChannel.Id);
        }

        public static void addBufferedMessage(string message, MessageType msgType)
        {
            m_bufferedMessages.Add(new OutputMessage(message, msgType));
        }

        public static void flushBufferedMessages()
        {
            foreach (OutputMessage msg in m_bufferedMessages)
                appendOutput(msg.m_message, msg.m_type);

            m_bufferedMessages.Clear();
        }

        #endregion
    }
}
