﻿/*******************************************\
| ContentStar : Rockstar Games © 2013       |
| ------------------------------------------|
| Author: Thomas Woodhead (R* London)       |
| Email: thomas.woodhead@rockstarlondon.com |
\*******************************************/

#region Usings

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Collections;
using System.Xml;
using System.Xml.Linq;
using System.Diagnostics;
using System.Threading;
using System.Xml.Serialization;
using ContentStar;
using contentStar;
using RSG.Metadata.Parser;
using RSG.Metadata.Model;

#endregion

namespace ContentStar
{
    [Serializable]
    [XmlRoot("FileFilters")]
    public class FileFilters
    {
        [XmlArray("MetaTagFilters")]
        [XmlArrayItem("MetaTag", typeof(string), IsNullable = false)]
        public List<string> m_metaTagFilters = new List<string>();

        [XmlArray("FilenameFilters")]
        [XmlArrayItem("Filename", typeof(string), IsNullable = false)]
        public List<string> m_fileNameFilters = new List<string>();

        public bool isMetaTagFiltered(string metaTag)
        {
            return m_metaTagFilters.Contains(metaTag);
        }

        public bool isFileNameFiltered(string fileName)
        {
            foreach (string fileNameFilter in m_fileNameFilters)
            {
                if (fileName.Contains(fileNameFilter))
                    return true;
            }

            return false;
        }

        public static FileFilters readFiltersFile(string filePath)
        {            
            try
            {
                if (File.Exists(filePath))
                {
                    OutputWindow.appendOutput("Reading " + filePath, OutputWindow.MessageType.IMPORTANT);

                    FileFilters newFilters = new FileFilters();
                    XmlSerializer filtersDeserialiser = new XmlSerializer(typeof(FileFilters));
                    using (TextReader readStream = new StreamReader(filePath))
                    {
                        newFilters = (FileFilters)filtersDeserialiser.Deserialize(readStream);
                        readStream.Close();
                    }

                    return newFilters;
                }
            }
            catch (Exception ex)
            {
                OutputWindow.appendOutput(ex.Message, OutputWindow.MessageType.ERROR);
            }

            return null;
        }
    }
}
