﻿/*******************************************\
| ContentStar : Rockstar Games © 2013       |
| ------------------------------------------|
| Author: Thomas Woodhead (R* London)       |
| Email: thomas.woodhead@rockstarlondon.com |
\*******************************************/

#region Usings

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Collections;
using System.Xml;
using System.Xml.Linq;
using System.Diagnostics;
using System.Xml.Serialization;
using ContentStar;
using System.Globalization;
using RSG.Base.Editor;
using RSG.Metadata;
using RSG.Metadata.Model;
using RSG.Metadata.Parser;
using RSG.Base.Configuration;
using RSG.Metadata.Data;
using RSG.Platform;
using System.Threading;
using Shell32;
using RSG.Base.Logging.Universal;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Drawing.Imaging;

#endregion

namespace contentStar
{
    public partial class frmMain : Form
    {
        #region Variables

        string m_lastSelectedBank = "";
        frmOptions m_optionsForm = null;
        frmProgress m_progressForm = null;
        List<contentTreeNode> m_projectsACList = new List<contentTreeNode>();
        List<contentTreeNode> m_foldersACList = new List<contentTreeNode>();
        List<contentTreeNode> m_filesACList = new List<contentTreeNode>();
        Dictionary<int, Project> m_projectCollisions = new Dictionary<int, Project>();
        NodeSorter m_nodeSorter = new NodeSorter();
        Process m_logProcess = null;
        contentTreeNode m_currSelectedNode = null; // Bit of a hack for historical reasons, wouldn't do this again, live and learn.
        Thread m_thread = null;

        #endregion

		#region ShippablePacks
		ShippablePackSelector m_shippablePacksForm = new ShippablePackSelector();
		#endregion

		#region Constructor

		public frmMain()
        {
            InitializeComponent();

            FontSwitcher(txtGlobalFieldTitleID, Globals.COUR_NEW);
            FontSwitcher(txtOfferId, Globals.COUR_NEW);
            FontSwitcher(txtPrevOfferId, Globals.COUR_NEW);
            FontSwitcher(txtContentCat, Globals.COUR_NEW);
            FontSwitcher(txtLicenseMask, Globals.COUR_NEW);
            FontSwitcher(txtXboxPubFlags, Globals.COUR_NEW);
            FontSwitcher(txtXboxTitleID, Globals.COUR_NEW);
            FontSwitcher(txtXboxOffID, Globals.COUR_NEW);
            FontSwitcher(txtPS4Code, Globals.COUR_NEW);
            FontSwitcher(txtPS4VolType, Globals.COUR_NEW);
            FontSwitcher(txtCurrGlobalBank, Globals.COUR_NEW);

            m_progressForm = new frmProgress(this);
        }

        private void FontSwitcher(Control control, Font newFont)
        {
            if (control.Font != null)
                control.Font.Dispose();

            control.Font = newFont;
        }

        public bool Initialise()
        {
            try
            {
                OutputWindow.initialise(rtbOutput, cboLogFilter);
                Globals.initialise();
                ProjectManager.initialise();
                SettingsManager.initialise();
                GlobalFieldsManager.initialise();
                FileTypeMappingMgr.initialise();
                CommandPrompt.initialise();
            }
            catch (System.Exception ex)
            {
                MessageBox.Show("Failed to initialise core modules, your tools version may be out of date.\n\nFull message:\n" + ex.Message, 
                    "It's not you, it's me...", MessageBoxButtons.OK, MessageBoxIcon.Error);

                return false;
            }

            return true;
        }

        public void Shutdown()
        {
            try
            {
                CommandPrompt.shutdown();
            }
            catch (System.Exception ex)
            {
                MessageBox.Show("Failed to shutdown correctly!\n\nFull message:\n" + ex.Message,
                    "It's not you, it's me...", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        #endregion

        #region Build process

        #region External process interface

        private void runAndDone(string exePath, string arguments, bool slashReplace, StreamWriter logTarget, bool printLog)
        {
            try
            {
                using (Process executable = new Process())
                {
                    executable.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                    executable.StartInfo.CreateNoWindow = true;
                    executable.StartInfo.FileName = exePath;
                    executable.StartInfo.UseShellExecute = false;
                    executable.StartInfo.RedirectStandardInput = true;
                    executable.StartInfo.RedirectStandardOutput = true;
                    executable.StartInfo.RedirectStandardError = true;
                    executable.StartInfo.WorkingDirectory = Path.GetDirectoryName(exePath);

                    if (slashReplace)
                        executable.StartInfo.Arguments = arguments.Replace("\\", "/");
                    else
                        executable.StartInfo.Arguments = arguments;

                    executable.Start();
                    executable.StandardInput.Close();

                    if (printLog)
                        OutputWindow.appendOutput("[" + executable.ProcessName + "] " + arguments, OutputWindow.MessageType.IMPORTANT);

                    StringBuilder sbOutput = new StringBuilder();
                    StringBuilder sbError = new StringBuilder();

                    using (AutoResetEvent outputWaitHandle = new AutoResetEvent(false))
                    using (AutoResetEvent errorWaitHandle = new AutoResetEvent(false))
                    {
                        // When some data arrives asynchronously, append it to the string builders 
                        executable.OutputDataReceived += (sender, e) =>
                        {
                            if (e.Data == null)
                                outputWaitHandle.Set();
                            else
                                sbOutput.AppendLine(e.Data);
                        };

                        executable.ErrorDataReceived += (sender, e) =>
                        {
                            if (e.Data == null)
                                errorWaitHandle.Set();
                            else
                                sbError.AppendLine(e.Data);
                        };

                        // Being async read
                        executable.BeginOutputReadLine();
                        executable.BeginErrorReadLine();

                        // Wait for output and error reads to finish and then wait for process to exit
                        outputWaitHandle.WaitOne();
                        errorWaitHandle.WaitOne();
                        executable.WaitForExit();

                        // Display output if required
                        string stdOutput = sbOutput.ToString();

                        if (stdOutput.Length > 0)
                        {
                            if (logTarget != null)
                            {
                                logTarget.Write(stdOutput);
                                logTarget.WriteLine("");
                                logTarget.WriteLine("");
                            }

                            OutputWindow.appendOutput(stdOutput, OutputWindow.MessageType.NORMAL);
							ExtractXbonePackageName(stdOutput);
							
                        }

                        string errOutput = sbError.ToString();

                        if (!errOutput.Contains("%\b\b\b\b\b") && errOutput.Length > 0)
                        {
                            if (logTarget != null)
                            {
                                logTarget.Write(errOutput);
                                logTarget.WriteLine("");
                                logTarget.WriteLine("");
                            }

                            OutputWindow.appendOutput(errOutput, OutputWindow.MessageType.ERROR);
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                OutputWindow.appendOutput(ex.Message, OutputWindow.MessageType.ERROR);
            }
        }

        #endregion

        #region Build Utils

        private void createProjectLogFile(Project thisProject)
        {
            string cgLogPath = thisProject.OutputDirectory + "\\" + thisProject.m_title + "_cg.txt";
            string ngLogPath = thisProject.OutputDirectory + "\\" + thisProject.m_title + "_ng.txt";

            CommandPrompt.perforceCheckout(cgLogPath);
            CommandPrompt.perforceCheckout(ngLogPath);

            // Write current gen data
            using (TextWriter writeStream = new StreamWriter(cgLogPath))
            {
                try
                {
                    StringBuilder logHeader = new StringBuilder(1024 * 100);
                    GlobalFields gfBank = GlobalFieldsManager.GetInstance.getGlobalFieldsFromBank(thisProject.m_globalBankName);
                    List<contentTreeNode> contentNodes = new List<contentTreeNode>();

                    extractContent(thisProject, null, thisProject.getBranchRootNode(Platform.Xbox360), contentNodes, false);

                    logHeader.Append("# Pack Information");
                    logHeader.AppendLine();
                    logHeader.AppendLine();
                    logHeader.Append("Name: ");
                    logHeader.Append(thisProject.m_title);
                    logHeader.AppendLine();
                    logHeader.Append("Built: ");
                    logHeader.Append(DateTime.Now.ToLongDateString());
                    logHeader.Append(" ");
                    logHeader.Append(DateTime.Now.ToShortTimeString());
                    logHeader.AppendLine();

                    logHeader.AppendLine();
                    logHeader.Append("# XBOX 360");
                    logHeader.AppendLine();
                    logHeader.AppendLine();
                    thisProject.m_xboxData.buildString(logHeader, gfBank);

                    logHeader.AppendLine();
                    logHeader.Append("# PS3");
                    logHeader.AppendLine();
                    logHeader.AppendLine();
                    thisProject.m_ps3Data.buildString(logHeader, gfBank);

                    logHeader.AppendLine();
                    logHeader.Append("# Files");
                    logHeader.AppendLine();
                    logHeader.AppendLine();

                    int longestStr = 0;

                    foreach (contentTreeNode currNode in contentNodes)
                    {
                        int currLength = currNode.ProjectRelativeFilePath.Length;

                        if (longestStr < currLength)
                            longestStr = currLength;
                    }

                    foreach (contentTreeNode currNode in contentNodes)
                    {
                        FileInfo fileInfo = new FileInfo(currNode.FullFilePath);

                        logHeader.Append(currNode.ProjectRelativeFilePath);
                        for (int i = currNode.ProjectRelativeFilePath.Length; i < longestStr; i++)
                        {
                            logHeader.Append(" ");
                        }
                        logHeader.Append(" Size: ");
                        logHeader.AppendLine(fileInfo.Length.ToString());
                    }

                    writeStream.Write(logHeader);

                    writeStream.Close();
                }
                catch (System.Exception ex)
                {
                    OutputWindow.appendOutput(ex.Message, OutputWindow.MessageType.ERROR);
                }
            }

            // Write next gen data
            using (TextWriter writeStream = new StreamWriter(ngLogPath))
            {
                try
                {
                    StringBuilder logHeader = new StringBuilder(1024 * 100);
                    GlobalFields gfBank = GlobalFieldsManager.GetInstance.getGlobalFieldsFromBank(thisProject.m_globalBankName);
                    List<contentTreeNode> contentNodes = new List<contentTreeNode>();

                    extractContent(thisProject, null, thisProject.getBranchRootNode(Platform.PS4), contentNodes, false);

                    logHeader.Append("# Pack Information");
                    logHeader.AppendLine();
                    logHeader.AppendLine();
                    logHeader.Append("Name: ");
                    logHeader.Append(thisProject.m_title);
                    logHeader.AppendLine();
                    logHeader.Append("Built: ");
                    logHeader.Append(DateTime.Now.ToLongDateString());
                    logHeader.Append(" ");
                    logHeader.Append(DateTime.Now.ToShortTimeString());
                    logHeader.AppendLine();

                    logHeader.AppendLine();
                    logHeader.Append("# PS4");
                    logHeader.AppendLine();
                    logHeader.AppendLine();
                    thisProject.m_ps4Data.buildString(logHeader, gfBank);

                    logHeader.AppendLine();
                    logHeader.Append("# XBOX ONE");
                    logHeader.AppendLine();
                    logHeader.AppendLine();
                    thisProject.m_xboneData.buildString(logHeader, gfBank);

                    logHeader.AppendLine();
                    logHeader.Append("# Files");
                    logHeader.AppendLine();
                    logHeader.AppendLine();

                    int longestStr = 0;

                    foreach (contentTreeNode currNode in contentNodes)
                    {
                        int currLength = currNode.ProjectRelativeFilePath.Length;

                        if (longestStr < currLength)
                            longestStr = currLength;
                    }

                    foreach (contentTreeNode currNode in contentNodes)
                    {
                        FileInfo fileInfo = new FileInfo(currNode.FullFilePath);

                        logHeader.Append(currNode.ProjectRelativeFilePath);
                        for (int i = currNode.ProjectRelativeFilePath.Length; i < longestStr; i++)
                        {
                            logHeader.Append(" ");
                        }
                        logHeader.Append(" Size: ");
                        logHeader.AppendLine(fileInfo.Length.ToString());
                    }

                    writeStream.Write(logHeader);

                    writeStream.Close();
                }
                catch (System.Exception ex)
                {
                    OutputWindow.appendOutput(ex.Message, OutputWindow.MessageType.ERROR);
                }
            }
        }

        private void createContentRPF(string rpfScriptLocation, string outputLocation, Project thisProject, Platform targetPlatform, List<contentTreeNode> nodeList = null, string replacementName = null)
        {
            // Ensure branch exists for selected platform.
            if (thisProject.getBranchRootNode(targetPlatform) == null)
                return;

            if (nodeList == null)
            {
                // Old functionality for guaranteed(TM) backwards compatibility.
                using (StreamWriter rbsScriptStream = new StreamWriter(rpfScriptLocation))
                {
                    string platform = BuildManager.GetRbsPlatformName(targetPlatform);
                    rbsScriptStream.WriteLine("set_platform( \"" + platform + "\" )");
                    rbsScriptStream.WriteLine("start_pack()");

                    tvContentTree.BeginUpdate();
                    setSelectedNode(thisProject.getBranchRootNode(targetPlatform));
                    copyFilesToRpf(thisProject.getBranchRootNode(targetPlatform), "/", rbsScriptStream, targetPlatform, replacementName);
                    tvContentTree.EndUpdate();

                    rbsScriptStream.WriteLine("save_pack(" + Globals.QUOTE + outputLocation.Replace("\\", "/") + Globals.QUOTE + ")");
                    rbsScriptStream.WriteLine("close_pack()");

                    rbsScriptStream.Close();
                }
                runAndDone(BuildManager.getRageBuilderPath(targetPlatform), rpfScriptLocation, false, thisProject.m_logStream, true);
            }
            else
            {
                m_progressForm.ResetProgress(nodeList.Count, "Building " + thisProject.m_title + " " + targetPlatform.ToString(), false);
                m_progressForm.ShowProgress();

                int currentIndex = 0;
                for (int i = 0; currentIndex < nodeList.Count || m_progressForm.GetCancel(); ++i)
                {
                    string rpfChunk = i > 0 ? Path.GetDirectoryName(outputLocation) + "\\dlc" + i + ".rpf" : outputLocation;
                    using (StreamWriter rbsScriptStream = new StreamWriter(rpfScriptLocation))
                    {
                        string platform = BuildManager.GetRbsPlatformName(targetPlatform);
                        rbsScriptStream.WriteLine("set_platform( \"" + platform + "\" )");
                        rbsScriptStream.WriteLine("start_pack()");
                        long divThreshold = thisProject.getRpfDivisionThreshold(targetPlatform);
                        currentIndex = copyFilesToRpf(nodeList, currentIndex, rbsScriptStream,divThreshold, replacementName);

                        rbsScriptStream.WriteLine("save_pack(" + Globals.QUOTE + rpfChunk.Replace("\\", "/") + Globals.QUOTE + ")");
                        rbsScriptStream.WriteLine("close_pack()");

                        rbsScriptStream.Close();
                    }
                    runAndDone(BuildManager.getRageBuilderPath(targetPlatform), rpfScriptLocation, false, thisProject.m_logStream, true);
                }
            }

        }

        private void getFileNodes(contentTreeNode currentNode, List<contentTreeNode> nodeList, Platform targetPlatform)
        {
            foreach (contentTreeNode childNode in currentNode.Nodes)
            {
                if(childNode.isIncludedInBuild(targetPlatform))
                {
                    if (childNode.NodeType == nodeType.NT_FILE)
                    {
                        if (childNode.Text == "setup2.xml" || childNode.Text == "content.xml")
                        {
                            nodeList.Insert(0, childNode);
                        }
                        else
                        {
                            nodeList.Add(childNode);
                        }
                    }
                    else if (childNode.NodeType == nodeType.NT_FOLDER)
                    {
                        getFileNodes(childNode, nodeList, targetPlatform);
                    }
                }
            }
        }

        private int fileNodePrePass(List<contentTreeNode> nodeList, long threshold)
        {
            int additionalRPFs = 0;
            for (int i = 0; i < nodeList.Count;)
            {
                long totalFileSize = 0;
                for (int j = i; j < nodeList.Count; ++j, ++i)
                {
                    totalFileSize += nodeList[j].Size;
                    if (totalFileSize > threshold)
                    {
                        ++additionalRPFs;
                        break;
                    }
                }
            }
            return additionalRPFs;
        }

		private int copyFilesToRpf(List<contentTreeNode> nodeList, int firstNodeIndex, StreamWriter rbsScriptStream, long threshold, string replacementBranch = null)
        {
            long totalFileSize = 0;
            for (int i = firstNodeIndex; i < nodeList.Count; ++i)
            {
                try
                {
                    contentTreeNode currentNode = nodeList[i];
                    totalFileSize += currentNode.Size;
                    if (totalFileSize > threshold)
                        return i;

                    string currentFolder = "";
                    contentTreeNode parentNode = (contentTreeNode)currentNode.Parent;
                    while (parentNode.NodeType != nodeType.NT_BRANCH_ROOT)
                    {
                        currentFolder = parentNode.Text + "/" + currentFolder;
                        parentNode = (contentTreeNode)parentNode.Parent;
                    }
                    currentFolder = "/" + currentFolder;

					string source = currentNode.FullFilePath;
					string text = source.Replace("dev",replacementBranch);
					if (File.Exists(text))
					{
						source = text;
					}

                    rbsScriptStream.WriteLine("add_to_pack(" + Globals.QUOTE + source.Replace("\\", "/") + Globals.QUOTE + ", " +
                        Globals.QUOTE + currentFolder + currentNode.Text + Globals.QUOTE + ", true)");

                    m_progressForm.IncrementProgress();
                    m_progressForm.Refresh();
                }
                catch (System.Exception ex)
                {
                    OutputWindow.appendOutput(ex.Message, OutputWindow.MessageType.ERROR);
                }
            }
            return nodeList.Count;
        }

        private void copyFilesToRpf(contentTreeNode currentNode, string currentFolder, StreamWriter rbsScriptStream, Platform targetPlatform, string replacementBranch = null)
        {
            foreach (contentTreeNode childNode in currentNode.Nodes)
            {
                try
                {
                    if (childNode.isIncludedInBuild(targetPlatform))
                    {
                        if (childNode.NodeType == nodeType.NT_FOLDER)
                            copyFilesToRpf(childNode, currentFolder + childNode.Text + "/", rbsScriptStream, targetPlatform);
                        else if (childNode.NodeType == nodeType.NT_FILE)
                        {
                            rbsScriptStream.WriteLine("add_to_pack(" + Globals.QUOTE + childNode.FullFilePath.Replace("\\", "/") + Globals.QUOTE + ", " +
                                Globals.QUOTE + currentFolder + childNode.Text + Globals.QUOTE + ", true)");
                        }
                    }
                }
                catch (System.Exception ex)
                {
                    OutputWindow.appendOutput(ex.Message, OutputWindow.MessageType.ERROR);
                }
            }
        }

        private void extractContent(Project thisProject, ContentXML contentXML, contentTreeNode currentNode, List<contentTreeNode> filesInProject, bool checkIgnores)
        {
            if (currentNode != null && currentNode.Nodes != null)
            {
                foreach (contentTreeNode childNode in currentNode.Nodes)
                {
                    if (checkIgnores && thisProject.isExcluded(childNode.FullFilePath, BuildManager.GetBuildType()))
                    {
                        continue;
                    }
                    else if (childNode.NodeType == nodeType.NT_FILE)
                    {
                        filesInProject.Add(childNode);
                    }
                    else if (childNode.NodeType == nodeType.NT_FOLDER)
                    {
                        extractContent(thisProject, contentXML, childNode, filesInProject, checkIgnores);
                    }
                }
            }
        }

        private void modifySetupXML(Project thisProject)
        {
            List<IBranch> branchesToBuild = new List<IBranch>();

            int buildInclusions = 0;
            BuildManager.addBuildInclusion(ref buildInclusions, (int)Platform.Win64);
            BuildManager.addBuildInclusion(ref buildInclusions, (int)Platform.XboxOne);
            BuildManager.addBuildInclusion(ref buildInclusions, (int)Platform.PS4);
            List<Platform> targetPlatforms = BuildManager.getIncludedPlatforms(buildInclusions);

            // Create a list of unique branches we need to update based on the platforms we are building
            foreach (Platform currPlatform in targetPlatforms)
            {
                IBranch branchForPlat = thisProject.getBranchForPlatform(currPlatform);

                if (branchForPlat != null && !branchesToBuild.Contains(branchForPlat))
                    branchesToBuild.Add(branchForPlat);
            }

            // Update the relevant branches build folders
            foreach (IBranch branchToBuild in branchesToBuild)
            {
                ContentXML thisContentXML = new ContentXML();
                SetupXML thisSetupXML = new SetupXML();
                string deviceName = string.Empty;
                string setupXML = branchToBuild.Build + "\\" + Globals.SETUP_XML;
                string contentXML = branchToBuild.Build + "\\" + Globals.CONTENT_XML;

                if (!File.Exists(setupXML))
                    return;

                thisSetupXML.read(setupXML);

                CommandPrompt.perforceCheckout(setupXML);

                thisSetupXML.modifySetupXML(thisProject);

                thisSetupXML.save();
            }
        }

        private void generateBuildXMLs(Project thisProject, int targetsToBuild)
        {
            try
            {
                if (!SettingsManager.GetInstance.m_generateSetupAndContent)
                    return;

                List<IBranch> branchesToBuild = new List<IBranch>();
                List<Platform> targetPlatforms = BuildManager.getIncludedPlatforms(targetsToBuild);

                // Create a list of unique branches we need to update based on the platforms we are building
                foreach (Platform currPlatform in targetPlatforms)
                {
                    IBranch branchForPlat = thisProject.getBranchForPlatform(currPlatform);

                    if (branchForPlat != null && !branchesToBuild.Contains(branchForPlat))
                        branchesToBuild.Add(branchForPlat);
                }

                // Update the relevant branches build folders
                foreach (IBranch branchToBuild in branchesToBuild)
                {
                    ContentXML thisContentXML = new ContentXML();
                    SetupXML thisSetupXML = new SetupXML();
                    string deviceName = string.Empty;
                    string setupXML = branchToBuild.Build + "\\" + Globals.SETUP_XML;
                    string contentXML = branchToBuild.Build + "\\" + Globals.CONTENT_XML;
                    bool freshSetupXML = false;
                    bool freshContentXML = false;

                    if (File.Exists(setupXML))
                        thisSetupXML.read(setupXML);
                    else
                    {
                        thisSetupXML.create(setupXML);
                        freshSetupXML = true;
                    }
                    m_progressForm.IncrementProgress();

                    if (File.Exists(contentXML))
                        thisContentXML.read(contentXML);
                    else
                    {
                        thisContentXML.create(contentXML);
                        freshContentXML = true;
                    }
                    m_progressForm.IncrementProgress();

                    CommandPrompt.perforceCheckout(setupXML);
                    CommandPrompt.perforceCheckout(contentXML);

                    if (thisSetupXML != null)
                        deviceName = thisSetupXML.getDeviceName(thisProject);

                    if (thisContentXML != null)
                    {
                        OutputWindow.appendOutput("About to generate " + thisContentXML.FilePath, OutputWindow.MessageType.IMPORTANT);

                        thisContentXML.preSerialise(thisProject);
                        {
                            List<contentTreeNode> filesInProject = new List<contentTreeNode>();

                            extractContent(thisProject, thisContentXML, thisProject.getBranchRootNode(branchToBuild), filesInProject, true);

                            if (!thisProject.m_compatPack)
                                thisContentXML.writeProject(thisProject, filesInProject, deviceName);
                            else
                                thisContentXML.mergeProjects(thisProject, thisProject.m_contentsToMerge, filesInProject, deviceName);
                        }
                        thisContentXML.save();

                        if (freshContentXML)
                        {
                            addNodeToContentTree(nodeType.NT_FILE, thisProject.getBranchRootNode(branchToBuild),
                                Path.GetFileName(contentXML), contentXML, false, thisProject, branchToBuild);
                        }
                    }
                    m_progressForm.IncrementProgress();

                    if (thisSetupXML != null)
                    {
                        OutputWindow.appendOutput("About to generate " + thisSetupXML.FilePath, OutputWindow.MessageType.IMPORTANT);

                        thisSetupXML.preSerialise(thisProject);
                        {
                            ArrayTunable finalChangeSets = (thisContentXML != null) ? thisContentXML.getContentChangeSets() : null;

                            if (!thisProject.m_compatPack)
                                thisSetupXML.writeProject(thisProject, deviceName);
                            else
                                thisSetupXML.mergeProjects(thisProject, thisProject.m_setupsToMerge, finalChangeSets, deviceName);
                        }
                        thisSetupXML.save();

                        if (freshSetupXML)
                        {
                            addNodeToContentTree(nodeType.NT_FILE, thisProject.getBranchRootNode(branchToBuild),
                                Path.GetFileName(setupXML), setupXML, false, thisProject, branchToBuild);
                        }
                    }
                    m_progressForm.IncrementProgress();
                }
            }
            catch (System.Exception ex)
            {
                OutputWindow.appendOutput(ex.Message, OutputWindow.MessageType.ERROR);
            }
        }

        private void gatherFiles(string directoryPath, string[] excludeFolders, List<string> gatheredFiles)
        {
            try
            {
                if (!Directory.Exists(directoryPath))
                    return;

                string[] sourceDirChildDirs = Directory.GetDirectories(directoryPath);
                string[] sourceDirFiles = Directory.GetFiles(directoryPath);

                foreach (string filePath in sourceDirFiles)
                    gatheredFiles.Add(filePath);

                foreach (string dirPath in sourceDirChildDirs)
                {
                    if (excludeFolders != null)
                    {
                        if (excludeFolders.Contains(Path.GetFileName(dirPath)))
                            continue;
                    }

                    gatherFiles(dirPath, excludeFolders, gatheredFiles);
                }
            }
            catch (System.Exception ex)
            {
                OutputWindow.appendOutput(ex.Message, OutputWindow.MessageType.ERROR);
            }
        }

        private void checkoutDirectory(string directoryPath, string[] excludeFolders)
        {
            try
            {
                List<string> gatheredFiles = new List<string>();

                gatherFiles(directoryPath, excludeFolders, gatheredFiles);
                CommandPrompt.perforceCheckoutArray(gatheredFiles.ToArray());
            }
            catch (System.Exception ex)
            {
                OutputWindow.appendOutput(ex.Message, OutputWindow.MessageType.ERROR);
            }
        }

        private void checkForContentIDCollision(string contentID, Project thisProject)
        {
            Project lookupProj = null;

            if (!m_projectCollisions.TryGetValue(Globals.getStringHashCode(contentID), out lookupProj))
                m_projectCollisions.Add(Globals.getStringHashCode(contentID), thisProject);
            else
            {
                OutputWindow.addBufferedMessage("Content ID collision between: " + thisProject.m_title + " and " +
                    lookupProj.m_title + ". ContentID = " + contentID, OutputWindow.MessageType.WARNING);
            }
        }

        private void dirCreateIfNotExists(string dirPath)
        {
            try
            {
                if (!Directory.Exists(dirPath))
                    Directory.CreateDirectory(dirPath);
            }
            catch (System.Exception ex)
            {
                OutputWindow.appendOutput(ex.Message, OutputWindow.MessageType.ERROR);
            }
        }

        private void copyFileRemoveReadOnly(string sourcePath, string destPath)
        {
            try
            {
                if (File.Exists(sourcePath))
                {
                    FileAttributes attributes;

                    File.Copy(sourcePath, destPath, true);
                    attributes = File.GetAttributes(destPath);

                    if ((attributes & FileAttributes.ReadOnly) == FileAttributes.ReadOnly)
                        File.SetAttributes(destPath, attributes ^ FileAttributes.ReadOnly);
                }
            }
            catch (System.Exception ex)
            {
                OutputWindow.appendOutput(ex.Message, OutputWindow.MessageType.ERROR);
            }
        }

        private void directoryCopy(string sourceDirName, string destDirName, bool copySubDirs)
        {
            try
            {
                if (!Directory.Exists(sourceDirName))
                    return;

                string[] sourceDirChildDirs = Directory.GetDirectories(sourceDirName);
                string[] sourceDirFiles = Directory.GetFiles(sourceDirName);

                dirCreateIfNotExists(destDirName);

                foreach (string filePath in sourceDirFiles)
                    File.Copy(filePath, Path.Combine(destDirName, Path.GetFileName(filePath)), true);

                if (copySubDirs)
                {
                    foreach (string dirPath in sourceDirChildDirs)
                        directoryCopy(dirPath, Path.Combine(destDirName, Path.GetFileName(dirPath)), copySubDirs);
                }
            }
            catch (System.Exception ex)
            {
                OutputWindow.appendOutput(ex.Message, OutputWindow.MessageType.ERROR);
            }
        }

        #endregion

        #region Pre & Post Solution

        public bool doPreBuildSolution(int targetsToBuild)
        {
            try
            {
                copyProjectDataFromForm(ProjectManager.GetInstance.CurrentProject);
                saveSolution(false);

                if (BuildManager.isIncludedInBuild(targetsToBuild, (int)Platform.PS3) && SettingsManager.GetInstance.m_ps3Targets.Count > 0)
                {
                    OutputWindow.appendOutput("Resetting PS3(s) to debug mode...", OutputWindow.MessageType.IMPORTANT);

                    foreach (DeployTarget target in SettingsManager.GetInstance.m_ps3Targets)
                    {
                        if (target.m_deploy)
                        {
                            OutputWindow.appendOutput("Resetting " + target.m_ipAddress + "...", OutputWindow.MessageType.IMPORTANT);
                            runAndDone(SettingsManager.GetInstance.m_ps3CtrlPath, "xmb -add " + target.m_ipAddress + ":PS3_512_DBG_DEX:" + target.m_ipAddress, false, null, true);
                            runAndDone(SettingsManager.GetInstance.m_ps3CtrlPath, "run -t " + target.m_ipAddress + " -r hard", false, null, true);
                        }
                    }
                }

                dirCreateIfNotExists(Globals.OUTPUT_DIRECTORY);

                return true;
            }
            catch (System.Exception ex)
            {
                OutputWindow.appendOutput(ex.Message, OutputWindow.MessageType.ERROR);

                return false;
            }
        }

        public void doPostBuildSolution()
        {
            OutputWindow.appendOutput("Check " + Globals.OUTPUT_DIRECTORY + " for content data", OutputWindow.MessageType.IMPORTANT);

            m_projectCollisions.Clear();
            OutputWindow.flushBufferedMessages();
        }

        #endregion

        #region Build Solution

        private void doBuildSolution(int targetsToBuild, bool buildCloud)
        {
            fillProjectsNodes(ProjectManager.GetInstance.Projects.ToArray());

            OutputWindow.startRecording();

            // Create progress bar to show during build.
            int progSteps = 4;
            int progPlatforms = 6;
            int totalProgress = ProjectManager.GetInstance.Projects.Count * (progSteps + progPlatforms);
            m_progressForm.ResetProgress(totalProgress, "Building project(s)");
            m_progressForm.ShowProgress();

            if (!doPreBuildSolution(targetsToBuild))
                return;

            // Join thread if it already exists for some reason.
            if (m_thread != null && m_thread.IsAlive)
            {
                m_thread.Join();
            }

            // Delegate building to new thread.
            m_thread = new Thread(delegate()
            {
                foreach (Project thisProject in ProjectManager.GetInstance.Projects)
                {
                    OutputWindow.pushActiveChannel(thisProject.m_channelId);

                    if (m_progressForm.GetCancel())
                    {
                        break;
                    }

                    if (!thisProject.getIsInBuild())
                    {
                        m_progressForm.IncrementProgress(progSteps + progPlatforms);

                        OutputWindow.appendOutput("Skipping " + thisProject.m_title, OutputWindow.MessageType.IMPORTANT);
                        continue;
                    }

                    try
                    {
                        if (!doPreBuild(thisProject, targetsToBuild))
                        {
                            m_progressForm.IncrementProgress(progPlatforms);
                            continue;
                        }

                        if (BuildManager.isIncludedInBuild(targetsToBuild, (int)Platform.Xbox360))
                        {
                            if (doPre360Build(thisProject))
                            {
                                do360Build(thisProject);
                                doPost360Build(thisProject);
                            }
                        }
                        m_progressForm.IncrementProgress();

                        if (BuildManager.isIncludedInBuild(targetsToBuild, (int)Platform.PS3))
                        {
                            if (doPrePS3Build(thisProject))
                            {
                                doPS3Build(thisProject);
                                doPostPS3Build(thisProject);
                            }
                        }
                        m_progressForm.IncrementProgress();

                        if (BuildManager.isIncludedInBuild(targetsToBuild, (int)Platform.Win32))
                        {
                            if (doPrePCBuild(thisProject))
                            {
                                doPCBuild(thisProject);
                                doPostPCBuild(thisProject);
                            }
                        }
                        m_progressForm.IncrementProgress();

                        if (BuildManager.isIncludedInBuild(targetsToBuild, (int)Platform.PS4))
                        {
                            if (doPrePS4Build(thisProject))
                            {
                                doPS4Build(thisProject);
                                doPostPS4Build(thisProject);
                            }
                        }
                        m_progressForm.IncrementProgress();

                        if (BuildManager.isIncludedInBuild(targetsToBuild, (int)Platform.XboxOne))
                        {
                            thisProject.m_xboneData.IntermediateDirectory = Globals.XB1_WORKAROUND_PATH + "\\WW";
                            if (doPreXboneBuild(thisProject))
                            {
                                doXboneBuild(thisProject);
                                doPostXboneBuild(thisProject, false);
                                clearXboneIntermediateDirs(thisProject, false);
                            }
                            thisProject.m_xboneData.IntermediateDirectory = Globals.XB1_WORKAROUND_PATH + "\\JPN";
                            if (doPreXboneBuild(thisProject, true))
                            {
                                doXboneBuild(thisProject);
                                doPostXboneBuild(thisProject, true);
                                clearXboneIntermediateDirs(thisProject, true);
                            }
                        }
                        m_progressForm.IncrementProgress();

                        if (buildCloud)
                        {
                            if (doPreCloudBuild(thisProject))
                            {
                                doCloudBuild(thisProject);
                                doPostCloudBuild(thisProject);
                            }
                        }
                        m_progressForm.IncrementProgress();

                        doPostBuild(thisProject);
                    }
                    catch (Exception ex)
                    {
                        OutputWindow.appendOutput(ex.Message, OutputWindow.MessageType.ERROR);
                    }
                }

                doPostBuildSolution();

                m_progressForm.IncrementProgress(m_progressForm.GetMaximum());

                OutputWindow.popActiveChannel();
                OutputWindow.stopRecording();
            }); // m_thread
                
            // Start the thread.
            m_thread.Start();
        }

		private void runXboneBuildThread(Project thisProject)
		{
			doXboneBuild(thisProject);
			doPostXboneBuild(thisProject,false);
		}
        private void runBuildTarget(string targetPath)
        {
            try
            {
                if (File.Exists(targetPath))
                {
                    OutputWindow.appendOutput("Calling target " + targetPath, OutputWindow.MessageType.IMPORTANT);

                    runAndDone("python.exe", targetPath, false, null, true);
                }
            }
            catch (System.Exception ex)
            {
                OutputWindow.appendOutput(ex.Message, OutputWindow.MessageType.ERROR);
            }
        }

        #endregion

        #region Pre & Post Project

        private bool doPreBuild(Project thisProject, int targetsToBuild)
        {
            try
            {
                generateBuildXMLs(thisProject, targetsToBuild);
                runBuildTarget(thisProject.m_preBuildPath);

                return true;
            }
            catch (System.Exception ex)
            {
                OutputWindow.appendOutput(ex.Message, OutputWindow.MessageType.ERROR);

                return false;
            }
        }

        private void doPostBuild(Project thisProject)
        {
            OutputWindow.appendOutput("Doing main post-build", OutputWindow.MessageType.IMPORTANT);

            try
            {
                runBuildTarget(thisProject.m_postBuildPath);

                checkForContentIDCollision(thisProject.getXboxContentID(Platform.Xbox360), thisProject);

                if (thisProject.getUSBuildValid(Platform.PS3))
                    checkForContentIDCollision(thisProject.getUSContentID(Platform.PS3), thisProject);

                if (thisProject.getEUBuildValid(Platform.PS3))
                    checkForContentIDCollision(thisProject.getEUContentID(Platform.PS3), thisProject);

                if (thisProject.getJPNBuildValid(Platform.PS3))
                    checkForContentIDCollision(thisProject.getJPNContentID(Platform.PS3), thisProject);

                createProjectLogFile(thisProject);
            }
            catch (System.Exception ex)
            {
                OutputWindow.appendOutput(ex.Message, OutputWindow.MessageType.ERROR);
            }

            setSelectedNode((contentTreeNode)tvContentTree.Nodes[0]);
        }

        #endregion

        #region 360

        private void writeXlastContentNode(contentTreeNode currentNode, XmlWriter xlastWriter)
        {
            foreach (contentTreeNode childNode in currentNode.Nodes)
            {
                try
                {
                    if (childNode.isIncludedInBuild(Platform.Xbox360))
                    {
                        if (childNode.NodeType == nodeType.NT_FILE)
                        {
                            xlastWriter.WriteStartElement("File");
                            {
                                xlastWriter.WriteAttributeString("clsid", "{8A0BC3DD-B402-4080-8E34-C22144FC1ECE}");
                                xlastWriter.WriteAttributeString("TargetName", childNode.Text);
                                xlastWriter.WriteAttributeString("SourceName", childNode.FullFilePath);
                            }
                            xlastWriter.WriteEndElement();
                        }
                        else if (childNode.NodeType == nodeType.NT_FOLDER)
                        {
                            xlastWriter.WriteStartElement("Folder");
                            {
                                xlastWriter.WriteAttributeString("clsid", "{0D8B38B9-F5A8-4050-8F8D-81F67E3B2456}");
                                xlastWriter.WriteAttributeString("TargetName", childNode.Text);

                                writeXlastContentNode(childNode, xlastWriter);
                            }
                            xlastWriter.WriteEndElement();
                        }
                    }
                }
                catch (System.Exception ex)
                {
                    OutputWindow.appendOutput(ex.Message, OutputWindow.MessageType.ERROR);
                }
            }
        }

        private void writeXlast(Project projectToWrite, string tempLocation)
        {
            try
            {
                XmlWriterSettings xlastWriteSettings = new XmlWriterSettings();
                XboxData xboxData = projectToWrite.m_xboxData;

                xlastWriteSettings.Indent = true;

                XmlWriter xlastWriter = XmlWriter.Create(tempLocation, xlastWriteSettings);
                {
                    xlastWriter.WriteStartDocument();
                    xlastWriter.WriteStartElement("XboxLiveSubmissionProject");
                    xlastWriter.WriteAttributeString("Version", Globals.XLAST_WRITER_VERSION);
                    {
                        xlastWriter.WriteStartElement("ContentProject");
                        {
                            xlastWriter.WriteAttributeString("clsid", "{AED6156D-A870-4FF7-924F-F375495A222A}");
                            xlastWriter.WriteAttributeString("Name", xboxData.m_packageName);
                            xlastWriter.WriteAttributeString("TitleID", GlobalFieldsManager.GetInstance.getGlobalFieldsFromBank(projectToWrite.m_globalBankName).m_titleID);
                            xlastWriter.WriteAttributeString("TitleName", GlobalFieldsManager.GetInstance.getGlobalFieldsFromBank(projectToWrite.m_globalBankName).m_gameTitle);
                            xlastWriter.WriteAttributeString("ActivationDate", xboxData.m_activationDate);
                            xlastWriter.WriteAttributeString("PubOfferingID", xboxData.m_offeringID);
                            xlastWriter.WriteAttributeString("PubBitFlags", xboxData.m_publisherFlags);
                            xlastWriter.WriteAttributeString("HasCost", xboxData.m_hasCost.ToString().ToLower());
                            xlastWriter.WriteAttributeString("IsMarketplace", xboxData.m_isMarketPlace.ToString().ToLower());
                            xlastWriter.WriteAttributeString("AllowProfileTransfer", xboxData.m_allowProfileTransfer.ToString().ToLower());
                            xlastWriter.WriteAttributeString("AllowDeviceTransfer", xboxData.m_allowDeviceTransfer.ToString().ToLower());
                            xlastWriter.WriteAttributeString("DashIconPath", xboxData.m_dashboardIcon);
                            xlastWriter.WriteAttributeString("TitleIconPath", xboxData.m_titleImage);
                            xlastWriter.WriteAttributeString("ContentType", "0x00000002"); /*TOMW FIX*/

                            xlastWriter.WriteStartElement("LanguageSettings");
                            {
                                xlastWriter.WriteAttributeString("clsid", "{F5BA1EE2-D217-447C-93C7-3C7AA6F25DD5}");

                                xlastWriter.WriteStartElement("Language");
                                {
                                    xlastWriter.WriteAttributeString("clsid", "{6424EAA3-FA00-4B6C-8CFB-BF063FC18845}");
                                    xlastWriter.WriteAttributeString("DashDisplayName", xboxData.m_packageName);
                                    xlastWriter.WriteAttributeString("Description", xboxData.m_packageName);
                                }
                                xlastWriter.WriteEndElement();
                            }
                            xlastWriter.WriteEndElement();

                            xlastWriter.WriteStartElement("Contents");
                            {
                                xlastWriter.WriteAttributeString("clsid", "{0D8B38B9-F5A8-4050-8F8D-81F67E3B2456}");

                                writeXlastContentNode(projectToWrite.getBranchRootNode(Platform.Xbox360), xlastWriter);
                            }
                            xlastWriter.WriteEndElement();

                            xlastWriter.WriteStartElement("ContentOffers");
                            {
                                xlastWriter.WriteAttributeString("clsid", "{146485F0-DCD7-4AB1-97E1-9B0E64150499}");

                                xlastWriter.WriteStartElement("ContentOffers");
                                {
                                    xlastWriter.WriteAttributeString("clsid", "{4AF8FE5B-CCF2-4C23-852F-D91A94AE0E24}");
                                    xlastWriter.WriteAttributeString("friendlyName", "New Offer");
                                    xlastWriter.WriteAttributeString("title", xboxData.m_packageName);
                                    xlastWriter.WriteAttributeString("sellText", xboxData.m_packageName);
                                    xlastWriter.WriteAttributeString("licenseMask", xboxData.m_licenseMask);
                                    xlastWriter.WriteAttributeString("contentCategory", xboxData.m_contentCategory);
                                    xlastWriter.WriteAttributeString("visibility", xboxData.m_visibility.ToString().ToLower());
                                    xlastWriter.WriteAttributeString("previewOfferId", xboxData.m_previewOfferID);
                                    xlastWriter.WriteAttributeString("offerId", xboxData.m_offerID);
                                    xlastWriter.WriteAttributeString("banner", xboxData.m_offerBanner);
                                    xlastWriter.WriteAttributeString("licenseLevel", xboxData.m_licenseLevel.ToString());
                                    xlastWriter.WriteAttributeString("consumable", xboxData.m_consumable.ToString().ToLower());

                                    if (xboxData.m_consumable)
                                    {
                                        xlastWriter.WriteAttributeString("assetId", "0"); // TOMW
                                        xlastWriter.WriteAttributeString("quantity", "0"); // TOMW
                                    }
                                }
                                xlastWriter.WriteEndElement();
                            }
                            xlastWriter.WriteEndElement();
                        }
                        xlastWriter.WriteEndElement();
                    }
                    xlastWriter.WriteEndElement();
                    xlastWriter.WriteEndDocument();
                }
                xlastWriter.Close();
            }
            catch (System.Exception ex)
            {
                OutputWindow.appendOutput(ex.Message, OutputWindow.MessageType.ERROR);
            }
        }

        private bool doPre360Build(Project thisProject)
        {
            OutputWindow.appendOutput("Doing XBOX pre-build", OutputWindow.MessageType.IMPORTANT);

            if (!File.Exists(SettingsManager.GetInstance.m_blastPath))
            {
                OutputWindow.appendOutput("Cannot locate blast.exe! Aborting!", OutputWindow.MessageType.ERROR);
                return false;
            }

            if (GlobalFieldsManager.GetInstance.getGlobalFieldsFromBank(thisProject.m_globalBankName).m_titleID.Length <= 0)
            {
                OutputWindow.appendOutput("You must supply a valid title ID!", OutputWindow.MessageType.ERROR);
                return false;
            }

            if (GlobalFieldsManager.GetInstance.getGlobalFieldsFromBank(thisProject.m_globalBankName).m_gameTitle.Length <= 0)
            {
                OutputWindow.appendOutput("You must supply a valid game title!", OutputWindow.MessageType.ERROR);
                return false;
            }

            if (thisProject.m_xboxData.m_packageName.Length <= 0)
            {
                OutputWindow.appendOutput("You must supply a valid package name!", OutputWindow.MessageType.ERROR);
                return false;
            }

            if (thisProject.m_xboxData.m_publisherFlags.Length <= 0)
            {
                OutputWindow.appendOutput("You must supply a valid publisher flags!", OutputWindow.MessageType.ERROR);
                return false;
            }

            if (thisProject.m_xboxData.m_offeringID.Length <= 0)
            {
                OutputWindow.appendOutput("You must supply a valid offering ID!", OutputWindow.MessageType.ERROR);
                return false;
            }

            if (thisProject.m_xboxData.m_activationDate.Length <= 0)
            {
                OutputWindow.appendOutput("You must supply a valid activation date!", OutputWindow.MessageType.ERROR);
                return false;
            }

            if (thisProject.m_xboxData.m_licenseMask.Length <= 0)
            {
                OutputWindow.appendOutput("You must supply a valid license mask!", OutputWindow.MessageType.ERROR);
                return false;
            }

            if (thisProject.m_xboxData.m_previewOfferID.Length <= 0)
            {
                OutputWindow.appendOutput("You must supply a valid Preview offer ID!", OutputWindow.MessageType.ERROR);
                return false;
            }

            if (thisProject.m_xboxData.m_contentCategory.Length <= 0)
            {
                OutputWindow.appendOutput("You must supply a valid content category!", OutputWindow.MessageType.ERROR);
                return false;
            }

            if (thisProject.m_xboxData.m_offerID.Length <= 0)
            {
                OutputWindow.appendOutput("You must supply a valid offer ID!", OutputWindow.MessageType.ERROR);
                return false;
            }

            if (thisProject.m_xboxData.m_titleImage.Length <= 0)
            {
                OutputWindow.appendOutput("You must supply a valid title image!", OutputWindow.MessageType.ERROR);
                return false;
            }

            if (thisProject.m_xboxData.m_dashboardIcon.Length <= 0)
            {
                OutputWindow.appendOutput("You must supply a valid dashboard icon!", OutputWindow.MessageType.ERROR);
                return false;
            }

            if (thisProject.m_xboxData.m_offerBanner.Length <= 0)
            {
                OutputWindow.appendOutput("You must supply a valid offer banner!", OutputWindow.MessageType.ERROR);
                return false;
            }

            if (!loadImage(thisProject.m_xboxData.m_offerBanner, pbXboxOffBan, false, PixelFormat.DontCare))
            {
                OutputWindow.appendOutput("You must supply a valid offer banner!", OutputWindow.MessageType.ERROR);
                return false;
            }

            if (!loadImage(thisProject.m_xboxData.m_dashboardIcon, pbXboxDBIcon, false, PixelFormat.DontCare))
            {
                OutputWindow.appendOutput("You must supply a valid dashboard icon!", OutputWindow.MessageType.ERROR);
                return false;
            }

            try
            {
                runBuildTarget(thisProject.m_xboxData.m_preBuildPath);
            }
            catch (System.Exception ex)
            {
                OutputWindow.appendOutput(ex.Message, OutputWindow.MessageType.ERROR);
            }

            return true;
        }

        private void do360Build(Project thisProject)
        {
            try
            {
                string Xbox360PlatformName = BuildManager.GetDirectoryPlatformName(Platform.Xbox360);
                string xboxRootDir = thisProject.OutputDirectory + "\\" + Xbox360PlatformName;
                string xboxPreBuildDir = thisProject.OutputDirectory + "\\" + Xbox360PlatformName + "\\preBuild";
                string xboxBinDir = thisProject.OutputDirectory + "\\" + Xbox360PlatformName + "\\bin";
                string xboxXlastFile = xboxPreBuildDir + "\\" + thisProject.m_title + ".xlast";
                string xboxLogFile = xboxBinDir + "\\" + thisProject.getXboxContentID(Platform.Xbox360) + ".txt";
                string[] excludeFolders = { "BuildFiles" };

                OutputWindow.appendOutput("Doing XBOX build", OutputWindow.MessageType.IMPORTANT);

                checkoutDirectory(xboxBinDir, excludeFolders);

                dirCreateIfNotExists(xboxRootDir);
                dirCreateIfNotExists(xboxPreBuildDir);
                dirCreateIfNotExists(xboxBinDir);

                writeXlast(thisProject, xboxXlastFile);

                // Deploy local builds to any target xboxes
                foreach (DeployTarget target in SettingsManager.GetInstance.m_xboxTargets)
                {
                    if (target.m_deploy)
                    {
                        string ipXboxTarget = "/x " + target.m_ipAddress;

                        runAndDone(SettingsManager.GetInstance.m_blastPath, xboxXlastFile + " /nologo /install:local " + ipXboxTarget, false, null, true);
                    }
                }

                thisProject.openLogStream(xboxLogFile);

                // Create a release build and copy it to the bin folder
                runAndDone(SettingsManager.GetInstance.m_blastPath, xboxXlastFile + " /nologo", false, thisProject.m_logStream, true);
                directoryCopy(xboxPreBuildDir + "\\Online", xboxBinDir, true);

                // Copy the xlast we generated and store it in the bin folder in case QA need it
                File.Copy(xboxXlastFile, xboxBinDir + "\\" + thisProject.m_title + ".xlast", true);

                thisProject.closeLogStream();

                // check out the directory again to account for new files
                checkoutDirectory(xboxBinDir, excludeFolders);

                Directory.Delete(xboxPreBuildDir, true); // remove the temporary directory
            }
            catch (System.Exception ex)
            {
                OutputWindow.appendOutput(ex.Message, OutputWindow.MessageType.ERROR);
            }
        }

        private void doPost360Build(Project thisProject)
        {
            OutputWindow.appendOutput("Doing XBOX post-build", OutputWindow.MessageType.IMPORTANT);

            try
            {
                runBuildTarget(thisProject.m_xboxData.m_postBuildPath);
            }
            catch (System.Exception ex)
            {
                OutputWindow.appendOutput(ex.Message, OutputWindow.MessageType.IMPORTANT);
            }
        }

        #endregion

        #region Xbone

		private void makeXB1Rpf(Project thisProject, string xb1DLCPackDir)
		{
			string xb1TempRbsRpfScript = xb1DLCPackDir + "\\DLC.rbs";
			string xb1RPF = xb1DLCPackDir + "\\DLC.rpf";

			dirCreateIfNotExists(xb1DLCPackDir);

			createContentRPF(xb1TempRbsRpfScript, xb1RPF, thisProject, Platform.XboxOne);

		}

        private bool doPreXboneBuild(Project thisProject, bool buildJPN = false)
        {
            OutputWindow.appendOutput("Doing Xbone pre-build", OutputWindow.MessageType.IMPORTANT);

            try
            {
				ProjectManager.GetInstance.CurrentProject = thisProject;
				IBranch platform = thisProject.getBranchForPlatform(Platform.XboxOne);
				string buildPath = thisProject.getBranchForPlatform(Platform.XboxOne).Build;
				string logosDir = "";
				XboxOneChunkLayout layout = thisProject.m_xboneData.ChunkLayout = new XboxOneChunkLayout();
				BuildRegistrationChunk(thisProject,ref layout);
				BuildLaunchChunkRPF(thisProject, ref layout);
				BuildAlignmentChunk(thisProject, ref layout);
				string selectedProductID = buildJPN?thisProject.m_xboneData.AllowedProductIdJPN  :thisProject.m_xboneData.AllowedProductID;
				thisProject.m_xboneData.AppManifest.Extensions.PackageExtension.ContentPackage.AllowedProductId.Id =
					thisProject.m_xboneData.BuildDebug ? Globals.XB1_TEST_PRODUCT_ID : selectedProductID;
				clearXboneIntermediateDirs(thisProject,buildJPN);
				string intermediateDir = thisProject.m_xboneData.IntermediateDirectory;// = buildPath + @"\out";
				dirCreateIfNotExists(thisProject.m_xboneData.IntermediateDirectory);


				FileStream f = File.OpenWrite(thisProject.m_xboneData.IntermediateDirectory + @"\Update.AlignmentChunk");
				f.WriteByte(48);
				f.Close();
				if (Globals.USE_XB1_WORKAROUND_PATH_FOR_VIRTUAL_DRIVES)
				{
					thisProject.m_xboneData.WriteAppManifest(thisProject.m_xboneData.IntermediateDirectory + @"\AppXManifest.xml");
					thisProject.m_xboneData.WriteChunkLayout(thisProject.m_xboneData.IntermediateDirectory + @"\chunksRPF.xml");
					logosDir = thisProject.m_xboneData.IntermediateLogoDirectory = thisProject.m_xboneData.IntermediateDirectory + @"\signage\";
				}
				else
				{
					thisProject.m_xboneData.WriteAppManifest(buildPath + @"\AppXManifest.xml");
					thisProject.m_xboneData.WriteChunkLayout(buildPath + @"\chunksRPF.xml");
					logosDir = thisProject.m_xboneData.IntermediateLogoDirectory = buildPath + @"\signage\";
				}
				dirCreateIfNotExists(logosDir);
				CContentPackageVisualElements vElems = thisProject.m_xboneData.AppManifest.Extensions.PackageExtension.ContentPackage.ContentPackageVisualElements;
				string storeLogo = vElems.Logo;
				string smallLogo = vElems.SmallLogo;
				string wideLogo = vElems.WideLogo;
				string liveLogo = thisProject.m_xboneData.AppManifest.Properties.Logo;
				copyAndSetNotReadOnly(storeLogo, logosDir + (Path.GetFileName(storeLogo)));
				copyAndSetNotReadOnly(smallLogo, logosDir + (Path.GetFileName(smallLogo)));
				copyAndSetNotReadOnly(wideLogo, logosDir + (Path.GetFileName(wideLogo)));
				copyAndSetNotReadOnly(liveLogo, logosDir + (Path.GetFileName(liveLogo)));
				return true;
            }
            catch (System.Exception ex)
            {
                OutputWindow.appendOutput(ex.Message, OutputWindow.MessageType.ERROR);
				return false;
            }
        }
		private void copyAndSetNotReadOnly(string source,string destination)
		{
			File.Copy(source,destination);
			new FileInfo(destination).Attributes &= ~FileAttributes.ReadOnly;
		}
		private void clearXboneIntermediateDirs(Project thisProject, bool buildingJpn )
		{
			string buildPath = thisProject.getBranchForPlatform(Platform.XboxOne).Build;
			string logosDir = thisProject.m_xboneData.IntermediateLogoDirectory = buildPath + @"\signage\";
			string intermediateDir = "";
			if (Globals.USE_XB1_WORKAROUND_PATH_FOR_VIRTUAL_DRIVES)
			{
				intermediateDir = thisProject.m_xboneData.IntermediateDirectory = Globals.XB1_WORKAROUND_PATH + (buildingJpn? "\\jpn" : "\\ww");
				logosDir = thisProject.m_xboneData.IntermediateLogoDirectory = Globals.XB1_WORKAROUND_PATH + (buildingJpn ? "\\jpn" : "\\ww") + @"\signage\";
			}
			else
			{
				intermediateDir = thisProject.m_xboneData.IntermediateDirectory = buildPath + @"\out";
			}
			if (Directory.Exists(intermediateDir))
			{
				clearDirectoryAttributes(intermediateDir);
				Directory.Delete(intermediateDir, true);
			}
			if (Directory.Exists(logosDir))
			{
				clearDirectoryAttributes(logosDir);
				Directory.Delete(logosDir, true);
			}
		}
		private void clearDirectoryAttributes(string dir)
		{
			DirectoryInfo dirInfo = new DirectoryInfo(dir);
			foreach (FileInfo info in dirInfo.EnumerateFiles())
			{
				info.Attributes &= ~FileAttributes.ReadOnly;
			}
		}

        private void doXboneBuild(Project thisProject)
		{
			string buildPath = thisProject.getBranchForPlatform(Platform.XboxOne).Build;
			string buildString = "";
			string licenseStr = thisProject.m_xboneData.BuildDebug ? @"/lt" : @"/l";
			//string buildString = " pack /lt /f " + buildPath + "\\chunks.xml /d " + buildPath + " /pd " + buildPath + "\\out";
			if (Globals.USE_XB1_WORKAROUND_PATH_FOR_VIRTUAL_DRIVES)
			{
				makeXB1Rpf(thisProject, thisProject.m_xboneData.IntermediateDirectory);
				buildString = " pack /v " + licenseStr + " /f \"" + thisProject.m_xboneData.IntermediateDirectory + "\\chunksRPF.xml\" /d \"" + thisProject.m_xboneData.IntermediateDirectory + "\" /pd \"" + thisProject.m_xboneData.IntermediateDirectory + "\"";
			}
			else
			{
				makeXB1Rpf(thisProject, buildPath + "\\out");
				buildString = " pack /v " + licenseStr + " /f \"" + buildPath + "\\chunksRPF.xml\" /d \"" + buildPath + "\" /pd \"" + buildPath + "\\out\"";
			}
			runAndDone(SettingsManager.GetInstance.m_XB1MakePackagePath,buildString, true, thisProject.m_logStream, true);

        }

        private void doPostXboneBuild(Project thisProject,bool buildingJpn)
        {
            OutputWindow.appendOutput("Doing Xbone post-build", OutputWindow.MessageType.IMPORTANT);

            try
			{
				string buildType =
					thisProject.m_xboneData.BuildDebug ? "\\dev" : "\\release";
				string xbonePlatformName = BuildManager.GetDirectoryPlatformName(Platform.XboxOne);
				string xbonePackageDir = thisProject.OutputDirectory + "\\" + xbonePlatformName + "\\bin" + buildType + (buildingJpn ? "\\jpn" : "\\ww");
				string logosDir = thisProject.m_xboneData.IntermediateLogoDirectory;
				string intermediateDir = thisProject.m_xboneData.IntermediateDirectory;
				string buildPath = thisProject.getBranchForPlatform(Platform.XboxOne).Build;
				if (Globals.USE_XB1_WORKAROUND_PATH_FOR_VIRTUAL_DRIVES)
				{
					buildPath = thisProject.m_xboneData.IntermediateDirectory;
				}
				//string chunkFile = buildPath + @"\chunks.xml";
				string chunkFile = buildPath + @"\chunksRPF.xml";
				string appManifest = buildPath + @"\AppXManifest.xml";
				checkoutDirectory(xbonePackageDir, null);
				directoryCopy(intermediateDir,xbonePackageDir,false);
				directoryCopy(logosDir,xbonePackageDir,false);
				File.Copy(appManifest, xbonePackageDir + "\\" + Path.GetFileName(appManifest), true);
				File.Copy(chunkFile, xbonePackageDir + "\\" + Path.GetFileName(chunkFile), true);
				// Deploy local builds to any target xbones
				foreach (DeployTarget target in SettingsManager.GetInstance.m_XB1Targets)
				{
					if (target.m_deploy)
					{
						string ipXboxTarget = target.m_ipAddress;

						runAndDone(SettingsManager.GetInstance.m_XB1xbconnectPath, ipXboxTarget, false, null, true);
						runAndDone(SettingsManager.GetInstance.m_XB1xbappPath, " install " + thisProject.m_xboneData.IntermediatePackageFile, false, null, true);
					}
				}

				File.Delete(chunkFile);
				File.Delete(appManifest);
				Directory.Delete(logosDir, true);
				Directory.Delete(intermediateDir, true);
            }
            catch (System.Exception ex)
            {
                OutputWindow.appendOutput(ex.Message, OutputWindow.MessageType.ERROR);
            }
        }

        #endregion

        #region PS3

        private bool doPrePS3Build(Project thisProject)
        {
            string usContentID = thisProject.getUSContentID(Platform.PS3);
            string euContentID = thisProject.getEUContentID(Platform.PS3);
            string jpnContentID = thisProject.getJPNContentID(Platform.PS3);

            OutputWindow.appendOutput("Doing PS3 pre-build", OutputWindow.MessageType.IMPORTANT);

            if (!File.Exists(SettingsManager.GetInstance.m_makePackagePath) || !File.Exists(SettingsManager.GetInstance.m_makeEdatPath))
            {
                OutputWindow.appendOutput("Cannot locate make_package or make_edat executables! Aborting!", OutputWindow.MessageType.ERROR);
                return false;
            }

            if (!File.Exists(thisProject.m_ps3Data.m_icon0))
            {
                OutputWindow.appendOutput("You must select a valid ICON0.png!", OutputWindow.MessageType.ERROR);
                return false;
            }

            if (!loadImage(thisProject.m_ps3Data.m_icon0, pbPs3Banner, true, PixelFormat.DontCare))
            {
                OutputWindow.appendOutput("You must select a valid ICON0.png!", OutputWindow.MessageType.ERROR);
                return false;
            }

            if (thisProject.m_ps3Data.m_drmType.Length <= 0)
            {
                OutputWindow.appendOutput("You must supply a DRM type!", OutputWindow.MessageType.ERROR);
                return false;
            }

            if (thisProject.m_ps3Data.m_contentType.Length <= 0)
            {
                OutputWindow.appendOutput("You must supply a content type!", OutputWindow.MessageType.ERROR);
                return false;
            }

            if (GlobalFieldsManager.GetInstance.getGlobalFieldsFromBank(thisProject.m_globalBankName).m_kLicensee.Length <= 0)
            {
                OutputWindow.appendOutput("You must supply a KLicensee!", OutputWindow.MessageType.ERROR);
                return false;
            }

            if (usContentID.Length < Globals.PS3_CID_FMT_LENGTH && euContentID.Length < Globals.PS3_CID_FMT_LENGTH && jpnContentID.Length < Globals.PS3_CID_FMT_LENGTH)
            {
                OutputWindow.appendOutput("You must enter at least one valid content ID!", OutputWindow.MessageType.ERROR);
                return false;
            }
            else
            {
                // ensure if we had a content ID we also have a valid param.sfo and vice versa
                if ((usContentID.Length == Globals.PS3_CID_FMT_LENGTH && thisProject.m_ps3Data.m_USparamSFO.Length <= 0) ||
                    (thisProject.m_ps3Data.m_USparamSFO.Length > 0 && usContentID.Length < Globals.PS3_CID_FMT_LENGTH))
                {
                    OutputWindow.appendOutput("You must supply both a valid content ID and a valid param.sfo file for US!", OutputWindow.MessageType.ERROR);
                    return false;
                }

                if ((euContentID.Length == Globals.PS3_CID_FMT_LENGTH && thisProject.m_ps3Data.m_EUparamSFO.Length <= 0) ||
                    (thisProject.m_ps3Data.m_EUparamSFO.Length > 0 && euContentID.Length < Globals.PS3_CID_FMT_LENGTH))
                {
                    OutputWindow.appendOutput("You must supply both a valid content ID and a valid param.sfo file for EU!", OutputWindow.MessageType.ERROR);
                    return false;
                }

                if ((jpnContentID.Length == Globals.PS3_CID_FMT_LENGTH && thisProject.m_ps3Data.m_JPNparamSFO.Length <= 0) ||
                    (thisProject.m_ps3Data.m_JPNparamSFO.Length > 0 && jpnContentID.Length < Globals.PS3_CID_FMT_LENGTH))
                {
                    OutputWindow.appendOutput("You must supply both a valid content ID and a valid param.sfo file for JPN!", OutputWindow.MessageType.ERROR);
                    return false;
                }

                if (thisProject.getUSBuildValid(Platform.PS3) && thisProject.m_ps3Data.m_versionUS.Length <= 0)
                {
                    OutputWindow.appendOutput("You must supply a version for US!", OutputWindow.MessageType.ERROR);
                    return false;
                }

                if (thisProject.getEUBuildValid(Platform.PS3) && thisProject.m_ps3Data.m_versionEU.Length <= 0)
                {
                    OutputWindow.appendOutput("You must supply a version for EU!", OutputWindow.MessageType.ERROR);
                    return false;
                }

                if (thisProject.getJPNBuildValid(Platform.PS3) && thisProject.m_ps3Data.m_versionJN.Length <= 0)
                {
                    OutputWindow.appendOutput("You must supply a version for JN!", OutputWindow.MessageType.ERROR);
                    return false;
                }
            }

            runBuildTarget(thisProject.m_ps3Data.m_preBuildPath);

            return true;
        }

        private void makePS3Package(Project thisProject, string ps3PreBuildDir, string ps3PkgDir, string paramSfoPath, string contentID, string version)
        {
            try
            {
                if (contentID.Length == Globals.PS3_CID_FMT_LENGTH)
                {
                    string confFilePath = ps3PreBuildDir + "\\ps3.conf";

                    using (StreamWriter confStream = new StreamWriter(confFilePath))
                    {
                        confStream.WriteLine("ContentID = " + contentID);
                        confStream.WriteLine("Klicensee = " + GlobalFieldsManager.GetInstance.getGlobalFieldsFromBank(thisProject.m_globalBankName).m_kLicensee);
                        confStream.WriteLine("DRMType = " + thisProject.m_ps3Data.m_drmType);
                        confStream.WriteLine("ContentType = " + thisProject.m_ps3Data.m_contentType);
                        confStream.WriteLine("PackageVersion = " + version);

                        confStream.Close();
                    }

                    copyFileRemoveReadOnly(thisProject.m_ps3Data.m_icon0, ps3PreBuildDir + "\\ICON0.png");
                    copyFileRemoveReadOnly(paramSfoPath, ps3PreBuildDir + "\\PARAM.sfo");

                    runAndDone(SettingsManager.GetInstance.m_makePackagePath, "-o " + ps3PkgDir + " " + confFilePath + " " + ps3PreBuildDir, true, thisProject.m_logStream, true);
                }
            }
            catch (System.Exception ex)
            {
                OutputWindow.appendOutput(ex.Message, OutputWindow.MessageType.ERROR);
            }
        }

        private void makeRbsAndRpfPS3(Project thisProject, string ps3DLCPackDir, string ps3PreBuildDir, string ps3PkgDir, string ps3EdatFolder)
        {
            string ps3TempRbsRpfScript = ps3DLCPackDir + "\\temp.rbs";
            string ps3TempRpf = ps3DLCPackDir + "\\temp.rpf";
            string ps3FinalEdat = ps3DLCPackDir + "\\DLC.edat";

            dirCreateIfNotExists(ps3PreBuildDir);
            dirCreateIfNotExists(ps3DLCPackDir);

            createContentRPF(ps3TempRbsRpfScript, ps3TempRpf, thisProject, Platform.PS3);

            runAndDone(SettingsManager.GetInstance.m_makeEdatPath, ps3TempRpf + " " + ps3FinalEdat, true, thisProject.m_logStream, true);

            if (File.Exists(ps3FinalEdat))
            {
                string edatPkgFolderTarget = ps3PkgDir + "\\" + ps3EdatFolder;
                string edatPkgTarget = edatPkgFolderTarget + "\\" + "DLC.edat";

                dirCreateIfNotExists(edatPkgFolderTarget);

                if (File.Exists(edatPkgTarget))
                    CommandPrompt.perforceCheckout(edatPkgTarget);

                File.Copy(ps3FinalEdat, edatPkgTarget, true);
                CommandPrompt.perforceCheckout(edatPkgTarget);
            }

            File.Delete(ps3TempRpf);
            File.Delete(ps3TempRbsRpfScript);
        }

        private void doPS3Build(Project thisProject)
        {
            try
            {
                string PS3PlatformName = BuildManager.GetDirectoryPlatformName(Platform.PS3);
                string usContentID = thisProject.getUSContentID(Platform.PS3);
                string euContentID = thisProject.getEUContentID(Platform.PS3);
                string jpnContentID = thisProject.getJPNContentID(Platform.PS3);
                string ps3PkgDir = thisProject.OutputDirectory + "\\" + PS3PlatformName + "\\pkgs";
                string ps3PrebuildsDir = thisProject.OutputDirectory + "\\" + PS3PlatformName + "\\preBuilds";

                OutputWindow.appendOutput("Doing PS3 build", OutputWindow.MessageType.IMPORTANT);
                
                dirCreateIfNotExists(ps3PkgDir);

                // Checkout any existing files
                checkoutDirectory(ps3PkgDir, null);

                if (thisProject.getUSBuildValid(Platform.PS3))
                {
                    string ps3PreBuildDirUS = ps3PrebuildsDir + "\\preBuildUS";
                    string ps3DLCPackDirUS = ps3PreBuildDirUS + "\\USRDIR" + "\\dlc\\dlc_" + thisProject.m_title;

                    thisProject.openLogStream(ps3PkgDir + "\\" + usContentID + ".txt");

                    makeRbsAndRpfPS3(thisProject, ps3DLCPackDirUS, ps3PreBuildDirUS, ps3PkgDir, "US");
                    makePS3Package(thisProject, ps3PreBuildDirUS, ps3PkgDir, thisProject.m_ps3Data.m_USparamSFO, usContentID, thisProject.m_ps3Data.m_versionUS);

                    thisProject.closeLogStream();
                }

                if (thisProject.getEUBuildValid(Platform.PS3))
                {
                    string ps3PreBuildDirEU = ps3PrebuildsDir + "\\preBuildEU";
                    string ps3DLCPackDirEU = ps3PreBuildDirEU + "\\USRDIR" + "\\dlc\\dlc_" + thisProject.m_title;

                    thisProject.openLogStream(ps3PkgDir + "\\" + euContentID + ".txt");

                    makeRbsAndRpfPS3(thisProject, ps3DLCPackDirEU, ps3PreBuildDirEU, ps3PkgDir, "EU");
                    makePS3Package(thisProject, ps3PreBuildDirEU, ps3PkgDir, thisProject.m_ps3Data.m_EUparamSFO, euContentID, thisProject.m_ps3Data.m_versionEU);

                    thisProject.closeLogStream();
                }

                if (thisProject.getJPNBuildValid(Platform.PS3))
                {
                    string ps3PreBuildDirJPN = ps3PrebuildsDir + "\\preBuildJPN";
                    string ps3DLCPackDirJPN = ps3PreBuildDirJPN + "\\USRDIR" + "\\dlc\\dlc_" + thisProject.m_title;

                    thisProject.openLogStream(ps3PkgDir + "\\" + jpnContentID + ".txt");

                    makeRbsAndRpfPS3(thisProject, ps3DLCPackDirJPN, ps3PreBuildDirJPN, ps3PkgDir, "JN");
                    makePS3Package(thisProject, ps3PreBuildDirJPN, ps3PkgDir, thisProject.m_ps3Data.m_JPNparamSFO, jpnContentID, thisProject.m_ps3Data.m_versionJN);

                    thisProject.closeLogStream();
                }

                // Add any newly created files
                checkoutDirectory(ps3PkgDir, null);

                Directory.Delete(ps3PrebuildsDir, true); // remove the temporary directory
            }
            catch (System.Exception ex)
            {
                OutputWindow.appendOutput(ex.Message, OutputWindow.MessageType.ERROR);
            }
        }

        private void doPostPS3Build(Project thisProject)
        {
            OutputWindow.appendOutput("Doing PS3 post-build", OutputWindow.MessageType.IMPORTANT);

            try
            {
                if (SettingsManager.GetInstance.m_ps3CtrlPath.Length > 0)
                {
                    string ps3PkgDir = thisProject.OutputDirectory + "\\ps3\\pkgs\\";
                    string usContentID = thisProject.getUSContentID(Platform.PS3) + ".pkg";
                    string euContentID = thisProject.getEUContentID(Platform.PS3) + ".pkg";
                    string jpnContentID = thisProject.getJPNContentID(Platform.PS3) + ".pkg";

                    foreach (DeployTarget target in SettingsManager.GetInstance.m_ps3Targets)
                    {
                        if (target.m_deploy)
                        {
                            if (SettingsManager.GetInstance.m_ps3DeployUS)
                                runAndDone(SettingsManager.GetInstance.m_ps3CtrlPath, "install-package -t " + target.m_ipAddress + " " + ps3PkgDir + usContentID, false, null, true);

                            if (SettingsManager.GetInstance.m_ps3DeployEU)
                                runAndDone(SettingsManager.GetInstance.m_ps3CtrlPath, "install-package -t " + target.m_ipAddress + " " + ps3PkgDir + euContentID, false, null, true);

                            if (SettingsManager.GetInstance.m_ps3DeployJN)
                                runAndDone(SettingsManager.GetInstance.m_ps3CtrlPath, "install-package -t " + target.m_ipAddress + " " + ps3PkgDir + jpnContentID, false, null, true);
                        }
                    }
                }

                runBuildTarget(thisProject.m_ps3Data.m_postBuildPath);
            }
            catch (System.Exception ex)
            {
                OutputWindow.appendOutput(ex.Message, OutputWindow.MessageType.ERROR);
            }
        }

        #endregion

        #region PS4

        private void writeGp4ContentNode(string currDir, string rootDir, XmlWriter gp4Writer, nodeType targetType)
        {
            string[] filesToEnumerate = Directory.GetFiles(currDir);
            string[] dirToEnumerate = Directory.GetDirectories(currDir);

            if (targetType == nodeType.NT_FILE)
            {
                foreach (string file in filesToEnumerate)
                {
                    try
                    {
                        gp4Writer.WriteStartElement("file");
                        {
                            string targetPath = file.Replace(rootDir, "");

                            if (targetPath != null && targetPath.Length > 0)
                            {
                                targetPath = targetPath.Replace("\\", "/");

                                if (targetPath[0] == '/')
                                    targetPath = targetPath.Remove(0, 1);
                            }

                            gp4Writer.WriteAttributeString("targ_path", targetPath);
                            gp4Writer.WriteAttributeString("orig_path", file);
                        }
                        gp4Writer.WriteEndElement();
                    }
                    catch (System.Exception ex)
                    {
                        OutputWindow.appendOutput(ex.Message, OutputWindow.MessageType.ERROR);
                    }
                }
            }

            foreach (string directory in dirToEnumerate)
            {
                try
                {
                    if (targetType == nodeType.NT_FOLDER)
                    {
                        gp4Writer.WriteStartElement("dir");
                        {
                            gp4Writer.WriteAttributeString("targ_name", Path.GetFileName(directory));
                            writeGp4ContentNode(directory, rootDir, gp4Writer, targetType);
                        }
                        gp4Writer.WriteEndElement();
                    }
                    else
                        writeGp4ContentNode(directory, rootDir, gp4Writer, targetType);
                }
                catch (System.Exception ex)
                {
                    OutputWindow.appendOutput(ex.Message, OutputWindow.MessageType.ERROR);
                }
            }
        }

        private void writeGp4File(Project projectToWrite, string rootDir, string tempLocation, string contentId)
        {
            try
            {
                XmlWriterSettings gp4WriteSettings = new XmlWriterSettings();
                Ps4Data ps4Data = projectToWrite.m_ps4Data;

                gp4WriteSettings.Indent = true;

                XmlWriter gp4Writer = XmlWriter.Create(tempLocation, gp4WriteSettings);
                {
                    gp4Writer.WriteStartDocument();
                    gp4Writer.WriteStartElement("psproject");
                    gp4Writer.WriteAttributeString("fmt", "gp4");
                    gp4Writer.WriteAttributeString("version", Globals.GP4_WRITER_VERSION);
                    {
                        gp4Writer.WriteStartElement("volume");
                        {
                            gp4Writer.WriteStartElement("volume_type");
                            {
                                gp4Writer.WriteValue(ps4Data.m_volumeType);
                            }
                            gp4Writer.WriteEndElement();

                            gp4Writer.WriteStartElement("volume_id");
                            {
                                gp4Writer.WriteValue("");
                            }
                            gp4Writer.WriteEndElement();

                            gp4Writer.WriteStartElement("volume_ts");
                            {
                                gp4Writer.WriteValue(DateTime.Now.ToString("yyyy-MM-dd HH:MM:ss"));
                            }
                            gp4Writer.WriteEndElement();

                            gp4Writer.WriteStartElement("package");
                            {
                                gp4Writer.WriteAttributeString("content_id", contentId);
                                gp4Writer.WriteAttributeString("passcode", ps4Data.m_passCode);
                            }
                            gp4Writer.WriteEndElement();
                        }
                        gp4Writer.WriteEndElement();

                        gp4Writer.WriteStartElement("files");
                        {
                            gp4Writer.WriteAttributeString("img_no", "0");
                            writeGp4ContentNode(rootDir, rootDir, gp4Writer, nodeType.NT_FILE);
                        }
                        gp4Writer.WriteEndElement();

                        gp4Writer.WriteStartElement("rootdir");
                        {
                            writeGp4ContentNode(rootDir, rootDir, gp4Writer, nodeType.NT_FOLDER);
                        }
                        gp4Writer.WriteEndElement();
                    }
                    gp4Writer.WriteEndElement();
                    gp4Writer.WriteEndDocument();
                    gp4Writer.Close();
                }
            }
            catch (System.Exception ex)
            {
                OutputWindow.appendOutput(ex.Message, OutputWindow.MessageType.ERROR);
            }
        }

        private void makePS4Package(Project thisProject, string ps4DLCPackDir, string ps4PreBuildDir, 
            string ps4PkgDir, string ps4Region, string contentId, string paramSfo)
        {
            string ps4TempRbsRpfScript = ps4DLCPackDir + "\\DLC.rbs";
            string ps4TempRpf = ps4DLCPackDir + "\\DLC.rpf";
            string ps4Gp4File = ps4PreBuildDir + "\\" + thisProject.m_title + "_" + ps4Region + ".gp4";
            string ps4SceSysDir = ps4DLCPackDir + "\\sce_sys";

            dirCreateIfNotExists(ps4DLCPackDir);
            dirCreateIfNotExists(ps4PreBuildDir);
            dirCreateIfNotExists(ps4SceSysDir);

            createContentRPF(ps4TempRbsRpfScript, ps4TempRpf, thisProject, Platform.PS4);
            File.Delete(ps4TempRbsRpfScript);

            copyFileRemoveReadOnly(thisProject.m_ps4Data.m_icon0, ps4SceSysDir + "\\icon0.png");
            copyFileRemoveReadOnly(paramSfo, ps4SceSysDir + "\\param.sfo");

            writeGp4File(thisProject, ps4DLCPackDir, ps4Gp4File, contentId);
            File.Copy(ps4Gp4File, ps4PkgDir + "\\" + thisProject.m_title + "_" + ps4Region + ".gp4", true);

            string ps4PkgArgs = "img_create --oformat pkg+subitem " + ps4Gp4File + " " + ps4PkgDir;
            runAndDone(SettingsManager.GetInstance.m_PS4MakePackagePath, ps4PkgArgs, true, thisProject.m_logStream, true);

            if (File.Exists(ps4TempRpf))
            {
                string ps4DLCRpfTarget = ps4PkgDir + "\\" + "DLC.rpf";
				var ps4BuildDlcTarget = Globals.TOOLS_CONFIG.CoreProject.Branches;
                if (File.Exists(ps4DLCRpfTarget))
                    CommandPrompt.perforceCheckout(ps4DLCRpfTarget);

                File.Copy(ps4TempRpf, ps4DLCRpfTarget, true);
                CommandPrompt.perforceCheckout(ps4DLCRpfTarget);
            }

            File.Delete(ps4TempRpf);
        }

        private bool doPrePS4Build(Project thisProject)
        {
            try
            {
                runBuildTarget(thisProject.m_ps4Data.m_preBuildPath);
            }
            catch (System.Exception ex)
            {
                OutputWindow.appendOutput(ex.Message, OutputWindow.MessageType.ERROR);
            }

            return true;
        }

        private void doPS4Build(Project thisProject)
        {
            try
            {
                string PS4PlatformName = BuildManager.GetDirectoryPlatformName(Platform.PS4);
                string usContentID = thisProject.getUSContentID(Platform.PS4);
                string euContentID = thisProject.getEUContentID(Platform.PS4);
                string jpnContentID = thisProject.getJPNContentID(Platform.PS4);
                string ps4PkgDir = thisProject.OutputDirectory + "\\" + PS4PlatformName + "\\pkgs";
                string ps4PrebuildsDir = thisProject.OutputDirectory + "\\" + PS4PlatformName + "\\preBuilds";

                OutputWindow.appendOutput("Doing PS4 build", OutputWindow.MessageType.IMPORTANT);

                dirCreateIfNotExists(ps4PkgDir);
                dirCreateIfNotExists(ps4PrebuildsDir);

                // Checkout any existing files
                checkoutDirectory(ps4PkgDir, null);

                if (thisProject.getUSBuildValid(Platform.PS4))
                {
                    string ps4PreBuildDirUS = ps4PrebuildsDir + "\\preBuildUS";
                    string ps4DLCPackDirUS = ps4PreBuildDirUS + "\\packData";
                    string ps4USPkgDir = ps4PkgDir + "\\US_subItems";
                    
                    dirCreateIfNotExists(ps4USPkgDir);

                    thisProject.openLogStream(ps4USPkgDir + "\\" + usContentID + ".txt");
                    {
                        makePS4Package(thisProject, ps4DLCPackDirUS, ps4PreBuildDirUS, ps4USPkgDir, "US", usContentID, thisProject.m_ps4Data.m_USparamSFO);
                    }
                    thisProject.closeLogStream();
                }

                if (thisProject.getEUBuildValid(Platform.PS4))
                {
                    string ps4PreBuildDirEU = ps4PrebuildsDir + "\\preBuildEU";
                    string ps4DLCPackDirEU = ps4PreBuildDirEU + "\\packData";
                    string ps4EUPkgDir = ps4PkgDir + "\\EU_subItems";

                    dirCreateIfNotExists(ps4EUPkgDir);

                    thisProject.openLogStream(ps4EUPkgDir + "\\" + euContentID + ".txt");
                    {
                        makePS4Package(thisProject, ps4DLCPackDirEU, ps4PreBuildDirEU, ps4EUPkgDir, "EU", euContentID, thisProject.m_ps4Data.m_EUparamSFO);
                    }
                    thisProject.closeLogStream();
                }

                if (thisProject.getJPNBuildValid(Platform.PS4))
                {
                    string ps4PreBuildDirJPN = ps4PrebuildsDir + "\\preBuildJPN";
                    string ps4DLCPackDirJPN = ps4PreBuildDirJPN + "\\packData";
                    string ps4JPNPkgDir = ps4PkgDir + "\\JPN_subItems";

                    dirCreateIfNotExists(ps4JPNPkgDir);

                    thisProject.openLogStream(ps4JPNPkgDir + "\\" + jpnContentID + ".txt");
                    {
                        makePS4Package(thisProject, ps4DLCPackDirJPN, ps4PreBuildDirJPN, ps4JPNPkgDir, "JN", jpnContentID, thisProject.m_ps4Data.m_JPNparamSFO);
                    }
                    thisProject.closeLogStream();
                }

                // Add any newly created files
                checkoutDirectory(ps4PkgDir, null);
                Directory.Delete(ps4PrebuildsDir, true); // remove the temporary directory
            }
            catch (System.Exception ex)
            {
                OutputWindow.appendOutput(ex.Message, OutputWindow.MessageType.ERROR);
            }
        }

        private void doPostPS4Build(Project thisProject)
        {
            OutputWindow.appendOutput("Doing PS4 post-build", OutputWindow.MessageType.IMPORTANT);

            try
            {
                if (SettingsManager.GetInstance.m_PS4CtrlPath.Length > 0)
                {
                    string ps4PkgDir = thisProject.OutputDirectory + "\\ps4\\pkgs\\";
                    string usPkgLocation = ps4PkgDir + "US_subItems\\" + thisProject.getUSContentID(Platform.PS4) + ".pkg";
                    string euPkgLocation = ps4PkgDir + "EU_subItems\\" + thisProject.getEUContentID(Platform.PS4) + ".pkg";
                    string jnPkgLocation = ps4PkgDir + "JPN_subItems\\" + thisProject.getJPNContentID(Platform.PS4) + ".pkg";

                    foreach (DeployTarget target in SettingsManager.GetInstance.m_PS4Targets)
                    {
                        if (target.m_deploy)
                        {
                            string ps4Ctrl = SettingsManager.GetInstance.m_PS4CtrlPath;

                            if (SettingsManager.GetInstance.m_ps4DeployUS)
                                runAndDone(ps4Ctrl, " pkg-install " + usPkgLocation + " " + target.m_ipAddress, false, null, true);

                            if (SettingsManager.GetInstance.m_ps4DeployEU)
                                runAndDone(ps4Ctrl, " pkg-install " + euPkgLocation + " " + target.m_ipAddress, false, null, true);

                            if (SettingsManager.GetInstance.m_ps4DeployJN)
                                runAndDone(ps4Ctrl, " pkg-install " + jnPkgLocation + " " + target.m_ipAddress, false, null, true);
                        }
                    }
                }

                runBuildTarget(thisProject.m_ps4Data.m_postBuildPath);
            }
            catch (System.Exception ex)
            {
                OutputWindow.appendOutput(ex.Message, OutputWindow.MessageType.ERROR);
            }
        }

        #endregion

        #region PC

        private bool doPrePCBuild(Project thisProject)
        {
            OutputWindow.appendOutput("Doing PC pre-build", OutputWindow.MessageType.IMPORTANT);

            try
            {
                runBuildTarget(thisProject.m_pcData.m_preBuildPath);
            }
            catch (System.Exception ex)
            {
                OutputWindow.appendOutput(ex.Message, OutputWindow.MessageType.ERROR);
            }

            return true;
        }

        private void doPCBuild(Project thisProject)
        {
            OutputWindow.appendOutput("Doing PC build", OutputWindow.MessageType.IMPORTANT);
        }

        private void doPostPCBuild(Project thisProject)
        {
            OutputWindow.appendOutput("Doing PC post-build", OutputWindow.MessageType.IMPORTANT);

            try
            {
                runBuildTarget(thisProject.m_pcData.m_postBuildPath);
            }
            catch (System.Exception ex)
            {
                OutputWindow.appendOutput(ex.Message, OutputWindow.MessageType.ERROR);
            }
        }

        #endregion

        #region Cloud

        private bool doPreCloudBuild(Project thisProject)
        {
            OutputWindow.appendOutput("Doing Cloud pre-build", OutputWindow.MessageType.IMPORTANT);

            try
            {
                // TOMW runBuildTarget(thisProject.m_pcData.m_preBuildPath);
            }
            catch (System.Exception ex)
            {
                OutputWindow.appendOutput(ex.Message, OutputWindow.MessageType.ERROR);
            }

            return true;
        }

        private void makeRbsAndRpfCloud(Project thisProject, string cloudPreBuildDir, string cloudBinDir, string contentId, Platform currPlatform)
        {
            string xboxTempRbsRpfScript = cloudPreBuildDir + "\\temp.rbs";
            string xboxTempRpf = cloudPreBuildDir + "\\temp.rpf";

            dirCreateIfNotExists(cloudPreBuildDir);
            createContentRPF(xboxTempRbsRpfScript, xboxTempRpf, thisProject, currPlatform);

            if (File.Exists(xboxTempRpf))
            {
                string newFileName = cloudBinDir + "\\" + contentId + ".rpf";

                if (File.Exists(newFileName))
                    CommandPrompt.perforceCheckout(newFileName);

                File.Copy(xboxTempRpf, newFileName, true);
                CommandPrompt.perforceCheckout(newFileName);
            }

            File.Delete(xboxTempRpf);
            File.Delete(xboxTempRbsRpfScript);
        }

        private void doCloudBuild(Project thisProject)
        {
            OutputWindow.appendOutput("Doing Cloud build", OutputWindow.MessageType.IMPORTANT);

            // Xbox 360
            {
                string Xbox360PlatformName = BuildManager.GetDirectoryPlatformName(Platform.Xbox360);
                string xboxPreBuildDir = thisProject.OutputDirectory + "\\" + Xbox360PlatformName + "\\preBuild";
                string xboxBinDir = thisProject.OutputDirectory + "\\" + Xbox360PlatformName + "\\bin";

                dirCreateIfNotExists(xboxBinDir);
                dirCreateIfNotExists(xboxPreBuildDir);

                makeRbsAndRpfCloud(thisProject, xboxPreBuildDir, xboxBinDir, thisProject.m_title, Platform.Xbox360);

                Directory.Delete(xboxPreBuildDir, true); // remove the temporary directory
            }

            // PS3
            {
                string PS3PlatformName = BuildManager.GetDirectoryPlatformName(Platform.PS3);
                string ps3PkgDir = thisProject.OutputDirectory + "\\" + PS3PlatformName + "\\pkgs";
                string ps3PrebuildsDir = thisProject.OutputDirectory + "\\" + PS3PlatformName + "\\preBuilds";

                dirCreateIfNotExists(ps3PkgDir);
                dirCreateIfNotExists(ps3PrebuildsDir);

                makeRbsAndRpfCloud(thisProject, ps3PrebuildsDir, ps3PkgDir, thisProject.m_title, Platform.PS3);

                Directory.Delete(ps3PrebuildsDir, true); // remove the temporary directory
            }

            // PS4
            {
                string PS4PlatformName = BuildManager.GetDirectoryPlatformName(Platform.PS4);
                string ps4PkgDir = thisProject.OutputDirectory + "\\" + PS4PlatformName + "\\pkgs";
                string ps4PrebuildsDir = thisProject.OutputDirectory + "\\" + PS4PlatformName + "\\preBuilds";

                dirCreateIfNotExists(ps4PkgDir);
                dirCreateIfNotExists(ps4PrebuildsDir);

                makeRbsAndRpfCloud(thisProject, ps4PrebuildsDir, ps4PkgDir, thisProject.m_title, Platform.PS4);

                Directory.Delete(ps4PrebuildsDir, true); // remove the temporary directory
            }
        }

        private void doPostCloudBuild(Project thisProject)
        {
            OutputWindow.appendOutput("Doing Cloud post-build", OutputWindow.MessageType.IMPORTANT);

            try
            {
                // TOMW runBuildTarget(thisProject.m_pcData.m_postBuildPath);
            }
            catch (System.Exception ex)
            {
                OutputWindow.appendOutput(ex.Message, OutputWindow.MessageType.ERROR);
            }
        }

        #endregion

        #endregion

        #region UI Events

        private void chkReleaseBuild_CheckedChanged(object sender, EventArgs e)
        {
            BuildManager.SetBuildType(chkReleaseBuild.Checked ? BuildManager.BuildType.RELEASE : BuildManager.BuildType.DEBUG);
            loadSolution();
        }

        private void btnPS4CodeGen_Click(object sender, EventArgs e)
        {
            txtPS4Code.Text = Globals.getRandomString(32);
        }

        private void mnuRefresh_Click(object sender, EventArgs e)
        {
            OutputWindow.appendOutput("Refreshing solution", OutputWindow.MessageType.IMPORTANT);
            loadSolution();
        }

        private void tvContentTree_DoubleClick(object sender, EventArgs e)
        {
            contentTreeNode hitNode = (contentTreeNode)tvContentTree.HitTest(tvContentTree.PointToClient(Cursor.Position)).Node;

            if (hitNode != null && hitNode.NodeType == nodeType.NT_FILE)
            {
                CommandPrompt.OpenPrompt();
                {
                    CommandPrompt.Run('"' + hitNode.FullFilePath + '"', false);
                }
                CommandPrompt.ClosePrompt(false, true);
            }
        }

        private void btnLogFilter_Click(object sender, EventArgs e)
        {
            OutputWindow.filterLog();
        }

        private void mnuClearOutputWnd_Click(object sender, EventArgs e)
        {
            OutputWindow.clearCurrent();
        }

        private void mnuOutWndClearAll_Click(object sender, EventArgs e)
        {
            OutputWindow.clearAll();
        }

        private void mnuClean_Click(object sender, EventArgs e)
        {
            if (Directory.Exists(Globals.OUTPUT_DIRECTORY))
            {
                string rootPath = Path.GetDirectoryName(Globals.OUTPUT_DIRECTORY);

                Directory.Delete(Globals.OUTPUT_DIRECTORY, true);

				// If directory is empty, delete it
                if (!Directory.EnumerateFileSystemEntries(rootPath).Any())
                    Directory.Delete(rootPath, true);

                OutputWindow.appendOutput("Cleaned " + Globals.OUTPUT_DIRECTORY, OutputWindow.MessageType.IMPORTANT);

                return;
            }

            OutputWindow.appendOutput("Solution is already clean!", OutputWindow.MessageType.IMPORTANT);
        }

        private void ctxtOpenExpHere_Click(object sender, EventArgs e)
        {
            contentTreeNode selectedNode = getSelectedNode();

            if (selectedNode != null && selectedNode.FullFilePath.Length > 0)
            {
                if (File.Exists(selectedNode.FullFilePath))
                    runAndDone("explorer.exe", Path.GetDirectoryName(selectedNode.FullFilePath), false, null, true);
                else if (Directory.Exists(selectedNode.FullFilePath))
                    runAndDone("explorer.exe", selectedNode.FullFilePath, false, null, true);
            }
        }

        private void mnuWikiOpt_Click(object sender, EventArgs e)
        {
            try
            {
                Process.Start(Globals.WIKI_PAGE_URL);
            }
            catch (System.Exception ex)
            {
                OutputWindow.appendOutput(ex.Message, OutputWindow.MessageType.ERROR);
            }
        }

        private void tsbOptions_Click(object sender, EventArgs e)
        {
            if (m_optionsForm == null || m_optionsForm.IsDisposed)
                m_optionsForm = new frmOptions();

            m_optionsForm.Show();
            readOptionValues();
        }

        private void tsbOpenContentFolder_Click(object sender, EventArgs e)
        {
            if (Directory.Exists(Globals.OUTPUT_DIRECTORY))
                runAndDone("explorer.exe", Globals.OUTPUT_DIRECTORY, false, null, true);
            else
                OutputWindow.appendOutput("Cannot locate output directory for this project!", OutputWindow.MessageType.ERROR);
        }

        private void ctxtBuildAll_Click(object sender, EventArgs e)
        {
            foreach (Project currProject in ProjectManager.GetInstance.Projects)
            {
                currProject.m_rootNode.setBuildStatus(true);
                ProjectManager.GetInstance.Dirty = true;
            }
        }

        private void ctxtBuildNone_Click(object sender, EventArgs e)
        {
            foreach (Project currProject in ProjectManager.GetInstance.Projects)
            {
                currProject.m_rootNode.setBuildStatus(false);
                ProjectManager.GetInstance.Dirty = true;
            }
        }

        private void ctxtToggleThisOnly_Click(object sender, EventArgs e)
        {
            toggleBuildStatusForNodes(getSelectedNode());
            ProjectManager.GetInstance.Dirty = true;
        }

        private void ctxtToggleBuildOnlyThis_Click(object sender, EventArgs e)
        {
            contentTreeNode selectedNode = getSelectedNode();

            foreach (Project currProject in ProjectManager.GetInstance.Projects)
            {
                currProject.m_rootNode.setBuildStatus(false);
                ProjectManager.GetInstance.Dirty = true;
            }

            setBuildStatusForNodes(selectedNode, true);
        }

        private void ctxtToggleBuildAllExceptThis_Click(object sender, EventArgs e)
        {
            contentTreeNode selectedNode = getSelectedNode();

            foreach (Project currProject in ProjectManager.GetInstance.Projects)
            {
                currProject.m_rootNode.setBuildStatus(true);
                ProjectManager.GetInstance.Dirty = true;
            }

            setBuildStatusForNodes(selectedNode, false);
        }

        private void toggleBuildStatusForNodes(contentTreeNode parentNode)
        {
            if (parentNode.NodeType == nodeType.NT_PROJECT_ROOT)
                parentNode.toggleBuildStatus();

            foreach (contentTreeNode childNode in parentNode.Nodes)
            {
                if (childNode.NodeType == nodeType.NT_PROJECT_ROOT)
                    childNode.toggleBuildStatus();
                else if (childNode.NodeType == nodeType.NT_FOLDER || childNode.NodeType == nodeType.NT_CONTENT_ROOT)
                    toggleBuildStatusForNodes(childNode);
            }
        }

        private void setBuildStatusForNodes(contentTreeNode parentNode, bool buildStatus)
        {
            if (parentNode.NodeType == nodeType.NT_PROJECT_ROOT)
                parentNode.setBuildStatus(buildStatus);

            foreach (contentTreeNode childNode in parentNode.Nodes)
            {
                if (childNode.NodeType == nodeType.NT_PROJECT_ROOT)
                    childNode.setBuildStatus(buildStatus);
                else if (childNode.NodeType == nodeType.NT_FOLDER || childNode.NodeType == nodeType.NT_CONTENT_ROOT)
                    setBuildStatusForNodes(childNode, buildStatus);
            }
        }

        public void clearSelectedNode()
        {
            m_currSelectedNode = null;
        }

        public contentTreeNode getSelectedNode()
        {
            return m_currSelectedNode;
        }

        public void setSelectedNode(contentTreeNode nodeToSelect)
        {
            if (nodeToSelect != null)
            {
                m_currSelectedNode = nodeToSelect;
            }
        }

        private void clearTxtField(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                if (MessageBox.Show("Are you sure?", "Clear field", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    TextBox tbSender = (TextBox)sender;

                    tbSender.Clear();
                }
            }
        }

        private void txtPS4Icon0_TextChanged(object sender, EventArgs e)
        {
            loadImage(txtPS4Icon0.Text, pbPS4Icon0, true, PixelFormat.Format24bppRgb);
            ProjectManager.GetInstance.Dirty = true;
        }

        private void txtXboxTitleImage_TextChanged(object sender, EventArgs e)
        {
            loadImage(txtXboxTitleImage.Text, pbXboxTitleImg, false, PixelFormat.DontCare);
            ProjectManager.GetInstance.Dirty = true;
        }

        private void txtXboxOffBan_TextChanged(object sender, EventArgs e)
        {
            loadImage(txtXboxOffBan.Text, pbXboxOffBan, false, PixelFormat.DontCare);
            ProjectManager.GetInstance.Dirty = true;
        }

        private void txtXboxDBIcon_TextChanged(object sender, EventArgs e)
        {
            loadImage(txtXboxDBIcon.Text, pbXboxDBIcon, false, PixelFormat.DontCare);
            ProjectManager.GetInstance.Dirty = true;
        }
		private void txtXB1StoreLogo_TextChanged(object sender, EventArgs e)
		{
            loadImage(txtXB1StoreLogo.Text, pbXB1StoreLogo, true, PixelFormat.DontCare);
			ProjectManager.GetInstance.Dirty = true;
		}
		private void txtXB1SmallLogo_TextChanged(object sender, EventArgs e)
		{
            loadImage(txtXB1VSmallLogo.Text, pbXB1SmallLogo, true, PixelFormat.DontCare);
			ProjectManager.GetInstance.Dirty = true;
		}
		private void txtXB1LiveLogo_TextChanged(object sender, EventArgs e)
		{
            loadImage(txtXB1VLiveLogo.Text, pbXB1LiveLogo, true, PixelFormat.DontCare);
			ProjectManager.GetInstance.Dirty = true;
		}
		private void txtXB1WideLogo_TextChanged(object sender, EventArgs e)
		{
            loadImage(txtXB1VWideLogo.Text, pbXB1WideLogo, true, PixelFormat.DontCare);
			ProjectManager.GetInstance.Dirty = true;
		}
        private void txtPs3Icon0_TextChanged(object sender, EventArgs e)
        {
            loadImage(txtPs3Icon0.Text, pbPs3Banner, true, PixelFormat.DontCare);
            ProjectManager.GetInstance.Dirty = true;
        }

        private bool loadImage(string imagePath, PictureBox destPictureBox, bool growToFit, PixelFormat reqPixFmt)
        {
            if (destPictureBox.Image != null)
            {
                destPictureBox.Image.Dispose();
                destPictureBox.Image = null;
            }

            if (imagePath.Length <= 0)
                return false;
            
            if (File.Exists(imagePath))
            {
                try
                {
                    using (Bitmap tempImage = new Bitmap(imagePath))
                    {
                        if (reqPixFmt != PixelFormat.DontCare && tempImage.PixelFormat != reqPixFmt)
                        {
                            OutputWindow.appendOutput("Invalid image pixel format! Required: " + reqPixFmt.ToString() +
                                " Supplied: " + tempImage.PixelFormat.ToString() + " Image: " + imagePath, OutputWindow.MessageType.ERROR);

                            return false;
                        }

                        if (!growToFit)
                        {
                            if (tempImage != null)
                            {
                                if (destPictureBox.Width != tempImage.Width || destPictureBox.Height != tempImage.Height)
                                {
                                    OutputWindow.appendOutput("Invalid image proportions! " + "Required: [W:" + destPictureBox.Width + " H:" + destPictureBox.Height + "]" +
                                        " Supplied: [W:" + tempImage.Width + " H:" + tempImage.Height + "] " + imagePath, OutputWindow.MessageType.ERROR);

                                    return false;
                                }
                            }
                        }
                    }

                    destPictureBox.Load(imagePath);

                    if (destPictureBox.Image != null)
                    {
                        if (growToFit)
                            destPictureBox.SizeMode = PictureBoxSizeMode.AutoSize;
                        else
                            destPictureBox.SizeMode = PictureBoxSizeMode.StretchImage;
                    }

                    return true;
                }
                catch (System.Exception ex)
                {
                    OutputWindow.appendOutput(ex.Message, OutputWindow.MessageType.ERROR);
                }
            }
            else
                OutputWindow.appendOutput("Image path does not exist! " + imagePath, OutputWindow.MessageType.ERROR);

            return false;
        }

        private void mnuBuildAllOpt_Click(object sender, EventArgs e)
        {
            int buildInclusions = 0;

            BuildManager.addBuildInclusion(ref buildInclusions, (int)Platform.Xbox360);
            BuildManager.addBuildInclusion(ref buildInclusions, (int)Platform.PS3);
            BuildManager.addBuildInclusion(ref buildInclusions, (int)Platform.Win32);
            BuildManager.addBuildInclusion(ref buildInclusions, (int)Platform.XboxOne);
            BuildManager.addBuildInclusion(ref buildInclusions, (int)Platform.PS4);

            doBuildSolution(buildInclusions, true);
        }

        private void mnuBuildXBOXOpt_Click(object sender, EventArgs e)
        {
            int buildInclusions = 0;

            BuildManager.addBuildInclusion(ref buildInclusions, (int)Platform.Xbox360);

            doBuildSolution(buildInclusions, false);
        }

        private void mnuBuildPS3Opt_Click(object sender, EventArgs e)
        {
            int buildInclusions = 0;

            BuildManager.addBuildInclusion(ref buildInclusions, (int)Platform.PS3);

            doBuildSolution(buildInclusions, false);
        }

        private void mnuBuildPCOpt_Click(object sender, EventArgs e)
        {
            int buildInclusions = 0;

            BuildManager.addBuildInclusion(ref buildInclusions, (int)Platform.Win32);

            doBuildSolution(buildInclusions, false);
        }

        private void mnuBuildCloudOpt_Click(object sender, EventArgs e)
        {
            int buildInclusions = 0;

            doBuildSolution(buildInclusions, true);
        }

        private void tsbBuildAll_Click(object sender, EventArgs e)
        {
            int buildInclusions = 0;

            BuildManager.addBuildInclusion(ref buildInclusions, (int)Platform.Xbox360);
            BuildManager.addBuildInclusion(ref buildInclusions, (int)Platform.PS3);
            BuildManager.addBuildInclusion(ref buildInclusions, (int)Platform.Win32);
            BuildManager.addBuildInclusion(ref buildInclusions, (int)Platform.XboxOne);
            BuildManager.addBuildInclusion(ref buildInclusions, (int)Platform.PS4);

            doBuildSolution(buildInclusions, true);
        }

        private void tsbBuildCloud_Click(object sender, EventArgs e)
        {
            int buildInclusions = 0;

            doBuildSolution(buildInclusions, true);
        }

        private void tsbBuildXbox_Click(object sender, EventArgs e)
        {
            int buildInclusions = 0;

            BuildManager.addBuildInclusion(ref buildInclusions, (int)Platform.Xbox360);

            doBuildSolution(buildInclusions, false);
        }

        private void tsbBuildPs3_Click(object sender, EventArgs e)
        {
            int buildInclusions = 0;

            BuildManager.addBuildInclusion(ref buildInclusions, (int)Platform.PS3);

            doBuildSolution(buildInclusions, false);
        }

        private void tsbBuildPC_Click(object sender, EventArgs e)
        {
            int buildInclusions = 0;

            BuildManager.addBuildInclusion(ref buildInclusions, (int)Platform.Win32);

            doBuildSolution(buildInclusions, false);
        }

        private void tsbBuildPS4_Click(object sender, EventArgs e)
        {
            int buildInclusions = 0;

            BuildManager.addBuildInclusion(ref buildInclusions, (int)Platform.PS4);

            doBuildSolution(buildInclusions, false);
        }

        private void tsbBuildXbone_Click(object sender, EventArgs e)
        {
            int buildInclusions = 0;

            BuildManager.addBuildInclusion(ref buildInclusions, (int)Platform.XboxOne);

            doBuildSolution(buildInclusions, false);
        }

        private void frmMain_Resize(object sender, EventArgs e)
        {
            tcAttributes.Invalidate();
        }

        private void showUniversalLog()
        {
            m_logProcess = RSG.Base.Logging.LogFactory.ShowUniversalLogViewer(RSG.Base.Logging.LogFactory.ApplicationLogFilename);
        }

        private void closeUniversalLog()
        {
            if (m_logProcess != null && !m_logProcess.HasExited)
            {
                m_logProcess.CloseMainWindow();

                if (!m_logProcess.WaitForExit(500))
                    m_logProcess.Kill();
            }
        }

        private static bool ms_shown = false;
        private void frmMain_Shown(object sender, EventArgs e)
        {
            // Just in case...
            if (!ms_shown)
                loadSolution();

            ms_shown = true;
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            m_optionsForm = new frmOptions();

            rtbOutput.ContextMenu = ctxtmnuOutputWnd;
            SettingsManager.readSettings();
            GlobalFieldsManager.readGlobalFields();
            FileTypeMappingMgr.readTypeMappings();
            coloriseFields();
            initialiseBankList();
            updateToolBarIcons();
            showUniversalLog();
            ProjectManager.GetInstance.Dirty = false;
        }

        private void frmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            SettingsManager.saveSettings();
            GlobalFieldsManager.saveGlobalFields();
            FileTypeMappingMgr.saveTypeMappings();

            if (ProjectManager.GetInstance.Dirty)
            {
                if (MessageBox.Show("Save changes?", "Exit", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                    saveSolution(false);
            }

            closeUniversalLog();
        }

        private void mnuExitOpt_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void tvContentTree_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                TreeViewHitTestInfo hitInfo = tvContentTree.HitTest(e.Location);
                contentTreeNode selectedNode = (contentTreeNode)hitInfo.Node;

                setSelectedNode(selectedNode);
            }
        }

        private void updateFileFolderInfoText(string name, long length)
        {
            txtBSize.Text = length + " bytes";
            txtKBSize.Text = (length / 1024) + " KB";
            txtMBSize.Text = (length / 1048576) + " MB";
        }

        private void readNodeContents(contentTreeNode nodeToRead, string nameOverride)
        {
            txtFullPath.Text = nodeToRead.FullFilePath;
            txtProjRelPath.Text = nodeToRead.ProjectRelativeFilePath;
            txtContentName.Text = (nameOverride != null) ? nameOverride : nodeToRead.Text;
            updateFileFolderInfoText(nodeToRead.Name, nodeToRead.Size);

            txtBuildInclusions.Text = nodeToRead.getIncludedBuildsText();
            txtBuildDeps.Text = "";
            txtBuildDependents.Text = "";

            if (nodeToRead.ContentProject != null)
            {
                foreach (Project depProj in nodeToRead.ContentProject.m_dependencies)
                    txtBuildDeps.Text += depProj.m_title + ", ";

                if (nodeToRead.ContentProject.m_dependentProj != null)
                    txtBuildDependents.Text = nodeToRead.ContentProject.m_dependentProj.m_title;
            }
        }

        private void tvContentTree_AfterSelect(object sender, TreeViewEventArgs e)
        {
            contentTreeNode selectedNode = (contentTreeNode)tvContentTree.SelectedNode;

            readNodeContents(selectedNode, null);

            if (selectedNode.NodeType != nodeType.NT_CONTENT_ROOT && selectedNode.ContentProject != null)
                ProjectManager.GetInstance.CurrentProject = selectedNode.ContentProject;
        }

        private void spContMainHorz_SplitterMoved(object sender, SplitterEventArgs e)
        {
            tcAttributes.Invalidate(true);
        }

        private void spContTopVert_SplitterMoved(object sender, SplitterEventArgs e)
        {
            tcAttributes.Invalidate(true);
        }

        public void decipherNodeImage(contentTreeNode node)
        {
            if (node == null)
                return;

            string expandIndex = "notInBuild";
            string collapseIndex = "notInBuild";

            switch (node.NodeType)
            {
                case nodeType.NT_CONTENT_ROOT:
                {
                    expandIndex = "dbRoot";
                    collapseIndex = "dbRoot";
                }
                break;

                case nodeType.NT_PROJECT_ROOT:
                {
                    Project contentProject = node.ContentProject;

                    if (contentProject != null)
                    {
                        if (contentProject.getIsInBuild())
                        {
                            expandIndex = "inBuild";
                            collapseIndex = "inBuild";
                        }
                        else
                        {
                            expandIndex = "notInBuild";
                            collapseIndex = "notInBuild";
                        }
                    }
                }
                break;

                case nodeType.NT_BRANCH_ROOT:
                case nodeType.NT_FOLDER:
                {
                    if (Directory.Exists(node.FullFilePath))
                    {
                        string imageKey = "folder";

                        if (!ilContentTree.Images.ContainsKey(imageKey))
                        {
                            Icon fileIcon = null;

                            fileIcon = Globals.ExtractIcon(node.FullFilePath);

                            if (fileIcon != null)
                                ilContentTree.Images.Add(imageKey, fileIcon);
                        }

                        if (ilContentTree.Images.ContainsKey(imageKey))
                        {
                            expandIndex = imageKey;
                            collapseIndex = imageKey;
                        }
                    }
                }
                break;

                case nodeType.NT_FILE:
                {
                    if (File.Exists(node.FullFilePath))
                    {
                        string imageKey = Path.GetExtension(node.FullFilePath);

                        if (!ilContentTree.Images.ContainsKey(imageKey))
                        {
                            Icon fileIcon = null;

                            fileIcon = Globals.ExtractIcon(node.FullFilePath);

                            if (fileIcon != null)
                                ilContentTree.Images.Add(imageKey, fileIcon);
                        }

                        if (ilContentTree.Images.ContainsKey(imageKey))
                        {
                            expandIndex = imageKey;
                            collapseIndex = imageKey;
                        }
                    }
                }
                break;
            }

            if (node.IsExpanded)
            {
                node.ImageIndex = ilContentTree.Images.IndexOfKey(expandIndex);
                node.SelectedImageIndex = ilContentTree.Images.IndexOfKey(expandIndex);
            }
            else
            {
                node.ImageIndex = ilContentTree.Images.IndexOfKey(collapseIndex);
                node.SelectedImageIndex = ilContentTree.Images.IndexOfKey(collapseIndex);
            }

            int numIncProjs = 0;
            string strToSet = string.Empty;
            txtBuildingProjs.Clear();

            foreach (Project currProj in ProjectManager.GetInstance.Projects)
            {
                if (currProj.getIsInBuild())
                {
                    numIncProjs++;
                    strToSet += numIncProjs + ": " + currProj.m_title + "\r\n";
                }
            }

            txtBuildingProjs.Text = strToSet;
        }

        private void btnBrowsePS4USParamSfo_Click(object sender, EventArgs e)
        {
            ofdContentData.Filter = "SFO files|*.sfo";

            if (ofdContentData.ShowDialog() == DialogResult.OK)
            {
                txtPS4USParamSfo.Text = ofdContentData.FileName;
            }
        }

        private void btnBrowsePS4EUParamSfo_Click(object sender, EventArgs e)
        {
            ofdContentData.Filter = "SFO files|*.sfo";

            if (ofdContentData.ShowDialog() == DialogResult.OK)
            {
                txtPS4EUParamSfo.Text = ofdContentData.FileName;
            }
        }

        private void btnBrowsePS4JPNParamSfo_Click(object sender, EventArgs e)
        {
            ofdContentData.Filter = "SFO files|*.sfo";

            if (ofdContentData.ShowDialog() == DialogResult.OK)
            {
                txtPS4JPNParamSfo.Text = ofdContentData.FileName;
            }
        }

        private void btnBrowsePs3EUParamSfo_Click(object sender, EventArgs e)
        {
            ofdContentData.Filter = "SFO files|*.sfo";

            if (ofdContentData.ShowDialog() == DialogResult.OK)
            {
                txtPs3EUParamSfo.Text = ofdContentData.FileName;
            }
        }

        private void btnBrowsePs3JPNParamSfo_Click(object sender, EventArgs e)
        {
            ofdContentData.Filter = "SFO files|*.sfo";

            if (ofdContentData.ShowDialog() == DialogResult.OK)
            {
                txtPs3JPNParamSfo.Text = ofdContentData.FileName;
            }
        }

        private void btnBrowsePs3ParamSfo_Click(object sender, EventArgs e)
        {
            ofdContentData.Filter = "SFO files|*.sfo";

            if (ofdContentData.ShowDialog() == DialogResult.OK)
            {
                txtPs3USParamSfo.Text = ofdContentData.FileName;
            }
        }

        private void btnBrowsePS4Icon0_Click(object sender, EventArgs e)
        {
            ofdContentData.Filter = "PNG files|*.png";

            if (ofdContentData.ShowDialog() == DialogResult.OK)
            {
                txtPS4Icon0.Text = ofdContentData.FileName;
            }
        }

        private void btnBrowsePs3Icon0_Click(object sender, EventArgs e)
        {
            ofdContentData.Filter = "PNG files|*.png";

            if (ofdContentData.ShowDialog() == DialogResult.OK)
            {
                txtPs3Icon0.Text = ofdContentData.FileName;
            }
        }

        private void btnBrowseXboxTitleImg_Click(object sender, EventArgs e)
        {
            ofdContentData.Filter = "PNG files|*.png";

            if (ofdContentData.ShowDialog() == DialogResult.OK)
            {
                txtXboxTitleImage.Text = ofdContentData.FileName;
            }
        }

		private void btnBrowseXboxOneStoreImg_Click(object sender, EventArgs e)
		{
			ofdContentData.Filter = "PNG files|*.png";

			if (ofdContentData.ShowDialog() == DialogResult.OK)
			{
				txtXB1StoreLogo.Text = ofdContentData.FileName;
			}
		}

		private void btnBrowseXboxOneLiveImg_Click(object sender, EventArgs e)
		{
			ofdContentData.Filter = "PNG files|*.png";

			if (ofdContentData.ShowDialog() == DialogResult.OK)
			{
				txtXB1VLiveLogo.Text = ofdContentData.FileName;
			}
		}
		private void btnBrowseXboxOneSmallImg_Click(object sender, EventArgs e)
		{
			ofdContentData.Filter = "PNG files|*.png";

			if (ofdContentData.ShowDialog() == DialogResult.OK)
			{
				txtXB1VSmallLogo.Text = ofdContentData.FileName;
			}
		}
		private void btnBrowseXboxOneWideImg_Click(object sender, EventArgs e)
		{
			ofdContentData.Filter = "PNG files|*.png";

			if (ofdContentData.ShowDialog() == DialogResult.OK)
			{
				txtXB1VWideLogo.Text = ofdContentData.FileName;
			}
		}
        private void btnBrowseXboxDBIcon_Click(object sender, EventArgs e)
        {
            ofdContentData.Filter = "PNG files|*.png";

            if (ofdContentData.ShowDialog() == DialogResult.OK)
            {
                txtXboxDBIcon.Text = ofdContentData.FileName;
            }
        }

        private void btnBrowseXboxOfferBan_Click(object sender, EventArgs e)
        {
            ofdContentData.Filter = "PNG files|*.png";

            if (ofdContentData.ShowDialog() == DialogResult.OK)
            {
                txtXboxOffBan.Text = ofdContentData.FileName;
            }
        }

        private void chkIntForMktPlc_CheckedChanged(object sender, EventArgs e)
        {
            if (chkIntForMktPlc.Checked)
            {
                chkProfTrans.Checked = true;
                chkDevTrans.Checked = true;
            }

            chkProfTrans.Enabled = !chkIntForMktPlc.Checked;
            chkDevTrans.Enabled = !chkIntForMktPlc.Checked;
        }

        private void tsbGotoConfigFiles_Click(object sender, EventArgs e)
        {
            runAndDone("explorer.exe", Application.StartupPath, false, null, true);
        }

        #endregion

        #region Toolbar UI

        private void updateToolBarIcons()
        {
            tsbBuildSetupAndContent.Checked = SettingsManager.GetInstance.m_generateSetupAndContent;
        }

        private void setGenerateSetupAndContent(bool value)
        {
            if (SettingsManager.GetInstance.m_generateSetupAndContent == false && value == true)
            {
                MessageBox.Show("Please carefully verify that the generated XMLs are as expected with this feature enabled.",
                    "Enabling auto generate setup and content XMLs", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

            SettingsManager.GetInstance.m_generateSetupAndContent = value;
            updateToolBarIcons();
            loadSolution();
        }

        private void tsbBuildSetupAndContent_Click(object sender, EventArgs e)
        {
            setGenerateSetupAndContent(!SettingsManager.GetInstance.m_generateSetupAndContent);
        }

        private void tsbBuildXMLs_Click(object sender, EventArgs e)
        {
            OutputWindow.appendOutput("Begin setup2 and content XML build", OutputWindow.MessageType.IMPORTANT);
            {
                setGenerateSetupAndContent(true);

                List<Project> selectedProjects = new List<Project>();

                foreach (Project thisProject in ProjectManager.GetInstance.Projects)
                {
                    if (thisProject.getIsInBuild())
                        selectedProjects.Add(thisProject);
                }

                fillProjectsNodes(selectedProjects.ToArray());

                foreach (Project thisProject in ProjectManager.GetInstance.Projects)
                {
                    OutputWindow.pushActiveChannel(thisProject.m_channelId);

                    if (thisProject.getIsInBuild())
                    {
                        int buildInclusions = 0;

                        BuildManager.addBuildInclusion(ref buildInclusions, (int)Platform.PS3);
                        BuildManager.addBuildInclusion(ref buildInclusions, (int)Platform.PS4);

                        generateBuildXMLs(thisProject, buildInclusions);
                    }
                }
            }
            OutputWindow.appendOutput("End setup2 and content XML build", OutputWindow.MessageType.IMPORTANT);
        }

        #endregion

        #region Solution UI

        private void clearControls(Control parentControl)
        {
            foreach (Control ctrl in parentControl.Controls)
            {
                if (ctrl is TextBoxBase)
                {
                    TextBox txtCtrl = (TextBox)ctrl;

                    txtCtrl.Clear();
                }
                else if (ctrl is PictureBox)
                {
                    PictureBox pbToClear = (PictureBox)ctrl;

                    if (pbToClear.Image != null)
                        pbToClear.Image.Dispose();

                    pbToClear.InitialImage = null;
                    pbToClear.Image = null;
                    pbToClear.ImageLocation = null;
                }
            }
        }

        private void clearTreeView()
        {
            tvContentTree.Nodes.Clear();
        }

        private void clearFormData()
        {
            clearControls(tabpgPC);
            clearControls(tabpgXbox);
            clearControls(tabpgPS3);
            clearControls(tbPS4);
        }

        public void updateTitleText()
        {
            string currProjTitle = string.Empty;

            if (ProjectManager.GetInstance.CurrentProject != null)
                currProjTitle = " :: <" + ProjectManager.GetInstance.CurrentProject.m_title + ">";

            if (ProjectManager.GetInstance.Dirty)
                this.Text = "ContentStar - <" + ProjectManager.GetInstance.SolutionTitle + ">*" + currProjTitle;
            else
                this.Text = "ContentStar - <" + ProjectManager.GetInstance.SolutionTitle + ">" + currProjTitle;
        }

        private void saveSolution(bool saveAs)
        {
            try
            {
                copyProjectDataFromForm(ProjectManager.GetInstance.CurrentProject);
                ProjectManager.GetInstance.saveProjects();
                ProjectManager.GetInstance.Dirty = false;
            }
            catch (System.Exception ex)
            {
                OutputWindow.appendOutput(ex.Message, OutputWindow.MessageType.ERROR);
            }
        }

        private void createNodesFromDirectory(string directoryPath, Project contentProject, bool copyingFromLink, 
            List<string> setupsToMerge, List<string> contentsToMerge, IBranch branch, bool prePass = false)
        {
            try
            {
                if (Directory.Exists(directoryPath))
                {
                    string[] dirToEnumerate = Directory.GetDirectories(directoryPath);
                    List<string> filesToEnumerate = Directory.GetFiles(directoryPath).ToList();
                    contentTreeNode newNode = null;

                    // Only get the directories in a prepass
                    if(!prePass)
                    {
                        // Hack here to remove some very, very special files.
                        for (int i = 0; i < filesToEnumerate.Count; i++)
                        {
                            if (contentProject.isFullyExcluded(filesToEnumerate[i], BuildManager.GetBuildType()))
                            {
                                filesToEnumerate.RemoveAt(i);
                                i--;
                            }
                        }

                        if (contentProject.m_compatPack && copyingFromLink)
                        {
                            bool addFile = true;
                            string dataTypeName = string.Empty;
                            string fileName = string.Empty;

                            foreach (string filePath in filesToEnumerate)
                            {
                                fileName = Path.GetFileName(filePath);
                                addFile = true;
                                dataTypeName = string.Empty;

                                try
                                {
                                    MetaFile metaFile = new MetaFile(filePath, Globals.META_DICTIONARY);

                                    if (metaFile != null && metaFile.Definition != null)
                                    {
                                        dataTypeName = metaFile.Definition.DataType;
                                        addFile = !Globals.EXCLUDE_META_TAGS.Contains(dataTypeName);

                                        if (contentProject.m_fileFilters != null && contentProject.m_fileFilters.isMetaTagFiltered(dataTypeName))
                                            addFile = false;
                                    }
                                }
                                catch (System.Exception /*ex*/) { }

                                if (contentProject.m_fileFilters != null && contentProject.m_fileFilters.isFileNameFiltered(fileName))
                                    addFile = false;

                                if (addFile)
                                    newNode = addNodeToContentTree(nodeType.NT_FILE, null, Path.GetFileName(filePath), filePath, false, contentProject, branch);
                                else
                                {
                                    if (setupsToMerge != null && dataTypeName == Globals.SETUP_TAG_HASH)
                                        setupsToMerge.Add(filePath);
                                    else if (contentsToMerge != null && dataTypeName == Globals.CONTENT_TAG_HASH)
                                        contentsToMerge.Add(filePath);

                                    OutputWindow.appendOutput("Stripping file from " + contentProject.m_title + " " + filePath, OutputWindow.MessageType.IMPORTANT);
                                }
                            }
                        }
                        else
                        {
                            foreach (string filePath in filesToEnumerate)
                                newNode = addNodeToContentTree(nodeType.NT_FILE, null, Path.GetFileName(filePath), filePath, false, contentProject, branch);
                        }
                    }
                    foreach (string dirPath in dirToEnumerate)
                    {
                        if (prePass)
                        {
                            m_progressForm.IncrementMaximum();
                            
                            createNodesFromDirectory(dirPath, contentProject, copyingFromLink, setupsToMerge, contentsToMerge, branch, true);
                        }
                        else
                        {
                            m_progressForm.IncrementProgress();

                            newNode = addNodeToContentTree(nodeType.NT_FOLDER, null, Path.GetFileName(dirPath), dirPath, false, contentProject, branch);

                            setSelectedNode(newNode);

                            createNodesFromDirectory(dirPath, contentProject, copyingFromLink, setupsToMerge, contentsToMerge, branch);
                            setSelectedNode((contentTreeNode)newNode.Parent);
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                OutputWindow.appendOutput(ex.Message, OutputWindow.MessageType.ERROR);
            }
        }

        public void createCompatPack(Project currProj, contentTreeNode currProjNode)
        {
            string buildParentDir = Path.GetDirectoryName(currProj.m_toolsProject.DefaultBranch.Build);

            if (Directory.Exists(buildParentDir))
            {
                string[] sourceDirChildDirs = Directory.GetDirectories(buildParentDir);
                string[] sourceDirFiles = Directory.GetFiles(buildParentDir, "*.lnk");
                FileFilters compatFilters = FileFilters.readFiltersFile(buildParentDir + "\\" + Globals.FILE_FILTERS_NAME);

                currProj.m_fileFilters = compatFilters;

                foreach (string filePath in sourceDirFiles)
                {

                    if (File.Exists(filePath))
                    {
                        CommandPrompt.perforceCheckout(filePath);

                        string pathOnly = Path.GetDirectoryName(filePath);
                        string filenameOnly = Path.GetFileName(filePath);
                        Folder folder = Globals.SHELL.NameSpace(pathOnly);
                        FolderItem folderItem = folder.ParseName(filenameOnly);

                        if (folderItem != null)
                        {
                            ShellLinkObject link = (ShellLinkObject)folderItem.GetLink;
                            string linkTarget = link.Path.ToLower();

                            if (Directory.Exists(linkTarget))
                            {
                                Project projDependency = ProjectManager.GetInstance.GetProject(Globals.getStringHashCode(linkTarget));

                                if (projDependency != null)
                                {
                                    if (!projDependency.m_compatPack)
                                    {
                                        OutputWindow.appendOutput("Merging target data from " + linkTarget +
                                        " into compatibility pack data " + currProj.m_toolsProject.DefaultBranch.Build, OutputWindow.MessageType.IMPORTANT);

                                        if (projDependency != null)
                                        {
                                            currProj.addDependency(projDependency);
                                            projDependency.setDependent(currProj);
                                        }

                                        setSelectedNode(currProjNode);
                                        createNodesFromDirectory(linkTarget, currProj, true, currProj.m_setupsToMerge, currProj.m_contentsToMerge, null);
                                    }
                                    else
                                        OutputWindow.appendOutput("Compat pack is dependency of compat pack! Remove link to " + projDependency.m_title, OutputWindow.MessageType.ERROR);
                                }
                                else
                                    OutputWindow.appendOutput("Bad link in compat pack! " + currProj.m_title + " link target " + linkTarget, OutputWindow.MessageType.ERROR);
                            }
                        }
                    }
                }
            }
        }

        private void setupRequiredFolders(Project currProj)
        {
            bool foundDir = false;
            contentTreeNode newFolderNode = null;
            TreeNodeCollection currNodeCollection = tvContentTree.Nodes;
            List<string> dirNodes = Globals.getDirectoriesAbove(currProj.m_fullPath);
            string fullPath = Path.GetPathRoot(currProj.m_fullPath);

            // Setup the required folders for this project to sit in...
            foreach (string currDir in dirNodes)
            {
                foundDir = false;
                fullPath += currDir + "\\";

                foreach (TreeNode currNode in currNodeCollection)
                {
                    if (String.Compare(currNode.Text, currDir, true) == 0)
                    {
                        currNodeCollection = currNode.Nodes;
                        foundDir = true;
                        setSelectedNode((contentTreeNode)currNode);
                        break;
                    }
                }

                if (!foundDir)
                {
                    newFolderNode = addNodeToContentTree(nodeType.NT_FOLDER, null, currDir, fullPath, false, null, null);
                    currNodeCollection = newFolderNode.Nodes;
                    setSelectedNode(newFolderNode);
                }
            }
        }

        public void fillProjectsNodes(Project[] projects)
        {
            bool updateTreeView = false;
            contentTreeNode selectedNode = getSelectedNode();

            tvContentTree.BeginUpdate();

            {
                foreach (Project currProj in projects)
                {
                    if (!currProj.m_nodesInitialised)
                    {
                        updateTreeView = true;
                        currProj.m_nodesInitialised = true;

                        List<IBranch> validBranches = currProj.getValidBranches();

                        // Progress form to show while waiting for the tree to be built.
                        m_progressForm.ResetProgress(0, "Filling " + currProj.m_title + " nodes", false);

                        // Do a pre-pass to calculate the directory count.
                        foreach (IBranch currBranch in validBranches)
                        {
                            createNodesFromDirectory(currBranch.Build, currProj, false, null, null, currBranch, true);
                        }

                        // Quick hack - don't show progress bars that would appear too briefly.
                        if (m_progressForm.GetMaximum() > 90)
                        {
                            m_progressForm.ShowProgress();
                        }

                        foreach (IBranch currBranch in validBranches)
                        {
                            contentTreeNode currBranchRoot = null;

                            setSelectedNode(currProj.m_rootNode);
                            currBranchRoot = addNodeToContentTree(nodeType.NT_BRANCH_ROOT, null, 
                                Path.GetFileName(currBranch.Build), currBranch.Build, false, null, currBranch);
                            setSelectedNode(currBranchRoot);

                            createNodesFromDirectory(currBranch.Build, currProj, false, null, null, currBranch);
                            setSelectedNode(currProj.m_rootNode);
                        };
                        m_progressForm.Update();

                        if (SettingsManager.GetInstance.m_generateSetupAndContent && currProj.m_compatPack)
                        {
                            createCompatPack(currProj, currProj.m_rootNode);
                        }
                        currProj.m_rootNode.Collapse(false);

                        // Hide the progress form.
                        m_progressForm.Hide();
                    }
                }
            }
            tvContentTree.EndUpdate();

            if (updateTreeView)
            {
                crawlSolution();
                setSelectedNode(selectedNode);
            }
        }

        public void loadSolution()
        {
            try
            {
                OutputWindow.reset();

                tvContentTree.Enabled = false;
                tvContentTree.BeginUpdate();
                {
                    contentTreeNode newSolNode = null;

                    clearSelectedNode();
                    clearTreeView();
                    clearFormData();
                    updateTitleText();
                    resetTreeOperations();
                    readOptionValues();

                    newSolNode = addNodeToContentTree(nodeType.NT_CONTENT_ROOT, null, 
                        ProjectManager.GetInstance.SolutionTitle, ProjectManager.GetInstance.SolutionRoot, false, null, null);

                    ProjectManager.GetInstance.readProjects();

                    foreach (Project currProject in ProjectManager.GetInstance.Projects)
                    {
                        try
                        {
                            setupRequiredFolders(currProject);
                            contentTreeNode projNode = addNodeToContentTree(nodeType.NT_PROJECT_ROOT, null, currProject.m_title,
                                currProject.m_toolsProject.Root, false, currProject, null);
                            setSelectedNode(projNode);
                        }
                        catch (System.Exception ex)
                        {
                            OutputWindow.appendOutput(ex.Message, OutputWindow.MessageType.ERROR);
                        }
                    }

                    crawlSolution();
                    Globals.fixupNodeText(newSolNode);
                    ProjectManager.GetInstance.Dirty = false;
                    btnCollapseAll_Click(null, null);
                }
                tvContentTree.EndUpdate();
                tvContentTree.Enabled = true;
                System.GC.Collect();
            }
            catch (System.Exception ex)
            {
                OutputWindow.appendOutput(ex.Message, OutputWindow.MessageType.ERROR);
            }
        }

        private void mnuSaveOpt_Click(object sender, EventArgs e)
        {
            saveSolution(false);
        }

        public void updateUIFromDirtyState()
        {
            coloriseFields();
            updateTitleText();
        }

        private void setSolutionDirty(object sender, EventArgs e)
        {
            ProjectManager.GetInstance.Dirty = true;
        }

        private void coloriseFields()
        {
            txtXboxTitleID.BackColor = Color.Gold;
            txtXboxGameTitle.BackColor = Color.Gold;

            txtPs3USCID_First.BackColor = Color.Gold;
            txtPs3USCID_Second.BackColor = Color.Gold;

            txtPs3EUCID_First.BackColor = Color.Gold;
            txtPs3EUCID_Second.BackColor = Color.Gold;

            txtPs3JPNCID_First.BackColor = Color.Gold;
            txtPs3JPNCID_Second.BackColor = Color.Gold;

            txtPs3KLicensee.BackColor = Color.Gold;
        }

        #endregion

        #region Project data UI

        public void updateCurrentProjectFields()
        {
            if (ProjectManager.GetInstance.CurrentProject != null)
                onSelectedProjectChanged(null, ProjectManager.GetInstance.CurrentProject);
        }

        private void copyProjectDataFromForm(Project thisProject)
        {
            if (thisProject != null)
            {
                #region PS3

                thisProject.m_ps3Data.m_UScontentIDThird = txtPs3USCID_Third.Text;
                thisProject.m_ps3Data.m_USparamSFO = txtPs3USParamSfo.Text;

                thisProject.m_ps3Data.m_EUcontentIDThird = txtPs3EUCID_Third.Text;
                thisProject.m_ps3Data.m_EUparamSFO = txtPs3EUParamSfo.Text;

                thisProject.m_ps3Data.m_JPNcontentIDThird = txtPs3JPNCID_Third.Text;
                thisProject.m_ps3Data.m_JPNparamSFO = txtPs3JPNParamSfo.Text;

                thisProject.m_ps3Data.m_drmType = txtPs3DRMType.Text;
                thisProject.m_ps3Data.m_contentType = txtPs3ContType.Text;
                thisProject.m_ps3Data.m_versionEU = txtPs3EUVer.Text;
                thisProject.m_ps3Data.m_versionUS = txtPs3USVer.Text;
                thisProject.m_ps3Data.m_versionJN = txtPs3JNVer.Text;
                thisProject.m_ps3Data.m_icon0 = txtPs3Icon0.Text;

                #endregion

                #region XBOX

                thisProject.m_xboxData.m_packageName = txtXboxPkgName.Text;
                thisProject.m_xboxData.m_titleImage = txtXboxTitleImage.Text;
                thisProject.m_xboxData.m_dashboardIcon = txtXboxDBIcon.Text;
                thisProject.m_xboxData.m_offerBanner = txtXboxOffBan.Text;
                thisProject.m_xboxData.m_publisherFlags = txtXboxPubFlags.Text;
                thisProject.m_xboxData.m_activationDate = dtPickerXboxActDate.Value.Month + "/" + dtPickerXboxActDate.Value.Day + "/" + dtPickerXboxActDate.Value.Year;
                thisProject.m_xboxData.m_offeringID = txtXboxOffID.Text;
                thisProject.m_xboxData.m_hasCost = chkHasCost.Checked;
                thisProject.m_xboxData.m_isMarketPlace = chkIntForMktPlc.Checked;
                thisProject.m_xboxData.m_allowProfileTransfer = chkProfTrans.Checked;
                thisProject.m_xboxData.m_allowDeviceTransfer = chkDevTrans.Checked;
                thisProject.m_xboxData.m_licenseMask = txtLicenseMask.Text;
                thisProject.m_xboxData.m_contentCategory = txtContentCat.Text;
                thisProject.m_xboxData.m_previewOfferID = txtPrevOfferId.Text;
                thisProject.m_xboxData.m_offerID = txtOfferId.Text;
                thisProject.m_xboxData.m_consumable = chkConsumableOffer.Checked;
                thisProject.m_xboxData.m_visibility = cboVisibility.SelectedIndex + 1;
                thisProject.m_xboxData.m_licenseLevel = chkRestrictedLicense.Checked ? 2 : 1;

                #endregion

                #region PS4

                thisProject.m_ps4Data.m_UScontentIDThird = txtPS4USContID_Third.Text;
                thisProject.m_ps4Data.m_USparamSFO = txtPS4USParamSfo.Text;

                thisProject.m_ps4Data.m_EUcontentIDThird = txtPS4EUContID_Third.Text;
                thisProject.m_ps4Data.m_EUparamSFO = txtPS4EUParamSfo.Text;

                thisProject.m_ps4Data.m_JPNcontentIDThird = txtPS4JPNContID_Third.Text;
                thisProject.m_ps4Data.m_JPNparamSFO = txtPS4JPNParamSfo.Text;

                thisProject.m_ps4Data.m_versionEU = txtPS4VerEU.Text;
                thisProject.m_ps4Data.m_versionUS = txtPS4VerUS.Text;
                thisProject.m_ps4Data.m_versionJN = txtPS4VerJPN.Text;
                thisProject.m_ps4Data.m_icon0 = txtPS4Icon0.Text;
                thisProject.m_ps4Data.m_passCode = txtPS4Code.Text;
                thisProject.m_ps4Data.m_volumeType = txtPS4VolType.Text;

                #endregion

				#region XBONE
				thisProject.m_xboneData.AppManifest.Extensions.PackageExtension.ContentPackage.AllowedProductId.Id = Globals.XB1_TEST_PRODUCT_ID;
				CContentPackageVisualElements visualElems = thisProject.m_xboneData.AppManifest.Extensions.PackageExtension.ContentPackage.ContentPackageVisualElements;
				thisProject.m_xboneData.AppManifest.Identity.Version = txtXB1IdentityVersion.Text;
				thisProject.m_xboneData.AppManifest.Identity.Name = txtXB1IdentityName.Text;
				thisProject.m_xboneData.AppManifest.Properties.Description = txtXB1PDescription.Text;
				thisProject.m_xboneData.AppManifest.Properties.DisplayName = txtXB1PDisplayName.Text;
				thisProject.m_xboneData.AppManifest.Properties.PublisherDisplayName = txtXB1PPublisherName.Text;
				thisProject.m_xboneData.AllowedProductID = txtXB1AllowedProductID.Text;
				thisProject.m_xboneData.AllowedProductIdJPN = txtXB1AllowedProductIDJPN.Text;
				thisProject.m_xboneData.BuildDebug = rbXB1Dev.Checked;
				CContentPackageVisualElements vElems = thisProject.m_xboneData.AppManifest.Extensions.PackageExtension.ContentPackage.ContentPackageVisualElements;
				vElems.Description = txtXB1VDescription.Text;
				vElems.DisplayName = txtXB1VDisplayName.Text;
				vElems.Logo=txtXB1VLiveLogo.Text;
				vElems.SmallLogo= txtXB1VSmallLogo.Text;
				vElems.WideLogo=txtXB1VWideLogo.Text;
				thisProject.m_xboneData.AppManifest.Properties.Logo = txtXB1StoreLogo.Text;
				#endregion
            }
        }

        public void onSelectedProjectChanged(Project previousProject, Project currentProject)
        {
            readOptionValues();
            copyProjectDataFromForm(previousProject);
            populateBankList();

            if (currentProject != null)
            {
                GlobalFieldsManager gfMan = GlobalFieldsManager.GetInstance;

                clearFormData();

                #region PS3

                txtPs3USCID_First.Text = gfMan.getGlobalFieldsFromBank(currentProject.m_globalBankName).m_UScontentIDFirst;
                txtPs3USCID_Second.Text = gfMan.getGlobalFieldsFromBank(currentProject.m_globalBankName).m_UScontentIDSecond;
                txtPs3USCID_Third.Text = currentProject.m_ps3Data.m_UScontentIDThird;
                txtPs3USParamSfo.Text = currentProject.m_ps3Data.m_USparamSFO;

                txtPs3EUCID_First.Text = gfMan.getGlobalFieldsFromBank(currentProject.m_globalBankName).m_EUcontentIDFirst;
                txtPs3EUCID_Second.Text = gfMan.getGlobalFieldsFromBank(currentProject.m_globalBankName).m_EUcontentIDSecond;
                txtPs3EUCID_Third.Text = currentProject.m_ps3Data.m_EUcontentIDThird;
                txtPs3EUParamSfo.Text = currentProject.m_ps3Data.m_EUparamSFO;

                txtPs3JPNCID_First.Text = gfMan.getGlobalFieldsFromBank(currentProject.m_globalBankName).m_JPNcontentIDFirst;
                txtPs3JPNCID_Second.Text = gfMan.getGlobalFieldsFromBank(currentProject.m_globalBankName).m_JPNcontentIDSecond;
                txtPs3JPNCID_Third.Text = currentProject.m_ps3Data.m_JPNcontentIDThird;
                txtPs3JPNParamSfo.Text = currentProject.m_ps3Data.m_JPNparamSFO;

                txtPs3DRMType.Text = currentProject.m_ps3Data.m_drmType;
                txtPs3ContType.Text = currentProject.m_ps3Data.m_contentType;
                txtPs3EUVer.Text = currentProject.m_ps3Data.m_versionEU;
                txtPs3USVer.Text = currentProject.m_ps3Data.m_versionUS;
                txtPs3JNVer.Text = currentProject.m_ps3Data.m_versionJN;
                txtPs3KLicensee.Text = gfMan.getGlobalFieldsFromBank(currentProject.m_globalBankName).m_kLicensee;
                txtPs3Icon0.Text = currentProject.m_ps3Data.m_icon0;

                #endregion

                #region XBOX

                DateTime recordedDate;
                CultureInfo culture = CultureInfo.CreateSpecificCulture("en-US");
                DateTimeStyles styles = DateTimeStyles.None;

                if (DateTime.TryParse(currentProject.m_xboxData.m_activationDate, culture, styles, out recordedDate))
                    dtPickerXboxActDate.Value = recordedDate;

                txtXboxGameTitle.Text = gfMan.getGlobalFieldsFromBank(currentProject.m_globalBankName).m_gameTitle;
                txtXboxPkgName.Text = currentProject.m_xboxData.m_packageName;
                txtXboxTitleImage.Text = currentProject.m_xboxData.m_titleImage;
                txtXboxDBIcon.Text = currentProject.m_xboxData.m_dashboardIcon;
                txtXboxOffBan.Text = currentProject.m_xboxData.m_offerBanner;
                txtXboxPubFlags.Text = currentProject.m_xboxData.m_publisherFlags;
                txtXboxTitleID.Text = gfMan.getGlobalFieldsFromBank(currentProject.m_globalBankName).m_titleID;
                txtXboxOffID.Text = currentProject.m_xboxData.m_offeringID;
                chkHasCost.Checked = currentProject.m_xboxData.m_hasCost;
                chkIntForMktPlc.Checked = currentProject.m_xboxData.m_isMarketPlace;
                chkProfTrans.Checked = currentProject.m_xboxData.m_allowProfileTransfer;
                chkDevTrans.Checked = currentProject.m_xboxData.m_allowDeviceTransfer;
                txtLicenseMask.Text = currentProject.m_xboxData.m_licenseMask;
                txtContentCat.Text = currentProject.m_xboxData.m_contentCategory;
                txtPrevOfferId.Text = currentProject.m_xboxData.m_previewOfferID;
                txtOfferId.Text = currentProject.m_xboxData.m_offerID;
                chkConsumableOffer.Checked = currentProject.m_xboxData.m_consumable;
                cboVisibility.SelectedIndex = currentProject.m_xboxData.m_visibility - 1;
                chkRestrictedLicense.Checked = currentProject.m_xboxData.m_licenseLevel == 2;

                #endregion

                #region PS4

                txtPS4USContID_First.Text = gfMan.getGlobalFieldsFromBank(currentProject.m_globalBankName).m_PS4UScontentIDFirst;
                txtPS4USContID_Second.Text = gfMan.getGlobalFieldsFromBank(currentProject.m_globalBankName).m_PS4UScontentIDSecond;
                txtPS4USContID_Third.Text = currentProject.m_ps4Data.m_UScontentIDThird;
                txtPS4USParamSfo.Text = currentProject.m_ps4Data.m_USparamSFO;

                txtPS4EUContID_First.Text = gfMan.getGlobalFieldsFromBank(currentProject.m_globalBankName).m_PS4EUcontentIDFirst;
                txtPS4EUContID_Second.Text = gfMan.getGlobalFieldsFromBank(currentProject.m_globalBankName).m_PS4EUcontentIDSecond;
                txtPS4EUContID_Third.Text = currentProject.m_ps4Data.m_EUcontentIDThird;
                txtPS4EUParamSfo.Text = currentProject.m_ps4Data.m_EUparamSFO;

                txtPS4JPNContID_First.Text = gfMan.getGlobalFieldsFromBank(currentProject.m_globalBankName).m_PS4JPNcontentIDFirst;
                txtPS4JPNContID_Second.Text = gfMan.getGlobalFieldsFromBank(currentProject.m_globalBankName).m_PS4JPNcontentIDSecond;
                txtPS4JPNContID_Third.Text = currentProject.m_ps4Data.m_JPNcontentIDThird;
                txtPS4JPNParamSfo.Text = currentProject.m_ps4Data.m_JPNparamSFO;

                txtPS4VerEU.Text = currentProject.m_ps4Data.m_versionEU;
                txtPS4VerUS.Text = currentProject.m_ps4Data.m_versionUS;
                txtPS4VerJPN.Text = currentProject.m_ps4Data.m_versionJN;
                txtPS4Icon0.Text = currentProject.m_ps4Data.m_icon0;
                txtPS4Entitlement.Text = gfMan.getGlobalFieldsFromBank(currentProject.m_globalBankName).m_PS4Entitlement;
                txtPS4Code.Text = currentProject.m_ps4Data.m_passCode;
                txtPS4VolType.Text = currentProject.m_ps4Data.m_volumeType;

                #endregion

				#region XBOX ONE
				txtXB1IdentityName.Text = currentProject.m_xboneData.AppManifest.Identity.Name;
				txtXB1IdentityVersion.Text = currentProject.m_xboneData.AppManifest.Identity.Version;
				txtXB1PDescription.Text = currentProject.m_xboneData.AppManifest.Properties.Description;
				txtXB1PDisplayName.Text = currentProject.m_xboneData.AppManifest.Properties.DisplayName;
				txtXB1PPublisherName.Text = currentProject.m_xboneData.AppManifest.Properties.PublisherDisplayName;
				txtXB1AllowedProductID.Text = currentProject.m_xboneData.AllowedProductID;
				txtXB1AllowedProductIDJPN.Text = currentProject.m_xboneData.AllowedProductIdJPN;
				CContentPackageVisualElements vElems = currentProject.m_xboneData.AppManifest.Extensions.PackageExtension.ContentPackage.ContentPackageVisualElements;
				txtXB1VLiveLogo.Text = vElems.Logo;
				txtXB1VSmallLogo.Text = vElems.SmallLogo;
				txtXB1VWideLogo.Text = vElems.WideLogo;
				txtXB1StoreLogo.Text = currentProject.m_xboneData.AppManifest.Properties.Logo;
				txtXB1VDescription.Text = vElems.Description;
				txtXB1VDisplayName.Text = vElems.DisplayName;
				rbXB1Dev.Checked = currentProject.m_xboneData.BuildDebug;
				rbXB1Release.Checked = !currentProject.m_xboneData.BuildDebug;
				#endregion
            }
        }

        #endregion

        #region Treeview UI

        enum SearchFilter { PROJECTS = 0, FOLDERS, FILES };

        private void resetTreeOperations()
        {
            cboSearchType.SelectedIndex = 0;
        }

        private void updateTreeOperations()
        {
            List<contentTreeNode> listToUse = null;

            if (cboSearchType.SelectedIndex == (int)SearchFilter.PROJECTS)
                listToUse = m_projectsACList;
            else if (cboSearchType.SelectedIndex == (int)SearchFilter.FOLDERS)
                listToUse = m_foldersACList;
            else if (cboSearchType.SelectedIndex == (int)SearchFilter.FILES)
                listToUse = m_filesACList;

            cboSearchWord.DroppedDown = false;
            cboSearchWord.Items.Clear();

            if (listToUse != null && cboSearchWord.Text.Length > 0)
            {
                int width = cboSearchWord.DropDownWidth;
                Graphics g = cboSearchWord.CreateGraphics();
                Font font = cboSearchWord.Font;
                int newWidth;

                foreach (contentTreeNode node in listToUse)
                {
                    if (node.FullFilePath.ToLower().Contains(cboSearchWord.Text.ToLower()))
                        cboSearchWord.Items.Add(node);

                    newWidth = (int)g.MeasureString(node.FullFilePath, font).Width;

                    if (width < newWidth)
                        width = newWidth;
                }

                cboSearchWord.DropDownWidth = width;
                cboSearchWord.DroppedDown = cboSearchWord.Items.Count > 0;
                Cursor.Current = Cursors.Default;
            }
        }

        private void cboSearchWord_SelectionChangeCommitted(object sender, EventArgs e)
        {
            setSelectedNode((contentTreeNode)cboSearchWord.SelectedItem);
            cboSearchWord.Text = "";
            cboSearchWord.SelectedIndex = -1;
        }

        private void collapseAllTreeNodes(bool selectRoot)
        {
            tvContentTree.BeginUpdate();
            {
                tvContentTree.CollapseAll();
                tvContentTree.Nodes[0].Expand();

                if (selectRoot)
                    setSelectedNode((contentTreeNode)tvContentTree.Nodes[0]);
            }
            tvContentTree.EndUpdate();
        }

        private void btnCollapseAll_Click(object sender, EventArgs e)
        {
            collapseAllTreeNodes(true);
        }

        private void btnExpandAll_Click(object sender, EventArgs e)
        {
            contentTreeNode selectedNode = getSelectedNode();

            tvContentTree.BeginUpdate();
            {
                fillProjectsNodes(ProjectManager.GetInstance.Projects.ToArray());
                tvContentTree.ExpandAll();
            }
            tvContentTree.EndUpdate();

            setSelectedNode(selectedNode);
        }

        private void cboSearchType_SelectionChangeCommitted(object sender, EventArgs e)
        {
            updateTreeOperations();
        }

        private void cboSearchWord_TextChanged(object sender, EventArgs e)
        {
            fillProjectsNodes(ProjectManager.GetInstance.Projects.ToArray());
            updateTreeOperations();
            cboSearchWord.Select(cboSearchWord.Text.Length, cboSearchWord.Text.Length);
        }

        public void crawlSolution()
        {
            long folderSize = 0;

            m_projectsACList.Clear();
            m_foldersACList.Clear();
            m_filesACList.Clear();

            if (tvContentTree.Nodes.Count > 0)
                crawlSolutionNodes((contentTreeNode)tvContentTree.Nodes[0], ref folderSize, "\\" + tvContentTree.Nodes[0].Text);

            updateTreeOperations();

            tvContentTree.TreeViewNodeSorter = m_nodeSorter;
            tvContentTree.Sort();
        }

        private long crawlSolutionNodes(contentTreeNode currentNode, ref long totalTreeSize, string currentPath)
        {
            long totalCurrentNodeSize = 0;

            foreach (contentTreeNode childNode in currentNode.Nodes)
            {
                if (childNode.NodeType == nodeType.NT_FILE)
                {
                    if (File.Exists(childNode.FullFilePath))
                    {
                        FileInfo fileInfo = new FileInfo(childNode.FullFilePath);

                        //childNode.Size = fileInfo.Length;
                        totalTreeSize += fileInfo.Length;
                        totalCurrentNodeSize += fileInfo.Length;
                        m_filesACList.Add(childNode);
                    }
                }
                else if (childNode.NodeType == nodeType.NT_FOLDER || childNode.NodeType == nodeType.NT_PROJECT_ROOT || childNode.NodeType == nodeType.NT_BRANCH_ROOT)
                {
                    long childSize = crawlSolutionNodes(childNode, ref totalTreeSize, currentPath + "\\" + childNode.Text);

                    totalCurrentNodeSize += childSize;
                    totalTreeSize += childSize;

                    if (childNode.NodeType == nodeType.NT_FOLDER)
                        m_foldersACList.Add(childNode);
                    else if (childNode.NodeType == nodeType.NT_PROJECT_ROOT)
                        m_projectsACList.Add(childNode);
                }
            }

            //currentNode.Size = totalCurrentNodeSize;

            return totalCurrentNodeSize;
        }

        public contentTreeNode addNodeToContentTree(nodeType type, contentTreeNode parentNode, string displayText,
            string fullFilePath, bool moveToNew, Project contentProject, IBranch branch)
        {
            try
            {
                contentTreeNode selectedNode = getSelectedNode();
                contentTreeNode newNode = null;
                TreeNodeCollection nodeCollection = null;
                Project parentProject = null;
                IBranch parentBranch = null;

                if (parentNode == null)
                {
                    if (selectedNode != null)
                    {
                        nodeCollection = selectedNode.Nodes;
                        parentProject = selectedNode.ContentProject;
                        parentBranch = selectedNode.Branch;
                    }
                    else
                        nodeCollection = tvContentTree.Nodes;
                }
                else
                {
                    nodeCollection = parentNode.Nodes;
                    parentProject = parentNode.ContentProject;
                    parentBranch = parentNode.Branch;
                }

                if (nodeCollection != null)
                {
                    foreach (contentTreeNode childNode in nodeCollection)
                    {
                        if (String.Compare(childNode.Text, displayText, true) == 0)
                        {
                            newNode = childNode;
                            break;
                        }
                    }
                }

                if (newNode == null)
                {
                    newNode = new contentTreeNode();

                    if (contentProject == null)
                        newNode.ContentProject = parentProject;
                    else
                        newNode.ContentProject = contentProject;

                    if (branch == null)
                        newNode.Branch = parentBranch;
                    else
                        newNode.Branch = branch;

                    if (type == nodeType.NT_PROJECT_ROOT)
                        newNode.ContentProject.m_rootNode = newNode;

                    nodeCollection.Add(newNode);
                    newNode.ContextMenuStrip = ctxtmnuTvNode;
                    newNode.NodeType = type;
                    newNode.Text = displayText;
                    newNode.FullFilePath = fullFilePath;
                    newNode.extractProjectRelativePath();
                    newNode.determineInternal();
                    decipherNodeImage(newNode);

                    if (type == nodeType.NT_FILE)
                    {
                        recalculateFolderSizes(newNode);
                    }
                }
                else
                {
                    if (type == nodeType.NT_FILE)
                        OutputWindow.appendOutput("Duplicate file(s) detected! " + fullFilePath + " vs " + newNode.FullFilePath, OutputWindow.MessageType.ERROR);
                }

                if (moveToNew)
                    setSelectedNode(newNode);

                ProjectManager.GetInstance.Dirty = true;

                return newNode;
            }
            catch (System.Exception ex)
            {
                OutputWindow.appendOutput(ex.Message, OutputWindow.MessageType.ERROR);
            }

            return null;
        }

        private void recalculateFolderSizes(contentTreeNode newNode)
        {
            FileInfo fileInfo = new FileInfo(newNode.FullFilePath);
            newNode.Size += fileInfo.Length;
            contentTreeNode ctNode = newNode;
            while (ctNode.NodeType != nodeType.NT_CONTENT_ROOT)
            {
                contentTreeNode ctParent = (contentTreeNode)ctNode.Parent;
                ctParent.Size += newNode.Size;
                ctNode = ctParent;

            }
        }

        private void tvContentTree_AfterExpand(object sender, TreeViewEventArgs e)
        {
            if (tvContentTree.Enabled)
            {
				Project[] projects = new Project[1];

                foreach (contentTreeNode childNode in e.Node.Nodes)
                {
                    if (childNode.NodeType == nodeType.NT_PROJECT_ROOT)
                    {
						projects[0] = childNode.ContentProject;
                        fillProjectsNodes(projects);
                    }
                }
            }
        }

        #endregion

        #region Options UI

        private void readOptionValues()
        {
            if (m_optionsForm != null || m_optionsForm.IsDisposed)
                m_optionsForm.readValues();
        }

        #endregion

        #region Global Fields UI

        public void initialiseBankList()
        {
            populateBankList();

            if (cboBankName.Items.Count > 0)
                cboBankName.SelectedIndex = 0;
        }

        private bool validateBankData()
        {
            return (cboBankName.Text.Length > 0);
        }

        private void populateBankList()
        {
            cboBankName.Items.Clear();

            foreach (GlobalFieldsBank currBank in GlobalFieldsManager.GetInstance.m_globalFieldBanks)
                cboBankName.Items.Add(currBank.m_name);

            populateFieldsFromBankName(cboBankName.Text);

            if (ProjectManager.GetInstance.CurrentProject != null)
            {
                txtCurrGlobalBank.Text = ProjectManager.GetInstance.CurrentProject.m_globalBankName;
                grpGlobalBanks.Text = "Project Bank (" + ProjectManager.GetInstance.CurrentProject.m_title + ")";
            }
        }

        private void btnAddNewBank_Click(object sender, EventArgs e)
        {
            if (validateBankData())
            {
                copyGlobalFieldFormDataToBank(GlobalFieldsManager.GetInstance.addOrFindBank(cboBankName.Text));
                updateCurrentProjectFields();
                populateBankList();
                ProjectManager.GetInstance.Dirty = true;
                GlobalFieldsManager.saveGlobalFields();
            }
            else
                OutputWindow.appendOutput("Failed to add new bank!", OutputWindow.MessageType.ERROR);
        }

        private void btnSetCurrBank_Click(object sender, EventArgs e)
        {
            ProjectManager.GetInstance.CurrentProject.m_globalBankName = cboBankName.Text;
            updateCurrentProjectFields();

            populateBankList();
            ProjectManager.GetInstance.Dirty = true;
        }

        private void btnDeleteBank_Click(object sender, EventArgs e)
        {
            GlobalFieldsManager.GetInstance.deleteGlobalFieldBank(cboBankName.Text);

            foreach (Project currProject in ProjectManager.GetInstance.Projects)
            {
                if (String.Compare(currProject.m_globalBankName, cboBankName.Text, true) == 0)
                    currProject.m_globalBankName = "";
            }

            populateBankList();
            ProjectManager.GetInstance.Dirty = true;

            if (GlobalFieldsManager.GetInstance.m_globalFieldBanks.Count == 0)
            {
                cboBankName.Text = "";
                populateFieldsFromBankName("");
            }
            else
                cboBankName.SelectedIndex = 0;
        }

        private void cboBankName_SelectedIndexChanged(object sender, EventArgs e)
        {
            m_lastSelectedBank = (string)cboBankName.SelectedItem;
            populateFieldsFromBankName(m_lastSelectedBank);
        }

        private void copyGlobalFieldFormDataToBank(GlobalFieldsBank bankToModify)
        {
            bankToModify.m_name = cboBankName.Text;
            bankToModify.m_fields.m_gameTitle = txtGlobalFieldGameTitle.Text;
            bankToModify.m_fields.m_titleID = txtGlobalFieldTitleID.Text;

            bankToModify.m_fields.m_UScontentIDFirst = txtGlobalFieldUSContentIDFirst.Text;
            bankToModify.m_fields.m_UScontentIDSecond = txtGlobalFieldUSContentIDSecond.Text;
            bankToModify.m_fields.m_EUcontentIDFirst = txtGlobalFieldEUContentIDFirst.Text;
            bankToModify.m_fields.m_EUcontentIDSecond = txtGlobalFieldEUContentIDSecond.Text;
            bankToModify.m_fields.m_JPNcontentIDFirst = txtGlobalFieldJPNContentIDFirst.Text;
            bankToModify.m_fields.m_JPNcontentIDSecond = txtGlobalFieldJPNContentIDSecond.Text;
            bankToModify.m_fields.m_kLicensee = txtGlobalFieldKLicense.Text;

            bankToModify.m_fields.m_PS4UScontentIDFirst = txtPS4GFUSContIDFirst.Text;
            bankToModify.m_fields.m_PS4UScontentIDSecond = txtPS4GFUSContIDSecond.Text;
            bankToModify.m_fields.m_PS4EUcontentIDFirst = txtPS4GFEUContIDFirst.Text;
            bankToModify.m_fields.m_PS4EUcontentIDSecond = txtPS4GFEUContIDSecond.Text;
            bankToModify.m_fields.m_PS4JPNcontentIDFirst = txtPS4GFJPNContIDFirst.Text;
            bankToModify.m_fields.m_PS4JPNcontentIDSecond = txtPS4GFJPNContIDSecond.Text;
            bankToModify.m_fields.m_PS4Entitlement = txtPS4GFEntitlement.Text;
        }

        private void populateFieldsFromBankName(string bankName)
        {
            GlobalFields currFields = GlobalFieldsManager.GetInstance.getGlobalFieldsFromBank(bankName);

            txtGlobalFieldGameTitle.Text = currFields.m_gameTitle;
            txtGlobalFieldTitleID.Text = currFields.m_titleID;

            txtGlobalFieldUSContentIDFirst.Text = currFields.m_UScontentIDFirst;
            txtGlobalFieldUSContentIDSecond.Text = currFields.m_UScontentIDSecond;
            txtGlobalFieldEUContentIDFirst.Text = currFields.m_EUcontentIDFirst;
            txtGlobalFieldEUContentIDSecond.Text = currFields.m_EUcontentIDSecond;
            txtGlobalFieldJPNContentIDFirst.Text = currFields.m_JPNcontentIDFirst;
            txtGlobalFieldJPNContentIDSecond.Text = currFields.m_JPNcontentIDSecond;
            txtGlobalFieldKLicense.Text = currFields.m_kLicensee;

            txtPS4GFUSContIDFirst.Text = currFields.m_PS4UScontentIDFirst;
            txtPS4GFUSContIDSecond.Text = currFields.m_PS4UScontentIDSecond;
            txtPS4GFEUContIDFirst.Text = currFields.m_PS4EUcontentIDFirst;
            txtPS4GFEUContIDSecond.Text = currFields.m_PS4EUcontentIDSecond;
            txtPS4GFJPNContIDFirst.Text = currFields.m_PS4JPNcontentIDFirst;
            txtPS4GFJPNContIDSecond.Text = currFields.m_PS4JPNcontentIDSecond;
            txtPS4GFEntitlement.Text = currFields.m_PS4Entitlement;
        }

        #endregion

		private void btnXB1GenerateChunk_Click(object sender, EventArgs e)
		{
			Project currentProject = ProjectManager.GetInstance.CurrentProject;
			IBranch platform = currentProject.getBranchForPlatform(Platform.XboxOne);
			XboxOneChunkLayout layout = currentProject.m_xboneData.ChunkLayout = new XboxOneChunkLayout();
			BuildRegistrationChunk(currentProject, ref layout);
			//BuildLaunchChunk(currentProject, ref layout);
			BuildLaunchChunkRPF(currentProject, ref layout);
		}
		private void BuildLaunchChunkRPF(Project currentProject, ref XboxOneChunkLayout layout)
		{
			//string targetDirectory = "dlc" + "\\";
			string targetDirectory = "\\";
			Chunk launchChunk = new Chunk("1000");//, "Launch");
			IBranch platform = currentProject.getBranchForPlatform(Platform.XboxOne);
			//DirectoryInfo[] commonFiles = new DirectoryInfo(platform.Build + @"\common").GetDirectories("*", SearchOption.AllDirectories);
			//DirectoryInfo[] platformFiles = new DirectoryInfo(platform.Build + @"\xboxone").GetDirectories("*", SearchOption.AllDirectories);
			if (Globals.USE_XB1_WORKAROUND_PATH_FOR_VIRTUAL_DRIVES)
			{
				launchChunk.AddFile(currentProject.m_xboneData.IntermediateDirectory, targetDirectory, "dlc.rpf");
			}
			else
			{
				launchChunk.AddFile(platform.Build + "\\out", targetDirectory, "dlc.rpf");
			}
			layout.Add(launchChunk);
		}
		private void BuildAlignmentChunk(Project currentProject, ref XboxOneChunkLayout layout)
		{
			string targetDirectory = "\\";
			Chunk launchChunk = new Chunk("1073741823", "Launch");
			IBranch platform = currentProject.getBranchForPlatform(Platform.XboxOne);
			Uri rootUri = new Uri(platform.Build + @"\dev_ng");
			if (Globals.USE_XB1_WORKAROUND_PATH_FOR_VIRTUAL_DRIVES)
			{

				launchChunk.AddFile(currentProject.m_xboneData.IntermediateDirectory, @"\", "Update.AlignmentChunk");
			}
			layout.Add(launchChunk);
		}
		private void BuildLaunchChunk(Project currentProject, ref XboxOneChunkLayout layout)
		{
			string targetDirectory = "\\";
			Chunk launchChunk = new Chunk("1000");//, "Launch");
			IBranch platform = currentProject.getBranchForPlatform(Platform.XboxOne);
			DirectoryInfo[] commonFiles = new DirectoryInfo(platform.Build + @"\common").GetDirectories("*",SearchOption.AllDirectories);
			DirectoryInfo[] platformFiles = new DirectoryInfo(platform.Build + @"\xboxone").GetDirectories("*", SearchOption.AllDirectories);
			Uri rootUri = new Uri(platform.Build+@"\dev_ng");
			launchChunk.AddFile(platform.Build, targetDirectory, "content.xml");
			launchChunk.AddFile(platform.Build, targetDirectory, "setup2.xml");
			foreach (DirectoryInfo file in commonFiles)
			{
				if(file.GetFiles().Length != 0)
				{
					string destinationPath = targetDirectory + rootUri.MakeRelativeUri(new Uri(file.FullName)).OriginalString.Replace('/', '\\');
					launchChunk.AddFile(file.FullName, destinationPath, "*");
				}
			}
			foreach (DirectoryInfo file in platformFiles)
			{
				if(file.GetFiles().Length != 0)
				{
					string destinationPath = targetDirectory + rootUri.MakeRelativeUri(new Uri(file.FullName)).OriginalString.Replace('/', '\\');
					launchChunk.AddFile(file.FullName, destinationPath, "*");
				}
			}	
			layout.Add(launchChunk);
		}
		private void BuildRegistrationChunk(Project currentProject, ref XboxOneChunkLayout layout)
		{
			Chunk registrationChunk = new Chunk("Registration");
			IBranch platform = currentProject.getBranchForPlatform(Platform.XboxOne);
			//string targetDirectory = "dlc" + "\\";
			string targetDirectory = "\\";
			string targetLogoDirectory = "signage\\";
			string sourceDirectory = platform.Build;
			CContentPackageVisualElements vElems = currentProject.m_xboneData.AppManifest.Extensions.PackageExtension.ContentPackage.ContentPackageVisualElements;
			CProperties properties = currentProject.m_xboneData.AppManifest.Properties;
			FileInfo storeLogo = new FileInfo(properties.Logo);
			FileInfo liveLogo = new FileInfo(vElems.Logo);
			FileInfo smallLogo = new FileInfo(vElems.SmallLogo);
			FileInfo wideLogo = new FileInfo(vElems.WideLogo);
			if (Globals.USE_XB1_WORKAROUND_PATH_FOR_VIRTUAL_DRIVES)
			{
				registrationChunk.AddFile(currentProject.m_xboneData.IntermediateDirectory, targetDirectory, "AppxManifest.xml");
				registrationChunk.AddFile(currentProject.m_xboneData.IntermediateDirectory + @"\signage", targetLogoDirectory, storeLogo.Name);
				registrationChunk.AddFile(currentProject.m_xboneData.IntermediateDirectory + @"\signage", targetLogoDirectory, liveLogo.Name);
				registrationChunk.AddFile(currentProject.m_xboneData.IntermediateDirectory + @"\signage", targetLogoDirectory, smallLogo.Name);
				registrationChunk.AddFile(currentProject.m_xboneData.IntermediateDirectory + @"\signage", targetLogoDirectory, wideLogo.Name);
			}
			else
			{
				registrationChunk.AddFile(sourceDirectory, targetDirectory, "AppxManifest.xml");
				registrationChunk.AddFile(storeLogo.DirectoryName, targetLogoDirectory, storeLogo.Name);
				registrationChunk.AddFile(liveLogo.DirectoryName, targetLogoDirectory, liveLogo.Name);
				registrationChunk.AddFile(smallLogo.DirectoryName, targetLogoDirectory, smallLogo.Name);
				registrationChunk.AddFile(wideLogo.DirectoryName, targetLogoDirectory, wideLogo.Name);
			}
			layout.Add(registrationChunk);
		}
		private void grpXB1Properties_Enter(object sender, EventArgs e)
		{

		}

		private void btnXB1SaveChunksFile_Click(object sender, EventArgs e)
		{
			Project CurrentProject = ProjectManager.GetInstance.CurrentProject;
			string buildPath = CurrentProject.getBranchForPlatform(Platform.XboxOne).Build;
			CurrentProject.m_xboneData.WriteAppManifest( buildPath + @"\AppXManifest.xml");
			CurrentProject.m_xboneData.WriteChunkLayout(Directory.GetParent(buildPath).FullName + @"\chunksRPF.xml");
			//CurrentProject.m_xboneData.WriteChunkLayout(Directory.GetParent(buildPath).FullName + @"\chunks.xml");
		}

		private void ExtractXbonePackageName(string input)
		{
			Match matches = Regex.Match(input, "Successfully created package '(.+)'");
			if (matches.Success)
			{
				Project currentProject = ProjectManager.GetInstance.CurrentProject;
				currentProject.m_xboneData.IntermediatePackageFile = matches.Groups[1].Value;
			}
		}

		private void tsbBuildShippablePack_Click(object sender, EventArgs e)
		{
			if (m_shippablePacksForm.IsDisposed)
			{
				m_shippablePacksForm = new ShippablePackSelector();
			}
			m_shippablePacksForm.SetBuildFunction(BuildShippablePacks);
			m_shippablePacksForm.Show();
		}

		private void BuildShippablePacks(List<string> targets)
		{
            // Hide the form, Orkun!
            if (m_shippablePacksForm != null)
                m_shippablePacksForm.Hide();

			string tempRpfPath = "";
			List<string> targetDirs = new List<string>();
			string replacementBranch = null;
			// I'M SO SORRY TOM
			foreach (string item in targets)
			{
				if (item == "build")
				{
					string targetDir = Globals.TOOLS_CONFIG.CoreProject.Branches["dev_ng"].Build;
					targetDirs.Add(targetDir);
				}
				if (item == "dev_ng")
				{
					string targetDir = @"x:\gta5\titleupdate\dev_ng";
					targetDirs.Add(targetDir);
				}
				if (item == "dev_temp")
				{
					string targetDir = @"x:\gta5\titleupdate\dev_temp";
					targetDirs.Add(targetDir);
				}
				if (item == "japanese")
				{
					string targetDir = @"x:\gta5\titleupdate\japanese_ng";
					replacementBranch = @"japanese";
					targetDirs.Add(targetDir);
				}
				if (item == "dev_ng_live")
				{
					string targetDir = @"x:\gta5\titleupdate\dev_ng_live";
					targetDirs.Add(targetDir);
				}
				if (item == "japanese_ng_live")
				{
					string targetDir = @"x:\gta5\titleupdate\japanese_ng_live";
					replacementBranch = @"japanese";
					targetDirs.Add(targetDir);
				}
				if (item == "patch")
				{
					string targetDir = @"x:\gta5\patches\";
					targetDirs.Add(targetDir);
				}
			}

			List<string> filesToCheckOut = new List<string>();
			List<Project> selectedProjects = new List<Project>();
			foreach (Project proj in ProjectManager.GetInstance.Projects)
			{
				if (proj.getIsInBuild())
					selectedProjects.Add(proj);
			}
			fillProjectsNodes(selectedProjects.ToArray());
			foreach (Project thisProject in ProjectManager.GetInstance.Projects)
			{
				OutputWindow.pushActiveChannel(thisProject.m_channelId);

				if (thisProject.getIsInBuild())
				{
					foreach (var test in Globals.TOOLS_CONFIG.CoreProject.Branches["dev_ng"].Targets)
					{
                        // Bail early if platform is not in project
                        if (thisProject.getBranchRootNode(test.Value.Platform) == null)
                            continue;

						string tempRbs = Globals.NG_TEMP_RPF_PATH;
						string[] path = test.Value.ResourcePath.Split(new char[] { '\\', '/' }, StringSplitOptions.None);
						string tempPath = Globals.NG_TEMP_RPF_PATH + path[path.Length - 1];
						string relativePath = "\\dlcPacks\\" + thisProject.m_toolsProject.Name;

                        List<contentTreeNode> nodeList = new List<contentTreeNode>();
						getFileNodes(thisProject.getBranchRootNode(test.Value.Platform), nodeList, test.Value.Platform);
						thisProject.cullExcludedNodes(ref nodeList, test.Value.Platform);
                        long divThreshold = thisProject.getRpfDivisionThreshold(test.Value.Platform);
                        thisProject.m_subPackCount = fileNodePrePass(nodeList, divThreshold);

						foreach (string targetDir in targetDirs)
						{
							filesToCheckOut.Add(targetDir + "\\" + path[path.Length - 1] + relativePath + "\\dlc.rpf");
                            for (int i = 1; i <= thisProject.m_subPackCount; ++i)
                            {
                                filesToCheckOut.Add(targetDir + "\\" + path[path.Length - 1] + relativePath + "\\dlc" + i +".rpf");
                            }
						}

                        modifySetupXML(thisProject);

						tempRpfPath = tempPath + relativePath;
						var x = test.Value;
						if (!Directory.Exists(tempRbs))
							Directory.CreateDirectory(tempRbs);
						if (!Directory.Exists(tempRpfPath))
							Directory.CreateDirectory(tempRpfPath);

						createContentRPF(tempRbs + "script.rbs", tempRpfPath + "\\dlc.rpf", thisProject, test.Value.Platform, nodeList, replacementBranch);
					}
				}
			}

			File.Delete(Globals.NG_TEMP_RPF_PATH + "script.rbs");
			CommandPrompt.perforceCheckoutArray(filesToCheckOut.ToArray());

            m_progressForm.ResetProgress(targetDirs.Count, "Copying files... ", false);
			foreach (string targetDir in targetDirs)
			{
                m_progressForm.SetLabel("Copying files to "+targetDir);
                m_progressForm.Refresh();
				directoryCopy(Globals.NG_TEMP_RPF_PATH, targetDir, true);
                m_progressForm.IncrementProgress();
			}

            try
            {
                Directory.Delete(Globals.NG_TEMP_RPF_PATH, true);
            }
            catch(System.Exception ex)
            {
                OutputWindow.appendOutput(ex.Message + "Please ensure that the temp directories are not read-only.", OutputWindow.MessageType.ERROR);
            }
		}

        private void toolStripMain_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }
    }
}
