﻿#region Usings

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Collections;
using System.Xml;
using System.Xml.Linq;
using System.Diagnostics;
using System.Threading;
using System.Xml.Serialization;
using ContentStar;
using contentStar;
using RSG.Metadata.Parser;
using RSG.Metadata.Model;
using RSG.Platform;

#endregion

namespace ContentStar
{
    [Serializable]
    [XmlRoot("ExcludedFiles")]
    public class FileExclusions
    {
        [XmlArray("ExclusionArray")]
        [XmlArrayItem("Exclusion", typeof(ExcludedItem), IsNullable = false)]
        public List<ExcludedItem> m_exclusions = new List<ExcludedItem>();

        public static FileExclusions ReadFile(string filePath)
        {
            FileExclusions newExclusions = new FileExclusions();

            if (File.Exists(filePath))
            {
                OutputWindow.appendOutput("Reading " + filePath, OutputWindow.MessageType.IMPORTANT);

                XmlSerializer filtersDeserialiser = new XmlSerializer(typeof(FileExclusions));
                using (TextReader readStream = new StreamReader(filePath))
                {
                    newExclusions = (FileExclusions)filtersDeserialiser.Deserialize(readStream);
                    readStream.Close();
                }
            }

            return newExclusions;
        }

		

		public void CullExcludedNodes(ref List<contentTreeNode> nodes, Platform currentPlatform)
		{
			BuildManager.BuildPlatform platform = (BuildManager.BuildPlatform)Enum.Parse(typeof(BuildManager.BuildPlatform), BuildManager.GetDirectoryPlatformName(currentPlatform));
			List<contentTreeNode> nodesToRemove = new List<contentTreeNode>();
			string platformString = currentPlatform.ToString();
			foreach (var exclusion in m_exclusions)
			{
				if(exclusion.exclusionType == ExclusionType.FULL)
				{
					foreach (var node in nodes)
					{
						if (node.FullFilePath.Contains(exclusion.fileName)&&
							BuildManager.isPlatformPath(node.FullFilePath)&&
							(exclusion.excludedPlatform.HasFlag(platform)))
						{
							OutputWindow.appendOutput("Stripping " + node.FullFilePath, OutputWindow.MessageType.IMPORTANT);
							Console.WriteLine("Stripping " + node.FullFilePath);
							nodesToRemove.Add(node);
						}
					}
				}
			}
			var newNodes = nodes.Except(nodesToRemove);
			nodes = newNodes.ToList();
		}
        public bool IsExcluded(string file, BuildManager.BuildType buildType)
        {
            for (int i = 0; i < m_exclusions.Count; ++i)
            {
                if (file.Contains(m_exclusions[i].fileName) &&
                    (	(m_exclusions[i].buildType == buildType || 
						m_exclusions[i].buildType == BuildManager.BuildType.ALL) &&
						m_exclusions[i].excludedPlatform == BuildManager.BuildPlatform.NONE))
				{
					OutputWindow.appendOutput("Stripping " + file, OutputWindow.MessageType.IMPORTANT);
					Console.WriteLine("Stripping " + file);
                    return true;
                }
            }

            return false;
        }

        public bool IsFullyExcluded(string file, BuildManager.BuildType buildType)
        {
            for (int i = 0; i < m_exclusions.Count; ++i)
            {
                if (file.Contains(m_exclusions[i].fileName) && 
                    m_exclusions[i].exclusionType == ExclusionType.FULL &&
					m_exclusions[i].excludedPlatform == BuildManager.BuildPlatform.NONE &&
                    (m_exclusions[i].buildType == buildType || m_exclusions[i].buildType == BuildManager.BuildType.ALL))
				{
					OutputWindow.appendOutput("Stripping " + file, OutputWindow.MessageType.IMPORTANT);
					Console.WriteLine("Stripping " + file);
                    return true;
                }
            }

            return false;
        }
    };

    public class ExcludedItem
    {
        public ExcludedItem()
        {
            buildType = BuildManager.BuildType.ALL;
			excludedPlatform = BuildManager.BuildPlatform.NONE;
        }

        [XmlAttribute("fileName")]
		public string fileName;

		[XmlAttribute("projectName")]
		public string projectName;

        [XmlAttribute("exclusionType")]
        public ExclusionType exclusionType;

        [XmlAttribute("buildType")]
		public BuildManager.BuildType buildType;

		[XmlAttribute("excludedPlatform")]
		public BuildManager.BuildPlatform excludedPlatform;
    };

    public enum ExclusionType
    { 
        [XmlEnum(Name = "Partial")]
        PARTIAL = 0,

        [XmlEnum(Name = "Full")]
        FULL
    };
}