﻿/*******************************************\
| ContentStar : Rockstar Games © 2013       |
| ------------------------------------------|
| Author: Thomas Woodhead (R* London)       |
| Email: thomas.woodhead@rockstarlondon.com |
\*******************************************/

#region Usings

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using contentStar;

#endregion

namespace ContentStar
{
    public partial class frmOptions : Form
    {
        #region Constructor

        public frmOptions()
        {
            InitializeComponent();

            btnMainPostBuildBrowse.linkedControl = txtMainPostBuildScr;
            btnMainPreBuildBrowse.linkedControl = txtMainPreBuildScr;

            btnPCPostBuildBrowse.linkedControl = txtPCPostBuildScr;
            btnPCPreBuildBrowse.linkedControl = txtPCPreBuildScr;

            btnPs3PostBuildBrowse.linkedControl = txtPs3PostBuildScr;
            btnPs3PreBuildBrowse.linkedControl = txtPs3PreBuildScr;

            btnXboxPostBuildBrowse.linkedControl = txtXboxPostBuildScr;
            btnXboxPreBuildBrowse.linkedControl = txtXboxPreBuildScr;

            btnBlastBrowse.linkedControl = txtBlastExe;
            btnPs3PkgBrowse.linkedControl = txtPs3PkgExe;
            btnPs3Edat.linkedControl = txtPs3Edat;

            btnPs3CtrlBrowse.linkedControl = txtPs3Ctrl;

            btnBrowsePS4PkgTarg.linkedControl = txtPS4PkgTarg;
            btnBrowsePS4CtrlTarg.linkedControl = txtPS4CtrlTarg;
        }

        #endregion

        #region Interface events

        private void copyListViewToSettings(ListView items, List<DeployTarget> list)
        {
            list.Clear();

            foreach (ListViewItem target in items.Items)
            {
                DeployTarget targetData = new DeployTarget();

                targetData.m_deploy = target.Checked;
                targetData.m_ipAddress = target.SubItems[1].Text;

                list.Add(targetData);
            }
        }

        private void frmOptions_VisibleChanged(object sender, EventArgs e)
        {
            lvXboxTargets.Items.Clear();
            lvPs3Targets.Items.Clear();
            lvPS4Targets.Items.Clear();

            foreach (DeployTarget target in SettingsManager.GetInstance.m_xboxTargets)
                addTarget(target.m_ipAddress, Convert.ToBoolean(target.m_deploy), false, lvXboxTargets);

            foreach (DeployTarget target in SettingsManager.GetInstance.m_ps3Targets)
                addTarget(target.m_ipAddress, Convert.ToBoolean(target.m_deploy), false, lvPs3Targets);

            foreach (DeployTarget target in SettingsManager.GetInstance.m_PS4Targets)
				addTarget(target.m_ipAddress, Convert.ToBoolean(target.m_deploy), false, lvPS4Targets);

			foreach (DeployTarget target in SettingsManager.GetInstance.m_XB1Targets)
				addTarget(target.m_ipAddress, Convert.ToBoolean(target.m_deploy), false, lvXB1Targets);
        }

        private void updateTargetSettings()
        {
            copyListViewToSettings(lvXboxTargets, SettingsManager.GetInstance.m_xboxTargets);
            copyListViewToSettings(lvPs3Targets, SettingsManager.GetInstance.m_ps3Targets);
			copyListViewToSettings(lvPS4Targets, SettingsManager.GetInstance.m_PS4Targets);
			copyListViewToSettings(lvXB1Targets, SettingsManager.GetInstance.m_XB1Targets);
        }

        public void addTarget(string ipAddress, bool deploy, bool addToSettings, ListView targetList)
        {
            ListViewItem targetListViewItem = new ListViewItem();
            System.Windows.Forms.ListViewItem.ListViewSubItem newSubItem = new System.Windows.Forms.ListViewItem.ListViewSubItem();

            newSubItem.Text = ipAddress;
            targetListViewItem.Checked = deploy;
            targetListViewItem.SubItems.Add(newSubItem);
            targetList.Items.Add(targetListViewItem);

            if (addToSettings)
                updateTargetSettings();
        }

        private void ctxtAddTarget_Click(object sender, EventArgs e)
        {
            string targetIPAddress = "";

            if (Globals.InputBox("IP address", "Enter target IP address:", ref targetIPAddress, this) == DialogResult.OK)
                addTarget(targetIPAddress, true, true, lvXboxTargets);

            this.Show();
        }

        private void ctxtRemoveTarget_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem xboxTarget in lvXboxTargets.SelectedItems)
                lvXboxTargets.Items.Remove(xboxTarget);

            updateTargetSettings();
        }

        private void ctxtAddTargetPS3_Click(object sender, EventArgs e)
        {
            string targetIPAddress = "";

            if (Globals.InputBox("IP address", "Enter target IP address:", ref targetIPAddress, this) == DialogResult.OK)
                addTarget(targetIPAddress, true, true, lvPs3Targets);

            this.Show();
        }

        private void ctxtRemoveTargetPS3_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem ps3Target in lvPs3Targets.SelectedItems)
                lvPs3Targets.Items.Remove(ps3Target);

            updateTargetSettings();
        }

        private void ctxtAddTargetPS4_Click(object sender, EventArgs e)
        {
            string targetIPAddress = "";

            if (Globals.InputBox("IP address", "Enter target IP address:", ref targetIPAddress, this) == DialogResult.OK)
                addTarget(targetIPAddress, true, true, lvPS4Targets);

            this.Show();
        }

        private void ctxtRemoveTargetPS4_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem ps4Target in lvPS4Targets.SelectedItems)
                lvPS4Targets.Items.Remove(ps4Target);

            updateTargetSettings();
        }

		private void ctxtAddTargetXB1_Click(object sender, EventArgs e)
		{
			string targetIPAddress = "";

			if (Globals.InputBox("IP address", "Enter target IP address:", ref targetIPAddress, this) == DialogResult.OK)
				addTarget(targetIPAddress, true, true, lvXB1Targets);

			this.Show();
		}

		private void ctxtRemoveTargetXB1_Click(object sender, EventArgs e)
		{
			foreach (ListViewItem xb1Target in lvXB1Targets.SelectedItems)
				lvXB1Targets.Items.Remove(xb1Target);

			updateTargetSettings();
		}

        private void frmOptions_Load(object sender, EventArgs e)
        {
            lvXboxTargets.ContextMenu = ctxtmnuAddTarget360;
            lvPs3Targets.ContextMenu = ctxtmnuAddTargetPS3;
			lvPS4Targets.ContextMenu = ctxtmnuAddTargetPS4;
			lvXB1Targets.ContextMenu = ctxtmnuAddTargetXB1;
        }

        private void ctxtmnuAddTarget_Popup(object sender, EventArgs e)
        {
            ctxtRemoveTarget360.Enabled = lvXboxTargets.SelectedItems.Count > 0;
        }

        private void ctxtmnuAddTargetPS3_Popup(object sender, EventArgs e)
        {
            ctxtRemoveTargetPS3.Enabled = lvPs3Targets.SelectedItems.Count > 0;
        }

        private void ctxtmnuAddTargetPS4_Popup(object sender, EventArgs e)
        {
            ctxtRemoveTargetPS4.Enabled = lvPS4Targets.SelectedItems.Count > 0;
        }

		private void ctxtmnuAddTargetXB1_Popup(object sender, EventArgs e)
		{
			ctxtRemoveTargetXB1.Enabled = lvXB1Targets.SelectedItems.Count > 0;
		}

        private void hideForm()
        {
            updateTargetSettings();

            this.Hide();
        }

        private void btnOptsOK_Click(object sender, EventArgs e)
        {
            applyValues();
            hideForm();
        }

        private void btnOptCancel_Click(object sender, EventArgs e)
        {
            hideForm();
        }

        private void btnOptApply_Click(object sender, EventArgs e)
        {
            applyValues();
        }

        private void findExecutableTarget(object sender, EventArgs e)
        {
            controlLinkedButton controlLinkedButton = (controlLinkedButton)sender;
            TextBox linkedTextBox = (TextBox)controlLinkedButton.linkedControl;

            if (linkedTextBox != null && ofdExecutable.ShowDialog() == DialogResult.OK)
                linkedTextBox.Text = ofdExecutable.FileName;
        }

        private void findScriptFileTarget(object sender, EventArgs e)
        {
            controlLinkedButton controlLinkedButton = (controlLinkedButton)sender;
            TextBox linkedTextBox = (TextBox)controlLinkedButton.linkedControl;

            if (linkedTextBox != null && ofdScriptTargets.ShowDialog() == DialogResult.OK)
                linkedTextBox.Text = ofdScriptTargets.FileName;
        }

        #endregion

        #region Apply values

        public void applyValues()
        {
            if (ProjectManager.GetInstance.CurrentProject != null)
            {
                Project currentProject = ProjectManager.GetInstance.CurrentProject;

                currentProject.m_preBuildPath = txtMainPreBuildScr.Text;
                currentProject.m_postBuildPath = txtMainPostBuildScr.Text;

                currentProject.m_xboxData.m_preBuildPath = txtXboxPreBuildScr.Text;
                currentProject.m_xboxData.m_postBuildPath = txtXboxPostBuildScr.Text;

                currentProject.m_ps3Data.m_preBuildPath = txtPs3PreBuildScr.Text;
                currentProject.m_ps3Data.m_postBuildPath = txtPs3PostBuildScr.Text;

                currentProject.m_pcData.m_preBuildPath = txtPCPreBuildScr.Text;
                currentProject.m_pcData.m_postBuildPath = txtPCPostBuildScr.Text;
            }

            if (SettingsManager.GetInstance != null)
            {
                SettingsManager settings = SettingsManager.GetInstance;

                settings.m_ps3DeployUS = chkPs3USDeploy.Checked;
                settings.m_ps3DeployEU = chkPs3EUDeploy.Checked;
                settings.m_ps3DeployJN = chkPs3JPNDeploy.Checked;

                settings.m_blastPath = txtBlastExe.Text;
                settings.m_makePackagePath = txtPs3PkgExe.Text;
                settings.m_makeEdatPath = txtPs3Edat.Text;
                settings.m_ps3CtrlPath = txtPs3Ctrl.Text;

                settings.m_PS4CtrlPath = txtPS4CtrlTarg.Text;
                settings.m_PS4MakePackagePath = txtPS4PkgTarg.Text;
                settings.m_ps4DeployUS = cbPS4USDepOpt.Checked;
                settings.m_ps4DeployEU = cbPS4EUDepOpt.Checked;
                settings.m_ps4DeployJN = cbPS4JNDepOpt.Checked;
            }

            ProjectManager.GetInstance.Dirty = true;
        }

        #endregion

        #region Read values

        public void readValues()
        {
            if (ProjectManager.GetInstance.CurrentProject != null)
            {
                Project currentProject = ProjectManager.GetInstance.CurrentProject;

                txtMainPreBuildScr.Text = currentProject.m_preBuildPath;
                txtMainPostBuildScr.Text = currentProject.m_postBuildPath;

                txtXboxPreBuildScr.Text = currentProject.m_xboxData.m_preBuildPath;
                txtXboxPostBuildScr.Text = currentProject.m_xboxData.m_postBuildPath;

                txtPs3PreBuildScr.Text = currentProject.m_ps3Data.m_preBuildPath;
                txtPs3PostBuildScr.Text = currentProject.m_ps3Data.m_postBuildPath;

                txtPCPreBuildScr.Text = currentProject.m_pcData.m_preBuildPath;
                txtPCPostBuildScr.Text = currentProject.m_pcData.m_postBuildPath;
            }

            if (SettingsManager.GetInstance != null)
            {
                SettingsManager settings = SettingsManager.GetInstance;

                txtBlastExe.Text = settings.m_blastPath;
                txtPs3PkgExe.Text = settings.m_makePackagePath;
                txtPs3Edat.Text = settings.m_makeEdatPath;
                txtPs3Ctrl.Text = settings.m_ps3CtrlPath;

                chkPs3USDeploy.Checked = settings.m_ps3DeployUS;
                chkPs3EUDeploy.Checked = settings.m_ps3DeployEU;
                chkPs3JPNDeploy.Checked = settings.m_ps3DeployJN;

                cbPS4USDepOpt.Checked = settings.m_ps4DeployUS;
                cbPS4EUDepOpt.Checked = settings.m_ps4DeployEU;
                cbPS4JNDepOpt.Checked = settings.m_ps4DeployJN;

                txtPS4CtrlTarg.Text = settings.m_PS4CtrlPath;
                txtPS4PkgTarg.Text = settings.m_PS4MakePackagePath;
            }
        }

        #endregion
    }
}
