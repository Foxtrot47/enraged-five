﻿/*******************************************\
| ContentStar : Rockstar Games © 2013       |
| ------------------------------------------|
| Author: Thomas Woodhead (R* London)       |
| Email: thomas.woodhead@rockstarlondon.com |
\*******************************************/

#region Usings

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Collections;
using System.Xml;
using System.Xml.Linq;
using System.Diagnostics;
using System.Xml.Serialization;
using ContentStar;
using System.Threading;
using contentStar;
using RSG.Platform;

#endregion

namespace ContentStar
{
    public class BuildManager
    {
        public enum BuildType
        {
            [XmlEnum(Name = "All")]
            ALL = 0,

            [XmlEnum(Name = "Debug")]
            DEBUG,

            [XmlEnum(Name = "Release")]
            RELEASE
        }

		[Flags]
		public enum BuildPlatform
		{
			[XmlEnum(Name = "none")]
			NONE = 1,

			[XmlEnum(Name = "x64")]
			x64 = 2,

			[XmlEnum(Name = "xbox360")]
			xbox360 = 4,

			[XmlEnum(Name = "ps3")]
			ps3 = 8,

			[XmlEnum(Name = "xboxone")]
			xboxone= 16,

			[XmlEnum(Name = "ps4")]
			ps4 = 32,

			[XmlEnum(Name = "ps5")]
			ps5 = 64,

			[XmlEnum(Name = "xbsx")]
			xbsx = 128,

		}

        private static BuildType ms_buildType = BuildType.DEBUG;
        private static string[] ms_platformNames = {    "Independent", "pc",    "x64",      "ps3",  "xbox360",  "ps4",      "xboxone" , "ps5" , "xbsx" };
        private static string[] ms_rbsPlatformNames = { "",             "",     "win64",         "ps3",  "xenon",    "orbis",    "durango", "prospero", "scarlett" };

        public static void contractFilePath(ref string filePath)
        {
            foreach (string platformName in ms_platformNames)
                filePath = filePath.Replace("/" + platformName + "/", @"/%PLATFORM%/");
        }

        public static bool isPlatformPath(string filePath)
        {
            bool isPlatform = false;

            filePath = filePath.ToLower();

            foreach (string platformName in ms_platformNames)
                isPlatform |= filePath.Contains(platformName);

            return isPlatform;
        }

        public static void addBuildInclusion(ref int buildInclusions, int flag)
        {
            buildInclusions |= (int)(1 << (int)flag);
        }

        public static bool isIncludedInBuild(int buildInclusions, int flag)
        {
            return ((buildInclusions & (int)(1 << (int)flag)) != 0);
        }

        public static string getRageBuilderPath(Platform targetPlatform)
        {
            if (targetPlatform == Platform.PS3 || targetPlatform == Platform.Xbox360 || targetPlatform == Platform.Win32)
                return Globals.RAGE_BUILDER_PATH;
            else if (targetPlatform == Platform.XboxOne || targetPlatform == Platform.PS4 || targetPlatform == Platform.Win64)
                return Globals.RAGE_BUILDER_PATH_NG;

            return string.Empty;
        }

        public static List<Platform> getIncludedPlatforms(int targetsToBuild)
        {
            List<Platform> includedPlatforms = new List<Platform>();

            foreach (Platform platform in Enum.GetValues(typeof(Platform)))
            {
                if (isIncludedInBuild(targetsToBuild, (int)platform))
                    includedPlatforms.Add(platform);
            }

            return includedPlatforms;
        }

        public static string GetRbsPlatformName(Platform targetPlatform)
        {
            return ms_rbsPlatformNames[(int)targetPlatform];
        }

        public static string GetDirectoryPlatformName(Platform targetPlatform)
        {
            return ms_platformNames[(int)targetPlatform];
        }

        public static BuildType GetBuildType()
        {
            return ms_buildType;
        }

        public static void SetBuildType(BuildType value)
        {
            ms_buildType = value;
        }
    }
}
