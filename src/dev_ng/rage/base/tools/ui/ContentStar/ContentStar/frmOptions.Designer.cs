﻿/*******************************************\
| ContentStar : Rockstar Games © 2013       |
| ------------------------------------------|
| Author: Thomas Woodhead (R* London)       |
| Email: thomas.woodhead@rockstarlondon.com |
\*******************************************/

namespace ContentStar
{
    partial class frmOptions
    {
        public class controlLinkedButton : System.Windows.Forms.Button
        {
            public System.Windows.Forms.Control linkedControl;
        }

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmOptions));
			this.btnOptApply = new System.Windows.Forms.Button();
			this.btnOptCancel = new System.Windows.Forms.Button();
			this.btnOptsOK = new System.Windows.Forms.Button();
			this.tcOptions = new System.Windows.Forms.TabControl();
			this.tpBuildOptions = new System.Windows.Forms.TabPage();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.btnBrowsePS4PkgTarg = new ContentStar.frmOptions.controlLinkedButton();
			this.txtPS4PkgTarg = new System.Windows.Forms.TextBox();
			this.lblPS4PkgTarg = new System.Windows.Forms.Label();
			this.btnBrowsePS4CtrlTarg = new ContentStar.frmOptions.controlLinkedButton();
			this.txtPS4CtrlTarg = new System.Windows.Forms.TextBox();
			this.lblPS4CtrlTarg = new System.Windows.Forms.Label();
			this.btnPs3CtrlBrowse = new ContentStar.frmOptions.controlLinkedButton();
			this.txtPs3Ctrl = new System.Windows.Forms.TextBox();
			this.lblPs3Ctrl = new System.Windows.Forms.Label();
			this.btnPs3Edat = new ContentStar.frmOptions.controlLinkedButton();
			this.txtPs3Edat = new System.Windows.Forms.TextBox();
			this.lblPs3Edat = new System.Windows.Forms.Label();
			this.btnPs3PkgBrowse = new ContentStar.frmOptions.controlLinkedButton();
			this.txtPs3PkgExe = new System.Windows.Forms.TextBox();
			this.lblPs3Pkg = new System.Windows.Forms.Label();
			this.btnBlastBrowse = new ContentStar.frmOptions.controlLinkedButton();
			this.txtBlastExe = new System.Windows.Forms.TextBox();
			this.lblBlastExe = new System.Windows.Forms.Label();
			this.gbMainScrOpts = new System.Windows.Forms.GroupBox();
			this.txtMainPreBuildScr = new System.Windows.Forms.TextBox();
			this.btnMainPostBuildBrowse = new ContentStar.frmOptions.controlLinkedButton();
			this.lblMainPreBuild = new System.Windows.Forms.Label();
			this.btnMainPreBuildBrowse = new ContentStar.frmOptions.controlLinkedButton();
			this.lblMainPostBuild = new System.Windows.Forms.Label();
			this.txtMainPostBuildScr = new System.Windows.Forms.TextBox();
			this.gbPCScrOpts = new System.Windows.Forms.GroupBox();
			this.btnPCPostBuildBrowse = new ContentStar.frmOptions.controlLinkedButton();
			this.btnPCPreBuildBrowse = new ContentStar.frmOptions.controlLinkedButton();
			this.txtPCPostBuildScr = new System.Windows.Forms.TextBox();
			this.lblPCPostBuild = new System.Windows.Forms.Label();
			this.txtPCPreBuildScr = new System.Windows.Forms.TextBox();
			this.lblPCPreBuild = new System.Windows.Forms.Label();
			this.gbPs3ScrOpts = new System.Windows.Forms.GroupBox();
			this.btnPs3PostBuildBrowse = new ContentStar.frmOptions.controlLinkedButton();
			this.btnPs3PreBuildBrowse = new ContentStar.frmOptions.controlLinkedButton();
			this.txtPs3PostBuildScr = new System.Windows.Forms.TextBox();
			this.lblPs3PostBuild = new System.Windows.Forms.Label();
			this.txtPs3PreBuildScr = new System.Windows.Forms.TextBox();
			this.lblPs3PreBuild = new System.Windows.Forms.Label();
			this.gbXboxScrOpts = new System.Windows.Forms.GroupBox();
			this.btnXboxPostBuildBrowse = new ContentStar.frmOptions.controlLinkedButton();
			this.btnXboxPreBuildBrowse = new ContentStar.frmOptions.controlLinkedButton();
			this.txtXboxPostBuildScr = new System.Windows.Forms.TextBox();
			this.lblXboxPostBuild = new System.Windows.Forms.Label();
			this.txtXboxPreBuildScr = new System.Windows.Forms.TextBox();
			this.lblXboxPreBuild = new System.Windows.Forms.Label();
			this.tpDeployOpts = new System.Windows.Forms.TabPage();
			this.tpDeployPlatforms = new System.Windows.Forms.TabControl();
			this.tpXboxTargets = new System.Windows.Forms.TabPage();
			this.lvXboxTargets = new System.Windows.Forms.ListView();
			this.lvtxtXboxDeploy = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.lvtxtXboxAddress = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.tbXb1Targets = new System.Windows.Forms.TabPage();
			this.lvXB1Targets = new System.Windows.Forms.ListView();
			this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.tpPs3Targets = new System.Windows.Forms.TabPage();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.chkPs3JPNDeploy = new System.Windows.Forms.CheckBox();
			this.chkPs3EUDeploy = new System.Windows.Forms.CheckBox();
			this.chkPs3USDeploy = new System.Windows.Forms.CheckBox();
			this.lvPs3Targets = new System.Windows.Forms.ListView();
			this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.tbDeployPS4 = new System.Windows.Forms.TabPage();
			this.gbPS4DepOpts = new System.Windows.Forms.GroupBox();
			this.cbPS4JNDepOpt = new System.Windows.Forms.CheckBox();
			this.cbPS4EUDepOpt = new System.Windows.Forms.CheckBox();
			this.cbPS4USDepOpt = new System.Windows.Forms.CheckBox();
			this.lvPS4Targets = new System.Windows.Forms.ListView();
			this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.ofdScriptTargets = new System.Windows.Forms.OpenFileDialog();
			this.ctxtmnuAddTarget360 = new System.Windows.Forms.ContextMenu();
			this.ctxtAddTarget360 = new System.Windows.Forms.MenuItem();
			this.ctxtRemoveTarget360 = new System.Windows.Forms.MenuItem();
			this.ofdExecutable = new System.Windows.Forms.OpenFileDialog();
			this.fbdOutputFolder = new System.Windows.Forms.FolderBrowserDialog();
			this.ctxtmnuAddTargetPS3 = new System.Windows.Forms.ContextMenu();
			this.ctxtAddTargetPS3 = new System.Windows.Forms.MenuItem();
			this.ctxtRemoveTargetPS3 = new System.Windows.Forms.MenuItem();
			this.ctxtmnuAddTargetPS4 = new System.Windows.Forms.ContextMenu();
			this.ctxtAddTargetPS4 = new System.Windows.Forms.MenuItem();
			this.ctxtRemoveTargetPS4 = new System.Windows.Forms.MenuItem();
			this.ctxtmnuAddTargetXB1 = new System.Windows.Forms.ContextMenu();
			this.ctxtAddTargetXB1 = new System.Windows.Forms.MenuItem();
			this.ctxtRemoveTargetXB1 = new System.Windows.Forms.MenuItem();
			this.tcOptions.SuspendLayout();
			this.tpBuildOptions.SuspendLayout();
			this.groupBox1.SuspendLayout();
			this.gbMainScrOpts.SuspendLayout();
			this.gbPCScrOpts.SuspendLayout();
			this.gbPs3ScrOpts.SuspendLayout();
			this.gbXboxScrOpts.SuspendLayout();
			this.tpDeployOpts.SuspendLayout();
			this.tpDeployPlatforms.SuspendLayout();
			this.tpXboxTargets.SuspendLayout();
			this.tbXb1Targets.SuspendLayout();
			this.tpPs3Targets.SuspendLayout();
			this.groupBox2.SuspendLayout();
			this.tbDeployPS4.SuspendLayout();
			this.gbPS4DepOpts.SuspendLayout();
			this.SuspendLayout();
			// 
			// btnOptApply
			// 
			this.btnOptApply.Location = new System.Drawing.Point(282, 560);
			this.btnOptApply.Name = "btnOptApply";
			this.btnOptApply.Size = new System.Drawing.Size(75, 23);
			this.btnOptApply.TabIndex = 30;
			this.btnOptApply.Text = "&Apply";
			this.btnOptApply.UseVisualStyleBackColor = true;
			this.btnOptApply.Click += new System.EventHandler(this.btnOptApply_Click);
			// 
			// btnOptCancel
			// 
			this.btnOptCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnOptCancel.Location = new System.Drawing.Point(201, 560);
			this.btnOptCancel.Name = "btnOptCancel";
			this.btnOptCancel.Size = new System.Drawing.Size(75, 23);
			this.btnOptCancel.TabIndex = 31;
			this.btnOptCancel.Text = "&Cancel";
			this.btnOptCancel.UseVisualStyleBackColor = true;
			this.btnOptCancel.Click += new System.EventHandler(this.btnOptCancel_Click);
			// 
			// btnOptsOK
			// 
			this.btnOptsOK.Location = new System.Drawing.Point(120, 560);
			this.btnOptsOK.Name = "btnOptsOK";
			this.btnOptsOK.Size = new System.Drawing.Size(75, 23);
			this.btnOptsOK.TabIndex = 32;
			this.btnOptsOK.Text = "&OK";
			this.btnOptsOK.UseVisualStyleBackColor = true;
			this.btnOptsOK.Click += new System.EventHandler(this.btnOptsOK_Click);
			// 
			// tcOptions
			// 
			this.tcOptions.Controls.Add(this.tpBuildOptions);
			this.tcOptions.Controls.Add(this.tpDeployOpts);
			this.tcOptions.Dock = System.Windows.Forms.DockStyle.Top;
			this.tcOptions.Location = new System.Drawing.Point(0, 0);
			this.tcOptions.Name = "tcOptions";
			this.tcOptions.SelectedIndex = 0;
			this.tcOptions.Size = new System.Drawing.Size(363, 554);
			this.tcOptions.TabIndex = 4;
			// 
			// tpBuildOptions
			// 
			this.tpBuildOptions.Controls.Add(this.groupBox1);
			this.tpBuildOptions.Controls.Add(this.gbMainScrOpts);
			this.tpBuildOptions.Controls.Add(this.gbPCScrOpts);
			this.tpBuildOptions.Controls.Add(this.gbPs3ScrOpts);
			this.tpBuildOptions.Controls.Add(this.gbXboxScrOpts);
			this.tpBuildOptions.Location = new System.Drawing.Point(4, 22);
			this.tpBuildOptions.Name = "tpBuildOptions";
			this.tpBuildOptions.Padding = new System.Windows.Forms.Padding(3);
			this.tpBuildOptions.Size = new System.Drawing.Size(355, 528);
			this.tpBuildOptions.TabIndex = 0;
			this.tpBuildOptions.Text = "Build";
			this.tpBuildOptions.UseVisualStyleBackColor = true;
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.btnBrowsePS4PkgTarg);
			this.groupBox1.Controls.Add(this.txtPS4PkgTarg);
			this.groupBox1.Controls.Add(this.lblPS4PkgTarg);
			this.groupBox1.Controls.Add(this.btnBrowsePS4CtrlTarg);
			this.groupBox1.Controls.Add(this.txtPS4CtrlTarg);
			this.groupBox1.Controls.Add(this.lblPS4CtrlTarg);
			this.groupBox1.Controls.Add(this.btnPs3CtrlBrowse);
			this.groupBox1.Controls.Add(this.txtPs3Ctrl);
			this.groupBox1.Controls.Add(this.lblPs3Ctrl);
			this.groupBox1.Controls.Add(this.btnPs3Edat);
			this.groupBox1.Controls.Add(this.txtPs3Edat);
			this.groupBox1.Controls.Add(this.lblPs3Edat);
			this.groupBox1.Controls.Add(this.btnPs3PkgBrowse);
			this.groupBox1.Controls.Add(this.txtPs3PkgExe);
			this.groupBox1.Controls.Add(this.lblPs3Pkg);
			this.groupBox1.Controls.Add(this.btnBlastBrowse);
			this.groupBox1.Controls.Add(this.txtBlastExe);
			this.groupBox1.Controls.Add(this.lblBlastExe);
			this.groupBox1.Location = new System.Drawing.Point(8, 340);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(341, 182);
			this.groupBox1.TabIndex = 20;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Targets";
			// 
			// btnBrowsePS4PkgTarg
			// 
			this.btnBrowsePS4PkgTarg.Location = new System.Drawing.Point(308, 121);
			this.btnBrowsePS4PkgTarg.Name = "btnBrowsePS4PkgTarg";
			this.btnBrowsePS4PkgTarg.Size = new System.Drawing.Size(27, 23);
			this.btnBrowsePS4PkgTarg.TabIndex = 38;
			this.btnBrowsePS4PkgTarg.Text = "...";
			this.btnBrowsePS4PkgTarg.UseVisualStyleBackColor = true;
			// 
			// txtPS4PkgTarg
			// 
			this.txtPS4PkgTarg.Location = new System.Drawing.Point(68, 123);
			this.txtPS4PkgTarg.Name = "txtPS4PkgTarg";
			this.txtPS4PkgTarg.Size = new System.Drawing.Size(234, 20);
			this.txtPS4PkgTarg.TabIndex = 37;
			// 
			// lblPS4PkgTarg
			// 
			this.lblPS4PkgTarg.AutoSize = true;
			this.lblPS4PkgTarg.Location = new System.Drawing.Point(10, 126);
			this.lblPS4PkgTarg.Name = "lblPS4PkgTarg";
			this.lblPS4PkgTarg.Size = new System.Drawing.Size(52, 13);
			this.lblPS4PkgTarg.TabIndex = 36;
			this.lblPS4PkgTarg.Text = "PS4 Pkg:";
			// 
			// btnBrowsePS4CtrlTarg
			// 
			this.btnBrowsePS4CtrlTarg.Location = new System.Drawing.Point(308, 147);
			this.btnBrowsePS4CtrlTarg.Name = "btnBrowsePS4CtrlTarg";
			this.btnBrowsePS4CtrlTarg.Size = new System.Drawing.Size(27, 23);
			this.btnBrowsePS4CtrlTarg.TabIndex = 35;
			this.btnBrowsePS4CtrlTarg.Text = "...";
			this.btnBrowsePS4CtrlTarg.UseVisualStyleBackColor = true;
			// 
			// txtPS4CtrlTarg
			// 
			this.txtPS4CtrlTarg.Location = new System.Drawing.Point(68, 149);
			this.txtPS4CtrlTarg.Name = "txtPS4CtrlTarg";
			this.txtPS4CtrlTarg.Size = new System.Drawing.Size(234, 20);
			this.txtPS4CtrlTarg.TabIndex = 34;
			// 
			// lblPS4CtrlTarg
			// 
			this.lblPS4CtrlTarg.AutoSize = true;
			this.lblPS4CtrlTarg.Location = new System.Drawing.Point(14, 152);
			this.lblPS4CtrlTarg.Name = "lblPS4CtrlTarg";
			this.lblPS4CtrlTarg.Size = new System.Drawing.Size(48, 13);
			this.lblPS4CtrlTarg.TabIndex = 33;
			this.lblPS4CtrlTarg.Text = "PS4 Ctrl:";
			// 
			// btnPs3CtrlBrowse
			// 
			this.btnPs3CtrlBrowse.Location = new System.Drawing.Point(308, 95);
			this.btnPs3CtrlBrowse.Name = "btnPs3CtrlBrowse";
			this.btnPs3CtrlBrowse.Size = new System.Drawing.Size(27, 23);
			this.btnPs3CtrlBrowse.TabIndex = 32;
			this.btnPs3CtrlBrowse.Text = "...";
			this.btnPs3CtrlBrowse.UseVisualStyleBackColor = true;
			// 
			// txtPs3Ctrl
			// 
			this.txtPs3Ctrl.Location = new System.Drawing.Point(68, 97);
			this.txtPs3Ctrl.Name = "txtPs3Ctrl";
			this.txtPs3Ctrl.Size = new System.Drawing.Size(234, 20);
			this.txtPs3Ctrl.TabIndex = 31;
			// 
			// lblPs3Ctrl
			// 
			this.lblPs3Ctrl.AutoSize = true;
			this.lblPs3Ctrl.Location = new System.Drawing.Point(14, 100);
			this.lblPs3Ctrl.Name = "lblPs3Ctrl";
			this.lblPs3Ctrl.Size = new System.Drawing.Size(48, 13);
			this.lblPs3Ctrl.TabIndex = 30;
			this.lblPs3Ctrl.Text = "PS3 Ctrl:";
			// 
			// btnPs3Edat
			// 
			this.btnPs3Edat.Location = new System.Drawing.Point(308, 69);
			this.btnPs3Edat.Name = "btnPs3Edat";
			this.btnPs3Edat.Size = new System.Drawing.Size(27, 23);
			this.btnPs3Edat.TabIndex = 26;
			this.btnPs3Edat.Text = "...";
			this.btnPs3Edat.UseVisualStyleBackColor = true;
			this.btnPs3Edat.Click += new System.EventHandler(this.findExecutableTarget);
			// 
			// txtPs3Edat
			// 
			this.txtPs3Edat.Location = new System.Drawing.Point(68, 71);
			this.txtPs3Edat.Name = "txtPs3Edat";
			this.txtPs3Edat.Size = new System.Drawing.Size(234, 20);
			this.txtPs3Edat.TabIndex = 25;
			// 
			// lblPs3Edat
			// 
			this.lblPs3Edat.AutoSize = true;
			this.lblPs3Edat.Location = new System.Drawing.Point(7, 74);
			this.lblPs3Edat.Name = "lblPs3Edat";
			this.lblPs3Edat.Size = new System.Drawing.Size(55, 13);
			this.lblPs3Edat.TabIndex = 18;
			this.lblPs3Edat.Text = "PS3 Edat:";
			// 
			// btnPs3PkgBrowse
			// 
			this.btnPs3PkgBrowse.Location = new System.Drawing.Point(308, 43);
			this.btnPs3PkgBrowse.Name = "btnPs3PkgBrowse";
			this.btnPs3PkgBrowse.Size = new System.Drawing.Size(27, 23);
			this.btnPs3PkgBrowse.TabIndex = 24;
			this.btnPs3PkgBrowse.Text = "...";
			this.btnPs3PkgBrowse.UseVisualStyleBackColor = true;
			this.btnPs3PkgBrowse.Click += new System.EventHandler(this.findExecutableTarget);
			// 
			// txtPs3PkgExe
			// 
			this.txtPs3PkgExe.Location = new System.Drawing.Point(68, 45);
			this.txtPs3PkgExe.Name = "txtPs3PkgExe";
			this.txtPs3PkgExe.Size = new System.Drawing.Size(234, 20);
			this.txtPs3PkgExe.TabIndex = 23;
			// 
			// lblPs3Pkg
			// 
			this.lblPs3Pkg.AutoSize = true;
			this.lblPs3Pkg.Location = new System.Drawing.Point(10, 48);
			this.lblPs3Pkg.Name = "lblPs3Pkg";
			this.lblPs3Pkg.Size = new System.Drawing.Size(52, 13);
			this.lblPs3Pkg.TabIndex = 15;
			this.lblPs3Pkg.Text = "PS3 Pkg:";
			// 
			// btnBlastBrowse
			// 
			this.btnBlastBrowse.Location = new System.Drawing.Point(308, 17);
			this.btnBlastBrowse.Name = "btnBlastBrowse";
			this.btnBlastBrowse.Size = new System.Drawing.Size(27, 23);
			this.btnBlastBrowse.TabIndex = 22;
			this.btnBlastBrowse.Text = "...";
			this.btnBlastBrowse.UseVisualStyleBackColor = true;
			this.btnBlastBrowse.Click += new System.EventHandler(this.findExecutableTarget);
			// 
			// txtBlastExe
			// 
			this.txtBlastExe.Location = new System.Drawing.Point(68, 19);
			this.txtBlastExe.Name = "txtBlastExe";
			this.txtBlastExe.Size = new System.Drawing.Size(234, 20);
			this.txtBlastExe.TabIndex = 21;
			// 
			// lblBlastExe
			// 
			this.lblBlastExe.AutoSize = true;
			this.lblBlastExe.Location = new System.Drawing.Point(29, 22);
			this.lblBlastExe.Name = "lblBlastExe";
			this.lblBlastExe.Size = new System.Drawing.Size(33, 13);
			this.lblBlastExe.TabIndex = 3;
			this.lblBlastExe.Text = "Blast:";
			// 
			// gbMainScrOpts
			// 
			this.gbMainScrOpts.Controls.Add(this.txtMainPreBuildScr);
			this.gbMainScrOpts.Controls.Add(this.btnMainPostBuildBrowse);
			this.gbMainScrOpts.Controls.Add(this.lblMainPreBuild);
			this.gbMainScrOpts.Controls.Add(this.btnMainPreBuildBrowse);
			this.gbMainScrOpts.Controls.Add(this.lblMainPostBuild);
			this.gbMainScrOpts.Controls.Add(this.txtMainPostBuildScr);
			this.gbMainScrOpts.Location = new System.Drawing.Point(8, 6);
			this.gbMainScrOpts.Name = "gbMainScrOpts";
			this.gbMainScrOpts.Size = new System.Drawing.Size(341, 73);
			this.gbMainScrOpts.TabIndex = 0;
			this.gbMainScrOpts.TabStop = false;
			this.gbMainScrOpts.Text = "Main";
			// 
			// txtMainPreBuildScr
			// 
			this.txtMainPreBuildScr.Location = new System.Drawing.Point(68, 19);
			this.txtMainPreBuildScr.Name = "txtMainPreBuildScr";
			this.txtMainPreBuildScr.Size = new System.Drawing.Size(234, 20);
			this.txtMainPreBuildScr.TabIndex = 1;
			// 
			// btnMainPostBuildBrowse
			// 
			this.btnMainPostBuildBrowse.Location = new System.Drawing.Point(308, 43);
			this.btnMainPostBuildBrowse.Name = "btnMainPostBuildBrowse";
			this.btnMainPostBuildBrowse.Size = new System.Drawing.Size(27, 23);
			this.btnMainPostBuildBrowse.TabIndex = 4;
			this.btnMainPostBuildBrowse.Text = "...";
			this.btnMainPostBuildBrowse.UseVisualStyleBackColor = true;
			this.btnMainPostBuildBrowse.Click += new System.EventHandler(this.findScriptFileTarget);
			// 
			// lblMainPreBuild
			// 
			this.lblMainPreBuild.AutoSize = true;
			this.lblMainPreBuild.Location = new System.Drawing.Point(11, 21);
			this.lblMainPreBuild.Name = "lblMainPreBuild";
			this.lblMainPreBuild.Size = new System.Drawing.Size(51, 13);
			this.lblMainPreBuild.TabIndex = 9;
			this.lblMainPreBuild.Text = "Pre-build:";
			// 
			// btnMainPreBuildBrowse
			// 
			this.btnMainPreBuildBrowse.Location = new System.Drawing.Point(308, 16);
			this.btnMainPreBuildBrowse.Name = "btnMainPreBuildBrowse";
			this.btnMainPreBuildBrowse.Size = new System.Drawing.Size(27, 23);
			this.btnMainPreBuildBrowse.TabIndex = 2;
			this.btnMainPreBuildBrowse.Text = "...";
			this.btnMainPreBuildBrowse.UseVisualStyleBackColor = true;
			this.btnMainPreBuildBrowse.Click += new System.EventHandler(this.findScriptFileTarget);
			// 
			// lblMainPostBuild
			// 
			this.lblMainPostBuild.AutoSize = true;
			this.lblMainPostBuild.Location = new System.Drawing.Point(6, 48);
			this.lblMainPostBuild.Name = "lblMainPostBuild";
			this.lblMainPostBuild.Size = new System.Drawing.Size(56, 13);
			this.lblMainPostBuild.TabIndex = 11;
			this.lblMainPostBuild.Text = "Post-build:";
			// 
			// txtMainPostBuildScr
			// 
			this.txtMainPostBuildScr.Location = new System.Drawing.Point(68, 45);
			this.txtMainPostBuildScr.Name = "txtMainPostBuildScr";
			this.txtMainPostBuildScr.Size = new System.Drawing.Size(234, 20);
			this.txtMainPostBuildScr.TabIndex = 3;
			// 
			// gbPCScrOpts
			// 
			this.gbPCScrOpts.Controls.Add(this.btnPCPostBuildBrowse);
			this.gbPCScrOpts.Controls.Add(this.btnPCPreBuildBrowse);
			this.gbPCScrOpts.Controls.Add(this.txtPCPostBuildScr);
			this.gbPCScrOpts.Controls.Add(this.lblPCPostBuild);
			this.gbPCScrOpts.Controls.Add(this.txtPCPreBuildScr);
			this.gbPCScrOpts.Controls.Add(this.lblPCPreBuild);
			this.gbPCScrOpts.Location = new System.Drawing.Point(8, 255);
			this.gbPCScrOpts.Name = "gbPCScrOpts";
			this.gbPCScrOpts.Size = new System.Drawing.Size(341, 79);
			this.gbPCScrOpts.TabIndex = 15;
			this.gbPCScrOpts.TabStop = false;
			this.gbPCScrOpts.Text = "PC";
			// 
			// btnPCPostBuildBrowse
			// 
			this.btnPCPostBuildBrowse.Location = new System.Drawing.Point(308, 44);
			this.btnPCPostBuildBrowse.Name = "btnPCPostBuildBrowse";
			this.btnPCPostBuildBrowse.Size = new System.Drawing.Size(27, 23);
			this.btnPCPostBuildBrowse.TabIndex = 19;
			this.btnPCPostBuildBrowse.Text = "...";
			this.btnPCPostBuildBrowse.UseVisualStyleBackColor = true;
			this.btnPCPostBuildBrowse.Click += new System.EventHandler(this.findScriptFileTarget);
			// 
			// btnPCPreBuildBrowse
			// 
			this.btnPCPreBuildBrowse.Location = new System.Drawing.Point(308, 17);
			this.btnPCPreBuildBrowse.Name = "btnPCPreBuildBrowse";
			this.btnPCPreBuildBrowse.Size = new System.Drawing.Size(27, 23);
			this.btnPCPreBuildBrowse.TabIndex = 17;
			this.btnPCPreBuildBrowse.Text = "...";
			this.btnPCPreBuildBrowse.UseVisualStyleBackColor = true;
			this.btnPCPreBuildBrowse.Click += new System.EventHandler(this.findScriptFileTarget);
			// 
			// txtPCPostBuildScr
			// 
			this.txtPCPostBuildScr.Location = new System.Drawing.Point(68, 45);
			this.txtPCPostBuildScr.Name = "txtPCPostBuildScr";
			this.txtPCPostBuildScr.Size = new System.Drawing.Size(234, 20);
			this.txtPCPostBuildScr.TabIndex = 18;
			// 
			// lblPCPostBuild
			// 
			this.lblPCPostBuild.AutoSize = true;
			this.lblPCPostBuild.Location = new System.Drawing.Point(6, 48);
			this.lblPCPostBuild.Name = "lblPCPostBuild";
			this.lblPCPostBuild.Size = new System.Drawing.Size(56, 13);
			this.lblPCPostBuild.TabIndex = 5;
			this.lblPCPostBuild.Text = "Post-build:";
			// 
			// txtPCPreBuildScr
			// 
			this.txtPCPreBuildScr.Location = new System.Drawing.Point(68, 19);
			this.txtPCPreBuildScr.Name = "txtPCPreBuildScr";
			this.txtPCPreBuildScr.Size = new System.Drawing.Size(234, 20);
			this.txtPCPreBuildScr.TabIndex = 16;
			// 
			// lblPCPreBuild
			// 
			this.lblPCPreBuild.AutoSize = true;
			this.lblPCPreBuild.Location = new System.Drawing.Point(11, 22);
			this.lblPCPreBuild.Name = "lblPCPreBuild";
			this.lblPCPreBuild.Size = new System.Drawing.Size(51, 13);
			this.lblPCPreBuild.TabIndex = 3;
			this.lblPCPreBuild.Text = "Pre-build:";
			// 
			// gbPs3ScrOpts
			// 
			this.gbPs3ScrOpts.Controls.Add(this.btnPs3PostBuildBrowse);
			this.gbPs3ScrOpts.Controls.Add(this.btnPs3PreBuildBrowse);
			this.gbPs3ScrOpts.Controls.Add(this.txtPs3PostBuildScr);
			this.gbPs3ScrOpts.Controls.Add(this.lblPs3PostBuild);
			this.gbPs3ScrOpts.Controls.Add(this.txtPs3PreBuildScr);
			this.gbPs3ScrOpts.Controls.Add(this.lblPs3PreBuild);
			this.gbPs3ScrOpts.Location = new System.Drawing.Point(8, 170);
			this.gbPs3ScrOpts.Name = "gbPs3ScrOpts";
			this.gbPs3ScrOpts.Size = new System.Drawing.Size(341, 79);
			this.gbPs3ScrOpts.TabIndex = 10;
			this.gbPs3ScrOpts.TabStop = false;
			this.gbPs3ScrOpts.Text = "PS3";
			// 
			// btnPs3PostBuildBrowse
			// 
			this.btnPs3PostBuildBrowse.Location = new System.Drawing.Point(308, 43);
			this.btnPs3PostBuildBrowse.Name = "btnPs3PostBuildBrowse";
			this.btnPs3PostBuildBrowse.Size = new System.Drawing.Size(27, 23);
			this.btnPs3PostBuildBrowse.TabIndex = 14;
			this.btnPs3PostBuildBrowse.Text = "...";
			this.btnPs3PostBuildBrowse.UseVisualStyleBackColor = true;
			this.btnPs3PostBuildBrowse.Click += new System.EventHandler(this.findScriptFileTarget);
			// 
			// btnPs3PreBuildBrowse
			// 
			this.btnPs3PreBuildBrowse.Location = new System.Drawing.Point(308, 16);
			this.btnPs3PreBuildBrowse.Name = "btnPs3PreBuildBrowse";
			this.btnPs3PreBuildBrowse.Size = new System.Drawing.Size(27, 23);
			this.btnPs3PreBuildBrowse.TabIndex = 12;
			this.btnPs3PreBuildBrowse.Text = "...";
			this.btnPs3PreBuildBrowse.UseVisualStyleBackColor = true;
			this.btnPs3PreBuildBrowse.Click += new System.EventHandler(this.findScriptFileTarget);
			// 
			// txtPs3PostBuildScr
			// 
			this.txtPs3PostBuildScr.Location = new System.Drawing.Point(68, 45);
			this.txtPs3PostBuildScr.Name = "txtPs3PostBuildScr";
			this.txtPs3PostBuildScr.Size = new System.Drawing.Size(234, 20);
			this.txtPs3PostBuildScr.TabIndex = 13;
			// 
			// lblPs3PostBuild
			// 
			this.lblPs3PostBuild.AutoSize = true;
			this.lblPs3PostBuild.Location = new System.Drawing.Point(6, 48);
			this.lblPs3PostBuild.Name = "lblPs3PostBuild";
			this.lblPs3PostBuild.Size = new System.Drawing.Size(56, 13);
			this.lblPs3PostBuild.TabIndex = 5;
			this.lblPs3PostBuild.Text = "Post-build:";
			// 
			// txtPs3PreBuildScr
			// 
			this.txtPs3PreBuildScr.Location = new System.Drawing.Point(68, 19);
			this.txtPs3PreBuildScr.Name = "txtPs3PreBuildScr";
			this.txtPs3PreBuildScr.Size = new System.Drawing.Size(234, 20);
			this.txtPs3PreBuildScr.TabIndex = 11;
			// 
			// lblPs3PreBuild
			// 
			this.lblPs3PreBuild.AutoSize = true;
			this.lblPs3PreBuild.Location = new System.Drawing.Point(11, 22);
			this.lblPs3PreBuild.Name = "lblPs3PreBuild";
			this.lblPs3PreBuild.Size = new System.Drawing.Size(51, 13);
			this.lblPs3PreBuild.TabIndex = 3;
			this.lblPs3PreBuild.Text = "Pre-build:";
			// 
			// gbXboxScrOpts
			// 
			this.gbXboxScrOpts.Controls.Add(this.btnXboxPostBuildBrowse);
			this.gbXboxScrOpts.Controls.Add(this.btnXboxPreBuildBrowse);
			this.gbXboxScrOpts.Controls.Add(this.txtXboxPostBuildScr);
			this.gbXboxScrOpts.Controls.Add(this.lblXboxPostBuild);
			this.gbXboxScrOpts.Controls.Add(this.txtXboxPreBuildScr);
			this.gbXboxScrOpts.Controls.Add(this.lblXboxPreBuild);
			this.gbXboxScrOpts.Location = new System.Drawing.Point(8, 85);
			this.gbXboxScrOpts.Name = "gbXboxScrOpts";
			this.gbXboxScrOpts.Size = new System.Drawing.Size(341, 79);
			this.gbXboxScrOpts.TabIndex = 5;
			this.gbXboxScrOpts.TabStop = false;
			this.gbXboxScrOpts.Text = "Xbox";
			// 
			// btnXboxPostBuildBrowse
			// 
			this.btnXboxPostBuildBrowse.Location = new System.Drawing.Point(308, 43);
			this.btnXboxPostBuildBrowse.Name = "btnXboxPostBuildBrowse";
			this.btnXboxPostBuildBrowse.Size = new System.Drawing.Size(27, 23);
			this.btnXboxPostBuildBrowse.TabIndex = 9;
			this.btnXboxPostBuildBrowse.Text = "...";
			this.btnXboxPostBuildBrowse.UseVisualStyleBackColor = true;
			this.btnXboxPostBuildBrowse.Click += new System.EventHandler(this.findScriptFileTarget);
			// 
			// btnXboxPreBuildBrowse
			// 
			this.btnXboxPreBuildBrowse.Location = new System.Drawing.Point(308, 17);
			this.btnXboxPreBuildBrowse.Name = "btnXboxPreBuildBrowse";
			this.btnXboxPreBuildBrowse.Size = new System.Drawing.Size(27, 23);
			this.btnXboxPreBuildBrowse.TabIndex = 7;
			this.btnXboxPreBuildBrowse.Text = "...";
			this.btnXboxPreBuildBrowse.UseVisualStyleBackColor = true;
			this.btnXboxPreBuildBrowse.Click += new System.EventHandler(this.findScriptFileTarget);
			// 
			// txtXboxPostBuildScr
			// 
			this.txtXboxPostBuildScr.Location = new System.Drawing.Point(68, 45);
			this.txtXboxPostBuildScr.Name = "txtXboxPostBuildScr";
			this.txtXboxPostBuildScr.Size = new System.Drawing.Size(234, 20);
			this.txtXboxPostBuildScr.TabIndex = 8;
			// 
			// lblXboxPostBuild
			// 
			this.lblXboxPostBuild.AutoSize = true;
			this.lblXboxPostBuild.Location = new System.Drawing.Point(6, 48);
			this.lblXboxPostBuild.Name = "lblXboxPostBuild";
			this.lblXboxPostBuild.Size = new System.Drawing.Size(56, 13);
			this.lblXboxPostBuild.TabIndex = 5;
			this.lblXboxPostBuild.Text = "Post-build:";
			// 
			// txtXboxPreBuildScr
			// 
			this.txtXboxPreBuildScr.Location = new System.Drawing.Point(68, 19);
			this.txtXboxPreBuildScr.Name = "txtXboxPreBuildScr";
			this.txtXboxPreBuildScr.Size = new System.Drawing.Size(234, 20);
			this.txtXboxPreBuildScr.TabIndex = 6;
			// 
			// lblXboxPreBuild
			// 
			this.lblXboxPreBuild.AutoSize = true;
			this.lblXboxPreBuild.Location = new System.Drawing.Point(11, 22);
			this.lblXboxPreBuild.Name = "lblXboxPreBuild";
			this.lblXboxPreBuild.Size = new System.Drawing.Size(51, 13);
			this.lblXboxPreBuild.TabIndex = 3;
			this.lblXboxPreBuild.Text = "Pre-build:";
			// 
			// tpDeployOpts
			// 
			this.tpDeployOpts.Controls.Add(this.tpDeployPlatforms);
			this.tpDeployOpts.Location = new System.Drawing.Point(4, 22);
			this.tpDeployOpts.Name = "tpDeployOpts";
			this.tpDeployOpts.Padding = new System.Windows.Forms.Padding(3);
			this.tpDeployOpts.Size = new System.Drawing.Size(355, 528);
			this.tpDeployOpts.TabIndex = 1;
			this.tpDeployOpts.Text = "Deployment";
			this.tpDeployOpts.UseVisualStyleBackColor = true;
			// 
			// tpDeployPlatforms
			// 
			this.tpDeployPlatforms.Controls.Add(this.tpXboxTargets);
			this.tpDeployPlatforms.Controls.Add(this.tbXb1Targets);
			this.tpDeployPlatforms.Controls.Add(this.tpPs3Targets);
			this.tpDeployPlatforms.Controls.Add(this.tbDeployPS4);
			this.tpDeployPlatforms.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tpDeployPlatforms.Location = new System.Drawing.Point(3, 3);
			this.tpDeployPlatforms.Name = "tpDeployPlatforms";
			this.tpDeployPlatforms.SelectedIndex = 0;
			this.tpDeployPlatforms.Size = new System.Drawing.Size(349, 522);
			this.tpDeployPlatforms.TabIndex = 1;
			// 
			// tpXboxTargets
			// 
			this.tpXboxTargets.Controls.Add(this.lvXboxTargets);
			this.tpXboxTargets.Location = new System.Drawing.Point(4, 22);
			this.tpXboxTargets.Name = "tpXboxTargets";
			this.tpXboxTargets.Padding = new System.Windows.Forms.Padding(3);
			this.tpXboxTargets.Size = new System.Drawing.Size(341, 496);
			this.tpXboxTargets.TabIndex = 0;
			this.tpXboxTargets.Text = "XBOX";
			this.tpXboxTargets.UseVisualStyleBackColor = true;
			// 
			// lvXboxTargets
			// 
			this.lvXboxTargets.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.lvXboxTargets.CheckBoxes = true;
			this.lvXboxTargets.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.lvtxtXboxDeploy,
            this.lvtxtXboxAddress});
			this.lvXboxTargets.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lvXboxTargets.FullRowSelect = true;
			this.lvXboxTargets.GridLines = true;
			this.lvXboxTargets.Location = new System.Drawing.Point(3, 3);
			this.lvXboxTargets.Name = "lvXboxTargets";
			this.lvXboxTargets.Size = new System.Drawing.Size(335, 490);
			this.lvXboxTargets.TabIndex = 0;
			this.lvXboxTargets.UseCompatibleStateImageBehavior = false;
			this.lvXboxTargets.View = System.Windows.Forms.View.Details;
			// 
			// lvtxtXboxDeploy
			// 
			this.lvtxtXboxDeploy.Text = "Deploy";
			this.lvtxtXboxDeploy.Width = 47;
			// 
			// lvtxtXboxAddress
			// 
			this.lvtxtXboxAddress.Text = "Address";
			this.lvtxtXboxAddress.Width = 288;
			// 
			// tbXb1Targets
			// 
			this.tbXb1Targets.Controls.Add(this.lvXB1Targets);
			this.tbXb1Targets.Location = new System.Drawing.Point(4, 22);
			this.tbXb1Targets.Name = "tbXb1Targets";
			this.tbXb1Targets.Padding = new System.Windows.Forms.Padding(3);
			this.tbXb1Targets.Size = new System.Drawing.Size(341, 496);
			this.tbXb1Targets.TabIndex = 3;
			this.tbXb1Targets.Text = "XB1";
			this.tbXb1Targets.UseVisualStyleBackColor = true;
			// 
			// lvXB1Targets
			// 
			this.lvXB1Targets.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.lvXB1Targets.CheckBoxes = true;
			this.lvXB1Targets.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader5,
            this.columnHeader6});
			this.lvXB1Targets.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lvXB1Targets.FullRowSelect = true;
			this.lvXB1Targets.GridLines = true;
			this.lvXB1Targets.Location = new System.Drawing.Point(3, 3);
			this.lvXB1Targets.Name = "lvXB1Targets";
			this.lvXB1Targets.Size = new System.Drawing.Size(335, 490);
			this.lvXB1Targets.TabIndex = 1;
			this.lvXB1Targets.UseCompatibleStateImageBehavior = false;
			this.lvXB1Targets.View = System.Windows.Forms.View.Details;
			// 
			// columnHeader5
			// 
			this.columnHeader5.Text = "Deploy";
			this.columnHeader5.Width = 47;
			// 
			// columnHeader6
			// 
			this.columnHeader6.Text = "Address";
			this.columnHeader6.Width = 288;
			// 
			// tpPs3Targets
			// 
			this.tpPs3Targets.Controls.Add(this.groupBox2);
			this.tpPs3Targets.Controls.Add(this.lvPs3Targets);
			this.tpPs3Targets.Location = new System.Drawing.Point(4, 22);
			this.tpPs3Targets.Name = "tpPs3Targets";
			this.tpPs3Targets.Padding = new System.Windows.Forms.Padding(3);
			this.tpPs3Targets.Size = new System.Drawing.Size(341, 496);
			this.tpPs3Targets.TabIndex = 1;
			this.tpPs3Targets.Text = "PS3";
			this.tpPs3Targets.UseVisualStyleBackColor = true;
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.chkPs3JPNDeploy);
			this.groupBox2.Controls.Add(this.chkPs3EUDeploy);
			this.groupBox2.Controls.Add(this.chkPs3USDeploy);
			this.groupBox2.Location = new System.Drawing.Point(6, 445);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(329, 45);
			this.groupBox2.TabIndex = 2;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "PS3 Deploy Options";
			// 
			// chkPs3JPNDeploy
			// 
			this.chkPs3JPNDeploy.AutoSize = true;
			this.chkPs3JPNDeploy.Location = new System.Drawing.Point(100, 19);
			this.chkPs3JPNDeploy.Name = "chkPs3JPNDeploy";
			this.chkPs3JPNDeploy.Size = new System.Drawing.Size(46, 17);
			this.chkPs3JPNDeploy.TabIndex = 2;
			this.chkPs3JPNDeploy.Text = "JPN";
			this.chkPs3JPNDeploy.UseVisualStyleBackColor = true;
			// 
			// chkPs3EUDeploy
			// 
			this.chkPs3EUDeploy.AutoSize = true;
			this.chkPs3EUDeploy.Location = new System.Drawing.Point(53, 19);
			this.chkPs3EUDeploy.Name = "chkPs3EUDeploy";
			this.chkPs3EUDeploy.Size = new System.Drawing.Size(41, 17);
			this.chkPs3EUDeploy.TabIndex = 1;
			this.chkPs3EUDeploy.Text = "EU";
			this.chkPs3EUDeploy.UseVisualStyleBackColor = true;
			// 
			// chkPs3USDeploy
			// 
			this.chkPs3USDeploy.AutoSize = true;
			this.chkPs3USDeploy.Location = new System.Drawing.Point(6, 19);
			this.chkPs3USDeploy.Name = "chkPs3USDeploy";
			this.chkPs3USDeploy.Size = new System.Drawing.Size(41, 17);
			this.chkPs3USDeploy.TabIndex = 0;
			this.chkPs3USDeploy.Text = "US";
			this.chkPs3USDeploy.UseVisualStyleBackColor = true;
			// 
			// lvPs3Targets
			// 
			this.lvPs3Targets.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.lvPs3Targets.CheckBoxes = true;
			this.lvPs3Targets.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2});
			this.lvPs3Targets.Dock = System.Windows.Forms.DockStyle.Top;
			this.lvPs3Targets.FullRowSelect = true;
			this.lvPs3Targets.GridLines = true;
			this.lvPs3Targets.Location = new System.Drawing.Point(3, 3);
			this.lvPs3Targets.Name = "lvPs3Targets";
			this.lvPs3Targets.Size = new System.Drawing.Size(335, 436);
			this.lvPs3Targets.TabIndex = 1;
			this.lvPs3Targets.UseCompatibleStateImageBehavior = false;
			this.lvPs3Targets.View = System.Windows.Forms.View.Details;
			// 
			// columnHeader1
			// 
			this.columnHeader1.Text = "Deploy";
			this.columnHeader1.Width = 47;
			// 
			// columnHeader2
			// 
			this.columnHeader2.Text = "Address";
			this.columnHeader2.Width = 288;
			// 
			// tbDeployPS4
			// 
			this.tbDeployPS4.Controls.Add(this.gbPS4DepOpts);
			this.tbDeployPS4.Controls.Add(this.lvPS4Targets);
			this.tbDeployPS4.Location = new System.Drawing.Point(4, 22);
			this.tbDeployPS4.Name = "tbDeployPS4";
			this.tbDeployPS4.Padding = new System.Windows.Forms.Padding(3);
			this.tbDeployPS4.Size = new System.Drawing.Size(341, 496);
			this.tbDeployPS4.TabIndex = 2;
			this.tbDeployPS4.Text = "PS4";
			this.tbDeployPS4.UseVisualStyleBackColor = true;
			// 
			// gbPS4DepOpts
			// 
			this.gbPS4DepOpts.Controls.Add(this.cbPS4JNDepOpt);
			this.gbPS4DepOpts.Controls.Add(this.cbPS4EUDepOpt);
			this.gbPS4DepOpts.Controls.Add(this.cbPS4USDepOpt);
			this.gbPS4DepOpts.Location = new System.Drawing.Point(6, 445);
			this.gbPS4DepOpts.Name = "gbPS4DepOpts";
			this.gbPS4DepOpts.Size = new System.Drawing.Size(329, 45);
			this.gbPS4DepOpts.TabIndex = 4;
			this.gbPS4DepOpts.TabStop = false;
			this.gbPS4DepOpts.Text = "PS4 Deploy Options";
			// 
			// cbPS4JNDepOpt
			// 
			this.cbPS4JNDepOpt.AutoSize = true;
			this.cbPS4JNDepOpt.Location = new System.Drawing.Point(100, 19);
			this.cbPS4JNDepOpt.Name = "cbPS4JNDepOpt";
			this.cbPS4JNDepOpt.Size = new System.Drawing.Size(46, 17);
			this.cbPS4JNDepOpt.TabIndex = 2;
			this.cbPS4JNDepOpt.Text = "JPN";
			this.cbPS4JNDepOpt.UseVisualStyleBackColor = true;
			// 
			// cbPS4EUDepOpt
			// 
			this.cbPS4EUDepOpt.AutoSize = true;
			this.cbPS4EUDepOpt.Location = new System.Drawing.Point(53, 19);
			this.cbPS4EUDepOpt.Name = "cbPS4EUDepOpt";
			this.cbPS4EUDepOpt.Size = new System.Drawing.Size(41, 17);
			this.cbPS4EUDepOpt.TabIndex = 1;
			this.cbPS4EUDepOpt.Text = "EU";
			this.cbPS4EUDepOpt.UseVisualStyleBackColor = true;
			// 
			// cbPS4USDepOpt
			// 
			this.cbPS4USDepOpt.AutoSize = true;
			this.cbPS4USDepOpt.Location = new System.Drawing.Point(6, 19);
			this.cbPS4USDepOpt.Name = "cbPS4USDepOpt";
			this.cbPS4USDepOpt.Size = new System.Drawing.Size(41, 17);
			this.cbPS4USDepOpt.TabIndex = 0;
			this.cbPS4USDepOpt.Text = "US";
			this.cbPS4USDepOpt.UseVisualStyleBackColor = true;
			// 
			// lvPS4Targets
			// 
			this.lvPS4Targets.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.lvPS4Targets.CheckBoxes = true;
			this.lvPS4Targets.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader3,
            this.columnHeader4});
			this.lvPS4Targets.Dock = System.Windows.Forms.DockStyle.Top;
			this.lvPS4Targets.FullRowSelect = true;
			this.lvPS4Targets.GridLines = true;
			this.lvPS4Targets.Location = new System.Drawing.Point(3, 3);
			this.lvPS4Targets.Name = "lvPS4Targets";
			this.lvPS4Targets.Size = new System.Drawing.Size(335, 436);
			this.lvPS4Targets.TabIndex = 3;
			this.lvPS4Targets.UseCompatibleStateImageBehavior = false;
			this.lvPS4Targets.View = System.Windows.Forms.View.Details;
			// 
			// columnHeader3
			// 
			this.columnHeader3.Text = "Deploy";
			this.columnHeader3.Width = 47;
			// 
			// columnHeader4
			// 
			this.columnHeader4.Text = "Address";
			this.columnHeader4.Width = 288;
			// 
			// ofdScriptTargets
			// 
			this.ofdScriptTargets.DefaultExt = "py";
			this.ofdScriptTargets.Filter = "Python sripts|*.py";
			this.ofdScriptTargets.Title = "Select python script...";
			// 
			// ctxtmnuAddTarget360
			// 
			this.ctxtmnuAddTarget360.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.ctxtAddTarget360,
            this.ctxtRemoveTarget360});
			this.ctxtmnuAddTarget360.Popup += new System.EventHandler(this.ctxtmnuAddTarget_Popup);
			// 
			// ctxtAddTarget360
			// 
			this.ctxtAddTarget360.Index = 0;
			this.ctxtAddTarget360.Text = "Add target";
			this.ctxtAddTarget360.Click += new System.EventHandler(this.ctxtAddTarget_Click);
			// 
			// ctxtRemoveTarget360
			// 
			this.ctxtRemoveTarget360.Index = 1;
			this.ctxtRemoveTarget360.Text = "Remove target";
			this.ctxtRemoveTarget360.Click += new System.EventHandler(this.ctxtRemoveTarget_Click);
			// 
			// ofdExecutable
			// 
			this.ofdExecutable.DefaultExt = "exe";
			this.ofdExecutable.Filter = "Executable|*.exe";
			this.ofdExecutable.Title = "Select an executable...";
			// 
			// ctxtmnuAddTargetPS3
			// 
			this.ctxtmnuAddTargetPS3.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.ctxtAddTargetPS3,
            this.ctxtRemoveTargetPS3});
			this.ctxtmnuAddTargetPS3.Popup += new System.EventHandler(this.ctxtmnuAddTargetPS3_Popup);
			// 
			// ctxtAddTargetPS3
			// 
			this.ctxtAddTargetPS3.Index = 0;
			this.ctxtAddTargetPS3.Text = "Add target";
			this.ctxtAddTargetPS3.Click += new System.EventHandler(this.ctxtAddTargetPS3_Click);
			// 
			// ctxtRemoveTargetPS3
			// 
			this.ctxtRemoveTargetPS3.Index = 1;
			this.ctxtRemoveTargetPS3.Text = "Remove target";
			this.ctxtRemoveTargetPS3.Click += new System.EventHandler(this.ctxtRemoveTargetPS3_Click);
			// 
			// ctxtmnuAddTargetPS4
			// 
			this.ctxtmnuAddTargetPS4.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.ctxtAddTargetPS4,
            this.ctxtRemoveTargetPS4});
			this.ctxtmnuAddTargetPS4.Popup += new System.EventHandler(this.ctxtmnuAddTargetPS4_Popup);
			// 
			// ctxtAddTargetPS4
			// 
			this.ctxtAddTargetPS4.Index = 0;
			this.ctxtAddTargetPS4.Text = "Add target";
			this.ctxtAddTargetPS4.Click += new System.EventHandler(this.ctxtAddTargetPS4_Click);
			// 
			// ctxtRemoveTargetPS4
			// 
			this.ctxtRemoveTargetPS4.Index = 1;
			this.ctxtRemoveTargetPS4.Text = "Remove target";
			this.ctxtRemoveTargetPS4.Click += new System.EventHandler(this.ctxtRemoveTargetPS4_Click);
			// 
			// ctxtmnuAddTargetXB1
			// 
			this.ctxtmnuAddTargetXB1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.ctxtAddTargetXB1,
            this.ctxtRemoveTargetXB1});
			this.ctxtmnuAddTargetXB1.Popup += new System.EventHandler(this.ctxtmnuAddTargetXB1_Popup);
			// 
			// ctxtAddTargetXB1
			// 
			this.ctxtAddTargetXB1.Index = 0;
			this.ctxtAddTargetXB1.Text = "Add target";
			this.ctxtAddTargetXB1.Click += new System.EventHandler(this.ctxtAddTargetXB1_Click);
			// 
			// ctxtRemoveTargetXB1
			// 
			this.ctxtRemoveTargetXB1.Index = 1;
			this.ctxtRemoveTargetXB1.Text = "Remove target";
			this.ctxtRemoveTargetXB1.Click += new System.EventHandler(this.ctxtRemoveTargetXB1_Click);
			// 
			// frmOptions
			// 
			this.AcceptButton = this.btnOptsOK;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.CancelButton = this.btnOptCancel;
			this.ClientSize = new System.Drawing.Size(363, 590);
			this.Controls.Add(this.tcOptions);
			this.Controls.Add(this.btnOptsOK);
			this.Controls.Add(this.btnOptCancel);
			this.Controls.Add(this.btnOptApply);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "frmOptions";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Options";
			this.TopMost = true;
			this.Load += new System.EventHandler(this.frmOptions_Load);
			this.VisibleChanged += new System.EventHandler(this.frmOptions_VisibleChanged);
			this.tcOptions.ResumeLayout(false);
			this.tpBuildOptions.ResumeLayout(false);
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.gbMainScrOpts.ResumeLayout(false);
			this.gbMainScrOpts.PerformLayout();
			this.gbPCScrOpts.ResumeLayout(false);
			this.gbPCScrOpts.PerformLayout();
			this.gbPs3ScrOpts.ResumeLayout(false);
			this.gbPs3ScrOpts.PerformLayout();
			this.gbXboxScrOpts.ResumeLayout(false);
			this.gbXboxScrOpts.PerformLayout();
			this.tpDeployOpts.ResumeLayout(false);
			this.tpDeployPlatforms.ResumeLayout(false);
			this.tpXboxTargets.ResumeLayout(false);
			this.tbXb1Targets.ResumeLayout(false);
			this.tpPs3Targets.ResumeLayout(false);
			this.groupBox2.ResumeLayout(false);
			this.groupBox2.PerformLayout();
			this.tbDeployPS4.ResumeLayout(false);
			this.gbPS4DepOpts.ResumeLayout(false);
			this.gbPS4DepOpts.PerformLayout();
			this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnOptApply;
        private System.Windows.Forms.Button btnOptCancel;
        private System.Windows.Forms.Button btnOptsOK;
        private System.Windows.Forms.TabControl tcOptions;
        private System.Windows.Forms.TabPage tpBuildOptions;
        private System.Windows.Forms.TabPage tpDeployOpts;
        private System.Windows.Forms.GroupBox gbXboxScrOpts;
        private System.Windows.Forms.TextBox txtXboxPreBuildScr;
        private System.Windows.Forms.Label lblXboxPreBuild;
        private System.Windows.Forms.TextBox txtXboxPostBuildScr;
        private System.Windows.Forms.Label lblXboxPostBuild;
        private controlLinkedButton btnXboxPreBuildBrowse;
        private controlLinkedButton btnXboxPostBuildBrowse;
        private System.Windows.Forms.GroupBox gbPs3ScrOpts;
        private controlLinkedButton btnPs3PostBuildBrowse;
        private controlLinkedButton btnPs3PreBuildBrowse;
        private System.Windows.Forms.TextBox txtPs3PostBuildScr;
        private System.Windows.Forms.Label lblPs3PostBuild;
        private System.Windows.Forms.TextBox txtPs3PreBuildScr;
        private System.Windows.Forms.Label lblPs3PreBuild;
        private System.Windows.Forms.GroupBox gbPCScrOpts;
        private controlLinkedButton btnPCPostBuildBrowse;
        private controlLinkedButton btnPCPreBuildBrowse;
        private System.Windows.Forms.TextBox txtPCPostBuildScr;
        private System.Windows.Forms.Label lblPCPostBuild;
        private System.Windows.Forms.TextBox txtPCPreBuildScr;
        private System.Windows.Forms.Label lblPCPreBuild;
        private controlLinkedButton btnMainPostBuildBrowse;
        private controlLinkedButton btnMainPreBuildBrowse;
        private System.Windows.Forms.TextBox txtMainPostBuildScr;
        private System.Windows.Forms.Label lblMainPostBuild;
        private System.Windows.Forms.TextBox txtMainPreBuildScr;
        private System.Windows.Forms.Label lblMainPreBuild;
        private System.Windows.Forms.OpenFileDialog ofdScriptTargets;
        private System.Windows.Forms.GroupBox gbMainScrOpts;
        public System.Windows.Forms.ListView lvXboxTargets;
        private System.Windows.Forms.ColumnHeader lvtxtXboxDeploy;
        private System.Windows.Forms.TabControl tpDeployPlatforms;
        private System.Windows.Forms.TabPage tpXboxTargets;
        private System.Windows.Forms.ColumnHeader lvtxtXboxAddress;
        private System.Windows.Forms.ContextMenu ctxtmnuAddTarget360;
        private System.Windows.Forms.MenuItem ctxtAddTarget360;
        private System.Windows.Forms.MenuItem ctxtRemoveTarget360;
        private System.Windows.Forms.GroupBox groupBox1;
        private frmOptions.controlLinkedButton btnBlastBrowse;
        private System.Windows.Forms.TextBox txtBlastExe;
        private System.Windows.Forms.Label lblBlastExe;
        private frmOptions.controlLinkedButton btnPs3PkgBrowse;
        private System.Windows.Forms.TextBox txtPs3PkgExe;
        private System.Windows.Forms.Label lblPs3Pkg;
        private frmOptions.controlLinkedButton btnPs3Edat;
        private System.Windows.Forms.TextBox txtPs3Edat;
        private System.Windows.Forms.Label lblPs3Edat;
        private System.Windows.Forms.OpenFileDialog ofdExecutable;
        private System.Windows.Forms.FolderBrowserDialog fbdOutputFolder;
        private System.Windows.Forms.TabPage tpPs3Targets;
        public System.Windows.Forms.ListView lvPs3Targets;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ContextMenu ctxtmnuAddTargetPS3;
        private System.Windows.Forms.MenuItem ctxtAddTargetPS3;
        private System.Windows.Forms.MenuItem ctxtRemoveTargetPS3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox chkPs3JPNDeploy;
        private System.Windows.Forms.CheckBox chkPs3EUDeploy;
        private System.Windows.Forms.CheckBox chkPs3USDeploy;
        private frmOptions.controlLinkedButton btnPs3CtrlBrowse;
        private System.Windows.Forms.TextBox txtPs3Ctrl;
        private System.Windows.Forms.Label lblPs3Ctrl;
        private System.Windows.Forms.TabPage tbDeployPS4;
        private System.Windows.Forms.GroupBox gbPS4DepOpts;
        private System.Windows.Forms.CheckBox cbPS4JNDepOpt;
        private System.Windows.Forms.CheckBox cbPS4EUDepOpt;
        private System.Windows.Forms.CheckBox cbPS4USDepOpt;
        public System.Windows.Forms.ListView lvPS4Targets;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private frmOptions.controlLinkedButton btnBrowsePS4PkgTarg;
        private System.Windows.Forms.TextBox txtPS4PkgTarg;
        private System.Windows.Forms.Label lblPS4PkgTarg;
        private frmOptions.controlLinkedButton btnBrowsePS4CtrlTarg;
        private System.Windows.Forms.TextBox txtPS4CtrlTarg;
        private System.Windows.Forms.Label lblPS4CtrlTarg;
        private System.Windows.Forms.ContextMenu ctxtmnuAddTargetPS4;
        private System.Windows.Forms.MenuItem ctxtAddTargetPS4;
        private System.Windows.Forms.MenuItem ctxtRemoveTargetPS4;
		private System.Windows.Forms.TabPage tbXb1Targets;
		public System.Windows.Forms.ListView lvXB1Targets;
		private System.Windows.Forms.ColumnHeader columnHeader5;
		private System.Windows.Forms.ColumnHeader columnHeader6;
		private System.Windows.Forms.ContextMenu ctxtmnuAddTargetXB1;
		private System.Windows.Forms.MenuItem ctxtAddTargetXB1;
		private System.Windows.Forms.MenuItem ctxtRemoveTargetXB1;
    }
}
