﻿/*******************************************\
| ContentStar : Rockstar Games © 2013       |
| ------------------------------------------|
| Author: Thomas Woodhead (R* London)       |
| Email: thomas.woodhead@rockstarlondon.com |
\*******************************************/

#region Usings

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Collections;
using System.Xml;
using System.Xml.Linq;
using System.Diagnostics;
using System.Xml.Serialization;
using ContentStar;
using System.Threading;
using contentStar;

#endregion

namespace ContentStar
{
    #region GlobalFields

    [Serializable]
    public class GlobalFields
    {
        #region XBOX 360
        [XmlElement("GameTitle")]
        public string m_gameTitle;

        [XmlElement("TitleID")]
        public string m_titleID;
        #endregion

        #region PS3
        [XmlElement("USContentIDFirst")]
        public string m_UScontentIDFirst;

        [XmlElement("UScontentIDSecond")]
        public string m_UScontentIDSecond;

        [XmlElement("EUcontentIDFirst")]
        public string m_EUcontentIDFirst;

        [XmlElement("EUcontentIDSecond")]
        public string m_EUcontentIDSecond;

        [XmlElement("JPNcontentIDFirst")]
        public string m_JPNcontentIDFirst;

        [XmlElement("JPNcontentIDSecond")]
        public string m_JPNcontentIDSecond;

        [XmlElement("KLicensee")]
        public string m_kLicensee;
        #endregion
        
        #region PS4
        [XmlElement("PS4USContentIDFirst")]
        public string m_PS4UScontentIDFirst;

        [XmlElement("PS4UScontentIDSecond")]
        public string m_PS4UScontentIDSecond;

        [XmlElement("PS4EUcontentIDFirst")]
        public string m_PS4EUcontentIDFirst;

        [XmlElement("PS4EUcontentIDSecond")]
        public string m_PS4EUcontentIDSecond;

        [XmlElement("PS4JPNcontentIDFirst")]
        public string m_PS4JPNcontentIDFirst;

        [XmlElement("PS4JPNcontentIDSecond")]
        public string m_PS4JPNcontentIDSecond;

        [XmlElement("PS4Entitlement")]
        public string m_PS4Entitlement;
        #endregion

        public GlobalFields()
        {
            m_gameTitle = "";
            m_titleID = "";
            m_UScontentIDFirst = "";
            m_UScontentIDSecond = "";
            m_EUcontentIDFirst = "";
            m_EUcontentIDSecond = "";
            m_JPNcontentIDFirst = "";
            m_JPNcontentIDSecond = "";
            m_kLicensee = "";
            m_PS4UScontentIDFirst = "";
            m_PS4UScontentIDSecond = "";
            m_PS4EUcontentIDFirst = "";
            m_PS4EUcontentIDSecond = "";
            m_PS4JPNcontentIDFirst = "";
            m_PS4JPNcontentIDSecond = "";
            m_PS4Entitlement = "";
        }
    }

    #endregion

    #region GlobalFieldsBank

    [Serializable]
    public class GlobalFieldsBank
    {
        [XmlElement("Name")]
        public string m_name;

        [XmlElement("Fields")]
        public GlobalFields m_fields;

        public GlobalFieldsBank()
        {
            m_name = "";
            m_fields = new GlobalFields();
        }
    }

    #endregion

    #region GlobalFieldsManager

    [Serializable]
    [XmlRoot("GlobalBanks")]
    public class GlobalFieldsManager
    {
        [XmlArray("GlobalFieldBanks")]
        [XmlArrayItem("Bank", typeof(GlobalFieldsBank), IsNullable = false)]
        public List<GlobalFieldsBank> m_globalFieldBanks;

        [XmlIgnoreAttribute()]
        public GlobalFieldsBank m_defaultBank;

        [XmlIgnoreAttribute()]
        private static GlobalFieldsManager m_globFieldMgrInst = null;

        [XmlIgnoreAttribute()]
        private static FileSystemWatcher m_fileWatcher = null;

        public static void initialise()
        {
            m_globFieldMgrInst = new GlobalFieldsManager();
        }

        public GlobalFieldsManager()
        {
            m_globalFieldBanks = new List<GlobalFieldsBank>();
            m_defaultBank = new GlobalFieldsBank();
        }

        public static void setupGlobalFieldsWatcher(string fullPath)
        {
            if (m_fileWatcher == null)
            {
                m_fileWatcher = new FileSystemWatcher();
                Globals.setupFileWatcher(m_fileWatcher, fullPath);
            }
        }

        public static void readGlobalFields()
        {
            try
            {
                string globalFieldsFilePath = Application.StartupPath + "\\" + Globals.GLOBAL_FIELDS_FILE;

                if (File.Exists(globalFieldsFilePath))
                {
                    OutputWindow.appendOutput("Reading " + globalFieldsFilePath, OutputWindow.MessageType.IMPORTANT);

                    XmlSerializer globalFieldsDeserialiser = new XmlSerializer(typeof(GlobalFieldsManager));
                    using (TextReader readStream = new StreamReader(globalFieldsFilePath))
                    {
                        m_globFieldMgrInst = (GlobalFieldsManager)globalFieldsDeserialiser.Deserialize(readStream);

                        setupGlobalFieldsWatcher(globalFieldsFilePath);

                        readStream.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                OutputWindow.appendOutput(ex.Message, OutputWindow.MessageType.ERROR);
            }
        }

        public static void saveGlobalFields()
        {
            try
            {
                string globalFieldsFilePath = Application.StartupPath + "\\" + Globals.GLOBAL_FIELDS_FILE;

                OutputWindow.appendOutput("Saving " + globalFieldsFilePath, OutputWindow.MessageType.IMPORTANT);

                CommandPrompt.perforceCheckout(globalFieldsFilePath);

                if (m_fileWatcher != null)
                    m_fileWatcher.EnableRaisingEvents = false; // Don't raise events about ourselves

                XmlSerializer globalFieldsSerialiser = new XmlSerializer(typeof(GlobalFieldsManager));
                using (TextWriter writeStream = new StreamWriter(globalFieldsFilePath))
                {
                    globalFieldsSerialiser.Serialize(writeStream, GlobalFieldsManager.GetInstance);

                    writeStream.Close();
                }

                if (m_fileWatcher != null)
                    m_fileWatcher.EnableRaisingEvents = true;

                setupGlobalFieldsWatcher(globalFieldsFilePath);
            }
            catch (Exception ex)
            {
                OutputWindow.appendOutput(ex.Message, OutputWindow.MessageType.ERROR);
            }
        }

        public GlobalFieldsBank addOrFindBank(string bankName)
        {
            foreach (GlobalFieldsBank currBank in m_globalFieldBanks)
            {
                if (String.Compare(currBank.m_name, bankName, true) == 0)
                    return currBank;
            }

            GlobalFieldsBank newBank = new GlobalFieldsBank();

            addNewGlobalFieldBank(newBank);

            return newBank;
        }

        public void deleteGlobalFieldBank(string bankName)
        {
            foreach (GlobalFieldsBank currBank in m_globalFieldBanks)
            {
                if (String.Compare(currBank.m_name, bankName, true) == 0)
                {
                    m_globalFieldBanks.Remove(currBank);
                    break;
                }
            }
        }

        public GlobalFields getGlobalFieldsFromBank(string bankName)
        {
            foreach (GlobalFieldsBank currBank in m_globalFieldBanks)
            {
                if (String.Compare(currBank.m_name, bankName, true) == 0)
                    return currBank.m_fields;
            }

            return m_defaultBank.m_fields;
        }

        public void addNewGlobalFieldBank(GlobalFieldsBank newBank)
        {
            m_globalFieldBanks.Add(newBank);
        }

        public bool globalFieldBankExists(string bankName)
        {
            foreach (GlobalFieldsBank currBank in m_globalFieldBanks)
            {
                if (String.Compare(currBank.m_name, bankName, true) == 0)
                    return true;
            }

            return false;
        }

        public static GlobalFieldsManager GetInstance
        {
            get { return m_globFieldMgrInst; }
        }
    }

    #endregion
}
