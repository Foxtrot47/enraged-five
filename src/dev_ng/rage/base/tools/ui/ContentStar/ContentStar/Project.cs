﻿/*******************************************\
| ContentStar : Rockstar Games © 2013       |
| ------------------------------------------|
| Author: Thomas Woodhead (R* London)       |
| Email: thomas.woodhead@rockstarlondon.com |
\*******************************************/

#region Usings

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Collections;
using System.Xml;
using System.Xml.Linq;
using System.Diagnostics;
using System.Threading;
using System.Xml.Serialization;
using ContentStar;
using contentStar;
using RSG.Base.Configuration;
using RSG.Platform;

#endregion

namespace contentStar
{
    #region XboxData

    [Serializable]
    public class XboxData
    {
        [XmlElement("PackageName")]
        public string m_packageName;

        [XmlElement("TitleImage")]
        public string m_titleImage;

        [XmlElement("DashboardIcon")]
        public string m_dashboardIcon;

        [XmlElement("OfferBanner")]
        public string m_offerBanner;

        [XmlElement("PublisherFlags")]
        public string m_publisherFlags;

        [XmlElement("ActivationDate")]
        public string m_activationDate;

        [XmlElement("OfferingID")]
        public string m_offeringID;

        [XmlElement("PreBuildPath")]
        public string m_preBuildPath;

        [XmlElement("PostBuildPath")]
        public string m_postBuildPath;

        [XmlElement("HasCost")]
        public bool m_hasCost;

        [XmlElement("IsMarketplace")]
        public bool m_isMarketPlace;

        [XmlElement("AllowProfileTransfer")]
        public bool m_allowProfileTransfer;

        [XmlElement("AllowDeviceTransfer")]
        public bool m_allowDeviceTransfer;

        [XmlElement("LicenseMask")]
        public string m_licenseMask;

        [XmlElement("ContentCategory")]
        public string m_contentCategory;

        [XmlElement("Visibility")]
        public int m_visibility;

        [XmlElement("PreviewOfferID")]
        public string m_previewOfferID;

        [XmlElement("OfferID")]
        public string m_offerID;

        [XmlElement("LicenseLevel")]
        public int m_licenseLevel;

        [XmlElement("Consumable")]
        public bool m_consumable;

        public XboxData()
        {
            m_packageName = "";
            m_titleImage = "";
            m_dashboardIcon = "";
            m_offerBanner = "";
            m_publisherFlags = "FFFFFFFF";
            m_activationDate = "";
            m_offeringID = "FFFFFFF";
            m_preBuildPath = "";
            m_postBuildPath = "";
            m_hasCost = true;
            m_isMarketPlace = true;
            m_allowProfileTransfer = true;
            m_allowDeviceTransfer = true;
            m_licenseMask = "0x00000000";
            m_contentCategory = "0x00000001";
            m_visibility = 1;
            m_previewOfferID = "0x00000000";
            m_offerID = "0x00000000";
            m_licenseLevel = 1;
            m_consumable = false;
        }

        public void buildString(StringBuilder stringToBuild, GlobalFields gfBank)
        {
            stringToBuild.Append("Game Title: "); stringToBuild.AppendLine(gfBank.m_gameTitle);
            stringToBuild.Append("Title Id: "); stringToBuild.AppendLine(gfBank.m_titleID);
            stringToBuild.Append("Package Name: "); stringToBuild.AppendLine(m_packageName);
            stringToBuild.Append("Title Image: "); stringToBuild.AppendLine(m_titleImage);
            stringToBuild.Append("Dashboard Icon: "); stringToBuild.AppendLine(m_dashboardIcon);
            stringToBuild.Append("Offer Banner: "); stringToBuild.AppendLine(m_offerBanner);
            stringToBuild.Append("Publisher Flags: "); stringToBuild.AppendLine(m_publisherFlags);
            stringToBuild.Append("Activation Date: "); stringToBuild.AppendLine(m_activationDate);
            stringToBuild.Append("Offering Id: "); stringToBuild.AppendLine(m_offeringID);
            stringToBuild.Append("Has Cost: "); stringToBuild.AppendLine(m_hasCost.ToString());
            stringToBuild.Append("Is Marketplace: "); stringToBuild.AppendLine(m_isMarketPlace.ToString());
            stringToBuild.Append("Allow Profile Transfer: "); stringToBuild.AppendLine(m_allowProfileTransfer.ToString());
            stringToBuild.Append("Allow Device Transfer: "); stringToBuild.AppendLine(m_allowDeviceTransfer.ToString());
            stringToBuild.Append("License Mask: "); stringToBuild.AppendLine(m_licenseMask);
            stringToBuild.Append("Content Category: "); stringToBuild.AppendLine(m_contentCategory);
            stringToBuild.Append("Visibility: "); stringToBuild.AppendLine(m_visibility.ToString());
            stringToBuild.Append("Preview Offer Id: "); stringToBuild.AppendLine(m_previewOfferID);
            stringToBuild.Append("Offer Id: "); stringToBuild.AppendLine(m_offerID);
            stringToBuild.Append("License Level: "); stringToBuild.AppendLine(m_licenseLevel.ToString());
            stringToBuild.Append("Consumable: "); stringToBuild.AppendLine(m_consumable.ToString());
        }
    }

    #endregion

    #region Ps3Data

    [Serializable]
    public class Ps3Data
    {
        [XmlElement("UScontentIDThird")]
        public string m_UScontentIDThird;

        [XmlElement("USparamSFO")]
        public string m_USparamSFO;

        [XmlElement("EUcontentIDThird")]
        public string m_EUcontentIDThird;

        [XmlElement("EUparamSFO")]
        public string m_EUparamSFO;

        [XmlElement("JPNcontentIDThird")]
        public string m_JPNcontentIDThird;

        [XmlElement("JPNparamSFO")]
        public string m_JPNparamSFO;

        [XmlElement("ContentType")]
        public string m_contentType;

        [XmlElement("Icon0")]
        public string m_icon0;

        [XmlElement("DrmType")]
        public string m_drmType;

        [XmlElement("VersionEU")]
        public string m_versionEU;

        [XmlElement("VersionUS")]
        public string m_versionUS;

        [XmlElement("VersionJN")]
        public string m_versionJN;

        [XmlElement("PreBuildPath")]
        public string m_preBuildPath;

        [XmlElement("PostBuildPath")]
        public string m_postBuildPath;

        public Ps3Data()
        {
            m_UScontentIDThird = "";
            m_USparamSFO = "";
            m_EUcontentIDThird = "";
            m_EUparamSFO = "";
            m_JPNcontentIDThird = "";
            m_JPNparamSFO = "";
            m_contentType = "GameData";
            m_icon0 = "";
            m_drmType = "Local";
            m_versionEU = "";
            m_versionUS = "";
            m_versionJN = "";
            m_preBuildPath = "";
            m_postBuildPath = "";
        }

        public void buildString(StringBuilder stringToBuild, GlobalFields gfBank)
        {
            stringToBuild.Append("First US Content Id: "); stringToBuild.AppendLine(gfBank.m_UScontentIDFirst);
            stringToBuild.Append("Second US Content Id: "); stringToBuild.AppendLine(gfBank.m_UScontentIDSecond);
            stringToBuild.Append("Third US Content Id: "); stringToBuild.AppendLine(m_UScontentIDThird);
            stringToBuild.Append("US Param SFO: "); stringToBuild.AppendLine(m_USparamSFO);
            stringToBuild.Append("US Version: "); stringToBuild.AppendLine(m_versionUS);

            stringToBuild.Append("First EU Content Id: "); stringToBuild.AppendLine(gfBank.m_EUcontentIDFirst);
            stringToBuild.Append("Second EU Content Id: "); stringToBuild.AppendLine(gfBank.m_EUcontentIDSecond);
            stringToBuild.Append("Third EU Content Id: "); stringToBuild.AppendLine(m_EUcontentIDThird);
            stringToBuild.Append("EU Param SFO: "); stringToBuild.AppendLine(m_EUparamSFO);
            stringToBuild.Append("EU Version: "); stringToBuild.AppendLine(m_versionEU);

            stringToBuild.Append("First JPN Content Id: "); stringToBuild.AppendLine(gfBank.m_JPNcontentIDFirst);
            stringToBuild.Append("Second JPN Content Id: "); stringToBuild.AppendLine(gfBank.m_JPNcontentIDSecond);
            stringToBuild.Append("Third JPN Content Id: "); stringToBuild.AppendLine(m_JPNcontentIDThird);
            stringToBuild.Append("JPN Param SFO: "); stringToBuild.AppendLine(m_JPNparamSFO);
            stringToBuild.Append("JPN Version: "); stringToBuild.AppendLine(m_versionJN);

            stringToBuild.Append("kLicensee: "); stringToBuild.AppendLine(gfBank.m_kLicensee);            
            stringToBuild.Append("ContentType: "); stringToBuild.AppendLine(m_contentType);
            stringToBuild.Append("Icon0: "); stringToBuild.AppendLine(m_icon0);
            stringToBuild.Append("DRM: "); stringToBuild.AppendLine(m_drmType);            
        }
    }

    #endregion

    #region Ps4Data

    [Serializable]
    public class Ps4Data
    {
        [XmlElement("RpfDivisionThreshold")]
        public long m_rpfDivisionThreshold = Globals.MAX_SIZE_PER_RPF;

        [XmlElement("UScontentIDThird")]
        public string m_UScontentIDThird;

        [XmlElement("USparamSFO")]
        public string m_USparamSFO;

        [XmlElement("EUcontentIDThird")]
        public string m_EUcontentIDThird;

        [XmlElement("EUparamSFO")]
        public string m_EUparamSFO;

        [XmlElement("JPNcontentIDThird")]
        public string m_JPNcontentIDThird;

        [XmlElement("JPNparamSFO")]
        public string m_JPNparamSFO;

        [XmlElement("Icon0")]
        public string m_icon0;

        [XmlElement("Passcode")]
        public string m_passCode;

        [XmlElement("VolumeType")]
        public string m_volumeType;

        [XmlElement("VersionEU")]
        public string m_versionEU;

        [XmlElement("VersionUS")]
        public string m_versionUS;

        [XmlElement("VersionJN")]
        public string m_versionJN;

        [XmlElement("PreBuildPath")]
        public string m_preBuildPath;

        [XmlElement("PostBuildPath")]
        public string m_postBuildPath;

        public Ps4Data()
        {
            m_UScontentIDThird = "";
            m_USparamSFO = "";
            m_EUcontentIDThird = "";
            m_EUparamSFO = "";
            m_JPNcontentIDThird = "";
            m_JPNparamSFO = "";
            m_icon0 = "";
            m_versionEU = "";
            m_versionUS = "";
            m_versionJN = "";
            m_preBuildPath = "";
            m_postBuildPath = "";
            m_passCode = "";
            m_volumeType = "";
        }

        public void buildString(StringBuilder stringToBuild, GlobalFields gfBank)
        {
            stringToBuild.Append("First US Content Id: "); stringToBuild.AppendLine(gfBank.m_PS4UScontentIDFirst);
            stringToBuild.Append("Second US Content Id: "); stringToBuild.AppendLine(gfBank.m_PS4UScontentIDSecond);
            stringToBuild.Append("Third US Content Id: "); stringToBuild.AppendLine(m_UScontentIDThird);
            stringToBuild.Append("US Param SFO: "); stringToBuild.AppendLine(m_USparamSFO);
            stringToBuild.Append("US Version: "); stringToBuild.AppendLine(m_versionUS);

            stringToBuild.Append("First EU Content Id: "); stringToBuild.AppendLine(gfBank.m_PS4EUcontentIDFirst);
            stringToBuild.Append("Second EU Content Id: "); stringToBuild.AppendLine(gfBank.m_PS4EUcontentIDSecond);
            stringToBuild.Append("Third EU Content Id: "); stringToBuild.AppendLine(m_EUcontentIDThird);
            stringToBuild.Append("EU Param SFO: "); stringToBuild.AppendLine(m_EUparamSFO);
            stringToBuild.Append("EU Version: "); stringToBuild.AppendLine(m_versionEU);

            stringToBuild.Append("First JPN Content Id: "); stringToBuild.AppendLine(gfBank.m_PS4JPNcontentIDFirst);
            stringToBuild.Append("Second JPN Content Id: "); stringToBuild.AppendLine(gfBank.m_PS4JPNcontentIDSecond);            
            stringToBuild.Append("Third JPN Content Id: "); stringToBuild.AppendLine(m_JPNcontentIDThird);
            stringToBuild.Append("JPN Param SFO: "); stringToBuild.AppendLine(m_JPNparamSFO);
            stringToBuild.Append("JPN Version: "); stringToBuild.AppendLine(m_versionJN);

            stringToBuild.Append("Icon0: "); stringToBuild.AppendLine(m_icon0);            
            stringToBuild.Append("Passcode: "); stringToBuild.AppendLine(m_passCode);
            stringToBuild.Append("Volume Type: "); stringToBuild.AppendLine(m_volumeType);
            stringToBuild.Append("Entitlement: "); stringToBuild.AppendLine(gfBank.m_PS4Entitlement);
        }
    }

    #endregion

    #region PCData

    [Serializable]
    public class PCData
    {
        [XmlElement("RpfDivisionThreshold")]
        public long m_rpfDivisionThreshold = Globals.MAX_SIZE_PER_RPF;
        [XmlElement("PreBuildPath")]
        public string m_preBuildPath;

        [XmlElement("PostBuildPath")]
        public string m_postBuildPath;

        public PCData()
        {
            m_preBuildPath = "";
            m_postBuildPath = "";
        }
    }

    #endregion

	#region XboxOneData
	#region XboxOneAppManifest

	#region Identity
	[Serializable]
	public class CIdentity
	{
		[XmlAttribute("Name")]
		public string Name { get; set; }
		[XmlAttribute("Version")]
		public string Version { get; set; }
		[XmlAttribute("Publisher")]
		public string Publisher;
		[XmlIgnoreAttribute]
		public string CompanyName { get; set; }
		[XmlIgnoreAttribute]
		public string Organisation { get; set; }
		[XmlIgnoreAttribute]
		public string Location { get; set; }
		[XmlIgnoreAttribute]
		public string State { get; set; }
		[XmlIgnoreAttribute]
		public string Country { get; set; }
		private void BuildPublisherString()
		{
			Publisher = "CN=" + CompanyName + ", " +
				"O=" + Organisation + ", " +
				"L=" + Location + ", " +
				"S=" + State + ", " +
				"C=" + Country;
		}
		public CIdentity()
		{
			Name = "GTAV.DLC.";
			CompanyName = "Rockstar Games";
			Organisation = "Rockstar Games";
			Location = "NYC";
			State = "NY";
			Country = "US";
			Version = "1.0.0.0";
			BuildPublisherString();
		}
	}
	#endregion

	#region Properties
	[Serializable]
	public class CProperties
	{
		[XmlElement("DisplayName")]
		public string DisplayName { get; set; }
		[XmlElement("PublisherDisplayName")]
		public string PublisherDisplayName { get; set; }
		[XmlElement("Logo")]
		public string Logo { get; set; }
		[XmlElement("Description")]
		public string Description { get; set; }
		[XmlElement("ContentPackage", Namespace = "http://schemas.microsoft.com/appx/2013/xbox/manifest")]
		public bool ContentPackage { get; set; }
		public CProperties()
		{
			DisplayName = "DLC";
			PublisherDisplayName = "Rockstar Games";
			Logo = "";
			Description = "pack";
			ContentPackage = true;
		}
	}
	#endregion

	#region Prerequisites
	[Serializable]
	public class CPrerequisites
	{
		[XmlElement("OSMinVersion")]
		public string OSMinVersion { get; set; }
		[XmlElement("OSMaxVersionTested")]
		public string OSMaxVersionTested { get; set; }
		[XmlElement("ApplicationEnvironment", Namespace = "http://schemas.microsoft.com/appx/2013/xbox/manifest")]
		public string ApplicationEnvironment { get; set; }
		[XmlElement("OSName", Namespace = "http://schemas.microsoft.com/appx/2013/xbox/manifest")]
		public string OSName { get; set; }
		public CPrerequisites()
		{
			OSMinVersion = "6.2";
			OSMaxVersionTested = "6.2";
			ApplicationEnvironment = "title";
			OSName = "era";
		}
	}
	#endregion

	#region Extensions
	[Serializable]
	public class CContentPackageVisualElements
	{
		static string ConvertColor(Color col)
		{
			return String.Format("#{0:X2}{1:X2}{2:X2}", col.R, col.G, col.B);
		}
		[XmlAttribute("DisplayName")]
		public string DisplayName { get; set; }
		[XmlAttribute("Logo")]
		public string Logo { get; set; }
		[XmlAttribute("SmallLogo")]
		public string SmallLogo { get; set; }
		[XmlAttribute("WideLogo")]
		public string WideLogo { get; set; }
		[XmlAttribute("Description")]
		public string Description { get; set; }
		[XmlAttribute("ForegroundText")]
		public string ForegroundText { get; set; }

		private Color _BackgroundColor;
		[XmlAttribute("BackgroundColor")]
		public string BackgroundColor
		{
			get { return ConvertColor(_BackgroundColor); }
			set { _BackgroundColor = ColorTranslator.FromHtml(value); }
		}

		public CContentPackageVisualElements()
		{
			DisplayName = "Pack Display Name";
			Logo = "Logo path";
			SmallLogo = "Small logo path";
			WideLogo = "Wide logo path";
			Description = "Pack description";
			ForegroundText = "dark";
			BackgroundColor = ConvertColor(Color.Crimson);
		}
	}

	[Serializable]
	public class CExtensions
	{
		[Serializable]
		public class CPackageExtension
		{
			[XmlAttribute("Category")]
			public string Category { get; set; }
			[Serializable]
			public class CContentPackage
			{
				[Serializable]
				public class CAllowedProduct
				{
					[XmlAttribute("Id")]
					public string Id { get; set; }
				}
				[XmlElement("AllowedProduct", Namespace = "http://schemas.microsoft.com/appx/2013/xbox/manifest")]
				public CAllowedProduct AllowedProductId { get; set; }
				[XmlElement("ContentPackageVisualElements", Namespace = "http://schemas.microsoft.com/appx/2013/xbox/manifest")]
				public CContentPackageVisualElements ContentPackageVisualElements { get; set; }
				public CContentPackage()
				{
					AllowedProductId = new CAllowedProduct();
					AllowedProductId.Id = "00000000-0000-0000-0000-000000000000";
					ContentPackageVisualElements = new CContentPackageVisualElements();
				}
			}
			public CContentPackage ContentPackage { get; set; }
			public CPackageExtension()
			{
				Category = "xbox.contentpackage";
				ContentPackage = new CContentPackage();
			}
		}
		[XmlElement("PackageExtension", Namespace = "http://schemas.microsoft.com/appx/2013/xbox/manifest")]
		public CPackageExtension PackageExtension { get; set; }
		public CExtensions()
		{
			PackageExtension = new CPackageExtension();
		}
	}
	#endregion

	#region ResourceList
	[Serializable]
	public class ResourceListItem
	{
		[XmlAttribute("Language")]
		public string Resource;
		public ResourceListItem()
		{
			Resource = "";
		}
		public ResourceListItem(string item)
		{
			Resource = item;
		}
	}
	[XmlType("Resources")]
	[Serializable]
	public class CResourceList
	{
		[XmlElement("Resource")]
		public List<ResourceListItem> items { get; set; }
		public CResourceList()
		{
			items = new List<ResourceListItem>();
		}
		public void Add(string resource)
		{
			items.Add(new ResourceListItem(resource));
		}
	}
	#endregion

	[Serializable]
	[XmlRoot("Package")]//,  Namespace = "http://schemas.microsoft.com/appx/2010/manifest")]
	public class XboxOneAppManifest
	{
		[XmlAttribute]
		public string IgnorableNamespaces { get; set; }

		#region Class Properties
		[XmlElement("Identity")]
		public CIdentity Identity { get; set; }
		[XmlElement("Properties")]
		public CProperties Properties { get; set; }
		[XmlElement("Prerequisites")]
		public CPrerequisites Prerequisites { get; set; }
		[XmlElement("Resources")]
		public CResourceList ResourceList { get; set; }
		[XmlElement("Extensions")]
		public CExtensions Extensions { get; set; }
		#endregion

		#region ctor
		public XboxOneAppManifest()
		{
			Identity = new CIdentity();
			Properties = new CProperties();
			Prerequisites = new CPrerequisites();
			ResourceList = new CResourceList();
			Extensions = new CExtensions();
			ResourceList.Add("en-US");
			IgnorableNamespaces = "mx";
		}
		#endregion

		#region Methods
		private void ChangeLogoPath(string path)
		{

		}
		public void WriteTo(string path)
		{
			XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
			XmlWriterSettings settings = new XmlWriterSettings();
			ns.Add("mx", "http://schemas.microsoft.com/appx/2013/xbox/manifest");
			settings.Indent = true;
			settings.NewLineOnAttributes = true;
			XmlSerializer test = new XmlSerializer(typeof(XboxOneAppManifest), "http://schemas.microsoft.com/appx/2010/manifest");
			CContentPackageVisualElements vElems = Extensions.PackageExtension.ContentPackage.ContentPackageVisualElements;
			string oldStoreLogo = Properties.Logo;
			string oldLiveLogo = vElems.Logo;
			string oldSmallLogo = vElems.SmallLogo;
			string oldWideLogo = vElems.WideLogo;
			string logoPath = @"signage\";
			Properties.Logo = logoPath + new FileInfo(oldStoreLogo).Name;
			vElems.Logo = logoPath + new FileInfo(oldLiveLogo).Name;
			vElems.SmallLogo = logoPath + new FileInfo(oldSmallLogo).Name;
			vElems.WideLogo = logoPath + new FileInfo(oldWideLogo).Name;
			using (XmlWriter writestream = XmlWriter.Create(path, settings))
			{
				test.Serialize(writestream, this, ns);
			}
			Properties.Logo = oldStoreLogo;
			vElems.Logo  = oldLiveLogo;
			vElems.SmallLogo = oldSmallLogo;
			vElems.WideLogo = oldWideLogo;
		}
		#endregion
	}
	#endregion

	#region XboxOneChunkLayout
	[Serializable]
	public class CFileGroup
	{
		[XmlAttribute("DestinationPath")]
		public string DestinationPath { get; set; }
		[XmlAttribute("SourcePath")]
		public string SourcePath { get; set; }
		[XmlAttribute("Include")]
		public string Include { get; set; }
		public CFileGroup()
		{
			DestinationPath = "asd";
			SourcePath = "asd";
		}
	}
	[XmlType("Chunk")]
	[Serializable]
	public class Chunk
	{
		[XmlAttribute("Id")]
		public string Id { get; set; }
		[XmlAttribute("Marker")]
		public string Marker { get; set; }
		[XmlElement("FileGroup")]
		public List<CFileGroup> FileGroup { get; set; }

		public Chunk()
		{
			FileGroup = new List<CFileGroup>();
		}
		public Chunk(string _id, string _marker = null)
		{
			Id = _id;
			Marker = _marker;
			FileGroup = new List<CFileGroup>();
		}
		public void AddFile(string sourcePath, string destPath, string include)
		{
			CFileGroup item = new CFileGroup();
			item.DestinationPath = destPath;
			item.SourcePath = sourcePath;
			item.Include = include;
			FileGroup.Add(item);
		}
	}
	[Serializable]
	[XmlRoot("Package")]
	public class XboxOneChunkLayout : List<Chunk>
	{
		public XboxOneChunkLayout()
		{
			//Add(new Chunk("Registration", null));
			//Add(new Chunk("1000", "Launch"));
		}
		public void WriteTo(string path)
		{

			XmlSerializer test = new XmlSerializer(typeof(XboxOneChunkLayout));
			XmlSerializerNamespaces ns1 = new XmlSerializerNamespaces();
			ns1.Add("", "");
			XmlWriterSettings settings = new XmlWriterSettings();
			settings.NewLineOnAttributes = false;
			settings.Indent = true;
			using (XmlWriter writestream = XmlWriter.Create(path, settings))
			{
				test.Serialize(writestream, this, ns1);
			}
		}
		public bool CheckRegistrationChunk()
		{
			foreach (Chunk i in this)
			{
				if (i.Id == "Registration")
				{
					return true;
				}
			}
			return false;
		}
		public bool CheckLaunchChunk()
		{
			foreach (Chunk i in this)
			{
				if (i.Marker == "Launch")
				{
					return true;
				}
			}
			return false;
		}
	}
	#endregion
	[Serializable]
	public class CXboxOneData
    {
        [XmlElement("RpfDivisionThreshold")]
        public long m_rpfDivisionThreshold = Globals.MAX_SIZE_PER_RPF;
        [XmlElement("AllowedProductId")]
		public string AllowedProductID { get; set; }
		[XmlElement("AllowedProductIdJPN")]
		public string AllowedProductIdJPN { get; set; }
		[XmlElement("AppManifest")]
		public XboxOneAppManifest AppManifest { get; set; }
		[XmlElement("ChunkLayout")]
		public XboxOneChunkLayout ChunkLayout { get; set; }
		[XmlElement("LastGeneratedProjectFile")]
		public string IntermediatePackageFile { get; set; }
		[XmlElement("IntermediateDirectory")]
		public string IntermediateDirectory { get; set; }
		[XmlElement("IntermediateLogoDirectory")]
		public string IntermediateLogoDirectory { get; set; }
		[XmlElement("BuildDebug")]
		public bool BuildDebug {get;set;}
		public void WriteAppManifest(string path) { AppManifest.WriteTo(path); }
		public void WriteChunkLayout(string path) { ChunkLayout.WriteTo(path); }
		public CXboxOneData()
		{
			BuildDebug = true;
			AllowedProductID = Globals.XB1_TEST_PRODUCT_ID;
			AllowedProductIdJPN = Globals.XB1_TEST_PRODUCT_ID;
			AppManifest = new XboxOneAppManifest();
			ChunkLayout = new XboxOneChunkLayout();
		}

        public void buildString(StringBuilder stringToBuild, GlobalFields gfBank)
        {
            stringToBuild.Append("Build Debug: "); stringToBuild.AppendLine(BuildDebug.ToString());
            stringToBuild.Append("Allowed Product Id: "); stringToBuild.AppendLine(AllowedProductID);
            stringToBuild.Append("Company Name: "); stringToBuild.AppendLine(AppManifest.Identity.CompanyName);
            stringToBuild.Append("Country: "); stringToBuild.AppendLine(AppManifest.Identity.Country);
            stringToBuild.Append("Location: "); stringToBuild.AppendLine(AppManifest.Identity.Location);
            stringToBuild.Append("Name: "); stringToBuild.AppendLine(AppManifest.Identity.Name);
            stringToBuild.Append("Organisation: "); stringToBuild.AppendLine(AppManifest.Identity.Organisation);
            stringToBuild.Append("State: "); stringToBuild.AppendLine(AppManifest.Identity.State);
            stringToBuild.Append("Version: "); stringToBuild.AppendLine(AppManifest.Identity.Version);
            stringToBuild.Append("Publisher: "); stringToBuild.AppendLine(AppManifest.Identity.Publisher);
        }
	}
	#endregion

    #region Project

    [Serializable]
    [XmlRoot("Project")]
    public class Project
    {
        [XmlIgnoreAttribute()]
        public string m_title;

        [XmlIgnoreAttribute()]
        public string m_fullPath;

        [XmlElement("PreBuildPath")]
        public string m_preBuildPath;

        [XmlElement("PostBuildPath")]
        public string m_postBuildPath;

        [XmlIgnoreAttribute()]
        public bool m_isInBuild;

        [XmlElement("PS3")]
        public Ps3Data m_ps3Data;

        [XmlElement("PS4")]
        public Ps4Data m_ps4Data;

        [XmlElement("XBOX")]
        public XboxData m_xboxData;

        [XmlElement("PC")]
        public PCData m_pcData;

		[XmlElement("XBOXONE")]
		public CXboxOneData m_xboneData;

        [XmlElement("GlobalBank")]
        public string m_globalBankName;

        [XmlIgnoreAttribute()]
        public contentTreeNode m_rootNode;

        [XmlIgnoreAttribute()]
        public StreamWriter m_logStream;

        [XmlIgnoreAttribute()]
        public FileSystemWatcher m_projectWatcher;

        [XmlIgnoreAttribute()]
        public int m_channelId;

        [XmlIgnoreAttribute()]
        public IProject m_toolsProject;

        [XmlIgnoreAttribute()]
        public bool m_compatPack;

        [XmlIgnoreAttribute()]
        public List<string> m_setupsToMerge;

        [XmlIgnoreAttribute()]
        public List<string> m_contentsToMerge;

        [XmlIgnoreAttribute()]
        public List<Project> m_dependencies;

        [XmlIgnoreAttribute()]
        public Project m_dependentProj;

        [XmlIgnoreAttribute()]
        public bool m_nodesInitialised;

        [XmlIgnoreAttribute()]
        public FileFilters m_fileFilters;

        [XmlIgnoreAttribute()]
        public FileExclusions m_fileExclusions;

        [XmlIgnoreAttribute()]
        public int m_subPackCount;

        public Project()
        {
            m_title = "";
            m_fullPath = "";
            m_isInBuild = true;
            m_ps3Data = new Ps3Data();
            m_ps4Data = new Ps4Data();
            m_xboxData = new XboxData();
            m_pcData = new PCData();
			m_xboneData = new CXboxOneData();
            m_rootNode = null;
            m_preBuildPath = "";
            m_postBuildPath = "";
            m_logStream = null;
            m_projectWatcher = null;
            m_globalBankName = "";
            m_channelId = -1;
            m_toolsProject = null;
            m_compatPack = false;
            m_setupsToMerge = new List<string>();
            m_contentsToMerge = new List<string>();
            m_dependencies = new List<Project>();
            m_dependentProj = null;
            m_nodesInitialised = false;
            m_fileFilters = null;
            m_fileExclusions = null;
            m_subPackCount = 0;
        }

        public void openLogStream(string path)
        {
            m_logStream = new StreamWriter(path);
        }

        public void closeLogStream()
        {
            m_logStream.Close();
            m_logStream = null;
        }

        public void addDependency(Project depProject)
        {
            m_dependencies.Add(depProject);
        }

        public void setDependent(Project dependentProj)
        {
            if (m_dependentProj == null)
                m_dependentProj = dependentProj;
            else
                OutputWindow.appendOutput("Project already has a dependent! " + m_dependentProj.m_title + " vs " + dependentProj.m_title, OutputWindow.MessageType.ERROR);
        }

        public void preSerialise()
        {
            m_projectWatcher.EnableRaisingEvents = false;

            if (m_globalBankName.Length > 0 && !GlobalFieldsManager.GetInstance.globalFieldBankExists(m_globalBankName))
            {
                OutputWindow.appendOutput("Project has orphaned global bank! Removing " + m_globalBankName,OutputWindow.MessageType.ERROR);
                m_globalBankName = "";
            }
        }

        public void postSerialise()
        {
            m_projectWatcher.EnableRaisingEvents = true;
        }

        public void setIsInBuild(bool value)
        {
            setIsInBuildValue(value);

            if (m_dependentProj == null)
            {
                if (value)
                {
                    foreach (Project decipherProject in m_dependencies)
                        decipherProject.setIsInBuildValue(value);
                }
            }
            else
            {
                if (!value)
                    m_dependentProj.setIsInBuildValue(value);
            }
        }
        
        private void setIsInBuildValue(bool value)
        {
            if (m_isInBuild != value)
            {
                m_isInBuild = value;
                Program.decipherNodeImage(m_rootNode);
            }
        }

        public bool getIsInBuild()
        {
            return m_isInBuild;
        }

        public string getXboxContentID(Platform target)
        {
            if (target == Platform.Xbox360)
                return GlobalFieldsManager.GetInstance.getGlobalFieldsFromBank(m_globalBankName).m_titleID + "0" + m_xboxData.m_offeringID;
            else
                return string.Empty;
        }

        public IBranch getBranchForPlatform(Platform targetPlatform)
        {
            List<IBranch> validBranches = getValidBranches();

            foreach (IBranch currBranch in validBranches)
            {
                if (currBranch.Targets.ContainsKey(targetPlatform))
                    return currBranch;
            }

            return null;
        }

        public contentTreeNode getBranchRootNode(IBranch targetBranch)
        {
            List<IBranch> validBranches = getValidBranches();

            foreach (IBranch currBranch in validBranches)
            {
                if (currBranch == targetBranch)
                {
                    foreach (contentTreeNode branchNode in m_rootNode.Nodes)
                    {
                        if (String.Compare(currBranch.Build, branchNode.FullFilePath) == 0)
                            return branchNode;
                    }
                }
            }

            return null;
        }

        public contentTreeNode getBranchRootNode(Platform targetPlatform)
        {
            List<IBranch> validBranches = getValidBranches();
            foreach (IBranch currBranch in validBranches)
            {
                if (currBranch.Targets.ContainsKey(targetPlatform))
                {
                    foreach (contentTreeNode branchNode in m_rootNode.Nodes)
                    {
						//HACK
						if (targetPlatform == Platform.Win64)
						{
							if (!currBranch.Build.Contains("\\build\\dev_ng"))
							{
								continue;	
							}
						}
						//ENDHACK
                        if (String.Compare(currBranch.Build, branchNode.FullFilePath) == 0)
                            return branchNode;
                    }
                }
            }

            return null;
        }

        public List<IBranch> getValidBranches()
        {
            List<IBranch> validBranches = new List<IBranch>();

            if (m_toolsProject != null)
            {
                foreach (KeyValuePair<string, IBranch> currBranch in m_toolsProject.Branches)
                {
                    if (currBranch.Value.Build.Contains("\\build\\dev") || currBranch.Value.Build.Contains("\\build\\dev_ng"))
                    {
                        validBranches.Add(currBranch.Value);
                    }
                }
            }

            return validBranches;
        }

        public string getUSContentID(Platform target)
        {
            GlobalFieldsManager globalFields = GlobalFieldsManager.GetInstance;

            if (target == Platform.PS3)
            {
                return globalFields.getGlobalFieldsFromBank(m_globalBankName).m_UScontentIDFirst + "-" +
                    globalFields.getGlobalFieldsFromBank(m_globalBankName).m_UScontentIDSecond + "_00-" + m_ps3Data.m_UScontentIDThird;
            }
            else if (target == Platform.PS4)
            {
                return globalFields.getGlobalFieldsFromBank(m_globalBankName).m_PS4UScontentIDFirst + "-" +
                    globalFields.getGlobalFieldsFromBank(m_globalBankName).m_PS4UScontentIDSecond + "_00-" + m_ps4Data.m_UScontentIDThird;
            }

            return string.Empty;
        }

        public long getRpfDivisionThreshold(Platform target)
        {
            switch (target)
            {
                case Platform.Win32:
                case Platform.Win64:
                    return m_pcData.m_rpfDivisionThreshold;
                case Platform.PS4:
                    return m_ps4Data.m_rpfDivisionThreshold;
                case Platform.XboxOne:
                    return m_xboneData.m_rpfDivisionThreshold;
                default:
                    return Globals.MAX_SIZE_PER_RPF;
            }
        }

        public string getEUContentID(Platform target)
        {
            GlobalFieldsManager globalFields = GlobalFieldsManager.GetInstance;

            if (target == Platform.PS3)
            {
                return globalFields.getGlobalFieldsFromBank(m_globalBankName).m_EUcontentIDFirst + "-" +
                globalFields.getGlobalFieldsFromBank(m_globalBankName).m_EUcontentIDSecond + "_00-" + m_ps3Data.m_EUcontentIDThird;
            }
            else if (target == Platform.PS4)
            {
                return globalFields.getGlobalFieldsFromBank(m_globalBankName).m_PS4EUcontentIDFirst + "-" +
                    globalFields.getGlobalFieldsFromBank(m_globalBankName).m_PS4EUcontentIDSecond + "_00-" + m_ps4Data.m_EUcontentIDThird;
            }

            return string.Empty;
        }

        public string getJPNContentID(Platform target)
        {
            GlobalFieldsManager globalFields = GlobalFieldsManager.GetInstance;

            if (target == Platform.PS3)
            {
                return globalFields.getGlobalFieldsFromBank(m_globalBankName).m_JPNcontentIDFirst + "-" +
                globalFields.getGlobalFieldsFromBank(m_globalBankName).m_JPNcontentIDSecond + "_00-" + m_ps3Data.m_JPNcontentIDThird;
            }
            else if (target == Platform.PS4)
            {
                return globalFields.getGlobalFieldsFromBank(m_globalBankName).m_PS4JPNcontentIDFirst + "-" +
                    globalFields.getGlobalFieldsFromBank(m_globalBankName).m_PS4JPNcontentIDSecond + "_00-" + m_ps4Data.m_JPNcontentIDThird;
            }

            return string.Empty;
        }

        public bool getUSBuildValid(Platform target)
        {
            if (target == Platform.PS3 || target == Platform.PS4)
                return getUSContentID(target).Length == Globals.PS3_CID_FMT_LENGTH;

            return false;
        }

        public bool getEUBuildValid(Platform target)
        {
            if (target == Platform.PS3 || target == Platform.PS4)
                return getEUContentID(target).Length == Globals.PS3_CID_FMT_LENGTH;

            return false;
        }

        public bool getJPNBuildValid(Platform target)
        {
            if (target == Platform.PS3 || target == Platform.PS4)
                return getJPNContentID(target).Length == Globals.PS3_CID_FMT_LENGTH;

            return false;
        }

        [XmlIgnoreAttribute()]
        public string OutputDirectory
        {
            get { return Globals.OUTPUT_DIRECTORY + "\\" + m_title; }
        }

		public void cullExcludedNodes(ref List<contentTreeNode> nodes, Platform currentPlatform)
		{
			Globals.GLOBAL_FILE_EXCLUSIONS.CullExcludedNodes(ref nodes,currentPlatform);
			m_fileExclusions.CullExcludedNodes(ref nodes, currentPlatform);
		}

        public bool isExcluded(string filePath, BuildManager.BuildType buildType)
        {
            return (Globals.GLOBAL_FILE_EXCLUSIONS.IsExcluded(filePath, buildType) || m_fileExclusions.IsExcluded(filePath, buildType));
        }

        public bool isFullyExcluded(string filePath, BuildManager.BuildType buildType)
        {
            return (Globals.GLOBAL_FILE_EXCLUSIONS.IsFullyExcluded(filePath, buildType) || m_fileExclusions.IsFullyExcluded(filePath, buildType));
        }
    }
    #endregion
}