﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;

namespace ContentStar
{
    public partial class frmProgress : Form
    {
        private string m_Label;
        private bool m_Cancel;
        private int m_Progress;
        private int m_Maximum;

        public frmProgress(Form parent, int total = 0, string label = "")
        {
            InitializeComponent();

            m_Label = label;
            m_Cancel = false;
            m_Progress = 0;
            m_Maximum = total;

            progressBar1.Value = 0;
            progressBar1.Maximum = total;
            label1.Text = label;
            Icon = parent.Icon;

            this.Update();
            this.Hide();
        }

        public void UseTimer(bool useit)
        {
            timer1.Enabled = useit;
        }

        public void IncrementProgress(int amount = 1)
        {
            m_Progress += amount;

            if (!timer1.Enabled)
                timer1_Tick(0, null);
        }

        public void IncrementMaximum(int amount = 1)
        {
            m_Maximum += amount;
        }

        public int GetProgress()
        {
            return m_Progress;
        }

        public int GetMaximum()
        {
            return m_Maximum;
        }

        public bool GetCancel()
        {
            return m_Cancel;
        }

        public void ResetProgress(int total = 0, string label = "", bool useTimer = true)
        {
            m_Label = label;
            m_Cancel = false;
            m_Progress = 0;
            m_Maximum = total;

            progressBar1.Value = 0;
            progressBar1.Maximum = total;
            label1.Text = label;
            timer1.Enabled = useTimer;

            this.Update();
            this.Hide();
        }

        public void SetLabel(string label)
        {
            m_Label = label;
        }

        public void ShowProgress()
        {
            this.Refresh();
            this.Show();
            this.BringToFront();
        }

        // Necessary for making the progress bar work as it should...
        private void UpdateProgressBarProperly(ProgressBar pb, int value)
        {
            if (value == pb.Maximum)
            {
                pb.Value = value;
                pb.Value = value - 1;
            }
            else
            {
                pb.Value = value + 1;
            }
            pb.Value = value;
        }

        private void progressBar1_Click(object sender, EventArgs e)
        {
            if (m_Label.Length < 45)
                m_Label += (".");
            else
                m_Label = "Caution: Too many dots";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            m_Cancel = true;
            m_Label = "Canceling";
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            progressBar1.Maximum = m_Maximum;

            if (m_Maximum > 0)
            {
                UpdateProgressBarProperly(progressBar1, Math.Min(m_Progress, m_Maximum));
            }

            float percentage = (float)m_Progress / (float)m_Maximum * 100.0f;
            label1.Text = m_Label + "... " + percentage.ToString("#.") + "%";

            if (m_Progress >= m_Maximum)
            {
                this.Hide();
            }

            this.Update();
        }
    }
}
