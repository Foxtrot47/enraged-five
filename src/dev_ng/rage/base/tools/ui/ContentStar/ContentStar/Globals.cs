﻿/*******************************************\
| ContentStar : Rockstar Games © 2013       |
| ------------------------------------------|
| Author: Thomas Woodhead (R* London)       |
| Email: thomas.woodhead@rockstarlondon.com |
\*******************************************/

#region Usings

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Collections;
using System.Xml;
using System.Xml.Linq;
using System.Diagnostics;
using System.Xml.Serialization;
using ContentStar;
using System.Threading;
using contentStar;
using RSG.Base.Configuration;
using RSG.Metadata.Model;
using P4API;
using System.Runtime.InteropServices;
using Shell32;

#endregion

namespace ContentStar
{
    #region Structs

    [StructLayout(LayoutKind.Sequential)]
    public struct SHFILEINFO
    {
        public IntPtr hIcon;
        public IntPtr iIcon;
        public uint dwAttributes;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 260)]
        public string szDisplayName;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 80)]
        public string szTypeName;
    };

    class Win32
    {
        public const uint SHGFI_ICON = 0x100;
        public const uint SHGFI_LARGEICON = 0x0; // 'Large icon
        public const uint SHGFI_SMALLICON = 0x1; // 'Small icon
        public const uint SHGFI_OPENICON = 0x2;

        [DllImport("shell32.dll")]
        public static extern IntPtr SHGetFileInfo(string pszPath, uint dwFileAttributes, ref SHFILEINFO psfi, uint cbSizeFileInfo, uint uFlags);
    }

    #endregion

    public class Globals
    {
        #region Variables

        public static long MAX_SIZE_PER_RPF = 4000000000;
        public static int PS3_CID_FMT_LENGTH = "WWWWWW-WWWWWWWWW_00-WWWWWWWWWWWWWWWW".Length;
		public static string DEFAULT_MAKE_XB1_PACKAGE_EXE = @"C:\Program Files (x86)\Microsoft Durango XDK\bin\makepkg.exe";
		public static string DEFAULT_XB1_CONNECT_EXE = @"C:\Program Files (x86)\Microsoft Durango XDK\bin\xbconnect.exe";
		public static string DEFAULT_XB1_XBAPP_EXE = @"C:\Program Files (x86)\Microsoft Durango XDK\bin\xbapp.exe";
        public static string PROJECT_CS_SUFFIX = "_AUTOGEN";
        public static string UNLOCKS_CS_SUFFIX = "_UNLOCKS_AUTOGEN";
        public static string SETTINGS_FILE = "cstarSettings.xml";
        public static string GLOBAL_FIELDS_FILE = "globalFields.xml";
        public static string FILE_TYPE_MAPPING_FILE = "fileTypeMappings.xml";
        public static string XLAST_WRITER_VERSION = "2.0.21076.0";
        public static string GP4_WRITER_VERSION = "0990";
        public static string DEFAULT_PS3_CTRL_32 = @"C:\Program Files (x86)\SN Systems\PS3\bin\ps3ctrl.exe";
        public static string DEFAULT_PS3_CTRL_64 = @"C:\Program Files\SN Systems\PS3\bin\ps3ctrl.exe";
        public static string DEFAULT_PS4_CTRL_64 = @"C:\Program Files\SCE\ORBIS\Tools\Target Manager Server\bin\orbis-ctrl.exe";        
        public static string DEFAULT_PS4_CTRL_32 = @"C:\Program Files (x86)\SCE\ORBIS\Tools\Target Manager Server\bin\orbis-ctrl.exe";
        public static string DEFAULT_MAKE_PS4_PACKAGE_EXE_32 = @"C:\Program Files (x86)\SCE\ORBIS\Tools\Publishing Tools\bin\orbis-pub-cmd.exe";
        public static string DEFAULT_MAKE_PS4_PACKAGE_EXE_64 = @"C:\Program Files\SCE\ORBIS\Tools\Publishing Tools\bin\orbis-pub-cmd.exe";
        public static string DEFAULT_MAKE_PS3_PACKAGE_EXE = @"X:\ps3sdk\dev\usr\local\430_001\cell\host-win32\bin\make_package_npdrm.exe";
        public static string DEFAULT_MAKE_PS3_EDAT_EXE = @"X:\ps3sdk\dev\usr\local\430_001\cell\host-win32\bin\make_edata_npdrm.exe";
        public static string DEFAULT_BLAST_32 = @"C:\Program Files (x86)\Microsoft Xbox 360 SDK\bin\win32\blast.exe";
        public static string DEFAULT_BLAST_64 = @"C:\Program Files\Microsoft Xbox 360 SDK\bin\win32\blast.exe";
        public static string RAGE_BUILDER_PATH = @"x:\gta5\tools\bin\ragebuilder_0378.exe";
        public static string RAGE_BUILDER_PATH_NG = @"x:\gta5\tools_ng\bin\ragebuilder_x64_0378.exe";
		public static string OUTPUT_DIRECTORY = @"X:\gta5_dlc\Packages";
		public static string NG_BUILD_RPF_PATH = @"dlcPacks\";
		public static string NG_TEMP_RPF_PATH = @"x:\cStarTemp\";
        public static string QUOTE = "\"";
        public static string WIKI_PAGE_URL = "https://devstar.rockstargames.com/wiki/index.php/Packaging_and_installing_DLC";
        public static string SETUP_XML = "setup2.xml";
        public static string CONTENT_XML = "content.xml";
        public static string PROJECT_EXT = ".cstarproj";
        public static string FILE_FILTERS_NAME = "fileFilters.xml";
        public static string FILE_EXCLUSIONS_NAME = "fileExclusions.xml";
        public static Font BOLD_FONT = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold);
        public static Font REGULAR_FONT = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular);
        public static Font OUTPUT_BOLD_FONT = new Font("Courier New", 11.25f, FontStyle.Bold);
        public static Font OUTPUT_REGULAR_FONT = new Font("Courier New", 11.25f, FontStyle.Regular);
        public static Font COUR_NEW = new Font("Courier New", 8.25F, FontStyle.Regular);
        public static IConfig TOOLS_CONFIG = null;
        public static StructureDictionary META_DICTIONARY = null;
        public static Dictionary<int, DateTime> LAST_WRITES = new Dictionary<int, DateTime>();
        public static int ACTIVE_CL = 0;
        public static string CONTENT_TAG_HASH = "CDataFileMgr::ContentsOfDataFileXml";
        public static string SETUP_TAG_HASH = "SSetupData";
        public static string CONTENT_UNLOCK_TAG_HASH = "SContentUnlocks";
        public static string[] EXCLUDE_META_TAGS = { CONTENT_TAG_HASH, SETUP_TAG_HASH, CONTENT_UNLOCK_TAG_HASH };
		public static string XB1_TEST_PRODUCT_ID = "00000000-0000-0000-0000-000000000000";
        public static Shell SHELL = new Shell();
        public static SHFILEINFO SHELL_INFO = new SHFILEINFO();
		public static bool USE_XB1_WORKAROUND_PATH_FOR_VIRTUAL_DRIVES = true;
		public static string XB1_WORKAROUND_PATH = @"C:\gtatemp";
        public static FileExclusions GLOBAL_FILE_EXCLUSIONS;

        #endregion

        #region Functions

        public static void initialise()
        {
            TOOLS_CONFIG = ConfigFactory.CreateConfig();
            META_DICTIONARY = new StructureDictionary();
            META_DICTIONARY.Load(TOOLS_CONFIG.Project.DefaultBranch, @"X:\gta5\assets_ng\metadata\definitions"); // Hard code this just in case someone isn't running tools_ng
            GLOBAL_FILE_EXCLUSIONS = FileExclusions.ReadFile(Application.StartupPath + "\\" + FILE_EXCLUSIONS_NAME);
        }

        public static void setupFileWatcher(FileSystemWatcher watcher, string filePath)
        {
            try
            {
                bool pathEmpty = watcher.Path.Length <= 0;

                watcher.Path = Path.GetDirectoryName(filePath);
                watcher.NotifyFilter = NotifyFilters.LastWrite;
                watcher.Filter = Path.GetFileName(filePath);
                watcher.IncludeSubdirectories = false;

                if (!LAST_WRITES.ContainsKey(Globals.getStringHashCode(filePath)) && File.Exists(filePath))
                    LAST_WRITES.Add(Globals.getStringHashCode(filePath), File.GetLastWriteTime(filePath));

                if (pathEmpty)
                {
                    watcher.Changed += new FileSystemEventHandler(onFileChanged);
                    watcher.EnableRaisingEvents = true;
                }
            }
            catch (System.Exception ex)
            {
                OutputWindow.appendOutput(ex.Message, OutputWindow.MessageType.ERROR);
            }
        }

        private static volatile bool projDialogActive = false;

        public static void onFileChanged(object source, FileSystemEventArgs e)
        {
            if (Globals.LAST_WRITES.ContainsKey(Globals.getStringHashCode(e.FullPath)))
            {
                bool prompt = false;
                DateTime lastWriteForFile;

                if (Globals.LAST_WRITES.TryGetValue(Globals.getStringHashCode(e.FullPath), out lastWriteForFile))
                {
                    TimeSpan deltaTimeSpan = File.GetLastWriteTime(e.FullPath) - lastWriteForFile;

                    // NOT COOL! I know... but this stops spammy messages when files are being written out in chunks.
                    if (deltaTimeSpan.TotalSeconds > 1)
                        prompt = true;

                    Globals.LAST_WRITES[Globals.getStringHashCode(e.FullPath)] = File.GetLastWriteTime(e.FullPath);
                }

                if (prompt)
                {
                    string fileName = Path.GetFileName(e.FullPath);
                    string extension = Path.GetExtension(e.FullPath);
                    string message = "The file " + fileName + " has been externally modified, would you like to reload it?";
                    string headerMsg = "File externally modified";

                    if (String.Compare(extension, Globals.PROJECT_EXT, true) == 0)
                    {
                        if (!projDialogActive)
                        {
                            headerMsg = "Solution externally modified";
                            message = "One or more of the project files has been externally modified, would you like to reload the solution?";

                            projDialogActive = true;
                            {
                                if (MessageBox.Show(message, headerMsg, MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                                    Program.reloadSolution();
                            }
                            projDialogActive = false;
                        }
                    }
                    else if (String.Compare(fileName, Globals.SETTINGS_FILE, true) == 0)
                    {
                        if (MessageBox.Show(message, headerMsg, MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            SettingsManager.readSettings();
                            Program.reloadSettings();
                        }
                    }
                    else if (String.Compare(fileName, Globals.GLOBAL_FIELDS_FILE, true) == 0)
                    {
                        if (MessageBox.Show(message, headerMsg, MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            GlobalFieldsManager.readGlobalFields();
                            Program.reloadGlobalFields();
                        }
                    }
                    else if (String.Compare(fileName, Globals.FILE_TYPE_MAPPING_FILE, true) == 0)
                    {
                        if (MessageBox.Show(message, headerMsg, MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            FileTypeMappingMgr.readTypeMappings();
                            Program.reloadTypeMappings();
                        }
                    }
                }
            }
        }

        public static Icon ExtractIcon(string fullPath)
        {
            IntPtr hImgSmall;

            hImgSmall = Win32.SHGetFileInfo(fullPath, 0, ref SHELL_INFO, (uint)Marshal.SizeOf(SHELL_INFO), Win32.SHGFI_ICON | Win32.SHGFI_SMALLICON);

            return Icon.FromHandle(SHELL_INFO.hIcon);
        }

        public static DialogResult InputBox(string title, string promptText, ref string value, Form parent)
        {
            DialogResult dialogResult = DialogResult.None;

            using (Form form = new Form())
			using (System.Windows.Forms.Label label = new System.Windows.Forms.Label())
            using (TextBox textBox = new TextBox())
			using (Button buttonOk = new Button())
			using (Button buttonCancel = new Button())
			{
	            form.Text = title;
	            label.Text = promptText;
	            textBox.Text = value;
	
	            buttonOk.Text = "OK";
	            buttonCancel.Text = "Cancel";
	            buttonOk.DialogResult = DialogResult.OK;
	            buttonCancel.DialogResult = DialogResult.Cancel;
	
	            label.SetBounds(9, 20, 372, 13);
	            textBox.SetBounds(12, 36, 372, 20);
	            buttonOk.SetBounds(228, 72, 75, 23);
	            buttonCancel.SetBounds(309, 72, 75, 23);
	
	            label.AutoSize = true;
	            textBox.Anchor = textBox.Anchor | AnchorStyles.Right;
	            buttonOk.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
	            buttonCancel.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
	
	            form.ClientSize = new Size(396, 107);
	            form.Controls.AddRange(new Control[] { label, textBox, buttonOk, buttonCancel });
	            form.ClientSize = new Size(Math.Max(300, label.Right + 10), form.ClientSize.Height);
	            form.FormBorderStyle = FormBorderStyle.FixedDialog;
	            form.StartPosition = FormStartPosition.CenterScreen;
	            form.MinimizeBox = false;
	            form.MaximizeBox = false;
	            form.AcceptButton = buttonOk;
	            form.CancelButton = buttonCancel;
	            form.Icon = parent.Icon;
	            form.TopMost = true;
	
	            dialogResult = form.ShowDialog();
	            value = textBox.Text;
			}

            return dialogResult;
        }

        public static string getRandomString(int length)
        {
            string retValue = string.Empty;

            while (true)
            {
                retValue += Path.GetRandomFileName().Replace(".", "");

                if (retValue.Length >= length)
                {
                    retValue = retValue.Remove(length - 1, retValue.Length - length);

                    if (retValue.Length != length)
                        retValue = string.Empty;

                    break;
                }
            }

            return retValue;
        }

        public static int getStringHashCode(string hashSource)
        {
            return hashSource.ToLower().GetHashCode();
        }

        public static List<string> getDirectoriesAbove(string fullPath)
        {
            string delims = "\\";
            string dirAboveCurrent = Path.GetDirectoryName(fullPath);
            List<string> dirList = dirAboveCurrent.Split(delims.ToArray()).ToList();

            if (dirList.Count > 0)
            {
                if (dirList[0].Contains(":"))
                    dirList.RemoveAt(0);
            }

            return dirList;
        }

        public static void fixupNodeText(TreeNode node)
        {
            node.Text += string.Empty; // Correct the crappy truncated bold font problem (http://support.microsoft.com/?scid=kb%3Ben-us%3B937215&x=8&y=13)
        }

        public static string prefixDeviceName(string deviceName)
        {
            string prefix = "dlc_";

            if (!deviceName.StartsWith(prefix))
                return prefix + deviceName;

            return deviceName;
        }
    }

    #endregion
}
