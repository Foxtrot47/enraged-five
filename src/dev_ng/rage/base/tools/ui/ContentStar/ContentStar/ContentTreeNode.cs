﻿/*******************************************\
| ContentStar : Rockstar Games © 2013       |
| ------------------------------------------|
| Author: Thomas Woodhead (R* London)       |
| Email: thomas.woodhead@rockstarlondon.com |
\*******************************************/

#region Usings

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Collections;
using System.Xml;
using System.Xml.Linq;
using System.Diagnostics;
using System.Threading;
using System.Xml.Serialization;
using ContentStar;
using contentStar;
using RSG.Base.Configuration;
using RSG.Platform;

#endregion

namespace contentStar
{
    #region Node Sorter

    public class NodeSorter : IComparer
    {
        public int Compare(object x, object y)
        {
            contentTreeNode nodeX = x as contentTreeNode;
            contentTreeNode nodeY = y as contentTreeNode;

            if (nodeX.NodeType == nodeY.NodeType)
                return string.Compare(nodeX.Text, nodeY.Text);
            else
            {
                if (nodeX.NodeType == nodeType.NT_FOLDER)
                    return -1;
                else
                    return 1;
            }
        }
    }

    #endregion

    #region Content tree node

    public enum nodeType { NT_FILE, NT_FOLDER, NT_CONTENT_ROOT, NT_PROJECT_ROOT, NT_BRANCH_ROOT }

    public class contentTreeNode : TreeNode
    {
        string m_fullFilePath;
        string m_projectRelativePath;
        bool m_internal;
        long m_size;
        int m_buildInclusions;
        nodeType m_type;
        Project m_contentProject;
        IBranch m_branch;

        public contentTreeNode()
        {
            m_type = nodeType.NT_FOLDER;
            m_contentProject = null;
            m_size = 0;
            m_buildInclusions = 0;
            m_projectRelativePath = string.Empty;
            m_internal = true;
            m_branch = null;

            foreach (Platform platform in Enum.GetValues(typeof(Platform)))
                addBuildInclusion(platform);
        }

        public override string ToString()
        {
            return m_fullFilePath;
        }

        public void toggleBuildStatus()
        {
            if (m_contentProject != null)
                m_contentProject.setIsInBuild(!m_contentProject.getIsInBuild());
        }

        private void addBuildInclusion(Platform flag)
        {
            BuildManager.addBuildInclusion(ref m_buildInclusions, (int)flag);
        }

        public bool isIncludedInBuild(Platform flag)
        {
            return BuildManager.isIncludedInBuild(m_buildInclusions, (int)flag);
        }

        private void decipherBuildInclusions()
        {
            m_buildInclusions = 0;

            if (m_contentProject != null && m_branch != null)
            {
                string pathToCheck = m_fullFilePath.ToLower() + "\\";
                List<IBranch> validBranches = m_contentProject.getValidBranches();

                foreach (KeyValuePair<Platform, ITarget> currTarget in m_branch.Targets)
                    addBuildInclusion(currTarget.Key);

                foreach (IBranch currBranch in validBranches)
                {
                    foreach (KeyValuePair<Platform, ITarget> currTarget in currBranch.Targets)
                    {
                        string value = "\\" + BuildManager.GetDirectoryPlatformName(currTarget.Key).ToLower() + "\\";

                        if (pathToCheck.Contains(value))
                        {
                            m_buildInclusions = 0;
                            addBuildInclusion(currTarget.Key);
                        }
                    }
                }
            }
        }

        public void determineInternal()
        {
            m_internal = false;

            if (m_contentProject != null)
            {
                List<IBranch> validBranches = m_contentProject.getValidBranches();

                foreach (IBranch currBranch in validBranches)
                    m_internal |= m_fullFilePath.Contains(currBranch.Build);
            }
        }

        public void extractProjectRelativePath()
        {
            m_projectRelativePath = string.Empty;

            if (m_contentProject != null)
            {
                contentTreeNode currNode = this;

                while (currNode != null && (currNode.NodeType != nodeType.NT_PROJECT_ROOT && currNode.NodeType != nodeType.NT_BRANCH_ROOT))
                {
                    m_projectRelativePath = "\\" + currNode.Text + m_projectRelativePath;
                    currNode = (contentTreeNode)currNode.Parent;
                }
            }
        }

        public string getIncludedBuildsText()
        {
            int addedBuilds = 0;
            string retValue = string.Empty;

            if (isIncludedInBuild(Platform.Xbox360))
            {
                retValue += (addedBuilds > 0) ? "// XBOX " : "XBOX ";
                addedBuilds++;
            }

            if (isIncludedInBuild(Platform.PS3))
            {
                retValue += (addedBuilds > 0) ? "// PS3 " : "PS3 ";
                addedBuilds++;
            }

            if (isIncludedInBuild(Platform.Win32))
            {
                retValue += (addedBuilds > 0) ? "// x86 " : "x86 ";
                addedBuilds++;
            }

            if (isIncludedInBuild(Platform.Win64))
            {
                retValue += (addedBuilds > 0) ? "// x64 " : "x64 ";
                addedBuilds++;
            }

            if (isIncludedInBuild(Platform.XboxOne))
            {
                retValue += (addedBuilds > 0) ? "// XBOXONE " : "XBOXONE ";
                addedBuilds++;
            }

            if (isIncludedInBuild(Platform.PS4))
            {
                retValue += (addedBuilds > 0) ? "// PS4 " : "PS4 ";
                addedBuilds++;
            }

            return retValue;
        }

        public void setBuildStatus(bool value)
        {
            if (m_contentProject != null)
                m_contentProject.setIsInBuild(value);
        }

        public string ProjectRelativeFilePath
        {
            get { return m_projectRelativePath; }
        }

        public bool Internal
        {
            get { return m_internal; }
            set { m_internal = value; }
        }

        public long Size
        {
            get { return m_size; }
            set { m_size = value; }
        }

        public Project ContentProject
        {
            get { return m_contentProject; }
            set { m_contentProject = value; }
        }

        public IBranch Branch
        {
            get { return m_branch; }
            set { m_branch = value; }
        }

        public nodeType NodeType
        {
            get { return m_type; }
            set { m_type = value; }
        }

        public string FullFilePath
        {
            get { return m_fullFilePath; }
            set { m_fullFilePath = value; decipherBuildInclusions(); }
        }
    }

    #endregion
}