﻿/*******************************************\
| ContentStar : Rockstar Games © 2013       |
| ------------------------------------------|
| Author: Thomas Woodhead (R* London)       |
| Email: thomas.woodhead@rockstarlondon.com |
\*******************************************/

#region Usings

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Collections;
using System.Xml;
using System.Xml.Linq;
using System.Diagnostics;
using System.Threading;
using System.Xml.Serialization;
using ContentStar;
using contentStar;
using RSG.Metadata.Parser;
using RSG.Metadata.Model;

#endregion

namespace ContentStar
{
    [Serializable]
    public class FileTypeMapping
    {
        public enum MappingType { DFMT_NONE = 0, DFMT_NAME_OR_EXTENSION, DFMT_ROOT_METATAG }

        [XmlAttribute("FileType")]
        public string m_fileType;

        [XmlAttribute("MappingType")]
        public MappingType m_mappingType;

        [XmlAttribute("Value")]
        public string m_mappingValue;

        public bool foundMapping(MappingType targetMapping, string mappingSearchString)
        {
            if (targetMapping == m_mappingType)
            {
                char[] delims = { ',' };
                string[] mappingValueTokens = m_mappingValue.Split(delims);

                mappingSearchString = mappingSearchString.ToLower();

                if (targetMapping == MappingType.DFMT_NAME_OR_EXTENSION)
                {
                    // Path must contain all of the mapping tokens
                    foreach (string mappingValue in mappingValueTokens)
                    {
                        if (!mappingSearchString.Contains(mappingValue.ToLower()))
                            return false;
                    }

                    return true;
                }
                else if (targetMapping == MappingType.DFMT_ROOT_METATAG)
                {
                    // Root tag must contain at least one of the mapping tokens
                    foreach (string mappingValue in mappingValueTokens)
                    {
                        if (String.Compare(mappingValue, mappingSearchString, true) == 0)
                            return true;
                    }

                    return false;
                }
            }

            return false;
        }
    }

    [Serializable]
    [XmlRoot("FileTypeMappings")]
    public class FileTypeMappingMgr
    {
        [XmlArray("Items")]
        [XmlArrayItem("Mapping", typeof(FileTypeMapping), IsNullable = false)]
        public List<FileTypeMapping> m_typeMappings;

        [XmlIgnoreAttribute()]
        private static FileSystemWatcher m_fileWatcher = null;

        [XmlIgnoreAttribute()]
        private static FileTypeMappingMgr m_fileTypeMapMgrInst = null;

        public static void initialise()
        {
            m_fileTypeMapMgrInst = new FileTypeMappingMgr();
        }

        private static void setupTypeMappingsWatcher(string fullPath)
        {
            if (m_fileWatcher == null)
            {
                m_fileWatcher = new FileSystemWatcher();
                Globals.setupFileWatcher(m_fileWatcher, fullPath);
            }
        }

        public static void readTypeMappings()
        {
            try
            {
                string typeMappingsFilePath = Application.StartupPath + "\\" + Globals.FILE_TYPE_MAPPING_FILE;

                if (File.Exists(typeMappingsFilePath))
                {
                    OutputWindow.appendOutput("Reading " + typeMappingsFilePath, OutputWindow.MessageType.IMPORTANT);

                    XmlSerializer fileTypeMappingDeserialiser = new XmlSerializer(typeof(FileTypeMappingMgr));
                    using (TextReader readStream = new StreamReader(typeMappingsFilePath))
                    {
                        m_fileTypeMapMgrInst = (FileTypeMappingMgr)fileTypeMappingDeserialiser.Deserialize(readStream);

                        setupTypeMappingsWatcher(typeMappingsFilePath);

                        readStream.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                OutputWindow.appendOutput(ex.Message, OutputWindow.MessageType.ERROR);
            }
        }

        public static void saveTypeMappings()
        {
            try
            {
                string fileTypeMappingFilePath = Application.StartupPath + "\\" + Globals.FILE_TYPE_MAPPING_FILE;

                OutputWindow.appendOutput("Saving " + fileTypeMappingFilePath, OutputWindow.MessageType.IMPORTANT);

                CommandPrompt.perforceCheckout(fileTypeMappingFilePath);

                if (m_fileWatcher != null)
                    m_fileWatcher.EnableRaisingEvents = false; // Don't raise events about ourselves

                XmlSerializer globalFieldsSerialiser = new XmlSerializer(typeof(FileTypeMappingMgr));
                using (TextWriter writeStream = new StreamWriter(fileTypeMappingFilePath))
                {
                    globalFieldsSerialiser.Serialize(writeStream, m_fileTypeMapMgrInst);

                    writeStream.Close();
                }

                if (m_fileWatcher != null)
                    m_fileWatcher.EnableRaisingEvents = true;

                setupTypeMappingsWatcher(fileTypeMappingFilePath);
            }
            catch (Exception ex)
            {
                OutputWindow.appendOutput(ex.Message, OutputWindow.MessageType.ERROR);
            }
        }

        public FileTypeMappingMgr()
        {
            m_typeMappings = new List<FileTypeMapping>();
        }

        private string findRootTag(string filePath)
        {
            try
            {
                // Try to load the supplied file as a meta file...
                MetaFile metaFile = new MetaFile(filePath, Globals.META_DICTIONARY);

                if (metaFile != null)
                {
                    if (metaFile.Definition != null)
                        return metaFile.Definition.Name;
                }
            }
            catch (System.Exception /*ex*/) { }

            try
            {
                // Try to read a plain XML file for root tag
                using (StreamReader xmlFileStream = new StreamReader(filePath))
                {
                    if (xmlFileStream != null)
                    {
                        XmlReader xmlReader = XmlReader.Create(xmlFileStream);

                        if (xmlReader != null && xmlReader.MoveToContent() == XmlNodeType.Element)
                            return xmlReader.Name;
                    }
                }
            }
            catch (System.Exception /*ex*/) { }

            return null;
        }

        public string addTypeMapping(string filePath, string enumMapping)
        {
            string rootTag = findRootTag(filePath);

            if (rootTag != null)
            {
                // Check to see if a mapping already exists for this tag
                foreach (FileTypeMapping currMapping in m_typeMappings)
                {
                    if (currMapping.foundMapping(FileTypeMapping.MappingType.DFMT_ROOT_METATAG, rootTag))
                        return currMapping.m_fileType;
                }

                if (MessageBox.Show("Would you like to add this mapping?\nEnum: " + enumMapping + " -> Root Tag: " + rootTag, 
                    "Add missing mapping", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    FileTypeMapping newMapping = new FileTypeMapping();

                    newMapping.m_fileType = enumMapping;
                    newMapping.m_mappingType = FileTypeMapping.MappingType.DFMT_ROOT_METATAG;
                    newMapping.m_mappingValue = rootTag;
                    m_typeMappings.Add(newMapping);

                    return enumMapping;
                }
            }

            string filePathMappingValue = string.Empty;

            if (Globals.InputBox("File path mapping value required",
                            "Enter file path mapping value for " + filePath + " (" + enumMapping + "):", ref filePathMappingValue, Program.getForm()) == DialogResult.OK)
            {
                // Check to see if a mapping already exists for file path
                foreach (FileTypeMapping currMapping in m_typeMappings)
                {
                    if (currMapping.foundMapping(FileTypeMapping.MappingType.DFMT_NAME_OR_EXTENSION, filePathMappingValue))
                        return currMapping.m_fileType;
                }

                if (MessageBox.Show("Would you like to add this mapping?\nEnum: " + enumMapping + " -> Path Mapping: " + filePathMappingValue,
                    "Add missing mapping", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    FileTypeMapping newMapping = new FileTypeMapping();

                    newMapping.m_fileType = enumMapping;
                    newMapping.m_mappingType = FileTypeMapping.MappingType.DFMT_NAME_OR_EXTENSION;
                    newMapping.m_mappingValue = filePathMappingValue;
                    m_typeMappings.Add(newMapping);

                    return enumMapping;
                }
            }

            return string.Empty;
        }

        public string getFileType(string filePath)
        {
            try
            {
                string rootTag = findRootTag(filePath);

                if (rootTag != null)
                {
                    foreach (FileTypeMapping currMapping in m_typeMappings)
                    {
                        if (currMapping.foundMapping(FileTypeMapping.MappingType.DFMT_ROOT_METATAG, rootTag))
                            return currMapping.m_fileType;
                    }
                }

                // If we have no root tag mappings or the root tag we did find doesn't have a mapping just check the file path
                foreach (FileTypeMapping currMapping in m_typeMappings)
                {
                    if (currMapping.foundMapping(FileTypeMapping.MappingType.DFMT_NAME_OR_EXTENSION, filePath))
                        return currMapping.m_fileType;
                }
            }
            catch (System.Exception ex)
            {
                OutputWindow.appendOutput(ex.Message, OutputWindow.MessageType.ERROR);
            }

            return null;
        }

        [XmlIgnoreAttribute()]
        public List<FileTypeMapping> MappingList
        {
            get { return m_typeMappings; }
        }

        [XmlIgnoreAttribute()]
        public static FileTypeMappingMgr GetInstance
        {
            get { return m_fileTypeMapMgrInst; }
        }
    }
}
