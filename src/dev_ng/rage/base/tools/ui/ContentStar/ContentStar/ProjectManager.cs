﻿/*******************************************\
| ContentStar : Rockstar Games © 2013       |
| ------------------------------------------|
| Author: Thomas Woodhead (R* London)       |
| Email: thomas.woodhead@rockstarlondon.com |
\*******************************************/

#region Usings

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Collections;
using System.Xml;
using System.Xml.Linq;
using System.Diagnostics;
using System.Xml.Serialization;
using ContentStar;
using System.Threading;
using contentStar;
using RSG.Base.Configuration;
using RSG.Metadata.Model;

#endregion

namespace ContentStar
{
    #region Project Sorter

    public class ProjectComparer : IComparer<Project>
    {
        public int Compare(Project projectX, Project projectY)
        {
            if (projectX == projectY)
                return 0;
            else if (!projectX.m_compatPack)
                return -1;
            else
                return 1;
        }
    }

    #endregion

    #region Project Restore Data

    public class ProjectRestoreData
    {
        public FileSystemWatcher m_fileWatcher;
        public bool m_isInBuild;
    }

    #endregion

    public class ProjectManager
    {
        #region Variables

        private static ProjectManager m_projManInst = null;
        private List<Project> m_projects = new List<Project>();
        private Project m_currentProject = null;
        private bool m_dirty = false;
        private string m_solutionTitle = "gta5_dlc";
        private Dictionary<int, Project> m_lookupTable = new Dictionary<int, Project>();
        private Dictionary<int, ProjectRestoreData> m_projRestoreData = new Dictionary<int, ProjectRestoreData>();
        private ProjectComparer m_projectSorter = new ProjectComparer();

        #endregion

        #region Initialise

        public static void initialise()
        {
            if (m_projManInst == null)
                m_projManInst = new ProjectManager();
        }

        #endregion

        #region Save

        public void saveProjects()
        {
            try
            {
                TextWriter writeStream = null;
                XmlSerializer projectSerialiser = new XmlSerializer(typeof(Project));
                string[] projectForP4 = new string[m_projects.Count];
                
                for (int i = 0; i < m_projects.Count; i++)
                    projectForP4[i] = m_projects[i].m_fullPath;

                CommandPrompt.perforceCheckoutArray(projectForP4);

                foreach (Project currentProject in m_projects)
                {
                    currentProject.preSerialise();
                    {
                        using (writeStream = new StreamWriter(currentProject.m_fullPath))
                        {
                            projectSerialiser.Serialize(writeStream, currentProject);
                            writeStream.Close();
                            currentProject.m_projectWatcher.EnableRaisingEvents = true;
                        }
                    }
                    currentProject.postSerialise();
                }

                OutputWindow.appendOutput("Done saving projects", OutputWindow.MessageType.IMPORTANT);
            }
            catch (System.Exception ex)
            {
                OutputWindow.appendOutput(ex.Message, OutputWindow.MessageType.ERROR);
            }
        }

        #endregion

        #region Read

        public void readProjects()
        {
            m_projRestoreData.Clear();

            // Store any values from the previous state of the projects that we want to transfer after the reload
            for (int i = 0; i < m_projects.Count; i++)
            {
                ProjectRestoreData newRestoreData = new ProjectRestoreData();
                int pathHash = Globals.getStringHashCode(m_projects[i].m_toolsProject.DefaultBranch.Build);

                newRestoreData.m_isInBuild = m_projects[i].m_isInBuild;
                newRestoreData.m_fileWatcher = m_projects[i].m_projectWatcher;

                m_projRestoreData.Add(pathHash, newRestoreData);
            }

            m_projects.Clear();
            m_lookupTable.Clear();

            try
            {
                IProject currProj = null;
                string[] sourceDirChildDirs = null;
                string[] sourceDirFiles = null;
                Project readProject = null;
                XmlSerializer projectSerialiser = new XmlSerializer(typeof(Project));

                foreach (KeyValuePair<string, IProject> currProjPair in Globals.TOOLS_CONFIG.DLCProjects)
                {
                    currProj = currProjPair.Value;

                    if (!Directory.Exists(currProj.Root))
                        continue;

                    sourceDirChildDirs = Directory.GetDirectories(currProj.Root);
                    sourceDirFiles = Directory.GetFiles(currProj.Root, "*" + Globals.PROJECT_EXT);
                    readProject = null;

                    foreach (string filePath in sourceDirFiles)
                    {
                        try
                        {
                            int projectHashName = Globals.getStringHashCode(currProj.DefaultBranch.Build);

							using (TextReader readStream = new StreamReader(filePath))
							{
                            	readProject = (Project)projectSerialiser.Deserialize(readStream);

                            	readStream.Close();
                            	readStream.Dispose();
							}

                            readProject.setIsInBuild(true);
                            readProject.m_title = currProj.Name;
                            readProject.m_fullPath = filePath;
                            readProject.m_channelId = OutputWindow.addChannel(readProject.m_title);
                            readProject.m_toolsProject = currProj;
                            readProject.m_compatPack = readProject.m_fullPath.Contains("compatPacks");
                            readProject.m_fileExclusions = FileExclusions.ReadFile(currProj.Root + "\\" + Globals.FILE_EXCLUSIONS_NAME);

                            // Try to restore any data from when we previously loaded this project
                            if (m_projRestoreData.ContainsKey(projectHashName))
                            {
                                ProjectRestoreData newRestoreData = m_projRestoreData[projectHashName];

                                readProject.setIsInBuild(newRestoreData.m_isInBuild);
                                readProject.m_projectWatcher = newRestoreData.m_fileWatcher;
                            }

                            if (readProject.m_projectWatcher == null)
                            {
                                readProject.m_projectWatcher = new FileSystemWatcher();
                                Globals.setupFileWatcher(readProject.m_projectWatcher, filePath);
                            }

                            m_projects.Add(readProject);
                            m_lookupTable.Add(projectHashName, m_projects[m_projects.Count - 1]);

                            break;
                        }
                        catch (System.Exception ex)
                        {
                            OutputWindow.appendOutput(ex.Message, OutputWindow.MessageType.ERROR);
                        }
                    }

                    // This DLC project doesn't have any C* project, lets make one.
                    if (readProject == null)
                    {
                        TextWriter writeStream = null;
                        string filePath = currProj.Root + "\\project" + Globals.PROJECT_EXT;
                        readProject = new Project();

                        readProject.setIsInBuild(true);
                        readProject.m_title = currProj.Name;
                        readProject.m_fullPath = filePath;
                        readProject.m_channelId = OutputWindow.addChannel(readProject.m_title);
                        readProject.m_toolsProject = currProj;
                        readProject.m_compatPack = readProject.m_fullPath.Contains("compatPacks");
                        readProject.m_projectWatcher = new FileSystemWatcher();
                        Globals.setupFileWatcher(readProject.m_projectWatcher, filePath);

                        readProject.preSerialise();
                        {
                            using (writeStream = new StreamWriter(readProject.m_fullPath))
                            {
                                projectSerialiser.Serialize(writeStream, readProject);
                                writeStream.Close();
                                readProject.m_projectWatcher.EnableRaisingEvents = true;

                                CommandPrompt.perforceCheckout(readProject.m_fullPath);
                            }
                        }
                        readProject.postSerialise();

                        m_projects.Add(readProject);
                        m_lookupTable.Add(Globals.getStringHashCode(currProj.DefaultBranch.Build), m_projects[m_projects.Count - 1]);
                    }
                }

                m_projects.Sort(m_projectSorter);
            }
            catch (System.Exception ex)
            {
                OutputWindow.appendOutput(ex.Message, OutputWindow.MessageType.ERROR);
            }

            m_projRestoreData.Clear();
        }

        #endregion

        #region Project UI

        private void setCurrentProjNodeFonts(Font font)
        {
            contentTreeNode targetNode = null;

            if (m_currentProject != null)
            {
                targetNode = m_currentProject.m_rootNode;

                while (targetNode != null)
                {
                    // TOMW: targetNode.NodeFont = font;
                    targetNode.Text += string.Empty;
                    targetNode = (contentTreeNode)targetNode.Parent;
                }
            }
        }

        #endregion

        #region Gets

        public Project GetProject(int buildDirHash)
        {
            if (m_lookupTable.ContainsKey(buildDirHash))
                return m_lookupTable[buildDirHash];

            return null;
        }

        #endregion

        #region Properties

        public static ProjectManager GetInstance
        {
            get { return m_projManInst; }
        }

        public List<Project> Projects
        {
            get { return m_projects; }
        }

        public Project CurrentProject
        {
            get { return m_currentProject; }
            set 
            { 
                if (value != m_currentProject)
                {
                    bool previousDirtyState = m_dirty; // switching projects clears forms, which changes the dirty state, don't want this...
                    Project previousProject = m_currentProject;

                    // TOMW: setCurrentProjNodeFonts(Globals.REGULAR_FONT);
                    m_currentProject = value;
                    // TOMW: setCurrentProjNodeFonts(Globals.BOLD_FONT);

                    Program.onSelectedProjectChanged(previousProject, m_currentProject);
                    Dirty = previousDirtyState;
                }
            }
        }

        public string SolutionTitle
        {
            get { return m_solutionTitle; }
        }

        public string SolutionRoot
        {
            get { return "x:\\" + m_solutionTitle; }
        }

        public bool Dirty
        {
            get { return m_dirty; }
            set { m_dirty = value; Program.updateUIFromDirtyState(); }
        }

        #endregion
    }
}
