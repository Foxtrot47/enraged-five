﻿/*******************************************\
| ContentStar : Rockstar Games © 2013       |
| ------------------------------------------|
| Author: Thomas Woodhead (R* London)       |
| Email: thomas.woodhead@rockstarlondon.com |
\*******************************************/

#region Usings

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Collections;
using System.Xml;
using System.Xml.Linq;
using System.Diagnostics;
using System.Xml.Serialization;
using ContentStar;
using System.Threading;
using contentStar;
using RSG.SourceControl.Perforce;
using P4API;

#endregion

namespace ContentStar
{
    public class CommandPrompt
    {
        #region Variables

        static Process m_cmdPrompt;
        static P4 m_p4;

        #endregion

        #region Initialise

        public static void initialise()
        {
            m_p4 = Globals.TOOLS_CONFIG.Project.SCMConnect();
        }

        #endregion

        #region Shutdown

        public static void shutdown()
        {
            m_p4.Disconnect();
            m_p4.Dispose();
        }

        #endregion

        #region Open

        public static void OpenPrompt()
        {
            try
            {
                m_cmdPrompt = new Process();
                m_cmdPrompt.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                m_cmdPrompt.StartInfo.CreateNoWindow = true;
                m_cmdPrompt.StartInfo.FileName = "cmd.exe";
                m_cmdPrompt.StartInfo.UseShellExecute = false;
                m_cmdPrompt.StartInfo.RedirectStandardInput = true;
                m_cmdPrompt.StartInfo.RedirectStandardOutput = true;
                m_cmdPrompt.StartInfo.RedirectStandardError = true;
                m_cmdPrompt.StartInfo.WorkingDirectory = "x:\\";
                m_cmdPrompt.Start();
            }
            catch (System.Exception ex)
            {
                OutputWindow.appendOutput(ex.Message, OutputWindow.MessageType.ERROR);
            }
        }

        #endregion

        #region Run

        public static void Run(string command, bool printLog)
        {
            try
            {
                if (printLog)
                    OutputWindow.appendOutput("[" + m_cmdPrompt.ProcessName + "] " + command, OutputWindow.MessageType.IMPORTANT);

                m_cmdPrompt.StandardInput.AutoFlush = true;
                m_cmdPrompt.StandardInput.WriteLine(command);
                m_cmdPrompt.StandardInput.Close();
            }
            catch (System.Exception ex)
            {
                OutputWindow.appendOutput(ex.Message, OutputWindow.MessageType.ERROR);
            }
        }

        #endregion

        #region Close

        public static void ClosePrompt(bool showStdOutput, bool showStdError)
        {
            try
            {
                StringBuilder sbOutput = new StringBuilder();
                StringBuilder sbError = new StringBuilder();

                using (AutoResetEvent outputWaitHandle = new AutoResetEvent(false))
                using (AutoResetEvent errorWaitHandle = new AutoResetEvent(false))
                {
                    // When some data arrives asynchronously, append it to the string builders 
                    m_cmdPrompt.OutputDataReceived += (sender, e) =>
                    {
                        if (e.Data == null)
                            outputWaitHandle.Set();
                        else
                            sbOutput.AppendLine(e.Data);
                    };

                    m_cmdPrompt.ErrorDataReceived += (sender, e) =>
                    {
                        if (e.Data == null)
                            errorWaitHandle.Set();
                        else
                            sbError.AppendLine(e.Data);
                    };

                    // Being async read
                    m_cmdPrompt.BeginOutputReadLine();
                    m_cmdPrompt.BeginErrorReadLine();

                    // Wait for output and error reads to finish and then wait for process to exit
                    outputWaitHandle.WaitOne();
                    errorWaitHandle.WaitOne();
                    m_cmdPrompt.WaitForExit();

                    // Display output if required
                    if (showStdOutput)
                    {
                        string stdOutput = sbOutput.ToString();

                        if (stdOutput.Length > 0)
                            OutputWindow.appendOutput(stdOutput, OutputWindow.MessageType.NORMAL);
                    }

                    if (showStdError)
                    {
                        string errOutput = sbError.ToString();

                        if (!errOutput.Contains("%\b\b\b\b\b") && errOutput.Length > 0)
                            OutputWindow.appendOutput(errOutput, OutputWindow.MessageType.ERROR);
                    }

                    m_cmdPrompt.Dispose();
                }
            }
            catch (System.Exception ex)
            {
                OutputWindow.appendOutput(ex.Message, OutputWindow.MessageType.ERROR);
            }
        }

        #endregion

        #region Perforce

        public static void createPerforceChangeSet()
        {
            try
            {
                bool clExists = false;                
                List<string> arguments = new List<string>();
                string clDescPrefix = "ContentStar DLC Build Data";

                arguments.Add("-u");
                arguments.Add(m_p4.User);
                arguments.Add("-s");
                arguments.Add("pending");

                P4RecordSet retSet = m_p4.Run("changes", false, arguments.ToArray());

                foreach (P4API.P4Record record in retSet)
                {
                    if (record["desc"].StartsWith(clDescPrefix))
                    {
                        clExists = true;

                        if (!int.TryParse(record["change"], out Globals.ACTIVE_CL))
                        {
                            clExists = false;
                            Globals.ACTIVE_CL = 0;
                        }

                        break;
                    }
                }

                if (!clExists)
                {
                    P4PendingChangelist newPendCL = m_p4.CreatePendingChangelist(clDescPrefix + " [" + DateTime.Now.ToShortDateString() + "]");

                    if (newPendCL != null)
                        Globals.ACTIVE_CL = newPendCL.Number;
                }

                if (Globals.ACTIVE_CL != 0)
                    OutputWindow.appendOutput("Targeting pending CL [" + Globals.ACTIVE_CL + "]", OutputWindow.MessageType.IMPORTANT);
                else
                    OutputWindow.appendOutput("Failed to create/find pending CL!", OutputWindow.MessageType.ERROR);
            }
            catch (Exception ex)
            {
                OutputWindow.appendOutput(ex.Message, OutputWindow.MessageType.ERROR);
            }
        }

        public static void perforceCheckoutArray(string[] fullPaths)
        {
            if (Globals.ACTIVE_CL == 0)
                createPerforceChangeSet();

            try
            {
                List<string> arguments = new List<string>();

                if (Globals.ACTIVE_CL != 0)
                {
					arguments.Add("-c");
                    arguments.Add(Globals.ACTIVE_CL.ToString());
                }

                for (int i = 0; i < fullPaths.Length; i++)
                {
                    OutputWindow.appendOutput("Add/Edit " + fullPaths[i] + " [" + Globals.ACTIVE_CL + "]", OutputWindow.MessageType.IMPORTANT);

                    arguments.Add(fullPaths[i]);
                }

                m_p4.Run("add", false, arguments.ToArray());
                m_p4.Run("edit", false, arguments.ToArray());
                
            }
            catch (System.Exception ex)
            {
                OutputWindow.appendOutput(ex.Message, OutputWindow.MessageType.ERROR);
            }
        }

        public static void perforceCheckout(string fullPath)
        {
            if (Globals.ACTIVE_CL == 0)
                createPerforceChangeSet();

            try
            {
                List<string> arguments = new List<string>();

                if (Globals.ACTIVE_CL != 0)
                {
                    arguments.Add("-c");
                    arguments.Add(Globals.ACTIVE_CL.ToString());
                    arguments.Add(fullPath);
                }

                OutputWindow.appendOutput("Add/Edit " + fullPath + " [" + Globals.ACTIVE_CL + "]", OutputWindow.MessageType.IMPORTANT);

                m_p4.Run("add", false, arguments.ToArray());
                m_p4.Run("edit", false, arguments.ToArray());
            }
            catch (System.Exception ex)
            {
                OutputWindow.appendOutput(ex.Message,OutputWindow.MessageType.ERROR);
            }
        }

        #endregion
    }
}
