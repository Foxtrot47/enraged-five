﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LumenWorks.Framework.IO.Csv;
using System.Windows.Forms;
using System.Drawing;

namespace PixCaptureAnalyzer
{
    public class PixAnalyzer
    {
        public PixAnalyzer()
        {
            m_timings = new Dictionary<string, PixTiming>[2];
            m_timings[0] = new Dictionary<string, PixTiming>();
            m_timings[1] = new Dictionary<string, PixTiming>();
        }

        // Dictionary of timings based on their string paths
        Dictionary<String, PixTiming>[] m_timings;

        // Class that holds all of the timing information for a single timing "path" - all of the cpu durations for that path across the capture
        public class PixTiming
        {
            public PixTiming(String path)
            {
                m_timingPath = path;
                string[] pathSplit = path.Split('/');
                m_name = pathSplit[pathSplit.Length - 1];

                m_avgCPUDuration = Double.NaN;
                m_minCPUDuration = Double.NaN;
                m_maxCPUDuration = Double.NaN;
                m_cpuDurations = new List<Double>();
            }

            // Fill a row in our data set based on this timing
            public void FillRow(TimingDataSet.TimingsRow row)
            {
                row.Name = Name;
                row.Path = TimingPath;
                row.Min = MinCPUDuration;
                row.Max = MaxCPUDuration;
                row.Average =AvgCPUDuration;
                row.Count = Count;
                row.Delta = Double.NaN;
            }

            private String m_name;
            public String Name
            {
                get { return m_name; }
            }

            private String m_timingPath;
            public String TimingPath
            {
                get { return m_timingPath; }
                set { m_timingPath = value; }
            }            

            private Double m_avgCPUDuration;
            public  Double AvgCPUDuration
            {
                get { return m_avgCPUDuration; }
                set { m_avgCPUDuration = value; }
            }

            private Double m_minCPUDuration;
            public Double MinCPUDuration
            {
                get { return m_minCPUDuration; }
            }

            public Double m_maxCPUDuration;
            public Double MaxCPUDuration
            {
                get { return m_maxCPUDuration; }
            }

            public Int32 Count
            {
                get { return m_cpuDurations.Count; }
            }

            private List<Double> m_cpuDurations;
            public void AddCPUDuration(Double duration)
            {
                m_cpuDurations.Add(duration);
            }

            // Calculate aggregate stats for this timing
            public void CalculateStats()
            {
                Double sum = 0;
                Double count = 0;
                foreach (Double duration in m_cpuDurations)
                {
                    if (Double.IsNaN(m_minCPUDuration) || m_minCPUDuration > duration)
                        m_minCPUDuration = duration;
                    if (Double.IsNaN(m_maxCPUDuration) || m_maxCPUDuration < duration)
                        m_maxCPUDuration = duration;
                    sum += duration;
                    count++;
                }
                m_avgCPUDuration = sum / count;
            }
        }
    
        // Load timings from a csv file
        public bool LoadTimings(String csvPath, int timingIndex)
        {
            try
            {
                CsvReader reader = new CsvReader(new System.IO.StreamReader(csvPath), true);

                String[] headers = reader.GetFieldHeaders(); // get the headers - we don't care about them right now
                while (reader.ReadNextRecord())
                {
                    String path = reader[0]; // the path is the first column;
                    Double duration = Double.Parse(reader[2]); // CPU duration is the third column

                    // find the path and add the duration
                    if (!m_timings[timingIndex].ContainsKey(path))
                    {
                        m_timings[timingIndex].Add(path, new PixTiming(path));
                    }
                    m_timings[timingIndex][path].AddCPUDuration(duration);                    
                }

                CalculateAllStats(timingIndex);
            }
            catch (System.Exception ex)
            {
                return false;
            }
            return true;
        }

        // Calculate stats for each timing
        private void CalculateAllStats(int timingIndex)
        {
            foreach (PixTiming timing in m_timings[timingIndex].Values)
            {
                timing.CalculateStats();
            }
        }

        // Fill a data table in our data set from a set of timings
        public void FillDataTable(TimingDataSet.TimingsDataTable dataTable, int timingIndex)
        {
            foreach(PixTiming timing in m_timings[timingIndex].Values)
            {
                TimingDataSet.TimingsRow row = dataTable.NewTimingsRow();

                timing.FillRow(row);                

                dataTable.AddTimingsRow(row);
            }
        }

        // Fill the output table by comparing the two sets of timings, taking the threshold into account
        public void CompareTimingsWithThreshold(Double threshold, TimingDataSet.TimingsDataTable outputDataTable)
        {
            foreach(PixTiming timing1 in m_timings[0].Values)
            {
                if(m_timings[1].ContainsKey(timing1.TimingPath))
                {
                    if(Math.Abs(timing1.AvgCPUDuration - m_timings[1][timing1.TimingPath].AvgCPUDuration) > threshold)
                    {
                        TimingDataSet.TimingsRow row = outputDataTable.NewTimingsRow();

                        // we want the second timing
                        m_timings[1][timing1.TimingPath].FillRow(row);
                        row.Delta = Math.Abs(timing1.AvgCPUDuration - m_timings[1][timing1.TimingPath].AvgCPUDuration);

                        outputDataTable.AddTimingsRow(row);
                    }
                }
            }
        }


        private TreeNode FindParent(TreeView tree, String parentID)
        {
            // Post: call TraverseParent method to search parent

            TreeNodeCollection collection = tree.Nodes;

            // Search parent recursively
            foreach (TreeNode n in collection)
            {
                TreeNode node = TraverseParent(n, parentID);
                if (node != null)
                    return node;
            }

            return null;
        }

        private TreeNode TraverseParent(TreeNode potentialParent, String parentID)
        {
            // Post: search for parent. When parent is found add child to parent

            // am i the parent that you're looking for?
            if (parentID.CompareTo(potentialParent.Name) == 0)
            {
                // found me! i'm your parent!

                // add Child to parent
                return potentialParent;
            }
            else
            {
                // i'm not your parent

                // continue to look for parent recursively
                foreach (TreeNode n in potentialParent.Nodes)
                {
                    TreeNode node = TraverseParent(n, parentID);
                    if (node != null)
                        return node;
                }

                return null;
            }
        }

        //Create a tree based on the difference between TimingSet1 and TimingSet2.  Each branch contain the name
        // of the system and also include the differences in us.  If timingSet2 is slower than timingSet1 the system name will be 
        // drawn using red color.  Otherwise it stay black.
        //
        public void CreateTree(TreeView tree, Double threshold)
        {
            //Clear our tree
            tree.Nodes.Clear();

            foreach (PixTiming timing1 in m_timings[0].Values)
            {
                //Add this one in our tree if both table has it
                if (m_timings[1].ContainsKey(timing1.TimingPath))
                {
                    //If the slow down is too small, discard
                    if (Math.Abs(timing1.AvgCPUDuration - m_timings[1][timing1.TimingPath].AvgCPUDuration) < threshold)
                        continue;

                    //This is the string containing our hirearchy
                    string currentString = timing1.TimingPath;
                   
                    //Iterate as long as we find sub-string (hirerarchy).  A substring start with '/'
                    int first = 0;
                    TreeNode parentNode = null;
                    while ((first = currentString.IndexOf('/', first)) != -1)
                    {
                        //Get the CPU diff between both pix capture and set the string color based on that
                        Double cpuDuration = (int)m_timings[1][timing1.TimingPath].AvgCPUDuration - (int)timing1.AvgCPUDuration;
                        Color stringColor;
                        if (cpuDuration > 0)
                            stringColor = Color.Red;
                        else
                            stringColor = Color.Black;

                        //Get the sub-string
                        int last = currentString.IndexOf('/', first + 1);
                        string subString;
                        string subStringText;
                        if (last != -1)
                        {
                            subString = currentString.Substring(first + 1, (last - 1) - first);
                        }
                        else
                        {
                            subString = currentString.Substring(first + 1);
                        }
                        //Add the cpu average to the string
                        subStringText = subString;
                        subStringText += "  ...  ";
                        subStringText += cpuDuration.ToString();

                        //Add
                        if (parentNode == null)
                        {
                            TreeNode parent = FindParent(tree, subString);
                            if (parent == null)
                            {
                                TreeNode treeNode = new TreeNode(subString);
                                treeNode.Name = subString;
                                tree.Nodes.Add(treeNode);
                                parent = FindParent(tree, subString);
                            }
                            parentNode = parent;
                        }
                        else
                        {
                            TreeNode parent = TraverseParent(parentNode, subString);
                            if (parent == null)
                            {
                                TreeNode treeNode = new TreeNode(subString);
                                treeNode.Name = subString;
                                parentNode.Nodes.Add(treeNode);
                                parentNode = TraverseParent(parentNode, subString);
                            }
                            else
                            {
                                parentNode = parent;
                            }
                        }

                        //Set the color and cpu duration on the last titem only - that way we know we have the total cpu average for this branch
                        if (last == -1)
                        {
                            parentNode.Text = subStringText;
                            parentNode.ForeColor = stringColor;
                        }

                        first++;
                    }
                }
            }
        }
    }
}
