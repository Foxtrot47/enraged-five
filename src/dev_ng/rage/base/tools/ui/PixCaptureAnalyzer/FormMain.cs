﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using LumenWorks.Framework.IO.Csv;
using System.IO;
using System.Xml.Linq;

namespace PixCaptureAnalyzer
{
    public partial class FormMain : Form
    {
        PixAnalyzer m_Analyzer;
        public FormMain()
        {
            InitializeComponent();
            m_Analyzer = new PixAnalyzer();
        }

        // Clear the output and run the analysis
        private void btnRunAnalysis_Click(object sender, EventArgs e)
        {           
            timingDataSetOuput.Clear();

            m_Analyzer.CreateTree(treeView1, Double.Parse(timeThreshold.Value.ToString()));

            m_Analyzer.CompareTimingsWithThreshold(Double.Parse(timeThreshold.Value.ToString()), timingDataSetOuput.Timings);
        }

        private void LoadTiming1(String csvPath)
        {
            timingDataSet1.Clear();
            timingBox1.Text = csvPath;
            if (m_Analyzer.LoadTimings(csvPath, 0))
            {
                m_Analyzer.FillDataTable(timingDataSet1.Timings, 0);
            }            
        }

        private void LoadTiming2(String csvPath)
        {
            timingDataSet2.Clear();
            timingBox2.Text = csvPath;
            if (m_Analyzer.LoadTimings(csvPath, 1))
            {
                m_Analyzer.FillDataTable(timingDataSet2.Timings, 1);
            }
        }
    
        // Load first timing file
        private void button1_Click(object sender, EventArgs e)
        {
            if(openTimingFile.ShowDialog() == DialogResult.OK)
            {
                textTimingPath1.Text = openTimingFile.FileName;
            }
            LoadTiming1(openTimingFile.FileName);
        }

        // Load second timing file
        private void button2_Click(object sender, EventArgs e)
        {
            if (openTimingFile.ShowDialog() == DialogResult.OK)
            {
                textTimingPath2.Text = openTimingFile.FileName;
            }
            LoadTiming2(openTimingFile.FileName);
        }

        private void dataGridTimings1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dataGridTimings2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {

        }

        

        private void label4_Click(object sender, EventArgs e)
        {
            
        }

        //Export tree
        private void button3_Click_1(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();

            sfd.Filter = "Word Documents (*.xls)|*.xls";

            sfd.FileName = "export.xls";

            if (sfd.ShowDialog() == DialogResult.OK)
            {
                //ToCsV(dataGridOutput, @"c:\export.xls");
               ToCsV(dataGridOutput, sfd.FileName);
            } 
        }
        
        private void ToCsV(DataGridView dGV, string filename)
        {
            string stOutput = "";
            // Export titles:
            string sHeaders = "";

            for (int j = 0; j < dGV.Columns.Count; j++)
                sHeaders = sHeaders.ToString() + Convert.ToString(dGV.Columns[j].HeaderText) + "\t";
            stOutput += sHeaders + "\r\n";
            // Export data.
            for (int i = 0; i < dGV.RowCount - 1; i++)
            {
                string stLine = "";
                for (int j = 0; j < dGV.Rows[i].Cells.Count; j++)
                    stLine = stLine.ToString() + Convert.ToString(dGV.Rows[i].Cells[j].Value) + "\t";
                stOutput += stLine + "\r\n";
            }
            Encoding utf16 = Encoding.GetEncoding(1254);
            byte[] output = utf16.GetBytes(stOutput);
            FileStream fs = new FileStream(filename, FileMode.Create);
            BinaryWriter bw = new BinaryWriter(fs);
            bw.Write(output, 0, output.Length); //write the encoded file
            bw.Flush();
            bw.Close();
            fs.Close();
        } 
    }
}
