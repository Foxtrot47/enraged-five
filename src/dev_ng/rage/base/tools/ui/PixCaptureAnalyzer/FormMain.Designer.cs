﻿namespace PixCaptureAnalyzer
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnRunAnalysis = new System.Windows.Forms.Button();
            this.dataGridTimings1 = new System.Windows.Forms.DataGridView();
            this.nameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pathDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.averageDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.minDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.maxDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.countDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.timingsBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.timingDataSet1 = new PixCaptureAnalyzer.TimingDataSet();
            this.dataGridTimings2 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.timingsBindingSource2 = new System.Windows.Forms.BindingSource(this.components);
            this.timingDataSet2 = new PixCaptureAnalyzer.TimingDataSet();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.timingBox1 = new System.Windows.Forms.GroupBox();
            this.timingBox2 = new System.Windows.Forms.GroupBox();
            this.timeThreshold = new System.Windows.Forms.NumericUpDown();
            this.timingOutput = new System.Windows.Forms.GroupBox();
            this.dataGridOutput = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Delta = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.timingBindingSourceOuput = new System.Windows.Forms.BindingSource(this.components);
            this.timingDataSetOuput = new PixCaptureAnalyzer.TimingDataSet();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.textTimingPath2 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textTimingPath1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.splitContainer3 = new System.Windows.Forms.SplitContainer();
            this.Tree = new System.Windows.Forms.GroupBox();
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.openTimingFile = new System.Windows.Forms.OpenFileDialog();
            this.button3 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridTimings1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.timingsBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.timingDataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridTimings2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.timingsBindingSource2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.timingDataSet2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.timingBox1.SuspendLayout();
            this.timingBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.timeThreshold)).BeginInit();
            this.timingOutput.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridOutput)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.timingBindingSourceOuput)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.timingDataSetOuput)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).BeginInit();
            this.splitContainer3.Panel1.SuspendLayout();
            this.splitContainer3.Panel2.SuspendLayout();
            this.splitContainer3.SuspendLayout();
            this.Tree.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnRunAnalysis
            // 
            this.btnRunAnalysis.Location = new System.Drawing.Point(161, 61);
            this.btnRunAnalysis.Name = "btnRunAnalysis";
            this.btnRunAnalysis.Size = new System.Drawing.Size(74, 20);
            this.btnRunAnalysis.TabIndex = 0;
            this.btnRunAnalysis.Text = "Run";
            this.btnRunAnalysis.UseVisualStyleBackColor = true;
            this.btnRunAnalysis.Click += new System.EventHandler(this.btnRunAnalysis_Click);
            // 
            // dataGridTimings1
            // 
            this.dataGridTimings1.AllowUserToAddRows = false;
            this.dataGridTimings1.AllowUserToDeleteRows = false;
            this.dataGridTimings1.AutoGenerateColumns = false;
            this.dataGridTimings1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridTimings1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.nameDataGridViewTextBoxColumn,
            this.pathDataGridViewTextBoxColumn,
            this.averageDataGridViewTextBoxColumn,
            this.minDataGridViewTextBoxColumn,
            this.maxDataGridViewTextBoxColumn,
            this.countDataGridViewTextBoxColumn});
            this.dataGridTimings1.DataSource = this.timingsBindingSource1;
            this.dataGridTimings1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridTimings1.Location = new System.Drawing.Point(3, 16);
            this.dataGridTimings1.Name = "dataGridTimings1";
            this.dataGridTimings1.ReadOnly = true;
            this.dataGridTimings1.Size = new System.Drawing.Size(195, 661);
            this.dataGridTimings1.TabIndex = 1;
            this.dataGridTimings1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridTimings1_CellContentClick);
            // 
            // nameDataGridViewTextBoxColumn
            // 
            this.nameDataGridViewTextBoxColumn.DataPropertyName = "Name";
            this.nameDataGridViewTextBoxColumn.HeaderText = "Name";
            this.nameDataGridViewTextBoxColumn.Name = "nameDataGridViewTextBoxColumn";
            this.nameDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // pathDataGridViewTextBoxColumn
            // 
            this.pathDataGridViewTextBoxColumn.DataPropertyName = "Path";
            this.pathDataGridViewTextBoxColumn.HeaderText = "Path";
            this.pathDataGridViewTextBoxColumn.Name = "pathDataGridViewTextBoxColumn";
            this.pathDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // averageDataGridViewTextBoxColumn
            // 
            this.averageDataGridViewTextBoxColumn.DataPropertyName = "Average";
            this.averageDataGridViewTextBoxColumn.HeaderText = "Average";
            this.averageDataGridViewTextBoxColumn.Name = "averageDataGridViewTextBoxColumn";
            this.averageDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // minDataGridViewTextBoxColumn
            // 
            this.minDataGridViewTextBoxColumn.DataPropertyName = "Min";
            this.minDataGridViewTextBoxColumn.HeaderText = "Min";
            this.minDataGridViewTextBoxColumn.Name = "minDataGridViewTextBoxColumn";
            this.minDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // maxDataGridViewTextBoxColumn
            // 
            this.maxDataGridViewTextBoxColumn.DataPropertyName = "Max";
            this.maxDataGridViewTextBoxColumn.HeaderText = "Max";
            this.maxDataGridViewTextBoxColumn.Name = "maxDataGridViewTextBoxColumn";
            this.maxDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // countDataGridViewTextBoxColumn
            // 
            this.countDataGridViewTextBoxColumn.DataPropertyName = "Count";
            this.countDataGridViewTextBoxColumn.HeaderText = "Count";
            this.countDataGridViewTextBoxColumn.Name = "countDataGridViewTextBoxColumn";
            this.countDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // timingsBindingSource1
            // 
            this.timingsBindingSource1.DataMember = "Timings";
            this.timingsBindingSource1.DataSource = this.timingDataSet1;
            // 
            // timingDataSet1
            // 
            this.timingDataSet1.DataSetName = "TimingDataSet";
            this.timingDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // dataGridTimings2
            // 
            this.dataGridTimings2.AllowUserToAddRows = false;
            this.dataGridTimings2.AllowUserToDeleteRows = false;
            this.dataGridTimings2.AutoGenerateColumns = false;
            this.dataGridTimings2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridTimings2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn3});
            this.dataGridTimings2.DataSource = this.timingsBindingSource2;
            this.dataGridTimings2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridTimings2.Location = new System.Drawing.Point(3, 16);
            this.dataGridTimings2.Name = "dataGridTimings2";
            this.dataGridTimings2.ReadOnly = true;
            this.dataGridTimings2.Size = new System.Drawing.Size(204, 661);
            this.dataGridTimings2.TabIndex = 2;
            this.dataGridTimings2.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridTimings2_CellContentClick);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "Name";
            this.dataGridViewTextBoxColumn1.HeaderText = "Name";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "Path";
            this.dataGridViewTextBoxColumn2.HeaderText = "Path";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "Average";
            this.dataGridViewTextBoxColumn4.HeaderText = "Average";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "Min";
            this.dataGridViewTextBoxColumn5.HeaderText = "Min";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "Max";
            this.dataGridViewTextBoxColumn6.HeaderText = "Max";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "Count";
            this.dataGridViewTextBoxColumn3.HeaderText = "Count";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            // 
            // timingsBindingSource2
            // 
            this.timingsBindingSource2.DataMember = "Timings";
            this.timingsBindingSource2.DataSource = this.timingDataSet2;
            // 
            // timingDataSet2
            // 
            this.timingDataSet2.DataSetName = "TimingDataSet";
            this.timingDataSet2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 112);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.timingBox1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.timingBox2);
            this.splitContainer1.Size = new System.Drawing.Size(415, 680);
            this.splitContainer1.SplitterDistance = 201;
            this.splitContainer1.TabIndex = 3;
            // 
            // timingBox1
            // 
            this.timingBox1.Controls.Add(this.dataGridTimings1);
            this.timingBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.timingBox1.Location = new System.Drawing.Point(0, 0);
            this.timingBox1.Name = "timingBox1";
            this.timingBox1.Size = new System.Drawing.Size(201, 680);
            this.timingBox1.TabIndex = 3;
            this.timingBox1.TabStop = false;
            this.timingBox1.Text = "Timing Set 1  (perf in us)";
            // 
            // timingBox2
            // 
            this.timingBox2.Controls.Add(this.dataGridTimings2);
            this.timingBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.timingBox2.Location = new System.Drawing.Point(0, 0);
            this.timingBox2.Name = "timingBox2";
            this.timingBox2.Size = new System.Drawing.Size(210, 680);
            this.timingBox2.TabIndex = 4;
            this.timingBox2.TabStop = false;
            this.timingBox2.Text = "Timing Set 2  (perf in us)";
            // 
            // timeThreshold
            // 
            this.timeThreshold.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.timeThreshold.Location = new System.Drawing.Point(75, 61);
            this.timeThreshold.Maximum = new decimal(new int[] {
            2000,
            0,
            0,
            0});
            this.timeThreshold.Name = "timeThreshold";
            this.timeThreshold.Size = new System.Drawing.Size(80, 20);
            this.timeThreshold.TabIndex = 1;
            // 
            // timingOutput
            // 
            this.timingOutput.Controls.Add(this.dataGridOutput);
            this.timingOutput.Dock = System.Windows.Forms.DockStyle.Fill;
            this.timingOutput.Location = new System.Drawing.Point(0, 0);
            this.timingOutput.Name = "timingOutput";
            this.timingOutput.Size = new System.Drawing.Size(283, 792);
            this.timingOutput.TabIndex = 5;
            this.timingOutput.TabStop = false;
            this.timingOutput.Text = "Output Table  (perf in us)";
            // 
            // dataGridOutput
            // 
            this.dataGridOutput.AllowUserToAddRows = false;
            this.dataGridOutput.AllowUserToDeleteRows = false;
            this.dataGridOutput.AutoGenerateColumns = false;
            this.dataGridOutput.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridOutput.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn8,
            this.dataGridViewTextBoxColumn9,
            this.Delta,
            this.dataGridViewTextBoxColumn10,
            this.dataGridViewTextBoxColumn11,
            this.dataGridViewTextBoxColumn12});
            this.dataGridOutput.DataSource = this.timingBindingSourceOuput;
            this.dataGridOutput.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridOutput.Location = new System.Drawing.Point(3, 16);
            this.dataGridOutput.Name = "dataGridOutput";
            this.dataGridOutput.ReadOnly = true;
            this.dataGridOutput.Size = new System.Drawing.Size(277, 773);
            this.dataGridOutput.TabIndex = 2;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "Name";
            this.dataGridViewTextBoxColumn7.HeaderText = "Name";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.DataPropertyName = "Path";
            this.dataGridViewTextBoxColumn8.HeaderText = "Path";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.DataPropertyName = "Average";
            this.dataGridViewTextBoxColumn9.HeaderText = "Average";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.ReadOnly = true;
            // 
            // Delta
            // 
            this.Delta.DataPropertyName = "Delta";
            this.Delta.HeaderText = "Delta";
            this.Delta.Name = "Delta";
            this.Delta.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.DataPropertyName = "Min";
            this.dataGridViewTextBoxColumn10.HeaderText = "Min";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.DataPropertyName = "Max";
            this.dataGridViewTextBoxColumn11.HeaderText = "Max";
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            this.dataGridViewTextBoxColumn11.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn12
            // 
            this.dataGridViewTextBoxColumn12.DataPropertyName = "Count";
            this.dataGridViewTextBoxColumn12.HeaderText = "Count";
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            this.dataGridViewTextBoxColumn12.ReadOnly = true;
            // 
            // timingBindingSourceOuput
            // 
            this.timingBindingSourceOuput.DataMember = "Timings";
            this.timingBindingSourceOuput.DataSource = this.timingDataSetOuput;
            // 
            // timingDataSetOuput
            // 
            this.timingDataSetOuput.DataSetName = "TimingDataSet";
            this.timingDataSetOuput.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.button3);
            this.groupBox1.Controls.Add(this.button2);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.textTimingPath2);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.textTimingPath1);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.timeThreshold);
            this.groupBox1.Controls.Add(this.btnRunAnalysis);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(415, 112);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(377, 35);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(26, 23);
            this.button2.TabIndex = 8;
            this.button2.Text = "...";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(377, 9);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(26, 23);
            this.button1.TabIndex = 7;
            this.button1.Text = "...";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(15, 40);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(50, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Timing 2:";
            // 
            // textTimingPath2
            // 
            this.textTimingPath2.Location = new System.Drawing.Point(71, 37);
            this.textTimingPath2.Name = "textTimingPath2";
            this.textTimingPath2.Size = new System.Drawing.Size(300, 20);
            this.textTimingPath2.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 14);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Timing 1:";
            // 
            // textTimingPath1
            // 
            this.textTimingPath1.Location = new System.Drawing.Point(71, 11);
            this.textTimingPath1.Name = "textTimingPath1";
            this.textTimingPath1.Size = new System.Drawing.Size(300, 20);
            this.textTimingPath1.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 63);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Threshold";
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.splitContainer1);
            this.splitContainer2.Panel1.Controls.Add(this.groupBox1);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.splitContainer3);
            this.splitContainer2.Size = new System.Drawing.Size(1279, 792);
            this.splitContainer2.SplitterDistance = 415;
            this.splitContainer2.TabIndex = 0;
            // 
            // splitContainer3
            // 
            this.splitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer3.Location = new System.Drawing.Point(0, 0);
            this.splitContainer3.Name = "splitContainer3";
            // 
            // splitContainer3.Panel1
            // 
            this.splitContainer3.Panel1.Controls.Add(this.timingOutput);
            // 
            // splitContainer3.Panel2
            // 
            this.splitContainer3.Panel2.Controls.Add(this.Tree);
            this.splitContainer3.Size = new System.Drawing.Size(860, 792);
            this.splitContainer3.SplitterDistance = 283;
            this.splitContainer3.TabIndex = 6;
            // 
            // Tree
            // 
            this.Tree.Controls.Add(this.treeView1);
            this.Tree.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Tree.Location = new System.Drawing.Point(0, 0);
            this.Tree.Name = "Tree";
            this.Tree.Size = new System.Drawing.Size(573, 792);
            this.Tree.TabIndex = 0;
            this.Tree.TabStop = false;
            this.Tree.Text = "Output Tree (perf in us) - Red = Timing 2 slower -- Black = Timing 2 faster";
            this.Tree.Enter += new System.EventHandler(this.groupBox2_Enter);
            // 
            // treeView1
            // 
            this.treeView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeView1.Location = new System.Drawing.Point(3, 16);
            this.treeView1.Name = "treeView1";
            this.treeView1.Size = new System.Drawing.Size(567, 773);
            this.treeView1.TabIndex = 0;
            this.treeView1.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treeView1_AfterSelect);
            // 
            // openTimingFile
            // 
            this.openTimingFile.FileName = "openTimingFile";
            this.openTimingFile.Filter = "csv files (*.csv)|*.csv";
            this.openTimingFile.Title = "Open Timing File";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(18, 83);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(86, 23);
            this.button3.TabIndex = 9;
            this.button3.Text = "Export Output";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click_1);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1279, 792);
            this.Controls.Add(this.splitContainer2);
            this.Name = "FormMain";
            this.Text = "Pix Capture Analyzer";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridTimings1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.timingsBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.timingDataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridTimings2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.timingsBindingSource2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.timingDataSet2)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.timingBox1.ResumeLayout(false);
            this.timingBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.timeThreshold)).EndInit();
            this.timingOutput.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridOutput)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.timingBindingSourceOuput)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.timingDataSetOuput)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.splitContainer3.Panel1.ResumeLayout(false);
            this.splitContainer3.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).EndInit();
            this.splitContainer3.ResumeLayout(false);
            this.Tree.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnRunAnalysis;
        private TimingDataSet timingDataSet1;
        private System.Windows.Forms.DataGridView dataGridTimings1;
        private System.Windows.Forms.BindingSource timingsBindingSource1;
        private TimingDataSet timingDataSet2;
        private System.Windows.Forms.DataGridView dataGridTimings2;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.GroupBox timingBox2;
        private System.Windows.Forms.GroupBox timingBox1;
        private System.Windows.Forms.BindingSource timingsBindingSource2;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn pathDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn averageDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn minDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn maxDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn countDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.NumericUpDown timeThreshold;
        private System.Windows.Forms.GroupBox timingOutput;
        private System.Windows.Forms.DataGridView dataGridOutput;
        private System.Windows.Forms.BindingSource timingBindingSourceOuput;
        private TimingDataSet timingDataSetOuput;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Delta;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textTimingPath1;
        private System.Windows.Forms.OpenFileDialog openTimingFile;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textTimingPath2;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.SplitContainer splitContainer3;
        private System.Windows.Forms.GroupBox Tree;
        private System.Windows.Forms.TreeView treeView1;
        private System.Windows.Forms.Button button3;
    }
}

