using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

namespace Rockstar.TargetManager
{
    public partial class AboutDialog : Form
    {
        public AboutDialog()
        {
            InitializeComponent();
        }

        private void AboutDialog_Load(object sender, EventArgs e)
        {
            Assembly asm = Assembly.GetExecutingAssembly();
            versionText.Text = asm.GetName().Version.ToString();
            foreach(Attribute attr in Attribute.GetCustomAttributes(asm))
            {
                if(attr.GetType() == typeof(AssemblyTitleAttribute))
                {
                    this.Text = "About " + ((AssemblyTitleAttribute)attr).Title;
                }
                else if(attr.GetType() == typeof(AssemblyCopyrightAttribute))
                {
                    copywriteText.Text = ((AssemblyCopyrightAttribute)attr).Copyright;
                }
            }
        }
    }
}