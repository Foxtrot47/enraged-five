﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Rockstar.TargetManager
{
	////////////////////////////////////////////////////////////////////////////
	// LogCleanupWindow
	////////////////////////////////////////////////////////////////////////////
	public partial class LogCleanupWindow : Form
	{
		const uint MAX_DAYS_OLD_PLUS_ONE = 64;

		private struct Item
		{
			public string filename;
			public bool   isChecked;
		};
		private List<Item>[] m_tmpItemsPerDaysOld;
		private Item[][] m_itemsPerDaysOld;
		private uint[] m_numItemsOlderThan;

		private uint m_daysOld = MAX_DAYS_OLD_PLUS_ONE;


		////////////////////////////////////////////////////////////////////////
		// LogCleanupWindow
		////////////////////////////////////////////////////////////////////////
		public LogCleanupWindow()
		{
			InitializeComponent();

			m_tmpItemsPerDaysOld = new List<Item>[MAX_DAYS_OLD_PLUS_ONE];
			for(uint i=0; i<MAX_DAYS_OLD_PLUS_ONE; ++i)
			{
				m_tmpItemsPerDaysOld[i] = new List<Item>();
			}

			m_numItemsOlderThan = new uint[MAX_DAYS_OLD_PLUS_ONE];
		}

		////////////////////////////////////////////////////////////////////////
		// trackBarDays_Scroll
		////////////////////////////////////////////////////////////////////////
		private void trackBarDays_Scroll(object sender, EventArgs e)
		{
			UpdateDaysOld((uint)trackBarDays.Value);
		}

		////////////////////////////////////////////////////////////////////////
		// UpdateDaysOld
		////////////////////////////////////////////////////////////////////////
		private void UpdateDaysOld(uint daysOld)
		{
			textBoxDays.Text = daysOld.ToString();

			daysOld = Math.Min(daysOld, (uint)(m_itemsPerDaysOld.Length-1));
			if(m_daysOld == daysOld)
			{
				return;
			}

			uint prevDaysOld = m_daysOld;
			m_daysOld = daysOld;

			checkedListBoxFiles.BeginUpdate();

			// If we are increasing the threshold, then we need to remove items from checkedListBoxFiles
			if(prevDaysOld < daysOld)
			{
				// Remove the youngest first, since they will be at the end of the list
				for(uint age=prevDaysOld; age<daysOld; ++age)
				{
					uint numAgeItems   = (uint)m_itemsPerDaysOld[age].Length;
					uint numOlderItems = m_numItemsOlderThan[age];
					uint numTotalItems = numOlderItems + numAgeItems;
					Debug.Assert(numTotalItems == (uint)checkedListBoxFiles.Items.Count);

					for(uint i=numTotalItems; i-->numOlderItems;)
					{
						m_itemsPerDaysOld[age][i-numOlderItems].isChecked = checkedListBoxFiles.GetItemChecked((int)i);
						checkedListBoxFiles.Items.RemoveAt((int)i);
					}
				}
			}

			// Else we are decreasing the threshold, so adding items to checkedListBoxFiles
			else
			{
				// Add the eldest ones first (ie, reverse of the remove order)
				for(uint age=prevDaysOld; age-->daysOld;)
				{
					uint numAgeItems   = (uint)m_itemsPerDaysOld[age].Length;
					uint numOlderItems = m_numItemsOlderThan[age];
					Debug.Assert(numOlderItems == (uint)checkedListBoxFiles.Items.Count);

					for(uint i=0; i<numAgeItems; ++i)
					{
						checkedListBoxFiles.Items.Add(m_itemsPerDaysOld[age][i].filename);
						checkedListBoxFiles.SetItemChecked((int)(i+numOlderItems), m_itemsPerDaysOld[age][i].isChecked);
					}
				}
			}

			checkedListBoxFiles.EndUpdate();
		}

		////////////////////////////////////////////////////////////////////////
		// AddFile
		////////////////////////////////////////////////////////////////////////
		public void AddFile(string filename, uint daysOld)
		{
			daysOld = Math.Min(daysOld, (uint)(m_tmpItemsPerDaysOld.Length-1));
			var item = new Item();
			item.filename  = filename;
			item.isChecked = true;
			m_tmpItemsPerDaysOld[daysOld].Add(item);
		}

		////////////////////////////////////////////////////////////////////////
		// EndAddingFiles
		////////////////////////////////////////////////////////////////////////
		public void EndAddingFiles()
		{
			m_itemsPerDaysOld = new Item[MAX_DAYS_OLD_PLUS_ONE][];
			uint count = 0;
			for(uint i=(uint)m_numItemsOlderThan.Length; i-->0;)
			{
				m_itemsPerDaysOld[i] = m_tmpItemsPerDaysOld[i].ToArray();
				m_numItemsOlderThan[i] = count;
				count += (uint)m_itemsPerDaysOld[i].Length;
			}
			m_tmpItemsPerDaysOld = null;
		}

		////////////////////////////////////////////////////////////////////////
		// SetInitialDaysOldThreshold
		////////////////////////////////////////////////////////////////////////
		public bool SetInitialDaysOldThreshold(uint daysOld)
		{
			trackBarDays.Value = (int)daysOld;
			UpdateDaysOld(daysOld);
			return checkedListBoxFiles.Items.Count > 0;
		}

		////////////////////////////////////////////////////////////////////////
		// GetCheckedFiles
		////////////////////////////////////////////////////////////////////////
		public List<string> GetCheckedFiles()
		{
			var files = new List<string>();
			foreach(var i in checkedListBoxFiles.CheckedItems)
			{
				files.Add(i.ToString());
			}
			return files;
		}
	}
}
