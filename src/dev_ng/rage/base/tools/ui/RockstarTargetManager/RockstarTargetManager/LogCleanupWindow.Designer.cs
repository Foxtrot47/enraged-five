﻿namespace Rockstar.TargetManager
{
	partial class LogCleanupWindow
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LogCleanupWindow));
			this.checkedListBoxFiles = new System.Windows.Forms.CheckedListBox();
			this.labelDescription = new System.Windows.Forms.Label();
			this.buttonCancel = new System.Windows.Forms.Button();
			this.buttonOk = new System.Windows.Forms.Button();
			this.trackBarDays = new System.Windows.Forms.TrackBar();
			this.textBoxDays = new System.Windows.Forms.TextBox();
			this.labelDays = new System.Windows.Forms.Label();
			((System.ComponentModel.ISupportInitialize)(this.trackBarDays)).BeginInit();
			this.SuspendLayout();
			// 
			// checkedListBoxFiles
			// 
			this.checkedListBoxFiles.CheckOnClick = true;
			this.checkedListBoxFiles.FormattingEnabled = true;
			this.checkedListBoxFiles.Location = new System.Drawing.Point(12, 96);
			this.checkedListBoxFiles.Name = "checkedListBoxFiles";
			this.checkedListBoxFiles.Size = new System.Drawing.Size(583, 289);
			this.checkedListBoxFiles.TabIndex = 0;
			// 
			// labelDescription
			// 
			this.labelDescription.AutoSize = true;
			this.labelDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.labelDescription.Location = new System.Drawing.Point(15, 19);
			this.labelDescription.Name = "labelDescription";
			this.labelDescription.Size = new System.Drawing.Size(309, 13);
			this.labelDescription.TabIndex = 1;
			this.labelDescription.Text = "The following files are old, select those to be deleted";
			// 
			// buttonCancel
			// 
			this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.buttonCancel.Location = new System.Drawing.Point(379, 403);
			this.buttonCancel.Name = "buttonCancel";
			this.buttonCancel.Size = new System.Drawing.Size(105, 27);
			this.buttonCancel.TabIndex = 2;
			this.buttonCancel.Text = "&Cancel";
			this.buttonCancel.UseVisualStyleBackColor = true;
			// 
			// buttonOk
			// 
			this.buttonOk.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.buttonOk.Location = new System.Drawing.Point(490, 403);
			this.buttonOk.Name = "buttonOk";
			this.buttonOk.Size = new System.Drawing.Size(105, 27);
			this.buttonOk.TabIndex = 1;
			this.buttonOk.Text = "&OK";
			this.buttonOk.UseVisualStyleBackColor = true;
			// 
			// trackBarDays
			// 
			this.trackBarDays.Location = new System.Drawing.Point(12, 49);
			this.trackBarDays.Maximum = 31;
			this.trackBarDays.Minimum = 7;
			this.trackBarDays.Name = "trackBarDays";
			this.trackBarDays.Size = new System.Drawing.Size(263, 42);
			this.trackBarDays.TabIndex = 3;
			this.trackBarDays.Value = 14;
			this.trackBarDays.Scroll += new System.EventHandler(this.trackBarDays_Scroll);
			// 
			// textBoxDays
			// 
			this.textBoxDays.Location = new System.Drawing.Point(292, 49);
			this.textBoxDays.Name = "textBoxDays";
			this.textBoxDays.ReadOnly = true;
			this.textBoxDays.Size = new System.Drawing.Size(45, 20);
			this.textBoxDays.TabIndex = 5;
			// 
			// labelDays
			// 
			this.labelDays.AutoSize = true;
			this.labelDays.Location = new System.Drawing.Point(349, 48);
			this.labelDays.Name = "labelDays";
			this.labelDays.Size = new System.Drawing.Size(46, 13);
			this.labelDays.TabIndex = 5;
			this.labelDays.Text = "days old";
			// 
			// LogCleanupWindow
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.CancelButton = this.buttonCancel;
			this.ClientSize = new System.Drawing.Size(607, 442);
			this.Controls.Add(this.labelDays);
			this.Controls.Add(this.textBoxDays);
			this.Controls.Add(this.trackBarDays);
			this.Controls.Add(this.buttonOk);
			this.Controls.Add(this.buttonCancel);
			this.Controls.Add(this.labelDescription);
			this.Controls.Add(this.checkedListBoxFiles);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "LogCleanupWindow";
			this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
			this.Text = "R*TM Log Directory Cleanup";
			this.TopMost = true;
			((System.ComponentModel.ISupportInitialize)(this.trackBarDays)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.CheckedListBox checkedListBoxFiles;
		private System.Windows.Forms.Label labelDescription;
		private System.Windows.Forms.Button buttonCancel;
		private System.Windows.Forms.Button buttonOk;
		private System.Windows.Forms.TrackBar trackBarDays;
		private System.Windows.Forms.TextBox textBoxDays;
		private System.Windows.Forms.Label labelDays;
	}
}
