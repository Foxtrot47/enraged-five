using System;
using System.Windows.Forms;
using System.IO;

namespace Rockstar.TargetManager
{
    internal partial class ScriptConsole:Form
    {
        private string m_OrigTitle = "";
        private ScriptEngine m_ScriptEngine;
        private FileInfo m_Finfo = null;

        public ScriptConsole(ScriptEngine scriptEngine)
        {
            m_ScriptEngine = scriptEngine;

            InitializeComponent();

            m_OrigTitle = this.Text;
        }

        public void LoadFile(string filename)
        {
            FileInfo finfo = new FileInfo(filename);
            if(finfo.Exists)
            {
                this.Text =  finfo.Name + " - " + m_OrigTitle;
                compileButton.Enabled = true;
                runButton.Enabled = true;
                m_Finfo = finfo;
            }
        }

        public void Compile()
        {
            string code = this.LoadCode();
            if(null != code)
            {
                m_ScriptEngine.Compile(code);
            }
        }

        public void Run()
        {
            string code = this.LoadCode();
            if(null != code)
            {
                m_ScriptEngine.Run(code);
            }
        }

        private string LoadCode()
        {
            string code = null;

            if(null != m_Finfo && m_Finfo.Exists)
            {
                FileStream fs = m_Finfo.OpenRead();

                if(null != fs)
                {
                    StreamReader sr = new StreamReader(fs);
                    code = sr.ReadToEnd();
                    sr.Close();
                }
            }

            return code;
        }

        private void loadToolStripMenuItem_Click(object sender,EventArgs e)
        {
            openFileDialog1.Reset();
            openFileDialog1.Title = "Load Script";
            openFileDialog1.Filter = "Script Files (*.rtmsc)|*.rtmsc";
            DialogResult dr = openFileDialog1.ShowDialog(this);

            if(DialogResult.OK == dr)
            {
                this.LoadFile(openFileDialog1.FileName);
            }
        }

        private void compileButton_Click(object sender,EventArgs e)
        {
            this.Compile();
        }

        private void runButton_Click(object sender,EventArgs e)
        {
            this.Run();
        }

        private void ScriptConsole_FormClosing(object sender,FormClosingEventArgs e)
        {
            if(this.Visible && e.CloseReason != CloseReason.FormOwnerClosing)
            {
                e.Cancel = true;
                this.Hide();
            }
        }
    }
}