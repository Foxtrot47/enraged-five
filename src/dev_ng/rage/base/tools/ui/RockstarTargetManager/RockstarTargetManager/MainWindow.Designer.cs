namespace Rockstar.TargetManager {
    partial class MainWindow {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if(disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainWindow));
			this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
			this.imageList1 = new System.Windows.Forms.ImageList(this.components);
			this.panel1 = new System.Windows.Forms.Panel();
			this.splitter1 = new System.Windows.Forms.Splitter();
			this.panel2 = new System.Windows.Forms.Panel();
			this.targetTreeView = new System.Windows.Forms.TreeView();
			this.splitter2 = new System.Windows.Forms.Splitter();
			this.mainTabControl = new System.Windows.Forms.TabControl();
			this.menuStrip1 = new System.Windows.Forms.MenuStrip();
			this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.loadWorkspaceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
			this.saveWorkspaceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.saveWorkspaceAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.reloadSolutionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.scriptToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.consoleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.pluginsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.clearOldLogFilesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.wikiPageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
			this.panel2.SuspendLayout();
			this.menuStrip1.SuspendLayout();
			this.SuspendLayout();
			// 
			// imageList1
			// 
			this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
			this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
			this.imageList1.Images.SetKeyName(0, "file-open-16x16.png");
			this.imageList1.Images.SetKeyName(1, "stop-16x16.png");
			this.imageList1.Images.SetKeyName(2, "launch-16x16.png");
			this.imageList1.Images.SetKeyName(3, "button_cance.ico");
			// 
			// panel1
			// 
			this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panel1.Location = new System.Drawing.Point(0, 558);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(1016, 180);
			this.panel1.TabIndex = 5;
			// 
			// splitter1
			// 
			this.splitter1.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.splitter1.Location = new System.Drawing.Point(0, 555);
			this.splitter1.Name = "splitter1";
			this.splitter1.Size = new System.Drawing.Size(1016, 3);
			this.splitter1.TabIndex = 6;
			this.splitter1.TabStop = false;
			// 
			// panel2
			// 
			this.panel2.Controls.Add(this.targetTreeView);
			this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
			this.panel2.Location = new System.Drawing.Point(0, 24);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(200, 531);
			this.panel2.TabIndex = 7;
			// 
			// targetTreeView
			// 
			this.targetTreeView.CheckBoxes = true;
			this.targetTreeView.Dock = System.Windows.Forms.DockStyle.Fill;
			this.targetTreeView.ImageIndex = 0;
			this.targetTreeView.ImageList = this.imageList1;
			this.targetTreeView.ItemHeight = 18;
			this.targetTreeView.Location = new System.Drawing.Point(0, 0);
			this.targetTreeView.Name = "targetTreeView";
			this.targetTreeView.SelectedImageIndex = 0;
			this.targetTreeView.Size = new System.Drawing.Size(200, 531);
			this.targetTreeView.TabIndex = 0;
			this.targetTreeView.AfterCheck += new System.Windows.Forms.TreeViewEventHandler(this.OnCheck);
			// 
			// splitter2
			// 
			this.splitter2.Location = new System.Drawing.Point(200, 24);
			this.splitter2.Name = "splitter2";
			this.splitter2.Size = new System.Drawing.Size(3, 531);
			this.splitter2.TabIndex = 8;
			this.splitter2.TabStop = false;
			// 
			// mainTabControl
			// 
			this.mainTabControl.Alignment = System.Windows.Forms.TabAlignment.Bottom;
			this.mainTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
			this.mainTabControl.Location = new System.Drawing.Point(203, 24);
			this.mainTabControl.Name = "mainTabControl";
			this.mainTabControl.SelectedIndex = 0;
			this.mainTabControl.Size = new System.Drawing.Size(813, 531);
			this.mainTabControl.TabIndex = 10;
			// 
			// menuStrip1
			// 
			this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.scriptToolStripMenuItem,
            this.pluginsToolStripMenuItem,
            this.toolsToolStripMenuItem,
            this.helpToolStripMenuItem});
			this.menuStrip1.Location = new System.Drawing.Point(0, 0);
			this.menuStrip1.Name = "menuStrip1";
			this.menuStrip1.Size = new System.Drawing.Size(1016, 24);
			this.menuStrip1.TabIndex = 12;
			this.menuStrip1.Text = "menuStrip1";
			// 
			// fileToolStripMenuItem
			// 
			this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadWorkspaceToolStripMenuItem,
            this.toolStripSeparator1,
            this.saveWorkspaceToolStripMenuItem,
            this.saveWorkspaceAsToolStripMenuItem,
            this.reloadSolutionToolStripMenuItem});
			this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
			this.fileToolStripMenuItem.Size = new System.Drawing.Size(35, 20);
			this.fileToolStripMenuItem.Text = "File";
			// 
			// loadWorkspaceToolStripMenuItem
			// 
			this.loadWorkspaceToolStripMenuItem.Name = "loadWorkspaceToolStripMenuItem";
			this.loadWorkspaceToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
			this.loadWorkspaceToolStripMenuItem.Text = "Load Workspace...";
			this.loadWorkspaceToolStripMenuItem.Click += new System.EventHandler(this.loadWorkspaceToolStripMenuItem_Click);
			// 
			// toolStripSeparator1
			// 
			this.toolStripSeparator1.Name = "toolStripSeparator1";
			this.toolStripSeparator1.Size = new System.Drawing.Size(189, 6);
			// 
			// saveWorkspaceToolStripMenuItem
			// 
			this.saveWorkspaceToolStripMenuItem.Enabled = false;
			this.saveWorkspaceToolStripMenuItem.Name = "saveWorkspaceToolStripMenuItem";
			this.saveWorkspaceToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
			this.saveWorkspaceToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
			this.saveWorkspaceToolStripMenuItem.Text = "Save Workspace";
			this.saveWorkspaceToolStripMenuItem.Click += new System.EventHandler(this.saveWorkspaceToolStripMenuItem_Click);
			// 
			// saveWorkspaceAsToolStripMenuItem
			// 
			this.saveWorkspaceAsToolStripMenuItem.Enabled = false;
			this.saveWorkspaceAsToolStripMenuItem.Name = "saveWorkspaceAsToolStripMenuItem";
			this.saveWorkspaceAsToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
			this.saveWorkspaceAsToolStripMenuItem.Text = "Save Workspace As...";
			this.saveWorkspaceAsToolStripMenuItem.Click += new System.EventHandler(this.saveWorkspaceAsToolStripMenuItem_Click);
			// 
			// reloadSolutionToolStripMenuItem
			// 
			this.reloadSolutionToolStripMenuItem.Enabled = false;
			this.reloadSolutionToolStripMenuItem.Name = "reloadSolutionToolStripMenuItem";
			this.reloadSolutionToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
			this.reloadSolutionToolStripMenuItem.Text = "Reload Solution...";
			this.reloadSolutionToolStripMenuItem.Click += new System.EventHandler(this.reloadSolutionToolStripMenuItem_Click);
			// 
			// scriptToolStripMenuItem
			// 
			this.scriptToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.consoleToolStripMenuItem});
			this.scriptToolStripMenuItem.Name = "scriptToolStripMenuItem";
			this.scriptToolStripMenuItem.Size = new System.Drawing.Size(46, 20);
			this.scriptToolStripMenuItem.Text = "Script";
			// 
			// consoleToolStripMenuItem
			// 
			this.consoleToolStripMenuItem.Name = "consoleToolStripMenuItem";
			this.consoleToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
			this.consoleToolStripMenuItem.Text = "Console...";
			this.consoleToolStripMenuItem.Click += new System.EventHandler(this.consoleToolStripMenuItem_Click);
			// 
			// pluginsToolStripMenuItem
			// 
			this.pluginsToolStripMenuItem.Name = "pluginsToolStripMenuItem";
			this.pluginsToolStripMenuItem.Size = new System.Drawing.Size(52, 20);
			this.pluginsToolStripMenuItem.Text = "Plugins";
			// 
			// toolsToolStripMenuItem
			// 
			this.toolsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.clearOldLogFilesToolStripMenuItem});
			this.toolsToolStripMenuItem.Name = "toolsToolStripMenuItem";
			this.toolsToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
			this.toolsToolStripMenuItem.Text = "Tools";
			// 
			// clearOldLogFilesToolStripMenuItem
			// 
			this.clearOldLogFilesToolStripMenuItem.Name = "clearOldLogFilesToolStripMenuItem";
			this.clearOldLogFilesToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
			this.clearOldLogFilesToolStripMenuItem.Text = "Clear old log files";
			this.clearOldLogFilesToolStripMenuItem.Click += new System.EventHandler(this.clearOldLogFilesToolStripMenuItem_Click);
			// 
			// helpToolStripMenuItem
			// 
			this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.wikiPageToolStripMenuItem,
            this.aboutToolStripMenuItem});
			this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
			this.helpToolStripMenuItem.Size = new System.Drawing.Size(40, 20);
			this.helpToolStripMenuItem.Text = "Help";
			// 
			// wikiPageToolStripMenuItem
			// 
			this.wikiPageToolStripMenuItem.Name = "wikiPageToolStripMenuItem";
			this.wikiPageToolStripMenuItem.Size = new System.Drawing.Size(120, 22);
			this.wikiPageToolStripMenuItem.Text = "Wiki Page";
			this.wikiPageToolStripMenuItem.Click += new System.EventHandler(this.wikiPageToolStripMenuItem_Click);
			// 
			// aboutToolStripMenuItem
			// 
			this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
			this.aboutToolStripMenuItem.Size = new System.Drawing.Size(120, 22);
			this.aboutToolStripMenuItem.Text = "About";
			this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
			// 
			// MainWindow
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1016, 738);
			this.Controls.Add(this.mainTabControl);
			this.Controls.Add(this.splitter2);
			this.Controls.Add(this.panel2);
			this.Controls.Add(this.splitter1);
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.menuStrip1);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.IsMdiContainer = true;
			this.MainMenuStrip = this.menuStrip1;
			this.Name = "MainWindow";
			this.Text = "Rockstar Target Manager";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainWindow_FormClosing);
			this.Load += new System.EventHandler(this.MainWindow_Load);
			this.panel2.ResumeLayout(false);
			this.menuStrip1.ResumeLayout(false);
			this.menuStrip1.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TreeView targetTreeView;
        private System.Windows.Forms.Splitter splitter2;
        private System.Windows.Forms.TabControl mainTabControl;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadWorkspaceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveWorkspaceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveWorkspaceAsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reloadSolutionToolStripMenuItem;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem pluginsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem scriptToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem consoleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem wikiPageToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem toolsToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem clearOldLogFilesToolStripMenuItem;
    }
}

