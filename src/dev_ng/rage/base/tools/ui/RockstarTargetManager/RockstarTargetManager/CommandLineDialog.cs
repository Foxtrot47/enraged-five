using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Rockstar.TargetManager {
    public partial class CommandLineDialog:Form {
        public CommandLineDialog() {
            InitializeComponent();
        }

        private void localTextBox_TextChanged(object sender,EventArgs e)
        {
            this.RefreshFinal();
        }

        private void replaceGlobal_CheckedChanged(object sender,EventArgs e)
        {
            this.RefreshFinal();
        }

        private void appendToGlobal_CheckedChanged(object sender,EventArgs e)
        {
            this.RefreshFinal();
        }

        public void EditGlobalCommandLine()
        {
            globalTextBox.ReadOnly = false;
            localTextBox.ReadOnly = true;
            replaceGlobal.Enabled = false;
            appendToGlobal.Enabled = false;
        }

        private void globalTextBox_TextChanged(object sender, EventArgs e)
        {
            this.RefreshFinal();
        }

        private void CommandLineDialog_Shown(object sender, EventArgs e)
        {
            if(localTextBox.ReadOnly)
            {
                globalTextBox.Focus();
            }
            else
            {
                localTextBox.Focus();
            }
        }
    }
}