using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Windows.Forms;

namespace Rockstar.TargetManager
{
	////////////////////////////////////////////////////////////////////////////
	// LogCleanup
	////////////////////////////////////////////////////////////////////////////
	public class LogCleanup : IDisposable
	{
		private const uint AUTO_CLEANUP_IF_DAYS_OLD_THRESHOLD = 14;

		private string m_Dir;
		private Thread m_WorkerThread;
		private volatile bool m_WorkerThreadExit;
		private volatile bool m_ManualTrigger;


		////////////////////////////////////////////////////////////////////////
		// LogCleanup
		////////////////////////////////////////////////////////////////////////
		public LogCleanup(string dir)
		{
			m_Dir = dir;
			if(dir != null)
			{
				m_ManualTrigger = false;
				m_WorkerThreadExit = false;
				m_WorkerThread = new Thread(WorkerThread);
				m_WorkerThread.Name = "LogCleanup::WorkerThread";
				m_WorkerThread.Start();
			}
		}

		////////////////////////////////////////////////////////////////////////
		// ~LogCleanup
		////////////////////////////////////////////////////////////////////////
		~LogCleanup()
		{
			Dispose(false);
		}

		////////////////////////////////////////////////////////////////////////
		// Dispose
		////////////////////////////////////////////////////////////////////////
		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		////////////////////////////////////////////////////////////////////////
		// Dispose
		////////////////////////////////////////////////////////////////////////
		protected virtual void Dispose(bool disposing)
		{
			if(disposing)
			{
				if(m_WorkerThread != null)
				{
					m_WorkerThreadExit = true;
					m_WorkerThread.Join();
				}
			}
		}

		////////////////////////////////////////////////////////////////////////
		// ManualTrigger
		////////////////////////////////////////////////////////////////////////
		public void ManualTrigger()
		{
			m_ManualTrigger = true;
		}

		////////////////////////////////////////////////////////////////////////
		// WorkerThread
		////////////////////////////////////////////////////////////////////////
		private void WorkerThread()
		{
			const uint pollExitMs = 100;
			const uint checkFilesHours = 24;
			const uint checkFilesMs = checkFilesHours*60*60*1000;
			const uint checkFilesLoops = checkFilesMs / pollExitMs;

			uint loopCount = checkFilesLoops;
			while(!m_WorkerThreadExit)
			{
				bool manualTrigger = m_ManualTrigger; // lame threading race condition here
				m_ManualTrigger = false;

				if(loopCount >= checkFilesLoops || manualTrigger)
				{
					// Reset the loop count
					loopCount = 0;

					if(Directory.Exists(m_Dir))
					{
						var di = new DirectoryInfo(m_Dir);
						if(di.GetFiles().Length > 0)
						{
							var window = new LogCleanupWindow();

							// Add all the files in the directory to the window
							var currTime = DateTime.Now;
							foreach(var fi in di.GetFiles())
							{
								uint daysOld = (uint)((currTime - fi.LastWriteTime).Days);
								window.AddFile(fi.FullName, daysOld);
							}
							window.EndAddingFiles();

							// Launch the window if there are any files older
							// than the threshold, or if window manually
							// triggered
							if(window.SetInitialDaysOldThreshold(AUTO_CLEANUP_IF_DAYS_OLD_THRESHOLD) || manualTrigger)
							{
								var result = window.ShowDialog();
								if(result == DialogResult.OK)
								{
									// Delete all checked files
									foreach(var f in window.GetCheckedFiles())
									{
										try
										{
											File.Delete(f);
										}
										catch
										{
										}
									}
								}
							}
						}
					}
				}


				Thread.Sleep((int)pollExitMs);
				++loopCount;
			}
		}
	}
}
