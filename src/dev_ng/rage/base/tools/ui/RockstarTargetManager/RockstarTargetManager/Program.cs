using Microsoft.Win32.SafeHandles;
using Microsoft.Win32;
using Rockstar.LogJam;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Net;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;

namespace Rockstar.TargetManager
{
	static class Program
	{
		static void Usage()
		{
			string appName = Environment.GetCommandLineArgs()[0];
			appName = System.IO.Path.GetFileName(appName);
			Console.Out.WriteLine("Usage: "+appName+" [options] [script_file]");
			Console.Out.WriteLine("Options:");
			Console.Out.WriteLine("  --help             Display this information then exit.");
			Console.Out.WriteLine("  --gui              Force gui mode even when started from a console.");
			Console.Out.WriteLine("  --retry-start=<n>  Keep retrying to load for specified number of seconds.");
			Console.Out.WriteLine("  --version          Display version information then exit.");
			Console.Out.WriteLine("  --xb1=<ipaddr>     Enable the specified Xbox One target.");
			Console.Out.WriteLine("                     May be specified multiple times with different targets.");
		}

		static void UnhExceptionHandler(object sender,
										UnhandledExceptionEventArgs args)
		{
			Exception exc = args.ExceptionObject as Exception;
			MessageBox.Show(exc.ToString());
		}

		static float GetDotNetFrameworkVersion()
		{
			// Based on code from http://stackoverflow.com/questions/951856/is-there-an-easy-way-to-check-net-framework-version-using-c.
			// That code is a bit old though, and the registry layout has changed a bit from v4 on.
			var installedVersKey = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\NET Framework Setup\NDP");
			var version_names = installedVersKey.GetSubKeyNames();
			//version names start with 'v', eg, 'v3.5' which needs to be trimmed off before conversion
			var majorVerStr = version_names[version_names.Length - 1];
			double majorVer = Convert.ToDouble(majorVerStr.Remove(0, 1), CultureInfo.InvariantCulture);
			RegistryKey exactVerKey;
			if(majorVer < 4.0)
			{
				exactVerKey = installedVersKey.OpenSubKey(majorVerStr);
			}
			else
			{
				exactVerKey = installedVersKey.OpenSubKey(Regex.Replace(majorVerStr, @"^(v[0-9]+).*", @"$1\\Client"));
			}
			var exactVerStr = (string)exactVerKey.GetValue("Version");
			return (float)Convert.ToDouble(Regex.Replace(exactVerStr, @"^([0-9]+\.[0-9]+).*", "$1"), CultureInfo.InvariantCulture);
		}

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static int Main()
		{
			AppDomain.CurrentDomain.UnhandledException +=
				new UnhandledExceptionEventHandler(UnhExceptionHandler);

			// Ensure that we have a recent enough .NET framework installed
			const float dotNetFrameworkRequired = 4.5f;
			float dotNetFrameworkInstalled = GetDotNetFrameworkVersion();
			if(dotNetFrameworkInstalled < dotNetFrameworkRequired)
			{
				MessageBox.Show("R*TM Requires at least .NET "+dotNetFrameworkRequired+", but only .NET "+dotNetFrameworkInstalled.ToString()+" is installed.\nSelect OK to exit R*TM and open .NET install in web browser.");
				Process.Start("http://www.microsoft.com/en-us/download/details.aspx?id=30653");
				return 1;
			}

			string[] args = Environment.GetCommandLineArgs();
			bool gui = !AttachToConsole();
			UInt32 retryStart = 0;

			foreach(string a in args)
			{
				if(a == "--help")
				{
					Usage();
					return 0;
				}
			}

			foreach(string a in args)
			{
				if(a == "--version")
				{
					Console.Out.WriteLine("R*TM v"+Assembly.GetEntryAssembly().GetName().Version.ToString());
					return 0;
				}
			}

			var xb1Enables = new List<string>();

			foreach(string a in args)
			{
				if(a == "--gui")
				{
					gui = true;
				}
				else if(a.StartsWith("--retry-start="))
				{
					if(!UInt32.TryParse(a.Substring(14), out retryStart))
					{
						Console.Out.WriteLine("Error parsing --retry-start command line option, must specify decimal integer (in seconds).");
						return 1;
					}
				}
				else if(a.StartsWith("--xb1="))
				{
					var xb1Str = a.Substring(6);
					IPAddress xb1IpAddr;
					if(!IPAddress.TryParse(xb1Str, out xb1IpAddr))
					{
						Console.Out.WriteLine("Error parsing --xb1 command line option, must specify an ip address.");
						return 1;
					}
					xb1Enables.Add(xb1Str);
				}
			}

			XB1TargetManager.EnableTargets(xb1Enables);

            // Check version numbers of DLLs match the version of the exe
            string[] dlls = {"LogJam.dll", "RtmLib.dll", "Plugins/PlugPuller.dll", "Plugins/Watson.dll"};
            Version expectedVersion = Assembly.GetExecutingAssembly().GetName().Version;
            foreach(string dll in dlls)
            {
                try
                {
                    Assembly assembly = Assembly.LoadFrom(dll);
                    Version ver = assembly.GetName().Version;
                    if (ver != expectedVersion)
                    {
                        MessageBox.Show(dll + " is version " + ver + ", expected version " + expectedVersion +
                            ".\n\nRockstar Target Manager will now quit.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return 1;
                    }
                }
                catch
                {
                    MessageBox.Show("Failed to load version info from " + dll +
                        "\n\nRockstar Target Manager will now quit.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return 1;
                }
            }

			if(gui)
			{
				// Only allow one gui instance to be running at a time.	 This means
				// that batch files for launching the game can always run R*TM.
				const bool initiallyOwned = false;
				using(var globalMutex = new Mutex(initiallyOwned, "Global\\Rockstar Target Manager"))
				{
					bool canRunGui = false;
					try
					{
						canRunGui = globalMutex.WaitOne((int)retryStart*1000);
					}
					catch(AbandonedMutexException)
					{
						canRunGui = true;
					}
					if(canRunGui)
					{
						try
						{
							string errorMsg;
							if(!HostPcIpAddr.Init(out errorMsg))
							{
								Trace.WriteLine(errorMsg);
								MessageBox.Show(errorMsg);
								return 1;
							}

							Application.EnableVisualStyles();
							Application.SetCompatibleTextRenderingDefault(false);
							Application.Run(new MainWindow());
						}
						finally
						{
							globalMutex.ReleaseMutex();
						}
					}
					else
					{
						// Write to both console and trace.  Using trace is
						// helpful for when the program doesn't seem to launch
						// in Visual Studio.
						var alreadyRunning = "R*TM already running, not starting a new instance";
						Console.Out.WriteLine(alreadyRunning);
						Trace.WriteLine(alreadyRunning);
						return 1;
					}
				}
			}

			else
			{
				string errorMsg;
				if(!HostPcIpAddr.Init(out errorMsg))
				{
					Trace.WriteLine(errorMsg);
					return 1;
				}

				//SynchronizationContext sc = new SynchronizationContext();
				//SynchronizationContext.SetSynchronizationContext(sc);

				EventWaitHandle ewh = new EventWaitHandle(false, EventResetMode.AutoReset);
				//sc.Post(RunProg, ewh);
				RunProg(ewh, args);

				ewh.WaitOne();
			}

			return 0;
		}

		static void RunProg(object state, string[] args)
		{
			Console.Out.WriteLine("RunProg");

			EventWaitHandle ewh = (EventWaitHandle) state;

			//Load the startup script

			if(args.Length > 1)
			{
				Log log = new Log(Console.Out);

				Debug.Listeners.Add(new LogJam.TraceListener(log));

				Trace.WriteLine("FOO FOO");

				RockstarTargetManager rtm = new RockstarTargetManager(Environment.CurrentDirectory);

				Console.Out.WriteLine("Running " + args[1]);

				rtm.ScriptEngine.RunFile(args[1]);
			}
			else
			{
				Usage();
			}

			ewh.Set();
		}

		[DllImport("kernel32.dll")]
		static extern IntPtr GetCurrentProcess();
		[DllImport("kernel32.dll")]
		static extern bool AttachConsole(UInt32 dwProcessId);
		[DllImport("kernel32.dll")]
		static extern bool FreeConsole();
		[DllImport("kernel32.dll")]
		static extern bool AllocConsole();
		[DllImport("kernel32.dll")]
		static extern bool GetFileInformationByHandle(SafeFileHandle hFile, out BY_HANDLE_FILE_INFORMATION lpFileInformation);
		[DllImport("kernel32.dll")]
		static extern SafeFileHandle GetStdHandle(UInt32 nStdHandle);
		[DllImport("kernel32.dll")]
		static extern bool SetStdHandle(UInt32 nStdHandle, SafeFileHandle hHandle);
		[DllImport("kernel32.dll")]
		static extern bool DuplicateHandle(IntPtr hSourceProcessHandle,
											SafeFileHandle hSourceHandle,
											IntPtr hTargetProcessHandle,
											out SafeFileHandle lpTargetHandle,
											UInt32 dwDesiredAccess,
											Boolean bInheritHandle,
											UInt32 dwOptions);
		[DllImport("kernel32.dll")]
		static extern bool CloseHandle(SafeHandle hObject);

		const UInt32 ATTACH_PARENT_PROCESS = 0xFFFFFFFF;
		const UInt32 STD_INPUT_HANDLE = 0xFFFFFFF6;
		const UInt32 STD_OUTPUT_HANDLE = 0xFFFFFFF5;
		const UInt32 STD_ERROR_HANDLE = 0xFFFFFFF4;
		const UInt32 DUPLICATE_SAME_ACCESS = 2;

		static SafeFileHandle hStdOut, hStdIn, hStdErr;
		static SafeFileHandle hStdOutDup, hStdInDup, hStdErrDup;
		static BY_HANDLE_FILE_INFORMATION bhfi;

		struct BY_HANDLE_FILE_INFORMATION
		{
			public UInt32 FileAttributes;
			public System.Runtime.InteropServices.ComTypes.FILETIME CreationTime;
			public System.Runtime.InteropServices.ComTypes.FILETIME LastAccessTime;
			public System.Runtime.InteropServices.ComTypes.FILETIME LastWriteTime;
			public UInt32 VolumeSerialNumber;
			public UInt32 FileSizeHigh;
			public UInt32 FileSizeLow;
			public UInt32 NumberOfLinks;
			public UInt32 FileIndexHigh;
			public UInt32 FileIndexLow;
		}

		static bool AttachToConsole()
		{
			hStdOut = GetStdHandle(STD_OUTPUT_HANDLE);
			hStdIn = GetStdHandle(STD_INPUT_HANDLE);
			hStdErr = GetStdHandle(STD_ERROR_HANDLE);

			 // Get current process handle
			IntPtr hProcess = Process.GetCurrentProcess().Handle;

			//Duplicate stdout handle to save initial value
			DuplicateHandle(hProcess,
							hStdOut,
							hProcess,
							out hStdOutDup,
							0,
							false,
							DUPLICATE_SAME_ACCESS);

			//Duplicate stdin handle to save initial value
			DuplicateHandle(hProcess,
							hStdIn,
							hProcess,
							out hStdInDup,
							0,
							false,
							DUPLICATE_SAME_ACCESS);

			//Duplicate stderr handle to save initial value
			DuplicateHandle(hProcess,
							hStdErr,
							hProcess,
							out hStdErrDup,
							0,
							false,
							DUPLICATE_SAME_ACCESS);

			bool asConsole = AttachConsole(ATTACH_PARENT_PROCESS);

			if(asConsole)
			{
				// Adjust the standard handles
				if(GetFileInformationByHandle(GetStdHandle(STD_OUTPUT_HANDLE), out bhfi))
				{
					SetStdHandle(STD_OUTPUT_HANDLE, hStdOutDup);
				}
				else
				{
					SetStdHandle(STD_OUTPUT_HANDLE, hStdOut);
				}

				if(GetFileInformationByHandle(GetStdHandle(STD_INPUT_HANDLE), out bhfi))
				{
					SetStdHandle(STD_INPUT_HANDLE, hStdInDup);
				}
				else
				{
					SetStdHandle(STD_INPUT_HANDLE, hStdIn);
				}

				if(GetFileInformationByHandle(GetStdHandle(STD_ERROR_HANDLE), out bhfi))
				{
					SetStdHandle(STD_ERROR_HANDLE, hStdErrDup);
				}
				else
				{
					SetStdHandle(STD_ERROR_HANDLE, hStdErr);
				}
				
				/*Console.Out.WriteLine("Hello World: {0}", asConsole?"true":"false");
				Console.Out.WriteLine("Hello World: {0}", asConsole?"true":"false");
				Console.Out.WriteLine("Hello World: {0}", asConsole?"true":"false");
				Console.Out.WriteLine("Hello World: {0}", asConsole?"true":"false");
				Environment.Exit(32);*/
			}
			else
			{
				CloseHandle(hStdOutDup);
				CloseHandle(hStdInDup);
				CloseHandle(hStdErrDup);
			}

			return asConsole;
		}
	}
}
