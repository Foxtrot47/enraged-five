using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Collections;
using System.Text.RegularExpressions;
using System.Runtime.InteropServices;
using System.ComponentModel;
using System.Xml;
using System.Xml.Serialization;
using Rockstar.LogJam;

//Icons from http://www.iconarchive.com/category/system/kids-icons-icons-by-everaldo.html

namespace Rockstar.TargetManager
{
	public partial class MainWindow:Form
	{
		const string APP_NAME = "Rockstar Target Manager";

		RockstarTargetManager m_Rtm;

		ScriptConsole m_ScriptConsole;
		Dictionary<TargetPlatformId, PlatformTreeNode> m_PlatformTreenodes = new Dictionary<TargetPlatformId, PlatformTreeNode>();
		List<TargetTreeNode> m_TargetTreeNodes = new List<TargetTreeNode>();
		LogView m_LogView = new LogView();
		Timer m_RefreshTreenodesTimer = new Timer();
		LogCleanup m_LogCleanup;

		class TargetTreeNode : TreeNode
		{
			RockstarTargetManager m_Rtm;
			Target m_Target;
			ToolStripMenuItem m_RebootMi;
			ToolStripMenuItem m_FastRebootMi;
			ToolStripMenuItem m_CmdLineMi;
			ToolStripMenuItem m_DeployMi;
			ToolStripMenuItem m_LaunchMi;
			ToolStripMenuItem m_StopGoMi;
			ToolStripMenuItem m_CoredumpMi;
			ToolStripMenuItem m_ScreenshotMi;
            ToolStripMenuItem m_TargetSpecificMi;
            ToolStripMenuItem m_RemoveTargetMi;

			public TargetTreeNode(RockstarTargetManager rtm, Target t)
			{
				m_Target = t;
				m_Rtm = rtm;

				base.Text = t.Description;
				base.ImageIndex = base.SelectedImageIndex = 1;
				base.Checked = true;

				ContextMenuStrip cm = new ContextMenuStrip();

				m_RebootMi = new ToolStripMenuItem("Reboot");
				m_RebootMi.Click += delegate(object sender, EventArgs e)
				{
					m_Target.Reboot();
				};
				m_RebootMi.Enabled = false;
				m_RebootMi.ToolTipText = "Reboot target";

				m_FastRebootMi = new ToolStripMenuItem("Fast Reboot");
				m_FastRebootMi.Click += delegate(object sender, EventArgs e)
				{
					m_Target.FastReboot();
				};
				m_FastRebootMi.Enabled = false;
				m_FastRebootMi.ToolTipText = "Fast reboot target";

				m_CmdLineMi = new ToolStripMenuItem("Command Line...");
				m_CmdLineMi.Click += this.OnCmdLineMi;
				m_CmdLineMi.Enabled = !string.IsNullOrEmpty(m_Rtm.WorkspaceFileName);
				m_CmdLineMi.ToolTipText = "Edit target command line";

				m_DeployMi = new ToolStripMenuItem("Deploy");
				m_DeployMi.Click += delegate(object sender, EventArgs e)
				{
					m_Target.Deploy(DeployType.Force);
				};
				m_DeployMi.Enabled = false;
				m_DeployMi.ToolTipText = "Deploy files to target";

				m_LaunchMi = new ToolStripMenuItem("Launch");
				m_LaunchMi.Click += delegate(object sender, EventArgs e)
				{
					m_Target.Launch();
				};
				m_LaunchMi.Enabled = false;
				m_LaunchMi.ToolTipText = "Deploy files to target and launch";

				m_StopGoMi = new ToolStripMenuItem("Stop");
				m_StopGoMi.Click += delegate(object sender, EventArgs e)
				{
					if(m_Target.Stopped)
					{
						m_Target.Go();
					}
					else
					{
						m_Target.Stop();
					}
				};
				m_StopGoMi.Enabled = false;
				m_StopGoMi.ToolTipText = "Start/Stop target processing";

				m_CoredumpMi = new ToolStripMenuItem("Coredump");
				m_CoredumpMi.Click += delegate(object sender, EventArgs e)
				{
					m_Target.Coredump();
				};
				m_CoredumpMi.Enabled = false;
				m_CoredumpMi.ToolTipText = "Trigger a coredump on target";

				m_ScreenshotMi = new ToolStripMenuItem("Screenshot");
				m_ScreenshotMi.Click += delegate(object sender, EventArgs e)
				{
					Trace.WriteLine("Capturing screen for " + m_Target.Name + "...");
					string fn = Path.GetRandomFileName();
					fn = Path.ChangeExtension(fn, ".bmp");
					fn = Path.Combine(Path.GetTempPath(), fn);

					if(m_Target.ScreenShot(fn))
					{
						Image img = Image.FromFile(fn);
						Clipboard.SetImage(img);
						Trace.WriteLine("Screen captured to clipboard for " + m_Target.Name);
						img.Dispose();
						File.Delete(fn);
					}
					else
					{
						Trace.WriteLine("Failed to Capture screen for " + m_Target.Name);
					}
				};
				m_ScreenshotMi.Enabled = false;
				m_ScreenshotMi.ToolTipText = "Take a screenshot (copied to clipboard)";

                m_TargetSpecificMi = new ToolStripMenuItem("Target-specific command");
                m_TargetSpecificMi.Click += delegate(object sender, EventArgs e)
                {
                    m_Target.TargetSpecificCommand();
                };
                m_TargetSpecificMi.Enabled = false;
                m_TargetSpecificMi.ToolTipText = "Invoke a target-specific command on the target";

				m_RemoveTargetMi = new ToolStripMenuItem("Remove");
				m_RemoveTargetMi.Click += delegate(object sender, EventArgs e)
				{
					m_Rtm.RemoveTarget(m_Target.Name);
				};
				m_RemoveTargetMi.ToolTipText = "Remove target";

				cm.Items.Add(m_RebootMi);
				cm.Items.Add(m_FastRebootMi);
				cm.Items.Add(m_CmdLineMi);
				cm.Items.Add(m_DeployMi);
				cm.Items.Add(m_LaunchMi);
				cm.Items.Add(m_StopGoMi);
				cm.Items.Add(m_CoredumpMi);
				cm.Items.Add(m_ScreenshotMi);
                cm.Items.Add(m_TargetSpecificMi);
                cm.Items.Add(new ToolStripSeparator());
				cm.Items.Add(m_RemoveTargetMi);
				cm.Items.Add(new ToolStripSeparator());

				cm.Opening += new CancelEventHandler(
					delegate(Object sender, CancelEventArgs args)
				{
					m_StopGoMi.Text = m_Target.Stopped ? "Go" : "Stop";
				});

				base.ContextMenuStrip = cm;

				m_Rtm.Workspace.Event.AddHandler(this.OnWorkspaceEvent);
			}

			public void Refresh()
			{
				int icon = (int)m_Target.SelectedIcon;
				if(base.ImageIndex != icon)
				{
					base.ImageIndex = base.SelectedImageIndex = icon;
				}

				if(base.Text != m_Target.Description)
				{
					base.Text = m_Target.Description;
				}

				if(this.Checked != m_Target.Enabled)
				{
					this.Checked = m_Target.Enabled;
				}

				m_RebootMi.Enabled = m_Target.EnableReboot;
				m_FastRebootMi.Enabled = m_Target.EnableFastReboot;

				m_LaunchMi.Enabled = m_Target.EnableLaunch && m_Target.Config.IsValid;
				m_DeployMi.Enabled = m_Target.EnableDeploy && m_Target.Config.IsValid;

				m_StopGoMi.Enabled = m_Target.EnableStopGo;

				m_CoredumpMi.Enabled = m_Target.EnableCoredump;

				m_ScreenshotMi.Enabled = m_Target.EnableScreenShot;

                m_TargetSpecificMi.Enabled = m_Target.EnableTargetSpecific;

				//Enable the cmd line when we have a valid workspace.
				m_CmdLineMi.Enabled = !string.IsNullOrEmpty(m_Rtm.WorkspaceFileName);
			}

			public Target Target
			{
				get{return m_Target;}
			}

			public string LocalCommandLine
			{
				get{return m_Rtm.Workspace.LocalSettings[m_Target.Name].CommandLine;}
			}

			void OnCmdLineMi(object sender, EventArgs args)
			{
				CommandLineDialog dlg = new CommandLineDialog();
				dlg.LocalCommandLine = LocalCommandLine;
				dlg.GlobalCommandLine = m_Rtm.Workspace.Platforms[m_Target.PlatformId].CommandLine;
				dlg.AppendToGlobal =
					m_Rtm.Workspace.LocalSettings[m_Target.Name].CommandLine.AppendToGlobal;

				dlg.RefreshFinal();

				DialogResult dr = dlg.ShowDialog(this.TreeView);

				if(DialogResult.OK == dr)
				{
					m_Rtm.Workspace.LocalSettings[m_Target.Name].CommandLine.AppendToGlobal = dlg.AppendToGlobal;
					m_Rtm.Workspace.LocalSettings[m_Target.Name].CommandLine.Set(dlg.LocalCommandLine);
				}
			}

			void OnWorkspaceEvent(object sender, WorkspaceEventArgs args)
			{
				if(args.EventType == WorkspaceEventType.WorkspaceChanged)
				{
					this.Refresh();
				}
			}
		}

		class PlatformTreeNode : TreeNode
		{
			TargetPlatformId m_PlatformId;
			MainWindow m_MainWindow;
			RockstarTargetManager m_Rtm;
			ToolStripMenuItem m_MasterConfigsMi = new ToolStripMenuItem("Select Configuration");
			ToolStripMenuItem m_DeployAllMi = new ToolStripMenuItem("Deploy All");
			ToolStripMenuItem m_LaunchAllMi = new ToolStripMenuItem("Launch All");
			ToolStripMenuItem m_RebootAllMi = new ToolStripMenuItem("Reboot All");
			ToolStripMenuItem m_FastRebootAllMi = new ToolStripMenuItem("Fast Reboot All");
			ToolStripMenuItem m_StopAllMi = new ToolStripMenuItem("Stop All");
			ToolStripMenuItem m_StartAllMi = new ToolStripMenuItem("Start All");
			ToolStripMenuItem m_AddTargetMi = new ToolStripMenuItem("Add Target");

			public PlatformTreeNode(TargetPlatformId platformId, MainWindow mw)
			{
				m_PlatformId = platformId;
				m_MainWindow = mw;
				m_Rtm = mw.m_Rtm;

				base.Text = this.PlatformId.CustomToString();

				ToolStripMenuItem masterCmdLineMi = new ToolStripMenuItem("Command Line...");
				masterCmdLineMi.ToolTipText = "Edit master command line";
				masterCmdLineMi.Click += delegate(object sender, EventArgs e)
				{
					CommandLineDialog dlg = new CommandLineDialog();
					dlg.LocalCommandLine = "";
					dlg.GlobalCommandLine = this.Platform.CommandLine;
					dlg.AppendToGlobal = true;
					dlg.EditGlobalCommandLine();

					dlg.RefreshFinal();

					DialogResult dr = dlg.ShowDialog(m_MainWindow);

					if (DialogResult.OK == dr)
					{
						m_Rtm.Workspace.SetCommandLine(m_PlatformId, dlg.GlobalCommandLine);
					}
				};

				m_MasterConfigsMi.Enabled = false;
				m_MasterConfigsMi.ToolTipText = "Select build configuration";

				m_DeployAllMi.Enabled = false;
				m_DeployAllMi.ToolTipText = "Deploy files to all targets";
				m_DeployAllMi.Click += delegate(object sender, EventArgs e)
				{
					this.DeployAll();
				};

				m_LaunchAllMi.Enabled = false;
				m_LaunchAllMi.ToolTipText = "Deploy files to all targets and launch";
				m_LaunchAllMi.Click += delegate(object sender, EventArgs e)
				{
					this.LaunchAll();
				};

				m_RebootAllMi.ToolTipText = "Reboot all targets";
				m_RebootAllMi.Click += delegate(object sender, EventArgs e)
				{
					this.RebootAll();
				};

				m_FastRebootAllMi.ToolTipText = "Fast reboot all targets";
				m_FastRebootAllMi.Click += delegate(object sender, EventArgs e)
				{
					this.FastRebootAll();
				};

				m_StopAllMi.ToolTipText = "Stop all targets";
				m_StopAllMi.Click += delegate(object sender, EventArgs e)
				{
					this.StopAll();
				};

				m_StartAllMi.ToolTipText = "Resume all targets";
				m_StartAllMi.Click += delegate(object sender, EventArgs e)
				{
					this.StartAll();
				};

				m_AddTargetMi.ToolTipText = "Add a target";
				m_AddTargetMi.Click += delegate(object sender, EventArgs e)
				{
					TextEntryDialog dlg = new TextEntryDialog();
					dlg.Text = "Target Name/IP";
					DialogResult dr = dlg.ShowDialog(m_MainWindow);

					if(DialogResult.OK == dr && !string.IsNullOrEmpty(dlg.TheText))
					{
						m_Rtm.AddTarget(dlg.TheText, platformId);
					}
				};

				this.ContextMenuStrip = new ContextMenuStrip();
				this.ContextMenuStrip.Items.Add(m_MasterConfigsMi);
				this.ContextMenuStrip.Items.Add(masterCmdLineMi);
				this.ContextMenuStrip.Items.Add(m_DeployAllMi);
				this.ContextMenuStrip.Items.Add(m_LaunchAllMi);
				this.ContextMenuStrip.Items.Add(m_RebootAllMi);
				this.ContextMenuStrip.Items.Add(m_FastRebootAllMi);
				this.ContextMenuStrip.Items.Add(m_StopAllMi);
				this.ContextMenuStrip.Items.Add(m_StartAllMi);
				this.ContextMenuStrip.Items.Add(new ToolStripSeparator());
				this.ContextMenuStrip.Items.Add(m_AddTargetMi);

				mw.m_Rtm.Workspace.Event.AddHandler(this.OnWorkspaceEvent);
			}

			void OnWorkspaceEvent(object sender, WorkspaceEventArgs args)
			{
				if(args.EventType == WorkspaceEventType.WorkspaceChanged)
				{
					m_MasterConfigsMi.Enabled = false;
					m_DeployAllMi.Enabled = false;
					m_LaunchAllMi.Enabled = false;

					m_MasterConfigsMi.Enabled = (this.Platform.NumConfigs > 0);
					m_DeployAllMi.Enabled = (this.Platform.NumConfigs > 0);
					m_LaunchAllMi.Enabled = (this.Platform.NumConfigs > 0);

					m_MasterConfigsMi.DropDownItems.Clear();

					foreach(Configuration c in this.Platform.Configurations)
					{
						string name = c.Name;
						ToolStripMenuItem mi = new ToolStripMenuItem(name);
						mi.Click += delegate(object o, EventArgs e)
						{
							m_Rtm.Workspace.SetMasterConfig(Platform.PlatformId, name);
						};

						m_MasterConfigsMi.DropDownItems.Add(mi);
					}
				}
				else if(args.EventType == WorkspaceEventType.GlobalCommandLineChanged)
				{
					if(args.PlatformId == PlatformId)
					{
					}
				}
				else if(args.EventType == WorkspaceEventType.MasterConfigChanged)
				{
					if(args.PlatformId == PlatformId)
					{
						Configuration config =
							m_Rtm.Workspace.Platforms[args.PlatformId].MasterConfig;

						foreach(TreeNode tn in this.Nodes)
						{
							TargetTreeNode ttn = tn as TargetTreeNode;

							if(null == ttn)
							{
								continue;
							}

							ttn.Refresh();
						}
					}
				}
			}

			public TargetPlatformId PlatformId
			{
				get{return m_PlatformId;}
			}

			public Platform Platform
			{
				get{return m_Rtm.Workspace.Platforms[this.PlatformId];}
			}

			private void DeployAll()
			{
				m_Rtm.DeployAll(m_PlatformId);
			}

			private void LaunchAll()
			{
				m_Rtm.LaunchAll(m_PlatformId);
			}

			private void RebootAll()
			{
				DialogResult dr =
					MessageBox.Show("Reboot all?",
									"Reboot",
									MessageBoxButtons.OKCancel,
									MessageBoxIcon.Stop);

				if(DialogResult.OK == dr)
				{
					m_Rtm.RebootAll(m_PlatformId);
				}
			}

			private void FastRebootAll()
			{
				DialogResult dr =
					MessageBox.Show("Fast reboot all?",
									"Fast Reboot",
									MessageBoxButtons.OKCancel,
									MessageBoxIcon.Stop);

				if (DialogResult.OK == dr)
				{
					m_Rtm.FastRebootAll(m_PlatformId);
				}
			}

			private void StopAll()
			{
				m_Rtm.StopAll(m_PlatformId);
			}

			private void StartAll()
			{
				m_Rtm.StartAll(m_PlatformId);
			}
		}

		public MainWindow()
		{
			InitializeComponent();

			this.Text = APP_NAME;

			m_LogView.Dock = DockStyle.Fill;
			panel1.Controls.Add(m_LogView);

			Debug.Listeners.Add(new LogJam.TraceListener(m_LogView.Log));

			this.Show();

			m_Rtm = new RockstarTargetManager(Application.StartupPath);

			m_Rtm.Event.AddHandler(this.OnRtmEvent);
			m_Rtm.Workspace.Event.AddHandler(this.OnWorkspaceEvent);

			m_ScriptConsole = new ScriptConsole(m_Rtm.ScriptEngine);

			this.LoadPlugins();

			this.BuildTargetsTreeview();

			LoadDefaultWorkspace();

			mainTabControl.GotFocus += delegate(object sender, EventArgs e)
			{
				//When the tab control receives focus give it to
				//the currently selected tab, which should give it
				//to its contained control.
				if(null != mainTabControl.SelectedTab)
				{
					mainTabControl.SelectedTab.Focus();
				}
			};

			//Load the startup script
			string[] args = Environment.GetCommandLineArgs();

			if(args.Length > 1)
			{
				m_ScriptConsole.LoadFile(args[1]);
				m_ScriptConsole.Run();
			}

			//Setup periodic cleaning of crash dump directory
			m_LogCleanup = new LogCleanup(RockstarTargetManager.GetCrashdumpDir());
		}

		const string DEFAULT_WORKSPACE_FILE_NAME = "RTMDefaultWorkspace.rtmws";
		public void LoadDefaultWorkspace()
		{
			//See if there is a default workspace
			string ws = Directory.GetCurrentDirectory() + Path.DirectorySeparatorChar + DEFAULT_WORKSPACE_FILE_NAME;
			Trace.WriteLine("Locating default workspace file: " + ws);

			FileInfo finfo = new FileInfo(ws);
			if (!finfo.Exists)
			{
				Trace.WriteLine("Failed to load default workspace " + ws);
				m_Rtm.WorkspaceFileName = DEFAULT_WORKSPACE_FILE_NAME;
				return;
			}

			Trace.WriteLine("Loading default workspace " + ws);
			m_Rtm.LoadWorkspace(ws);
		}

		public bool LoadWorkspace(string filename)
		{
			bool success = m_Rtm.LoadWorkspace(filename);
			if(!success)
			{
				MessageBox.Show(this,
								"Error Loading Workspace",
								"Error",
								MessageBoxButtons.OK,
								MessageBoxIcon.Error);
			}

			return success;
		}

		void OnRtmEvent(object sender, RtmEventArgs args)
		{
			if(args.EventType == RtmEventType.TargetAdded)
			{
				this.OnTargetAdded(((RtmTargetEventArgs) args).TargetName);
			}
			else if(args.EventType == RtmEventType.TargetRemoved)
			{
				this.OnTargetRemoved(((RtmTargetEventArgs) args).TargetName);
			}
		}

		void OnTargetAdded(string name)
		{
			Target target = m_Rtm.GetTarget(name);

			if(null != target)
			{
				//Add a new treenode.
				TargetTreeNode ttn = new TargetTreeNode(m_Rtm, target);

				m_TargetTreeNodes.Add(ttn);

				m_PlatformTreenodes[target.PlatformId].Nodes.Add(ttn);
				m_PlatformTreenodes[target.PlatformId].Expand();

				ttn.Refresh();

				//Add the plugin context menus to the tree node.
				foreach(IPlugin plugin in m_Rtm.Plugins)
				{
					ToolStripMenuItem mi = plugin.GetTargetMenuItem(target);

					if(null != mi)
					{
						ttn.ContextMenuStrip.Items.Add(mi);
					}
				}
			}
		}

		void OnTargetRemoved(string name)
		{
			//Remove the treenode.
			TargetTreeNode ttn = null;
			Target target = null;
			foreach(TargetTreeNode tmp in m_TargetTreeNodes)
			{
				if(tmp.Target.Name == name)
				{
					ttn = tmp;
					target = tmp.Target;

					m_PlatformTreenodes[target.PlatformId].Nodes.Remove(tmp);

					m_TargetTreeNodes.Remove(tmp);

					break;
				}
			}

			if(null != ttn)
			{
				//Remove the plugin context menus from the tree node.
				foreach(IPlugin plugin in m_Rtm.Plugins)
				{
					ToolStripMenuItem mi = plugin.GetTargetMenuItem(target);

					if(null != mi)
					{
						ttn.ContextMenuStrip.Items.Remove(mi);
					}
				}
			}
		}

		void LoadPlugins()
		{
			foreach(IPlugin plugin in m_Rtm.Plugins)
			{
				Control control = plugin.GetControl();
				if(null != control)
				{
					TabPage tp = new TabPage(plugin.Name);

					control.Dock = DockStyle.Fill;
					tp.Controls.Add(control);
					//When the tab page receives focus give it to
					//the contained control.
					tp.GotFocus += delegate(object sender, EventArgs e)
					{
						control.Focus();
					};
					mainTabControl.Controls.Add(tp);
					if(plugin.Name == "Watson")
					{
						//Special case for watson - make it the first tab
						int index = mainTabControl.TabPages.Count - 1;
						mainTabControl.TabPages.RemoveAt(index);
						mainTabControl.TabPages.Insert(0, tp);
						mainTabControl.SelectTab(0);
					}
				}

				ToolStripMenuItem mainMi = plugin.GetMainMenuItem();
				if(null != mainMi)
				{
					pluginsToolStripMenuItem.DropDownItems.Add(mainMi);
				}
			}
		}

		void BuildTargetsTreeview()
		{
			targetTreeView.Nodes.Clear();

			TreeNode targetsTn = new TreeNode("Targets");
			targetsTn.ImageIndex = targetsTn.SelectedImageIndex = 0;
			targetTreeView.Nodes.Add(targetsTn);

			foreach(TargetPlatformId platformId in Enum.GetValues(typeof(TargetPlatformId)))
			{
				if(platformId != TargetPlatformId.Unknown)
				{
					PlatformTreeNode ptn = new PlatformTreeNode(platformId, this);;
					m_PlatformTreenodes[platformId] = ptn;
					ptn.ImageIndex = ptn.SelectedImageIndex = 0;
					ptn.Expand();
					targetsTn.Nodes.Add(ptn);
					this.HideCheckbox(ptn);
				}
			}
			targetsTn.Expand();
			this.HideCheckbox(targetsTn);

			foreach(Target t in m_Rtm.Targets)
			{
				this.OnTargetAdded(t.Name);
			}

			m_RefreshTreenodesTimer.Tick += delegate(Object sender, EventArgs e)
			{
				this.RefreshTargetTreeNodes();
			};
			m_RefreshTreenodesTimer.Interval = 100;
			m_RefreshTreenodesTimer.Start();
		}

		void RefreshTargetTreeNodes()
		{
			if(null != m_TargetTreeNodes)
			{
				foreach(TargetTreeNode ttn in m_TargetTreeNodes)
				{
					ttn.Refresh();
				}
			}
		}

		void SaveWorkspace()
		{
			this.SaveWorkspace(m_Rtm.WorkspaceFileName);
		}

		void SaveWorkspace(string filename)
		{
			bool success = m_Rtm.SaveWorkspace(filename);
			if(!success)
			{
				MessageBox.Show(this,
								"Error Saving Workspace",
								"Error",
								MessageBoxButtons.OK,
								MessageBoxIcon.Error);
			}
		}

		DialogResult ShowLoadWorkspaceDialog()
		{
			openFileDialog1.Reset();
			openFileDialog1.Title = "Load Workspace";
			openFileDialog1.Filter = "Workspace Files (*.rtmws)|*.rtmws|Solution Files (*.sln)|*.sln";
			DialogResult dr = openFileDialog1.ShowDialog(this);

			if(DialogResult.OK == dr)
			{
				this.LoadWorkspace(openFileDialog1.FileName);
			}

			return dr;
		}

		DialogResult ShowSaveWorkspaceDialog()
		{
			saveFileDialog1.Reset();
			saveFileDialog1.Title = "Save Workspace";
			saveFileDialog1.Filter = "Workspace Files (*.rtmws)|*.rtmws";
			saveFileDialog1.DefaultExt = "rtmws";
			string dirStr = Path.GetDirectoryName(m_Rtm.WorkspaceFileName);
			string fnStr = Path.GetFileNameWithoutExtension(m_Rtm.WorkspaceFileName);
			saveFileDialog1.FileName = Path.Combine(dirStr, fnStr) + ".rtmws";
			DialogResult dr = saveFileDialog1.ShowDialog(this);

			if(DialogResult.OK == dr)
			{
				this.SaveWorkspace(saveFileDialog1.FileName);
			}

			return dr;
		}

		DialogResult ShowReloadSolutionDialog()
		{
			openFileDialog1.Reset();
			openFileDialog1.Title = "Load Solution";
			openFileDialog1.Filter = "Solution Files (*.sln)|*.sln";
			openFileDialog1.DefaultExt = "sln";
			DialogResult dr = openFileDialog1.ShowDialog(this);

			if(DialogResult.OK == dr)
			{
				m_Rtm.ReloadSolution(openFileDialog1.FileName);
			}

			return dr;
		}

		DialogResult ShowRequestSaveDialog()
		{
			DialogResult dr =
				MessageBox.Show("Save the current workspace before continuing?",
								"Unsaved Workspace",
								MessageBoxButtons.YesNoCancel,
								MessageBoxIcon.Stop);

			if(DialogResult.Yes == dr)
			{
				if(m_Rtm.Workspace.HasBeenSavedAtLeastOnce)
				{
					this.SaveWorkspace();
				}
				else
				{
					DialogResult tmpDr = this.ShowSaveWorkspaceDialog();

					//If we canceled then pass the cancel back up the chain
					if(DialogResult.Cancel == tmpDr)
					{
						dr = tmpDr;
					}
				}
			}

			return dr;
		}

		void OnWorkspaceEvent(object sender, WorkspaceEventArgs args)
		{
			if(args.EventType == WorkspaceEventType.WorkspaceChanged)
			{
				saveWorkspaceAsToolStripMenuItem.Enabled = true;
				reloadSolutionToolStripMenuItem.Enabled = true;
				saveWorkspaceToolStripMenuItem.Enabled =
					m_Rtm.Workspace.IsDirty || !m_Rtm.Workspace.HasBeenSavedAtLeastOnce;

				if(saveWorkspaceToolStripMenuItem.Enabled)
				{
					this.Text = Path.GetFileNameWithoutExtension(m_Rtm.WorkspaceFileName) + "* - " + APP_NAME;
				}
				else
				{
					this.Text = Path.GetFileNameWithoutExtension(m_Rtm.WorkspaceFileName) + " - " + APP_NAME;
				}
			}
		}

		const int TVIF_STATE			= 0x0008;
		const int TVIS_STATEIMAGEMASK	= 0xF000;
		const int TV_FIRST				= 0x1100;
		const int TVM_GETITEM			= (TV_FIRST + 62);
		const int TVM_SETITEM			= TV_FIRST + 63;

		struct TVITEM 
		{
			public int mask;
			public IntPtr hItem;
			public int state;
			public int stateMask;
			[MarshalAs(UnmanagedType.LPTStr)]
			public String lpszText;
			public int cchTextMax;
			public int iImage;
			public int iSelectedImage;
			public int cChildren;
			public IntPtr lParam;
		};

		[DllImport("user32.dll")]
		static extern int SendMessage(IntPtr h,
												uint msg,
												IntPtr wParam,
												ref TVITEM tvItem);

		void HideCheckbox(TreeNode tn)
		{
			TVITEM tvi = new TVITEM();

			tvi.mask = TVIF_STATE;
			tvi.stateMask = TVIS_STATEIMAGEMASK;
			tvi.state = 0;
			tvi.hItem = tn.Handle;
			SendMessage(tn.TreeView.Handle, TVM_SETITEM, IntPtr.Zero, ref tvi);
		}

		void OnCheck(object sender,TreeViewEventArgs e)
		{
			if(e.Action != TreeViewAction.Unknown)
			{
				TargetTreeNode tn = (TargetTreeNode) e.Node;
				m_Rtm.Workspace.LocalSettings[tn.Target.Name].Enabled = tn.Checked;
				tn.Refresh();
			}
		}

		void MainWindow_FormClosing(object sender,FormClosingEventArgs e)
		{
            if (Globals.m_CopyingFileCount > 0)
            {
                if (MessageBox.Show("A file copy operation is still in progress. Are you sure you want to quit?",
                    "File copy still in progress", MessageBoxButtons.YesNo, MessageBoxIcon.Stop,
                    MessageBoxDefaultButton.Button2) == DialogResult.No)
                {
                    e.Cancel = true;
                    return;
                }
            }
            if (m_Rtm.Workspace.IsDirty)
			{
				DialogResult dr = this.ShowRequestSaveDialog();

				if(DialogResult.Cancel == dr)
				{
					e.Cancel = true;
					return;
				}
			}

			// Stop log cleanup background worker thread.
			m_LogCleanup.Dispose();
			m_LogCleanup = null;

			// Forcably dispose of the TargetManager here so that everything
			// gets shutdown in the correct order.	Cannot rely on garbage
			// collection orderring.
			m_RefreshTreenodesTimer.Dispose();
			m_Rtm.Dispose();
			m_Rtm = null;
		}

		void loadWorkspaceToolStripMenuItem_Click(object sender,EventArgs e)
		{
			DialogResult dr = DialogResult.None;

			if(m_Rtm.Workspace.IsDirty)
			{
				dr = this.ShowRequestSaveDialog();
			}

			if(DialogResult.Cancel != dr)
			{
				this.ShowLoadWorkspaceDialog();
			}
		}

		void saveWorkspaceToolStripMenuItem_Click(object sender,EventArgs e)
		{
			if(m_Rtm.Workspace.HasBeenSavedAtLeastOnce)
			{
				this.SaveWorkspace();
			}
			else
			{
				this.ShowSaveWorkspaceDialog();
			}
		}

		void saveWorkspaceAsToolStripMenuItem_Click(object sender,EventArgs e)
		{
			this.ShowSaveWorkspaceDialog();
		}

		void reloadSolutionToolStripMenuItem_Click(object sender,EventArgs e)
		{
			DialogResult dr = DialogResult.None;

			if(m_Rtm.Workspace.IsDirty)
			{
				dr = this.ShowRequestSaveDialog();
			}

			if(DialogResult.Cancel != dr)
			{
				this.ShowReloadSolutionDialog();
			}
		}

		void consoleToolStripMenuItem_Click(object sender,EventArgs e)
		{
			m_ScriptConsole.Show(this);
		}

		private void clearOldLogFilesToolStripMenuItem_Click(object sender, EventArgs e)
		{
			m_LogCleanup.ManualTrigger();
		}

		private void wikiPageToolStripMenuItem_Click(object sender, EventArgs e)
		{
			Process.Start("https://devstar.rockstargames.com/wiki/index.php/Rockstar_Target_Manager");
		}

		private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
		{
			AboutDialog aboutDlg = new AboutDialog();
			aboutDlg.ShowDialog(this);
		}

		private void MainWindow_Load(object sender, EventArgs e)
		{

		}
	}
}
