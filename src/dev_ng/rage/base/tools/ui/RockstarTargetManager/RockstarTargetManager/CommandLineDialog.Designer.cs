namespace Rockstar.TargetManager {
    partial class CommandLineDialog {

        bool m_RefreshingFinal = false;

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if(disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        public void RefreshFinal()
        {
            if(!m_RefreshingFinal)
            {
                m_RefreshingFinal = true;
                if(this.AppendToGlobal)
                {
                    this.FinalCommandLine = this.GlobalCommandLine + " " + this.LocalCommandLine;
                }
                else
                {
                    this.FinalCommandLine = this.LocalCommandLine;
                }

                m_RefreshingFinal = false;
            }
        }

        public string GlobalCommandLine
        {
            get{return globalTextBox.Text;}
            set{globalTextBox.Text = value;}
        }

        public string LocalCommandLine
        {
            get{return localTextBox.Text;}
            set{localTextBox.Text = value;}
        }

        public string FinalCommandLine
        {
            get{return finalTextBox.Text;}
            set{finalTextBox.Text = value;}
        }

        public bool AppendToGlobal
        {
            get{return appendToGlobal.Checked;}
            set
            {
                if(value)
                {
                    appendToGlobal.Checked = true;
                }
                else
                {
                    replaceGlobal.Checked = true;
                }
            }
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.localTextBox = new System.Windows.Forms.TextBox();
            this.okButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.globalTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.replaceGlobal = new System.Windows.Forms.RadioButton();
            this.appendToGlobal = new System.Windows.Forms.RadioButton();
            this.finalTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // localTextBox
            // 
            this.localTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.localTextBox.Location = new System.Drawing.Point(8, 56);
            this.localTextBox.Name = "localTextBox";
            this.localTextBox.Size = new System.Drawing.Size(272, 20);
            this.localTextBox.TabIndex = 0;
            this.localTextBox.TextChanged += new System.EventHandler(this.localTextBox_TextChanged);
            // 
            // okButton
            // 
            this.okButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.okButton.Location = new System.Drawing.Point(120, 128);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(75, 23);
            this.okButton.TabIndex = 1;
            this.okButton.Text = "Ok";
            this.okButton.UseVisualStyleBackColor = true;
            // 
            // cancelButton
            // 
            this.cancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.Location = new System.Drawing.Point(200, 128);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 2;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            // 
            // globalTextBox
            // 
            this.globalTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.globalTextBox.Location = new System.Drawing.Point(8, 16);
            this.globalTextBox.Name = "globalTextBox";
            this.globalTextBox.ReadOnly = true;
            this.globalTextBox.Size = new System.Drawing.Size(272, 20);
            this.globalTextBox.TabIndex = 4;
            this.globalTextBox.TextChanged += new System.EventHandler(this.globalTextBox_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(37, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Global";
            // 
            // replaceGlobal
            // 
            this.replaceGlobal.AutoSize = true;
            this.replaceGlobal.Location = new System.Drawing.Point(8, 120);
            this.replaceGlobal.Name = "replaceGlobal";
            this.replaceGlobal.Size = new System.Drawing.Size(98, 17);
            this.replaceGlobal.TabIndex = 6;
            this.replaceGlobal.Text = "Replace Global";
            this.replaceGlobal.UseVisualStyleBackColor = true;
            this.replaceGlobal.CheckedChanged += new System.EventHandler(this.replaceGlobal_CheckedChanged);
            // 
            // appendToGlobal
            // 
            this.appendToGlobal.AutoSize = true;
            this.appendToGlobal.Checked = true;
            this.appendToGlobal.Location = new System.Drawing.Point(8, 136);
            this.appendToGlobal.Name = "appendToGlobal";
            this.appendToGlobal.Size = new System.Drawing.Size(107, 17);
            this.appendToGlobal.TabIndex = 7;
            this.appendToGlobal.TabStop = true;
            this.appendToGlobal.Text = "Append to Global";
            this.appendToGlobal.UseVisualStyleBackColor = true;
            this.appendToGlobal.CheckedChanged += new System.EventHandler(this.appendToGlobal_CheckedChanged);
            // 
            // finalTextBox
            // 
            this.finalTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.finalTextBox.Location = new System.Drawing.Point(8, 96);
            this.finalTextBox.Name = "finalTextBox";
            this.finalTextBox.ReadOnly = true;
            this.finalTextBox.Size = new System.Drawing.Size(272, 20);
            this.finalTextBox.TabIndex = 8;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 40);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Local";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 80);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Final";
            // 
            // CommandLineDialog
            // 
            this.AcceptButton = this.okButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cancelButton;
            this.ClientSize = new System.Drawing.Size(292, 166);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.finalTextBox);
            this.Controls.Add(this.appendToGlobal);
            this.Controls.Add(this.replaceGlobal);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.globalTextBox);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.okButton);
            this.Controls.Add(this.localTextBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CommandLineDialog";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Command Line";
            this.Shown += new System.EventHandler(this.CommandLineDialog_Shown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox localTextBox;
        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.TextBox globalTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton replaceGlobal;
        private System.Windows.Forms.RadioButton appendToGlobal;
        private System.Windows.Forms.TextBox finalTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
    }
}