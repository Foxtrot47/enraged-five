using System;
using System.Windows.Forms;
using System.Diagnostics;
using System.Collections.Generic;

using Rockstar.TargetManager;

namespace PlugPuller
{
    public class PlugPullerPlugin : IPlugin
    {
        private IPluginHost m_PluginHost = null;
        private Timer m_CheckTargetsTimer = new Timer();
        private Dictionary<Target, ToolStripMenuItem> m_TargetMenuItems =
            new Dictionary<Target, ToolStripMenuItem>();

        public PlugPullerPlugin()
        {
            //Periodically check the menu items and enable/disable
            //them based on the availability of the target.
            m_CheckTargetsTimer.Tick += delegate(Object sender, EventArgs args)
            {
                foreach(KeyValuePair<Target, ToolStripMenuItem> kvp in m_TargetMenuItems)
                {
                    if(kvp.Key.Enabled != kvp.Value.Enabled)
                    {
                        kvp.Value.Enabled = kvp.Key.Enabled;
                    }
                }
            };
            m_CheckTargetsTimer.Interval = 100;
            m_CheckTargetsTimer.Start();
        }

        //Implements IPlugin interface
        public bool Init(IPluginHost plgHost)
        {
            m_PluginHost = plgHost;

            return true;
        }

        //Implements IPlugin interface
        public void Shutdown()
        {
        }

        //Implements IPlugin interface
        public void AddTarget(Target t)
        {
        }

        //Implements IPlugin interface
        public void RemoveTarget(Target t)
        {
        }

        //Implements IPlugin interface
        public Control GetControl()
        {
            return null;
        }

        //Implements IPlugin interface
        public ToolStripMenuItem GetMainMenuItem()
        {
            return null;
        }

        //Implements IPlugin interface
        public ToolStripMenuItem GetTargetMenuItem(Target t)
        {
            ToolStripMenuItem menuItem;

            if(!m_TargetMenuItems.TryGetValue(t, out menuItem))
            {
                menuItem = new ToolStripMenuItem("Plug Puller");

                ToolStripMenuItem miPull = new ToolStripMenuItem("Pull");
                ToolStripMenuItem miReplace = new ToolStripMenuItem("Replace");

                miPull.ToolTipText = "Simulate cable disconnect";
                miReplace.ToolTipText = "Simulate cable replacement";

                menuItem.DropDownItems.Add(miPull);
                menuItem.DropDownItems.Add(miReplace);

                string xedkDir = Environment.GetEnvironmentVariable("XEDK");

                if(null != xedkDir && xedkDir.Length > 0)
                {
                    ProcessStartInfo pullStartInfo = new ProcessStartInfo();
                    ProcessStartInfo replaceStartInfo = new ProcessStartInfo();

                    //These two are supposed to ensure no dos window
                    //pops up when we run the command, but they don't appear
                    //to work.
                    pullStartInfo.CreateNoWindow = replaceStartInfo.CreateNoWindow = true;
                    pullStartInfo.UseShellExecute = replaceStartInfo.UseShellExecute = false;

                    pullStartInfo.FileName = replaceStartInfo.FileName =
                        xedkDir + "\\bin\\win32\\xbsetcfg.exe";

                    pullStartInfo.Arguments = "/X " + t.Name + " /NIC wifi";
                    replaceStartInfo.Arguments = "/X " + t.Name + " /NIC auto";

                    miPull.Click += delegate(object sender, EventArgs e)
                    {
                        Process.Start(pullStartInfo);
                    };

                    miReplace.Click += delegate(object sender, EventArgs e)
                    {
                        Process.Start(replaceStartInfo);
                    };
                }
                else
                {
                    miPull.Enabled = false;
                    miReplace.Enabled = false;
                }

                m_TargetMenuItems.Add(t, menuItem);
            }

            return menuItem;
        }

        //Implements IPlugin interface
        public ScriptInterface GetScriptInterface()
        {
            return null;
        }

        //Implements IPlugin interface
        public string Name
        {
            get{return "Plug Puller";}
        }
    }
}
