using Rockstar.LogJam;
using Rockstar.TargetManager;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO.Compression;
using System.IO;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace Watson
{
	delegate void MyDelegate();

	public class WatsonScriptInterface : ScriptInterface
	{
		WatsonPlugin m_Watson;

		internal WatsonScriptInterface(WatsonPlugin watson)
		{
			m_Watson = watson;
		}

		public bool AddLog(string name)
		{
			return m_Watson.AddLog(name);
		}

		public void RemoveLog(string name)
		{
			m_Watson.RemoveLog(name);
		}

		public void ClearAll()
		{
			m_Watson.ClearAll();
		}

		public void EnableAll()
		{
			m_Watson.SetAllEnabled(true);
		}

		public void DisableAll()
		{
			m_Watson.SetAllEnabled(false);
		}

		public override string Name
		{
			get{return "Watson";}
		}
	}

	class SavedLogView : LogView
	{
		private static Object sm_Lock = new Object();
		private static List<SavedLogView> sm_Instances = new List<SavedLogView>();
		private static Thread sm_WorkerThread;
		private static volatile bool sm_WorkerThreadExit;

		private Object m_Lock = new Object();
		private StreamWriter m_Stream;
		private string m_FilenamePattern;
		private uint m_Index;

		public SavedLogView(string filenamePattern)
		{
			m_FilenamePattern = filenamePattern;
			m_Index = 0;

			lock(sm_Lock)
			{
				if(sm_Instances.Count == 0)
				{
					sm_WorkerThreadExit = false;
					sm_WorkerThread = new Thread(WorkerThread);
					sm_WorkerThread.Name = "Watson Worker Thread";
					sm_WorkerThread.Start();
				}
				sm_Instances.Add(this);
			}
		}

		protected override void Dispose(bool disposing)
		{
			try
			{
				if(disposing)
				{
					lock(sm_Lock)
					{
						sm_Instances.Remove(this);
						if(sm_Instances.Count == 0)
						{
							sm_WorkerThreadExit = true;
							sm_WorkerThread.Join();
						}
					}
					if(m_Stream != null)
					{
						m_Stream.Flush();
						m_Stream.Dispose();
					}
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		public void Flush()
		{
			lock(m_Lock)
			{
				if(m_Stream != null)
				{
					m_Stream.Flush();
				}
			}
		}

		public void NewFile()
		{
			lock(m_Lock)
			{
				if(m_Stream != null)
				{
					m_Stream.Flush();
					m_Stream.Dispose();
					m_Stream = null;
					++m_Index;
				}
			}

			MyDelegate d = delegate() { Clear(); };
			BeginInvoke(d);
		}

		public void CopyLogFile(ZipArchive zip)
		{
			lock(m_Lock)
			{
				if(m_Stream != null)
				{
					WriteLine("  --- end of copied log file ---");

					// Need to temporarily close the file before it can be added to the zip
					m_Stream.Flush();
					m_Stream.Dispose();
					m_Stream = null;

					string file = String.Format(m_FilenamePattern, m_Index.ToString());
					string result;
					try
					{
						zip.CreateEntryFromFile(file, Path.GetFileName(file), CompressionLevel.Optimal);
						result = "  \""+file+"\"";
					}
					catch(Exception e)
					{
						result = "  Error compressing \""+file+"\": "+e.Message;
					}

					// Re-open logfile
					const bool append = true;
					m_Stream = new StreamWriter(file, append);

					// Obviously this output now misses the cutoff for the file
					// copy, but still useful for the user running R*TM
					WriteLine(result);
				}
			}
		}

		private void EnsureFileOpen()
		{
			while(m_Stream == null)
			{
				string filename = String.Format(m_FilenamePattern, m_Index.ToString());
				if(!File.Exists(filename))
				{
					try
					{
						m_Stream = File.CreateText(filename);
						WriteLine("R*TM v"+Assembly.GetEntryAssembly().GetName().Version.ToString());
						break;
					}
					catch
					{
					}
				}
				++m_Index;
			}
		}

		public new void Write(string str)
		{
			if(!String.IsNullOrEmpty(str))
			{
				lock(m_Lock)
				{
					EnsureFileOpen();

					// Strip invalid UTF surrogate pairs
					char[] chArr = str.ToCharArray();
					for (int i = 0; i < chArr.Length; ++i)
					{
						if (chArr[i] >= 0xd000)
						{
							chArr[i] = '?';
						}
					}
					string msg = new string(chArr); // Sigh @ C#
					m_Stream.Write(msg.Replace("\n", "\r\n"));
				}
				base.Write(str);
			}
		}

		public new void WriteLine(string str)
		{
			if(str != null)
			{
				lock(m_Lock)
				{
					EnsureFileOpen();

					// Strip invalid UTF surrogate pairs
					char[] chArr = str.ToCharArray();
					for (int i = 0; i < chArr.Length; ++i)
					{
						if (chArr[i] >= 0xd000)
						{
							chArr[i] = '?';
						}
					}
					string msg = new string(chArr); // Sigh @ C#
					m_Stream.WriteLine(msg);
				}
				base.WriteLine(str);
			}
		}

		private static void WorkerThread()
		{
			while(!sm_WorkerThreadExit)
			{
				lock(sm_Lock)
				{
					foreach(SavedLogView inst in sm_Instances)
					{
						inst.Flush();
					}
				}
				for(uint i=0; i<100 && !sm_WorkerThreadExit; ++i)
				{
					Thread.Sleep(100);
				}
			}
		}
	}

	class WatsonTabPage : TabPage
	{
		IPluginHost m_PlgHost;
		Target m_Target;
		SavedLogView m_LogView;

		SpewView m_SpewView = new SpewView();

		public WatsonTabPage(IPluginHost plgHost, Target t)
		{
			t.TargetEvent.AddHandler(this.OnTargetEvent);

			m_PlgHost = plgHost;

			m_LogView = new SavedLogView(RockstarTargetManager.GetCrashdumpDir()+"console-"+t.Name+"-{0}.log");

			m_Target = t;

			m_LogView.Dock = System.Windows.Forms.DockStyle.Fill;

			m_SpewView.Dock = System.Windows.Forms.DockStyle.Fill;
			m_SpewView.Controls.Add(m_LogView);

			base.Controls.Add(m_SpewView);

			base.Text = t.FullName;
		}

		protected override void Dispose(bool disposing)
		{
			try
			{
				if(disposing)
				{
					if(m_LogView != null)	m_LogView.Dispose();
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		public Target Target
		{
			get{return m_Target;}
		}

		public LogView LogView
		{
			get{return m_LogView;}
		}

		private void OnTargetEvent(object sender, TargetEventArgs eventArgs)
		{
			try
			{
				switch(eventArgs.EventType)
				{
					case TargetEventType.RequestNewLogFile:
						m_LogView.NewFile();
						break;

					case TargetEventType.BugRaised:
						m_LogView.CopyLogFile(((BugRaisedTargetEventArgs)eventArgs).Zip);
						break;

					//assert() causes this to fire.
					case TargetEventType.AssertionFailed:
						Trace.WriteLine(String.Format("Assertion addr:0x{0:x08} thread:0x{1:x08}",
										eventArgs.Address,
										eventArgs.ThreadId));
						m_LogView.Flush();
						break;

					case TargetEventType.DebugString:
						m_LogView.Write(eventArgs.Message);
						break;

					case TargetEventType.Rebooting:
						m_LogView.Write(eventArgs.Message);
						break;

					case TargetEventType.MemoryReadException:
					case TargetEventType.MemoryWriteException:
						m_LogView.Flush();
						break;

					case TargetEventType.ExecutionBreak:
						//__debugbreak causes this to fire.
						Trace.WriteLine(String.Format("Execution Break addr:0x{0:x08} thread:0x{1:x08}",
										eventArgs.Address,
										eventArgs.ThreadId));
						m_LogView.Flush();
						break;

					case TargetEventType.DataBreak:
						Trace.WriteLine(String.Format("Data Break addr:0x{0:x08} thread:0x{1:x08}",
										eventArgs.Address,
										eventArgs.ThreadId));
						m_LogView.Flush();
						break;

					case TargetEventType.Rip:
						Trace.WriteLine(String.Format("RIP addr:0x{0:x08} thread:0x{1:x08}",
										eventArgs.Address,
										eventArgs.ThreadId));
						m_LogView.Flush();
						break;

					case TargetEventType.Acquired:
						BeginInvoke(new MethodInvoker(delegate
						{
							this.Text = m_Target.FullName;
						}));
						Trace.WriteLine(eventArgs.Message);
						break;

					case TargetEventType.Lost:
						Trace.WriteLine(eventArgs.Message);
						break;

					case TargetEventType.Launch:
						MyDelegate d = delegate()
						{
							m_LogView.Clear();
							m_LogView.ScrollOnAppend = true;
						};
						m_LogView.BeginInvoke(d);
						break;

					default:
						m_LogView.WriteLine(eventArgs.Message);
						break;
				}
			}
			catch (System.Exception e)
			{
				Trace.WriteLine(e);
			}
		}
	}

	public class WatsonPlugin : IPlugin
	{
		IPluginHost m_PluginHost;
		List<WatsonTabPage> m_TabPages = new List<WatsonTabPage>();
		TabControl m_TabControl = new TabControl();
		MenuStrip m_MenuStrip = new MenuStrip();
		ToolStripMenuItem m_EditMenu = new ToolStripMenuItem("Edit");
		ContextMenuStrip m_CtxMenu = new ContextMenuStrip();
		System.Windows.Forms.Timer m_CheckTargetsTimer =
			new System.Windows.Forms.Timer();
		FindDialog m_FindDlg = null;
		FilterDialog m_FilterDlg = null;
		ToolStripMenuItem m_PlgInMainMenuItem = new ToolStripMenuItem("Watson");
		WatsonScriptInterface m_ScriptInterface = null;
		Dictionary<Target, ToolStripMenuItem> m_TargetMenuItems =
			new Dictionary<Target,ToolStripMenuItem>();

		public WatsonPlugin()
		{
			m_FindDlg = new FindDialog();
			m_FilterDlg = new FilterDialog();

			m_CheckTargetsTimer.Tick += delegate(Object sender, EventArgs args)
			{
				//Periodically check the menu items and enable/disable
				//them based on the availability of the target.
				foreach(KeyValuePair<Target, ToolStripMenuItem> kvp in m_TargetMenuItems)
				{
                    if(kvp.Key.Enabled != kvp.Value.Enabled)
                    {
                        kvp.Value.Enabled = kvp.Key.Enabled;
                    }
				}
			};
			m_CheckTargetsTimer.Interval = 100;
			m_CheckTargetsTimer.Start();

			//When a tab is selected add the menu to the tab page.
			m_TabControl.Selected += delegate(object sender, TabControlEventArgs e)
			{
				//Add the toolstrip menu to the tab control.
				if(null != e.TabPage)
				{
					e.TabPage.Controls.Add(m_MenuStrip);
					m_FindDlg.Text = "Find - " + this.Target.Name;
					m_FindDlg.SetLogView(this.ActiveLogView);
					m_FilterDlg.Text = "Filter - " + this.Target.Name;
					m_FilterDlg.SetLog(this.ActiveLogView.Log);
				}
				else
				{
					m_FindDlg.SetLogView(null);
					m_FilterDlg.SetLog(null);
				}
			};

			m_TabControl.SelectedIndexChanged += delegate(object sender, EventArgs e)
			{
				//When the selected view changes give it focus.
				if(null != this.ActiveLogView)
				{
					this.ActiveLogView.Focus();
				}
			};

			m_TabControl.GotFocus += delegate(object sender, EventArgs e)
			{
				//When the tab control receives focus give it to the current
				//log view.
				if(null != this.ActiveLogView)
				{
					this.ActiveLogView.Focus();
				}
			};

			//------- Toolstrip menu -------

			//The toolstrip menu exists solely to provide keyboard
			//shortcuts for common tasks such as "Find".  We make it
			//invisible because each of these menu items is also available
			//in the context menu, defined below.
			m_MenuStrip.Visible = false;

			ToolStripMenuItem tsFindMi = new ToolStripMenuItem("Find...");
			tsFindMi.Click += delegate(object sender, EventArgs e)
			{
				this.ShowFindDialog();
			};
			tsFindMi.ShortcutKeys = Keys.Control | Keys.F;

			ToolStripMenuItem tsFindNextMi = new ToolStripMenuItem("Find Next");
			tsFindNextMi.Click += delegate(object sender, EventArgs e)
			{
				m_FindDlg.FindDirection = FindDirection.Forward;
				m_FindDlg.DoFind();
			};
			tsFindNextMi.ShortcutKeys = Keys.F3;

			ToolStripMenuItem tsFindPrevMi = new ToolStripMenuItem("Find Prev");
			tsFindPrevMi.Click += delegate(object sender, EventArgs e)
			{
				m_FindDlg.FindDirection = FindDirection.Backward;
				m_FindDlg.DoFind();
			};
			tsFindPrevMi.ShortcutKeys = Keys.Shift | Keys.F3;
			
			ToolStripMenuItem[] tsMenuItems = new ToolStripMenuItem[]
			{
				tsFindMi,
				tsFindNextMi,
				tsFindPrevMi,
			};

			foreach(ToolStripMenuItem mi in tsMenuItems)
			{
				mi.Enabled = false;
			}

			m_EditMenu.DropDownItems.AddRange(tsMenuItems);
			m_MenuStrip.Items.Add(m_EditMenu);

			//------- Context menu -------

			ToolStripMenuItem scrollOnAppendMi = new ToolStripMenuItem("Auto Scroll");
			scrollOnAppendMi.Click += delegate(object sender, EventArgs e)
			{
				if(null != this.ActiveLogView)
				{
					scrollOnAppendMi.Checked = !scrollOnAppendMi.Checked;
					this.ActiveLogView.ScrollOnAppend = scrollOnAppendMi.Checked;
				}
			};
			scrollOnAppendMi.ToolTipText = "Enable/Disable auto scrolling";

			ToolStripMenuItem clearMi = new ToolStripMenuItem("Clear", null, null, Keys.Control | Keys.L);
			clearMi.Click += delegate(object sender, EventArgs e)
			{
				if(null != this.ActiveLogView)
				{
					this.ActiveLogView.Clear();
				}
			};
			clearMi.ToolTipText = "Clear the log";

			ToolStripMenuItem clearAllMi = new ToolStripMenuItem("Clear All");
			clearAllMi.Click += delegate(Object sender, EventArgs args)
			{
				this.ClearAll();
			};
			clearAllMi.ToolTipText = "Clear all logs";

			EventHandler findOnClick =
				delegate(object sender, EventArgs e)
				{
					this.ShowFindDialog();
				};
			ToolStripMenuItem findMi = new ToolStripMenuItem("Find...", null, findOnClick, Keys.Control | Keys.F);
			findMi.ToolTipText = "Find text in the log";

			ToolStripMenuItem editFilterMi = new ToolStripMenuItem("Filter...");
			editFilterMi.Click += delegate(object sender, EventArgs e)
			{
				this.ShowFilterDialog();
			};
			editFilterMi.ToolTipText = "Add/Modify the text filter";

			ToolStripMenuItem clearFilterMi = new ToolStripMenuItem("Clear Filter");
			clearFilterMi.Click += delegate(object sender, EventArgs e)
			{
				if(null != this.ActiveLogView)
				{
					this.ActiveLogView.Log.Filter = new Filter();
				}
			};
			clearFilterMi.ToolTipText = "Clear the text filter";

			ToolStripMenuItem enabledMi = new ToolStripMenuItem("Enabled");
			enabledMi.Click += delegate(object sender, EventArgs e)
			{
				if(null != this.ActiveLogView)
				{
					enabledMi.Checked = !enabledMi.Checked;
					this.ActiveLogView.Enabled = enabledMi.Checked;
				}
			};
			enabledMi.ToolTipText = "Enable/Disable logging";

			ToolStripItem[] menuItems = new ToolStripItem[]
			{
				scrollOnAppendMi,
				findMi,
				editFilterMi,
				clearFilterMi,
				enabledMi,
				new ToolStripSeparator(),
				clearMi,
				clearAllMi,
			};

			m_CtxMenu.Items.AddRange(menuItems);

			m_CtxMenu.Opening += new CancelEventHandler(
				delegate(Object sender, CancelEventArgs args)
			{
				if(null != this.ActiveLogView)
				{
					scrollOnAppendMi.Checked = this.ActiveLogView.ScrollOnAppend;
					clearFilterMi.Enabled =
						(this.ActiveLogView.Log.Filter.Text.Length > 0)
						|| (this.ActiveLogView.Log.Filter.ExcludeText.Length > 0);
					enabledMi.Checked = this.ActiveLogView.Enabled;
				}
			});

			//------- Plugin menu -------

			ToolStripMenuItem enableAllMi = new ToolStripMenuItem("Enable All");
			enableAllMi.Click += delegate(Object sender, EventArgs args)
			{
				this.SetAllEnabled(true);
			};

			ToolStripMenuItem disableAllMi = new ToolStripMenuItem("Disable All");
			disableAllMi.Click += delegate(Object sender, EventArgs args)
			{
				this.SetAllEnabled(false);
			};

			clearAllMi = new ToolStripMenuItem("Clear All");
			clearAllMi.Click += delegate(Object sender, EventArgs args)
			{
				this.ClearAll();
			};

			m_PlgInMainMenuItem.DropDownItems.Add(enableAllMi);
			m_PlgInMainMenuItem.DropDownItems.Add(disableAllMi);
			m_PlgInMainMenuItem.DropDownItems.Add(new ToolStripSeparator());
			m_PlgInMainMenuItem.DropDownItems.Add(clearAllMi);

			m_ScriptInterface = new WatsonScriptInterface(this);
		}

		//Implements IPlugin interface
		public bool Init(IPluginHost plgHost)
		{
			m_PluginHost = plgHost;

			return true;
		}

		//Implements IPlugin interface
		public void Shutdown()
		{
			foreach(WatsonTabPage wtp in m_TabPages)
			{
				this.RemoveTabPage(wtp);
				wtp.Dispose();
			}

			m_TabPages.Clear();
			m_TabControl.Controls.Clear();
		}

		//Implements IPlugin interface
		public void AddTarget(Target t)
		{
			WatsonTabPage wtp = new WatsonTabPage(m_PluginHost, t);

			wtp.LogView.ContextMenuStrip = m_CtxMenu;
			m_TabControl.Controls.Add(wtp);
			m_TabPages.Add(wtp);

			if(m_TabPages.Count == 1)
			{
				//Add the toolstrip menu to the first tab control.
				wtp.Controls.Add(m_MenuStrip);
			}

			foreach(ToolStripMenuItem mi in m_EditMenu.DropDownItems)
			{
				mi.Enabled = (m_TabPages.Count > 0);
			}
		}

		private void RemoveTabPage(WatsonTabPage wtp)
		{
			if(m_TabControl.SelectedTab == wtp)
			{
				m_TabControl.DeselectTab(wtp);
			}

			m_TabControl.Controls.Remove(wtp);

			if(m_TabPages.Count == 0)
			{
				//Remove the toolstrip menu.
				wtp.Controls.Remove(m_MenuStrip);
			}

			foreach(ToolStripMenuItem mi in m_EditMenu.DropDownItems)
			{
				mi.Enabled = (m_TabPages.Count > 0);
			}
		}

		//Implements IPlugin interface
		public void RemoveTarget(Target t)
		{
			WatsonTabPage wtp = this.GetTabPage(t);

			if(null != wtp)
			{
				RemoveTabPage(wtp);
				m_TabPages.Remove(wtp);
				wtp.Dispose();
			}
		}

		//Implements IPlugin interface
		public Control GetControl()
		{
			return m_TabControl;
		}

		//Implements IPlugin interface
		public ToolStripMenuItem GetMainMenuItem()
		{
			return m_PlgInMainMenuItem;
		}

		//Implements IPlugin interface
		public ToolStripMenuItem GetTargetMenuItem(Target t)
		{
			return null;
		}

		//Implements IPlugin interface
		public ScriptInterface GetScriptInterface()
		{
			return m_ScriptInterface;
		}

		//Implements IPlugin interface
		public string Name
		{
			get{return "Watson";}
		}

		public void ClearAll()
		{
			foreach(WatsonTabPage wtp in m_TabPages)
			{
				wtp.LogView.Clear();
			}
		}

		public void SetAllEnabled(bool enabled)
		{
			foreach(WatsonTabPage wtp in m_TabPages)
			{
				wtp.LogView.Enabled = enabled;
			}
		}

		public Target Target
		{
			get
			{
				WatsonTabPage wtp = ActiveTabPage;
				return (null != wtp) ? wtp.Target : null;
			}
		}

		internal bool AddLog(string name)
		{
			Target t = m_PluginHost.GetTarget(name);

			if(null != t)
			{
				this.AddTarget(t);
			}

			return (null != t);
		}

		internal void RemoveLog(string name)
		{
			Target t = m_PluginHost.GetTarget(name);

			if(null != t)
			{
				this.RemoveTarget(t);
			}
		}

		LogView ActiveLogView
		{
			get
			{
				return (null != m_TabControl.SelectedTab)
						? ((WatsonTabPage) m_TabControl.SelectedTab).LogView
						: null;
			}
		}

		WatsonTabPage ActiveTabPage
		{
			get
			{
				return (m_TabPages.Count > 0)
						? (WatsonTabPage) m_TabControl.SelectedTab
						: null;
			}
		}

		WatsonTabPage GetTabPage(Target t)
		{
			WatsonTabPage wtp = null;

			foreach(WatsonTabPage tmp in m_TabPages)
			{
				if(tmp.Target == t)
				{
					wtp = tmp;
					break;
				}
			}

			return wtp;
		}

		void ShowFindDialog()
		{
			if(null != this.ActiveLogView)
			{
				Form topForm = m_TabControl.TopLevelControl.FindForm();
				if(null != topForm
					&& topForm.Visible
					&& topForm.WindowState != FormWindowState.Minimized)
				{
					m_FindDlg.SetLogView(this.ActiveLogView);
					m_FindDlg.Text = "Find - " + this.Target.Name;
					m_FindDlg.TextToFind = this.ActiveLogView.SelectedText;
					m_FindDlg.Visible = false;
					m_FindDlg.Show(topForm);
				}
			}
		}

		void ShowFilterDialog()
		{
			if(null != this.ActiveLogView)
			{
				Form topForm = m_TabControl.TopLevelControl.FindForm();
				if(null != topForm
					&& topForm.Visible
					&& topForm.WindowState != FormWindowState.Minimized)
				{
					m_FilterDlg.SetLog(this.ActiveLogView.Log);
					m_FilterDlg.Text = "Filter - " + this.Target.Name;
					m_FilterDlg.FilterText = this.ActiveLogView.Log.Filter.Text;
					m_FilterDlg.Visible = false;
					m_FilterDlg.Show(topForm);
				}
			}
		}
	}
}
