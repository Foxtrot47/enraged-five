@echo off
setlocal enabledelayedexpansion

REM -- Put the Windows executable directory to front of PATH so things like find, don't clash with Cygwin
PATH=%ComSpec:cmd.exe=%;%PATH%

REM -- Clear any PWD environment variable that may have been set by Cygwin
set PWD=

REM -- Validate command line args
if "%1"=="Debug"   goto args_ok
if "%1"=="debug"   goto args_ok
if "%1"=="Release" goto args_ok
if "%1"=="release" goto args_ok
goto usage
:args_ok

REM -- Validate environment variables
if "%RS_TOOLSROOT%"=="" goto no_rs_toolsroot


set src=%~dp0RockstarTargetManager\bin\x64\%1
set dst=%RS_TOOLSROOT%\bin\RockstarTargetManager
echo Updating "%src%" -^> "%dst%"
echo.

echo Deleting unused files...
call :recursive_delete %dst%
echo.

echo Editting changed files...
call :recursive_edit   %src%
echo.

echo Adding new files...
call :recursive_add    %src%
echo.

goto :EOF



:usage
echo "usage: CopyToTools build_type"
echo "where build_type = Debug or Release"
goto :EOF

:no_rs_toolsroot
echo "environment variable RS_TOOLSROOT not set"
goto :EOF

:recursive_delete
pushd %1
set prefix=%2\
if "%prefix%"=="\" set prefix=
for %%i in (*.exe *.dll) do (
	set tmp=%%i
	set tmp=!tmp:.rtmws=IGNORE_THIS_FILE!
	if exist !tmp! (
		if not exist %src%\%prefix%%%i p4 delete "%prefix%%%i"
	)
)
for /d %%i in (*) do (
	call :recursive_delete %%i %prefix%%%i
)
popd
goto :EOF

:recursive_edit
pushd %1
set prefix=%2\
if "%prefix%"=="\" set prefix=
for %%i in (*.exe *.dll) do (
	if exist %dst%\%prefix%%%i (
		set same=0
		for /f "usebackq" %%j in (`fc /b "%src%\%prefix%%%i" "%dst%\%prefix%%%i" ^| find "FC: no differences encountered"`) do set same=1
		if "!same!" == "0" (
			p4 edit "%dst%\%prefix%%%i"
			copy /v "%src%\%prefix%%%i" "%dst%\%prefix%%%i"
		)
	)
)
for /d %%i in (*) do (
	call :recursive_edit %%i %prefix%%%i
)
popd
goto :EOF

:recursive_add
pushd %1
set prefix=%2\
if "%prefix%"=="\" set prefix=
for %%i in (*.exe *.dll) do (
	set tmp=%%i
	set tmp=!tmp:.vshost.exe=IGNORE_THIS_FILE!
	if exist !tmp! (
		if not exist %dst%\%prefix%%%i (
			copy /v "%src%\%prefix%%%i" "%dst%\%prefix%%%i"
			p4 add "%dst%\%prefix%%%i"
		)
	)
)
for /d %%i in (*) do (
	call :recursive_add %%i %prefix%%%i
)
popd
goto :EOF
