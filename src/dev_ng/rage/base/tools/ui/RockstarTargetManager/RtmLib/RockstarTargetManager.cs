using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using LogJam;

namespace Rockstar.TargetManager
{
	////////////////////////////////////////////////////////////////////////////
	// RtmEventType
	////////////////////////////////////////////////////////////////////////////
	public enum RtmEventType
	{
		TargetAdded,
		TargetRemoved
	}

	////////////////////////////////////////////////////////////////////////////
	// RtmEventArgs
	////////////////////////////////////////////////////////////////////////////
	public class RtmEventArgs : EventArgs
	{
		private RtmEventType m_EventType;

		////////////////////////////////////////////////////////////////////////
		// RtmEventArgs
		////////////////////////////////////////////////////////////////////////
		protected RtmEventArgs(RtmEventType eventType)
		{
			m_EventType = eventType;
		}

		////////////////////////////////////////////////////////////////////////
		// EventType
		////////////////////////////////////////////////////////////////////////
		public RtmEventType EventType
		{
			get{return m_EventType;}
		}

		////////////////////////////////////////////////////////////////////////
		// CreateTargetAdded
		////////////////////////////////////////////////////////////////////////
		internal static RtmTargetEventArgs CreateTargetAdded(string name)
		{
			return new RtmTargetEventArgs(RtmEventType.TargetAdded, name);
		}

		////////////////////////////////////////////////////////////////////////
		// CreateTargetRemoved
		////////////////////////////////////////////////////////////////////////
		internal static RtmTargetEventArgs CreateTargetRemoved(string name)
		{
			return new RtmTargetEventArgs(RtmEventType.TargetRemoved, name);
		}
	}


	////////////////////////////////////////////////////////////////////////////
	// RtmTargetEventArgs
	////////////////////////////////////////////////////////////////////////////
	public class RtmTargetEventArgs : RtmEventArgs
	{
		private string m_TargetName;

		////////////////////////////////////////////////////////////////////////
		// RtmTargetEventArgs
		////////////////////////////////////////////////////////////////////////
		internal RtmTargetEventArgs(RtmEventType eventType, string name) : base(eventType)
		{
			m_TargetName = name;
		}

		////////////////////////////////////////////////////////////////////////
		// TargetName
		////////////////////////////////////////////////////////////////////////
		public string TargetName
		{
			get{return m_TargetName;}
		}
	}

	////////////////////////////////////////////////////////////////////////////
	// RockstarTargetManager
	////////////////////////////////////////////////////////////////////////////
	public class RockstarTargetManager : IDisposable
	{
		private Workspace m_Workspace = new Workspace();
		private string m_WsFilename = "";
		private string m_WorkingDir = "";
		private RtmPluginHost m_PluginHost;
		private RtmScriptHost m_ScriptHost;
		private ScriptEngine m_ScriptEngine;
		private List<Target> m_Targets = new List<Target>();
		private Dictionary<string, Target> m_TargetsByName = new Dictionary<string, Target>();
		private IEnumerable<IPlugin> m_Plugins;
		private LogJam.WeakEvent<RtmEventArgs> m_Event;
		private NetworkTtyListener m_NetworkTtyListener;
		private PS3TargetManager  m_PS3TargetManager;
		private PS4TargetManager  m_PS4TargetManager;
		private XboxTargetManager m_XboxTargetManager;
		private XB1TargetManager  m_XB1TargetManager;
		private PCTargetManager   m_PCTargetManager;


		////////////////////////////////////////////////////////////////////////
		// RtmPluginHost
		////////////////////////////////////////////////////////////////////////
		//Passed to plugins' Init().
		private class RtmPluginHost : IPluginHost
		{
			RockstarTargetManager m_Rtm;

			////////////////////////////////////////////////////////////////////
			// RtmPluginHost
			////////////////////////////////////////////////////////////////////
			public RtmPluginHost(RockstarTargetManager rtm)
			{
				m_Rtm = rtm;
			}

			////////////////////////////////////////////////////////////////////
			// GetTarget
			////////////////////////////////////////////////////////////////////
			public Target GetTarget(string name)
			{
				return m_Rtm.GetTarget(name);
			}

			////////////////////////////////////////////////////////////////////
			// WriteLine
			////////////////////////////////////////////////////////////////////
			public void WriteLine(string s)
			{
				Trace.WriteLine(s);
			}
		}

		////////////////////////////////////////////////////////////////////////
		// RtmScriptHost
		////////////////////////////////////////////////////////////////////////
		//Passed to ScriptEngine's Init().
		private class RtmScriptHost : ScriptContextBase.IScriptHostImpl
		{
			RockstarTargetManager m_Rtm;
			List<ScriptContextBase.Target> m_Targets =
				new List<ScriptContextBase.Target>();
			Dictionary<string, ScriptContextBase.Target> m_TargetsByName =
				new Dictionary<string, ScriptContextBase.Target>();


			////////////////////////////////////////////////////////////////////
			// RtmScriptHost
			////////////////////////////////////////////////////////////////////
			public RtmScriptHost(RockstarTargetManager rtm)
			{
				m_Rtm = rtm;
			}

			////////////////////////////////////////////////////////////////////
			// WriteLine
			////////////////////////////////////////////////////////////////////
			public void WriteLine(string s)
			{
				Trace.WriteLine("[Script] " + s);
			}

			////////////////////////////////////////////////////////////////////
			// LoadWorkspace
			////////////////////////////////////////////////////////////////////
			public void LoadWorkspace(string filename)
			{
				m_Rtm.LoadWorkspace(filename);
			}

			////////////////////////////////////////////////////////////////////
			// AddTarget
			////////////////////////////////////////////////////////////////////
			public bool AddTarget(string name, TargetPlatformId platformId)
			{
				return m_Rtm.AddTarget(name, platformId);
			}

			////////////////////////////////////////////////////////////////////
			// RemoveTarget
			////////////////////////////////////////////////////////////////////
			public void RemoveTarget(string name)
			{
				m_Rtm.RemoveTarget(name);
			}

			////////////////////////////////////////////////////////////////////
			// GetTarget
			////////////////////////////////////////////////////////////////////
			public Target GetTarget(string name)
			{
				return m_Rtm.GetTarget(name);
			}

			////////////////////////////////////////////////////////////////////
			// DeployAll
			////////////////////////////////////////////////////////////////////
			public void DeployAll(TargetPlatformId platformId)
			{
				m_Rtm.DeployAll(platformId);
			}

			////////////////////////////////////////////////////////////////////
			// LaunchAll
			////////////////////////////////////////////////////////////////////
			public void LaunchAll(TargetPlatformId platformId)
			{
				m_Rtm.LaunchAll(platformId);
			}

			////////////////////////////////////////////////////////////////////
			// RebootAll
			////////////////////////////////////////////////////////////////////
			public void RebootAll(TargetPlatformId platformId)
			{
				m_Rtm.RebootAll(platformId);
			}

			////////////////////////////////////////////////////////////////////
			// StopAll
			////////////////////////////////////////////////////////////////////
			public void StopAll(TargetPlatformId platformId)
			{
				m_Rtm.StopAll(platformId);
			}

			////////////////////////////////////////////////////////////////////
			// StartAll
			////////////////////////////////////////////////////////////////////
			public void StartAll(TargetPlatformId platformId)
			{
				m_Rtm.StartAll(platformId);
			}

			////////////////////////////////////////////////////////////////////
			// Targets
			////////////////////////////////////////////////////////////////////
			public IEnumerable<ScriptContextBase.Target> Targets
			{
				get{return m_Targets;}
			}

			////////////////////////////////////////////////////////////////////
			// GetCommandLine
			////////////////////////////////////////////////////////////////////
			public string GetCommandLine(TargetPlatformId platformId)
			{
				return m_Rtm.Workspace.Platforms[platformId].CommandLine;
			}

			////////////////////////////////////////////////////////////////////
			// SetCommandLine
			////////////////////////////////////////////////////////////////////
			public void SetCommandLine(TargetPlatformId platformId, string str)
			{
				 m_Rtm.Workspace.Platforms[TargetPlatformId.Xbox360].CommandLine = str;
			}

			////////////////////////////////////////////////////////////////////
			// AddTarget
			////////////////////////////////////////////////////////////////////
			internal void AddTarget(Target target)
			{
				ScriptContextBase.Target scTarget;

				if(!m_TargetsByName.ContainsKey(target.Name))
				{
					scTarget = new ScriptContextBase.Target(target);
					m_Targets.Add(scTarget);
					m_TargetsByName.Add(target.Name, scTarget);
				}
			}

			////////////////////////////////////////////////////////////////////
			// RemoveTarget
			////////////////////////////////////////////////////////////////////
			internal void RemoveTarget(Target target)
			{
				ScriptContextBase.Target scTarget;
				if(m_TargetsByName.TryGetValue(target.Name, out scTarget))
				{
					m_Targets.Remove(scTarget);
					m_TargetsByName.Remove(target.Name);
				}
			}
		}

		////////////////////////////////////////////////////////////////////////
		// RockstarTargetManager
		////////////////////////////////////////////////////////////////////////
		public RockstarTargetManager(string startupPath)
		{
			m_NetworkTtyListener = new NetworkTtyListener();

			m_PS3TargetManager  = new PS3TargetManager (m_NetworkTtyListener);
			m_PS4TargetManager  = new PS4TargetManager (m_NetworkTtyListener);
			m_XboxTargetManager = new XboxTargetManager(m_NetworkTtyListener);
			m_XB1TargetManager  = new XB1TargetManager (m_NetworkTtyListener);
			m_PCTargetManager   = new PCTargetManager  (m_NetworkTtyListener);

			m_Event = new LogJam.WeakEvent<RtmEventArgs>(this);

			m_WorkingDir = string.IsNullOrEmpty(startupPath) ? "" : startupPath;

			m_ScriptHost = new RtmScriptHost(this);
			m_ScriptEngine = new ScriptEngine(m_ScriptHost, m_WorkingDir);

			this.LoadPlugins();

			//Add default targets

			foreach(string tname in m_PS3TargetManager.GetDefaultTargetNames())
			{
			   this.AddTarget(tname, TargetPlatformId.PS3);
			}

			foreach(string tname in m_PS4TargetManager.GetDefaultTargetNames())
			{
			   this.AddTarget(tname, TargetPlatformId.PS4);
			}

			foreach(string tname in m_XboxTargetManager.GetDefaultTargetNames())
			{
			   this.AddTarget(tname, TargetPlatformId.Xbox360);
			}

			foreach(string tname in m_XB1TargetManager.GetDefaultTargetNames())
			{
				this.AddTarget(tname, TargetPlatformId.XB1);
			}

			foreach(string tname in m_PCTargetManager.GetDefaultTargetNames())
			{
				this.AddTarget(tname, TargetPlatformId.PC);
			}

			m_Workspace.Event.AddHandler(this.OnWorkspaceEvent);
		}

		////////////////////////////////////////////////////////////////////////
		// ~RockstarTargetManager
		////////////////////////////////////////////////////////////////////////
		~RockstarTargetManager()
		{
			Dispose(false);
		}

		////////////////////////////////////////////////////////////////////////
		// Dispose
		////////////////////////////////////////////////////////////////////////
		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		////////////////////////////////////////////////////////////////////////
		// Dispose
		////////////////////////////////////////////////////////////////////////
		protected virtual void Dispose(bool disposing)
		{
			if(disposing)
			{
				if(m_PS3TargetManager !=null)	m_PS3TargetManager .Dispose();
				if(m_PS4TargetManager !=null)	m_PS4TargetManager .Dispose();
				if(m_XboxTargetManager!=null)	m_XboxTargetManager.Dispose();
				if(m_XB1TargetManager !=null)	m_XB1TargetManager .Dispose();
				if(m_PCTargetManager  !=null)   m_PCTargetManager  .Dispose();

				if(m_NetworkTtyListener != null)    m_NetworkTtyListener.Dispose();

				UnloadPlugins();
			}
		}

		////////////////////////////////////////////////////////////////////////
		// LoadWorkspace
		////////////////////////////////////////////////////////////////////////
		public bool LoadWorkspace(string filename)
		{
			bool success = false;

			Trace.WriteLine("Loading workspace: " + filename + "...");

			string oldFn = m_WsFilename;

			m_WsFilename = filename;

			if(m_Workspace.Load(filename))
			{
                // Acquire targets in the local settings but not enumerated already
                Trace.WriteLine("Acquiring targets from local settings...");
                foreach (LocalTargetSettings lts in m_Workspace.LocalSettings)
                {
					TargetPlatformId platform = Configuration.PlatformIdFromName(lts.Platform);
					Target target = null;
					if (platform != TargetPlatformId.Unknown && lts.TargetName != null && !m_TargetsByName.TryGetValue(lts.TargetName, out target))
                    {
                        AddTarget(lts.TargetName, platform);
                    }
                }

				success = true;
			}
			else
			{
				m_WsFilename = oldFn;
			}

			return success;
		}

		////////////////////////////////////////////////////////////////////////
		// ReloadSolution
		////////////////////////////////////////////////////////////////////////
		public bool ReloadSolution(string slnFileName)
		{
			bool success = false;

			IEnumerable<Configuration> configs =
				VisualStudioSolution.LoadSolution(slnFileName);

			if(null != configs)
			{
				m_Workspace.SetConfigs(configs);

				success = true;
			}

			return success;
		}

		////////////////////////////////////////////////////////////////////////
		// SaveWorkspace
		////////////////////////////////////////////////////////////////////////
		public bool SaveWorkspace(string filename)
		{
			bool success = false;

			Trace.WriteLine("Saving workspace " + filename + "...");

			string oldFn = m_WsFilename;

			m_WsFilename = filename;

			if(m_Workspace.Save(filename))
			{
				success = true;
			}
			else
			{
				m_WsFilename = oldFn;
			}

			return success;
		}

		////////////////////////////////////////////////////////////////////////
		// GetTarget
		////////////////////////////////////////////////////////////////////////
		public Target GetTarget(string name)
		{
			Target target;

			if(!m_TargetsByName.TryGetValue(name, out target))
			{
				target = null;
			}

			return target;
		}

		////////////////////////////////////////////////////////////////////////
		// DeployAll
		////////////////////////////////////////////////////////////////////////
		public void DeployAll(TargetPlatformId platformId)
		{
			foreach(Target t in m_Targets)
			{
				if(t.PlatformId == platformId && t.EnableDeploy)
				{
					t.Deploy(DeployType.Force);
				}
			}
		}

		////////////////////////////////////////////////////////////////////////
		// LaunchAll
		////////////////////////////////////////////////////////////////////////
		public void LaunchAll(TargetPlatformId platformId)
		{
			foreach(Target t in m_Targets)
			{
				if(t.PlatformId == platformId && t.EnableLaunch)
				{
					t.Launch();
				}
			}
		}

		////////////////////////////////////////////////////////////////////////
		// RebootAll
		////////////////////////////////////////////////////////////////////////
		public void RebootAll(TargetPlatformId platformId)
		{
			foreach(Target t in m_Targets)
			{
				if(t.PlatformId == platformId && t.EnableReboot)
				{
					t.Reboot();
				}
			}
		}

		////////////////////////////////////////////////////////////////////////
		// FastRebootAll
		////////////////////////////////////////////////////////////////////////
		public void FastRebootAll(TargetPlatformId platformId)
		{
			foreach(Target t in m_Targets)
			{
				if(t.PlatformId == platformId)
				{
					if(t.EnableFastReboot)
					{
						t.FastReboot();
					}
					else if(t.EnableReboot)
					{
						t.Reboot();
					}
				}
			}
		}

		////////////////////////////////////////////////////////////////////////
		// StopAll
		////////////////////////////////////////////////////////////////////////
		public void StopAll(TargetPlatformId platformId)
		{
			foreach(Target t in m_Targets)
			{
				if(t.PlatformId == platformId && t.EnableStopGo && !t.Stopped)
				{
					t.Stop();
				}
			}
		}

		////////////////////////////////////////////////////////////////////////
		// StartAll
		////////////////////////////////////////////////////////////////////////
		public void StartAll(TargetPlatformId platformId)
		{
			foreach(Target t in m_Targets)
			{
				if(t.PlatformId == platformId && t.EnableStopGo && t.Stopped)
				{
					t.Go();
				}
			}
		}

		////////////////////////////////////////////////////////////////////////
		// AddTarget
		////////////////////////////////////////////////////////////////////////
		public bool AddTarget(string name, TargetPlatformId platformId)
		{
			Target target = null;

			if(!m_TargetsByName.TryGetValue(name, out target))
			{
				target = null;

				if(TargetPlatformId.PS3 == platformId)
				{
					target = m_PS3TargetManager.AddTarget(name);
				}
				else if(TargetPlatformId.PS4 == platformId)
				{
					target = m_PS4TargetManager.AddTarget(name);
				}
				else if(TargetPlatformId.Xbox360 == platformId)
				{
					target = m_XboxTargetManager.AddTarget(name);
				}
				else if(TargetPlatformId.XB1 == platformId)
				{
					target = m_XB1TargetManager.AddTarget(name);
				}
				else if(TargetPlatformId.PC == platformId)
				{
					target = m_PCTargetManager.AddTarget(name);
				}

				if (null == target)
				{
					Trace.WriteLine("Failed to connect to " + Configuration.PlatformNameFromId(platformId) +
						" target " + name);
				}
				else
				{
					m_Workspace.LocalSettings[name].Platform = Configuration.PlatformNameFromId(platformId);

					// The platform specific target manager may have returned a
					// pre-existng target that was aliased with another name.
					// Check to ensure we don't re-add the same target twice.
					Target dup = null;
					if (m_TargetsByName.TryGetValue(target.Name, out dup))
					{
						Debug.Assert(target.Name != name);
						Debug.Assert(dup == target);
					}
					name = target.Name;

					if(null == dup)
					{
						m_Targets.Add(target);
						m_TargetsByName.Add(name, target);

						Platform platform = m_Workspace.Platforms[platformId];
						target.Config = platform.MasterConfig;
						this.UpdateCommandLine(target.Name);

						// XB1 has its own special case handling of the target enable flag
						if(platformId != TargetPlatformId.XB1)
							this.UpdateEnabled(target.Name);

						m_ScriptHost.AddTarget(target);

						//Tell the plugins about the new target.
						if(null != m_Plugins)
						{
							foreach(IPlugin plugin in m_Plugins)
							{
								plugin.AddTarget(target);
							}
						}

						m_Event.Invoke(RtmEventArgs.CreateTargetAdded(name));
					}
				}
			}

			return (null != target);
		}

		////////////////////////////////////////////////////////////////////////
		// RemoveTarget
		////////////////////////////////////////////////////////////////////////
		public void RemoveTarget(string name)
		{
			Target target;

			if(m_TargetsByName.TryGetValue(name, out target))
			{
				m_Targets.Remove(target);
				m_TargetsByName.Remove(name);
				m_ScriptHost.RemoveTarget(target);
				m_Workspace.LocalSettings.Remove(name);
				m_Workspace.IsDirty = true;

				//Remove the target from each of the plugins.
				if(null != m_Plugins)
				{
					foreach(IPlugin plugin in m_Plugins)
					{
						plugin.RemoveTarget(target);
					}
				}

				m_Event.Invoke(RtmEventArgs.CreateTargetRemoved(name));
			}
		}

		////////////////////////////////////////////////////////////////////////
		// Event
		////////////////////////////////////////////////////////////////////////
		public LogJam.WeakEvent<RtmEventArgs> Event
		{
			get{return m_Event;}
		}

		////////////////////////////////////////////////////////////////////////
		// Workspace
		////////////////////////////////////////////////////////////////////////
		public Workspace Workspace
		{
			get{return m_Workspace;}
		}

		////////////////////////////////////////////////////////////////////////
		// WorkspaceFileName
		////////////////////////////////////////////////////////////////////////
		public string WorkspaceFileName
		{
			get { return m_WsFilename; }
			set {m_WsFilename = value;}
		}

		////////////////////////////////////////////////////////////////////////
		// Targets
		////////////////////////////////////////////////////////////////////////
		public IEnumerable<Target> Targets
		{
			get{return m_Targets;}
		}

		////////////////////////////////////////////////////////////////////////
		// Plugins
		////////////////////////////////////////////////////////////////////////
		public IEnumerable<IPlugin> Plugins
		{
			get{return m_Plugins;}
		}

		////////////////////////////////////////////////////////////////////////
		// ScriptEngine
		////////////////////////////////////////////////////////////////////////
		public ScriptEngine ScriptEngine
		{
			get{return m_ScriptEngine;}
		}

		////////////////////////////////////////////////////////////////////////
		// GetCrashdumpDir
		////////////////////////////////////////////////////////////////////////
		// Get the directory specified by RAGE_CRASHDUMP_DIR environment
		// variable, and ensure it exists. String garunteed to end with a
		// backslash.  If RAGE_CRASHDUMP_DIR is not set, then it is forced to
		// the default, "x:\\dumps\\".
		public static string GetCrashdumpDir()
		{
			const string ENV_VAR = "RAGE_CRASHDUMP_DIR";
			string dir;
			dir = Environment.GetEnvironmentVariable(ENV_VAR, EnvironmentVariableTarget.Process);
			if(dir == null)
			{
				dir = Environment.GetEnvironmentVariable(ENV_VAR, EnvironmentVariableTarget.User);
				if(dir == null)
				{
					dir = Environment.GetEnvironmentVariable(ENV_VAR, EnvironmentVariableTarget.Machine);
					if(dir == null)
					{
						dir = "x:\\dumps\\";
						Trace.WriteLine("RAGE_CRASHDUMP_DIR environment variable not set, setting to default \""+dir+"\".");
						Environment.SetEnvironmentVariable(ENV_VAR, dir, EnvironmentVariableTarget.User);
						Environment.SetEnvironmentVariable(ENV_VAR, dir, EnvironmentVariableTarget.Process);
					}
				}
			}

			if(dir[dir.Length-1] != '\\')
			{
				dir += "\\";
			}
			if(!Directory.Exists(dir))
			{
				try
				{
					Directory.CreateDirectory(dir);
				}
				catch
				{
					Trace.WriteLine("Unable to create directory \""+dir+"\" (specified by RAGE_CRASHDUMP_DIR environment variable).");
					dir = null;
				}
			}
			return dir;
		}

		////////////////////////////////////////////////////////////////////////
		// LoadPlugins
		////////////////////////////////////////////////////////////////////////
		private void LoadPlugins()
		{
			m_PluginHost = new RtmPluginHost(this);
			m_Plugins = PluginLoader<IPlugin>.LoadPlugins(m_WorkingDir);

			foreach(IPlugin plugin in m_Plugins)
			{
				plugin.Init(m_PluginHost);

				ScriptInterface siface = plugin.GetScriptInterface();
				if(null != siface)
				{
					m_ScriptEngine.AddInterface(siface);
				}
			}
		}

		////////////////////////////////////////////////////////////////////////
		// UnloadPlugins
		////////////////////////////////////////////////////////////////////////
		private void UnloadPlugins()
		{
			foreach(IPlugin plugin in m_Plugins)
			{
				ScriptInterface siface = plugin.GetScriptInterface();
				if(null != siface)
				{
					m_ScriptEngine.RemoveInterface(siface);
				}

				plugin.Shutdown();
			}
		}

		////////////////////////////////////////////////////////////////////////
		// OnWorkspaceEvent
		////////////////////////////////////////////////////////////////////////
		private void OnWorkspaceEvent(object sender, WorkspaceEventArgs args)
		{
			if(WorkspaceEventType.MasterConfigChanged == args.EventType)
			{
				Configuration masterConfig = m_Workspace.Platforms[args.PlatformId].MasterConfig;

				foreach(Target t in m_Targets)
				{
					if(t.PlatformId == args.PlatformId)
					{
						t.Config = masterConfig;
					}
				}
			}
			else if(WorkspaceEventType.GlobalCommandLineChanged == args.EventType)
			{
				foreach(Target t in m_Targets)
				{
					if(t.PlatformId == args.PlatformId)
					{
						this.UpdateCommandLine(t.Name);
					}
				}
			}
			else if (WorkspaceEventType.LocalCommandLineChanged == args.EventType)
			{
				this.UpdateCommandLine(args.TargetName);
			}
			else if (WorkspaceEventType.EnabledChanged == args.EventType)
			{
				this.UpdateEnabled(args.TargetName);
			}
		}

		////////////////////////////////////////////////////////////////////////
		// UpdateCommandLine
		////////////////////////////////////////////////////////////////////////
		private void UpdateCommandLine(string targetName)
		{
			Target target = this.GetTarget(targetName);

			if(null != target)
			{
				LocalCommandLine lcl =
					m_Workspace.LocalSettings[targetName].CommandLine;
				string cmdLine = lcl.ToString();
				if(lcl.AppendToGlobal)
				{
					cmdLine =
						m_Workspace.Platforms[target.PlatformId].CommandLine
						+ " "
						+ cmdLine;
				}

				target.CommandLine = cmdLine;
			}
		}

		////////////////////////////////////////////////////////////////////////
		// UpdateEnabled
		////////////////////////////////////////////////////////////////////////
		private void UpdateEnabled(string targetName)
		{
			Target target = this.GetTarget(targetName);
			if (null != target)
			{
				target.Enabled = m_Workspace.LocalSettings[targetName].Enabled;
			}
		}
	}
}
