using System;
using System.Reflection;
using System.IO;
using System.Diagnostics;
using System.Collections.Generic;

namespace Rockstar.TargetManager
{
    //Plugin host interface.  Instances of this are passed to
    //plugins' Init() method.
    public interface IPluginHost
    {
        Target GetTarget(string name);

        void WriteLine(string s);
    }

    //To qualify as a plugin a DLL must contain one or more classes that
    //implement this interface.
    public interface IPlugin
    {
        //Initializes the plugin.
        bool Init(IPluginHost host);

        //Shuts down the plugin.
        void Shutdown();

        //Adds a managed target.
        void AddTarget(Target t);

        //Removes a managed target.
        void RemoveTarget(Target t);

        //Returns the control that's displayed in the main RTM panel.
        //Returns null if no control should be displayed.
        System.Windows.Forms.Control GetControl();

        //Returns the menu that should be displayed in the main RTM
        //tool strip menu.
        //Returns null if no menu should be displayed.
        System.Windows.Forms.ToolStripMenuItem GetMainMenuItem();

        //Returns the menu that should be displayed in the target's
        //context (right-click) menu.
        //Returns null if no menu should be displayed.
        System.Windows.Forms.ToolStripMenuItem GetTargetMenuItem(Target t);

        //Returns an interface that will be made accessible to scripts.
        //Returns null if there's no script interface.
        ScriptInterface GetScriptInterface();

        //Returns the name of the plugin.
        string Name{get;}
    }

    //PURPOSE:
    //  Used to find DLLs that contain implementations of the plugin interface.
    class PluginChecker<IFace> : MarshalByRefObject
    {
        public bool HasPlugins(string path)
        {
            bool hasPlugins = false;

            try
            {
                Assembly ass = Assembly.LoadFile(path);

                if(null != ass)
                {
                    foreach(Type t in ass.GetExportedTypes())
                    {
                        if(null != t.GetInterface(typeof(IFace).FullName)
                            //Don't load the interface itself.  This happens when
                            //a plugin defines a plugin interface and tries to load
                            //other plugins.
                            && !t.Equals(typeof(IFace)))
                        {
                            hasPlugins = true;
                            break;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Trace.WriteLine(e.ToString());
            }

            return hasPlugins;
        }
    }

    //PURPOSE:
    //  Implements loading of plugins.
    //  Plugins are classes contained in DLLs that implement the interface
    //  defined by the IFace template parameter.
    public class PluginLoader<IFace>
    {
        //PURPOSE
        //  Searches the application's startup directory for and loads DLLs
        //  that contain classes that implement IFace.
        //RETURNS
        //  A (possibly empty) collection of IFace instances.
        static public IEnumerable<IFace> LoadPlugins(string startupPath)
        {
            List<IFace> plugins = new List<IFace>();

            string pluginDir = startupPath + "/plugins";

            if(Directory.Exists(pluginDir))
            {
                string[] pluginFiles = Directory.GetFiles(pluginDir, "*.dll");

                //Once loaded DLLs cannot be unloaded from an AppDomain.
                //However, in order to check DLLs for plugins we must load
                //them.  We want to avoid irreversibly loading DLLs that don't
                //contain plugins so we create a separate AppDomain in which
                //to load them.  When we're done we simply release the AppDomain.

                //Configuration for the AppDomains into which we'll load
                //assemblies in order to check them for interesting plugins.
                AppDomainSetup setup = new AppDomainSetup();
                setup.ApplicationName = "Plugins";
                setup.ApplicationBase = Environment.CurrentDirectory;
                setup.PrivateBinPath =  
                    Path.GetDirectoryName(pluginDir).Substring(
                    Path.GetDirectoryName(pluginDir).LastIndexOf(
                    Path.DirectorySeparatorChar) + 1);
                //setup.CachePath = Path.Combine(pluginDir, "cache" + Path.DirectorySeparatorChar);
                //setup.ShadowCopyFiles = "true";
                //setup.ShadowCopyDirectories = pluginDir;

                Type checkerType = typeof(PluginChecker<IFace>);

                foreach(string plgFn in pluginFiles)
                {
                    //Before loading the assembly check if it contains
                    //any interesting plugins.  We do this in a separate
                    //AppDomain so we can unload the assemblies after checking
                    //them.  If we loaded the assemblies in our AppDomain
                    //we would be unable to unload them.
                    AppDomain appDomain = AppDomain.CreateDomain("Plugins", null, setup);
                    PluginChecker<IFace> checker =
                        (PluginChecker<IFace>) appDomain.CreateInstanceAndUnwrap(checkerType.Assembly.FullName, checkerType.FullName);

                    bool hasPlugins = checker.HasPlugins(plgFn);

                    //Unload the assemblies.
                    AppDomain.Unload(appDomain);

                    if(hasPlugins)
                    {
                        try
                        {
                            Assembly ass = Assembly.LoadFile(plgFn);

                            if(null != ass)
                            {
                                foreach(Type t in ass.GetExportedTypes())
                                {
                                    if(null != t.GetInterface(typeof(IFace).FullName))
                                    {
                                        Trace.WriteLine("Loading plugin " + t.FullName + "...");
                                        IFace plg = (IFace) Activator.CreateInstance(t);
                                        plugins.Add(plg);
                                    }
                                }
                            }
                        }
                        catch (Exception e)
                        {
                            Trace.WriteLine(e.ToString());
                        }
                    }
                }
            }

            return plugins;
        }
    }
}
