using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;

namespace Rockstar.TargetManager
{
	////////////////////////////////////////////////////////////////////////////
	// NetworkTtyListener
	////////////////////////////////////////////////////////////////////////////
	public class NetworkTtyListener : IDisposable
	{
		private class ThreadAndExitFlag
		{
			internal Thread         m_Thread;
			internal volatile bool  m_ExitFlag;
		}

		private Object m_LockObj = new Object();
		private TcpListener m_Server;
		private Thread m_ServerWorkerThread;
		private volatile bool m_ServerWorkerThreadExit;
		private Dictionary<string, Target> m_RegisteredTargets = new Dictionary<string, Target>();
		private Dictionary<Target, ThreadAndExitFlag> m_ConnectionThreads = new Dictionary<Target, ThreadAndExitFlag>();

		// If this is changed, be sure to also change "${RS_BUILDBRANCH}/game_*_common.bat".
		public const int PORT = 1895;

		////////////////////////////////////////////////////////////////////////
		// NetworkTtyListener
		////////////////////////////////////////////////////////////////////////
		public NetworkTtyListener()
		{
			m_Server = new TcpListener(HostPcIpAddr.Get(), PORT);
			m_Server.Start();

			m_ServerWorkerThreadExit = false;
			m_ServerWorkerThread = new Thread(ServerWorkerThread);
			m_ServerWorkerThread.Name = "NetworkTtyListener";
			m_ServerWorkerThread.Start();
		}

		////////////////////////////////////////////////////////////////////////
		// ~NetworkTtyListener
		////////////////////////////////////////////////////////////////////////
		~NetworkTtyListener()
		{
			Dispose(false);
		}

		////////////////////////////////////////////////////////////////////////
		// Dispose
		////////////////////////////////////////////////////////////////////////
		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		////////////////////////////////////////////////////////////////////////
		// Dispose
		////////////////////////////////////////////////////////////////////////
		private void Dispose(bool disposing)
		{
			if(disposing)
			{
				if(m_ServerWorkerThread != null)
				{
					m_ServerWorkerThreadExit = true;
					m_ServerWorkerThread.Join();
				}
			}
		}

		////////////////////////////////////////////////////////////////////////
		// LocalEndpoint
		////////////////////////////////////////////////////////////////////////
		public EndPoint LocalEndpoint
		{
			get { return m_Server.LocalEndpoint; }
		}

		////////////////////////////////////////////////////////////////////////
		// Register
		////////////////////////////////////////////////////////////////////////
		public void Register(string ipAddr, Target target)
		{
			lock(m_LockObj)
			{
				m_RegisteredTargets.Remove(ipAddr);
				m_RegisteredTargets.Add(ipAddr, target);
			}
		}

		////////////////////////////////////////////////////////////////////////
		// Deregister
		////////////////////////////////////////////////////////////////////////
		public void Deregister(string ipAddr, Target target)
		{
			lock(m_LockObj)
			{
				m_RegisteredTargets.Remove(ipAddr);
			}
		}

		////////////////////////////////////////////////////////////////////////
		// IsRegistered
		////////////////////////////////////////////////////////////////////////
		public bool IsRegistered(Target target)
		{
			bool found = false;
			lock (m_LockObj)
			{
				foreach (var pair in m_RegisteredTargets)
				{
					if (pair.Value == target)
					{
						found = true;
						break;
					}
				}
			}
			return found;
		}

		////////////////////////////////////////////////////////////////////////
		// ServerWorkerThread
		////////////////////////////////////////////////////////////////////////
		private void ServerWorkerThread()
		{
			var timespan = TimeSpan.FromMilliseconds(50);
			while(!m_ServerWorkerThreadExit)
			{
				try
				{
					using(var task = m_Server.AcceptTcpClientAsync())
					{
						while(!m_ServerWorkerThreadExit)
						{
							if(task.Wait(timespan))
							{
								var connection = task.Result;

								// Don't allow the socket to be inherited.  This is mainly for
								// debugging purposes.  If a KD process is spawned after this socket
								// has been created, then by default it will inherit the socket.  If
								// the main R*TM process is stopped via Visual Studio, then the KD
								// process won't get cleaned up, and therefore, neither will the
								// socket.
								//
								// A bit more info at
								// http://stackoverflow.com/questions/15216881/pid-exists-in-netstat-but-does-not-exist-in-task-manager
								//
								var socketHandle = connection.Client.Handle;
								if(!Win32.SetHandleInformation(socketHandle, Win32.HANDLE_FLAG_INHERIT, 0))
								{
									Trace.WriteLine("SetHandleInformation failed, err=0x"+Marshal.GetLastWin32Error().ToString("x8"));
								}

								var ipAddrPort = connection.Client.RemoteEndPoint.ToString();
								var ipAddr = ipAddrPort;
								var colon = ipAddrPort.IndexOf(':');
								if(colon >= 0)
								{
									ipAddr = ipAddrPort.Substring(0, colon);
								}

								Target target;
								lock(m_LockObj)
								{
									m_RegisteredTargets.TryGetValue(ipAddr, out target);
								}

								if(target == null)
								{
									Trace.WriteLine("Ignoring attempted connection from unknown "+ipAddrPort);
									connection.Close();
								}
								else
								{
									var tef = new ThreadAndExitFlag();
									tef.m_ExitFlag = false;
									var thread = new Thread(() => ConnectionWorkerThread(target, connection, tef));
									tef.m_Thread = thread;
									Trace.WriteLine("Accepted TTY connection from "+ipAddrPort);
									lock(m_LockObj)
									{
										// Close any previous connections
										foreach(var tef2 in m_ConnectionThreads)
										{
											if (tef2.Key == target)
											{
												tef2.Value.m_ExitFlag = true;
												tef2.Value.m_Thread.Join();
												m_ConnectionThreads.Remove(target);
												break;
											}
										}

										m_ConnectionThreads.Add(target, tef);
									}
									thread.Name = "NetworkTty - "+ipAddr;
									thread.Start();

									// Make sure the target is enabled and in the "Go" state; since it's connected, it must be running. Right?
									target.Enabled = true;
									target.Go();
								}

								break;
							}
						}
					}
				}
				catch(InvalidOperationException)
				{
					// Disposing of a non-cancelled Task will throw an
					// InvalidOperationException.  But TcpListener doesn't have
					// a version of AcceptTcpClientAsync that creates a
					// cancellable Task.  So simply eat this exception.
				}
			}

			lock(m_LockObj)
			{
				foreach(var tef in m_ConnectionThreads)
				{
					tef.Value.m_ExitFlag = true;
					tef.Value.m_Thread.Join();
				}
				m_ConnectionThreads.Clear();
			}
		}

		////////////////////////////////////////////////////////////////////////
		// ConnectionWorkerThread
		////////////////////////////////////////////////////////////////////////
		private void ConnectionWorkerThread(Target target, TcpClient connection, ThreadAndExitFlag tef)
		{
			try
			{
				connection.LingerState.Enabled = true;
				connection.LingerState.LingerTime = 0;

				const int BUF_SIZE = 0x100000;
				var buf = new byte[BUF_SIZE];

				var ns = connection.GetStream();

				int begin = 0;
				int end = 0;
				while(!tef.m_ExitFlag && connection.Client.Connected)
				{
					if(!ns.DataAvailable)
					{
						Thread.Sleep(10);
					}
					else
					{
						var num = ns.Read(buf, end, BUF_SIZE-end);
						end += num;
						while(begin+2 <= end)
						{
							// Read little endian u16 length from buffer
							uint len = (uint)buf[begin] + ((uint)buf[begin+1]<<8);

							// Special case commands.
							if(len > 0xfffd)
							{
								// Flush.
								if(len == 0xfffe)
								{
									// Expected packet is 6-bytes.  Wait until full packet is available.
									if(begin+6 > end)
									{
										break;
									}
									// Flush logfile, then echo packet back.
									target.FlushLogFile();
									ns.Write(buf, begin, 6);
									begin += 6;
								}
								// Note that 0xffff (buffer wrap), should never
								// actually get sent.  If data has been
								// corrupted and we see 0xffff, just ignore it.
								else if(len == 0xffff)
								{
									target.TtyOutput("R*TM: Warning corrupted network tty\n");
									begin += 2;
								}
							}

							// Full string available ?
							else if(begin+len <= end)
							{
								uint strlen = len-2-1 - (buf[begin+len-2]==0?1u:0u);
								var str = Encoding.UTF8.GetString(buf, begin+2, (int)strlen);
								target.TtyOutput(str);
								begin += (int)len;
							}

							// Partial string ?
							else
							{
								break;
							}
						}

						// Non-complete string left in the buffer ?
						if(begin > 0)
						{
							// Move it back to the start of the buffer.
							for(int i=0; i<end-begin; ++i)
							{
								buf[i] = buf[begin+i];
							}
							end = end-begin;
							begin = 0;
						}
					}
				}
			}
			catch
			{
			}

			try
			{
				connection.Close();
			}
			catch
			{
			}

			while(!tef.m_ExitFlag)
			{
				if(Monitor.TryEnter(m_LockObj, 1))
				{
					try
					{
						m_ConnectionThreads.Remove(target);
					}
					catch
					{
					}
					Monitor.Exit(m_LockObj);
					break;
				}
			}
		}
	}
}
