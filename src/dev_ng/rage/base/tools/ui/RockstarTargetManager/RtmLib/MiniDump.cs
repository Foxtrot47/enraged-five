using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;

namespace Rockstar.TargetManager
{
	////////////////////////////////////////////////////////////////////////////
	// MiniDump
	////////////////////////////////////////////////////////////////////////////
	public class MiniDump
	{
		private const UInt32 MDMP_SIGNATURE = 0x504d444d; // "MDMP", little endian

		// Copied from DbgHelp.h
		private enum eStreamType
		{
			UnusedStream                = 0,
			ReservedStream0             = 1,
			ReservedStream1             = 2,
			ThreadListStream            = 3,        // used
			ModuleListStream            = 4,        // used
			MemoryListStream            = 5,
			ExceptionStream             = 6,        // used
			SystemInfoStream            = 7,        // used
			ThreadExListStream          = 8,
			Memory64ListStream          = 9,        // used
			CommentStreamA              = 10,
			CommentStreamW              = 11,
			HandleDataStream            = 12,
			FunctionTableStream         = 13,
			UnloadedModuleListStream    = 14,
			MiscInfoStream              = 15,       // used
			MemoryInfoListStream        = 16,
			ThreadInfoListStream        = 17,
			HandleOperationListStream   = 18,
			TokenStream                 = 19
		};

		private struct MiniDumpHeader
		{
			public UInt32 version;
			public UInt32 timeDateStamp;
			public UInt64 flags;
		};
		private MiniDumpHeader m_Header = new MiniDumpHeader();

		private struct MiniDumpDirectory
		{
			public eStreamType streamType;
			public UInt32 dataSize;
			public UInt32 rva;
		};

		private class MiniDumpStreamBase
		{
			public UInt32 sizeDirectory;
			public UInt32 sizeTotal;
		}

		private class MiniDumpGenericStream : MiniDumpStreamBase
		{
			public eStreamType streamType;
			public byte[] data;
		};
		private MiniDumpGenericStream m_MiscInfo = null;

		private class MiniDumpThread
		{
			public UInt32 threadId;
			public UInt32 suspendCount;
			public UInt32 priorityClass;
			public UInt32 priority;
			public UInt64 teb;
			public UInt64 stackStartOfMemoryRange;
			public UInt32 stackDataSize;
			public byte[] threadContext;
		};
		private class MiniDumpThreads : MiniDumpStreamBase
		{
			public List<MiniDumpThread> threads;
			public UInt32               sizeContexts;
		};
		private MiniDumpThreads m_Threads = null;

		private class MiniDumpModule
		{
			public UInt64 baseOfImage;
			public UInt32 sizeOfImage;
			public UInt32 checkSum;
			public UInt32 timeDateStamp;
			public byte[] moduleName;

			// VS_FIXEDFILEINFO versionInfo
			public UInt32 signature;            // e.g. 0xfeef04bd
			public UInt32 strucVersion;         // e.g. 0x00000042 = "0.42"
			public UInt32 fileVersionMS;        // e.g. 0x00030075 = "3.75"
			public UInt32 fileVersionLS;        // e.g. 0x00000031 = "0.31"
			public UInt32 productVersionMS;     // e.g. 0x00030010 = "3.10"
			public UInt32 productVersionLS;     // e.g. 0x00000031 = "0.31"
			public UInt32 fileFlagsMask;        // = 0x3F for version "0.42"
			public UInt32 fileFlags;            // e.g. VFF_DEBUG | VFF_PRERELEASE
			public UInt32 fileOS;               // e.g. VOS_DOS_WINDOWS16
			public UInt32 fileType;             // e.g. VFT_DRIVER
			public UInt32 fileSubtype;          // e.g. VFT2_DRV_KEYBOARD
			public UInt32 fileDateMS;           // e.g. 0
			public UInt32 fileDateLS;           // e.g. 0

			public UInt64 reserved0;
			public UInt64 reserved1;
		};
		private class MiniDumpModules : MiniDumpStreamBase
		{
			public List<MiniDumpModule> modules;
			public UInt32               sizeStrings;
		};
		private MiniDumpModules m_Modules = null;

		private class MiniDumpException : MiniDumpStreamBase
		{
			public const uint EXCEPTION_MAXIMUM_PARAMETERS = 15;

			public UInt32   threadId;
			public UInt32   __alignment;
			public UInt32   exceptionCode;
			public UInt32   exceptionFlags;
			public UInt64   exceptionRecord;
			public UInt64   exceptionAddress;
			public UInt32   numberParameters;
			public UInt32   __unusedAlignment;
			public UInt64[] exceptionInformation = new UInt64[EXCEPTION_MAXIMUM_PARAMETERS];
			public byte[]   threadContext;
		};
		private MiniDumpException m_Exception = null;

		private class MiniDumpSystemInfo : MiniDumpStreamBase
		{
			public UInt16 processorArchitecture;
			public UInt16 processorLevel;
			public UInt16 processorRevision;
			public byte   numberOfProcessors;
			public byte   productType;
			public UInt32 majorVersion;
			public UInt32 minorVersion;
			public UInt32 buildNumber;
			public UInt32 platformId;
			public byte[] csdVersion;
			public UInt16 suiteMask;
			public UInt16 reserved2;
			public byte[] blob;
		};
		private MiniDumpSystemInfo m_SystemInfo = null;

		private class MiniDumpMemoryRange
		{
			public UInt64 ptr;
			public byte[] data;
		};
		private class MiniDumpMemoryRanges : MiniDumpStreamBase
		{
			public List<MiniDumpMemoryRange> ranges;
		};
		private MiniDumpMemoryRanges m_MemoryRanges = null;


		////////////////////////////////////////////////////////////////////////
		// MiniDump
		////////////////////////////////////////////////////////////////////////
		private MiniDump()
		{
		}

		////////////////////////////////////////////////////////////////////////
		// Load
		////////////////////////////////////////////////////////////////////////
		public static MiniDump Load(string filename)
		{
			MiniDump dmp = new MiniDump();
			using(FileStream stream = new FileStream(filename, FileMode.Open, FileAccess.Read))
			{
				using(BinaryReader reader = new BinaryReader(stream))
				{
					UInt32 signature            = reader.ReadUInt32();
					dmp.m_Header.version        = reader.ReadUInt32();
					UInt32 numberOfStreams      = reader.ReadUInt32();
					UInt32 streamDirectoryRva   = reader.ReadUInt32();
					Debug.Assert(streamDirectoryRva == 0x20);
					UInt32 checkSum             = reader.ReadUInt32();
					Debug.Assert(checkSum == 0);
					dmp.m_Header.timeDateStamp  = reader.ReadUInt32();
					dmp.m_Header.flags          = reader.ReadUInt64();

					if(signature != MDMP_SIGNATURE)
					{
						return null;
					}

					stream.Seek(streamDirectoryRva, SeekOrigin.Begin);

					MiniDumpDirectory[] directories = new MiniDumpDirectory[numberOfStreams];
					for(uint i=0; i<numberOfStreams; ++i)
					{
						directories[i].streamType = (eStreamType)reader.ReadUInt32();
						directories[i].dataSize   = reader.ReadUInt32();
						directories[i].rva        = reader.ReadUInt32();
					}

					foreach(MiniDumpDirectory d in directories)
					{
						stream.Seek(d.rva, SeekOrigin.Begin);
						switch(d.streamType)
						{
							case eStreamType.ThreadListStream:
							{
								UInt32 numberOfThreads = reader.ReadUInt32();
								MiniDumpThreads ts = new MiniDumpThreads();
								dmp.m_Threads = ts;
								ts.sizeDirectory = ts.sizeTotal = 4+numberOfThreads*0x30;
								Debug.Assert(ts.sizeDirectory == d.dataSize);
								ts.threads = new List<MiniDumpThread>();
								ts.sizeContexts = 0;
								UInt32[] threadContextDataSize = new UInt32[numberOfThreads];
								UInt32[] threadContextRva      = new UInt32[numberOfThreads];
								for(UInt32 i=0; i<numberOfThreads; ++i)
								{
									MiniDumpThread t = new MiniDumpThread();
									t.threadId = reader.ReadUInt32();
									t.suspendCount = reader.ReadUInt32();
									t.priorityClass = reader.ReadUInt32();
									t.priority = reader.ReadUInt32();
									t.teb = reader.ReadUInt64();
									t.stackStartOfMemoryRange = reader.ReadUInt64();
									t.stackDataSize = reader.ReadUInt32();
									UInt32 stackRva = reader.ReadUInt32();
									Debug.Assert(stackRva == 0);
									threadContextDataSize[i] = reader.ReadUInt32();
									ts.sizeContexts += threadContextDataSize[i];
									ts.sizeTotal += threadContextDataSize[i];
									threadContextRva[i] = reader.ReadUInt32();
									ts.threads.Add(t);
								}
								UInt32 j = 0;
								foreach(MiniDumpThread t in dmp.m_Threads.threads)
								{
									stream.Seek(threadContextRva[j], SeekOrigin.Begin);
									t.threadContext = reader.ReadBytes((int)threadContextDataSize[j]);
									++j;
								}
								break;
							}

							case eStreamType.ModuleListStream:
							{
								UInt32 numberOfModules = reader.ReadUInt32();
								MiniDumpModules ms = new MiniDumpModules();
								dmp.m_Modules = ms;
								ms.modules = new List<MiniDumpModule>();
								ms.sizeDirectory = ms.sizeTotal = 4+numberOfModules*0x6c;
								Debug.Assert(ms.sizeDirectory == d.dataSize);
								UInt32[] moduleNameRva      = new UInt32[numberOfModules];
								for(UInt32 i=0; i<numberOfModules; ++i)
								{
									MiniDumpModule m = new MiniDumpModule();
									m.baseOfImage               = reader.ReadUInt64();
									m.sizeOfImage               = reader.ReadUInt32();
									m.checkSum                  = reader.ReadUInt32();
									m.timeDateStamp             = reader.ReadUInt32();
									moduleNameRva[i]            = reader.ReadUInt32();
									m.signature                 = reader.ReadUInt32();
									m.strucVersion              = reader.ReadUInt32();
									m.fileVersionMS             = reader.ReadUInt32();
									m.fileVersionLS             = reader.ReadUInt32();
									m.productVersionMS          = reader.ReadUInt32();
									m.productVersionLS          = reader.ReadUInt32();
									m.fileFlagsMask             = reader.ReadUInt32();
									m.fileFlags                 = reader.ReadUInt32();
									m.fileOS                    = reader.ReadUInt32();
									m.fileType                  = reader.ReadUInt32();
									m.fileSubtype               = reader.ReadUInt32();
									m.fileDateMS                = reader.ReadUInt32();
									m.fileDateLS                = reader.ReadUInt32();
									UInt32 cvRecordDataSize     = reader.ReadUInt32();
									UInt32 cvRecordRva          = reader.ReadUInt32();
									UInt32 miscRecordDataSize   = reader.ReadUInt32();
									UInt32 miscRecordRva        = reader.ReadUInt32();
									Debug.Assert(cvRecordDataSize   == 0);
									Debug.Assert(cvRecordRva        == 0);
									Debug.Assert(miscRecordDataSize == 0);
									Debug.Assert(miscRecordRva      == 0);
									m.reserved0                 = reader.ReadUInt64();
									m.reserved1                 = reader.ReadUInt64();
									ms.modules.Add(m);
								}
								ms.sizeStrings = 0;
								UInt32 j = 0;
								foreach(MiniDumpModule m in ms.modules)
								{
									stream.Seek(moduleNameRva[j], SeekOrigin.Begin);
									UInt32 nameNumBytes = reader.ReadUInt32();   // note that the stored value doesn't include the 2-byte UTF-16 null char
									m.moduleName = reader.ReadBytes((int)nameNumBytes);
									ms.sizeStrings += 4+nameNumBytes+2;
									ms.sizeTotal += 4+nameNumBytes+2;
									++j;
								}
								break;
							}

							case eStreamType.ExceptionStream:
							{
								MiniDumpException e = new MiniDumpException();
								dmp.m_Exception = e;
								e.sizeDirectory = e.sizeTotal = d.dataSize;
								e.threadId = reader.ReadUInt32();
								e.__alignment = reader.ReadUInt32();
								e.exceptionCode = reader.ReadUInt32();
								e.exceptionFlags = reader.ReadUInt32();
								e.exceptionRecord = reader.ReadUInt64();
								e.exceptionAddress = reader.ReadUInt64();
								e.numberParameters = reader.ReadUInt32();
								e.__unusedAlignment = reader.ReadUInt32();
								for(uint i=0; i<MiniDumpException.EXCEPTION_MAXIMUM_PARAMETERS; ++i)
								{
									e.exceptionInformation[i] = reader.ReadUInt64();
								}
								UInt32 threadContextDataSize = reader.ReadUInt32();
								UInt32 threadContextRva      = reader.ReadUInt32();
								stream.Seek(threadContextRva, SeekOrigin.Begin);
								e.threadContext = reader.ReadBytes((int)threadContextDataSize);
								break;
							}

							case eStreamType.SystemInfoStream:
							{
								MiniDumpSystemInfo si = new MiniDumpSystemInfo();
								dmp.m_SystemInfo = si;
								si.sizeDirectory = si.sizeTotal = d.dataSize;
								si.processorArchitecture = reader.ReadUInt16();
								si.processorLevel = reader.ReadUInt16();
								si.processorRevision = reader.ReadUInt16();
								si.numberOfProcessors = reader.ReadByte();
								si.productType = reader.ReadByte();
								si.majorVersion = reader.ReadUInt32();
								si.minorVersion = reader.ReadUInt32();
								si.buildNumber = reader.ReadUInt32();
								si.platformId = reader.ReadUInt32();
								UInt32 csdVersionRva = reader.ReadUInt32();
								si.suiteMask = reader.ReadUInt16();
								si.reserved2 = reader.ReadUInt16();
								si.blob = reader.ReadBytes((int)(d.dataSize-0x20)); // CPU_INFORMATION
								stream.Seek(csdVersionRva, SeekOrigin.Begin);
								UInt32 numBytes = reader.ReadUInt32();
								si.csdVersion = reader.ReadBytes((int)numBytes);
								break;
							}

							case eStreamType.Memory64ListStream:
							{
								UInt64 numberOfMemoryRanges = reader.ReadUInt64();
								MiniDumpMemoryRanges ms = new MiniDumpMemoryRanges();
								ms.sizeDirectory = ms.sizeTotal = (UInt32)(16+numberOfMemoryRanges*16);
								dmp.m_MemoryRanges = ms;
								ms.ranges = new List<MiniDumpMemoryRange>();
								Debug.Assert(ms.sizeDirectory == d.dataSize);
								UInt64 baseRva = reader.ReadUInt64();
								UInt64[] size = new UInt64[numberOfMemoryRanges];
								for(UInt64 i=0; i<numberOfMemoryRanges; ++i)
								{
									MiniDumpMemoryRange m = new MiniDumpMemoryRange();
									m.ptr = reader.ReadUInt64();
									size[i] = reader.ReadUInt64();
									ms.sizeTotal += (UInt32)size[i];
									ms.ranges.Add(m);
								}
								stream.Seek((UInt32)baseRva, SeekOrigin.Begin);
								int j = 0;
								foreach(MiniDumpMemoryRange m in dmp.m_MemoryRanges.ranges)
								{
									m.data = reader.ReadBytes((int)(size[j++]));
								}
								break;
							}

							case eStreamType.MiscInfoStream:
							{
								dmp.m_MiscInfo = new MiniDumpGenericStream();
								dmp.m_MiscInfo.streamType = eStreamType.MiscInfoStream;
								stream.Seek(d.rva, SeekOrigin.Begin);
								dmp.m_MiscInfo.data = reader.ReadBytes((int)d.dataSize);
								dmp.m_MiscInfo.sizeDirectory = dmp.m_MiscInfo.sizeTotal = d.dataSize;
								break;
							}
						}
					}
				}
			}
			return dmp;
		}

		////////////////////////////////////////////////////////////////////////
		// WriteDirectoryEntry
		////////////////////////////////////////////////////////////////////////
		private UInt32 WriteDirectoryEntry(BinaryWriter writer, eStreamType type, MiniDumpStreamBase stream, UInt32 rva)
		{
			if(stream != null)
			{
				writer.Write((UInt32)type);
				writer.Write(stream.sizeDirectory);
				writer.Write(rva);
				rva += stream.sizeTotal;
			}
			return rva;
		}

		////////////////////////////////////////////////////////////////////////
		// Store
		////////////////////////////////////////////////////////////////////////
		public void Store(string filename)
		{
			using(FileStream stream = File.Create(filename))
			{
				using(BinaryWriter writer = new BinaryWriter(stream))
				{
					UInt32 rva = 0x20; // stream directory immediately following header

					// Write header
					writer.Write(MDMP_SIGNATURE);
					writer.Write(m_Header.version);
					UInt32 numberOfStreams
						= (m_Threads      !=null?1u:0u)
						+ (m_Modules      !=null?1u:0u)
						+ (m_MemoryRanges !=null?1u:0u)
						+ (m_Exception    !=null?1u:0u)
						+ (m_SystemInfo   !=null?1u:0u)
						+ (m_MiscInfo     !=null?1u:0u)
						+ 2u /* 2*UnusedStream */;
					writer.Write(numberOfStreams);
					writer.Write(rva);
					writer.Write((UInt32)0);     // checkSum
					writer.Write(m_Header.timeDateStamp);
					writer.Write(m_Header.flags);

					// Calculate the rvas for everything.  The slightly weird
					// order here is to mimic the same behaviour as a normal
					// coredump.
					rva += numberOfStreams * 12;
					UInt32 systemInfoRva             = rva;
					UInt32 miscInfoRva               = systemInfoRva + (m_SystemInfo   != null ? m_SystemInfo   .sizeDirectory : 0);
					UInt32 exceptionRva              = miscInfoRva   + (m_MiscInfo     != null ? m_MiscInfo     .sizeDirectory : 0);
					UInt32 threadListRva             = exceptionRva  + (m_Exception    != null ? m_Exception    .sizeDirectory : 0);
					UInt32 moduleListRva             = threadListRva + (m_Threads      != null ? m_Threads      .sizeDirectory : 0);
					UInt32 stringTableRva            = moduleListRva + (m_Modules      != null ? m_Modules      .sizeDirectory : 0);
					UInt32 csdVersionRva             = stringTableRva;
					UInt32 moduleNamesRva            = csdVersionRva              + (m_SystemInfo   != null ? 4+(UInt32)m_SystemInfo.csdVersion.Length+2 : 0);
					UInt32 threadContextsRva         = moduleNamesRva             + (m_Modules      != null ? m_Modules.sizeStrings                      : 0);
					UInt32 exceptionThreadContextRva = threadContextsRva;
					UInt32 threadListContextsRva     = exceptionThreadContextRva  + (m_Exception    != null ? (UInt32)m_Exception.threadContext.Length   : 0);
					// There is a weird 16-bytes worth of zero here in a normal dump file, NFI what it is for, and nothing seems to reference it
					UInt32 memoryListRva             = 16 + threadListContextsRva + (m_Threads      != null ? m_Threads.sizeContexts                     : 0);
					UInt32 totalFileSize             = memoryListRva              + (m_MemoryRanges != null ? m_MemoryRanges.sizeTotal                   : 0);

					// Write the stream directory.  The order of the streams is
					// chosen here to be the same as a normal coredump.
					WriteDirectoryEntry(writer, eStreamType.ThreadListStream,     m_Threads,      threadListRva);
					WriteDirectoryEntry(writer, eStreamType.ModuleListStream,     m_Modules,      moduleListRva);
					WriteDirectoryEntry(writer, eStreamType.Memory64ListStream,   m_MemoryRanges, memoryListRva);
					WriteDirectoryEntry(writer, eStreamType.ExceptionStream,      m_Exception,    exceptionRva);
					WriteDirectoryEntry(writer, eStreamType.SystemInfoStream,     m_SystemInfo,   systemInfoRva);
					WriteDirectoryEntry(writer, eStreamType.MiscInfoStream,       m_MiscInfo,     miscInfoRva);
					writer.Write((UInt32)eStreamType.UnusedStream);
					writer.Write((UInt32)0);
					writer.Write((UInt32)0);
					writer.Write((UInt32)eStreamType.UnusedStream);
					writer.Write((UInt32)0);
					writer.Write((UInt32)0);

					// eStreamType.SystemInfoStream
					if(m_SystemInfo != null)
					{
						Debug.Assert(rva == systemInfoRva);
						writer.Write(m_SystemInfo.processorArchitecture);
						writer.Write(m_SystemInfo.processorLevel);
						writer.Write(m_SystemInfo.processorRevision);
						writer.Write(m_SystemInfo.numberOfProcessors);
						writer.Write(m_SystemInfo.productType);
						writer.Write(m_SystemInfo.majorVersion);
						writer.Write(m_SystemInfo.minorVersion);
						writer.Write(m_SystemInfo.buildNumber);
						writer.Write(m_SystemInfo.platformId);
						writer.Write(csdVersionRva);
						writer.Write(m_SystemInfo.suiteMask);
						writer.Write(m_SystemInfo.reserved2);
						writer.Write(m_SystemInfo.blob);
						rva += m_SystemInfo.sizeTotal;
					}

					// eStreamType.MiscInfoStream
					if(m_MiscInfo != null)
					{
						Debug.Assert(rva == miscInfoRva);
						writer.Write(m_MiscInfo.data);
						rva += m_MiscInfo.sizeTotal;
					}

					// eStreamType.ExceptionStream
					if(m_Exception != null)
					{
						Debug.Assert(rva == exceptionRva);
						writer.Write(m_Exception.threadId);
						writer.Write(m_Exception.__alignment);
						writer.Write(m_Exception.exceptionCode);
						writer.Write(m_Exception.exceptionFlags);
						writer.Write(m_Exception.exceptionRecord);
						writer.Write(m_Exception.exceptionAddress);
						writer.Write(m_Exception.numberParameters);
						writer.Write(m_Exception.__unusedAlignment);
						foreach(UInt64 p in m_Exception.exceptionInformation)
						{
							writer.Write(p);
						}
						writer.Write((UInt32)m_Exception.threadContext.Length);
						writer.Write(exceptionThreadContextRva);
						rva += m_Exception.sizeTotal;
					}

					// eStreamType.ThreadListStream
					if(m_Threads != null)
					{
						Debug.Assert(rva == threadListRva);
						UInt32 numberOfThreads = (UInt32)m_Threads.threads.Count;
						rva += 4+numberOfThreads*0x30;
						UInt32 contextRva = threadListContextsRva;
						writer.Write(numberOfThreads);
						foreach(MiniDumpThread t in m_Threads.threads)
						{
							writer.Write(t.threadId);
							writer.Write(t.suspendCount);
							writer.Write(t.priorityClass);
							writer.Write(t.priority);
							writer.Write(t.teb);
							writer.Write(t.stackStartOfMemoryRange);
							writer.Write(t.stackDataSize);
							writer.Write((UInt32)0);    // stackRva
							writer.Write((UInt32)t.threadContext.Length);
							writer.Write(contextRva);
							contextRva += (UInt32)t.threadContext.Length;
						}
					}

					// eStreamType.ModuleListStream
					if(m_Modules != null)
					{
						Debug.Assert(rva == moduleListRva);
						UInt32 numberOfModules = (UInt32)m_Modules.modules.Count;
						UInt32 moduleNameRva = moduleNamesRva;

						writer.Write(numberOfModules);
						foreach(MiniDumpModule m in m_Modules.modules)
						{
							writer.Write(m.baseOfImage);
							writer.Write(m.sizeOfImage);
							writer.Write(m.checkSum);
							writer.Write(m.timeDateStamp);
							writer.Write(moduleNameRva);
							moduleNameRva += (UInt32)(4+m.moduleName.Length+2);
							writer.Write(m.signature);
							writer.Write(m.strucVersion);
							writer.Write(m.fileVersionMS);
							writer.Write(m.fileVersionLS);
							writer.Write(m.productVersionMS);
							writer.Write(m.productVersionLS);
							writer.Write(m.fileFlagsMask);
							writer.Write(m.fileFlags);
							writer.Write(m.fileOS);
							writer.Write(m.fileType);
							writer.Write(m.fileSubtype);
							writer.Write(m.fileDateMS);
							writer.Write(m.fileDateLS);
							writer.Write((UInt32)0);        // cvRecordDataSize
							writer.Write((UInt32)0);        // cvRecordRva
							writer.Write((UInt32)0);        // miscRecordDataSize
							writer.Write((UInt32)0);        // miscRecordRva
							writer.Write(m.reserved0);
							writer.Write(m.reserved1);
						}

						rva += 4+numberOfModules*0x6c;
					}

					// String table
					{
						Debug.Assert(rva == stringTableRva);

						if(m_SystemInfo != null)
						{
							Debug.Assert(rva == csdVersionRva);
							writer.Write((UInt32)m_SystemInfo.csdVersion.Length);
							writer.Write(m_SystemInfo.csdVersion);
							writer.Write((UInt16)0);
							rva += 4+(UInt32)m_SystemInfo.csdVersion.Length+2;
						}

						if(m_Modules != null)
						{
							Debug.Assert(rva == moduleNamesRva);
							foreach(MiniDumpModule m in m_Modules.modules)
							{
								writer.Write((UInt32)m.moduleName.Length);
								writer.Write(m.moduleName);
								writer.Write((UInt16)0);
								rva += 4+(UInt32)m.moduleName.Length+2;
							}
						}
					}

					// Thread contexts
					if(m_Threads != null)
					{
						Debug.Assert(rva == threadContextsRva);

						if(m_Exception != null)
						{
							Debug.Assert(rva == exceptionThreadContextRva);
							writer.Write(m_Exception.threadContext);
							rva += (UInt32)m_Exception.threadContext.Length;
						}

						if(m_Threads != null)
						{
							Debug.Assert(rva == threadListContextsRva);
							foreach(MiniDumpThread t in m_Threads.threads)
							{
								writer.Write(t.threadContext);
								rva += (UInt32)t.threadContext.Length;
							}
						}
					}

					// Unknown 16-bytes worth of zero
					for(uint i=0; i<16; ++i)
					{
						writer.Write((byte)0);
					}
					rva += 16;

					// eStreamType.Memory64ListStream
					if(m_MemoryRanges != null)
					{
						Debug.Assert(rva == memoryListRva);
						UInt64 numberOfMemoryRanges = (UInt64)m_MemoryRanges.ranges.Count;
						UInt64 baseRva = rva + 16+numberOfMemoryRanges*16;
						writer.Write(numberOfMemoryRanges);
						writer.Write(baseRva);
						foreach(MiniDumpMemoryRange m in m_MemoryRanges.ranges)
						{
							writer.Write(m.ptr);
							writer.Write((UInt64)m.data.Length);
						}
						foreach(MiniDumpMemoryRange m in m_MemoryRanges.ranges)
						{
							writer.Write(m.data);
						}
						rva += m_MemoryRanges.sizeTotal;
					}

					Debug.Assert(rva == totalFileSize);
				}
			}
		}

		////////////////////////////////////////////////////////////////////////
		// AddMemoryRange
		////////////////////////////////////////////////////////////////////////
		public void AddMemoryRange(UInt32 ptr, byte[] data)
		{
			MiniDumpMemoryRange m = new MiniDumpMemoryRange();
			m.ptr  = ptr;
			m.data = data;
			m_MemoryRanges.ranges.Add(m);
			m_MemoryRanges.sizeDirectory += 16;
			m_MemoryRanges.sizeTotal     += 16 + (UInt32)(data.Length);
		}
	}
}
