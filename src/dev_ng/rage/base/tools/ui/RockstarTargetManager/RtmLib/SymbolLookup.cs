using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading;

namespace Rockstar.TargetManager
{
	////////////////////////////////////////////////////////////////////////////
	// SymbolLookup
	////////////////////////////////////////////////////////////////////////////
	public class SymbolLookup : IDisposable
	{
		private TargetPlatformId m_Platform;
		private FileStream m_Stream = null;
		private BinaryReader m_Reader = null;
		private UInt64 m_SymbolCount;
		private UInt64 m_PreferredLoadAddress;
		private UInt64 m_UnrelocatedMainAddress;
		private UInt64 m_SegmentBaseAddress;
		private UInt64 m_ExecutableSize;
		private UInt64 m_AddressSize;
		private UInt64 m_NameOffsetSize;
		private UInt64 m_SymbolTableOffset;

		public class SpuEaGuidMapEntry
		{
			public UInt32[] guidInsns = new UInt32[4];
			public UInt32   ea;
		}
		private SpuEaGuidMapEntry[] m_SpuEaGuidMap;
		public SpuEaGuidMapEntry[] SpuEaGuidMap
		{
			get{return m_SpuEaGuidMap;}
		}

		public UInt64 SegmentBaseAddress
		{
			get { return m_SegmentBaseAddress; }
			set { m_SegmentBaseAddress = value; }
		}

		public UInt64 ExecutableSize
		{
			get { return m_ExecutableSize; }
			set { m_ExecutableSize = value; }
		}

		public struct SpuLsGuidMapEntry
		{
			public UInt64 guid;
			public UInt32 ls;
			public UInt32 ea;
		}

		////////////////////////////////////////////////////////////////////////
		// SymbolLookup
		////////////////////////////////////////////////////////////////////////
		public SymbolLookup(string cmpFilename, TargetPlatformId platform)
		{
			m_Platform = platform;
			if(cmpFilename == null)
			{
				return;
			}
			try
			{
				m_Stream = new FileStream(cmpFilename, FileMode.Open, FileAccess.Read);
				try
				{
					m_Reader = new BinaryReader(m_Stream);
					try
					{
						switch(platform)
						{
							case TargetPlatformId.PS3:
							{
								m_SymbolCount = ReadUInt32();
								m_PreferredLoadAddress = ReadUInt32();
								m_UnrelocatedMainAddress = 0;
								m_AddressSize = 4;
								m_NameOffsetSize = 4;
								m_SymbolTableOffset = 8;

								// PS3 .cmp file has an additional table of SPU GUIDS at the end
								m_Stream.Seek(-4, SeekOrigin.End);
								UInt32 numGuids = ReadUInt32();
								m_SpuEaGuidMap = new SpuEaGuidMapEntry[numGuids];
								m_Stream.Seek(-4-numGuids*20, SeekOrigin.End);
								for(UInt32 i=0; i<numGuids; ++i)
								{
									// Only the guid instructions needs endian swapping, not the ea
									m_SpuEaGuidMap[i] = new SpuEaGuidMapEntry();
									m_SpuEaGuidMap[i].guidInsns[0] = EndianSwap(ReadUInt32());
									m_SpuEaGuidMap[i].guidInsns[1] = EndianSwap(ReadUInt32());
									m_SpuEaGuidMap[i].guidInsns[2] = EndianSwap(ReadUInt32());
									m_SpuEaGuidMap[i].guidInsns[3] = EndianSwap(ReadUInt32());
									m_SpuEaGuidMap[i].ea = ReadUInt32();
								}
								break;
							}

							case TargetPlatformId.PS4:
							{
								m_SymbolCount = ReadUInt64();
								m_PreferredLoadAddress = ReadUInt64();
								m_UnrelocatedMainAddress = ReadUInt64();
								m_AddressSize = 8;
								m_NameOffsetSize = 8;
								m_SymbolTableOffset = 24;
								break;
							}

							case TargetPlatformId.Xbox360:
							{
								m_SymbolCount = ReadUInt32();
								// Check for 360 specific placeholder file
								if(m_SymbolCount == 0x506c6163) // "Plac"
								{
									throw new Exception();
								}
								m_PreferredLoadAddress = ReadUInt32();
								m_UnrelocatedMainAddress = 0;
								m_AddressSize = 4;
								m_NameOffsetSize = 4;
								m_SymbolTableOffset = 8;
								break;
							}

							case TargetPlatformId.XB1:
							{
								m_SymbolCount = ReadUInt64();
								m_PreferredLoadAddress = ReadUInt64();
								m_UnrelocatedMainAddress = ReadUInt64();
								m_AddressSize = 8;
								m_NameOffsetSize = 8;
								m_SymbolTableOffset = 24;
								break;
							}

							case TargetPlatformId.PC:
							{
								m_SymbolCount = ReadUInt64();
								m_PreferredLoadAddress = ReadUInt64();
								m_UnrelocatedMainAddress = 0;
								m_AddressSize = 8;
								m_NameOffsetSize = 8;
								m_SymbolTableOffset = 16;
								break;
							}

							default:
								throw new Exception();
						}
					}
					catch
					{
						m_Reader.Dispose();
						m_Reader = null;
						throw;
					}
				}
				catch
				{
					m_Stream.Dispose();
					m_Stream = null;
				}
			}
			catch
			{
			}
		}

		////////////////////////////////////////////////////////////////////////
		// ~SymbolLookup
		////////////////////////////////////////////////////////////////////////
		~SymbolLookup()
		{
			Dispose(false);
		}

		////////////////////////////////////////////////////////////////////////
		// Dispose
		////////////////////////////////////////////////////////////////////////
		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		////////////////////////////////////////////////////////////////////////
		// Dispose
		////////////////////////////////////////////////////////////////////////
		protected virtual void Dispose(bool disposing)
		{
			if(disposing)
			{
				if(m_Reader != null)    m_Reader.Dispose();
				if(m_Stream != null)    m_Stream.Dispose();
			}
		}

		////////////////////////////////////////////////////////////////////////
		// EndianSwap
		////////////////////////////////////////////////////////////////////////
		private static UInt32 EndianSwap(UInt32 val)
		{
			return ((val&0xff000000)>>24)
			     | ((val&0x00ff0000)>>8)
			     | ((val&0x0000ff00)<<8)
			     | ((val&0x000000ff)<<24);
		}

		////////////////////////////////////////////////////////////////////////
		// EndianSwap
		////////////////////////////////////////////////////////////////////////
		private static UInt64 EndianSwap(UInt64 val)
		{
			return ((val&0xff00000000000000)>>56)
			     | ((val&0x00ff000000000000)>>40)
			     | ((val&0x0000ff0000000000)>>24)
			     | ((val&0x000000ff00000000)>>8)
			     | ((val&0x00000000ff000000)<<8)
			     | ((val&0x0000000000ff0000)<<24)
			     | ((val&0x000000000000ff00)<<40)
			     | ((val&0x00000000000000ff)<<56);
		}

		////////////////////////////////////////////////////////////////////////
		// ReadUInt32
		////////////////////////////////////////////////////////////////////////
		private UInt32 ReadUInt32()
		{
			return m_Reader.ReadUInt32();
		}

		////////////////////////////////////////////////////////////////////////
		// ReadUInt64
		////////////////////////////////////////////////////////////////////////
		private UInt64 ReadUInt64()
		{
			return m_Reader.ReadUInt64();
		}

		////////////////////////////////////////////////////////////////////////
		// ReadValue
		////////////////////////////////////////////////////////////////////////
		private UInt64 ReadValue(UInt64 size)
		{
			if(size == 4)
			{
				return ReadUInt32();
			}
			else if(size == 8)
			{
				return ReadUInt64();
			}
			else
			{
				return 0;
			}
		}

		////////////////////////////////////////////////////////////////////////
		// ReadAddress
		////////////////////////////////////////////////////////////////////////
		private UInt64 ReadAddress()
		{
			return ReadValue(m_AddressSize);
		}

		////////////////////////////////////////////////////////////////////////
		// ReadNameOffset
		////////////////////////////////////////////////////////////////////////
		private UInt64 ReadNameOffset()
		{
			return ReadValue(m_NameOffsetSize);
		}

		////////////////////////////////////////////////////////////////////////
		// Lookup
		////////////////////////////////////////////////////////////////////////
		public string Lookup(UInt64 addr, out UInt64 offs)
		{
			offs = 0;

			if(m_Platform == TargetPlatformId.PC)
			{
				if(addr>=m_SegmentBaseAddress && addr<m_SegmentBaseAddress+m_ExecutableSize)
				{
					addr += m_PreferredLoadAddress;
				}
				else
				{
					return "notfound";
				}
			}

			try
			{
				addr -= m_SegmentBaseAddress;
				UInt64 low = 0;
				UInt64 high = m_SymbolCount-2;
				while(low < high)
				{
					UInt64 mid = (low + high) >> 1;
					m_Stream.Seek((long)(m_SymbolTableOffset + mid*(m_AddressSize+m_NameOffsetSize)), SeekOrigin.Begin);
					UInt64 addr0 = ReadAddress();
					UInt64 name0 = ReadNameOffset();
					UInt64 addr1 = ReadAddress();
					if(addr>=addr0 && addr<addr1)
					{
						m_Stream.Seek((long)name0, SeekOrigin.Begin);
						char[] chars = m_Reader.ReadChars(128);
						int strlen = Array.FindIndex(chars, c => c==0);
						if(strlen >= 0)
						{
							Array.Resize(ref chars, strlen);
						}
						offs = addr - addr0;
						return new string(chars)+"+0x"+offs.ToString("x");
					}
					else if(addr > addr0)
					{
						low = mid+1;
					}
					else
					{
						high = mid;
					}
				}
			}
			catch
			{
			}
			return "notfound";
		}

		////////////////////////////////////////////////////////////////////////
		// Lookup
		////////////////////////////////////////////////////////////////////////
		public string Lookup(UInt64 addr)
		{
			UInt64 offs;
			return Lookup(addr, out offs);
		}

		////////////////////////////////////////////////////////////////////////
		// Lookup
		////////////////////////////////////////////////////////////////////////
		public string Lookup(UInt32 addr, SpuLsGuidMapEntry lsGuidMapEntry, out UInt32 offs)
		{
			UInt64 offs64;
			string str = Lookup(addr - lsGuidMapEntry.ls + lsGuidMapEntry.ea, out offs64);
			offs = (UInt32)offs64;
			return str;
		}

		////////////////////////////////////////////////////////////////////////
		// Lookup
		////////////////////////////////////////////////////////////////////////
		public string Lookup(UInt32 addr, SpuLsGuidMapEntry lsGuidMapEntry)
		{
			UInt64 offs;
			return Lookup(addr - lsGuidMapEntry.ls + lsGuidMapEntry.ea, out offs);
		}

		////////////////////////////////////////////////////////////////////////
		// Lookup
		////////////////////////////////////////////////////////////////////////
		public UInt64 Lookup(string symbol)
		{
			try
			{
				// Find symbol name
				UInt64 nextNameOffset = m_SymbolTableOffset + m_SymbolCount*(m_AddressSize+m_NameOffsetSize);
				UInt64 nameOffset = 0;
				m_Stream.Seek((long)nextNameOffset, SeekOrigin.Begin);
				while(nextNameOffset < (UInt64)m_Stream.Length)
				{
					UInt64 currNameOffset = nextNameOffset;
					string s = "";
					for(;;)
					{
						char c;
						c = (char)m_Reader.Read();
						++nextNameOffset;
						if(c==0 || nextNameOffset==(UInt64)m_Stream.Length)
						{
							break;
						}
						s += c;
					}
					if(s == symbol)
					{
						nameOffset = currNameOffset;
						break;
					}
				}
				if(nameOffset == 0)
				{
					return 0;
				}

				// Find symbol address / name offset pair that matches
				m_Stream.Seek((long)m_SymbolTableOffset, SeekOrigin.Begin);
				for(UInt64 i=0; i<m_SymbolCount; ++i)
				{
					UInt64 addr = ReadAddress();
					UInt64 name = ReadNameOffset();
					if(name == nameOffset)
					{
						if(m_Platform == TargetPlatformId.PC)
						{
							addr -= m_PreferredLoadAddress;
						}
						return addr+m_SegmentBaseAddress;
					}
				}
			}
			catch
			{
			}
			return 0;
		}

		////////////////////////////////////////////////////////////////////////
		// FindSpuLsGuidMapEntry
		////////////////////////////////////////////////////////////////////////
		public static SpuLsGuidMapEntry FindSpuLsGuidMapEntry(SpuLsGuidMapEntry[] lsGuidMap, UInt32 ls)
		{
			if(lsGuidMap.Length==0 || ls<lsGuidMap[0].ls)
			{
				// Not found
				return new SpuLsGuidMapEntry();
			}

			// Find the corresponding guid
			uint i;
			for(i=0; i<lsGuidMap.Length; ++i)
			{
				if(ls<lsGuidMap[i].ls)
				{
					break;
				}
			}
			return lsGuidMap[i-1];
		}
	}

	////////////////////////////////////////////////////////////////////////////
	// AsyncSymbolLookup
	////////////////////////////////////////////////////////////////////////////
	public class AsyncSymbolLookup : IDisposable
	{
        private Dictionary<string, UInt64> m_Symbols = new Dictionary<string, UInt64>();
		private Thread m_WorkerThread;

		////////////////////////////////////////////////////////////////////////
		// AsyncSymbolLookup
		////////////////////////////////////////////////////////////////////////
		public AsyncSymbolLookup(string cmpFilename, TargetPlatformId platform, string[] symbols, UInt64 segmentBaseAddress=0, UInt64 executableSize=0xffffffffffffffff)
		{
			if(cmpFilename != null)
			{
				m_WorkerThread = new Thread(WorkerThread);
				m_WorkerThread.Name = "AsyncSymbolLookup \""+cmpFilename+"\"";
				m_WorkerThread.Start(new WorkerThreadArgs(this, cmpFilename, platform, symbols, segmentBaseAddress, executableSize));
			}
			else
			{
				foreach(var s in symbols)
				{
					m_Symbols.Add(s, 0);
				}
			}
		}

		////////////////////////////////////////////////////////////////////////
		// ~AsyncSymbolLookup
		////////////////////////////////////////////////////////////////////////
		~AsyncSymbolLookup()
		{
			Dispose(false);
		}

		////////////////////////////////////////////////////////////////////////
		// Dispose
		////////////////////////////////////////////////////////////////////////
		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		////////////////////////////////////////////////////////////////////////
		// Dispose
		////////////////////////////////////////////////////////////////////////
		protected virtual void Dispose(bool disposing)
		{
			if(disposing)
			{
		        if(m_WorkerThread != null)
				{
					m_WorkerThread.Join();
				}
			}
		}

		////////////////////////////////////////////////////////////////////////
		// WorkerThreadArgs
		////////////////////////////////////////////////////////////////////////
		private class WorkerThreadArgs
		{
			internal AsyncSymbolLookup  m_Self;
			internal string             m_CmpFilename;
			internal TargetPlatformId   m_Platform;
			internal string[]           m_Symbols;
			internal UInt64             m_SegmentBaseAddress;
			internal UInt64             m_ExecutableSize;

			internal WorkerThreadArgs(AsyncSymbolLookup self, string cmpFilename, TargetPlatformId platform, string[] symbols, UInt64 segmentBaseAddress, UInt64 executableSize)
			{
				m_Self               = self;
				m_CmpFilename        = cmpFilename;
				m_Platform           = platform;
				m_Symbols            = symbols;
				m_SegmentBaseAddress = segmentBaseAddress;
				m_ExecutableSize     = executableSize;
			}
		}

		////////////////////////////////////////////////////////////////////////
		// WorkerThread
		////////////////////////////////////////////////////////////////////////
		private static void WorkerThread(object args_)
		{
			var args = (WorkerThreadArgs)args_;

			lock(args.m_Self.m_Symbols)
			{
				using(var sym = new SymbolLookup(args.m_CmpFilename, args.m_Platform))
				{
					sym.SegmentBaseAddress = args.m_SegmentBaseAddress;
					sym.ExecutableSize     = args.m_ExecutableSize;
					foreach(var s in args.m_Symbols)
					{
						var addr = sym.Lookup(s);
						args.m_Self.m_Symbols.Add(s, addr);
					}
				}
			}
		}

		////////////////////////////////////////////////////////////////////////
		// Lookup
		////////////////////////////////////////////////////////////////////////
		public UInt64 Lookup(string symbol)
		{
			for(;;)
			{
				lock(m_Symbols)
				{
					UInt64 addr;
					if(m_Symbols.TryGetValue(symbol, out addr))
					{
						return addr;
					}
				}

				// TryGetValue may validly fail if we aqcuired the lock before
				// the worker thread got scheduled.  Though that is fairly
				// unlikely.  Much more likely is a bug in the calling code
				// where symbol was not one of the symbols originally requested.

				Thread.Sleep(10);
			}
		}
	}
}
