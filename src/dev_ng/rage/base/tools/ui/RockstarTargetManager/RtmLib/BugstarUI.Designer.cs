﻿namespace Rockstar.TargetManager
{
	partial class BugstarUI
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BugstarUI));
			this.radioButtonNew = new System.Windows.Forms.RadioButton();
			this.radioButtonExisting = new System.Windows.Forms.RadioButton();
			this.labelDescription = new System.Windows.Forms.Label();
			this.buttonOk = new System.Windows.Forms.Button();
			this.buttonCancel = new System.Windows.Forms.Button();
			this.textBoxExisting = new System.Windows.Forms.TextBox();
			this.buttonViewBug = new System.Windows.Forms.Button();
			this.labelUsername = new System.Windows.Forms.Label();
			this.labelPassword = new System.Windows.Forms.Label();
			this.textBoxUsername = new System.Windows.Forms.TextBox();
			this.textBoxPassword = new System.Windows.Forms.TextBox();
			this.buttonLogin = new System.Windows.Forms.Button();
			this.progressBarDuplicateSearch = new System.Windows.Forms.ProgressBar();
			this.labelDuplicateSearch = new System.Windows.Forms.Label();
			this.buttonDuplicateSearchStop = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// radioButtonNew
			// 
			this.radioButtonNew.AutoSize = true;
			this.radioButtonNew.Location = new System.Drawing.Point(15, 202);
			this.radioButtonNew.Name = "radioButtonNew";
			this.radioButtonNew.Size = new System.Drawing.Size(100, 17);
			this.radioButtonNew.TabIndex = 2;
			this.radioButtonNew.TabStop = true;
			this.radioButtonNew.Text = "Create &new bug";
			this.radioButtonNew.UseVisualStyleBackColor = true;
			this.radioButtonNew.CheckedChanged += new System.EventHandler(this.radioButtonNew_CheckedChanged);
			// 
			// radioButtonExisting
			// 
			this.radioButtonExisting.AutoSize = true;
			this.radioButtonExisting.Location = new System.Drawing.Point(15, 225);
			this.radioButtonExisting.Name = "radioButtonExisting";
			this.radioButtonExisting.Size = new System.Drawing.Size(115, 17);
			this.radioButtonExisting.TabIndex = 3;
			this.radioButtonExisting.TabStop = true;
			this.radioButtonExisting.Text = "Add to &existing bug";
			this.radioButtonExisting.UseVisualStyleBackColor = true;
			this.radioButtonExisting.CheckedChanged += new System.EventHandler(this.radioButtonExisting_CheckedChanged);
			// 
			// labelDescription
			// 
			this.labelDescription.AutoSize = true;
			this.labelDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.labelDescription.Location = new System.Drawing.Point(18, 13);
			this.labelDescription.Name = "labelDescription";
			this.labelDescription.Size = new System.Drawing.Size(97, 13);
			this.labelDescription.TabIndex = 0;
			this.labelDescription.Text = "PLACEHOLDER";
			// 
			// buttonOk
			// 
			this.buttonOk.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.buttonOk.Location = new System.Drawing.Point(541, 223);
			this.buttonOk.Name = "buttonOk";
			this.buttonOk.Size = new System.Drawing.Size(75, 23);
			this.buttonOk.TabIndex = 6;
			this.buttonOk.Text = "&OK";
			this.buttonOk.UseVisualStyleBackColor = true;
			this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
			// 
			// buttonCancel
			// 
			this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.buttonCancel.Location = new System.Drawing.Point(444, 223);
			this.buttonCancel.Name = "buttonCancel";
			this.buttonCancel.Size = new System.Drawing.Size(75, 23);
			this.buttonCancel.TabIndex = 7;
			this.buttonCancel.Text = "&Cancel";
			this.buttonCancel.UseVisualStyleBackColor = true;
			this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
			// 
			// textBoxExisting
			// 
			this.textBoxExisting.Location = new System.Drawing.Point(136, 225);
			this.textBoxExisting.Name = "textBoxExisting";
			this.textBoxExisting.Size = new System.Drawing.Size(100, 20);
			this.textBoxExisting.TabIndex = 4;
			this.textBoxExisting.TextChanged += new System.EventHandler(this.textBoxExisting_TextChanged);
			// 
			// buttonViewBug
			// 
			this.buttonViewBug.Location = new System.Drawing.Point(255, 222);
			this.buttonViewBug.Name = "buttonViewBug";
			this.buttonViewBug.Size = new System.Drawing.Size(75, 23);
			this.buttonViewBug.TabIndex = 5;
			this.buttonViewBug.Text = "&View bug";
			this.buttonViewBug.UseVisualStyleBackColor = true;
			this.buttonViewBug.Click += new System.EventHandler(this.buttonViewBug_Click);
			// 
			// labelUsername
			// 
			this.labelUsername.AutoSize = true;
			this.labelUsername.Location = new System.Drawing.Point(12, 47);
			this.labelUsername.Name = "labelUsername";
			this.labelUsername.Size = new System.Drawing.Size(55, 13);
			this.labelUsername.TabIndex = 6;
			this.labelUsername.Text = "Username";
			// 
			// labelPassword
			// 
			this.labelPassword.AutoSize = true;
			this.labelPassword.Location = new System.Drawing.Point(12, 77);
			this.labelPassword.Name = "labelPassword";
			this.labelPassword.Size = new System.Drawing.Size(53, 13);
			this.labelPassword.TabIndex = 7;
			this.labelPassword.Text = "Password";
			// 
			// textBoxUsername
			// 
			this.textBoxUsername.Location = new System.Drawing.Point(70, 44);
			this.textBoxUsername.Name = "textBoxUsername";
			this.textBoxUsername.Size = new System.Drawing.Size(239, 20);
			this.textBoxUsername.TabIndex = 8;
			// 
			// textBoxPassword
			// 
			this.textBoxPassword.Location = new System.Drawing.Point(70, 74);
			this.textBoxPassword.Name = "textBoxPassword";
			this.textBoxPassword.Size = new System.Drawing.Size(238, 20);
			this.textBoxPassword.TabIndex = 0;
			this.textBoxPassword.UseSystemPasswordChar = true;
			this.textBoxPassword.TextChanged += new System.EventHandler(this.textBoxPassword_TextChanged);
			// 
			// buttonLogin
			// 
			this.buttonLogin.Location = new System.Drawing.Point(541, 74);
			this.buttonLogin.Name = "buttonLogin";
			this.buttonLogin.Size = new System.Drawing.Size(75, 23);
			this.buttonLogin.TabIndex = 1;
			this.buttonLogin.Text = "&Login";
			this.buttonLogin.UseVisualStyleBackColor = true;
			this.buttonLogin.Click += new System.EventHandler(this.buttonLogin_Click);
			// 
			// progressBarDuplicateSearch
			// 
			this.progressBarDuplicateSearch.Location = new System.Drawing.Point(15, 144);
			this.progressBarDuplicateSearch.Name = "progressBarDuplicateSearch";
			this.progressBarDuplicateSearch.Size = new System.Drawing.Size(221, 23);
			this.progressBarDuplicateSearch.TabIndex = 9;
			// 
			// labelDuplicateSearch
			// 
			this.labelDuplicateSearch.AutoSize = true;
			this.labelDuplicateSearch.Location = new System.Drawing.Point(13, 128);
			this.labelDuplicateSearch.Name = "labelDuplicateSearch";
			this.labelDuplicateSearch.Size = new System.Drawing.Size(117, 13);
			this.labelDuplicateSearch.TabIndex = 10;
			this.labelDuplicateSearch.Text = "Duplicate bug search...";
			// 
			// buttonDuplicateSearchStop
			// 
			this.buttonDuplicateSearchStop.Location = new System.Drawing.Point(255, 144);
			this.buttonDuplicateSearchStop.Name = "buttonDuplicateSearchStop";
			this.buttonDuplicateSearchStop.Size = new System.Drawing.Size(75, 23);
			this.buttonDuplicateSearchStop.TabIndex = 11;
			this.buttonDuplicateSearchStop.Text = "&Stop";
			this.buttonDuplicateSearchStop.UseVisualStyleBackColor = true;
			this.buttonDuplicateSearchStop.Click += new System.EventHandler(this.buttonDuplicateSearchStop_Click);
			// 
			// BugstarUI
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.CancelButton = this.buttonCancel;
			this.ClientSize = new System.Drawing.Size(630, 266);
			this.Controls.Add(this.buttonDuplicateSearchStop);
			this.Controls.Add(this.labelDuplicateSearch);
			this.Controls.Add(this.progressBarDuplicateSearch);
			this.Controls.Add(this.buttonLogin);
			this.Controls.Add(this.textBoxPassword);
			this.Controls.Add(this.textBoxUsername);
			this.Controls.Add(this.labelPassword);
			this.Controls.Add(this.labelUsername);
			this.Controls.Add(this.buttonViewBug);
			this.Controls.Add(this.textBoxExisting);
			this.Controls.Add(this.buttonCancel);
			this.Controls.Add(this.buttonOk);
			this.Controls.Add(this.labelDescription);
			this.Controls.Add(this.radioButtonExisting);
			this.Controls.Add(this.radioButtonNew);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximizeBox = false;
			this.MaximumSize = new System.Drawing.Size(636, 285);
			this.MinimizeBox = false;
			this.MinimumSize = new System.Drawing.Size(636, 285);
			this.Name = "BugstarUI";
			this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
			this.Text = "R*TM Bugstar Integration";
			this.TopMost = true;
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.BugstarUI_FormClosing);
			this.Shown += new System.EventHandler(this.BugstarUI_Shown);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.RadioButton radioButtonNew;
		private System.Windows.Forms.RadioButton radioButtonExisting;
		private System.Windows.Forms.Label labelDescription;
		private System.Windows.Forms.Button buttonOk;
		private System.Windows.Forms.Button buttonCancel;
		private System.Windows.Forms.TextBox textBoxExisting;
		private System.Windows.Forms.Button buttonViewBug;
		private System.Windows.Forms.Label labelUsername;
		private System.Windows.Forms.Label labelPassword;
		private System.Windows.Forms.TextBox textBoxUsername;
		private System.Windows.Forms.TextBox textBoxPassword;
		private System.Windows.Forms.Button buttonLogin;
		private System.Windows.Forms.ProgressBar progressBarDuplicateSearch;
		private System.Windows.Forms.Label labelDuplicateSearch;
		private System.Windows.Forms.Button buttonDuplicateSearchStop;
	}
}
