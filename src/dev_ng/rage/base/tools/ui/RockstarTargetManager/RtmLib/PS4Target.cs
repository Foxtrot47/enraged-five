using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing.Imaging;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms; // For MessageBox

using ORTMAPILib;

namespace Rockstar.TargetManager
{
	////////////////////////////////////////////////////////////////////////////
	// PS4Target
	////////////////////////////////////////////////////////////////////////////
	public class PS4Target : Target
	{
		private  PS4TargetManager m_Tm;
		private  string m_Name;
		private  string m_Alias;
		internal ITarget m_TmapiTarget;
		private  ConsoleOutputEventHandler m_ConsoleOutputEventHandler;
		private  TestKitTtyParser m_TestKitTtyParser;
		private  DebugEventHandler m_DebugEventHandler;
		private  TargetEventHandler m_TargetEventHandler;
		private  bool m_Connected;
		internal bool m_IsTestKit;
		internal delegate void VoidCallbackVoid();
		private  VoidCallbackVoid m_WorkerThreadCallback;
		internal bool m_StartNewLog = false;

		////////////////////////////////////////////////////////////////////////
		// PS4Target::PS4Target
		////////////////////////////////////////////////////////////////////////
		internal PS4Target(string name, ITarget tmapiTarget, PS4TargetManager tm)
		{
			m_Tm = tm;
			m_Name = name;
			bool exception = false;
			try
			{
				m_Alias = tmapiTarget.Name;
			}
			catch
			{
				exception = true;
				Enabled = false;
			}
			m_TmapiTarget = tmapiTarget;
			m_TargetEventHandler = new TargetEventHandler(this);

			// Assume testkit until we know otherwise
			m_IsTestKit = true;

			// Don't make more TMAPI calls if the kit is unavailable
			if(!exception)
			{
				OnEnabledChanged(Enabled);

				// Check if this is a testkit or devkit
				bool hwAttributesFound = false;
				Array info = tmapiTarget.TargetInfo;
				foreach(INameValuePair nvp in info)
				{
					if(nvp.Name == "HwAttributes")
					{
						m_IsTestKit = ((nvp.Value & (uint)eHwAttributes.HW_ATTRIBS_TEST_KIT) != 0);
						hwAttributesFound = true;
						break;
					}
				}
				if(!hwAttributesFound)
				{
					Trace.WriteLine("PS4 "+name+": unable to fetch hardware attributes, assuming testkit.");
				}
			}
		}

		////////////////////////////////////////////////////////////////////////
		// PS4Target::Dispose
		////////////////////////////////////////////////////////////////////////
		protected override void Dispose(bool disposing)
		{
			try
			{
				if(disposing)
				{
					if(m_ConsoleOutputEventHandler != null) m_ConsoleOutputEventHandler.Dispose();
					if(m_DebugEventHandler != null)         m_DebugEventHandler.Dispose();
					if(m_TargetEventHandler != null)        m_TargetEventHandler.Dispose();
					if(m_Connected)                         m_TmapiTarget.ReleaseConnection();
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		////////////////////////////////////////////////////////////////////////
		// PS4Target::WorkerThreadUpdate
		////////////////////////////////////////////////////////////////////////
		internal void WorkerThreadUpdate()
		{
			lock(this)
			{
				var callback = Interlocked.Exchange(ref m_WorkerThreadCallback, null);
				if(callback != null)
				{
					callback();
				}

				if(m_DebugEventHandler != null)
				{
					m_DebugEventHandler.WorkerThreadUpdate();
				}
			}
		}

		////////////////////////////////////////////////////////////////////////
		// PS4Target::SetSetting
		////////////////////////////////////////////////////////////////////////
		private void SetSetting(string description, UInt32 key, Int32 value)
		{
			// The key, size, and type for individual settings can be
			// obtained by running ITarget.GetSettingsLayout, eg.
			//     m_TmapiTarget.GetSettingsLayout("c:/tmp/ps4_settings.xml");
			//
			// Looking at "${SCE_ROOT_DIR}/ORBIS/Tools/Target Manager Server/samples/tmapi/orcui/orctrl/SettingsPlugIn.cs",
			// its not really clear what ISetting.Destination should be set
			// to.  Some places use 0 as the default, and others ~0.  ~0
			// doesn't seem to work here, but 0 does.
			//
			// This overload assumes 4-byte integer.  Other SetSetting()
			// overloads can be added as required.
			//

			var settings = new ISetting[1];

			settings[0] = m_TmapiTarget.AllocSetting();
			settings[0].Destination = 0;
			settings[0].Key         = key;
			settings[0].Marked      = true;
			settings[0].Size        = 4;
			settings[0].Type        = eSettingType.SETTING_TYPE_INTEGER;
			settings[0].Value       = value;

			try
			{
				m_TmapiTarget.SetSettings(settings, true);
				TtyOutput("Set target setting \""+description+"\" to "+value.ToString()+".\n");
			}
			catch
			{
				TtyOutput("Failed to set target setting \""+description+"\" to "+value.ToString()+"\n");
			}
		}

		////////////////////////////////////////////////////////////////////////
		// PS4Target::TmCmdSubmitDoneExceptionEnabled
		////////////////////////////////////////////////////////////////////////
		private void TmCmdSubmitDoneExceptionEnabled(bool enable)
		{
			SetSetting("SubmitDone Exception", 0x7802a600, enable?1:0);
		}

		////////////////////////////////////////////////////////////////////////
		// PS4Target::TmCmdConfig
		////////////////////////////////////////////////////////////////////////
		private void TmCmdConfig(string str)
		{
			// Currently no config values we care about for PS4
		}

		////////////////////////////////////////////////////////////////////
		// PS4Target::ElfFilename
		////////////////////////////////////////////////////////////////////
		private string ElfFilename(string ps4Path)
		{
			if(ps4Path.StartsWith("/app0/"))
			{
				uint options;
				var driveChar = m_Tm.GetPFSDrive(out options);
				if(driveChar == 0)
				{
					TtyOutput("PlayStation File System drive not mapped.  Unable to access /app0/...\n");
					return null;
				}
				else
				{
					var driveStr = ""+(char)driveChar;
					var targetFolderOptions = options & (uint)(ePFSFlags.PFS_FLAG_TARGETFOLDER_USE_NAME | ePFSFlags.PFS_FLAG_TARGETFOLDER_USE_DOTTEDIP);
					bool useDottedIp = (targetFolderOptions == (uint)ePFSFlags.PFS_FLAG_TARGETFOLDER_USE_DOTTEDIP);
					if(!useDottedIp && targetFolderOptions != (uint)ePFSFlags.PFS_FLAG_TARGETFOLDER_USE_NAME)
					{
						TtyOutput("PlayStation File System unexpected target folder options value 0x"+options.ToString("x8")+".  Unable to access /app0/...\n");
						return null;
					}
					return Regex.Replace(ps4Path, @"^/app0/(.*)$", driveStr+":/"+(useDottedIp?m_Name:m_Alias)+"/data/app/$1");
				}
			}
			else if(ps4Path.StartsWith("/host/"))
			{
				return Regex.Replace(ps4Path, @"^/host/(.*)$", "$1");
			}
			else
			{
				TtyOutput("Unknown process path prefix (\""+ps4Path+"\").\n");
				return null;
			}
		}

		////////////////////////////////////////////////////////////////////
		// PS4Target::ElfFilename
		////////////////////////////////////////////////////////////////////
		private string ElfFilename(IProcessInfo pInfo)
		{
			return ElfFilename(pInfo.Path);
		}

		////////////////////////////////////////////////////////////////////////
		// PS4Target::TestKitTtyParser
		////////////////////////////////////////////////////////////////////////
		private class TestKitTtyParser
		{
			private PS4Target m_Target;

			private Regex m_RegexTmCmd                          = new Regex("^\\>\\>R\\*TM ");

			private Regex m_RegexNop                            = new Regex("^\\>\\>R\\*TM NOP\n$");
			private Regex m_RegexPrintAllThreadStacks           = new Regex("^\\>\\>R\\*TM PRINT_ALL_THREAD_STACKS\n$");
			private Regex m_RegexStopNoErrorReport              = new Regex("^\\>\\>R\\*TM STOP_NO_ERROR_REPORT\n$");
			private Regex m_RegexQuitf                          = new Regex("^\\>\\>R\\*TM QUITF:(.*)\n$");
			private Regex m_RegexGpuHang                        = new Regex("^\\>\\>R\\*TM GPU_HANG\n$");
			private Regex m_RegexCpuHang                        = new Regex("^\\>\\>R\\*TM CPU_HANG\n$");
			private Regex m_RegexConfig                         = new Regex("^\\>\\>R\\*TM CONFIG:(.*)\n$");
			private Regex m_RegexCreateDump                     = new Regex("^\\>\\>R\\*TM CREATE_DUMP\n$");
			private Regex m_RegexExceptionHandlerBegin          = new Regex("^\\>\\>R\\*TM EXCEPTION_HANDLER_BEGIN\n$");
			private Regex m_RegexExceptionHandlerEnd            = new Regex("^\\>\\>R\\*TM EXCEPTION_HANDLER_END:([^;]*);(.*)\n$");
			private Regex m_RegexSubmitDoneExceptionEnabled     = new Regex("^\\>\\>R\\*TM ORBIS_SUBMIT_DONE_EXCEPTION_ENABLED:(.*)\n$");

			private Regex m_RegexCrashProcName;
			private Regex m_RegexCrashElfFullPath = null;
			private Regex m_RegexNotifyAppCrash;
			private Regex m_RegexCoredumpComplete;
			private string m_CoredumpPath;

			private string m_ElfPs4FullPath = null;
			private string m_BugstarConfig;
			private string m_BugSummary;
			private string m_BugDescription;
			private bool m_EnableBugDupCheck = true;

			private enum State
			{
				NORMAL,
				EXPECT_EXCEPTION_HANDLER_END,
				EXPECT_CRASH_PROC_NAME,
				EXPECT_CRASH_ELF_FULL_PATH,
				EXPECT_NOTITY_APP_CRASH,
				EXPECT_COREDUMP_COMPLETE,
			};
			private State m_State = State.NORMAL;


			////////////////////////////////////////////////////////////////////
			// PS4Target::TestKitTtyParser::TestKitTtyParser
			////////////////////////////////////////////////////////////////////
			internal TestKitTtyParser(PS4Target target)
			{
				m_Target = target;

				// These regexes are relying on undocumented behaviour, and are
				// very likely going to be firmware version specific.
				//
				// Below are examples from different versions...
				//
				// 1.750.061 .. 2.030.001
				//      # proc name: game_orbis_beta.elf
				//      <118>[SceLncService] notifyAppCrash() pid={0x000001b7}, appId={0x0b0c1b08}, appLocalPid={0x11b08260}, coredumpPath={/user/data/sce_coredumps/CUSA00411_1417822469/}, corefileName={orbiscore-1417822469-0x000001b7-game_orbis_beta.elf.orbisdmp}, coredumpMode={0}, enableCrashReport={1}
				//      <118>[Syscore App] Coredump Complete : PID=0x228, exitStatus=0
				//
				m_RegexCrashProcName = new Regex("^# proc name: (.*)\n$");
				m_RegexNotifyAppCrash = new Regex("^\\<[0-9]+\\>\\[SceLncService\\]\\s+notifyAppCrash\\(\\).*[,\\s]coredumpPath\\s*=\\s*{([^}]+)}.*\n$");
				m_RegexCoredumpComplete = new Regex("^\\<[0-9]+\\>\\[Syscore App\\]\\s+Coredump Complete[\\s:].*\n$");
			}

			////////////////////////////////////////////////////////////////////
			// PS4Target::TestKitTtyParser::CheckState
			////////////////////////////////////////////////////////////////////
			private void CheckState(State s)
			{
				if(m_State != s)
				{
					m_Target.TtyOutput("R*TM: Synchronization error, unexpected state.  Is "+m_State+", expected "+s+".\n");
				}
			}

			////////////////////////////////////////////////////////////////////
			// PS4Target::TestKitTtyParser::EnsureState
			////////////////////////////////////////////////////////////////////
			private void EnsureState(State s)
			{
				CheckState(s);
				m_State = s;
			}

			////////////////////////////////////////////////////////////////////
			// PS4Target::TestKitTtyParser::Parse
			////////////////////////////////////////////////////////////////////
			internal string Parse(string tty)
			{
				// Check for target manager commands
				try
				{
					if(m_RegexTmCmd.IsMatch(tty))
					{
						// sysTmCmdNop
						if (m_RegexNop.IsMatch(tty))
						{
							EnsureState(State.NORMAL);
						}

						// sysTmCmdPrintAllThreadStacks
						else if(m_RegexPrintAllThreadStacks.IsMatch(tty))
						{
							// Currently no way to support this on testkits.
							EnsureState(State.NORMAL);
						}

						// sysTmCmdStopNoErrorReport
						else if(m_RegexStopNoErrorReport.IsMatch(tty))
						{
							// Don't need to do anything else here, the game will just sit
							// in an infinite loop waiting for the process to be killed.
							EnsureState(State.NORMAL);
						}

						// sysTmCmdQuitf
						else if(m_RegexQuitf.IsMatch(tty))
						{
							m_BugSummary = m_RegexQuitf.Replace(tty, "$1");
							Debug.Assert(m_EnableBugDupCheck == true);
							m_EnableBugDupCheck = true;
							EnsureState(State.NORMAL);
						}

						// sysTmCmdGpuHang
						else if(m_RegexGpuHang.IsMatch(tty))
						{
							m_BugSummary = "PS4 GPU Error";
							m_EnableBugDupCheck = false;
							EnsureState(State.NORMAL);
						}

						// sysTmCmdCpuHang
						else if(m_RegexCpuHang.IsMatch(tty))
						{
							m_BugSummary = "CPU Hang";
							m_EnableBugDupCheck = false;
							EnsureState(State.NORMAL);
						}

						// sysTmCmdConfig
						else if(m_RegexConfig.IsMatch(tty))
						{
							var config = m_RegexConfig.Replace(tty, "$1");
							m_Target.TmCmdConfig(config);
							EnsureState(State.NORMAL);
						}

						// sysTmCmdCreateDump
						else if(m_RegexCreateDump.IsMatch(tty))
						{
							// TODO: Is this possible on a testkit ?
							EnsureState(State.NORMAL);
						}

						// sysTmCmdExceptionHandlerBegin
						else if(m_RegexExceptionHandlerBegin.IsMatch(tty))
						{
							m_Target.BeginCaptureTtyOutput();
							CheckState(State.NORMAL);
							m_State = State.EXPECT_EXCEPTION_HANDLER_END;
						}

						// sysTmCmdExceptionHandlerEnd
						else if(m_RegexExceptionHandlerEnd.IsMatch(tty))
						{
							CheckState(State.EXPECT_EXCEPTION_HANDLER_END);
							m_State = State.EXPECT_CRASH_PROC_NAME;

							//var coredump    = m_RegexExceptionHandlerEnd.Replace(tty, "$1");  // always an empty string on PS4
							m_BugstarConfig = m_RegexExceptionHandlerEnd.Replace(tty, "$2");

							m_BugDescription = m_Target.EndCaptureTtyOutput();

							// Barf... during coredump processing, we get a callback for the game's PID being killed, and
							// then being created again. Make sure we don't start a new logfile when that happens.
							m_Target.m_StartNewLog = false;
						}

						// sysTmCmdSubmitDoneExceptionEnabled
						else if(m_RegexSubmitDoneExceptionEnabled.IsMatch(tty))
						{
							// This doesn't actually do anything on testkits right now, since the
							// "SubmitDone Exception" setting doesn't actually exist.  Leaving the
							// code here, as hopefully we can get Sony to add support.
							var val = m_RegexSubmitDoneExceptionEnabled.Replace(tty, "$1");
							var enable = Convert.ToInt32(val)!=0;
							m_Target.TmCmdSubmitDoneExceptionEnabled(enable);
							EnsureState(State.NORMAL);
						}
					}
				}
				catch(Exception e)
				{
					m_Target.TtyOutput("R*TM: Error processing command \""+tty+"\", "+e+"\n");
				}

				// Check for crash/coredump related messages

				// Process name
				if(m_RegexCrashProcName.IsMatch(tty))
				{
					CheckState(State.EXPECT_CRASH_PROC_NAME);
					m_State = State.EXPECT_CRASH_ELF_FULL_PATH;
					var elfName = m_RegexCrashProcName.Replace(tty, "$1");
					var escaped = Regex.Replace(elfName, "\\.", "\\.");
					try
					{
						m_RegexCrashElfFullPath = new Regex("^# (/.*[\\\\/]"+escaped+")\n$");
					}
					catch(Exception e)
					{
						m_Target.TtyOutput("R*TM: Error generating elf full path regex from input tty \""+tty+"\", "+e+"\n");
						m_State = State.EXPECT_NOTITY_APP_CRASH;
					}
				}

				// Elf full path
				else if(m_RegexCrashElfFullPath!=null && m_RegexCrashElfFullPath.IsMatch(tty))
				{
					CheckState(State.EXPECT_CRASH_ELF_FULL_PATH);
					m_State = State.EXPECT_NOTITY_APP_CRASH;
					m_ElfPs4FullPath = m_RegexCrashElfFullPath.Replace(tty, "$1");
					m_RegexCrashElfFullPath = null;
				}

				// Coredump begin
				else if(m_RegexNotifyAppCrash.IsMatch(tty))
				{
					CheckState(State.EXPECT_NOTITY_APP_CRASH);
					m_State = State.EXPECT_COREDUMP_COMPLETE;
					m_CoredumpPath = m_RegexNotifyAppCrash.Replace(tty, "$1");
				}

				// Coredump end
				else if(m_RegexCoredumpComplete.IsMatch(tty))
				{
					CheckState(State.EXPECT_COREDUMP_COMPLETE);
					m_State = State.NORMAL;

					m_Target.m_WorkerThreadCallback = () =>
					{
						var filesToCopy = new List<string>();

						if(m_ElfPs4FullPath == null)
						{
							m_Target.TtyOutput("R*TM: Warning, coredump completed with no known elf path.\n");
						}
						else
						{
							var elfLocalFullPath = m_Target.ElfFilename(m_ElfPs4FullPath);
							if(elfLocalFullPath == null)
							{
								m_Target.TtyOutput("R*TM: Warning, could not convert PS4 elf path, \""+m_ElfPs4FullPath+"\", to local path.  Please manually attach elf file to bug.\n");
							}
							else
							{
								filesToCopy.Add(elfLocalFullPath);
							}
						}

						CopyCoredumpFiles(ref filesToCopy, m_CoredumpPath);

						if(m_BugstarConfig == null)
						{
							m_Target.TtyOutput("R*TM: No Bugstar config info available, unable to raise bug.\n");
						}
						else
						{
							m_Target.TtyOutput("Raising bug...\n");
							try
							{
								var config = Bugstar.Config.FromString(m_BugstarConfig);
								m_Target.BaseRaiseBug(config, m_BugSummary, m_BugDescription, filesToCopy.ToArray(), m_EnableBugDupCheck);
							}
							catch(Exception e)
							{
								m_Target.TtyOutput("R*TM: Error attempting to raise bug, "+e+"\n");
							}
						}
						m_ElfPs4FullPath = null;
						m_CoredumpPath = null;
						m_BugstarConfig = null;
						m_BugSummary = null;
						m_BugDescription = null;
						m_EnableBugDupCheck = true;
						m_Target.m_StartNewLog = true;
					};
				}

				return tty;
			}

			////////////////////////////////////////////////////////////////////
			// PS4Target::TestKitTtyParser::CopyCoredumpFiles
			////////////////////////////////////////////////////////////////////
			private void CopyCoredumpFiles(ref List<string> filesToCopy, string coredumpPath)
			{
				try
				{
					// Check source path is in a format we expect.
					if(coredumpPath == null)
					{
						m_Target.TtyOutput("R*TM: Coredump path on console unknown.  Please manually find and copy.\n");
						return;
					}
					if(!coredumpPath.StartsWith("/user/"))
					{
						m_Target.TtyOutput("R*TM: Unexpected coredump path, \""+coredumpPath+"\".  Please manually copy files.\n");
						return;
					}

					// Check that we have access via the PlayStation File System.
					uint options;
					var driveChar = m_Target.m_Tm.GetPFSDrive(out options);
					if(driveChar == 0)
					{
						m_Target.TtyOutput("R*TM: PlayStation File System drive not mapped.  Please manually copy coredump files from \""+coredumpPath+"\".\n");
						return;
					}
					var driveStr = ""+(char)driveChar;
					var targetFolderOptions = options & (uint)(ePFSFlags.PFS_FLAG_TARGETFOLDER_USE_NAME | ePFSFlags.PFS_FLAG_TARGETFOLDER_USE_DOTTEDIP);
					bool useDottedIp = (targetFolderOptions == (uint)ePFSFlags.PFS_FLAG_TARGETFOLDER_USE_DOTTEDIP);
					if(!useDottedIp && targetFolderOptions != (uint)ePFSFlags.PFS_FLAG_TARGETFOLDER_USE_NAME)
					{
						m_Target.TtyOutput("R*TM: PlayStation File System unexpected target folder options value 0x"+options.ToString("x8")+".  Please manually copy coredump files from \""+coredumpPath+"\".\n");
						return;
					}

					// Host PC paths.
					var srcPath = Regex.Replace(coredumpPath, @"^/user/(.*)$", driveStr+":/"+(useDottedIp?m_Target.m_Name:m_Target.m_Alias)+"/$1");
					var dstPath = RockstarTargetManager.GetCrashdumpDir();

					// Get directory listing of everything in the console's directory.
					var srcFiles = new List<string>();
					foreach(var f in Directory.EnumerateFiles(srcPath))
					{
						// Enumerated files have a full path, so strip down to just the filename before adding to srcFiles.
						srcFiles.Add(Path.GetFileName(f));
					}

					// Build regex for converting coredump name(s) to our naming scheme.
					Regex coredumpRegex = new Regex("(.*)");
					string coredumpReplace = "$1";
					string dstNameBase = "";
					bool foundCoredump = false;
					foreach(var f in srcFiles)
					{
						if(f.EndsWith(".orbisdmp"))
						{
							// The source coredump name format depends on the console's firmware version.
							// Pre 2.00 (???) the format is
							//      orbiscore-1417832915-0x0000034a-game_orbis_beta.elf.orbisdmp
							var regex0 = new Regex("^orbiscore-[0-9]+-0x([0-9a-fA-F]+)-([^.]+)(\\..*)?\\.orbisdmp$");
							if(regex0.IsMatch(f))
							{
								var pid = Convert.ToUInt32(regex0.Replace(f, "$1"), 16);
								dstNameBase = regex0.Replace(f, "$2"+DateTime.Now.ToString("-yyyyMMdd-HHmmss-")+pid.ToString());
							}
							if(dstNameBase == "")
							{
								m_Target.TtyOutput("R*TM: Error, found coredump file, but didn't recognise name format, \""+Path.Combine(coredumpPath, f)+"\".\n");
								// Don't return here though, will still do our best to just copy everything in the directory.
							}
							else
							{
								coredumpRegex = new Regex(Regex.Replace(f, "^(.*)\\.orbisdmp$", "$1(-[0-9]+)?\\.(orbis[^.]+)$"));
								coredumpReplace = dstNameBase+"$1.$2";
							}
							foundCoredump = true;
							break;
						}
					}
					if(!foundCoredump)
					{
						m_Target.TtyOutput("R*TM: Error, unable to find coredump file in \""+coredumpPath+"\".\n");
						// Don't return here though, will still do our best to just copy everything in the directory.
					}

					// Copy across all the files.
					bool allFilesMoved = true;
					foreach(var sBase in srcFiles)
					{
						var s = Path.Combine(srcPath, sBase);

						string d;
						if(coredumpRegex.IsMatch(sBase))
						{
							d = Path.Combine(dstPath, coredumpRegex.Replace(sBase, coredumpReplace));
						}
						else
						{
							d = Path.Combine(dstPath, dstNameBase+"-"+sBase);
						}

						// If this is the manifest file, then we need to rename the file entries listed inside of it.
						if(s.EndsWith(".orbisdmpmanifest"))
						{
							try
							{
								using(var sstrm = new StreamReader(s))
								{
									using(var dstrm = new StreamWriter(d))
									{
										var manifestDmpRegex = new Regex("^(file[0-9]+\\.name: )"+coredumpRegex);
										var manifestDmpReplace = "$1"+dstNameBase+"$2.$3";
										var manifestOtherRegex = new Regex("^(file[0-9]+\\.name: )(.*)$");
										var manifestOtherReplace = "$1"+dstNameBase+"-$2";
										for(;;)
										{
											var line = sstrm.ReadLine();
											if(line == null)
											{
												break;
											}

											if(manifestDmpRegex.IsMatch(line))
											{
												line = manifestDmpRegex.Replace(line, manifestDmpReplace);
											}
											else if(manifestOtherRegex.IsMatch(line))
											{
												line = manifestOtherRegex.Replace(line, manifestOtherReplace);
											}

											dstrm.WriteLine(line);
										}
									}
								}
								File.Delete(s);
								m_Target.TtyOutput("R*TM: Moved \""+s+"\" -> \""+d+"\".\n");
								filesToCopy.Add(d);
							}
							catch(Exception)
							{
								m_Target.TtyOutput("R*TM: Error transfering coredump manifest \""+s+"\" -> \""+d+"\".  Please manually copy file.\n");
								allFilesMoved = false;
							}
						}

						// Otherwise, just copy the file.
						else
						{
							try
							{
								File.Copy(s, d);
								File.Delete(s);
								m_Target.TtyOutput("R*TM: Moved \""+s+"\" -> \""+d+"\".\n");
								filesToCopy.Add(d);
							}
							catch(Exception e)
							{
								m_Target.TtyOutput("R*TM: Error copying \""+s+"\" -> \""+d+"\".  Please manually copy file.\n");
								m_Target.TtyOutput(e+"\n");
								allFilesMoved = false;
							}
						}
					}

					// If all the files were successfully moved, then delete the now empty directory on the console.
					if(allFilesMoved)
					{
						try
						{
							Directory.Delete(srcPath);
						}
						catch(Exception e)
						{
							m_Target.TtyOutput("R*TM: Error removing directory \""+srcPath+"\" from console.\n");
							m_Target.TtyOutput(e+"\n");
						}
					}
				}
				catch(Exception e)
				{
					m_Target.TtyOutput("R*TM: Unexpected error copying files from console (\""+coredumpPath+"\").  Please manually copy files\n");
					m_Target.TtyOutput(e+"\n");
				}
			}
		}

		////////////////////////////////////////////////////////////////////////
		// PS4Target::Connected
		////////////////////////////////////////////////////////////////////////
		private void Connected()
		{
			if(Enabled && !m_Connected)
			{
				Debug.Assert(m_ConsoleOutputEventHandler == null);
				Debug.Assert(m_DebugEventHandler         == null);
				m_ConsoleOutputEventHandler = new ConsoleOutputEventHandler(this);
				m_DebugEventHandler = new DebugEventHandler(this);
				m_Connected = true;

				if(m_IsTestKit)
				{
					m_TestKitTtyParser = new TestKitTtyParser(this);
					PushTtyParser((str) => m_TestKitTtyParser.Parse(str));
				}

				// "PA Debug" needs to be enabled for
				// sce::Gnm::validateCommandBuffers() to work (called in the
				// case of a GPU crash).
				SetSetting("PA Debug", 0x78028b00, 1);

				// Make sure coredumps are setup correctly.
				SetSetting("Dump Level",                    0x6e040000, 1); // 1 = "Full Dump"
				SetSetting("Enable System Crash Reporting", 0x70010000, 1); // 1 = "On"
				SetSetting("Keep Corefiles",                0x70020000, 1); // 1 = "On"

				// Register as TTY listener if needed
				if (!m_Tm.TtyListener.IsRegistered(this))
				{
					TtyOutput("Attempting to look up Game LAN IP address for PS4 console " + m_Name + " that has just connected... ");
					string gameIP = LookupGameLanIpAddress();
					if (gameIP != null)
					{
						m_Tm.TtyListener.Register(gameIP, this);
						TtyOutput("Registered game LAN IP " + gameIP + " as a TTY listener.\n");
					}
					else
					{
						// I managed to get this to happen by unplugging the CAT-5 cables from my PS4 in various combinations.
						// Somehow PS4 target manager got into a bad state, and "orbis-ctrl info" gave this output:
						//   HostName: 10.11.25.227
						// <snip>
						//   DriverVersion: 2.0.0.30
						//   # [ERROR]: TMAPI_COMMS_ERR_NO_CONNECT. (0x80040246)
						TtyOutput("Failed to look up game LAN IP!\n");
						MessageBox.Show(null, "Unable to look up game LAN IP address for PS4 devkit at " + m_Name +
							", R*TM may not fuction as expected.\n\nPlease reboot your PS4 devkit.",
                            "Devkit in invalid state", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					}
					m_Tm.TtyListener.Register(m_Name, this);
					TtyOutput("Registered host LAN IP " + m_Name + " as a TTY listener.\n");
				}
			}
		}

		////////////////////////////////////////////////////////////////////////
		// PS4Target::Disconnected
		////////////////////////////////////////////////////////////////////////
		private void Disconnected()
		{
			if(m_Connected)
			{
				Debug.Assert(m_ConsoleOutputEventHandler != null);
				Debug.Assert(m_DebugEventHandler         != null);

				if(m_IsTestKit)
				{
					Debug.Assert(m_TestKitTtyParser != null);
					PopTtyParser();
					m_TestKitTtyParser = null;
				}

				m_ConsoleOutputEventHandler .Dispose();
				m_DebugEventHandler         .Dispose();
				m_ConsoleOutputEventHandler = null;
				m_DebugEventHandler         = null;
				m_Connected = false;
			}
		}

		////////////////////////////////////////////////////////////////////////
		// PS4Target::IsConnected
		////////////////////////////////////////////////////////////////////////
		public bool IsConnected()
		{
			return m_Connected;
		}

		////////////////////////////////////////////////////////////////////////
		// PS4Target::LookupGameLanIpAddress
		////////////////////////////////////////////////////////////////////////
		public string LookupGameLanIpAddress()
		{
			string gameIP = null;
			if (Enabled)
			{
				try
				{
					foreach (var nv in m_TmapiTarget.TargetInfo)
					{
						if (nv.Name == "GameLanIpAddress")
						{
							gameIP = nv.ValueString;
							break;
						}
					}
				}
				catch
				{
				}
			}
			return gameIP;
		}

		////////////////////////////////////////////////////////////////////////
		// PS4Target::OnEnabledChanged
		////////////////////////////////////////////////////////////////////////
		protected override void OnEnabledChanged(bool enabled)
		{
			lock(this)
			{
				if(!enabled)
				{
					m_StartNewLog = false;

					Disconnected();
				}
				else
				{
					// Note that accessing ITarget.GetConnectionState can throw when
					// powered off, so handle that the same was as being disconnected.
					eConnectionState connectionState;
					try
					{
						connectionState = m_TmapiTarget.ConnectionState;
					}
					catch
					{
						connectionState = eConnectionState.CONNECTION_AVAILABLE;
					}

					switch(connectionState)
					{
						case eConnectionState.CONNECTION_AVAILABLE:
						{
							Disconnected();
							break;
						}

						case eConnectionState.CONNECTION_CONNECTED:
						{
							Connected();
							break;
						}

						case eConnectionState.CONNECTION_IN_USE:
						{
							// CONNECTION_IN_USE is meant to mean that "someone else" is
							// already connected to the devkit.  But that "someone else"
							// is can actually be the current user.  This can happen
							// when the host PC is rebooted while still connected to the
							// devkit.
							try
							{
								string connected = m_TmapiTarget.ConnectionInfo; // user@host
								string user = Environment.UserName;
								string host = Environment.MachineName;
								if(connected == user+"@"+host)
								{
									m_TmapiTarget.ForceDisconnect();
									Trace.WriteLine("Current user@host ("+connected+") was already connected, now forcably disconnected and retrying connection");
									m_TmapiTarget.RequestConnection();
									Connected();
								}
							}
							catch
							{
								Disconnected();
							}
							break;
						}
					}
				}
			}
		}

		////////////////////////////////////////////////////////////////////////
		// PS4Target::SelectedIcon
		////////////////////////////////////////////////////////////////////////
		public override Icon SelectedIcon
		{
			get
			{
				if(Monitor.TryEnter(this, 1))
				{
					try
					{
						if(!m_Connected)
						{
							return Icon.NOT_AVAILABLE;
						}
						else if(!m_DebugEventHandler.IsAttached)
						{
							return Icon.NOT_RUNNING;
						}
						else
						{
							return Icon.RUNNING;
						}
					}
					finally
					{
						Monitor.Exit(this);
					}
				}
				return Icon.NOT_RUNNING;
			}
		}

		////////////////////////////////////////////////////////////////////////
		// PS4Target::Launch
		////////////////////////////////////////////////////////////////////////
		public override bool Launch()
		{
			lock(this)
			{
				string path = Config.GetExpandedEnvVar("TargetPath");
				string cmdLine = CommandLine;
				cmdLine += " -ttyframeprefix"
				        +  " -rockstartargetmanager"
				        +  " tty=" + HostPcIpAddr.GetStr() + ":" + NetworkTtyListener.PORT;
				string[] delims = { " ", "\t", "\n", Environment.NewLine };
				string[] argv = cmdLine.Split(delims, StringSplitOptions.RemoveEmptyEntries);
				m_WorkerThreadCallback = () =>
				{
					if(EnableLaunch)
					{
						if(m_StartNewLog)
						{
							RequestNewLogFile();
							m_StartNewLog = false;
						}

						try
						{
							// Note that we create the process suspended.  This
							// ensures we can debug attach to it before the
							// sysTmpCmdNop.  Not really essential, but prevents the
							// chance of debug spam from a spurious "crash".
							uint loadOptions = (uint)(eLoadOptions.LOAD_OPTIONS_DEFAULT);
							if(!m_IsTestKit)
							{
								loadOptions |= (uint)(eLoadOptions.LOAD_OPTIONS_FULL_DEBUG_REQUIRED | eLoadOptions.LOAD_OPTIONS_LOAD_SUSPENDED);
							}

							IProcess p = m_TmapiTarget.LoadProcessArgv(eDevice.DEVICE_HOST, path,
								loadOptions, 0, argv, Path.GetDirectoryName(path));

							// Manually call OnProcessCreate.  We will get the real
							// call eventually, but it can take quite a long time.
							// This will also resume the suspended process.
							m_DebugEventHandler.OnProcessCreate(p.Id);
						}
						catch
						{
						}
					}
				};
				return true;
			}
		}

		////////////////////////////////////////////////////////////////////////
		// PS4Target::EnableLaunch
		////////////////////////////////////////////////////////////////////////
		public override bool EnableLaunch
		{
			get
			{
				if(Monitor.TryEnter(this, 1))
				{
					try
					{
						return m_Connected && Config!=null;
					}
					finally
					{
						Monitor.Exit(this);
					}
				}
				return false;
			}
		}

		////////////////////////////////////////////////////////////////////////
		// PS4Target::Reboot
		////////////////////////////////////////////////////////////////////////
		public override void Reboot()
		{
			m_WorkerThreadCallback = () =>
			{
				lock(this)
				{
					if(EnableReboot)
					{
						Trace.WriteLine(String.Format("Rebooting {0}...", this.Name));
						if(m_StartNewLog)
						{
							RequestNewLogFile();
							m_StartNewLog = false;
						}
						m_DebugEventHandler.Reboot();
					}
				}
			};
		}

		////////////////////////////////////////////////////////////////////////
		// PS4Target::EnableReboot
		////////////////////////////////////////////////////////////////////////
		public override bool EnableReboot
		{
			get { return m_Connected; }
		}

		////////////////////////////////////////////////////////////////////////
		// PS4Target::FastReboot
		////////////////////////////////////////////////////////////////////////
		public override void FastReboot()
		{
			m_WorkerThreadCallback = () =>
			{
				lock(this)
				{
					if(EnableFastReboot)
					{
						Trace.WriteLine(String.Format("Fast rebooting {0}...", this.Name));
						if(m_StartNewLog)
						{
							RequestNewLogFile();
							m_StartNewLog = false;
						}
						m_DebugEventHandler.FastReboot();
					}
				}
			};
		}

		////////////////////////////////////////////////////////////////////////
		// PS4Target::EnableFastReboot
		////////////////////////////////////////////////////////////////////////
		public override bool EnableFastReboot
		{
			// The only reason we disable fast reboot during a coredump is that
			// killing the process just doesn't work.  If it does start working
			// in a future TMAPI version, then it can be re-enabled.
			get
			{
				if(Monitor.TryEnter(this, 1))
				{
					try
					{
						return m_Connected && m_DebugEventHandler.IsAttached;
					}
					finally
					{
						Monitor.Exit(this);
					}
				}
				return false;
			}
		}

		////////////////////////////////////////////////////////////////////////
		// PS4Target::Coredump
		////////////////////////////////////////////////////////////////////////
		public override void Coredump()
		{
			m_WorkerThreadCallback = () =>
			{
				lock(this)
				{
					if(EnableCoredump)
					{
						m_DebugEventHandler.TriggerCoredump();
					}
				}
			};
		}

		////////////////////////////////////////////////////////////////////////
		// PS4Target::EnableCoredump
		////////////////////////////////////////////////////////////////////////
		public override bool EnableCoredump
		{
			get
			{
				if(Monitor.TryEnter(this, 1))
				{
					try
					{
						var deh = m_DebugEventHandler;
						return deh!=null && deh.IsAttached && !deh.IsCoredumpInProgress();
					}
					finally
					{
						Monitor.Exit(this);
					}
				}
				return false;
			}
		}

		////////////////////////////////////////////////////////////////////////
		// PS4Target::ScreenShot
		////////////////////////////////////////////////////////////////////////
		public override bool ScreenShot(string filename)
		{
			lock(this)
			{
				// Code copied from Sony orctrl sample (ScreenShotPlugIn.cs)
				try
				{
					ScreenShotData config;
					Array data = (Array)m_TmapiTarget.ScreenShot(eScreenShotMode.SCREEN_SHOT_MODE_AUTO, out config);

		            Debug.Assert(config.PixelFormat == ePixelFormat.A8R8G8B8);

		            IntPtr pData = Marshal.UnsafeAddrOfPinnedArrayElement(data, 0);

		            Bitmap image = new Bitmap(
		                (int)config.Width,
		                (int)config.Height,
		                (int)config.Width * 4,
		                PixelFormat.Format32bppArgb,
		                pData
		            );

		            try
		            {
		                ImageFormat fmt = ImageFormat.Bmp;

		                switch (Path.GetExtension(filename).ToLower())
		                {
		                    case ".png":
		                        fmt = ImageFormat.Png;
		                        break;
		                    case ".jpeg":
		                    case ".jpg":
		                        fmt = ImageFormat.Jpeg;
		                        break;
		                }

		                string folder = Path.GetDirectoryName(filename);

		                if (folder != string.Empty)
		                {
		                    Directory.CreateDirectory(folder);
		                }

		                // See http://social.msdn.microsoft.com/Forums/en-US/netfxbcl/thread/b15357f1-ad9d-4c80-9ec1-92c786cca4e6/
		                // for why we make a copy.
		                Bitmap bitmap = new Bitmap(image);
		                bitmap.Save(filename, fmt);
		            }
		            catch (Exception e)
		            {
		                throw new Exception("Could not write to file : " + e.Message);
		            }

					return true;
				}
				catch (COMException /*e*/)
				{
				}
				return false;
			}
		}

		////////////////////////////////////////////////////////////////////////
		// PS4Target::EnableScreenShot
		////////////////////////////////////////////////////////////////////////
		public override bool EnableScreenShot
		{
			get{return m_Connected;}
		}

		////////////////////////////////////////////////////////////////////////
		// PS4Target::Name
		////////////////////////////////////////////////////////////////////////
		public override string Name
		{
			get{return m_Name;}
		}

		////////////////////////////////////////////////////////////////////////
		// PS4Target::Alias
		////////////////////////////////////////////////////////////////////////
		public override string Alias
		{
			get{return m_Alias;}
		}

		////////////////////////////////////////////////////////////////////////
		// PS4Target::PlatformId
		////////////////////////////////////////////////////////////////////////
		public override TargetPlatformId PlatformId
		{
			get{return TargetPlatformId.PS4;}
		}

		////////////////////////////////////////////////////////////////////////
		// Helper functions to make the TMAPI interfaces simpler

		////////////////////////////////////////////////////////////////////////
		// PS4Target::GetRegister
		////////////////////////////////////////////////////////////////////////
		static internal UInt64 GetRegister(IThread t, eRegisters r)
		{
			eRegisters[] regs = {r};
			return ((UInt64[])t.GetRegisters(regs))[0];
		}

		////////////////////////////////////////////////////////////////////////
		// PS4Target::SetRegister
		////////////////////////////////////////////////////////////////////////
		static internal void SetRegister(IThread t, eRegisters r, UInt64 v)
		{
			eRegisters[] regs = {r};
			UInt64[] vals = {v,0,0,0};
			t.SetRegisters(regs, vals);
		}

		////////////////////////////////////////////////////////////////////////
		// PS4Target::AddRegister
		////////////////////////////////////////////////////////////////////////
		static internal UInt64 AddRegister(IThread t, eRegisters r, Int64 add)
		{
			UInt64 val = (UInt64)((Int64)GetRegister(t, r) + add);
			SetRegister(t, r, val);
			return val;
		}

		////////////////////////////////////////////////////////////////////////
		// PS4Target::SubRegister
		////////////////////////////////////////////////////////////////////////
		static internal UInt64 SubRegister(IThread t, eRegisters r, Int64 sub)
		{
			return AddRegister(t, r, -sub);
		}

		////////////////////////////////////////////////////////////////////////
		// PS4Target::GetMemory
		////////////////////////////////////////////////////////////////////////
		static internal bool GetMemory(IProcess p, UInt64 addr, ref byte[] buf)
		{
			UInt32 badCount;
			buf = (byte[])p.GetMemory(addr, (uint)buf.Length, out badCount);
			return (badCount == 0);
		}

		////////////////////////////////////////////////////////////////////////
		// PS4Target::GetMemory
		////////////////////////////////////////////////////////////////////////
		static internal bool GetMemory(IProcess p, UInt64 addr, out UInt32 data)
		{
			data = 0;
			var buf = new byte[4];
			if(!GetMemory(p, addr, ref buf))
			{
				return false;
			}
			for(uint i=4; i-->0;)
			{
				data <<= 8;
				data += buf[i];
			}
			return true;
		}

		////////////////////////////////////////////////////////////////////////
		// PS4Target::GetMemory
		////////////////////////////////////////////////////////////////////////
		static internal bool GetMemory(IProcess p, UInt64 addr, out UInt64 data)
		{
			data = 0;
			var buf = new byte[8];
			if(!GetMemory(p, addr, ref buf))
			{
				return false;
			}
			for(uint i=8; i-->0;)
			{
				data <<= 8;
				data += buf[i];
			}
			return true;
		}

		////////////////////////////////////////////////////////////////////////
		// PS4Target::GetMemory
		////////////////////////////////////////////////////////////////////////
		static internal bool GetMemory(IProcess p, UInt64 addr, out string str)
		{
			return GetString(addr, Encoding.UTF8, 1, out str, (UInt64 a, ref byte[] buf) => GetMemory(p, a, ref buf));
		}

		////////////////////////////////////////////////////////////////////////
		// PS4Target::WriteUInt64
		////////////////////////////////////////////////////////////////////////
		static internal void WriteUInt64(IProcess p, UInt64 addr, UInt64 val)
		{
			byte[] mem = {
				(byte)(val      &0xff), (byte)((val>>8) &0xff), (byte)((val>>16)&0xff), (byte)((val>>24)&0xff),
				(byte)((val>>32)&0xff), (byte)((val>>40)&0xff), (byte)((val>>48)&0xff), (byte)((val>>56)&0xff)};
			p.SetMemory(addr, mem);
		}

		////////////////////////////////////////////////////////////////////////
		// PS4Target::GetThreadInfo
		////////////////////////////////////////////////////////////////////////
		static internal IThreadInfo GetThreadInfo(Array threadInfos, UInt32 tid)
		{
			foreach(IThreadInfo t in threadInfos)
			{
				if(t.Id == tid)
				{
					return t;
				}
			}
			return null;
		}


		////////////////////////////////////////////////////////////////////////
		// PS4Target::ConsoleOutputEventHandler
		////////////////////////////////////////////////////////////////////////
		internal class ConsoleOutputEventHandler : IEventConsoleOutput, IDisposable
		{
			private PS4Target m_Target;
			private string m_BufferedLine = "";


			////////////////////////////////////////////////////////////////////
			// PS4Target::ConsoleOutputEventHandler::ConsoleOutputEventHandler
			////////////////////////////////////////////////////////////////////
			public ConsoleOutputEventHandler(PS4Target target)
			{
				m_Target = target;
				m_Target.m_TmapiTarget.AdviseConsoleOutputEvents(this);
			}

			////////////////////////////////////////////////////////////////////
			// PS4Target::ConsoleOutputEventHandler::~ConsoleOutputEventHandler
			////////////////////////////////////////////////////////////////////
			~ConsoleOutputEventHandler()
			{
				Dispose(false);
			}

			////////////////////////////////////////////////////////////////////
			// PS4Target::ConsoleOutputEventHandler::Dispose
			////////////////////////////////////////////////////////////////////
			public void Dispose()
			{
				Dispose(true);
				GC.SuppressFinalize(this);
			}

			////////////////////////////////////////////////////////////////////
			// PS4Target::ConsoleOutputEventHandler::Dispose
			////////////////////////////////////////////////////////////////////
			protected virtual void Dispose(bool disposing)
			{
				if(disposing)
				{
					if(m_Target != null)    m_Target.m_TmapiTarget.UnadviseConsoleOutputEvents(this);
				}
			}

			////////////////////////////////////////////////////////////////////
			// PS4Target::ConsoleOutputEventHandler::OnBufferReady
			////////////////////////////////////////////////////////////////////
			public void OnBufferReady(IBufferReadyEvent pEvent)
			{
			}

			////////////////////////////////////////////////////////////////////
			// PS4Target::ConsoleOutputEventHandler::OnConsoleOutput
			////////////////////////////////////////////////////////////////////
			public void OnConsoleOutput(IConsoleOutputEvent pEvent)
			{
				lock(m_Target)
				{
					// For ease of parsing the input tty is buffered and split on newlines.

					string input = pEvent.Text;
					m_BufferedLine += input;

					int start = 0;
					for(;;)
					{
						int newline = m_BufferedLine.IndexOf('\n', start);
						if(newline == -1)
						{
							m_BufferedLine = m_BufferedLine.Substring(start);
							break;
						}
						else
						{
							var line = m_BufferedLine.Substring(start, newline-start+1);
							start = newline+1;

							// There's a known issue in the SDK / firmware whereby GPU faults cause the kit
							// to get into a bad state and future dumps fail to generate. Detect the TTY
							// message that's output in these cases, and warn the user via a MessageBox.
							if(line.Contains("mDBG: Rejected dupulicated dump request"))
							{
								MessageBox.Show(null, "R*TM has detected that the PS4 devkit at " + m_Target.Name +
									" has apparently rejected a duplicate dump request.\n" +
									"This usually indicates that the devkit is in a bad state and will require " +
									"rebooting\n\n" +
									"*** It is unlikely that the devkit will generate a crash dump until it is rebooted! ***",
		                            "Devkit in invalid state", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							}

							m_Target.TtyOutput(line);
						}
					}
				}
			}
		}


		////////////////////////////////////////////////////////////////////////
		// PS4Target::DebugEventHandler
		////////////////////////////////////////////////////////////////////////
		private class DebugEventHandler : IEventDebug, IDisposable
		{
			// TODO: Once both GTA and RDR runtime code is updated to use rage::sysException::TargetCrashCallback, then this code should be
			// changed to stop using the deprecated rage::sysStack::TargetCrashCallback.
			private static string TARGET_CRASH_CALLBACK_BASE_NAME    = "rage::sysStack::TargetCrashCallback";
// 			private static string TARGET_CRASH_CALLBACK_BASE_NAME    = "rage::sysException::TargetCrashCallback";
			private static string TARGET_CRASH_CALLBACK_NAME         = TARGET_CRASH_CALLBACK_BASE_NAME+"(unsigned int)";
			private static string TARGET_GET_BUGSTAR_RTM_CONFIG_NAME = "CBugstarIntegration::GetRockstarTargetManagerConfig()";
			private static string TARGET_GPU_CRASH_CALLBACK_NAME     = "rage::OrbisGpuCrashCallback(unsigned long, unsigned long, unsigned long)";
			private static string[] TARGET_SYMBOLS =
			{
				TARGET_CRASH_CALLBACK_NAME,
				TARGET_GET_BUGSTAR_RTM_CONFIG_NAME,
				TARGET_GPU_CRASH_CALLBACK_NAME
			};

			private PS4Target m_Target;
			private List<IProcess> m_AttachedProcesses = new List<IProcess>();
			private delegate void VoidCallbackBool(bool b);
			private delegate void VoidCallbackBoolUInt64(bool b, UInt64 val);
			private struct StopNotificationCallback { internal VoidCallbackBool callback; internal DateTime utcTimeout; internal VoidCallbackVoid resumeThreads; }
			private Dictionary<UInt32, VoidCallbackVoid> m_OnCoredumpCompletedCallbacks = new Dictionary<UInt32, VoidCallbackVoid>();
			private Dictionary<UInt32, StopNotificationCallback> m_OnStopNotificationCallback = new Dictionary<UInt32, StopNotificationCallback>();
			private Dictionary<UInt32, AsyncSymbolLookup> m_AsyncSymbolLookup = new Dictionary<UInt32, AsyncSymbolLookup>();
			private Dictionary<UInt32, string> m_CmpFileCache = new Dictionary<UInt32, string>();
			private bool m_EnableBugDupCheck = true;
			private UInt64 m_SegmentBaseAddress = 0;


			////////////////////////////////////////////////////////////////////
			// PS4Target::DebugEventHandler::DebugEventHandler
			////////////////////////////////////////////////////////////////////
			public DebugEventHandler(PS4Target target)
			{
				m_Target = target;
				AttachToExistingProcesses();
				target.m_TmapiTarget.AdviseDebugEvents(this);
			}

			////////////////////////////////////////////////////////////////////
			// PS4Target::DebugEventHandler::~DebugEventHandler
			////////////////////////////////////////////////////////////////////
			~DebugEventHandler()
			{
				Dispose(false);
			}

			////////////////////////////////////////////////////////////////////
			// PS4Target::DebugEventHandler::Dispose
			////////////////////////////////////////////////////////////////////
			public void Dispose()
			{
				Dispose(true);
				GC.SuppressFinalize(this);
			}

			////////////////////////////////////////////////////////////////////
			// PS4Target::DebugEventHandler::Dispose
			////////////////////////////////////////////////////////////////////
			protected virtual void Dispose(bool disposing)
			{
				if(disposing)
				{
					if(m_AttachedProcesses != null)
					{
						foreach(IProcess proc in m_AttachedProcesses)
						{
							// Do our best to detach from all processes, but not
							// the end of the world if some fail.  Actually some
							// will "fail" with TMAPI_COMMS_ERR_INVALID_PID, yet
							// they do actually detach in this case.
							try
							{
								proc.Detach((uint)eDetachFlags.DETACH_RESUME);
							}
							catch
							{
							}
						}
					}

					if(m_Target != null)    m_Target.m_TmapiTarget.UnadviseDebugEvents(this);
				}
			}

			////////////////////////////////////////////////////////////////////
			// PS4Target::DebugEventHandler::IsCoredumpInProgress
			////////////////////////////////////////////////////////////////////
			internal bool IsCoredumpInProgress()
			{
				return m_OnCoredumpCompletedCallbacks.Count > 0;
			}

			////////////////////////////////////////////////////////////////////
			// PS4Target::DebugEventHandler::CheckForConnectionThread
			////////////////////////////////////////////////////////////////////
			internal void CheckForConnectionThread()
			{
				var procInfos = (Array)m_Target.m_TmapiTarget.ProcessInfoSnapshot;

				foreach(IProcess proc in m_AttachedProcesses)
				{
					// Find and suspend the target process
					bool resume = false;
					foreach(IProcessInfo pi in procInfos)
					{
						if(pi.Id == proc.Id)
						{
							if((pi.Attributes & eProcessAttr.PROCESS_ATTR_SUSPENDED) == 0)
							{
								IProcessInfo outProcInfo;
								Object outThreadInfo;
								Object outFailedThreads;
								proc.Suspend(out outProcInfo, out outThreadInfo, out outFailedThreads);
								resume = true;
							}
							break;
						}
					}

					try
					{
						// Locate the "R*TM" thread
						var threads = (Array)proc.ThreadInfoSnapshot;
						foreach (IThreadInfo tinfo in threads)
						{
							if (tinfo.Name == "R*TM")
							{
								var t = tinfo.ThreadInterface;
								if (GetRegister(t, eRegisters.REG_ID_RDI) == 0x4d542a52) // "R*TM"
								{
									// Set RSI to 1 to cause the game to continue
									var rsi = GetRegister(t, eRegisters.REG_ID_RSI);
									byte[] flag = { 1 };
									proc.SetMemory(rsi, flag);
								}
								break;
							}
						}

						// Find the executable load address
						var modules = (Array)proc.Modules;
						foreach (IModule mod in modules)
						{
							// We only care about the main module
							if (mod.Id != 0)
							{
								continue;
							}

							// Look for the executable segment
							var segments = (Array)mod.Segments;
							foreach (ISegmentInfo seg in segments)
							{
								if ((seg.Attributes & eSegmentAttr.SEGMENT_ATTR_EXECUTABLE) != 0)
								{
									m_SegmentBaseAddress = seg.BaseAddress;
									break;
								}
							}
							break;
						}
					}
					catch
					{
					}

					// Resume the process
					if(resume)
					{
						proc.Resume(null);
					}
				}
			}

			////////////////////////////////////////////////////////////////////
			// PS4Target::DebugEventHandler::AttachToExistingProcesses
			////////////////////////////////////////////////////////////////////
			private void AttachToExistingProcesses()
			{
				Array procInfos = null;
				try
				{
					procInfos = (Array)m_Target.m_TmapiTarget.ProcessInfoSnapshot;
				}
				catch
				{
				}
				if(procInfos != null)
				{
					foreach(IProcessInfo info in procInfos)
					{
						try
						{
							OnProcessCreate(info.Id);
						}
						catch
						{
						}
					}
				}
				if(m_AttachedProcesses.Count > 0)
				{
					m_Target.m_StartNewLog = true;
				}
			}

			////////////////////////////////////////////////////////////////////
			// PS4Target::DebugEventHandler::WorkerThreadUpdate
			////////////////////////////////////////////////////////////////////
			internal void WorkerThreadUpdate()
			{
				// When the game is launched as a deployed build (from
				// "/app0/eboot.bin"), we don't get an OnProcessCreate callback,
				// so instead we need to keep looking for new processes.
				if(m_AttachedProcesses.Count == 0)
				{
					AttachToExistingProcesses();
				}

				DateTime utcNow = DateTime.UtcNow;
				bool timedout;
				do
				{
					timedout = false;
					VoidCallbackBool timedoutCallback = null;
					foreach(KeyValuePair<UInt32,StopNotificationCallback> e in m_OnStopNotificationCallback)
					{
						if(utcNow > e.Value.utcTimeout)
						{
							m_OnStopNotificationCallback.Remove(e.Key);

							// If the other threads are all suspended, then
							// it is possible that the callback has just
							// deadlocked.  Resume the other threads and try
							// again.
							if(e.Value.resumeThreads != null)
							{
								TtyOutput("First attempt at running target callback timed out, resuming all threads in case of deadlock.\n");
								StopNotificationCallback snc;
								snc.callback      = e.Value.callback;
								snc.utcTimeout    = utcNow.AddSeconds(10);
								snc.resumeThreads = null;
								m_OnStopNotificationCallback.Add(e.Key, snc);
								e.Value.resumeThreads();
							}

							// Otherwise if we have already retried with the
							// other threads resumed, then the callback has
							// failed.
							else
							{
								TtyOutput("Second attempt at running target callback timed out, failing callback.\n");
								timedoutCallback = e.Value.callback;
							}

							timedout = true;
							break;
						}
					}
					if(timedoutCallback != null)
					{
						timedoutCallback(false);
					}
				}
				while(timedout);
			}

			////////////////////////////////////////////////////////////////////
			// PS4Target::DebugEventHandler::AddStopNotificationCallback
			////////////////////////////////////////////////////////////////////
			private void AddStopNotificationCallback(UInt32 pid, VoidCallbackVoid resumeThreads, VoidCallbackBool callback)
			{
				StopNotificationCallback snc;
				snc.callback = callback;
				snc.utcTimeout = DateTime.UtcNow.AddSeconds(10);
				snc.resumeThreads = resumeThreads;
				m_OnStopNotificationCallback.Add(pid, snc);
			}

			////////////////////////////////////////////////////////////////////
			// PS4Target::DebugEventHandler::IsAttached
			////////////////////////////////////////////////////////////////////
			internal bool IsAttached
			{
				get{return m_AttachedProcesses.Count > 0;}
			}

			////////////////////////////////////////////////////////////////////
			// PS4Target::DebugEventHandler::Reboot
			////////////////////////////////////////////////////////////////////
			internal void Reboot()
			{
				var procs = m_AttachedProcesses;
				foreach(IProcess proc in procs)
				{
					try { proc.Resume(null);        } catch {}
					try { proc.CancelCoredump();    } catch {}
					// If there was actually a coredump being generated (and .:
					// a coredump handler present), then DetachAllProcesses will
					// clear that.
				}

				DetachAllProcesses();
				m_Target.m_TmapiTarget.Reboot();
			}

			////////////////////////////////////////////////////////////////////
			// PS4Target::DebugEventHandler::FastReboot
			////////////////////////////////////////////////////////////////////
			internal void FastReboot()
			{
				var procs = m_AttachedProcesses;
				foreach(IProcess proc in procs)
				{
					try { proc.Resume(null);        } catch {}
					try { proc.CancelCoredump();    } catch {}
					try { proc.Kill();              } catch {}
				}

				DetachAllProcesses();
			}

			////////////////////////////////////////////////////////////////////
			// PS4Target::DebugEventHandler::OnCoredumpCompleted
			////////////////////////////////////////////////////////////////////
			public void OnCoredumpCompleted(ICoredumpCompletedEvent pEvent)
			{
				lock(m_Target)
				{
					UInt32 pid = pEvent.ProcessId;
					TtyOutput("\nCoredump Done [pid:0x"+pid.ToString("x8")+", result:0x"+pEvent.RawResult.ToString("x8")+"]\n");
					VoidCallbackVoid callback;
					// Warning: If the console is rebooted during a coredump,
					// then earlish during the next launched process, we get a
					// coredump completed callback.  So there may not be a
					// callback registerred for this pid.
					if(m_OnCoredumpCompletedCallbacks.TryGetValue(pid, out callback))
					{
						m_OnCoredumpCompletedCallbacks.Remove(pid);
					}
					if(callback != null)
					{
						callback();
					}
				}
			}

			////////////////////////////////////////////////////////////////////
			// PS4Target::DebugEventHandler::OnCoredumpInProgress
			////////////////////////////////////////////////////////////////////
			public void OnCoredumpInProgress(ICoredumpInProgressEvent pEvent)
			{
				lock(m_Target)
				{
					TtyOutput("... "+pEvent.Percentage.ToString()+"%\n");
				}
			}

			////////////////////////////////////////////////////////////////////
			// PS4Target::DebugEventHandler::OnDynamicLibraryLoad
			////////////////////////////////////////////////////////////////////
			public void OnDynamicLibraryLoad(IDynamicLibraryLoadEvent pEvent)
			{
			}

			////////////////////////////////////////////////////////////////////
			// PS4Target::DebugEventHandler::OnDynamicLibraryUnload
			////////////////////////////////////////////////////////////////////
			public void OnDynamicLibraryUnload(IDynamicLibraryUnloadEvent pEvent)
			{
			}

			////////////////////////////////////////////////////////////////////
			// PS4Target::DebugEventHandler::OnProcessCreate
			////////////////////////////////////////////////////////////////////
			internal void OnProcessCreate(UInt32 pid)
			{
				// Due to the manual call, we may already be attached to the
				// process.
				foreach(IProcess p in m_AttachedProcesses)
				{
					if(p.Id == pid)
					{
						return;
					}
				}

				// There seems to be some weird timing issue where
				// ProcessInfoSnapshot doesn't immediately contain the new
				// IProcessInfo
				const uint MAX_TRIES = 500;
				uint retry = MAX_TRIES;
				while(retry != 0)
				{
					try
					{
						Array procInfos = (Array)m_Target.m_TmapiTarget.ProcessInfoSnapshot;
						foreach(IProcessInfo info in procInfos)
						{
							if(info.Id == pid)
							{
								IProcess proc = info.ProcessInterface;

								// Only attempt to attach to the process if this is a devkit.
								if(!m_Target.m_IsTestKit)
								{
									try
									{
										proc.Attach(0);
									  //Trace.WriteLine("Attached to process 0x"+pid.ToString("x8"));
									}
									catch(Exception e)
									{
										COMException ce = e as COMException;
	                                    if (ce != null)
	                                    {
	                                        // We seem to be getting TMAPI_COMMS_ERR_ALREADY_ATTACHED, yet we are not properly attached,
	                                        // and won't get OnStopNotification calls.  Detach and try again.
	                                        //Trace.WriteLine("Failed to attach to process 0x"+pid.ToString("x8")
	                                        //    +", HRESULT = "+((eErrorCode)ce.ErrorCode).ToString()+" (0x"+ce.ErrorCode.ToString("x8")+")");
	                                        if ((eErrorCode)ce.ErrorCode == eErrorCode.TMAPI_COMMS_ERR_ALREADY_ATTACHED && retry != 0)
	                                        {
	                                            //Trace.WriteLine("Detaching and trying again to connect to process 0x"+pid.ToString("x8"));
	                                            --retry;
	                                            try
	                                            {
	                                                proc.Detach(0);
	                                            }
	                                            catch
	                                            {
	                                                // Hahaha, great, Detach can throw TMAPI_COMMS_ERR_NOT_ATTACHED o_O
	                                                //Trace.WriteLine("Failed to detach from process 0x"+pid.ToString("x8")
	                                                //    +", HRESULT = "+((eErrorCode)ce.ErrorCode).ToString()+" (0x"+ce.ErrorCode.ToString("x8")+")");
	                                            }
	                                        }
	                                        else
	                                        {
	                                            Trace.WriteLine(ce.ToString());
	                                        }
	                                    }
	                                    else
	                                    {
	                                        Trace.WriteLine(e.ToString());
	                                    }
										break;
									}
								}

								if(m_Target.m_StartNewLog)
								{
									m_Target.RequestNewLogFile();
								}
								m_Target.m_StartNewLog = true;
								m_AttachedProcesses.Add(proc);

								// Connection thread and resuming the process
								// can only be done on a devkit.  Plus there is
								// no need for async symbol looked on a testkit
								// as we can never stop the process.
								if(!m_Target.m_IsTestKit)
								{
									// TODO: Remove this deprecated connection
									// thread support once a build has been released
									// where the runtime code does the initial R*TM
									// connection via the TTY socket.
									CheckForConnectionThread();

									// Doing small reads from the .cmp file across PFS
									// is extremely slow.  Copy it locally first.
									CacheCmpFile(info);

									// Now that we are attached, resume the potentially
									// suspended process.  The process will be suspended if
									// it was created by Launch or started from the game
									// launch batchfiles.
									try
									{
										proc.Resume(null);
									}
									catch
									{
									}

									// Kick off asynchronous symbol lookup
									m_AsyncSymbolLookup.Add(pid, new AsyncSymbolLookup(CmpFilename(pid), TargetPlatformId.PS4, TARGET_SYMBOLS, m_SegmentBaseAddress));
								}

								return;
							}
						}
					}
					catch
					{
					}
					Thread.Sleep(10);
				}

				Trace.WriteLine("Repeatedly failed to attach to process 0x"+pid.ToString("x8"));
			}

			////////////////////////////////////////////////////////////////////
			// PS4Target::DebugEventHandler::OnProcessCreate
			////////////////////////////////////////////////////////////////////
			public void OnProcessCreate(IProcessCreateEvent pEvent)
			{
				lock(m_Target)
				{
					OnProcessCreate(pEvent.ProcessId);
				}
			}

			////////////////////////////////////////////////////////////////////
			// PS4Target::DebugEventHandler::OnProcessExit
			////////////////////////////////////////////////////////////////////
			public void OnProcessExit(IProcessExitEvent pEvent)
			{
				lock(m_Target)
				{
					DetachProcess(pEvent.ProcessId);
				}
			}

			////////////////////////////////////////////////////////////////////
			// PS4Target::DebugEventHandler::OnProcessKill
			////////////////////////////////////////////////////////////////////
			public void OnProcessKill(IProcessKillEvent pEvent)
			{
				lock(m_Target)
				{
					DetachProcess(pEvent.ProcessId);
				}
			}

			////////////////////////////////////////////////////////////////////
			// PS4Target::DebugEventHandler::OnProcessLoading
			////////////////////////////////////////////////////////////////////
			public void OnProcessLoading(IProcessLoadingEvent pEvent)
			{
			}

			////////////////////////////////////////////////////////////////////
			// PS4Target::DebugEventHandler::OnStopNotification
			////////////////////////////////////////////////////////////////////
			public void OnStopNotification(IStopNotificationEvent pEvent)
			{
				lock(m_Target)
				{
					IStopNotification notification = pEvent.StopNotification;
					IProcessInfo procInfo = notification.ProcessInfo;
					UInt32 tid = notification.ThreadId;
					UInt32 pid = procInfo.Id;
					IThreadInfo threadInfo = GetThreadInfo((Array)notification.Threads, tid);
					IThread thread = threadInfo.ThreadInterface;
					IProcess proc = procInfo.ProcessInterface;
					UInt64 rip = GetRegister(thread, eRegisters.REG_ID_RIP);

					// If this process is waiting for the completion of the target
					// crash callback, then process that.
					bool hasSnc = false;
					StopNotificationCallback snc;
					if(m_OnStopNotificationCallback.TryGetValue(pid, out snc))
					{
						m_OnStopNotificationCallback.Remove(pid);
						hasSnc = true;
					}
					if(hasSnc)
					{
						bool callbackValid = true;

						if(rip != 0)
						{
							TtyOutput("Registered target callback failed.  Target stopped unexpectedly at 0x"+rip.ToString("x16")+"\n");
							callbackValid = false;
						}

						snc.callback(callbackValid);
					}

					// Initial stop notification handling.
					else
					{
						// Check for custom Rockstar Target Manager calls.
						// For Orbis, these are encoded as
						//      cd 41           int 41h         ; __debugbreak()
						//      eb ..           jmp short 0f
						//      52 2a 54 4d     .byte "R*TM"
						//      ..              .byte opcode
						//      ........        ; any opcode data
						//  0:
						//
						// Note that in debug builds, the jmp is being encoded as a
						// jmp near instead of a jmp short, so check for eb .. .. .. ..
						// as well.
						//
						bool standardCrashHandling = true;
						bool gpuHang = false;
						bool processResume = false;
						string bugSummary = null;
						if(procInfo.StopReason.Code == eStopNotificationReason.STOP_REASON_SIGBUS)
						{
							try
							{
								UInt32 badCount;
								byte[] mem = (byte[])proc.GetMemory(rip, 12, out badCount);
								if(badCount == 0)
								{
									if((mem[0]==0xcd && mem[1]==0x41 && mem[2]==0xeb && mem[4]==0x52 && mem[5]==0x2a && mem[6]==0x54 && mem[7]==0x4d)
									 ||(mem[0]==0xcd && mem[1]==0x41 && mem[2]==0xe9 && mem[7]==0x52 && mem[8]==0x2a && mem[9]==0x54 && mem[10]==0x4d))
									{
										bool jmpShort = (mem[2]==0xeb);
										TMCommands opcode = (TMCommands)mem[jmpShort?8:11];
										int size = mem[3]-5;
										if(size >= 0)
										{
											byte[] data = size>0
												? (byte[])proc.GetMemory(rip+(UInt64)(jmpShort?9:12), (uint)size, out badCount)
												: null;
											if(badCount == 0)
											{
												standardCrashHandling = false;
												processResume = true;
												switch(opcode)
												{
													case TMCommands.NOP:
													{
														break;
													}

													case TMCommands.PRINT_ALL_THREAD_STACKS:
													{
														PrintAllThreadStacks(pEvent);
														break;
													}

													case TMCommands.STOP_NO_ERROR_REPORT:
													{
														TtyOutput("\nsysTmCmdStopNoErrorReport recieved\n\n");
														PrintThreadStack("", proc, thread);
														processResume = false;
														break;
													}

													case TMCommands.QUITF:
													{
														UInt64 rdi = GetRegister(thread, eRegisters.REG_ID_RDI);
														if(GetMemory(proc, rdi, out bugSummary))
														{
															bugSummary = "Quitf \""+bugSummary+"\"";
														}
														standardCrashHandling = true;
														processResume = false;
														break;
													}

													case TMCommands.GPU_HANG:
													{
														standardCrashHandling = true;
														processResume = false;
														gpuHang = true;
														bugSummary = "PS4 GPU Error";
														m_EnableBugDupCheck = false;
														break;
													}

													case TMCommands.CPU_HANG:
													{
														standardCrashHandling = true;
														processResume = false;
														bugSummary = "CPU Hang";
														m_EnableBugDupCheck = false;
														break;
													}

													case TMCommands.CONFIG:
													{
														UInt64 rdi = GetRegister(thread, eRegisters.REG_ID_RDI);
														string config;
														if(GetMemory(proc, rdi, out config))
														{
															m_Target.TmCmdConfig(config);
														}
														break;
													}

													case TMCommands.CREATE_DUMP:
													{
														standardCrashHandling = true;
														bugSummary = "Forced dump";
														m_EnableBugDupCheck = false;
														break;
													}

													case TMCommands.ORBIS_SUBMIT_DONE_EXCEPTION_ENABLED:
													{
														var enable = (GetRegister(thread, eRegisters.REG_ID_RDI) & 0xff) != 0;
														m_Target.TmCmdSubmitDoneExceptionEnabled(enable);
														break;
													}

													default:
													{
														standardCrashHandling = true;
														processResume = false;
														break;
													}
												}
											}
										}
									}
								}
							}
							catch
							{
							}
						}

						if (IsCoredumpInProgress())
						{
							Trace.WriteLine("Warning: Got stop notification while writing core dump, new notification ignored.");
						}

						// Was handled as a special target manager call?
						else if(!standardCrashHandling)
						{
							if(processResume)
							{
								// Skip over the __debugbreak and resume
								AddRegister(thread, eRegisters.REG_ID_RIP, 2);
								UInt32[] tids = {tid};
								proc.Resume(tids);
							}
						}

						// GPU fault?
						else if(procInfo.StopReason.Code == eStopNotificationReason.STOP_REASON_GPU_FAULT_ASYNC
						     || procInfo.StopReason.Code == eStopNotificationReason.STOP_REASON_GPU_HP3D_TIMEOUT_ASYNC
						     || procInfo.StopReason.Code == eStopNotificationReason.STOP_REASON_GPU_SUBMITDONE_TIMEOUT_ASYNC
						     || procInfo.StopReason.Code == eStopNotificationReason.STOP_REASON_GPU_BREAK_ASYNC
							 || procInfo.StopReason.Code == eStopNotificationReason.STOP_REASON_GPU_FAULT_PAGE_FAULT_ASYNC
							 || procInfo.StopReason.Code == eStopNotificationReason.STOP_REASON_GPU_FAULT_BAD_OP_CODE_ASYNC
							 || procInfo.StopReason.Code == eStopNotificationReason.STOP_REASON_GPU_FAULT_SUBMITDONE_TIMEOUT_IN_RUN_ASYNC
							 || procInfo.StopReason.Code == eStopNotificationReason.STOP_REASON_GPU_FAULT_SUBMITDONE_TIMEOUT_IN_SUSPEND_ASYNC
							 || procInfo.StopReason.Code == eStopNotificationReason.STOP_REASON_GPU_FAULT_IDLE_TIMEOUT_AFTER_SUBMITDONE_ASYNC)
						{
							// Call TARGET_GPU_CRASH_CALLBACK_NAME to prevent any
							// more commands being pushed to the GPU, then resume
							// the process and wait for the sysTmCmdGpuHang call.
							AsyncSymbolLookup symbols;
							symbols = m_AsyncSymbolLookup[procInfo.Id];
							var targetGpuCrashCallbackAddr = symbols.Lookup(TARGET_GPU_CRASH_CALLBACK_NAME);
							TtyOutput("\n"+procInfo.StopReason.Code+" recieved.\n");
							ExecuteTargetCallback(procInfo, thread, targetGpuCrashCallbackAddr, 1, 0, 0, (bool valid, UInt64 ret) =>
							{
								try
								{
									TtyOutput("Resuming process and waiting for timeout...\n\n");
									proc.Resume(null);
								}
								catch
								{
								}
							});
						}

						// Otherwise, run the standard stop handling.
						else
						{
                            string crashLine = "Generating dump file for target " + m_Target.m_Name + ". Please wait...";
                            string breakLine = new string('*', crashLine.Length);
                            Trace.WriteLine(breakLine);
                            Trace.WriteLine(crashLine);
                            Trace.WriteLine(breakLine);

                            // Determine the addresses of the callback functions (if they exists), and create the bug summary.
							AsyncSymbolLookup symbols;
							symbols = m_AsyncSymbolLookup[procInfo.Id];
							var targetCrashCallbackAddr      = symbols.Lookup(TARGET_CRASH_CALLBACK_NAME);
							var getBugstarConfigCallbackAddr = symbols.Lookup(TARGET_GET_BUGSTAR_RTM_CONFIG_NAME);
							var targetGpuCrashCallbackAddr   = gpuHang ? symbols.Lookup(TARGET_GPU_CRASH_CALLBACK_NAME) : 0;

							string bugDescription;

							var filesToCopy = new List<string>();
							ExecuteTargetCallback(procInfo, thread, targetCrashCallbackAddr, 0, (bool valid0) =>
							{
								if(!valid0)
								{
									TtyOutput("Failure executing "+TARGET_CRASH_CALLBACK_BASE_NAME+"(0)\n\n");
								}

								// Grab the Bugstar configuration early here.  If a target callback
								// deadlocks, we may not be able to ever call any more callbacks after
								// that (thread is left in OS code, and cannot modify registers in that
								// state).  The game code callback that executes in
								// TargetCrashCallback(1) is particuarly susceptable to this problem.
								ExecuteTargetCallback(procInfo, thread, getBugstarConfigCallbackAddr, 0, (bool valid1, UInt64 bugstarConfigAddr) =>
								{
									// Don't warn about a failure in the callback here though, wait till the
									// RaiseBug near the end bug handling so that it is more prominent in the logs.

									TtyOutput("\n**** BEGIN EXCEPTION DUMP\n\n");
									m_Target.BeginCaptureTtyOutput();

									// Display appropriate basic info for the crash type.
									if(gpuHang)
									{
										DisplayGpuCrashInfo(procInfo, threadInfo);
									}
									else
									{
										DisplayCpuCrashInfo(procInfo, threadInfo);
									}

									bugDescription = m_Target.EndCaptureTtyOutput();

									// There have been a few occurrences of kernel bugs which hang the devkit
									// when attempting to do save a coredump for a GPU crash (see
									// https://ps4.scedev.net/forums/thread/55399/).  And so far no cases of
									// Sony's command buffer validation code crashing.  Even if the command
									// buffer validation did crash, R*TM can still recover from that, but it
									// cannot recover from a non-responsive devkit that needs to be powerred
									// off.  So run the validation code before reporting the problem to R*TM.
									//
									// If it was not a GPU crash, then we have set targetGpuCrashCallbackAddr to
									// zero (regardless of symbol found or not), to prevent this from doing
									// anything here.
									//
									var ttyParser = new GpuCrashTtyParser(m_Target);
									m_Target.PushTtyParser((str) => ttyParser.Parse(str));
									if(gpuHang)
									{
										TtyOutput("Executing rage::OrbisGpuCrashCallback()...\n");
									}
									ExecuteTargetCallback(procInfo, thread, targetGpuCrashCallbackAddr, 0, 0, 0, (bool valid2, UInt64 ret) =>
									{
										m_Target.PopTtyParser();
										ttyParser.InputFinished();
										if(gpuHang && !valid2)
										{
											TtyOutput("Failure executing rage::OrbisGpuCrashCallback(0,0,0)\n\n");
										}

										// Save the coredump.
										var elfFilename = m_Target.ElfFilename(procInfo);
										if(elfFilename == null)
										{
											TtyOutput("Unable to access process .elf to add to bug.\n");
										}
										else
										{
											filesToCopy.Add(elfFilename);
										}
										string baseCoredump = null; // needs to be explicitly initialised to compile
										baseCoredump = TriggerCoredump(procInfo, () =>
										{
											filesToCopy.Add(baseCoredump);

											// Since SDK2, coredumps are split into multiple files.  Add any further
											// files to the list to be copied to a bug.
											var coredumpPath = Path.GetDirectoryName(baseCoredump);
											if(coredumpPath != null)
											{
												var coredumpRegex = new Regex(Path.GetFileNameWithoutExtension(baseCoredump)+@"(-[0-9]+)?\.(orbisdmpmanifest|orbismemdmp)$");
												var candidateFiles = new List<string>();
												foreach(var f in Directory.EnumerateFiles(coredumpPath))
												{
													if(coredumpRegex.IsMatch(Path.GetFileName(f)))
													{
														filesToCopy.Add(f);
													}
												}
											}

											// Call the game code callback _after_ a coredump has been saved.  Have
											// less trust in this code messing stuff up, so do it last.
											TtyOutput("Executing "+TARGET_CRASH_CALLBACK_BASE_NAME+"()...\n");
											ExecuteTargetCallback(procInfo, thread, targetCrashCallbackAddr, processResume ? 2UL : 1UL, (bool valid3) =>
											{
												if(!valid3)
												{
                                                    TtyOutput("Failure executing "+TARGET_CRASH_CALLBACK_BASE_NAME+"(" +
                                                        (processResume ? 2UL : 1UL) + ")\n\n");
												}

												try
												{
													SendEventToPlugins(pEvent, thread);
												}
												catch(Exception e)
												{
													TtyOutput("Exception thrown when sending event to plugins, "+e+".\n");
												}

                                                if(processResume)
                                                {
													TtyOutput("Async raising bug.\n");
													// Do this async, so the console can continue running. Otherwise, the systray connection
                                                    // times out, and the game will halt.
                                                    var bugThread = new Thread(() =>
                                                    {
                                                        RaiseBug(procInfo, bugstarConfigAddr, bugSummary, bugDescription, filesToCopy);
                                                    });
                                                    bugThread.Name = "Bugstar async crashdump report";
                                                    bugThread.Start();
                                                }
                                                else
                                                {
													TtyOutput("Raising bug...\n");
													RaiseBug(procInfo, bugstarConfigAddr, bugSummary, bugDescription, filesToCopy);
                                                }
												TtyOutput("\n**** END EXCEPTION DUMP\n");

												if(processResume)
												{
                                                    // Skip over the __debugbreak and resume
                                                    AddRegister(thread, eRegisters.REG_ID_RIP, 2);
                                                    UInt32[] tids = { tid };
                                                    proc.Resume(tids);
                                                }
											});
										});
									});
								});
							});
						}
					}
				}
			}

			////////////////////////////////////////////////////////////////////
			// PS4Target::DebugEventHandler::ExecuteTargetCallback
			////////////////////////////////////////////////////////////////////
			private void ExecuteTargetCallback(IProcessInfo procInfo, IThread executeThread, UInt64 targetCallbackAddr, UInt64 arg0, UInt64 arg1, UInt64 arg2, VoidCallbackBoolUInt64 thenDo)
			{
				if(targetCallbackAddr != 0)
				{
					try
					{
						IProcess proc = procInfo.ProcessInterface;
						TargetCallbackStateSaver stateSaver = new TargetCallbackStateSaver(proc, executeThread);
						AddStopNotificationCallback(procInfo.Id, stateSaver.ResumeThreads, (bool valid) =>
						{
							if(valid)
							{
								UInt64 rax = GetRegister(executeThread, eRegisters.REG_ID_RAX);
								stateSaver.Dispose();
								thenDo(true, rax);
							}
							else
							{
								IProcessInfo outProcInfo;
								Object outThreadInfo;
								Object outFailedThreads;
								try
								{
									proc.Suspend(out outProcInfo, out outThreadInfo, out outFailedThreads);
								}
								catch
								{
								}
								stateSaver.Dispose();
								thenDo(false, 0);
							}
						});

						// TODO: Currently just using the thread's stack, would be best
						// if there was a pre-allocated stack that we switch to instead
						// (so we can handle stack overflow crashes, etc).  But, the
						// catch is that we need to know the address of the stack to
						// switch to.  The .cmp file format only includes functions, not
						// other symbols, so would need some hacky way to put the
						// addresses in the .text segment, or maybe get the runtime code
						// to do the stack switch.
						//
						// System V ABI requires (rsp+8)%16 = 0, the -1 also takes care of
						// the rip that the call we are emulating would push.  But
						// instead of pushing a valid return address, push 0 so the
						// target will crash again on function completion and we can
						// regain control.
						//
						// ABI also specifies a 128 byte red zone on the stack,
						// so that needs to be left untouched too.
						//
						UInt64 rsp = GetRegister(executeThread, eRegisters.REG_ID_RSP);
						rsp = (UInt64)((Int64)rsp&~15)-8-128;
						WriteUInt64(proc, rsp, 0);
						SetRegister(executeThread, eRegisters.REG_ID_RSP, rsp);
						SetRegister(executeThread, eRegisters.REG_ID_RIP, targetCallbackAddr);
						SetRegister(executeThread, eRegisters.REG_ID_RDI, arg0);
						SetRegister(executeThread, eRegisters.REG_ID_RSI, arg1);
						SetRegister(executeThread, eRegisters.REG_ID_RDX, arg2);
						// Clear RFLAGS.DF (direction flag)
						SetRegister(executeThread, eRegisters.REG_ID_RFLAGS,
							(UInt64)((Int64)GetRegister(executeThread, eRegisters.REG_ID_RFLAGS) & ~0x400));

						UInt32[] tids = {executeThread.Id};
						proc.Resume(tids);
						return;
					}
					catch(Exception e)
					{
						TtyOutput("Threw exception trying to run target callback, "+e.ToString()+"\n");
					}
				}

				thenDo(false, 0);
			}

			////////////////////////////////////////////////////////////////////
			// PS4Target::DebugEventHandler::ExecuteTargetCallback
			////////////////////////////////////////////////////////////////////
			private void ExecuteTargetCallback(IProcessInfo procInfo, IThread executeThread, UInt64 targetCallbackAddr, UInt64 arg, VoidCallbackBoolUInt64 thenDo)
			{
				ExecuteTargetCallback(procInfo, executeThread, targetCallbackAddr, arg, 0, 0, thenDo);
			}

			////////////////////////////////////////////////////////////////////
			// PS4Target::DebugEventHandler::ExecuteTargetCallback
			////////////////////////////////////////////////////////////////////
			private void ExecuteTargetCallback(IProcessInfo procInfo, IThread executeThread, UInt64 targetCallbackAddr, UInt64 arg, VoidCallbackBool thenDo)
			{
				ExecuteTargetCallback(procInfo, executeThread, targetCallbackAddr, arg,
					(bool valid, UInt64 ret) => thenDo(valid));
			}

			////////////////////////////////////////////////////////////////////
			// PS4Target::DebugEventHandler::PrintAllThreadStacks
			////////////////////////////////////////////////////////////////////
			private void PrintAllThreadStacks(IStopNotificationEvent pEvent)
			{
				IStopNotification notification = pEvent.StopNotification;
				IProcessInfo procInfo = notification.ProcessInfo;
				foreach(IThreadInfo t in (Array)notification.Threads)
				{
					TtyOutput("\nThread 0x"+t.Id.ToString("x8")+" "+t.Name+"\n");
					PrintThreadStack("", procInfo, t);
				}
				TtyOutput("\n");
			}

			////////////////////////////////////////////////////////////////////
			// PS4Target::DebugEventHandler::DisplayCrashInfoProcess
			////////////////////////////////////////////////////////////////////
			private void DisplayCrashInfoProcess(IProcessInfo procInfo, IThreadInfo threadInfo)
			{
				UInt32 pid = procInfo.Id;
				UInt32 tid = threadInfo.Id;

				TtyOutput("Process Attributes:\n");
				foreach(eProcessAttr attr in Enum.GetValues(typeof(eProcessAttr)))
				{
					if((procInfo.Attributes&attr) != 0)
					{
						TtyOutput("    "+attr.ToString()+"\n");
					}
				}
				TtyOutput("Fingerprint:              "+procInfo.Fingerprint+"\n");
				TtyOutput("Process ID:               0x"+pid.ToString("x8")+"\n");
				TtyOutput("Process Name:             "+procInfo.Name+"\n");
				TtyOutput("Path:                     "+procInfo.Path+"\n");
				TtyOutput("Thread ID:                0x"+tid.ToString("x8")+"\n");
				TtyOutput("Exception Frame Address:  0x"+procInfo.ExceptionHandlingFrame.FrameAddress.ToString("x16")+"\n");
				TtyOutput("Exception Frame Size:     0x"+procInfo.ExceptionHandlingFrame.FrameSize.ToString("x8")+"\n");
				TtyOutput("Exception Header Address: 0x"+procInfo.ExceptionHandlingFrame.HeaderAddress.ToString("x16")+"\n");
				TtyOutput("Exception Header Size:    0x"+procInfo.ExceptionHandlingFrame.HeaderSize.ToString("x8")+"\n\n");
			}

			////////////////////////////////////////////////////////////////////
			// PS4Target::DebugEventHandler::DisplayCpuCrashInfo
			////////////////////////////////////////////////////////////////////
			private void DisplayCpuCrashInfo(IProcessInfo procInfo, IThreadInfo threadInfo)
			{
				// note that StopReason.StopId is not always valid and may throw
				string stopReason = "Stop Reason: "+procInfo.StopReason.Code.ToString();
				try{stopReason += " (0x"+procInfo.StopReason.StopId.ToString("x16")+")";}catch(System.ArgumentException){}
				TtyOutput(stopReason+"\n");

				DisplayCrashInfoProcess(procInfo, threadInfo);

				PrintThreadInfo("", threadInfo);
				TtyOutput("\n");
				PrintThreadRegisters("", threadInfo);
				TtyOutput("\n");
				PrintThreadStack("", procInfo, threadInfo);
				TtyOutput("\n");
			}

			////////////////////////////////////////////////////////////////////
			// PS4Target::DebugEventHandler::DisplayGpuCrashInfo
			////////////////////////////////////////////////////////////////////
			private void DisplayGpuCrashInfo(IProcessInfo procInfo, IThreadInfo threadInfo)
			{
				TtyOutput("GPU Error\n");
				DisplayCrashInfoProcess(procInfo, threadInfo);
			}

			////////////////////////////////////////////////////////////////////
			// PS4Target::DebugEventHandler::GpuCrashTtyParser
			////////////////////////////////////////////////////////////////////
			private class GpuCrashTtyParser
			{
				private PS4Target m_Target;
				private StringBuilder m_StringBuilder = new StringBuilder();

				////////////////////////////////////////////////////////////////
				// PS4Target::DebugEventHandler::GpuCrashTtyParser::GpuCrashTtyParser
				////////////////////////////////////////////////////////////////
				internal GpuCrashTtyParser(PS4Target target)
				{
					m_Target = target;
				}

				////////////////////////////////////////////////////////////////
				// PS4Target::DebugEventHandler::GpuCrashTtyParser::Parse
				////////////////////////////////////////////////////////////////
				internal string Parse(string inTty)
				{
					m_StringBuilder.Append(inTty);
					return null;
				}

				////////////////////////////////////////////////////////////////
				// PS4Target::DebugEventHandler::GpuCrashTtyParser::InputFinished
				////////////////////////////////////////////////////////////////
				internal void InputFinished()
				{
					var inTty = m_StringBuilder.ToString();
					var outTty = new StringBuilder();

					var stripHashRegex = new Regex("^(# )?(.*)");

					foreach(var line in inTty.Split('\n'))
					{
						outTty.Append(stripHashRegex.Replace(line, "$2\n"));
					}

					m_Target.TtyOutput(outTty.ToString());
				}
			}

			////////////////////////////////////////////////////////////////////
			// PS4Target::DebugEventHandler::OnThreadCreate
			////////////////////////////////////////////////////////////////////
			public void OnThreadCreate(IThreadCreateEvent pEvent)
			{
				lock(m_Target)
				{
					m_Target.m_DebugEventHandler.CheckForConnectionThread();
				}
			}

			////////////////////////////////////////////////////////////////////
			// PS4Target::DebugEventHandler::OnThreadExit
			////////////////////////////////////////////////////////////////////
			public void OnThreadExit(IThreadExitEvent pEvent)
			{
			}

			////////////////////////////////////////////////////////////////////
			// PS4Target::DebugEventHandler::TtyOutput
			////////////////////////////////////////////////////////////////////
			private void TtyOutput(string str)
			{
				lock(m_Target)
				{
					m_Target.TtyOutput(str);
				}
			}

			////////////////////////////////////////////////////////////////////
			// PS4Target::DebugEventHandler::DetachProcess
			////////////////////////////////////////////////////////////////////
			private void DetachProcess(IProcess proc)
			{
				m_AttachedProcesses.Remove(proc);
				try
				{
					proc.Detach((uint)eDetachFlags.DETACH_RESUME);
				}
				catch
				{
					// This often appears to fail with
					// TMAPI_COMMS_ERR_INVALID_PID, but the Detach call
					// is actually still important.  Otherwise orbis-run
					// can complain with
					//  [ERROR]: Target error - The process is already attached.
				}

				UInt32 pid = proc.Id;
				m_OnStopNotificationCallback.Remove(pid);
				m_OnCoredumpCompletedCallbacks.Remove(pid);

				AsyncSymbolLookup s;
				if(m_AsyncSymbolLookup.TryGetValue(pid, out s))
				{
					m_AsyncSymbolLookup.Remove(pid);
					s.Dispose();
				}

				string cmp;
				if(m_CmpFileCache.TryGetValue(pid, out cmp))
				{
					try
					{
						File.Delete(cmp);
					}
					catch
					{
					}
					m_CmpFileCache.Remove(pid);
				}
			}

			////////////////////////////////////////////////////////////////////
			// PS4Target::DebugEventHandler::DetachProcess
			////////////////////////////////////////////////////////////////////
			private void DetachProcess(UInt32 pid)
			{
				foreach(IProcess proc in m_AttachedProcesses)
				{
					if(proc.Id == pid)
					{
						DetachProcess(proc);
						break;
					}
				}
			}

			////////////////////////////////////////////////////////////////////
			// PS4Target::DebugEventHandler::DetachAllProcesses
			////////////////////////////////////////////////////////////////////
			internal void DetachAllProcesses()
			{
				while (m_AttachedProcesses.Count > 0)
				{
					DetachProcess(m_AttachedProcesses[0]);
				}
			}

			////////////////////////////////////////////////////////////////////
			// PS4Target::DebugEventHandler::PrintThreadInfo
			////////////////////////////////////////////////////////////////////
			private void PrintThreadInfo(string indent, IThreadInfo t)
			{
				try
				{
					TtyOutput(indent+"Thread 0x"+t.Id.ToString("x8")+" "+t.Name+"\n");
					TtyOutput(indent+"State:           "+t.State.ToString()+"\n");
					// note that StopReason.StopId is not always valid and may throw
					string stopReason = indent+"Stop Reason:     "+t.StopReason.Code.ToString();
					try{stopReason += " (0x"+t.StopReason.StopId.ToString("x16")+")";}catch(System.ArgumentException){}
					TtyOutput(stopReason+"\n");
					TtyOutput(indent+"Exit Code:       0x"+t.ExitCode.ToString("x16")+"\n");
					TtyOutput(indent+"Type:            "+t.Type.ToString()+"\n");
					TtyOutput(indent+"Wait State:      "+t.WaitState.ToString()+"\n");
					TtyOutput(indent+"Priority Class:  0x"+t.PriorityClass.ToString("x8")+"\n");
					TtyOutput(indent+"Priority Level:  0x"+t.PriorityLevel.ToString("x8")+"\n");
					TtyOutput(indent+"Priority Native: 0x"+t.PriorityNative.ToString("x8")+"\n");
					TtyOutput(indent+"Priority User:   0x"+t.PriorityUser.ToString("x8")+"\n");
					TtyOutput(indent+"Stack Address:   0x"+t.StackAddressTop.ToString("x16")+"\n");
					TtyOutput(indent+"Stack Size:      0x"+t.StackSize.ToString("x16")+"\n");
				}
				catch
				{
				}
			}

			////////////////////////////////////////////////////////////////////
			// PS4Target::DebugEventHandler::Register64
			////////////////////////////////////////////////////////////////////
			private string Register64(IThread t, eRegisters r)
			{
				try
				{
					eRegisters[] regs = {r};
					UInt64[] val = (UInt64[])t.GetRegisters(regs);
					return val[0].ToString("x16");
				}
				catch
				{
					return "????????????????";
				}
			}

			////////////////////////////////////////////////////////////////////
			// PS4Target::DebugEventHandler::StrFloat32
			////////////////////////////////////////////////////////////////////
			private string StrFloat32(UInt32 u32)
			{
				UInt32 sgn = (u32&0x80000000)>>31;
				UInt32 exp = (u32&0x7f800000)>>23;
				UInt32 man = (u32&0x007fffff)>>0;

				// Infinite or NaN ?
				if(exp == 0xff)
				{
					// Infinite ?
					if(man == 0)
					{
						return ((sgn!=0)?"-":"+") + "Inf";
					}
					// NaN ?
					else
					{
						return "NaN";
					}
				}

				// Zero or denormal ?
				else if(exp == 0)
				{
					// Zero ?
					if(man == 0)
					{
						return ((sgn!=0)?"-":"") + "0";
					}
					// Denormal ?
					else
					{
						// Multiply by 1.0/(1<<(126+23)), but obviously cannot have an intermediate integer that large, so
						// 126+23 = 149 = 30+30+30+30+29
						float smallestDenorm = ((((1.0f/(1<<30))/(1<<30))/(1<<30))/(1<<30))/(1<<29);
						float val = ((sgn!=0)?-1.0f:+1.0f)*smallestDenorm*man;
						return val.ToString();
					}
				}

				// Normal number ?
				else
				{
					// Generate multiplier to apply to mantissa
					float expMul = 1.0f;
					int sexp = (int)exp - 0x7f - 23;   // adjust for bias and binary place
					if(sexp < 0)
					{
						int shift = - sexp;
						while(shift >= 30)
						{
							expMul /= 1<<30;
							shift -= 30;
						}
						expMul /= 1<<shift;
					}
					else
					{
						int shift = sexp;
						while(shift >= 30)
						{
							expMul *= 1<<30;
							shift -= 30;
						}
						expMul *= 1<<shift;
					}
					man += 0x00800000; // add J-bit to mantissa
					float val = ((sgn!=0)?-1.0f:+1.0f)*expMul*man;
					return val.ToString();
				}
			}

			////////////////////////////////////////////////////////////////////
			// PS4Target::DebugEventHandler::RegisterFloat80
			////////////////////////////////////////////////////////////////////
			private string RegisterFloat80(IThread t, eRegisters r)
			{
				try
				{
					eRegisters[] regs = {r};
					UInt64[] val = (UInt64[])t.GetRegisters(regs);
					string ret = (val[1]&0xff).ToString("x2")+val[0].ToString("x16");
					// TODO: Should also convert to 80-bit float
					return ret;
				}
				catch
				{
					return "????????????????????";
				}
			}

			////////////////////////////////////////////////////////////////////
			// PS4Target::DebugEventHandler::RegisterAvx
			////////////////////////////////////////////////////////////////////
			private string RegisterAvx(IThread t, eRegisters r)
			{
				try
				{
					eRegisters[] regs = {r};
					UInt64[] val = (UInt64[])t.GetRegisters(regs);

					UInt64 v = val[0];
					string ret = (v&0xffffffff).ToString("x8")+" "+((v>>32)&0xffffffff).ToString("x8");
					for(uint i=1; i<4; ++i)
					{
						v = val[i];
						ret += " "+(v&0xffffffff).ToString("x8")+" "+((v>>32)&0xffffffff).ToString("x8");
					}

					v = val[0];
					ret += " {"+StrFloat32((UInt32)(v&0xffffffff))+", "+StrFloat32((UInt32)((v>>32)&0xffffffff));
					for(uint i=1; i<4; ++i)
					{
						v = val[i];
						ret += ", "+StrFloat32((UInt32)(v&0xffffffff))+", "+StrFloat32((UInt32)((v>>32)&0xffffffff));
					}
					ret += "}";

					return ret;
				}
				catch
				{
					return "???????? ???????? ???????? ???????? ???????? ???????? ???????? ????????";
				}
			}

			////////////////////////////////////////////////////////////////////
			// PS4Target::DebugEventHandler::PrintThreadRegisters
			////////////////////////////////////////////////////////////////////
			private void PrintThreadRegisters(string indent, IThreadInfo tInfo)
			{
				try
				{
					IThread t = tInfo.ThreadInterface;

					TtyOutput(indent+"RAX "+Register64(t,eRegisters.REG_ID_RAX)+"    R8   "+Register64(t,eRegisters.REG_ID_R8 )+"\n");
					TtyOutput(indent+"RCX "+Register64(t,eRegisters.REG_ID_RCX)+"    R9   "+Register64(t,eRegisters.REG_ID_R9 )+"\n");
					TtyOutput(indent+"RDX "+Register64(t,eRegisters.REG_ID_RDX)+"    R10  "+Register64(t,eRegisters.REG_ID_R10)+"\n");
					TtyOutput(indent+"RBX "+Register64(t,eRegisters.REG_ID_RBX)+"    R11  "+Register64(t,eRegisters.REG_ID_R11)+"\n");
					TtyOutput(indent+"RSP "+Register64(t,eRegisters.REG_ID_RSP)+"    R12  "+Register64(t,eRegisters.REG_ID_R12)+"\n");
					TtyOutput(indent+"RBP "+Register64(t,eRegisters.REG_ID_RBP)+"    R13  "+Register64(t,eRegisters.REG_ID_R13)+"\n");
					TtyOutput(indent+"RSI "+Register64(t,eRegisters.REG_ID_RSI)+"    R14  "+Register64(t,eRegisters.REG_ID_R14)+"\n");
					TtyOutput(indent+"RDI "+Register64(t,eRegisters.REG_ID_RDI)+"    R15  "+Register64(t,eRegisters.REG_ID_R15)+"\n\n");

					TtyOutput(indent+"FSBASE "+Register64(t,eRegisters.REG_ID_FSBASE)+"\n");
					TtyOutput(indent+"RIP    "+Register64(t,eRegisters.REG_ID_RIP   )+"\n");
					TtyOutput(indent+"RFLAGS "+Register64(t,eRegisters.REG_ID_RFLAGS)+"\n");
					TtyOutput(indent+"TRAPNO "+Register64(t,eRegisters.REG_ID_TRAPNO)+"\n\n");

					TtyOutput(indent+"DR0 "+Register64(t,eRegisters.REG_ID_DR0)+"    DR8  "+Register64(t,eRegisters.REG_ID_DR8 )+"\n");
					TtyOutput(indent+"DR1 "+Register64(t,eRegisters.REG_ID_DR1)+"    DR9  "+Register64(t,eRegisters.REG_ID_DR9 )+"\n");
					TtyOutput(indent+"DR2 "+Register64(t,eRegisters.REG_ID_DR2)+"    DR10 "+Register64(t,eRegisters.REG_ID_DR10)+"\n");
					TtyOutput(indent+"DR3 "+Register64(t,eRegisters.REG_ID_DR3)+"    DR11 "+Register64(t,eRegisters.REG_ID_DR11)+"\n");
					TtyOutput(indent+"DR4 "+Register64(t,eRegisters.REG_ID_DR4)+"    DR12 "+Register64(t,eRegisters.REG_ID_DR12)+"\n");
					TtyOutput(indent+"DR5 "+Register64(t,eRegisters.REG_ID_DR5)+"    DR13 "+Register64(t,eRegisters.REG_ID_DR13)+"\n");
					TtyOutput(indent+"DR6 "+Register64(t,eRegisters.REG_ID_DR6)+"    DR14 "+Register64(t,eRegisters.REG_ID_DR14)+"\n");
					TtyOutput(indent+"DR7 "+Register64(t,eRegisters.REG_ID_DR7)+"    DR15 "+Register64(t,eRegisters.REG_ID_DR15)+"\n\n");

					TtyOutput(indent+"FCW        "+Register64(t,eRegisters.REG_ID_EN_CW)+"\n");
					TtyOutput(indent+"FSW        "+Register64(t,eRegisters.REG_ID_EN_SW)+"\n");
					TtyOutput(indent+"FTW        "+Register64(t,eRegisters.REG_ID_EN_TW)+"\n");
					TtyOutput(indent+"FOP        "+Register64(t,eRegisters.REG_ID_EN_OPCODE)+"\n");
					TtyOutput(indent+"x87 RIP    "+Register64(t,eRegisters.REG_ID_EN_RIP)+"\n");
					TtyOutput(indent+"RDP        "+Register64(t,eRegisters.REG_ID_EN_RDP)+"\n");
					TtyOutput(indent+"MXCSR      "+Register64(t,eRegisters.REG_ID_EN_MXCSR)+"\n");
					TtyOutput(indent+"MXCSR_MASK "+Register64(t,eRegisters.REG_ID_EN_MXCSR_MASK)+"\n");
					TtyOutput(indent+"EN_ZERO    "+Register64(t,eRegisters.REG_ID_EN_ZERO)+"\n");
					TtyOutput(indent+"ERR        "+Register64(t,eRegisters.REG_ID_ERR)+"\n\n");

					TtyOutput(indent+"FPR0 "+RegisterFloat80(t,eRegisters.REG_ID_FP_ACC0)+"\n");
					TtyOutput(indent+"FPR1 "+RegisterFloat80(t,eRegisters.REG_ID_FP_ACC1)+"\n");
					TtyOutput(indent+"FPR2 "+RegisterFloat80(t,eRegisters.REG_ID_FP_ACC2)+"\n");
					TtyOutput(indent+"FPR3 "+RegisterFloat80(t,eRegisters.REG_ID_FP_ACC3)+"\n");
					TtyOutput(indent+"FPR4 "+RegisterFloat80(t,eRegisters.REG_ID_FP_ACC4)+"\n");
					TtyOutput(indent+"FPR5 "+RegisterFloat80(t,eRegisters.REG_ID_FP_ACC5)+"\n");
					TtyOutput(indent+"FPR6 "+RegisterFloat80(t,eRegisters.REG_ID_FP_ACC6)+"\n");
					TtyOutput(indent+"FPR7 "+RegisterFloat80(t,eRegisters.REG_ID_FP_ACC7)+"\n\n");

					TtyOutput(indent+"YMM0  "+RegisterAvx(t,eRegisters.REG_ID_YMM0 )+"\n");
					TtyOutput(indent+"YMM1  "+RegisterAvx(t,eRegisters.REG_ID_YMM1 )+"\n");
					TtyOutput(indent+"YMM2  "+RegisterAvx(t,eRegisters.REG_ID_YMM2 )+"\n");
					TtyOutput(indent+"YMM3  "+RegisterAvx(t,eRegisters.REG_ID_YMM3 )+"\n");
					TtyOutput(indent+"YMM4  "+RegisterAvx(t,eRegisters.REG_ID_YMM4 )+"\n");
					TtyOutput(indent+"YMM5  "+RegisterAvx(t,eRegisters.REG_ID_YMM5 )+"\n");
					TtyOutput(indent+"YMM6  "+RegisterAvx(t,eRegisters.REG_ID_YMM6 )+"\n");
					TtyOutput(indent+"YMM7  "+RegisterAvx(t,eRegisters.REG_ID_YMM7 )+"\n");
					TtyOutput(indent+"YMM8  "+RegisterAvx(t,eRegisters.REG_ID_YMM8 )+"\n");
					TtyOutput(indent+"YMM9  "+RegisterAvx(t,eRegisters.REG_ID_YMM9 )+"\n");
					TtyOutput(indent+"YMM10 "+RegisterAvx(t,eRegisters.REG_ID_YMM10)+"\n");
					TtyOutput(indent+"YMM11 "+RegisterAvx(t,eRegisters.REG_ID_YMM11)+"\n");
					TtyOutput(indent+"YMM12 "+RegisterAvx(t,eRegisters.REG_ID_YMM12)+"\n");
					TtyOutput(indent+"YMM13 "+RegisterAvx(t,eRegisters.REG_ID_YMM13)+"\n");
					TtyOutput(indent+"YMM14 "+RegisterAvx(t,eRegisters.REG_ID_YMM14)+"\n");
					TtyOutput(indent+"YMM15 "+RegisterAvx(t,eRegisters.REG_ID_YMM15)+"\n");
				}
				catch
				{
				}
			}

			////////////////////////////////////////////////////////////////////
			// PS4Target::DebugEventHandler::CacheCmpFile
			////////////////////////////////////////////////////////////////////
			private void CacheCmpFile(IProcessInfo pInfo)
			{
				var elf = m_Target.ElfFilename(pInfo);
				if(elf != null)
				{
					var src = Regex.Replace(elf, @"(.*)\.[^.]+$", "$1.cmp");
					var dst = Path.Combine(Path.GetTempPath(), "rtm-"+m_Target.m_Name+"-"+pInfo.Id+".cmp");
					try
					{
						File.Copy(src, dst, true);
						m_CmpFileCache.Add(pInfo.Id, dst);
					}
					catch
					{
					}
				}
			}

			////////////////////////////////////////////////////////////////////
			// PS4Target::DebugEventHandler::CmpFilename
			////////////////////////////////////////////////////////////////////
			private string CmpFilename(UInt32 pid)
			{
				string cmp;
				m_CmpFileCache.TryGetValue(pid, out cmp);
				return cmp;
			}

			////////////////////////////////////////////////////////////////////
			// PS4Target::DebugEventHandler::LookupSymbol
			////////////////////////////////////////////////////////////////////
			private UInt64 LookupSymbol(SymbolLookup sym, string symbol)
			{
				TtyOutput("Searching symbols for "+symbol+" ...\n");
				UInt64 targetCallbackAddr = sym.Lookup(symbol);
				if(targetCallbackAddr == 0)
				{
					TtyOutput("... not found\n");
				}
				else
				{
					TtyOutput("... found at 0x"+targetCallbackAddr.ToString("x16")+"\n");
				}
				return targetCallbackAddr;
			}

			////////////////////////////////////////////////////////////////////
			// PS4Target::DebugEventHandler::RaiseBug
			////////////////////////////////////////////////////////////////////
			private void RaiseBug(IProcessInfo pInfo, UInt64 configAddr, string summary, string description, List<string> filesToCopy)
			{
				string configXml;
				if(GetMemory(pInfo.ProcessInterface, configAddr, out configXml))
				{
					var config = Bugstar.Config.FromString(configXml);
					m_Target.BaseRaiseBug(config, summary, description, filesToCopy.ToArray(), m_EnableBugDupCheck);
				}
				else
				{
					TtyOutput("Unable to read Bugstar configuration from the game to automatically log a bug (address=0x"+configAddr.ToString("x16")+").  Please create bug manually\n");
				}
				m_EnableBugDupCheck = true;
			}

			////////////////////////////////////////////////////////////////////
			// PS4Target::DebugEventHandler::PrintThreadStack
			////////////////////////////////////////////////////////////////////
			private void PrintThreadStack(string indent, IProcess proc, IThread t)
			{
				try
				{
					using(SymbolLookup sym = new SymbolLookup(CmpFilename(proc.Id), TargetPlatformId.PS4))
					{
						sym.SegmentBaseAddress = m_SegmentBaseAddress;
						eRegisters[] rip = {eRegisters.REG_ID_RIP};
						UInt64 addr = ((UInt64[])t.GetRegisters(rip))[0];

						TtyOutput(indent+"0x"+addr.ToString("x16")+" - "+sym.Lookup(addr)+"\n");

						eRegisters[] rbp = {eRegisters.REG_ID_RBP};
						UInt64 frame = ((UInt64[])t.GetRegisters(rbp))[0];
						while(frame != 0)
						{
							if(!GetMemory(proc, frame+8, out addr) || addr==0)
							{
								break;
							}
							TtyOutput(indent+"0x"+addr.ToString("x16")+" - "+sym.Lookup(addr)+"\n");
							if(!GetMemory(proc, frame, out frame))
							{
								break;
							}
						}
					}
				}
				catch
				{
				}
			}

			////////////////////////////////////////////////////////////////////
			// PS4Target::DebugEventHandler::PrintThreadStack
			////////////////////////////////////////////////////////////////////
			private void PrintThreadStack(string indent, IProcessInfo pInfo, IThreadInfo tInfo)
			{
				PrintThreadStack(indent, pInfo.ProcessInterface, tInfo.ThreadInterface);
			}

			////////////////////////////////////////////////////////////////////
			// PS4Target::DebugEventHandler::TriggerCoredump
			////////////////////////////////////////////////////////////////////
			private string TriggerCoredump(IProcessInfo pInfo, VoidCallbackVoid thenDo)
			{
				UInt32 pid = pInfo.Id;
				m_OnCoredumpCompletedCallbacks.Add(pid, thenDo);

				string name = Regex.Replace(pInfo.Name, @"^(.+)\.elf$", "$1");
				string path = RockstarTargetManager.GetCrashdumpDir()+name+DateTime.Now.ToString("-yyyyMMdd-HHmmss-")+pInfo.Id.ToString()+".orbisdmp";

				// Suspend the process
				if ((pInfo.Attributes & eProcessAttr.PROCESS_ATTR_SUSPENDED) == 0)
				{
					IProcessInfo outProcInfo;
					Object outThreadInfo;
					Object outFailedThreads;
					try
					{
						pInfo.ProcessInterface.Suspend(out outProcInfo, out outThreadInfo, out outFailedThreads);
					}
					catch (COMException e)
					{
						// TMAPI_COMMS_ERR_PROCESS_IS_SUSPENDED_ALREADY occurs when right clicking the target and selecting "Coredump"
						// because the process is suspended by the calling function.
						if (e.ErrorCode != (int)eErrorCode.TMAPI_COMMS_ERR_PROCESS_IS_SUSPENDED_ALREADY)
						{
							TtyOutput("Warning: Failed to suspend process! Error: 0x" + e.ErrorCode.ToString("x8") + "\n");
						}
					}
				}

				TtyOutput("Generating coredump \"" + path + "\"\n\n");
				pInfo.ProcessInterface.TriggerCoredump(path, eCoreDumpType.COREDUMP_FULL);
				return path;
			}

			////////////////////////////////////////////////////////////////////
			// PS4Target::DebugEventHandler::TriggerCoredump
			////////////////////////////////////////////////////////////////////
			internal void TriggerCoredump()
			{
				if(m_AttachedProcesses.Count <= 0)
				{
					return;
				}

				var mainProcId = m_AttachedProcesses[0].Id;
				IProcessInfo mainProcInfo = null;

				var resumeProcesses = new List<IProcess>();
				var procInfos = (Array)m_Target.m_TmapiTarget.ProcessInfoSnapshot;
				foreach(IProcessInfo pi in procInfos)
				{
					if((pi.Attributes & eProcessAttr.PROCESS_ATTR_SUSPENDED) == 0)
					{
						IProcessInfo outProcInfo;
						Object outThreadInfo;
						Object outFailedThreads;
						var p = pi.ProcessInterface;
						p.Suspend(out outProcInfo, out outThreadInfo, out outFailedThreads);
						resumeProcesses.Add(p);
					}

					if(pi.Id == mainProcId)
					{
						mainProcInfo = pi;
					}
				}
				VoidCallbackVoid resumeProcessesDelegate = () =>
				{
					foreach(var p in resumeProcesses)
					{
						p.Resume(null);
					}
				};

				if(mainProcInfo != null)
				{
					TriggerCoredump(mainProcInfo, resumeProcessesDelegate);
				}
				else
				{
					resumeProcessesDelegate();
				}
			}

			////////////////////////////////////////////////////////////////////
			// PS4Target::DebugEventHandler::SendEventToPlugins
			////////////////////////////////////////////////////////////////////
			private void SendEventToPlugins(IStopNotificationEvent pEvent, IThread thread)
			{
				IStopNotification notification = pEvent.StopNotification;
				IProcessInfo procInfo = notification.ProcessInfo;
				UInt32 tid = notification.ThreadId;

				TargetEventArgs e = new TargetEventArgs();
				e.m_DataAddress = 0;
				switch(procInfo.StopReason.Code)
				{
					case eStopNotificationReason.STOP_REASON_LIBC_ASSERT:
					{
						e.m_EventType = TargetEventType.AssertionFailed;
						break;
					}

					case eStopNotificationReason.STOP_REASON_SIGILL:
					case eStopNotificationReason.STOP_REASON_SIGTRAP:
					case eStopNotificationReason.STOP_REASON_SIGFPE:
					case eStopNotificationReason.STOP_REASON_SIGBUS:    // this should really be a memory exception, but __debugbreak/"int 41h" raises this
					case eStopNotificationReason.STOP_REASON_SIGSWBRKPT:
					case eStopNotificationReason.STOP_REASON_LIBC_PURE_VIRTUAL:
					case eStopNotificationReason.STOP_REASON_LIBC_ABORT:
					{
						e.m_EventType = TargetEventType.ExecutionBreak;
						break;
					}

					case eStopNotificationReason.STOP_REASON_SIGHWBRKPT:
					{
						e.m_EventType = TargetEventType.DataBreak;
						// DR6 is the debug status register.
						UInt64 dr6 = GetRegister(thread, eRegisters.REG_ID_DR6);
						// Any of the breakpoint conditions [0..3] detected?
						if((dr6 & 15) != 0)
						{
							// Expect only one condition to be matched, but in
							// case several are, arbitrarily select the least
							// significant bit.
							int cond = (int)((Int64)dr6 & -(Int64)dr6);
							e.m_DataAddress = GetRegister(thread, eRegisters.REG_ID_DR0+cond);
						}
						break;
					}

					case eStopNotificationReason.STOP_REASON_SIGSEGV:
					{
						// Page fault error code pushed onto the exception
						// handler stack contains a bit to determine if the
						// memory access was a read or a write.  But there
						// doesn't currently seem to be any way to access this
						// error code via TMAPI.
						e.m_EventType = TargetEventType.MemoryReadException;
						// The linear address that caused the page fault is
						// stored in CR2, but TMAPI doesn't allow access to
						// that.
// 						e.m_DataAddress = GetRegister(thread, eRegisters.REG_ID_CR2);
						break;
					}

					default:
					{
						e.m_EventType = TargetEventType.ExecutionBreak;
						break;
					}
				}
				e.m_Message = "TargetEventType."+procInfo.StopReason.Code.ToString();
				e.m_ThreadId = tid;
				e.m_Address = GetRegister(thread, eRegisters.REG_ID_RIP);
				e.m_ExceptionCode = 0;
				e.m_IsThreadStopped = true;
				m_Target.TargetEvent.Invoke(e);
			}

			////////////////////////////////////////////////////////////////////
			// PS4Target::DebugEventHandler::TargetCallbackStateSaver
			////////////////////////////////////////////////////////////////////
			// Saves and restores registers, and also suspends all but the
			// specified thread.
			private class TargetCallbackStateSaver : IDisposable
			{
				// Cannot simply use Enum.GetValues(typeof(eRegisters)), since
				// some registers are read only.  Order is unimportant.
				private static eRegisters[] m_RegIds =
				{
					eRegisters.REG_ID_RAX,          eRegisters.REG_ID_YMM0,         eRegisters.REG_ID_FP_ACC0,
					eRegisters.REG_ID_RCX,          eRegisters.REG_ID_YMM1,         eRegisters.REG_ID_FP_ACC1,
					eRegisters.REG_ID_RDX,          eRegisters.REG_ID_YMM2,         eRegisters.REG_ID_FP_ACC2,
					eRegisters.REG_ID_RBX,          eRegisters.REG_ID_YMM3,         eRegisters.REG_ID_FP_ACC3,
					eRegisters.REG_ID_RSP,          eRegisters.REG_ID_YMM4,         eRegisters.REG_ID_FP_ACC4,
					eRegisters.REG_ID_RBP,          eRegisters.REG_ID_YMM5,         eRegisters.REG_ID_FP_ACC5,
					eRegisters.REG_ID_RSI,          eRegisters.REG_ID_YMM6,         eRegisters.REG_ID_FP_ACC6,
					eRegisters.REG_ID_RDI,          eRegisters.REG_ID_YMM7,         eRegisters.REG_ID_FP_ACC7,
					eRegisters.REG_ID_R8,           eRegisters.REG_ID_YMM8,
					eRegisters.REG_ID_R9,           eRegisters.REG_ID_YMM9,         eRegisters.REG_ID_RFLAGS,
					eRegisters.REG_ID_R10,          eRegisters.REG_ID_YMM10,        eRegisters.REG_ID_EN_CW,
					eRegisters.REG_ID_R11,          eRegisters.REG_ID_YMM11,        eRegisters.REG_ID_EN_MXCSR,
					eRegisters.REG_ID_R12,          eRegisters.REG_ID_YMM12,
					eRegisters.REG_ID_R13,          eRegisters.REG_ID_YMM13,
					eRegisters.REG_ID_R14,          eRegisters.REG_ID_YMM14,
					eRegisters.REG_ID_R15,          eRegisters.REG_ID_YMM15,
					eRegisters.REG_ID_RIP,
				};
				private List<IThreadInfo> m_SuspendedThreads = new List<IThreadInfo>();
				private Array m_SavedRegs = null;
				private IThread m_ExecuteThread;

				////////////////////////////////////////////////////////////////
				// PS4Target::DebugEventHandler::TargetCallbackStateSaver::TargetCallbackStateSaver
				////////////////////////////////////////////////////////////////
				public TargetCallbackStateSaver(IProcess proc, IThread executeThread)
				{
					m_ExecuteThread = executeThread;

					// Suspend all runnable threads
					foreach(IThreadInfo t in proc.ThreadInfoSnapshot)
					{
						switch(t.State)
						{
							case eThreadState.TDS_INACTIVE:
							case eThreadState.TDS_INHIBITED:
							{
								// Do nothing, thread does not need to be suspended
								break;
							}

							case eThreadState.TDS_CAN_RUN:
							case eThreadState.TDS_RUNQ:
							case eThreadState.TDS_RUNNING:
							{
								eThreadState ignored;
								t.ThreadInterface.Suspend(out ignored);
								m_SuspendedThreads.Add(t);
								break;
							}
						}
					}

					// Save all current register values for the thread
					m_SavedRegs = executeThread.GetRegisters(m_RegIds);
				}

				////////////////////////////////////////////////////////////////
				// PS4Target::DebugEventHandler::TargetCallbackStateSaver::~TargetCallbackStateSaver
				////////////////////////////////////////////////////////////////
				~TargetCallbackStateSaver()
				{
					Dispose(false);
				}

				////////////////////////////////////////////////////////////////
				// PS4Target::DebugEventHandler::TargetCallbackStateSaver::Dispose
				////////////////////////////////////////////////////////////////
				public void Dispose()
				{
					Dispose(true);
					GC.SuppressFinalize(this);
				}

				////////////////////////////////////////////////////////////////
				// PS4Target::DebugEventHandler::TargetCallbackStateSaver::Dispose
				////////////////////////////////////////////////////////////////
				protected virtual void Dispose(bool disposing)
				{
					if(disposing)
					{
						// Restore registers
						if(m_SavedRegs != null)
						{
							m_ExecuteThread.SetRegisters(m_RegIds, m_SavedRegs);
						}
						ResumeThreads();
					}
				}

				////////////////////////////////////////////////////////////////
				// PS4Target::DebugEventHandler::TargetCallbackStateSaver::ResumeThreads
				////////////////////////////////////////////////////////////////
				internal void ResumeThreads()
				{
					// Resume threads
					foreach(IThreadInfo t in m_SuspendedThreads)
					{
						t.ThreadInterface.Resume();
					}
					m_SuspendedThreads.Clear();
				}
			}
		}

		////////////////////////////////////////////////////////////////////////
		// PS4Target::TargetEventHandler
		////////////////////////////////////////////////////////////////////////
		private class TargetEventHandler : IEventTarget, IDisposable
		{
			private PS4Target m_Target;

			////////////////////////////////////////////////////////////////////
			// PS4Target::TargetEventHandler::TargetEventHandler
			////////////////////////////////////////////////////////////////////
			public TargetEventHandler(PS4Target target)
			{
				m_Target = target;
				target.m_TmapiTarget.AdviseTargetEvents(this);
			}

			////////////////////////////////////////////////////////////////////
			// PS4Target::TargetEventHandler::~TargetEventHandler
			////////////////////////////////////////////////////////////////////
			~TargetEventHandler()
			{
				Dispose(false);
			}

			////////////////////////////////////////////////////////////////////
			// PS4Target::TargetEventHandler::Dispose
			////////////////////////////////////////////////////////////////////
			public void Dispose()
			{
				Dispose(true);
				GC.SuppressFinalize(this);
			}

			////////////////////////////////////////////////////////////////////
			// PS4Target::TargetEventHandler::Dispose
			////////////////////////////////////////////////////////////////////
			protected virtual void Dispose(bool disposing)
			{
				if(disposing)
				{
					if(m_Target != null)    m_Target.m_TmapiTarget.UnadviseTargetEvents(this);
				}
			}

			////////////////////////////////////////////////////////////////////
			// PS4Target::TargetEventHandler::OnBusy
			////////////////////////////////////////////////////////////////////
			public void OnBusy(IBusyEvent pEvent)
			{
			}

			////////////////////////////////////////////////////////////////////
			// PS4Target::TargetEventHandler::OnConnect
			////////////////////////////////////////////////////////////////////
			public void OnConnect(IConnectEvent pEvent)
			{
				// OnConnect seems to be called when the current user connects to the target.
				lock(m_Target)
				{
					m_Target.Connected();
				}
			}

			////////////////////////////////////////////////////////////////////
			// PS4Target::TargetEventHandler::OnConnected
			////////////////////////////////////////////////////////////////////
			public void OnConnected(IConnectedEvent pEvent)
			{
				// OnConnected seems to be called when any user connects to the
				// target.  If the user is the current user, then OnConnect is
				// also called, so it is redundant to check the connection
				// string here.
			}

			////////////////////////////////////////////////////////////////////
			// PS4Target::TargetEventHandler::OnDisconnect
			////////////////////////////////////////////////////////////////////
			public void OnDisconnect(IDisconnectEvent pEvent)
			{
				lock(m_Target)
				{
					m_Target.Disconnected();
				}
			}

			////////////////////////////////////////////////////////////////////
			// PS4Target::TargetEventHandler::OnExpiryTime
			////////////////////////////////////////////////////////////////////
			public void OnExpiryTime(IExpiryTimeEvent pEvent)
			{
			}

			////////////////////////////////////////////////////////////////////
			// PS4Target::TargetEventHandler::OnFileServingCaseSensitivityChanged
			////////////////////////////////////////////////////////////////////
			public void OnFileServingCaseSensitivityChanged(IFileServingCaseSensitivityChangedEvent pEvent)
			{
			}

			////////////////////////////////////////////////////////////////////
			// PS4Target::TargetEventHandler::OnFileServingRootChanged
			////////////////////////////////////////////////////////////////////
			public void OnFileServingRootChanged(IFileServingRootChangedEvent pEvent)
			{
			}

			////////////////////////////////////////////////////////////////////
			// PS4Target::TargetEventHandler::OnForceDisconnected
			////////////////////////////////////////////////////////////////////
			public void OnForceDisconnected(IForceDisconnectedEvent pEvent)
			{
			}

			////////////////////////////////////////////////////////////////////
			// PS4Target::TargetEventHandler::OnForcedPowerOff
			////////////////////////////////////////////////////////////////////
			public void OnForcedPowerOff(IForcedPowerOffEvent pEvent)
			{
			}

			////////////////////////////////////////////////////////////////////
			// PS4Target::TargetEventHandler::OnIdle
			////////////////////////////////////////////////////////////////////
			public void OnIdle(IIdleEvent pEvent)
			{
			}

			////////////////////////////////////////////////////////////////////
			// PS4Target::TargetEventHandler::OnMultiPhaseProgress
			////////////////////////////////////////////////////////////////////
			public void OnMultiPhaseProgress(IMultiPhaseProgressEvent pEvent)
			{
			}

			////////////////////////////////////////////////////////////////////
			// PS4Target::TargetEventHandler::OnMultiPhaseProgressError
			////////////////////////////////////////////////////////////////////
			public void OnMultiPhaseProgressError(IMultiPhaseProgressErrorEvent pEvent)
			{
			}

			////////////////////////////////////////////////////////////////////
			// PS4Target::TargetEventHandler::OnNameUpdate
			////////////////////////////////////////////////////////////////////
			public void OnNameUpdate(INameUpdateEvent pEvent)
			{
			}

			////////////////////////////////////////////////////////////////////
			// PS4Target::TargetEventHandler::OnPowerState
			////////////////////////////////////////////////////////////////////
			public void OnPowerState(IPowerStateEvent pEvent)
			{
				lock(m_Target)
				{
					switch(pEvent.Operation)
					{
						case ePowerOperation.POWEROP_BOOT:
							// While it would seem logical to start a new logfile
							// and call DetachAllProcesses() here, we seem to get
							// suprious POWEROP_BOOT events when the R*TM connects
							// to an already running game.  So don't.
							break;

						case ePowerOperation.POWEROP_SHUTDOWN:
							m_Target.m_DebugEventHandler.DetachAllProcesses();
							break;

						case ePowerOperation.POWEROP_RESUME:
							break;

						case ePowerOperation.POWEROP_SUSPEND:
							break;

						case ePowerOperation.POWEROP_NO_POWER:
							m_Target.m_DebugEventHandler.DetachAllProcesses();
							break;
					}
				}
			}

			////////////////////////////////////////////////////////////////////
			// PS4Target::TargetEventHandler::OnProgress
			////////////////////////////////////////////////////////////////////
			public void OnProgress(IProgressEvent pEvent)
			{
			}

			////////////////////////////////////////////////////////////////////
			// PS4Target::TargetEventHandler::OnSettingsChanged
			////////////////////////////////////////////////////////////////////
			public void OnSettingsChanged(ISettingsChangedEvent pEvent)
			{
			}

			////////////////////////////////////////////////////////////////////
			// PS4Target::TargetEventHandler::OnUpdateError
			////////////////////////////////////////////////////////////////////
			public void OnUpdateError(IUpdateErrorEvent pEvent)
			{
			}

			////////////////////////////////////////////////////////////////////
			// PS4Target::TargetEventHandler::OnUpdateProgress
			////////////////////////////////////////////////////////////////////
			public void OnUpdateProgress(IUpdateProgressEvent pEvent)
			{
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////
	// PS4TargetManager
	////////////////////////////////////////////////////////////////////////////
	public class PS4TargetManager : IDisposable
	{
		private Object m_LockObj = new Object();
		private IORTMAPI m_Tmapi;
        private List<PS4Target> m_Targets = new List<PS4Target>();
        private Dictionary<string, PS4Target> m_TargetsByName = new Dictionary<string, PS4Target>();
        private NetworkTtyListener m_NetworkTtyListener;
		private bool m_WorkerThreadExit = false;
		private Thread m_WorkerThread;

		////////////////////////////////////////////////////////////////////////
		// PS4TargetManager::PS4TargetManager
		////////////////////////////////////////////////////////////////////////
		public PS4TargetManager(NetworkTtyListener networkTtyListener)
		{
            m_NetworkTtyListener = networkTtyListener;

			try
			{
				// new ORTMAPI() will throw if the Orbis SDK has not been
				// installed becuase the COM class has not been registerred
				// (REGDB_E_CLASSNOTREG).  If that happens, handle it
				// gracefully.
				m_Tmapi = new ORTMAPI();

				m_Tmapi.CheckCompatibility((uint) eCompatibleVersion.BuildVersion);

				m_WorkerThread = new Thread(WorkerThread);
				m_WorkerThread.Name = "PS4TargetManager::WorkerThread";
				m_WorkerThread.Start();
			}
			catch
			{
			}
		}

		////////////////////////////////////////////////////////////////////////
		// PS4TargetManager::~PS4TargetManager
		////////////////////////////////////////////////////////////////////////
		~PS4TargetManager()
		{
			Dispose(false);
		}

		////////////////////////////////////////////////////////////////////////
		// PS4TargetManager::TtyListener
		////////////////////////////////////////////////////////////////////////
		public NetworkTtyListener TtyListener
		{
			get
			{
				return m_NetworkTtyListener;
			}
		}

		////////////////////////////////////////////////////////////////////////
		// PS4TargetManager::Dispose
		////////////////////////////////////////////////////////////////////////
		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		////////////////////////////////////////////////////////////////////////
		// PS4TargetManager::Dispose
		////////////////////////////////////////////////////////////////////////
		protected virtual void Dispose(bool disposing)
		{
			if(disposing)
			{
		        if(m_WorkerThread != null)
				{
					m_WorkerThreadExit = true;
					m_WorkerThread.Join();
				}

				if(m_Targets != null)
				{
					foreach(PS4Target target in m_Targets)
					{
						target.Dispose();
					}
				}

				if (m_Tmapi != null)    m_Tmapi.Dispose();
			}
		}

		////////////////////////////////////////////////////////////////////////
		// PS4TargetManager::GetDefaultTargetNames
		////////////////////////////////////////////////////////////////////////
		public string[] GetDefaultTargetNames()
		{
			List<string> ret = new List<string>();
			if(m_Tmapi != null)
			{
				Array targets = (Array)m_Tmapi.Targets;
				foreach(ITarget t in targets)
				{
					ret.Add(t.HostName);
				}
			}
			return ret.ToArray();
		}

		////////////////////////////////////////////////////////////////////////
		// PS4TargetManager::AddTarget
		////////////////////////////////////////////////////////////////////////
        public Target AddTarget(string name)
        {
			if(m_Tmapi == null)
			{
				return null;
			}

			lock(m_LockObj)
			{
	            Trace.WriteLine("Acquiring target " + name + "...");

	            PS4Target target = null;

	            if(!m_TargetsByName.TryGetValue(name, out target))
	            {
					Array tmapiTargets = (Array)m_Tmapi.GetTargetsByHost(name);
					if(tmapiTargets.Length == 0)
					{
						return null;
					}

					ITarget tmapiTarget = (ITarget)tmapiTargets.GetValue(0);
					if(tmapiTarget == null)
					{
						return null;
					}
					target = new PS4Target(name, tmapiTarget, this);

					m_Targets.Add(target);
	                m_TargetsByName.Add(name, target);
					Trace.WriteLine("Attempting to look up Game LAN IP address for PS4 console " + name + " being added...");
					string gameIP = target.LookupGameLanIpAddress();
					if (gameIP != null)
					{
	                    m_NetworkTtyListener.Register(gameIP, target);
						Trace.WriteLine("Registered game LAN IP " + gameIP + " as a TTY listener");
						m_NetworkTtyListener.Register(name, target);
						Trace.WriteLine("Registered host LAN IP " + name + " as a TTY listener");
					}
					else
					{
						Trace.WriteLine("Failed to look up game LAN IP. The console is probably offline.");
					}
	            }

	            return target;
			}
        }

		////////////////////////////////////////////////////////////////////////
		// PS4TargetManager::WorkerThread
		////////////////////////////////////////////////////////////////////////
		private void WorkerThread()
		{
			while(!m_WorkerThreadExit)
			{
				List<PS4Target> targetListCopy;
				lock(m_LockObj)
				{
					targetListCopy = new List<PS4Target>(m_Targets);
				}

				foreach(PS4Target t in targetListCopy)
				{
					t.WorkerThreadUpdate();
				}

				Thread.Sleep(500);
			}
		}

		////////////////////////////////////////////////////////////////////////
		// PS4TargetManager::GetPFSDrive
		////////////////////////////////////////////////////////////////////////
		internal sbyte GetPFSDrive(out uint options)
		{
			lock(m_LockObj)
			{
				return m_Tmapi.GetPFSDrive(out options);
			}
		}
	}
}
