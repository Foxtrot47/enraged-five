using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;

namespace Rockstar.TargetManager
{
	public class HostPcIpAddr
	{
		private static string s_Str;
		private static IPAddress s_Addr;

		public static bool Init(out string error)
		{
			error = null;
			try
			{
				// Read the ip address out of "%TEMP%/rfs.dat".  This is the address that anything
				// launched via a batchfile is going to attempt to connect to, so we had may as
				// well use that.
				//
				// Note that we are using Environment.GetEnvironmentVariable("TEMP") rather than
				// Path.GetTempPath() to match the behaviour of the game launch batchfiles.
				//
				var tempDir = Environment.GetEnvironmentVariable("TEMP");
				if(tempDir == null)
				{
					error = "Fatal Error: TEMP environment variable not set.";
					return false;
				}
				var rfsDotDat = Path.Combine(tempDir, "rfs.dat");
				if(!File.Exists(rfsDotDat))
				{
					error = "Fatal Error: \"%TEMP%\\rfs.dat\" does not exist (%TEMP%=\""+tempDir+"\").";
					return false;
				}
				using(var reader = new StreamReader(rfsDotDat))
				{
					s_Str = reader.ReadToEnd().Trim();
				}
				if(!IPAddress.TryParse(s_Str, out s_Addr))
				{
					error = "Fatal Error: \""+rfsDotDat+"\" does not contain a valid IPv4 address.";
					return false;
				}

				// Verify that there is a network interface with the address specified in rfs.dat.
				//
				// Note that Dns.GetHostName() is NOT the same thing as using
				// Environment.MachineName.  Environment.MachineName is truncated to
				// a 15-character NetBIOS name, and will fail the DNS lookup.
				//
				bool valid = false;
				foreach(var ip in Dns.GetHostAddresses(Dns.GetHostName()))
				{
					if(ip.Equals(s_Addr))
					{
						valid = true;
						break;
					}
				}
				if(!valid)
				{
					error = "Fatal Error: IP address "+s_Str+" read from \""+rfsDotDat+"\" is not assigned to any network interface.";
					return false;
				}

				Trace.WriteLine("IP address "+s_Str+" read from \""+rfsDotDat+"\".");
				return true;
			}
			catch(Exception e)
			{
				error = "Unexpected Fatal Error: "+e.Message;
				return false;
			}
		}

		public static IPAddress Get()
		{
			return s_Addr;
		}

		public static string GetStr()
		{
			return s_Str;
		}
	}
}
