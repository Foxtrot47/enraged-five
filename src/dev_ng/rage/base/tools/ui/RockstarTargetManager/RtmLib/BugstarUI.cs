﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Rockstar.TargetManager
{
	////////////////////////////////////////////////////////////////////////////
	// BugstarUI
	////////////////////////////////////////////////////////////////////////////
	public partial class BugstarUI : Form
	{
		public delegate bool CheckValidBug(string bug);
		public delegate bool TryLogin(BugstarUI ui, string username, string password);

		public volatile bool IsCancelled;
		public volatile bool IsLoggedIn;
		public volatile bool IsDone;

		private CheckValidBug m_checkValidBug;
		private TryLogin m_tryLoginFunc;
		private bool m_duplicateSearchStop;
        private string m_duplicateBugText;

		private Semaphore m_win32HandleCreated;


		////////////////////////////////////////////////////////////////////////
		// BugstarUI
		////////////////////////////////////////////////////////////////////////
		public BugstarUI(string text, CheckValidBug checkValidBug, TryLogin tryLogin)
		{
			InitializeComponent();

			IsCancelled = false;
			IsLoggedIn = false;
			IsDone = false;

			m_checkValidBug = checkValidBug;
			m_tryLoginFunc = tryLogin;

			// Crash description
			labelDescription.Text = text;

			// Initial state is requesting B* login
			textBoxUsername.Text = Environment.GetEnvironmentVariable("USERNAME");
			textBoxUsername.Enabled = true;
			textBoxPassword.Enabled = true;
			buttonLogin.Enabled = true;
			textBoxPassword.Select();
			AcceptButton = buttonLogin;
			CancelButton = buttonCancel;

			// Next state, duplicate bug search is disabled
			labelDuplicateSearch.Enabled = false;
			progressBarDuplicateSearch.Enabled = false;
			buttonDuplicateSearchStop.Enabled = false;
			m_duplicateSearchStop = false;
            m_duplicateBugText = labelDuplicateSearch.Text;

			// Final state, logging bug also disabled
			buttonOk.Enabled = false;
			radioButtonNew.Enabled = false;
			radioButtonExisting.Enabled = false;
			// textBoxExisting only enabled when radioButtonExisting checked
			textBoxExisting.Enabled = false;
			// radioButtonExisting must be checked for these to be enabled
			textBoxExisting.Enabled = false;
			buttonViewBug.Enabled = false;
			// Setup default radio buttons and initial selection
			radioButtonNew.Checked = false;
			radioButtonExisting.Checked = false;

			m_win32HandleCreated = new Semaphore(0, 1);
        }

        ////////////////////////////////////////////////////////////////////////
		// AsyncShowDialog
        ////////////////////////////////////////////////////////////////////////
		public Thread AsyncShowDialog(string threadName)
		{
			// Launch the Bugstar dialog as a modal dialog on its own thread.
			// This seems to be the simplest way to do the heavy work off the UI
			// thread (ie, do the Bugstar database queries on this thread).
			BringToFront();
			var dialogThread = new Thread(()=>ShowDialog());
			dialogThread.Name = "Bugstar Dialog";
			dialogThread.Start();
			m_win32HandleCreated.WaitOne();
			return dialogThread;
		}

        ////////////////////////////////////////////////////////////////////////
        // BugstarUI_FormClosing
        ////////////////////////////////////////////////////////////////////////
        private void BugstarUI_FormClosing(object sender, FormClosingEventArgs e)
        {
            IsCancelled = true;
        }

		////////////////////////////////////////////////////////////////////////
		// BugstarUI_Shown
		////////////////////////////////////////////////////////////////////////
		private void BugstarUI_Shown(object sender, EventArgs e)
		{
			// Signal the semaphore that is blocking the main thread now that we
			// have been shown.  The reason for this is the win32 window handle
			// is not created until ShowDialog has been called, and ShowDialog
			// is not called from the same thread that creates this BugstarUI
			// object.  This creates a race condition where this thread may
			// attempt an Invoke before the window handle is created.  The race
			// can cause the error
			//
			//     System.InvalidOperationException: Invoke or BeginInvoke cannot be called on a control until the window handle has been created.
			//
			// There is an interesting article on this problem at
			//
			//     http://ikriv.com/dev/dotnet/MysteriousHang.html
			//
			// But the solution of just aquiring the handle directly doesn't
			// work in this case due to the threading setup.  So instead we use
			// this semaphore.
			//
			var grabHandle = Handle;
			m_win32HandleCreated.Release();
		}

		////////////////////////////////////////////////////////////////////////
		// SetLoginDone
		////////////////////////////////////////////////////////////////////////
		public void SetLoginDone()
		{
			// Set password text to 8 chars long so that we hide the true
			// password length
			textBoxPassword.Text = "        ";

			// Disable login controls
			textBoxUsername.Enabled = false;
			textBoxPassword.Enabled = false;
			labelUsername.Enabled = false;
			labelPassword.Enabled = false;

			// Note that changing textBoxPassword.Text affects
			// buttonLogin.Enabled, so make sure we disable it afterwards.
			buttonLogin.Enabled = false;

			// Enable duplicate bug search
			labelDuplicateSearch.Enabled = true;
			progressBarDuplicateSearch.Enabled = true;
			buttonDuplicateSearchStop.Enabled = true;
			buttonDuplicateSearchStop.Select();
			CancelButton = null;
			AcceptButton = buttonDuplicateSearchStop;
			buttonCancel.Enabled = false;

			IsLoggedIn = true;
		}

		////////////////////////////////////////////////////////////////////////
		// DuplicateSearchProgressUpdate
		////////////////////////////////////////////////////////////////////////
		public bool DuplicateSearchProgressUpdate(int processed, int total, string currentBest)
		{
			Invoke(new MethodInvoker(() =>
			{
				progressBarDuplicateSearch.Minimum = 0;
				progressBarDuplicateSearch.Maximum = total;
				progressBarDuplicateSearch.Value   = processed;
                labelDuplicateSearch.Text = m_duplicateBugText + String.Format(" ({0} / {1})", processed, total);

				// Once we have found the first reasonable match, enable bug view button.
				if(currentBest != null)
				{
					radioButtonNew.Checked = false;
					radioButtonExisting.Checked = true;
					textBoxExisting.Text = currentBest;
					buttonViewBug.Enabled = true;
					buttonViewBug.Select();
				}
			}));
			return m_duplicateSearchStop;
		}

		////////////////////////////////////////////////////////////////////////
		// DuplicateSearchDone
		////////////////////////////////////////////////////////////////////////
		public void DuplicateSearchDone()
		{
			Invoke(new MethodInvoker(() =>
			{
				// Disable duplicate bug search
				labelDuplicateSearch.Enabled = false;
                labelDuplicateSearch.Text = m_duplicateBugText;
                progressBarDuplicateSearch.Enabled = false;
				buttonDuplicateSearchStop.Enabled = false;

				// Enable bug creation
				radioButtonNew.Enabled = true;
				radioButtonExisting.Enabled = true;
				buttonOk.Enabled = true;

				// Duplicate bug suggested ?
				if(textBoxExisting.Text != "")
				{
					radioButtonNew.Checked = false;
					radioButtonExisting.Checked = true;
					buttonViewBug.Select();
				}

				// No duplicate bug suggested ?
				else
				{
					radioButtonNew.Checked = true;
					radioButtonExisting.Checked = false;
					radioButtonNew.Select();
				}

				buttonOk.Enabled = true;
				buttonCancel.Enabled = true;
				AcceptButton = buttonOk;
				CancelButton = buttonCancel;
			}));
		}

		////////////////////////////////////////////////////////////////////////
		// textBoxPassword_TextChanged
		////////////////////////////////////////////////////////////////////////
		private void textBoxPassword_TextChanged(object sender, EventArgs e)
		{
			buttonLogin.Enabled = (textBoxPassword.Text != "");
		}

		////////////////////////////////////////////////////////////////////////
		// buttonOk_Click
		////////////////////////////////////////////////////////////////////////
		private void buttonOk_Click(object sender, EventArgs e)
		{
			IsDone = true;
		}

		////////////////////////////////////////////////////////////////////////
		// buttonCancel_Click
		////////////////////////////////////////////////////////////////////////
		private void buttonCancel_Click(object sender, EventArgs e)
		{
			IsCancelled = true;
		}

		////////////////////////////////////////////////////////////////////////
		// buttonDuplicateSearchStop_Click
		////////////////////////////////////////////////////////////////////////
		private void buttonDuplicateSearchStop_Click(object sender, EventArgs e)
		{
			m_duplicateSearchStop = true;
		}

		////////////////////////////////////////////////////////////////////////
		// buttonLogin_Click
		////////////////////////////////////////////////////////////////////////
		private void buttonLogin_Click(object sender, EventArgs e)
		{
			Enabled = false;
            string prevText = Text;
            Text = "Logging in...";
			if(m_tryLoginFunc(this, textBoxUsername.Text, textBoxPassword.Text))
			{
                Text = prevText;
                SetLoginDone();
			}
			else
			{
                Text = prevText;
                MessageBox.Show(this, "Error logging in to Bugstar. Please check your details and try again.\n\n" +
                    Bugstar.GetLastError(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
				textBoxPassword.Text = "";
				textBoxPassword.Select();
			}

			Enabled = true;
		}

		////////////////////////////////////////////////////////////////////////
		// textBoxExisting_IsValid
		////////////////////////////////////////////////////////////////////////
		private bool textBoxExisting_IsValid()
		{
			// Simple check that the text is all numbers
			string str = textBoxExisting.Text;
			if(str == "")
			{
				return false;
			}
			foreach(char c in str)
			{
				if(c<'0' || c>'9')
				{
					return false;
				}
			}

			// Lookup to check bug number is valid
			return m_checkValidBug(str);
		}

		////////////////////////////////////////////////////////////////////////
		// textBoxExisting_TextChanged
		////////////////////////////////////////////////////////////////////////
		private void textBoxExisting_TextChanged(object sender, EventArgs e)
		{
			Debug.Assert(radioButtonExisting.Checked);
			bool valid = textBoxExisting_IsValid();
			textBoxExisting.ForeColor = valid ? Color.Black : Color.Red;
			buttonViewBug.Enabled = valid;
			buttonOk.Enabled = valid;
		}

		////////////////////////////////////////////////////////////////////////
		// radioButtonNew_CheckedChanged
		////////////////////////////////////////////////////////////////////////
		private void radioButtonNew_CheckedChanged(object sender, EventArgs e)
		{
			if(radioButtonNew.Checked)
			{
				textBoxExisting.Enabled = false;
				buttonViewBug.Enabled = false;
				buttonOk.Enabled = IsLoggedIn;
			}
			else
			{
				textBoxExisting.Enabled = IsLoggedIn;
				bool valid = textBoxExisting_IsValid();
				buttonViewBug.Enabled = valid && IsLoggedIn;
				buttonOk.Enabled = valid && IsLoggedIn;
			}
		}

		////////////////////////////////////////////////////////////////////////
		// radioButtonExisting_CheckedChanged
		////////////////////////////////////////////////////////////////////////
		private void radioButtonExisting_CheckedChanged(object sender, EventArgs e)
		{
			radioButtonNew_CheckedChanged(sender, e);
		}

		////////////////////////////////////////////////////////////////////////
		// buttonViewBug_Click
		////////////////////////////////////////////////////////////////////////
		private void buttonViewBug_Click(object sender, EventArgs e)
		{
			Process.Start("url:bugstar:"+textBoxExisting.Text);
		}

		////////////////////////////////////////////////////////////////////////
		// NewBug
		////////////////////////////////////////////////////////////////////////
		public bool NewBug
		{
			get{ return radioButtonNew.Checked; }
		}

		////////////////////////////////////////////////////////////////////////
		// ExistingBugNumber
		////////////////////////////////////////////////////////////////////////
		public UInt32 ExistingBugNumber
		{
			get
			{
				Debug.Assert(!NewBug);
				try
				{
					return UInt32.Parse(textBoxExisting.Text);
				}
				catch
				{
					return 0;
				}
			}
		}
	}
}
