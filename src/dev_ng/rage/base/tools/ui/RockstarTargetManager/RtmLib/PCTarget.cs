using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.IO.MemoryMappedFiles;
using System.IO.Pipes;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;

namespace Rockstar.TargetManager
{
	////////////////////////////////////////////////////////////////////////////
	// PCTarget
	////////////////////////////////////////////////////////////////////////////
	public class PCTarget : Target
	{
		private string m_IpAddr;    // currently the same as m_Name, but that may change in the future so keep seperate to make things clearer
		private string m_Name;
		private string m_Alias;
		private PCTargetManager m_Tm;
		private delegate void VoidCallbackVoid();
		private delegate void VoidCallbackBool(bool b);
		private delegate void VoidCallbackBoolUInt64(bool b, UInt64 val);
		private struct StopNotificationCallback { internal VoidCallbackBool callback; internal DateTime utcTimeout; internal VoidCallbackVoid resumeThreads; }
		private Dictionary<UInt32, StopNotificationCallback> m_OnStopNotificationCallback = new Dictionary<UInt32, StopNotificationCallback>();

		private Thread m_DebuggerThread;
		private VoidCallbackVoid m_DebuggerThreadCallback = null;
		private volatile bool m_DebuggerThreadExit = false;

		private class ThreadInfo
		{
			internal string m_Name      = null;
			internal IntPtr m_Handle    = IntPtr.Zero;
		}
		private class ProcessInfo
		{
			internal IntPtr m_Handle                            = IntPtr.Zero;
			internal bool   m_SymbolsInitialized                = false;
			internal string m_ExeName                           = null;
			internal Dictionary<UInt32, ThreadInfo> m_Threads   = new Dictionary<UInt32, ThreadInfo>();
		}
		private Dictionary<UInt32, ProcessInfo> m_Processes = new Dictionary<UInt32, ProcessInfo>();

		private string m_BugSummary = null;
		private bool m_EnableBugDupCheck = true;

		private static string TARGET_CRASH_CALLBACK_BASE_NAME    = "rage::sysException::TargetCrashCallback";
		private static string TARGET_CRASH_CALLBACK_NAME         = TARGET_CRASH_CALLBACK_BASE_NAME+"(u32)";
		private static string TARGET_GET_BUGSTAR_RTM_CONFIG_NAME = "CBugstarIntegration::GetRockstarTargetManagerConfig(void)";
		private static string[] TARGET_SYMBOLS =
		{
			TARGET_CRASH_CALLBACK_NAME,
			TARGET_GET_BUGSTAR_RTM_CONFIG_NAME,
		};
		private Dictionary<UInt32, AsyncSymbolLookup> m_AsyncSymbolLookup = new Dictionary<UInt32, AsyncSymbolLookup>();

		private UInt32 m_ContinueDebugEvent;

		private Regex m_AttachPidRegex = new Regex("^\\>\\>R\\*TM ATTACH_PID:(.*)\n$");
		private AutoResetEvent m_DebuggerThreadAttachEvent = new AutoResetEvent(false);



		////////////////////////////////////////////////////////////////////////
		// PCTarget::PCTarget
		////////////////////////////////////////////////////////////////////////
		internal PCTarget(string name, string alias, PCTargetManager tm)
		{
			m_IpAddr = name;
			m_Name = name;
			m_Alias = alias;
			m_Tm = tm;

			if(alias == "localhost")
			{
				m_DebuggerThread = new Thread(DebuggerThread);
				m_DebuggerThread.Name = "PCTarget::DebuggerThread";
				m_DebuggerThread.Start();
			}

			// For games launched outside of R*TM control (eg, via a batchfile),
			// connection is handled by the game sending the process id inside a
			// network tty string.  So push a tty parser to watch for this
			// string.
			PushTtyParser(AttachPidTtyParser);

			tm.RegisterTargetAsTtyListener(this);
		}

		////////////////////////////////////////////////////////////////////////
		// PCTarget::CloseThreadHandles
		////////////////////////////////////////////////////////////////////////
		private void CloseThreadHandles(ProcessInfo pinfo)
		{
			foreach(var tinfo in pinfo.m_Threads)
			{
				var h = tinfo.Value.m_Handle;
				if(h != IntPtr.Zero)
				{
					try
					{
						Win32.CloseHandle(h);
					}
					catch(Exception e)
					{
						Trace.WriteLine("Failed to close thread handle (tid "+tinfo.Key+"), "+e);
					}
				}
			}
		}

		////////////////////////////////////////////////////////////////////////
		// PCTarget::CloseProcessHandles
		////////////////////////////////////////////////////////////////////////
		private void CloseProcessHandles(UInt32 pid)
		{
			ProcessInfo pinfo;
			if(m_Processes.TryGetValue(pid, out pinfo))
			{
				CloseThreadHandles(pinfo);
				if(pinfo.m_Handle != IntPtr.Zero)
				{
					try
					{
						Win32.CloseHandle(pinfo.m_Handle);
					}
					catch(Exception e)
					{
						Trace.WriteLine("Failed to close process handle (pid "+pid+"), "+e);
					}
				}
				m_Processes.Remove(pid);
			}
		}

		////////////////////////////////////////////////////////////////////////
		// PCTarget::TerminateAllProcesses
		////////////////////////////////////////////////////////////////////////
		private void TerminateAllProcesses()
		{
			foreach(var p in m_Processes)
			{
				var pid = p.Key;
				var pinfo = p.Value;

				CloseThreadHandles(pinfo);
				if(pinfo.m_Handle != IntPtr.Zero)
				{
					try
					{
						Win32.DebugActiveProcessStop(pid);
					}
					catch(Exception e)
					{
						Trace.WriteLine("Failed to detach debugging from pid "+pid+", "+e);
					}
					try
					{
						Win32.TerminateProcess(pinfo.m_Handle, 0);
					}
					catch(Exception e)
					{
						Trace.WriteLine("TerminateProcess threw "+e);
					}
					try
					{
						Win32.CloseHandle(pinfo.m_Handle);
					}
					catch(Exception e)
					{
						Trace.WriteLine("Failed to close process handle (pid "+pid+"), "+e);
					}
				}
			}
			m_Processes.Clear();
		}

		////////////////////////////////////////////////////////////////////////
		// PCTarget::Dispose
		////////////////////////////////////////////////////////////////////////
		protected override void Dispose(bool disposing)
		{
			if(disposing)
			{
				if(m_DebuggerThread != null)
				{
					// TerminateAllProcesses must be called on the debugger thread,
					// otherwise we get messages for process closing while we are
					// trying to shut everything down.
					m_DebuggerThreadCallback = () =>
					{
						TerminateAllProcesses();

						// Set the exit flag from within the callback so that
						// there is no race condition that could skip the
						// TerminateAllProcesses call.
						m_DebuggerThreadExit = true;
					};

					m_DebuggerThread.Join();
				}
			}
		}

		////////////////////////////////////////////////////////////////////////
		// PCTarget::SelectedIcon
		////////////////////////////////////////////////////////////////////////
		public override Icon SelectedIcon
		{
			get
			{
				return m_Processes.Count>0 ? Icon.RUNNING : Icon.NOT_RUNNING;
			}
		}

		////////////////////////////////////////////////////////////////////////
		// PCTarget::Launch
		////////////////////////////////////////////////////////////////////////
		public override bool Launch()
		{
			if(!EnableLaunch)
			{
				return false;
			}
			m_DebuggerThreadCallback = () =>
			{
				if(EnableLaunch)
				{
					try
					{
						RequestNewLogFile();

						var currentDirectory = Config.GetExpandedEnvVar("TargetDir");
						var applicationName = Config.GetExpandedEnvVar("TargetPath");
						var commandLine = "\""+applicationName+"\" "
						                + CommandLine
						                + " -ttyframeprefix"
						                + " -rockstartargetmanager "
						                + "tty=" + HostPcIpAddr.GetStr() + ":" + NetworkTtyListener.PORT;

						var startupInfo = new Win32.STARTUPINFO();
						startupInfo.cb = Marshal.SizeOf(startupInfo);
						startupInfo.flags = 0;
						IntPtr processAttributes = IntPtr.Zero;
						IntPtr threadAttributes  = IntPtr.Zero;
						bool inheritHandles = true;
						const Int32 creationFlags = Win32.DEBUG_PROCESS;
						IntPtr environment = IntPtr.Zero;
						Win32.PROCESS_INFORMATION processInformation;
						if(!Win32.CreateProcess(applicationName, commandLine, processAttributes, threadAttributes, inheritHandles, creationFlags, environment, currentDirectory, ref startupInfo, out processInformation))
						{
							Trace.WriteLine("Warning: Failed to launch \""+applicationName+"\", err=0x"+Marshal.GetLastWin32Error().ToString("x8"));
						}
						else
						{
							var pid = (UInt32)processInformation.processId;
							var pinfo = new ProcessInfo();
							pinfo.m_Handle = processInformation.process;

							var tid = (UInt32)processInformation.threadId;
							var tinfo = new ThreadInfo();
							tinfo.m_Name = Config.GetExpandedEnvVar("TargetFileName");
							tinfo.m_Handle = processInformation.thread;

							pinfo.m_Threads.Add(tid, tinfo);
							m_Processes.Add(pid, pinfo);
						}
					}
					catch(Exception e)
					{
						Trace.WriteLine("Exception attempting to launch to PC, "+e);
					}
				}
			};
			return true;
		}

		////////////////////////////////////////////////////////////////////////
		// PCTarget::EnableLaunch
		////////////////////////////////////////////////////////////////////////
		public override bool EnableLaunch
		{
			get { return m_Processes.Count == 0; }
		}

		////////////////////////////////////////////////////////////////////////
		// PCTarget::FastReboot
		////////////////////////////////////////////////////////////////////////
		public override void FastReboot()
		{
			if(EnableFastReboot)
			{
				m_DebuggerThreadCallback = () =>
				{
					if(EnableFastReboot)
					{
						TerminateAllProcesses();
					}
				};
			}
		}

		////////////////////////////////////////////////////////////////////////
		// PCTarget::EnableFastReboot
		////////////////////////////////////////////////////////////////////////
		public override bool EnableFastReboot
		{
			get { return m_Processes.Count > 0; }
		}


		////////////////////////////////////////////////////////////////////////
		// PCTarget::Coredump
		////////////////////////////////////////////////////////////////////////
		public override void Coredump()
		{
			m_DebuggerThreadCallback = () =>
			{
				lock(this)
				{
					if(EnableCoredump)
					{
						foreach(var p in m_Processes)
						{
							TriggerCoredump(p.Value, p.Key, IntPtr.Zero);
						}
					}
				}
			};
		}

		////////////////////////////////////////////////////////////////////////
		// PCTarget::EnableCoredump
		////////////////////////////////////////////////////////////////////////
		public override bool EnableCoredump
		{
			get { return m_Processes.Count > 0; }
		}

		////////////////////////////////////////////////////////////////////////
		// PCTarget::Name
		////////////////////////////////////////////////////////////////////////
		public override string Name
		{
			get { return m_Name; }
		}

		////////////////////////////////////////////////////////////////////////
		// PCTarget::Alias
		////////////////////////////////////////////////////////////////////////
		public override string Alias
		{
			get { return m_Alias; }
		}

		////////////////////////////////////////////////////////////////////////
		// PCTarget::IpAddr
		////////////////////////////////////////////////////////////////////////
		internal string IpAddr
		{
			get { return m_IpAddr; }
		}

		////////////////////////////////////////////////////////////////////////
		// PCTarget::PlatformId
		////////////////////////////////////////////////////////////////////////
		public override TargetPlatformId PlatformId
		{
			get { return TargetPlatformId.PC; }
		}

		////////////////////////////////////////////////////////////////////////
		// PCTarget::AttachPidTtyParser
		////////////////////////////////////////////////////////////////////////
		private string AttachPidTtyParser(string tty)
		{
			if(m_AttachPidRegex.IsMatch(tty))
			{
				var pidStr = m_AttachPidRegex.Replace(tty, "$1");
				UInt32 pid;
				if(!UInt32.TryParse(pidStr, out pid))
				{
					TtyOutput("R*TM, error extracting pid from tty.  Unable to connect to game.\n");
				}
				else
				{
					// If we aren't already attached to something, then this
					// process was launched outside of R*TM.
					bool externalProcess = (m_Processes.Count == 0);
					if(externalProcess)
					{
						RequestNewLogFile();

						// Connection needs to be done on the debugger thread so
						// that it can receive the debug events.
						string errorTty = null;
						m_DebuggerThreadCallback = () =>
						{
							// Warning, can't use TtyOutput in this callback, otherwise we will deadlock!

							if(!Win32.DebugActiveProcess(pid))
							{
								errorTty = "R*TM Failed to attach as a debugger to "+pid+", err=0x"+Marshal.GetLastWin32Error().ToString("x8")+".\n";
							}

							m_DebuggerThreadAttachEvent.Set();
						};

						// Wait for the debugger thread to attach.
						m_DebuggerThreadAttachEvent.WaitOne();
						if(errorTty != null)
						{
							TtyOutput(errorTty);
						}
					}
				}
			}

			return tty;
		}

		////////////////////////////////////////////////////////////////////////
		// PCTarget::RaiseBug
		////////////////////////////////////////////////////////////////////////
		private void RaiseBug(ProcessInfo pinfo, string summary, string description, bool enableBugDupCheck, string bugstarConfigXml, string coredump)
		{
			if(!String.IsNullOrEmpty(bugstarConfigXml))
			{
				var config = Bugstar.Config.FromString(bugstarConfigXml);
				if(config == null)
				{
					TtyOutput("Unable to read Bugstar configuration from the game to automatically log a bug.  Please create bug manually\n");
				}
				else
				{
					var filesToCopy = new List<string>();
					if(!String.IsNullOrEmpty(coredump))
					{
						filesToCopy.Add(coredump);
					}

					// PDB and map file
					if(!String.IsNullOrEmpty(pinfo.m_ExeName))
					{
						var regex = new Regex(@"^(.*)\.exe$");
						if(regex.IsMatch(pinfo.m_ExeName))
						{
							var pdb = regex.Replace(pinfo.m_ExeName, "$1.pdb");
							filesToCopy.Add(pdb);

							var map = regex.Replace(pinfo.m_ExeName, "$1.map");
							filesToCopy.Add(map);
						}
					}

					BaseRaiseBug(config, summary, description, filesToCopy.ToArray(), enableBugDupCheck);
				}
			}
		}

		////////////////////////////////////////////////////////////////////////
		// PCTarget::AddStopNotificationCallback
		////////////////////////////////////////////////////////////////////////
		private void AddStopNotificationCallback(UInt32 pid, VoidCallbackVoid resumeThreads, VoidCallbackBool callback)
		{
			StopNotificationCallback snc;
			snc.callback = callback;
			snc.utcTimeout = DateTime.UtcNow.AddSeconds(10);
			snc.resumeThreads = resumeThreads;
			m_OnStopNotificationCallback.Add(pid, snc);
		}

		////////////////////////////////////////////////////////////////////////
		// PCTarget::TryGetProcessInfo
		////////////////////////////////////////////////////////////////////////
		private bool TryGetProcessInfo(UInt32 pid, out ProcessInfo pinfo)
		{
			pinfo = null;
			try
			{
				if(m_Processes.TryGetValue(pid, out pinfo))
				{
					return true;
				}
				const UInt32 access =
					Win32.PROCESS_QUERY_INFORMATION |
					Win32.PROCESS_SET_INFORMATION |
					Win32.PROCESS_SUSPEND_RESUME |
					Win32.PROCESS_TERMINATE |
					Win32.PROCESS_VM_OPERATION |
					Win32.PROCESS_VM_READ |
					Win32.PROCESS_VM_WRITE;
				var handle = Win32.OpenProcess(access, false, pid);
				if(handle == IntPtr.Zero)
				{
					return false;
				}
				pinfo = new ProcessInfo();
				pinfo.m_Handle = handle;
				m_Processes.Add(pid, pinfo);
				return true;
			}
			catch
			{
				return false;
			}
		}

		////////////////////////////////////////////////////////////////////////
		// PCTarget::TryGetThreadInfo
		////////////////////////////////////////////////////////////////////////
		private bool TryGetThreadInfo(ProcessInfo pinfo, UInt32 pid, UInt32 tid, out ThreadInfo tinfo)
		{
			tinfo = null;
			try
			{
				if(pinfo.m_Threads.TryGetValue(tid, out tinfo))
				{
					return true;
				}
				const UInt32 access =
					Win32.THREAD_GET_CONTEXT |
					Win32.THREAD_QUERY_INFORMATION |
					Win32.THREAD_SET_CONTEXT |
					Win32.THREAD_SET_INFORMATION |
					Win32.THREAD_SUSPEND_RESUME |
					Win32.THREAD_TERMINATE;
				var handle = Win32.OpenThread(access, false, tid);
				if(handle == IntPtr.Zero)
				{
					return false;
				}
				tinfo = new ThreadInfo();
				tinfo.m_Name = "unknown";
				tinfo.m_Handle = handle;
				pinfo.m_Threads.Add(tid, tinfo);
				m_Processes[pid] = pinfo;
				return true;
			}
			catch
			{
				return false;
			}
		}

		////////////////////////////////////////////////////////////////////////
		// PCTarget::TryGetThreadInfo
		////////////////////////////////////////////////////////////////////////
		private bool TryGetThreadInfo(UInt32 pid, UInt32 tid, out ThreadInfo tinfo)
		{
			tinfo = null;
			ProcessInfo pinfo;
			if(!TryGetProcessInfo(pid, out pinfo))
			{
				return false;
			}
			return TryGetThreadInfo(pinfo, pid, tid, out tinfo);
		}

		////////////////////////////////////////////////////////////////////////
		// PCTarget::GetMemory
		////////////////////////////////////////////////////////////////////////
		private bool GetMemory(ProcessInfo pinfo, UInt64 addr, ref byte[] buf)
		{
			try
			{
				IntPtr numberOfBytesRead;
				if(!Win32.ReadProcessMemory(pinfo.m_Handle, (IntPtr)addr, buf, buf.Length, out numberOfBytesRead))
				{
					return false;
				}
				return (numberOfBytesRead == (IntPtr)buf.Length);
			}
			catch(Exception /*e*/)
			{
				//Trace.WriteLine("Failure reading process memory, pid "+pid+", "+e);
				return false;
			}
		}

		////////////////////////////////////////////////////////////////////////
		// PCTarget::GetMemory
		////////////////////////////////////////////////////////////////////////
		private bool GetMemory(ProcessInfo pinfo, UInt64 addr, out UInt32 data)
		{
			data = 0;
			var buf = new byte[4];
			if(!GetMemory(pinfo, addr, ref buf))
			{
				return false;
			}
			for(uint i=4; i-->0;)
			{
				data <<= 8;
				data += buf[i];
			}
			return true;
		}

		////////////////////////////////////////////////////////////////////////
		// PCTarget::GetMemory
		////////////////////////////////////////////////////////////////////////
		private bool GetMemory(ProcessInfo pinfo, UInt64 addr, out UInt64 data)
		{
			data = 0;
			var buf = new byte[8];
			if(!GetMemory(pinfo, addr, ref buf))
			{
				return false;
			}
			for(uint i=8; i-->0;)
			{
				data <<= 8;
				data += buf[i];
			}
			return true;
		}

		////////////////////////////////////////////////////////////////////////
		// PCTarget::GetMemoryAscii
		////////////////////////////////////////////////////////////////////////
		private bool GetMemoryAscii(ProcessInfo pinfo, UInt64 addr, out string str)
		{
			return GetString(addr, Encoding.ASCII, 1, out str, (UInt64 a, ref byte[] buf) => GetMemory(pinfo, a, ref buf));
		}

		////////////////////////////////////////////////////////////////////////
		// PCTarget::GetMemoryUnicode
		////////////////////////////////////////////////////////////////////////
		private bool GetMemoryUnicode(ProcessInfo pinfo, UInt64 addr, out string str)
		{
			return GetString(addr, Encoding.Unicode, 2, out str, (UInt64 a, ref byte[] buf) => GetMemory(pinfo, a, ref buf));
		}

		////////////////////////////////////////////////////////////////////////
		// PCTarget::GetMemory
		////////////////////////////////////////////////////////////////////////
		private bool GetMemory(ProcessInfo pinfo, UInt64 addr, int unicode, out string str)
		{
			return unicode!=0 ? GetMemoryUnicode(pinfo, addr, out str) : GetMemoryAscii(pinfo, addr, out str);
		}

		////////////////////////////////////////////////////////////////////////
		// PCTarget::SetMemory
		////////////////////////////////////////////////////////////////////////
		private bool SetMemory(ProcessInfo pinfo, UInt64 addr, byte[] buf)
		{
			UInt64 numberOfBytesWriten;
			if(!Win32.WriteProcessMemory(pinfo.m_Handle, addr, buf, (UInt64)buf.Length, out numberOfBytesWriten))
			{
				TtyOutput("Warning: Failed to set target memory, err=0x"+Marshal.GetLastWin32Error().ToString("x8")+".\n");
				return false;
			}
			return numberOfBytesWriten == (UInt64)buf.Length;
		}

		////////////////////////////////////////////////////////////////////////
		// PCTarget::WriteUInt64
		////////////////////////////////////////////////////////////////////////
		private bool WriteUInt64(ProcessInfo pinfo, UInt64 addr, UInt64 val)
		{
			byte[] mem = {
				(byte)(val      &0xff), (byte)((val>>8) &0xff), (byte)((val>>16)&0xff), (byte)((val>>24)&0xff),
				(byte)((val>>32)&0xff), (byte)((val>>40)&0xff), (byte)((val>>48)&0xff), (byte)((val>>56)&0xff)};
			return SetMemory(pinfo, addr, mem);
		}

		////////////////////////////////////////////////////////////////////////
		// PCTarget::TargetCallbackStateSaver
		////////////////////////////////////////////////////////////////////////
		// Saves and restores registers, and also suspends all but the
		// specified thread.
		private class TargetCallbackStateSaver : IDisposable
		{
			private PCTarget m_Target;
			private List<IntPtr> m_SuspendedThreads = new List<IntPtr>();
			private IntPtr m_ExecuteThread;
			private Win32.CONTEXT m_Context;


			////////////////////////////////////////////////////////////////
			// PCTarget::TargetCallbackStateSaver::TargetCallbackStateSaver
			////////////////////////////////////////////////////////////////
			public TargetCallbackStateSaver(PCTarget target, ProcessInfo pinfo, IntPtr executeThread, Win32.CONTEXT context)
			{
				m_Target = target;
				m_ExecuteThread = executeThread;
				m_Context = context;

				// Suspend all runnable threads
				foreach(var t in pinfo.m_Threads)
				{
					var th = t.Value.m_Handle;
					if(th != executeThread)
					{
						if(Win32.SuspendThread(th) == (UInt32)0xffffffff)
						{
							target.TtyOutput("Warning: Failed to suspend thread "+t.Key+", err=0x"+Marshal.GetLastWin32Error().ToString("x8")+".\n");
						}
						else
						{
							m_SuspendedThreads.Add(th);
						}
					}
				}
			}

			////////////////////////////////////////////////////////////////
			// PCTarget::TargetCallbackStateSaver::~TargetCallbackStateSaver
			////////////////////////////////////////////////////////////////
			~TargetCallbackStateSaver()
			{
				Dispose(false);
			}

			////////////////////////////////////////////////////////////////
			// PCTarget::TargetCallbackStateSaver::Dispose
			////////////////////////////////////////////////////////////////
			public void Dispose()
			{
				Dispose(true);
				GC.SuppressFinalize(this);
			}

			////////////////////////////////////////////////////////////////
			// PCTarget::TargetCallbackStateSaver::Dispose
			////////////////////////////////////////////////////////////////
			protected virtual void Dispose(bool disposing)
			{
				if(disposing)
				{
					m_Target.SetThreadContext(m_ExecuteThread, m_Context);
					ResumeThreads();
				}
			}

			////////////////////////////////////////////////////////////////
			// PCTarget::TargetCallbackStateSaver::ResumeThreads
			////////////////////////////////////////////////////////////////
			internal void ResumeThreads()
			{
				// Resume threads
				foreach(var t in m_SuspendedThreads)
				{
					if(Win32.ResumeThread(t) == (UInt32)0xffffffff)
					{
						m_Target.TtyOutput("Warning: Failed to resume thread "+t+", err=0x"+Marshal.GetLastWin32Error().ToString("x8")+".\n");
					}
				}
				m_SuspendedThreads.Clear();
			}
		}

		////////////////////////////////////////////////////////////////////////
		// PCTarget::ExecuteTargetCallback
		////////////////////////////////////////////////////////////////////////
		private void ExecuteTargetCallback(ProcessInfo pinfo, UInt32 pid, IntPtr threadHandle, UInt32 tid, Win32.CONTEXT context, UInt64 targetCallbackAddr, UInt64 arg0, UInt64 arg1, UInt64 arg2, VoidCallbackBoolUInt64 thenDo)
		{
			if(targetCallbackAddr != 0)
			{
				try
				{
					var stateSaver = new TargetCallbackStateSaver(this, pinfo, threadHandle, context);
					AddStopNotificationCallback(pid, stateSaver.ResumeThreads, (bool valid) =>
					{
						if(valid)
						{
							UInt64 rax = 0;
							GetThreadContext(pinfo, pid, tid, (threadHandle2, name, context2) =>
							{
								rax = context2.rax;
							});
							stateSaver.Dispose();
							thenDo(true, rax);
						}
						else
						{
							stateSaver.Dispose();
							thenDo(false, 0);
						}
					});

					// TODO: Currently just using the thread's stack, would be best
					// if there was a pre-allocated stack that we switch to instead
					// (so we can handle stack overflow crashes, etc).  But, the
					// catch is that we need to know the address of the stack to
					// switch to.  The .cmp file format only includes functions, not
					// other symbols, so would need some hacky way to put the
					// addresses in the .text segment, or maybe get the runtime code
					// to do the stack switch.
					//
					// Win64 ABI requires rsp%16=0, except withing a function
					// prologue.  Since it is possible the crash occurred during
					// the prologue, first align the stack pointer, then
					// decrement by 8 to simulate a near call.
					//
					var rsp = context.rsp;
					rsp = (UInt64)((Int64)rsp&~15)-8;
					WriteUInt64(pinfo, rsp, 0);
					context.rsp = rsp;
					context.rip = targetCallbackAddr;
					context.rcx = arg0;
					context.rdx = arg1;
					context.r8  = arg2;
					context.eFlags &= (UInt32)0xfffffbff; // ~0x400, clear RFLAGS.DF (direction flag)
					SetThreadContext(threadHandle, context);

					// Main loop will contine from the current debug event, which will restart the process
					return;
				}
				catch(Exception e)
				{
					TtyOutput("Threw exception trying to run target callback, "+e.ToString()+"\n");
				}
			}

			thenDo(false, 0);
		}

		////////////////////////////////////////////////////////////////////////
		// PCTarget::ExecuteTargetCallback
		////////////////////////////////////////////////////////////////////////
		private void ExecuteTargetCallback(ProcessInfo pinfo, UInt32 pid, IntPtr threadHandle, UInt32 tid, Win32.CONTEXT context, UInt64 targetCallbackAddr, UInt64 arg0, VoidCallbackBoolUInt64 thenDo)
		{
			ExecuteTargetCallback(pinfo, pid, threadHandle, tid, context, targetCallbackAddr, arg0, 0, 0, thenDo);
		}

		////////////////////////////////////////////////////////////////////////
		// PCTarget::ExecuteTargetCallback
		////////////////////////////////////////////////////////////////////////
		private void ExecuteTargetCallback(ProcessInfo pinfo, UInt32 pid, IntPtr threadHandle, UInt32 tid, Win32.CONTEXT context, UInt64 targetCallbackAddr, UInt64 arg0, UInt64 arg1, UInt64 arg2, VoidCallbackBool thenDo)
		{
			ExecuteTargetCallback(pinfo, pid, threadHandle, tid, context, targetCallbackAddr, arg0, arg1, arg2, (b, ignored) => thenDo(b));
		}

		////////////////////////////////////////////////////////////////////////
		// PCTarget::ExecuteTargetCallback
		////////////////////////////////////////////////////////////////////////
		private void ExecuteTargetCallback(ProcessInfo pinfo, UInt32 pid, IntPtr threadHandle, UInt32 tid, Win32.CONTEXT context, UInt64 targetCallbackAddr, UInt64 arg0, VoidCallbackBool thenDo)
		{
			ExecuteTargetCallback(pinfo, pid, threadHandle, tid, context, targetCallbackAddr, arg0, 0, 0, (b, ignored) => thenDo(b));
		}

		////////////////////////////////////////////////////////////////////////
		// PCTarget::CmpFilename
		////////////////////////////////////////////////////////////////////////
		private string CmpFilename(string exeName)
		{
			var regex = new Regex(@"^(.*)\.exe$");
			if(exeName==null || !regex.IsMatch(exeName))
			{
				return null;
			}
			return regex.Replace(exeName, "$1.cmp");
		}

		////////////////////////////////////////////////////////////////////////
		// PCTarget::StartAsyncSymbolLookup
		////////////////////////////////////////////////////////////////////////
		private void StartAsyncSymbolLookup(ProcessInfo pinfo, UInt32 pid)
		{
			var procHandle = pinfo.m_Handle;
			if(procHandle == IntPtr.Zero)
			{
				return;
			}

			var exeName = pinfo.m_ExeName;
			if(String.IsNullOrEmpty(exeName))
			{
				return;
			}

			var module = new IntPtr[1];
			UInt32 cbNeeded;
			if(!Win32.EnumProcessModules(procHandle, module, (UInt32)Marshal.SizeOf(module[0]), out cbNeeded))
			{
				Trace.WriteLine("EnumProcessModules failed, err=0x"+Marshal.GetLastWin32Error().ToString("x8"));
				return;
			}

			Win32.MODULEINFO modinfo = default(Win32.MODULEINFO);
			if(!Win32.GetModuleInformation(procHandle, module[0], ref modinfo, (UInt32)Marshal.SizeOf(modinfo)))
			{
				Trace.WriteLine("GetModuleInformation failed, err=0x"+Marshal.GetLastWin32Error().ToString("x8"));
				return;
			}

			var sym = new AsyncSymbolLookup(CmpFilename(exeName), TargetPlatformId.PC, TARGET_SYMBOLS, (UInt64)modinfo.baseOfDll, (UInt64)modinfo.sizeOfImage);
			m_AsyncSymbolLookup.Add(pid, sym);
		}

		////////////////////////////////////////////////////////////////////////
		// PCTarget::HandleTmCommand
		////////////////////////////////////////////////////////////////////////
		private bool HandleTmCommand(ProcessInfo pinfo, UInt32 pid, UInt32 tid, UInt64 exceptionAddress, byte cmd, ref bool runCrashHandler)
		{
			bool handled = true;
			runCrashHandler = false;
			switch((TMCommands)cmd)
			{
				case TMCommands.NOP:
				{
					// Do nothing
					break;
				}

				case TMCommands.PRINT_ALL_THREAD_STACKS:
				{
					PrintAllThreadStacks(pinfo);
					break;
				}

				case TMCommands.STOP_NO_ERROR_REPORT:
				{
					m_ContinueDebugEvent = 0;
					TtyOutput("\nsysTmCmdStopNoErrorReport recieved\n\n");
					ThreadInfo tinfo;
					if(TryGetThreadInfo(pinfo, pid, tid, out tinfo))
					{
						PrintThreadStack("", pinfo, tinfo);
					}
					break;
				}

				case TMCommands.QUITF:
				{
					m_BugSummary = "Quitf";
					GetThreadContext(pinfo, pid, tid, (threadHandle, name, context) =>
					{
						if(GetMemoryAscii(pinfo, context.rcx, out m_BugSummary))
						{
							m_BugSummary = "Quitf \""+m_BugSummary+"\"";
						}
					});
					runCrashHandler = true;
					m_ContinueDebugEvent = 0;
					break;
				}

				case TMCommands.GPU_HANG:
				{
					m_BugSummary = "PC GPU Error";
					runCrashHandler = true;
					m_ContinueDebugEvent = 0;
					m_EnableBugDupCheck = false;
					break;
				}

				case TMCommands.CPU_HANG:
				{
					m_BugSummary = "CPU Hang";
					runCrashHandler = true;
					m_ContinueDebugEvent = 0;
					m_EnableBugDupCheck = false;
					break;
				}

				case TMCommands.CONFIG:
				{
					GetThreadContext(pinfo, pid, tid, (threadHandle, name, context) =>
					{
						string config;
						if(GetMemoryAscii(pinfo, context.rcx, out config))
						{
							// Currently no PC config info, just echo it to the tty
							TtyOutput("sysTmCmdConfig: "+config+"\n");
						}
					});
					break;
				}

				case TMCommands.CREATE_DUMP:
				{
					// TODO ???
					break;
				}

				// EXCEPTION_HANDLER_(BEGIN|END) not expected on PC.
				case TMCommands.EXCEPTION_HANDLER_BEGIN:
				case TMCommands.EXCEPTION_HANDLER_END:
				default:
				{
					handled = false;
					runCrashHandler = true;
					break;
				}
			}

			if(handled)
			{
			}

			return handled;
		}

		////////////////////////////////////////////////////////////////////////
		// PCTarget::PrintContextRegisters
		////////////////////////////////////////////////////////////////////////
		private void PrintContextRegisters(string indent, Win32.CONTEXT ctx)
		{
			TtyOutput(indent+"RAX "+ctx.rax.ToString("x16")+"    R8   "+ctx.r8 .ToString("x16")+"\n");
			TtyOutput(indent+"RCX "+ctx.rcx.ToString("x16")+"    R9   "+ctx.r9 .ToString("x16")+"\n");
			TtyOutput(indent+"RDX "+ctx.rdx.ToString("x16")+"    R10  "+ctx.r10.ToString("x16")+"\n");
			TtyOutput(indent+"RBX "+ctx.rbx.ToString("x16")+"    R11  "+ctx.r11.ToString("x16")+"\n");
			TtyOutput(indent+"RSP "+ctx.rsp.ToString("x16")+"    R12  "+ctx.r12.ToString("x16")+"\n");
			TtyOutput(indent+"RBP "+ctx.rbp.ToString("x16")+"    R13  "+ctx.r13.ToString("x16")+"\n");
			TtyOutput(indent+"RSI "+ctx.rsi.ToString("x16")+"    R14  "+ctx.r14.ToString("x16")+"\n");
			TtyOutput(indent+"RDI "+ctx.rdi.ToString("x16")+"    R15  "+ctx.r15.ToString("x16")+"\n\n");

			TtyOutput(indent+"RIP    "+ctx.rip   .ToString("x16")+"\n");
			TtyOutput(indent+"EFLAGS "+ctx.eFlags.ToString("x8")+"\n\n");

			// The debug register really are not useful to be spamming out
			//TtyOutput(indent+"DR0 "+ctx.dr0.ToString("x16")+"\n");
			//TtyOutput(indent+"DR1 "+ctx.dr1.ToString("x16")+"\n");
			//TtyOutput(indent+"DR2 "+ctx.dr2.ToString("x16")+"\n");
			//TtyOutput(indent+"DR3 "+ctx.dr3.ToString("x16")+"\n");
			//TtyOutput(indent+"DR6 "+ctx.dr6.ToString("x16")+"\n");
			//TtyOutput(indent+"DR7 "+ctx.dr7.ToString("x16")+"\n");
		}

		////////////////////////////////////////////////////////////////////////
		// PCTarget::GetSymbol
		////////////////////////////////////////////////////////////////////////
		private string GetSymbol(IntPtr processHandle, UInt64 addr)
		{
			// Function name
			string symbolName;
			UInt64 disp = 0;
			unsafe
			{
				const int MAX_NAME_LEN = 256;
				int unpaddedStructSize = Marshal.SizeOf(typeof(Win32.SYMBOL_INFO));
				int BUF_SIZE = unpaddedStructSize+MAX_NAME_LEN*2;
				var buf = new byte[BUF_SIZE];
				fixed(void* ptr = &buf[0])
				{
					var symbolInfo = (Win32.SYMBOL_INFO*)ptr;
					symbolInfo->maxNameLen = MAX_NAME_LEN;
					int paddedStructSize = (unpaddedStructSize+7)&~7;
					symbolInfo->sizeOfStruct = (UInt32)paddedStructSize;
					if(Win32.SymFromAddrW(processHandle, addr, out disp, symbolInfo))
					{
						symbolName = Encoding.Unicode.GetString(buf, unpaddedStructSize, BUF_SIZE-unpaddedStructSize);
						var nullChar = symbolName.IndexOf('\0');
						if(nullChar >= 0)
						{
							symbolName = symbolName.Substring(0, nullChar);
						}
					}
					else
					{
						//Trace.WriteLine("SymFromAddrW err=0x"+Marshal.GetLastWin32Error().ToString("x8"));
						symbolName = "unknown";
						disp = 0;
					}
				}
			}

			// Module name
			string moduleName;
			unsafe
			{
				var moduleInfo = new Win32.IMAGEHLP_MODULEW64();
				moduleInfo.sizeOfStruct = (UInt32)Marshal.SizeOf(moduleInfo);
				if(!Win32.SymGetModuleInfoW64(processHandle, addr, ref moduleInfo))
				{
					Trace.WriteLine("SymGetModuleInfoW64 err=0x"+Marshal.GetLastWin32Error().ToString("x8"));
					moduleName = "unknown";
					disp = 0;
				}
				else
				{
					// IMAGEHLP_MODULEW64.moduleName does not contain the file
					// extension, so for compatability with the runtime
					// exception handler, use loadedImageName and strip off the
					// path.
					int strlen;
					for(strlen=0; strlen<Win32.IMAGEHLP_MODULEW64_NELEM_LOADEDIMAGENAME; ++strlen)
					{
						if(moduleInfo.loadedImageName[strlen] == '\0')
						{
							break;
						}
					}
					var loadedImageName = new string((char*)moduleInfo.loadedImageName, 0, strlen);
					moduleName = Path.GetFileName(loadedImageName);
				}
			}

			// Line number
			string lineStr = "";
			Win32.IMAGEHLP_LINE64 line = default(Win32.IMAGEHLP_LINE64);
			UInt32 dispLine;
			line.sizeOfStruct = (UInt32)Marshal.SizeOf(line);
			if(Win32.SymGetLineFromAddr64(processHandle, addr, out dispLine, out line))
			{
				lineStr = "(Line "+line.lineNumber+")";
			}
			else
			{
				//Trace.WriteLine("SymGetLineFromAddr64 err=0x"+Marshal.GetLastWin32Error().ToString("x8"));
			}

			return moduleName+"!"+symbolName+lineStr;
		}

		////////////////////////////////////////////////////////////////////////
		// PCTarget::PrintThreadStack
		////////////////////////////////////////////////////////////////////////
		private void PrintThreadStack(string indent, IntPtr processHandle, IntPtr threadHandle, Win32.CONTEXT ctx)
		{
			Win32.STACKFRAME64 stackFrame = default(Win32.STACKFRAME64);
			stackFrame.addrPC.offset        = ctx.rip;
			stackFrame.addrPC.segment       = ctx.segCs;
			stackFrame.addrPC.mode          = Win32.AddrModeFlat;
			stackFrame.addrReturn.offset    = 0;
			stackFrame.addrReturn.segment   = 0;
			stackFrame.addrReturn.mode      = Win32.AddrModeFlat;
			stackFrame.addrFrame.offset     = ctx.rbp;
			stackFrame.addrFrame.segment    = ctx.segSs;
			stackFrame.addrFrame.mode       = Win32.AddrModeFlat;
			stackFrame.addrStack.offset     = ctx.rsp;
			stackFrame.addrStack.segment    = ctx.segSs;
			stackFrame.addrStack.mode       = Win32.AddrModeFlat;
			stackFrame.addrBStore.offset    = 0;
			stackFrame.addrBStore.segment   = 0;
			stackFrame.addrBStore.mode      = Win32.AddrModeFlat;
			stackFrame.funcTableEntry       = 0;
			stackFrame.param0               = 0;
			stackFrame.param1               = 0;
			stackFrame.param2               = 0;
			stackFrame.param3               = 0;
			stackFrame.far                  = false;
			stackFrame.virtual_             = false;
			stackFrame.reserved0            = 0;
			stackFrame.reserved1            = 0;
			stackFrame.reserved2            = 0;
			// stackFrame.kdHelp only used for kernel mode stacks

			Win32.CONTEXT ctxFrame = ctx; // TODO: ensure this is copying by value

			while(Win32.StackWalk64(Win32.IMAGE_FILE_MACHINE_AMD64, processHandle, threadHandle, ref stackFrame, ref ctxFrame, IntPtr.Zero, IntPtr.Zero, IntPtr.Zero, IntPtr.Zero))
			{
				var addr = stackFrame.addrPC.offset;
				var sym = GetSymbol(processHandle, addr);
				TtyOutput(indent+"0x"+addr.ToString("x16")+" - "+sym+"\n");
			}
		}

		////////////////////////////////////////////////////////////////////////
		// PCTarget::PrintThreadStack
		////////////////////////////////////////////////////////////////////////
		private void PrintThreadStack(string indent, ProcessInfo pinfo, ThreadInfo tinfo)
		{
			GetThreadContext(tinfo, (threadHandle, name, context) =>
			{
				PrintThreadStack("", pinfo.m_Handle, tinfo.m_Handle, context);
			});
		}

		////////////////////////////////////////////////////////////////////////
		// PCTarget::PrintAllThreadStacks
		////////////////////////////////////////////////////////////////////////
		private void PrintAllThreadStacks(ProcessInfo pinfo)
		{
			foreach(var t in pinfo.m_Threads)
			{
				TtyOutput("\nThread "+t.Key+" "+t.Value.m_Name+"\n");
				PrintThreadStack("", pinfo, t.Value);
			}
			TtyOutput("\n");
		}

		////////////////////////////////////////////////////////////////////////
		// PCTarget::GetThreadContext
		////////////////////////////////////////////////////////////////////////
		private delegate void GetThreadContextCallback(IntPtr handle, string name, Win32.CONTEXT cotext);
		private void GetThreadContext(ThreadInfo tinfo, GetThreadContextCallback thenDo)
		{
			var threadName = tinfo.m_Name;
			var threadHandle = tinfo.m_Handle;
			if(threadHandle != IntPtr.Zero)
			{
				var buf = new byte[Marshal.SizeOf(typeof(Win32.CONTEXT))+15];
				unsafe
				{
					fixed(void* ptr = &buf[0])
					{
						var aligned = (IntPtr)(((UInt64)ptr+15)&~(UInt64)15);
						var context = (Win32.CONTEXT)Marshal.PtrToStructure(aligned, typeof(Win32.CONTEXT));
						context.contextFlags = Win32.CONTEXT_ALL;
						if(Win32.GetThreadContext(threadHandle, ref context))
						{
							thenDo(threadHandle, threadName, context);
						}
						else
						{
							TtyOutput("R*TM failed getting thread context, err=0x"+Marshal.GetLastWin32Error().ToString("x8")+".\n");
						}
					}
				}
			}
		}

		////////////////////////////////////////////////////////////////////////
		// PCTarget::GetThreadContext
		////////////////////////////////////////////////////////////////////////
		private void GetThreadContext(ProcessInfo pinfo, UInt32 pid, UInt32 tid, GetThreadContextCallback thenDo)
		{
			ThreadInfo tinfo;
			if(TryGetThreadInfo(pinfo, pid, tid, out tinfo))
			{
				GetThreadContext(tinfo, thenDo);
			}
		}

		////////////////////////////////////////////////////////////////////////
		// PCTarget::GetThreadContext
		////////////////////////////////////////////////////////////////////////
		private void GetThreadContext(UInt32 pid, UInt32 tid, GetThreadContextCallback thenDo)
		{
			ProcessInfo pinfo;
			if(TryGetProcessInfo(pid, out pinfo))
			{
				GetThreadContext(pinfo, pid, tid, thenDo);
			}
		}

		////////////////////////////////////////////////////////////////////////
		// PCTarget::SetThreadContext
		////////////////////////////////////////////////////////////////////////
		private void SetThreadContext(IntPtr threadHandle, Win32.CONTEXT unalignedContext)
		{
			if(threadHandle != IntPtr.Zero)
			{
				var buf = new byte[Marshal.SizeOf(typeof(Win32.CONTEXT))+15];
				unsafe
				{
					fixed(void* ptr = &buf[0])
					{
						var aligned = (IntPtr)(((UInt64)ptr+15)&~(UInt64)15);
						var context = (Win32.CONTEXT*)aligned;
						*context = unalignedContext;
						if(!Win32.SetThreadContext(threadHandle, context))
						{
							TtyOutput("R*TM failed setting thread context, err=0x"+Marshal.GetLastWin32Error().ToString("x8")+".\n");
						}
					}
				}
			}
		}

		////////////////////////////////////////////////////////////////////////
		// PCTarget::TriggerCoredump
		////////////////////////////////////////////////////////////////////////
		private string TriggerCoredump(ProcessInfo pinfo, UInt32 pid, IntPtr expParams)
		{
			var name = Path.GetFileNameWithoutExtension(pinfo.m_ExeName);
			var path = RockstarTargetManager.GetCrashdumpDir()+name+"-rtm"+DateTime.Now.ToString("-yyyyMMdd-HHmmss-")+pid.ToString()+".dmp";

			FileStream fs = null;
			try
			{
				fs = new FileStream(path, FileMode.Create, FileAccess.ReadWrite, FileShare.None);
			}
			catch(Exception e)
			{
				TtyOutput("Failed to create dmp file \""+path+"\", error: "+e.ToString()+"\n");
				return null;
			}

			const Int32 dumpType
				= Win32.MiniDumpWithFullMemory
				| Win32.MiniDumpWithHandleData
				| Win32.MiniDumpWithProcessThreadData
				| Win32.MiniDumpWithFullMemoryInfo
				| Win32.MiniDumpWithThreadInfo;

			TtyOutput("Writing dmp file \""+path+"\"\n");
			if(!Win32.MiniDumpWriteDump(pinfo.m_Handle, pid, fs.SafeFileHandle, dumpType, expParams, IntPtr.Zero, IntPtr.Zero))
			{
				TtyOutput("Failed to write dmp file \""+path+"\", err=0x"+Marshal.GetLastWin32Error().ToString("x8")+"\n");
				return null;
			}
			fs.Dispose();
			TtyOutput("... successfull\n");
			return path;
		}

		private string TriggerCoredump(ProcessInfo pinfo, UInt32 pid, UInt32 tid, Win32.EXCEPTION_RECORD exceptionRecord, Win32.CONTEXT context)
		{
			try
			{
				using(var expRec = new HGlobal(exceptionRecord))
				{
					using(var ctxRec = new HGlobal(context, 16))
					{
						Win32.EXCEPTION_POINTERS expPtrsMan;
						expPtrsMan.exceptionRecord = expRec.Addr;
						expPtrsMan.contextRecord   = ctxRec.Addr;
						using(var expPtrs = new HGlobal(expPtrsMan))
						{
							Win32.MINIDUMP_EXCEPTION_INFORMATION expParamsMan;
							expParamsMan.threadId = tid;
							expParamsMan.exceptionPointers = expPtrs.Addr;
							expParamsMan.clientPointers = false;
							using(var expParams = new HGlobal(expParamsMan))
							{
								return TriggerCoredump(pinfo, pid, expParams.Addr);
							}
						}
					}
				}
			}
			catch(Exception)
			{
				return null;
			}
		}

		////////////////////////////////////////////////////////////////////////
		// PCTarget::CrashHandler
		////////////////////////////////////////////////////////////////////////
		private void CrashHandler(UInt32 pid, UInt32 tid, Win32.EXCEPTION_RECORD exceptionRecord)
		{
			UInt64 targetCrashCallbackAddr = 0;
			UInt64 getBugstarConfigCallbackAddr = 0;
			AsyncSymbolLookup symbols;
			if(m_AsyncSymbolLookup.TryGetValue(pid, out symbols))
			{
				targetCrashCallbackAddr      = symbols.Lookup(TARGET_CRASH_CALLBACK_NAME);
				getBugstarConfigCallbackAddr = symbols.Lookup(TARGET_GET_BUGSTAR_RTM_CONFIG_NAME);
			}

			string bugDescription;

			ProcessInfo pinfo;
			if(TryGetProcessInfo(pid, out pinfo))
			{
				var processHandle = pinfo.m_Handle;
				GetThreadContext(pid, tid, (threadHandle, name, context) =>
				{
					ExecuteTargetCallback(pinfo, pid, threadHandle, tid, context, targetCrashCallbackAddr, 0, (valid0) =>
					{
						if(!valid0)
						{
							TtyOutput("Failure executing "+TARGET_CRASH_CALLBACK_BASE_NAME+"(0)\n\n");
						}

						// Grab the Bugstar configuration early here.  If a target callback
						// deadlocks, we may not be able to ever call any more callbacks after
						// that (thread is left in OS code, and cannot modify registers in that
						// state).  The game code callback that executes in
						// TargetCrashCallback(1) is particuarly susceptable to this problem.
						ExecuteTargetCallback(pinfo, pid, threadHandle, tid, context, getBugstarConfigCallbackAddr, 0, (bool valid1, UInt64 bugstarConfigAddr) =>
						{
							// Don't warn about a failure in the callback here though, wait till the
							// RaiseBug near the end bug handling so that it is more prominent in the logs.
							string bugstarConfigXml = null;
							if(valid1)
							{
								GetMemoryAscii(pinfo, bugstarConfigAddr, out bugstarConfigXml);
							}

							TtyOutput("\n**** BEGIN EXCEPTION DUMP\n\n");
							BeginCaptureTtyOutput();

							TtyOutput("Thread id:       "+tid+"\n");
							TtyOutput("Thread name:     "+name+"\n\n");
							PrintContextRegisters("", context);
							TtyOutput("\n");
							PrintThreadStack("", processHandle, threadHandle, context);
							TtyOutput("\n");

							bugDescription = EndCaptureTtyOutput();

							var dmp = TriggerCoredump(pinfo, pid, tid, exceptionRecord, context);

							ExecuteTargetCallback(pinfo, pid, threadHandle, tid, context, targetCrashCallbackAddr, 1, (valid2) =>
							{
								if(!valid2)
								{
									TtyOutput("Failure executing "+TARGET_CRASH_CALLBACK_BASE_NAME+"(1)\n\n");
								}

								if(bugstarConfigXml != null)
								{
									RaiseBug(pinfo, m_BugSummary, bugDescription, m_EnableBugDupCheck, bugstarConfigXml, dmp);
								}
								else
								{
									TtyOutput("Unable to fetch bugstar configuration from game, please manually create a bug.\n");
								}

								TtyOutput("\n**** END EXCEPTION DUMP\n");

								m_BugSummary = null;
								m_EnableBugDupCheck = true;
								m_ContinueDebugEvent = 0;
							});
						});
					});
				});
			}
		}

		////////////////////////////////////////////////////////////////////////
		// PCTarget::HandleDebugEvent
		////////////////////////////////////////////////////////////////////////
		private void HandleDebugEvent(uint timeoutMs)
		{
			Win32.DEBUG_EVENT debugEvent;
			if(Win32.WaitForDebugEvent(out debugEvent, timeoutMs))
			{
				var pid = debugEvent.processId;
				var tid = debugEvent.threadId;
				m_ContinueDebugEvent = Win32.DBG_CONTINUE;
				switch(debugEvent.debugEventCode)
				{
					case Win32.CREATE_PROCESS_DEBUG_EVENT:
					{
						TtyOutput("Process "+pid+" started\n");

						ProcessInfo pinfo;
						if(TryGetProcessInfo(pid, out pinfo))
						{
							ThreadInfo tinfo;
							TryGetThreadInfo(pinfo, pid, tid, out tinfo);
						}

						var exeNameBuf = new char[512];
						UInt32 strlen = (UInt32)exeNameBuf.Length;
						if(!Win32.QueryFullProcessImageName(pinfo.m_Handle, 0, exeNameBuf, ref strlen))
						{
							Trace.WriteLine("GetProcessImageFileName failed, err=0x"+Marshal.GetLastWin32Error().ToString("x8"));
						}
						else
						{
							pinfo.m_ExeName = new string(exeNameBuf, 0, (int)strlen);
						}

						// Calling SymInitialize here with debugEvent.u.createProcessInfo.process always fails.  See LOAD_DLL_DEBUG_EVENT.

						break;
					}

					case Win32.CREATE_THREAD_DEBUG_EVENT:
					{
						ThreadInfo tinfo;
						TryGetThreadInfo(pid, tid, out tinfo);
						break;
					}

					case Win32.EXCEPTION_DEBUG_EVENT:
					{
						ProcessInfo pinfo;
						if(TryGetProcessInfo(pid, out pinfo))
						{
							bool runCrashHandler = true;
							var exception = debugEvent.u.exception;
							var exceptionRecord = exception.exceptionRecord;
							var exceptionCode = exceptionRecord.exceptionCode;
							var exceptionAddress = (UInt64)exceptionRecord.exceptionAddress;
							//TtyOutput("EXCEPTION_DEBUG_EVENT, 0x"+exceptionCode.ToString("x8")+"\n");

							// If this process is waiting for the completion of the target
							// crash callback, then process that.
							StopNotificationCallback snc;
							if(m_OnStopNotificationCallback.TryGetValue(pid, out snc))
							{
								m_OnStopNotificationCallback.Remove(pid);
								bool callbackValid = true;
								if(exceptionAddress != 0)
								{
									TtyOutput("Registered target callback failed.  Target stopped unexpectedly at 0x"+exceptionAddress.ToString("x16")+"\n");
									ThreadInfo tinfo;
									if(TryGetThreadInfo(pinfo, pid, tid, out tinfo))
									{
										PrintThreadStack("", pinfo, tinfo);
									}
									callbackValid = false;
								}
								snc.callback(callbackValid);
								runCrashHandler = false;
							}
							else
							{
								switch(exceptionCode)
								{
									case Win32.EXCEPTION_BREAKPOINT:
									{
										bool isTmCommand = false;
										var buf = new byte[8];
										if(GetMemory(pinfo, exceptionAddress, ref buf))
										{
											if(buf[0]==0xcc && buf[1]==0xeb &&
											   buf[3]==(byte)'R' && buf[4]==(byte)'*' && buf[5]==(byte)'T' && buf[6]==(byte)'M')
											{
												if(HandleTmCommand(pinfo, pid, tid, exceptionAddress, buf[7], ref runCrashHandler))
												{
													isTmCommand = true;
												}
											}
										}

										// If this wasn't a target manager command, check if it a known
										// hardcoded Windows breakpoint that we want to ignore.
										if(!isTmCommand)
										{
											var sym = GetSymbol(pinfo.m_Handle, exceptionAddress);
											// When a process is first launched from R*TM, a breakpoint in CsrSetPriorityClass is hit.
											if(sym == "ntdll.dll!CsrSetPriorityClass")
											{
												runCrashHandler = false;
											}
											// When an external process is first attached to by R*TM, a breakpoint in DbgBreakPoint is hit.
											else if(sym == "ntdll.dll!DbgBreakPoint")
											{
												runCrashHandler = false;
											}
										}
										break;
									}

									case Win32.MS_VC_EXCEPTION:
									{
										ulong qw0, qw1, qw2;
										unsafe
										{
											qw0 = exceptionRecord.exceptionInformation[0];
											qw1 = exceptionRecord.exceptionInformation[1];
											qw2 = exceptionRecord.exceptionInformation[2];
										}
										Win32.THREADNAME_INFO tnInfo;
										tnInfo.type     = (UInt32)(qw0&0xffffffff);
										tnInfo.name     = (IntPtr)qw1;
										tnInfo.threadId = (UInt32)(qw2&0xffffffff);
										tnInfo.flags    = (UInt32)(qw2>>32);
										var threadId = tnInfo.threadId!=0xffffffff ? tnInfo.threadId : tid;
										string name;
										if(GetMemoryAscii(pinfo, (UInt64)tnInfo.name, out name))
										{
											TtyOutput("Thread id "+threadId+", name=\""+name+"\"\n");
											ThreadInfo tinfo;
											if(TryGetThreadInfo(pinfo, pid, tid, out tinfo))
											{
												m_Processes[pid].m_Threads[tid].m_Name = name;
											}
										}
										runCrashHandler = false;
										break;
									}

									// Ignore the following exceptions
									case Win32.STATUS_WX86_BREAKPOINT:
									{
										runCrashHandler = false;
										break;
									}

									default:
									{
										if(exception.firstChance != 0)
										{
											runCrashHandler = false;
											m_ContinueDebugEvent = Win32.DBG_EXCEPTION_NOT_HANDLED;
										}
										else
										{
											switch(exceptionCode)
											{
												// C++ exception, "Exception msc", http://support.microsoft.com/kb/185294
												case 0xe06d7363:
												{
													TtyOutput("Unhandled C++ exception.\n");
													break;
												}

												default:
												{
													TtyOutput("Unhandled exception 0x"+exceptionCode.ToString("x8")+".\n");
													break;
												}
											}

											runCrashHandler = true;
										}
										break;
									}
								}
							}

							if(runCrashHandler)
							{
								CrashHandler(pid, tid, exceptionRecord);
							}
						}
						break;
					}

					case Win32.EXIT_PROCESS_DEBUG_EVENT:
					{
						ProcessInfo pinfo;
						if(TryGetProcessInfo(pid, out pinfo))
						{
							if(!Win32.SymCleanup(pinfo.m_Handle))
							{
							}
						}

						CloseProcessHandles(pid);
						TtyOutput("Process "+pid+" closed with exit code "+debugEvent.u.exitProcess.exitCode+"\n");
						break;
					}

					case Win32.EXIT_THREAD_DEBUG_EVENT:
					{
						ProcessInfo pinfo;
						if(TryGetProcessInfo(pid, out pinfo))
						{
							ThreadInfo tinfo;
							if(pinfo.m_Threads.TryGetValue(tid, out tinfo))
							{
								var exitCode = debugEvent.u.exitThread.exitCode;
								if(tinfo.m_Name != null)
								{
									TtyOutput("Thread "+tid+" (\""+tinfo.m_Name+"\"), has exitted with code 0x"+exitCode.ToString("x8")+"\n");
								}
								else
								{
									TtyOutput("Thread "+tid+", has exitted with code 0x"+exitCode.ToString("x8")+"\n");
								}
								pinfo.m_Threads.Remove(tid);
								m_Processes[pid] = pinfo;
							}
						}
						break;
					}

					case Win32.LOAD_DLL_DEBUG_EVENT:
					{
						// Calling SymInitialize fails until the (2nd?) LOAD_DLL_DEBUG_EVENT when process is launched with DEBUG_PROCESS.
						// Calling earlier after the CreateProcess call or in CREATE_PROCESS_DEBUG_EVENT does not work.
						// See http://stackoverflow.com/questions/7687230/problems-with-opening-a-process-with-debug-flags
						ProcessInfo pinfo;
						if(TryGetProcessInfo(pid, out pinfo))
						{
							if(!pinfo.m_SymbolsInitialized)
							{
								if(Win32.SymInitialize(pinfo.m_Handle, null, true))
								{
									pinfo.m_SymbolsInitialized = true;
									m_Processes[pid] = pinfo;

									// Once SymInitialize works, modules are in a loaded (enough) state that we can
									// start the async symbol lookup.
									StartAsyncSymbolLookup(pinfo, pid);
								}
								else
								{
									Trace.WriteLine("SymInitialize err=0x"+Marshal.GetLastWin32Error().ToString("x8"));
								}
							}
							else
							{
								if(!Win32.SymRefreshModuleList(pinfo.m_Handle))
								{
									Trace.WriteLine("SymRefreshModuleList err=0x"+Marshal.GetLastWin32Error().ToString("x8"));
								}
							}
						}
						break;
					}

					case Win32.OUTPUT_DEBUG_STRING_EVENT:
					{
						ProcessInfo pinfo;
						if(TryGetProcessInfo(pid, out pinfo))
						{
							var strlenPlus1 = debugEvent.u.debugString.debugStringLength;
							if(strlenPlus1 > 0)
							{
								var strlen = strlenPlus1-1;
								int bufSize;
								Encoding encoding;
								if(debugEvent.u.debugString.unicode != 0)
								{
									bufSize = strlen*2;
									encoding = Encoding.Unicode;
								}
								else
								{
									bufSize = strlen;
									encoding = Encoding.ASCII;
								}
								var buf = new byte[bufSize];
								if(GetMemory(pinfo, (UInt64)debugEvent.u.debugString.debugStringData, ref buf))
								{
									// For performance reasons, as soon as we've read the process memory,
									// let the game continue running before we do the tty output.
									Win32.ContinueDebugEvent(pid, tid, Win32.DBG_CONTINUE);
									m_ContinueDebugEvent = 0;
									TtyOutput(encoding.GetString(buf));
								}
							}
						}
						break;
					}

					case Win32.RIP_EVENT:
					{
						// TODO ???
						break;
					}

					case Win32.UNLOAD_DLL_DEBUG_EVENT:
					{
						break;
					}
				}

				if(m_ContinueDebugEvent != 0)
				{
					Win32.ContinueDebugEvent(pid, tid, m_ContinueDebugEvent);
				}
			}
		}

		////////////////////////////////////////////////////////////////////////
		// PCTarget::CheckTargetCallbackTimeout
		////////////////////////////////////////////////////////////////////////
		private void CheckTargetCallbackTimeout()
		{
			var utcNow = DateTime.UtcNow;
			bool timedout;
			do
			{
				timedout = false;
				VoidCallbackBool timedoutCallback = null;
				foreach(var e in m_OnStopNotificationCallback)
				{
					if(utcNow > e.Value.utcTimeout)
					{
						m_OnStopNotificationCallback.Remove(e.Key);

						// If the other threads are all suspended, then
						// it is possible that the callback has just
						// deadlocked.  Resume the other threads and try
						// again.
						if(e.Value.resumeThreads != null)
						{
							TtyOutput("First attempt at running target callback timed out, resuming all threads in case of deadlock.\n");
							StopNotificationCallback snc;
							snc.callback      = e.Value.callback;
							snc.utcTimeout    = utcNow.AddSeconds(10);
							snc.resumeThreads = null;
							m_OnStopNotificationCallback.Add(e.Key, snc);
							e.Value.resumeThreads();
						}

						// Otherwise if we have already retried with the
						// other threads resumed, then the callback has
						// failed.
						else
						{
							TtyOutput("Second attempt at running target callback timed out, failing callback.\n");
							timedoutCallback = e.Value.callback;
						}

						timedout = true;
						break;
					}
				}
				if(timedoutCallback != null)
				{
					timedoutCallback(false);
				}
			}
			while(timedout);
		}

		////////////////////////////////////////////////////////////////////////
		// PCTarget::DebuggerThread
		////////////////////////////////////////////////////////////////////////
		private void DebuggerThread()
		{
			while(!m_DebuggerThreadExit)
			{
				const uint pollIntervalMs = 100;
				HandleDebugEvent(pollIntervalMs);

				CheckTargetCallbackTimeout();

				// Process any callback from other threads
				var callback = Interlocked.Exchange(ref m_DebuggerThreadCallback, null);
				if(callback != null)
				{
					callback();
				}
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////
	// PCTargetManager
	////////////////////////////////////////////////////////////////////////////
	public class PCTargetManager : IDisposable
	{
		private List<PCTarget> m_Targets = new List<PCTarget>();
		private Dictionary<string, PCTarget> m_TargetsByName = new Dictionary<string, PCTarget>();
		private NetworkTtyListener m_NetworkTtyListener;


		////////////////////////////////////////////////////////////////////////
		// PCTargetManager::PCTargetManager
		////////////////////////////////////////////////////////////////////////
		public PCTargetManager(NetworkTtyListener networkTtyListener)
		{
			m_NetworkTtyListener = networkTtyListener;

			Win32.SymSetOptions(Win32.SYMOPT_LOAD_LINES);
		}

		////////////////////////////////////////////////////////////////////////
		// PCTargetManager::~PCTargetManager
		////////////////////////////////////////////////////////////////////////
		~PCTargetManager()
		{
			Dispose(false);
		}

		////////////////////////////////////////////////////////////////////////
		// PCTargetManager::Dispose
		////////////////////////////////////////////////////////////////////////
		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		////////////////////////////////////////////////////////////////////////
		// PCTargetManager::Dispose
		////////////////////////////////////////////////////////////////////////
		protected virtual void Dispose(bool disposing)
		{
			if(disposing)
			{
				if(m_Targets != null)
				{
					foreach(PCTarget t in m_Targets)
					{
						t.Dispose();
					}
				}
			}
		}

		////////////////////////////////////////////////////////////////////////
		// PCTargetManager::GetDefaultTargetNames
		////////////////////////////////////////////////////////////////////////
		public string[] GetDefaultTargetNames()
		{
			var ret = new string[1];
			ret[0] = HostPcIpAddr.GetStr();     // currently no support for remote targets
			return ret;
		}

		////////////////////////////////////////////////////////////////////////
		// PCTargetManager::RegisterTargetAsTtyListener
		////////////////////////////////////////////////////////////////////////
		internal void RegisterTargetAsTtyListener(PCTarget t)
		{
			lock(this)
			{
				if(!m_NetworkTtyListener.IsRegistered(t))
				{
					m_NetworkTtyListener.Register(t.IpAddr, t);
					t.TtyOutput("Registered system IP " + t.IpAddr + " as a TTY listener.\n");
				}
			}
		}

		////////////////////////////////////////////////////////////////////////
		// PCTargetManager::AddTarget
		////////////////////////////////////////////////////////////////////////
		public Target AddTarget(string name)
		{
			// Currently no remote targets supported.  Due to the way the
			// workspaces work, an old IP address could be passed in here if the
			// PC is assigned a new address.  If that happens, just ignore it.
			if(name != HostPcIpAddr.GetStr())
			{
				return null;
			}

			lock(this)
			{
				PCTarget target = null;
				if(!m_TargetsByName.TryGetValue(name, out target))
				{
					target = new PCTarget(name, "localhost", this);
					m_Targets.Add(target);
					m_TargetsByName.Add(name, target);
				}

				return target;
			}
		}
	}
}
