using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using System.Diagnostics;
using System.IO;
using Rockstar.LogJam;

namespace Rockstar.TargetManager
{
    public enum WorkspaceEventType
    {
        WorkspaceChanged,
        MasterConfigChanged,
        GlobalCommandLineChanged,
        LocalCommandLineChanged,
        EnabledChanged
    }

    public class WorkspaceEventArgs : EventArgs
    {
        WorkspaceEventType m_EventType;
        TargetPlatformId m_PlatformId = TargetPlatformId.Unknown;
        string m_TargetName = "";

        public WorkspaceEventArgs(WorkspaceEventType eventType)
        {
            m_EventType = eventType;
        }

        public WorkspaceEventArgs(WorkspaceEventType eventType,
                                TargetPlatformId platformId)
        {
            m_EventType = eventType;
            m_PlatformId = platformId;
        }

        public WorkspaceEventArgs(WorkspaceEventType eventType,
                                string targetName)
        {
            m_EventType = eventType;
            m_TargetName = string.IsNullOrEmpty(targetName) ? "" : targetName;
        }

        public WorkspaceEventType EventType
        {
            get{return m_EventType;}
        }

        public TargetPlatformId PlatformId
        {
            get{return m_PlatformId;}
        }

        public string TargetName
        {
            get{return m_TargetName;}
        }
    }

    public class Platform
    {
        static readonly Configuration DEFAULT_CONFIG = new Configuration("***");

        TargetPlatformId m_PlatformId = TargetPlatformId.Unknown;
        SortedDictionary<string, Configuration> m_Configs = new SortedDictionary<string, Configuration>();
        Configuration m_MasterConfig = DEFAULT_CONFIG;
        string m_CommandLine = "";

        public Platform()
        {
        }

        internal Platform(TargetPlatformId platformId)
        {
            m_PlatformId = platformId;
        }

        public Configuration GetConfig(string name)
        {
            Configuration config;

            if(!m_Configs.TryGetValue(name, out config))
            {
                config = null;
            }

            return config;
        }

        public TargetPlatformId PlatformId
        {
            get{return m_PlatformId;}
        }

        public IEnumerable<Configuration> Configurations
        {
            get{return m_Configs.Values;}
        }

        public int NumConfigs
        {
            get{return m_Configs.Count;}
        }

        public Configuration MasterConfig
        {
            get{return m_MasterConfig;}
            internal set
            {
                if(null != value)
                {
                    m_MasterConfig = value;
                }
            }
        }

        public string CommandLine
        {
            get{return m_CommandLine;}
            internal set
            {
                if(null != value)
                {
                    m_CommandLine = value;
                }
            }
        }

        public void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("Platform");

            writer.WriteElementString("Name", Configuration.PlatformNameFromId(this.PlatformId));
            writer.WriteElementString("MasterConfig", this.MasterConfig.Name);
            writer.WriteElementString("CommandLine", this.CommandLine);

            writer.WriteStartElement("Configs");

            XmlSerializer s = new XmlSerializer(typeof(Configuration));
            foreach(Configuration c in this.Configurations)
            {
                s.Serialize(writer, c);
            }

            writer.WriteEndElement();   //Configs
            writer.WriteEndElement();   //Platform
        }

        public void ReadXml(XmlReader reader)
        {
            reader.ReadStartElement("Platform");

            string platName = reader.ReadElementString("Name");
            m_PlatformId = Configuration.PlatformIdFromName(platName);

            string masterConfigName = reader.ReadElementString("MasterConfig");

            m_CommandLine = reader.ReadElementString("CommandLine");

            List<Configuration> configs = new List<Configuration>();

            reader.MoveToContent();
            bool isEmpty = reader.IsEmptyElement;
            reader.ReadStartElement("Configs");
            if(!isEmpty)
            {
                XmlSerializer s = new XmlSerializer(typeof(Configuration));
                while(reader.IsStartElement())
                {
                    Configuration c = (Configuration) s.Deserialize(reader);
                    System.Diagnostics.Debug.Assert(c.PlatformId == m_PlatformId);
                    configs.Add(c);
                }
                reader.ReadEndElement();    //Configs
            }
            reader.ReadEndElement();    //Platform

            this.SetConfigs(configs);

            m_MasterConfig = this.GetConfig(masterConfigName);
            if(null == m_MasterConfig)
            {
                m_MasterConfig = DEFAULT_CONFIG;
            }
        }

        public XmlSchema GetSchema()
        {
            return null;
        }

        internal void AddConfig(Configuration config)
        {
            if(null != config)
            {
                Debug.Assert(config.PlatformId == m_PlatformId);

                if(!m_Configs.ContainsKey(config.Name))
                {
                    m_Configs.Add(config.Name, config);
                }
                else
                {
                    Debug.Assert(!m_Configs.ContainsKey(config.Name));
                }
            }
        }

        internal void SetConfigs(IEnumerable<Configuration> configs)
        {
            m_Configs.Clear();
            m_MasterConfig = DEFAULT_CONFIG;

            foreach(Configuration c in configs)
            {
                this.AddConfig(c);
            }
        }
    }

    public class PlatformCollection
    {
        Dictionary<TargetPlatformId, Platform> m_Platforms;

        internal PlatformCollection()
        {
            m_Platforms = new Dictionary<TargetPlatformId,Platform>();

            //Add an entry for each platform defined in the enum.
            foreach(TargetPlatformId platformId in Enum.GetValues(typeof(TargetPlatformId)))
            {
                if(TargetPlatformId.Unknown == platformId)
                {
                    continue;
                }

                m_Platforms.Add(platformId, new Platform(platformId));
            }
        }

        public bool TryGetValue(TargetPlatformId id, out Platform platform)
        {
            return m_Platforms.TryGetValue(id, out platform);
        }

        public IEnumerator GetEnumerator()
        {
            return m_Platforms.Values.GetEnumerator();
        }

        public Platform this[TargetPlatformId platformId]
        {
            get
            {
                return m_Platforms[platformId];
            }

            internal set
            {
                m_Platforms[platformId] = value;
            }
        }
    }

    public class LocalCommandLine
    {
        string m_TargetName;
        string m_CmdLine = "";
        bool m_AppendToGlobal = true;
        Workspace m_Workspace;

        internal LocalCommandLine(string name, Workspace ws)
        {
            m_TargetName = name;
            m_Workspace = ws;
        }

        public void Set(string cmdLine)
        {
            if(null != cmdLine && cmdLine != m_CmdLine)
            {
                m_CmdLine = cmdLine;
                WorkspaceEventArgs args =
                    new WorkspaceEventArgs(WorkspaceEventType.LocalCommandLineChanged, TargetName);
                m_Workspace.Event.Invoke(args);
                m_Workspace.IsDirty = true;
            }
        }

        public bool AppendToGlobal
        {
            get{return m_AppendToGlobal;}
            set
            {
                if(value != m_AppendToGlobal)
                {
                    m_AppendToGlobal = value;
                    WorkspaceEventArgs args =
                        new WorkspaceEventArgs(WorkspaceEventType.LocalCommandLineChanged, TargetName);
                    m_Workspace.Event.Invoke(args);
                    m_Workspace.IsDirty = true;
                }
            }
        }

        public override string ToString()
        {
            return m_CmdLine;
        }

        public static implicit operator string(LocalCommandLine lcl)
        {
            return lcl.ToString();
        }

        internal string TargetName
        {
            get{return m_TargetName;}
        }
    }

    public class LocalTargetSettings
    {
        string m_TargetName;
		string m_Platform;
        LocalCommandLine m_LocalCmdLine;
        Workspace m_Workspace;
        bool m_enabled = true;

        internal LocalTargetSettings(string name, Workspace ws)
        {
            m_TargetName = name;
            m_LocalCmdLine = new LocalCommandLine(name, ws);
            m_Workspace = ws;
        }

        public LocalCommandLine CommandLine
        {
            get{return m_LocalCmdLine;}
        }

		public string Platform
		{
			get { return m_Platform; }
			set { m_Platform = value; }
		}

        public bool Enabled
        {
            get { return m_enabled;  }
            set
            {
                m_enabled = value;
                WorkspaceEventArgs args = new WorkspaceEventArgs(WorkspaceEventType.EnabledChanged, TargetName);
                m_Workspace.Event.Invoke(args);
                m_Workspace.IsDirty = true;
            }
        }

        internal string TargetName
        {
            get{return m_TargetName;}
        }
    }

    public class LocalSettings
    {
        Workspace m_Workspace;
        Dictionary<string, LocalTargetSettings> m_Settings = new Dictionary<string, LocalTargetSettings>();
        public LocalSettings(Workspace ws)
        {
            m_Workspace = ws;
        }

        public IEnumerator GetEnumerator()
        {
            return m_Settings.Values.GetEnumerator();
        }

		public LocalTargetSettings this[string name]
		{
			get
			{
				LocalTargetSettings lts;

				if (!m_Settings.TryGetValue(name, out lts))
				{
					lts = new LocalTargetSettings(name, m_Workspace);
					m_Settings.Add(name, lts);
				}

				return lts;
			}
		}

		public bool Remove(string name)
		{
			return m_Settings.Remove(name);
		}

        internal void Set(string name, string cmdLine)
        {
            LocalTargetSettings lts = this[name];

            lts.CommandLine.Set(cmdLine);
        }
    }

    public class Workspace : IXmlSerializable
    {
        PlatformCollection m_Platforms = new PlatformCollection();
        LocalSettings m_LocalSettings;
        LogJam.WeakEvent<WorkspaceEventArgs> m_Event;
        bool m_IsDirty = false;
        bool m_HasBeenSavedLeastOnce = false;

        //Parameterless constructor to permit serialization.
        public Workspace()
        {
            m_Event = new WeakEvent<WorkspaceEventArgs>(this);
            m_LocalSettings = new LocalSettings(this);
        }

        public bool Save(string filename)
        {
            bool success = false;

            TextWriter w = null;

            try
            {
                XmlSerializer s = new XmlSerializer(this.GetType());
                w = new StreamWriter(filename);
                s.Serialize(w, this);

                this.SaveLocalSettings(Path.GetDirectoryName(filename));

                IsDirty = false;
                HasBeenSavedAtLeastOnce = true;

                success = true;
            }
            catch (System.Exception e)
            {
                Trace.WriteLine(e.ToString());
            }
            finally
            {
                if(null != w){w.Close();}
            }

            return success;
        }

        public bool Load(string filename)
        {
            bool success = false;

            if(Path.GetExtension(filename) == ".rtmws")
            {
                XmlReader xr = null;

                try
                {
                    xr = XmlReader.Create(filename);
                    xr.ReadStartElement("Workspace");
                    this.ReadXml(xr);

                    this.LoadLocalSettings(Path.GetDirectoryName(filename));

                    foreach(TargetPlatformId platformId in Enum.GetValues(typeof(TargetPlatformId)))
                    {
                        if(TargetPlatformId.Unknown == platformId)
                        {
                            continue;
                        }

                        WorkspaceEventArgs args;

                        args = new WorkspaceEventArgs(WorkspaceEventType.MasterConfigChanged,
                                                    platformId);

                        this.DispatchEvent(args);

                        args = new WorkspaceEventArgs(WorkspaceEventType.GlobalCommandLineChanged,
                                                    platformId);

                        this.DispatchEvent(args);
                    }

                    IsDirty = false;
                    HasBeenSavedAtLeastOnce = true;

                    success = true;
                }
                catch (System.Exception e)
                {
                    Trace.WriteLine(e.ToString());
                }
                finally
                {
                    if(null != xr){xr.Close();}
                }
            }
            else
            {
                success = this.Create(filename);
            }

            return success;
        }

        public void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("Platforms");
            foreach(Platform p in m_Platforms)
            {
                p.WriteXml(writer);
            }
            writer.WriteEndElement();   //Platforms

            IsDirty = false;
        }

        public void ReadXml(XmlReader reader)
        {
            m_Platforms = new PlatformCollection();

            reader.ReadStartElement("Platforms");
            while(reader.IsStartElement())
            {
                Platform p = new Platform();
                p.ReadXml(reader);

                m_Platforms[p.PlatformId] = p;
            }
            reader.ReadEndElement();    //Platforms

            reader.ReadEndElement();    //Workspace

            IsDirty = false;
            HasBeenSavedAtLeastOnce = true;
        }

        public XmlSchema GetSchema()
        {
            return null;
        }

        public void SetConfigs(IEnumerable<Configuration> configs)
        {
            PlatformCollection oldPi = m_Platforms;

            m_Platforms = new PlatformCollection();

            foreach(Configuration config in configs)
            {
                Platform p;
                if(m_Platforms.TryGetValue(config.PlatformId, out p))
                {
                    p.AddConfig(config);
                    IsDirty = true;
                }
            }

            //Preserve the command line and master config settings.
            foreach(Platform p in oldPi)
            {
                Platform tmp;
                if(m_Platforms.TryGetValue(p.PlatformId, out tmp))
                {
                    this.SetCommandLine(p.PlatformId, p.CommandLine);
                    this.SetMasterConfig(p.PlatformId, p.MasterConfig.Name);
                }
            }
        }

        public void SetMasterConfig(TargetPlatformId platformId, string name)
        {
            Platform p = m_Platforms[platformId];

            if(null != p
                && !string.IsNullOrEmpty(name)
                && p.MasterConfig.Name != name)
            {
                p.MasterConfig = p.GetConfig(name);

                this.DispatchEvent(new WorkspaceEventArgs(WorkspaceEventType.MasterConfigChanged,
                                                        platformId));

                IsDirty = true;
            }
        }

        public void SetCommandLine(TargetPlatformId platformId, string cmdLine)
        {
            Platform p = m_Platforms[platformId];

            if(null != p
                && null != cmdLine
                && p.CommandLine != cmdLine)
            {
                p.CommandLine = cmdLine;

                this.DispatchEvent(new WorkspaceEventArgs(WorkspaceEventType.GlobalCommandLineChanged,
                                                        platformId));

                IsDirty = true;
            }
        }

        public LogJam.WeakEvent<WorkspaceEventArgs> Event
        {
            get{return m_Event;}
        }

        public PlatformCollection Platforms
        {
            get{return m_Platforms;}
        }

        public LocalSettings LocalSettings
        {
            get{return m_LocalSettings;}
        }

        public bool IsDirty
        {
            get{return m_IsDirty;}
            internal set
            {
                m_IsDirty = value;

                if(m_IsDirty)
                {
                    this.DispatchEvent(new WorkspaceEventArgs(WorkspaceEventType.WorkspaceChanged));
                }
            }
        }

        public bool HasBeenSavedAtLeastOnce
        {
            get{return m_HasBeenSavedLeastOnce;}
            set
            {
                m_HasBeenSavedLeastOnce = value;

                this.DispatchEvent(new WorkspaceEventArgs(WorkspaceEventType.WorkspaceChanged));
            }
        }

        bool Create(string slnFileName)
        {
            bool success = false;

            string filename =
                Path.GetDirectoryName(slnFileName)
                + "\\"
                + Path.GetFileNameWithoutExtension(slnFileName)
                + ".rtmws";

            IEnumerable<Configuration> configs =
                VisualStudioSolution.LoadSolution(slnFileName);

            if(null != configs)
            {
                this.SetConfigs(configs);

                success = true;
            }

            return success;
        }

        const string LOCAL_SETTINGS_FILE_NAME = "RTMLocalSettings.xml";
        void LoadLocalSettings(string dir)
        {
            try
            {
                //See if there is a local settings file
                string lsfPath;

                if (dir.Length == 0)
                    lsfPath = LOCAL_SETTINGS_FILE_NAME;
                else
                    lsfPath = dir + Path.DirectorySeparatorChar + LOCAL_SETTINGS_FILE_NAME;

                Trace.WriteLine("Loading Local Settings: " + lsfPath + "...");

                FileInfo finfo = new FileInfo(lsfPath);
                if(!finfo.Exists)
                {
                    Trace.WriteLine("Failed to load local settings from " + lsfPath);
                    return;
                }

                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(lsfPath);

                XmlNodeList xboxList = xmlDoc.GetElementsByTagName("Target");

                XmlNodeList targetList = xmlDoc.GetElementsByTagName("Target");
                for (int i = 0; i < targetList.Count; ++i)
                {
                    string nameAttr = targetList[i].Attributes.GetNamedItem("Name").Value;
                    string enabledAttr = targetList[i].Attributes.GetNamedItem("Enabled").Value;
                    string cmdLnAttr = targetList[i].Attributes.GetNamedItem("CommandLine").Value;
                    string appendToGlobalAttr = targetList[i].Attributes.GetNamedItem("AppendToGlobal").Value;
					XmlNode platformAttrXml = targetList[i].Attributes.GetNamedItem("Platform");

					string platformAttr = "";
					if (platformAttrXml != null)
					{
						// This attribute was added later, so it's optional
						platformAttr = platformAttrXml.Value;
					}
					m_LocalSettings[nameAttr].Platform = platformAttr;

                    if (cmdLnAttr.Length > 0)
                    {
                        m_LocalSettings[nameAttr].CommandLine.Set(cmdLnAttr);
                    } 
                    if(enabledAttr.Length > 0)
                    {
                        m_LocalSettings[nameAttr].Enabled = Boolean.Parse(enabledAttr);
                    }
                    bool appendToGlobal;
                    if (appendToGlobalAttr.Length > 0
                        && bool.TryParse(appendToGlobalAttr, out appendToGlobal))
                    {
                        m_LocalSettings[nameAttr].CommandLine.AppendToGlobal = appendToGlobal;
                    } 
                }
            }
            catch(System.Exception ex)
            {
                Trace.WriteLine(ex.ToString());
            }
        }

        void SaveLocalSettings(string dir)
        {
            try
            {
                string lsfPath = dir + Path.DirectorySeparatorChar + LOCAL_SETTINGS_FILE_NAME;

                Trace.WriteLine("Saving Local Settings " + lsfPath + "...");

                XmlTextWriter w = new XmlTextWriter(lsfPath,null);
                w.Formatting = Formatting.Indented;
                w.WriteStartDocument(false);
                w.WriteStartElement("LocalSettings");

                foreach(LocalTargetSettings lts in m_LocalSettings)
                {
                    w.WriteStartElement("Target");
                    w.WriteAttributeString("Name", lts.TargetName);
					w.WriteAttributeString("Platform", lts.Platform);
                    w.WriteAttributeString("Enabled", lts.Enabled.ToString());
                    w.WriteAttributeString("CommandLine", lts.CommandLine);
                    w.WriteAttributeString("AppendToGlobal", lts.CommandLine.AppendToGlobal.ToString());
                    w.WriteEndElement();
                }

                w.WriteEndElement();
                w.Flush();
                w.Close();
            }
            catch(System.Exception ex)
            {
                Trace.WriteLine(ex.ToString());
            }
        }

        void DispatchEvent(WorkspaceEventArgs args)
        {
            Event.Invoke(args);
        }
    }
}
