using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing.Imaging;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.IO.Pipes;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;
using XTF=Microsoft.Xbox.XTF;
using Microsoft.VisualBasic;

namespace Rockstar.TargetManager
{
	////////////////////////////////////////////////////////////////////////////
	// XB1Target
	////////////////////////////////////////////////////////////////////////////
	public class XB1Target : Target
	{
		private string m_IpAddr;    // currently the same as m_Name, but that may change in the future so keep seperate to make things clearer
		private string m_Name;
		private string m_Alias;
		private XB1TargetManager m_Tm;
		private PartitionConnection m_PartitionMain;
		private PartitionConnection m_PartitionTitle;
		private KdProcess m_KdProcess;
		private XTF.IXtfApplicationClient m_ApplicationClient;
		private Object m_LockObj = new Object();
		private delegate void VoidCallbackVoid();
		private delegate void VoidCallbackUInt32(UInt32 val);
		private delegate void VoidCallbackUInt64(UInt64 val);
		private delegate void VoidCallbackString(string val);
		private delegate void VoidCallbackString2(string val0, string val1);
		private delegate void VoidCallbackString3(string val0, string val1, string val2);
		private delegate void VoidCallbackStringArray(string[] val);
		private VoidCallbackVoid m_WorkerThreadCallback;
		private bool m_StartNewLog = false;
		private enum KitType { UNKNOWN, TEST, DEV };
		private KitType m_KitType = KitType.UNKNOWN;
		private bool m_CheckKitTypeThreadExit = false;
		private Thread m_CheckKitTypeThread;
		private TestKitTtyParser m_TestKitTtyParser = null;
		private string m_PackageFullName = "";
		private string m_Pdb = "";

		private const uint XTFSHUTDOWN_REBOOT = 1; // can't seem to find this flag defined anywhere :/

		private static string XDK_SYMPATH = @"%DurangoXDK%xdk\symbols";


		[DllImport("XtfApplication.dll", CharSet=CharSet.Unicode)]
		private static extern Int32 XtfCreateApplicationClient(string address, ref Guid riid, out XTF.IXtfApplicationClient ppvObject);

		[DllImport("XtfConsoleControl.dll", CharSet=CharSet.Unicode)]
		private static extern Int32 XtfCreateConsoleControlClient(string address, ref Guid riid, out XTF.IXtfConsoleControlClient ppvObject);

		[DllImport("XtfFileIO.dll", CharSet=CharSet.Unicode)]
		private static extern Int32 XtfCreateFileIOClient(string address, ref Guid riid, out XTF.IXtfFileIOClient ppvObject);


		////////////////////////////////////////////////////////////////////////
		// XB1Target::XB1Target
		////////////////////////////////////////////////////////////////////////
		internal XB1Target(XTF._XTFCONSOLEDATA console, XB1TargetManager tm, IntPtr jobObject, bool enable)
		{
			m_IpAddr = console.bstrAddress;
			m_Name   = console.bstrAddress;
			m_Alias  = console.bstrAlias;
			m_Tm     = tm;
			m_PartitionMain  = new PartitionConnection(this, m_IpAddr);
			m_PartitionTitle = new PartitionConnection(this, m_IpAddr+"/title");
			m_KdProcess = new KdProcess(this, m_IpAddr, jobObject);

			// Spawn a worker thread to determine what type of kit this is.
			// This is way too slow a process to do synchronously here, and it
			// can fail if the kit is currently turned off.
			m_CheckKitTypeThread = new Thread(CheckKitTypeWorkerThread);
			m_CheckKitTypeThread.Name = "XB1 CheckKitType";
			m_CheckKitTypeThread.Start();

			// Default enable to false so that this won't create a KD process
			// that could try to attach to someone else's devkit.  This default
			// is overriden by the --xb1=<ipaddr> command line argument.
			Enabled = enable;
		}

		////////////////////////////////////////////////////////////////////////
		// XB1Target::CheckKitTypeWorkerThread
		////////////////////////////////////////////////////////////////////////
		private void CheckKitTypeWorkerThread()
		{
			Debug.Assert(m_KitType == KitType.UNKNOWN);
			while(!m_CheckKitTypeThreadExit)
			{
				// Create process info checking xbDiagInfo
				ProcessStartInfo startInfo = new ProcessStartInfo();
				startInfo.FileName = Path.Combine(Environment.ExpandEnvironmentVariables("%DurangoXDK%"), "bin", "xbDiagInfo.exe");
				startInfo.Arguments = String.Format("/x {0}", m_IpAddr);
				startInfo.UseShellExecute = false;
				startInfo.RedirectStandardOutput = true;
				startInfo.CreateNoWindow = true;

				// actually create & launch the process
				Process proc = new Process();
				proc.StartInfo = startInfo;

				bool launchSuccess;
				try
				{
					launchSuccess = proc.Start();
				}
				catch(Exception)
				{
					Trace.WriteLine("Warning: Failed to launch " + startInfo.FileName);
					launchSuccess = false;
				}

				// if the process succeeded...
				if(launchSuccess)
				{
					// Read all data from process output.  This is done
					// asynchronously so that we can also poll the thread exit
					// flag.
					var resultDataTask = proc.StandardOutput.ReadToEndAsync();

					// Wait for the process to exit.
					while(!proc.WaitForExit(100))
					{
						if(m_CheckKitTypeThreadExit)
						{
							try { proc.CancelOutputRead(); } catch {}
							try { proc.Kill();    		   } catch {}
							return;
						}
					}

					var resultData = resultDataTask.Result.Trim();

					// Executing xbDiagInfo on a kit that's powered off just returns "Diagnostics:\r\n" (after an extremely long time).
					if(String.IsNullOrEmpty(resultData) || resultData.Length <= 32)
					{
						// Loop and try again.
					}

	   				// All the devkits have a CCat2 and CCat3 entry, but testkits don't
	   				else if(resultData.Contains("CCat2") && resultData.Contains("CCat3"))
	   				{
	   					Trace.WriteLine("XB1 at " + m_IpAddr + " is a devkit.");
	   					m_KitType = KitType.DEV;
	   					return;
	   				}

					else
					{
						Trace.WriteLine("XB1 at " + m_IpAddr + " is a testkit.");
						m_KitType = KitType.TEST;
						return;
					}
				}
			}
		}

		////////////////////////////////////////////////////////////////////////
		// XB1Target::Dispose
		////////////////////////////////////////////////////////////////////////
		protected override void Dispose(bool disposing)
		{
			// These should already have been cleaned up in ShutdownFromThread
			Debug.Assert(m_KdProcess == null);
			Debug.Assert(m_PartitionMain  == null);
			Debug.Assert(m_PartitionTitle == null);
			Debug.Assert(m_ApplicationClient == null);
			Debug.Assert(m_CheckKitTypeThread == null);
		}

		////////////////////////////////////////////////////////////////////////
		// XB1Target::SelectedIcon
		////////////////////////////////////////////////////////////////////////
		public override Icon SelectedIcon
		{
			get
			{
				if(m_PartitionMain==null || !m_PartitionMain.IsConnected || m_KitType==KitType.UNKNOWN)
				{
					return Icon.NOT_AVAILABLE;
				}
				else if(m_PartitionTitle==null || !m_PartitionTitle.IsConnected)
				{
					return Icon.NOT_RUNNING;
				}
				else
				{
					return Icon.RUNNING;
				}
			}
		}

		////////////////////////////////////////////////////////////////////////
		// XB1Target::IsPdbOnTarget
		////////////////////////////////////////////////////////////////////////
		private bool IsPdbOnTarget()
		{
			return Regex.IsMatch(m_Pdb, "^[xX][^:]:");
		}

		////////////////////////////////////////////////////////////////////////
		// XB1Target::TmCmdConfig
		////////////////////////////////////////////////////////////////////////
		internal void TmCmdConfig(string config, out List<string> kdCommands)
		{
			kdCommands = new List<string>();

			// Some of these commands that may be generated (eg. .sympath+),
			// cannot be all executed on one line seperated by a semicolon.     So
			// instead build up a list of commands, then execute them one at a
			// time.
			if(config != "")
			{
				var doc = XDocument.Parse(config);
				XElement el;

				var sympath = ".sympath "+XDK_SYMPATH;

				// Process any game specified pdb path
				el = doc.Root.XPathSelectElement("/config/pdb");
				if(el != null)
				{
					var pdb = el.Value.Replace('/', '\\');
					m_Pdb = pdb;

					// If pdb is not on the console, add its directory to the symbol path.
					if(!IsPdbOnTarget())
					{
						sympath += ";"+Path.GetDirectoryName(pdb);
					}
				}

				// Process any full package name
				el = doc.Root.XPathSelectElement("/config/fullpackagename");
				if(el != null)
				{
					m_PackageFullName = el.Value;
				}

				// Process any game specified symbol server
				el = doc.Root.XPathSelectElement("/config/symbol_server");
				if(el != null)
				{
					// Check if this symbol server is already specified in
					// _NT_SYMBOL_PATH with a cache directory.  If it is, then
					// keep that.
					var cacheDir = Path.Combine(Path.GetTempPath(), "symbol_cache");
					var ntSymbolPath = Environment.GetEnvironmentVariable("_NT_SYMBOL_PATH");
					if(ntSymbolPath != null)
					{
						var regex = new Regex(
							@"^(.*;)?srv\*([^*]+)\*"+el.Value.Replace("\\","\\\\")+@"\\?(;.*)?$",
							RegexOptions.IgnoreCase);
						if(regex.IsMatch(ntSymbolPath))
						{
							cacheDir = regex.Replace(ntSymbolPath, "$2");
						}
					}
					sympath += ";srv*"+cacheDir+"*"+el.Value;
				}
				kdCommands.Add(sympath);
				kdCommands.Add(".reload /f");
			}
		}

		////////////////////////////////////////////////////////////////////////
		// XB1Target::FileCopyCallback
		////////////////////////////////////////////////////////////////////////
		private class FileCopyCallback : XTF.IXtfCopyFileCallback
		{
			private VoidCallbackUInt32 m_Progress;
			private uint m_NextProgress = 10;

			////////////////////////////////////////////////////////////////////
			// XB1Target::FileCopyCallback::FileCopyCallback
			////////////////////////////////////////////////////////////////////
			internal FileCopyCallback(VoidCallbackUInt32 progress)
			{
				m_Progress = progress;
			}

			////////////////////////////////////////////////////////////////////
			// XB1Target::FileCopyCallback::OnStartFileCopy
			////////////////////////////////////////////////////////////////////
			public void OnStartFileCopy(string rootDirectory, string searchPattern, ref Microsoft.Xbox.XTF.XTFFILEINFO srcFileInfo, string dstFileName)
			{
			}

			////////////////////////////////////////////////////////////////////
			// XB1Target::FileCopyCallback::OnFileCopyProgress
			////////////////////////////////////////////////////////////////////
			public void OnFileCopyProgress(string srcFileName, string dstFileName, ulong fileSize, ulong bytesCopied)
			{
				double percentDbl = (double)(bytesCopied*100) / (double)fileSize;
				uint percentUint = (uint)percentDbl;
				while(m_NextProgress <= percentUint)
				{
					m_Progress(m_NextProgress);
					m_NextProgress += 10;
				}
			}

			////////////////////////////////////////////////////////////////////
			// XB1Target::FileCopyCallback::OnEndFileCopy
			////////////////////////////////////////////////////////////////////
			public void OnEndFileCopy(string rootDirectory, string searchPattern, ref Microsoft.Xbox.XTF.XTFFILEINFO srcFileInfo, string dstFileName, int hrErrorCode)
			{
			}
		}

		////////////////////////////////////////////////////////////////////////
		// XB1Target::CopyFileFromTarget
		////////////////////////////////////////////////////////////////////////
		private bool CopyOrMoveFileFromTarget(XTF.IXtfFileIOClient fileIOClient, string src, string dst, bool deleteSrc)
		{
			Debug.Assert(fileIOClient != null);
			const UInt32 includeAttributes = ~0u;
			const UInt32 excludeAttributes = 0;
			const UInt32 recursionLevels = 1;
			const UInt32 flags = 0;
			string copyMsg = "Copying \"" + src + "\" -> \"" + dst + "\"";
			Trace.WriteLine(copyMsg + "...");
			TtyOutput(copyMsg + "\n");
			try
			{
				fileIOClient.CopyFiles(src, includeAttributes, excludeAttributes, recursionLevels, dst, flags,
					new FileCopyCallback((uint percent) =>
						{
							Trace.WriteLine("Copy progress: " + percent.ToString()+"%");
							TtyOutput("... " + percent.ToString() + "%\n");
						}));
				Trace.WriteLine("File copy complete.");
				TtyOutput("... done\n");

				if(deleteSrc)
				{
					TtyOutput("Deleting \""+src+"\"\n");
					fileIOClient.DeleteFiles(src, includeAttributes, excludeAttributes, recursionLevels, flags, null);
				}
				return true;
			}
			catch(COMException e)
			{
				TtyOutput("Error: "+e.Message+", HRESULT=0x"+e.HResult.ToString("x8")+": "+m_Tm.FormatError(e.HResult)+"\n");
			}
			catch(Exception e)
			{
				TtyOutput("Error: "+e.Message+"\n");
			}
			return false;
		}

		////////////////////////////////////////////////////////////////////////
		// XB1Target::CopyFileFromTarget
		////////////////////////////////////////////////////////////////////////
		private bool CopyFileFromTarget(XTF.IXtfFileIOClient fileIOClient, string src, string dst)
		{
			const bool deleteSrc = false;
			return CopyOrMoveFileFromTarget(fileIOClient, src, dst, deleteSrc);
		}

		////////////////////////////////////////////////////////////////////////
		// XB1Target::MoveFileFromTarget
		////////////////////////////////////////////////////////////////////////
		private bool MoveFileFromTarget(XTF.IXtfFileIOClient fileIOClient, string src, string dst)
		{
			const bool deleteSrc = true;
			return CopyOrMoveFileFromTarget(fileIOClient, src, dst, deleteSrc);
		}

		////////////////////////////////////////////////////////////////////////
		// XB1Target::RaiseBug
		////////////////////////////////////////////////////////////////////////
		private void RaiseBug(string summary, string description, bool enableBugDupCheck, string bugstarConfigXml, string coredump, string xhitFilename)
		{
			Bugstar.Config config = null;
			if(bugstarConfigXml != "")
			{
				config = Bugstar.Config.FromString(bugstarConfigXml);
			}

			var filesToCopy = new List<string>();
			if(coredump != "")
			{
				string crashDmpDir = RockstarTargetManager.GetCrashdumpDir();

				string pdbSrc=null, pdbDst=null, pdbZip=null;
				string mapSrc=null, mapDst=null, mapZip=null;
				if(IsPdbOnTarget())
				{
					pdbSrc = m_Pdb;
					pdbDst = Path.Combine(crashDmpDir, Regex.Replace(
						coredump, @"^.*\\([^\\]+-[0-9]{8}-[0-9]{6}-[0-9]+)\.dmp$", "$1.pdb"));
					pdbZip = pdbDst+"*"+Path.GetFileName(pdbSrc);

					// .map file handled the same as the .pdb
					mapSrc = Path.ChangeExtension(pdbSrc, ".map");
					mapDst = Path.ChangeExtension(pdbDst, ".map");
					mapZip = mapDst+"*"+Path.GetFileName(mapSrc);
				}

				// Move the coredump (and xhit file if specified) off the console and onto the host PC
				string dmpSrc = "x"+coredump;
				string dmpDst = Path.Combine(crashDmpDir, Path.GetFileName(coredump));
				string xhitSrc = null;
				string xhitDst = null;
				if(!String.IsNullOrWhiteSpace(xhitFilename))
				{
					xhitSrc = "x"+xhitFilename;
					xhitDst = Path.Combine(crashDmpDir, Path.GetFileName(xhitFilename));
				}
				XTF.IXtfFileIOClient fileIOClient;
				Guid iid = Marshal.GenerateGuidForType(typeof(XTF.IXtfFileIOClient));
				Int32 hresult = XtfCreateFileIOClient(m_IpAddr+"/title", ref iid, out fileIOClient);
				if(hresult < 0)
				{
					Trace.WriteLine("XtfCreateFileIOClient("+m_IpAddr+") failed, HRESULT=0x"+hresult.ToString("x8")+": "+m_Tm.FormatError(hresult));
					Debug.Assert(fileIOClient == null);
				}
				else
				{
					try
					{
						if(MoveFileFromTarget(fileIOClient, dmpSrc, dmpDst))
						{
							filesToCopy.Add(dmpDst);
						}
						if(xhitSrc!=null && MoveFileFromTarget(fileIOClient, xhitSrc, xhitDst))
						{
							filesToCopy.Add(xhitDst);
						}
						if(m_Pdb != "")
						{
							if(pdbSrc!=null)
							{
								if(CopyFileFromTarget(fileIOClient, pdbSrc, pdbDst))
								{
									filesToCopy.Add(pdbZip);
								}
								if(CopyFileFromTarget(fileIOClient, mapSrc, mapDst))
								{
									filesToCopy.Add(mapZip);
								}
							}
							else
							{
								filesToCopy.Add(m_Pdb);
								filesToCopy.Add(Path.ChangeExtension(m_Pdb, ".map"));
							}
						}
					}
					finally
					{
						Marshal.ReleaseComObject(fileIOClient);
					}
				}
			}

			if(config != null)
			{
				BaseRaiseBug(config, summary, description, filesToCopy.ToArray(), enableBugDupCheck);
			}
			else
			{
				TtyOutput("Unable to read Bugstar configuration from the game to automatically log a bug.  Please create bug manually\n");
			}
		}

		////////////////////////////////////////////////////////////////////////
		// XB1Target::PartitionConnection
		////////////////////////////////////////////////////////////////////////
		private class PartitionConnection
		{
			private  XB1Target                      m_Target;
			private  string                         m_Address;
			internal XTF.IXtfConsoleControlClient   m_ConsoleControlClient = null;
			private  volatile bool                  m_IsConnected = false;


			////////////////////////////////////////////////////////////////////
			// XB1Target::PartitionConnection::PartitionConnection
			////////////////////////////////////////////////////////////////////
			internal PartitionConnection(XB1Target target, string address)
			{
				m_Target  = target;
				m_Address = address;
			}

			////////////////////////////////////////////////////////////////////
			// XB1Target::PartitionConnection::~PartitionConnection
			////////////////////////////////////////////////////////////////////
			~PartitionConnection()
			{
				// These should have been disconnected before the object is finalized
				Debug.Assert(m_ConsoleControlClient == null);
				Debug.Assert(!m_IsConnected);
			}

			////////////////////////////////////////////////////////////////////
			// XB1Target::PartitionConnection::Update
			////////////////////////////////////////////////////////////////////
			internal bool Update()
			{
				Int32 hresult;
				if(m_ConsoleControlClient == null)
				{
					Debug.Assert(!m_IsConnected);
					Guid iid = Marshal.GenerateGuidForType(typeof(XTF.IXtfConsoleControlClient));
					if((hresult = XtfCreateConsoleControlClient(m_Address, ref iid, out m_ConsoleControlClient)) < 0)
					{
						Trace.WriteLine("XtfCreateConsoleControlClient("+m_Address+") failed, HRESULT=0x"+hresult.ToString("x8")+": "+m_Target.m_Tm.FormatError(hresult));
						Debug.Assert(m_ConsoleControlClient == null);
						return false;
					}
					Debug.Assert(m_ConsoleControlClient != null);
				}

				// Do a ping check (with 1s timeout) before doing a full check
				// (much longer timeout).  This helps a little, but not as much
				// as you'd hope.  An "off" devkit still responds to a ping, so
				// we still get stuck in
				// IXtfConsoleControlClient.GetRunningProcesses for a long time.
				// Still needs to be a fairly long timeout though, since the
				// consoles do stop responding every now and then.
				if(new Ping().Send(m_Target.m_IpAddr, 1000).Status != IPStatus.Success)
				{
					m_IsConnected = false;
					return false;
				}

				// Grab the current list of processes.  This can throw when the
				// connection is lost (eg. when the console is powered off).
				// Currently this is really just used as a way to poll for
				// connection lost.
				List<XTF.XTFPROCESSINFO> curr = new List<XTF.XTFPROCESSINFO>();
				try
				{
					m_ConsoleControlClient.GetRunningProcesses(new GetRunningProcessCallback(
						(ref XTF.XTFPROCESSINFO p) =>
						{
							curr.Add(p);
						}));
				}
				catch//(Exception e)
				{
					//Trace.WriteLine("IXtfConsoleControlClient("+m_Address+").GetRunningProcesses threw \""+e.ToString()+"\"");
					m_IsConnected = false;
					return false;
				}

				m_IsConnected = true;
				return true;
			}

			////////////////////////////////////////////////////////////////////
			// XB1Target::PartitionConnection::IsConnected
			////////////////////////////////////////////////////////////////////
			internal bool IsConnected
			{
				get{return m_IsConnected;}
			}

			////////////////////////////////////////////////////////////////////
			// XB1Target::PartitionConnection::Disconnect
			////////////////////////////////////////////////////////////////////
			internal void Disconnect()
			{
				if(m_ConsoleControlClient != null)
				{
					Marshal.ReleaseComObject(m_ConsoleControlClient);
					m_ConsoleControlClient = null;
					m_IsConnected = false;
				}
			}

			////////////////////////////////////////////////////////////////////
			// XB1Target::PartitionConnection::GetRunningProcessCallback
			////////////////////////////////////////////////////////////////////
			private class GetRunningProcessCallback : XTF.IXtfRunningProcessCallback
			{
				internal delegate void Callback(ref XTF.XTFPROCESSINFO processData);
				private Callback m_Callback;
				internal GetRunningProcessCallback(Callback callback) { m_Callback = callback; }
				public void OnFoundProcess(ref XTF.XTFPROCESSINFO processData) { m_Callback(ref processData); }
			}
		}

		////////////////////////////////////////////////////////////////////////
		// XB1Target::KdProcess
		////////////////////////////////////////////////////////////////////////
		private class KdProcess : IDisposable
		{
			private XB1Target m_Target;
			private Thread m_WorkerThread;
			private volatile bool m_WorkerThreadExit;
			private delegate void VoidCallbackVoid();
			private VoidCallbackVoid m_OnLineReadCallback = null;
			private VoidCallbackVoid m_OnPromptCallback = null;
			private string m_Process = "";
			private HashSet<string> m_RecordedThreads = new HashSet<string>();
			internal volatile bool m_RebootRequested = false;
			private string m_BugSummary;
			private string m_XhitFilename = "";
			private bool m_EnableBugDupCheck = true;
			private string m_Line;
			private bool m_IsPrompt;
			private string m_NextCommand = null;


			internal enum StopState
			{
				GO,
				REQUEST_STOP,
				STOPPING,
				STOPPED,
				REQUEST_GO,
			};
			internal volatile StopState m_StopState = StopState.GO;


			// This is lame, System.Diagnostics.Process nearly does what we
			// want, but the fact that the stderr and stdout streams are
			// blocking screws everything up.  And
			// BeginOutputReadLine/BeginErrorReadLine is no good, since we miss
			// the prompt if we need to read complete lines.
			//
			// Basically it all comes down to needing to call PeekNamedPipe.
			//

			////////////////////////////////////////////////////////////////////
			// XB1Target::KdProcess::KdProcess
			////////////////////////////////////////////////////////////////////
			internal KdProcess(XB1Target target, string ipAddr, IntPtr jobObject)
			{
				m_Target = target;

				m_WorkerThreadExit = false;
				m_WorkerThread = new Thread(()=>WorkerThread(ipAddr, jobObject));
				m_WorkerThread.Name = "XB1 KD "+ipAddr;
				m_WorkerThread.Start();
			}

			////////////////////////////////////////////////////////////////////
			// XB1Target::KdProcess::~KdProcess
			////////////////////////////////////////////////////////////////////
			~KdProcess()
			{
				Dispose(false);
			}

			////////////////////////////////////////////////////////////////////
			// XB1Target::KdProcess::Dispose
			////////////////////////////////////////////////////////////////////
			public void Dispose()
			{
				Dispose(true);
				GC.SuppressFinalize(this);
			}

			////////////////////////////////////////////////////////////////////
			// XB1Target::KdProcess::ExecuteCommand
			////////////////////////////////////////////////////////////////////
			public void ExecuteCommand(string cmd)
			{
				m_NextCommand = cmd;
			}

			////////////////////////////////////////////////////////////////////
			// XB1Target::KdProcess::Dispose
			////////////////////////////////////////////////////////////////////
			protected virtual void Dispose(bool disposing)
			{
				if(disposing)
				{
					if(m_WorkerThread != null)
					{
						m_WorkerThreadExit = true;
						m_WorkerThread.Join();
					}
				}
			}

			////////////////////////////////////////////////////////////////////
			// XB1Target::KdProcess::Win32ProcessWrapper
			////////////////////////////////////////////////////////////////////
			private class Win32ProcessWrapper : IDisposable
			{
				private IntPtr m_StdinRd,  m_StdinWr;
				private IntPtr m_StdoutRd, m_StdoutWr;
				private IntPtr m_StderrRd, m_StderrWr;
				private Win32.PROCESS_INFORMATION m_ProcessInformation;


				////////////////////////////////////////////////////////////////
				// XB1Target::KdProcess::Win32ProcessWrapper::Win32ProcessWrapper
				////////////////////////////////////////////////////////////////
				internal Win32ProcessWrapper(string ipAddr, IntPtr jobObject)
				{
					try
					{
						// Create pipes for stdin, stdout and stderr
						var pipeSecurityAttributes = new Win32.SECURITY_ATTRIBUTES();
						pipeSecurityAttributes.length = Marshal.SizeOf(pipeSecurityAttributes);
						pipeSecurityAttributes.inheritHandle = true;
						if(!Win32.CreatePipe(out m_StdinRd,  out m_StdinWr,  ref pipeSecurityAttributes, 0))
						{
							Trace.WriteLine("CreatePipe failed, err=0x"+Marshal.GetLastWin32Error().ToString("x8"));
							throw new Exception();
						}
						if(!Win32.CreatePipe(out m_StdoutRd, out m_StdoutWr, ref pipeSecurityAttributes, 0))
						{
							Trace.WriteLine("CreatePipe failed, err=0x"+Marshal.GetLastWin32Error().ToString("x8"));
							throw new Exception();
						}
						if(!Win32.CreatePipe(out m_StderrRd, out m_StderrWr, ref pipeSecurityAttributes, 0))
						{
							Trace.WriteLine("CreatePipe failed, err=0x"+Marshal.GetLastWin32Error().ToString("x8"));
							throw new Exception();
						}

						// Build command line for executing KD
						string exeLocation = System.Reflection.Assembly.GetEntryAssembly().Location;
						string exeDir = System.IO.Path.GetDirectoryName(exeLocation);
						if(exeDir == null)
						{
							exeDir = System.IO.Path.GetPathRoot(exeLocation);
							Debug.Assert(exeDir != null);
						}
						exeDir.TrimEnd(new char[]{'\\'});
						string currentDirectory = exeDir;
						string applicationName = exeDir+"\\kd.exe";
						string commandLine = "\""+applicationName+"\" -k net:port=50039,target="+ipAddr/*+" -kqm"*/;

						// Spawn KD processes
						var startupInfo = new Win32.STARTUPINFO();
						startupInfo.cb = Marshal.SizeOf(startupInfo);
						startupInfo.flags = Win32.STARTF_USESHOWWINDOW | Win32.STARTF_USESTDHANDLES;
						startupInfo.showWindow = Win32.SW_HIDE;
						startupInfo.stdInput  = m_StdinRd;
						startupInfo.stdOutput = m_StdoutWr;
						startupInfo.stdError  = m_StderrWr;
						IntPtr processAttributes = IntPtr.Zero;
						IntPtr threadAttributes  = IntPtr.Zero;
						bool inheritHandles = true;
						const Int32 creationFlags = Win32.CREATE_NO_WINDOW;
						IntPtr environment = IntPtr.Zero;
						if(!Win32.CreateProcess(applicationName, commandLine, processAttributes, threadAttributes, inheritHandles, creationFlags, environment, currentDirectory, ref startupInfo, out m_ProcessInformation))
						{
							Trace.WriteLine("CreatePipe failed, err=0x"+Marshal.GetLastWin32Error().ToString("x8"));
							throw new Exception();
						}
						if(!Win32.AssignProcessToJobObject(jobObject, m_ProcessInformation.process))
						{
							Int32 err = Marshal.GetLastWin32Error();
							if(err == Win32.ERROR_ACCESS_DENIED)
							{
								Trace.WriteLine("AssignProcessToJobObject failed, ERROR_ACCESS_DENIED.  This is expected when launching from Visual Studio.");
							}
							else
							{
								Trace.WriteLine("AssignProcessToJobObject failed, err=0x"+err.ToString("x8"));
								throw new Exception();
							}
						}
						Win32.CloseHandle(m_ProcessInformation.thread);
					}
					catch(Exception e)
					{
						Dispose(true);
						throw e;
					}
				}

				////////////////////////////////////////////////////////////////
				// XB1Target::KdProcess::Win32ProcessWrapper::~Win32ProcessWrapper
				////////////////////////////////////////////////////////////////
				~Win32ProcessWrapper()
				{
					Dispose(false);
				}

				////////////////////////////////////////////////////////////////
				// XB1Target::KdProcess::Win32ProcessWrapper::Dispose
				////////////////////////////////////////////////////////////////
				public void Dispose()
				{
					Dispose(true);
					GC.SuppressFinalize(this);
				}

				////////////////////////////////////////////////////////////////
				// XB1Target::KdProcess::Win32ProcessWrapper::Dispose
				////////////////////////////////////////////////////////////////
				protected virtual void Dispose(bool disposing)
				{
					if(disposing)
					{
						if(m_ProcessInformation.process != IntPtr.Zero)
						{
							const uint exitCode = 0;
							if(!Win32.TerminateProcess(m_ProcessInformation.process, exitCode))
							{
								Trace.WriteLine("TerminateProcess failed, err=0x"+Marshal.GetLastWin32Error().ToString("x8"));
							}
							Win32.CloseHandle(m_ProcessInformation.process);
						}

						if(m_StdinRd  != IntPtr.Zero)   Win32.CloseHandle(m_StdinRd);
						if(m_StdinWr  != IntPtr.Zero)   Win32.CloseHandle(m_StdinWr);
						if(m_StdoutRd != IntPtr.Zero)   Win32.CloseHandle(m_StdoutRd);
						if(m_StdoutWr != IntPtr.Zero)   Win32.CloseHandle(m_StdoutWr);
						if(m_StderrRd != IntPtr.Zero)   Win32.CloseHandle(m_StderrRd);
						if(m_StderrWr != IntPtr.Zero)   Win32.CloseHandle(m_StderrWr);
					}
				}

				////////////////////////////////////////////////////////////////
				// XB1Target::KdProcess::Win32ProcessWrapper::Stdin
				////////////////////////////////////////////////////////////////
				internal IntPtr Stdin
				{
					get { return m_StdinWr; }
				}

				////////////////////////////////////////////////////////////////
				// XB1Target::KdProcess::Win32ProcessWrapper::Stdout
				////////////////////////////////////////////////////////////////
				internal IntPtr Stdout
				{
					get { return m_StdoutRd; }
				}

				////////////////////////////////////////////////////////////////
				// XB1Target::KdProcess::Win32ProcessWrapper::Stderr
				////////////////////////////////////////////////////////////////
				internal IntPtr Stderr
				{
					get { return m_StderrRd; }
				}

				////////////////////////////////////////////////////////////////
				// XB1Target::KdProcess::Win32ProcessWrapper::SendCtrlC
				////////////////////////////////////////////////////////////////
				internal bool SendCtrlC()
				{
					// Wow, Win32 API is lame.  Apparantly this is the best way to do this.
					// Based on idea at http://stanislavs.org/stopping-command-line-applications-programatically-with-ctrl-c-events-from-net/.
					// Sucks that this cannot be extended to other control key combos.  Ctrl-B would be useful for terminating KD.

					if(!Win32.AttachConsole(m_ProcessInformation.processId))
					{
						Trace.WriteLine("AttachConsole failed, err=0x"+Marshal.GetLastWin32Error().ToString("x8"));
						return false;
					}

					if(!Win32.SetConsoleCtrlHandler(IntPtr.Zero, true))
					{
						Trace.WriteLine("SetConsoleCtrlHandler(0,true) failed, err=0x"+Marshal.GetLastWin32Error().ToString("x8"));
						return false;
					}

					if(!Win32.GenerateConsoleCtrlEvent(Win32.CTRL_C_EVENT, 0))
					{
						Trace.WriteLine("GenerateConsoleCtrlEvent failed, err=0x"+Marshal.GetLastWin32Error().ToString("x8"));
						return false;
					}

					// Surely there is a better solution than this!?  But it
					// does seem necissary, as without it, occasionally when
					// attempting to stop the Durango console, R*TM will exit.
					Thread.Sleep(2000);

					if(!Win32.FreeConsole())
					{
						Trace.WriteLine("FreeConsole failed, err=0x"+Marshal.GetLastWin32Error().ToString("x8"));
						return false;
					}

					if(!Win32.SetConsoleCtrlHandler(IntPtr.Zero, false))
					{
						Trace.WriteLine("SetConsoleCtrlHandler(0,false) failed, err=0x"+Marshal.GetLastWin32Error().ToString("x8"));
						return false;
					}

					return true;
				}
			}

			////////////////////////////////////////////////////////////////////
			// XB1Target::KdProcess::StdoutReader
			////////////////////////////////////////////////////////////////////
			private class StdoutReader
			{
				private Regex m_PromptRegex = new Regex(@"^[0-9]+: kd\> $");

				private const Int32 BUF_SIZE = 1024;
				private byte[] m_Buf = new byte[BUF_SIZE];
				private Int32  m_BufBegin = 0;
				private Int32  m_BufEnd   = 0;
				private IntPtr m_Pipe;
				private bool   m_IsPrompt;
				private string m_Line;
				private bool   m_StartNewLine = true;

				////////////////////////////////////////////////////////////////
				// XB1Target::KdProcess::StdoutReader::StdoutReader
				////////////////////////////////////////////////////////////////
				internal StdoutReader(IntPtr stdoutRd)
				{
					m_Pipe = stdoutRd;
				}

				////////////////////////////////////////////////////////////////
				// XB1Target::KdProcess::StdoutReader::ReadLine
				////////////////////////////////////////////////////////////////
				internal string ReadLine()
				{
					if(m_StartNewLine)
					{
						m_Line = "";
						m_StartNewLine = false;
					}
					m_IsPrompt = false;

					for(;;)
					{
						// Buffer empty ?
						if(m_BufBegin >= m_BufEnd)
						{
							m_BufBegin = 0;
							m_BufEnd   = 0;

							// First determine how many bytes are available to
							// read from the pipe so that ReadFile will not
							// block.
							IntPtr buffer = IntPtr.Zero;
							Int32 bufferSize = 0;
							IntPtr bytesRead = IntPtr.Zero;
							Int32 totalBytesAvail;
							IntPtr bytesLeftThisMessage = IntPtr.Zero;
							if(!Win32.PeekNamedPipe(m_Pipe, buffer, bufferSize, bytesRead, out totalBytesAvail, bytesLeftThisMessage))
							{
								Trace.WriteLine("PeekNamedPipe failed, err=0x"+Marshal.GetLastWin32Error().ToString("x8"));
								totalBytesAvail = 0;
							}
							if(totalBytesAvail <= 0)
							{
								return null;
							}

							// Read as much data as is available in the pipe,
							// and will fit in out buffer.
							Int32 numberOfBytesToRead = Math.Min(BUF_SIZE, totalBytesAvail);
							Int32 numberOfBytesRead;
							IntPtr overlapped = IntPtr.Zero;
							if(!Win32.ReadFile(m_Pipe, m_Buf, numberOfBytesToRead, out numberOfBytesRead, overlapped))
							{
								Trace.WriteLine("ReadFile failed, err=0x"+Marshal.GetLastWin32Error().ToString("x8"));
								continue;
							}
							Debug.Assert(numberOfBytesRead == numberOfBytesToRead);
							m_BufEnd = numberOfBytesRead;
						}

						// While it seems wrong to assume that the pipes are 8-bit characters,
						// (a) there seems to be no way to create utf-16 pipes (???)
						// (b) even when kd is started from a unicode command shell (cmd /u),
						//     its output when redirected to a file is still 8-bits per char
						char c = (char)m_Buf[m_BufBegin++];
						m_Line += c;

						// If that's the end of a line break out of this loop so we can process it
						if(c == '\n')
						{
							break;
						}

						// If it is a prompt (which does not end in a newline), break out to process it
						if(m_PromptRegex.IsMatch(m_Line))
						{
							m_IsPrompt = true;
							break;
						}
					}
					m_StartNewLine = true;
					return m_Line;
				}

				////////////////////////////////////////////////////////////////
				// XB1Target::KdProcess::StdoutReader::IsPrompt
				////////////////////////////////////////////////////////////////
				internal bool IsPrompt
				{
					get { return m_IsPrompt; }
				}
			}

			////////////////////////////////////////////////////////////////////
			// XB1Target::KdProcess::WriteStringToPipe
			////////////////////////////////////////////////////////////////////
			private void WriteStringToPipe(IntPtr pipe, string str)
			{
				var buf = new byte[str.Length];
				Int32 ret = Win32.WideCharToMultiByte(Win32.CP_ACP, 0, str, str.Length, buf, str.Length, IntPtr.Zero, IntPtr.Zero);
				Debug.Assert(ret == str.Length);
				Int32 numberOfBytesWriten;
				if(!Win32.WriteFile(pipe, buf, str.Length, out numberOfBytesWriten, IntPtr.Zero))
				{
					Trace.WriteLine("WriteFile failed, err=0x"+Marshal.GetLastWin32Error().ToString("x8"));
				}
				Debug.Assert(numberOfBytesWriten == str.Length);

				// Echo to TTY
				m_Target.TtyOutput(str);
			}

			////////////////////////////////////////////////////////////////////
			// XB1Target::KdProcess::OnLineReadCallback
			////////////////////////////////////////////////////////////////////
			private VoidCallbackVoid OnLineReadCallback
			{
				get { return m_OnPromptCallback==null ? m_OnLineReadCallback : null; }
				set { m_OnLineReadCallback = value; m_OnPromptCallback = null; }
			}

			////////////////////////////////////////////////////////////////////
			// XB1Target::KdProcess::OnPromptCallback
			////////////////////////////////////////////////////////////////////
			private VoidCallbackVoid OnPromptCallback
			{
				get { return m_OnPromptCallback; }
				set { m_OnPromptCallback = value; m_OnLineReadCallback = ()=>{if(m_IsPrompt)value();}; }
			}

			////////////////////////////////////////////////////////////////////
			// XB1Target::KdProcess::StartNewProcess
			////////////////////////////////////////////////////////////////////
			internal void StartNewProcess()
			{
				m_Process = "";
				m_Target.m_Pdb = "";
				m_RecordedThreads.Clear();
				m_BugSummary = null;
				m_EnableBugDupCheck = true;
				m_XhitFilename = null;
			}

			////////////////////////////////////////////////////////////////////
			// XB1Target::KdProcess::ContinueFromBreakpoint
			////////////////////////////////////////////////////////////////////
			private class RegexReplace
			{
				internal Regex   regex;
				internal string  replace;

				internal RegexReplace(string _regex, string _replace)
				{
					regex = new Regex(_regex);
					replace = _replace;
				}
			}
			private void ContinueFromBreakpoint(Win32ProcessWrapper proc)
			{
				// This is a joke.  Simply executing "gh" should be enough, but
				// KD has a threading race condition with that.  It seems like
				// KD set a breakpoint by replacing the byte with 0xcc (int3).
				// When continuing past the breakpoint, it temporarily replaces
				// the overwriten byte, set RFLAGS.TF to single step, and
				// resumes execution.  When the single step exception is caught,
				// KD then puts back the breakpoint, clears RFLAGS.TF then
				// continues.  But this doesn't work correctly when multiple
				// threads are hitting the breakpoint simultaneously.
				//
				// One error condition is missed breakpoints, a second thread
				// can execute past the breakpoint while it is temporarily
				// disabled.  A second and even worse error condition is two
				// threads hitting the breakpoint, both threads setting
				// RFLAGS.TF, but KD seemingly only expecting one single step
				// exception.  The second exception then causes KD to stop at an
				// input prompt.
				//
				// To solve all this, we instead emulate the instruction at the
				// breakpoint location, so the instruction in memory never needs
				// to be modified.  We are only expecting the breakpoint to be
				// set in a function prologue here, so at least the number of
				// instructions that need to be emulated is limited.  This is
				// still very fragile to XDK updates, and may need further
				// instructions supported.
				//
				WriteStringToPipe(proc.Stdin, "u @rip L1\n");
				var regexes = new List<RegexReplace>();
				string prefix = @"^[0-9a-f]{8}`[0-9a-f]{8}\s+([0-9a-f]+)\s+";
				regexes.Add(new RegexReplace(
					prefix+"sub\\s+([a-z0-9]+),([0-9a-f]+)h\n$",
					"r$2=@$2-$3;"));
				regexes.Add(new RegexReplace(
					prefix+"push\\s+([a-z0-9]+)\n$",
					"rrsp=@rsp-8; eq @rsp @$2;"));
				string kdcmd = "";
				string x64bytes = "";
				OnLineReadCallback = () =>
				{
					if(!m_IsPrompt)
					{
						foreach(var rr in regexes)
						{
							if(rr.regex.IsMatch(m_Line))
							{
								kdcmd = rr.regex.Replace(m_Line, kdcmd+rr.replace);
								x64bytes = rr.regex.Replace(m_Line, x64bytes+"$1");
							}
						}
					}
					else
					{
						Debug.Assert(kdcmd    != "");
						Debug.Assert(x64bytes != "");
						WriteStringToPipe(proc.Stdin, "rrip=@rip+"+(x64bytes.Length/2)+"; "+kdcmd+" gh\n");
						OnLineReadCallback = null;
					}
				};
			}

			////////////////////////////////////////////////////////////////////
			// XB1Target::KdProcess::CommandThenFilterOutput
			////////////////////////////////////////////////////////////////////
			private void CommandThenFilterOutput(Win32ProcessWrapper proc, string command, string[] regex, VoidCallbackStringArray thenDo)
			{
				WriteStringToPipe(proc.Stdin, command+"\n");
				uint num = (uint)regex.Length;
				string[] results = new string[num];
				for(uint i=0; i<num; ++i)
				{
					results[i] = "";
				}
				OnLineReadCallback = () =>
				{
					if(!m_IsPrompt)
					{
						for(uint i=0; i<num; ++i)
						{
							results[i] += Regex.Replace(m_Line, "^(("+regex[i]+")|.*)\n$", "$3");
						}
					}
					else
					{
						thenDo(results);
					}
				};
			}

			////////////////////////////////////////////////////////////////////
			// XB1Target::KdProcess::CommandThenFilterOutput
			////////////////////////////////////////////////////////////////////
			private void CommandThenFilterOutput(Win32ProcessWrapper proc, string command, string regex, VoidCallbackString thenDo)
			{
				string[] regexes = {regex};
				CommandThenFilterOutput(proc, command, regexes, (results)=>{thenDo(results[0]);});
			}

			////////////////////////////////////////////////////////////////////
			// XB1Target::KdProcess::CommandThenFilterOutput
			////////////////////////////////////////////////////////////////////
			private void CommandThenFilterOutput(Win32ProcessWrapper proc, string command, string regex0, string regex1, VoidCallbackString2 thenDo)
			{
				string[] regexes = {regex0, regex1};
				CommandThenFilterOutput(proc, command, regexes, (results)=>{thenDo(results[0], results[1]);});
			}

			////////////////////////////////////////////////////////////////////
			// XB1Target::KdProcess::CommandThenFilterOutput
			////////////////////////////////////////////////////////////////////
			private void CommandThenFilterOutput(Win32ProcessWrapper proc, string command, string regex0, string regex1, string regex2, VoidCallbackString3 thenDo)
			{
				string[] regexes = {regex0, regex1, regex2};
				CommandThenFilterOutput(proc, command, regexes, (results)=>{thenDo(results[0], results[1], results[2]);});
			}

			////////////////////////////////////////////////////////////////////
			// XB1Target::KdProcess::HandleThreadBeginEnd
			////////////////////////////////////////////////////////////////////
			private void HandleThreadBeginEnd(Win32ProcessWrapper proc, bool begin)
			{
				CommandThenFilterOutput(proc,
					".thread",
					"Implicit thread is now (........`........)",
				(threadString) =>
				{
					if(threadString != "")
					{
						if(begin)
						{
							m_RecordedThreads.Add(threadString);
						}
						else
						{
							m_RecordedThreads.Remove(threadString);
						}
					}
					WriteStringToPipe(proc.Stdin, "rrip=@rip+1; gh\n");
					OnLineReadCallback = null;
				});
			}

			////////////////////////////////////////////////////////////////////
			// XB1Target::KdProcess::HandleTmCommand
			////////////////////////////////////////////////////////////////////
			private void HandleTmCommand(Win32ProcessWrapper proc)
			{
				// Extract the command number from the data
				TMCommands cmd = (TMCommands)Convert.ToUInt32(m_Line.Substring(40,2), 16);
				switch(cmd)
				{
					case TMCommands.NOP:
						OnPromptCallback = () =>
						{
							// Get the current process.  Useful for debugging when manually connecting with KD.
							CommandThenFilterOutput(proc,
								".process",
								"Implicit process is now (........`........)",
							(process) =>
							{
								m_Process = process;

								// Should kind of be unnecissary to clear
								// m_RecordedThreads here, as it should have really
								// been done by StartNewProcess.  But in case the
								// process start was missed (which is possible since
								// that is really messy the way it "works"), clear
								// out the recorder thread set again as it is always
								// safe to do so here.
								m_RecordedThreads.Clear();
								HandleThreadBeginEnd(proc, true);
							});
						};
						break;

					case TMCommands.PRINT_ALL_THREAD_STACKS:
						{
							// TODO: Once we are absolutely sure that everything
							// listed in m_RecordedThreads is still going to be
							// valid, then this loop can be simplified.
							var threadItor = m_RecordedThreads.GetEnumerator();
							bool threadItorValid = threadItor.MoveNext();
							var keepRecordedThreads = new HashSet<string>();
							uint state = 0;
							bool threadStillValid = false;
							OnLineReadCallback = () =>
							{
								if(m_IsPrompt)
								{
									bool again;
									do
									{
										again = false;
										if(threadItorValid)
										{
											switch(state)
											{
												case 0:
													// Try to switch thread context.
													WriteStringToPipe(proc.Stdin, ".thread "+threadItor.Current+"\n");
													threadStillValid = false;
													state = 1;
													break;

												case 1:
													// If the thread context was still valid, then
													// display the stack.  Also remember the thread so
													// that we keep it around for later (invalid threads
													// will get purged).
													if(threadStillValid)
													{
														keepRecordedThreads.Add(threadItor.Current);
														WriteStringToPipe(proc.Stdin, "kn\n");
													}
													// Otherwise go to the next thread.
													else
													{
														// Now that we are using ContinueFromBreakpoint(), there
														// should not really be any way for the thread to be
														// invalid (implies that we missed a
														// ntdll!RtlExitUserThread breakpoint).
														Debug.Assert(threadStillValid);
														again = true;
													}
													threadItorValid = threadItor.MoveNext();
													state = 0;
													break;
											}
										}
										else
										{
											WriteStringToPipe(proc.Stdin, ".thread; rrip=@rip+1; gh\n");
											m_RecordedThreads = keepRecordedThreads;
											OnLineReadCallback = null;
										}
									}
									while(again);
								}
								else
								{
									if(state == 1)
									{
										if(Regex.IsMatch(m_Line, "^Implicit thread is now [0-9a-f]{8}`[0-9a-f]{8}\n$"))
										{
											threadStillValid = true;
										}
									}
								}
							};
						}
						break;

					case TMCommands.STOP_NO_ERROR_REPORT:
						OnPromptCallback = () =>
						{
							m_Target.TtyOutput("\nsysTmCmdStopNoErrorReport recieved\n\n");
							WriteStringToPipe(proc.Stdin, ".reload; kn\n");
							OnPromptCallback = () =>
							{
								m_StopState = StopState.STOPPED;
								OnLineReadCallback = null;
							};
						};
						break;

					case TMCommands.QUITF:
						OnPromptCallback = () =>
						{
							CommandThenFilterOutput(proc,
								".if(@rcx!=0){.printf \"bugsummary: Quitf \\\"%ma\\\"\\n\",@rcx}",
								"bugsummary: (.+)",
							(bugSummary) =>
							{
								m_BugSummary = bugSummary;

								// sysTmCmdQuitf contains another __debugbreak, so
								// continuing on with the current __debugbreak handled,
								// means we hit the second one, which then triggers the in
								// game exception handler.  See sysTmCmdQuitf implementation
								// for more comments on why it is done this way.
								WriteStringToPipe(proc.Stdin, "rrip=@rip+1; gh\n");
								OnLineReadCallback = null;
							});
						};
						break;

					case TMCommands.GPU_HANG:
						m_BugSummary = "XB1 GPU Error";
						m_EnableBugDupCheck = false;
						OnPromptCallback = () =>
						{
							CommandThenFilterOutput(proc,
								".if(@rcx!=0){.printf \"xhitfilename: \\\"%mu\\\"\\n\",@rcx}",
								"xhitfilename: \"(.+)\"",
							(xhitFilename) =>
							{
								m_XhitFilename = xhitFilename;

								// Like sysTmCmdQuitf, there is another __debugbreak, so
								// continue on to that to trigger the exception handler.
								WriteStringToPipe(proc.Stdin, "rrip=@rip+1; gh\n");
								OnLineReadCallback = null;
							});
						};
						break;

					case TMCommands.CPU_HANG:
						m_BugSummary = "CPU Hang";
						m_EnableBugDupCheck = false;
						OnPromptCallback = () =>
						{
							// Like sysTmCmdQuitf, there is another __debugbreak, so
							// continue on to that to trigger the exception handler.
							WriteStringToPipe(proc.Stdin, "rrip=@rip+1; gh\n");
							OnLineReadCallback = null;
						};
						break;

					case TMCommands.EXCEPTION_HANDLER_BEGIN:
					case TMCommands.DURANGO_DEPRECATED_EXCEPTION_HANDLER_BEGIN:
						m_Target.BeginCaptureTtyOutput();
						OnPromptCallback = () =>
						{
							WriteStringToPipe(proc.Stdin, "rrip=@rip+1; gh\n");
							OnLineReadCallback = null;
						};
						break;

					case TMCommands.EXCEPTION_HANDLER_END:
					case TMCommands.DURANGO_DEPRECATED_EXCEPTION_HANDLER_END:
						OnPromptCallback = () =>
						{
							string bugDescription = m_Target.EndCaptureTtyOutput();
							bool isStandaloneDump = bugDescription == null;

							CommandThenFilterOutput(proc,
								// Print out data from the sysTmCmdExceptionHandlerEnd call
								// in a format that is simple to parse here
								".if(@rcx!=0){.printf \"coredump:      %ma\\n\",@rcx};"+
								".if(@rdx!=0){.printf \"bugstarconfig: %ma\\n\",@rdx}",

								"coredump:      (.+)",
								"bugstarconfig: (.+)",

							(coredump, bugstarConfig) =>
							{
								WriteStringToPipe(proc.Stdin, "rrip=@rip+1; gh\n");
								OnLineReadCallback = null;

								// RaiseBug must be after the gh so that the console is
								// running again to copy the coredump off of it.
								if(isStandaloneDump)
								{
									// Do this async, so the console can continue running. Otherwise, the systray connection
									// times out, and the game will halt.
									var bugThread = new Thread(() =>
										{
											m_Target.RaiseBug("Forced dump", "", m_EnableBugDupCheck, bugstarConfig, coredump, m_XhitFilename);
											m_EnableBugDupCheck = true;
										});
									bugThread.Name = "Bugstar async crashdump report";
									bugThread.Start();
								}
								else
								{
									m_Target.RaiseBug(m_BugSummary, bugDescription, m_EnableBugDupCheck, bugstarConfig, coredump, m_XhitFilename);
									m_EnableBugDupCheck = true;
								}
								m_BugSummary = null;
								m_XhitFilename = null;
							});
						};
						break;

					case TMCommands.CONFIG:
						OnPromptCallback = () =>
						{
							CommandThenFilterOutput(proc,
								".if(@rcx!=0){.printf \"config: %ma\\n\",@rcx}",
								"config: (.+)",
							(config) =>
							{
								List<string> cmds;
								m_Target.TmCmdConfig(config, out cmds);

								var cmdItor = cmds.GetEnumerator();
								OnPromptCallback = () =>
								{
									if(cmdItor.MoveNext())
									{
										WriteStringToPipe(proc.Stdin, cmdItor.Current+"\n");
									}
									else
									{
										WriteStringToPipe(proc.Stdin, "rrip=@rip+1; gh\n");
										OnLineReadCallback = null;
									}
								};
								OnPromptCallback();
							});
						};
						break;

					case TMCommands.CREATE_DUMP:
						// Should never occur on XB1; just continue
						OnPromptCallback = () =>
						{
							WriteStringToPipe(proc.Stdin, "rrip=@rip+1; gh\n");
							OnLineReadCallback = null;
						};
						break;

					case TMCommands.DURANGO_THREAD_BEGIN:
						OnPromptCallback = () =>
						{
							HandleThreadBeginEnd(proc, true);
						};
						break;

					case TMCommands.DURANGO_THREAD_END:
						OnPromptCallback = () =>
						{
							HandleThreadBeginEnd(proc, false);
						};
						break;

					case TMCommands.DURANGO_REPORT_GPU_HANG_HACK:
						OnPromptCallback = () =>
						{
							// The July 2014 XDK (including QFE1) has an infinite loop bug in
							// umd*!XhitPerformHangSummary.  Looks like the code is missing a volatile,
							// so compiler has optimized out the loads from memory.  Fixing the bug just
							// requires patching the jne offset so that ecx and edx are reloaded.
							//
							// See https://forums.xboxlive.com/AnswerPage.aspx?qid=ce9fa94e-936b-4bf4-8d92-98e55211815a&tgt=1
							// for more details.

							// 3: kd> u umd!XhitPerformHangSummary L80
							// umd!XhitPerformHangSummary [d:\9190\xbox\drivers\graphics\debug\xhit\umd.cpp @ 46]:
							// 00000106`5f9a951c 48895c2418      mov     qword ptr [rsp+18h],rbx
							// 00000106`5f9a9521 55              push    rbp
							// 00000106`5f9a9522 56              push    rsi
							// 00000106`5f9a9523 57              push    rdi
							// ...
							// # jne should be jumping back to here to reload ecx and edx
							// 00000106`5f9a9626 488b055b350a00  mov     rax,qword ptr [umd!g_HangAnalysisClientContext+0x8 (00000106`5fa4cb88)]
							// 00000106`5f9a962d 8b8840a00000    mov     ecx,dword ptr [rax+0A040h]
							// 00000106`5f9a9633 488b0546350a00  mov     rax,qword ptr [umd!g_HangAnalysisClientContext (00000106`5fa4cb80)]
							// 00000106`5f9a963a 8b9030140000    mov     edx,dword ptr [rax+1430h]
							// # but instead it is jumping to here, causing an infinite loop bug
							// 00000106`5f9a9640 3bca            cmp     ecx,edx
							// 00000106`5f9a9642 75fc            jne     umd!XhitPerformHangSummary+0x124 (00000106`5f9a9640)
							// ...

							// The four variants of this DLL have the same code, just the offsets to g_HangAnalysisClientContext are different

							// 3: kd> db /c1e umd!XhitPerformHangSummary+10A L1e
							// 00000106`5f9a9626  48 8b 05 5b 35 0a 00 8b-88 40 a0 00 00 48 8b 05 46 35 0a 00 8b 90 30 14 00 00 3b ca 75 fc  H..[5....@...H..F5....0...;.u.

							// 3: kd> db /c1e umd_i!XhitPerformHangSummary+10A L1e
							// 00000103`e5c02dc6  48 8b 05 d3 76 0b 00 8b-88 40 a0 00 00 48 8b 05 be 76 0b 00 8b 90 30 14 00 00 3b ca 75 fc  H...v....@...H...v....0...;.u.

							// 4: kd> db /c1e umd_v!XhitPerformHangSummary+10A L1e
							// 00000104`52c1a60a  48 8b 05 2f 02 0f 00 8b-88 40 a0 00 00 48 8b 05 1a 02 0f 00 8b 90 30 14 00 00 3b ca 75 fc  H../.....@...H........0...;.u.

							// 3: kd> db /c1e umd_d!XhitPerformHangSummary+10A L1e
							// 00000107`ae3e6d86  48 8b 05 53 7e 13 00 8b-88 40 a0 00 00 48 8b 05 3e 7e 13 00 8b 90 30 14 00 00 3b ca 75 fc  H..S~....@...H..>~....0...;.u.

							WriteStringToPipe(proc.Stdin, ".reload /f umd*; db /c1e XhitPerformHangSummary+10A L1e\n");
							bool isMatch = false;
							OnLineReadCallback = () =>
							{
								if(!m_IsPrompt)
								{
									// Look for the infinite loop bug.
									if(Regex.IsMatch(m_Line, "^........`........  48 8b 05 .. .. .. .. 8b-88 40 a0 00 00 48 8b 05 .. .. .. .. 8b 90 30 14 00 00 3b ca 75 fc  "))
									{
										isMatch = true;
									}
								}
								else
								{
									if(isMatch)
									{
										// Patch jump offset to fix bug.
										WriteStringToPipe(proc.Stdin, "eb XhitPerformHangSummary+127 e2; rrip=@rip+1; gh\n");
									}
									else
									{
										// Infinite loop bug not found.  Since we can't patch the bug, instead
										// replace the first byte of XhitPerformHangSummary with a retn.
										WriteStringToPipe(proc.Stdin, "eb XhitPerformHangSummary c3; rrip=@rip+1; gh\n");
										m_Target.TtyOutput("XhitPerformHangSummary does not match, unable to patch XDK bug\n");
									}
									OnLineReadCallback = null;
								}
							};
						};
						break;

					case TMCommands.DURANGO_GET_GPU_COMMAND_BUFFER_ACCESS:
						OnPromptCallback = () =>
						{
							CommandThenFilterOutput(proc,
								".reload /f umd*; lm m umd*",
								"........`........ ........`........   (umd_.) .*",
							(umd) =>
							{
								if(umd == "")
								{
									WriteStringToPipe(proc.Stdin, "rrax=0; rrip=@rip+1; gh\n");
									OnLineReadCallback = null;
								}
								else
								{
									CommandThenFilterOutput(proc,
										"?"+umd+"!TinyD3D::Device::StartNewSegment",
										"Evaluate expression: [0-9]+ = (........`........)",
									(startNewSegment) =>
									{
										CommandThenFilterOutput(proc,
											"dt "+umd+"!CD3DContext",
											"   \\+(0x020) m_TinyDevice     : D3D11XTinyDevice",
										(tinyDevice) =>
										{
											CommandThenFilterOutput(proc,
												"dt "+umd+"!TinyD3D::Device",
												"   \\+(0x.*) m_CeBuffer       : TinyD3D::DeviceCommandBuffer",
												"   \\+(0x.*) m_DeBuffer       : TinyD3D::DeviceCommandBuffer",
											(ceBuffer, deBuffer) =>
											{
												CommandThenFilterOutput(proc,
													"dt "+umd+"!TinyD3D::DeviceCommandBuffer",
													"   \\+(0x.*) m_pPos           : Ptr64 Uint4B",
													"   \\+(0x.*) m_pLimit         : Ptr64 Uint4B",
													"   \\+(0x.*) m_ActiveDwords   : Uint4B",
												(pos, limit, activeDwords) =>
												{
													if(tinyDevice=="" || deBuffer=="" || ceBuffer=="" || pos=="" || limit=="" || activeDwords=="")
													{
														WriteStringToPipe(proc.Stdin, "rrax=0; rrip=@rip+1; gh\n");
													}
													else
													{
														WriteStringToPipe(proc.Stdin,
															"eq @rcx "+
																tinyDevice+" "+
																tinyDevice+"+"+deBuffer+"+"+pos+" "+
																tinyDevice+"+"+deBuffer+"+"+limit+" "+
																tinyDevice+"+"+deBuffer+"+"+activeDwords+" "+
																tinyDevice+"+"+ceBuffer+"+"+pos+" "+
																tinyDevice+"+"+ceBuffer+"+"+limit+" "+
																tinyDevice+"+"+ceBuffer+"+"+activeDwords+" "+
																startNewSegment+";"+
															"rrax=1; rrip=@rip+1; gh\n");
													}
													OnLineReadCallback = null;
												});
											});
										});
									});
								}
							});
						};
						break;

					default:
						// Move rip past the breakpoint, then go with the
						// exception handled.
						OnPromptCallback = () =>
						{
							WriteStringToPipe(proc.Stdin, "rrip=@rip+1; gh\n");
							OnLineReadCallback = null;
						};
						break;
				}
			}

			////////////////////////////////////////////////////////////////////
			// XB1Target::KdProcess::HandlePrompt
			////////////////////////////////////////////////////////////////////
			private void HandlePrompt(Win32ProcessWrapper proc)
			{
				// Check if this is a break from a R*TM command
				// embedded in the code.  Do this by dumping out
				// the memory at rip.
				WriteStringToPipe(proc.Stdin, "db @rip L10\n");
				OnLineReadCallback = () =>
				{
					Debug.Assert(!m_IsPrompt);
					if(!Regex.IsMatch(m_Line, "^........`........  cc eb .. 52 2a 54 4d "))
					{
						// Not a R*TM command.

						OnPromptCallback = () =>
						{
							WriteStringToPipe(proc.Stdin, "ln @rip\n");
							OnLineReadCallback = () =>
							{
								// Are we stopped in RtlReportCriticalFailure?
								if(Regex.IsMatch(m_Line, @"^\(........`........\)   ntdll!RtlReportCriticalFailure"))
								{
									string dumpFileName = RockstarTargetManager.GetCrashdumpDir() + "kernel.dmp";
									Trace.WriteLine("Critical failure detected - generating kernel dump " + dumpFileName +
										". Please attach it to the bug, this is not performed automatically!\n");
									OnPromptCallback = () =>
									{
										WriteStringToPipe(proc.Stdin, ".dump /f " + dumpFileName + "\n");
										m_StopState = StopState.STOPPED;
										OnLineReadCallback = null;
									};
								}

								// Not a manual break in or R*TM breakpoint ?
								else if(m_IsPrompt)
								{
									// Start the game running again, without handling the exception.
									// This then lets the exception handler in the game handle it.
									WriteStringToPipe(proc.Stdin, "kn; gn\n");
									OnLineReadCallback = null;
								}
							};
						};
					}
					else
					{
						HandleTmCommand(proc);
					}
				};
			}

			////////////////////////////////////////////////////////////////////
			// XB1Target::KdProcess::WorkerThread
			////////////////////////////////////////////////////////////////////
			private void WorkerThread(string ipAddr, IntPtr jobObject)
			{
				while(!m_WorkerThreadExit)
				{
					if(!m_Target.m_PartitionMain.IsConnected || m_Target.m_KitType!=KitType.DEV)
					{
						Thread.Sleep(10);
					}
					else
					{
						using(var proc = new Win32ProcessWrapper(ipAddr, jobObject))
						{
							bool isFirstPrompt = true;
							var lines = new StdoutReader(proc.Stdout);
							m_IsPrompt = false;

							while(!m_WorkerThreadExit && m_Target.m_PartitionMain.IsConnected)
							{
								// Handle stop/go and reboot requests
								switch(m_StopState)
								{
									case StopState.REQUEST_STOP:
										proc.SendCtrlC();
										m_StopState = StopState.STOPPING;
										OnPromptCallback = () =>
										{
											if(m_Process != "")
											{
												WriteStringToPipe(proc.Stdin, ".process "+m_Process+"\n");
											}
											OnPromptCallback = () =>
											{
												m_StopState = StopState.STOPPED;
												OnLineReadCallback = null;
											};
											if(m_Process == "")
											{
												OnPromptCallback();
											}
										};
										break;
									case StopState.STOPPED:
										if(m_RebootRequested)
										{
											WriteStringToPipe(proc.Stdin, ".reboot\n");
											m_StopState = StopState.GO;
											m_RebootRequested = false;
										}
										break;
									case StopState.REQUEST_GO:
										WriteStringToPipe(proc.Stdin, "g\n");
										m_StopState = StopState.GO;
										m_RebootRequested = false;
										break;
									case StopState.GO:
										m_RebootRequested = false;
										break;
								}

								// Read a line from KD's stdout
								m_Line = lines.ReadLine();
								if(m_Line == null)
								{
									// Pending command?
									if(m_NextCommand != null && m_NextCommand != "")
									{
										string cmd = m_NextCommand;
										m_NextCommand = null;
										proc.SendCtrlC();
										OnPromptCallback = () =>
										{
											WriteStringToPipe(proc.Stdin, cmd + "\n");
											OnLineReadCallback = () =>
											{
												if(m_IsPrompt)
												{
													cmd = Interaction.InputBox(m_Line, "KD response", "g");
													WriteStringToPipe(proc.Stdin, cmd + "\n");
													if(cmd == "g" || cmd == "")
													{
														OnLineReadCallback = null;
													}
												}
											};
										};
									}
									Thread.Sleep(1);
									continue;
								}

								// Always echo stdout to tty
								m_Target.TtyOutput(m_Line);

								m_IsPrompt = lines.IsPrompt;
								if(m_OnLineReadCallback != null)
								{
									m_OnLineReadCallback();
								}
								else
								{
									if(m_IsPrompt && m_StopState != StopState.STOPPED)
									{
										// First prompt that is hit, set some kd configuration
										if(isFirstPrompt)
										{
											WriteStringToPipe(proc.Stdin, ".sympath "+XDK_SYMPATH+"\n");
											OnPromptCallback = () =>
											{
												WriteStringToPipe(proc.Stdin, ".reload /f; .lines -e; .cache forcedecodeuser\n");
												OnPromptCallback = () =>
												{
													OnLineReadCallback = null;
													isFirstPrompt = false;
													HandlePrompt(proc);
												};
											};
										}
										else
										{
											HandlePrompt(proc);
										}
									}
									else if(m_IsPrompt)
									{
										// Got a KD prompt when the process was stopped?
										m_Target.TtyOutput("Got KD prompt while target was in the STOPPED state.\n");
									}
								}
							}

							// Would be nice if we could actually ask KD to exit here, ie,
							// send it Ctrl+B.  But this appears to be impossible.  See
							// Win32ProcessWrapper::SendCtrlC() for more details.
							// Instead, we just let the Win32ProcessWrapper disposal
							// kill the KD process.
						}
					}
				}
			}
		}

		////////////////////////////////////////////////////////////////////////
		// XB1Target::TestKitTtyParser
		////////////////////////////////////////////////////////////////////////
		private class TestKitTtyParser
		{
			private XB1Target m_Target;

			private Regex m_RegexTmCmd                          = new Regex("^\\>\\>R\\*TM ");

			private Regex m_RegexNop                            = new Regex("^\\>\\>R\\*TM NOP\n$");
			private Regex m_RegexPrintAllThreadStacks           = new Regex("^\\>\\>R\\*TM PRINT_ALL_THREAD_STACKS\n$");
			private Regex m_RegexStopNoErrorReport              = new Regex("^\\>\\>R\\*TM STOP_NO_ERROR_REPORT\n$");
			private Regex m_RegexQuitf                          = new Regex("^\\>\\>R\\*TM QUITF:(.*)\n$");
			private Regex m_RegexGpuHang                        = new Regex("^\\>\\>R\\*TM GPU_HANG:(.*)\n$");
			private Regex m_RegexCpuHang                        = new Regex("^\\>\\>R\\*TM CPU_HANG\n$");
			private Regex m_RegexConfig                         = new Regex("^\\>\\>R\\*TM CONFIG:(.*)\n$");
			// No sysTmCmdCreateDump, that is handled completely on the console.
			private Regex m_RegexExceptionHandlerBegin          = new Regex("^\\>\\>R\\*TM EXCEPTION_HANDLER_BEGIN\n$");
			private Regex m_RegexExceptionHandlerEnd            = new Regex("^\\>\\>R\\*TM EXCEPTION_HANDLER_END:([^;]*);(.*)\n$");

			// Note that sysTmCmdThreadBegin, sysTmCmdThreadEnd,
			// sysTmCmdReportGpuHangHack and sysTmCmdGetGpuCommandBufferAccess
			// have no testkit versions as they specifically rely on KD.

			private string m_XhitFilename;
			private string m_BugSummary;
			private bool m_EnableBugDupCheck = true;

			private enum State
			{
				NORMAL,
				EXPECT_EXCEPTION_HANDLER_END,
			};
			private State m_State = State.NORMAL;


			////////////////////////////////////////////////////////////////////
			// XB1Target::TestKitTtyParser::TestKitTtyParser
			////////////////////////////////////////////////////////////////////
			internal TestKitTtyParser(XB1Target target)
			{
				m_Target = target;
			}

			////////////////////////////////////////////////////////////////////
			// XB1Target::TestKitTtyParser::StartNewProcess
			////////////////////////////////////////////////////////////////////
			internal void StartNewProcess()
			{
				m_EnableBugDupCheck = true;
				m_BugSummary = null;
				m_XhitFilename = null;
				m_State = State.NORMAL;
			}

			////////////////////////////////////////////////////////////////////
			// XB1Target::TestKitTtyParser::CheckState
			////////////////////////////////////////////////////////////////////
			private void CheckState(State s)
			{
				if(m_State != s)
				{
					m_Target.TtyOutput("R*TM: Synchronization error, unexpected state.  Is "+m_State+", expected "+s+".\n");
				}
			}

			////////////////////////////////////////////////////////////////////
			// XB1Target::TestKitTtyParser::EnsureState
			////////////////////////////////////////////////////////////////////
			private void EnsureState(State s)
			{
				CheckState(s);
				m_State = s;
			}

			////////////////////////////////////////////////////////////////////
			// XB1Target::TestKitTtyParser::Parse
			////////////////////////////////////////////////////////////////////
			internal string Parse(string tty)
			{
				// Check for target manager commands
				try
				{
					if(m_RegexTmCmd.IsMatch(tty))
					{
						// sysTmCmdNop
						if(m_RegexNop.IsMatch(tty))
						{
							EnsureState(State.NORMAL);
						}

						// sysTmCmdPrintAllThreadStacks
						else if(m_RegexPrintAllThreadStacks.IsMatch(tty))
						{
							// Currently no way to support this on testkits.
							EnsureState(State.NORMAL);
						}

						// sysTmCmdStopNoErrorReport
						else if(m_RegexStopNoErrorReport.IsMatch(tty))
						{
							// Don't need to do anything else here, the game will just sit
							// in an infinite loop waiting for the process to be killed.
							EnsureState(State.NORMAL);
						}

						// sysTmCmdQuitf
						else if(m_RegexQuitf.IsMatch(tty))
						{
							m_BugSummary = m_RegexQuitf.Replace(tty, "$1");
							Debug.Assert(m_EnableBugDupCheck == true);
							m_EnableBugDupCheck = true;
							EnsureState(State.NORMAL);
						}

						// sysTmCmdGpuHang
						else if(m_RegexGpuHang.IsMatch(tty))
						{
							m_BugSummary = "XB1 GPU Error";
							m_XhitFilename = m_RegexGpuHang.Replace(tty, "$1");
							m_EnableBugDupCheck = false;
							EnsureState(State.NORMAL);
						}

						// sysTmCmdCpuHang
						else if(m_RegexCpuHang.IsMatch(tty))
						{
							m_BugSummary = "CPU Hang";
							m_EnableBugDupCheck = false;
							EnsureState(State.NORMAL);
						}

						// sysTmCmdConfig
						else if(m_RegexConfig.IsMatch(tty))
						{
							var config = m_RegexConfig.Replace(tty, "$1");
							List<string> ignoreKdCommands;
							m_Target.TmCmdConfig(config, out ignoreKdCommands);
							EnsureState(State.NORMAL);
						}

						// sysTmCmdExceptionHandlerBegin
						else if(m_RegexExceptionHandlerBegin.IsMatch(tty))
						{
							m_Target.BeginCaptureTtyOutput();
							CheckState(State.NORMAL);
							m_State = State.EXPECT_EXCEPTION_HANDLER_END;
						}

						// sysTmCmdExceptionHandlerEnd
						else if(m_RegexExceptionHandlerEnd.IsMatch(tty))
						{
							CheckState(State.EXPECT_EXCEPTION_HANDLER_END);
							m_State = State.NORMAL;

							var coredump       = m_RegexExceptionHandlerEnd.Replace(tty, "$1");
							var bugstarConfig  = m_RegexExceptionHandlerEnd.Replace(tty, "$2");
							var bugDescription = m_Target.EndCaptureTtyOutput();

							m_Target.RaiseBug(m_BugSummary, bugDescription, m_EnableBugDupCheck, bugstarConfig, coredump, m_XhitFilename);

							m_EnableBugDupCheck = true;
							m_BugSummary = null;
							m_XhitFilename = null;
						}
					}
				}
				catch(Exception e)
				{
					m_Target.TtyOutput("R*TM: Error processing command \""+tty+"\", "+e+"\n");
				}

				return tty;
			}
		}

		////////////////////////////////////////////////////////////////////////
		// XB1Target::UpdateFromThread
		////////////////////////////////////////////////////////////////////////
		internal void UpdateFromThread()
		{
			lock(m_LockObj)
			{
				var callback = Interlocked.Exchange(ref m_WorkerThreadCallback, null);
				if(callback != null)
				{
					callback();
					return;
				}

				bool mainWasConnected  = m_PartitionMain.IsConnected;
				bool titleWasConnected = m_PartitionTitle.IsConnected;

				if(!Enabled || !m_PartitionMain.Update())
				{
					m_PartitionTitle.Disconnect();
					m_PartitionMain.Disconnect();

					if(m_ApplicationClient != null)
					{
						Marshal.ReleaseComObject(m_ApplicationClient);
						m_ApplicationClient = null;
					}
				}

				// Main partition connection state changed ?
				if(mainWasConnected != m_PartitionMain.IsConnected)
				{
					// Disconnected ?
					if(mainWasConnected)
					{
					}

					// Connected ?
					else
					{
						// If we're not currently registered as a TTY listener, do that now. This can
						// happen if R*TM is started when the XB1 is powered off.
						if(!m_Tm.TtyListener.IsRegistered(this))
						{
							m_Tm.RegisterTargetAsTtyListener(this);
						}

						// Ensure that kernel debugging is enabled.  Note that
						// GetConfigValue can throw "The data necissary to
						// complete this operation is not yet available"
						// (HRESULT=0x8000000a) under some rare/unknown
						// circumstances.  When that happens, just skip the
						// EnableKernelDebugging check.
						bool rebootingConsole = false;
						try
						{
							string setting = m_PartitionMain.m_ConsoleControlClient.GetConfigValue("EnableKernelDebugging");
							if(setting != "true")
							{
								Trace.WriteLine(m_Name+": EnableKernelDebugging=\""+setting+"\", setting to \"true\" and rebooting console...\n");
								m_PartitionMain.m_ConsoleControlClient.SetConfigValue("EnableKernelDebugging", "true");
								m_PartitionMain.m_ConsoleControlClient.ShutdownConsole(XTFSHUTDOWN_REBOOT);
								rebootingConsole = true;
							}
						}
						catch(Exception e)
						{
							Trace.WriteLine(m_Name+": GetConfigValue(\"EnableKernelDebugging\") threw \""+e.Message+"\"");
						}

						if(!rebootingConsole)
						{
							// Create the application client
							Debug.Assert(m_ApplicationClient == null);
							Guid iid = Marshal.GenerateGuidForType(typeof(XTF.IXtfApplicationClient));
							Int32 hresult = XtfCreateApplicationClient(m_IpAddr, ref iid, out m_ApplicationClient);
							if(hresult < 0)
							{
								Debug.Assert(m_ApplicationClient == null);
								Trace.WriteLine("XtfCreateApplicationClient("+m_IpAddr+") failed, HRESULT=0x"+hresult.ToString("x8")+": "+m_Tm.FormatError(hresult));
							}
							else
							{
								Debug.Assert(m_ApplicationClient != null);
							}
						}
					}
				}

				// Main partition still connected ?
				else if(m_PartitionMain.IsConnected)
				{
					if(!m_PartitionTitle.Update())
					{
						m_PartitionTitle.Disconnect();
					}

					// Title partition connection state changed ?
					if(titleWasConnected != m_PartitionTitle.IsConnected)
					{
						// Disconnected ?
						if(titleWasConnected)
						{
						}

						// Connected ?
						else
						{
							// Don't clear the log file here if m_StartNewLog is
							// already set, since we are currently getting
							// spurious disconnects.  If the Microsoft libraries
							// are ever fixed to not suck, then this can be
							// changed.

							m_StartNewLog = true;
						}
					}
				}

				// If we now know that we are dealing with a testkit, but
				// haven't yet installed the tty parser, then do so.
				if(m_KitType==KitType.TEST && m_TestKitTtyParser==null)
				{
					m_TestKitTtyParser = new TestKitTtyParser(this);
					PushTtyParser((str) => m_TestKitTtyParser.Parse(str));
				}
			}
		}

		////////////////////////////////////////////////////////////////////////
		// XB1Target::ShutdownFromThread
		////////////////////////////////////////////////////////////////////////
		internal void ShutdownFromThread()
		{
			if(m_CheckKitTypeThread != null)
			{
				m_CheckKitTypeThreadExit = true;
			}

			if(m_KdProcess != null)
			{
				m_KdProcess.Dispose();
				m_KdProcess = null;
			}

			if(m_PartitionMain != null)
			{
				m_PartitionMain.Disconnect();
				m_PartitionMain = null;
			}

			if(m_PartitionTitle != null)
			{
				m_PartitionTitle.Disconnect();
				m_PartitionTitle = null;
			}

			if(m_ApplicationClient != null)
			{
				Marshal.ReleaseComObject(m_ApplicationClient);
				m_ApplicationClient = null;
			}

			if(m_CheckKitTypeThread != null)
			{
				m_CheckKitTypeThreadExit = true;
				m_CheckKitTypeThread.Join();
				m_CheckKitTypeThread = null;
			}
		}

		////////////////////////////////////////////////////////////////////////
		// XB1Target::IXtfDeployCallback_Action
		////////////////////////////////////////////////////////////////////////
		private enum IXtfDeployCallback_Action
		{
			IXtfDeployInitializing,
			IXtfDeployEvaluate,
			IXtfDeployMetrics,
			IXtfDeployUpdate,
			IXtfDeployNotifyExtraFileRemoved,
			IXtfDeployNotifyExtraFileDetected,
			IXtfDeployNotifyError,
		};

		////////////////////////////////////////////////////////////////////////
		// XB1Target::DeployCallback
		////////////////////////////////////////////////////////////////////////
		private class DeployCallback : XTF.IXtfDeployCallback
		{
			Object m_LockObj = new Object();
			internal delegate void Callback(IXtfDeployCallback_Action callbackPurpose, string filepath, ulong data1, ulong data2);
			private Callback m_Callback;
			internal DeployCallback(Callback callback) { m_Callback = callback; }

			public void OnDeployInitialized()
			{
				// These callbacks all come in on different threads, so use m_LockObj to ensure we don't process two concurrently.
				lock(m_LockObj)
				{
					m_Callback(IXtfDeployCallback_Action.IXtfDeployInitializing, null, 0, 0);
				}
			}

			public void OnDeployChangeEvaluation(ulong ullFilesProcessed, ulong ullTotalFiles)
			{
				lock(m_LockObj)
				{
					m_Callback(IXtfDeployCallback_Action.IXtfDeployEvaluate, null, ullFilesProcessed, ullTotalFiles);
				}
			}

			public void OnDeployMetrics(ulong ullTotalFiles, ulong ullTotalBytes)
			{
				lock(m_LockObj)
				{
					m_Callback(IXtfDeployCallback_Action.IXtfDeployMetrics, null, ullTotalFiles, ullTotalBytes);
				}
			}

			public void OnDeployFileProgress(string pszFilePath, ulong ullBytesTransferred, ulong ullFileSize)
			{
				lock(m_LockObj)
				{
					m_Callback(IXtfDeployCallback_Action.IXtfDeployUpdate, pszFilePath, ullBytesTransferred, ullFileSize);
				}
			}

			public void OnDeployExtraFileRemoved(string pszFilePath)
			{
				lock(m_LockObj)
				{
					m_Callback(IXtfDeployCallback_Action.IXtfDeployNotifyExtraFileRemoved, pszFilePath, 0, 0);
				}
			}

			public void OnDeployExtraFileDetected(string pszFilePath)
			{
				lock(m_LockObj)
				{
					m_Callback(IXtfDeployCallback_Action.IXtfDeployNotifyExtraFileDetected, pszFilePath, 0, 0);
				}
			}

			public void OnDeployError(int hrError)
			{
				lock(m_LockObj)
				{
					m_Callback(IXtfDeployCallback_Action.IXtfDeployNotifyError, null, (ulong)(uint)hrError, 0);
				}
			}
		}

		////////////////////////////////////////////////////////////////////////
		// XB1Target::CopyFiles
		////////////////////////////////////////////////////////////////////////
		private bool CopyFiles(string dstDir, string srcDir, string srcPattern)
		{
			try
			{
				bool errorFree = true;
				foreach(var srcFile in Directory.EnumerateFiles(srcDir, srcPattern))
				{
					var dstFile = Path.Combine(dstDir, Path.GetFileName(srcFile));
					try
					{
						// Note that if dstFile does not exist
						// GetLastWriteTimeUtc returns 12:00 midnight, January
						// 1, 1601 UTC, so it will be considerred out of date
						// and the copy performed.
						if(File.GetLastWriteTimeUtc(srcFile) > File.GetLastWriteTimeUtc(dstFile))
						{
							const bool overwrite = true;
							File.Copy(srcFile, dstFile, overwrite);
							Trace.WriteLine("Copied file \""+srcFile+"\" -> \""+dstFile+"\"");
						}
					}
					catch
					{
						Trace.WriteLine("Error copying file \""+srcFile+"\" -> \""+dstFile+"\"");
						errorFree = false;
					}
				}
				return errorFree;
			}
			catch
			{
			}
			return false;
		}

		////////////////////////////////////////////////////////////////////////
		// XB1Target::DoDeploy
		////////////////////////////////////////////////////////////////////////
		private string DoDeploy(DeployType dt)
		{
			try
			{
				var looseDir = Config.GetExpandedEnvVar("LooseDir");
				if(!Directory.Exists(looseDir))
				{
					TtyOutput("Error: Loose file directory \""+looseDir+"\" specified in workspace does not exist\n");
					return null;
				}

				// Generate the rfs.dat file
				var rfsDotDat = Path.Combine(looseDir, "rfs.dat");
				bool generatedRfsDotDat = false;
				try
				{
					var hostPcIpAddr = HostPcIpAddr.GetStr();
					if(hostPcIpAddr != null)
					{
						bool updateFile = true;
						try
						{
							if(File.Exists(rfsDotDat))
							{
								using(var reader = new StreamReader(rfsDotDat))
								{
									var currFileContents = reader.ReadLine();
									if(currFileContents == hostPcIpAddr)
									{
										updateFile = false;
									}
								}
							}
						}
						catch
						{
						}
						if(updateFile)
						{
							using(var writer = new StreamWriter(rfsDotDat))
							{
								writer.WriteLine(hostPcIpAddr);
							}
							Trace.WriteLine("Generated \""+rfsDotDat+"\" ("+hostPcIpAddr+")");
						}
						generatedRfsDotDat = true;
					}
				}
				catch
				{
				}
				if(!generatedRfsDotDat)
				{
					Trace.WriteLine("Error generating \""+rfsDotDat+"\"");
					// Note that we still continue with the deployment here,
					// since this may not be a fatal error depending on what
					// aumid within the package will be run.
				}

				// Copy across executables and compressed symbol files
				var buildDir = Config.GetExpandedEnvVar("TargetDir");
				CopyFiles(looseDir, buildDir, "game_durango_*.exe");
				CopyFiles(looseDir, buildDir, "game_durango_*.cmp");
				CopyFiles(looseDir, buildDir, "game_durango_*.pdb");
				CopyFiles(looseDir, buildDir, "game_durango_*.map");

				// Deploy.  Output is intended to be the same format as xbapp deploy /v, but with ip address prefix.
				TtyOutput("Deployment transfer starting.\n");
				const int removeExtraFiles = 0;
				XTF.IXtfDeployCallback callback = new DeployCallback(
					(IXtfDeployCallback_Action callbackPurpose, string filepath, ulong data1, ulong data2) =>
					{
						//Trace.WriteLine("callbackPurpose="+callbackPurpose.ToString()+", filepath=\""+filepath+"\", "
						//	+"data1=0x"+data1.ToString("x8")+", data2=0x"+data2.ToString("x8")+"\n");
						switch(callbackPurpose)
						{
							case IXtfDeployCallback_Action.IXtfDeployEvaluate:
								if(data1 == 0)
								{
									TtyOutput("Total files to evaluate: "+data2.ToString()+"\n");
								}
								else
								{
									TtyOutput("Evaluated "+data1.ToString()+" of "+data2.ToString()+" files.\n");
								}
								break;

							case IXtfDeployCallback_Action.IXtfDeployMetrics:
								TtyOutput("Total files to transfer: "+data1.ToString()+"\n");
								TtyOutput("Total bytes to transfer: "+data2.ToString()+"\n");
								break;

							case IXtfDeployCallback_Action.IXtfDeployUpdate:
								if(data1 == data2)
								{
									TtyOutput(filepath+"\n");
								}
								break;
						}
					});
				int cancelled;
				int result;
				var aumidList = m_ApplicationClient.Deploy(looseDir, removeExtraFiles, callback,
					out cancelled, out result, out m_PackageFullName);
				TtyOutput("Package Full Name: "+m_PackageFullName+"\n");
				aumidList = Regex.Replace(aumidList, ".*\"Applications\":\\[([^\\]]*)\\].*", "$1");
				var aumids = aumidList.Split(new char[]{'"',','}, StringSplitOptions.RemoveEmptyEntries);
				TtyOutput("Aumids returned:\n");
				foreach(var aumid in aumids)
				{
					TtyOutput("  "+aumid+"\n");
				}
				if(cancelled != 0)
				{
					TtyOutput("Cancelled: "+cancelled.ToString()+"\n");
				}
				if(result != 0)
				{
					TtyOutput("HRESULT:   0x"+result.ToString("x8")+"\n");
				}
				if(aumids.Length == 0)
				{
					TtyOutput("Error, no aumids found.\n");
					return null;
				}

				// Enable debug
				m_ApplicationClient.DebugEnable(m_PackageFullName, 1);
				TtyOutput(m_PackageFullName+" debug enabled\n");

				// All aumids are meant to have the same package family name, so doesn't matter which one we extract it from
				return Regex.Replace(aumids[0], @"^([^!]+)!.*", "$1");
			}
			catch(Exception e)
			{
				TtyOutput(e.ToString()+"\n");
			}
			return null;
		}

		////////////////////////////////////////////////////////////////////////
		// XB1Target::Deploy
		////////////////////////////////////////////////////////////////////////
		public override bool Deploy(DeployType dt)
		{
			if(!EnableDeploy)
			{
				return false;
			}
			m_WorkerThreadCallback = () =>
			{
				if(EnableDeploy)
				{
					DoDeploy(dt);
				}
			};
			return true;
		}

		////////////////////////////////////////////////////////////////////////
		// XB1Target::EnableDeploy
		////////////////////////////////////////////////////////////////////////
		public override bool EnableDeploy
		{
			get { return m_PartitionMain!=null && m_PartitionMain.IsConnected
					 && m_PartitionTitle!=null && !m_PartitionTitle.IsConnected
					 && !Stopped && m_KitType!=KitType.UNKNOWN
					 && Config!=null && m_ApplicationClient!=null; }
		}

		////////////////////////////////////////////////////////////////////////
		// XB1Target::StartNewProcess
		////////////////////////////////////////////////////////////////////////
		private void StartNewProcess()
		{
			if(m_StartNewLog)
			{
				RequestNewLogFile();
				m_StartNewLog = false;
			}

			if(m_KdProcess != null)
			{
				m_KdProcess.StartNewProcess();
			}

			if(m_TestKitTtyParser != null)
			{
				m_TestKitTtyParser.StartNewProcess();
			}
		}

		////////////////////////////////////////////////////////////////////////
		// XB1Target::Launch
		////////////////////////////////////////////////////////////////////////
		public override bool Launch()
		{
			if(!EnableLaunch)
			{
				return false;
			}
			m_WorkerThreadCallback = () =>
			{
				if(EnableLaunch)
				{
					StartNewProcess();

					var packageFamilyName = DoDeploy(DeployType.Normal);
					if(packageFamilyName == null)
					{
						TtyOutput("Unable to deploy.  Not launching.\n");
					}
					else
					{
						var appId = Config.GetExpandedEnvVar("AppId");
						var aumid = packageFamilyName + "!" + appId;
						try
						{
							string cmdLine = CommandLine;
							cmdLine += " -ttyframeprefix"
							        +  " -rockstartargetmanager "
							        +  "pdb=" + Path.Combine(Config.GetExpandedEnvVar("LooseDir"), Config.GetExpandedEnvVar("TargetName") + ".pdb") + ";"
							        +  "tty=" + HostPcIpAddr.GetStr() + ":" + NetworkTtyListener.PORT + ";"
							        +  "fullpackagename=" + m_PackageFullName;
							if (m_KitType == KitType.DEV)
							{
								cmdLine += " -devkit";
							}
							m_ApplicationClient.Launch(aumid+" "+cmdLine);
							TtyOutput("Launched "+aumid+"\n");
							m_StartNewLog = true;
						}
						catch
						{
							TtyOutput("Error launching "+aumid+"\n");
						}
					}
				}
			};
			return true;
		}

		////////////////////////////////////////////////////////////////////////
		// XB1Target::EnableLaunch
		////////////////////////////////////////////////////////////////////////
		public override bool EnableLaunch
		{
			get { return EnableDeploy; }
		}

		////////////////////////////////////////////////////////////////////////
		// XB1Target::Reboot
		////////////////////////////////////////////////////////////////////////
		public override void Reboot()
		{
			m_WorkerThreadCallback = () =>
			{
				if(EnableReboot)
				{
					StartNewProcess();

					m_PartitionMain.m_ConsoleControlClient.ShutdownConsole(XTFSHUTDOWN_REBOOT);
				}
			};
		}

		////////////////////////////////////////////////////////////////////////
		// XB1Target::EnableReboot
		////////////////////////////////////////////////////////////////////////
		public override bool EnableReboot
		{
			get { return m_PartitionMain!=null && m_PartitionMain.IsConnected; }
		}

		////////////////////////////////////////////////////////////////////////
		// XB1Target::FastReboot
		////////////////////////////////////////////////////////////////////////
		public override void FastReboot()
		{
			m_WorkerThreadCallback = () =>
			{
				if(EnableFastReboot)
				{
					StartNewProcess();

					Debug.Assert(m_KitType != KitType.UNKNOWN);
					if(m_KitType == KitType.DEV)
					{
						// do devkit stuff
						Stop();
						m_KdProcess.m_RebootRequested = true;
					}
					else
					{
						// try to terminate based on package name
						if (!String.IsNullOrEmpty(m_PackageFullName))
						{
							m_ApplicationClient.Terminate(m_PackageFullName);
						}
					}

					m_PackageFullName = "";
				}
			};
		}

		////////////////////////////////////////////////////////////////////////
		// XB1Target::EnableFastReboot
		////////////////////////////////////////////////////////////////////////
		public override bool EnableFastReboot
		{
			get { return (m_PartitionTitle!=null && m_PartitionTitle.IsConnected) || Stopped; }
		}

		////////////////////////////////////////////////////////////////////////
		// XB1Target::Stop
		////////////////////////////////////////////////////////////////////////
		public override void Stop()
		{
			if(m_KdProcess.m_StopState == KdProcess.StopState.GO)
			{
				m_KdProcess.m_StopState = KdProcess.StopState.REQUEST_STOP;
			}
		}

		////////////////////////////////////////////////////////////////////////
		// XB1Target::Go
		////////////////////////////////////////////////////////////////////////
		public override void Go()
		{
			if(m_KdProcess.m_StopState == KdProcess.StopState.STOPPED)
			{
				m_KdProcess.m_StopState = KdProcess.StopState.REQUEST_GO;
			}
		}

		////////////////////////////////////////////////////////////////////////
		// XB1Target::EnableStopGo
		////////////////////////////////////////////////////////////////////////
		public override bool EnableStopGo
		{
			get { return TitleConnected && m_KitType==KitType.DEV; }
		}

		////////////////////////////////////////////////////////////////////////
		// XB1Target::TitleConnected
		////////////////////////////////////////////////////////////////////////
		private bool TitleConnected
		{
			get { return (m_PartitionTitle!=null && m_PartitionTitle.IsConnected) || Stopped; }
		}

		////////////////////////////////////////////////////////////////////////
		// XB1Target::Stopped
		////////////////////////////////////////////////////////////////////////
		public override bool Stopped
		{
			get { return m_KdProcess!=null && m_KdProcess.m_StopState==KdProcess.StopState.STOPPED; }
		}

		////////////////////////////////////////////////////////////////////////
		// XB1Target::EnableTargetSpecific
		////////////////////////////////////////////////////////////////////////
		public override bool EnableTargetSpecific
		{
			get { return m_KdProcess!=null && TitleConnected && m_KitType==KitType.DEV; }
		}

		////////////////////////////////////////////////////////////////////////
		// XB1Target::TargetSpecificCommand
		////////////////////////////////////////////////////////////////////////
		public override void TargetSpecificCommand()
		{
			string cmd = Interaction.InputBox("Enter KD command to execute:", "Target-specific command", "k");
			if (cmd != "g")
			{
				// Don't halt the process just to continue...
				m_KdProcess.ExecuteCommand(cmd);
			}
		}

		////////////////////////////////////////////////////////////////////////
		// XB1Target::Name
		////////////////////////////////////////////////////////////////////////
		public override string Name
		{
			get { return m_Name; }
		}

		////////////////////////////////////////////////////////////////////////
		// XB1Target::Alias
		////////////////////////////////////////////////////////////////////////
		public override string Alias
		{
			get { return m_Alias; }
		}

		////////////////////////////////////////////////////////////////////////
		// XB1Target::PlatformId
		////////////////////////////////////////////////////////////////////////
		public override TargetPlatformId PlatformId
		{
			get { return TargetPlatformId.XB1; }
		}
	}


	////////////////////////////////////////////////////////////////////////////
	// XB1TargetManager
	////////////////////////////////////////////////////////////////////////////
	public class XB1TargetManager : IDisposable
	{
		private Object m_LockObj = new Object();
		private XTF.IXtfConsoleManager      m_ConsoleManager;
		private List<XB1Target> m_Targets = new List<XB1Target>();
		private Dictionary<string, XB1Target> m_TargetsByName = new Dictionary<string, XB1Target>();
		private NetworkTtyListener m_NetworkTtyListener;
		private volatile bool m_WorkerThreadExit = false;
		private Thread m_WorkerThread;
		private volatile bool m_MailslotThreadExit = false;
		private Thread m_MailslotThread;
		private static string MAILSLOT_NAME = @"\\.\mailslot\Rockstar Target Manager\XB1 Enable";
		private IntPtr m_JobObject;

		static List<string> s_EnableTargets = new List<string>();

		[DllImport("XtfConsoleManager.dll")]
		private static extern Int32 XtfCreateConsoleManager(IntPtr pCallback, ref Guid riid, out XTF.IXtfConsoleManager ppvObject);

		[DllImport("XtfApi.dll", CharSet=CharSet.Unicode)]
		private static extern Int32 XtfGetErrorText(Int32 hresult, char[] errorMessageBuffer, ref UInt32 errorMessageBufferLength, char[] userActionTextBuffer, ref UInt32 userActionTextBufferLength);

		[DllImport("XtfApi.dll", CharSet=CharSet.Unicode)]
		private static extern Int32 XtfGetErrorText(Int32 hresult, char[] errorMessageBuffer, ref UInt32 errorMessageBufferLength, IntPtr zero, ref UInt32 userActionTextBufferLength);

		[DllImport("XtfApi.dll", CharSet=CharSet.Unicode)]
		private static extern Int32 XtfGetErrorText(Int32 hresult, IntPtr zero0, ref UInt32 errorMessageBufferLength, IntPtr zero1, ref UInt32 userActionTextBufferLength);


		////////////////////////////////////////////////////////////////////////
		// XB1TargetManager::TtyListener
		////////////////////////////////////////////////////////////////////////
		public NetworkTtyListener TtyListener
		{
			get
			{
				return m_NetworkTtyListener;
			}
		}

		////////////////////////////////////////////////////////////////////////
		// XB1TargetManager::XB1TargetManager
		////////////////////////////////////////////////////////////////////////
		public XB1TargetManager(NetworkTtyListener networkTtyListener)
		{
			m_NetworkTtyListener = networkTtyListener;

			Guid iidConsoleManager = Marshal.GenerateGuidForType(typeof(XTF.IXtfConsoleManager));
			Int32 hresult;
			if((hresult = XtfCreateConsoleManager((IntPtr)0, ref iidConsoleManager, out m_ConsoleManager)) < 0)
			{
				Trace.WriteLine("XtfCreateConsoleManager failed, HRESULT=0x"+hresult.ToString("x8"));
				throw new Exception();
			}

			// We create a job object so that the KD processes get automatically
			// cleaned up if the main process is killed.
			// http://stackoverflow.com/questions/3342941/kill-child-process-when-parent-process-is-killed
			// is a good source of info on this.

			m_JobObject = Win32.CreateJobObject(null, null);
			if(m_JobObject == IntPtr.Zero)
			{
				Trace.WriteLine("CreateJobObject failed, err=0x"+Marshal.GetLastWin32Error().ToString("x8"));
				throw new Exception();
			}

			var info = new Win32.JOBOBJECT_EXTENDED_LIMIT_INFORMATION();
			info.basicLimitInformation.limitFlags = Win32.JOB_OBJECT_LIMIT_KILL_ON_JOB_CLOSE;
			if(!Win32.SetInformationJobObject(m_JobObject, Win32.JOBOBJECTINFOCLASS.JobObjectExtendedLimitInformation, ref info, Marshal.SizeOf(info)))
			{
				Trace.WriteLine("SetInformationJobObject failed, err=0x"+Marshal.GetLastWin32Error().ToString("x8"));
				throw new Exception();
			}

			m_WorkerThread = new Thread(WorkerThread);
			m_WorkerThread.Name = "XB1TargetManager::WorkerThread";
			m_WorkerThread.Start();

			m_MailslotThread = new Thread(MailslotThread);
			m_MailslotThread.Name = "XB1TargetManager::MailslotThread";
			m_MailslotThread.Start();

			// The job object should take care of cleaning up most of the kd
			// processes automatically, but to be sure to be sure, kill off any
			// we find now (launching R*TM through Visual Studio is one case
			// were there could be some kd processes lying around).
			//
			// Note that the regex here checks that a directory
			// "RockstarTargetManager" is in the path so that we don't kill
			// other kd processes that the user may have launched manually.
			// Other subdirectories are also allowed so that the regex will
			// match for both the checked in tools binary, and a locally built
			// executable running through Visual Studio.
			//
			var kdRegex = new Regex(@".*\\RockstarTargetManager\\(.*\\)?kd\.exe$");
			foreach(var proc in Process.GetProcesses())
			{
				try
				{
					var filename = proc.MainModule.FileName;
					if(kdRegex.IsMatch(filename))
					{
						Trace.WriteLine("Killing orphaned kd process, pid="+proc.Id.ToString()+", \""+filename+"\"");
						proc.Kill();
					}
				}
				catch
				{
				}
			}
		}

		////////////////////////////////////////////////////////////////////////
		// XB1TargetManager::~XB1TargetManager
		////////////////////////////////////////////////////////////////////////
		~XB1TargetManager()
		{
			Dispose(false);
		}

		////////////////////////////////////////////////////////////////////////
		// XB1TargetManager::Dispose
		////////////////////////////////////////////////////////////////////////
		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		////////////////////////////////////////////////////////////////////////
		// XB1TargetManager::Dispose
		////////////////////////////////////////////////////////////////////////
		protected virtual void Dispose(bool disposing)
		{
			if(disposing)
			{
				if(m_WorkerThread != null)
				{
					m_WorkerThreadExit = true;
					m_WorkerThread.Join();
				}

				if(m_MailslotThread != null)
				{
					m_MailslotThreadExit = true;
					m_MailslotThread.Join();
				}

				if(m_Targets != null)
				{
					foreach(XB1Target t in m_Targets)
					{
						t.Dispose();
					}
				}

				if(m_JobObject != IntPtr.Zero)  Win32.CloseHandle(m_JobObject);

				if(m_ConsoleManager != null)    Marshal.ReleaseComObject(m_ConsoleManager);
			}
		}

		////////////////////////////////////////////////////////////////////////
		// XB1TargetManager::FormatError
		////////////////////////////////////////////////////////////////////////
		internal string FormatError(Int32 hresult)
		{
			try
			{
				UInt32 errorMessageBufferLength   = 0;
				UInt32 userActionTextBufferLength = 0;
				Int32 hresult2;
				hresult2 = XtfGetErrorText(hresult, IntPtr.Zero, ref errorMessageBufferLength, IntPtr.Zero, ref userActionTextBufferLength);
				const Int32 ERROR_MORE_DATA = unchecked((int)0x800700ea);
				if(hresult2 < 0 && hresult2 != ERROR_MORE_DATA)
				{
					return "unable to get error string, HRESULT=0x"+hresult2.ToString("x8");
				}

				if(errorMessageBufferLength == 0)
				{
					return "";
				}

				var errorMessageBuffer   = new char[errorMessageBufferLength];
				var userActionTextBuffer = new char[userActionTextBufferLength];
				hresult2 = XtfGetErrorText(hresult, errorMessageBuffer, ref errorMessageBufferLength, userActionTextBuffer, ref userActionTextBufferLength);
				if(hresult2 < 0)
				{
					return "unable to get error string, HRESULT=0x"+hresult2.ToString("x8");
				}

				var errorMessage = new string(errorMessageBuffer, 0, (int)errorMessageBufferLength-1);
				if(userActionTextBufferLength == 0)
				{
					return errorMessage;
				}
				else
				{
					var userActionText = new string(userActionTextBuffer, 0, (int)userActionTextBufferLength-1);
					return errorMessage+"\n"+userActionText;
				}
			}
			catch(Exception e)
			{
				return "exception thrown while decoding hresult, "+e.ToString();
			}
		}

		////////////////////////////////////////////////////////////////////////
		// XB1TargetManager::ConsoleEnumerator
		////////////////////////////////////////////////////////////////////////
		private class ConsoleEnumerator : XTF.IXtfEnumerateConsolesCallback
		{
			internal delegate void Callback(ref XTF._XTFCONSOLEDATA data);
			private Callback m_Callback;
			internal ConsoleEnumerator(Callback callback) { m_Callback = callback; }
			public void OnConsoleFound(ref XTF._XTFCONSOLEDATA data) { m_Callback(ref data); }
		}

		////////////////////////////////////////////////////////////////////////
		// XB1TargetManager::GetDefaultTargetNames
		////////////////////////////////////////////////////////////////////////
		public string[] GetDefaultTargetNames()
		{
			List<string> names = new List<string>();
			m_ConsoleManager.EnumerateConsoles(new ConsoleEnumerator((ref XTF._XTFCONSOLEDATA c) => {names.Add(c.bstrAddress);}));
			return names.ToArray();
		}

		////////////////////////////////////////////////////////////////////////
		// XB1TargetManager::EnableTargetsWhenCreated
		////////////////////////////////////////////////////////////////////////
		private static void EnableTargetsWhenCreated(List<string> names)
		{
			s_EnableTargets.AddRange(names);
		}

		////////////////////////////////////////////////////////////////////////
		// XB1TargetManager::EnableTargetsInOtherProcess
		////////////////////////////////////////////////////////////////////////
		private static void EnableTargetsInOtherProcess(List<string> names)
		{
			// Try to open the mailslot that another existing R*TM instance may be listenning to.
			IntPtr securityAttributes = IntPtr.Zero;
			var fileHandle = Win32.CreateFile(MAILSLOT_NAME, Win32.GENERIC_WRITE, Win32.FILE_SHARE_READ,
				securityAttributes, Win32.OPEN_EXISTING, Win32.FILE_ATTRIBUTE_NORMAL, IntPtr.Zero);
			if(fileHandle == Win32.INVALID_HANDLE_VALUE)
			{
				return;
			}

			try
			{
				// Send each target name as a separate message to the mailslot.
				foreach(var name in names)
				{
					var buf = new byte[name.Length*sizeof(char)];
					Buffer.BlockCopy(name.ToCharArray(), 0, buf, 0, buf.Length);
					Int32 numberOfBytesWriten;
					if(!Win32.WriteFile(fileHandle, buf, buf.Length, out numberOfBytesWriten, IntPtr.Zero))
					{
						Trace.WriteLine("WriteFile failed, err=0x"+Marshal.GetLastWin32Error().ToString("x8"));
					}
					Debug.Assert(numberOfBytesWriten == buf.Length);
				}
			}
			finally
			{
				Win32.CloseHandle(fileHandle);
			}
		}

		////////////////////////////////////////////////////////////////////////
		// XB1TargetManager::EnableTargets
		////////////////////////////////////////////////////////////////////////
		public static void EnableTargets(List<string> names)
		{
			if(names.Count == 0)
			{
				return;
			}
			EnableTargetsWhenCreated(names);
			EnableTargetsInOtherProcess(names);
		}

		////////////////////////////////////////////////////////////////////////
		// XB1TargetManager::XbConnect
		////////////////////////////////////////////////////////////////////////
		private class XbConnectResult
		{
			public string m_Host;
			public string m_System;
		};
		private XbConnectResult XbConnect(string name)
		{
			// Given a "system" address, there doesn't seem to be any way to get
			// the "host" address for a console.  Other than by calling the
			// xbconnect command line utility.  While the system address will
			// work for deploying the game, the kernel debugger can only connect
			// to the host address.
			try
			{
				var xdk = Environment.GetEnvironmentVariable("DurangoXDK");
				var proc = new Process();
				proc.StartInfo.FileName                 = Path.Combine(xdk, @"bin\xbconnect.exe");
				proc.StartInfo.Arguments                = name + " /Q";
				proc.StartInfo.CreateNoWindow           = true;
				proc.StartInfo.RedirectStandardOutput   = true;
				proc.StartInfo.UseShellExecute          = false;
				proc.StartInfo.WorkingDirectory         = xdk;

				try
				{
					proc.Start();
				}
				catch(Exception)
				{
					Trace.WriteLine("Warning: Failed to launch " + proc.StartInfo.FileName);
					return null;
				}

				var hostRegex = new Regex(@"^((\s*HOST: [^@]+@([0-9]+.[0-9]+.[0-9]+.[0-9]+))|.*)$");
				var systemRegex = new Regex(@"^((\s*SYSTEM: [^@]+@([0-9]+.[0-9]+.[0-9]+.[0-9]+))|.*)$");
				var ret = new XbConnectResult();

				// Async read line - xbconnect hangs if the default kit isn't connected
				proc.OutputDataReceived += (sender, outputLine) =>
				{
					if(outputLine.Data != null)
					{
						ret.m_Host   += hostRegex.Replace(outputLine.Data,   "$3");
						ret.m_System += systemRegex.Replace(outputLine.Data, "$3");
					}
				};
				proc.BeginOutputReadLine();

				// Hmm, arbitrary 2 second timeout...
				if(!proc.WaitForExit(2000))
				{
					try
					{
						proc.Kill();
					}
					catch
					{
					}
				}
				if(ret.m_Host != "" && ret.m_Host != null && ret.m_System != "" && ret.m_System != null)
				{
					return ret;
				}
			}
			catch
			{
			}
			return null;
		}

		////////////////////////////////////////////////////////////////////////
		// XB1TargetManager::RegisterTargetAsTtyListener
		//
		// Will look up the targets system IP based on it's host IP, and then
		// add it to the network TTY listener.
		// Returns true if the target was added, else false
		////////////////////////////////////////////////////////////////////////
		public bool RegisterTargetAsTtyListener(Target t)
		{
			// No-op if already added
			if (m_NetworkTtyListener.IsRegistered(t))
			{
				return true;
			}

			t.TtyOutput("Looking up system IP address for host " + t.Name + "... ");
			bool added = false;
			lock (m_LockObj)
			{
				var connected = XbConnect(t.Name);
				if(connected != null)
				{
					m_NetworkTtyListener.Register(connected.m_System, t);
					t.TtyOutput("Registered system IP " + connected.m_System + " as a TTY listener.\n");
					m_NetworkTtyListener.Register(connected.m_Host, t);
					t.TtyOutput("Registered host IP " + connected.m_Host + " as a TTY listener.\n");
					added = true;
				}
				else
				{
					t.TtyOutput("Failed to look up system IP from host IP!\n");
				}
			}
			return added;
		}

		////////////////////////////////////////////////////////////////////////
		// XB1TargetManager::AddTarget
		////////////////////////////////////////////////////////////////////////
		public Target AddTarget(string name)
		{
			if (name == null)
			{
				return null;
			}

			lock(m_LockObj)
			{
				XB1Target target = null;
				bool targetExists = false;
				if(m_TargetsByName.TryGetValue(name, out target))
				{
					targetExists = true;
				}
				else
				{
					try
					{
						// See if the XB1 is specified by system IP
						var connected = XbConnect(name);
						if (connected != null && name != connected.m_Host)
						{
							Trace.WriteLine("Specified Xbox One address "+name+" is the System OS address, using Host OS address "+connected.m_Host+" instead");
							name = connected.m_Host;
							if(m_TargetsByName.TryGetValue(name, out target))
							{
								targetExists = true;
							}
							else
							{
								connected = XbConnect(name);
							}
						}

						// Add the target if it doesn't exist
						if(!targetExists)
						{
							try
							{
								m_ConsoleManager.EnumerateConsoles(new ConsoleEnumerator(
									(ref XTF._XTFCONSOLEDATA c) =>
									{
										if(c.bstrAddress == name && !targetExists)
										{
											Trace.WriteLine("Attempting to look up system IP address for XB1 console " + name + " being added...");
											bool enable = s_EnableTargets.Contains(name);
											target = new XB1Target(c, this, m_JobObject, enable);
											m_Targets.Add(target);
											m_TargetsByName.Add(name, target);
											if (connected != null)
											{
												m_NetworkTtyListener.Register(connected.m_System, target);
												Trace.WriteLine("Registered system IP " + connected.m_System + " as a TTY listener");
												m_NetworkTtyListener.Register(connected.m_Host, target);
												Trace.WriteLine("Registered host IP " + connected.m_Host + " as a TTY listener");
											}
											else
											{
												Trace.WriteLine("Failed to look up system IP. The console is probably offline.");
											}
											targetExists = true;
										}
									}));
							}
							catch(Exception e)
							{
								Trace.WriteLine("Error: Failure enumerating consoles");
								Trace.WriteLine(e.ToString());
							}
						}
					}
					catch
					{
					}
				}

				// Create dummy target if needed
				if (target == null)
				{
					XTF._XTFCONSOLEDATA c = new XTF._XTFCONSOLEDATA();
					c.bstrAddress = name;
					c.bstrAlias = name;
					target = new XB1Target(c, this, m_JobObject, false);
					m_Targets.Add(target);
					m_TargetsByName.Add(name, target);
				}
				return target;
			}
		}

		////////////////////////////////////////////////////////////////////////
		// XB1TargetManager::WorkerThread
		////////////////////////////////////////////////////////////////////////
		private void WorkerThread()
		{
			List<XB1Target> targetListCopy;

			while(!m_WorkerThreadExit)
			{
				lock(m_LockObj)
				{
					targetListCopy = new List<XB1Target>(m_Targets);
				}
				foreach(XB1Target t in targetListCopy)
				{
					t.UpdateFromThread();
				}

				Thread.Sleep(500);
			}

			foreach(XB1Target t in m_Targets)
			{
				t.ShutdownFromThread();
			}
		}

		////////////////////////////////////////////////////////////////////////
		// XB1TargetManager::MailslotThread
		////////////////////////////////////////////////////////////////////////
		private void MailslotThread()
		{
			const Int32 maxMessageSize = 0;
			const Int32 readTimeout = Win32.MAILSLOT_WAIT_FOREVER;
			IntPtr securityAttributes = IntPtr.Zero;
			var mailslotHandle = Win32.CreateMailslot(MAILSLOT_NAME, maxMessageSize, readTimeout, securityAttributes);
			try
			{
				while(!m_MailslotThreadExit)
				{
					Int32 getMaxMessageSize;
					Int32 getNextSize;
					Int32 getMessageCount;
					Int32 getReadTimeout;
					if(!Win32.GetMailslotInfo(mailslotHandle, out getMaxMessageSize, out getNextSize, out getMessageCount, out getReadTimeout))
					{
						Trace.WriteLine("GetMailslotInfo failed, err=0x"+Marshal.GetLastWin32Error().ToString("x8"));
					}
					else if(getNextSize != Win32.MAILSLOT_NO_MESSAGE && getMessageCount >= 1)
					{
						var buf = new byte[getNextSize];
						var numberOfBytesToRead = getNextSize;
						Int32 numberOfBytesRead;
						IntPtr overlapped = IntPtr.Zero;
						if(!Win32.ReadFile(mailslotHandle, buf, numberOfBytesToRead, out numberOfBytesRead, overlapped))
						{
							Trace.WriteLine("ReadFile failed, err=0x"+Marshal.GetLastWin32Error().ToString("x8"));
						}
						else
						{
							Debug.Assert(numberOfBytesRead == numberOfBytesToRead);
							Debug.Assert(numberOfBytesRead % sizeof(char) == 0);

							var wideCharBuf = new char[numberOfBytesRead/sizeof(char)];
							Buffer.BlockCopy(buf, 0, wideCharBuf, 0, wideCharBuf.Length*sizeof(char));
							var name = new string(wideCharBuf);
							lock(m_LockObj)
							{
								s_EnableTargets.Add(name);
								XB1Target target;
								if(m_TargetsByName.TryGetValue(name, out target))
								{
									target.Enabled = true;

									// Also call FastReboot to ensure that the
									// kernel debugger doesn't have the target
									// stopped.
									target.FastReboot();
								}
							}
						}
					}

					Thread.Sleep(100);
				}
			}
			finally
			{
				Win32.CloseHandle(mailslotHandle);
			}
		}
	}
}
