using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.ComponentModel;
using System.Threading;
using System.Windows.Forms; // For MessageBox

namespace Rockstar.TargetManager
{
    public static class Globals
    {
        public static int m_CopyingFileCount;
    }

    public enum DeployType
	{
		None,
		Normal,
		Force
	}

	public enum TargetPlatformId
	{
		Unknown,
		PS3,
		PS4,
		Xbox360,
		XB1,
		PC,
	}

	public static class TargetPlatformIdExtensions
	{
		public static string CustomToString(this TargetPlatformId id)
		{
			switch(id)
			{
				case TargetPlatformId.PS3:		return "PS3";
				case TargetPlatformId.PS4:		return "PS4";
				case TargetPlatformId.Xbox360:	return "Xbox 360";
				case TargetPlatformId.XB1:		return "Xbox One";
				case TargetPlatformId.PC:       return "PC";
				default:						return "UNKNOWN PLATFORM";
			}
		}
	}

	public enum TargetEventType
	{
		RequestNewLogFile,
		FlushLogFile,
		BugRaised,
		AssertionFailed,
		ExecutionBreak,
		DataBreak,
		DebugString,
		MemoryReadException,
		MemoryWriteException,
		Stopped,
		Rebooting,
		Running,
		Rip,
		Acquired,
		Lost,
		Launch,
	}

	public class TargetEventArgs : EventArgs
	{
		internal TargetEventType m_EventType;
		internal string m_Message;
		internal UInt64 m_ThreadId;
		internal UInt64 m_Address;
		internal UInt64 m_DataAddress;
		internal UInt64 m_ExceptionCode;
		internal bool m_IsThreadStopped;

		internal TargetEventArgs()
		{
		}

		public TargetEventType EventType
		{
			get{return m_EventType;}
		}

		public string Message
		{
			get{return m_Message;}
		}

		public UInt64 ThreadId
		{
			get{return m_ThreadId;}
		}

		public UInt64 Address
		{
			get{return m_Address;}
		}

		public UInt64 DataAddress
		{
			get{return m_DataAddress;}
		}

		public bool IsThreadStopped
		{
			get{return m_IsThreadStopped;}
		}
	};

	public class BugRaisedTargetEventArgs : TargetEventArgs
	{
		internal ZipArchive m_Zip;

		public ZipArchive Zip
		{
			get{return m_Zip;}
		}
	}


	public abstract class Target : IDisposable
	{
		internal delegate string TtyParserDelegate(string tty);
		private List<TtyParserDelegate> m_TtyParsers = new List<TtyParserDelegate>();
		private uint m_NestedTtyOutputCount = 0;
		private LinkedList<string> m_DelayedTtyOutput = new LinkedList<string>();

		private Configuration m_Config = new Configuration();
		private string m_CmdLine = "";
		private string m_TtyCaptureString = null;
		private LogJam.WeakEvent<TargetEventArgs> m_TargetEvent;
		private bool m_Enabled = true;

		protected Target()
		{
			m_TargetEvent = new LogJam.WeakEvent<TargetEventArgs>(this);
		}

		~Target()
		{
			Dispose(false);
		}

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		protected virtual void Dispose(bool disposing)
		{
		}

		public enum Icon
		{
			NOT_AVAILABLE   = 1,
			NOT_RUNNING     = 3,
			RUNNING         = 2,
		}

		public abstract Icon SelectedIcon
		{
			get;
		}

		public virtual bool Deploy(DeployType dt)
		{
			return false;
		}

		public virtual bool EnableDeploy
		{
			get { return false; }
		}

		public virtual bool Launch()
		{
			return false;
		}

		public virtual bool EnableLaunch
		{
			get { return false; }
		}

		public virtual void Reboot()
		{
		}

		public virtual bool EnableReboot
		{
			get { return false; }
		}

		public virtual void FastReboot()
		{
		}

		public virtual bool EnableFastReboot
		{
			get { return false; }
		}

		public virtual void Stop()
		{
		}

		public virtual void Go()
		{
		}

		public virtual bool EnableStopGo
		{
			get { return false; }
		}

		public virtual bool Stopped
		{
			get { return false; }
		}

		public virtual void Coredump()
		{
		}

		public virtual bool EnableCoredump
		{
			get { return false; }
		}

		public virtual bool ScreenShot(string filename)
		{
			return false;
		}

		public virtual bool EnableScreenShot
		{
			get { return false; }
		}

        public virtual void TargetSpecificCommand()
        {
        }

        public virtual bool EnableTargetSpecific
        {
            get { return false; }
        }

        public LogJam.WeakEvent<TargetEventArgs> TargetEvent
		{
			get{return m_TargetEvent;}
		}

		public bool Enabled
		{
			get { return m_Enabled; }
			set
			{
				if(m_Enabled != value)
				{
					m_Enabled = value;
					OnEnabledChanged(value);
					TtyOutput("==== R*TM console enabled "+value.ToString()+" ====\n");
				}
			}
		}

		protected virtual void OnEnabledChanged(bool enabled) {}

		public abstract string Name { get;}
		public abstract string Alias { get;}

		public virtual string FullName
		{
			get
			{
				string str = "";
				if (this.Alias != null) { str += this.Alias + " : "; }
				str += this.Name;
				return str;
			}
		}

		public virtual string Description
		{
			get
			{
				string config = this.Config.Name;
				if (config == null) { config = "***"; }
				string str = String.Format("{0} {1}", this.FullName, config);
				return str;
			}
		}

		public Configuration Config
		{
			get{return m_Config;}
			set
			{
				if(null != value)
				{
					m_Config = value;
				}
			}
		}

		public string CommandLine
		{
			get{return m_CmdLine;}
			set
			{
				m_CmdLine = (null == value) ? "" : value;
			}
		}

		public virtual TargetPlatformId PlatformId
		{
			get
			{
				return TargetPlatformId.Unknown;
			}
		}

		public TargetEventArgs CreateEvent(TargetEventType type, string message)
		{
			var ev = new TargetEventArgs();
			ev.m_EventType = type;
			ev.m_Message   = message;
			return ev;
		}

		protected void BeginCaptureTtyOutput()
		{
			lock(this)
			{
				m_TtyCaptureString = "";
			}
		}

		protected string EndCaptureTtyOutput()
		{
			lock(this)
			{
				string str = m_TtyCaptureString;
				m_TtyCaptureString = null;
				return str;
			}
		}

		internal void PushTtyParser(TtyParserDelegate parser)
		{
			lock(this)
			{
				m_TtyParsers.Add(parser);
			}
		}

		internal void PopTtyParser()
		{
			lock(this)
			{
				m_TtyParsers.RemoveAt(m_TtyParsers.Count-1);
			}
		}

		// Fake TTY output.	 Interleaved with the real console TTY.
		public void TtyOutput(string str)
		{
			lock(this)
			{
				// Tty parsers can recursively call TtyOutput.  This is handled by buffering
				// up the strings into a fifo.  The fifo makes sure the correct order of the
				// strings is preserved, and prevents recursion getting out of control.
				if(m_NestedTtyOutputCount != 0)
				{
					m_DelayedTtyOutput.AddLast(str);
					return;
				}

				for(;;)
				{
					++m_NestedTtyOutputCount;

					for(var i=m_TtyParsers.Count; i-->0; )
					{
						if(str == null)
						{
							break;
						}
						try
						{
							str = m_TtyParsers[i](str);
						}
						catch(Exception e)
						{
							TtyOutput("R*TM: Tty parser error, "+e+"\n");
						}
					}

					if(str != null)
					{
						var e = new TargetEventArgs();
						e.m_EventType = TargetEventType.DebugString;
						e.m_Message   = str;
						TargetEvent.Invoke(e);

						if(m_TtyCaptureString != null)
						{
							m_TtyCaptureString += str;
						}
					}

					--m_NestedTtyOutputCount;

					if(m_DelayedTtyOutput.Count != 0)
					{
						str = m_DelayedTtyOutput.First.Value;
						m_DelayedTtyOutput.RemoveFirst();
					}
					else
					{
						break;
					}
				}
			}
		}

		// Broadcast to plugins that now would be a good time to start a new logfile.
		protected void RequestNewLogFile()
		{
			var e = new TargetEventArgs();
			e.m_EventType = TargetEventType.RequestNewLogFile;
			TargetEvent.Invoke(e);
		}

		// Broadcast to plugins to flush any logfiles.
		public void FlushLogFile()
		{
			var e = new TargetEventArgs();
			e.m_EventType = TargetEventType.FlushLogFile;
			TargetEvent.Invoke(e);
		}

		// Create a bug in the Bugstar database
		protected void BaseRaiseBug(Bugstar.Config config, string summary, string description, string[] filesToCopy, bool enableBugDupCheck)
		{
			// If no bug summary was provided, then use the callstack in the description to create one
			if(String.IsNullOrWhiteSpace(summary))
			{
				var stack = Bugstar.ExtractCallstack(description);
				summary = "Crashed in " + ((stack.Length>0) ? stack[0] : "unknown");
			}

			// Create bug
			UInt32 bugId = Bugstar.RaiseBug(this, config, summary, description, enableBugDupCheck, TtyOutput);
			if(bugId == 0)
			{
				return;
			}

			string bugUrl = "url:bugstar:"+bugId.ToString();
			TtyOutput("\nRaised "+bugUrl+"\n");

			// If bug was created, then copy logs to appropriate directory
			string commentFormatString = "Crash information copied to \"{0}\" ("+PlatformId.CustomToString();
			if(!String.IsNullOrWhiteSpace(config.BuildId))
			{
				commentFormatString += ", "+config.BuildId;
			}
			commentFormatString += ").";
			string zipFilename = Bugstar.GetZipName(config, bugId, commentFormatString, TtyOutput);
			if(zipFilename == null)
			{
				return;
			}
            Interlocked.Increment(ref Globals.m_CopyingFileCount);
            bool needsDec = true;
            try
			{
				using(var zipStream = File.Create(zipFilename))
				{
					using(var zip = new ZipArchive(zipStream, ZipArchiveMode.Create))
					{
                        Trace.WriteLine("Compressing files to \"" + zipFilename + "\". This could take several minutes, you will be notified when it is complete.\n");
                        TtyOutput("Compressing files to \"" + zipFilename + "\" ...\n");
                        foreach (string file in filesToCopy)
                        {
							// filename can contain a '*' to override the name
							// inside the zip file.  For example "c:\\foo\\bar"
							// will store a zip entry named "bar", while
							// "c:\\foo\\bar*bar2" will store an entry named
							// "bar2".
							string src, dst;
							string done;
							int astrix = file.IndexOf('*');
							if(astrix == -1)
							{
								src = file;
								dst = Path.GetFileName(file);
								done = "  \""+file+"\"\n";
							}
							else
							{
								src = file.Substring(0, astrix);
								dst = file.Substring(astrix+1);
								done = "  \""+src+" -> "+dst+"\"\n";
							}

							try
							{
								zip.CreateEntryFromFile(src, dst, CompressionLevel.Optimal);
								TtyOutput(done);
							}
							catch(Exception ex)
							{
								TtyOutput("  Error compressing \""+file+"\": "+ex.Message+"\n");
							}
						}

						// Notify plugins to copy additional output files
						var ev = new BugRaisedTargetEventArgs();
						ev.m_EventType = TargetEventType.BugRaised;
						ev.m_Zip = zip;
						TargetEvent.Invoke(ev);

                        // Pop up message box to inform user that copy is complete
                        if (filesToCopy.Length > 0)
                        {
                            Interlocked.Decrement(ref Globals.m_CopyingFileCount);
                            needsDec = false;
                            MessageBox.Show(null, "Crash information files have been successfully copied to " + zipFilename + ".",
                                "File copy complete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
				}
			}
			catch
			{
			}
            if (needsDec)
            {
                Interlocked.Decrement(ref Globals.m_CopyingFileCount);
            }

			// Open the bug so user can edit it to add more info
			try
			{
				Process.Start(bugUrl);
			}
			catch
			{
			}

			TtyOutput("\n");
		}

		public static UInt32 EndianSwap(UInt32 x)
		{
			return ((x&0xff000000)>>24)
			     | ((x&0x00ff0000)>>8)
			     | ((x&0x0000ff00)<<8)
			     | ((x&0x000000ff)<<24);
		}

		public static UInt64 EndianSwap(UInt64 x)
		{
			return ((x&0xff00000000000000)>>56)
			     | ((x&0x00ff000000000000)>>40)
			     | ((x&0x0000ff0000000000)>>24)
			     | ((x&0x000000ff00000000)>>8)
			     | ((x&0x00000000ff000000)<<8)
			     | ((x&0x0000000000ff0000)<<24)
			     | ((x&0x000000000000ff00)<<40)
			     | ((x&0x00000000000000ff)<<56);
		}

		protected delegate bool GetMemoryDelegate(UInt64 addr, ref byte[] buf);
		protected static bool GetString(UInt64 addr, Encoding encoding, uint numNullTerminatorBytes, out string str, GetMemoryDelegate getMemory)
		{
			// Try to grab the entire string in one go at the start
			uint initialReadSize = 1024;
			byte[] initialReadBuf;
			for(;;)
			{
				initialReadBuf = new byte[initialReadSize];
				if(getMemory(addr, ref initialReadBuf))
				{
					break;
				}
				initialReadSize >>= 1;
				if(initialReadSize == 0)
				{
					str = "";
					return false;
				}
			}

			// Check if we have the entire string
			uint strlen;
			uint numZeroes = 0;
			for(strlen=0; strlen<initialReadSize; ++strlen)
			{
				if(initialReadBuf[strlen] == 0)
				{
					if(++numZeroes == numNullTerminatorBytes)
					{
						Array.Resize(ref initialReadBuf, (int)strlen+1-(int)numZeroes);
						str = encoding.GetString(initialReadBuf);
						return true;
					}
				}
				else
				{
					numZeroes = 0;
				}
			}

			// Still need to fetch the rest of the string
			var additionalBytes = new List<byte>();
			numZeroes = 0;
			for(;;)
			{
				var singleByte = new byte[1];
				if(!getMemory(addr+strlen, ref singleByte))
				{
					str = "";
					return false;
				}

				if(singleByte[0] == 0)
				{
					if(++numZeroes == numNullTerminatorBytes)
					{
						break;
					}
				}
				else
				{
					for(; numZeroes>0; --numZeroes)
					{
						additionalBytes.Add(0);
					}
					additionalBytes.Add(singleByte[0]);
					++strlen;
				}
			}
			Array.Resize(ref initialReadBuf, (int)strlen);
			Array.Copy(additionalBytes.ToArray(), 0, initialReadBuf, (int)initialReadSize, additionalBytes.Count);
			str = encoding.GetString(initialReadBuf);
			return true;
		}
	}
}
