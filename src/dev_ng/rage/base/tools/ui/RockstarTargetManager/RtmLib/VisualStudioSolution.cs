using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Diagnostics;
using System.IO;
using System.Text.RegularExpressions;

namespace Rockstar.TargetManager
{
    public class VisualStudioSolution
    {
        static public IEnumerable<Configuration> LoadSolution(string slnFilename)
        {
            Trace.WriteLine("Loading solution: " + slnFilename + "...");

            StreamReader sr = null;

            IEnumerable<Configuration> configs = null;

            try
            {
                FileInfo slnFinfo = new FileInfo(slnFilename);
                FileStream fs = slnFinfo.Open(FileMode.Open, FileAccess.Read);
                sr = new StreamReader(fs);

                string line;
                while(null != (line = sr.ReadLine()))
                {
                    //Find the name of the vcproj file.
                    Regex r = new Regex("Project([^\\,]*)\\,([^\\,]*)\\,");
                    Match m = r.Match(line);

                    if(3 == m.Groups.Count)
                    {
                        char[] trimChars = new char[1]{'\"'};
                        string projPath = m.Groups[2].Value.Trim().Trim(trimChars);

                        FileInfo projFinfo = new FileInfo(projPath);

                        if(!projFinfo.Exists)
                        {
                            projPath = slnFinfo.DirectoryName + "\\" + projPath;
                        }

                        //Trace.WriteLine(projPath);
                        if (Path.GetExtension(projPath).Equals(".vcproj", StringComparison.InvariantCultureIgnoreCase))
                        {
                            configs = VisualStudioSolution.LoadProject(slnFilename, projPath);
                        }
                        else if (Path.GetExtension(projPath).Equals(".vcxproj", StringComparison.InvariantCultureIgnoreCase))
                        {
                            configs = VisualStudioSolution.LoadProjectX(slnFilename, projPath);
                        }
                        else
                        {
                            Trace.WriteLine("Unknown project file extension: " + Path.GetExtension(projPath));
                        }

                        break;
                    }
                }
            }
            catch (System.Exception e)
            {
            	Trace.WriteLine(e.ToString());
                configs = null;
            }

            if(null != sr)
            {
                sr.Close();
            }

            if(null == configs)
            {
                configs = new List<Configuration>();
            }

            return configs;
        }

        static private IEnumerable<Configuration> LoadProject(string slnFilename, string projFilename)
        {
            Trace.WriteLine("Loading vcproj project: " + projFilename + "...");

            List<Configuration> configs = null;

            try
            {
                FileInfo slnFinfo = new FileInfo(slnFilename);
                FileInfo projFinfo = new FileInfo(projFilename);
                XmlDocument doc = new XmlDocument();
                doc.Load(projFilename);

                configs = new List<Configuration>();

                XmlElement proj = doc.DocumentElement;
                XmlAttribute projNameAttr = proj.Attributes["Name"];

                string slnDir = slnFinfo.DirectoryName + "\\";
                string slnPath = slnFinfo.FullName;
                string slnName = Path.GetFileNameWithoutExtension(slnFinfo.Name);
                string slnFileName = slnFinfo.Name;
                string slnExt = slnFinfo.Extension;

                string projDir = projFinfo.DirectoryName + "\\";
                string projPath = projFinfo.FullName;
                string projName = projNameAttr.Value;
                string projFileName = projFinfo.Name;
                string projExt = projFinfo.Extension;

                XmlElement configsEl = doc.DocumentElement["Configurations"];
                XmlElement configEl = (XmlElement) configsEl.FirstChild;

                for(; null != configEl; configEl = (XmlElement) configEl.NextSibling)
                {
                    try
                    {
                        string configName = configEl.Attributes["Name"].Value;

                        Configuration config =
                            new Configuration(configName);

                        if(TargetPlatformId.Unknown == config.PlatformId)
                        {
                            continue;
                        }

                        XmlElement toolEl = (XmlElement) configEl.FirstChild;
                        XmlElement linkerToolEl = null;
                        XmlElement image360ToolEl = null;
                        XmlElement deploy360ToolEl = null;

                        for(; null != toolEl; toolEl = (XmlElement) toolEl.NextSibling)
                        {
                            if(null == toolEl.Name || "Tool" != toolEl.Name)
                            {
                                continue;
                            }

                            XmlAttribute toolNameAttr = toolEl.Attributes["Name"];

                            if(null == toolNameAttr)
                            {
                                continue;
                            }

                            if(TargetPlatformId.Xbox360 == config.PlatformId)
                            {
                                if("VCX360LinkerTool" == toolNameAttr.Value)
                                {
                                    linkerToolEl = toolEl;
                                }
                                else if("VCX360ImageTool" == toolNameAttr.Value)
                                {
                                    image360ToolEl = toolEl;
                                }
                                else if("VCX360DeploymentTool" == toolNameAttr.Value)
                                {
                                    deploy360ToolEl = toolEl;
                                }
                            }
                            else if(TargetPlatformId.PS3 == config.PlatformId)
                            {
                                if("VCLinkerTool" == toolNameAttr.Value)
                                {
                                    linkerToolEl = toolEl;
                                }
                            }
                        }

                        config.SetEnvVar("SolutionDir", slnDir);
                        config.SetEnvVar("SolutionPath", slnPath);
                        config.SetEnvVar("SolutionName", slnName);
                        config.SetEnvVar("SolutionFileName", slnFileName);
                        config.SetEnvVar("SolutionExt", slnExt);

                        config.SetEnvVar("ProjectDir", projDir);
                        config.SetEnvVar("ProjectPath", projPath);
                        config.SetEnvVar("ProjectName", projName);
                        config.SetEnvVar("ProjectFileName", projFileName);
                        config.SetEnvVar("ProjectExt", projExt);

                        char[] separator = new char[1]{'|'};
                        string[] configStrs = configName.Split(separator);

                        config.SetEnvVar("ConfigurationName", configStrs[0]);
                        config.SetEnvVar("PlatformName", configStrs[1]);

                        //Environment.SetEnvironmentVariable("BUILD_FOLDER", "x:\\jimmy\\build\\dev");

                        XmlAttribute outDirAttr = configEl.Attributes["OutputDirectory"];
                        XmlAttribute intDirAttr = configEl.Attributes["IntermediateDirectory"];
                        
                        if(null == outDirAttr || null == intDirAttr)
                        {
                            continue;
                        }

                        config.SetEnvVar("OutDir", outDirAttr.Value);
                        config.SetEnvVar("IntDir", intDirAttr.Value);

                        if(TargetPlatformId.Xbox360 == config.PlatformId)
                        {
                            XmlAttribute imageFileNameAttr = image360ToolEl.Attributes["OutputFileName"];
                            string imagePath = config.ExpandEnvVars(imageFileNameAttr.Value);
                            if(!Path.IsPathRooted(imagePath))
                            {
                                imagePath = projDir + imagePath;
                            }

                            config.SetEnvVar("ImagePath", imagePath);

                            XmlAttribute remoteRootAttr = deploy360ToolEl.Attributes["RemoteRoot"];
                            string remoteRoot = "devkit:\\$(SolutionName)";
                            
                            if(null != remoteRootAttr)
                            {
                                remoteRoot = remoteRootAttr.Value;
                            }

                            config.SetEnvVar("RemoteRoot", remoteRoot);

                            FileInfo imgFinfo = new FileInfo(config.GetEnvVar("ImagePath"));
                            config.SetEnvVar("ImageDir", imgFinfo.DirectoryName + "\\");
                            config.SetEnvVar("ImageFileName", imgFinfo.Name);
                            config.SetEnvVar("ImageName", Path.GetFileNameWithoutExtension(imgFinfo.Name));
                            config.SetEnvVar("ImageExt", imgFinfo.Extension);

                            XmlAttribute targetFileAttr = linkerToolEl.Attributes["OutputFile"];
                            string targetPath = config.ExpandEnvVars(targetFileAttr.Value);
                            if(!Path.IsPathRooted(targetPath))
                            {
                                targetPath = projDir + targetPath;
                            }

                            config.SetEnvVar("TargetPath", targetPath);

                            FileInfo targetFinfo = new FileInfo(config.GetEnvVar("TargetPath"));
                            config.SetEnvVar("TargetDir", targetFinfo.DirectoryName + "\\");
                            config.SetEnvVar("TargetFileName", targetFinfo.Name);
                            config.SetEnvVar("TargetName", Path.GetFileNameWithoutExtension(targetFinfo.Name));
                            config.SetEnvVar("TargetExt", targetFinfo.Extension);

                            string remotePath = remoteRoot + "\\" + config.GetEnvVar("ImageFileName");
                            config.SetEnvVar("RemotePath", remotePath);

                            XmlAttribute deploymentFilesAttr = deploy360ToolEl.Attributes["DeploymentFiles"];
                            string dfStr = (null != deploymentFilesAttr) ? deploymentFilesAttr.Value : "$(RemoteRoot)=$(ImagePath)";
                            config.SetDeploymentFiles(dfStr);
                        }
                        else if(TargetPlatformId.PS3 == config.PlatformId)
                        {
                            XmlAttribute outputFileAttr = linkerToolEl.Attributes["OutputFile"];
                            string outputFile = config.ExpandEnvVars(outputFileAttr.Value);
                            string fullPath =
                                Path.IsPathRooted(outputFile)
                                ? outputFile
                                : projDir + outputFile;
                            string dirName = Path.GetDirectoryName(fullPath) + "\\";
                            string name = Path.GetFileNameWithoutExtension(fullPath);
                            string filename = Path.GetFileName(fullPath);
                            string ext = Path.GetExtension(fullPath);
                            config.SetEnvVar("TargetDir", dirName);
                            config.SetEnvVar("TargetPath", fullPath);
                            config.SetEnvVar("TargetName", name);
                            config.SetEnvVar("TargetFileName", filename);
                            config.SetEnvVar("TargetExt", ext);
                        }

                        configs.Add(config);
                    }
                    catch (System.Exception e)
                    {
                        Trace.WriteLine(e.ToString());
                    }
                }
            }
            catch (System.Exception e)
            {
            	Trace.WriteLine(e.ToString());
                configs = null;
            }

            if(null == configs)
            {
                configs = new List<Configuration>();
            }

            return configs;
        }

        static private IEnumerable<Configuration> LoadProjectX(string slnFilename, string projFilename)
        {
            Trace.WriteLine("Loading vcxproj project: " + projFilename + "...");

            List<Configuration> configs = null;

            try
            {
                FileInfo slnFinfo = new FileInfo(slnFilename);
                FileInfo projFinfo = new FileInfo(projFilename);
                XmlDocument doc = new XmlDocument();
                doc.Load(projFilename);

                configs = new List<Configuration>();

                XmlElement proj = doc.DocumentElement;

                string slnDir = slnFinfo.DirectoryName + "\\";
                string slnPath = slnFinfo.FullName;
                string slnName = Path.GetFileNameWithoutExtension(slnFinfo.Name);
                string slnFileName = slnFinfo.Name;
                string slnExt = slnFinfo.Extension;

                string projDir = projFinfo.DirectoryName + "\\";
                string projPath = projFinfo.FullName;
                string projName = "Unknown"; // filled in below
                string projFileName = projFinfo.Name;
                string projExt = projFinfo.Extension;

				bool gotName = false;
				bool gotConfigs = false;

                // Process all ItemGroup's and PropertyGroup's
				for (XmlElement itemGroup = (XmlElement)doc.DocumentElement.FirstChild;
					itemGroup != null; itemGroup = (XmlElement)itemGroup.NextSibling)
				{
					// Bail once we've got all the information we need
					if (gotName && gotConfigs)
					{
						break;
					}

					if (itemGroup.Name == "ItemGroup")
					{
						// Find the "ProjectConfigurations" ItemGroup
						string label = itemGroup.GetAttribute("Label");
						if (label == null || label != "ProjectConfigurations")
						{
							continue;
						}

						// Process all ProjectConfiguration's
						for (XmlElement configEl = (XmlElement)itemGroup.FirstChild;
							configEl != null; configEl = (XmlElement)configEl.NextSibling)
						{
							try
							{
								string configName = configEl.Attributes["Include"].Value;

								Configuration config =
									new Configuration(configName);

								if (TargetPlatformId.Unknown == config.PlatformId)
								{
									continue;
								}

								config.SetEnvVar("SolutionDir", slnDir);
								config.SetEnvVar("SolutionPath", slnPath);
								config.SetEnvVar("SolutionName", slnName);
								config.SetEnvVar("SolutionFileName", slnFileName);
								config.SetEnvVar("SolutionExt", slnExt);

								config.SetEnvVar("ProjectDir", projDir);
								config.SetEnvVar("ProjectPath", projPath);
								config.SetEnvVar("ProjectName", projName);
								config.SetEnvVar("ProjectFileName", projFileName);
								config.SetEnvVar("ProjectExt", projExt);

								char[] separator = new char[1] { '|' };
								string[] configStrs = configName.Split(separator);

								config.SetEnvVar("ConfigurationName", configStrs[0]);
								config.SetEnvVar("PlatformName", configStrs[1]);

								configs.Add(config);
							}
							catch (System.Exception e)
							{
								Trace.WriteLine(e.ToString());
							}
						}
						gotConfigs = true;
					}
					else if (itemGroup.Name == "PropertyGroup")
					{
						// Find the "Globals" ItemGroup
						string label = itemGroup.GetAttribute("Label");
						if (label == null || label != "Globals")
						{
							continue;
						}

						// Find <ProjectName> element
						for (XmlElement propertyEl = (XmlElement)itemGroup.FirstChild;
							propertyEl != null; propertyEl = (XmlElement)propertyEl.NextSibling)
						{
							if (propertyEl.Name == "ProjectName")
							{
								projName = propertyEl.FirstChild.Value;
								gotName = true;
								break;
							}
						}
					}
					else
					{
						continue;
					}
				}
            }
            catch (System.Exception e)
            {
            	Trace.WriteLine(e.ToString());
                configs = null;
            }

            if(null == configs)
            {
                configs = new List<Configuration>();
            }

            return configs;
        }
    }
}
