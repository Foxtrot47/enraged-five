using System;
using System.Reflection;
using System.CodeDom.Compiler;
using Microsoft.CSharp;
using Microsoft.VisualBasic;
using System.IO;
using System.CodeDom;
using System.Collections;
using System.Collections.Generic;
using Rockstar.TargetManager;
using System.Diagnostics;

namespace Rockstar.TargetManager
{
	public abstract class ScriptInterface : MarshalByRefObject
	{
		public abstract string Name
		{
			get;
		}
	}

	public class ScriptContextBase
	{
		public static TargetManager.Target GetRtmTarget(Target t)
		{
			return t.m_RtmTarget;
		}

		public class Target : ScriptInterface
		{
			internal TargetManager.Target m_RtmTarget;

			public Target(TargetManager.Target t)
			{
				m_RtmTarget = t;
			}

			public bool Deploy()
			{
				return m_RtmTarget.Deploy(TargetManager.DeployType.Normal);
			}

			public bool Launch()
			{
				return m_RtmTarget.Launch();
			}

			public void Reboot()
			{
				m_RtmTarget.Reboot();
			}

			public bool IsActive
			{
				get{return m_RtmTarget.Enabled;}
				set{m_RtmTarget.Enabled = value;}
			}

			public override string Name
			{
				get{return m_RtmTarget.Name;}
			}

			public TargetPlatformId PlatformId
			{
				get{return m_RtmTarget.PlatformId;}
			}

			public string CommandLine
			{
				get{return m_RtmTarget.CommandLine;}
				set{m_RtmTarget.CommandLine = value;}
			}
		}

		public interface IScriptHostImpl
		{
			void WriteLine(string s);

			void LoadWorkspace(string filename);

			bool AddTarget(string name, TargetPlatformId platformId);

			void RemoveTarget(string name);

			TargetManager.Target GetTarget(string name);

			void DeployAll(TargetPlatformId platformId);
			void LaunchAll(TargetPlatformId platformId);
			void RebootAll(TargetPlatformId platformId);
			void StopAll(TargetPlatformId platformId);
			void StartAll(TargetPlatformId platformId);

			IEnumerable<Target> Targets
			{
				get;
			}

			string GetCommandLine(TargetPlatformId platformId);
			void SetCommandLine(TargetPlatformId platformId, string str);
		}

		public class ScriptHost : ScriptInterface
		{
			IScriptHostImpl m_ShImpl;

			public ScriptHost(IScriptHostImpl shImpl)
			{
				m_ShImpl = shImpl;
			}

			public void WriteLine(string s)
			{
				m_ShImpl.WriteLine(s);
			}

			public void LoadWorkspace(string filename)
			{
				m_ShImpl.LoadWorkspace(filename);
			}

			public bool AddTarget(string name, TargetPlatformId platformId)
			{
				return m_ShImpl.AddTarget(name, platformId);
			}

			public void RemoveTarget(string name)
			{
				m_ShImpl.RemoveTarget(name);
			}

			public Target GetTarget(string name)
			{
				TargetManager.Target t = m_ShImpl.GetTarget(name);

				return (null != t) ? new Target(t) : null;
			}

			public void DeployAll(TargetPlatformId platformId)
			{
				m_ShImpl.DeployAll(platformId);
			}

			public void LaunchAll(TargetPlatformId platformId)
			{
				m_ShImpl.LaunchAll(platformId);
			}

			public void RebootAll(TargetPlatformId platformId)
			{
				m_ShImpl.RebootAll(platformId);
			}

			public void StopAll(TargetPlatformId platformId)
			{
				m_ShImpl.StopAll(platformId);
			}

			public void StartAll(TargetPlatformId platformId)
			{
				m_ShImpl.StartAll(platformId);
			}

			public IEnumerable<Target> Targets
			{
				get
				{
					return m_ShImpl.Targets;
				}
			}

			public string Ps3CommandLine
			{
				get{return m_ShImpl.GetCommandLine(TargetPlatformId.PS3);}
				set{m_ShImpl.SetCommandLine(TargetPlatformId.PS3, value);}
			}

			public string Ps4CommandLine
			{
				get{return m_ShImpl.GetCommandLine(TargetPlatformId.PS4);}
				set{m_ShImpl.SetCommandLine(TargetPlatformId.PS4, value);}
			}

			public string Xbox360CommandLine
			{
				get{return m_ShImpl.GetCommandLine(TargetPlatformId.Xbox360);}
				set{m_ShImpl.SetCommandLine(TargetPlatformId.Xbox360, value);}
			}

			public string XB1CommandLine
			{
				get{return m_ShImpl.GetCommandLine(TargetPlatformId.XB1);}
				set{m_ShImpl.SetCommandLine(TargetPlatformId.XB1, value);}
			}

			public string PCCommandLine
			{
				get{return m_ShImpl.GetCommandLine(TargetPlatformId.PC);}
				set{m_ShImpl.SetCommandLine(TargetPlatformId.PC, value);}
			}

			public override string Name
			{
				get{return "Host";}
			}
		}
	}

	public class ScriptEngine : MarshalByRefObject
	{
		Dictionary<string, ScriptInterface> m_Interfaces =
			new Dictionary<string, ScriptInterface>();
		ScriptContextBase.ScriptHost m_ScriptHost;
		string m_WorkingDir;

		public ScriptEngine(ScriptContextBase.IScriptHostImpl shImpl,
							string workingDir)
		{
			m_ScriptHost = new ScriptContextBase.ScriptHost(shImpl);
			m_WorkingDir = workingDir;
			//this.AddInterface(m_ScriptHost);
		}

		public void AddInterface(ScriptInterface iface)
		{
			if(m_Interfaces.ContainsKey(iface.Name))
			{
				this.WriteLine("Already have an interface called " + iface.Name);
			}
			else
			{
				m_Interfaces.Add(iface.Name, iface);
			}
		}

		public void RemoveInterface(ScriptInterface iface)
		{
			m_Interfaces.Remove(iface.Name);
		}

		public void WriteLine(string s)
		{
			Trace.WriteLine(s);
		}

		public void RunFile(string filename)
		{
			if(File.Exists(filename))
			{
				StreamReader sr = null;
				string code = null;

				try
				{
					sr = new StreamReader(filename);
					code = sr.ReadToEnd();
					sr.Close();
				}
				catch (System.Exception ex)
				{
					m_ScriptHost.WriteLine(ex.ToString());
				}
				finally
				{
					if(null != sr)
					{
						sr.Dispose();
					}
				}

				if(null != code)
				{
					this.Run(code);
				}
			}
		}

		//Each time we compile a script a new assembly is created and
		//loaded.  As assemblies can't be unloaded from AppDomains the
		//result is a resource leak.  Thus we compile and run in a separate
		//AppDomain and then unload the AppDomain.

		public void Run(string script)
		{
			AppDomain appDomain = this.CreateAppDomain();
			Type compilerType = typeof(Compiler);
			Compiler compiler =
				(Compiler) appDomain.CreateInstanceAndUnwrap(compilerType.Assembly.FullName, compilerType.FullName);

			compiler.Run(m_ScriptHost, m_Interfaces.Values, script);

			AppDomain.Unload(appDomain);
		}

		public void Compile(string script)
		{
			AppDomain appDomain = this.CreateAppDomain();
			Type compilerType = typeof(Compiler);
			Compiler compiler =
				(Compiler) appDomain.CreateInstanceAndUnwrap(compilerType.Assembly.FullName, compilerType.FullName);

			//Call CompileConly() because it doesn't return an Assembly
			//that would need to cross AppDomains.
			compiler.CompileOnly(m_ScriptHost, m_Interfaces.Values, script);

			AppDomain.Unload(appDomain);
		}

		AppDomain CreateAppDomain()
		{
			AppDomainSetup setup = new AppDomainSetup();

			setup.ApplicationBase = m_WorkingDir;

			Dictionary<string, string> paths = new Dictionary<string, string>();

			//For each interface add it's assembly path
			//to the app domain.
			foreach(ScriptInterface si in m_Interfaces.Values)
			{
				string path = si.GetType().Assembly.Location;

				if(path.Length <= m_WorkingDir.Length
					|| path.IndexOf(m_WorkingDir) != 0)
				{
					Debug.Fail("Can't load " + path + " into script AppDomain");
				}
				else
				{
					path = Path.GetDirectoryName(path);
					path = path.Substring(m_WorkingDir.Length);

					while(path.Length > 0 &&
						(path[0] == '\\' || path[0] == '/'))
					{
						path = path.Substring(1, path.Length - 1);
					}
				}

				if(path.Length > 0 && !paths.ContainsKey(path))
				{
					paths.Add(path, path);
				}
			}

			foreach(string path in paths.Values)
			{
				if(null == setup.PrivateBinPath)
				{
					setup.PrivateBinPath = path;
				}
				else
				{
					setup.PrivateBinPath += ";" + path;
				}
			}

			return AppDomain.CreateDomain("", null, setup);
		}

		class Compiler : MarshalByRefObject
		{
			//In-memory assemblies (created by dynamically compiling script)
			//cannot cross AppDomains, so we have CompileOnly() which can be
			//called from a different AppDomain and doesn't return an Assembly
			//(like Compile()) that would cross AppDomains.
			public void CompileOnly(ScriptContextBase.ScriptHost scriptHost,
									IEnumerable<ScriptInterface> interfaces,
									string script)
			{
				this.Compile(scriptHost, interfaces, script);
			}

			public Assembly Compile(ScriptContextBase.ScriptHost scriptHost,
									IEnumerable<ScriptInterface> interfaces,
									string script)
			{
				Assembly assembly = null;

				string language = "c#";
				CodeDomProvider compiler = CodeDomProvider.CreateProvider(language);

				if(null == compiler
					|| "c++" == language
					|| "cpp" == language
					|| compiler.FileExtension == ".h")
				{
					//c++ code provider throws a NotImplementedException when
					//calling CompileAssemblyFromSource();
					scriptHost.WriteLine(language + " is not a supported language");
				}
				else
				{
					CompilerParameters options = new CompilerParameters();

					//options.ReferencedAssemblies.Add("System.dll");
					//options.ReferencedAssemblies.Add("System.Windows.Forms.dll");
					options.GenerateInMemory = true;

					string code = @"namespace Rockstar.TargetManager {" + Environment.NewLine;
					code += @"public class ScriptContext:ScriptContextBase {" + Environment.NewLine;
					code += @"static public void Run(ScriptContextBase.ScriptHost Host";

					string asmName = Assembly.GetAssembly(scriptHost.GetType()).Location;
					options.ReferencedAssemblies.Add(asmName);

					foreach(ScriptInterface iface in interfaces)
					{
						Type ifaceType = iface.GetType();
						options.ReferencedAssemblies.Add(ifaceType.Assembly.Location);
						string classname = ifaceType.FullName.Replace('+', '.');

						code += "," + Environment.NewLine;
						code += classname + " " + iface.Name;
					}

					code += @") {" + Environment.NewLine;
					code += "try{" + Environment.NewLine + script + Environment.NewLine + "}";
					code += "catch(System.Exception e){Host.WriteLine(e.ToString());}}}}";

					CompilerResults compilerResults =
					   compiler.CompileAssemblyFromSource(options, code);

					if(compilerResults.Errors.HasErrors)
					{
						string errorMsg = "";
						errorMsg = compilerResults.Errors.Count.ToString() + " Errors:";

						for(int i = 0; i < compilerResults.Errors.Count; i++)
						{
							CompilerError ce = compilerResults.Errors[i];

							string err = String.Format("Error({0},{1}): error {2}: {3}",
														ce.Line - 1,
														ce.Column,
														ce.ErrorNumber,
														ce.ErrorText);
							scriptHost.WriteLine(err);
						}

						scriptHost.WriteLine("Compile: FAILED");
					}
					else
					{
						assembly = compilerResults.CompiledAssembly;
						scriptHost.WriteLine("Compile: OK");
					}
				}

				compiler.Dispose();

				return assembly;
			}

			public void Run(ScriptContextBase.ScriptHost scriptHost,
							IEnumerable<ScriptInterface> interfaces,
							string script)
			{
				Assembly assembly =
					this.Compile(scriptHost, interfaces, script);
				
				if(null != assembly)
				{
					Type type = assembly.GetType("Rockstar.TargetManager.ScriptContext");

					if(null == type)
					{
						scriptHost.WriteLine("Couldn't load class.");
						return;
					}

					try
					{
						List<object> args = new List<object>();

						args.Add(scriptHost);

						foreach(ScriptInterface iface in interfaces)
						{
							args.Add(iface);
						}

						type.InvokeMember("Run",
											BindingFlags.InvokeMethod,
											null,
											null,
											args.ToArray());
					}
					catch(System.Exception e)
					{
						scriptHost.WriteLine(e.ToString());
					}
				}
			}
		}
	}
}
