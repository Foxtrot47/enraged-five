using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Net;
using System.Net.NetworkInformation;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using XDevkit;

namespace Rockstar.TargetManager
{
	////////////////////////////////////////////////////////////////////////////
	// XboxTargetManager
	////////////////////////////////////////////////////////////////////////////
	public class XboxTargetManager : IDisposable
	{
		private bool m_WorkerThreadExit = false;
		private Thread m_WorkerThread;
		private XboxManagerClass m_Xbm;
		private List<XboxTarget> m_Targets = new List<XboxTarget>();
		private Dictionary<string, XboxTarget> m_TargetsByName = new Dictionary<string, XboxTarget>();

		////////////////////////////////////////////////////////////////////////
		// XboxTargetManager
		////////////////////////////////////////////////////////////////////////
		public XboxTargetManager(NetworkTtyListener networkTtyListener)
		{
			try
			{
				// This will throw if the XDK is not installed
				m_Xbm = new XboxManagerClass();

				m_WorkerThread = new Thread(UpdateXboxes);
				m_WorkerThread.Name = "XboxTargetManager::UpdateXboxes";
				m_WorkerThread.Start();
			}
			catch
			{
				m_Xbm = null;
			}
		}

		////////////////////////////////////////////////////////////////////////
		// ~XboxTargetManager
		////////////////////////////////////////////////////////////////////////
		~XboxTargetManager()
		{
			Dispose(false);
		}

		////////////////////////////////////////////////////////////////////////
		// Dispose
		////////////////////////////////////////////////////////////////////////
		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		////////////////////////////////////////////////////////////////////////
		// Dispose
		////////////////////////////////////////////////////////////////////////
		protected virtual void Dispose(bool disposing)
		{
			if(disposing)
			{
				if(m_WorkerThread != null)
				{
					m_WorkerThreadExit = true;
					m_WorkerThread.Join();
				}

				if(m_Targets != null)
				{
					foreach(XboxTarget target in m_Targets)
					{
						target.Dispose();
					}
				}
			}
		}

		////////////////////////////////////////////////////////////////////////
		// UpdateXboxes
		////////////////////////////////////////////////////////////////////////
		private void UpdateXboxes()
		{
			while(!m_WorkerThreadExit)
			{
				// List could be changed while we traverse it (maybe)
				List<XboxTarget> targetListCopy = new List<XboxTarget>(m_Targets);

				foreach(XboxTarget target in targetListCopy)
				{
					// Check whether any of the targets is flagged for launch or deploy (a bit odd to do this here).
					bool launchCheck = true;
					while(launchCheck)
					{
						launchCheck = false;

						foreach(XboxTarget launchTarget in targetListCopy)
						{
							if(target.m_LaunchRequested)
							{
								target.LaunchFromThread();
								target.m_LaunchRequested = false;
								launchCheck = true;
							}

							if(target.m_CoredumpRequested)
							{
								target.CoredumpFromThread();
								target.m_CoredumpRequested = false;
								launchCheck = true;
							}

							if(target.m_DeployRequested != DeployType.None)
							{
								target.DeployFromThread(target.m_DeployRequested);
								target.m_DeployRequested = DeployType.None;
								launchCheck = true;
							}
						}
					}

					XboxConsole xbc = null;
					try
					{
						xbc = m_Xbm.OpenConsole(target.Name);

						// Do a ping check (with 50ms timeout) before doing a full check (much longer timeout).
						if(new Ping().Send(XboxUintToIPAddress(xbc.IPAddress), 50).Status == IPStatus.Success)
						{
							// Try to 'find' the console - will give an exception if it fails.
							xbc.FindConsole(0, 0);
						}
						else
							xbc = null;
					}
					catch(System.Exception /*e*/)
					{
						xbc = null;
					}

					bool wasRunnable = target.Runnable;
					bool isRunnable = (null != xbc);

					if(isRunnable)
					{
						if(!wasRunnable)
						{
							target.SetConsole(xbc);
						}
						else
						{
							target.LastRunnableTime = DateTime.Now;
						}
					}
					else if(!isRunnable && wasRunnable)
					{
						if((DateTime.Now-target.LastRunnableTime).Seconds > 10)
						{
							target.SetConsole(null);
						}
					}

					if(isRunnable)
					{
						// If this is just newly runnable, then possible that
						// SetConsole has failed, so need to check m_Xbc is
						// non-null.
						if(target.Runnable)
						{
							target.ConnectToGame();
						}
					}
				}

				Thread.Sleep(500);
			}
		}

		////////////////////////////////////////////////////////////////////////
		// GetDefaultTargetNames
		////////////////////////////////////////////////////////////////////////
		public string[] GetDefaultTargetNames()
		{
			if (m_Xbm == null)
			{
				return new string[0];
			}
			else
			{
				string[] names = new string[m_Xbm.Consoles.Count];

				for(int i = 0; i < m_Xbm.Consoles.Count; ++i)
				{
					names[i] = m_Xbm.Consoles[i];
				}

				return names;
			}
		}

		////////////////////////////////////////////////////////////////////////
		// AddTarget
		////////////////////////////////////////////////////////////////////////
		public Target AddTarget(string name)
		{
			if (m_Xbm == null)
			{
				return null;
			}

			Trace.WriteLine("Acquiring target " + name + "...");

			XboxTarget target = null;
			bool success = false;

			if (m_TargetsByName.TryGetValue(name, out target))
			{
				success = true;
			}
			else
			{
				try
				{
					target = new XboxTarget(name);

					m_Targets.Add(target);
					m_TargetsByName.Add(name, target);

					success = true;
				}
				catch (Exception)
				{
				}
			}

			return success ? target : null;
		}

		////////////////////////////////////////////////////////////////////////
		// XboxUintToIPAddress
		////////////////////////////////////////////////////////////////////////
		public static System.Net.IPAddress XboxUintToIPAddress(uint xboxip)
		{
			//Reverse the endian IP address
			xboxip = (uint)((((xboxip & 0x000000FF) << 24)
						   | ((xboxip & 0x0000FF00) << 8)
						   | ((xboxip & 0x00FF0000) >> 8)
						   | ((xboxip & 0xFF000000) >> 24)));

			return new System.Net.IPAddress(xboxip);
		}
	}

	////////////////////////////////////////////////////////////////////////////
	// XboxTarget
	////////////////////////////////////////////////////////////////////////////
	public class XboxTarget : Target
	{
		private const string DEBUGGER_NAME = "Rockstar Target Manager";

		private const string MEM_LIST_CALLBACK_NAME           = "rage::sysMemListPhysicalAllocs(void)";
		private const string TARGET_CRASH_CALLBACK_NAME       = "rage::sysStack::TargetCrashCallback(u32)";
		private const string GET_BUGSTAR_CONFIG_CALLBACK_NAME = "CBugstarIntegration::GetRockstarTargetManagerConfig(void)";

		internal bool m_LaunchRequested = false;
		internal bool m_CoredumpRequested = false;
		internal DeployType m_DeployRequested = DeployType.None;

		private XboxConsole m_Xbc = null;
		private IXboxDebugTarget m_Xbdb = null;
		private UInt32 m_TargetBreakAddr = 0;
		private IXboxThread m_TargetThread = null;
		private string m_Name = "";
		private string m_Alias = null;
		private UInt32 m_ProcessId;
		private string m_ProgramName;
		private uint m_TtyDisableCount = 0;
		private bool m_IsStopped = true;
		private bool m_StartNewLog = false;
		private string m_CmpFileCache = null;
		private delegate void OnEventCallback();
		private delegate void OnEventCallbackArgBoolUInt64(bool b, UInt64 u64);
		private OnEventCallback m_OnStopCallback = null;

		private System.Object m_Lock = new System.Object();

		private class TestkitCrashState
		{
			public Bugstar.Config BugstarConfig;
			public string BugSummary = null;
			public string BugDescription = "";
			public List<string> FilesToCopy = new List<string>();
			public UInt32 ProcessId;
			public string ProgramName;
			public bool ProcessingDump = false;
		};

		private TestkitCrashState m_TestkitCrashState = null;



		////////////////////////////////////////////////////////////////////////
		// XboxTarget
		////////////////////////////////////////////////////////////////////////
		public XboxTarget(string name)
		{
			m_Name = name;
			m_Alias = null;
		}

		////////////////////////////////////////////////////////////////////////
		// Dispose
		////////////////////////////////////////////////////////////////////////
		protected override void Dispose(bool disposing)
		{
			try
			{
				if(disposing)
				{
					SetConsole(null);

					if(m_CmpFileCache != null && File.Exists(m_CmpFileCache))
					{
						try
						{
							File.Delete(m_CmpFileCache);
						}
						catch
						{
						}
					}
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		////////////////////////////////////////////////////////////////////////
		// SelectedIcon
		////////////////////////////////////////////////////////////////////////
		public override Icon SelectedIcon
		{
			get
			{
				if(m_Xbc == null)
				{
					return Icon.NOT_AVAILABLE;
				}
				else if(m_Xbdb == null || m_IsStopped)
				{
					return Icon.NOT_RUNNING;
				}
				else
				{
					return Icon.RUNNING;
				}
			}
		}

		////////////////////////////////////////////////////////////////////////
		// OnEnabledChanged
		////////////////////////////////////////////////////////////////////////
		protected override void OnEnabledChanged(bool enabled)
		{
			if(!enabled)
			{
				SetConsole(null);
			}
		}

		////////////////////////////////////////////////////////////////////////
		// SetConsole
		////////////////////////////////////////////////////////////////////////
		public void SetConsole(XboxConsole xbc)
		{
			// WARNING: The OnStdNotify event handler must not be changed while
			// m_Lock is held, else there is a deadlock condition where another
			// thread may be in the event handler then try to acquire m_Lock.
			//
			// No other thread writes to m_Xbc (except the ctor), so we do not
			// need to get a reference inside the lock either.
			//
			if(null != m_Xbc)
			{
				try
				{
					m_Xbc.OnStdNotify -= xboxConsole_OnStdNotify;
				}
				catch(System.Exception e)
				{
					Trace.WriteLine(e);
				}
			}

			// If the target is not enabled, then don't allow a connection
			if (!Enabled)
			{
				xbc = null;
			}

			lock(m_Lock)
			{
				if(null != m_Xbc)
				{
					Trace.WriteLine("Lost " + FullName);
					TargetEvent.Invoke(CreateEvent(TargetEventType.Lost, "Lost"));
					m_Xbc = null;
					m_Xbdb = null;
					m_TargetBreakAddr = 0;
					m_TargetThread = null;
				}
				if(null != xbc)
				{
					// I've seen an instance where adding the handler
					// throws an exception.	I believe it was a problematic
					// xbox.
					//
					// Don't know the exact origin of the previous comment, in
					// particular "I believe it was a problematic xbox.".  But
					// have also seen adding the handler throw after a overnight
					// test with the xbox still running.  Retrying did not ever
					// fix it, but restarting the R*TM (without restarting the
					// xbox), did fix it.
					//
					try
					{
						xbc.OnStdNotify  += xboxConsole_OnStdNotify;
						m_Xbc = xbc;
						m_Alias = m_Xbc.Name;
						LastRunnableTime = DateTime.Now;
						Trace.WriteLine("Acquired " + FullName);
						TargetEvent.Invoke(CreateEvent(TargetEventType.Acquired, "Acquired"));

						ConnectToGame();
					}
					catch(System.Exception /*e*/)
					{
						//m_IsBadXbox = true;
						m_Xbc = null;
						m_Xbdb = null;

						Trace.WriteLine("Failed to acquire " + FullName);
					}
				}
			}
		}

		////////////////////////////////////////////////////////////////////////
		// ConnectToGame
		////////////////////////////////////////////////////////////////////////
		public void ConnectToGame()
		{
			try
			{
				UInt32 pid  = m_Xbc.RunningProcessInfo.ProcessId;
				string name = m_Xbc.RunningProcessInfo.ProgramName;
				m_ProcessId   = pid;
				m_ProgramName = name;
			}
			catch
			{
				return;
			}

			// Just return if already connected
			if(m_Xbdb != null)
			{
				return;
			}

			IXboxDebugTarget xbdb = m_Xbc.DebugTarget;
			try
			{
				xbdb.ConnectAsDebugger(DEBUGGER_NAME, XboxDebugConnectFlags.Force);
			}
			catch
			{
				return;
			}

			// Check if there is a thread called "R*TM" running on the target.
			try
			{
				foreach(IXboxThread t in xbdb.Threads)
				{
					if(t.ThreadInfo.Name == "R*TM")
					{
						// Check for the signature "R*TM" in r31 to make
						// sure thread has reached the main loop.  Note that
						// we may need to walk up the stack to find this.
						for(IXboxStackFrame sf=t.TopOfStack; sf!=null; sf=sf.NextStackFrame)
						{
							Int64 r1, r31;
							sf.GetRegister64(XboxRegisters64.r1,  out r1);
							sf.GetRegister64(XboxRegisters64.r31, out r31);

							if((r31>>32)==0x522a544d && (r31&0xffffffff)==(r1&0xffffffff))
							{
								Int64 r30;
								sf.GetRegister64(XboxRegisters64.r30, out r30);
								m_TargetBreakAddr = (UInt32)r30;
								m_TargetThread = t;

								// Let the target know we have connected
								byte[] data = {1};
								UInt32 bytesWritten;
								xbdb.SetMemory(m_TargetBreakAddr, 1, data, out bytesWritten);

								m_Xbdb = xbdb;
								return;
							}
						}
					}
				}
			}
			catch
			{
			}
			xbdb.DisconnectAsDebugger();
		}

		////////////////////////////////////////////////////////////////////////
		// Deploy
		////////////////////////////////////////////////////////////////////////
		public override bool Deploy(DeployType dt)
		{
			m_DeployRequested = dt;
			return true;
		}

		////////////////////////////////////////////////////////////////////////
		// EnableDeploy
		////////////////////////////////////////////////////////////////////////
		public override bool EnableDeploy
		{
			get{return Config.IsValid && Runnable;}
		}

		////////////////////////////////////////////////////////////////////////
		// DeployFromThread
		////////////////////////////////////////////////////////////////////////
		public bool DeployFromThread(DeployType dt)
		{
			bool success = true;

			Trace.WriteLine(String.Format("Deploying files to {0}...",
										FullName));

			foreach(Configuration.DeploymentFile df in Config.DeploymentFiles)
			{
				string src = Config.ExpandEnvVars(df.Src);
				string dstdir = Config.ExpandEnvVars(df.DstDir);

				FileInfo srcFinfo = new FileInfo(src);
				string dst = dstdir + "\\" + srcFinfo.Name;

				Trace.Write(string.Format("Deploying {0} to {1}...",
										srcFinfo.FullName,
										dst));

				if(srcFinfo.Exists)
				{
					IXboxFile xbf = null;

					try
					{
						xbf = m_Xbc.GetFileObject(dstdir);
					}
					catch (System.Exception /*e*/)
					{
						xbf = null;
					}

					if(null == xbf)
					{
						try
						{
							m_Xbc.MakeDirectory(dstdir);
							xbf = m_Xbc.GetFileObject(dstdir);
						}
						catch(System.Exception /*e*/)
						{
							xbf = null;
						}
					}

					if(null != xbf && xbf.IsDirectory)
					{
						try
						{
							xbf = m_Xbc.GetFileObject(dst);
						}
						catch (System.Exception /*e*/)
						{
							xbf = null;
						}

						DateTime xbfDateTime = srcFinfo.LastWriteTime;

						if(null != xbf)
						{
							try
							{
								xbfDateTime = (DateTime) xbf.ChangeTime;
							}
							catch (System.Exception /*e*/)
							{
								//Trace.WriteLine(String.Format(" EXCEPTION THROWN getting Change time for {0}.	 Try deleting the file and trying again", dst));
								//xbf = null;
								//success = false;
								xbf = null;
							}
						}

						if(null == xbf
							|| DeployType.Force == dt
							|| xbfDateTime < srcFinfo.LastWriteTime
							|| xbf.Size != (ulong) srcFinfo.Length
							|| srcFinfo.Length < 4096)	// Force small files to be copied - usually rfs.dat
						{
							m_Xbc.SendFile(srcFinfo.FullName, dst);
							Trace.WriteLine("  Ok");
						}
						else
						{
							Trace.WriteLine("  Skipped");
						}
					}
					else
					{
						Trace.WriteLine(String.Format("	 **CAN'T DEPLOY to {0}**", dstdir));

						success = false;
					}
				}
				else
				{
					Trace.WriteLine("  **DOES NOT EXIST**");

					success = false;
				}
			}

			return success;
		}

		////////////////////////////////////////////////////////////////////////
		// Launch
		////////////////////////////////////////////////////////////////////////
		public override bool Launch()
		{
			m_LaunchRequested = true;
			return true;
		}

		////////////////////////////////////////////////////////////////////////
		// EnableLaunch
		////////////////////////////////////////////////////////////////////////
		public override bool EnableLaunch
		{
			get{return Config.IsValid && Runnable;}
		}

		////////////////////////////////////////////////////////////////////////
		// LaunchFromThread
		////////////////////////////////////////////////////////////////////////
		public bool LaunchFromThread()
		{
			bool success = false;

			TargetEvent.Invoke(CreateEvent(TargetEventType.Launch, "Launch"));
			Trace.WriteLine(String.Format("Launching {0}...", FullName));

			string cmdLine = CommandLine;
			cmdLine += " -rockstartargetmanager";
			Trace.WriteLine(String.Format("	 Command line: {0}", cmdLine));

			if(DeployFromThread(DeployType.Normal))
			{
				string remotePath = Config.GetExpandedEnvVar("RemotePath");
				string remoteRoot = Config.GetExpandedEnvVar("RemoteRoot");

				IXboxFile xbf = null;

				try
				{
					xbf = m_Xbc.GetFileObject(remotePath);
				}
				catch (System.Exception /*e*/)
				{
					xbf = null;
				}

				if(null != xbf)
				{
					// UpdateXbox();

					if(null != m_Xbc)
					{
						try
						{
							m_Xbc.Reboot(remotePath,
										remoteRoot,
										cmdLine,
										XboxRebootFlags.Title);
							//XboxRebootFlags.Cold);

							//bool notStopped;
							//m_Xbdb.Go(out notStopped);

							success = true;
						}
						catch (System.Exception /*e*/)
						{
						}
					}
				}
				else
				{
					Trace.TraceError(remotePath + " does not exist");
				}
			}

			if(!success)
			{
				Trace.WriteLine("Failed to launch " + FullName);
			}

			return success;
		}

		////////////////////////////////////////////////////////////////////////
		// Reboot
		////////////////////////////////////////////////////////////////////////
		public override void Reboot()
		{
			Trace.WriteLine(String.Format("Rebooting {0}...", FullName));
			m_Xbc.Reboot(null, null, null, XboxRebootFlags.Cold);
		}

		////////////////////////////////////////////////////////////////////////
		// EnableReboot
		////////////////////////////////////////////////////////////////////////
		public override bool EnableReboot
		{
			get{return Runnable;}
		}

		////////////////////////////////////////////////////////////////////////
		// FastReboot
		////////////////////////////////////////////////////////////////////////
		public override void FastReboot()
		{
			Trace.WriteLine(String.Format("Fast Rebooting {0}...", FullName));
			m_Xbc.Reboot(null, null, null, XboxRebootFlags.Title);
		}

		////////////////////////////////////////////////////////////////////////
		// EnableFastReboot
		////////////////////////////////////////////////////////////////////////
		public override bool EnableFastReboot
		{
			get{return Runnable;}
		}

		////////////////////////////////////////////////////////////////////////
		// Stop
		////////////////////////////////////////////////////////////////////////
		public override void Stop()
		{
			Trace.WriteLine(String.Format("Stopping {0}...", FullName));
			bool alreadyStopped;
			m_Xbdb.Stop(out alreadyStopped);
		}

		////////////////////////////////////////////////////////////////////////
		// GoInternal
		////////////////////////////////////////////////////////////////////////
		private void GoInternal()
		{
			bool notStopped;
			m_Xbdb.Go(out notStopped);

			try
			{
				foreach(IXboxThread th in m_Xbdb.Threads)
				{
					if(0 != th.StopEventInfo.IsThreadStopped)
					{
						th.Continue(false);
					}
				}
			}
			catch
			{
			}
		}

		////////////////////////////////////////////////////////////////////////
		// Go
		////////////////////////////////////////////////////////////////////////
		public override void Go()
		{
			Trace.WriteLine(String.Format("Starting {0}...", FullName));
			GoInternal();
		}

		////////////////////////////////////////////////////////////////////////
		// EnableStopGo
		////////////////////////////////////////////////////////////////////////
		public override bool EnableStopGo
		{
			get{return Runnable && m_Xbdb!=null;}
		}

		////////////////////////////////////////////////////////////////////////
		// Stopped
		////////////////////////////////////////////////////////////////////////
		public override bool Stopped
		{
			get{lock(m_Lock){return m_IsStopped;}}
		}

		////////////////////////////////////////////////////////////////////////
		// Coredump
		////////////////////////////////////////////////////////////////////////
		public override void Coredump()
		{
			m_CoredumpRequested = true;
		}

		////////////////////////////////////////////////////////////////////////
		// EnableCoredump
		////////////////////////////////////////////////////////////////////////
		public override bool EnableCoredump
		{
			get{return Runnable && m_Xbdb!=null;}
		}

		////////////////////////////////////////////////////////////////////////
		// CoredumpFromThread
		////////////////////////////////////////////////////////////////////////
		public void CoredumpFromThread()
		{
			var ignoredFilesToCopy = new List<string>();
			foreach(IXboxThread t in m_Xbdb.Threads)
			{
				if(t.StopEventInfo.IsThreadStopped != 0)
				{
					TriggerCoredump(t, ref ignoredFilesToCopy, ()=>{});
					return;
				}
			}
			if(m_TargetThread != null)
			{
				StopTargetTmThread(
				() => TriggerCoredump(m_TargetThread, ref ignoredFilesToCopy,
				() => ResumeTargetTmThread()));
			}
		}

		////////////////////////////////////////////////////////////////////////
		// StopTargetTmThread
		////////////////////////////////////////////////////////////////////////
		private void StopTargetTmThread(OnEventCallback thenDo)
		{
			m_OnStopCallback = thenDo;

			byte[] data = {2};
			UInt32 bytesWritten;
			m_Xbdb.SetMemory(m_TargetBreakAddr, 1, data, out bytesWritten);
		}

		////////////////////////////////////////////////////////////////////////
		// ResumeTargetTmThread
		////////////////////////////////////////////////////////////////////////
		private void ResumeTargetTmThread()
		{
			bool exception = false;
			m_TargetThread.Continue(exception);

			bool notStopped;
			m_Xbdb.Go(out notStopped);
		}

		////////////////////////////////////////////////////////////////////////
		// LookupSymbol
		////////////////////////////////////////////////////////////////////////
		private UInt32 LookupSymbol(SymbolLookup sym, string symbol)
		{
			// Determine the address of the physical memory allocation callback function (if it exists).
			UInt32 targetCallbackAddr;
			TtyOutput("Searching symbols for "+symbol+" ...\n");
			targetCallbackAddr = (UInt32)sym.Lookup(symbol);
			if(targetCallbackAddr == 0)
			{
				TtyOutput("... not found\n");
			}
			else
			{
				TtyOutput("... found at 0x"+targetCallbackAddr.ToString("x8")+"\n");
			}
			return targetCallbackAddr;
		}

		////////////////////////////////////////////////////////////////////////
		// LookupSymbol
		////////////////////////////////////////////////////////////////////////
		private UInt32 LookupSymbol(string symbol)
		{
			using(SymbolLookup sym = new SymbolLookup(CmpFilename(), TargetPlatformId.Xbox360))
			{
				return LookupSymbol(sym, symbol);
			}
		}

		////////////////////////////////////////////////////////////////////////
		// TriggerCoredump
		////////////////////////////////////////////////////////////////////////
		private void TriggerCoredump(IXboxThread thread, ref List<string> filesToCopy, OnEventCallback thenDo)
		{
			string xexName = m_Xbc.RunningProcessInfo.ProgramName;
			UInt32 pid = m_Xbc.RunningProcessInfo.ProcessId;
			string name = Regex.Replace(xexName, @".*\\([^\\]+)\.xex$", "$1");
			string initialDmpFile = RockstarTargetManager.GetCrashdumpDir()+name+DateTime.Now.ToString("-yyyyMMdd-HHmmss-")+pid.ToString("x8")+".mini.dmp";
			TtyOutput("Generating mini dump file \""+initialDmpFile+"\" ...\n");
			m_Xbdb.WriteDump(initialDmpFile, XboxDumpFlags.WithFullMemory);
			filesToCopy.Add(initialDmpFile);

			UInt32 callbackAddr = LookupSymbol(MEM_LIST_CALLBACK_NAME);
			if(callbackAddr != 0)
			{
				string fullDmpFile = Regex.Replace(initialDmpFile, @"\.mini\.dmp$", ".full.dmp");
				filesToCopy.Add(fullDmpFile);
				TtyOutput("Executing target callback "+MEM_LIST_CALLBACK_NAME+" at 0x"+callbackAddr.ToString("x8")+"\n");
				ExecuteTargetCallback(thread, callbackAddr,
					(bool valid, UInt64 callbackRet) => CreateFullMemoryCoredump(valid, callbackRet, initialDmpFile, fullDmpFile, thenDo));
			}
			else
			{
				thenDo();
			}
		}

		////////////////////////////////////////////////////////////////////////
		// ExecuteTargetCallback
		////////////////////////////////////////////////////////////////////////
		private void ExecuteTargetCallback(IXboxThread thread, UInt32 addr, UInt64 arg0, OnEventCallbackArgBoolUInt64 thenDo)
		{
			TtyOutput("Execute target callback 0x"+addr.ToString("x8")+"\n");
			if(addr != 0)
			{
				TargetCallbackStateSaver stateSaver = new TargetCallbackStateSaver(m_Xbdb, thread);

				m_OnStopCallback = () =>
				{
					IXboxStackFrame stk2 = thread.TopOfStack;
					Int64 r3;
					stk2.GetRegister64(XboxRegisters64.r3, out r3);
					stateSaver.Dispose();
					thenDo(true, (UInt64)r3);
				};

				try
				{
					// We are going to get another XboxExecutionState.Running
					// event, so clear the m_StartNewLog (the event handler will
					// the set it again).
					m_StartNewLog = false;

					IXboxStackFrame stk = thread.TopOfStack;
					stk.SetRegister32(XboxRegisters32.lr,  0);
					stk.SetRegister32(XboxRegisters32.iar, (Int32)addr);
					stk.SetRegister64(XboxRegisters64.r3,  (Int64)arg0);
					stk.FlushRegisterChanges();

					bool exception = false;
					thread.Continue(exception);

					bool notStopped;
					m_Xbdb.Go(out notStopped);

					return;
				}
				catch(Exception e)
				{
					TtyOutput("Failed to run target callback: "+e.ToString()+"\n");
					m_OnStopCallback = null;
				}
			}

			thenDo(false, 0);
		}

		////////////////////////////////////////////////////////////////////////
		// ExecuteTargetCallback
		////////////////////////////////////////////////////////////////////////
		private void ExecuteTargetCallback(IXboxThread thread, UInt32 addr, OnEventCallbackArgBoolUInt64 thenDo)
		{
			ExecuteTargetCallback(thread, addr, 0, thenDo);
		}

		////////////////////////////////////////////////////////////////////////
		// ExecuteTargetCallback
		////////////////////////////////////////////////////////////////////////
		private void ExecuteTargetCallback(IXboxThread thread, UInt32 addr, UInt64 arg0, OnEventCallback thenDo)
		{
			ExecuteTargetCallback(thread, addr, arg0, (bool ignoredValid, UInt64 ignoredRet) => thenDo());
		}

		////////////////////////////////////////////////////////////////////////
		// PhysicalAllocMemoryRegion
		////////////////////////////////////////////////////////////////////////
		private struct PhysicalAllocMemoryRegion
		{
			public UInt32 ptr;
			public UInt32 size;
		};

		////////////////////////////////////////////////////////////////////////
		// GetMemory
		////////////////////////////////////////////////////////////////////////
		private bool GetMemory(UInt64 addr, ref byte[] buf)
		{
			uint size = (uint)buf.Length;
			uint bytesRead;
			m_Xbdb.GetMemory((UInt32)addr, (uint)size, buf, out bytesRead);
			return bytesRead == buf.Length;
		}

		////////////////////////////////////////////////////////////////////////
		// GetMemory
		////////////////////////////////////////////////////////////////////////
		private bool GetMemory(UInt64 addr, out byte data)
		{
			byte[] buf = new byte[1];
			if(!GetMemory(addr, ref buf))
			{
				data = 0;
				return false;
			}
			data = buf[0];
			return true;
		}

		////////////////////////////////////////////////////////////////////////
		// GetMemory
		////////////////////////////////////////////////////////////////////////
		private bool GetMemory(UInt64 addr, out UInt32 data)
		{
			byte[] buf = new byte[4];
			if(!GetMemory(addr, ref buf))
			{
				data = 0;
				return false;
			}
			data = ((UInt32)buf[0]<<24) | ((UInt32)buf[1]<<16) | ((UInt32)buf[2]<<8) | ((UInt32)buf[3]);
			return true;
		}

		////////////////////////////////////////////////////////////////////////
		// GetMemory
		////////////////////////////////////////////////////////////////////////
		private bool GetMemory(UInt64 addr, out UInt64 data)
		{
			byte[] buf = new byte[8];
			if(!GetMemory(addr, ref buf))
			{
				data = 0;
				return false;
			}
			data = ((UInt64)buf[0]<<56) | ((UInt64)buf[1]<<48) | ((UInt64)buf[2]<<40) | ((UInt64)buf[3]<<32)
			     | ((UInt64)buf[4]<<24) | ((UInt64)buf[5]<<16) | ((UInt64)buf[6]<<8)  | ((UInt64)buf[7]);
			return true;
		}

		////////////////////////////////////////////////////////////////////////
		// GetMemory
		////////////////////////////////////////////////////////////////////////
		private bool GetMemory(UInt64 addr, out string str)
		{
			return GetString(addr, Encoding.UTF8, 1, out str, GetMemory);
		}

		////////////////////////////////////////////////////////////////////////
		// CreateFullMemoryCoredump
		////////////////////////////////////////////////////////////////////////
		private void CreateFullMemoryCoredump(bool valid, UInt64 callbackRet, string initialDmpFile, string fullDmpFile, OnEventCallback thenDo)
		{
			if(valid)
			{
				// Load the initial minidump file
				TtyOutput("Loading \""+initialDmpFile+"\"\n");
				MiniDump dmp = MiniDump.Load(initialDmpFile);

				// Read the list of physical allocations
				List<PhysicalAllocMemoryRegion> regions = new List<PhysicalAllocMemoryRegion>();
				UInt32 listAddr = (UInt32)callbackRet;
				byte[] readBuf = new byte[256];
				for(;;)
				{
					uint bytesRead;
					m_Xbdb.GetMemory(listAddr, (uint)readBuf.Length, readBuf, out bytesRead);
					if(bytesRead == 0)
					{
						break;
					}
					for(uint i=0; i+8<=bytesRead; i+=8)
					{
						PhysicalAllocMemoryRegion m;
						m.ptr  = ((UInt32)readBuf[i+0]<<24) | ((UInt32)readBuf[i+1]<<16) | ((UInt32)readBuf[i+2]<<8) | ((UInt32)readBuf[i+3]);
						m.size = ((UInt32)readBuf[i+4]<<24) | ((UInt32)readBuf[i+5]<<16) | ((UInt32)readBuf[i+6]<<8) | ((UInt32)readBuf[i+7]);
						if(m.ptr == 0)
						{
							goto finished_reading_region_array;
						}
						if(m.size != 0)
						{
							regions.Add(m);
						}
					}
					listAddr += bytesRead;
				}
			finished_reading_region_array:
				readBuf = null;

				// Now read the memory for each region, and add them to the minidump
				foreach(PhysicalAllocMemoryRegion m in regions)
				{
					TtyOutput("Fetching physical allocation [0x"+m.ptr.ToString("x8")+"..0x"+(m.ptr+m.size-1).ToString("x8")+"] ...\n");
					var data = new byte[m.size];
					if(GetMemory(m.ptr, ref data))
					{
						dmp.AddMemoryRange(m.ptr, data);
					}
				}

				// Write out the new "mini" dump
				TtyOutput("Saving \""+fullDmpFile+"\" ...\n");
				dmp.Store(fullDmpFile);
				TtyOutput("... Done\n\n");
			}

			thenDo();
		}

		////////////////////////////////////////////////////////////////////////
		// ScreenShot
		////////////////////////////////////////////////////////////////////////
		public override bool ScreenShot(string filename)
		{
			m_Xbc.ScreenShot(filename);
			return true;
		}

		////////////////////////////////////////////////////////////////////////
		// EnableScreenShot
		////////////////////////////////////////////////////////////////////////
		public override bool EnableScreenShot
		{
			get{return m_Xbc!=null;}
		}

		////////////////////////////////////////////////////////////////////////
		// ReleaseDebugger
		////////////////////////////////////////////////////////////////////////
		public void ReleaseDebugger()
		{
			if(null != m_Xbdb)
			{
				m_Xbdb.DisconnectAsDebugger();
				m_Xbdb = null;
			}
		}

		////////////////////////////////////////////////////////////////////////
		// Runnable
		////////////////////////////////////////////////////////////////////////
		internal bool Runnable
		{
			get{lock(m_Lock){return (null != m_Xbc);}}
		}

		////////////////////////////////////////////////////////////////////////
		// LastRunnableTime
		////////////////////////////////////////////////////////////////////////
		internal DateTime LastRunnableTime;

		////////////////////////////////////////////////////////////////////////
		// Name
		////////////////////////////////////////////////////////////////////////
		public override string Name
		{
			get{lock(m_Lock){return m_Name;}}
		}

		////////////////////////////////////////////////////////////////////////
		// Alias
		////////////////////////////////////////////////////////////////////////
		public override string Alias
		{
			get { return m_Alias; }
		}

		////////////////////////////////////////////////////////////////////////
		// PlatformId
		////////////////////////////////////////////////////////////////////////
		public override TargetPlatformId PlatformId
		{
			get { return TargetPlatformId.Xbox360; }
		}

		////////////////////////////////////////////////////////////////////////
		// CmpFilename
		////////////////////////////////////////////////////////////////////////
		private string CmpFilename()
		{
			string name = m_Xbc.RunningProcessInfo.ProgramName;
			if(name != "")
			{
				if(Regex.IsMatch(name, @"^[a-zA-Z]:.*\.xex$"))
				{
					UInt32 pid = m_Xbc.RunningProcessInfo.ProcessId;
					string localName = Path.Combine(Path.GetTempPath(), Regex.Replace(name, @".*\\([^\\]+)\.xex$", "$1-"+pid.ToString("x8")+".cmp"));
					if(m_CmpFileCache == null || m_CmpFileCache != localName)
					{
						if(m_CmpFileCache != null)
						{
							try
							{
								File.Delete(m_CmpFileCache);
							}
							catch
							{
							}
						}

						string remoteName = Regex.Replace(name, @"^(.*)\.xex$", "x$1.cmp");

						TtyOutput("Caching \""+remoteName+"\" -> \""+localName+"\" ...\n");
						m_Xbc.ReceiveFile(localName, remoteName);

						m_CmpFileCache = localName;
					}
					return localName;
				}
			}
			return null;
		}

		////////////////////////////////////////////////////////////////////////
		// PrintThreadInfo
		////////////////////////////////////////////////////////////////////////
		private void PrintThreadInfo(string indent, IXboxThread t)
		{
			try
			{
				TtyOutput(indent+"Thread:              0x"+t.ThreadId.ToString("x8")+" "+t.ThreadInfo.Name+"\n");
				TtyOutput(indent+"Suspend Count:       0x"+t.ThreadInfo.SuspendCount.ToString("x8")+"\n");
				TtyOutput(indent+"Priority:            0x"+t.ThreadInfo.Priority.ToString("x8")+"\n");
				TtyOutput(indent+"TLS Base:            0x"+t.ThreadInfo.TlsBase.ToString("x8")+"\n");
				TtyOutput(indent+"Start Address:       0x"+t.ThreadInfo.StartAddress.ToString("x8")+"\n");
				TtyOutput(indent+"Stack Base:          0x"+t.ThreadInfo.StackBase.ToString("x8")+"\n");
				TtyOutput(indent+"Stack Limit:         0x"+t.ThreadInfo.StackLimit.ToString("x8")+"\n");
				TtyOutput(indent+"Stack Slack Space:   0x"+t.ThreadInfo.StackSlackSpace.ToString("x8")+"\n");
				TtyOutput(indent+"Create Time:         0x"+t.ThreadInfo.CreateTime.ToString()+"\n");
			}
			catch
			{
			}
		}

		////////////////////////////////////////////////////////////////////////
		// Register32
		////////////////////////////////////////////////////////////////////////
		private string Register32(IXboxStackFrame s, XboxRegisters32 r)
		{
			Int32 val;
			if(s.GetRegister32(r, out val))
			{
				return val.ToString("x8");
			}
			else
			{
				return "????????";
			}
		}

		////////////////////////////////////////////////////////////////////////
		// Register64
		////////////////////////////////////////////////////////////////////////
		private string Register64(IXboxStackFrame s, XboxRegisters64 r)
		{
			Int64 val;
			if(s.GetRegister64(r, out val))
			{
				return val.ToString("x16");
			}
			else
			{
				return "????????????????";
			}
		}

		////////////////////////////////////////////////////////////////////////
		// RegisterDouble
		////////////////////////////////////////////////////////////////////////
		private string RegisterDouble(IXboxStackFrame s, XboxRegistersDouble r)
		{
			double val;
			if(s.GetRegisterDouble(r, out val))
			{
				return val.ToString("f16");
			}
			else
			{
				return "????????????????";
			}
		}

		////////////////////////////////////////////////////////////////////////
		// PrintThreadRegisters
		////////////////////////////////////////////////////////////////////////
		private void PrintThreadRegisters(string indent, IXboxThread t)
		{
			try
			{
				IXboxStackFrame stk = t.TopOfStack;

				TtyOutput("IAR "+Register32(stk,XboxRegisters32.iar)+"\n");
				TtyOutput("LR  "+Register32(stk,XboxRegisters32.lr )+"\n");
				TtyOutput("CR  "+Register32(stk,XboxRegisters32.cr )+"\n");
				TtyOutput("MSR "+Register32(stk,XboxRegisters32.msr)+"\n");
				TtyOutput("XER "+Register32(stk,XboxRegisters32.xer)+"\n");
				TtyOutput("CTR "+Register64(stk,XboxRegisters64.ctr)+"\n\n");

				TtyOutput("r0  "+Register64(stk,XboxRegisters64.r0 )+"    r16 "+Register64(stk,XboxRegisters64.r16)+"\n");
				TtyOutput("r1  "+Register64(stk,XboxRegisters64.r1 )+"    r17 "+Register64(stk,XboxRegisters64.r17)+"\n");
				TtyOutput("r2  "+Register64(stk,XboxRegisters64.r2 )+"    r18 "+Register64(stk,XboxRegisters64.r18)+"\n");
				TtyOutput("r3  "+Register64(stk,XboxRegisters64.r3 )+"    r19 "+Register64(stk,XboxRegisters64.r19)+"\n");
				TtyOutput("r4  "+Register64(stk,XboxRegisters64.r4 )+"    r20 "+Register64(stk,XboxRegisters64.r20)+"\n");
				TtyOutput("r5  "+Register64(stk,XboxRegisters64.r5 )+"    r21 "+Register64(stk,XboxRegisters64.r21)+"\n");
				TtyOutput("r6  "+Register64(stk,XboxRegisters64.r6 )+"    r22 "+Register64(stk,XboxRegisters64.r22)+"\n");
				TtyOutput("r7  "+Register64(stk,XboxRegisters64.r7 )+"    r23 "+Register64(stk,XboxRegisters64.r23)+"\n");
				TtyOutput("r8  "+Register64(stk,XboxRegisters64.r8 )+"    r24 "+Register64(stk,XboxRegisters64.r24)+"\n");
				TtyOutput("r9  "+Register64(stk,XboxRegisters64.r9 )+"    r25 "+Register64(stk,XboxRegisters64.r25)+"\n");
				TtyOutput("r10 "+Register64(stk,XboxRegisters64.r10)+"    r26 "+Register64(stk,XboxRegisters64.r26)+"\n");
				TtyOutput("r11 "+Register64(stk,XboxRegisters64.r11)+"    r27 "+Register64(stk,XboxRegisters64.r27)+"\n");
				TtyOutput("r12 "+Register64(stk,XboxRegisters64.r12)+"    r28 "+Register64(stk,XboxRegisters64.r28)+"\n");
				TtyOutput("r13 "+Register64(stk,XboxRegisters64.r13)+"    r29 "+Register64(stk,XboxRegisters64.r29)+"\n");
				TtyOutput("r14 "+Register64(stk,XboxRegisters64.r14)+"    r30 "+Register64(stk,XboxRegisters64.r30)+"\n");
				TtyOutput("r15 "+Register64(stk,XboxRegisters64.r15)+"    r31 "+Register64(stk,XboxRegisters64.r31)+"\n\n");

// 				TtyOutput("f0  "+RegisterDouble(stk,XboxRegistersDouble.fp0 )+"    f16 "+RegisterDouble(stk,XboxRegistersDouble.fp16)+"\n");
// 				TtyOutput("f1  "+RegisterDouble(stk,XboxRegistersDouble.fp1 )+"    f17 "+RegisterDouble(stk,XboxRegistersDouble.fp17)+"\n");
// 				TtyOutput("f2  "+RegisterDouble(stk,XboxRegistersDouble.fp2 )+"    f18 "+RegisterDouble(stk,XboxRegistersDouble.fp18)+"\n");
// 				TtyOutput("f3  "+RegisterDouble(stk,XboxRegistersDouble.fp3 )+"    f19 "+RegisterDouble(stk,XboxRegistersDouble.fp19)+"\n");
// 				TtyOutput("f4  "+RegisterDouble(stk,XboxRegistersDouble.fp4 )+"    f20 "+RegisterDouble(stk,XboxRegistersDouble.fp20)+"\n");
// 				TtyOutput("f5  "+RegisterDouble(stk,XboxRegistersDouble.fp5 )+"    f21 "+RegisterDouble(stk,XboxRegistersDouble.fp21)+"\n");
// 				TtyOutput("f6  "+RegisterDouble(stk,XboxRegistersDouble.fp6 )+"    f22 "+RegisterDouble(stk,XboxRegistersDouble.fp22)+"\n");
// 				TtyOutput("f7  "+RegisterDouble(stk,XboxRegistersDouble.fp7 )+"    f23 "+RegisterDouble(stk,XboxRegistersDouble.fp23)+"\n");
// 				TtyOutput("f8  "+RegisterDouble(stk,XboxRegistersDouble.fp8 )+"    f24 "+RegisterDouble(stk,XboxRegistersDouble.fp24)+"\n");
// 				TtyOutput("f9  "+RegisterDouble(stk,XboxRegistersDouble.fp9 )+"    f25 "+RegisterDouble(stk,XboxRegistersDouble.fp25)+"\n");
// 				TtyOutput("f10 "+RegisterDouble(stk,XboxRegistersDouble.fp10)+"    f26 "+RegisterDouble(stk,XboxRegistersDouble.fp26)+"\n");
// 				TtyOutput("f11 "+RegisterDouble(stk,XboxRegistersDouble.fp11)+"    f27 "+RegisterDouble(stk,XboxRegistersDouble.fp27)+"\n");
// 				TtyOutput("f12 "+RegisterDouble(stk,XboxRegistersDouble.fp12)+"    f28 "+RegisterDouble(stk,XboxRegistersDouble.fp28)+"\n");
// 				TtyOutput("f13 "+RegisterDouble(stk,XboxRegistersDouble.fp13)+"    f29 "+RegisterDouble(stk,XboxRegistersDouble.fp29)+"\n");
// 				TtyOutput("f14 "+RegisterDouble(stk,XboxRegistersDouble.fp14)+"    f30 "+RegisterDouble(stk,XboxRegistersDouble.fp30)+"\n");
// 				TtyOutput("f15 "+RegisterDouble(stk,XboxRegistersDouble.fp15)+"    f31 "+RegisterDouble(stk,XboxRegistersDouble.fp31)+"\n\n");
			}
			catch
			{
			}
		}

		////////////////////////////////////////////////////////////////////////
		// PrintThreadStack
		////////////////////////////////////////////////////////////////////////
		private void PrintThreadStack(string indent, IXboxThread t)
		{
			try
			{
				using(SymbolLookup sym = new SymbolLookup(CmpFilename(), TargetPlatformId.Xbox360))
				{
					for(IXboxStackFrame s=t.TopOfStack; null!=s; s=s.NextStackFrame)
					{
						Int32 iar;
						s.GetRegister32(XboxRegisters32.iar, out iar);
						TtyOutput(indent+"0x"+iar.ToString("x8")+" - "+sym.Lookup((UInt32)iar)+"\n");
					}
				}
			}
			catch
			{
			}
		}

		////////////////////////////////////////////////////////////////////////
		// TestkitCmdHandler
		////////////////////////////////////////////////////////////////////////
		private void TestkitCmdHandler(string cmd)
		{
			if(cmd.StartsWith("BEGIN_EXCEPTION"))
			{
				BeginCaptureTtyOutput();
				if(m_TestkitCrashState == null)
				{
					m_TestkitCrashState = new TestkitCrashState();
				}
				m_TestkitCrashState.ProcessId   = m_ProcessId;
				m_TestkitCrashState.ProgramName = m_ProgramName;
			}
			else if(cmd.StartsWith("END_BUG_DESCRIPTION"))
			{
				if(m_TestkitCrashState != null)
				{
					m_TestkitCrashState.BugDescription = EndCaptureTtyOutput();
				}
			}
			else if(cmd.StartsWith("BUGSTAR_CONFIG:"))
			{
				if(m_TestkitCrashState != null)
				{
					m_TestkitCrashState.BugstarConfig = Bugstar.Config.FromString(cmd.Remove(0, 15));
				}
			}
			else if(cmd.StartsWith("MEMDMP:"))
			{
				if(m_TestkitCrashState != null)
				{
					m_TestkitCrashState.FilesToCopy.Add(cmd.Remove(0, 7));
				}
			}
			else if(cmd.StartsWith("LOG_STOP"))
			{
				DisableTtyOutput();
			}
			else if(cmd.StartsWith("QUITF:"))
			{
				Debug.Assert(m_TestkitCrashState == null);
				m_TestkitCrashState = new TestkitCrashState();
				m_TestkitCrashState.BugSummary = "Quitf \""+cmd.Remove(0, 6)+"\"";
			}
			else if(cmd.StartsWith("CONFIG:"))
			{
			}
			else
			{
				Trace.WriteLine("unrecognized 360 testkit command string \""+cmd+"\".");
			}
		}

		////////////////////////////////////////////////////////////////////////
		// CrashHandler
		////////////////////////////////////////////////////////////////////////
		private void CrashHandler(XboxDebugEventType eventCode, IXboxEventInfo eventInfo, ref TargetEventArgs pluginEventArgs)
		{
			// m_Xbdb will be null for a testkit.  Testkits do the exception
			// handling themselves, so just do nothing here unless running on a
			// devkit (with a debug connection).
			if(m_Xbdb != null)
			{
				IXboxThread t = eventInfo.Info.Thread;

				string bugSummary = null;

				// Check if this is a sysTmCmdXXX
				if(eventCode == XDevkit.XboxDebugEventType.ExecutionBreak)
				{
					Int32 iar;
					t.TopOfStack.GetRegister32(XboxRegisters32.iar, out iar);
					UInt32 debugbreak, branch, signature;
					byte opcode;
					if(GetMemory((UInt32)iar+0,  out debugbreak) && // __debugbreak
					   GetMemory((UInt32)iar+4,  out branch)     && // b         .+?
					   GetMemory((UInt32)iar+8,  out signature)  && // .ascii    "R*TM"
					   GetMemory((UInt32)iar+12, out opcode)     &&
					   debugbreak          == 0x0fe00016         &&
					   (branch&0xfc000003) == 0x48000000         &&
					   signature           == 0x522a544d)
					{
						bool standardCrashHandling = false;
						bool processContinue = true;
						switch((TMCommands)opcode)
						{
							case TMCommands.NOP:
							{
								break;
							}

							case TMCommands.PRINT_ALL_THREAD_STACKS:
							{
								// TODO
								break;
							}

							case TMCommands.STOP_NO_ERROR_REPORT:
							{
								processContinue = false;
								break;
							}

							case TMCommands.QUITF:
							{
								standardCrashHandling = true;
								Int64 r3;
								t.TopOfStack.GetRegister64(XboxRegisters64.r3, out r3);
								if(GetMemory((UInt64)r3, out bugSummary))
								{
									bugSummary = "Quitf \""+bugSummary+"\"";
								}
								break;
							}

							case TMCommands.GPU_HANG:
							case TMCommands.CPU_HANG:
							case TMCommands.CONFIG:
							{
								break;
							}

							default:
							{
								standardCrashHandling = true;
								break;
							}
						}

						if(!standardCrashHandling)
						{
							pluginEventArgs = null;
							if(processContinue)
							{
								GoInternal();
							}
							return;
						}
					}
				}

				// Determine the address of the target crash callback function (if it exists).
				UInt32 targetCallbackAddr = LookupSymbol(TARGET_CRASH_CALLBACK_NAME);
				if(targetCallbackAddr != 0) TtyOutput("Executing target callback "+TARGET_CRASH_CALLBACK_NAME+" at 0x"+targetCallbackAddr.ToString("x8")+"\n");
				var filesToCopy = new List<string>();
				ExecuteTargetCallback(t, targetCallbackAddr, 0,
				() =>{TtyOutput("\n**** BEGIN EXCEPTION DUMP\n\n");
				      BeginCaptureTtyOutput();
				      DisplayCrashInfo(eventCode, eventInfo);
				      string bugDescription = EndCaptureTtyOutput();
				      TriggerCoredump(t, ref filesToCopy,
				() => ExecuteTargetCallback(t, targetCallbackAddr, 1,
				() => RaiseBug(t, bugSummary, bugDescription, filesToCopy,
				() => TtyOutput("\n**** END EXCEPTION DUMP\n"))));});
			}
		}

		////////////////////////////////////////////////////////////////////////
		// CrashHandler
		////////////////////////////////////////////////////////////////////////
		private void CrashHandler(XboxDebugEventType eventCode, IXboxEventInfo eventInfo)
		{
			TargetEventArgs pluginEventArgs = null;
			CrashHandler(eventCode, eventInfo, ref pluginEventArgs);
		}

		////////////////////////////////////////////////////////////////////////
		// FinishTestkitCrashHandling
		////////////////////////////////////////////////////////////////////////
		private void FinishTestkitCrashHandling()
		{
			var filesToCopy = new List<string>();

			try
			{
				// Grab the 360 generated coredump off the console
				var xboxDmpDir = Regex.Replace(m_TestkitCrashState.ProgramName, @"^.*\\([^\\]+)$", @"E:\dumps\$1");
				var xboxDmpFiles = new List<string>();
				foreach(var fileInDir in m_Xbc.DirectoryFiles(xboxDmpDir))
				{
					xboxDmpFiles.Add(((IXboxFile)fileInDir).Name);
				}
				xboxDmpFiles.Sort();
				var xboxDmpFile = xboxDmpFiles[xboxDmpFiles.Count-1];
				var pcMiniDmpFile = Regex.Replace(m_TestkitCrashState.ProgramName, @"^.*\\([^\\]+)\.xex$",
					RockstarTargetManager.GetCrashdumpDir()+"$1"+DateTime.Now.ToString("-yyyyMMdd-HHmmss-")+m_TestkitCrashState.ProcessId.ToString()+".mini.dmp");
				TtyOutput("\nMoving dump from \"x"+xboxDmpFile+"\" to \""+pcMiniDmpFile+"\" ...\n");
				m_Xbc.ReceiveFile(pcMiniDmpFile, xboxDmpFile);
				filesToCopy.Add(pcMiniDmpFile);
				m_Xbc.DeleteFile(xboxDmpFile);

				// Grab any additional memory dump files
				var dmp = MiniDump.Load(pcMiniDmpFile);
				foreach(var xboxMemFilename in m_TestkitCrashState.FilesToCopy)
				{
					try
					{
						TtyOutput("Reading additional memory dump file \"x"+xboxMemFilename+"\" ...\n");
						var xboxMemFile = m_Xbc.GetFileObject(xboxMemFilename);
						const uint fileOffset = 0;
						uint count = (uint)xboxMemFile.Size;
						var data = new byte[count];
						uint bytesRead;
						m_Xbc.ReadFileBytes(xboxMemFilename, fileOffset, count, data, out bytesRead);
						if(bytesRead < count)
						{
							TtyOutput("... read error, only read 0x"+bytesRead.ToString("x8")+" bytes of 0x"+count.ToString("x8")+"\n");
						}
						var addr = UInt32.Parse(Regex.Replace(xboxMemFilename, @"^.*\\([^\\]+)\.mem$", "$1"), NumberStyles.HexNumber);
						dmp.AddMemoryRange(addr, data);
						m_Xbc.DeleteFile(xboxMemFilename);
					}
					catch
					{
					}
				}

				// Create a full memory dump
				var pcFullDmpFile = Regex.Replace(pcMiniDmpFile, @"^(.*)\.mini\.dmp$", "$1.full.dmp");
				TtyOutput("Saving \""+pcFullDmpFile+"\" ...\n");
				dmp.Store(pcFullDmpFile);
				TtyOutput("... done\n\n");
				filesToCopy.Add(pcFullDmpFile);
			}
			catch
			{
			}

			if(m_TestkitCrashState.BugstarConfig != null)
			{
				// Find corresponding pdb on host pc
				if(!String.IsNullOrWhiteSpace(m_TestkitCrashState.BugstarConfig.RootDir))
				{
					string xboxXexName = m_TestkitCrashState.ProgramName;
					string pcPdbName = Path.Combine(m_TestkitCrashState.BugstarConfig.RootDir, Regex.Replace(xboxXexName, @".*\\([^\\]+)\.xex$", "$1.pdb"));
					filesToCopy.Add(pcPdbName);
				}

				// Raise bug
				const bool enableBugDupCheck = true;
				BaseRaiseBug(m_TestkitCrashState.BugstarConfig, m_TestkitCrashState.BugSummary, m_TestkitCrashState.BugDescription, filesToCopy.ToArray(), enableBugDupCheck);
			}
			else
			{
				TtyOutput("No Bugstar config information read from game, please manually create a bug.\n");
			}

			TtyOutput("\n**** END EXCEPTION DUMP\n");
			m_TestkitCrashState = null;
			m_StartNewLog = true;
		}

		////////////////////////////////////////////////////////////////////////
		// DisplayCrashInfo
		////////////////////////////////////////////////////////////////////////
		private void DisplayCrashInfo(XboxDebugEventType eventCode, IXboxEventInfo eventInfo)
		{
			try
			{
				TtyOutput("Event Type:          "+eventCode.ToString()+"\n");
				TtyOutput("Message:             \""+eventInfo.Info.Message+"\"\n");
				TtyOutput("Code:                0x"+eventInfo.Info.Code.ToString("x8")+"\n");
				TtyOutput("Address:             0x"+eventInfo.Info.Address.ToString("x8")+"\n");
				TtyOutput("Process ID:          0x"+m_Xbc.RunningProcessInfo.ProcessId.ToString("x8")+"\n");
				TtyOutput("Process Name:        \""+m_Xbc.RunningProcessInfo.ProgramName+"\"\n\n");

				if(eventInfo.Info.Module != null)
				{
					TtyOutput("Module Name:         \""+eventInfo.Info.Module.ModuleInfo.Name+"\"\n");
					TtyOutput("Module Full Name:    \""+eventInfo.Info.Module.ModuleInfo.FullName+"\"\n");
					TtyOutput("Module Base Address: 0x"+eventInfo.Info.Module.ModuleInfo.BaseAddress.ToString("x8")+"\n");
					TtyOutput("Module Size:         0x"+eventInfo.Info.Module.ModuleInfo.Size.ToString("x8")+"\n");
					TtyOutput("Module Time Stamp:   0x"+eventInfo.Info.Module.ModuleInfo.TimeStamp.ToString("x8")+"\n");
					TtyOutput("Module Checksum:     0x"+eventInfo.Info.Module.ModuleInfo.CheckSum.ToString("x8")+"\n\n");
				}

				IXboxThread t = eventInfo.Info.Thread;
				PrintThreadInfo("", t);
				TtyOutput("\n");
				PrintThreadRegisters("", t);
				TtyOutput("\n");
				PrintThreadStack("", t);
				TtyOutput("\n");
			}
			catch
			{
			}
		}

		////////////////////////////////////////////////////////////////////////
		// RaiseBug
		////////////////////////////////////////////////////////////////////////
		private void RaiseBug(IXboxThread thread, string summary, string description, List<string> filesToCopy, OnEventCallback thenDo)
		{
			UInt32 targetCallbackAddr = LookupSymbol(GET_BUGSTAR_CONFIG_CALLBACK_NAME);
			ExecuteTargetCallback(thread, targetCallbackAddr, 0, (bool valid, UInt64 ret) =>
			{
				bool warnConfig = true;
				if(valid)
				{
					string configStr;
					if(GetMemory(ret, out configStr))
					{
						var config = Bugstar.Config.FromString(configStr);

						if(!String.IsNullOrWhiteSpace(config.RootDir))
						{
							string xboxXexName = m_Xbc.RunningProcessInfo.ProgramName;
							string pcPdbName = Path.Combine(config.RootDir, Regex.Replace(xboxXexName, @".*\\([^\\]+)\.xex$", "$1.pdb"));
							filesToCopy.Add(pcPdbName);
						}

						const bool enableBugDupCheck = true;
						BaseRaiseBug(config, summary, description, filesToCopy.ToArray(), enableBugDupCheck);
						warnConfig = false;
					}
				}
				if(warnConfig)
				{
					TtyOutput("Unable to read Bugstar configuration from the game to automatically log a bug.  Please create bug manually\n");
				}

				thenDo();
			});
		}

		////////////////////////////////////////////////////////////////////////
		// DisableTtyOutput
		////////////////////////////////////////////////////////////////////////
		private void DisableTtyOutput()
		{
			++m_TtyDisableCount;
			Debug.Assert(m_TtyDisableCount != 0);
		}

		////////////////////////////////////////////////////////////////////////
		// EnableTtyOutput
		////////////////////////////////////////////////////////////////////////
		private void EnableTtyOutput()
		{
			Debug.Assert(m_TtyDisableCount != 0);
			--m_TtyDisableCount;
		}

		////////////////////////////////////////////////////////////////////////
		// ForceEnableTtyOutput
		////////////////////////////////////////////////////////////////////////
		private void ForceEnableTtyOutput()
		{
			m_TtyDisableCount = 0;
		}

		////////////////////////////////////////////////////////////////////////
		// xboxConsole_OnStdNotify
		////////////////////////////////////////////////////////////////////////
		private void xboxConsole_OnStdNotify(XboxDebugEventType eventCode, IXboxEventInfo eventInfo)
		{
			// Just ignore first chance exceptions, we'll catch it on the second
			// chance.  Otherwise we get issues with double handling.  This is
			// especially problematic after executing target callbacks.
			if(XDevkit.XboxDebugEventType.Exception == eventCode)
			{
				if((eventInfo.Info.Flags & XboxExceptionFlags.FirstChance) != 0)
				{
					return;
				}
			}

			// Callbacks are now done by setting LR=0, so that we get an
			// exception when the function returns.  Legacy code used a
			// __debugbreak(), which will trigger an ExecutionBreak event
			// instead.
			if(XDevkit.XboxDebugEventType.Exception      == eventCode ||
			   XDevkit.XboxDebugEventType.ExecutionBreak == eventCode)
			{
				if(null != m_OnStopCallback)
				{
					OnEventCallback callback = m_OnStopCallback;
					m_OnStopCallback = null;
					callback();
					return;
				}
			}
			else if(XDevkit.XboxDebugEventType.ExecStateChange == eventCode)
			{
				lock(m_Lock)
				{
					m_IsStopped = (XboxExecutionState.Stopped == eventInfo.Info.ExecState);
				}
			}

			TargetEventArgs e = new TargetEventArgs();

			e.m_Message = eventInfo.Info.Message;
			if(null != eventInfo.Info.Thread)
			{
				e.m_ThreadId = eventInfo.Info.Thread.ThreadId;
			}
			else
			{
				e.m_ThreadId = ~0u;
			}
			e.m_Address = eventInfo.Info.Address;
			e.m_DataAddress = 0;
			e.m_ExceptionCode = eventInfo.Info.Code;
			e.m_IsThreadStopped = (eventInfo.Info.IsThreadStopped != 0);

			switch(eventCode)
			{
				case XDevkit.XboxDebugEventType.DebugString:
					// Rather than just passing on the event to the plugins,
					// make sure we go through Target.TtyOutput so that tty
					// capture to string works.
					e = null;
					// Check for special R*TM commands embedded in the TTY.
					// Testkits need to use this method since the R*TM cannot
					// respond properly to a regular sysTmCmd* function.  If
					// R*TM is slow at processing messages, they get combined,
					// so need to split them back up into individual lines.
					var msg = eventInfo.Info.Message;
					var lines = msg.Split('\n');
					uint numLines = (uint)lines.Length;
					for(uint i=0; i<numLines; ++i)
					{
						var line = lines[i];
						if(line.StartsWith(">>R*TM "))
						{
							TestkitCmdHandler(line.Remove(0,7));
						}
						else if(m_TtyDisableCount == 0)
						{
							// Pass on as normal TTY
							if(i+1 < numLines)
							{
								TtyOutput(line+"\n");
							}
							else if(line != "")
							{
								TtyOutput(line);
							}
						}
					}
					break;

				case XDevkit.XboxDebugEventType.ExecStateChange:
					switch (eventInfo.Info.ExecState)
					{
						case XboxExecutionState.Stopped:
							e.m_EventType = TargetEventType.Stopped;
							break;

						case XboxExecutionState.Running:
							e.m_EventType = TargetEventType.Running;

							// Whether or not the next reboot should start a new
							// log file, depends on what is being run.	Anything
							// system related seems to start with "\Device\", so
							// no need to create a new log for them.
							string name = null;

							// Are we checking in the correct event callback
							// here?  Getting empty strings for a while :/
							for(;;)
							{
								name = m_Xbc.RunningProcessInfo.ProgramName;
								if(name != "")
								{
									break;
								}
								Thread.Sleep(300);
							}
							m_ProcessId   = 0;
							m_ProgramName = null;   // don't set to name, ConnectToGame will do that for us if appropriate

							// If we are in the process of handling an exception
							// on a testkit, then look for after the system
							// executable ProcessDump has completed.  Then it is
							// safe to grab the coredump.
							if(m_TestkitCrashState != null)
							{
								if(name == @"\Device\Flash\ProcessDump.Xex")
								{
									m_TestkitCrashState.ProcessingDump = true;
								}
								else if(m_TestkitCrashState.ProcessingDump)
								{
									FinishTestkitCrashHandling();
								}
							}

							if(!Regex.IsMatch(name, @"^\\Device\\.*$"))
							{
								ForceEnableTtyOutput();

								// If launching the game after a previous crash, then we won't have
								// already started a new log file yet, so do that here
								if(m_StartNewLog)
								{
									RequestNewLogFile();
								}
								m_StartNewLog = true;

								ConnectToGame();
							}
							break;

						case XboxExecutionState.Rebooting:
						case XboxExecutionState.RebootingTitle:
							ReleaseDebugger();
							e.m_EventType = TargetEventType.Rebooting;
							if(m_StartNewLog && m_TestkitCrashState==null)
							{
								RequestNewLogFile();
								m_StartNewLog = false;
							}
							break;

//						case XboxExecutionState.Pending:
//						case XboxExecutionState.PendingTitle:
						default:
							//Not handled
							e = null;
							break;
					}
					break;

				case XDevkit.XboxDebugEventType.Exception:
					// Ignore the setthreadname exception
					if(eventInfo.Info.Code != 0x406D1388)
					{
						CrashHandler(eventCode, eventInfo, ref e);

						if(eventInfo.Info.Parameters[0] != 0)
						{
							e.m_EventType   = TargetEventType.MemoryWriteException;
							e.m_DataAddress = eventInfo.Info.Parameters[1];
						}
						else
						{
							e.m_EventType   = TargetEventType.MemoryReadException;
							e.m_DataAddress = eventInfo.Info.Parameters[1];
						}
					}
					else
					{
						e = null;
					}
					break;

				case XDevkit.XboxDebugEventType.AssertionFailed:
					//assert() causes this to fire.
					CrashHandler(eventCode, eventInfo);
					e.m_EventType = TargetEventType.AssertionFailed;
					break;

				case XDevkit.XboxDebugEventType.AssertionFailedEx:
					CrashHandler(eventCode, eventInfo);
					e.m_EventType = TargetEventType.AssertionFailed;
					break;

				case XDevkit.XboxDebugEventType.ExecutionBreak:
					//Execution breakpoints and __debugbreak cause this to fire.
					CrashHandler(eventCode, eventInfo);
					e.m_EventType = TargetEventType.ExecutionBreak;
					break;

				case XDevkit.XboxDebugEventType.DataBreak:
					//Data breakpoints cause this to fire.
					CrashHandler(eventCode, eventInfo);
					e.m_EventType = TargetEventType.DataBreak;
					break;

				case XDevkit.XboxDebugEventType.RIP:
					CrashHandler(eventCode, eventInfo);
					e.m_EventType = TargetEventType.Rip;
					break;

//				case XDevkit.XboxDebugEventType.SingleStep:
//				case XDevkit.XboxDebugEventType.ModuleLoad:
//				case XDevkit.XboxDebugEventType.ModuleUnload:
//				case XDevkit.XboxDebugEventType.ThreadCreate:
//				case XDevkit.XboxDebugEventType.ThreadDestroy:
//				case XDevkit.XboxDebugEventType.SectionLoad:
//				case XDevkit.XboxDebugEventType.SectionUnload:
//				case XDevkit.XboxDebugEventType.StackTrace:
//				case XDevkit.XboxDebugEventType.FiberCreate:
//				case XDevkit.XboxDebugEventType.FiberDestroy:
//				case XDevkit.XboxDebugEventType.BugCheck:
				default:
					//Not handled
					e = null;
					break;
			}

			if(null != e)
			{
				TargetEvent.Invoke(e);
			}
		}



		////////////////////////////////////////////////////////////////////////
		// TargetCallbackStateSaver
		////////////////////////////////////////////////////////////////////////
		// Saves and restores registers, and also suspends all but the
		// specified thread.
		private class TargetCallbackStateSaver : IDisposable
		{
			private IXboxDebugTarget m_Target;
			private bool m_SuspendedProcess;
			private Int32[]	 m_SavedRegs32 = new Int32 [Enum.GetValues(typeof(XboxRegisters32)).Length];
			private Int64[]	 m_SavedRegs64 = new Int64 [Enum.GetValues(typeof(XboxRegisters64)).Length];
			private double[] m_SavedRegsF  = new double[Enum.GetValues(typeof(XboxRegistersDouble)).Length];
			private float[]	 m_SavedRegsV  = new float [Enum.GetValues(typeof(XboxRegistersVector)).Length*4];
			private IXboxThread m_ExecuteThread;

			////////////////////////////////////////////////////////////////////
			// TargetCallbackStateSaver
			////////////////////////////////////////////////////////////////////
			public TargetCallbackStateSaver(IXboxDebugTarget target, IXboxThread executeThread)
			{
				m_Target = target;
				m_ExecuteThread = executeThread;

				bool alreadyStopped;
				target.Stop(out alreadyStopped);
				m_SuspendedProcess = !alreadyStopped;

				// Save all current register values for the thread
				IXboxStackFrame stk = executeThread.TopOfStack;
				uint i = 0;
				foreach(XboxRegisters32 r in Enum.GetValues(typeof(XboxRegisters32)))
				{
					stk.GetRegister32(r, out m_SavedRegs32[i++]);
				}
				i = 0;
				foreach(XboxRegisters64 r in Enum.GetValues(typeof(XboxRegisters64)))
				{
					stk.GetRegister64(r, out m_SavedRegs64[i++]);
				}
				i = 0;
				foreach(XboxRegistersDouble r in Enum.GetValues(typeof(XboxRegistersDouble)))
				{
					stk.GetRegisterDouble(r, out m_SavedRegsF[i++]);
				}
				i = 0;
				float[] arr = new float[4];
				foreach(XboxRegistersVector r in Enum.GetValues(typeof(XboxRegistersVector)))
				{
					stk.GetRegisterVector(r, arr);
					foreach(var v in arr)
					{
						m_SavedRegsV[i++] = v;
					}
				}
			}

			////////////////////////////////////////////////////////////////////
			// ~TargetCallbackStateSaver
			////////////////////////////////////////////////////////////////////
			~TargetCallbackStateSaver()
			{
				Dispose(false);
			}

			////////////////////////////////////////////////////////////////////
			// Dispose
			////////////////////////////////////////////////////////////////////
			public void Dispose()
			{
				Dispose(true);
				GC.SuppressFinalize(this);
			}

			////////////////////////////////////////////////////////////////////
			// Dispose
			////////////////////////////////////////////////////////////////////
			protected virtual void Dispose(bool disposing)
			{
				if(disposing)
				{
					try
					{
						// Restore registers
						IXboxStackFrame stk = m_ExecuteThread.TopOfStack;
						uint i = 0;
						float[] arr = new float[4];
						foreach(XboxRegistersVector r in Enum.GetValues(typeof(XboxRegistersVector)))
						{
							arr[0] = m_SavedRegsV[i+0];
							arr[1] = m_SavedRegsV[i+1];
							arr[2] = m_SavedRegsV[i+2];
							arr[3] = m_SavedRegsV[i+3];
							stk.SetRegisterVector(r, arr);
							i += 4;
						}
						i = 0;
						foreach(XboxRegistersDouble r in Enum.GetValues(typeof(XboxRegistersDouble)))
						{
							stk.SetRegisterDouble(r, m_SavedRegsF[i++]);
						}
						i = 0;
						foreach(XboxRegisters64 r in Enum.GetValues(typeof(XboxRegisters64)))
						{
							stk.SetRegister64(r, m_SavedRegs64[i++]);
						}
						i = 0;
						foreach(XboxRegisters32 r in Enum.GetValues(typeof(XboxRegisters32)))
						{
							stk.SetRegister32(r, m_SavedRegs32[i++]);
						}

						stk.FlushRegisterChanges();

						// Resume process
						if(m_SuspendedProcess)
						{
							bool notStopped;
							m_Target.Go(out notStopped);
						}
					}
					catch
					{
						Trace.WriteLine("ERROR: Failed to restore thread registers after callback");
					}
				}
			}
		}
	}
}
