namespace Rockstar.TargetManager
{
	public enum TMCommands
	{
		NOP                                         = 0x00,
		PRINT_ALL_THREAD_STACKS                     = 0x01,
		STOP_NO_ERROR_REPORT                        = 0x02,
		QUITF                                       = 0x03,
		GPU_HANG                                    = 0x04,
		CPU_HANG                                    = 0x05,
		CONFIG                                      = 0x06,
		CREATE_DUMP                                 = 0x07,
		EXCEPTION_HANDLER_BEGIN                     = 0x08,
		EXCEPTION_HANDLER_END                       = 0x09,

		DURANGO_DEPRECATED_EXCEPTION_HANDLER_BEGIN  = 0x80,
		DURANGO_DEPRECATED_EXCEPTION_HANDLER_END    = 0x81,
		DURANGO_THREAD_BEGIN                        = 0x82,
		DURANGO_THREAD_END                          = 0x83,
		DURANGO_REPORT_GPU_HANG_HACK                = 0x84,
		DURANGO_GET_GPU_COMMAND_BUFFER_ACCESS       = 0x85,

		ORBIS_SUBMIT_DONE_EXCEPTION_ENABLED         = 0x80,
	}
}
