using System;
using System.Runtime.InteropServices;


public class Win32
{
	public const  Int32  AddrModeFlat                                   = 3;
	public const  UInt32 CONTEXT_AMD64                                  = 0x00100000;
	public const  UInt32 CONTEXT_CONTROL                                = (CONTEXT_AMD64 | 0x00000001);
	public const  UInt32 CONTEXT_INTEGER                                = (CONTEXT_AMD64 | 0x00000002);
	public const  UInt32 CONTEXT_SEGMENTS                               = (CONTEXT_AMD64 | 0x00000004);
	public const  UInt32 CONTEXT_FLOATING_POINT                         = (CONTEXT_AMD64 | 0x00000008);
	public const  UInt32 CONTEXT_DEBUG_REGISTERS                        = (CONTEXT_AMD64 | 0x00000010);
	public const  UInt32 CONTEXT_FULL                                   = (CONTEXT_CONTROL | CONTEXT_INTEGER | CONTEXT_FLOATING_POINT);
	public const  UInt32 CONTEXT_ALL                                    = (CONTEXT_CONTROL | CONTEXT_INTEGER | CONTEXT_SEGMENTS | CONTEXT_FLOATING_POINT | CONTEXT_DEBUG_REGISTERS);
	public const  UInt32 CONTEXT_XSTATE                                 = (CONTEXT_AMD64 | 0x00000040);
	public const  UInt32 CP_ACP                                         = 0;
	public const  Int32  CREATE_ALWAYS                                  = 2;
	public const  Int32  CREATE_NEW                                     = 1;
	public const  Int32  CREATE_NO_WINDOW                               = 0x08000000;
	public const  UInt32 CREATE_PROCESS_DEBUG_EVENT                     = 3;
	public const  UInt32 CREATE_THREAD_DEBUG_EVENT                      = 2;
	public const  Int32  CTRL_C_EVENT                                   = 0;
	public const  UInt32 DBG_CONTINUE                                   = 0x00010002;
	public const  UInt32 DBG_EXCEPTION_NOT_HANDLED                      = 0x80010001;
	public const  Int32  DEBUG_ONLY_THIS_PROCESS                        = 2;
	public const  Int32  DEBUG_PROCESS                                  = 1;
	public const  UInt32 DELETE                                         = 0x00010000;
	public const  Int32  ERROR_ACCESS_DENIED                            = 5;
	public const  UInt32 EXCEPTION_ACCESS_VIOLATION                     = 0xc0000005;
	public const  UInt32 EXCEPTION_ARRAY_BOUNDS_EXCEEDED                = 0xc000008c;
	public const  UInt32 EXCEPTION_BREAKPOINT                           = 0x80000003;
	public const  UInt32 EXCEPTION_DATATYPE_MISALIGNMENT                = 0x80000002;
	public const  UInt32 EXCEPTION_DEBUG_EVENT                          = 1;
	public const  UInt32 EXCEPTION_FLT_DENORMAL_OPERAND                 = 0xc000008d;
	public const  UInt32 EXCEPTION_FLT_DIVIDE_BY_ZERO                   = 0xc000008e;
	public const  UInt32 EXCEPTION_FLT_INEXACT_RESULT                   = 0xc000008f;
	public const  UInt32 EXCEPTION_FLT_INVALID_OPERATION                = 0xc0000090;
	public const  UInt32 EXCEPTION_FLT_OVERFLOW                         = 0xc0000091;
	public const  UInt32 EXCEPTION_FLT_STACK_CHECK                      = 0xc0000092;
	public const  UInt32 EXCEPTION_FLT_UNDERFLOW                        = 0xc0000093;
	public const  UInt32 EXCEPTION_GUARD_PAGE                           = 0x80000001;
	public const  UInt32 EXCEPTION_ILLEGAL_INSTRUCTION                  = 0xc000001d;
	public const  UInt32 EXCEPTION_INT_DIVIDE_BY_ZERO                   = 0xc0000094;
	public const  UInt32 EXCEPTION_INT_OVERFLOW                         = 0xc0000095;
	public const  UInt32 EXCEPTION_INVALID_DISPOSITION                  = 0xc0000026;
	public const  UInt32 EXCEPTION_INVALID_HANDLE                       = 0xc0000008;
	public const  UInt32 EXCEPTION_IN_PAGE_ERROR                        = 0xc0000006;
	public const  int    EXCEPTION_MAXIMUM_PARAMETERS                   = 15;
	public const  UInt32 EXCEPTION_NONCONTINUABLE_EXCEPTION             = 0xc0000025;
	public const  UInt32 EXCEPTION_POSSIBLE_DEADLOCK                    = 0xc0000194;
	public const  UInt32 EXCEPTION_PRIV_INSTRUCTION                     = 0xc0000096;
	public const  UInt32 EXCEPTION_SINGLE_STEP                          = 0x80000004;
	public const  UInt32 EXCEPTION_STACK_OVERFLOW                       = 0xc00000fd;
	public const  UInt32 EXIT_PROCESS_DEBUG_EVENT                       = 5;
	public const  UInt32 EXIT_THREAD_DEBUG_EVENT                        = 4;
	public const  Int32  FILE_ATTRIBUTE_NORMAL                          = 0x80;
	public const  Int32  FILE_SHARE_READ                                = 1;
	public const  Int32  FILE_SHARE_WRITE                               = 2;
	public const  UInt32 GET_MODULE_HANDLE_EX_FLAG_FROM_ADDRESS         = 0x00000004;
	public const  UInt32 GET_MODULE_HANDLE_EX_FLAG_PIN                  = 0x00000001;
	public const  UInt32 GET_MODULE_HANDLE_EX_FLAG_UNCHANGED_REFCOUNT   = 0x00000002;
	public const  Int32  GENERIC_EXECUTE                                = 0x20000000;
	public const  Int32  GENERIC_READ                                   = 0x40000000<<1;    // stupid hack to generate 0x80000000 as an Int32
	public const  Int32  GENERIC_WRITE                                  = 0x40000000;
	public const  Int32  HANDLE_FLAG_INHERIT                            = 1;
	public const  UInt32 IMAGE_FILE_MACHINE_AMD64                       = 0x8664;
	public const  Int32  HANDLE_FLAG_PROTECT_FROM_CLOSE                 = 2;
	public static IntPtr INVALID_HANDLE_VALUE                           = (IntPtr)(-1);
	public const  Int32  JOB_OBJECT_LIMIT_KILL_ON_JOB_CLOSE             = 0x00002000;
	public const  UInt32 LOAD_DLL_DEBUG_EVENT                           = 6;
	public const  Int32  MAILSLOT_NO_MESSAGE                            = -1;
	public const  Int32  MAILSLOT_WAIT_FOREVER                          = -1;
	public const  int    MAX_PATH                                       = 260;
	public const  Int32  MiniDumpFilterMemory                           = 0x00000008;
	public const  Int32  MiniDumpFilterModulePaths                      = 0x00000080;
	public const  Int32  MiniDumpFilterTriage                           = 0x00100000;
	public const  Int32  MiniDumpIgnoreInaccessibleMemory               = 0x00020000;
	public const  Int32  MiniDumpNormal                                 = 0x00000000;
	public const  Int32  MiniDumpScanMemory                             = 0x00000010;
	public const  Int32  MiniDumpValidTypeFlags                         = 0x001fffff;
	public const  Int32  MiniDumpWithCodeSegs                           = 0x00002000;
	public const  Int32  MiniDumpWithDataSegs                           = 0x00000001;
	public const  Int32  MiniDumpWithFullAuxiliaryState                 = 0x00008000;
	public const  Int32  MiniDumpWithFullMemory                         = 0x00000002;
	public const  Int32  MiniDumpWithFullMemoryInfo                     = 0x00000800;
	public const  Int32  MiniDumpWithHandleData                         = 0x00000004;
	public const  Int32  MiniDumpWithIndirectlyReferencedMemory         = 0x00000040;
	public const  Int32  MiniDumpWithModuleHeaders                      = 0x00080000;
	public const  Int32  MiniDumpWithPrivateReadWriteMemory             = 0x00000200;
	public const  Int32  MiniDumpWithPrivateWriteCopyMemory             = 0x00010000;
	public const  Int32  MiniDumpWithProcessThreadData                  = 0x00000100;
	public const  Int32  MiniDumpWithThreadInfo                         = 0x00001000;
	public const  Int32  MiniDumpWithTokenInformation                   = 0x00040000;
	public const  Int32  MiniDumpWithUnloadedModules                    = 0x00000020;
	public const  Int32  MiniDumpWithoutAuxiliaryState                  = 0x00004000;
	public const  Int32  MiniDumpWithoutOptionalData                    = 0x00000400;
	public const  UInt32 MS_VC_EXCEPTION                                = 0x406d1388;
	public const  Int32  OPEN_ALWAYS                                    = 4;
	public const  Int32  OPEN_EXISTING                                  = 3;
	public const  UInt32 OUTPUT_DEBUG_STRING_EVENT                      = 8;
	public const  UInt32 PROCESS_CREATE_PROCESS                         = 0x0080;
	public const  UInt32 PROCESS_CREATE_THREAD                          = 0x0002;
	public const  UInt32 PROCESS_DUP_HANDLE                             = 0x0040;
	public const  UInt32 PROCESS_QUERY_INFORMATION                      = 0x0400;
	public const  UInt32 PROCESS_QUERY_LIMITED_INFORMATION              = 0x1000;
	public const  UInt32 PROCESS_SET_INFORMATION                        = 0x0200;
	public const  UInt32 PROCESS_SET_QUOTA                              = 0x0100;
	public const  UInt32 PROCESS_SUSPEND_RESUME                         = 0x0800;
	public const  UInt32 PROCESS_TERMINATE                              = 0x0001;
	public const  UInt32 PROCESS_VM_OPERATION                           = 0x0008;
	public const  UInt32 PROCESS_VM_READ                                = 0x0010;
	public const  UInt32 PROCESS_VM_WRITE                               = 0x0020;
	public const  UInt32 READ_CONTROL                                   = 0x00020000;
	public const  UInt32 RIP_EVENT                                      = 9;
	public const  Int32  STARTF_USESHOWWINDOW                           = 0x00000001;
	public const  Int32  STARTF_USESTDHANDLES                           = 0x00000100;
	public const  UInt32 STATUS_WX86_BREAKPOINT                         = 0x4000001f;
	public const  Int16  SW_HIDE                                        = 0;
	public const  UInt32 SYMOPT_ALLOW_ABSOLUTE_SYMBOLS                  = 0x00000800;
	public const  UInt32 SYMOPT_ALLOW_ZERO_ADDRESS                      = 0x01000000;
	public const  UInt32 SYMOPT_AUTO_PUBLICS                            = 0x00010000;
	public const  UInt32 SYMOPT_CASE_INSENSITIVE                        = 0x00000001;
	public const  UInt32 SYMOPT_DEBUG                                   = 0x80000000;
	public const  UInt32 SYMOPT_DEFERRED_LOADS                          = 0x00000004;
	public const  UInt32 SYMOPT_DISABLE_SYMSRV_AUTODETECT               = 0x02000000;
	public const  UInt32 SYMOPT_EXACT_SYMBOLS                           = 0x00000400;
	public const  UInt32 SYMOPT_FAIL_CRITICAL_ERRORS                    = 0x00000200;
	public const  UInt32 SYMOPT_FAVOR_COMPRESSED                        = 0x00800000;
	public const  UInt32 SYMOPT_FLAT_DIRECTORY                          = 0x00400000;
	public const  UInt32 SYMOPT_IGNORE_CVREC                            = 0x00000080;
	public const  UInt32 SYMOPT_IGNORE_IMAGEDIR                         = 0x00200000;
	public const  UInt32 SYMOPT_IGNORE_NT_SYMPATH                       = 0x00001000;
	public const  UInt32 SYMOPT_INCLUDE_32BIT_MODULES                   = 0x00002000;
	public const  UInt32 SYMOPT_LOAD_ANYTHING                           = 0x00000040;
	public const  UInt32 SYMOPT_LOAD_LINES                              = 0x00000010;
	public const  UInt32 SYMOPT_NO_CPP                                  = 0x00000008;
	public const  UInt32 SYMOPT_NO_IMAGE_SEARCH                         = 0x00020000;
	public const  UInt32 SYMOPT_NO_PROMPTS                              = 0x00080000;
	public const  UInt32 SYMOPT_NO_PUBLICS                              = 0x00008000;
	public const  UInt32 SYMOPT_NO_UNQUALIFIED_LOADS                    = 0x00000100;
	public const  UInt32 SYMOPT_OVERWRITE                               = 0x00100000;
	public const  UInt32 SYMOPT_PUBLICS_ONLY                            = 0x00004000;
	public const  UInt32 SYMOPT_SECURE                                  = 0x00040000;
	public const  UInt32 SYMOPT_UNDNAME                                 = 0x00000002;
	public const  UInt32 SYNCHRONIZE                                    = 0x00100000;
	public const  UInt32 THREAD_DIRECT_IMPERSONATION                    = 0x0200;
	public const  UInt32 THREAD_GET_CONTEXT                             = 0x0008;
	public const  UInt32 THREAD_IMPERSONATE                             = 0x0100;
	public const  UInt32 THREAD_QUERY_INFORMATION                       = 0x0040;
	public const  UInt32 THREAD_QUERY_LIMITED_INFORMATION               = 0x0800;
	public const  UInt32 THREAD_SET_CONTEXT                             = 0x0010;
	public const  UInt32 THREAD_SET_INFORMATION                         = 0x0020;
	public const  UInt32 THREAD_SET_LIMITED_INFORMATION                 = 0x0400;
	public const  UInt32 THREAD_SET_THREAD_TOKEN                        = 0x0080;
	public const  UInt32 THREAD_SUSPEND_RESUME                          = 0x0002;
	public const  UInt32 THREAD_TERMINATE                               = 0x0001;
	public const  Int32  TRUNCATE_EXISTING                              = 5;
	public const  UInt32 UNLOAD_DLL_DEBUG_EVENT                         = 7;
	public const  UInt32 WRITE_DAC                                      = 0x00040000;
	public const  UInt32 WRITE_OWNER                                    = 0x00080000;




	public enum JOBOBJECTINFOCLASS
	{
		JobObjectBasicLimitInformation                          = 2,
		JobObjectBasicUIRestrictions                            = 4,
		JobObjectSecurityLimitInformation                       = 5,
		JobObjectEndOfJobTimeInformation                        = 6,
		JobObjectAssociateCompletionPortInformation             = 7,
		JobObjectExtendedLimitInformation                       = 9,
		JobObjectGroupInformation                               = 11,
		JobObjectNotificationLimitInformation                   = 12,
		JobObjectGroupInformationEx                             = 14,
		JobObjectCpuRateControlInformation                      = 15,
	}

	public enum SYM_TYPE
	{
		SymNone = 0,        // SYM_TYPE enum
	    SymCoff,
	    SymCv,
	    SymPdb,
	    SymExport,
	    SymDeferred,
	    SymSym,             // .sym file
	    SymDia,
	    SymVirtual,
	    NumSymTypes
	}


	[StructLayout(LayoutKind.Sequential)]
	public struct M128A
	{
		public UInt64   low;
		public UInt64   high;
	}

	// WARNING: This structure must be 16-byte aligned.
	[StructLayout(LayoutKind.Sequential)]
	public unsafe struct CONTEXT
	{
		public UInt64       p1Home;
		public UInt64       p2Home;
		public UInt64       p3Home;
		public UInt64       p4Home;
		public UInt64       p5Home;
		public UInt64       p6Home;

		public UInt32       contextFlags;
		public UInt32       mxCsr;

		public UInt16       segCs;
		public UInt16       segDs;
		public UInt16       segEs;
		public UInt16       segFs;
		public UInt16       segGs;
		public UInt16       segSs;
		public UInt32       eFlags;

		public UInt64       dr0;
		public UInt64       dr1;
		public UInt64       dr2;
		public UInt64       dr3;
		public UInt64       dr6;
		public UInt64       dr7;

		public UInt64       rax;
		public UInt64       rcx;
		public UInt64       rdx;
		public UInt64       rbx;
		public UInt64       rsp;
		public UInt64       rbp;
		public UInt64       rsi;
		public UInt64       rdi;
		public UInt64       r8;
		public UInt64       r9;
		public UInt64       r10;
		public UInt64       r11;
		public UInt64       r12;
		public UInt64       r13;
		public UInt64       r14;
		public UInt64       r15;

		public UInt64       rip;

		public UInt16       controlWord;
		public UInt16       statusWord;
		public byte         tagWord;
		public byte         reserved1;
		public UInt16       errorOpcode;
		public UInt32       errorOffset;
		public UInt16       errorSelector;
		public UInt16       reserved2;
		public UInt32       dataOffset;
		public UInt16       dataSelector;
		public UInt32       reserved3;
		public UInt32       mxCsr2;
		public UInt32       mxCsr_Mask;

		public fixed ulong  floatRegisters[8*2];        // should be an array of 8*M128A,  but that isn't allowed with fixed.
		public fixed ulong  xmmRegisters[16*2];         // should be an array of 16*M128A, but that isn't allowed with fixed.
		public fixed byte   reserved4[96];

		public fixed ulong  vectorRegister[26*2];       // should be an array of 26*M128A, but that isn't allowed with fixed.
		public UInt64       vectorControl;

		public UInt64       debugControl;
		public UInt64       lastBranchToRip;
		public UInt64       lastBranchFromRip;
		public UInt64       lastExceptionToRip;
		public UInt64       lastExceptionFromRip;
	}

	[StructLayout(LayoutKind.Sequential)]
	public unsafe struct EXCEPTION_RECORD
	{
		public UInt32       exceptionCode;
		public UInt32       exceptionFlags;
		public IntPtr       exceptionRecord;
		public IntPtr       exceptionAddress;
		public UInt32       numberParameters;
		public fixed ulong  exceptionInformation[EXCEPTION_MAXIMUM_PARAMETERS];
	}

	[StructLayout(LayoutKind.Sequential)]
	public struct EXCEPTION_DEBUG_INFO
	{
		public EXCEPTION_RECORD     exceptionRecord;
		public UInt32               firstChance;
	}

	[StructLayout(LayoutKind.Sequential)]
	public struct CREATE_THREAD_DEBUG_INFO
	{
		public IntPtr   thread;
		public IntPtr   threadLocalBase;
		public IntPtr   startAddress;
	}

	[StructLayout(LayoutKind.Sequential)]
	public struct CREATE_PROCESS_DEBUG_INFO
	{
		public IntPtr   file;
		public IntPtr   process;
		public IntPtr   thread;
		public IntPtr   baseOfImage;
		public UInt32   debugInfoFileOffset;
		public UInt32   debugInfoSi;
		public IntPtr   threadLocalBase;
		public IntPtr   startAddress;
		public IntPtr   imageName;
		public UInt16   unicode;
	}

	[StructLayout(LayoutKind.Sequential)]
	public struct EXIT_THREAD_DEBUG_INFO
	{
		public UInt32   exitCode;
	}

	[StructLayout(LayoutKind.Sequential)]
	public struct EXIT_PROCESS_DEBUG_INFO
	{
		public UInt32   exitCode;
	}

	[StructLayout(LayoutKind.Sequential)]
	public struct LOAD_DLL_DEBUG_INFO
	{
		public IntPtr   file;
		public IntPtr   baseOfDll;
		public UInt32   debugInfoFileOffset;
		public UInt32   debugInfoSize;
		public IntPtr   imageName;
		public UInt16   unicode;
	}

	[StructLayout(LayoutKind.Sequential)]
	public struct UNLOAD_DLL_DEBUG_INFO
	{
		public IntPtr   baseOfDll;
	}

	[StructLayout(LayoutKind.Sequential)]
	public struct OUTPUT_DEBUG_STRING_INFO
	{
		public IntPtr   debugStringData;
		public UInt16   unicode;
		public UInt16   debugStringLength;
	}

	[StructLayout(LayoutKind.Sequential)]
	public struct RIP_INFO
	{
		public UInt32   error;
		public UInt32   type;
	}

	// This union is an anonymous union in the Win32 ABI, but giving it a name makes the C# code neater
	[StructLayout(LayoutKind.Explicit)]
	public struct DEBUG_EVENT_UNION
	{
		[FieldOffset(0)]    public EXCEPTION_DEBUG_INFO         exception;
		[FieldOffset(0)]    public CREATE_THREAD_DEBUG_INFO     createThread;
		[FieldOffset(0)]    public CREATE_PROCESS_DEBUG_INFO    createProcessInfo;  // this inconsistent Info suffix is like this in the windows api
		[FieldOffset(0)]    public EXIT_THREAD_DEBUG_INFO       exitThread;
		[FieldOffset(0)]    public EXIT_PROCESS_DEBUG_INFO      exitProcess;
		[FieldOffset(0)]    public LOAD_DLL_DEBUG_INFO          loadDll;
		[FieldOffset(0)]    public UNLOAD_DLL_DEBUG_INFO        unloadDll;
		[FieldOffset(0)]    public OUTPUT_DEBUG_STRING_INFO     debugString;
		[FieldOffset(0)]    public RIP_INFO                     ripInfo;
	}

	[StructLayout(LayoutKind.Sequential)]
	public struct DEBUG_EVENT
	{
		public UInt32               debugEventCode;
		public UInt32               processId;
		public UInt32               threadId;
		public DEBUG_EVENT_UNION    u;
	}

	[StructLayout(LayoutKind.Sequential)]
	public unsafe struct GUID
	{
		public UInt32       data1;
		public UInt16       data2;
		public UInt16       data3;
		public fixed byte   data4[8];
	}

	[StructLayout(LayoutKind.Sequential)]
	public struct IMAGEHLP_LINE64
	{
		public UInt32   sizeOfStruct;
		public IntPtr   key;
		public UInt32   lineNumber;
		public IntPtr   fileName;
		public UInt64   address;
	}

	public const int IMAGEHLP_MODULEW64_NELEM_MODULENAME        = 32;
	public const int IMAGEHLP_MODULEW64_NELEM_IMAGENAME         = 256;
	public const int IMAGEHLP_MODULEW64_NELEM_LOADEDIMAGENAME   = 256;
	public const int IMAGEHLP_MODULEW64_NELEM_LOADEDPDBNAME     = 256;

	// C# is piece of shit.  Marshaling fixed size arrays is buggy when the
	// structure is not bltable (ie, the marshaler can't just do a memcpy).
	// bool is not a bltable type, so instead use Int32 (the Win32 API BOOL is
	// 32-bits).  Similarly the fixed arrays are really WCHARs, but char is also
	// a non-bltable type.
	//
	// See
	// http://go4answers.webhost4life.com/Example/why-does-first-element-fixed-array-25500.aspx
	// and also
	// http://stackoverflow.com/questions/9152119/marshal-structuretoptr-fails-with-bool-and-fixed-size-array.
	//
	[StructLayout(LayoutKind.Sequential)]
	public unsafe struct IMAGEHLP_MODULEW64
	{
		public UInt32       sizeOfStruct;
		public UInt64       baseOfImage;
		public UInt32       imageSize;
		public UInt32       timeDateStamp;
		public UInt32       checkSum;
		public UInt32       numSyms;
		public Int32        symType;            // SYM_TYPE enum
		public fixed UInt16 moduleName[IMAGEHLP_MODULEW64_NELEM_MODULENAME];
		public fixed UInt16 imageName[IMAGEHLP_MODULEW64_NELEM_IMAGENAME];
		public fixed UInt16 loadedImageName[IMAGEHLP_MODULEW64_NELEM_LOADEDIMAGENAME];
		public fixed UInt16 loadedPdbName[IMAGEHLP_MODULEW64_NELEM_LOADEDPDBNAME];
		public UInt32       cvSig;
		public fixed UInt16 cvData[MAX_PATH*3];
		public UInt32       pdbSig;
		public GUID         pdbSig70;
		public UInt32       pdbAge;
		public Int32        pdbUnmatched;       // bool
		public Int32        dbgUnmatched;       // bool
		public Int32        lineNumbers;        // bool
		public Int32        globalSymbols;      // bool
		public Int32        typeInfo;           // bool
		public Int32        sourceIndexed;      // bool
		public Int32        publics;            // bool
	}

	[StructLayout(LayoutKind.Sequential)]
	public struct IO_COUNTERS
	{
		public UInt64   readOperationCount;
		public UInt64   writeOpterationCount;
		public UInt64   otherOperationCount;
		public UInt64   readTransferCount;
		public UInt64   writeTransferCount;
		public UInt64   otherTransferCount;
	}

	[StructLayout(LayoutKind.Sequential)]
	public struct JOBOBJECT_BASIC_LIMIT_INFORMATION
	{
		public Int64    perProcessUserTimeLimit;
		public Int64    perJobUserTimeLimit;
		public Int32    limitFlags;
		public IntPtr   minimumWorkingSetSize;
		public IntPtr   maximumWorkingSetSize;
		public Int32    activeProcessLimit;
		public IntPtr   affinity;
		public Int32    priorityClass;
		public Int32    schedulingClass;
	}

	[StructLayout(LayoutKind.Sequential)]
	public struct JOBOBJECT_EXTENDED_LIMIT_INFORMATION
	{
		public JOBOBJECT_BASIC_LIMIT_INFORMATION    basicLimitInformation;
		public IO_COUNTERS                          ioInfo;
		public IntPtr                               processMemoryLimit;
		public IntPtr                               jobMemoryLimit;
		public IntPtr                               peakProcessMemoryUsed;
		public IntPtr                               peakJobMemoryUsed;
	}

	[StructLayout(LayoutKind.Sequential)]
	public struct MODULEINFO
	{
		public IntPtr   baseOfDll;
		public UInt32   sizeOfImage;
		public IntPtr   entryPoint;
	}

	[StructLayout(LayoutKind.Sequential)]
	public struct PROCESS_INFORMATION
	{
		public IntPtr process;
		public IntPtr thread;
		public Int32  processId;
		public Int32  threadId;
	}

	[StructLayout(LayoutKind.Sequential)]
	public struct SECURITY_ATTRIBUTES
	{
		public Int32  length;
		public IntPtr securityDescriptor;
		public bool   inheritHandle;
	}

	[StructLayout(LayoutKind.Sequential)]
	public struct ADDRESS64
	{
		public UInt64   offset;
		public UInt16   segment;
		public Int32    mode;
	}

	[StructLayout(LayoutKind.Sequential)]
	public struct KDHELP64
	{
		public UInt64   thread;
		public UInt32   thCallbackStack;
		public UInt32   thCallbackBStore;
		public UInt32   nextCallback;
		public UInt32   framePointer;
		public UInt64   kiCallUserMode;
		public UInt64   keUserCallbackDispatcher;
		public UInt64   systemRangeStart;
		public UInt64   kiUserExceptionDispatcher;
		public UInt64   stackBase;
		public UInt64   stackLimit;
		public UInt64   reserved0;
		public UInt64   reserved1;
		public UInt64   reserved2;
		public UInt64   reserved3;
		public UInt64   reserved4;
	}

	[StructLayout(LayoutKind.Sequential)]
	public struct EXCEPTION_POINTERS
	{
		public IntPtr   exceptionRecord;
		public IntPtr   contextRecord;
	}

	// It is not documented on MSDN, but in DbgHelp.h, there is a #include
	// <pshpack4.h> a while before the definition of
	// MINIDUMP_EXCEPTION_INFORMATION, hence we need the Pack=4 here.
	[StructLayout(LayoutKind.Sequential, Pack=4)]
	public struct MINIDUMP_EXCEPTION_INFORMATION
	{
		public UInt32   threadId;
		public IntPtr   exceptionPointers;
		public bool     clientPointers;
	}

	[StructLayout(LayoutKind.Sequential)]
	public struct STACKFRAME64
	{
		public ADDRESS64    addrPC;
		public ADDRESS64    addrReturn;
		public ADDRESS64    addrFrame;
		public ADDRESS64    addrStack;
		public ADDRESS64    addrBStore;
		public UInt64       funcTableEntry;
		public UInt64       param0;
		public UInt64       param1;
		public UInt64       param2;
		public UInt64       param3;
		public bool         far;
		public bool         virtual_;
		public UInt64       reserved0;
		public UInt64       reserved1;
		public UInt64       reserved2;
		public KDHELP64     kdHelp;
	}

	[StructLayout(LayoutKind.Sequential, CharSet=CharSet.Unicode)]
	public struct STARTUPINFO
	{
		public Int32  cb;
		public string reserved;
		public string desktop;
		public string title;
		public Int32  x;
		public Int32  y;
		public Int32  xSize;
		public Int32  ySize;
		public Int32  xCountChars;
		public Int32  yCountChars;
		public Int32  fillAttribute;
		public Int32  flags;
		public Int16  showWindow;
		public Int16  reserved2;
		public IntPtr reserved3;
		public IntPtr stdInput;
		public IntPtr stdOutput;
		public IntPtr stdError;
	}

	[StructLayout(LayoutKind.Sequential, Pack=1)]
	public struct SYMBOL_INFO
	{
		public UInt32   sizeOfStruct;
		public UInt32   typeIndex;
		public UInt64   reserved0;
		public UInt64   reserved1;
		public UInt32   index;
		public UInt32   size;
		public UInt64   modBase;
		public UInt32   flags;
		public UInt32   _pad;
		public UInt64   value;
		public UInt64   address;
		public UInt32   register_;
		public UInt32   scope;
		public UInt32   tag;
		public UInt32   nameLen;
		public UInt32   maxNameLen;
		// public char name[*]
	}

	[StructLayout(LayoutKind.Sequential)]
	public struct THREADNAME_INFO
	{
		public UInt32   type;
		public IntPtr   name;
		public UInt32   threadId;
		public UInt32   flags;
	}





	[DllImport("kernel32.dll", SetLastError=true)]
	public static extern bool AssignProcessToJobObject(IntPtr handle, IntPtr process);

	[DllImport("kernel32.dll", SetLastError=true)]
	public static extern bool AttachConsole(Int32 processId);

	[DllImport("kernel32.dll", SetLastError=true)]
	public static extern bool CloseHandle(IntPtr obj);

	[DllImport("kernel32.dll", SetLastError=true)]
	public static extern bool ContinueDebugEvent(UInt32 processId, UInt32 threadId, UInt32 continueStatus);

	[DllImport("kernel32.dll", SetLastError=true)]
	public static extern IntPtr CreateFile(string fileName, Int32 desiredAccess, Int32 shareMode, IntPtr securityAttributes, Int32 creationDisposition, Int32 flagsAndAttributes, IntPtr templateFile);

	[DllImport("kernel32.dll", SetLastError=true)]
	public static extern IntPtr CreateJobObject(object jobAttributes, string name);

	[DllImport("kernel32.dll", SetLastError=true)]
	public static extern IntPtr CreateMailslot(string name, Int32 maxMessageSize, Int32 readTimeout, IntPtr securityAttributes);

	[DllImport("kernel32.dll", SetLastError=true)]
	public static extern bool CreatePipe(out IntPtr readPipe, out IntPtr writePipe, [In] ref SECURITY_ATTRIBUTES pipeAttributes, Int32 size);

	[DllImport("kernel32.dll", SetLastError=true)]
	public static extern bool CreateProcess(string applicationName, string commandLine, IntPtr processAttributes, IntPtr threadAttributes, bool inheritHandles, Int32 creationFlags, IntPtr environment, string currentDirectory, [In] ref STARTUPINFO startupInfo, out PROCESS_INFORMATION processInformation);

	[DllImport("kernel32.dll", SetLastError=true)]
	public static extern bool DebugActiveProcess(UInt32 pid);

	[DllImport("kernel32.dll", SetLastError=true)]
	public static extern bool DebugActiveProcessStop(UInt32 pid);

	[DllImport("kernel32.dll", SetLastError=true)]
	public static extern bool FreeConsole();

	[DllImport("psapi.dll", SetLastError=true)]
	public static extern bool EnumProcessModules(IntPtr process, IntPtr[] module, UInt32 cb, out UInt32 cbNeeded);

	[DllImport("kernel32.dll", SetLastError=true)]
	public static extern bool GenerateConsoleCtrlEvent(Int32 ctrlEvent, Int32 processGroupId);

	[DllImport("kernel32.dll", SetLastError=true)]
	public static extern bool GetMailslotInfo(IntPtr mailslot, out Int32 maxMessageSize, out Int32 nextSize, out Int32 messageCount, out Int32 readTimeout);

	[DllImport("psapi.dll", SetLastError=true, CharSet=CharSet.Unicode)]
	public static extern UInt32 GetModuleBaseNameW(IntPtr process, IntPtr module, char[] baseName, UInt32 size);

	[DllImport("kernel32.dll", SetLastError=true)]
	public static extern bool GetModuleHandleEx(UInt32 flags, IntPtr moduleName, out IntPtr module);

	[DllImport("psapi.dll", SetLastError=true)]
	public static extern bool GetModuleInformation(IntPtr process, IntPtr module, ref MODULEINFO modinfo, UInt32 cb);

	[DllImport("kernel32.dll", SetLastError=true)]
	public static extern bool GetThreadContext(IntPtr thread, ref CONTEXT context);

	[DllImport("dbghelp.dll", SetLastError=true)]
	public static extern bool MiniDumpWriteDump(IntPtr process, UInt32 processId, SafeHandle file, Int32 dumpType, IntPtr exceptionParam, IntPtr userStreamParam, IntPtr callbackParam);

	[DllImport("kernel32.dll", SetLastError=true, CharSet=CharSet.Unicode)]
	public static extern Int32 MultiByteToWideChar(UInt32 codePage, Int32 flags, byte[] multiByteStr, Int32 cbMultiByte, char[] wideCharStr, Int32 cchWideChar);

	[DllImport("kernel32.dll", SetLastError=true)]
	public static extern IntPtr OpenProcess(UInt32 desiredAccess, bool inheritHandle, UInt32 processId);

	[DllImport("kernel32.dll", SetLastError=true)]
	public static extern IntPtr OpenThread(UInt32 desiredAccess, bool inheritHandle, UInt32 threadId);

	[DllImport("kernel32.dll", SetLastError=true)]
	public static extern bool PeekNamedPipe(IntPtr namedPipe, IntPtr buffer, Int32 bufferSize, IntPtr bytesRead, out Int32 totalBytesAvail, IntPtr bytesLeftThisMessage);

	[DllImport("kernel32.dll", SetLastError=true, CharSet=CharSet.Unicode)]
	public static extern bool QueryFullProcessImageName(IntPtr process, UInt32 flags, char[] exeName, ref UInt32 size);

	[DllImport("kernel32.dll", SetLastError=true)]
	public static extern bool ReadFile(IntPtr file, byte[] buffer, Int32 numberOfBytesToRead, out Int32 numberOfBytesRead, IntPtr overlapped);

	[DllImport("kernel32.dll", SetLastError=true)]
	public static extern bool ReadProcessMemory(IntPtr process, IntPtr baseAddress, byte[] buffer, int size, out IntPtr numberOfBytesRead);

	[DllImport("kernel32.dll", SetLastError=true)]
	public static extern UInt32 ResumeThread(IntPtr thread);

	[DllImport("kernel32.dll", SetLastError=true)]
	public static extern bool SetConsoleCtrlHandler(IntPtr handleRoutine, bool add);

	[DllImport("kernel32.dll", SetLastError=true)]
	public static extern bool SetHandleInformation(IntPtr handle, Int32 dwMask, Int32 dwFlags);

	[DllImport("kernel32.dll", SetLastError=true)]
	public static extern bool SetInformationJobObject(IntPtr job, JOBOBJECTINFOCLASS jobObjectInfoClass, [In] ref JOBOBJECT_EXTENDED_LIMIT_INFORMATION jobObjectInfo, Int32 jobObjectInfoLength);

	[DllImport("kernel32.dll", SetLastError=true)]
	public unsafe static extern bool SetThreadContext(IntPtr thread, CONTEXT* context);

	[DllImport("dbghelp.dll", SetLastError=false)]
	public static extern bool StackWalk64(UInt32 machineType, IntPtr process, IntPtr thread, ref STACKFRAME64 stackFrame, ref CONTEXT contextRecord, IntPtr readMemoryRoutine, IntPtr functionTableAccessRoutine, IntPtr getModuleBaseRoutine, IntPtr translateAddress);

	[DllImport("kernel32.dll", SetLastError=true)]
	public static extern UInt32 SuspendThread(IntPtr thread);

	[DllImport("dbghelp.dll", SetLastError=true)]
	public static extern bool SymCleanup(IntPtr process);

	[DllImport("dbghelp.dll", SetLastError=true)]
	public unsafe static extern bool SymFromAddrW(IntPtr process, UInt64 address, out UInt64 displacement, SYMBOL_INFO* symbol);

	[DllImport("dbghelp.dll", SetLastError=true)]
	public static extern bool SymGetLineFromAddr64(IntPtr process, UInt64 addr, out UInt32 displacement, out IMAGEHLP_LINE64 line);

	[DllImport("dbghelp.dll", SetLastError=true)]
	public static extern bool SymGetModuleInfoW64(IntPtr process, UInt64 addr, ref IMAGEHLP_MODULEW64 moduleInfo);

	[DllImport("dbghelp.dll", SetLastError=false)]
	public static extern UInt32 SymGetOptions();

	[DllImport("dbghelp.dll", SetLastError=true)]
	public static extern bool SymRefreshModuleList(IntPtr process);

	[DllImport("dbghelp.dll", SetLastError=true)]
	public static extern bool SymInitialize(IntPtr process, string userSearchPath, bool invadeProcess);

	[DllImport("dbghelp.dll", SetLastError=false)]
	public static extern UInt32 SymSetOptions(UInt32 symOptions);

	[DllImport("kernel32.dll", SetLastError=true)]
	public static extern bool TerminateProcess(IntPtr process, uint exitCode);

	[DllImport("kernel32.dll", SetLastError=true)]
	public static extern bool WaitForDebugEvent(out DEBUG_EVENT debugEvent, UInt32 milliseconds);

	[DllImport("kernel32.dll", SetLastError=true, CharSet=CharSet.Unicode)]
	public static extern Int32 WideCharToMultiByte(UInt32 codePage, Int32 flags, string wideCharStr, Int32 cchWideChar, byte[] multiByteStr, Int32 cbMultiByte, IntPtr lpDefaultChar, IntPtr lpUsedDefaultChar);

	[DllImport("kernel32.dll", SetLastError=true)]
	public static extern bool WriteFile(IntPtr file, byte[] buffer, Int32 numberOfBytesToWrite, out Int32 numberOfBytesWriten, IntPtr overlapped);

	[DllImport("kernel32.dll", SetLastError=true)]
	public static extern bool WriteProcessMemory(IntPtr process, UInt64 baseAddress, byte[] buffer, UInt64 size, out UInt64 numberOfBytesWriten);
}
