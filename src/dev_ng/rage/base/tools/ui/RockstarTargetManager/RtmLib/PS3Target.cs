using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;

namespace Rockstar.TargetManager
{
	////////////////////////////////////////////////////////////////////////////
	// PS3Target
	////////////////////////////////////////////////////////////////////////////
	public class PS3Target : Target
	{
		private const UInt32 SNPS3_TTY_ALL_STREAMS = 0xffffffffu;
		private const UInt32 SNPS3_DEF_PROCESS_PRI = 0x3e9;

		private static string TARGET_CRASH_CALLBACK_NAME         = "rage::sysStack::TargetCrashCallback(unsigned int)";
		private static string TARGET_GET_BUGSTAR_RTM_CONFIG_NAME = "CBugstarIntegration::GetRockstarTargetManagerConfig()";

		private int m_hTarget;
		private string m_Name;
		private string m_Alias;
		private bool m_StartNewLog;
		private bool m_FirstUpdateDone = false;
		private volatile bool m_IsConnected = false;
		private volatile bool m_IsRunning = false;
		private UInt32 m_Pid;
		private delegate void VoidCallbackVoid();
		private delegate void VoidCallbackString(string str);
		private delegate void VoidCallbackBoolUInt64(bool b, UInt64 val);
		private VoidCallbackVoid   m_WorkerThreadCallback;
		private VoidCallbackString m_PostCoredumpCallback;
		private VoidCallbackVoid   m_PpuExceptionHandlerCallback;
		private bool m_CrashedThisKick;


		////////////////////////////////////////////////////////////////////////
		// PS3Target
		////////////////////////////////////////////////////////////////////////
		internal PS3Target(string name, int hTarget)
		{
			m_Name = name;
			m_hTarget = hTarget;

			var targetInfo = new PS3TMAPI.TargetInfo();
			targetInfo.Flags = PS3TMAPI.TargetInfoFlag.TargetID;
			targetInfo.Target = hTarget;
			if(PS3TMAPI.SUCCEEDED(PS3TMAPI.GetTargetInfo(ref targetInfo)))
			{
				m_Alias = targetInfo.Name;
			}
		}

		////////////////////////////////////////////////////////////////////////
		// Dispose
		////////////////////////////////////////////////////////////////////////
		protected override void Dispose(bool disposing)
		{
			try
			{
				if(disposing)
				{
					if(m_FirstUpdateDone)
					{
						PS3TMAPI.CancelTargetEvents(m_hTarget);
						PS3TMAPI.CancelTTYEvents(m_hTarget, SNPS3_TTY_ALL_STREAMS);
					}
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		////////////////////////////////////////////////////////////////////////
		// UpdatePid
		////////////////////////////////////////////////////////////////////////
		private void UpdatePid()
		{
			m_Pid = 0;
			if(m_IsConnected)
			{
				UInt32[] processIDs;
				if(PS3TMAPI.SUCCEEDED(PS3TMAPI.GetProcessList(m_hTarget, out processIDs)))
				{
					if(processIDs.Length > 0)
					{
						m_Pid = processIDs[0];
					}
				}
			}
		}

		////////////////////////////////////////////////////////////////////////
		// SetIsConnected
		////////////////////////////////////////////////////////////////////////
		private void SetIsConnected(bool connected)
		{
			m_IsConnected = connected;
			if(connected)
			{
				// Force some of the target manager boot options to specific values
				PS3TMAPI.ExtraLoadFlag extraLoadFlags;
				if(PS3TMAPI.SUCCEEDED(PS3TMAPI.GetExtraLoadFlags(m_hTarget, out extraLoadFlags)))
				{
					extraLoadFlags |=  PS3TMAPI.ExtraLoadFlag.EnableCoreDump;
					extraLoadFlags |=  PS3TMAPI.ExtraLoadFlag.EnableGCMDebug;
					extraLoadFlags |=  PS3TMAPI.ExtraLoadFlag.EnableLv2ExceptionHandler;
					extraLoadFlags &= ~PS3TMAPI.ExtraLoadFlag.EnableMAT;
					extraLoadFlags |=  PS3TMAPI.ExtraLoadFlag.EnableMiscSettings;
					PS3TMAPI.SetExtraLoadFlags(m_hTarget, extraLoadFlags, PS3TMAPI.ExtraLoadFlagMask.All);
				}
				PS3TMAPI.CoreDumpFlag coredumpFlags;
				if(PS3TMAPI.SUCCEEDED(PS3TMAPI.GetCoreDumpFlags(m_hTarget, out coredumpFlags)))
				{
					coredumpFlags &= ~PS3TMAPI.CoreDumpFlag.DisableFootSwitchDetection;
					coredumpFlags &= ~PS3TMAPI.CoreDumpFlag.DisableMemoryDump;
					coredumpFlags &= ~PS3TMAPI.CoreDumpFlag.DisablePPUExceptionDetection;
					coredumpFlags |=  PS3TMAPI.CoreDumpFlag.DisableRSXExceptionDetection;
					coredumpFlags |=  PS3TMAPI.CoreDumpFlag.DisableSPUExceptionDetection;
					coredumpFlags |=  PS3TMAPI.CoreDumpFlag.EnableKeepRunningHandler;
					coredumpFlags &= ~PS3TMAPI.CoreDumpFlag.EnableRestartProcess;  // this seems to allow the game to run during the coredump if set
					// Not modifying the coredump destination flags, someone may
					// have set to another location (eg, usb) for a reason.
					PS3TMAPI.SetCoreDumpFlags(m_hTarget, coredumpFlags);
				}
			}

			// Check if there is already a process loaded
			UpdatePid();
		}

		////////////////////////////////////////////////////////////////////////
		// HostPcFilename
		////////////////////////////////////////////////////////////////////////
		private string HostPcFilename(string extension)
		{
			if(m_Pid == 0)
			{
				return null;
			}

			PS3TMAPI.ProcessInfo processInfo;
			if(PS3TMAPI.FAILED(PS3TMAPI.GetProcessInfo(m_hTarget, m_Pid, out processInfo)))
			{
				return null;
			}

			string elf_pc;
			string elf_ps3 = processInfo.Hdr.ELFPath;
			// app_home absolute path?
			if(Regex.IsMatch(elf_ps3, @"^/app_home/[a-zA-Z]:"))
			{
				elf_pc = elf_ps3.Substring(10);
			}
			// app_home relative path?
			else if(Regex.IsMatch(elf_ps3, @"^/app_home/"))
			{
				var targetInfo = new PS3TMAPI.TargetInfo();
				targetInfo.Flags = PS3TMAPI.TargetInfoFlag.TargetID;
				targetInfo.Target = m_hTarget;
				if(PS3TMAPI.FAILED(PS3TMAPI.GetTargetInfo(ref targetInfo)))
				{
					return null;
				}
				string app_home = targetInfo.FSDir;
				elf_pc = Path.Combine(app_home, elf_ps3.Substring(10));
			}
			else
			{
				return null;
			}

			return Regex.Replace(elf_pc, @"^(.+)\.self$", "$1."+extension);
		}

		////////////////////////////////////////////////////////////////////////
		// CmpFilename
		////////////////////////////////////////////////////////////////////////
		private string CmpFilename()
		{
			return HostPcFilename("cmp");
		}

		////////////////////////////////////////////////////////////////////////
		// PpuSelfFilename
		////////////////////////////////////////////////////////////////////////
		private string PpuSelfFilename()
		{
			return HostPcFilename("self");
		}

		////////////////////////////////////////////////////////////////////////
		// ThreadGetRegisters
		////////////////////////////////////////////////////////////////////////
		[DllImport("ps3tmapix64.dll")]
		private static extern PS3TMAPI.SNRESULT SNPS3ThreadGetRegisters(int hTarget, PS3TMAPI.UnitType uUnit, UInt32 uProcessID, UInt64 uThreadID, Int32 uNumRegisters, UInt32[] puNum, byte[] pRegBuffer);
		private bool ThreadGetRegisters(PS3TMAPI.UnitType unit, UInt64 threadId, UInt32[] registerNums, out UInt64[] registerValues)
		{
			// PS3TMAPI.ThreadGetRegisters is badly broken, so need to use
			// pinvoke SNPS3ThreadGetRegisters instead.  As of SDK440 release,
			// PS3TMAPI.ThreadGetRegisters incorrectly allocates an output
			// buffer half the required number of UInt64s, and ends up trashing
			// memory.

			registerValues = new UInt64[registerNums.Length*2];
			byte[] buf = new byte[registerNums.Length*16];
			PS3TMAPI.SNRESULT res = SNPS3ThreadGetRegisters(m_hTarget, unit, m_Pid, threadId, registerNums.Length, registerNums, buf);
			if(PS3TMAPI.FAILED(res))
			{
				return false;
			}
			for(uint i=0; i<registerNums.Length; ++i)
			{
				registerValues[i*2+0] = ((UInt64)buf[i*16+0] <<56) | ((UInt64)buf[i*16+1] <<48) | ((UInt64)buf[i*16+2] <<40) | ((UInt64)buf[i*16+3] <<32)
				                      | ((UInt64)buf[i*16+4] <<24) | ((UInt64)buf[i*16+5] <<16) | ((UInt64)buf[i*16+6] <<8)  | ((UInt64)buf[i*16+7] );
				registerValues[i*2+1] = ((UInt64)buf[i*16+8] <<56) | ((UInt64)buf[i*16+9] <<48) | ((UInt64)buf[i*16+10]<<40) | ((UInt64)buf[i*16+11]<<32)
				                      | ((UInt64)buf[i*16+12]<<24) | ((UInt64)buf[i*16+13]<<16) | ((UInt64)buf[i*16+14]<<8)  | ((UInt64)buf[i*16+15]);
			}
			return true;
		}

		////////////////////////////////////////////////////////////////////////
		// PpuGetRegisters
		////////////////////////////////////////////////////////////////////////
		private bool PpuGetRegisters(UInt64 threadId, UInt32[] registerNums, out UInt64[] registerValues)
		{
			return ThreadGetRegisters(PS3TMAPI.UnitType.PPU, threadId, registerNums, out registerValues);
		}

		////////////////////////////////////////////////////////////////////////
		// SpuGetRegisters
		////////////////////////////////////////////////////////////////////////
		private bool SpuGetRegisters(UInt32 threadId, UInt32[] registerNums, out UInt64[] registerValues)
		{
			return ThreadGetRegisters(PS3TMAPI.UnitType.SPU, threadId, registerNums, out registerValues);
		}

		////////////////////////////////////////////////////////////////////////
		// ThreadSetRegisters
		////////////////////////////////////////////////////////////////////////
		[DllImport("ps3tmapix64.dll")]
		private static extern PS3TMAPI.SNRESULT SNPS3ThreadSetRegisters(int hTarget, PS3TMAPI.UnitType uUnit, UInt32 uProcessID, UInt64 uThreadID, Int32 uNumRegisters, UInt32[] puNum, byte[] pRegBuffer);
		private bool ThreadSetRegisters(PS3TMAPI.UnitType unit, UInt64 threadId, UInt32[] registerNums, UInt64[] registerValues)
		{
			// PS3TMAPI.ThreadSetRegisters seems broken in a similar way to
			// PS3TMAPI.ThreadGetRegisters, so pinvoke SNPS3ThreadSetRegisters
			// instead.

			Debug.Assert(registerValues.Length == registerNums.Length*2);
			byte[] buf = new byte[registerNums.Length*16];
			for(uint i=0; i<registerNums.Length; ++i)
			{
				buf[i*16+0]  = (byte)((registerValues[i*2+0]>>56)&0xff);
				buf[i*16+1]  = (byte)((registerValues[i*2+0]>>48)&0xff);
				buf[i*16+2]  = (byte)((registerValues[i*2+0]>>40)&0xff);
				buf[i*16+3]  = (byte)((registerValues[i*2+0]>>32)&0xff);
				buf[i*16+4]  = (byte)((registerValues[i*2+0]>>24)&0xff);
				buf[i*16+5]  = (byte)((registerValues[i*2+0]>>16)&0xff);
				buf[i*16+6]  = (byte)((registerValues[i*2+0]>>8) &0xff);
				buf[i*16+7]  = (byte)((registerValues[i*2+0]>>0) &0xff);
				buf[i*16+8]  = (byte)((registerValues[i*2+1]>>56)&0xff);
				buf[i*16+9]  = (byte)((registerValues[i*2+1]>>48)&0xff);
				buf[i*16+10] = (byte)((registerValues[i*2+1]>>40)&0xff);
				buf[i*16+11] = (byte)((registerValues[i*2+1]>>32)&0xff);
				buf[i*16+12] = (byte)((registerValues[i*2+1]>>24)&0xff);
				buf[i*16+13] = (byte)((registerValues[i*2+1]>>16)&0xff);
				buf[i*16+14] = (byte)((registerValues[i*2+1]>>8) &0xff);
				buf[i*16+15] = (byte)((registerValues[i*2+1]>>0) &0xff);
			}

			PS3TMAPI.SNRESULT res = SNPS3ThreadSetRegisters(m_hTarget, unit, m_Pid, threadId, registerNums.Length, registerNums, buf);
			return PS3TMAPI.SUCCEEDED(res);
		}

		////////////////////////////////////////////////////////////////////////
		// PpuSetRegisters
		////////////////////////////////////////////////////////////////////////
		private bool PpuSetRegisters(UInt64 threadId, UInt32[] registerNums, UInt64[] registerValues)
		{
			return ThreadSetRegisters(PS3TMAPI.UnitType.PPU, threadId, registerNums, registerValues);
		}

		////////////////////////////////////////////////////////////////////////
		// SpuSetRegisters
		////////////////////////////////////////////////////////////////////////
		private bool SpuSetRegisters(UInt32 threadId, UInt32[] registerNums, UInt64[] registerValues)
		{
			return ThreadSetRegisters(PS3TMAPI.UnitType.SPU, threadId, registerNums, registerValues);
		}

		////////////////////////////////////////////////////////////////////////
		// PpuGetMemory
		////////////////////////////////////////////////////////////////////////
		private bool PpuGetMemory(UInt64 addr, ref byte[] buf)
		{
			return PS3TMAPI.SUCCEEDED(PS3TMAPI.ProcessGetMemory(m_hTarget, PS3TMAPI.UnitType.PPU, m_Pid, 0, addr, ref buf));
		}

		////////////////////////////////////////////////////////////////////////
		// PpuGetMemory
		////////////////////////////////////////////////////////////////////////
		private bool PpuGetMemory(UInt64 addr, out UInt32 data)
		{
			byte[] buf = new byte[4];
			if(!PpuGetMemory(addr, ref buf))
			{
				data = 0;
				return false;
			}
			data = ((UInt32)buf[0]<<24) | ((UInt32)buf[1]<<16) | ((UInt32)buf[2]<<8) | ((UInt32)buf[3]);
			return true;
		}

		////////////////////////////////////////////////////////////////////////
		// PpuGetMemory
		////////////////////////////////////////////////////////////////////////
		private bool PpuGetMemory(UInt64 addr, out UInt64 data)
		{
			byte[] buf = new byte[8];
			if(!PpuGetMemory(addr, ref buf))
			{
				data = 0;
				return false;
			}
			data = ((UInt64)buf[0]<<56) | ((UInt64)buf[1]<<48) | ((UInt64)buf[2]<<40) | ((UInt64)buf[3]<<32)
			     | ((UInt64)buf[4]<<24) | ((UInt64)buf[5]<<16) | ((UInt64)buf[6]<<8)  | ((UInt64)buf[7]);
			return true;
		}

		////////////////////////////////////////////////////////////////////////
		// PpuGetMemory
		////////////////////////////////////////////////////////////////////////
		private bool PpuGetMemory(UInt64 addr, out string str)
		{
			return GetString(addr, Encoding.UTF8, 1, out str, PpuGetMemory);
		}

		////////////////////////////////////////////////////////////////////////
		// SpuGetMemory
		////////////////////////////////////////////////////////////////////////
		private bool SpuGetMemory(UInt32 threadId, UInt32 addr, out UInt32 data)
		{
			byte[] buf = new byte[4];
			if(PS3TMAPI.FAILED(PS3TMAPI.ProcessGetMemory(m_hTarget, PS3TMAPI.UnitType.SPU, m_Pid, threadId, addr, ref buf)))
			{
				data = 0;
				return false;
			}
			data = ((UInt32)buf[0]<<24) | ((UInt32)buf[1]<<16) | ((UInt32)buf[2]<<8) | ((UInt32)buf[3]);
			return true;
		}

		////////////////////////////////////////////////////////////////////////
		// SpuGetMemory
		////////////////////////////////////////////////////////////////////////
		private bool SpuGetMemory(UInt32 threadId, UInt32 addr, ref UInt32[] data)
		{
			byte[] buf = new byte[data.Length*4];
			if(PS3TMAPI.FAILED(PS3TMAPI.ProcessGetMemory(m_hTarget, PS3TMAPI.UnitType.SPU, m_Pid, threadId, addr, ref buf)))
			{
				return false;
			}
			for(uint i=0; i<data.Length; ++i)
			{
				data[i] = ((UInt32)buf[i*4+0]<<24) | ((UInt32)buf[i*4+1]<<16) | ((UInt32)buf[i*4+2]<<8) | ((UInt32)buf[i*4+3]);
			}
			return true;
		}

		////////////////////////////////////////////////////////////////////////
		// TargetCallbackStateSaver
		////////////////////////////////////////////////////////////////////////
		private class TargetCallbackStateSaver : IDisposable
		{
			// The register numbers are missing from the .NET wrapper, so grabbing them from ps3tmapi.h,
			//  [SNPS3_gpr_0 .. SNPS3_gpr_31] = [0x00 .. 0x1f]
			//  [SNPS3_fpr_0 .. SNPS3_fpr_31] = [0x20 .. 0x3f]
			//  SNPS3_pc                      = 0x40
			//  SNPS3_cr                      = 0x41
			//  SNPS3_lr                      = 0x42
			//  SNPS3_ctr                     = 0x43
			//  SNPS3_xer                     = 0x44
			//  SNPS3_fpscr                   = 0x45
			//  SNPS3_vscr                    = 0x46
			//  SNPS3_vrsave                  = 0x47
			//  SNPS3_msr                     = 0x48
			//  [SNPS3_vmx_0 .. SNPS3_vmx_31] = [0x60 .. 0x7f]
			private static UInt32[] m_RegNums =
			{
				// [r0 .. r31]
				0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f,
				0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x1a, 0x1b, 0x1c, 0x1d, 0x1e, 0x1f,

				// [f0 .. f31]
				0x20, 0x21, 0x22, 0x23, 0x24, 0x25, 0x26, 0x27, 0x28, 0x29, 0x2a, 0x2b, 0x2c, 0x2d, 0x2e, 0x2f,
				0x30, 0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39, 0x3a, 0x3b, 0x3c, 0x3d, 0x3e, 0x3f,

				// pc, cr, lr, ctr, xer, fpscr, vscr, vrsave, msr
				0x40, 0x41, 0x42, 0x43, 0x44, 0x45, 0x46, 0x47, 0x48,

				// [v0 .. v31]
				0x60, 0x61, 0x62, 0x63, 0x64, 0x65, 0x66, 0x67, 0x68, 0x69, 0x6a, 0x6b, 0x6c, 0x6d, 0x6e, 0x6f,
				0x70, 0x71, 0x72, 0x73, 0x74, 0x75, 0x76, 0x77, 0x78, 0x79, 0x7a, 0x7b, 0x7c, 0x7d, 0x7e, 0x7f,
			};

			private PS3Target m_Target;
			UInt64 m_ThreadId;
			private UInt64[] m_SavedRegs;


			////////////////////////////////////////////////////////////////////
			// TargetCallbackStateSaver
			////////////////////////////////////////////////////////////////////
			public TargetCallbackStateSaver(PS3Target target, UInt64 threadId)
			{
				m_Target = target;
				m_ThreadId = threadId;
				target.PpuGetRegisters(threadId, m_RegNums, out m_SavedRegs);
			}

			////////////////////////////////////////////////////////////////////
			// ~TargetCallbackStateSaver
			////////////////////////////////////////////////////////////////////
			~TargetCallbackStateSaver()
			{
				Dispose(false);
			}

			////////////////////////////////////////////////////////////////////
			// Dispose
			////////////////////////////////////////////////////////////////////
			public void Dispose()
			{
				Dispose(true);
				GC.SuppressFinalize(this);
			}

			////////////////////////////////////////////////////////////////////
			// Dispose
			////////////////////////////////////////////////////////////////////
			protected virtual void Dispose(bool disposing)
			{
				if(disposing)
				{
					// Restore registers
					if(m_SavedRegs != null)
					{
						m_Target.PpuSetRegisters(m_ThreadId, m_RegNums, m_SavedRegs);
					}
				}
			}
		}

		////////////////////////////////////////////////////////////////////////
		// LookupTargetCallback
		////////////////////////////////////////////////////////////////////////
		private UInt64 LookupTargetCallback(SymbolLookup sym, string callback)
		{
			TtyOutput("Searching symbols for "+callback+" ...\n");
			UInt64 targetCallbackAddr = sym.Lookup(callback);
			if(targetCallbackAddr == 0)
			{
				TtyOutput("... not found\n");
			}
			else
			{
				TtyOutput("... found at 0x"+targetCallbackAddr.ToString("x8")+"\n");
			}
			return targetCallbackAddr;
		}

		////////////////////////////////////////////////////////////////////////
		// ExecuteTargetCallback
		////////////////////////////////////////////////////////////////////////
		private void ExecuteTargetCallback(UInt64 executeThreadId, UInt64 targetCallbackAddr, UInt64 r3, VoidCallbackBoolUInt64 thenDo)
		{
			if(targetCallbackAddr != 0)
			{
				TargetCallbackStateSaver stateSaver = new TargetCallbackStateSaver(this, executeThreadId);
				UInt32[] setRegNums = {0x40/*pc*/, 0x42/*lr*/, 0x03/*r3*/};
				UInt64[] setRegVals = {targetCallbackAddr,0, 0,0, r3,0};
				if(!PpuSetRegisters(executeThreadId, setRegNums, setRegVals) || !PS3TMAPI.SUCCEEDED(PS3TMAPI.ProcessContinue(m_hTarget, m_Pid)))
				{
					stateSaver.Dispose();
				}
				else
				{
					m_PpuExceptionHandlerCallback = () =>
					{
						UInt32[] getRegNums = {0x03/*r3*/};
						UInt64[] getRegVals;
						bool callbackCalledSuccesfully = PpuGetRegisters(executeThreadId, getRegNums, out getRegVals);
						stateSaver.Dispose();
						thenDo(callbackCalledSuccesfully, getRegVals[0]);
					};
					return;
				}
			}
			thenDo(false, 0);
		}

		////////////////////////////////////////////////////////////////////////
		// ExecuteTargetCallback
		////////////////////////////////////////////////////////////////////////
		private void ExecuteTargetCallback(UInt64 executeThreadId, UInt64 targetCallbackAddr, UInt64 r3, VoidCallbackVoid thenDo)
		{
			ExecuteTargetCallback(executeThreadId, targetCallbackAddr, r3, (bool b, UInt64 val) => thenDo());
		}

		////////////////////////////////////////////////////////////////////////
		// PpuPrintThreadStack
		////////////////////////////////////////////////////////////////////////
		private void PpuPrintThreadStack(SymbolLookup sym, UInt64 pc, UInt64 lr, UInt64 sp)
		{
			// Special case to start stack walk.  If we are in a leaf function,
			// then the first stackframe (caller's frame) will not have lr saved
			// into it.  Detect a leaf function by pc and lr not being inside
			// the same function.  This is as close to correct as possible(?),
			// but may miss one level of the stack if a recursive function
			// overflows the stack.
			UInt64 pcOffs;
			string pcStr = sym.Lookup(pc, out pcOffs);
			TtyOutput("0x"+pc.ToString("x8")+" - "+pcStr+"\n");

			if(lr != 0)
			{
				UInt64 spNext=0;
				UInt64 pcNext=0;
				bool invalid = !PpuGetMemory(sp, out spNext) || !PpuGetMemory(spNext+16, out pcNext);

				UInt64 lrOffs;
				string lrStr = sym.Lookup(lr, out lrOffs);
				UInt64 pcFunc = pc-pcOffs;
				UInt64 lrFunc = lr-lrOffs;

				// Not a leaf function?
				// Note that when sleeping inside a syscall, lr is returned as
				// 0xd15ea5edd15ea5ed.  Skipping this just saves displaying some
				// garbage in the stack.
				if(pcFunc == lrFunc || lr == 0xd15ea5edd15ea5ed)
				{
					// Read both sp and lr from the stackframe (ie, leave pcNext and spNext as is).
				}

				// Possibly a leaf function (or no child function yet been called)?
				else
				{
					TtyOutput("0x"+lr.ToString("x8")+" - "+lrStr+"\n");

					// We don't want to unwind an additional stackframe here
					// if we are a leaf function, but we do wan't to in the
					// case of a non-leaf function that just hasn't called
					// anything yet (so lr not modified).  Detect the
					// non-leaf case by checking if the lr stored in the
					// caller's frame is the same as the register.
					//
					// This is not bulletproof.  In the case of a leaf
					// function, the lr slot in the caller's stackframe will
					// be uninitialized, so there is a small chance of an
					// erroneous match.
					//
					spNext = (pcNext == lr) ? spNext : sp;

					// Don't read lr from stack frame, use the register instead.
					pcNext = lr;
				}

				// PPU thread callstacks seem to terminate with
				// 0xbadadd0000000000|(tid<<4).  Terminating at that (rather
				// than waiting for an invalid dereference) saves printing a
				// final line of garbage for each thread's stack.
				while((pcNext>>32)!=0xbadadd00 && !invalid)
				{
					pcStr = sym.Lookup(pcNext);
					TtyOutput("0x"+pcNext.ToString("x8")+" - "+pcStr+"\n");
					invalid = !PpuGetMemory(spNext, out spNext) || !PpuGetMemory(spNext+16, out pcNext);
				}
			}
			TtyOutput("\n");
		}

		////////////////////////////////////////////////////////////////////////
		// SpuPrintThreadStack
		////////////////////////////////////////////////////////////////////////
		private void SpuPrintThreadStack(SymbolLookup sym, UInt32 threadId, UInt32 pc, UInt32 lr, UInt32 sp)
		{
			// This code is very similar to the PPU stack walking code (in
			// PpuPrintThreadStack).  Rather than duplicate all the comments
			// here, see PpuPrintThreadStack for explanation.
			SymbolLookup.SpuLsGuidMapEntry[] lsGuidMap = ScanSpuGuids(sym, threadId);
			DisplayLsGuidMap(lsGuidMap, sym);
			SymbolLookup.SpuLsGuidMapEntry lgme = SymbolLookup.FindSpuLsGuidMapEntry(lsGuidMap, pc);
			UInt32 pcOffs;
			string pcStr = sym.Lookup(pc, lgme, out pcOffs);
			TtyOutput("0x"+pc.ToString("x6")+" (0x"+(pc-lgme.ls).ToString("x6")+") - "+pcStr+"\n");
			if(lr != 0)
			{
				UInt32 spNext=0;
				UInt32 pcNext=0;
				bool invalid = !SpuGetMemory(threadId, sp, out spNext) || !SpuGetMemory(threadId, spNext+16, out pcNext);

				lgme = SymbolLookup.FindSpuLsGuidMapEntry(lsGuidMap, lr);
				UInt32 lrOffs;
				string lrStr = sym.Lookup(lr, lgme, out lrOffs);
				UInt32 pcFunc = pc-pcOffs;
				UInt32 lrFunc = lr-lrOffs;

				// Possibly a leaf function (or no child function yet been called)?
				if(pcFunc != lrFunc)
				{
					TtyOutput("0x"+lr.ToString("x6")+" (0x"+(lr-lgme.ls).ToString("x6")+") - "+lrStr+"\n");
					spNext = (pcNext == lr) ? spNext : sp;
					pcNext = lr;
				}

				// As per ABI, stacks are terminated with 0.
				while(spNext!=0 && !invalid)
				{
					lgme = SymbolLookup.FindSpuLsGuidMapEntry(lsGuidMap, pcNext);
					pcStr = sym.Lookup(pcNext, lgme);
					TtyOutput("0x"+pcNext.ToString("x6")+" (0x"+(pcNext-lgme.ls).ToString("x6")+") - "+pcStr+"\n");
					invalid = !SpuGetMemory(threadId, spNext, out spNext) || !SpuGetMemory(threadId, spNext+16, out pcNext);
				}
			}
			TtyOutput("\n");
		}

		////////////////////////////////////////////////////////////////////////
		// TmCmdPrintAllThreadStacks
		////////////////////////////////////////////////////////////////////////
		private void TmCmdPrintAllThreadStacks()
		{
			using(SymbolLookup sym = new SymbolLookup(CmpFilename(), TargetPlatformId.PS3))
			{
				UInt64[] ppuThreadIds;
				UInt64[] spuThreadGroupIds;
				if(PS3TMAPI.SUCCEEDED(PS3TMAPI.GetThreadList(m_hTarget, m_Pid, out ppuThreadIds, out spuThreadGroupIds)) && ppuThreadIds.Length>0)
				{
					Array.Sort(ppuThreadIds);
					foreach(UInt64 ppuTid in ppuThreadIds)
					{
						PS3TMAPI.PPUThreadInfoEx threadInfo;
						if(PS3TMAPI.SUCCEEDED(PS3TMAPI.GetPPUThreadInfoEx(m_hTarget, m_Pid, ppuTid, out threadInfo)))
						{
							TtyOutput("### PPU thread \""+threadInfo.ThreadName+"\" (0x"+ppuTid.ToString("x16")+")\n");
						}
						else
						{
							TtyOutput("### PPU thread 0x"+ppuTid.ToString("x16")+"\n");
						}
						UInt32[] regNums = {0x40/*pc*/, 0x42/*lr*/, 0x01/*sp*/};
						UInt64[] regVals;
						if(PpuGetRegisters(ppuTid, regNums, out regVals))
						{
							Debug.Assert(regVals.Length == 3*2);
							PpuPrintThreadStack(sym, regVals[0*2], regVals[1*2], regVals[2*2]);
						}
					}

					var stoppedSpuThreadGroupIds = new List<UInt64>();
					foreach(UInt64 spuTgid in spuThreadGroupIds)
					{
						PS3TMAPI.SPUThreadGroupInfo threadGroupInfo;
						if(PS3TMAPI.SUCCEEDED(PS3TMAPI.GetSPUThreadGroupInfo(m_hTarget, m_Pid, spuTgid, out threadGroupInfo)))
						{
							TtyOutput("### SPU thread group \""+threadGroupInfo.GroupName+"\" (0x"+spuTgid.ToString("x16")+")\n\n");

							if(threadGroupInfo.State != PS3TMAPI.SPUThreadGroupState.Stopped)
							{
								if(PS3TMAPI.SUCCEEDED(PS3TMAPI.SPUThreadGroupStop(m_hTarget, m_Pid, spuTgid)))
								{
									stoppedSpuThreadGroupIds.Add(spuTgid);
								}
							}
							foreach(UInt32 spuTid in threadGroupInfo.ThreadIDs)
							{
								PS3TMAPI.SPUThreadInfo threadInfo;
								if(PS3TMAPI.SUCCEEDED(PS3TMAPI.GetSPUThreadInfo(m_hTarget, m_Pid, spuTid, out threadInfo)))
								{
									TtyOutput("### SPU thread \""+threadInfo.ThreadName+"\" (0x"+spuTid.ToString("x8")+")\n");
								}
								else
								{
									TtyOutput("### SPU thread 0x"+spuTid.ToString("x8")+"\n");
								}
								UInt32[] regNums = {0x80/*pc*/, 0x00/*lr*/, 0x01/*sp*/};
								UInt64[] regVals;
								if(SpuGetRegisters(spuTid, regNums, out regVals))
								{
									Debug.Assert(regVals.Length == 3*2);
									SpuPrintThreadStack(sym, spuTid, (UInt32)(regVals[0*2]>>32), (UInt32)(regVals[1*2]>>32), (UInt32)(regVals[2*2]>>32));
								}
							}
						}
					}

					foreach(UInt64 spuTgid in stoppedSpuThreadGroupIds)
					{
						PS3TMAPI.SPUThreadGroupContinue(m_hTarget, m_Pid, spuTgid);
					}
				}
			}
		}

		////////////////////////////////////////////////////////////////////////
		// HandleTmCmd
		////////////////////////////////////////////////////////////////////////
		private bool HandleTmCmd(UInt64 tid, UInt64 pc, out string bugSummary)
		{
			bool ret = false;
			bugSummary = null;

			// r4 contains the command code
			UInt32[] r4num = {0x04};
			UInt64[] r4val;
			if(PpuGetRegisters(tid, r4num, out r4val))
			{
				ret = true;
				bool processContinue = true;
				switch((TMCommands)(r4val[0]))
				{
					case TMCommands.NOP:
					{
						break;
					}

					case TMCommands.PRINT_ALL_THREAD_STACKS:
					{
						TmCmdPrintAllThreadStacks();
						break;
					}

					case TMCommands.STOP_NO_ERROR_REPORT:
					{
						TtyOutput("\nsysTmCmdStopNoErrorReport recieved\n\n");
						using(SymbolLookup sym = new SymbolLookup(CmpFilename(), TargetPlatformId.PS3))
						{
							UInt32[] regNums = {0x42/*lr*/, 0x01/*sp*/};
							UInt64[] regVals;
							if(PpuGetRegisters(tid, regNums, out regVals))
							{
								Debug.Assert(regVals.Length == 3*2);
								PpuPrintThreadStack(sym, pc, regVals[0*2], regVals[1*2]);
							}
						}
						processContinue = false;
						break;
					}

					case TMCommands.QUITF:
					{
						ret = false;
						UInt32[] regNums = {0x05/*r5*/};
						UInt64[] regVals;
						if(PpuGetRegisters(tid, regNums, out regVals))
						{
							if(PpuGetMemory(regVals[0*2], out bugSummary))
							{
								bugSummary = "Quitf \""+bugSummary+"\"";
							}
						}
						break;
					}

					case TMCommands.GPU_HANG:
					case TMCommands.CPU_HANG:
					case TMCommands.CONFIG:
					{
						break;
					}

					default:
					{
						ret = false;
						break;
					}
				}

				if(ret)
				{
					if(!processContinue)
					{
						m_IsRunning = false;
						return true;
					}
					else
					{
						UInt32[] pcNum = {0x40/*pc*/};
						UInt64[] pcVal = {pc+4, 0};
						if(PpuSetRegisters(tid, pcNum, pcVal))
						{
							return PS3TMAPI.SUCCEEDED(PS3TMAPI.ProcessContinue(m_hTarget, m_Pid));
						}
					}
				}
			}
			return ret;
		}

		////////////////////////////////////////////////////////////////////////
		// RaiseBug
		////////////////////////////////////////////////////////////////////////
		private void RaiseBug(SymbolLookup sym, UInt64 threadId, string summary, string description, string[] filesToCopy, VoidCallbackVoid thenDo)
		{
			// Fetch config from console
			UInt64 targetCallbackAddr = LookupTargetCallback(sym, TARGET_GET_BUGSTAR_RTM_CONFIG_NAME);
			ExecuteTargetCallback(threadId, targetCallbackAddr, 0, (bool valid, UInt64 ret) =>
			{
				bool warnConfig = true;
				if(valid)
				{
					string configXml;
					if(PpuGetMemory(ret, out configXml))
					{
						var config = Bugstar.Config.FromString(configXml);
						const bool enableBugDupCheck = true;
						BaseRaiseBug(config, summary, description, filesToCopy, enableBugDupCheck);
						warnConfig = false;
					}
				}
				if(warnConfig)
				{
					TtyOutput("Unable to read Bugstar configuration from the game to automatically log a bug.  Please create bug manually\n");
				}

				thenDo();
			});
		}

		////////////////////////////////////////////////////////////////////////
		// PpuExceptionHandler
		////////////////////////////////////////////////////////////////////////
		private void PpuExceptionHandler(PS3TMAPI.TargetSpecificEventType type, UInt64 pc, UInt64 sp, UInt64 threadId, UInt32 hwThreadNumber, bool darValid, UInt64 dar, UInt64 dsisr)
		{
			VoidCallbackVoid callback = m_PpuExceptionHandlerCallback;
			if(callback != null)
			{
				m_PpuExceptionHandlerCallback = null;
				callback();
				return;
			}

			// Input values seem to be the wrong endian
			pc             = EndianSwap(pc);
			sp             = EndianSwap(sp);
			threadId       = EndianSwap(threadId);
			hwThreadNumber = EndianSwap(hwThreadNumber);
			dar            = EndianSwap(dar);
			dsisr          = EndianSwap(dsisr);

			// Check for sysTmCmd*
			string bugSummary = null;
			if(darValid && dar==0x522a544d)   // "R*TM"
			{
				if(HandleTmCmd(threadId, pc, out bugSummary))
				{
					return;
				}
			}

			// Can only really handle the first crash we get notified about this
			// kick (eg, multiple SPUs can crash simultaneously).  The resulting
			// coredump will contain all crashes, so this isn't a real issue.
			if(m_CrashedThisKick)
			{
				return;
			}
			m_CrashedThisKick = true;
			m_IsRunning = false;

			// Determine the address of the target crash callback function (if it exists).
			SymbolLookup sym = new SymbolLookup(CmpFilename(), TargetPlatformId.PS3);
			UInt64 targetCallbackAddr = LookupTargetCallback(sym, TARGET_CRASH_CALLBACK_NAME);
			if(targetCallbackAddr != 0)
			{
				TtyOutput("Executing "+TARGET_CRASH_CALLBACK_NAME+", pass 0\n");
			}
			ExecuteTargetCallback(threadId, targetCallbackAddr, 0, () =>
			{
				TtyOutput("\n**** BEGIN EXCEPTION DUMP\n\n");

				BeginCaptureTtyOutput();

				// Display basic thread information
				PS3TMAPI.PPUThreadInfoEx threadInfo;
				if(PS3TMAPI.SUCCEEDED(PS3TMAPI.GetPPUThreadInfoEx(m_hTarget, m_Pid, threadId, out threadInfo)))
				{
					TtyOutput("**** Exception "+type.ToString()+" caught in thread ["+threadInfo.ThreadName+"] at address 0x"+pc.ToString("x8")+"\n");
					TtyOutput("Thread id            0x"+threadId.ToString("x16")+"\n");
					TtyOutput("Thread name          "+threadInfo.ThreadName+"\n");
					TtyOutput("Thread state         "+threadInfo.State.ToString()+"\n");
					TtyOutput("Thread stack address 0x"+threadInfo.StackAddress.ToString("x8")+"\n");
					TtyOutput("Thread stack size    0x"+threadInfo.StackSize.ToString("x8")+"\n");
					TtyOutput("Thread base priority 0x"+threadInfo.BasePriority.ToString("x8")+"\n");
					TtyOutput("Thread priority      0x"+threadInfo.Priority.ToString("x8")+"\n");
				}
				else
				{
					TtyOutput("**** Exception "+type.ToString()+" caught in thread 0x"+threadId.ToString("x16")+" at address 0x"+pc.ToString("x8")+"\n");
					TtyOutput("Thread id            0x"+threadId.ToString("x16")+"\n");
				}
				TtyOutput("Hardware Thread      "+hwThreadNumber.ToString()+"\n\n");
				if(darValid)
				{
					TtyOutput("DAR                  0x"+dar.ToString("x16")+"\n");
					TtyOutput("DSISR                0x"+dsisr.ToString("x16")+"\n");
				}
				TtyOutput("PC                   0x"+pc.ToString("x16")+"\n");
				TtyOutput("SP                   0x"+sp.ToString("x16")+"\n");

				// Display basic register values (see TargetCallbackStateSaver
				// comments for where these magic numbers come from).
				UInt32[] registerNums = new UInt32[]
				{
					// [r0 .. r31]
					0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f,
					0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x1a, 0x1b, 0x1c, 0x1d, 0x1e, 0x1f,

					// lr
					0x42,
				};
				UInt64[] registerValues;
				UInt64 lr = 0;
				if(PpuGetRegisters(threadId, registerNums, out registerValues))
				{
					lr = registerValues[0x20*2];
					TtyOutput("LR                   0x"+lr.ToString("x16")+"\n\n");
					for(uint i=0; i<32; i+=2)
					{
						TtyOutput("GPR"+(i+0).ToString("d2")+" 0x"+registerValues[i*2+0].ToString("x16")+" -- "
						         +"GPR"+(i+1).ToString("d2")+" 0x"+registerValues[i*2+2].ToString("x16")+"\n");
					}
				}

				// Walk stack.
				TtyOutput("\n");
				PpuPrintThreadStack(sym, pc, lr, sp);

				// Trigger a coredump
				const UInt64 userData1 = 0;
				const UInt64 userData2 = 0;
				const UInt64 userData3 = 0;
				if(PS3TMAPI.SUCCEEDED(PS3TMAPI.TriggerCoreDump(m_hTarget, m_Pid, userData1, userData2, userData3)))
				{
					m_PostCoredumpCallback = (string coredump) =>
					{
						TtyOutput("Executing "+TARGET_CRASH_CALLBACK_NAME+", pass 1\n");
						ExecuteTargetCallback(threadId, targetCallbackAddr, 1, () =>
						{
							m_CrashedThisKick = false;
							string bugDescription = EndCaptureTtyOutput();
							RaiseBug(sym, threadId, bugSummary, bugDescription,
								new string[]{coredump, PpuSelfFilename()},
								() => TtyOutput("\n**** END EXCEPTION DUMP\n"));
							sym.Dispose();
						});
					};
				}
				else
				{
					EndCaptureTtyOutput();
					sym.Dispose();
				}
			});
		}

		////////////////////////////////////////////////////////////////////////
		// PpuExceptionHandler
		////////////////////////////////////////////////////////////////////////
		private void PpuExceptionHandler(PS3TMAPI.TargetSpecificEventType type, UInt64 pc, UInt64 sp, UInt64 threadId, UInt32 hwThreadNumber, UInt64 dar, UInt64 dsisr)
		{
			PpuExceptionHandler(type, pc, sp, threadId, hwThreadNumber, true, dar, dsisr);
		}

		////////////////////////////////////////////////////////////////////////
		// PpuExceptionHandler
		////////////////////////////////////////////////////////////////////////
		private void PpuExceptionHandler(PS3TMAPI.TargetSpecificEventType type, UInt64 pc, UInt64 sp, UInt64 threadId, UInt32 hwThreadNumber)
		{
			PpuExceptionHandler(type, pc, sp, threadId, hwThreadNumber, false, 0, 0);
		}

		////////////////////////////////////////////////////////////////////////
		// IsSpuGuid
		////////////////////////////////////////////////////////////////////////
		private static bool IsSpuGuid(UInt32[] insns)
		{
			// Is this four ILA instructions with register r2 as the destination, and
			// the least significant two bits of each constant, 0, 1, 2 and 3.
		 	// See https://ps3.scedev.net/forums/thread/35449/.
			//   bits (big endian numbering)
			//    0.. 6  ILA opcode (0x21)
			//    7..24  18-bit immediate value
			//   25..31  destination register
			return ((insns[0] & 0xfe0001ff) == 0x42000002 &&
			        (insns[1] & 0xfe0001ff) == 0x42000082 &&
			        (insns[2] & 0xfe0001ff) == 0x42000102 &&
			        (insns[3] & 0xfe0001ff) == 0x42000182);
		}

		////////////////////////////////////////////////////////////////////////
		// ExtractSpuGuid
		////////////////////////////////////////////////////////////////////////
		private static UInt64 ExtractSpuGuid(UInt32[] insns)
		{
			return ((((UInt64)insns[0])<<39)&0xffff000000000000)
			     | ((((UInt64)insns[1])<<23)&0x0000ffff00000000)
			     | ((((UInt64)insns[2])<< 7)&0x00000000ffff0000)
			     | ((((UInt64)insns[3])>> 9)&0x000000000000ffff);
		}

		////////////////////////////////////////////////////////////////////////
		// ScanSpuGuids
		////////////////////////////////////////////////////////////////////////
		private SymbolLookup.SpuLsGuidMapEntry[] ScanSpuGuids(SymbolLookup sym, UInt32 threadId)
		{
			var lsMap = new List<SymbolLookup.SpuLsGuidMapEntry>();
			var insns = new UInt32[4];

			for(UInt32 ls=0; ls<0x40000; ls+=128)
			{
				if(SpuGetMemory(threadId, ls, ref insns))
				{
					if(IsSpuGuid(insns))
					{
						var e = new SymbolLookup.SpuLsGuidMapEntry();
						e.guid = ExtractSpuGuid(insns);
						e.ls   = ls;
						e.ea   = 0;

						foreach(SymbolLookup.SpuEaGuidMapEntry egme in sym.SpuEaGuidMap)
						{
							if(egme.guidInsns[0]==insns[0] && egme.guidInsns[1]==insns[1] &&
							   egme.guidInsns[2]==insns[2] && egme.guidInsns[3]==insns[3])
							{
								e.ea = egme.ea;
								break;
							}
						}

						lsMap.Add(e);
					}
				}
			}

			return lsMap.ToArray();
		}

		////////////////////////////////////////////////////////////////////////
		// DisplayEaGuidMap
		////////////////////////////////////////////////////////////////////////
		// Useful debugging function, please do not delete
		private void DisplayEaGuidMap(SymbolLookup sym)
		{
			TtyOutput("\n*** ALL SPU GUIDS ***\n");
			foreach(SymbolLookup.SpuEaGuidMapEntry egme in sym.SpuEaGuidMap)
			{
				UInt64 guid = ExtractSpuGuid(egme.guidInsns);
				TtyOutput(" 0x"+egme.ea.ToString("x8")+" : GUID = "
					+((guid>>56)&0xff).ToString("x2")+" "+((guid>>48)&0xff).ToString("x2")+" "
					+((guid>>40)&0xff).ToString("x2")+" "+((guid>>32)&0xff).ToString("x2")+" "
					+((guid>>24)&0xff).ToString("x2")+" "+((guid>>16)&0xff).ToString("x2")+" "
					+((guid>> 8)&0xff).ToString("x2")+" "+((guid>> 0)&0xff).ToString("x2")+" "
					+"\""+sym.Lookup(egme.ea)+"\"\n");
			}
		}

		////////////////////////////////////////////////////////////////////////
		// DisplayLsGuidMap
		////////////////////////////////////////////////////////////////////////
		private void DisplayLsGuidMap(SymbolLookup.SpuLsGuidMapEntry[] lsGuidMap, SymbolLookup sym)
		{
			TtyOutput("*** SPU GUIDS ***\n");
			foreach(SymbolLookup.SpuLsGuidMapEntry lgme in lsGuidMap)
			{
				UInt64 guid = lgme.guid;
				TtyOutput(" 0x"+lgme.ls.ToString("x6")+" : GUID = "
					+((guid>>56)&0xff).ToString("x2")+" "+((guid>>48)&0xff).ToString("x2")+" "
					+((guid>>40)&0xff).ToString("x2")+" "+((guid>>32)&0xff).ToString("x2")+" "
					+((guid>>24)&0xff).ToString("x2")+" "+((guid>>16)&0xff).ToString("x2")+" "
					+((guid>> 8)&0xff).ToString("x2")+" "+((guid>> 0)&0xff).ToString("x2")+" "
					+"\""+sym.Lookup(lgme.ea)+"\" (ea 0x"+lgme.ea.ToString("x8")+")\n");
			}
		}

		////////////////////////////////////////////////////////////////////////
		// SpuExceptionHandler
		////////////////////////////////////////////////////////////////////////
		private void SpuExceptionHandler(PS3TMAPI.SPUThreadStopReason reason, UInt32 pc, UInt32 sp, UInt32 threadGroupId, UInt32 threadId, bool darValid, UInt64 dar, UInt64 dsipr, UInt64 dsisr)
		{
			// Input values seem to be the wrong endian
			reason        = (PS3TMAPI.SPUThreadStopReason)EndianSwap((UInt32)reason);
			pc            = EndianSwap(pc);
			sp            = EndianSwap(sp);
			threadGroupId = EndianSwap(threadGroupId);
			threadId      = EndianSwap(threadId);
			dar           = EndianSwap(dar);
			dsipr         = EndianSwap(dsipr);
			dsisr         = EndianSwap(dsisr);

			// All SPU threads within the same thread group will stop.  Only
			// want to handle ones that have actually crashed.
			if(reason == PS3TMAPI.SPUThreadStopReason.NoException)
			{
				return;
			}

			// Can only really handle the first crash we get notified about this
			// kick (eg, multiple SPUs can crash simultaneously).  The resulting
			// coredump will contain all crashes, so this isn't a real issue.
			if(m_CrashedThisKick)
			{
				return;
			}
			m_CrashedThisKick = true;
			m_IsRunning = false;

			// Notice that this is different from all other platforms.  For SPU
			// crashes, we gather register and stack info, then trigger a
			// coredump, before any target crash callbacks are executed.  If a
			// target crash callback is executed too soon, we can lose the
			// critical information.
			UInt64 targetCallbackAddr;
			var sym = new SymbolLookup(CmpFilename(), TargetPlatformId.PS3);

			TtyOutput("\n**** BEGIN EXCEPTION DUMP\n\n");

			BeginCaptureTtyOutput();

			// Display basic thread group information
			PS3TMAPI.SPUThreadGroupInfo threadGroupInfo;
			if(PS3TMAPI.SUCCEEDED(PS3TMAPI.GetSPUThreadGroupInfo(m_hTarget, m_Pid, threadGroupId, out threadGroupInfo)))
			{
				TtyOutput("**** Exception "+reason.ToString()+" caught in thread group ["+threadGroupInfo.GroupName+"] at address 0x"+pc.ToString("x8")+"\n");
				TtyOutput("Thread group id      0x"+threadGroupId.ToString("x8")+"\n");
				TtyOutput("Thread group name    "+threadGroupInfo.GroupName+"\n");
				TtyOutput("Thread group state   "+threadGroupInfo.State.ToString()+"\n");
			}
			else
			{
				TtyOutput("**** Exception "+reason.ToString()+" caught in thread group 0x"+threadGroupId.ToString("x8")+" at address 0x"+pc.ToString("x8")+"\n");
				TtyOutput("Thread group id      0x"+threadGroupId.ToString("x8")+"\n");
			}

			// Display basic thread information
			PS3TMAPI.SPUThreadInfo threadInfo;
			TtyOutput("Thread id            0x"+threadId.ToString("x8")+"\n");
			if(PS3TMAPI.SUCCEEDED(PS3TMAPI.GetSPUThreadInfo(m_hTarget, m_Pid, threadId, out threadInfo)))
			{
				TtyOutput("Thread name          "+threadInfo.ThreadName+"\n");
				TtyOutput("Thread filename      "+threadInfo.Filename+"\n");
			}

			// Display basic register values
			if(darValid)
			{
				TtyOutput("MFCDAR               0x"+dar.ToString("x16")+"\n");
				TtyOutput("MFCDSIPR             0x"+dsipr.ToString("x16")+"\n");
				TtyOutput("MFCDSISR             0x"+dsisr.ToString("x16")+"\n");
			}
			TtyOutput("PC                   0x"+pc.ToString("x8")+"\n");
			TtyOutput("SP                   0x"+sp.ToString("x8")+"\n");

			// Display basic register values (see TargetCallbackStateSaver
			// comments for where these magic numbers come from).
			UInt32[] registerNums = new UInt32[]
			{
				// [r0 .. r127]
				0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f,
				0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x1a, 0x1b, 0x1c, 0x1d, 0x1e, 0x1f,
				0x20, 0x21, 0x22, 0x23, 0x24, 0x25, 0x26, 0x27, 0x28, 0x29, 0x2a, 0x2b, 0x2c, 0x2d, 0x2e, 0x2f,
				0x30, 0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39, 0x3a, 0x3b, 0x3c, 0x3d, 0x3e, 0x3f,
				0x40, 0x41, 0x42, 0x43, 0x44, 0x45, 0x46, 0x47, 0x48, 0x49, 0x4a, 0x4b, 0x4c, 0x4d, 0x4e, 0x4f,
				0x50, 0x51, 0x52, 0x53, 0x54, 0x55, 0x56, 0x57, 0x58, 0x59, 0x5a, 0x5b, 0x5c, 0x5d, 0x5e, 0x5f,
				0x60, 0x61, 0x62, 0x63, 0x64, 0x65, 0x66, 0x67, 0x68, 0x69, 0x6a, 0x6b, 0x6c, 0x6d, 0x6e, 0x6f,
				0x70, 0x71, 0x72, 0x73, 0x74, 0x75, 0x76, 0x77, 0x78, 0x79, 0x7a, 0x7b, 0x7c, 0x7d, 0x7e, 0x7f,

				0x81,   // fpscr
				0x90,   // decrementer
				0x91,   // srr0
				0x92,   // mb_stat
				0x93,   // mb_0
				0x94,   // mb_1
				0x95,   // mb_2
				0x96,   // mb_3
				0x97,   // pu_mb
				0x98,   // status
				0x99,   // cfg

				// [mfc_cq_sr0 .. mfc_cq_sr63]
				0xa0, 0xa1, 0xa2, 0xa3, 0xa4, 0xa5, 0xa6, 0xa7, 0xa8, 0xa9, 0xaa, 0xab, 0xac, 0xad, 0xae, 0xaf,
				0xb0, 0xb1, 0xb2, 0xb3, 0xb4, 0xb5, 0xb6, 0xb7, 0xb8, 0xb9, 0xba, 0xbb, 0xbc, 0xbd, 0xbe, 0xbf,
				0xc0, 0xc1, 0xc2, 0xc3, 0xc4, 0xc5, 0xc6, 0xc7, 0xc8, 0xc9, 0xca, 0xcb, 0xcc, 0xcd, 0xce, 0xcf,
				0xd0, 0xd1, 0xd2, 0xd3, 0xd4, 0xd5, 0xd6, 0xd7, 0xd8, 0xd9, 0xda, 0xdb, 0xdc, 0xdd, 0xde, 0xdf,

				// [mfc_cq_sr64 .. mfc_cq_sr95] are the PPU proxy queue, which we don't really care about
			  //0xe0, 0xe1, 0xe2, 0xe3, 0xe4, 0xe5, 0xe6, 0xe7, 0xe8, 0xe9, 0xea, 0xeb, 0xec, 0xed, 0xee, 0xef,
			  //0xf0, 0xf1, 0xf2, 0xf3, 0xf4, 0xf5, 0xf6, 0xf7, 0xf8, 0xf9, 0xfa, 0xfb, 0xfc, 0xfd, 0xfe, 0xff,
			};
			UInt32 lr = 0;
			UInt64[] registerValues;
			if(SpuGetRegisters(threadId, registerNums, out registerValues))
			{
				lr = (UInt32)(registerValues[0]>>32);

				TtyOutput("FPSCR                0x"+registerValues[0x80*2].ToString("x8")+"\n");
				TtyOutput("DECREMENTER          0x"+registerValues[0x81*2].ToString("x8")+"\n");
				TtyOutput("SRR0                 0x"+registerValues[0x82*2].ToString("x8")+"\n");
				TtyOutput("SPU_MB_STAT          0x"+registerValues[0x83*2].ToString("x8")+"\n");
				TtyOutput("SPU_MB_0             0x"+registerValues[0x84*2].ToString("x8")+"\n");
				TtyOutput("SPU_MB_1             0x"+registerValues[0x85*2].ToString("x8")+"\n");
				TtyOutput("SPU_MB_2             0x"+registerValues[0x86*2].ToString("x8")+"\n");
				TtyOutput("SPU_MB_3             0x"+registerValues[0x87*2].ToString("x8")+"\n");
				TtyOutput("PU_MB                0x"+registerValues[0x88*2].ToString("x8")+"\n");
				TtyOutput("SPU_STATUS           0x"+registerValues[0x89*2].ToString("x8")+"\n");
				TtyOutput("SPU_CFG              0x"+registerValues[0x8a*2].ToString("x8")+"\n\n");

				for(uint i=0,j=64; i<64; ++i,++j)
				{
					TtyOutput("r"+i.ToString("d3")+"=["+(registerValues[i*2+0]>>32).ToString("x8")+"|"
					                                   +(registerValues[i*2+0]&0xffffffff).ToString("x8")+"|"
					                                   +(registerValues[i*2+1]>>32).ToString("x8")+"|"
					                                   +(registerValues[i*2+1]&0xffffffff).ToString("x8")+"]    "
					         +"r"+j.ToString("d3")+"=["+(registerValues[j*2+0]>>32).ToString("x8")+"|"
					                                   +(registerValues[j*2+0]&0xffffffff).ToString("x8")+"|"
					                                   +(registerValues[j*2+1]>>32).ToString("x8")+"|"
					                                   +(registerValues[j*2+1]&0xffffffff).ToString("x8")+"]\n");
				}

				// SPU MFC command queue.
				//
				// For documentation on these registers, see
				// "Cell Broadband Engine Registers v1.5",
				// "3.3.2.1 MFC Command Queue Context Save/Restore Register (MFC_CQ_SR)".
				//
				// For readability, the display output is designed to match the
				// register display in ProDG.
				//
				TtyOutput("\nQueue Valid? EA               LSA    Size  Op       Tag RT LtAddr LtSize LNQECVBSI SM:DepState Cmd DepType\n");
				for(uint i=0; i<16; ++i)
				{
					UInt64 cmdq_dw0   = registerValues[0x8b*2+i*8+0];
					UInt64 cmdq_dw1   = registerValues[0x8b*2+i*8+2];
					UInt64 cmdq_dw2   = registerValues[0x8b*2+i*8+4];
					UInt32 issue_word = (UInt32)registerValues[0x8b*2+i*8+6];

					// CMDQ_DW0
					UInt64 ltaddr     = (cmdq_dw0&0xfffe000000000000)>>49;
					UInt64 ltsize     = (cmdq_dw0&0x0001ffe000000000)>>37;
					UInt64 opcode     = (cmdq_dw0&0x0000001fe0000000)>>29;
					UInt64 tag        = (cmdq_dw0&0x000000001f000000)>>24;
					UInt64 listvalid  = (cmdq_dw0&0x0000000000800000)>>23;
					UInt64 rclassid   = (cmdq_dw0&0x0000000000700000)>>20;
					UInt64 tclassid   = (cmdq_dw0&0x00000000000e0000)>>17;
					UInt64 reserved0  = (cmdq_dw0&0x000000000001ffff)>>0;
					string L  = (listvalid!=0) ? "L" : "-";
					UInt64 rt = rclassid * 16 + tclassid;
					string opcodestr;
					switch(opcode)
					{
						case 0x0020: opcodestr="PUT     "; break;       case 0x0046: opcodestr="GETLF   "; break;
						case 0x0021: opcodestr="PUTB    "; break;       case 0x0048: opcodestr="GETS    "; break;
						case 0x0022: opcodestr="PUTF    "; break;       case 0x0049: opcodestr="GETBS   "; break;
						case 0x0024: opcodestr="PUTL    "; break;       case 0x004a: opcodestr="GETFS   "; break;
						case 0x0025: opcodestr="PUTLB   "; break;       case 0x0080: opcodestr="SDCRT   "; break;
						case 0x0026: opcodestr="PUTLF   "; break;       case 0x0081: opcodestr="SDCRTST "; break;
						case 0x0028: opcodestr="PUTS    "; break;       case 0x0089: opcodestr="SDCRZ   "; break;
						case 0x0029: opcodestr="PUTBS   "; break;       case 0x008d: opcodestr="SDCRST  "; break;
						case 0x002a: opcodestr="PUTFS   "; break;       case 0x008f: opcodestr="SDCRF   "; break;
						case 0x0030: opcodestr="PUTR    "; break;       case 0x00a0: opcodestr="SNDSIG  "; break;
						case 0x0031: opcodestr="PUTRB   "; break;       case 0x00a1: opcodestr="SNDSIGB "; break;
						case 0x0032: opcodestr="PUTRF   "; break;       case 0x00a2: opcodestr="SNDSIGF "; break;
						case 0x0034: opcodestr="PUTRL   "; break;       case 0x00b0: opcodestr="PUTLLUC "; break;
						case 0x0035: opcodestr="PUTRLB  "; break;       case 0x00b4: opcodestr="PUTLLC  "; break;
						case 0x0036: opcodestr="PUTRLF  "; break;       case 0x00b8: opcodestr="PUTQLLUC"; break;
						case 0x0040: opcodestr="GET     "; break;       case 0x00c0: opcodestr="BARRIER "; break;
						case 0x0041: opcodestr="GETB    "; break;       case 0x00c8: opcodestr="MFCEIEIO"; break;
						case 0x0042: opcodestr="GETF    "; break;       case 0x00cc: opcodestr="MFCSYNC "; break;
						case 0x0044: opcodestr="GETL    "; break;       case 0x00d0: opcodestr="GETLLAR "; break;
						case 0x0045: opcodestr="GETLB   "; break;       default:     opcodestr="????????"; break;
					}

					// CMDQ_DW1
					UInt64 eah        = cmdq_dw1&0xfffffffffffff000;
					UInt64 reserved1  = cmdq_dw1&0x0000000000000fff;

					// CMDQ_DW2
					UInt64 lsa        = (cmdq_dw2&0xfffc000000000000)>>50;
					UInt64 size       = (cmdq_dw2&0x0003ff8000000000)>>39;
					UInt64 eal        = (cmdq_dw2&0x0000007ff8000000)>>27;
					UInt64 nopvalid   = (cmdq_dw2&0x0000000004000000)>>26;
					UInt64 qwordvalid = (cmdq_dw2&0x0000000002000000)>>25;
					UInt64 eavalid    = (cmdq_dw2&0x0000000001000000)>>24;
					UInt64 commanderr = (cmdq_dw2&0x0000000000800000)>>23;
					UInt64 reserved2  = (cmdq_dw2&0x00000000007fffff);
					UInt64 ea = eah | eal;
					string N  = (nopvalid   !=0) ? "N" : "-";
					string Q  = (qwordvalid !=0) ? "Q" : "-";
					string Q0 = (qwordvalid !=0) ? "0" : " ";
					string E  = (eavalid    !=0) ? "E" : "-";
					string C  = (commanderr !=0) ? "C" : "-";

					// issue_word
					UInt32 entrydepstate = (issue_word&0xffff0000)>>16;
					UInt32 entryvalid    = (issue_word&0x00008000)>>15;
					UInt32 issuetclass   = (issue_word&0x00006000)>>13;
					UInt32 issuetag      = (issue_word&0x00001f00)>>8;
					UInt32 issuecmd      = (issue_word&0x00000080)>>7;
					UInt32 last          = (issue_word&0x00000040)>>6;
					UInt32 deptype       = (issue_word&0x00000030)>>4;
					UInt32 stall         = (issue_word&0x00000008)>>3;
					UInt32 dependency    = (issue_word&0x00000004)>>2;
					UInt32 reservediw    = (issue_word&0x00000003);
					string validstr = (entryvalid != 0) ? "Yes" : "No ";
					string V = (entryvalid != 0) ? "V" : "-";
					string B = (last       != 0) ? "-" : "B";
					string S = (stall      != 0) ? "S" : "-";
					string I = (dependency != 0) ? "-" : "I";
					string cmd = (issuecmd != 0) ? "PUT" : "GET";
					string dep;
					switch(deptype)
					{
						case 0:  dep="Normal";           break;
						case 1:  dep="Barrier Modifier"; break;
						case 2:  dep="Barrier Command";  break;
						default: dep="Reserved";         break;
					}

					TtyOutput(i.ToString("x")+"     "+validstr+"    "+ea.ToString("x16")+" "+lsa.ToString("x5")+"* "+size.ToString("x3")+Q0+"  "+opcodestr+" "+tag.ToString("x2")+"  "+rt.ToString("x2")+" "+ltaddr.ToString("x4")+"   "+ltsize.ToString("x3")+"    "+L+N+Q+E+C+V+B+S+I+"    "+entrydepstate.ToString("x4")+"     "+cmd+" "+dep+"\n");
				}
			}

			// Walk stack.
			TtyOutput("\n");
		  //DisplayEaGuidMap(sym);
			SpuPrintThreadStack(sym, threadId, pc, lr, sp);

			// Determine the address of the target crash callback function (if it exists)
			targetCallbackAddr = LookupTargetCallback(sym, TARGET_CRASH_CALLBACK_NAME);

			// Trigger a coredump
			TtyOutput("\n");
			const UInt64 userData1 = 0;
			const UInt64 userData2 = 0;
			const UInt64 userData3 = 0;
			if(PS3TMAPI.SUCCEEDED(PS3TMAPI.TriggerCoreDump(m_hTarget, m_Pid, userData1, userData2, userData3)))
			{
				m_PostCoredumpCallback = (string coredump) =>
				{
					// Now that all the critical information has been gathered,
					// call the target crash callbacks.

					// First choose a PPU thread to execute the target crash
					// callback on. Need to find a stopped thread, cannot use a
					// sleeping thread.
					UInt64 targetCallbackTid = 0;
					string targetCallbackThreadName = "";
					if(targetCallbackAddr != 0)
					{
						uint retryCount = 0;
						for(;;)
						{
							if(PS3TMAPI.FAILED(PS3TMAPI.ProcessStop(m_hTarget, m_Pid)))
							{
								break;
							}
							UInt64[] ppuThreadIds;
							UInt64[] spuThreadGroupIds;
							if(PS3TMAPI.SUCCEEDED(PS3TMAPI.GetThreadList(m_hTarget, m_Pid, out ppuThreadIds, out spuThreadGroupIds)) && ppuThreadIds.Length>0)
							{
								foreach(UInt64 ppuTid in ppuThreadIds)
								{
									PS3TMAPI.PPUThreadInfoEx tmpThreadInfo;
									if(PS3TMAPI.SUCCEEDED(PS3TMAPI.GetPPUThreadInfoEx(m_hTarget, m_Pid, ppuTid, out tmpThreadInfo)))
									{
										if(tmpThreadInfo.State == PS3TMAPI.PPUThreadState.Stop)
										{
											targetCallbackTid = ppuTid;
											targetCallbackThreadName = tmpThreadInfo.ThreadName;
											break;
										}
									}
								}
							}
							if(targetCallbackTid!=0 || ++retryCount>=10 || PS3TMAPI.FAILED(PS3TMAPI.ProcessContinue(m_hTarget, m_Pid)))
							{
								break;
							}
						}
					}

					if(targetCallbackTid == 0)
					{
						TtyOutput("\nFailed to find an appropriate PPU thread for running the target callbacks.\n");
						PS3TMAPI.ProcessStop(m_hTarget, m_Pid);
						m_CrashedThisKick = false;
						EndCaptureTtyOutput();
						// Since we don't have a PPU thread, we cannot fetch the
						// Bugstar config, and hence cannot raise a bug.
						sym.Dispose();
						TtyOutput("\n**** END EXCEPTION DUMP\n");
					}
					else
					{
						TtyOutput("\nSelected PPU thread \""+targetCallbackThreadName+"\" (0x"+targetCallbackTid.ToString("x8")+") for executing target crash callback.\n");
						TtyOutput("Executing "+TARGET_CRASH_CALLBACK_NAME+", pass 0\n");
						ExecuteTargetCallback(targetCallbackTid, targetCallbackAddr, 0, () =>
						{
							TtyOutput("Executing "+TARGET_CRASH_CALLBACK_NAME+", pass 1\n");
							ExecuteTargetCallback(targetCallbackTid, targetCallbackAddr, 1, () =>
							{
								PS3TMAPI.ProcessStop(m_hTarget, m_Pid);
								m_CrashedThisKick = false;
								string bugDescription = EndCaptureTtyOutput();
								RaiseBug(sym, targetCallbackTid, null, bugDescription,
									new string[]{coredump, PpuSelfFilename()},
									() => TtyOutput("\n**** END EXCEPTION DUMP\n"));
								sym.Dispose();
							});
						});
					}
				};
			}
		}

		////////////////////////////////////////////////////////////////////////
		// SpuExceptionHandler
		////////////////////////////////////////////////////////////////////////
		private void SpuExceptionHandler(PS3TMAPI.SPUThreadStopReason reason, UInt32 pc, UInt32 sp, UInt32 threadGroupId, UInt32 threadId, UInt64 dar, UInt64 dsipr, UInt64 dsisr)
		{
			SpuExceptionHandler(reason, pc, sp, threadGroupId, threadId, true, dar, dsipr, dsisr);
		}

		////////////////////////////////////////////////////////////////////////
		// SpuExceptionHandler
		////////////////////////////////////////////////////////////////////////
		private void SpuExceptionHandler(PS3TMAPI.SPUThreadStopReason reason, UInt32 pc, UInt32 sp, UInt32 threadGroupId, UInt32 threadId)
		{
			SpuExceptionHandler(reason, pc, sp, threadGroupId, threadId, false, 0, 0, 0);
		}

		////////////////////////////////////////////////////////////////////////
		// TargetEventCallback
		////////////////////////////////////////////////////////////////////////
		private static void TargetEventCallback(int hTarget, PS3TMAPI.SNRESULT res, PS3TMAPI.TargetEvent[] targetEventList, object userData)
		{
			var self = (PS3Target)userData;
			foreach(PS3TMAPI.TargetEvent e in targetEventList)
			{
				switch(e.Type)
				{
					case PS3TMAPI.TargetEventType.UnitStatusChange:
					{
						switch (e.EventData.UnitStatusChangeData.Status)
						{
							case PS3TMAPI.UnitStatus.Connected:
							{
								self.SetIsConnected(true);
								break;
							}

							case PS3TMAPI.UnitStatus.NotConnected:
							{
								self.SetIsConnected(false);
								break;
							}

							case PS3TMAPI.UnitStatus.Running:
							{
								self.m_IsRunning = true;
								self.m_StartNewLog = true;
								// It is possible to get a Running status
								// without a Connected status (if R*TM is booted
								// while the PS3 is rebooting), so query the
								// connection status here.  Calling
								// SetIsConnected will then call UpdatePid to
								// get the new process id.
								PS3TMAPI.ConnectStatus connectStatus;
								string usage;
								if(PS3TMAPI.SUCCEEDED(PS3TMAPI.GetConnectStatus(hTarget, out connectStatus, out usage)))
								{
									bool connected = (connectStatus == PS3TMAPI.ConnectStatus.Connected);
									self.SetIsConnected(connected);
								}
								break;
							}

							case PS3TMAPI.UnitStatus.Resetting:
							{
								self.m_IsRunning = false;
								if(self.m_StartNewLog)
								{
									self.RequestNewLogFile();
									self.m_StartNewLog = false;
								}
								break;
							}

							case PS3TMAPI.UnitStatus.Reset:
							case PS3TMAPI.UnitStatus.Stopped:
							case PS3TMAPI.UnitStatus.Signalled:
							{
								self.m_IsRunning = false;
								break;
							}

						  //case PS3TMAPI.UnitStatus.Missing:
						  //case PS3TMAPI.UnitStatus.StatusChange:
						  //case PS3TMAPI.UnitStatus.Unknown:
						}
						break;
					}

					case PS3TMAPI.TargetEventType.TargetSpecific:
					{
						PS3TMAPI.TargetSpecificEventType type = e.TargetSpecific.Data.Type;
						switch(type)
						{
							case PS3TMAPI.TargetSpecificEventType.PPUExcAlignment:
								self.PpuExceptionHandler(
									type,
									e.TargetSpecific.Data.PPUAlignmentException.PC,
									e.TargetSpecific.Data.PPUAlignmentException.SP,
									e.TargetSpecific.Data.PPUAlignmentException.ThreadID,
									e.TargetSpecific.Data.PPUAlignmentException.HWThreadNumber,
									e.TargetSpecific.Data.PPUAlignmentException.DAR,
									e.TargetSpecific.Data.PPUAlignmentException.DSISR);
								break;

							case PS3TMAPI.TargetSpecificEventType.PPUExcDataMAT:
								self.PpuExceptionHandler(
									type,
									e.TargetSpecific.Data.PPUDataMatException.PC,
									e.TargetSpecific.Data.PPUDataMatException.SP,
									e.TargetSpecific.Data.PPUDataMatException.ThreadID,
									e.TargetSpecific.Data.PPUDataMatException.HWThreadNumber,
									e.TargetSpecific.Data.PPUDataMatException.DAR,
									e.TargetSpecific.Data.PPUDataMatException.DSISR);
								break;

							case PS3TMAPI.TargetSpecificEventType.PPUExcDabrMatch:
							case PS3TMAPI.TargetSpecificEventType.PPUExcDataHtabMiss:
							case PS3TMAPI.TargetSpecificEventType.PPUExcDataSlbMiss:
							case PS3TMAPI.TargetSpecificEventType.PPUExcFloat:
							case PS3TMAPI.TargetSpecificEventType.PPUExcIllInst:
							case PS3TMAPI.TargetSpecificEventType.PPUExcPrevInt:
							case PS3TMAPI.TargetSpecificEventType.PPUExcTextHtabMiss:
							case PS3TMAPI.TargetSpecificEventType.PPUExcTextSlbMiss:
							case PS3TMAPI.TargetSpecificEventType.PPUExcTrap:
								self.PpuExceptionHandler(
									type,
									e.TargetSpecific.Data.PPUException.PC,
									e.TargetSpecific.Data.PPUException.SP,
									e.TargetSpecific.Data.PPUException.ThreadID,
									e.TargetSpecific.Data.PPUException.HWThreadNumber);
								break;

							case PS3TMAPI.TargetSpecificEventType.CoreDumpComplete:
							{
								string coredump = null;
								if(self.m_Pid != 0)
								{
									PS3TMAPI.ProcessInfo processInfo;
									if(PS3TMAPI.SUCCEEDED(PS3TMAPI.GetProcessInfo(self.m_hTarget, self.m_Pid, out processInfo)))
									{
										string name = Regex.Replace(processInfo.Hdr.ELFPath, @"^.*?([^\\/]+)\.self$", "$1");
										string dst = RockstarTargetManager.GetCrashdumpDir()+name+DateTime.Now.ToString("-yyyyMMdd-HHmmss-")+self.m_Pid.ToString()+".elf";
										PS3TMAPI.CoreDumpFlag flags;
										if(PS3TMAPI.SUCCEEDED(PS3TMAPI.GetCoreDumpFlags(self.m_hTarget, out flags)))
										{
											if((flags & PS3TMAPI.CoreDumpFlag.ToAppHome) != 0)
											{
												var targetInfo = new PS3TMAPI.TargetInfo();
												targetInfo.Flags = PS3TMAPI.TargetInfoFlag.TargetID;
												targetInfo.Target = self.m_hTarget;
												if(PS3TMAPI.SUCCEEDED(PS3TMAPI.GetTargetInfo(ref targetInfo)))
												{
													string app_home = targetInfo.FSDir;
													string src = Path.Combine(app_home, e.TargetSpecific.Data.CoreDumpComplete.Filename);
													try
													{
														File.Move(src, dst);
														coredump = dst;
														self.TtyOutput("\nCoredump moved from \""+src+"\" to \""+coredump+"\"\n\n");
													}
													catch
													{
													}
												}
											}
											else if((flags & (PS3TMAPI.CoreDumpFlag.ToDevHDD0|PS3TMAPI.CoreDumpFlag.ToDevMS|PS3TMAPI.CoreDumpFlag.ToDevUSB)) != 0)
											{
											}
											else
											{
											}
										}
									}
								}
								VoidCallbackString postCoredumpCallback = self.m_PostCoredumpCallback;
								if(postCoredumpCallback != null)
								{
									self.m_PostCoredumpCallback = null;
									postCoredumpCallback(coredump);
								}
								break;
							}

 					    	case PS3TMAPI.TargetSpecificEventType.SPUThreadStop:
							{
								self.SpuExceptionHandler(
									e.TargetSpecific.Data.SPUThreadStop.Reason,
									e.TargetSpecific.Data.SPUThreadStop.PC,
									e.TargetSpecific.Data.SPUThreadStop.SP,
									e.TargetSpecific.Data.SPUThreadStop.ThreadGroupID,
									e.TargetSpecific.Data.SPUThreadStop.ThreadID);
								break;
							}

	 						case PS3TMAPI.TargetSpecificEventType.SPUThreadStopEx:
							{
								self.SpuExceptionHandler(
									e.TargetSpecific.Data.SPUThreadStopEx.Reason,
									e.TargetSpecific.Data.SPUThreadStopEx.PC,
									e.TargetSpecific.Data.SPUThreadStopEx.SP,
									e.TargetSpecific.Data.SPUThreadStopEx.ThreadGroupID,
									e.TargetSpecific.Data.SPUThreadStopEx.ThreadID,
									e.TargetSpecific.Data.SPUThreadStopEx.MFCDAR,
									e.TargetSpecific.Data.SPUThreadStopEx.MFCDSIPR,
									e.TargetSpecific.Data.SPUThreadStopEx.MFCDSISR);
								break;
							}

							case PS3TMAPI.TargetSpecificEventType.PPUExcStopInit:
							{
								self.m_IsRunning = PS3TMAPI.SUCCEEDED(PS3TMAPI.ProcessContinue(self.m_hTarget, self.m_Pid));
								break;
							}

 						  //case PS3TMAPI.TargetSpecificEventType.CoreDumpStart:
 						  //case PS3TMAPI.TargetSpecificEventType.DAInitialised:
 						  //case PS3TMAPI.TargetSpecificEventType.Footswitch:
 						  //case PS3TMAPI.TargetSpecificEventType.InstallPackagePath:
 						  //case PS3TMAPI.TargetSpecificEventType.InstallPackageProgress:
						  //case PS3TMAPI.TargetSpecificEventType.PPUExcStop:
 						  //case PS3TMAPI.TargetSpecificEventType.PPUThreadCreate:
 						  //case PS3TMAPI.TargetSpecificEventType.PPUThreadExit:
 						  //case PS3TMAPI.TargetSpecificEventType.ProcessCreate:
 						  //case PS3TMAPI.TargetSpecificEventType.ProcessExit:
 						  //case PS3TMAPI.TargetSpecificEventType.ProcessExitSpawn:
 						  //case PS3TMAPI.TargetSpecificEventType.ProcessKill:
 						  //case PS3TMAPI.TargetSpecificEventType.PRXLoad:
 						  //case PS3TMAPI.TargetSpecificEventType.PRXUnload:
 						  //case PS3TMAPI.TargetSpecificEventType.RawNotify:
 						  //case PS3TMAPI.TargetSpecificEventType.SPUThreadGroupDestroy:
 						  //case PS3TMAPI.TargetSpecificEventType.SPUThreadStart:
 						  //case PS3TMAPI.TargetSpecificEventType.SPUThreadStopInit:
						}
						break;
					}


				  //case PS3TMAPI.TargetEventType.BDFormatFinished:
				  //case PS3TMAPI.TargetEventType.BDFormatStarted:
				  //case PS3TMAPI.TargetEventType.BDIsotransferFinished:
				  //case PS3TMAPI.TargetEventType.BDIsotransferStarted:
				  //case PS3TMAPI.TargetEventType.BDMountFinished:
				  //case PS3TMAPI.TargetEventType.BDMountStarted:
				  //case PS3TMAPI.TargetEventType.BDUnmountFinished:
				  //case PS3TMAPI.TargetEventType.BDUnmountStarted:
				  //case PS3TMAPI.TargetEventType.Details:
				  //case PS3TMAPI.TargetEventType.ModuleDoneRemove:
				  //case PS3TMAPI.TargetEventType.ModuleDoneResident:
				  //case PS3TMAPI.TargetEventType.ModuleLoad:
				  //case PS3TMAPI.TargetEventType.ModuleRunning:
				  //case PS3TMAPI.TargetEventType.ModuleStopped:
				  //case PS3TMAPI.TargetEventType.ModuleStoppedRemove:
				  //case PS3TMAPI.TargetEventType.PowerStatusChange:
				  //case PS3TMAPI.TargetEventType.ResetEnd:
				  //case PS3TMAPI.TargetEventType.ResetStarted:
				  //case PS3TMAPI.TargetEventType.TTYStreamAdded:
				  //case PS3TMAPI.TargetEventType.TTYStreamDeleted:
				}
			}
		}

		////////////////////////////////////////////////////////////////////////
		// TTYEventCallback
		////////////////////////////////////////////////////////////////////////
		private static void TTYEventCallback(int hTarget, UInt32 streamID, PS3TMAPI.SNRESULT res, string data, object userData)
		{
			var self = (PS3Target)userData;
			self.TtyOutput(data);
		}

		////////////////////////////////////////////////////////////////////////
		// Update
		////////////////////////////////////////////////////////////////////////
		internal void Update()
		{
			if(!Enabled)
			{
				if(m_FirstUpdateDone)
				{
					PS3TMAPI.CancelTargetEvents(m_hTarget);
					PS3TMAPI.CancelTTYEvents(m_hTarget, SNPS3_TTY_ALL_STREAMS);
					m_FirstUpdateDone = false;
				}
				m_IsConnected = false;
				return;
			}

			if(!m_FirstUpdateDone)
			{
				Object t = (Object)this;
				if(PS3TMAPI.FAILED(PS3TMAPI.RegisterTargetEventHandler(m_hTarget, TargetEventCallback, ref t)))
				{
					return;
				}
				if(PS3TMAPI.FAILED(PS3TMAPI.RegisterTTYEventHandler(m_hTarget, SNPS3_TTY_ALL_STREAMS, TTYEventCallback, ref t)))
				{
					return;
				}

				// Before calling PS3TMAPI.GetConnectStatus() check the status of
				// the CPU.  If the CPU is resetting PS3TMAPI.GetConnectStatus()
				// will block, so skip calling PS3TMAPI.GetConnectStatus().  The
				// connected state will be updated in the target event handler when
				// the reset completes.
				bool connected = false;
				PS3TMAPI.UnitStatus unitStatus;
				if(PS3TMAPI.SUCCEEDED(PS3TMAPI.GetStatus(m_hTarget, PS3TMAPI.UnitType.PPU, out unitStatus)))
				{
					if(unitStatus != PS3TMAPI.UnitStatus.Resetting)
					{
						PS3TMAPI.ConnectStatus connectStatus;
						string usage;
						if(PS3TMAPI.SUCCEEDED(PS3TMAPI.GetConnectStatus(m_hTarget, out connectStatus, out usage)))
						{
							connected = (connectStatus == PS3TMAPI.ConnectStatus.Connected);
						}
					}
				}
				SetIsConnected(connected);

				m_StartNewLog = (m_Pid != 0);

				m_IsRunning = false;
				if(m_Pid != 0)
				{
					m_IsRunning = (unitStatus == PS3TMAPI.UnitStatus.Running);

					// Check if any of the PPU threads are stopped
					if(m_IsRunning)
					{
						UInt64[] ppuThreadIds;
						UInt64[] spuThreadGroups;
						if(PS3TMAPI.SUCCEEDED(PS3TMAPI.GetThreadList(m_hTarget, m_Pid, out ppuThreadIds, out spuThreadGroups)))
						{
							foreach(UInt64 ppuTid in ppuThreadIds)
							{
								PS3TMAPI.PPUThreadInfoEx threadInfo;
								if(PS3TMAPI.SUCCEEDED(PS3TMAPI.GetPPUThreadInfoEx(m_hTarget, m_Pid, ppuTid, out threadInfo)))
								{
									// Found a stopped thread
									if(threadInfo.State == PS3TMAPI.PPUThreadState.Stop)
									{
										m_IsRunning = false;

										// Check if stopped at a sysTmCmdXXX function (probably waiting for R*TM connection)
										UInt32[] regNums = {0x40/*pc*/, 0x03/*r3*/};
										UInt64[] regVals;
										if(PpuGetRegisters(ppuTid, regNums, out regVals))
										{
											UInt64 pc = regVals[0*2];
											UInt64 r3 = regVals[1*2];
											if(r3 == 0x522a544d/*"R*TM"*/)
											{
												// Instruction at pc is "lfs f1, 0(r3)" ?
												UInt32 insn;
												if(PpuGetMemory(pc, out insn) && insn==0xc0230000)
												{
													string ignoredBugSummary;
													m_IsRunning = HandleTmCmd(ppuTid, pc, out ignoredBugSummary);
													break;
												}
											}
										}
									}
								}
							}
						}
					}
				}

				m_FirstUpdateDone = true;
			}

			VoidCallbackVoid callback = Interlocked.Exchange(ref m_WorkerThreadCallback, null);
			if(callback != null)
			{
				callback();
			}
		}

		////////////////////////////////////////////////////////////////////////
		// SelectedIcon
		////////////////////////////////////////////////////////////////////////
		public override Icon SelectedIcon
		{
			get
			{
				if(!m_IsConnected)
				{
					return Icon.NOT_AVAILABLE;
				}
				else if(!m_IsRunning)
				{
					return Icon.NOT_RUNNING;
				}
				else
				{
					return Icon.RUNNING;
				}
			}
		}

		////////////////////////////////////////////////////////////////////////
		// Launch
		////////////////////////////////////////////////////////////////////////
		public override bool Launch()
		{
			string path = Config.GetExpandedEnvVar("TargetPath");
			string cmdLine = CommandLine;
			cmdLine += " -rockstartargetmanager";
			string[] delims = {" ", "\t", "\n", Environment.NewLine};
			string[] argv = (path+" "+cmdLine).Split(delims, StringSplitOptions.RemoveEmptyEntries);
			m_WorkerThreadCallback = () =>
			{
				if(EnableLaunch)
				{
					if(m_StartNewLog)
					{
						RequestNewLogFile();
						m_StartNewLog = false;
					}

					m_IsRunning = false;
					if(PS3TMAPI.FAILED(PS3TMAPI.ResetEx(m_hTarget,
						PS3TMAPI.BootParameter.DebugMode,
						PS3TMAPI.BootParameterMask.BootMode,
						PS3TMAPI.ResetParameter.Hard,
						PS3TMAPI.ResetParameterMask.All,
						PS3TMAPI.SystemParameter.TargetModel60GB,
						PS3TMAPI.SystemParameterMask.TargetModel)))
					{
						m_IsRunning = true;
					}
					else
					{
						m_Pid = 0;
						UInt64 tid;
						if(PS3TMAPI.SUCCEEDED(PS3TMAPI.ProcessLoad(m_hTarget, SNPS3_DEF_PROCESS_PRI,
							argv[0], argv, null, out m_Pid, out tid,
							PS3TMAPI.LoadFlag.EnableDebugging|PS3TMAPI.LoadFlag.UseELFPriority|PS3TMAPI.LoadFlag.UseELFStackSize)))
						{
							m_IsRunning = PS3TMAPI.SUCCEEDED(PS3TMAPI.ProcessContinue(m_hTarget, m_Pid));
						}
					}
				}
			};
			return true;
		}

		////////////////////////////////////////////////////////////////////////
		// EnableLaunch
		////////////////////////////////////////////////////////////////////////
		public override bool EnableLaunch
		{
			get{return m_IsConnected && Config!=null;}
		}

		////////////////////////////////////////////////////////////////////////
		// Reboot
		////////////////////////////////////////////////////////////////////////
		public override void Reboot()
		{
			m_WorkerThreadCallback = () =>
			{
				if(EnableReboot)
				{
					Trace.WriteLine(String.Format("Rebooting {0}...", Name));
					m_IsRunning = false;
					if(m_StartNewLog)
					{
						RequestNewLogFile();
						m_StartNewLog = false;
					}
					PS3TMAPI.ResetEx(m_hTarget,
						PS3TMAPI.BootParameter.DebugMode,
						PS3TMAPI.BootParameterMask.BootMode,
						PS3TMAPI.ResetParameter.Hard,
						PS3TMAPI.ResetParameterMask.All,
						PS3TMAPI.SystemParameter.TargetModel60GB,
						PS3TMAPI.SystemParameterMask.TargetModel);
				}
			};
		}

		////////////////////////////////////////////////////////////////////////
		// EnableReboot
		////////////////////////////////////////////////////////////////////////
		public override bool EnableReboot
		{
			get{return m_IsConnected;}
		}

		////////////////////////////////////////////////////////////////////////
		// Stop
		////////////////////////////////////////////////////////////////////////
		public override void Stop()
		{
			m_WorkerThreadCallback = () =>
			{
				if(EnableStopGo)
				{
					if(PS3TMAPI.SUCCEEDED(PS3TMAPI.ProcessStop(m_hTarget, m_Pid)))
					{
						m_IsRunning = false;
					}
				}
			};
		}

		////////////////////////////////////////////////////////////////////////
		// Go
		////////////////////////////////////////////////////////////////////////
		public override void Go()
		{
			m_WorkerThreadCallback = () =>
			{
				if(EnableStopGo)
				{
					if(PS3TMAPI.SUCCEEDED(PS3TMAPI.ProcessContinue(m_hTarget, m_Pid)))
					{
						m_IsRunning = true;
					}
				}
			};
		}

		////////////////////////////////////////////////////////////////////////
		// EnableStopGo
		////////////////////////////////////////////////////////////////////////
		public override bool EnableStopGo
		{
			get{return m_Pid!=0;}
		}

		////////////////////////////////////////////////////////////////////////
		// Stopped
		////////////////////////////////////////////////////////////////////////
		public override bool Stopped
		{
			get{return !m_IsRunning;}
		}

		////////////////////////////////////////////////////////////////////////
		// Coredump
		////////////////////////////////////////////////////////////////////////
		public override void Coredump()
		{
			m_WorkerThreadCallback = () =>
			{
				if(EnableCoredump)
				{
					const UInt64 userData1 = 0;
					const UInt64 userData2 = 0;
					const UInt64 userData3 = 0;
					if(PS3TMAPI.SUCCEEDED(PS3TMAPI.TriggerCoreDump(m_hTarget, m_Pid, userData1, userData2, userData3)))
					{
						if(!m_IsRunning)
						{
							PS3TMAPI.ProcessContinue(m_hTarget, m_Pid);
						}
						else
						{
							m_IsRunning = false;
						}
					}
				}
			};
		}

		////////////////////////////////////////////////////////////////////////
		// EnableCoredump
		////////////////////////////////////////////////////////////////////////
		public override bool EnableCoredump
		{
			get{return m_Pid!=0;}
		}

		////////////////////////////////////////////////////////////////////////
		// Name
		////////////////////////////////////////////////////////////////////////
		public override string Name
		{
			get{return m_Name;}
		}

		////////////////////////////////////////////////////////////////////////
		// Alias
		////////////////////////////////////////////////////////////////////////
		public override string Alias
		{
			get { return m_Alias; }
		}

		////////////////////////////////////////////////////////////////////////
		// PlatformId
		////////////////////////////////////////////////////////////////////////
		public override TargetPlatformId PlatformId
		{
			get { return TargetPlatformId.PS3; }
		}
	}


	////////////////////////////////////////////////////////////////////////////
	// PS3TargetManager
	////////////////////////////////////////////////////////////////////////////
	public class PS3TargetManager : IDisposable
	{
		private Object m_LockObj = new Object();
		private PS3TMAPI m_Tmapi;
		private List<PS3Target> m_Targets = new List<PS3Target>();
		private Dictionary<string, PS3Target> m_TargetsByName = new Dictionary<string, PS3Target>();
		private Thread m_WorkerThread;
		private volatile bool m_WorkerThreadExit;


		////////////////////////////////////////////////////////////////////////
		// PS3TargetManager
		////////////////////////////////////////////////////////////////////////
		public PS3TargetManager(NetworkTtyListener networkTtyListener)
		{
			m_Tmapi = new PS3TMAPI();
			if(PS3TMAPI.FAILED(PS3TMAPI.InitTargetComms()))
			{
				m_Tmapi = null;
			}
			else
			{
				m_WorkerThreadExit = false;
				m_WorkerThread = new Thread(WorkerThread);
				m_WorkerThread.Name = "PS3TargetManager::WorkerThread";
				m_WorkerThread.Start();
			}
		}

		////////////////////////////////////////////////////////////////////////
		// ~PS3TargetManager
		////////////////////////////////////////////////////////////////////////
		~PS3TargetManager()
		{
			Dispose(false);
		}

		////////////////////////////////////////////////////////////////////////
		// Dispose
		////////////////////////////////////////////////////////////////////////
		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		////////////////////////////////////////////////////////////////////////
		// Dispose
		////////////////////////////////////////////////////////////////////////
		protected virtual void Dispose(bool disposing)
		{
			if(disposing)
			{
				if(m_WorkerThread != null)
				{
					m_WorkerThreadExit = true;
					m_WorkerThread.Join();
					// PS3Targets are disposed of in the worker thread
				}

				if(m_Tmapi != null)
				{
					PS3TMAPI.CloseTargetComms();
				}
			}
		}

		////////////////////////////////////////////////////////////////////////
		// GetDefaultTargetNames
		////////////////////////////////////////////////////////////////////////
		public string[] GetDefaultTargetNames()
		{
			List<string> ret = new List<string>();
			if(m_Tmapi != null)
			{
				PS3TMAPI.EnumerateTargets((int hTarget) =>
				{
					var info = new PS3TMAPI.TargetInfo();
					info.Flags = PS3TMAPI.TargetInfoFlag.TargetID;
					info.Target = hTarget;
					if(PS3TMAPI.SUCCEEDED(PS3TMAPI.GetTargetInfo(ref info)))
					{
						if(info.Type != "PS3_CORE_DUMP")
						{
							PS3TMAPI.TCPIPConnectProperties connectProperties;
							if(PS3TMAPI.SUCCEEDED(PS3TMAPI.GetConnectionInfo(hTarget, out connectProperties)))
							{
								ret.Add(connectProperties.IPAddress);
							}
						}
					}
					return 0;
				});
			}
			return ret.ToArray();
		}

		////////////////////////////////////////////////////////////////////////
		// AddTarget
		////////////////////////////////////////////////////////////////////////
		public Target AddTarget(string name)
		{
			if(m_Tmapi == null)
			{
				return null;
			}

			Trace.WriteLine("Acquiring target " + name + "...");

			PS3Target target = null;

			if(!m_TargetsByName.TryGetValue(name, out target))
			{
				PS3TMAPI.EnumerateTargets((int hTarget) =>
				{
					PS3TMAPI.TCPIPConnectProperties connectProperties;
					if(PS3TMAPI.SUCCEEDED(PS3TMAPI.GetConnectionInfo(hTarget, out connectProperties)))
					{
						if(connectProperties.IPAddress == name)
						{
							try
							{
								target = new PS3Target(name, hTarget);
								lock(m_LockObj)
								{
									m_Targets.Add(target);
									m_TargetsByName.Add(name, target);
								}
							}
							catch
							{
							}
							return 1;
						}
					}
					return 0;
				});
			}

			return target;
		}

		////////////////////////////////////////////////////////////////////////
		// WorkerThread
		////////////////////////////////////////////////////////////////////////
		private void WorkerThread()
		{
			if(PS3TMAPI.FAILED(PS3TMAPI.InitTargetComms()))
			{
				return;
			}

			while(!m_WorkerThreadExit)
			{
				PS3TMAPI.Kick();

				lock(m_LockObj)
				{
					foreach(PS3Target t in m_Targets)
					{
						t.Update();
					}
				}

				Thread.Sleep(300);
			}

			foreach(Target target in m_Targets)
			{
				target.Dispose();
			}
			m_Targets.Clear();

			PS3TMAPI.CloseTargetComms();
		}
	}
}
