using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;

namespace Rockstar.TargetManager
{
	////////////////////////////////////////////////////////////////////////////
	// Bugstar
	////////////////////////////////////////////////////////////////////////////
	public class Bugstar
	{
		private static bool   s_IsLoggedIn = false;
		private static string s_Username;
		private static string s_Authorization;
        private static string s_LastError = "";

        public static string GetLastError()
        {
            return s_LastError;
        }

		public delegate void VoidCallbackString(string str);


		////////////////////////////////////////////////////////////////////////
		// Config
		////////////////////////////////////////////////////////////////////////
		public class Config
		{
			private static string GetConfigString(XDocument doc, string xpath)
			{
				var el = doc.Root.XPathSelectElement(xpath);
				return el!=null ? el.Value : null;
			}

			private static UInt32 GetConfigUInt32(XDocument doc, string xpath)
			{
				UInt32 val;
				UInt32.TryParse(GetConfigString(doc, xpath), out val);
				return val;
			}

			public static Config FromString(string str)
			{
				var doc = XDocument.Parse(str);
				var config = new Config();
				config.RestServer   = GetConfigString(doc, "/bugstar_config/rest_server");
				config.LogDirectory = GetConfigString(doc, "/bugstar_config/log_directory");
				config.ProjectId    = GetConfigUInt32(doc, "/bugstar_config/project_id");
				config.BuildId      = GetConfigString(doc, "/bugstar_config/build_id");
				config.RootDir      = GetConfigString(doc, "/bugstar_config/root_dir");
				return config;
			}

			private string m_RestServer;
			public string RestServer
			{
				get{return m_RestServer;}
				set{m_RestServer = (value==null) ? null : EnsureEndsWith(value, '/');}
			}

			private string m_LogDirectory;
			public string LogDirectory
			{
				get{return m_LogDirectory;}
				set{m_LogDirectory = (value==null) ? null : EnsureEndsWith(value, '\\');}
			}

			public UInt32 ProjectId;

			public string BuildId;

			private string m_RootDir;
			public string RootDir
			{
				get{return m_RootDir;}
				set{m_RootDir = (value==null) ? null : EnsureEndsWith(value, '/');}
			}

			private static string EnsureEndsWith(string str, char end)
			{
				string s = str;
				if(s[s.Length-1] != end)
				{
					s += end;
				}
				return s;
			}
		}

		////////////////////////////////////////////////////////////////////////
		// HttpGet
		////////////////////////////////////////////////////////////////////////
		public static XDocument HttpGet(string uri)
		{
			var request = WebRequest.Create(uri);
			request.Headers.Add("Authorization", s_Authorization);
			request.Method = "GET";
			try
			{
                s_LastError = "<No error>";
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                XDocument doc = XDocument.Load(response.GetResponseStream());
                if(doc == null)
                {
                    s_LastError = "No error, null doc with status code " + response.StatusCode + " and response: \n" +
                        response.GetResponseStream().ToString();
                }
                return doc;
			}
            catch (Exception e)
            {
                Trace.WriteLine(e.ToString());
                s_LastError = "Exception: " + e.ToString();
            }
			return null;
		}

		////////////////////////////////////////////////////////////////////////
		// HttpPost
		////////////////////////////////////////////////////////////////////////
		public static XDocument HttpPost(string uri, XDocument doc, VoidCallbackString tty)
		{
			var ms = new MemoryStream();
			doc.Save(ms);
			byte[] data = Encoding.UTF8.GetBytes(Encoding.UTF8.GetString(ms.ToArray()));

			var request = WebRequest.Create(uri);
			request.Headers.Add("Authorization", s_Authorization);
			request.Method = "POST";
			request.ContentType = "application/xml";
			request.ContentLength = data.Length;
			try
			{
				var dataStream = request.GetRequestStream();
				dataStream.Write(data, 0, data.Length);
				dataStream.Close();
			}
			catch(Exception e)
			{
				tty("HttpPost failed: " + e.ToString());
				return null;
			}
			try
			{
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
				XDocument ret = XDocument.Load(response.GetResponseStream());
                if(ret == null)
                {
					tty("HttpPost: Got null doc with status code " + response.StatusCode + " and response: \n" +
                        response.GetResponseStream().ToString());
                }
				return ret;
			}
			catch(WebException e)
			{
				tty("HttpPost failed: " + e.ToString());

				Stream responseStream = e.Response.GetResponseStream();
				StreamReader reader = new StreamReader(responseStream);
				string responseFromServer = reader.ReadToEnd();
				tty("Response: " + responseFromServer);

				ms.Position = 0;
				reader = new StreamReader(ms);
				string requestString = reader.ReadToEnd();
				tty("Request: " + requestString);
				return null;
			}
			catch(Exception e)
			{
				tty("HttpPost failed: " + e.ToString());
				return null;
			}
		}

		////////////////////////////////////////////////////////////////////////
		// LookupUserId
		////////////////////////////////////////////////////////////////////////
		private static string LookupUserId(Config config, string uri, string username)
		{
            var doc = HttpGet(uri);
			if(doc == null)
			{
				Trace.WriteLine("Failed to look up user ID for " + username);
				return null;
			}
            try
            {
                // Need to go through this mess because usernames are not case sensitive
                IEnumerable<XElement> users = doc.Root.Elements();
                foreach (XElement user in users)
                {
                    string thisUsername = user.Element("username").Value;
                    if (String.Equals(thisUsername, username, StringComparison.OrdinalIgnoreCase))
                    {
                        return user.Element("id").Value;
                    }
                }
            }
            catch (Exception e)
            {
                Trace.WriteLine(e.ToString());

                FileStream fileStream = new FileStream("X:\\RTMError.xml", FileMode.Create);
                doc.Save(fileStream);
                fileStream.Close();
                s_LastError = "Failed to get ID for user " + username + ". See X:\\RTMError.xml.";
                return null;
            }
			return null;
		}

		////////////////////////////////////////////////////////////////////////
		// LookupUserId
		////////////////////////////////////////////////////////////////////////
		private static string LookupUserId(Config config)
		{
			// There is also
			// config.RestServer+"Projects/"+config.ProjectId.ToString()+"/CurrentUser"
			// that could be used, but may as well share the LookupUserId
			// function here.
			var uri = config.RestServer+"Projects/"+config.ProjectId.ToString()+"/Users";
			return LookupUserId(config, uri, s_Username);
		}

		////////////////////////////////////////////////////////////////////////
		// CheckValidBugCallback
		////////////////////////////////////////////////////////////////////////
		private static bool CheckValidBugCallback(Config config, string bug)
		{
			var uri = config.RestServer+"Projects/"+config.ProjectId.ToString()+"/Bugs/"+bug;
			return HttpGet(uri) != null;
		}

		////////////////////////////////////////////////////////////////////////
		// LoginCallback
		////////////////////////////////////////////////////////////////////////
		private static bool LoginCallback(Config config, string description, BugstarUI ui, string username, string password)
		{
			var authStr = username+"@rockstar.t2.corp:"+password;
			byte[] authBytes = Encoding.UTF8.GetBytes(authStr);
			s_Authorization = "Basic "+Convert.ToBase64String(authBytes);
			s_Username = username;
			var userId = LookupUserId(config);
			s_IsLoggedIn = (userId != null);
			return s_IsLoggedIn;
		}

		////////////////////////////////////////////////////////////////////////
		// ExtractCallstack
		////////////////////////////////////////////////////////////////////////
		public static string[] ExtractCallstack(string description)
		{
			var stack = new List<string>();

			// Format for line of callstack is "address - functionname(args)+offset".
			// Want to extract just the function name for matching.
			// On PS3, function name can start with "[local to filename]::", which we want to strip off.
			var regex = new Regex(
				@"^"+

				// Try matching callstacks generated by R*TM or an old-style exception handler
				@"("+                           // $1
					@"\s*"+
					@"(\[[0-9;]*m)?"+           // $2       // optional ANSI "select graphics rendition"
					@"(\x1b\[[0-9;]*m)?"+       // $3       // optional ANSI "select graphics rendition" + escape character
					@"\s*"+
					@"(0x)?[0-9a-fA-F]+"+       // $4       // address
					@" - "+
					@"(\[.*\]::)?"+             // $5       // optional "[local to filename]::"
					@"([^[\](<]+)"+             // $6       // function name up to first '<' or '('.  must not include '[' or ']'
					@".*"+                                  // generally this will then be followed by template args, function args and method quantifiers,
					                                        // but if this string gets too long it will be truncated.  simplest to just match all here.
					                                        // on PC this will also match the optional line number.
					@"(\+(0x)?[0-9]+)?"+        // $7,$8    // optional offset from function start (not present on PC)
					@"(\[[0-9;]*m)?"+           // $9       // optional ANSI "select graphics rendition"
				@")"+

				// Try matching callstacks copied from Visual Studio
				@"|("+                          // $10
					@"[> ]\t"+                              // callstack level selector
					@"[^! \t]+!"+                           // executable name followed by an exclamation mark
					@"([^(<]+)"+                // $11      // function name up to first '<' or '('
					@".*\)"+                                // function signature closing parenthesis
					@"( Line [0-9]+)?"+         // $11      // optional line number
					@"( \+ [0-9]+ bytes)?"+     // $12      // optional offset
					@"\tC\+\+"+                             // C++
				@")"+

				// Else replace everything with an empty string
				@"|.*$");

			// Functions to strip out of stack
			string[] ignore = {
				"",             // was not a stack line
				"notfound",     // symbol was not found in the .cmp symbol file

				// Some really common functions that we don't want to effect the
				// stack matching metric (and we don't want to be used as the
				// initial B* database search if they are the top of the stack).
				"rage::sysTmCmdQuitf",
				"rage::Quitf",

				// Artefacts of the 360 testkit exception handler
				"rage::sysStack::ExceptionFilter",
				"UnhandledExceptionFilter",
				"XapiThreadStartup",
			};

			foreach(var line in description.Split('\n'))
			{
				var matched = regex.Replace(line, "$6$11");
				if(!Array.Exists(ignore, str => str==matched))
				{
					stack.Add(matched);
				}
			}
			return stack.ToArray();
		}

		////////////////////////////////////////////////////////////////////////
		// CompareStacks
		////////////////////////////////////////////////////////////////////////
		public static int CompareStacks(string[] stk0, string[] stk1)
		{
            // Find the last entry in stk0 that exists in stk1
            int idx0 = stk0.Length-1;
            int idx1 = stk1.Length-1;
            int score = 0;
            while (idx0 >= 0 && idx1 >= 0)
            {
                bool found = false;
                for (int i = idx0; i >= 0; --i)
                {
                    for (int j = idx1; j >= 0; --j)
                    {
                        if (stk0[i] == stk1[j])
                        {
                            found = true;
                            idx0 = i;
                            idx1 = j;
                            break;
                        }
                    }
                    if (found)
                    {
                        break;
                    }
                }
                if (!found)
                {
                    break;
                }

                // Count the number of successive common stack frames
                int successiveFrames = 0;
                while(idx0 >= 0 && idx1 >= 0)
                {
                    if (stk0[idx0] == stk1[idx1])
                    {
                        --idx0;
                        --idx1;
                        ++successiveFrames;
                    }
                    else
                    {
                        break;
                    }
                }

                score += successiveFrames;
            }

            return score;
		}

		////////////////////////////////////////////////////////////////////////
		// SuggestDuplicate
		////////////////////////////////////////////////////////////////////////
		private static void SuggestDuplicate(Config config, string newBugDescription, BugstarUI ui, VoidCallbackString tty)
		{
			// This duplicate check is really still quite experimental, there is
			// almost certainly better ways to do this.

			// Extract callstack from new bug description.
			string[] newBugStack = ExtractCallstack(newBugDescription);
			if(newBugStack.Length <= 0)
			{
				return;
			}

			// Use function at top of stack for searching the bug data base.
			var crashFunc = newBugStack[0];
			if(crashFunc == "")
			{
				return;
			}

			// Get matching bugs.
			var bugIds = new List<UInt32>();
			var bugListDoc = new XDocument(
				new XDeclaration("1.0", "utf-8", "yes"),
				new XElement("Search",
					new XAttribute("query", "description:\""+crashFunc+"\""),
					new XAttribute("countOnly", "0")));
			var uri = config.RestServer+"Projects/"+config.ProjectId.ToString()+"/FulltextSearch";
			bugListDoc = HttpPost(uri, bugListDoc, tty);
			if (bugListDoc == null)
			{
				return;
			}
			foreach(var bugElem in bugListDoc.Root.XPathSelectElements("/Search/bug"))
			{
				var idAttrib = bugElem.Attribute("id");
				if(idAttrib != null)
				{
					var idStr = idAttrib.Value;
					if(idStr != null)
					{
						UInt32 idVal;
						if(UInt32.TryParse(idStr, out idVal))
						{
							bugIds.Add(idVal);
						}
					}
				}
			}

			// Sort ids so newest bug is checked first (and therefore is the
			// preferred duplicate when otherwise a tie).
			bugIds.Sort(delegate(UInt32 x, UInt32 y){return (int)(y-x);});

			// Find the closest matching bug
			UInt32 closestBugId = 0;
			int closestBugScore = Math.Min(3, newBugStack.Length) - 1; // -1 since we need better than this score to consider as a dup
			int totalNumBugs = bugIds.Count;
			int processedBugs = 0;
			DateTime thresholdTime = DateTime.Now.AddMonths(-1);
			foreach(var bugId in bugIds)
			{
				var bugUri = config.RestServer+"Projects/"+config.ProjectId.ToString()+"/Bugs/"+bugId.ToString();
				var bugDoc = HttpGet(bugUri);
				if(bugDoc != null)
				{
					// Ignore bugs more than a month old
					bool skipCheck = false;
					var lastModifiedElem = bugDoc.Root.XPathSelectElement("/bug/lastModifiedOn");
					if (lastModifiedElem != null)
					{
						DateTime dt = Convert.ToDateTime(lastModifiedElem.Value);
						if (dt < thresholdTime)
						{
							skipCheck = true;
						}
					}

					var descriptionElem = bugDoc.Root.XPathSelectElement("/bug/description");
					if(descriptionElem != null && !skipCheck)
					{
						var existingBugDescription = descriptionElem.Value;
						if(existingBugDescription != null)
						{
							var existingBugStack = ExtractCallstack(existingBugDescription);
							int score = CompareStacks(newBugStack, existingBugStack);
							if(closestBugScore < score)
							{
								closestBugScore = score;
								closestBugId = bugId;
							}
						}
					}
				}

				// Update progress indicator, and abort the search if requested by the ui.
				++processedBugs;
				if(ui.DuplicateSearchProgressUpdate(processedBugs, totalNumBugs, closestBugId!=0?closestBugId.ToString():null))
				{
					break;
				}
			}
		}

		////////////////////////////////////////////////////////////////////////
		// SanitizeString
		////////////////////////////////////////////////////////////////////////
		public static string SanitizeString(string src)
		{
			// Strip ANSI colour codes
			var regex = new Regex(@"(\x1b\[[0-9;]*m)");
			string strippedSrc = regex.Replace(src, "");

			// Convert non-XML characters to _
			var dst = new char[strippedSrc.Length];
			for(int i=0; i<strippedSrc.Length; ++i)
			{
				dst[i] = XmlConvert.IsXmlChar(strippedSrc[i]) ? strippedSrc[i] : '_';
			}
			return new string(dst);
		}

		////////////////////////////////////////////////////////////////////////
		// RaiseBug
		////////////////////////////////////////////////////////////////////////
		public static UInt32 RaiseBug(Target target, Config config, string summary, string description, bool enableBugDupCheck, VoidCallbackString tty)
		{
			summary     = SanitizeString(summary);
			description = SanitizeString(description);

			// Check if the user wants to create a new bug, add to an existing
			// one, or bail out.
			BugstarUI.CheckValidBug checkValidBug = delegate(string bug)
			{
				return CheckValidBugCallback(config, bug);
			};
			BugstarUI.TryLogin tryLogin = delegate(BugstarUI ui, string username, string password)
			{
				return LoginCallback(config, description, ui, username, password);
			};
			var dialog = new BugstarUI(target.Name+": "+summary, checkValidBug, tryLogin);
			if(s_IsLoggedIn)
			{
				dialog.SetLoginDone();
			}

			var dialogThread = dialog.AsyncShowDialog("Bugstar Dialog");

			// Wait until either logged into Bugstar.
			while(!dialog.IsLoggedIn)
			{
				if(dialog.IsCancelled)
				{
					dialogThread.Join();
					return 0;
				}
				Thread.Sleep(10);
			}

			// Do the duplicate bug search.
			if(enableBugDupCheck)
			{
				SuggestDuplicate(config, description, dialog, tty);
			}
			dialog.DuplicateSearchDone();

			// Wait for user to create a new bug.
			while(!dialog.IsDone)
			{
				if(dialog.IsCancelled)
				{
					dialogThread.Join();
					return 0;
				}
				Thread.Sleep(10);
			}

			// Worker thread should have now completed.
			dialogThread.Join();

			if(!dialog.NewBug)
			{
				return dialog.ExistingBugNumber;
			}

			string userId = LookupUserId(config);
			if(userId == null)
			{
				tty("Bug creation failed(1) for user ID " + userId);
				return 0;
			}

			// Lookup the *Default Code* user id so that the bug can be assigned
			string developersUri = config.RestServer+"Projects/"+config.ProjectId.ToString()+"/Developers";
			string defaultCodeUserId = LookupUserId(config, developersUri, "*Default Code*");
			if(defaultCodeUserId == null)
			{
				tty("Bug creation failed(2) for user ID " + userId);
				return 0;
			}

			// Create basic bug structure
			var bugElem = new XElement("bug",
				new XElement("summary", summary),
				new XElement("description", description),
				new XElement("developer", defaultCodeUserId),
				new XElement("tester", userId),
				new XElement("category", "A"),
				new XElement("state", "DEV"),
				new XElement("priority", "3")
			);

			// If there is a build id string, then lookup the build id number
			// and add that to the bug element.
			if(!String.IsNullOrWhiteSpace(config.BuildId))
			{
				string buildUri = config.RestServer+"Projects/"+config.ProjectId.ToString()+"/Builds";
				var buildsDoc = HttpGet(buildUri);
				if(buildsDoc != null)
				{
					var buildIdXpath = "/builds/builds[buildId='"+config.BuildId+"']/id";
					var idElem = buildsDoc.Root.XPathSelectElement(buildIdXpath);
					if(idElem != null)
					{
						string id = idElem.Value;
						if(id != null)
						{
							bugElem.Add(new XElement("foundIn", id));
						}
					}
				}
			}

			// Create the bug on the server
			var bugDoc = new XDocument(
				new XDeclaration("1.0", "utf-8", "yes"),
				bugElem
			);
			var bugsUri = config.RestServer+"Projects/"+config.ProjectId.ToString()+"/Bugs";
			bugDoc = HttpPost(bugsUri, bugDoc, tty);
			if(bugDoc == null)
			{
				tty("Bug creation failed(3) for user ID " + userId);
				return 0;
			}

			// Parse response to extract bug id
			var bugIdElem = bugDoc.Root.XPathSelectElement("/bug/id");
			if(bugIdElem != null)
			{
				UInt32 bugId = 0;
				if(UInt32.TryParse(bugIdElem.Value, out bugId))
				{
					return bugId;
				}
			}
			tty("Bug creation failed: unable to parse response from server");
			return 0;
		}

        // From http://www.codeproject.com/Articles/10160/Directory-CreateDirectory-method-bug-fixed
        public static void CreateDirectory(string DirectoryPath)
        {
            // Trim leading \ character
            DirectoryPath = DirectoryPath.TrimEnd(Path.DirectorySeparatorChar);
            Scripting.FileSystemObject fso = new Scripting.FileSystemObjectClass();

            // Check if folder exists, if yes - no work to do
            if(!fso.FolderExists(DirectoryPath))
            {
                int i = DirectoryPath.LastIndexOf(Path.DirectorySeparatorChar);
                // Find last\lowest folder name
                string CurrentDirectoryName = DirectoryPath.Substring(i + 1,
                        DirectoryPath.Length - i - 1);
                // Find parent folder of the last folder
                string ParentDirectoryPath = DirectoryPath.Substring(0, i);
                // Recursive calling of function to create all parent folders
                CreateDirectory(ParentDirectoryPath);
                // Create last folder in current path
                Scripting.Folder folder = fso.GetFolder(ParentDirectoryPath);
                folder.SubFolders.Add(CurrentDirectoryName);
            }
        }

		////////////////////////////////////////////////////////////////////////
		// GetZipName
		////////////////////////////////////////////////////////////////////////
		public static string GetZipName(Config config, UInt32 bugId, string commentFormatString, VoidCallbackString ttyOutput)
		{
			Debug.Assert(bugId != 0);

			// Ensure directory exists
			bool serverAccess = true;
			var logDir = Path.Combine(config.LogDirectory, bugId.ToString());
			if(!Directory.Exists(logDir))
			{
				try
				{
					CreateDirectory(logDir);
				}
				catch(Exception e)
				{
					ttyOutput("\nFailed to create directory \""+logDir+"\", "+e.ToString()+"\n");
					if(e is UnauthorizedAccessException)
					{
						ttyOutput("Please request write access from IT.\n");
					}
					logDir = RockstarTargetManager.GetCrashdumpDir();
					ttyOutput("Using \""+logDir+"\" as a fallback.\nZip file will need to be manually copied somewhere accessable to the bug owner.\n\n");
					serverAccess = false;
				}
			}

			// Form zip file name
			var zip = Path.Combine(logDir, bugId.ToString()+DateTime.Now.ToString("-yyyyMMdd-HHmmss")+".zip");

			var userId = LookupUserId(config);
			if(userId == null)
			{
				return null;
			}

			// Add comment to bug, linking to zipfile
			var bugUri = config.RestServer+"Projects/"+config.ProjectId.ToString()+"/Bugs/"+bugId.ToString();
			var doc = HttpGet(bugUri);
			if(doc != null)
			{
                var bugElem = doc.Element("bug");
                if (bugElem == null)
                {
                    return null;
                }

                // Always add user as a CC
				if(doc.Root.XPathSelectElement("/bug/ccList[cc='"+userId+"']") == null)
				{
					var ccListElem = bugElem.Element("ccList");
					if(ccListElem == null)
					{
						ccListElem = new XElement("ccList");
						bugElem.Add(ccListElem);
					}
					ccListElem.Add(new XElement("cc", userId));
				}

                // Increment seen count
                var seenElem = bugElem.Element("seen");
                if (seenElem == null)
                {
                    seenElem = new XElement("seen", "1");
                    bugElem.Add(seenElem);
                }
                else
                {
                    uint seenCount = 0;
                    try
                    {
                        seenCount = Convert.ToUInt32(seenElem.Value);
                        ++seenCount;
                        seenElem.Value = seenCount.ToString();
                    }
                    catch
                    {
                    }
                }

				if(serverAccess)
				{
					// Add the comment
					doc.Root.Add(new XElement("addcomment",
						new XElement("text", String.Format(commentFormatString, zip))));
				}

				// Update bug on server
				if (HttpPost(bugUri, doc, ttyOutput) == null)
				{
					return null;
				}

				return zip;
			}
			return null;
		}
	}
}
