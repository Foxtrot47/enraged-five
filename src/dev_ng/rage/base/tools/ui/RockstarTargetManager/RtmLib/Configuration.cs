using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.IO;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace Rockstar.TargetManager
{
    public class Configuration : IXmlSerializable
    {
        private string m_Name = "";
        private TargetPlatformId m_PlatformId = TargetPlatformId.Unknown;
        private List<DeploymentFile> m_DeploymentFiles = new List<DeploymentFile>();
        private IDictionary<string, string> m_EnvVars = new Dictionary<string, string>();

        public class DeploymentFile
        {
            private string m_Src = null;
            private string m_DstDir = null;

            public DeploymentFile(string src, string dst)
            {
                m_Src = src;
                m_DstDir = dst;
            }

            public string Src
            {
                get{return m_Src;}
            }

            public string DstDir
            {
                get{return m_DstDir;}
            }
        };

        public Configuration()
        {
        }

        public Configuration(string name)
        {
            m_Name = name;

            m_PlatformId = PlatformIdFromName(name);

            /*System.Diagnostics.Debug.Assert(Platform.UNKNOWN != m_Platform,
                                            "Unknown platform: " + name);*/
        }

        public bool SetEnvVar(string key, string value)
        {
            //Trace.WriteLine(String.Format("Setting env var $({0}) = {1}...", key, value));

            m_EnvVars.Add(key, value);
            return true;

            /*bool success = true;
            string tmpVal = this.ExpandEnvVars(value);

            Regex r = new Regex("\\$\\([^\\)]*\\)");
            MatchCollection mc = r.Matches(tmpVal);
            for(int i = 0; i < mc.Count; ++i)
            {
                Trace.TraceError(String.Format("Unresolved env var: \"{0}\" in $({1}) = {2}",
                                mc[i].Value,
                                key,
                                tmpVal));
                success = false;
            }

            //Trace.WriteLine(String.Format("  Set env var $({0}) = {1}", key, tmpVal));

            m_EnvVars.Add(key, tmpVal);

            return success;*/
        }

        public string GetEnvVar(string key)
        {
            string val;
            if(!m_EnvVars.TryGetValue(key, out val))
            {
                val = "";
            }

            return val;
        }

        public string GetExpandedEnvVar(string key)
        {
            return this.ExpandEnvVars(this.GetEnvVar(key));
        }

        public string ExpandEnvVars(string str)
        {
            IDictionary envVars = Environment.GetEnvironmentVariables();
            string result = str;

            bool done = false;

            while(!done) 
            {
                done = true;

                foreach(string key in envVars.Keys)
                {
                    string tmp = String.Format("$({0})", key);
                    int idx = result.IndexOf(tmp, StringComparison.InvariantCultureIgnoreCase);

                    if(idx >= 0)
                    {
                        string old = result.Substring(idx, tmp.Length);
                        result = result.Replace(old, (string) envVars[key]);
                        done = false;
                    }
                }

                foreach(KeyValuePair<string, string> kv in m_EnvVars)
                {
                    string key = String.Format("$({0})", kv.Key);

                    int idx = result.IndexOf(key, StringComparison.InvariantCultureIgnoreCase);

                    if(idx >= 0)
                    {
                        string old = result.Substring(idx, key.Length);
                        result = result.Replace(old, kv.Value);
                        done = false;
                    }
                }
            }

            return result;
        }

        public void DumpEnvVars()
        {
            Trace.WriteLine("Environment Variables");
            foreach(KeyValuePair<string, string> kvp in m_EnvVars)
            {
                Trace.WriteLine("  " + kvp.Key + ": " + this.ExpandEnvVars(kvp.Value));
            }
        }

        public bool SetDeploymentFiles(string deplomentFiles)
        {
            char[] sep = new char[2]{';', '='};
            string[] items = deplomentFiles.Split(sep);

            m_DeploymentFiles.Clear();

            for(int i = 0; i < items.Length; i += 2)
            {
                if(i + 1 < items.Length)
                {
                    //string src = this.ExpandEnvVars(items[i+1]);
                    //string dst = this.ExpandEnvVars(items[i]);
                    //m_DeploymentFiles.Add(new DeploymentFile(src, dst));

                    m_DeploymentFiles.Add(new DeploymentFile(items[i+1], items[i]));
                }
            }

            return true;
        }

        public void WriteXml(XmlWriter writer)
        {
            writer.WriteElementString("Name", m_Name);

            writer.WriteStartElement("EnvVars");
            foreach(KeyValuePair<string, string> kv in m_EnvVars)
            {
                writer.WriteStartElement("EnvVar");
                writer.WriteElementString("key", kv.Key);
                writer.WriteElementString("value", kv.Value);
                writer.WriteEndElement();
            }
            writer.WriteEndElement();

            string dfstr = "";

            foreach(DeploymentFile df in m_DeploymentFiles)
            {
                if(dfstr.Length == 0)
                {
                    dfstr = df.DstDir + "=" + df.Src;
                }
                else
                {
                    dfstr += ";" + df.DstDir + "=" + df.Src;
                }
            }

            writer.WriteElementString("DeployFiles", dfstr);
        }

        public void ReadXml(XmlReader reader)
        {
            reader.Read();

            m_Name = reader.ReadElementString("Name");

            m_PlatformId = PlatformIdFromName(m_Name);

            System.Diagnostics.Debug.Assert(TargetPlatformId.Unknown != m_PlatformId,
                                            "Unknown platform: " + m_Name);

            m_EnvVars.Clear();

            reader.ReadStartElement("EnvVars");
            while(reader.IsStartElement())
            {
                reader.ReadStartElement("EnvVar");
                string key = reader.ReadElementString("key");
                string value = reader.ReadElementString("value");
                m_EnvVars.Add(key, value);
                reader.ReadEndElement();    //EnvVar
            }
            reader.ReadEndElement();    //EnvVars

            string df = reader.ReadElementString("DeployFiles");

            this.SetDeploymentFiles(df);

            reader.ReadEndElement();    //Configuration
        }

        public XmlSchema GetSchema()
        {
            return null;
        }

        public string Name
        {
            get{return m_Name;}
        }

        public bool IsValid
        {
            get
            {
                return !string.IsNullOrEmpty(Name)
                        && TargetPlatformId.Unknown != m_PlatformId;
            }
        }

        public TargetPlatformId PlatformId
        {
            get{return m_PlatformId;}
        }

        public IEnumerable<DeploymentFile> DeploymentFiles
        {
            get{return m_DeploymentFiles;}
        }

        public static string PlatformNameFromId(TargetPlatformId id)
		{
			string name = null;

			if(id == TargetPlatformId.PS3)
			{
				name = "PS3";
			}
			else if(id == TargetPlatformId.PS4)
			{
				name = "PS4";
			}
			else if(id == TargetPlatformId.Xbox360)
			{
				name = "Xbox 360";
			}
			else if(id == TargetPlatformId.XB1)
			{
				name = "Xbox One";
			}
			else if(id == TargetPlatformId.PC)
			{
				name = "PC";
			}

			return name;
		}

        public static TargetPlatformId PlatformIdFromName(string name)
        {
            TargetPlatformId platformId = TargetPlatformId.Unknown;
			if (name != null)
			{
				if(name.Contains("PS3"))
				{
					platformId = TargetPlatformId.PS3;
				}
				else if(name.Contains("PS4") || name.Contains("ORBIS"))
				{
					platformId = TargetPlatformId.PS4;
				}
				else if(name.Contains("Xbox 360"))
				{
					platformId = TargetPlatformId.Xbox360;
				}
				else if(name.Contains("Xbox One") || name.Contains("Durango"))
				{
					platformId = TargetPlatformId.XB1;
				}
				else if(name.Contains("Xbox"))  // backwards compatability for workspaces created pre 1.0.1.6
				{
					platformId = TargetPlatformId.Xbox360;
				}
				else if(name.Contains("PC") || name.Contains("x64"))
				{
					platformId = TargetPlatformId.PC;
				}
			}
            return platformId;
        }
    }
}
