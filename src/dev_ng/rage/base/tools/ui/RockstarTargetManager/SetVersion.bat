@echo off
setlocal enabledelayedexpansion

cd /d %~dp0

if "%1" == "--help" (
	echo.usage %~nx0 [version_number]
	goto :EOF
)

set version=%1
set tmpFile="%TEMP%\RtmSetVersion"

if "x%version%" == "x" (
	REM :: Extract R*TM version number out of source code.
	REM :: http://stackoverflow.com/questions/7516064/escaping-double-quote-in-delims-option-of-for-f
	REM :: Why ^"^" instead of just ^"?  Because having just one " confuses Emacs syntax highlighting.
	set version=
	set assembly_cs="RockstarTargetManager\Properties\AssemblyInfo.cs"
	for /F usebackq^ tokens^=2^ delims^=^"^" %%i in (`findstr "^\[assembly:\ AssemblyVersion" !assembly_cs!`) do set version=%%i
	if "x!version!" == "x" (
		echo.Unable to extract R*TM version number out of !assembly_cs!
		goto :EOF
	)
	for /F "usebackq tokens=1-4 delims=." %%a in (`echo !version!`) do (
		set /A D=%%d+1
		set version=%%a.%%b.%%c.!D!
	)
)

:: Put Windows executables in front of path to make sure that version of find and ping are used
:: (as opposed to other versions like from Cygwin).
set PATH=%ComSpec:cmd.exe=%;%PATH%

:: Clear PWD for people running from Cygwin Bash.
set PWD=


p4 change -o | findstr /v /b /c:"	" | findstr /v "^Files:" | findstr /v "^Description:" > %tmpFile%
echo.Description:>>                     %tmpFile%
echo.	R*TM version bump, %version%.>> %tmpFile%
for /F "usebackq tokens=2" %%i in (`p4 change -i ^< %tmpFile%`) do set changelist=%%i

for /F "usebackq" %%i in (`dir /b /s AssemblyInfo.cs`) do call :PatchFile %%i
call :PatchFile ..\..\libs\LogJam\Properties\AssemblyInfo.cs

goto :EOF


:PatchFile
set file=%1
p4 edit -c %changelist% %file%
type %file% | findstr /v "^\[assembly:.Assembly.*Version(.*)\]" > %tmpFile%
echo.[assembly: AssemblyVersion("%version%")]>>     %tmpFile%
echo.[assembly: AssemblyFileVersion("%version%")]>> %tmpFile%
move %tmpFile% %file%
goto :EOF
