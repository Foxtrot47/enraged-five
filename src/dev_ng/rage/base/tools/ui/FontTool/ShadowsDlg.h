#if !defined(AFX_SHADOWSDLG_H__A4036B2F_B3EE_4156_84E8_7A595332A4B3__INCLUDED_)
#define AFX_SHADOWSDLG_H__A4036B2F_B3EE_4156_84E8_7A595332A4B3__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ShadowsDlg.h : header file
//

namespace rage {

/////////////////////////////////////////////////////////////////////////////
// ShadowsDlg dialog

class ShadowsDlg : public CDialog
{
// Construction
public:
	ShadowsDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(ShadowsDlg)
	enum { IDD = IDD_SHADOWS };
	CSliderCtrl	m_ControlAlpha;
	CSliderCtrl	m_ControlY2;
	CSliderCtrl	m_ControlY1;
	CSliderCtrl	m_ControlX2;
	CSliderCtrl	m_ControlX1;
	int		m_ShadowX1;
	int		m_ShadowX2;
	int		m_ShadowY1;
	int		m_ShadowY2;
	int		m_ShadowAlpha;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(ShadowsDlg)
	public:
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

public:
// Accessors
	float GetAlpha() const			{return 1.f-m_ShadowAlpha/255.f;}

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(ShadowsDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnApply();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

} // namespace rage

#endif // !defined(AFX_SHADOWSDLG_H__A4036B2F_B3EE_4156_84E8_7A595332A4B3__INCLUDED_)
