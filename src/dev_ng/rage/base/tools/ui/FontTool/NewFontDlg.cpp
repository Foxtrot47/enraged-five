// NewFontDlg.cpp : implementation file
//

#include "stdafx.h"
#include "FontTool.h"
#include "NewFontDlg.h"

namespace rage {

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// NewFontDlg dialog


NewFontDlg::NewFontDlg(CWnd* pParent /*=NULL*/)
	: CDialog(NewFontDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(NewFontDlg)
	m_Height = 760;
	m_Width = 800;
	m_FontName = _T("");
	//}}AFX_DATA_INIT
}


void NewFontDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(NewFontDlg)
	DDX_Text(pDX, IDC_HEIGHT, m_Height);
	DDV_MinMaxInt(pDX, m_Height, 2, 3200);
	DDX_Text(pDX, IDC_WIDTH, m_Width);
	DDV_MinMaxInt(pDX, m_Width, 2, 800);
	DDX_Text(pDX, IDC_FONTNAME, m_FontName);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(NewFontDlg, CDialog)
	//{{AFX_MSG_MAP(NewFontDlg)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// NewFontDlg message handlers

} // namespace rage
