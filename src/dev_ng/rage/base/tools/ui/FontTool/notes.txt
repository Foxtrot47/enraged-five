
--- High Priority ---

What's up with freeing textures sometimes crashing Windows 2000??



--- Task List ---

Add dummy localization language

Add support for multiple font variants (animating fonts)

Search for HACKs

Is string table's width parameter working yet?


--- Bugs ---

I'm calling txtFreeTexture() in txtFontTex::SetTexture() but the FontTool doesn't allocate
textures using txtGetTexture().

Should print a warning if desktop is in only 16-bit mode?

'New' should clear out all glyphs and the already generated font

Draw position is inaccurate, relative to DrawExtents and GetExtents

Should 'Draw All' button ask for confirmation?

Should 'Slice' button ask for confirmation?

Should 'Shrink All' button ask for confirmation?

One-by-one pixel characters are illegal (!IsValid())

In 'Load As...' and 'Save As...' when switching from *.fontproj to *.fonttex, the extension on the
filename listed should change as well.

Various Errorf's and Warningf's should be AfxMessageBox's


--- Wish List ---

Add 'Clear Glyph' button (draw character 32 - space)

Merge some functionality of txtFontTex::Draw() with PIPE.BlitText() for optimization

Better file revision management
- Each chunk in the file should store a length for forward compatibility
- Save GDI font info in .fontproj

Global options screen

Option for rendering fullwidth characters as normal characters

Option for rendering fullwidth characters for normal characters

Stringtable should have column showing which file(s) the entry ends up in

Stringtable should be stored in a versatile database format while in the FontTool

Stringtable should have feature for removing wide characters

Better browsing of stringtable while in FontTool

'Use String Table' option should have a

Add a visible option for adding boxes to 

Color code unassigned glyph boxes

Don't print the warning when some glyphs are unassigned and you hit 'Generate' or 'Save'

Standalone stringtable browser/converter

Implement txtContext, containing cursor position and more flags/parameters

When loading a .tga file, shouldn't it warn you if a .fontproj already exists?

Unicode Character box should accept input

Add alphabet dialog box (non-modal) for listing characters still needed

Character width is off by 1 from what humans expect

Reimplement using Single Document Interface

Use Property Sheets for stringtable/font/glyph?

Add support for kerning

Format for stringtable.strtbl file is kinda weak; should possibly have all languages in one file?

Scrollable and zoomable edit window!

Implement txtFontGDI and txtStringGDI

Size of output textures should be adjustable













--- Fonts for SR ---

12 Arial Black

14 Arial Black

20 Arial Narrow

\\zen\projects\sr\Shop\frontend\fonts

Ok, I've zipped up the font's we're using in the UI.  We are using the
WaterTower font, and the interactive interface should be 16 pixels high,
which is about 23points high in Illustrator.  -dm



--- Fonts for MC ---

20 Serpentine Sans Bold

15 Serpentine Sans Bold

13 Microscan A

15 Microscan A

12 Microscan
- 12 Pixels high Upper Case
- 11 Pixels high lower case

16 Supertouch

12 Supertouch

8 Supertouch

See \\zen\projects\mc\art\PR\UI_comp_screens

\\zen\projects\mc\Shop\frontend\fonts








