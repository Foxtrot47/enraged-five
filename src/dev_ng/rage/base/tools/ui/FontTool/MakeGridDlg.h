#if !defined(AFX_MAKEGRIDDLG_H__556B1BB3_8AE4_4A7F_8889_03EB16D4D434__INCLUDED_)
#define AFX_MAKEGRIDDLG_H__556B1BB3_8AE4_4A7F_8889_03EB16D4D434__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// MakeGridDlg.h : header file
//

namespace rage {

/////////////////////////////////////////////////////////////////////////////
// MakeGridDlg dialog

class MakeGridDlg : public CDialog
{
// Construction
public:
	MakeGridDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(MakeGridDlg)
	enum { IDD = IDD_MAKEGRID_DIALOG };
	CButton	m_RegenerateAlphabetButton;
	CButton	m_AddEmptyGlyphsButton;
	CComboBox	m_StringTableCombo;
	CSpinButtonCtrl	m_GridBaselineSpin;
	CSpinButtonCtrl	m_GridWidthSpin;
	CSpinButtonCtrl	m_GridHeightSpin;
	int		m_GridHeight;
	int		m_GridWidth;
	int		m_GridBaseline;
	BOOL	m_AllLowers;
	BOOL	m_AllNumbers;
	BOOL	m_AllSymbols;
	BOOL	m_AllUppers;
	BOOL	m_AllCommonSymbols;
	BOOL	m_EuroLowers;
	BOOL	m_EuroSymbols;
	BOOL	m_EuroUppers;
	BOOL	m_MoreSymbols;
	BOOL	m_UseAllStringTable;
	BOOL	m_UseSomeStringTable;
	CString	m_MatchFont;
	//}}AFX_DATA

	void UpdateDialog();

	int GetFlags() const;

	void MakeGridDlg::Serialize(CArchive &archive);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(MakeGridDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(MakeGridDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnAddEmptyGlyphs();
	afx_msg void OnRegenerateAlphabet();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

} // namespace rage

#endif // !defined(AFX_MAKEGRIDDLG_H__556B1BB3_8AE4_4A7F_8889_03EB16D4D434__INCLUDED_)
