// FontToolDlg.h : header file
//

#if !defined(AFX_TESTDLG_H__8DC30888_979D_4372_8A41_5FC404C4119D__INCLUDED_)
#define AFX_TESTDLG_H__8DC30888_979D_4372_8A41_5FC404C4119D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Glyph.h"
#include "ShadowsDlg.h"
#include "MakeGridDlg.h"
#include "texttools/stringeditor.h"
#include "text/language.h"
#include "vector/matrix44.h"
#include "obsolete/hash.h"
#include "afxwin.h"

namespace rage {

class txtEditString;
class txtFontTex;
class Glyph;
class StringEntry;
class txtStringTable;
class devPicker;
class bkBank;
class gfxImage;
class gfxBitmap;

/////////////////////////////////////////////////////////////////////////////
// CFontToolDlg dialog

class CFontToolDlg : public CDialog
{
// Construction
public:
	CFontToolDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CFontToolDlg)
	enum { IDD = IDD_TEST_DIALOG };
	CSpinButtonCtrl	m_GlyphBaselineSpin;
	CSpinButtonCtrl	m_GlyphWidthSpin;
	CSpinButtonCtrl	m_GlyphTopSpin;
	CSpinButtonCtrl	m_GlyphRightSpin;
	CSpinButtonCtrl	m_GlyphLeftSpin;
	CSpinButtonCtrl	m_GlyphBottomSpin;
	CSpinButtonCtrl	m_SpaceWidthSpin;
	CSpinButtonCtrl	m_CharSpacingSpin;
	CSpinButtonCtrl	m_CharHeightSpin;
	CScrollBar	m_StringScrollBar;
	CEdit	m_UnicodeValueCtrl;
	CEdit	m_UnicodeTextCtrl;
	CSpinButtonCtrl	m_UnicodeSpin;
	CEdit	m_StringText;
	CEdit	m_StringFont;
	CEdit	m_StringEnglish;
	CEdit	m_StringIdentifier;
	CComboBox	m_Language;
	CStatic	m_Viewport;
	CButton	m_Running;
	int		m_CharHeight;
	int		m_CharSpacing;
	int		m_SpaceWidth;
	int		m_UnicodeValue;
	CString	m_UnicodeText;
	int		m_GlyphBottom;
	int		m_GlyphLeft;
	int		m_GlyphRight;
	int		m_GlyphTop;
	int		m_GlyphWidth;
	int		m_GlyphBaseline;
	BOOL	m_4BitTextures;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CFontToolDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	virtual BOOL OnNotify(WPARAM wParam, LPARAM lParam, LRESULT* pResult);
	virtual void PostNcDestroy();
	//}}AFX_VIRTUAL

public:

	void Init();
	void InitFontEditor();

	void Update();
	void UpdateFontEditor();

	void Draw();
	void DrawFontEditor();

	void Redraw();

	// String Editor //

	void UpdateStringWidgets();
	void DisableStringWidgets();

	// Test Text //

	void DrawTestText();

	// Manipulators //

	void MakeGrid(int charWidth,int charHeight,int baseline,int alphabetFlags,bool addEmpty,const char *matchFont);

	bool GenerateFont();
	void GenerateShadows();
	void UpdateFontParameters();
	void ChangedFontParameters();
	void SaveFont(const char *filename);

	// Accessors //

	gfxImage *GetImage() const				{return mImage;}

	// Font Editor //

	void Serialize(CArchive &archive);

	void LoadFontProject();					// uses mFontName
	void SaveFontProject();					//
	void LoadFontImage();					//
	void SaveFontImage();					//

	void KillGlyphs();
	int KillUnusedGlyphs();					// returns number deleted
	void SetFontName(CString name);
	void ResizeAll();
	void ShrinkRectangle(Glyph *bmpRect);
	void ExpandRectangle(Glyph *bmpRect);
	void Slice();
	unsigned int GetPixel( int i, int j );
	bool IsForeground( int i, int j );
	void RecursePixels(int i, int j, Glyph * rect);
	Glyph *GetGlyphFromXY(int i,int j);

	bool TestClick(const CPoint &point,CPoint *relativePoint,Glyph **glyph);
	void AddMatchFonts(CComboBox &combo);

	// Widgets //

	void NewCurrentGlyph();
	void ChangedEditGlyph();
	void EnableFontWidgets();

// Implementation
protected:
	void OnVScrollViewport(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);

	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CFontToolDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnRunning();
	afx_msg void OnViewport();
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnNewFont();
	afx_msg void OnMakeGrid();
	afx_msg void OnResizeAll();
	afx_msg void OnLoadFont();
	afx_msg void OnSaveFont();
	afx_msg void OnExportFont();
	afx_msg void OnGenerateFont();
	afx_msg void OnLoadStringTable();
	afx_msg void OnSaveStringTable();
	afx_msg void OnEditChangeLanguage();
	afx_msg void OnDrawGlyph();
	afx_msg void OnDeleteGlyph();
	afx_msg void OnShrinkGlyph();
	afx_msg void OnDrawAll();
	afx_msg void OnPickFont();
	afx_msg void OnChangeUnicodeText();
	afx_msg void OnChangeUnicodeValue();
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnSelChangeLanguage();
	afx_msg void OnChangeGlyph();
	afx_msg void OnChangeFontParameter();
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnGlyphPopupDelete();
	afx_msg void OnGlyphPopupShrink();
	afx_msg void OnGlyphPopupExpand();
	afx_msg void OnSlice();
	afx_msg void OnExit();
	afx_msg void OnOk();
	afx_msg void OnCancel();
	afx_msg void OnExpandGlyph();
	afx_msg void OnShadows();
	afx_msg void OnResizeGlyph();
	afx_msg void OnGlyphPopupResize();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	// Dialogs //

	MakeGridDlg m_MakeGridDlg;
//	NewFontDlg m_NewFontDlg;				// this would be a good idea

	// String Editor //

	txtStringEditor *mStringEditor;

	txtEditString *m_CurrentString;

	// Image //

	CString mFontName;
	gfxImage *mImage;
	gfxBitmap *mBitmap;
	int mImageHeight,mImageWidth;
	bool mWreckedAlpha;						// true=>alpha channel derived from RGA channel threshold

	// Glyphs //

	CList<Glyph*,Glyph*> mGlyphList;
	Glyph *m_CurrentGlyph;

	// Shadows //

	ShadowsDlg m_ShadowsDlg;

	// From FontSlicer //

	unsigned int mBackgroundColor;

	// Windows Font //

	LOGFONT m_LogFont;
	COLORREF m_crText;
	CFont *m_Font;

	// Generated Font //

	txtFontTex *mFont;

	int		Version; // .fontproj file version

	CScrollBar SB_Horz;
	CScrollBar SB_Vert;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

} // namespace rage

#endif // !defined(AFX_TESTDLG_H__8DC30888_979D_4372_8A41_5FC404C4119D__INCLUDED_)
