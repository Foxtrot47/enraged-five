set TESTERS=FontTool
set TESTER_BASE=mfc
set BASE_LIBS=%RAGE_CORE_LIBS% %RAGE_GFX_LIBS%
set BASE_LIBS=%BASE_LIBS% text texttools
set LIBS=%BASE_LIBS% 
set XPROJ=%RAGE_DIR%\base\src %RAGE_DIR%\base\tools\cli
set FILES=AboutDlg Alphabet GeneratingTexturesDlg Glyph MakeGridDlg NewFontDlg ShadowsDlg StdAfx FontToolDlg
set CODEONLY=savefont
