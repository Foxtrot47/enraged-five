// FontTool.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "FontTool.h"
#include "FontToolDlg.h"

//#include "gfx/simple.h"
#include "diag/output.h"

using namespace rage;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CFontToolApp

BEGIN_MESSAGE_MAP(CFontToolApp, CWinApp)
	//{{AFX_MSG_MAP(CFontToolApp)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG
	ON_COMMAND(ID_HELP, CWinApp::OnHelp)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CFontToolApp construction

CFontToolApp::CFontToolApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CFontToolApp object

CFontToolApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CFontToolApp initialization

// This should ideally output to an MFC window of some sort

static void myPrintString(const char *s)
{
	printf("%s",s);
}

BOOL CFontToolApp::InitInstance()
{
	AfxEnableControlContainer();

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	//  of your final executable, you should remove from the following
	//  the specific initialization routines you do not need.

// VS.2003 is 1310
#if _MSC_VER < 1310
#ifdef _AFXDLL
	Enable3dControls();			// Call this when using MFC in a shared DLL
#else
	Enable3dControlsStatic();	// Call this when linking to MFC statically
#endif
#endif

	extern void (*PrintString)(const char*);			// defined in core/output.cpp
	PrintString=myPrintString;

	CCommandLineInfo cmdInfo;
	ParseCommandLine(cmdInfo);

	USES_CONVERSION;
	Displayf("filename='%s'",W2A(LPCTSTR(cmdInfo.m_strFileName)));

	CFontToolDlg dlg;
	m_pMainWnd = &dlg;

	dlg.SetFontName(cmdInfo.m_strFileName);

	int nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with OK
	}
	else if (nResponse == IDCANCEL)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with Cancel
	}

	//ageEndGfx();

	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	return FALSE;
}
