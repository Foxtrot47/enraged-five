// ShadowsDlg.cpp : implementation file
//

#include "stdafx.h"
#include "FontTool.h"
#include "ShadowsDlg.h"
#include "FontToolDlg.h"
#include "diag/output.h"

namespace rage {

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// ShadowsDlg dialog


ShadowsDlg::ShadowsDlg(CWnd* pParent /*=NULL*/)
	: CDialog(ShadowsDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(ShadowsDlg)
	m_ShadowX1 = 0;
	m_ShadowX2 = 0;
	m_ShadowY1 = 0;
	m_ShadowY2 = 0;
	m_ShadowAlpha = 0;
	//}}AFX_DATA_INIT
}


void ShadowsDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(ShadowsDlg)
	DDX_Control(pDX, IDC_SHADOWALPHA, m_ControlAlpha);
	DDX_Control(pDX, IDC_SHADOWY2, m_ControlY2);
	DDX_Control(pDX, IDC_SHADOWY1, m_ControlY1);
	DDX_Control(pDX, IDC_SHADOWX2, m_ControlX2);
	DDX_Control(pDX, IDC_SHADOWX1, m_ControlX1);
	DDX_Slider(pDX, IDC_SHADOWX1, m_ShadowX1);
	DDX_Slider(pDX, IDC_SHADOWX2, m_ShadowX2);
	DDX_Slider(pDX, IDC_SHADOWY1, m_ShadowY1);
	DDX_Slider(pDX, IDC_SHADOWY2, m_ShadowY2);
	DDX_Slider(pDX, IDC_SHADOWALPHA, m_ShadowAlpha);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(ShadowsDlg, CDialog)
	//{{AFX_MSG_MAP(ShadowsDlg)
	ON_BN_CLICKED(IDC_APPLY, OnApply)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// ShadowsDlg message handlers

BOOL ShadowsDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	m_ControlAlpha.SetRange(0,255,true);
	m_ControlAlpha.SetTicFreq(16);

	m_ControlX1.SetRange(-5,5,true);
	m_ControlX2.SetRange(-5,5,true);
	m_ControlY1.SetRange(-5,5,true);
	m_ControlY2.SetRange(-5,5,true);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void ShadowsDlg::OnApply() 
{
	UpdateData(true);

	CFontToolDlg *fontToolDlg=(CFontToolDlg*)GetParent();
	fontToolDlg->GenerateFont();
	fontToolDlg->Redraw();
}

} // namespace rage
