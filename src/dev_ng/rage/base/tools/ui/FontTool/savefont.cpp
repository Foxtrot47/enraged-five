// savefont.c

#if 0
#include "stdafx.h"
#include "text/fonttex.h"
#include "quant/quant4.h"
#include "quant/quant3.h"
#include "gfx/loadimg.h"
#include "gfx/image.h"
#include "image/image32.h"
#include "image/tex.h"
#include "core/output.h"
#include "core/assert.h"

namespace rage {

extern bool gfxSaveDDSImage(const char *filename,gfxImage *image,int format);

void ReadPixel(gfxImage *image,int x,int y,u8 rgba[4])
{
	Assert(image->GetDepth()==32);

	u8 *ptr = (image->GetBits8() + x * 4 + image->GetStride() * y);
	rgba[0] = *ptr++;
	rgba[1] = *ptr++;
	rgba[2] = *ptr++;
	rgba[3] = *ptr++;
}

void MapColors (Image32 *image,quant4 * q4, int nc4, quant3 * q3, int nc3) {
	const int q4_bit_shift = 5;
	const int q3_bit_shift = 7;
	int num_colors = nc4 + nc3;

	int wide, high;
	image->GetRes(wide, high);
	image->AllocateIndices();
	image->AllocateTable(num_colors);

	for (int i = 0; i < nc4; i ++) {
		Rgba c;
		if (q4->GetColor(i, c)) {
			Rgba cc=c;

			c.r |= (c.r >> q4_bit_shift);
			c.g |= (c.g >> q4_bit_shift);
			c.b |= (c.b >> q4_bit_shift);
			c.a |= (c.a >> q4_bit_shift);
		} else {
			printf("Frame::MapColors could not find color for index: %d\n", i);
			c.Set(0, 0, 0, i);
		}
		int err = image->SetLut(i, c);
		if (err)
			printf("Frame::MapColors error setting Lut for index: %d (%d)\n", i, err);
	}

	for (i = 0; i < nc3; i ++) {
		Rgba c;
		if (q3->GetColor(i, c)) {
			Rgba cc=c;
			c.r |= (c.r >> q3_bit_shift);
			c.g |= (c.g >> q3_bit_shift);
			c.b |= (c.b >> q3_bit_shift);
			c.a = 255;
		} else
			c.Set(0, 255, 0);
		int err = image->SetLut(i + nc4, c);
		if (err)
			printf("Frame::MapColors error setting Lut for index: %d (%d)\n", i, err);
	}

	for (int y = 0; y < high; y ++) {
		for (int x = 0; x < wide; x ++) {
			Rgba c;
			image->GetRgba(x, y, c);
			if (c.a < 255) {
				int n = 0;
				if (! q4->GetIndex(c, n))
					printf("Frame::MapColors could not find index for color: %d %d %d %d\n", c.r, c.g, c.b, c.a);
				int err = image->SetIndex(x, y, (BYTE) n);
				if (err) printf("Frame::MapColors error setting Index at: (%d, %d) (%d)\n", x, y, err);
			} else {
				int n = 0;
				if (! q3->GetIndex(c, n))
					printf("Frame::MapColors could not find index for color: %d %d %d\n", c.r, c.g, c.b);
				int err = image->SetIndex(x, y, (BYTE) (n + nc4));
				if (err) printf("Frame::MapColors error setting Index at: (%d, %d) (%d)\n", x, y, err);
			}
		}
	}
}

// This one Quantizes and saves the images as .tex files
bool txtFontTex::SaveFontTexturesQuantized(const char *basePathname,bool force4BitTextures)
{
	Rgba color;

	bool monochrome=true;

	Image32 *images=Alloca(Image32,GetNumTextures());
	memset(images,0x00000000,sizeof(Image32)*GetNumTextures());

	for (int texNum=0;texNum<GetNumTextures();texNum++)
	{
		images[texNum].SetRes(m_Images[0]->GetWidth(),m_Images[0]->GetHeight());
		images[texNum].AllocatePixels();

		for (int i=0;i<m_Images[texNum]->GetHeight();i++)
		{
			for (int j=0;j<m_Images[texNum]->GetWidth();j++)
			{
				u8 rgba[4];
				u16 irgba[4];

				ReadPixel(m_Images[texNum],j,i,rgba);
				if (rgba[3]==0)
					irgba[0]=irgba[1]=irgba[2]=irgba[3]=0;
				else
				{
					irgba[0]=(u16)(rgba[0]);
					irgba[1]=(u16)(rgba[1]);
					irgba[2]=(u16)(rgba[2]);
					irgba[3]=(u16)(rgba[3]);
				}

				if (rgba[0]!=rgba[1] || rgba[1]!=rgba[2])
				{
//					Displayf("0x%08X",*rgba);
					monochrome=false;
				}

				color.Set(irgba[0],irgba[1],irgba[2],irgba[3]);
				images[texNum].SetRgba(j,i,color);
			}
		}
	}

	quant4 q4;
	q4.Reset(4);
	for (texNum=0;texNum<GetNumTextures();texNum++)
		q4.Tally(images[texNum].GetPixels(),images[texNum].GetNumPixels());

	int ntc = q4.Colors();
	Displayf("---Translucent colors tallied: %d",ntc);

	quant3 q3;
	q3.Reset(6);
	q3.SetTallyOpaqueOnly(true);
	for (texNum=0;texNum<GetNumTextures();texNum++)
		q3.Tally(images[texNum].GetPixels(),images[texNum].GetNumPixels());

	int noc = q3.Colors();
	Displayf("---Opaque colors tallied: %d",noc);

	int nac = ntc + noc;
	int mtc = 0;
	int moc = 0;

	int maxColors=256;

	if (monochrome)
	{
		Displayf("---Monochrome - reducing maxcolors to 16");
		maxColors=16;
	}
	else if (force4BitTextures)
	{
		Displayf("---forcing maxcolors to 16");
		maxColors=16;
	}

	if (nac > maxColors)
		nac = maxColors;

	mtc = (int) (float(ntc) / float(ntc + noc) * float(nac));
	moc = nac - mtc;

	Displayf("---Number of palette colors: %d = %d + %d",nac,mtc,moc);

	int texFormat=TEX_FMT_PA_8;

	if (nac<=16)
		texFormat=TEX_FMT_PA_4;

	Displayf("Format is %s",texFormat==TEX_FMT_PA_8?"TEX_FMT_PA_8":(texFormat==TEX_FMT_PA_4?"TEX_FMT_PA_4":"TEX_FMT_I_8"));

	if (mtc > 0) {
		q4.Normalize(q4.Popular());
		q4.Quantize(mtc);
	}

	if (moc > 0) {
		q3.Normalize(q3.Popular());
		q3.Quantize(moc);
	}

	for (texNum=0;texNum<GetNumTextures();texNum++)
	{
		MapColors(&images[texNum],&q4,mtc,&q3,moc);

		// Save .tex File //

		char buf[1024];
		formatf(buf,1023,"%s_%02d.tex",basePathname,texNum);

		Displayf("Saving '%s'",buf);
		TexHeader tex;
		tex.SetType(texFormat);
		tex.SetNumMips(1);
		tex.SetBits(0x00010001); // make sure the clamp bits are set...
		tex.SetImage(0,&images[texNum]);
		if (tex.Save(buf)<0)
			return false;
	}

	return true;
}

bool txtFontTex::SaveFontTexturesCompressed(const char *basePathname)
{
	for (int texNum=0;texNum<GetNumTextures();texNum++)
	{
		char buf[1024];
		formatf(buf,1023,"%s_%02d.dds",basePathname,texNum);
		if (!gfxSaveDDSImage(buf,m_Images[texNum],gfxImage::gfDXT5))
			return false;
	}

	return true;
}


// This one Quantizes and saves the images as .tex files
bool txtFontTex::SaveFontShadowsQuantized(const char *basePathname,bool force4BitTextures)
{
	if (!m_ShadowLeft && !m_ShadowRight && !m_ShadowTop && !m_ShadowBottom)
		return true;

	Rgba color;

	bool monochrome=true;

	Image32 *images=Alloca(Image32,GetNumTextures());
	memset(images,0x00000000,sizeof(Image32)*GetNumTextures());

	for (int texNum=0;texNum<GetNumTextures();texNum++)
	{
		images[texNum].SetRes(m_ShadowImages[0]->GetWidth(),m_ShadowImages[0]->GetHeight());
		images[texNum].AllocatePixels();

		for (int i=0;i<m_ShadowImages[texNum]->GetHeight();i++)
		{
			for (int j=0;j<m_ShadowImages[texNum]->GetWidth();j++)
			{
				u8 rgba[4];
				u16 irgba[4];

				ReadPixel(m_ShadowImages[texNum],j,i,rgba);
				if (rgba[3]==0)
					irgba[0]=irgba[1]=irgba[2]=irgba[3]=0;
				else
				{
					irgba[0]=(u16)(rgba[0]);
					irgba[1]=(u16)(rgba[1]);
					irgba[2]=(u16)(rgba[2]);
					irgba[3]=(u16)(rgba[3]);
				}

				if (rgba[0]!=rgba[1] || rgba[1]!=rgba[2])
				{
//					Displayf("0x%08X",*rgba);
					monochrome=false;
				}

				color.Set(irgba[0],irgba[1],irgba[2],irgba[3]);
				images[texNum].SetRgba(j,i,color);
			}
		}
	}

	quant4 q4;
	q4.Reset(4);
	for (texNum=0;texNum<GetNumTextures();texNum++)
		q4.Tally(images[texNum].GetPixels(),images[texNum].GetNumPixels());

	int ntc = q4.Colors();
	Displayf("---Translucent colors tallied: %d",ntc);

	quant3 q3;
	q3.Reset(6);
	q3.SetTallyOpaqueOnly(true);
	for (texNum=0;texNum<GetNumTextures();texNum++)
		q3.Tally(images[texNum].GetPixels(),images[texNum].GetNumPixels());

	int noc = q3.Colors();
	Displayf("---Opaque colors tallied: %d",noc);

	int nac = ntc + noc;
	int mtc = 0;
	int moc = 0;

	int maxColors=256;

	if (monochrome)
	{
		Displayf("---Monochrome - reducing maxcolors to 16");
		maxColors=16;
	}
	else if (force4BitTextures)
	{
		Displayf("---forcing maxcolors to 16");
		maxColors=16;
	}

	if (nac > maxColors)
		nac = maxColors;

	mtc = (int) (float(ntc) / float(ntc + noc) * float(nac));
	moc = nac - mtc;

	Displayf("---Number of palette colors: %d = %d + %d",nac,mtc,moc);

	int texFormat=TEX_FMT_PA_8;

	if (nac<=16)
		texFormat=TEX_FMT_PA_4;

	Displayf("Format is %s",texFormat==TEX_FMT_PA_8?"TEX_FMT_PA_8":(texFormat==TEX_FMT_PA_4?"TEX_FMT_PA_4":"TEX_FMT_I_8"));

	if (mtc > 0) {
		q4.Normalize(q4.Popular());
		q4.Quantize(mtc);
	}

	if (moc > 0) {
		q3.Normalize(q3.Popular());
		q3.Quantize(moc);
	}

	for (texNum=0;texNum<GetNumTextures();texNum++)
	{
		MapColors(&images[texNum],&q4,mtc,&q3,moc);

		// Save .tex File //

		char buf[1024];
		formatf(buf,1023,"%s_%02ds.tex",basePathname,texNum);

		Displayf("Saving '%s'",buf);
		TexHeader tex;
		tex.SetType(texFormat);
		tex.SetNumMips(1);
		tex.SetBits(0x00010001); // make sure the clamp bits are set...
		tex.SetImage(0,&images[texNum]);
		if (tex.Save(buf)<0)
			return false;
	}

	return true;
}


bool txtFontTex::SaveFontShadowsCompressed(const char *basePathname)
{
	if (!m_ShadowLeft && !m_ShadowRight && !m_ShadowTop && !m_ShadowBottom)
		return true;

	for (int texNum=0;texNum<GetNumTextures();texNum++)
	{
		// Save .tex File //
		char buf[1024];
		formatf(buf,1023,"%s_%02ds.dds",basePathname,texNum);
		if (!gfxSaveDDSImage(buf,m_ShadowImages[texNum],gfxImage::gfDXT5))
			return false;
	}

	return true;
}

#pragma comment(lib,"xgraphics.lib")
#include <windows.h>
#include <d3d8-xbox.h>
#include <xgraphics.h>

static const u32 DDPF_ALPHAPIXELS = 0x00000001;
static const u32 DDPF_ALPHA = 0x00000002;
static const u32 DDPF_FOURCC = 0x00000004;
static const u32 DDPF_RGB = 0x00000040;
static const u32 DDPF_LUMINANCE = 0x00020000;

struct DDPIXELFORMAT {
	u32 dwSize;		// Size of structure. This member must be set to 32.
	u32 dwFlags;	// Flags to indicate valid fields. Uncompressed formats will usually use DDPF_RGB to indicate an RGB format, while compressed formats will use DDPF_FOURCC with a four-character code.
	u32 dwFourCC;	// This is the four-character code for compressed formats. dwFlags should include DDPF_FOURCC in this case. For DXTn compression, this is set to "DXT1", "DXT2", "DXT3", "DXT4", or "DXT5".
	u32 dwRGBBitCount; // For RGB formats, this is the total number of bits in the format. dwFlags should include DDPF_RGB in this case. This value is usually 16, 24, or 32. For A8R8G8B8, this value would be 32.
	u32 dwRBitMask;
	u32 dwGBitMask;
	u32 dwBBitMask;	// For RGB formats, these three fields contain the masks for the red, green, and blue channels. For A8R8G8B8, these values would be 0x00ff0000, 0x0000ff00, and 0x000000ff respectively.
	u32 dwRGBAlphaBitMask; // For RGB formats, this contains the mask for the alpha channel, if any. dwFlags should include DDPF_ALPHAPIXELS in this case. For A8R8G8B8, this value would be 0xff000000.
};

static const u32 DDSCAPS_COMPLEX = 0x00000008;
static const u32 DDSCAPS_TEXTURE = 0x00001000;
static const u32 DDSCAPS_MIPMAP = 0x00400000;
static const u32 DDSCAPS2_CUBEMAP = 0x00000200;
static const u32 DDSCAPS2_CUBEMAP_POSITIVEX = 0x00000400;
static const u32 DDSCAPS2_CUBEMAP_NEGATIVEX = 0x00000800;
static const u32 DDSCAPS2_CUBEMAP_POSITIVEY = 0x00001000;
static const u32 DDSCAPS2_CUBEMAP_NEGATIVEY = 0x00002000;
static const u32 DDSCAPS2_CUBEMAP_POSITIVEZ = 0x00004000;
static const u32 DDSCAPS2_CUBEMAP_NEGATIVEZ = 0x00008000;
static const u32 DDSCAPS2_VOLUME = 0x00200000;

struct DDCAPS2 {
	u32 dwCaps1;
	u32 dwCaps2;
	u32 Reserved[2];
};

static const u32 DDSD_CAPS = 0x00000001;
static const u32 DDSD_HEIGHT = 0x00000002;
static const u32 DDSD_WIDTH = 0x00000004;
static const u32 DDSD_PITCH = 0x00000008;
static const u32 DDSD_PIXELFORMAT = 0x00001000;
static const u32 DDSD_MIPMAPCOUNT = 0x00020000;
static const u32 DDSD_LINEARSIZE = 0x00080000;
static const u32 DDSD_DEPTH = 0x00800000;

struct DDSURFACEDESC2 {
	u32 dwSize;		// Size of structure. This member must be set to 124.
	u32 dwFlags;	// Flags to indicate valid fields. Always include DDSD_CAPS, DDSD_PIXELFORMAT, DDSD_WIDTH, DDSD_HEIGHT.
	u32 dwHeight;	// Height of the main image in pixels
	u32 dwWidth;	// Width of the main image in pixels
	u32 dwPitchOrLinearSize;	// For uncompressed formats, this is the number of bytes per scan line (DWORD> aligned) for the main image. dwFlags should include DDSD_PITCH in this case. For compressed formats, this is the total number of bytes for the main image. dwFlags should be include DDSD_LINEARSIZE in this case.
	u32 dwDepth;	// For volume textures, this is the depth of the volume. dwFlags should include DDSD_DEPTH in this case.
	u32 dwMipMapCount; // For items with mipmap levels, this is the total number of levels in the mipmap chain of the main image. dwFlags should include DDSD_MIPMAPCOUNT in this case.
	u32 dwReserved1[11]; //	Unused	
	DDPIXELFORMAT ddpfPixelFormat; // 32-byte value that specifies the pixel format structure.
	DDCAPS2 ddsCaps; // 16-byte value that specifies the capabilities structure.
	u32 dwReserved2; // 	Unused
};

bool gfxSaveDDSImage(const char *filename,gfxImage *image,int format) {
	if (image->GetFormat() != gfxImage::gf8888 || (format != gfxImage::gfDXT1 && format != gfxImage::gfDXT5))
		return false;
	Stream *S = ASSET.Create(filename,"dds");
	if (!S)
		return false;

	int magic = MAKE_MAGIC_NUMBER('D','D','S',' ');
	S->WriteInt(&magic, 1);

	int stride = (format == gfxImage::gfDXT5? image->GetWidth() : image->GetWidth()>>1);
	int width = image->GetWidth();
	int height = image->GetHeight();
	DDSURFACEDESC2 header;
	memset(&header,0,sizeof(header));
	header.dwSize = sizeof(header);
	header.dwFlags = DDSD_CAPS | DDSD_PIXELFORMAT | DDSD_WIDTH | DDSD_HEIGHT;
	header.dwHeight = height;
	header.dwWidth = width;
	header.dwPitchOrLinearSize = stride * height;
	header.dwFlags |= DDSD_LINEARSIZE;
	header.dwDepth = 0;
	header.dwMipMapCount = 1;

	DDPIXELFORMAT &ddpf = header.ddpfPixelFormat;
	ddpf.dwSize = sizeof(ddpf);

	bool dxt5 = false;
	switch (format) {
	case gfxImage::gfDXT1:
		ddpf.dwFlags = DDPF_FOURCC;
		ddpf.dwFourCC = MAKE_MAGIC_NUMBER('D','X','T','1');
		break;
	case gfxImage::gfDXT5:
		ddpf.dwFlags = DDPF_FOURCC;
		ddpf.dwFourCC = MAKE_MAGIC_NUMBER('D','X','T','5');
		dxt5 = true;
		break;
	}

	header.ddsCaps.dwCaps1 = DDSCAPS_COMPLEX | DDSCAPS_TEXTURE;

	S->WriteInt(&header.dwSize,sizeof(header)/4);

	u8 *dest = new u8[stride * height];
    u8 refAlpha = 8;
	XGCompressRect(dest,dxt5? D3DFMT_DXT5 : D3DFMT_DXT1,image->GetWidth() << (1 + dxt5),image->GetWidth(),image->GetHeight(),image->GetBitsVoid(),
		// Note -- LIN_A8B8G8R8 matches the AGE internal RGBA image format, saves us the hassle of converting.
		D3DFMT_LIN_A8B8G8R8,image->GetStride(),refAlpha,0);
	S->Write(dest,stride*height);
	delete[] dest;
	S->Close();
	return true;
}

} // namespace rage

#endif
