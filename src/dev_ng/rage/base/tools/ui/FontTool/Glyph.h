// Glyph.h: interface for the Glyph class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_GLYPH_H__4EE8DC43_C44F_479B_90B8_7D6CC88D09D1__INCLUDED_)
#define AFX_GLYPH_H__4EE8DC43_C44F_479B_90B8_7D6CC88D09D1__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

namespace rage {

class bkBank;

class Glyph  
{
public:
	Glyph();
	virtual ~Glyph();

	void DrawWire(float alpha,int offsetX,int offsetY);
 	void AddWidgets(bkBank &bank);

	void operator=(Glyph&);

	bool IsValid() const;
	bool ContainsPoint(int x,int y) const;

	void Serialize(CArchive &archive);

//	void Load(CArchive &archive);
//	void Save(CArchive &archive);

public:

	int mUnicode;

	int mLeft,mTop,mRight,mBottom;

	int mBaseline;		// bottom of a capital letter
	int mWidth;			// including extra space after character, if any
};

} // namespace rage

#endif // !defined(AFX_GLYPH_H__4EE8DC43_C44F_479B_90B8_7D6CC88D09D1__INCLUDED_)
