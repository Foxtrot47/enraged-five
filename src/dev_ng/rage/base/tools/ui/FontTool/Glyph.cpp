// Glyph.cpp: implementation of the Glyph class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "FontTool.h"
#include "Glyph.h"

#include "grcore/im.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

namespace rage {

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Glyph::Glyph()
{
	mUnicode=0;

	mLeft=0;
	mRight=0;
	mTop=0;
	mBottom=0;
	mBaseline=0;
	mWidth=0;
}

Glyph::~Glyph()
{
}

void Glyph::operator=(Glyph &from)
{
	mUnicode=from.mUnicode;

	mLeft=from.mLeft;
	mRight=from.mRight;
	mTop=from.mTop;
	mBottom=from.mBottom;
	mBaseline=from.mBaseline;
	mWidth=from.mWidth;
}

void Glyph::DrawWire(float alpha,int offsetX,int offsetY)
{
	if (!IsValid())
		return;

	if (mUnicode==0)
		grcColor4f(1,0,0,alpha);
	else
		grcColor4f(0.5f,1,0.5f,alpha);

	float left=float(mLeft+offsetX);
	float right=float(mRight+offsetX);
	float top=float(mTop+offsetY);
	float bottom=float(mBottom+offsetY);

	grcBegin(drawLineStrip,5);
	grcVertex2f(left,top);
	grcVertex2f(right,top);
	grcVertex2f(right,bottom);
	grcVertex2f(left,bottom);
	grcVertex2f(left,top);
	grcEnd();

	grcColor4f(1,1,0,alpha);
	grcBegin(drawLines,2);
	grcVertex2f(left,bottom-mBaseline);
	grcVertex2f(left+mWidth,bottom-mBaseline);
	grcEnd();
}

bool Glyph::IsValid() const
{
	if (mRight==mLeft && mTop==mBottom)
		return false;
	if (mRight<mLeft)
		return false;
	if (mBottom<mTop)
		return false;

//	if ((mRight>=mLeft && mTop<mBottom) || (mRight>mLeft && mTop<=mBottom))
//		return true;
//	else
//		return false;

	return true;
}

bool Glyph::ContainsPoint(int x,int y) const
{
	if (x>=mLeft && x<=mRight && y>=mTop && y<=mBottom)
		return true;
	else
		return false;
}

void Glyph::Serialize(CArchive &archive)
{
	if (archive.IsStoring())
	{
		archive << mUnicode;
		archive << mLeft;
		archive << mRight;
		archive << mTop;
		archive << mBottom;
		archive << mBaseline;
		archive << mWidth;
	}
	else
	{
		archive >> mUnicode;
		archive >> mLeft;
		archive >> mRight;
		archive >> mTop;
		archive >> mBottom;
		archive >> mBaseline;
		archive >> mWidth;
	}
}

/**
void Glyph::Load(CArchive &archive)
{
	archive >> mUnicode;
	archive >> mLeft;
	archive >> mRight;
	archive >> mTop;
	archive >> mBottom;
	archive >> mBaseline;
	archive >> mWidth;
}

void Glyph::Save(CArchive &archive)
{
	archive << mUnicode;
	archive << mLeft;
	archive << mRight;
	archive << mTop;
	archive << mBottom;
	archive << mBaseline;
	archive << mWidth;
}
**/

/**
void Glyph::AddWidgets(bkBank &bank)
{
	bank.AddSlider("Unicode",&mUnicode,0,65535,1);

//	bank.AddSlider("Left",&mLeft,0,txtFontEditor::mImageWidth-1,1);
//	bank.AddSlider("Right",&mRight,0,txtFontEditor::mImageWidth-1,1);
//	bank.AddSlider("Top",&mTop,0,txtFontEditor::mImageHeight-1,1);
//	bank.AddSlider("Bottom",&mBottom,0,txtFontEditor::mImageHeight-1,1);
}
***/

} // namespace rage
