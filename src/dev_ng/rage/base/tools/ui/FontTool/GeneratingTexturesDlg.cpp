// GeneratingTexturesDlg.cpp : implementation file
//

#include "stdafx.h"
#include "FontTool.h"
#include "GeneratingTexturesDlg.h"

namespace rage {

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// GeneratingTexturesDlg dialog


GeneratingTexturesDlg::GeneratingTexturesDlg(CWnd* pParent /*=NULL*/)
	: CDialog(GeneratingTexturesDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(GeneratingTexturesDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void GeneratingTexturesDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(GeneratingTexturesDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(GeneratingTexturesDlg, CDialog)
	//{{AFX_MSG_MAP(GeneratingTexturesDlg)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// GeneratingTexturesDlg message handlers

} // namespace rage
