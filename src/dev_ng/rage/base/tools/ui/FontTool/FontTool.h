// FontTool.h : main header file for the TEST application
//

#if !defined(AFX_TEST_H__BCB51AE6_4E36_476E_BE02_8EEF4DD33FE8__INCLUDED_)
#define AFX_TEST_H__BCB51AE6_4E36_476E_BE02_8EEF4DD33FE8__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CFontToolApp:
// See FontTool.cpp for the implementation of this class
//

class CFontToolApp : public CWinApp
{
public:
	CFontToolApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CFontToolApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CFontToolApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TEST_H__BCB51AE6_4E36_476E_BE02_8EEF4DD33FE8__INCLUDED_)
