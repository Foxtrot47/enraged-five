// Alphabet.h: interface for the Alphabet class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ALPHABET_H__7A64BC28_1A3F_44C9_8394_CF846B7DC89A__INCLUDED_)
#define AFX_ALPHABET_H__7A64BC28_1A3F_44C9_8394_CF846B7DC89A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class HashTable;

namespace rage {

class txtStringEditor;

class Alphabet  
{
public:
	Alphabet(int flags,txtStringEditor *stringEditor,const char *matchFont);		// flags is actually an eFlags
	virtual ~Alphabet();

	void Print() const;
	void Save(const char *pathname);

	unsigned short GetUnicode(int glyphNum);

	int m_MaxGlyphs;
	unsigned short *m_GlyphToUnicode;

	enum eFlags {
		kUppers=			0x0001,
		kLowers=			0x0002,
		kNumbers=			0x0004,
		kCommonSymbols=		0x0008,
		kAllSymbols=		0x0010,
		kEuroUppers=		0x0020,
		kEuroLowers=		0x0040,
		kEuroSymbols=		0x0080,
		kMoreSymbols=		0x0100,
//		kStringTable=		0x0200,	
		kStringTableAll=	0x0200,
		kStringTableSome=	0x0800
	};

	static _TCHAR *sCommonSymbols;
	static _TCHAR sEuroUppers[];
	static _TCHAR sEuroLowers[];
	static _TCHAR sEuroSymbols[];
	static _TCHAR sMoreSymbols[];
};

}

#endif // !defined(AFX_ALPHABET_H__7A64BC28_1A3F_44C9_8394_CF846B7DC89A__INCLUDED_)
