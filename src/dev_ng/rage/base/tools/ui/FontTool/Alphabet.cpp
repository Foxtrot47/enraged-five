// Alphabet.cpp: implementation of the Alphabet class.
//
//////////////////////////////////////////////////////////////////////

#include "StdAfx.h"
#include "FontTool.h"
#include "Alphabet.h"

#include "atl/string.h"
#include "diag/output.h"
#include "file/stream.h"
#include "obsolete/hash.h"
#include "texttools/editstring.h"
#include "texttools/stringeditor.h"

namespace rage {

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

_TCHAR *Alphabet::sCommonSymbols=_T("!\"()+,-./:<=>?");

// Check out \\zen\projects\age\assets\fonts\charactersets.xls
// for descriptions of these characters and to generate these
// lists

_TCHAR Alphabet::sEuroUppers[]={
	0x00C7,0x00C4,0x00C5,0x00C9,0x00C6,0x00D6,0x00DC,0x00D8,
	0x00D1,0x00C1,0x00C2,0x00C0,0x00C3,0x00D0,0x00CA,0x00CB,
	0x00C8,0x00CD,0x00CE,0x00CF,0x00CC,0x00D3,0x00D4,0x00D2,
	0x00D5,0x00DE,0x00DA,0x00DB,0x00D9,0x00DD,
	0
};

_TCHAR Alphabet::sEuroLowers[]={
	0x00FC,0x00E9,0x00E2,0x00E4,0x00E0,0x00E5,0x00E7,0x00EA,
	0x00EB,0x00E8,0x00EF,0x00EE,0x00EC,0x00E6,0x00F4,0x00F6,
	0x00F2,0x00FB,0x00F9,0x00FF,0x00F8,0x0192,0x00E1,0x00ED,
	0x00F3,0x00FA,0x00F1,0x00E3,0x00F0,0x0131,0x00DF,
	0x00F5,0x00FE,0x00FD,
	0
};

_TCHAR Alphabet::sEuroSymbols[]={
	0x00BF,0x00A1,0x00AB,0x00B0,0x00BB,0x20AC,
	0
};

_TCHAR Alphabet::sMoreSymbols[]={
	0x00A9,0x00AE,0x2018,0x2019,0x201C,0x201D,0x2122,
	0
};

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

inline bool getBit(unsigned char *bits,int index)
{
	return (bits[index>>3]&(1<<(index%8))) ? true : false;
}

inline void setBit(unsigned char *bits,int index)
{
	bits[index>>3]|=1<<(index%8);
}


Alphabet::Alphabet(int flags,txtStringEditor *stringEditor,const char *matchFont)
{
	unsigned char *bits=Alloca(unsigned char,8192);
	memset(bits,0,8192);

	int chr;

	// Add Uppercase characters //

	if (flags&kUppers)
		for (chr=_T('A');chr<=_T('Z');chr++)
			setBit(bits,chr);

	// Add Lowercase characters //

	if (flags&kLowers)
		for (chr=_T('a');chr<=_T('z');chr++)
			setBit(bits,chr);

	// Add Numbers //

	if (flags&kNumbers)
		for (chr=_T('0');chr<=_T('9');chr++)
			setBit(bits,chr);

	// Add All ASCII Symbols //

	if (flags&kAllSymbols)
	{
		for (chr=_T('"');chr<=_T('/');chr++)
			setBit(bits,chr);
		for (chr=_T(':');chr<=_T('@');chr++)
			setBit(bits,chr);
		for (chr=_T('[');chr<=_T('`');chr++)
			setBit(bits,chr);
		for (chr=_T('{');chr<=_T('~');chr++)
			setBit(bits,chr);
	}

	// Add Common Symbols //

	if (flags&kCommonSymbols)
		for (_TCHAR *chr=sCommonSymbols;*chr;chr++)
			setBit(bits,*chr);

	// Add Euro Uppers //

	if (flags&kEuroUppers)
		for (_TCHAR *chr=sEuroUppers;*chr;chr++)
			setBit(bits,*chr);

	// Add Euro Lowers //

	if (flags&kEuroLowers)
		for (_TCHAR *chr=sEuroLowers;*chr;chr++)
			setBit(bits,*chr);

	// Add Euro Symbols //

	if (flags&kEuroSymbols)
		for (_TCHAR *chr=sEuroSymbols;*chr;chr++)
			setBit(bits,*chr);

	// Add More Symbols //

	if (flags&kMoreSymbols)
		for (_TCHAR *chr=sMoreSymbols;*chr;chr++)
			setBit(bits,*chr);

	// Add all characters from String Table //

	if (flags&kStringTableAll)
	{
		txtStringEditor::Map& map=stringEditor->GetHashTable();

		txtStringEditor::Map::Iterator it = map.CreateIterator();
		while (!it.AtEnd())
		{
			txtStringEntry *entry=(txtStringEntry*)it.GetData();
			Assert(entry);

			for (int language=0;language<txtLanguage::GetCount();language++)
			{
				if (!entry->mStrings[language])
					continue;

				const _TCHAR *_tstring=(const _TCHAR *)entry->mStrings[language]->GetString();
				const _TCHAR *ptr=_tstring;

				while (*ptr)
				{
//					Displayf("char %d '%c' - bits[%d]=0x%02X",*ptr,char(*ptr&0xff),int(*ptr>>3),int(bits[*ptr>>3]));

					if (*ptr>32)				// skip control characters and <space>
						setBit(bits,*ptr);
					ptr++;
				}
			}
		}
	}

	// Add some characters from String Table //

	if (flags&kStringTableSome)
	{
		txtStringEditor::Map& map=stringEditor->GetHashTable();

		txtStringEditor::Map::Iterator it = map.CreateIterator();
		while (!it.AtEnd())
		{
			txtStringEntry *entry=(txtStringEntry*)it.GetData();
			Assert(entry);

			for (int language=0;language<txtLanguage::GetCount();language++)
			{
				if (!entry->mStrings[language])
					continue;

				if (!_stricmp(entry->mStrings[language]->mFont,matchFont))
				{
					const _TCHAR *_tstring=(const _TCHAR *)entry->mStrings[language]->GetString();
					const _TCHAR *ptr=_tstring;

					while (*ptr)
					{
//						Displayf("char %d '%c' - bits[%d]=0x%02X",*ptr,char(*ptr&0xff),int(*ptr>>3),int(bits[*ptr>>3]));

						if (*ptr>32)				// skip control characters and <space>
							setBit(bits,*ptr);
						ptr++;
					}
				}
			}
		}
	}

	// Count characters //

	int numGlyphs=0;
	for (int i=0;i<65536;i++)
		if (getBit(bits,i))
			numGlyphs++;

	Displayf("numGlyphs=%d",numGlyphs);	

	// Create List of characters //

	m_MaxGlyphs=numGlyphs;
	m_GlyphToUnicode=new unsigned short[numGlyphs];

	int glyphNum=0;
	for (int i=0;i<65536;i++)
		if (bits[i>>3]&(1<<(i%8)))
		{
			m_GlyphToUnicode[glyphNum]=(unsigned short)i;
			glyphNum++;
		}

	if (GetAsyncKeyState(VK_CONTROL)&0x8000)
	{
		Save("unicode.txt");
		Print();
	}
}

Alphabet::~Alphabet()
{
	delete []m_GlyphToUnicode;
}

unsigned short Alphabet::GetUnicode(int glyphNum)
{
	if (glyphNum>=m_MaxGlyphs)
		return 0;
	else
		return m_GlyphToUnicode[glyphNum];
}

void Alphabet::Print() const
{
	for (int i=0;i<m_MaxGlyphs;i++)
	{
		int chr=m_GlyphToUnicode[i];
		Displayf("Glyph %d = Unicode character %d '%c'",i,chr,chr>255?'?':char(chr));
	}
}

void Alphabet::Save(const char *pathname)
{
	fiStream *stream=fiStream::Create(pathname);

	unsigned short i=0xFEFF;
	stream->Write(&i,2);

	stream->Write(m_GlyphToUnicode,m_MaxGlyphs*2);

	stream->Close();
}

} // namespace rage
