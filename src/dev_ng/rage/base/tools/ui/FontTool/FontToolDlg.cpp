// FontToolDlg.cpp : implementation file
//

#if 0

#include "stdafx.h"
#include "FontTool.h"
#include "FontToolDlg.h"
#include "AboutDlg.h"
#include "Alphabet.h"
#include "Glyph.h"
#include "NewFontDlg.h"
#include "MakeGridDlg.h"
#include "GeneratingTexturesDlg.h"

#include "texttools/editstring.h"
#include "text/stringtable.h"
#include "text/stringtex.h"
#include "texttools/tsv.h"
#include "text/fonttex.h"
#include "text/language.h"
#include "devcam/polarcam.h"
#include "gfx/simple.h"
#include "input/keyboard.h"
#include "input/keys.h"
#include "gfx/image.h"
#include "gfx/loadimg.h"
#include "gfx/simpletex.h"
#include "gfx/pipe.h"
#include "gfx/viewport.h"
#include "gfx/rstate.h"
#include "gfx/winpriv.h"
#include "bank/bkmgr.h"
#include "bank/bank.h"
#include "data/callback.h"
#include "data/timemgr.h"
#include "core/output.h"
#include "core/alloca.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// oversampling happens in DrawGlyph and DrawAll
#define ENABLE_OVERSAMPLING 1

extern HWND hwndMain;

devPolarCam *gCamera=NULL;

const int kMaxGlyphs=65535;

/////////////////////////////////////////////////////////////////////////////
// CFontToolDlg dialog

CFontToolDlg::CFontToolDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CFontToolDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CFontToolDlg)
	m_CharHeight = 20;
	m_CharSpacing = 0;
	m_SpaceWidth = 14;
	m_UnicodeValue = 0;
	m_UnicodeText = _T("");
	m_GlyphBottom = 0;
	m_GlyphLeft = 0;
	m_GlyphRight = 0;
	m_GlyphTop = 0;
	m_GlyphWidth = 0;
	m_GlyphBaseline = 0;
	m_4BitTextures = FALSE;
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDI_RSSD);

	mFontName=_T("Untitled");
	mWreckedAlpha=false;
	Version=0;
}

void CFontToolDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CFontToolDlg)
	DDX_Control(pDX, IDC_GLYPHBASELINESPIN, m_GlyphBaselineSpin);
	DDX_Control(pDX, IDC_GLYPHWIDTHSPIN, m_GlyphWidthSpin);
	DDX_Control(pDX, IDC_GLYPHTOPSPIN, m_GlyphTopSpin);
	DDX_Control(pDX, IDC_GLYPHRIGHTSPIN, m_GlyphRightSpin);
	DDX_Control(pDX, IDC_GLYPHLEFTSPIN, m_GlyphLeftSpin);
	DDX_Control(pDX, IDC_GLYPHBOTTOMSPIN, m_GlyphBottomSpin);
	DDX_Control(pDX, IDC_SPACEWIDTHSPIN, m_SpaceWidthSpin);
	DDX_Control(pDX, IDC_CHARSPACINGSPIN, m_CharSpacingSpin);
	DDX_Control(pDX, IDC_CHARHEIGHTSPIN, m_CharHeightSpin);
	DDX_Control(pDX, IDC_STRINGSCROLLBAR, m_StringScrollBar);
	DDX_Control(pDX, IDC_UNICODEVALUE, m_UnicodeValueCtrl);
	DDX_Control(pDX, IDC_UNICODETEXT, m_UnicodeTextCtrl);
	DDX_Control(pDX, IDC_UNICODESPIN, m_UnicodeSpin);
	DDX_Control(pDX, IDC_STRINGTEXT, m_StringText);
	DDX_Control(pDX, IDC_STRINGFONT, m_StringFont);
	DDX_Control(pDX, IDC_STRINGENGLISH, m_StringEnglish);
	DDX_Control(pDX, IDC_STRINGIDENTIFIER, m_StringIdentifier);
	DDX_Control(pDX, IDC_LANGUAGE, m_Language);
	DDX_Control(pDX, IDC_VIEWPORT, m_Viewport);
	DDX_Control(pDX, IDC_RUNNING, m_Running);
	DDX_Text(pDX, IDC_CHARHEIGHT, m_CharHeight);
	DDV_MinMaxInt(pDX, m_CharHeight, 1, 255);
	DDX_Text(pDX, IDC_CHARSPACING, m_CharSpacing);
	DDV_MinMaxInt(pDX, m_CharSpacing, -255, 255);
	DDX_Text(pDX, IDC_SPACEWIDTH, m_SpaceWidth);
	DDV_MinMaxInt(pDX, m_SpaceWidth, -255, 255);
	DDX_Text(pDX, IDC_UNICODEVALUE, m_UnicodeValue);
	DDV_MinMaxInt(pDX, m_UnicodeValue, 0, 65535);
	DDX_Text(pDX, IDC_UNICODETEXT, m_UnicodeText);
	DDV_MaxChars(pDX, m_UnicodeText, 1);
	DDX_Text(pDX, IDC_GLYPHBOTTOM, m_GlyphBottom);
	DDX_Text(pDX, IDC_GLYPHLEFT, m_GlyphLeft);
	DDX_Text(pDX, IDC_GLYPHRIGHT, m_GlyphRight);
	DDX_Text(pDX, IDC_GLYPHTOP, m_GlyphTop);
	DDX_Text(pDX, IDC_GLYPHWIDTH, m_GlyphWidth);
	DDX_Text(pDX, IDC_GLYPHBASELINE, m_GlyphBaseline);
	DDX_Check(pDX, IDC_4BITTEXTURES, m_4BitTextures);
	//}}AFX_DATA_MAP
	DDX_Control(pDX, IDC_SB_HORZ, SB_Horz);
	DDX_Control(pDX, IDC_SB_VERT, SB_Vert);
}

BEGIN_MESSAGE_MAP(CFontToolDlg, CDialog)
	//{{AFX_MSG_MAP(CFontToolDlg)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_RUNNING, OnRunning)
	ON_BN_CLICKED(IDC_VIEWPORT, OnViewport)
	ON_WM_LBUTTONDOWN()
	ON_BN_CLICKED(IDC_NEWFONT, OnNewFont)
	ON_BN_CLICKED(IDC_MAKEGRID, OnMakeGrid)
	ON_BN_CLICKED(IDC_RESIZEALL, OnResizeAll)
	ON_BN_CLICKED(IDC_LOADFONT, OnLoadFont)
	ON_BN_CLICKED(IDC_SAVEFONT, OnSaveFont)
	ON_BN_CLICKED(IDC_EXPORTFONT, OnExportFont)
	ON_BN_CLICKED(IDC_GENERATEFONT, OnGenerateFont)
	ON_BN_CLICKED(IDC_LOADSTRINGTABLE, OnLoadStringTable)
	ON_BN_CLICKED(IDC_SAVESTRINGTABLE, OnSaveStringTable)
	ON_CBN_EDITCHANGE(IDC_LANGUAGE, OnEditChangeLanguage)
	ON_BN_CLICKED(IDC_DRAWGLYPH, OnDrawGlyph)
	ON_BN_CLICKED(IDC_DELETEGLYPH, OnDeleteGlyph)
	ON_BN_CLICKED(IDC_SHRINKGLYPH, OnShrinkGlyph)
	ON_BN_CLICKED(IDC_DRAWALL, OnDrawAll)
	ON_BN_CLICKED(IDC_PICKFONT, OnPickFont)
	ON_EN_CHANGE(IDC_UNICODETEXT, OnChangeUnicodeText)
	ON_EN_CHANGE(IDC_UNICODEVALUE, OnChangeUnicodeValue)
	ON_WM_VSCROLL()
	ON_WM_HSCROLL()
	ON_CBN_SELCHANGE(IDC_LANGUAGE, OnSelChangeLanguage)
	ON_EN_CHANGE(IDC_GLYPHBASELINE, OnChangeGlyph)
	ON_EN_CHANGE(IDC_CHARHEIGHT, OnChangeFontParameter)
	ON_WM_RBUTTONDOWN()
	ON_COMMAND(ID_GLYPH_POPUP_DELETE, OnGlyphPopupDelete)
	ON_COMMAND(ID_GLYPH_POPUP_SHRINK, OnGlyphPopupShrink)
	ON_COMMAND(ID_GLYPH_POPUP_EXPAND, OnGlyphPopupExpand)
	ON_BN_CLICKED(IDC_SLICE, OnSlice)
	ON_BN_CLICKED(IDC_EXIT, OnExit)
	ON_BN_CLICKED(IDC_OK, OnOk)
	ON_BN_CLICKED(IDC_EXPANDGLYPH, OnExpandGlyph)
	ON_BN_CLICKED(IDC_SHADOWS, OnShadows)
	ON_BN_CLICKED(IDC_RESIZEGLYPH, OnResizeGlyph)
	ON_COMMAND(ID_GLYPH_POPUP_RESIZE, OnGlyphPopupResize)
	ON_WM_CLOSE()
	ON_EN_CHANGE(IDC_GLYPHBOTTOM, OnChangeGlyph)
	ON_EN_CHANGE(IDC_GLYPHLEFT, OnChangeGlyph)
	ON_EN_CHANGE(IDC_GLYPHTOP, OnChangeGlyph)
	ON_EN_CHANGE(IDC_GLYPHWIDTH, OnChangeGlyph)
	ON_EN_CHANGE(IDC_CHARSPACING, OnChangeFontParameter)
	ON_EN_CHANGE(IDC_SPACEWIDTH, OnChangeFontParameter)
	ON_WM_DESTROY()
	ON_WM_CANCELMODE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CFontToolDlg message handlers

BOOL CFontToolDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	// TODO: Add extra initialization here

	// AGE Setup stuff

	_CrtSetDbgFlag(0);				// disable memory leak dump when exiting

	ARGS.Init(__argc,__argv);

	ageInitAge("");

	hwndMain = m_hWnd;	// assign view's handle to age's

	hwndMain = *GetDlgItem(IDC_VIEWPORT);

	CRect rect;
	m_Viewport.GetClientRect(&rect);

	PIPE.SetWindow(true,0,0);
	const int colorDepth=24;
	const int zDepth=8;
	PIPE.SetRes(rect.right,rect.bottom,colorDepth,zDepth);

	if (!PIPE.InitClass())
		Quitf("Could not start pipeline.  Please check that you're in a 32bpp video mode.");

//	vglSetFormat(FVF_VCT1);

	if (!PIPE.Begin())
	{
		AfxMessageBox(_T("Failed to Start Graphics.  Time to upgrade."));
		exit(-1);
	}

//	PIPE.GetViewport()->Perspective(60.0f,0,0.01f,100.0f);

//	ageClearColor=MKRGB(0xa0,0xa0,0xc0);
	ageClearColor=MKRGB(59,144,210);

	// Dialogs //

	m_MakeGridDlg.Create(IDD_MAKEGRID_DIALOG);
	m_MakeGridDlg.CenterWindow();

	// Shadows Dialog //

	m_ShadowsDlg.Create(IDD_SHADOWS);
//	m_ShadowsDlg.ShowWindow(SW_SHOW);
	m_ShadowsDlg.CenterWindow();

	// String Table Initialization //

	mStringEditor=new txtStringEditor();
	m_CurrentString=NULL;

	USES_CONVERSION;
	for (int i=0;i<txtLanguage::GetCount();i++)
	{
		const _TCHAR *_tlanguage=A2W(txtLanguage::mNames[i]);
		m_Language.AddString(_tlanguage);
		m_Language.SetItemData(i,i);
	}
	m_Language.SetCurSel(txtLanguage::kEn);

	SCROLLINFO si;
	si.fMask=SIF_RANGE|SIF_POS|SIF_PAGE;
	si.nMin=0;
	si.nMax=0;
	si.nPage=1;
	si.nPos=0;
	m_StringScrollBar.SetScrollInfo(&si,true);

	m_StringScrollBar.EnableWindow(false);
	DisableStringWidgets();

	// Font Editor Initialization //

	mImage=NULL;
	mImageWidth=0;
	mImageHeight=0;
	mBitmap=NULL;

	mFont=new txtFontTex;

	// Glyph Editor Initialization //

	m_CurrentGlyph=NULL;

	// Camera //

	gCamera=new devPolarCam;

#if __BANK
	// Widgets //

//	ageInitBank("FontTool");

//	bkManager::DeleteBankManager();

//	bkBank &cameraBank=BANKMGR.CreateBank("Camera");
//	gCamera->AddWidgets(cameraBank);
#endif

	// txtFontTex Initialization //

	txtFontTex::sm_FreeTextures=false;		// this prevents Windows 2000 from crashing but
											// leaks texture memory

//	txtFontTex::mOldPositionHack=true;

	// Init //

	Init();

	// Default Font //

	memset(&m_LogFont, 0, sizeof m_LogFont);
	m_LogFont.lfHeight = 24;
	lstrcpy(m_LogFont.lfFaceName,_T("Arial Unicode MS"));
	m_LogFont.lfOutPrecision = OUT_OUTLINE_PRECIS;
	m_LogFont.lfClipPrecision = CLIP_DEFAULT_PRECIS;
	m_LogFont.lfQuality = ANTIALIASED_QUALITY;			// undocumented but cool
	m_LogFont.lfPitchAndFamily = FF_SWISS | VARIABLE_PITCH;

	m_crText = COLOR_WINDOWTEXT+1;

	m_Font=new CFont;
	if (!m_Font->CreateFontIndirect(&m_LogFont))
		m_Font=NULL;

	// Widgets //

	m_CharHeightSpin.SetBuddy(GetDlgItem(IDC_CHARHEIGHT));
	m_CharHeightSpin.SetRange32(-100,100);
	m_CharSpacingSpin.SetBuddy(GetDlgItem(IDC_CHARSPACING));
	m_CharSpacingSpin.SetRange32(-100,100);
	m_SpaceWidthSpin.SetBuddy(GetDlgItem(IDC_SPACEWIDTH));
	m_SpaceWidthSpin.SetRange32(-100,100);

	m_UnicodeSpin.SetBuddy(GetDlgItem(IDC_UNICODEVALUE));
	m_UnicodeSpin.SetRange32(0,65535);

	m_GlyphLeftSpin.SetBuddy(GetDlgItem(IDC_GLYPHLEFT));
	m_GlyphLeftSpin.SetRange(0,9999);
	m_GlyphRightSpin.SetBuddy(GetDlgItem(IDC_GLYPHRIGHT));
	m_GlyphRightSpin.SetRange(0,9999);
	m_GlyphTopSpin.SetBuddy(GetDlgItem(IDC_GLYPHTOP));
	m_GlyphTopSpin.SetRange(0,9999);
	m_GlyphBottomSpin.SetBuddy(GetDlgItem(IDC_GLYPHBOTTOM));
	m_GlyphBottomSpin.SetRange(0,9999);
	m_GlyphBaselineSpin.SetBuddy(GetDlgItem(IDC_GLYPHBASELINE));
	m_GlyphBaselineSpin.SetRange(-9999,9999);
	m_GlyphWidthSpin.SetBuddy(GetDlgItem(IDC_GLYPHWIDTH));
	m_GlyphWidthSpin.SetRange(0,9999);

	CFont *unicodeFont=new CFont;
	unicodeFont->CreatePointFont(240,_T("Arial Unicode MS"));
	m_UnicodeTextCtrl.SetFont(unicodeFont);
	m_StringText.SetFont(unicodeFont);

	m_Running.SetCheck(0);
	OnRunning();

	// Load Project File //

	if (mFontName.CompareNoCase(_T("Untitled")))
	{
		LoadFontProject();
		LoadFontImage();
	}

/**
	if (ARGS.Argv[1] && ARGS.Argv[1][0]!='-')
	{
		USES_CONVERSION;
		mFontName=A2W(ARGS.Argv[1]);
		LoadFontProject();
		LoadFontImage();
	}
**/

	// TODO: Add extra initialization here
	
	SB_Horz.EnableScrollBar(ESB_DISABLE_BOTH);   
	SB_Horz.SetScrollPos(0);
	SB_Horz.ShowWindow(SW_HIDE);  // for now, hide the horizontal scroll bar
	SB_Vert.EnableScrollBar(ESB_DISABLE_BOTH);
	SB_Vert.SetScrollPos(0);
	
	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CFontToolDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CFontToolDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
//		CPaintDC dc(this);

		CDialog::OnPaint();
	}

	Update();
	Draw();
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CFontToolDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

void CFontToolDlg::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
	int nMin=0;
	int nMax=100;

	// TODO: Add your message handler code here and/or call default
	int position, pos_change;

	position = pScrollBar->GetScrollPos();
	switch ( nSBCode ) {
	case SB_THUMBPOSITION:
		pScrollBar->SetScrollPos( nPos );
		break;

	case SB_LINELEFT: // left arrow button
		pos_change = (nMax - nMin) / 10;
		if (( position - pos_change ) > nMin ) {
			position -= pos_change;
		}
		else {
			position = nMin;
		}
		pScrollBar->SetScrollPos( position );
		break;

	case SB_LINERIGHT: // right arrow button
		pos_change = (nMax - nMin) / 10;
		if (( position + pos_change ) < nMax ) {
			position += pos_change;
		}
		else {
			position = nMax;
		}
		pScrollBar->SetScrollPos( position );
		break;

	case SB_PAGELEFT: // click on left side of button
		pos_change = (nMax - nMin) / 3;
		if (( position - pos_change ) > nMin ) {
			position -= pos_change;
		}
		else {
			position = nMin;
		}
		pScrollBar->SetScrollPos( position );
		break;

	case SB_PAGERIGHT: // click on right side of button
		pos_change = (nMax - nMin) / 3;
		if (( position + pos_change ) < nMax ) {
			position += pos_change;
		}
		else {
			position = nMax;
		}
		pScrollBar->SetScrollPos( position );
		break;

	}; // switch

	Redraw();
	// CDialog::OnHScroll(nSBCode, nPos, pScrollBar);
}

void CFontToolDlg::OnVScrollViewport(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
	int nMin=0;
	int nMax=100;
	
	SB_Vert.GetScrollRange(&nMin,&nMax);

	CRect clientRect;
	m_Viewport.GetClientRect(&clientRect);

	int width	=(clientRect.right-clientRect.left);
	int height	=(clientRect.bottom-clientRect.top);

	// TODO: Add your message handler code here and/or call default

	int position, pos_change;

	position = pScrollBar->GetScrollPos();
	switch ( nSBCode ) {
	case SB_THUMBPOSITION:
		pScrollBar->SetScrollPos( nPos );
		break;

	case SB_LINEUP: // left arrow button
		pos_change = 32;
		if (( position - pos_change ) > nMin ) {
			position -= pos_change;
		}
		else {
			position = nMin;
		}
		pScrollBar->SetScrollPos( position );
		break;

	case SB_LINEDOWN: // right arrow button
		pos_change = 32;
		if (( position + pos_change ) < nMax ) {
			position += pos_change;
		}
		else {
			position = nMax;
		}
		pScrollBar->SetScrollPos( position );
		break;

	case SB_PAGEUP: // click on left side of button
	
		pos_change = mImageHeight/height;
		if (( position - pos_change ) > nMin ) {
			position -= pos_change;
		}
		else {
			position = nMin;
		}
		pScrollBar->SetScrollPos( position );
		break;

	case SB_PAGEDOWN: // click on right side of button
		pos_change = mImageHeight/height;
		if (( position + pos_change ) < nMax ) {
			position += pos_change;
		}
		else {
			position = nMax;
		}
		pScrollBar->SetScrollPos( position );
		break;

	}; // switch

	//  CDialog::OnVScroll(nSBCode, nPos, pScrollBar);
	Redraw();
}

void CFontToolDlg::Init()
{
	RSTATE.SetLighting(true);

	// FontEditor //

	InitFontEditor();
}

void CFontToolDlg::InitFontEditor()
{
}

void CFontToolDlg::Update()
{
	ChangedEditGlyph();
	gCamera->Update();

	TIME.Update();

	UpdateFontEditor();
}

void CFontToolDlg::UpdateFontEditor()
{
}

void CFontToolDlg::Draw()
{
	static bool inDrawAlready = false;

	// we're already in a draw loop, so exit since we're redrawing anyways:
	if (inDrawAlready)
		return;
	inDrawAlready = true;

	ageBeginFrame();

	RSTATE.SetCamera(gCamera->GetWorldMtx());
	RSTATE.SetIdentity();
	RSTATE.SetAlphaBlendEnable(true);

	DrawFontEditor();

	DrawTestText();

	ageEndFrame();
	
	inDrawAlready = false;
}

void CFontToolDlg::DrawFontEditor()
{
	if (!mImage)
		return;

	CRect rect;
	m_Viewport.GetClientRect(&rect);

	int vertOffset=SB_Vert.GetScrollPos();
	int horzOffset=SB_Horz.GetScrollPos();
	PIPE.CopyBitmap(0,0,horzOffset,vertOffset,rect.Width()-1+horzOffset,rect.Height()-1+vertOffset,mBitmap);

	RSTATE.SetZTestEnable(false);
	gfxViewport *oldViewport=PIPE.GetViewport();
	PIPE.SetViewport(gfxPipeline::OrthoVP);

	RSTATE.SetIdentity();

	RSTATE.SetAlphaBlendEnable(true);
	RSTATE.SetBlendSet(blendSet_SrcAlpha_InvSrcAlpha);
	RSTATE.SetMaterial(&gfxMaterial::FlatWhite);

	RSTATE.SetTexture(NoTexture);
	RSTATE.SetLighting(false);
	vglColor4f(.5f,.5f,1,1);

	POSITION position=mGlyphList.GetHeadPosition();
	while (position)
	{
		Glyph *glyph=mGlyphList.GetNext(position);
		glyph->DrawWire(0.4f,-SB_Horz.GetScrollPos(),-SB_Vert.GetScrollPos());
	}

	if (m_CurrentGlyph)
		m_CurrentGlyph->DrawWire(1.f,-SB_Horz.GetScrollPos(),-SB_Vert.GetScrollPos());

	PIPE.SetViewport(oldViewport);
}

void CFontToolDlg::Redraw()
{
	Draw();
}

static bool Overlaps(const Glyph &glyph,const CList<Glyph*,Glyph*> &glyphList)
{
	POSITION position=glyphList.GetHeadPosition();
	while (position)
	{
		Glyph *current=glyphList.GetNext(position);

		if (!current->IsValid())
			continue;

		if (current->mLeft>glyph.mRight || current->mRight<glyph.mLeft || current->mTop>glyph.mBottom || current->mBottom<glyph.mTop)
			continue;

		return true;
	}

	return false;
}

void CFontToolDlg::MakeGrid(int charWidth,int charHeight,int baseline,int alphabetFlags,bool addEmpty,const char *matchFont)
{
	m_CurrentGlyph=NULL;

	if (charWidth<2 || charHeight<2)
	{
		Errorf("CFontToolDlg::MakeGrid() - charWidth and charHeight must be at least 2 each");
		return;
	}

	if (alphabetFlags&Alphabet::kStringTableAll || alphabetFlags&Alphabet::kStringTableSome)
	{
		if (!addEmpty && !mStringEditor->IsValid())
		{
			AfxMessageBox(_T("You must Load a String Table to create an alphabet which uses the string table."));
			return;
		}
	}

	Alphabet alphabet(alphabetFlags,mStringEditor,matchFont);
	
	if (addEmpty)
	{
		int numRemoved=KillUnusedGlyphs();
		if (numRemoved)
			Displayf("Removed %d glyphs",numRemoved);

		alphabetFlags=0;
	}
	else
	{
		if (!mGlyphList.IsEmpty())
		{
			if (AfxMessageBox(_T("Warning: This will remove your current glyph boxes and their Unicode assignments.\nAre you sure you want to do this?"),MB_YESNO)!=IDYES)
				return;
		}

		KillGlyphs();
	}

	int x=0,y=0;
	int glyphNum=0;

	while (1)
	{
		if (y+charHeight>mImageHeight || x+charWidth>mImageWidth || glyphNum==kMaxGlyphs)
			break;

		Glyph *newGlyph=new Glyph();
		newGlyph->mLeft=x;
		newGlyph->mRight=x+charWidth-1;
		newGlyph->mTop=y;
		newGlyph->mBottom=y+charHeight-1;
		newGlyph->mBaseline=baseline;
		newGlyph->mWidth=charWidth+1;
		newGlyph->mUnicode=alphabet.GetUnicode(glyphNum);

		if (Overlaps(*newGlyph,mGlyphList))
			delete newGlyph;
		else
			mGlyphList.AddTail(newGlyph);

		x+=charWidth;
		if (x+charWidth>mImageWidth)
		{
			x=0;
			y+=charHeight;
		}

		glyphNum++;
	}

	Displayf("Created %d glyphs",glyphNum);
	NewCurrentGlyph();

	m_CurrentGlyph=NULL;
}

static void FillImage(gfxImage *image)
{
	memset(image->GetBits32(),0,image->GetWidth()*image->GetHeight()*4);
}

static int compare(const void *g1,const void *g2)
{
	Glyph *glyph1=*(Glyph**)g1;
	Glyph *glyph2=*(Glyph**)g2;

//	Displayf("Comparing %d and %d",int(glyph1->mUnicode),int(glyph2->mUnicode));

	if (glyph1->mUnicode>glyph2->mUnicode)
		return 1;
	else if (glyph1->mUnicode<glyph2->mUnicode)
		return -1;
	else
		return 0;
}

bool CFontToolDlg::GenerateFont()
{
	UpdateData(TRUE);

	int numGlyphs=mGlyphList.GetCount();

	if (numGlyphs==0)
	{
		AfxMessageBox(_T("No valid glyphs"));
		return false;
	}

	Glyph **sortedGlyphs=Alloca(Glyph*,mGlyphList.GetCount());
	POSITION pos=mGlyphList.GetHeadPosition();
	for (int i=0;i<numGlyphs;i++)
		sortedGlyphs[i]=mGlyphList.GetNext(pos);

	qsort((void*)sortedGlyphs,numGlyphs,sizeof(Glyph*),compare);

	mFont->Init(numGlyphs);

	Assert(mImage->GetDepth()==32);

	// Image refers to the source (an arbitrarily sized image)
	// Texture refers to the destination (usually a series of 256x256 textures

	const int kDestWidth=128;								// HACK - these should be adjustable
	const int kDestHeight=128;								//

	gfxImage *image=gfxImage::Create(kDestWidth,kDestHeight,gfxImage::gf8888);
	FillImage(image);

	bool truncateFlag=false;
	int numImages=1;

	mFont->SetNumImages(1);
	mFont->SetImage(numImages-1,image);

	int x1=Min(m_ShadowsDlg.m_ShadowX1,m_ShadowsDlg.m_ShadowX2);
	int y1=Min(m_ShadowsDlg.m_ShadowY1,m_ShadowsDlg.m_ShadowY2);
	int x2=Max(m_ShadowsDlg.m_ShadowX1,m_ShadowsDlg.m_ShadowX2);
	int y2=Max(m_ShadowsDlg.m_ShadowY1,m_ShadowsDlg.m_ShadowY2);

	int gutterLeft=Max(-x1,0);		// these are also calculated in SetShadowParameters
	int gutterRight=Max(x2,0);
	int gutterTop=Max(-y1,0);
	int gutterBottom=Max(y2,0);

	mFont->SetShadowParameters(x1,x2,y1,y2);		// this is done again in GenerateShadows

	int destX=gutterLeft,destY=gutterTop;
	int maxDestX=0,maxDestY=0;
	int maxAscent=0,maxDescent=0;

	int dstGlyphNum=0;

	for (int glyphNum=0;glyphNum<numGlyphs;glyphNum++)
	{
		Glyph *srcGlyph=sortedGlyphs[glyphNum];
		if (srcGlyph->mUnicode==0)
			continue;

		if (srcGlyph->mRight-srcGlyph->mLeft+1 >= kDestWidth-gutterRight)			// too wide to fit?
		{
			srcGlyph->mRight=srcGlyph->mLeft+kDestWidth-gutterRight-1;
			truncateFlag=true;
		}

		if (srcGlyph->mBottom-srcGlyph->mTop+1 >= kDestHeight-gutterBottom)			// too tall to fit?
		{
			srcGlyph->mBottom=srcGlyph->mTop+kDestHeight-gutterBottom-1;
			truncateFlag=true;
		}

		if (destX+srcGlyph->mRight-srcGlyph->mLeft+1 > kDestWidth-gutterRight)		// move to next line?
		{
			destX=gutterLeft;
			destY=maxDestY+gutterBottom+gutterTop+1;
		}

		if (destY+srcGlyph->mBottom-srcGlyph->mTop+1 > kDestHeight-gutterBottom)	// filled the whole texture?
		{
			// needed for the Glyphs per texture - it's silly the SetGlyphsPerTexture()
			// call was called befor mFont->SetNumImages():
			mFont->SetNumTextures(numImages+1); 

			mFont->SetGlyphsPerTexture(numImages-1,dstGlyphNum);
			image=gfxImage::Create(kDestWidth,kDestHeight,gfxImage::gf8888);
			FillImage(image);

//			Displayf("Another Texture...");

			numImages++;
			mFont->SetNumImages(numImages);
			mFont->SetImage(numImages-1,image);

			destX=gutterLeft;
			destY=gutterTop;
			maxDestX=maxDestY=0;
		}

//		Displayf("Copying from l=%d r=%d t=%d b=%d    Copying to x=%d y=%d    Max x=%d y=%d",
//			srcGlyph->mLeft,srcGlyph->mRight,srcGlyph->mTop,srcGlyph->mBottom,
//			destX,destY,maxDestX,maxDestY
//		);

		for (int y=0;y<=srcGlyph->mBottom-srcGlyph->mTop;y++)
			for (int x=0;x<=srcGlyph->mRight-srcGlyph->mLeft;x++)
			{
				gfxImageRGBA &src=mImage->GetBitsRGBA()[(srcGlyph->mTop+y)*mImageWidth + (srcGlyph->mLeft+x)];
				gfxImageRGBA &dst=image->GetBitsRGBA()[(destY+y)*kDestWidth + (destX+x)];
				dst=src;
			}

		txtFontTexGlyph *newGlyph=mFont->GetGlyph(dstGlyphNum);
		newGlyph->m_Character=srcGlyph->mUnicode;
		newGlyph->m_X=(unsigned char)destX;
		newGlyph->m_Y=(unsigned char)destY;
		newGlyph->m_W=(unsigned char)(srcGlyph->mRight-srcGlyph->mLeft+1);
		newGlyph->m_H=(unsigned char)(srcGlyph->mBottom-srcGlyph->mTop+1);
		newGlyph->m_Baseline=char(srcGlyph->mBaseline-(srcGlyph->mBottom-srcGlyph->mTop));

		newGlyph->m_X=(unsigned char)(newGlyph->m_X-(unsigned char)gutterLeft);
		newGlyph->m_Y=(unsigned char)(newGlyph->m_Y-(unsigned char)gutterTop);
		newGlyph->m_W=(unsigned char)(newGlyph->m_W+(unsigned char)(gutterLeft+gutterRight));
		newGlyph->m_H=(unsigned char)(newGlyph->m_H+(unsigned char)(gutterTop+gutterBottom));

		maxAscent=Max(maxAscent,srcGlyph->mBottom-srcGlyph->mTop-srcGlyph->mBaseline);
		maxDescent=Max(maxDescent,srcGlyph->mBaseline);

		newGlyph->m_AddWidth=(char)(srcGlyph->mWidth-newGlyph->m_W);
		dstGlyphNum++;

//		if (srcGlyph->mUnicode==_T('i') || srcGlyph->mUnicode==_T('j') || srcGlyph->mUnicode==_T('k'))
//			Displayf("%c - src: baseline=%d  top=%d  bottom=%d  CharHeight=%d    dest: y=%d  h=%d  baseline=%d",char(srcGlyph->mUnicode&0xff),
//				srcGlyph->mBaseline,srcGlyph->mTop,srcGlyph->mBottom,m_CharHeight,int(newGlyph->y),int(newGlyph->h),int(newGlyph->baseline));

//		Displayf("x=%d  y=%d  w=%d  h=%d  baseline=%d",int(newGlyph->x),int(newGlyph->y),int(newGlyph->w),int(newGlyph->h),int(newGlyph->baseline));

		destX+=srcGlyph->mRight-srcGlyph->mLeft+gutterLeft+gutterRight+1;
		int tempY=destY+srcGlyph->mBottom-srcGlyph->mTop;
		if (tempY>maxDestY)
			maxDestY=tempY;
	}

	if (dstGlyphNum!=numGlyphs)
	{
//		CString string;
//		string.Format(_T("Generated only %d glyphs out of %d"),dstGlyphNum,numGlyphs);
//		AfxMessageBox(LPCTSTR(string));

		Displayf("Generated only %d glyphs out of %d",dstGlyphNum,numGlyphs);

		mFont->DecreaseNumGlyphs(dstGlyphNum);
	}

	mFont->SetGlyphsPerTexture(numImages-1,dstGlyphNum);

	mFont->SetMaxAscent(maxAscent);
	mFont->SetMaxDescent(maxDescent);

	if (truncateFlag)
		AfxMessageBox(_T("Some Characters had to be truncated to fit on a %d x %d texture"));

	GenerateShadows();

	mFont->SetNumTextures(numImages);
	for (i=0;i<numImages;i++)
	{
		gfxImage *image=mFont->GetImage(i);
		gfxSimpleTex *texture=gfxSimpleTex::Create(image);
		mFont->SetTexture(i,texture);

		image=mFont->GetShadowImage(i);
		if (image)
		{
			texture=gfxSimpleTex::Create(image);
			mFont->SetShadowTexture(i,texture);
		}
	}

	UpdateFontParameters();

	return true;
}

static void Shade(gfxImage *srcImage,gfxImage *destImage,int srcX,int srcY,int srcW,int srcH,int destX,int destY,float alpha)
{
	int offsetX=destX-srcX;
	int offsetY=destY-srcY;

	for (int y=srcY;y<srcY+srcH;y++)
	{
		if (y+offsetY<0 || y+offsetY>=destImage->GetHeight())
			continue;

		for (int x=srcX;x<srcX+srcW;x++)
		{
			if (x+offsetX<0 || x+offsetX>=destImage->GetWidth())
				continue;

			gfxImageRGBA *src=&srcImage->GetBitsRGBA()[x+y*srcImage->GetWidth()];
			gfxImageRGBA *dest=&destImage->GetBitsRGBA()[x+offsetX+(y+offsetY)*destImage->GetWidth()];

			if (src->a)
			{
				float a=alpha*float(src->a);
				dest->a=unsigned char (Min(dest->a+a,255.f));
			}
		}
	}
}

void CFontToolDlg::GenerateShadows()
{
	int x1=Min(m_ShadowsDlg.m_ShadowX1,m_ShadowsDlg.m_ShadowX2);
	int y1=Min(m_ShadowsDlg.m_ShadowY1,m_ShadowsDlg.m_ShadowY2);
	int x2=Max(m_ShadowsDlg.m_ShadowX1,m_ShadowsDlg.m_ShadowX2);
	int y2=Max(m_ShadowsDlg.m_ShadowY1,m_ShadowsDlg.m_ShadowY2);

	int gutterLeft=Max(-x1,0);		// these are also calculated in SetShadowParameters
	int gutterRight=Max(x2,0);
	int gutterTop=Max(-y1,0);
	int gutterBottom=Max(y2,0);

	mFont->SetShadowParameters(x1,x2,y1,y2);

	float alpha=m_ShadowsDlg.GetAlpha();

	for (int i=0;i<mFont->GetNumImages();i++)
	{
		gfxImage *image=mFont->GetImage(i);
		gfxImage *shadowImage=gfxImage::Create(image->GetWidth(),image->GetHeight(),gfxImage::gf8888);

		FillImage(shadowImage);

		int width=image->GetWidth()-gutterLeft-gutterRight;
		int height=image->GetHeight()-gutterTop-gutterBottom;

		for (int y=y1;y<=y2;y++)
			for (int x=x1;x<=x2;x++)
				Shade(image,shadowImage,gutterLeft,gutterTop,width,height,x+gutterLeft,y+gutterTop,alpha);

		mFont->SetShadowImage(i,shadowImage);
	}
}

void CFontToolDlg::UpdateFontParameters()
{
	mFont->SetCharHeight(m_CharHeight);
	mFont->SetCharSpacing(m_CharSpacing);
	mFont->SetSpaceWidth(m_SpaceWidth);
}

void CFontToolDlg::ChangedFontParameters()
{
	UpdateData(true);
}

unsigned int CFontToolDlg::GetPixel(int i,int j)
{
	Assert(mImage->GetDepth()==32);
	return mImage->GetBits32()[i+j*mImage->GetWidth()];
}

bool CFontToolDlg::IsForeground(int i,int j)
{
	return (GetPixel(i,j)>>24)?true:false;
}

void CFontToolDlg::KillGlyphs()
{
	if (mGlyphList.IsEmpty())
		return;

	Glyph *glyph;
	while (!mGlyphList.IsEmpty())
	{
		glyph=mGlyphList.GetHead();
		delete glyph;
		mGlyphList.RemoveHead();
	}
}

int CFontToolDlg::KillUnusedGlyphs()
{
	if (mGlyphList.IsEmpty())
		return 0;

	int numRemoved=0;

	POSITION pos=mGlyphList.GetHeadPosition();
	while (pos)
	{
		Glyph *glyph=mGlyphList.GetNext(pos);

		if (!glyph->IsValid() || glyph->mUnicode==0)
		{
			numRemoved++;

			delete glyph;
			glyph=NULL;
			if (pos)
			{
				POSITION tempPos=pos;
				mGlyphList.GetPrev(tempPos);
				mGlyphList.RemoveAt(tempPos);
			}
			else
				mGlyphList.RemoveTail();
		}
	}

	m_CurrentGlyph=NULL;

	NewCurrentGlyph();

	return numRemoved;
}

void CFontToolDlg::SetFontName(CString name)
{
	USES_CONVERSION;
	Displayf("SetFontName - '%s'",W2A(LPCTSTR(name)));

	if (name.GetLength()==0)
	{
		mFontName=_T("Untitled");
		return;
	}

	if (name.Right(9).CompareNoCase(_T(".fontproj")) == 0)
		mFontName=name.Left(name.GetLength()-9);
	else
		mFontName=_T("Untitled");
}

void CFontToolDlg::ResizeAll()
{
	if (mGlyphList.IsEmpty())
		return;

	POSITION pos=mGlyphList.GetHeadPosition();
	while (pos)
	{
		Glyph *glyph=mGlyphList.GetNext(pos);
		ShrinkRectangle(glyph);

		if (!glyph->IsValid())
		{
			delete glyph;
			glyph=NULL;
			if (pos)
			{
				POSITION tempPos=pos;
				mGlyphList.GetPrev(tempPos);
				mGlyphList.RemoveAt(tempPos);
			}
			else
				mGlyphList.RemoveTail();
		}

		if (glyph)
			ExpandRectangle(glyph);
	}

	m_CurrentGlyph=NULL;

	NewCurrentGlyph();
}

// Font creation flags
#define D3DFONT_BOLD        0x0001
#define D3DFONT_ITALIC      0x0002

// Font rendering flags
#define D3DFONT_CENTERED    0x0001
#define D3DFONT_TWOSIDED    0x0002
#define D3DFONT_FILTERED    0x0004

void CFontToolDlg::OnDrawAll() 
{
	if (!mBitmap || !mImage)
	{
		Errorf("CFontToolDlg::OnDrawAll() - Must create image first");
		return;
	}

	if (mGlyphList.IsEmpty())
	{
		Errorf("CFontToolDlg::OnDrawAll() - Must make some glyphs first (hit Make Grid)");
		return;
	}

	if (!mWreckedAlpha)
	{
		if (AfxMessageBox(_T("Warning: This command will regenerate the alpha in your image based on an RGB threshold.  It will destroy any current alpha data in the image.\nAre you sure you want to do this?"),MB_OKCANCEL)==IDOK)
		{
			mWreckedAlpha=true;
		}
		else
			return;
	}

	// Create MFC handle to DIB
	CDC dc;
	dc.CreateCompatibleDC(NULL);

	// Create Device Independent Bitmap (DIB)
	DWORD*      pBitmapBits;
	BITMAPINFO bmi;
	ZeroMemory( &bmi.bmiHeader,  sizeof(BITMAPINFOHEADER) );
	bmi.bmiHeader.biSize        = sizeof(BITMAPINFOHEADER);
#if ENABLE_OVERSAMPLING
	bmi.bmiHeader.biWidth       =  (int)mImageWidth*2;
	bmi.bmiHeader.biHeight      = -(int)mImageHeight*2;
#else
	bmi.bmiHeader.biWidth       =  (int)mImageWidth;
	bmi.bmiHeader.biHeight      = -(int)mImageHeight;
#endif
	bmi.bmiHeader.biPlanes      = 1;
	bmi.bmiHeader.biCompression = BI_RGB;
	bmi.bmiHeader.biBitCount    = 32;

	HBITMAP bitmap=CreateDIBSection(dc.m_hDC,&bmi,DIB_RGB_COLORS,(void**)&pBitmapBits,NULL,0);

	// Copy the current gfxImage into DIB
	for (int y=0;y<mImageHeight;y++)
		for (int x=0;x<mImageWidth;x++)
		{
			u8 *src=(u8*)&mImage->GetBits32()[mImageWidth*y + x];

#if ENABLE_OVERSAMPLING
			u8 *dest1=(u8*)&pBitmapBits[(2*mImageWidth)*(y*2+0) + (x*2+0)];
			u8 *dest2=(u8*)&pBitmapBits[(2*mImageWidth)*(y*2+0) + (x*2+1)];
			u8 *dest3=(u8*)&pBitmapBits[(2*mImageWidth)*(y*2+1) + (x*2+0)];
			u8 *dest4=(u8*)&pBitmapBits[(2*mImageWidth)*(y*2+1) + (x*2+1)];

			dest1[0] = dest2[0] = dest3[0] = dest4[0] = src[0];
			dest1[1] = dest2[1] = dest3[1] = dest4[1] = src[1];
			dest1[2] = dest2[2] = dest3[2] = dest4[2] = src[2];
			dest1[3] = dest2[3] = dest3[3] = dest4[3] = src[3];

			if ((((x+y)%101)==7) && (src[0] || src[1] || src[2] || src[3]))
				Displayf("before src=%d %d %d %d",int(src[0]),int(src[1]),int(src[2]),int(src[3]));
#else
			u8 *dest=(u8*)&pBitmapBits[mImageWidth*y + x];

			dest[0] = src[0];
			dest[1] = src[1];
			dest[2] = src[2];
			dest[3] = src[3];

			if ((((x+y)%101)==7) && (src[0] || src[1] || src[2] || src[3]))
				Displayf("before src=%d %d %d %d",int(src[0]),int(src[1]),int(src[2]),int(src[3]));
#endif
		}

	// Set text rendering mode
#if ENABLE_OVERSAMPLING
	dc.SetMapMode(MM_ANISOTROPIC);
	dc.SetViewportExt(mImageWidth*2,mImageHeight*2);
	dc.SetWindowExt(mImageWidth,mImageHeight);
#else
	dc.SetMapMode(MM_TEXT);
#endif

	dc.SelectObject(bitmap);
	dc.SelectObject(m_Font);
	dc.SetTextColor(RGB(255,255,255));
	dc.SetBkColor(0x00000000);
	dc.SetTextAlign(TA_TOP);

	// Render characters into the DIB
	POSITION pos=mGlyphList.GetHeadPosition();
	while (pos)
	{
		Glyph *glyph=mGlyphList.GetNext(pos);

		CRect rect;
		rect.left=glyph->mLeft;
		rect.right=glyph->mRight+1;
		rect.top=glyph->mTop;
		rect.bottom=glyph->mBottom+1;

		CBrush brush(RGB(0,0,0));
		dc.SelectObject(brush);
		dc.Rectangle(rect);

		CString string;
		unsigned short unicode=unsigned short(glyph->mUnicode);

/**
		// render fullwidth characters as normal characters
		if (unicode>65280 && unicode<=65374)
			unicode-=65248;
**
		// render ASCII characters as fullwidth characters
		if (unicode>32 && unicode<127)
			unicode+=65248;
**/

		string.Format(_T("%c"),unicode);

		dc.DrawText(string,_tcslen(string),&rect,DT_NOPREFIX);
	}

	// Copy DIB into a gfxImage
	for (y=0;y<mImageHeight;y++)
		for (int x=0;x<mImageWidth;x++)
		{
#if ENABLE_OVERSAMPLING
			u8 *src1=(u8*)&pBitmapBits[(2*mImageWidth)*(y*2+0) + (x*2+0)];
			u8 *src2=(u8*)&pBitmapBits[(2*mImageWidth)*(y*2+1) + (x*2+0)];
			u8 *src3=(u8*)&pBitmapBits[(2*mImageWidth)*(y*2+0) + (x*2+1)];
			u8 *src4=(u8*)&pBitmapBits[(2*mImageWidth)*(y*2+1) + (x*2+1)];

			int totals[4];
			totals[0]=int(src1[0])+int(src2[0])+int(src3[0])+int(src4[0]);
			totals[1]=int(src1[1])+int(src2[1])+int(src3[1])+int(src4[1]);
			totals[2]=int(src1[2])+int(src2[2])+int(src3[2])+int(src4[2]);
			totals[3]=int(src1[3])+int(src2[3])+int(src3[3])+int(src4[3]);
			int total=totals[0]+totals[1]+totals[2]+totals[3];
			int average=total/12;
			int alpha=totals[3]/4;
#else
			u8 *src=(u8*)&pBitmapBits[mImageWidth*y + x];
			int total=int(src[0])+int(src[1])+int(src[2])+int(src[3]);
			int average=total/3;
			int alpha=src[3];

			if ((((x+y)%101)==7) && (src[0] || src[1] || src[2] || src[3]))
				Displayf("after src=%d %d %d %d",int(src[0]),int(src[1]),int(src[2]),int(src[3]));
#endif

			u8 *dest=(u8*)&mImage->GetBits32()[mImageWidth*y + x];

			if (total > 0)
			{
				if (alpha==0)
				{
					// newly rendered pixel
					dest[0]=dest[1]=dest[2]=255;
					dest[3]=(unsigned char)average;
				}
				else
				{
					// previously rendered pixel
#if ENABLE_OVERSAMPLING
					dest[0]=(u8)(totals[0]/4);
					dest[1]=(u8)(totals[1]/4);
					dest[2]=(u8)(totals[2]/4);
					dest[3]=(u8)(totals[3]/4);
#else
					dest[0]=src[0];
					dest[1]=src[1];
					dest[2]=src[2];
					dest[3]=src[3];
#endif
				}
			}
			else
				dest[0]=dest[1]=dest[2]=dest[3]=0;
		}

//	gfxSaveTargaImage("C:\\Documents and Settings\\mark\\Desktop\\Temp\\junk.tga",mImage);

	// Create gfxBitmap from the gfxImage
	if (mBitmap)
		mBitmap->Release();
	mBitmap=gfxBitmap::Create(mImage);

	// Delete DIB
	DeleteObject(bitmap);

	Redraw();
}

void CFontToolDlg::ShrinkRectangle(Glyph *glyph)
{
	bool isBackground;
	int i;

	int oldBottom=glyph->mBottom;
	int oldRight=glyph->mRight;
	int oldLeft=glyph->mLeft;

	// move left edge until hit non-background
	isBackground=false;
	do
	{
		for (i=glyph->mTop;i<=glyph->mBottom;i++)
		{
			if (IsForeground(glyph->mLeft,i))
			{
				isBackground=true;
				break;
			}
		}
		if (glyph->mLeft>=glyph->mRight)
			isBackground=true;
		if (!isBackground)
			glyph->mLeft++;
	} while (!isBackground);

	// move right edge until hit non-background
	isBackground=false;
	do {
		for (i=glyph->mTop;i<=glyph->mBottom;i++)
		{
			if (IsForeground(glyph->mRight,i))
			{
				isBackground=true;
				break;
			}
		}
		if (glyph->mLeft>=glyph->mRight)
			isBackground=true;
		if (!isBackground)
			glyph->mRight--;
	} while (!isBackground);

	// move top edge until hit non-background
	isBackground=false;
	do {
		for (i=glyph->mLeft;i<=glyph->mRight;i++)
		{
			if (IsForeground(i,glyph->mTop))
			{
				isBackground=true;
				break;
			}
		}
		if (glyph->mTop>=glyph->mBottom)
			isBackground=true;
		if (!isBackground)
			glyph->mTop++;
	} while (!isBackground);

	// move bottom edge until hit non-background
	isBackground=false;
	do {
		for (i=glyph->mLeft;i<=glyph->mRight;i++)
		{
			if (IsForeground(i,glyph->mBottom))
			{
				isBackground=true;
				break;
			}
		}
		if (glyph->mTop>=glyph->mBottom)
			isBackground=true;
		if (!isBackground)
			glyph->mBottom--;
	} while (!isBackground);

	if (glyph->mLeft!=oldLeft || glyph->mRight!=oldRight)
		glyph->mWidth=glyph->mRight-glyph->mLeft;

	glyph->mBaseline-=oldBottom-glyph->mBottom;
}

void CFontToolDlg::ExpandRectangle(Glyph *glyph)
{
	bool isBackground;
	int i;

	int oldBottom=glyph->mBottom;
	int oldRight=glyph->mRight;
	int oldLeft=glyph->mLeft;

	// move left edge until hit background then back off one
	do
	{
		glyph->mLeft--;
		if (glyph->mLeft<0)
			break;

		isBackground=true;
		for (i=glyph->mTop;i<=glyph->mBottom;i++)
		{
			if (IsForeground(glyph->mLeft,i))
			{
				isBackground=false;
				break;
			}
		}
	} while (!isBackground);
	glyph->mLeft++;

	// move right edge until hit background then back off one
	do {
		glyph->mRight++;
		if (glyph->mRight>=mImage->GetWidth())
			break;

		isBackground=true;
		for (i=glyph->mTop;i<=glyph->mBottom;i++)
		{
			if (IsForeground(glyph->mRight,i))
			{
				isBackground=false;
				break;
			}
		}
	} while (!isBackground);
	glyph->mRight--;

	// move top edge until hit background then back off one
	do {
		glyph->mTop--;
		if (glyph->mTop<0)
			break;

		isBackground=true;
		for (i=glyph->mLeft;i<=glyph->mRight;i++)
		{
			if (IsForeground(i,glyph->mTop))
			{
				isBackground=false;
				break;
			}
		}
	} while (!isBackground);
	glyph->mTop++;

	// move bottom edge until hit background then back off one
	do {
		glyph->mBottom++;
		if (glyph->mBottom>=mImage->GetHeight())
			break;

		isBackground=true;
		for (i=glyph->mLeft;i<=glyph->mRight;i++)
		{
			if (IsForeground(i,glyph->mBottom))
			{
				isBackground=false;
				break;
			}
		}
	} while (!isBackground);
	glyph->mBottom--;

	if (glyph->mLeft!=oldLeft || glyph->mRight!=oldRight)
		glyph->mWidth=glyph->mRight-glyph->mLeft;

	glyph->mBaseline-=oldBottom-glyph->mBottom;
}

void CFontToolDlg::Slice()
{
	int i,j;
	Glyph *glyph;

	KillGlyphs();

	// locate all islands
	// do top to bottom, then left to right

	j=0;
	do {
		i=0;
		do {
			if (IsForeground(i,j))
			{
				// located something... check that it is not already in a rect
				glyph=GetGlyphFromXY(i,j);
				if (!glyph)
				{
					Glyph *newGlyph=new Glyph;

					// scan for extents of connected pixels
					newGlyph->mLeft=i;
					newGlyph->mRight=i;
					newGlyph->mTop=j;
					newGlyph->mBottom=j;

					RecursePixels(i,j,newGlyph);
//					newGlyph->mRight++;					// HACK - necessary?
//					newGlyph->mBottom++;				//

					i += newGlyph->mRight-newGlyph->mLeft+1;

					mGlyphList.AddTail(newGlyph);
				}
				else
					i+=glyph->mRight-glyph->mLeft+1;
			}
			else
				i++;

		} while ((i<mImageWidth) && (mGlyphList.GetCount()<kMaxGlyphs));

		j++;

	} while ((j<mImageHeight) && (mGlyphList.GetCount()<kMaxGlyphs));

	POSITION pos=mGlyphList.GetHeadPosition();
	while (pos)
	{
		Glyph *glyph=mGlyphList.GetNext(pos);
		glyph->mWidth=glyph->mRight-glyph->mLeft;
		glyph->mBaseline=0;
	}

	Displayf("Generated %d glyphs",mGlyphList.GetCount());

	NewCurrentGlyph();
}

Glyph *CFontToolDlg::GetGlyphFromXY(int x,int y)
{
	if (mGlyphList.IsEmpty())
		return NULL;

	POSITION pos=mGlyphList.GetHeadPosition();
	while (pos)
	{
		Glyph *glyph=mGlyphList.GetNext(pos);
		if (glyph->ContainsPoint(x+SB_Horz.GetScrollPos(),y+SB_Vert.GetScrollPos()))
			return glyph;
	}

	return NULL;
}

/* given a rectangle that encloses at least part of the non-background pixels, 
	and a test point to depart from, look for min's and max's */
void CFontToolDlg::RecursePixels(int i, int j, Glyph * rect)
{
	int ii = i;
	int jj = j;
	Glyph localrect;

	do
	{
		localrect = *rect;

		while ( ((ii>0)&&(jj>0)) &&
				(((ii-1) < rect->mLeft) || ((jj-1) < rect->mTop)) &&
				IsForeground(ii-1,jj-1))
				{
					// keep moving left and up
					ii--;
					jj--;
				}
		if ((ii != i) || (jj != j))
		{
			if (ii < rect->mLeft)
				rect->mLeft = ii;
			if (jj < rect->mTop)
				rect->mTop = jj;
		}

		ii = i;
		jj = j;
		while ( (jj>0) &&
				((jj-1) < rect->mTop) &&
				IsForeground(ii,jj-1))
				{
					// keep moving up
					jj--;
				}
		if (jj != j)
		{
			if (jj < rect->mTop)
				rect->mTop = jj;
		}

		ii = i;
		jj = j;
		while ( ((ii<(mImageWidth-1)) && (jj>0)) &&
				(((ii+1) > rect->mRight) || ((jj-1) < rect->mTop)) &&
				IsForeground(ii+1,jj-1))
				{
					// keep moving right and up
					ii++;
					jj--;
				}
		if ((ii != i) || (jj != j))
		{
			if (ii > rect->mRight)
				rect->mRight = ii;
			if (jj < rect->mTop)
				rect->mTop = jj;
		}

		ii = i;
		jj = j;
		while ( (ii>0) &&
				((ii-1) < rect->mLeft) &&
				IsForeground(ii-1,jj))
				{
					// keep moving left
					ii--;
				}
		if (ii != i)
		{
			if (ii < rect->mLeft)
				rect->mLeft = ii;
		}

		ii = i;
		jj = j;
		while ( (ii<(mImageWidth-1)) &&
				((ii+1) > rect->mRight) &&
				IsForeground(ii+1,jj))
				{
					// keep moving right
					ii++;
				}
		if (ii != i)
		{
			if (ii > rect->mRight)
				rect->mRight = ii;
		}

		ii = i;
		jj = j;
		while ( ((ii>0) && (jj<(mImageHeight-1))) &&
				(((ii-1) < rect->mLeft) || ((jj+1) > rect->mBottom)) &&
				IsForeground(ii-1,jj+1))
				{
					ii--;
					jj++;
				}
		if ((ii != i) || (jj != j))
		{
			// move down and left
			if (ii < rect->mLeft)
				rect->mLeft = ii;
			if (jj > rect->mBottom)
				rect->mBottom = jj;
		}

		ii = i;
		jj = j;
		while ( (jj<(mImageHeight-1)) &&
				((jj+1) > rect->mBottom) &&
				IsForeground(ii,jj+1))
				{
					// keep moving down
					jj++;
				}
		if (jj != j)
		{
			if (jj > rect->mBottom)
				rect->mBottom = jj;
		}

		ii = i;
		jj = j;
		while ( ((ii<(mImageWidth-1)) && (jj<(mImageHeight-1))) &&
				(((ii+1) > rect->mRight) || ((jj+1) > rect->mBottom)) &&
				IsForeground(ii+1,jj+1))
				{
					// keep moving right and down
					ii++;
					jj++;
				}
		if ((ii != i) || (jj != j))
		{
			if (ii > rect->mRight)
				rect->mRight = ii;
			if (jj > rect->mBottom)
				rect->mBottom = jj;
		}

		if ((localrect.mLeft != rect->mLeft) ||
			(localrect.mTop != rect->mTop) ||
			(localrect.mRight != rect->mRight) ||
			(localrect.mBottom != rect->mBottom))
		{
			localrect = *rect;

			for (jj=localrect.mTop; jj<=localrect.mBottom; jj++)
				for (ii=localrect.mLeft; ii<=localrect.mRight; ii++)
					RecursePixels( ii, jj, rect );
		}

	} while ((localrect.mLeft != rect->mLeft) ||
			(localrect.mTop != rect->mTop) ||
			(localrect.mRight != rect->mRight) ||
			(localrect.mBottom != rect->mBottom));
}

void CFontToolDlg::NewCurrentGlyph()
{
	if (!m_CurrentGlyph)
	{
		GetDlgItem(IDC_UNICODESPIN)->EnableWindow(false);
		GetDlgItem(IDC_UNICODETEXT)->EnableWindow(false);
		GetDlgItem(IDC_UNICODEVALUE)->EnableWindow(false);
		GetDlgItem(IDC_GLYPHLEFT)->EnableWindow(false);
		GetDlgItem(IDC_GLYPHRIGHT)->EnableWindow(false);
		GetDlgItem(IDC_GLYPHTOP)->EnableWindow(false);
		GetDlgItem(IDC_GLYPHBOTTOM)->EnableWindow(false);
		GetDlgItem(IDC_GLYPHBASELINE)->EnableWindow(false);
		GetDlgItem(IDC_GLYPHWIDTH)->EnableWindow(false);
		GetDlgItem(IDC_GLYPHLEFTSPIN)->EnableWindow(false);
		GetDlgItem(IDC_GLYPHRIGHTSPIN)->EnableWindow(false);
		GetDlgItem(IDC_GLYPHTOPSPIN)->EnableWindow(false);
		GetDlgItem(IDC_GLYPHBOTTOMSPIN)->EnableWindow(false);
		GetDlgItem(IDC_GLYPHBASELINESPIN)->EnableWindow(false);
		GetDlgItem(IDC_GLYPHWIDTHSPIN)->EnableWindow(false);
	}
	else
	{
		GetDlgItem(IDC_UNICODESPIN)->EnableWindow(true);
		GetDlgItem(IDC_UNICODETEXT)->EnableWindow(true);
		GetDlgItem(IDC_UNICODEVALUE)->EnableWindow(true);
		GetDlgItem(IDC_GLYPHLEFT)->EnableWindow(true);
		GetDlgItem(IDC_GLYPHRIGHT)->EnableWindow(true);
		GetDlgItem(IDC_GLYPHTOP)->EnableWindow(true);
		GetDlgItem(IDC_GLYPHBOTTOM)->EnableWindow(true);
		GetDlgItem(IDC_GLYPHBASELINE)->EnableWindow(true);
		GetDlgItem(IDC_GLYPHWIDTH)->EnableWindow(true);
		GetDlgItem(IDC_GLYPHLEFTSPIN)->EnableWindow(true);
		GetDlgItem(IDC_GLYPHRIGHTSPIN)->EnableWindow(true);
		GetDlgItem(IDC_GLYPHTOPSPIN)->EnableWindow(true);
		GetDlgItem(IDC_GLYPHBOTTOMSPIN)->EnableWindow(true);
		GetDlgItem(IDC_GLYPHBASELINESPIN)->EnableWindow(true);
		GetDlgItem(IDC_GLYPHWIDTHSPIN)->EnableWindow(true);

		m_UnicodeValue=m_CurrentGlyph->mUnicode;
		m_UnicodeText.Format(_T("%c"),m_CurrentGlyph->mUnicode);

		m_GlyphLeft=m_CurrentGlyph->mLeft;
		m_GlyphRight=m_CurrentGlyph->mRight;
		m_GlyphTop=m_CurrentGlyph->mTop;
		m_GlyphBottom=m_CurrentGlyph->mBottom;
		m_GlyphBaseline=m_CurrentGlyph->mBaseline;
		m_GlyphWidth=m_CurrentGlyph->mWidth;
	}

	UpdateData(false);
}

void CFontToolDlg::ChangedEditGlyph()
{
	if (m_CurrentGlyph)
	{
		UpdateData(true);

		m_CurrentGlyph->mLeft=m_GlyphLeft;
		m_CurrentGlyph->mRight=m_GlyphRight;
		m_CurrentGlyph->mTop=m_GlyphTop;
		m_CurrentGlyph->mBottom=m_GlyphBottom;
		m_CurrentGlyph->mBaseline=m_GlyphBaseline;
		m_CurrentGlyph->mWidth=m_GlyphWidth;
	}
}

void CFontToolDlg::EnableFontWidgets()
{
	UpdateData(false);

	GetDlgItem(IDC_CHARHEIGHT)->EnableWindow(true);
	GetDlgItem(IDC_CHARHEIGHTSPIN)->EnableWindow(true);
	GetDlgItem(IDC_CHARSPACING)->EnableWindow(true);
	GetDlgItem(IDC_CHARSPACINGSPIN)->EnableWindow(true);
	GetDlgItem(IDC_SPACEWIDTH)->EnableWindow(true);
	GetDlgItem(IDC_SPACEWIDTHSPIN)->EnableWindow(true);
}

void CFontToolDlg::DrawTestText()
{
	if (mFont->GetGlyph(0))
	{
		vglColor4f(1,1,0,0);
		vglBegin(drawLine,2);
		vglVertex2f(0,512);
		vglVertex2f(float(mImageWidth),512);
		vglEnd();

		vglColor(0xffffffff);

		if (m_CurrentString)
		{
			int xsize=0,ysize=0;
			mFont->ComputeExtents(&xsize,&ysize,m_CurrentString->GetString());

			mFont->Draw(0,512-ysize,m_CurrentString->GetString());
			mFont->Draw(0,512,m_CurrentString->GetString());
			mFont->Draw(0,512+ysize,m_CurrentString->GetString());

//			mFont->DrawExtents(0,512-ysize,m_CurrentString->mString);
//			mFont->DrawExtents(0,512,m_CurrentString->mString);
//			mFont->DrawExtents(0,512+ysize,m_CurrentString->mString);
		}
		else
		{
//			const _TCHAR *text=TEXT("ijk");
			const _TCHAR *text=TEXT("ABCDEFGHIJKLMNOPQRSTUVWXYZ\nTHIS IS just a TEST!\nabcdefghijklmnopqrstuvwxyz\n0123456789");
			int xsize=0,ysize=0;
			mFont->ComputeExtents(&xsize,&ysize,text);

//			mFont->Draw(1,512-ysize,text);
			mFont->Draw(1,512,text);
//			mFont->Draw(1,512+ysize,text);
//			mFont->DrawExtents(1,512-ysize,text);
			mFont->DrawExtents(1,512,text);
//			mFont->DrawExtents(1,512+ysize,text);
		}
	}
}

void CFontToolDlg::OnTimer(UINT nIDEvent) 
{
	Update();
	Draw();
		
	CDialog::OnTimer(nIDEvent);
}

void CFontToolDlg::OnRunning() 
{
	if (m_Running.GetCheck())
		SetTimer(1,30,NULL);
	else
		KillTimer(1);
}

void CFontToolDlg::OnViewport() 
{
	Displayf("OnViewport()");
}

void CFontToolDlg::OnLButtonDown(UINT nFlags, CPoint point) 
{
	CPoint relativePoint;
	Glyph *glyph=NULL;

	int lastUnicode=0;
	if (m_CurrentGlyph)
		lastUnicode=m_CurrentGlyph->mUnicode;

	if (TestClick(point,&relativePoint,&glyph))
	{
		m_CurrentGlyph=glyph;
		NewCurrentGlyph();

		CRectTracker tracker;
		if (tracker.TrackRubberBand(this, point, true))
		{
			// see if rubber band intersects with the doc's tracker
			CRect rectT;
			tracker.m_rect.NormalizeRect(); // so intersect rect works

			CRect rect;
			m_Viewport.GetClientRect(rect);

			tracker.m_rect.left=Max(tracker.m_rect.left,0);
			tracker.m_rect.right=Min(tracker.m_rect.right,rect.right-1);
			tracker.m_rect.top=Max(tracker.m_rect.top,0);
			tracker.m_rect.bottom=Min(tracker.m_rect.bottom,rect.bottom-1);

			if (tracker.m_rect.Width()<10 || tracker.m_rect.Height()<10)
			{
				m_CurrentGlyph=glyph;
				NewCurrentGlyph();

				if (GetAsyncKeyState(VK_CONTROL)&0x8000)
				{
					Displayf("Control down");
					if (lastUnicode && m_CurrentGlyph)
					{
						Displayf("Assigning sequential UNICODE %d",lastUnicode+1);
						m_CurrentGlyph->mUnicode=lastUnicode+1;
						NewCurrentGlyph();
					}
				}

				CDialog::OnLButtonDown(nFlags,point);
				Redraw();

				return;							// assume user meant to click, not drag
			}
//			Displayf("selected %d %d %d %d",tracker.m_rect.left,tracker.m_rect.right,tracker.m_rect.top,tracker.m_rect.bottom);

			Glyph *newGlyph=new Glyph();
			newGlyph->mLeft=tracker.m_rect.left+SB_Horz.GetScrollPos();
			newGlyph->mRight=tracker.m_rect.right+SB_Horz.GetScrollPos();
			newGlyph->mTop=tracker.m_rect.top+SB_Vert.GetScrollPos();
			newGlyph->mBottom=tracker.m_rect.bottom+SB_Vert.GetScrollPos();
			newGlyph->mBaseline=int(0.25f*(tracker.m_rect.bottom-tracker.m_rect.top));
			newGlyph->mWidth=tracker.m_rect.Width();
			newGlyph->mUnicode=0;
			mGlyphList.AddTail(newGlyph);

			m_CurrentGlyph=newGlyph;
			NewCurrentGlyph();
		}
	}

	if (GetAsyncKeyState(VK_CONTROL)&0x8000)
	{
		Displayf("Control down");
		if (lastUnicode && m_CurrentGlyph)
		{
			Displayf("Assigning sequential UNICODE %d",lastUnicode+1);
			m_CurrentGlyph->mUnicode=lastUnicode+1;
			NewCurrentGlyph();
		}
	}

	CDialog::OnLButtonDown(nFlags,point);
	Redraw();
}

void CFontToolDlg::OnRButtonDown(UINT nFlags, CPoint point) 
{
	CPoint relativePoint;
	Glyph *glyph=NULL;

	if (TestClick(point,&relativePoint,&glyph))
	{
		m_CurrentGlyph=glyph;
		NewCurrentGlyph();

		Redraw();

		if (glyph)
		{
			CMenu menu;
			if (!menu.LoadMenu(IDR_GLYPH_POPUP_MENU))
			{
				Errorf("LoadMenu failed");
				CDialog::OnRButtonDown(nFlags,point);
				Redraw();
				return;
			}
			CMenu *popup=menu.GetSubMenu(0);
			Assert(popup);
			CRect rect;
			GetClientRect(&rect);
			ClientToScreen(&rect);
			popup->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON,rect.left+point.x,rect.top+point.y,AfxGetMainWnd());
		}
	}

	Redraw();

	CDialog::OnRButtonDown(nFlags, point);
}

bool CFontToolDlg::TestClick(const CPoint &point,CPoint *relativePoint,Glyph **glyph)
{
	CRect rect;
	GetClientRect(&rect);

	CPoint screenPoint(point);
	ClientToScreen(&screenPoint);

	CRect screenRect;
	m_Viewport.GetClientRect(&screenRect);
	m_Viewport.ClientToScreen(&screenRect);

	if (screenPoint.x<screenRect.left || screenPoint.x>screenRect.right || screenPoint.y<screenRect.top || screenPoint.y>screenRect.bottom)
	{
		if (relativePoint)
			relativePoint->x=relativePoint->y=0;
		if (glyph)
			*glyph=NULL;
		return false;
	}

	*relativePoint=screenPoint-screenRect.TopLeft();

	CRect size=screenRect;
	m_Viewport.ScreenToClient(&size);

	*glyph=GetGlyphFromXY(relativePoint->x,relativePoint->y);

	return true;
}

void CFontToolDlg::OnNewFont() 
{
	m_CurrentGlyph=NULL;			// HACK - trying to prevent a crash bug

	NewFontDlg dlg;
	int nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with OK
	}
	else if (nResponse == IDCANCEL)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with Cancel

		return;
	}

	if (mImage)
	{
		if (AfxMessageBox(_T("This will discard your current font"),MB_OKCANCEL|MB_APPLMODAL|MB_ICONEXCLAMATION) != IDOK)
			return;

		mImage->Release();
		mImage=NULL;

		if (mBitmap)
			mBitmap->Release();
		mBitmap=NULL;

		mFontName="Untitled";
		mImageWidth=0;
		mImageHeight=0;
	}

	mImageWidth=dlg.m_Width;
	mImageHeight=dlg.m_Height;

	CRect clientRect;
	m_Viewport.GetClientRect(&clientRect);

	int width	=(clientRect.right-clientRect.left);
	int height	=(clientRect.bottom-clientRect.top);

	// vertical scroll bar info:
	SB_Vert.SetScrollPos(0);
	SB_Vert.SetScrollRange(0,mImageHeight-height);

	// horizontal scroll bar info:
	SB_Horz.SetScrollPos(0);
	SB_Horz.SetScrollRange(0,mImageWidth-width);
	
	SB_Vert.EnableScrollBar((mImageHeight>height)?ESB_ENABLE_BOTH:ESB_DISABLE_BOTH);
	SB_Horz.EnableScrollBar((mImageWidth>width)?ESB_ENABLE_BOTH:ESB_DISABLE_BOTH);

	mImage=gfxImage::Create(mImageWidth,mImageHeight,gfxImage::gf8888);

	const gfxImageRGBA clearColor={0,0,0,0};
	for (int i=0;i<mImage->GetWidth()*mImage->GetHeight();i++)
		mImage->GetBitsRGBA()[i]=clearColor;

	Assert(!mBitmap);
	mBitmap=gfxBitmap::Create(mImage);

	mWreckedAlpha=true;

	Displayf("Created new font");

	EnableFontWidgets();

	Redraw();
}

void CFontToolDlg::OnLoadFont() 
{
	CFileDialog fileDialog(TRUE,_T("fonttex"),NULL,OFN_HIDEREADONLY,
		_T("Font Projects (*.fontproj)|*.fontproj|Image Files (*.tga;*.tex)|*.tga; *.tex|All Files (*.*)|*.*||"));
	
	fileDialog.m_ofn.lpstrTitle = _T("Open Image or Font Project");

	if (fileDialog.DoModal()==IDOK)
	{
		if (fileDialog.GetFileExt().CompareNoCase(_T("fontproj")) == 0)
		{
			mFontName=fileDialog.GetPathName().Left(fileDialog.GetPathName().GetLength()-9);

			USES_CONVERSION;
			Displayf("Loading Font Project '%s.fontproj'",W2A(LPCTSTR(mFontName)));
			LoadFontProject();
			LoadFontImage();
//			ChangedFontParameters();
//UpdateData(false);
			EnableFontWidgets();
		}
		else if (!fileDialog.GetFileExt().CompareNoCase(_T("tga")) || !fileDialog.GetFileExt().CompareNoCase(_T("tex")))
		{
			USES_CONVERSION;
			const char *pathname=W2A(LPCTSTR(fileDialog.GetPathName()));
			Displayf("Loading Image '%s'",pathname);

			gfxImage *newImage=gfxLoadImage(pathname,false,false);
			if (!newImage)
			{
				char buffer[256];
				sprintf(buffer,"Failed to load image '%s'",pathname);
				AfxMessageBox(A2W(buffer));
				Redraw();
				return;
			}

			if (newImage->GetDepth()!=gfxImage::gf8888)
			{
				AfxMessageBox(_T("Image must be in 32-bit RGBA"));
				newImage->Release();
				Redraw();
				return;
			}

			if (mImage)
				mImage->Release();

			mImage=newImage;

			if (mBitmap)
				mBitmap->Release();

			mBitmap=gfxBitmap::Create(mImage);
			Assert(mBitmap);

			mFontName=fileDialog.GetPathName().Left(fileDialog.GetPathName().GetLength()-4);

			mImageWidth=mImage->GetWidth();
			mImageHeight=mImage->GetHeight();

			EnableFontWidgets();
		}
		else
		{
			AfxMessageBox(_T("Unrecognized file extension"));
		}
	}

	Redraw();
}

void CFontToolDlg::Serialize(CArchive &archive)
{
	if (archive.IsStoring())
	{
		archive << mGlyphList.GetCount();

		POSITION pos=mGlyphList.GetHeadPosition();
		while (pos)
		{
			Glyph *glyph=mGlyphList.GetNext(pos);
			glyph->Serialize(archive);
		}

		archive << m_CharHeight;
		archive << m_CharSpacing;
		archive << m_SpaceWidth;

		archive << m_ShadowsDlg.m_ShadowX1;
		archive << m_ShadowsDlg.m_ShadowX2;
		archive << m_ShadowsDlg.m_ShadowY1;
		archive << m_ShadowsDlg.m_ShadowY2;
		archive << m_ShadowsDlg.m_ShadowAlpha;

		archive << m_4BitTextures;

		m_MakeGridDlg.Serialize(archive);
//		m_Font->Serialize(archive);					// wouldn't this be cool!
	}
	else
	{
		int numGlyphs=0;
		archive >> numGlyphs;

		for (int i=0;i<numGlyphs;i++)
		{
			Glyph *glyph=new Glyph;
			glyph->Serialize(archive);
			mGlyphList.AddTail(glyph);
		}

		archive >> m_CharHeight;
		archive >> m_CharSpacing;
		archive >> m_SpaceWidth;

		int temp;
		archive >> temp;			// HACK - don't choke on old project files
		if (temp<=5)
		{
			m_ShadowsDlg.m_ShadowX1=temp;			// this is a future compatibility problem
//			archive >> m_ShadowsDlg.m_ShadowX1;		//

			archive >> m_ShadowsDlg.m_ShadowX2;
			archive >> m_ShadowsDlg.m_ShadowY1;
			archive >> m_ShadowsDlg.m_ShadowY2;
			archive >> m_ShadowsDlg.m_ShadowAlpha;
		}
		else
		{
			m_ShadowsDlg.m_ShadowX1=0;
			m_ShadowsDlg.m_ShadowX2=0;
			m_ShadowsDlg.m_ShadowY1=0;
			m_ShadowsDlg.m_ShadowY2=0;
			m_ShadowsDlg.m_ShadowAlpha=0;
		}

		// version 2.1 and greater features:
		if (Version>=0x21000)
		{
			archive >> m_4BitTextures;
		}
		else 
			m_4BitTextures=FALSE;

		if (temp<=5)
		{
			m_MakeGridDlg.Serialize(archive);
//			m_Font->Serialize(archive);				// wouldn't this be cool!
		}
	}

	m_ShadowsDlg.UpdateData(false);
	m_MakeGridDlg.UpdateData(false);
}

void CFontToolDlg::LoadFontProject()
{
	KillGlyphs();

	CString pathname=mFontName+_T(".fontproj");

	CFile file;
	file.Open(pathname,CFile::modeRead);
	CArchive archive(&file,CArchive::load);


	CString string;
	archive >> string;
	if (!string.Compare(_T("Font Project File")))
	{
		Version=0;
	}
	else if (!string.Compare(_T("Font Project File Ver. 2.0")))
	{
		Version=0x20000;
	}
	else if (!string.Compare(_T("Font Project File Ver. 2.1")))
	{
		Version=0x21000;
	}
	else
	{
		AfxMessageBox(_T("Not a valid Font Project File"));
		archive.Close();
		file.Close();
		return;
	}

	Serialize(archive);

	archive.Close();
	file.Close();
}

void CFontToolDlg::LoadFontImage()
{
	USES_CONVERSION;
	const char *pathname=W2A(LPCTSTR(mFontName));
	Displayf("Loading Image '%s'",pathname);

	if (mImage)
		mImage->Release();

	mImage=gfxLoadImage(pathname,false,false);
	if (!mImage)
		Quitf("Failed to load image '%s'",pathname);

	if (mBitmap)
		mBitmap->Release();

	mBitmap=gfxBitmap::Create(mImage);
	Assert(mBitmap);

	mImageWidth=mImage->GetWidth();
	mImageHeight=mImage->GetHeight();

	EnableFontWidgets();

	mWreckedAlpha=false;
}

void CFontToolDlg::SaveFontImage()
{
	if (!mImage)
	{
		Errorf("No image to save");
		return;
	}

	USES_CONVERSION;
	const char *pathname=W2A(LPCTSTR(mFontName));

	Displayf("Saving Image to '%s.tga'",pathname);
	if (!gfxSaveTargaImage(pathname,mImage))
	{
		AfxMessageBox(_T("Failed to save image file (write protected?)"));
		return;
	}
}

void CFontToolDlg::SaveFontProject()
{
	CString pathname=mFontName+_T(".fontproj");

	CFile file;
	if (!file.Open(pathname,CFile::modeWrite|CFile::modeCreate))
	{
		AfxMessageBox(_T("Failed to save project file (write protected?)"));
		return;
	}

	CArchive archive(&file,CArchive::store);

	archive << CString("Font Project File Ver. 2.1");

	Serialize(archive);

	archive.Close();
	file.Close();
}

void CFontToolDlg::OnSaveFont() 
{
//	if (!GenerateFont())
//		return;

	CString defaultFilename=mFontName+_T(".fontproj");

	CFileDialog fileDialog(FALSE,_T("fontproj"),LPCTSTR(defaultFilename),OFN_HIDEREADONLY,
		_T("Font Projects (*.fontproj)|*.fontproj|All Files (*.*)|*.*||"));

	fileDialog.m_ofn.lpstrTitle = _T("Save Font Project");

	if (fileDialog.DoModal()==IDOK)
	{
		if (fileDialog.GetFileExt().CompareNoCase(_T("fontproj")) == 0)
		{
			USES_CONVERSION;
			const char *pathname=W2A(LPCTSTR(fileDialog.GetPathName()));
			Displayf("Saving Font Project '%s'",pathname);

			mFontName=fileDialog.GetPathName().Left(fileDialog.GetPathName().GetLength()-9);

			SaveFontProject();

			CString message;
			
			if (AfxMessageBox(_T("Would you also like to save the corresponding Targa Image?"),MB_YESNO)==IDYES)
				SaveFontImage();
		}
		else
		{
			AfxMessageBox(_T("Unrecognized file extension.  If you are trying to save a 'fonttex' or 'tga' file then use the 'Export' button instead."));
		}
	}

	Redraw();
}

void CFontToolDlg::OnExportFont() 
{
	if (!GenerateFont())
		return;

	CString defaultFilename=mFontName+_T(".fonttex");

	CFileDialog fileDialog(FALSE,_T("fonttex"),LPCTSTR(defaultFilename),OFN_HIDEREADONLY,
		_T("Font Files (*.fonttex)|*.fonttex|Image Files (*.tga)|*.tga|All Files (*.*)|*.*||"));

	fileDialog.m_ofn.lpstrTitle = _T("Export Font or Image File");

	if (fileDialog.DoModal()==IDOK)
	{
		if (fileDialog.GetFileExt().CompareNoCase(_T("fonttex")) == 0)
		{
			USES_CONVERSION;

			const char *pathname=W2A(LPCTSTR(fileDialog.GetPathName()));
			Displayf("pathname='%s'",pathname);

			if (!mFont->SaveFontInfo(pathname))
			{
				AfxMessageBox(_T("Failed to save .fonttex file (write protected?)"));
				return;
			}

			CString temp=fileDialog.GetPathName().Left(fileDialog.GetPathName().GetLength()-8);
			const char *texturePathname=W2A(LPCTSTR(temp));

			CWaitCursor wait;

			if (!mFont->SaveFontTexturesQuantized(texturePathname,m_4BitTextures!=FALSE))
			{
				AfxMessageBox(_T("Failed to save texture files (write protected?)"));
				return;
			}

			if (!mFont->SaveFontShadowsQuantized(texturePathname,m_4BitTextures!=FALSE))
			{
				AfxMessageBox(_T("Failed to save font shadow files (write protected?)"));
				return;
			}

			if (!mFont->SaveFontTexturesCompressed(texturePathname))
			{
				AfxMessageBox(_T("Failed to save texture files (write protected?)"));
				return;
			}

			if (!mFont->SaveFontShadowsCompressed(texturePathname))
			{
				AfxMessageBox(_T("Failed to save font shadow files (write protected?)"));
				return;
			}
		}
		else if (fileDialog.GetFileExt().CompareNoCase(_T("tga")) == 0)
		{
			if (!mImage)
			{
				Errorf("No image to save");
				return;
			}

			CString cpathname=fileDialog.GetPathName().Left(fileDialog.GetPathName().GetLength()-4);

			USES_CONVERSION;
			const char *pathname=W2A(LPCTSTR(cpathname));

			Displayf("Saving Image to '%s.tga'",pathname);
			if (!gfxSaveTargaImage(pathname,mImage))
			{
				AfxMessageBox(_T("Failed to save image file (write protected?)"));
				return;
			}
		}
	}

	Redraw();
}

void CFontToolDlg::OnGenerateFont() 
{
	if (!mImage)
	{
		AfxMessageBox(_T("Create a new font image before hitting Generate"));
		return;
	}

	if (!GenerateFont())
		AfxMessageBox(_T("Error generating font"));

	Redraw();
}

void CFontToolDlg::OnMakeGrid() 
{
	if (m_MakeGridDlg.IsWindowVisible())
		m_MakeGridDlg.ShowWindow(SW_HIDE);
	else
	{
		m_MakeGridDlg.UpdateDialog();
		m_MakeGridDlg.ShowWindow(SW_SHOW);
	}
}

void CFontToolDlg::OnResizeAll()
{
	ResizeAll();

	Redraw();
}

void CFontToolDlg::OnLoadStringTable()
{
	CFileDialog fileDialog(TRUE,_T("txt"),NULL,OFN_HIDEREADONLY,
		_T("Text Files (*.txt)|*.txt|All Files (*.*)|*.*||"));
	
	fileDialog.m_ofn.lpstrTitle=_T("Open String Table File");

	if (fileDialog.DoModal()==IDOK)
	{
		if (fileDialog.GetFileExt().CompareNoCase(_T("txt")) == 0)
		{
			USES_CONVERSION;
			const char *pathname=W2A(LPCTSTR(fileDialog.GetPathName()));
			Displayf("Loading String Table '%s'",pathname);

			mStringEditor->Load(pathname);
//			mStringEditor->Print();

			Redraw();

			m_Language.EnableWindow(true);
		}
	}

	int numStringEntries=mStringEditor->GetNumStringEntries();

	if (numStringEntries==0)
		return;

	SCROLLINFO si;
	si.fMask=SIF_RANGE|SIF_POS|SIF_PAGE;
	si.nMin=0;
	si.nMax=numStringEntries-1;
	si.nPage=1;
	si.nPos=0;
	m_StringScrollBar.SetScrollInfo(&si,true);

	UpdateStringWidgets();

	Redraw();
}

void CFontToolDlg::OnSaveStringTable() 
{
	CFileDialog fileDialog(FALSE,_T("strtbl"),_T(""),OFN_HIDEREADONLY,
		_T("String Table Files (*.strtbl)|*.strtbl|All Files (*.*)|*.*||"));

	fileDialog.m_ofn.lpstrTitle = _T("Save String Table");

	if (fileDialog.DoModal()==IDOK)
	{
		if (fileDialog.GetFileExt().CompareNoCase(_T("strtbl")) == 0)
		{
			USES_CONVERSION;

			CString temp=fileDialog.GetPathName().Left(fileDialog.GetPathName().GetLength()-7);
			for (int language=0;language<txtLanguage::GetCount();language++)
				if (temp.Right(2)==A2W(txtLanguage::mAbbreviations[language]) && temp[temp.GetLength()-3]==_T('_'))
					temp=temp.Left(temp.GetLength()-3);

			const char *basePathname=W2A(LPCTSTR(temp));
			mStringEditor->Export(basePathname);
		}
	}
}

void CFontToolDlg::OnEditChangeLanguage() 
{
	UpdateStringWidgets();

	Redraw();
}

void CFontToolDlg::OnDrawGlyph()
{
	if (!mBitmap || !mImage)
	{
		Errorf("CFontToolDlg::OnDrawGlyph() - Must create image first");
		return;
	}

	if (!m_CurrentGlyph)
	{
		Errorf("Must select a glyph to draw");
		return;
	}

	if (!mWreckedAlpha)
	{
		if (AfxMessageBox(_T("Warning: This command will regenerate the alpha in your image based on an RGB threshold.  It will destroy any current alpha data in the image.\nAre you sure you want to do this?"),MB_OKCANCEL)==IDOK)
		{
			mWreckedAlpha=true;
		}
		else
			return;
	}

	// Create MFC handle to DIB
	CDC dc;
	dc.CreateCompatibleDC(NULL);

	// Create Device Independent Bitmap (DIB)
	DWORD*      pBitmapBits;
	BITMAPINFO bmi;
	ZeroMemory( &bmi.bmiHeader,  sizeof(BITMAPINFOHEADER) );
	bmi.bmiHeader.biSize        = sizeof(BITMAPINFOHEADER);
#if ENABLE_OVERSAMPLING
	bmi.bmiHeader.biWidth       =  (int)mImageWidth*2;
	bmi.bmiHeader.biHeight      = -(int)mImageHeight*2;
#else
	bmi.bmiHeader.biWidth       =  (int)mImageWidth;
	bmi.bmiHeader.biHeight      = -(int)mImageHeight;
#endif
	bmi.bmiHeader.biPlanes      = 1;
	bmi.bmiHeader.biCompression = BI_RGB;
	bmi.bmiHeader.biBitCount    = 32;

	HBITMAP bitmap=CreateDIBSection(dc.m_hDC,&bmi,DIB_RGB_COLORS,(void**)&pBitmapBits,NULL,0);

	// Copy the current gfxImage into DIB
	for (int y=0;y<mImageHeight;y++)
		for (int x=0;x<mImageWidth;x++)
		{
			u8 *src=(u8*)&mImage->GetBits32()[mImageWidth*y + x];

#if ENABLE_OVERSAMPLING
			u8 *dest1=(u8*)&pBitmapBits[(2*mImageWidth)*(y*2+0) + (x*2+0)];
			u8 *dest2=(u8*)&pBitmapBits[(2*mImageWidth)*(y*2+0) + (x*2+1)];
			u8 *dest3=(u8*)&pBitmapBits[(2*mImageWidth)*(y*2+1) + (x*2+0)];
			u8 *dest4=(u8*)&pBitmapBits[(2*mImageWidth)*(y*2+1) + (x*2+1)];

			dest1[0] = dest2[0] = dest3[0] = dest4[0] = src[0];
			dest1[1] = dest2[1] = dest3[1] = dest4[1] = src[1];
			dest1[2] = dest2[2] = dest3[2] = dest4[2] = src[2];
			dest1[3] = dest2[3] = dest3[3] = dest4[3] = src[3];
#else
			u8 *dest=(u8*)&pBitmapBits[mImageWidth*y + x];

			dest[0] = src[0];
			dest[1] = src[1];
			dest[2] = src[2];
			dest[3] = src[3];
#endif
		}

	// Set text rendering mode
#if ENABLE_OVERSAMPLING
	dc.SetMapMode(MM_ANISOTROPIC);
	dc.SetViewportExt(mImageWidth*2,mImageHeight*2);
	dc.SetWindowExt(mImageWidth,mImageHeight);
#else
	dc.SetMapMode(MM_TEXT);
#endif

	dc.SelectObject(bitmap);
	dc.SelectObject(m_Font);
	dc.SetTextColor(RGB(255,255,255));
	dc.SetBkColor(0x00000000);
	dc.SetTextAlign(TA_TOP);

	// Render character into the DIB
	Glyph *glyph=m_CurrentGlyph;

	CRect rect;
	rect.left=glyph->mLeft;
	rect.right=glyph->mRight+1;
	rect.top=glyph->mTop;
	rect.bottom=glyph->mBottom+1;

	CBrush brush(RGB(0,0,0));
	dc.SelectObject(brush);
	dc.Rectangle(rect);

	CString string;
	unsigned short unicode=(unsigned short)glyph->mUnicode;

/**
	// render fullwidth characters as normal characters
	if (unicode>65280 && unicode<=65374)
		unicode-=65248;
**
	// render ASCII characters as fullwidth characters
	if (unicode>32 && unicode<127)
		unicode+=65248;
**/

	string.Format(_T("%c"),unicode);

	dc.DrawText(string,_tcslen(string),&rect,DT_NOPREFIX);

	// Copy DIB into a gfxImage
	for (y=0;y<mImageHeight;y++)
		for (int x=0;x<mImageWidth;x++)
		{
#if ENABLE_OVERSAMPLING
			u8 *src1=(u8*)&pBitmapBits[(2*mImageWidth)*(y*2+0) + (x*2+0)];
			u8 *src2=(u8*)&pBitmapBits[(2*mImageWidth)*(y*2+1) + (x*2+0)];
			u8 *src3=(u8*)&pBitmapBits[(2*mImageWidth)*(y*2+0) + (x*2+1)];
			u8 *src4=(u8*)&pBitmapBits[(2*mImageWidth)*(y*2+1) + (x*2+1)];

			int totals[4];
			totals[0]=int(src1[0])+int(src2[0])+int(src3[0])+int(src4[0]);
			totals[1]=int(src1[1])+int(src2[1])+int(src3[1])+int(src4[1]);
			totals[2]=int(src1[2])+int(src2[2])+int(src3[2])+int(src4[2]);
			totals[3]=int(src1[3])+int(src2[3])+int(src3[3])+int(src4[3]);
			int total=totals[0]+totals[1]+totals[2]+totals[3];
			int average=total/12;
			int alpha=totals[3]/4;
#else
			u8 *src=(u8*)&pBitmapBits[mImageWidth*y + x];
			int total=int(src[0])+int(src[1])+int(src[2])+int(src[3]);
			int average=total/3;
			int alpha=src[3];
#endif

			u8 *dest=(u8*)&mImage->GetBits32()[mImageWidth*y + x];

			if (total > 0)
			{
				if (alpha==0)
				{
					dest[0]=dest[1]=dest[2]=255;
					dest[3]=(unsigned char)average;
				}
				else
				{
#if ENABLE_OVERSAMPLING
					dest[0]=(u8)(totals[0]/4);
					dest[1]=(u8)(totals[1]/4);
					dest[2]=(u8)(totals[2]/4);
					dest[3]=(u8)(totals[3]/4);
#else
					dest[0]=src[0];
					dest[1]=src[1];
					dest[2]=src[2];
					dest[3]=src[3];
#endif
				}
			}
			else
				dest[0]=dest[1]=dest[2]=dest[3]=0;
		}

//	gfxSaveTargaImage("C:\\Documents and Settings\\mark\\Desktop\\Temp\\junk.tga",mImage);

	// Create gfxTexture from the gfxImage
	if (mBitmap)
		mBitmap->Release();
	mBitmap=gfxBitmap::Create(mImage);

	// Delete DIB
	DeleteObject(bitmap);

	Redraw();
}

void CFontToolDlg::OnDeleteGlyph() 
{
	POSITION pos=mGlyphList.GetHeadPosition();
	while (pos)
	{
		Glyph *glyph=mGlyphList.GetNext(pos);
		if (glyph==m_CurrentGlyph)
		{
			if (pos)
			{
				POSITION tempPos=pos;
				mGlyphList.GetPrev(tempPos);
				mGlyphList.RemoveAt(tempPos);
			}
			else
				mGlyphList.RemoveTail();

			m_CurrentGlyph=NULL;
			NewCurrentGlyph();
			Redraw();

			return;
		}

	}

	Warningf("Couldn't find glyph to delete");

	NewCurrentGlyph();

	Redraw();
}

void CFontToolDlg::OnShrinkGlyph() 
{
	if (m_CurrentGlyph)
		ShrinkRectangle(m_CurrentGlyph);

	NewCurrentGlyph();

	Redraw();
}

void CFontToolDlg::OnExpandGlyph() 
{
	if (m_CurrentGlyph)
		ExpandRectangle(m_CurrentGlyph);

	NewCurrentGlyph();

	Redraw();
}

void CFontToolDlg::OnResizeGlyph() 
{
	if (m_CurrentGlyph)
		ShrinkRectangle(m_CurrentGlyph);

	NewCurrentGlyph();

	if (m_CurrentGlyph)
		ExpandRectangle(m_CurrentGlyph);

	NewCurrentGlyph();

	Redraw();
}

void CFontToolDlg::OnPickFont() 
{
	CClientDC dc(NULL);
	LOGFONT lf = m_LogFont;
//	lf.lfHeight = -lf.lfHeight;
//	lf.lfHeight = -::MulDiv(-lf.lfHeight, dc.GetDeviceCaps(LOGPIXELSY), 72);
	CFontDialog dlg(&lf);
	dlg.m_cf.rgbColors = m_crText;
	if (dlg.DoModal() == IDOK)
	{
//		lf.lfHeight = -::MulDiv(-lf.lfHeight, 72, dc.GetDeviceCaps(LOGPIXELSY));
//		lf.lfHeight=-lf.lfHeight;
		lf.lfQuality=ANTIALIASED_QUALITY;			// undocumented but cool
		lf.lfOutPrecision=OUT_OUTLINE_PRECIS;
		m_crText = dlg.GetColor();
		m_LogFont = lf;

/**
		// convert points in m_logfont.lfHeight to logical
		m_logfont.lfHeight=-::MulDiv(-m_logfont.lfHeight,pDC->GetDeviceCaps(LOGPIXELSY),72);

		// set the text color as appropriate
		COLORREF cr = m_crText;
		if (cr == COLOR_WINDOW+1)
			cr = GetSysColor(COLOR_WINDOW);
		pDC->SetTextColor(m_crText);
**/

		if (m_Font)
			delete m_Font;

		m_Font=new CFont;

		// create the font object
		if (!m_Font->CreateFontIndirect(&m_LogFont))
			m_Font=NULL;

		// select the font
//		m_Font=pDC->SelectObject(&font);
	}
}

static bool hack=false;

void CFontToolDlg::OnChangeUnicodeText() 
{
	if (hack)
		return;

	if (m_UnicodeTextCtrl.m_hWnd==0)
		return;

	CString text;
	m_UnicodeTextCtrl.GetWindowText(text);
	if (text.GetLength()<1)
		return;

	hack=true;

	m_CurrentGlyph->mUnicode=text[0];

	Displayf("Unicode Text Changed to %d",m_CurrentGlyph->mUnicode);

	CString value;
	value.Format(_T("%c"),m_CurrentGlyph->mUnicode);
	m_UnicodeValueCtrl.SetWindowText(value);

	Displayf("Unicode Text Changed to %d (%c)",m_CurrentGlyph->mUnicode,m_CurrentGlyph->mUnicode);

	hack=false;
}

void CFontToolDlg::OnChangeUnicodeValue() 
{
	if (hack)
		return;

	if (m_UnicodeValueCtrl.m_hWnd==0)
		return;

	CString value;
	m_UnicodeValueCtrl.GetWindowText(value);
	if (value.GetLength()<1)
		return;

	hack=true;

	m_CurrentGlyph->mUnicode=_ttoi(value);

	CString text;
	text.Format(_T("%c"),m_CurrentGlyph->mUnicode);
	m_UnicodeTextCtrl.SetWindowText(text);

	Displayf("Unicode Value Changed to %d (%c)",m_CurrentGlyph->mUnicode,m_CurrentGlyph->mUnicode);

	hack=false;
}

void CFontToolDlg::UpdateStringWidgets()
{
	m_CurrentString=NULL;
	txtStringEntry *currentStringEntry=NULL;

	HashPosition position;
	if (!mStringEditor->GetHashTable()->GetFirstEntry(position))
	{
		DisableStringWidgets();
		return;
	}

	for (int i=m_StringScrollBar.GetScrollPos();i;i--)
		if (!mStringEditor->GetHashTable()->GetNextEntry(position))
			break;

	int language=m_Language.GetCurSel();

	if (language<0)
	{
		language=txtLanguage::kEs;
		Warningf("Unrecognized language.  Using %s",txtLanguage::Get(language));
	}

	currentStringEntry=(txtStringEntry*)position.data;
	m_CurrentString=currentStringEntry->mStrings[language];

	if (m_CurrentString)
	{
		m_StringEnglish.EnableWindow(true);
		m_StringText.EnableWindow(true);
		m_StringFont.EnableWindow(true);
		m_StringIdentifier.EnableWindow(true);
		m_StringScrollBar.EnableWindow(true);

//		CString string;
//		m_StringEnglish.GetWindowText(string);
//		_tprintf(_T("string='%s'"),string);

#if __UNICODE
		USES_CONVERSION;
		const _TCHAR *_tidentifier=A2W(position.id);
		const _TCHAR *_tfont=A2W(m_CurrentString->mFont);
//		const _TCHAR *_tlanguage=A2W(m_CurrentString->mLanguage);
		const _TCHAR *_ttext=m_CurrentString->GetString();
		const _TCHAR *_tenglish;
		if (currentStringEntry->mStrings[txtLanguage::kEn])
			_tenglish=currentStringEntry->mStrings[txtLanguage::kEn]->GetString();
		else
			_tenglish=_T("(no english)");

//		if (hasWides(_ttext))
//			_ttext=_T("[unprintable]");
#else
		USES_CONVERSION;
		const char *_tidentifier=position.id;
		const char *_tfont=m_CurrentString->mFont;
		const char *_tlanguage=m_CurrentString->mLanguage;
		const char *_ttext=W2A(m_CurrentString->mString);
		const char *_tenglish=W2A(currentStringEntry->mStrings[txtLanguage::kEn]->mString);
#endif

		m_StringIdentifier.SetWindowText(_tidentifier);
		m_StringFont.SetWindowText(_tfont);
		m_StringEnglish.SetWindowText(_tenglish);
		m_StringText.SetWindowText(_ttext);
	}
	else
		DisableStringWidgets();
}

void CFontToolDlg::DisableStringWidgets()
{
	m_StringIdentifier.EnableWindow(false);
	m_StringFont.EnableWindow(false);
	m_StringEnglish.EnableWindow(false);
	m_StringText.EnableWindow(false);
}

void CFontToolDlg::OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
	if (pScrollBar==&SB_Vert)
	{
		OnVScrollViewport(nSBCode,nPos,pScrollBar);
		return;
	}

	// TODO: Add your message handler code here and/or call default

//	m_StringScrollBar;
//	UpdateStringWidgets();

	int iMin,iMax,iPos;
	CRect rc;
	int dn=0;

	pScrollBar->GetScrollRange(&iMin, &iMax);
	iPos = pScrollBar->GetScrollPos();
	pScrollBar->GetClientRect(&rc);

//	switch (GET_WM_VSCROLL_CODE(wParam, lParam)) {
	switch (nSBCode) {
		case SB_LINEDOWN:
//			Displayf("LineDown");
			dn=1;
//			dn =  rc.bottom / 16 + 1;
			break;

		case SB_LINEUP:
//			Displayf("LineUp");
			dn=-1;
//			dn = -rc.bottom / 16 + 1;
			break;

		case SB_PAGEDOWN:
//			Displayf("PageDown");
			dn=10;
//			dn =  rc.bottom / 2  + 1;
			break;

		case SB_PAGEUP:
//			Displayf("PageUp");
			dn=-10;
//			dn = -rc.bottom / 2  + 1;
			break;

		case SB_THUMBTRACK:
		case SB_THUMBPOSITION:
//			Displayf("Thumb");
//			dn = GET_WM_VSCROLL_POS(wParam, lParam)-iPos;
			break;

		default:
//			Displayf("Default");
			dn = 0;
			break;
	}

//	Displayf("dn=%d  iPos=%d  iPos+dn=%d",dn,iPos,iPos+dn);
	pScrollBar->SetScrollPos(iPos+dn,true);
	CDialog::OnVScroll(nSBCode, iPos+dn, pScrollBar);

	UpdateStringWidgets();

/**
	SCROLLINFO si;
	si.fMask=SIF_RANGE|SIF_POS;
//	si.fMask=SIF_RANGE|SIF_POS|SIF_PAGE;
//	si.fMask=SIF_ALL;
	si.nMin=0;
	si.nMax=99;
//	si.nPage=25;
	si.nPos=50;
//	si.nTrackPos=
	m_StringScrollBar.SetScrollInfo(&si,true);
**/

//	CDialog::OnVScroll(nSBCode, nPos, pScrollBar);

	Redraw();
}

void CFontToolDlg::OnSelChangeLanguage() 
{
	UpdateStringWidgets();

	Redraw();
}

BOOL CFontToolDlg::OnNotify(WPARAM wParam, LPARAM lParam, LRESULT* pResult) 
{
	// TODO: Add your specialized code here and/or call the base class

//	Displayf("Notify got %d %d %d",wParam,lParam,EN_CHANGE);
	ChangedEditGlyph();
	
	return CDialog::OnNotify(wParam, lParam, pResult);
}

void CFontToolDlg::OnChangeGlyph() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here
	ChangedEditGlyph();
	Redraw();
}

void CFontToolDlg::OnChangeFontParameter() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here

	UpdateData(TRUE);
	UpdateFontParameters();
	Redraw();
}

void CFontToolDlg::OnGlyphPopupDelete() 
{
	POSITION pos=mGlyphList.GetHeadPosition();
	while (pos)
	{
		Glyph *glyph=mGlyphList.GetNext(pos);
		if (glyph==m_CurrentGlyph)
		{
			if (pos)
			{
				POSITION tempPos=pos;
				mGlyphList.GetPrev(tempPos);
				mGlyphList.RemoveAt(tempPos);
			}
			else
				mGlyphList.RemoveTail();

			m_CurrentGlyph=NULL;
			NewCurrentGlyph();
			Redraw();

			return;
		}

	}

	Warningf("Couldn't find glyph to delete");

	NewCurrentGlyph();
	Redraw();
}

void CFontToolDlg::OnGlyphPopupShrink() 
{
	if (m_CurrentGlyph)
		ShrinkRectangle(m_CurrentGlyph);

	NewCurrentGlyph();
}

void CFontToolDlg::OnGlyphPopupExpand() 
{
	if (m_CurrentGlyph)
		ExpandRectangle(m_CurrentGlyph);

	NewCurrentGlyph();
}

void CFontToolDlg::OnGlyphPopupResize() 
{	
	if (m_CurrentGlyph)
	{
		ExpandRectangle(m_CurrentGlyph);
		ShrinkRectangle(m_CurrentGlyph);
	}

	NewCurrentGlyph();
}

void CFontToolDlg::OnSlice() 
{
	if (AfxMessageBox(_T("Warning: This will replace your current glyph boxes and their Unicode assignments.\nAre you sure you want to do this?"),MB_YESNO)!=IDYES)
		return;

	Slice();	
	Redraw();
}

void CFontToolDlg::OnExit() 
{
	if (AfxMessageBox(_T("Are you sure you want to exit?"),MB_YESNOCANCEL)==IDYES)
		EndDialog(IDOK);
}

void CFontToolDlg::OnOk() 
{
	// do nothing here so it doesn't exit
}

void CFontToolDlg::OnCancel() 
{
	if (AfxMessageBox(_T("Are you sure you want to exit?"),MB_YESNOCANCEL)==IDYES)
		EndDialog(IDOK);
}

void CFontToolDlg::OnShadows() 
{
	if (m_ShadowsDlg.IsWindowVisible())
		m_ShadowsDlg.ShowWindow(SW_HIDE);
	else
		m_ShadowsDlg.ShowWindow(SW_SHOW);
}

void CFontToolDlg::AddMatchFonts(CComboBox &combo)
{
	combo.Clear();

	HashPosition position;
	if (!mStringEditor->GetHashTable()->GetFirstEntry(position))
		return;

	USES_CONVERSION;

	while (mStringEditor->GetHashTable()->GetNextEntry(position))
	{
		for (int language=0;language<txtLanguage::GetCount();language++)
		{
			txtStringEntry *currentStringEntry=(txtStringEntry*)position.data;
			if (currentStringEntry->mStrings[language])
			{
				_TCHAR *string=A2W(currentStringEntry->mStrings[language]->mFont);
				if (combo.FindStringExact(-1,string)==CB_ERR)
					combo.AddString(string);
			}
		}
	}		
}

void CFontToolDlg::PostNcDestroy() 
{
	// TODO: Add your specialized code here and/or call the base class

	// HACK - this avoids an Assert() in txtFontTex::ResetMemory()
	// because FontTool doesn't add fonts to the font hashtable
	txtFontTex::sm_First = NULL;
	
	CDialog::PostNcDestroy();
}

#endif
