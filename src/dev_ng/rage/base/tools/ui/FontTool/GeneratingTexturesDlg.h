#if !defined(AFX_GENERATINGTEXTURESDLG_H__E6C5AAFD_C0E4_47E4_AC1D_F479095F7129__INCLUDED_)
#define AFX_GENERATINGTEXTURESDLG_H__E6C5AAFD_C0E4_47E4_AC1D_F479095F7129__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// GeneratingTexturesDlg.h : header file
//

namespace rage {

/////////////////////////////////////////////////////////////////////////////
// GeneratingTexturesDlg dialog

class GeneratingTexturesDlg : public CDialog
{
// Construction
public:
	GeneratingTexturesDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(GeneratingTexturesDlg)
	enum { IDD = IDD_TEXTURES };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(GeneratingTexturesDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(GeneratingTexturesDlg)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

} // namespace rage

#endif // !defined(AFX_GENERATINGTEXTURESDLG_H__E6C5AAFD_C0E4_47E4_AC1D_F479095F7129__INCLUDED_)
