// stdafx.h : include file for standard system include files,
//  or project specific include files that are used frequently, but
//      are changed infrequently
//

#if !defined(AFX_STDAFX_H__3B3278C2_35E1_40B1_B28A_87C8EEF21698__INCLUDED_)
#define AFX_STDAFX_H__3B3278C2_35E1_40B1_B28A_87C8EEF21698__INCLUDED_

#include "text/config.h"

# pragma warning(disable: 4668)
# define _WIN32_WINNT 0x500

#define UNICODE

#if _MSC_VER >= 1300
#undef A2W
#undef W2A
#undef USES_CONVERSION
#endif

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions
#include <afxtempl.h>
#include <afxdisp.h>        // MFC Automation classes
#include <afxdtctl.h>		// MFC support for Internet Explorer 4 Common Controls
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT


//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STDAFX_H__3B3278C2_35E1_40B1_B28A_87C8EEF21698__INCLUDED_)
