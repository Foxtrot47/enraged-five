// MakeGridDlg.cpp : implementation file
//

#include "stdafx.h"
#include "FontTool.h"
#include "FontToolDlg.h"
#include "MakeGridDlg.h"

#include "Alphabet.h"
#include "diag/output.h"

namespace rage {

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// MakeGridDlg dialog


MakeGridDlg::MakeGridDlg(CWnd* pParent /*=NULL*/)
	: CDialog(MakeGridDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(MakeGridDlg)
	m_GridHeight = 32;
	m_GridWidth = 32;
	m_GridBaseline = 6;
	m_AllLowers = FALSE;
	m_AllNumbers = FALSE;
	m_AllSymbols = FALSE;
	m_AllUppers = FALSE;
	m_AllCommonSymbols = FALSE;
	m_EuroLowers = FALSE;
	m_EuroSymbols = FALSE;
	m_EuroUppers = FALSE;
	m_MoreSymbols = FALSE;
	m_UseAllStringTable = FALSE;
	m_UseSomeStringTable = FALSE;
	m_MatchFont = _T("");
	//}}AFX_DATA_INIT
}


void MakeGridDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(MakeGridDlg)
	DDX_Control(pDX, IDC_REGENERATEALPHABET, m_RegenerateAlphabetButton);
	DDX_Control(pDX, IDC_ADDEMPTYGLYPHS, m_AddEmptyGlyphsButton);
	DDX_Control(pDX, IDC_STRINGTABLECOMBO, m_StringTableCombo);
	DDX_Control(pDX, IDC_GRIDBASELINESPIN, m_GridBaselineSpin);
	DDX_Control(pDX, IDC_GRIDWIDTHSPIN, m_GridWidthSpin);
	DDX_Control(pDX, IDC_GRIDHEIGHTSPIN, m_GridHeightSpin);
	DDX_Text(pDX, IDC_GRIDHEIGHT, m_GridHeight);
	DDV_MinMaxInt(pDX, m_GridHeight, 2, 128);
	DDX_Text(pDX, IDC_GRIDWIDTH, m_GridWidth);
	DDV_MinMaxInt(pDX, m_GridWidth, 2, 128);
	DDX_Text(pDX, IDC_GRIDBASELINE, m_GridBaseline);
	DDV_MinMaxInt(pDX, m_GridBaseline, 2, 128);
	DDX_Check(pDX, IDC_ALLLOWERS, m_AllLowers);
	DDX_Check(pDX, IDC_ALLNUMBERS, m_AllNumbers);
	DDX_Check(pDX, IDC_ALLSYMBOLS, m_AllSymbols);
	DDX_Check(pDX, IDC_ALLUPPERS, m_AllUppers);
	DDX_Check(pDX, IDC_ALLCOMMONSYMBOLS, m_AllCommonSymbols);
	DDX_Check(pDX, IDC_EUROLOWERS, m_EuroLowers);
	DDX_Check(pDX, IDC_EUROSYMBOLS, m_EuroSymbols);
	DDX_Check(pDX, IDC_EUROUPPERS, m_EuroUppers);
	DDX_Check(pDX, IDC_MORESYMBOLS, m_MoreSymbols);
	DDX_Check(pDX, IDC_USEALLSTRINGTABLE, m_UseAllStringTable);
	DDX_Check(pDX, IDC_USESOMESTRINGTABLE, m_UseSomeStringTable);
	DDX_CBString(pDX, IDC_STRINGTABLECOMBO, m_MatchFont);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(MakeGridDlg, CDialog)
	//{{AFX_MSG_MAP(MakeGridDlg)
	ON_BN_CLICKED(IDC_ADDEMPTYGLYPHS, OnAddEmptyGlyphs)
	ON_BN_CLICKED(IDC_REGENERATEALPHABET, OnRegenerateAlphabet)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


int MakeGridDlg::GetFlags() const
{
	int flags=0;
	flags |= m_AllLowers?Alphabet::kLowers:0;
	flags |= m_AllUppers?Alphabet::kUppers:0;
	flags |= m_AllNumbers?Alphabet::kNumbers:0;
	flags |= m_AllCommonSymbols?Alphabet::kCommonSymbols:0;
	flags |= m_AllSymbols?Alphabet::kAllSymbols:0;
	flags |= m_EuroUppers?Alphabet::kEuroUppers:0;
	flags |= m_EuroLowers?Alphabet::kEuroLowers:0;
	flags |= m_EuroSymbols?Alphabet::kEuroSymbols:0;
	flags |= m_MoreSymbols?Alphabet::kMoreSymbols:0;

	flags |= m_UseAllStringTable?Alphabet::kStringTableAll:0;
	flags |= m_UseSomeStringTable?Alphabet::kStringTableSome:0;

	return flags;
}

struct MakeGridData
{
	BOOL m_GridHeight;
	BOOL m_GridWidth;
	BOOL m_GridBaseline;
	BOOL m_AllLowers;
	BOOL m_AllNumbers;
	BOOL m_AllSymbols;
	BOOL m_AllUppers;
	BOOL m_AllCommonSymbols;
	BOOL m_EuroUppers;
	BOOL m_EuroLowers;
	BOOL m_EuroSymbols;
	BOOL m_MoreSymbols;
	BOOL m_UseAllStringTable;
	BOOL m_UseSomeStringTable;
};

void MakeGridDlg::Serialize(CArchive &archive)
{
	if (archive.IsStoring())
	{
		int version=0x0100;

		archive << version;

		MakeGridData makeGridData;
		makeGridData.m_GridHeight=m_GridHeight;
		makeGridData.m_GridWidth=m_GridWidth;
		makeGridData.m_GridBaseline=m_GridBaseline;
		makeGridData.m_AllLowers=m_AllLowers;
		makeGridData.m_AllNumbers=m_AllNumbers;
		makeGridData.m_AllSymbols=m_AllSymbols;
		makeGridData.m_AllUppers=m_AllUppers;
		makeGridData.m_AllCommonSymbols=m_AllCommonSymbols;
		makeGridData.m_EuroUppers=m_EuroUppers;
		makeGridData.m_EuroLowers=m_EuroLowers;
		makeGridData.m_EuroSymbols=m_EuroSymbols;
		makeGridData.m_MoreSymbols=m_MoreSymbols;
		makeGridData.m_UseAllStringTable=m_UseAllStringTable;
		makeGridData.m_UseSomeStringTable=m_UseSomeStringTable;
		archive << sizeof(MakeGridData);
		archive.Write(&makeGridData,sizeof(MakeGridData));
	}
	else
	{
		int version=0x0100;

		archive >> version;

		if (version<0x0100)
		{
			m_GridHeight=version;		// version was added later
			archive >> m_GridWidth;
			archive >> m_GridBaseline;
			archive >> m_AllLowers;
			archive >> m_AllNumbers;
			archive >> m_AllSymbols;
			archive >> m_AllUppers;
			archive >> m_UseAllStringTable;
			archive >> m_AllCommonSymbols;
		}
		else
		{
			int size;
			archive >> size;
			char *data=Alloca(char,size);
			archive.Read(data,size);

			MakeGridData *makeGridData=(MakeGridData*)data;

			m_GridHeight=makeGridData->m_GridHeight;
			m_GridWidth=makeGridData->m_GridWidth;
			m_GridBaseline=makeGridData->m_GridBaseline;
			m_AllLowers=makeGridData->m_AllLowers;
			m_AllNumbers=makeGridData->m_AllNumbers;
			m_AllSymbols=makeGridData->m_AllSymbols;
			m_AllUppers=makeGridData->m_AllUppers;
			m_AllCommonSymbols=makeGridData->m_AllCommonSymbols;
			m_EuroUppers=makeGridData->m_EuroUppers;
			m_EuroLowers=makeGridData->m_EuroLowers;
			m_EuroSymbols=makeGridData->m_EuroSymbols;
			m_MoreSymbols=makeGridData->m_MoreSymbols;
			m_UseAllStringTable=makeGridData->m_UseAllStringTable;
			m_UseSomeStringTable=makeGridData->m_UseSomeStringTable;
		}
	}
}

/////////////////////////////////////////////////////////////////////////////
// MakeGridDlg message handlers

BOOL MakeGridDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	m_GridWidthSpin.SetBuddy(GetDlgItem(IDC_GRIDWIDTH));
	m_GridWidthSpin.SetRange32(2,255);
	m_GridHeightSpin.SetBuddy(GetDlgItem(IDC_GRIDHEIGHT));
	m_GridHeightSpin.SetRange32(2,255);
	m_GridBaselineSpin.SetBuddy(GetDlgItem(IDC_GRIDBASELINE));
	m_GridBaselineSpin.SetRange32(2,255);

	m_AddEmptyGlyphsButton.SetCheck(1);
	OnAddEmptyGlyphs();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void MakeGridDlg::UpdateDialog()
{
	CFontToolDlg *fontToolDlg=(CFontToolDlg*)GetParent();
	fontToolDlg->AddMatchFonts(m_StringTableCombo);
}

void MakeGridDlg::OnOK() 
{
	CFontToolDlg *fontToolDlg=(CFontToolDlg*)GetParent();

	// TODO: Add extra validation here

	if (!fontToolDlg->GetImage())
	{
		AfxMessageBox(_T("You must create a new font image before you make a grid"));
		return;
	}

	CDialog::OnOK();

	bool addEmpty=false;
	if (m_AddEmptyGlyphsButton.GetCheck()==1)
		addEmpty=true;

	int alphabetFlags=GetFlags();
	USES_CONVERSION;
//	Displayf("Alphabet Flags is 0x%04X.  MatchFont is %s",alphabetFlags,W2A(LPCTSTR(m_MatchFont)));
	fontToolDlg->MakeGrid(m_GridWidth,m_GridHeight,m_GridBaseline,alphabetFlags,addEmpty,W2A(LPCTSTR(m_MatchFont)));
	fontToolDlg->Redraw();
}

void MakeGridDlg::OnAddEmptyGlyphs() 
{
	// TODO: Add your control notification handler code here

 	GetDlgItem(IDC_ALLLOWERS)->EnableWindow(false);
	GetDlgItem(IDC_ALLNUMBERS)->EnableWindow(false);
	GetDlgItem(IDC_ALLSYMBOLS)->EnableWindow(false);
	GetDlgItem(IDC_ALLUPPERS)->EnableWindow(false);
	GetDlgItem(IDC_ALLCOMMONSYMBOLS)->EnableWindow(false);
	GetDlgItem(IDC_EUROLOWERS)->EnableWindow(false);
	GetDlgItem(IDC_EUROSYMBOLS)->EnableWindow(false);
	GetDlgItem(IDC_EUROUPPERS)->EnableWindow(false);
	GetDlgItem(IDC_MORESYMBOLS)->EnableWindow(false);
	GetDlgItem(IDC_USEALLSTRINGTABLE)->EnableWindow(false);
	GetDlgItem(IDC_USESOMESTRINGTABLE)->EnableWindow(false);
	GetDlgItem(IDC_STRINGTABLECOMBO)->EnableWindow(false);

	m_RegenerateAlphabetButton.SetCheck(0);
}

void MakeGridDlg::OnRegenerateAlphabet() 
{
	// TODO: Add your control notification handler code here

	GetDlgItem(IDC_ALLLOWERS)->EnableWindow(true);
	GetDlgItem(IDC_ALLNUMBERS)->EnableWindow(true);
	GetDlgItem(IDC_ALLSYMBOLS)->EnableWindow(true);
	GetDlgItem(IDC_ALLUPPERS)->EnableWindow(true);
	GetDlgItem(IDC_ALLCOMMONSYMBOLS)->EnableWindow(true);
	GetDlgItem(IDC_EUROLOWERS)->EnableWindow(true);
	GetDlgItem(IDC_EUROSYMBOLS)->EnableWindow(true);
	GetDlgItem(IDC_EUROUPPERS)->EnableWindow(true);
	GetDlgItem(IDC_MORESYMBOLS)->EnableWindow(true);
	GetDlgItem(IDC_USEALLSTRINGTABLE)->EnableWindow(true);
	GetDlgItem(IDC_USESOMESTRINGTABLE)->EnableWindow(true);
	GetDlgItem(IDC_STRINGTABLECOMBO)->EnableWindow(true);

	m_AddEmptyGlyphsButton.SetCheck(0);
}

} // namespace rage
