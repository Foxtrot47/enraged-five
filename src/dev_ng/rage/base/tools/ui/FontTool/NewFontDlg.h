#if !defined(AFX_NEWFONTDLG_H__1EF0B53B_8886_4E64_A7DB_F31BB61DDC46__INCLUDED_)
#define AFX_NEWFONTDLG_H__1EF0B53B_8886_4E64_A7DB_F31BB61DDC46__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// NewFontDlg.h : header file
//

namespace rage {

/////////////////////////////////////////////////////////////////////////////
// NewFontDlg dialog

class NewFontDlg : public CDialog
{
// Construction
public:
	NewFontDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(NewFontDlg)
	enum { IDD = IDD_NEWFONT_DIALOG };
	int		m_Height;
	int		m_Width;
	CString	m_FontName;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(NewFont)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(NewFontDlg)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

} // namespace rage

#endif // !defined(AFX_NEWFONTDLG_H__1EF0B53B_8886_4E64_A7DB_F31BB61DDC46__INCLUDED_)
