////////////////////
// viewgraph.cpp  //
////////////////////


#include "bank/bank.h"
#include "bank/bkmgr.h"
#include "devcam/freecam.h"
#include "grcore/im.h"
#include "input/keyboard.h"
#include "input/keys.h"
#include "input/pad.h"
#include "phbound/bound.h"
#include "phbound/boundgrid.h"
#include "phcore/materialmgrflag.h"
#include "phcore/materialmgrimpl.h"
#include "grprofile/drawmanager.h"
#include "sample_grcore/sample_grcore.h"
#include "system/main.h"
#include "system/param.h"
#include "system/timer.h"
#include "vector/colors.h"


namespace rage {

PFD_DECLARE_GROUP_ON(ViewGraph);
PFD_DECLARE_ITEM_ON(GraphSegments,Color_white,ViewGraph);
PFD_DECLARE_ITEM(GraphNormals,Color_blue,ViewGraph);

} // namespace rage 

namespace ragesamples {

using namespace rage;

////////////////////////////////////////////////////////////////
// 

class viewGraphManager : public grcSampleManager
{
public:
	enum { THREE_D, TWO_D_X, TWO_D_Y };

	viewGraphManager() : m_CameraTarget(ORIGIN), 
						m_FixedX(0.0f), m_FixedY(0.0f), m_DomainMinX(-1.0f), m_DomainMaxX(1.0f), m_DomainMinY(-1.0f), m_DomainMaxY(1.0f), 
						m_Const1(1.0f), m_Const2(1.0f), m_Const3(1.0f), m_Const4(1.0f), m_Const5(1.0f), m_Const6(1.0f),
						m_ElementsX(100), m_ElementsY(100), m_Dimension(THREE_D), m_DrawSolid(false)
	{}

	Vector3 ComputeFunction (float x, float y)
	{
		// Enter any function here to compute z from x and/or y.
		// Examples:

		// saddle -
		float z = square(x) - square(y);

		// spring force -
		// x is stretch/compression, y is sideways motion, m_Const1 is spring constant, m_Const2 is rest length
		//float z = m_Const1*x*(1.0f-m_Const2*invsqrtf(square(x)+square(y)));

		// collision detection screen resulting from screw motion -
		//float rSq = square(x)+square(y);
		//float z = (m_Const1*(rSq*square(m_Const2)+square(m_Const3))-rSq*safe_atan2f(x,y)*m_Const2)/m_Const3;

		// Return a 3D position to draw.
		return Vector3(x,y,z);
	}

	void DrawGraph ()
	{
		if (m_Dimension==THREE_D)
		{
			float rangeX = m_DomainMaxX-m_DomainMinX;
			float rangeY = m_DomainMaxY-m_DomainMinY;
			float delX = rangeX/(float)(m_ElementsX-1);
			float delY = rangeY/(float)(m_ElementsY-1);
			int indexX,indexY;
			if (m_DrawSolid)
			{
				for (indexX=0; indexX<m_ElementsX; indexX++)
				{
					grcBegin(drawTriStrip,2*m_ElementsY);
					float x = m_DomainMinX+delX*(float)indexX;
					for (indexY=0; indexY<m_ElementsY; indexY++)
					{
						float y = m_DomainMinY+delY*(float)indexY;
						grcVertex3f(ComputeFunction(x,y));
						grcVertex3f(ComputeFunction(x+delX,y));
					}

					grcEnd();
				}
			}
			else
			{
				for (indexX=0; indexX<m_ElementsX; indexX++)
				{
					grcBegin(drawLineStrip,m_ElementsY);
					float x = m_DomainMinX+delX*(float)indexX;
					for (indexY=0; indexY<m_ElementsY; indexY++)
					{
						float y = m_DomainMinY+delY*(float)indexY;
						grcVertex3f(ComputeFunction(x,y));
					}

					grcEnd();
				}

				for (indexY=0; indexY<m_ElementsY; indexY++)
				{
					grcBegin(drawLineStrip,m_ElementsX);
					float y = m_DomainMinY+delY*(float)indexY;
					for (indexX=0; indexX<m_ElementsX; indexX++)
					{
						float x = m_DomainMinX+delX*(float)indexX;
						grcVertex3f(ComputeFunction(x,y));
					}

					grcEnd();
				}
			}
		}
		else if (m_Dimension==TWO_D_X)
		{
			float rangeX = m_DomainMaxX-m_DomainMinX;
			float invElementsX = 1.0f/(float)m_ElementsX;
			for (int indexX=0; indexX<m_ElementsX; indexX++)
			{
				float x = m_DomainMinX+rangeX*(float)indexX*invElementsX;
				grcVertex3f(ComputeFunction(x,m_FixedY));
			}
		}
		else
		{
			Assert(m_Dimension==TWO_D_Y);
			float rangeY = m_DomainMaxY-m_DomainMinY;
			float invElementsY = 1.0f/(float)m_ElementsY;
			for (int indexY=0; indexY<m_ElementsY; indexY++)
			{
				float y = m_DomainMinY+rangeY*(float)indexY*invElementsY;
				grcVertex3f(ComputeFunction(m_FixedX,y));
			}
		}
	}

	virtual void InitClient()
	{
		PFD_GROUP_ENABLE(ViewGraph,true);

	#if __PFDRAW
		GetRageProfileDraw().Init(20000000, true, grcBatcher::BUF_FULL_ONSCREEN_WARNING);
		GetRageProfileDraw().SetEnabled(true);
	#endif

	}

	void InitLights()
	{
		m_Lights.Reset();
		m_Lights.SetColor(0, 1.0f,1.0f,1.0f);
		m_Lights.SetColor(1, 0.0f,0.0f,0.0f);
		m_Lights.SetColor(2, 0.0f,0.0f,0.0f);
		m_Lights.SetPosition(0, Vec3V(1.0f,2.0f,3.0f));
		m_Lights.SetPosition(1, Vec3V(-1.27f,-0.5f,-1.0f));
		m_Lights.SetPosition(2, Vec3V(1.2f,-0.575f,-2.15f));
		m_Lights.SetIntensity(0, 1.0f);
		m_Lights.SetIntensity(1, 0.0f);
		m_Lights.SetIntensity(2, 0.0f);
		m_Lights.SetAmbient(0.0f, 0.0f, 0.2f);

		//Initialize the manipulators for the lights
		//InitLightManipulators(m_Lights);
		// disabled because it crashes and isn't needed in viewgraph (NC 1 March 06)
	}

	virtual void ShutdownClient()
	{

	#if __PFDRAW
		GetRageProfileDraw().Shutdown();
	#endif

	}

	virtual void Update()
	{
		grcSampleManager::Update();
	}

	virtual void DrawClient()
	{

#if __PFDRAW
		grcBindTexture(NULL);
		grcLightState::SetEnabled(true);
		if (PFD_GraphSegments.Begin())
		{
			grcColor(PFD_GraphSegments.GetBaseColor());
			DrawGraph();
			PFD_GraphSegments.End();
		}

		GetRageProfileDraw().Render();
#endif

	}

#if __BANK
	virtual void AddWidgetsClient ()
	{
		bkBank& bank = BANKMGR.CreateBank("ViewGraph");

		// domain control
		const float maxDomain = 10.0f;
		const float domainStep = 0.01f;
		bank.AddSlider("m_DomainMinX",&m_DomainMinX,-maxDomain,maxDomain,domainStep);
		bank.AddSlider("m_DomainMaxX",&m_DomainMaxX,-maxDomain,maxDomain,domainStep);
		bank.AddSlider("m_DomainMinY",&m_DomainMinY,-maxDomain,maxDomain,domainStep);
		bank.AddSlider("m_DomainMaxY",&m_DomainMaxY,-maxDomain,maxDomain,domainStep);

		// constant value control
		const float maxConst = 10.0f;
		const float constStep = 0.01f;
		bank.AddSlider("m_Const1",&m_Const1,-maxConst,maxConst,constStep);
		bank.AddSlider("m_Const2",&m_Const2,-maxConst,maxConst,constStep);
		bank.AddSlider("m_Const3",&m_Const3,-maxConst,maxConst,constStep);
		bank.AddSlider("m_Const4",&m_Const4,-maxConst,maxConst,constStep);
		bank.AddSlider("m_Const5",&m_Const5,-maxConst,maxConst,constStep);
		bank.AddSlider("m_Const6",&m_Const6,-maxConst,maxConst,constStep);

		// graph resolution control
		const int minElements = 2;
		const int maxElements = 1000;
		const float elementStep = 1.0f;
		bank.AddSlider("m_ElementsX",&m_ElementsX,minElements,maxElements,elementStep);
		bank.AddSlider("m_ElementsY",&m_ElementsY,minElements,maxElements,elementStep);

		m_Viewport->AddWidgets(bank);
	}
#endif


private:
	Vector3 m_CameraTarget;
	float m_FixedX,m_FixedY,m_DomainMinX,m_DomainMaxX,m_DomainMinY,m_DomainMaxY;
	float m_Const1,m_Const2,m_Const3,m_Const4,m_Const5,m_Const6;
	int m_ElementsX,m_ElementsY,m_Dimension;
	bool m_DrawSolid;
};

} // namespace ragesamples

// main application
int Main()
{
	{
		ragesamples::viewGraphManager viewGraph;
		viewGraph.Init();

		viewGraph.UpdateLoop();

		viewGraph.Shutdown();
	}

	return 0;
}
