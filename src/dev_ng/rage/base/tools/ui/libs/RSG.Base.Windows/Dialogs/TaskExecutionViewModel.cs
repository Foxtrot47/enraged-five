﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using RSG.Base.Editor;
using RSG.Base.Collections;
using RSG.Base.Editor.Command;
using RSG.Base.Windows.Helpers;
using RSG.Base.Tasks;
using System.Windows.Data;
using System.Windows.Shell;
using System.Globalization;
using System.Windows.Media.Imaging;

namespace RSG.Base.Windows.Dialogs
{
    /// <summary>
    /// 
    /// </summary>
    public class TaskExecutionViewModel : ViewModelBase
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public string Title
        {
            get { return m_title; }
            set
            {
                SetPropertyValue(value, m_title, () => this.Title,
                    new PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_title = (string)newValue;
                        }
                    ));
            }
        }
        private string m_title;

        /// <summary>
        /// 
        /// </summary>
        public Boolean? DialogState
        {
            get { return m_dialogState; }
            set
            {
                SetPropertyValue(value, m_dialogState, () => this.DialogState,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_dialogState = (Boolean?)newValue;
                        }
                    ));
            }
        }
        private Boolean? m_dialogState;

        /// <summary>
        /// Task view model.
        /// </summary>
        public TaskViewModel TaskVM
        {
            get { return m_task; }
            set
            {
                SetPropertyValue(value, m_task, () => this.TaskVM,
                    new PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_task = (TaskViewModel)newValue;
                        }
                    ));
            }
        }
        private TaskViewModel m_task;

        /// <summary>
        /// 
        /// </summary>
        public RelayCommand CancelCommand
        {
            get
            {
                if (m_cancelCommand == null)
                {
                    m_cancelCommand = new RelayCommand(param => this.Cancel(param), param => this.CanCancel(param));
                }
                return m_cancelCommand;
            }
        }
        private RelayCommand m_cancelCommand;

        /// <summary>
        /// 
        /// </summary>
        public bool ExecutingTasks
        {
            get { return m_executingTasks; }
            set
            {
                SetPropertyValue(value, m_executingTasks, () => this.ExecutingTasks,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_executingTasks = (bool)newValue;
                            System.Windows.Input.CommandManager.InvalidateRequerySuggested();
                        }
                    ));
            }
        }
        private bool m_executingTasks;

        /// <summary>
        /// Whether the dialog should close itself after completing the task.
        /// </summary>
        public bool AutoCloseOnCompletion
        {
            get;
            set;
        }

        /// <summary>
        /// The source for cancelling tasks that are queued up
        /// </summary>
        private CancellationTokenSource TaskCancellationSouce
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        private ITaskContext TaskContext
        {
            get;
            set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="tasks"></param>
        public TaskExecutionViewModel(string title, ITask task, CancellationTokenSource cts, ITaskContext context)
            : base()
        {
            Title = title;

            // Create a view model for the task
            if (task is CompositeTask)
            {
                TaskVM = new CompositeTaskViewModel((CompositeTask)task);
            }
            else
            {
                TaskVM = new TaskViewModel(task);
            }

            // Keep track of the cancellation token source
            TaskCancellationSouce = cts;
            TaskContext = context;

            // Hook up the event handler for determining when to close the task
            if (task.Progress is EventProgress<TaskProgress>)
            {
                ((EventProgress<TaskProgress>)task.Progress).ProgressChanged += new EventHandler<TaskProgress>(TaskProgressChanged);
            }
        }
        #endregion // Constructor(s)

        #region Public Methods
        /// <summary>
        /// 
        /// </summary>
        public void StartTaskExecution()
        {
            ExecutingTasks = true;
            TaskVM.ExecuteTask(TaskContext);
        }

        /// <summary>
        /// 
        /// </summary>
        public void CancelExecution()
        {
            TaskCancellationSouce.Cancel();
        }
        #endregion // Public Methods

        #region Commands
        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameter"></param>
        private void Cancel(Object parameter)
        {
            CancelExecution();
            DialogState = false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameter"></param>
        /// <returns></returns>
        private Boolean CanCancel(Object parameter)
        {
            return ExecutingTasks;
        }
        #endregion // Commands

        #region Event Handlers
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void TaskProgressChanged(object sender, TaskProgress e)
        {
            ITask task = (ITask)sender;

            if (DialogState == null && task.Status == TaskStatus.Completed && AutoCloseOnCompletion)
            {
                DialogState = true;
            }
            else if (task.Status == TaskStatus.Faulted)
            {
                System.Windows.Application.Current.Dispatcher.Invoke(
                    new Action
                        (
                            delegate()
                            {
                                RSG.Base.Windows.ExceptionStackTraceDlg dlg =
                                    new RSG.Base.Windows.ExceptionStackTraceDlg(task.Exception, null);
                                dlg.Show();
                            }
                        ),
                        System.Windows.Threading.DispatcherPriority.ContextIdle
                );
                DialogState = false;
            }
        }
        #endregion // Event Handlers
    } // ProgressTaskExecutionViewModel


    /// <summary>
    /// TaskStatus to TaskbarItemProgressState converter
    /// </summary>
    [ValueConversion(typeof(TaskStatus), typeof(TaskbarItemProgressState))]
    public class TaskStatusToTaskbarStatusConverter : IValueConverter
    {
        #region IValueConverter Members
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object Convert(Object value, Type targetType, Object parameter, CultureInfo culture)
        {
            TaskStatus status = (TaskStatus)value;

            if (status == TaskStatus.Faulted)
            {
                return TaskbarItemProgressState.Error;
            }
            else
            {
                return TaskbarItemProgressState.Normal;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object ConvertBack(Object value, Type targetType, Object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
        #endregion // IValueConverter Members
    } // TaskStatusToTaskbarStatusConverter

    /// <summary>
    /// TaskStatus to Image converted
    /// </summary>
    [ValueConversion(typeof(TaskStatus), typeof(BitmapImage))]
    public class TaskStatusToImageConverter : IValueConverter
    {
        #region Constants
        private static readonly Uri c_pendingImageSource;
        private static readonly Uri c_runningImageSource;
        private static readonly Uri c_completedImageSource;
        private static readonly Uri c_cancelledImageSource;
        private static readonly Uri c_faultedImageSource;
        #endregion // Constants

        #region Static Constructor
        /// <summary>
        /// 
        /// </summary>
        static TaskStatusToImageConverter()
        {
            c_pendingImageSource = new Uri("pack://application:,,,/RSG.Base.Windows;component/Resources/TaskStatus/pending.png");
            c_runningImageSource = new Uri("pack://application:,,,/RSG.Base.Windows;component/Resources/TaskStatus/running.png");
            c_completedImageSource = new Uri("pack://application:,,,/RSG.Base.Windows;component/Resources/TaskStatus/completed.png");
            c_cancelledImageSource = new Uri("pack://application:,,,/RSG.Base.Windows;component/Resources/TaskStatus/cancelled.png");
            c_faultedImageSource = new Uri("pack://application:,,,/RSG.Base.Windows;component/Resources/TaskStatus/faulted.png");
        }
        #endregion // Static Constructor

        #region IValueConverter Members
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object Convert(Object value, Type targetType, Object parameter, CultureInfo culture)
        {
            TaskStatus status = (TaskStatus)value;

            switch (status)
            {
                case TaskStatus.Pending:
                    return new BitmapImage(c_pendingImageSource);

                case TaskStatus.Running:
                    return new BitmapImage(c_runningImageSource);

                case TaskStatus.Completed:
                    return new BitmapImage(c_completedImageSource);

                case TaskStatus.Cancelled:
                    return new BitmapImage(c_cancelledImageSource);

                case TaskStatus.Faulted:
                    return new BitmapImage(c_faultedImageSource);

                default:
                    throw new NotSupportedException();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object ConvertBack(Object value, Type targetType, Object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
        #endregion // IValueConverter Members
    } // TaskStatusToImageConverter

    /// <summary>
    /// Double in 0-1 range converted to 0-100.
    /// </summary>
    [ValueConversion(typeof(double), typeof(double))]
    public class DoubleToPercentageConverter : IValueConverter
    {
        #region IValueConverter Members
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object Convert(Object value, Type targetType, Object parameter, CultureInfo culture)
        {
            return (double)value * 100.0;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object ConvertBack(Object value, Type targetType, Object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
        #endregion // IValueConverter Members
    } // DoubleToPercentageConverter
}
