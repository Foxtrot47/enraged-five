﻿using System;
using System.Windows;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace RSG.Base.Windows.Controls
{
    /// <summary>
    /// Defines a image that greys itself out automatically if the IsEnabled property on a parent control (i.e a button) is set to false
    /// </summary>
    public class GreyableImage : Image
    {
        #region Constructor(s)

        /// <summary>
        /// Static constructor used to override the metadata
        /// </summary>
        static GreyableImage()
        {
            IsEnabledProperty.AddOwner(typeof(GreyableImage), new FrameworkPropertyMetadata(true, new PropertyChangedCallback(OnIsEnabledPropertyChanged)));
        }

        /// <summary>
        /// Initializes a new instance of the class.
        /// </summary>
        public GreyableImage()
        {
        }

        #endregion // Constructor(s)

        #region Overridden Function(s)

        /// <summary>
        /// Need to make sure that images inside templates that get initialised i.e get a source after the enabled property changes also get 
        /// greyed out.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnInitialized(EventArgs e)
        {
            base.OnInitialized(e);
            OnAutoGreyScaleImageInitialised(this, new DependencyPropertyChangedEventArgs(IsEnabledProperty, IsEnabled, IsEnabled));
        }

        #endregion // Overridden Function(s)

        #region Event Callbacks

        /// <summary>
        /// Called when auto grey scale image is enabled property changed.
        /// </summary>
        /// <param name="source">The source of the event</param>
        /// <param name="args">The arguments for the event</param>
        private static void OnIsEnabledPropertyChanged(DependencyObject source, DependencyPropertyChangedEventArgs args)
        {
            var autoGreyScaleImg = source as GreyableImage;
            var isEnable = Convert.ToBoolean(args.NewValue);

            // Make sure that the image has been initialised (i.e has a valid source), images that are 
            // inside a template will reach here before being initialised.
            if (autoGreyScaleImg.IsInitialized == false)
                return;

            if (autoGreyScaleImg != null && autoGreyScaleImg.Source != null)
            {
                if (!isEnable)
                {
                    // Get the source bitmap
                    if (autoGreyScaleImg.Source is BitmapImage)
                    {
                        if ((autoGreyScaleImg.Source as BitmapImage).BaseUri != null)
                        {
                            BitmapImage bitmapImage = new BitmapImage(new Uri(autoGreyScaleImg.Source.ToString()));

                            // Convert it to Gray
                            autoGreyScaleImg.Source = new FormatConvertedBitmap(bitmapImage, PixelFormats.Gray32Float, null, 0);

                            // Create Opacity Mask for greyscale image as FormatConvertedBitmap does not keep transparency info
                            autoGreyScaleImg.OpacityMask = new ImageBrush(bitmapImage);
                        }
                        else if ((autoGreyScaleImg.Source as BitmapImage).StreamSource != null)
                        {
                            BitmapImage bitmapImage = new BitmapImage();
                            bitmapImage.BeginInit();
                            System.IO.MemoryStream stream = (autoGreyScaleImg.Source as BitmapImage).StreamSource as System.IO.MemoryStream;
                            bitmapImage.StreamSource = new System.IO.MemoryStream(stream.ToArray());
                            bitmapImage.EndInit();

                            // Convert it to Gray
                            autoGreyScaleImg.Source = new FormatConvertedBitmap(bitmapImage, PixelFormats.Gray32Float, null, 0);

                            // Create Opacity Mask for greyscale image as FormatConvertedBitmap does not keep transparency info
                            autoGreyScaleImg.OpacityMask = new ImageBrush(bitmapImage);
                        }
                        else if ((autoGreyScaleImg.Source as BitmapImage).UriSource != null)
                        {
                            BitmapImage bitmapImage = new BitmapImage(new Uri(autoGreyScaleImg.Source.ToString()));

                            // Convert it to Gray
                            autoGreyScaleImg.Source = new FormatConvertedBitmap(bitmapImage, PixelFormats.Gray32Float, null, 0);

                            // Create Opacity Mask for greyscale image as FormatConvertedBitmap does not keep transparency info
                            autoGreyScaleImg.OpacityMask = new ImageBrush(bitmapImage);
                        }
                    }
                }
                else
                {
                    // Set the Source property to the original value.
                    if (autoGreyScaleImg.Source is FormatConvertedBitmap)
                    {
                        autoGreyScaleImg.Source = ((FormatConvertedBitmap)autoGreyScaleImg.Source).Source;
                    }

                    // Reset the Opcity Mask
                    autoGreyScaleImg.OpacityMask = null;
                }
            }
        }

        /// <summary>
        /// Need to make sure that images inside templates that get initialised i.e get a source after the enabled property changes also get 
        /// greyed out.
        /// </summary>
        /// <param name="source"></param>
        /// <param name="args"></param>
        private static void OnAutoGreyScaleImageInitialised(DependencyObject source, DependencyPropertyChangedEventArgs args)
        {
            var autoGreyScaleImg = source as GreyableImage;
            var isEnable = Convert.ToBoolean(args.NewValue);

            if (autoGreyScaleImg != null)
            {
                if (!isEnable)
                {
                    // Get the source bitmap
                    if (autoGreyScaleImg.Source is FormatConvertedBitmap)
                    {
                        BitmapImage bitmapImage = new BitmapImage(new Uri((autoGreyScaleImg.Source as FormatConvertedBitmap).Source.ToString()));
                        autoGreyScaleImg.Source = new FormatConvertedBitmap(bitmapImage, PixelFormats.Gray32Float, null, 0);
                        autoGreyScaleImg.OpacityMask = new ImageBrush(bitmapImage);
                    }
                    else if (autoGreyScaleImg.Source != null)
                    {
                        BitmapImage bitmapImage = new BitmapImage(new Uri(autoGreyScaleImg.Source.ToString()));

                        // Convert it to Gray
                        autoGreyScaleImg.Source = new FormatConvertedBitmap(bitmapImage, PixelFormats.Gray32Float, null, 0);

                        // Create Opacity Mask for greyscale image as FormatConvertedBitmap does not keep transparency info
                        autoGreyScaleImg.OpacityMask = new ImageBrush(bitmapImage);
                    }
                }
            }
        }

        #endregion // Event Callbacks

    }
}
