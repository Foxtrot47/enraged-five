﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows.Data;

namespace RSG.Base.Windows.Converters
{

    /// <summary>
    /// 
    /// </summary>
    public class StringCollectionToStringConverter : IValueConverter
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public Object Convert(Object value, Type targetType, Object parameter, CultureInfo culture)
        {
            if (!(value is IEnumerable<String>))
                return (null);

            IEnumerable<String> list = (IEnumerable<String>)value;
            return (String.Join(Environment.NewLine, list));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public Object ConvertBack(Object value, Type targetType, Object parameter, CultureInfo culture)
        {
            throw new NotImplementedException("This converter cannot be used in two-way binding.");
        }
    }

} // RSG.Base.Windows.Converters namespace
