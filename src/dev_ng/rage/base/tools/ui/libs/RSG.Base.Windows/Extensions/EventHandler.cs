using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows.Threading;

using RSG.Base.Extensions;

namespace RSG.Base.Windows.Extensions
{
    public static class EventHandlerExtensions
    {
        /// <summary>
        /// Raise an event on another thread, also checking for null. This does an asychronous call (BeginInvoke)
        /// and returns immediately.
        /// 
        /// Example:
        /// statusChanged.RaiseOnThread(remoteHostDispatcher, this, "new status message!")
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="evt"></param>
        /// <param name="d">The dispatcher that should raise the event</param>
        /// <param name="sender"></param>
        /// <param name="payload"></param>
        public static void RaiseOnThread<T>(this EventHandler<SimpleEventArgs<T>> evt, Dispatcher d, object sender, T payload)
        {
            if (evt != null)
            {
                Object[] argArray = new Object[2];
                argArray[0] = sender;
                argArray[1] = new SimpleEventArgs<T>(payload);
                d.BeginInvoke(evt, argArray);
            }
        }

        /// <summary>
        /// Raise an event on another thread, also checking for null. This does an asychronous call (BeginInvoke)
        /// and returns immediately.
        /// 
        /// Example:
        /// statusChanged.RaiseOnThread(remoteHostDispatcher, this, new StatusChangedEventArgs("new status message!", StatusOK))
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="evt"></param>
        /// <param name="d"></param>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        public static void RaiseOnThread<T>(this EventHandler<T> evt, Dispatcher d, object sender, T args) where T : EventArgs
        {
            if (evt != null)
            {
                Object[] argArray = new Object[2];
                argArray[0] = sender;
                argArray[1] = args;
                d.BeginInvoke(evt, argArray);
            }
        }


        /// <summary>
        /// Raise an event on another thread, also checking for null. This does an asychronous call (BeginInvoke)
        /// and returns immediately.
        /// 
        /// Example:
        /// statusChanged.RaiseOnThread(remoteHostDispatcher, this)
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="evt"></param>
        /// <param name="d"></param>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        public static void RaiseOnThread(this EventHandler<EventArgs> evt, Dispatcher d, object sender)
        {
            if (evt != null)
            {
                Object[] argArray = new Object[2];
                argArray[0] = sender;
                argArray[1] = EventArgs.Empty;
                d.BeginInvoke(evt, argArray);
            }
        }
    }
}