﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Math;
using System.Windows;
using System.Windows.Media.Imaging;
using System.Windows.Interop;
using System.Drawing;
using System.Drawing.Imaging;

namespace RSG.Base.Windows.Controls.WpfViewport2D.Geometry
{
    /// <summary>
    /// 
    /// </summary>
    public class Viewport2DImageOverlay : Viewport2DImage
    {
        #region Member Data
        /// <summary>
        /// Flag indicating whether the overlay is in the process of being updated
        /// </summary>
        private bool m_updating;
        private Dictionary<Color, SolidBrush> m_ColorBrushes;
        #endregion // Member Data

        #region Properties
        /// <summary>
        /// Size that the image will be
        /// </summary>
        private Vector2i ImageSize
        {
            get;
            set;
        }

        /// <summary>
        /// Bitmap that is used to store the image data
        /// </summary>
        private Bitmap RenderBitmap
        {
            get;
            set;
        }

        /// <summary>
        /// Graphics object we will use for rendering to
        /// </summary>
        private Graphics ImageGraphics
        {
            get;
            set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="space"></param>
        /// <param name="name"></param>
        /// <param name="pos"></param>
        /// <param name="size"></param>
        public Viewport2DImageOverlay(etCoordSpace space, String name, Vector2i imageSize, Vector2f pos, Vector2f renderSize)
            : base(space, name, (BitmapImage)null, pos, renderSize)
        {
            ImageSize = imageSize;
            m_updating = false;
            m_ColorBrushes = new Dictionary<Color, SolidBrush>();
        }
        #endregion // Constructor(s)

        #region Public Interface
        /// <summary>
        /// 
        /// </summary>
        public void BeginUpdate()
        {
            try
            {
                RenderBitmap = null;
                GC.WaitForPendingFinalizers();
                GC.Collect();
                GC.WaitForPendingFinalizers();

                RenderBitmap = new Bitmap(ImageSize.X, ImageSize.Y, PixelFormat.Format32bppArgb);
                RenderBitmap.MakeTransparent();
                ImageGraphics = Graphics.FromImage(RenderBitmap);
                m_updating = true;
                m_ColorBrushes.Clear();
            }
            catch
            {
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void EndUpdate()
        {
            this.Image = new BitmapImage();
            using (System.IO.MemoryStream ms = new System.IO.MemoryStream())
            {
                RenderBitmap.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                ms.Position = 0;

                this.Image.BeginInit();
                this.Image.CacheOption = BitmapCacheOption.OnLoad;
                this.Image.UriSource = null;
                this.Image.StreamSource = ms;
                this.Image.EndInit();
            }
            this.Image.Freeze();

            ImageGraphics.Dispose();
            RenderBitmap.Dispose();
            RenderBitmap = null;
            m_updating = false;
        }

        /// <summary>
        /// 
        /// </summary>
        public void ClearImage()
        {
            BeginUpdate();
            EndUpdate();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="text"></param>
        /// <param name="font"></param>
        /// <param name="brush"></param>
        /// <param name="position"></param>
        public void RenderText(Vector2f position, string text, Font font, Color textColour)
        {
            ThrowIfNotUpdating();

            Vector2f worldPosition = ConvertCoordinate(position);
            ImageGraphics.DrawString(text, font, new SolidBrush(textColour), new PointF(worldPosition.X, worldPosition.Y));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="center"></param>
        /// <param name="radius"></param>
        /// <param name="outlineColour"></param>
        /// <param name="outlineWidth"></param>
        public void RenderCircle(Vector2f center, float radius, Color outlineColour, float outlineWidth)
        {
            ThrowIfNotUpdating();

            Vector2f worldCenter = ConvertCoordinate(center - new Vector2f(radius, -radius));
            ImageGraphics.DrawEllipse(new Pen(outlineColour, outlineWidth), worldCenter.X, worldCenter.Y, ConvertX(radius*2), ConvertY(radius*2));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="center"></param>
        /// <param name="radius"></param>
        /// <param name="fillColour"></param>
        public void RenderCircle(Vector2f center, float radius, Color fillColour)
        {
            ThrowIfNotUpdating();

            if ( m_ColorBrushes.ContainsKey(fillColour) == false)
                m_ColorBrushes.Add(fillColour, new SolidBrush(fillColour));

            Vector2f worldCenter = ConvertCoordinate(center - new Vector2f(radius, -radius));
            ImageGraphics.FillEllipse(m_ColorBrushes[fillColour], worldCenter.X, worldCenter.Y, ConvertX(radius * 2), ConvertY(radius * 2));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="center"></param>
        /// <param name="radius"></param>
        /// <param name="fillColour"></param>
        public void RenderCircle(Vector3f center, float radius, Color fillColour)
        {
            ThrowIfNotUpdating();

            if (m_ColorBrushes.ContainsKey(fillColour) == false)
                m_ColorBrushes.Add(fillColour, new SolidBrush(fillColour));

            Vector2f worldCenter = ConvertCoordinate(center - new Vector3f(radius, -radius, 0.0f));
            ImageGraphics.FillEllipse(m_ColorBrushes[fillColour], worldCenter.X, worldCenter.Y, ConvertX(radius * 2), ConvertY(radius * 2));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="points"></param>
        /// <param name="outlineColor"></param>
        /// <param name="outlineWidth"></param>
        public void RenderPolygon(Vector2f[] points, Color outlineColor, int outlineWidth)
        {
            ThrowIfNotUpdating();

            if (m_ColorBrushes.ContainsKey(outlineColor) == false)
                m_ColorBrushes.Add(outlineColor, new SolidBrush(outlineColor));

            ImageGraphics.DrawPolygon(new Pen(m_ColorBrushes[outlineColor], outlineWidth), points.Select(item => ConvertCoordinate(item)).Select(item => new PointF(item.X, item.Y)).ToArray());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="points"></param>
        /// <param name="outlineColor"></param>
        /// <param name="outlineWidth"></param>
        /// <param name="fillColour"></param>
        public void RenderPolygon(Vector2f[] points, Color fillColour)
        {
            ThrowIfNotUpdating();

            if (m_ColorBrushes.ContainsKey(fillColour) == false)
                m_ColorBrushes.Add(fillColour, new SolidBrush(fillColour));

            ImageGraphics.FillPolygon(m_ColorBrushes[fillColour], points.Select(item => ConvertCoordinate(item)).Select(item => new PointF(item.X, item.Y)).ToArray());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="points"></param>
        /// <param name="lineColour"></param>
        /// <param name="lineWidth"></param>
        public void RenderLines(Vector2f[] points, Color lineColour, int lineWidth)
        {
            ThrowIfNotUpdating();

            if (m_ColorBrushes.ContainsKey(lineColour) == false)
                m_ColorBrushes.Add(lineColour, new SolidBrush(lineColour));

            ImageGraphics.DrawLines(new Pen(m_ColorBrushes[lineColour], lineWidth), points.Select(item => ConvertCoordinate(item)).Select(item => new PointF(item.X, item.Y)).ToArray());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="points"></param>
        /// <param name="lineColour"></param>
        /// <param name="lineWidth"></param>
        public void RenderLines(Vector3f[] points, Color lineColour, int lineWidth)
        {
            ThrowIfNotUpdating();

            if (m_ColorBrushes.ContainsKey(lineColour) == false)
                m_ColorBrushes.Add(lineColour, new SolidBrush(lineColour));

            ImageGraphics.DrawLines(new Pen(m_ColorBrushes[lineColour], lineWidth), points.Select(item => ConvertCoordinate(item)).Select(item => new PointF(item.X, item.Y)).ToArray());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="text"></param>
        /// <param name="textColour"></param>
        /// <param name="fontSize"></param>
        public void RenderText(string text, Vector2f position, Color textColour, int fontSize)
        {
            RenderText(text, position, textColour, fontSize, false);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="text"></param>
        /// <param name="textColour"></param>
        /// <param name="fontSize"></param>
        /// <param name="centered"></param>
        public void RenderText(string text, Vector2f position, Color textColour, int fontSize, bool centered)
        {
            ThrowIfNotUpdating();

            // Create font and brush.
            Font drawFont = new Font("Arial", fontSize);
            SolidBrush drawBrush = new SolidBrush(Color.Black);

            // Determine where we will be rendering the text.
            Vector2f convertedPosition = ConvertCoordinate(position);

            PointF startPoint;
            if (centered)
            {
                SizeF stringSize = ImageGraphics.MeasureString(text, drawFont);
                startPoint = new PointF(convertedPosition.X - stringSize.Width / 2.0f, convertedPosition.Y - stringSize.Height / 2.0f);
            }
            else
            {
                startPoint = new PointF(convertedPosition.X, convertedPosition.Y);
            }

            ImageGraphics.DrawString(text, drawFont, drawBrush, startPoint);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="alpha"></param>
        public void UpdateImageOpacity(byte alpha)
        {
            // Lock the bitmap's bits.  
            Rectangle rect = new Rectangle(0, 0, RenderBitmap.Width, RenderBitmap.Height);
            System.Drawing.Imaging.BitmapData bmpData = RenderBitmap.LockBits(rect, System.Drawing.Imaging.ImageLockMode.ReadWrite, RenderBitmap.PixelFormat);

            // Get the address of the first line.
            IntPtr ptr = bmpData.Scan0;

            // Declare an array to hold the bytes of the bitmap.
            int bytes = System.Math.Abs(bmpData.Stride) * RenderBitmap.Height;
            byte[] rgbValues = new byte[bytes];

            // Copy the RGB values into the array.
            System.Runtime.InteropServices.Marshal.Copy(ptr, rgbValues, 0, bytes);

            // Set every 4th value to 255. A 24bpp bitmap will look red.  
            for (int counter = 0; counter < rgbValues.Length / 4; counter++)
            {
                if (rgbValues[counter * 4 + 0] != 0 ||
                    rgbValues[counter * 4 + 1] != 0 ||
                    rgbValues[counter * 4 + 2] != 0)
                {
                    rgbValues[counter * 4 + 3] = alpha;
                }
            }

            // Copy the RGB values back to the bitmap
            System.Runtime.InteropServices.Marshal.Copy(rgbValues, 0, ptr, bytes);

            // Unlock the bits.
            RenderBitmap.UnlockBits(bmpData);
        }
        #endregion // Public Interface

        #region Private Interface
        /// <summary>
        /// 
        /// </summary>
        private void ThrowIfNotUpdating()
        {
            if (!m_updating)
            {
                throw new ApplicationException("Unable to render to viewport image overlay outside of Begin/EndUpdate calls.");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        private float ConvertX(float value)
        {
            return value * ImageSize.X / (float)Size.Width;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        private float ConvertY(float value)
        {
            return value * ImageSize.Y / (float)Size.Height;
        }

        /// <summary>
        /// Converts a world space coordinate to an image space one
        /// </summary>
        /// <param name="coord"></param>
        /// <returns></returns>
        private Vector2f ConvertCoordinate(Vector2f coord)
        {
            // Conversion factors for converting world space -> image space
            double widthFactor = ImageSize.X / Size.Width;
            double heightFactor = ImageSize.Y / Size.Height;

            Vector2f centerOffset = new Vector2f(-(float)(Position.X * widthFactor), (float)(Position.Y * heightFactor));
            Vector2f scaledPosition = new Vector2f((float)(coord.X * widthFactor), -(float)(coord.Y * heightFactor));

            return scaledPosition + centerOffset;
        }

        /// <summary>
        /// Converts a world space coordinate to an image space one
        /// </summary>
        /// <param name="coord"></param>
        /// <returns></returns>
        private Vector2f ConvertCoordinate(Vector3f coord)
        {
            // Conversion factors for converting world space -> image space
            double widthFactor = ImageSize.X / Size.Width;
            double heightFactor = ImageSize.Y / Size.Height;

            Vector2f centerOffset = new Vector2f(-(float)(Position.X * widthFactor), (float)(Position.Y * heightFactor));
            Vector2f scaledPosition = new Vector2f((float)(coord.X * widthFactor), -(float)(coord.Y * heightFactor));

            return scaledPosition + centerOffset;
        }
        #endregion // Private Interface
    } // Viewport2DImageOverlay
} // RSG.Base.Windows.Controls.WpfViewport2D.Geometry
