﻿using System;
using System.Windows;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Windows.DragDrop.Utilities;

namespace RSG.Base.Windows.DragDrop
{
    /// <summary>
    /// 
    /// </summary>
    public class DefaultDragHandler : IDragSource
    {
        public virtual Object StartDrag(DragInfo dragInfo)
        {
            int itemCount = dragInfo.SourceItems.Cast<Object>().Count();
            dragInfo.Effects = (dragInfo.Data != null) ? DragDropEffects.Copy | DragDropEffects.Move : DragDropEffects.None;

            if (itemCount == 1)
            {
                return dragInfo.SourceItems.Cast<Object>().First();
            }
            else if (itemCount > 1)
            {
                return TypeUtil.CreateDynamicallyTypedList(dragInfo.SourceItems);
            }

            return null;
        }
    } // DefaultDragHandler
} // RSG.Base.Windows.DragDrop
