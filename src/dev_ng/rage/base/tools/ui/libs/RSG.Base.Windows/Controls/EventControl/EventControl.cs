﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System;
using System.Windows.Input;
using System.Windows.Threading;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Windows.Data;
using System.Collections.ObjectModel;
using System.Xml;
using System.Windows.Media;
using System.Windows.Documents;
using System.Windows.Shapes;

namespace RSG.Base.Windows.Controls
{
    /// <summary>
    /// </summary>
    public class EventControl : ListBox
    {
        #region Fields

        /// <summary>
        /// Identifies the <see cref="ScrollBarValue"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty ScrollBarValueProperty;
        /// <summary>
        /// Identifies the <see cref="CurrentUnit"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty CurrentUnitProperty;

        /// <summary>
        /// Identifies the <see cref="ShowUnitTracker"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty ShowUnitTrackerProperty;

        /// <summary>
        /// Identifies the <see cref="PixelsPerUnit"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty PixelsPerUnitProperty;

        /// <summary>
        /// Identies the <see cref="PixelsPerUnitMin"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty PixelsPerUnitMinProperty;

        /// <summary>
        /// Identies the <see cref="PixelsPerUnitMin"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty PixelsPerUnitMaxProperty;

        /// <summary>
        /// Identies the <see cref="ShowPixelsPerUnitScroll"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty ShowPixelsPerUnitScrollProperty;

        /// <summary>
        /// Identies the <see cref="SnapMovementToUnit"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty SnapMovementToUnitProperty;

        /// <summary>
        /// Identies the <see cref="SnapResizeToUnit"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty SnapResizeToUnitProperty;

        /// <summary>
        /// Identies the <see cref="TrackUnitStartOffset"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty TrackUnitStartOffsetProperty;

        /// <summary>
        /// Identifies the <see cref="IndicatorPosition"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty IndicatorPositionProperty =
            DependencyProperty.Register("IndicatorPosition", 
            typeof(double), 
            typeof(EventControl), new PropertyMetadata(OnIndicatorPositionChanged));

        /// <summary>
        /// Identifies the <see cref="ShowIndicator"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty IndicatorVisibilityProperty =
            DependencyProperty.Register("IndicatorVisibility",
            typeof(Visibility),
            typeof(EventControl), new PropertyMetadata(Visibility.Collapsed));

        /// <summary>
        /// Identies the <see cref="MaximumUnitValue"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty MaximumUnitValueProperty =
                DependencyProperty.Register(
                "MaximumUnitValue",
                typeof(double),
                typeof(EventControl),
                new FrameworkPropertyMetadata(
                    100.0,
                    FrameworkPropertyMetadataOptions.Inherits,
                    OnMaximumUnitValueChanged),
                    new ValidateValueCallback(EventPanel.IsDoublePositiveOrZero));

        /// <summary>
        /// Private reference to the scroll bar that controls the pixels per unit (zoom) value.
        /// </summary>
        private ScrollBar zoomScrollBar;

        /// <summary>
        /// Private reference to the scroll bar that controls the track offset value.
        /// </summary>
        private ScrollBar offsetScrollBar;
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises static members of the <see cref="EventControl"/> class.
        /// </summary>
        static EventControl()
        {
            SnapMovementToUnitProperty =
                DependencyProperty.Register(
                "SnapMovementToUnit",
                typeof(bool),
                typeof(EventControl),
                new FrameworkPropertyMetadata(
                    false,
                    FrameworkPropertyMetadataOptions.Inherits));

            SnapResizeToUnitProperty =
                DependencyProperty.Register(
                "SnapResizeToUnit",
                typeof(bool),
                typeof(EventControl),
                new FrameworkPropertyMetadata(
                    false,
                    FrameworkPropertyMetadataOptions.Inherits));

            CurrentUnitProperty =
                DependencyProperty.Register(
                "CurrentUnit",
                typeof(double),
                typeof(EventControl),
                new FrameworkPropertyMetadata(
                    0.0));

            ScrollBarValueProperty =
                DependencyProperty.Register(
                "ScrollBarValue",
                typeof(double),
                typeof(EventControl),
                new FrameworkPropertyMetadata(
                    0.0));

            ShowUnitTrackerProperty =
                DependencyProperty.Register(
                "ShowUnitTracker",
                typeof(bool),
                typeof(EventControl),
                new FrameworkPropertyMetadata(
                    false));           

            PixelsPerUnitProperty =
                DependencyProperty.Register(
                "PixelsPerUnit",
                typeof(double),
                typeof(EventControl),
                new FrameworkPropertyMetadata(
                    1.0,
                    FrameworkPropertyMetadataOptions.Inherits,
                    OnPixelsPerUnitChanged),
                    new ValidateValueCallback(EventPanel.IsDoublePositiveAndGreaterThanZero));

            PixelsPerUnitMinProperty =
                DependencyProperty.Register(
                "PixelsPerUnitMin",
                typeof(double),
                typeof(EventControl),
                new FrameworkPropertyMetadata(
                    1.0,
                    OnPixelsPerUnitRangeChanged),
                    new ValidateValueCallback(EventPanel.IsDoublePositiveAndGreaterThanZero));

            PixelsPerUnitMaxProperty =
                DependencyProperty.Register(
                "PixelsPerUnitMax",
                typeof(double),
                typeof(EventControl),
                new FrameworkPropertyMetadata(
                    1.0,
                    OnPixelsPerUnitRangeChanged),
                    new ValidateValueCallback(EventPanel.IsDoublePositiveAndGreaterThanZero));

            TrackUnitStartOffsetProperty =
                DependencyProperty.Register(
                "TrackUnitStartOffset",
                typeof(double),
                typeof(EventControl),
                new FrameworkPropertyMetadata(
                    0.0,
                    FrameworkPropertyMetadataOptions.Inherits,
                    null),
                    new ValidateValueCallback(EventPanel.IsDoublePositiveOrZero));

            ShowPixelsPerUnitScrollProperty =
                DependencyProperty.Register(
                "ShowPixelsPerUnitScroll",
                typeof(bool),
                typeof(EventControl),
                new FrameworkPropertyMetadata(true));
            

            DefaultStyleKeyProperty.OverrideMetadata(
                typeof(EventControl),
                new FrameworkPropertyMetadata(typeof(EventControl)));
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="EventControl"/> class.
        /// </summary>
        public EventControl()
        {
        }
        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the visibility of the indicator.
        /// </summary>
        public Visibility IndicatorVisibility
        {
            get { return (Visibility)GetValue(IndicatorVisibilityProperty); }
            set { SetValue(IndicatorVisibilityProperty, value); }
        }

        /// <summary>
        /// Gets or sets the indicator position.
        /// </summary>
        public double IndicatorPosition
        {
            get { return (double)GetValue(IndicatorPositionProperty); }
            set { SetValue(IndicatorPositionProperty, value); }
        }

        /// <summary>
        /// Gets or sets the unit position that is currently being marked by the track marker.
        /// </summary>
        public double CurrentUnit
        {
            get { return (double)this.GetValue(CurrentUnitProperty); }
            set { this.SetValue(CurrentUnitProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the unit tracker is shown at the current
        /// unit poisition.
        /// </summary>
        public bool ShowUnitTracker
        {
            get { return (bool)this.GetValue(ShowUnitTrackerProperty); }
            set { this.SetValue(ShowUnitTrackerProperty, value); }
        }

        /// <summary>
        /// Gets or sets the number of pixels that will be render for each unit.
        /// </summary>
        public double PixelsPerUnit
        {
            get { return (double)this.GetValue(PixelsPerUnitProperty); }
            set { this.SetValue(PixelsPerUnitProperty, value); }
        }

        /// <summary>
        /// Gets or sets the minimum number of pixels that will be render for each unit.
        /// </summary>
        public double PixelsPerUnitMin
        {
            get { return (double)this.GetValue(PixelsPerUnitMinProperty); }
            set { this.SetValue(PixelsPerUnitMinProperty, value); }
        }

        /// <summary>
        /// Gets or sets the maximum number of pixels that will be render for each unit.
        /// </summary>
        public double PixelsPerUnitMax
        {
            get { return (double)this.GetValue(PixelsPerUnitMaxProperty); }
            set { this.SetValue(PixelsPerUnitMaxProperty, value); }
        }

        /// <summary>
        /// Gets or sets the offset scrollbars value.
        /// </summary>
        public double ScrollBarValue
        {
            get { return (double)this.GetValue(ScrollBarValueProperty); }
            set { this.SetValue(ScrollBarValueProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the movement of events are limited
        /// to unit boundaries.
        /// </summary>
        public bool SnapMovementToUnit
        {
            get { return (bool)this.GetValue(SnapMovementToUnitProperty); }
            set { this.SetValue(SnapMovementToUnitProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the resizing of events are limited
        /// to unit boundaries.
        /// </summary>
        public bool SnapResizeToUnit
        {
            get { return (bool)this.GetValue(SnapResizeToUnitProperty); }
            set { this.SetValue(SnapResizeToUnitProperty, value); }
        }

        /// <summary>
        /// Gets or sets the first visible unit on the event tracks.
        /// </summary>
        public double TrackUnitStartOffset
        {
            get { return (double)this.GetValue(TrackUnitStartOffsetProperty); }
            set { this.SetValue(TrackUnitStartOffsetProperty, value); }
        }

        public bool ShowPixelsPerUnitScroll
        {
            get { return (bool)this.GetValue(ShowPixelsPerUnitScrollProperty); }
            set { this.SetValue(ShowPixelsPerUnitScrollProperty, value); }
        }

        /// <summary>
        /// Gets or sets the maximum unit value the control can display, this effects
        /// scrolling and manipulation of individual events.
        /// </summary>
        public double MaximumUnitValue
        {
            get { return (double)this.GetValue(MaximumUnitValueProperty); }
            set { this.SetValue(MaximumUnitValueProperty, value); }
        }
        #endregion
        
        #region Methods
        /// <summary>
        /// Gets called when the template is ever applied to the control.
        /// </summary>
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            this.zoomScrollBar = this.GetTemplateChild("PART_ZoomScrollBar") as ScrollBar;
            if (this.zoomScrollBar != null)
            {
                this.zoomScrollBar.ValueChanged += ZoomScrollBarValueChanged;
                this.zoomScrollBar.SizeChanged += ZoomScrollBarSizeChanged;
                this.zoomScrollBar.Minimum = this.PixelsPerUnitMin;
                this.zoomScrollBar.Maximum = PixelsPerUnitMax; // (this.PixelsPerUnitMax - PixelsPerUnitMin) * 100.0;
                this.zoomScrollBar.Value = this.PixelsPerUnit; // (this.PixelsPerUnit - this.PixelsPerUnitMin) * 100.0;

                OnPixelsPerUnitRangeChanged(
                    this,
                    new DependencyPropertyChangedEventArgs(
                        EventControl.PixelsPerUnitMaxProperty, 0.0, this.PixelsPerUnitMax));
            }

            this.offsetScrollBar = this.GetTemplateChild("PART_OffsetScrollBar") as ScrollBar;
            if (this.offsetScrollBar != null)
            {
                this.offsetScrollBar.ValueChanged += OffsetScrollBarValueChanged;
                this.offsetScrollBar.SizeChanged += OffsetScrollBarSizeChanged;

                OnMaximumUnitValueChanged(
                    this,
                    new DependencyPropertyChangedEventArgs(
                        EventControl.MaximumUnitValueProperty, 0.0, this.MaximumUnitValue));
            }
        }

        private void ZoomScrollBarSizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (this.zoomScrollBar == null)
            {
                return;
            }

            double diff = this.PixelsPerUnitMax - this.PixelsPerUnitMin;
            this.zoomScrollBar.ViewportSize = this.zoomScrollBar.ActualWidth + diff;
        }

        private void OffsetScrollBarSizeChanged(object sender, SizeChangedEventArgs e)
        {
            this.HandleChangeToOffsetScroll();
        }

        private void HandleChangeToOffsetScroll()
        {
            if (this.offsetScrollBar == null)
            {
                return;
            }

            double min = 0.0;
            double max = (this.MaximumUnitValue * this.PixelsPerUnit) - (this.offsetScrollBar.ActualWidth);
            double viewport = this.PixelsPerUnit * this.MaximumUnitValue;

            this.offsetScrollBar.Minimum = min;
            this.offsetScrollBar.Maximum = max;
            this.offsetScrollBar.ViewportSize = viewport;

            if (max <= 0.0)
            {
                this.offsetScrollBar.Visibility = System.Windows.Visibility.Hidden;
            }
            else
            {
                this.offsetScrollBar.Visibility = System.Windows.Visibility.Visible;
            }
        }

        /// <summary>
        /// Get called whenever the zoom scroll bar defined in the control template changes
        /// its value.
        /// </summary>
        /// <param name="sender">
        /// The object that this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.Windows.RoutedPropertyChangedEventArgs data for the event.
        /// </param>
        private void ZoomScrollBarValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (this.zoomScrollBar == null)
            {
                return;
            }

            if (this.zoomScrollBar.Value <= this.PixelsPerUnitMin)
            {
                this.PixelsPerUnit = this.PixelsPerUnitMin;
            }
            else if (this.zoomScrollBar.Value >= PixelsPerUnitMax)//(this.PixelsPerUnitMax - PixelsPerUnitMin) * 100.0)
            {
                this.PixelsPerUnit = this.PixelsPerUnitMax;
            }
            else
            {
                this.PixelsPerUnit = this.zoomScrollBar.Value;// (this.zoomScrollBar.Value / 100.0) + this.PixelsPerUnitMin;
            }
        }

        /// <summary>
        /// Get called whenever the offset scroll bar defined in the control template changes
        /// its value.
        /// </summary>
        /// <param name="sender">
        /// The object that this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.Windows.RoutedPropertyChangedEventArgs data for the event.
        /// </param>
        private void OffsetScrollBarValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (this.offsetScrollBar == null)
            {
                return;
            }

            ScrollBarValue = this.offsetScrollBar.Value;
            this.TrackUnitStartOffset = this.offsetScrollBar.Value;

            UpdateIndicator();
        }

        /// <summary>
        /// Update the indicator location.
        /// </summary>
        private void UpdateIndicator()
        {
            var indicatorPart = Template.FindName("PART_Indicator", this) as Rectangle;
            if (indicatorPart != null)
            {
                double indicatorPosition = (PixelsPerUnit * MaximumUnitValue * IndicatorPosition) - TrackUnitStartOffset;
                // Ensures that if the scroll bar nudges the indicator before the visual time line begins, we hide it
                indicatorPart.Visibility = indicatorPosition < 0 ? Visibility.Collapsed : Visibility = IndicatorVisibility; 
                indicatorPart.SetValue(Canvas.LeftProperty, indicatorPosition);
            }
        }

        /// <summary>
        /// Event handler for indicator position changed.
        /// </summary>
        /// <param name="s">Event control.</param>
        /// <param name="e">Event arguments.</param>
        private static void OnIndicatorPositionChanged(DependencyObject s, DependencyPropertyChangedEventArgs e)
        {
            var ec = s as EventControl;
            ec.UpdateIndicator();
        }

        private static void OnPixelsPerUnitRangeChanged(DependencyObject s, DependencyPropertyChangedEventArgs e)
        {
            EventControl control = (EventControl)s;
            if (control == null || control.zoomScrollBar == null)
            {
                return;
            }

            double diff = control.PixelsPerUnitMax - control.PixelsPerUnitMin;
            if (diff <= 0 || control.ShowPixelsPerUnitScroll == false)
            {
                control.zoomScrollBar.Visibility = Visibility.Collapsed;
            }
            else
            {
                control.zoomScrollBar.Visibility = Visibility.Visible;
            }

            control.zoomScrollBar.Minimum = control.PixelsPerUnitMin;
            control.zoomScrollBar.Maximum = control.PixelsPerUnitMax;// (control.PixelsPerUnitMax - control.PixelsPerUnitMin) * 100.0;
            control.zoomScrollBar.ViewportSize = (control.zoomScrollBar.ActualWidth + diff);
        }

        private static void OnPixelsPerUnitChanged(DependencyObject s, DependencyPropertyChangedEventArgs e)
        {
            EventControl control = (EventControl)s;
            if (control == null || control.zoomScrollBar == null)
            {
                return;
            }

            double newValue = (double)e.NewValue;
            if (newValue > control.PixelsPerUnitMax || newValue < control.PixelsPerUnitMin)
            {
                return;
            }

            control.zoomScrollBar.Value = newValue;// (newValue - control.PixelsPerUnitMin) * 100.0;
            control.HandleChangeToOffsetScroll();
        }

        private static void OnMaximumUnitValueChanged(DependencyObject s, DependencyPropertyChangedEventArgs e)
        {
            EventControl control = s as EventControl;
            if (control == null || control.offsetScrollBar == null)
            {
                return;
            }

            control.HandleChangeToOffsetScroll();
        }

        /// <summary>
        /// Creates or identifies the element used to display a specified item.
        /// </summary>
        /// <returns>
        /// A new instance of the type <see cref="EventControlTrack"/>.
        /// </returns>
        protected override DependencyObject GetContainerForItemOverride()
        {
            return new EventControlTrack();
        }

        /// <summary>
        /// Determines if the specified item is (or is eligible to be) its own container.
        /// </summary>
        /// <param name="item">
        /// The item to check.
        /// </param>
        /// <returns>
        /// True if the item is (or is eligible to be) its own container; otherwise, false.
        /// </returns>
        protected override bool IsItemItsOwnContainerOverride(object item)
        {
            return item is EventControlTrack;
        } 
        #endregion
    }
}
