﻿using System;
using System.Windows.Data;
using System.Windows.Media;

namespace RSG.Base.Windows.Converters
{
    public class BoolToColourConverter : IValueConverter
    {
        #region IValueConverter Members

        /// <summary>
        /// Converts a unsigned integer value to a Color.
        /// </summary>
        public Object Convert(Object value, Type targetType, Object parameter, System.Globalization.CultureInfo culture)
        {
            if ((bool)value)
            {
                return Color.FromRgb(0x00, 0xFF, 0x00);
            }
            else
            {
                return Color.FromRgb(0xFF, 0x00, 0x00);
            }
        }

        /// <summary>
        /// Converts a Color to a unsigned integer value.
        /// </summary>
        public Object ConvertBack(Object value, Type targetType, Object parameter, System.Globalization.CultureInfo culture)
        {
            if (value is Color)
            {
                Color c = (Color)value;
                return ((c.R == 0x0) && (c.G > 0x0) && (c.B == 0x0));
            }

            return value;
        }

        #endregion
    }
}
