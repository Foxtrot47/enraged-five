﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace RSG.Base.Windows.Dialogs
{
    /// <summary>
    /// Interaction logic for TaskExecutionDialog.xaml
    /// </summary>
    public partial class TaskExecutionDialog : Window
    {
        public TaskExecutionDialog()
        {
            InitializeComponent();
        }

        public TaskExecutionDialog(TaskExecutionViewModel vm)
        {
            this.DataContext = vm;
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            TaskExecutionViewModel vm = this.DataContext as TaskExecutionViewModel;
            if (vm != null)
            {
                vm.StartTaskExecution();
            }
        }

        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            TaskExecutionViewModel vm = this.DataContext as TaskExecutionViewModel;
            if (vm != null)
            {
                vm.CancelExecution();
            }

            base.OnClosing(e);
        }
    }
}
