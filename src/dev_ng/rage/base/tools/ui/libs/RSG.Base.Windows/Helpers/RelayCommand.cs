﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;

namespace RSG.Base.Windows.Helpers
{
    /// <summary>
    /// 
    /// </summary>
    public class RelayCommand : ICommand
    {
        #region Members
        private Action<Object> m_execute;
        private Predicate<Object> m_canExecute;
        #endregion // Members

        #region Constructor
        /// <summary>
        /// 
        /// </summary>
        /// <param name="execute"></param>
        public RelayCommand(Action<Object> execute)
            : this(execute, null)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="execute"></param>
        /// <param name="canExecute"></param>
        public RelayCommand(Action<Object> execute, Predicate<Object> canExecute)
        {
            this.m_execute = execute;
            this.m_canExecute = canExecute;
        }
        #endregion // Constructor

        #region ICommand Members
        /// <summary>
        /// Gets called when the manager is deciding if this
        /// command can be executed. This returns true be default
        /// </summary>
        public bool CanExecute(Object parameter)
        {
            if (m_canExecute != null)
                return m_canExecute(parameter);

            return true;
        }

        /// <summary>
        /// Gets called when the command is executed. This just
        /// relays the execution to the delegate function
        /// </summary>
        public void Execute(Object parameter)
        {
            if (m_execute != null)
                m_execute(parameter);
        }

        /// <summary>
        /// Makes sure that this command is hooked up into the
        /// command manager
        /// </summary>
        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }
        #endregion // ICommand Members
    } // RelayCommand
} // RSG.Base.Windows.Helpers
