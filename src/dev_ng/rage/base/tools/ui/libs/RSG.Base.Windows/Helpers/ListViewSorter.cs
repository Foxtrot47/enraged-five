﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace RSG.Base.Windows.Helpers
{
    /// <summary>
    /// http://www.codeproject.com/Articles/25964/A-more-generic-way-of-sorting-a-WPF-ListView-with
    /// </summary>
    public static class ListViewSorter
    {
        #region [ Fields ]

        private static Dictionary<String, ListViewSortItem> _listViewDefinitions = new Dictionary<String, ListViewSortItem>();

        #endregion

        #region [ DependencyProperties ]

        public static readonly DependencyProperty CustomListViewSorterProperty = DependencyProperty.RegisterAttached(
            "CustomListViewSorter",
            typeof(Type),
            typeof(ListViewSorter),
            new FrameworkPropertyMetadata(null, new PropertyChangedCallback(OnRegisterSortableGrid)));

        #region [ IsCustomListViewSorter get / set ]

        /// <summary>
        /// Gets the custom list view sorter.
        /// </summary>
        /// <param name="obj">The obj.</param>
        /// <returns></returns>
        public static Type GetCustomListViewSorter(DependencyObject obj)
        {
            return (Type)obj.GetValue(CustomListViewSorterProperty);
        }


        /// <summary>
        /// Sets the custom list view sorter.
        /// </summary>
        /// <param name="obj">The obj.</param>
        /// <param name="value">The value.</param>
        public static void SetCustomListViewSorter(DependencyObject obj, Type value)
        {
            obj.SetValue(CustomListViewSorterProperty, value);
        }
        #endregion

        #endregion

        #region [ Public Methods ]

        /// <summary>
        /// Grids the view column header clicked handler.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        public static void GridViewColumnHeaderClickedHandler(Object sender, RoutedEventArgs e)
        {
            ListView view = sender as ListView;
            if (view == null) return;

            ListViewSortItem listViewSortItem = (_listViewDefinitions.ContainsKey(view.Name)) ? _listViewDefinitions[view.Name] : null;
            if (listViewSortItem == null) return;

            GridViewColumnHeader headerClicked = e.OriginalSource as GridViewColumnHeader;
            if (headerClicked == null) return;

            ListCollectionView collectionView = CollectionViewSource.GetDefaultView(view.ItemsSource) as ListCollectionView;
            if (collectionView == null) return;

            ListSortDirection sortDirection = GetSortingDirection(headerClicked, listViewSortItem);

            // get header name
            String header = headerClicked.Column.Header as String;
            if (String.IsNullOrEmpty(header)) return;

            // sort listview
            if (listViewSortItem.Comparer != null)
            {
                listViewSortItem.Comparer.SortBy = header;
                listViewSortItem.Comparer.SortDirection = sortDirection;
                collectionView.CustomSort = listViewSortItem.Comparer;
                view.Items.Refresh();
            }
            else
            {
                view.Items.SortDescriptions.Clear();
                view.Items.SortDescriptions.Add(new SortDescription(headerClicked.Column.Header.ToString(), sortDirection));
                view.Items.Refresh();
            }

            // change datatemplate of previous and current column header
            headerClicked.Column.HeaderTemplate = GetHeaderColumnsDataTemplate(view, listViewSortItem, sortDirection);

            // Set current sort values as last sort values
            listViewSortItem.LastColumnHeaderClicked = headerClicked;
            listViewSortItem.LastSortDirection = sortDirection;
        }

        #endregion

        #region [ Private Methods ]

        /// <summary>
        /// Called when [register sortable grid].
        /// </summary>
        /// <param name="obj">The obj.</param>
        /// <param name="args">The <see cref="System.Windows.DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private static void OnRegisterSortableGrid(DependencyObject obj, DependencyPropertyChangedEventArgs args)
        {
            // Check if we are in design mode, if so don't do anything.
            if ((Boolean)(DesignerProperties.IsInDesignModeProperty.GetMetadata(typeof(DependencyObject)).DefaultValue)) return;

            ListView view = obj as ListView;
            if (view != null)
            {
                Type sorterType = GetCustomListViewSorter(obj);
                Debug.Assert(sorterType != null, "Unable to determine the type of the custom list view sorter object.");

                IListViewCustomComparer comparer = System.Activator.CreateInstance(sorterType) as IListViewCustomComparer;
                Debug.Assert(sorterType != null, "Unable to create a new instance of the custom listview sorter object.");

                _listViewDefinitions.Add(view.Name, new ListViewSortItem(comparer, null, ListSortDirection.Ascending));
                view.AddHandler(GridViewColumnHeader.ClickEvent, new RoutedEventHandler(GridViewColumnHeaderClickedHandler));
            }
        }

        /// <summary>
        /// Gets the header columns data template.
        /// </summary>
        /// <param name="view">The view.</param>
        /// <param name="listViewSortItem">The list view sort item.</param>
        /// <param name="sortDirection">The sort direction.</param>
        /// <returns></returns>
        private static DataTemplate GetHeaderColumnsDataTemplate(ListView view, ListViewSortItem listViewSortItem, ListSortDirection sortDirection)
        {
            // remove mark from previous sort column
            if (listViewSortItem.LastColumnHeaderClicked != null)
                listViewSortItem.LastColumnHeaderClicked.Column.HeaderTemplate = view.TryFindResource("ListViewHeaderTemplateNoSorting") as DataTemplate;

            // set correct mark to current column
            switch (sortDirection)
            {
                case ListSortDirection.Ascending:
                    return view.TryFindResource("ListViewHeaderTemplateAscendingSorting") as DataTemplate;
                case ListSortDirection.Descending:
                    return view.TryFindResource("ListViewHeaderTemplateDescendingSorting") as DataTemplate;
                default:
                    return null;
            }
        }

        /// <summary>
        /// Gets the sorting direction.
        /// </summary>
        /// <param name="headerClicked">The header clicked.</param>
        /// <param name="listViewSortItem">The list view sort item.</param>
        /// <returns></returns>
        private static ListSortDirection GetSortingDirection(GridViewColumnHeader headerClicked, ListViewSortItem listViewSortItem)
        {
            if (headerClicked != listViewSortItem.LastColumnHeaderClicked) return ListSortDirection.Ascending;
            else
                return (listViewSortItem.LastSortDirection == ListSortDirection.Ascending) ? ListSortDirection.Descending : ListSortDirection.Ascending;
        }

        #endregion
    }

}
