﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using System.Reflection;
using System.Globalization;
using System.ComponentModel;

namespace RSG.Base.Windows.Converters
{
    /// <summary>
    /// Extracts an enum values description.
    /// Useful for when a combobox or other items presenter is bound directly against an enums values.
    /// </summary>
    [ValueConversion(typeof(object), typeof(string))]
    public class EnumDescriptionConverter : IValueConverter
    {
        #region IValueConverter Members
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object Convert(Object value, Type targetType, Object parameter, CultureInfo culture)
        {
            Type type = value.GetType();

            if (type.IsEnum)
            {
                FieldInfo fi = type.GetField(value.ToString());
                DescriptionAttribute[] attributes = fi.GetCustomAttributes(typeof(DescriptionAttribute), false) as DescriptionAttribute[];

                return (attributes.Length > 0 ? attributes[0].Description : null);
            }
            else
            {
                throw new NotSupportedException();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object ConvertBack(Object value, Type targetType, Object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
        #endregion // IValueConverter Members
    } // EnumDescriptionConverter
}
