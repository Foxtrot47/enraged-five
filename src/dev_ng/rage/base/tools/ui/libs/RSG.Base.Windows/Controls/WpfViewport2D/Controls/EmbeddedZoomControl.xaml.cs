﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace RSG.Base.Windows.Controls.WpfViewport2D.Controls
{
    /// <summary>
    /// Interaction logic for EmbeddedZoomControl.xaml
    /// </summary>
    public partial class EmbeddedZoomControl :
        ViewportUserControl
    {
        #region Properties

        /// <summary>
        /// The values of the zoom as an integer represented on the 
        /// slider
        /// </summary>
        public int ZoomValue
        {
            get { return m_zoomValue; }
            set { m_zoomValue = value; OnPropertyChanged("ZoomValue"); }
        }
        private int m_zoomValue;

        /// <summary>
        /// The string that represents the zoom factor of the viewport
        /// </summary>
        public String ZoomString
        {
            get { return m_zoomString; }
            set { m_zoomString = value; OnPropertyChanged("ZoomString"); }
        }
        private String m_zoomString;

        /// <summary>
        /// The number of steps available in the slider
        /// </summary>
        public int SliderStepCount
        {
            get { return m_sliderStepCount; }
            set { m_sliderStepCount = value; OnPropertyChanged("SliderStepCount"); }
        }
        private int m_sliderStepCount;

        /// <summary>
        /// The maximum zoom factor this control can set
        /// </summary>
        public float MaximumZoomFactor
        {
            get { return m_maximumZoomFactor; }
            set { m_maximumZoomFactor = value; OnPropertyChanged("MaximumZoomFactor"); }
        }
        private float m_maximumZoomFactor;

        /// <summary>
        /// The minimum zoom factor this control can set
        /// </summary>
        public float MinimumZoomFactor
        {
            get { return m_mimimumZoomFactor; }
            set { m_mimimumZoomFactor = value; OnPropertyChanged("MinimumZoomFactor"); }
        }
        private float m_mimimumZoomFactor;

        /// <summary>
        /// The zoom factor increase step to be added when the + button is pressed
        /// </summary>
        public float ZoomFactorIncrease
        {
            get { return m_zoomFactorIncrease; }
            set { m_zoomFactorIncrease = value; OnPropertyChanged("ZoomFactorIncrease"); }
        }
        private float m_zoomFactorIncrease;

        /// <summary>
        /// The zoom factor decrease step to be added when the - button is pressed
        /// </summary>
        public float ZoomFactorDecrease
        {
            get { return m_zoomFactorDecrease; }
            set { m_zoomFactorDecrease = value; OnPropertyChanged("ZoomFactorDecrease"); }
        }
        private float m_zoomFactorDecrease;

        #endregion // Properties

        #region Constructor

        /// <summary>
        /// Default constructor
        /// </summary>
        public EmbeddedZoomControl()
        {
            InitializeComponent();
            this.SliderStepCount = 100;
            this.MinimumZoomFactor = 1.0f;
            this.MaximumZoomFactor = 100.0f;
            this.ZoomFactorIncrease = 10.0f;
            this.ZoomFactorDecrease = 10.0f;
            this.ZoomValue = 0;
            this.ZoomString = String.Format("{0:F1}:1", this.GetZoomFactorFromValue());
        }

        #endregion // Constructor

        #region Event Helpers

        /// <summary>
        /// This gets called whenever a property on the action element changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public override void OnManagerPropertyChanged(Object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "ZoomFactor")
            {
                int newZoomValue = GetZoomValueFromFactor((sender as Viewport2D).ZoomFactor);
                //if (this.ZoomSlider.Value != newZoomValue)
                //{
                //    this.ZoomSlider.Value = newZoomValue;
                //}
            }

            base.OnManagerPropertyChanged(sender, e);
        }

        #endregion Event Helpers

        #region Registration

        public override void RegisterActionsWithManager(Actions.ActionManager manager)
        {
            //manager.RegisterCommandAction(this.IncreaseZoomFactor, Button.ClickEvent,
            //    new Action
            //    (
            //        delegate()
            //        {
            //            int newValue = this.ZoomValue + GetValueStepFromFactorStep(this.ZoomFactorIncrease);
            //            this.ZoomValue = newValue > SliderStepCount ? SliderStepCount : newValue;
            //            if (manager.ActionElement is Viewport2D)
            //            {
            //                (manager.ActionElement as Viewport2D).ZoomFactor = GetZoomFactorFromValue();
            //            }
            //        }
            //    )
            //);

            //manager.RegisterCommandAction(this.DecreaseZoomFactor, Button.ClickEvent,
            //    new Action
            //    (
            //        delegate()
            //        {
            //            int newValue = this.ZoomValue - GetValueStepFromFactorStep(-this.ZoomFactorDecrease);
            //            this.ZoomValue = newValue < 0 ? 0 : newValue;
            //            if (manager.ActionElement is Viewport2D)
            //            {
            //                (manager.ActionElement as Viewport2D).ZoomFactor = GetZoomFactorFromValue();
            //            }
            //        }
            //    )
            //);

            //manager.RegisterCommandAction(this.ZoomSlider, Slider.ValueChangedEvent,
            //    new Action
            //    (
            //        delegate()
            //        {
            //            this.SetZoomString();
            //            if (manager.ActionElement is Viewport2D)
            //            {
            //                float newZoomFactor = this.GetZoomFactorFromValue();
            //                if ((manager.ActionElement as Viewport2D).ZoomFactor != newZoomFactor)
            //                {
            //                    (manager.ActionElement as Viewport2D).ZoomFactor = newZoomFactor;
            //                }   
            //            }
            //        }
            //    )
            //);

            manager.RegisterCommandAction(this.FitZoom, Button.ClickEvent,
                new Action
                (
                    delegate()
                    {
                        if (manager.ActionElement is Viewport2D)
                        {
                            float newZoomFactor = (manager.ActionElement as Viewport2D).GetZoomFactorToFit() + 0.1f;
                            int zoomValue = this.GetZoomValueFromFactor(newZoomFactor);
                            this.ZoomValue = zoomValue > SliderStepCount ? SliderStepCount : zoomValue < 0 ? 0 : zoomValue;
                            if ((manager.ActionElement as Viewport2D).ZoomFactor != newZoomFactor)
                            {
                                (manager.ActionElement as Viewport2D).ZoomFactor = newZoomFactor;
                            }
                            (manager.ActionElement as Viewport2D).Center();
                        }
                    }
                )
            );

            base.RegisterActionsWithManager(manager);
        }

        #endregion // Registration

        #region Private Function(s)

        /// <summary>
        /// Gets the slider zoom value from the viewport zoom factor
        /// </summary>
        /// <param name="zoomFactor"></param>
        /// <returns></returns>
        private int GetZoomValueFromFactor(double zoomFactor)
        {
            return (int)System.Math.Ceiling(((zoomFactor - MaximumZoomFactor) * (double)SliderStepCount) / (MinimumZoomFactor - MaximumZoomFactor));
        }

        /// <summary>
        /// Gets the viewport zoom factor from the slider zoom value
        /// </summary>
        /// <returns></returns>
        private float GetZoomFactorFromValue()
        {
            return MaximumZoomFactor + (((MinimumZoomFactor - MaximumZoomFactor) / (float)SliderStepCount) * this.ZoomValue);
        }

        /// <summary>
        /// Set the string that is displayed on the control to show the user the current zoom factor
        /// </summary>
        private void SetZoomString()
        {
            double newZoomFactor = GetZoomFactorFromValue();
            this.ZoomString = String.Format("{0:F1}:1", newZoomFactor);
        }

        /// <summary>
        /// Gets the step in value that need to be performed based on the
        /// given factor step
        /// </summary>
        /// <param name="factorStep"></param>
        /// <returns></returns>
        private int GetValueStepFromFactorStep(double factorStep)
        {
            return (int)System.Math.Ceiling(factorStep / ((MaximumZoomFactor - MinimumZoomFactor) / (double)SliderStepCount));
        }

        #endregion // Private Function(s)

    }
}
