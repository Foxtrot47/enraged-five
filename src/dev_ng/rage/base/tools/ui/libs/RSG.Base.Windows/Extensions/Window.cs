﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace RSG.Base.Windows.Extensions
{
    /// <summary>
    /// 
    /// </summary>
    public class WindowExtension
    {
        #region Dependency Properties

        #region AttachedDialogResult

        public static DependencyProperty AttachedDialogResultProperty = DependencyProperty.RegisterAttached(
            "AttachedDialogResult", typeof(Boolean?), typeof(WindowExtension),
            new PropertyMetadata(null, OnDialogResultChanged));

        public static void SetAttachedDialogResult(UIElement element, Boolean value)
        {
            element.SetValue(AttachedDialogResultProperty, value);
        }
        public static Boolean GetAttachedDialogResult(UIElement element)
        {
            return (Boolean)element.GetValue(AttachedDialogResultProperty);
        }

        private static void OnDialogResultChanged(Object s, DependencyPropertyChangedEventArgs e)
        {
            Window window = s as Window;
            if (s == null)
                return;

            window.DialogResult = e.NewValue as Boolean?;
        }

        #endregion // AttachedDialogResult

        #endregion // Dependency Properties
    } // WindowExtension
}
