﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace RSG.Base.Windows.Controls.WpfViewport2D.Controls
{
    /// <summary>
    /// Interaction logic for MousePanControl.xaml
    /// </summary>
    public partial class MousePanControl : 
        ViewportUserControl
    {
        #region Properties

        private Point StartDragWorldPosition
        {
            get;
            set;
        }

        private Boolean Dragging
        {
            get;
            set;
        }

        #endregion // Properties

        #region Constructor
        
        /// <summary>
        /// Default constructor
        /// </summary>
        public MousePanControl()
        {
            InitializeComponent();
            this.Dragging = false;
        }

        #endregion // Constructor

        #region Event Helpers

        /// <summary>
        /// When the middle mouse button is pressed we capture the position and start the panning
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public override void OnManagerMouseDown(Object sender, MouseButtonEventArgs e)
        {
            if (sender is Viewport2D && e.MiddleButton == MouseButtonState.Pressed)
            {
                e.MouseDevice.OverrideCursor = Cursors.Hand;
                e.MouseDevice.Capture(sender as Viewport2D, CaptureMode.Element);
                Point mouseViewPoint = e.MouseDevice.GetPosition(sender as Viewport2D);
                Point mouseWorldPoint = (sender as Viewport2D).ViewToWorld(mouseViewPoint);

                this.StartDragWorldPosition = mouseWorldPoint;
                this.Dragging = true;
            }
        }

        /// <summary>
        /// When the middle mouse button is released we stop panning
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public override void OnManagerMouseUp(Object sender, MouseButtonEventArgs e)
        {
            if (sender is Viewport2D && e.MiddleButton == MouseButtonState.Released)
            {
                e.MouseDevice.OverrideCursor = Cursors.Arrow;
                e.MouseDevice.Capture(sender as Viewport2D, CaptureMode.None);

                this.Dragging = false;
            }
        }

        /// <summary>
        /// Gets called when the action element sends a mouse wheel move event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public override void OnManagerMouseMove(Object sender, MouseEventArgs e)
        {
            if (sender is Viewport2D && this.Dragging == true)
            {
                // Calculate new look at point based on the finish world position
                Point mouseViewPoint = e.MouseDevice.GetPosition(sender as Viewport2D);
                Point mouseWorldPoint = (sender as Viewport2D).ViewToWorld(mouseViewPoint);
                Vector dragDisplacement = StartDragWorldPosition - mouseWorldPoint;

                (sender as Viewport2D).Pan(dragDisplacement);
            }
        }

        #endregion Event Helpers

        #region Registration

        public override void RegisterActionsWithManager(Actions.ActionManager manager)
        {
            manager.RegisterMouseCommand(this, Actions.MouseEvents.Mouse_Down | Actions.MouseEvents.Mouse_Up | Actions.MouseEvents.Mouse_Move);
        }

        #endregion // Registration
    }
}
