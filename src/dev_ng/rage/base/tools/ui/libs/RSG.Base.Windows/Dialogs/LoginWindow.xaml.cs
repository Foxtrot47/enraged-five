﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using RSG.Base.Win32;
using System.Windows.Interop;

namespace RSG.Base.Windows.Dialogs
{
    /// <summary>
    /// Interaction logic for LoginWindow.xaml
    /// </summary>
    public partial class LoginWindow : Window
    {
        #region Constants
        /// <summary>
        /// Devstar Help URL.
        /// </summary>
        private const String URL_HELP = "https://devstar.rockstargames.com/wiki/index.php/Workbench#Login";
        #endregion // Constants

        #region Constructor(s)
        /// <summary>
        /// Default constructor
        /// </summary>
        public LoginWindow()
            : this(null)
        {
        }
        
        /// <summary>
        /// Constructor accepting the login view model as an argument
        /// </summary>
        /// <param name="vm"></param>
        public LoginWindow(LoginViewModel vm)
        {
            this.DataContext = vm;
            InitializeComponent();
        }
        #endregion // Constructor(s)

        #region Event Handlers
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_Activated(object sender, EventArgs e)
        {
            this.Focus();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void username_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            LoginViewModel vm = this.DataContext as LoginViewModel;
            if (vm == null)
            {
                return;
            }

            if (e.Key == Key.Escape)
            {
                vm.Cancel(null);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void passwordBox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            LoginViewModel vm = this.DataContext as LoginViewModel;
            if (vm == null)
            {
                return;
            }

            if (e.Key == Key.Enter && !String.IsNullOrEmpty(vm.Password))
            {
                vm.Login(null);
            }
            else if (e.Key == Key.Escape)
            {
                vm.Cancel(null);
            }
        }
        #endregion // Event Handlers

        #region Dialog Help Button and Handler
        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        /// See http://stackoverflow.com/questions/1009983/help-button.
        /// 
        protected override void OnSourceInitialized(EventArgs e)
        {
            base.OnSourceInitialized(e);

            IntPtr hwnd = new System.Windows.Interop.WindowInteropHelper(this).Handle;
            uint styles = API.GetWindowLong(hwnd, API.GWL_STYLE);
            styles &= 0xFFFFFFFF ^ (API.WS_MINIMIZEBOX | API.WS_MAXIMIZEBOX);
            API.SetWindowLong(hwnd, API.GWL_STYLE, styles);
            styles = API.GetWindowLong(hwnd, API.GWL_EXSTYLE);
            styles |= API.WS_EX_CONTEXTHELP;
            API.SetWindowLong(hwnd, API.GWL_EXSTYLE, styles);
            API.SetWindowPos(hwnd, IntPtr.Zero, 0, 0, 0, 0, API.SWP_NOMOVE | API.SWP_NOSIZE | API.SWP_NOZORDER | API.SWP_FRAMECHANGED);
            ((HwndSource)PresentationSource.FromVisual(this)).AddHook(HelpHook);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="hwnd"></param>
        /// <param name="msg"></param>
        /// <param name="wParam"></param>
        /// <param name="lParam"></param>
        /// <param name="handled"></param>
        /// <returns></returns>
        private IntPtr HelpHook(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
        {
            if (API.WM_SYSCOMMAND == msg && ((int)wParam & 0xFFF0) == API.SC_CONTEXTHELP)
            {
                API.ShellExecute(IntPtr.Zero, "open", URL_HELP, String.Empty, String.Empty, 0);
                handled = true;
            }
            return IntPtr.Zero;
        }
        #endregion // Dialog Help Button and Handler
    } // LoginDialog
} // RSG.Base.Windows.Dialogs
