﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Editor;
using RSG.Base.Windows.Helpers;
using System.Windows.Media.Imaging;
using System.Diagnostics;

namespace RSG.Base.Windows.Dialogs
{
    /// <summary>
    /// Login details to provide to the login predicate.
    /// </summary>
    public struct LoginDetails
    {
        public String Username { get; set; }
        public String Password { get; set; }
    }

    /// <summary>
    /// Results of the login returned from the login predicate
    /// </summary>
    public struct LoginResults
    {
        public bool Success { get; set; }
        public String Message { get; set; }
    }

    /// <summary>
    /// View model for the login window.
    /// </summary>
    public class LoginViewModel : ViewModelBase
    {
        #region Constants
        /// <summary>
        /// Default image to use if another one hasn't been specified
        /// </summary>
        private static readonly Uri c_defaultImageSource;
        #endregion // Constants

        #region Members
        private Object m_sync = new object();

        /// <summary>
        /// Callback to check whether the login details are valid.
        /// </summary>
        private Func<LoginDetails, LoginResults> LoginPredicate { get; set; }
        #endregion // Members

        #region Properties and associated member data
        /// <summary>
        /// Title to display in the dialog
        /// </summary>
        public String Title
        {
            get { return m_title; }
            set
            {
                SetPropertyValue(value, m_title, () => this.Title,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_title = (String)newValue;
                        }
                    ));
            }
        }
        private string m_title;

        /// <summary>
        /// Message to display to the user
        /// </summary>
        public String Message
        {
            get { return m_message; }
            set
            {
                SetPropertyValue(value, m_message, () => this.Message,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_message = (String)newValue;
                        }
                    ));
            }
        }
        private string m_message;

        /// <summary>
        /// Image to use to display in the login window
        /// </summary>
        public BitmapImage Image
        {
            get { return m_image; }
            set
            {
                SetPropertyValue(value, m_image, () => this.Image,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_image = (BitmapImage)newValue;
                        }
                    ));
            }
        }
        private BitmapImage m_image;

        /// <summary>
        /// Uri containing the source location of the image to use
        /// </summary>
        public Uri ImageSource
        {
            get { return m_imageSource; }
            set
            {
                if (m_imageSource != value)
                {
                    m_imageSource = value;
                    Image = new BitmapImage(m_imageSource);
                }
            }
        }
        private Uri m_imageSource;

        /// <summary>
        /// 
        /// </summary>
        public Boolean? DialogState
        {
            get { return m_dialogState; }
            set
            {
                SetPropertyValue(value, m_dialogState, () => this.DialogState,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_dialogState = (Boolean?)newValue;
                        }
                    ));
            }
        }
        private Boolean? m_dialogState;

        /// <summary>
        /// Username
        /// </summary>
        public String Username
        {
            get { return m_username; }
            set
            {
                SetPropertyValue(value, m_username, () => this.Username,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_username = (String)newValue;
                        }
                    ));
            }
        }
        private string m_username;

        /// <summary>
        /// Flag indicating whether the username field should be readonly
        /// </summary>
        public bool IsUsernameReadOnly
        {
            get { return m_isUsernameReadonly; }
            set
            {
                SetPropertyValue(value, m_isUsernameReadonly, () => this.IsUsernameReadOnly,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_isUsernameReadonly = (bool)newValue;
                        }
                    ));
            }
        }
        private bool m_isUsernameReadonly;

        /// <summary>
        /// Password
        /// </summary>
        public String Password
        {
            get { return m_password; }
            set
            {
                SetPropertyValue(value, m_password, () => this.Password,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_password = (String)newValue;
                        }
                    ));
            }
        }
        private string m_password;

        /// <summary>
        /// Flag indicating whether the user wishes to be remembered
        /// </summary>
        public bool RememberMe
        {
            get { return m_rememberMe; }
            set
            {
                SetPropertyValue(value, m_rememberMe, () => this.RememberMe,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_rememberMe = (bool)newValue;
                        }
                    ));
            }
        }
        private bool m_rememberMe;

        /// <summary>
        /// Flag specifying whether we should display the "remember me" checkbox
        /// </summary>
        public bool ShowRememberMe
        {
            get { return m_showRememberMe; }
            set
            {
                SetPropertyValue(value, m_showRememberMe, () => this.ShowRememberMe,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_showRememberMe = (bool)newValue;
                        }
                    ));
            }
        }
        private bool m_showRememberMe;

        /// <summary>
        /// Flag specifying whether we should display the login failed message.
        /// </summary>
        public bool ShowLoginFailed
        {
            get { return m_loginFailed; }
            set
            {
                SetPropertyValue(value, m_loginFailed, () => this.ShowLoginFailed,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_loginFailed = (bool)newValue;
                        }
                    ));
            }
        }
        private bool m_loginFailed;

        /// <summary>
        /// Login failure message to display when login failed.
        /// </summary>
        public String LoginFailureMessage
        {
            get { return m_loginFailureMessage; }
            set
            {
                SetPropertyValue(value, m_loginFailureMessage, () => this.LoginFailureMessage,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_loginFailureMessage = (String)newValue;
                        }
                    ));
            }
        }
        private String m_loginFailureMessage;

        /// <summary>
        /// Command hooked up to the cancel button
        /// </summary>
        public RelayCommand CancelCommand
        {
            get
            {
                lock (m_sync)
                {
                    if (m_cancelCommand == null)
                    {
                        m_cancelCommand = new RelayCommand(param => this.Cancel(param));
                    }
                }
                return m_cancelCommand;
            }
        }
        private RelayCommand m_cancelCommand;

        /// <summary>
        /// Command hooked up to the login button
        /// </summary>
        public RelayCommand LoginCommand
        {
            get
            {
                lock (m_sync)
                {
                    if (m_loginCommand == null)
                    {
                        m_loginCommand = new RelayCommand(param => this.Login(param), param => this.CanLogin(param));
                    }
                }
                return m_loginCommand;
            }
        }
        private RelayCommand m_loginCommand;
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor
        /// </summary>
        public LoginViewModel()
            : this(null, "Login Dialog", "Please enter your login details below:", c_defaultImageSource)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        public LoginViewModel(Func<LoginDetails, LoginResults> loginPredicate)
            : this(loginPredicate, "Login Dialog", "Please enter your login details below:", c_defaultImageSource)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="username"></param>
        /// <param name="title"></param>
        /// <param name="message"></param>
        public LoginViewModel(string title, string message)
            : this(null, title, message, c_defaultImageSource)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="username"></param>
        /// <param name="title"></param>
        /// <param name="message"></param>
        public LoginViewModel(Func<LoginDetails, LoginResults> loginPredicate, string title, string message)
            : this(loginPredicate, title, message, c_defaultImageSource)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="username"></param>
        /// <param name="title"></param>
        /// <param name="message"></param>
        /// <param name="imageSource"></param>
        public LoginViewModel(Func<LoginDetails, LoginResults> loginPredicate, string title, string message, Uri imageSource)
            : base()
        {
            LoginPredicate = loginPredicate;
            IsUsernameReadOnly = false;
            Title = title;
            Message = message;
            ImageSource = imageSource;
            ShowRememberMe = false;
            ShowLoginFailed = false;
        }

        /// <summary>
        /// Static constructor is used to ensure that the pack uri helper is initialised so that
        /// the default image source doesn't throw a UriFormatException in console apps.
        /// </summary>
        static LoginViewModel()
        {
            string s = System.IO.Packaging.PackUriHelper.UriSchemePack;
            c_defaultImageSource = new Uri("pack://application:,,,/RSG.Base.Windows;component/Resources/login.png");
        }
        #endregion // Constructor(s)

        #region Commands
        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameter"></param>
        public void Cancel(Object parameter)
        {
            this.DialogState = false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameter"></param>
        public void Login(Object parameter)
        {
            if (LoginPredicate != null)
            {
                LoginResults results = LoginPredicate.Invoke(new LoginDetails { Username = Username, Password = Password });
                if (results.Success)
                {
                    this.DialogState = true;
                }
                else
                {
                    Password = "";
                    ShowLoginFailed = true;
                    LoginFailureMessage = results.Message;
                }
            }
            else
            {
                this.DialogState = true;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameter"></param>
        /// <returns></returns>
        private Boolean CanLogin(Object parameter)
        {
            return !String.IsNullOrEmpty(Password);
        }
        #endregion // Commands
    } // LoginViewModel
} // RSG.Base.Windows.Dialogs
