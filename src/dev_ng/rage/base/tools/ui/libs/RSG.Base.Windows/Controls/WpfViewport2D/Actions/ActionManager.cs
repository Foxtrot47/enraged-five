﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Data;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Collections;
using RSG.Base.Windows.Controls.WpfViewport2D;
using RSG.Base.Windows.Controls.WpfViewport2D.Controls;

namespace RSG.Base.Windows.Controls.WpfViewport2D.Actions
{
    public enum ToggleTypes
    {
        Primary,
        Secondary,
    }

    [Flags]
    public enum MouseEvents
    {
        Double_Click = 1,
        Mouse_Down = 2,
        Mouse_Up = 4,
        Wheel_Move = 8,
        Mouse_Move = 16,
    }

    /// <summary>
    /// Represents a toggle action, neither secondary or primary
    /// </summary>
    public class ToggleAction : INotifyPropertyChanged
    {
        #region Events

        /// <summary>
        /// Property changed event fired when the name changes so that the 
        /// binding can be dynamic.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Create the OnPropertyChanged method to raise the event
        /// </summary>
        /// <param name="name"></param>
        protected void OnPropertyChanged(String name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

        #endregion

        #region Properties

        /// <summary>
        /// A reference to the action manager that owns this action
        /// </summary>
        private ActionManager ActionManager
        {
            get { return m_actionManager; }
            set
            {
                m_actionManager = value;
                OnPropertyChanged("ActionManager");
            }
        }
        private ActionManager m_actionManager;

        /// <summary>
        /// The type of toggle action
        /// </summary>
        private ToggleTypes Type
        {
            get { return m_type; }
            set
            {
                m_type = value;
                OnPropertyChanged("Type");
            }
        }
        private ToggleTypes m_type;

        /// <summary>
        /// The action that is taken when the element is checked.
        /// </summary>
        private Action ToggleOn
        {
            get { return m_toggleOn; }
            set
            {
                m_toggleOn = value;
                OnPropertyChanged("ToggleOn");
            }
        }
        private Action m_toggleOn;

        /// <summary>
        /// The action that is taken when the element is unchecked.
        /// </summary>
        private Action ToggleOff
        {
            get { return m_toggleOff; }
            set
            {
                m_toggleOff = value;
                OnPropertyChanged("ToggleOff");
            }
        }
        private Action m_toggleOff;

        /// <summary>
        /// A value that represents if this action is toggled (bound to the uielement two ways)
        /// </summary>
        public bool? IsToggled
        {
            get { return m_isToggled; }
            set
            {
                m_isToggled = value;
                OnPropertyChanged("IsToggled");
                OnToggleChanged();
            }
        }
        private bool? m_isToggled;

        /// <summary>
        /// A value that represents whether this action is a default primary action (there can only be one)
        /// </summary>
        public Boolean IsDefault
        {
            get { return m_isDefault; }
            set { m_isDefault = value; OnPropertyChanged("IsDefault"); }
        }
        private Boolean m_isDefault;

        #endregion // Properties

        #region Constructor(s)

        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="type">Toggle type - Primary/Secondary</param>
        /// <param name="uiElement">The element that controls this toggle</param>
        /// <param name="toggleOn">The action to perform when it is toggled</param>
        /// <param name="toggleOff">The action to perform when it is un toggled</param>
        /// <param name="actionManager">The action manager that owns this action</param>
        public ToggleAction(ToggleTypes type, FrameworkElement uiElement, Action toggleOn, Action toggleOff, ActionManager actionManager, Boolean isDefault)
        {
            this.IsDefault = isDefault;
            this.Type = type;
            this.ToggleOn = toggleOn;
            this.ToggleOff = toggleOff;
            this.ActionManager = actionManager;

            System.Reflection.PropertyInfo test1 = uiElement.GetType().GetProperty("IsChecked");
            this.IsToggled = (bool?)test1.GetValue(uiElement, null);


            // Add a binding from the is checked property to the IsToggled property
            Binding binding = new Binding("IsToggled");
            binding.Source = this;
            binding.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
            binding.Mode = BindingMode.TwoWay;

            DependencyProperty dp = (DependencyProperty)uiElement.GetType().GetField("IsCheckedProperty").GetValue(uiElement);
            BindingOperations.SetBinding(uiElement, dp, binding);
        }

        #endregion // Constructor(s)

        #region Private Function(s)

        /// <summary>
        /// Called whenever the toggle state is changed
        /// </summary>
        private void OnToggleChanged()
        {
            if (this.Type == ToggleTypes.Primary)
            {
                if (this.IsToggled == true)
                {
                    this.ToggleOn();

                    if (this.ActionManager != null)
                    {
                        foreach (ToggleAction action in this.ActionManager.RegisteredToggleActions.Where(a => a != this))
                        {
                            action.IsToggled = false;
                        }
                    }
                }
                else
                {
                    if (this.IsDefault == false)
                    {
                        this.ToggleOff();
                        if (this.ActionManager != null)
                        {
                            foreach (ToggleAction action in this.ActionManager.RegisteredToggleActions.Where(a => a != this))
                            {
                                if (action.IsDefault == true)
                                {
                                    action.IsToggled = true;
                                }
                            }
                        }
                    }
                }
            }
            else if (this.Type == ToggleTypes.Secondary)
            {
                if (this.IsToggled == true)
                {
                    this.ToggleOn();
                }
                else
                {
                    this.ToggleOff();
                }
            }
        }

        #endregion // Private Function(s)

    }

    /// <summary>
    /// This is a top level class for the actions on a viewport.
    /// This is responsible for registering controls to the viewport
    /// and makeing sure that messages are past between them.
    /// </summary>
    public class ActionManager
    {
        #region Class Helpers

        #endregion // Class Helpers

        #region Events

        public event System.Windows.Input.KeyEventHandler KeyUp;
        public event System.Windows.Input.KeyEventHandler KeyDown;
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

        #endregion // Events

        #region Properties

        /// <summary>
        /// The UI element that this action manager is attached to
        /// </summary>
        public ActionElement ActionElement
        {
            get { return m_actionElement; }
            set { m_actionElement = value; }
        }
        private ActionElement m_actionElement;

        /// <summary>
        /// A observable list of UI elements that have been registered to this action manager.
        /// </summary>
        private ObservableCollection<ActionElementControl> RegisteredControls
        {
            get { return m_registeredControls; }
            set { m_registeredControls = value; }
        }
        private ObservableCollection<ActionElementControl> m_registeredControls;

        /// <summary>
        /// A collection of all the registered toggle actions so that primary actions
        /// can be sorted
        /// </summary>
        public ObservableCollection<ToggleAction> RegisteredToggleActions
        {
            get { return m_registeredToggleActions; }
            set { m_registeredToggleActions = value; }
        }
        private ObservableCollection<ToggleAction> m_registeredToggleActions;

        #region Mouse Event Registers

        /// <summary>
        /// A list of the controls currently registed to recieve the double click mouse event
        /// </summary>
        List<ActionElementControl> DoubleClickRegisters
        {
            get { return m_doubleClickRegisters; }
            set { m_doubleClickRegisters = value; }
        }
        private List<ActionElementControl> m_doubleClickRegisters = new List<ActionElementControl>();

        /// <summary>
        /// A list of the controls currently registed to recieve the mouse down mouse event
        /// </summary>
        List<ActionElementControl> MouseDownRegisters
        {
            get { return m_mouseDownRegisters; }
            set { m_mouseDownRegisters = value; }
        }
        private List<ActionElementControl> m_mouseDownRegisters = new List<ActionElementControl>();

        /// <summary>
        /// A list of the controls currently registed to recieve the mouse move mouse event
        /// </summary>
        List<ActionElementControl> MouseMoveRegisters
        {
            get { return m_mouseMoveRegisters; }
            set { m_mouseMoveRegisters = value; }
        }
        private List<ActionElementControl> m_mouseMoveRegisters = new List<ActionElementControl>();

        /// <summary>
        /// A list of the controls currently registed to recieve the mouse up mouse event
        /// </summary>
        List<ActionElementControl> MouseUpRegisters
        {
            get { return m_mouseUpRegisters; }
            set { m_mouseUpRegisters = value; }
        }
        private List<ActionElementControl> m_mouseUpRegisters = new List<ActionElementControl>();

        /// <summary>
        /// A list of the controls currently registed to recieve the wheel move mouse event
        /// </summary>
        List<ActionElementControl> WheelMoveRegisters
        {
            get { return m_wheelMoveRegisters; }
            set { m_wheelMoveRegisters = value; }
        }
        private List<ActionElementControl> m_wheelMoveRegisters = new List<ActionElementControl>();

        #endregion Mouse Event Registers

        #endregion // Properties

        #region Constructor

        /// <summary>
        /// Default Constructor
        /// </summary>
        public ActionManager()
        {
            this.RegisteredControls = new ObservableCollection<ActionElementControl>();
            this.RegisteredToggleActions = new ObservableCollection<ToggleAction>();
        }

        #endregion // Constructor

        #region Event Helpers

        /// <summary>
        /// Gets called when the action element sends a mouse double click event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void OnMouseDoubleClick(Object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            foreach (ActionElementControl control in this.DoubleClickRegisters)
            {
                control.OnManagerMouseDoubleClick(sender, e);
            }
        }

        /// <summary>
        /// Gets called when the action element sends a mouse button down event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void OnMouseDown(Object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            foreach (ActionElementControl control in this.MouseDownRegisters)
            {
                control.OnManagerMouseDown(sender, e);
            }
        }

        /// <summary>
        /// Gets called when the action element sends a mouse button up event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void OnMouseUp(Object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            foreach (ActionElementControl control in this.MouseUpRegisters)
            {
                control.OnManagerMouseUp(sender, e);
            }
        }

        /// <summary>
        /// Gets called when the action element sends a mouse wheel move event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void OnMouseWheelMove(Object sender, System.Windows.Input.MouseWheelEventArgs e)
        {
            foreach (ActionElementControl control in this.WheelMoveRegisters)
            {
                control.OnManagerMouseWheelMove(sender, e);
            }
        }

        /// <summary>
        /// Gets called when the action element sends a mouse move event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void OnMouseMove(Object sender, System.Windows.Input.MouseEventArgs e)
        {
            foreach (ActionElementControl control in this.MouseMoveRegisters)
            {
                control.OnManagerMouseMove(sender, e);
            }
        }

        /// <summary>
        /// Gets called when the action element sends a key up event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void OnKeyUp(Object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (this.KeyUp != null)
                this.KeyUp(sender, e);
        }

        /// <summary>
        /// Gets called when the action element sends a key down event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void OnKeyDown(Object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (this.KeyDown != null)
                this.KeyDown(sender, e);
        }

        /// <summary>
        /// This gets called whenever a property on the action element changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void OnPropertyChanged(Object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (this.PropertyChanged != null)
                this.PropertyChanged(sender, e);
        }

        #endregion Event Helpers

        #region Register Function(s)

        /// <summary>
        /// Registers this action manager to a specific action element.
        /// </summary>
        /// <param name="element"></param>
        public void RegisterActionManagerWithActionElement(ActionElement element)
        {
            if (this.ActionElement != null)
            {
                UnRegisterActionManagerFromActionElement();
            }
            this.ActionElement = element;

            this.ActionElement.MouseDoubleClick += OnMouseDoubleClick;
            this.ActionElement.MouseDown += OnMouseDown;
            this.ActionElement.MouseUp += OnMouseUp;
            this.ActionElement.MouseWheel += OnMouseWheelMove;
            this.ActionElement.MouseMove += OnMouseMove;
            this.ActionElement.KeyUp += OnKeyUp;
            this.ActionElement.KeyDown += OnKeyDown;
            this.ActionElement.PropertyChanged += OnPropertyChanged;
        }

        /// <summary>
        /// Unregisters the action manager with whatever action element it is currently
        /// registered to.
        /// </summary>
        public void UnRegisterActionManagerFromActionElement()
        {
            if (this.ActionElement != null)
            {
                this.ActionElement.MouseDoubleClick -= OnMouseDoubleClick;
                this.ActionElement.MouseDown -= OnMouseDown;
                this.ActionElement.MouseUp -= OnMouseUp;
                this.ActionElement.MouseWheel -= OnMouseWheelMove;
                this.ActionElement.MouseMove -= OnMouseMove;
                this.ActionElement.KeyUp -= OnKeyUp;
                this.ActionElement.KeyDown -= OnKeyDown;
                this.ActionElement.PropertyChanged -= OnPropertyChanged;

                this.ActionElement = null;
            }
        }

        /// <summary>
        /// This registers a action control to this manager
        /// </summary>
        public void RegisterActionElementControl(ActionElementControl control)
        {
            if (!this.RegisteredControls.Contains(control))
            {
                control.RegisterControlWithManager(this);
                this.RegisteredControls.Add(control);
            }
        }

        /// <summary>
        /// This registers a action control to this manager
        /// </summary>
        public void UnRegisterActionElementControl(ActionElementControl control)
        {
            if (this.RegisteredControls.Contains(control))
            {
                control.UnRegisterControlWithManager(this);
                this.RegisteredControls.Remove(control);
            }
        }

        /// <summary>
        /// Sets up the command action so that when the routed event fires the action
        /// gets called
        /// </summary>
        /// <param name="uiElement"></param>
        /// <param name="triggerEvent"></param>
        /// <param name="action"></param>
        public void RegisterCommandAction(FrameworkElement uiElement, RoutedEvent triggerEvent, Action action)
        {
            uiElement.AddHandler(triggerEvent, 
                new RoutedEventHandler
                ( 
                    delegate(Object sender, RoutedEventArgs e)
                    {
                        action();
                    }
                ));
        }

        /// <summary>
        /// Register a control to receive the given mouse events
        /// </summary>
        /// <param name="control"></param>
        /// <param name="events"></param>
        public void RegisterMouseCommand(ActionElementControl control, MouseEvents events)
        {
            if ((events & MouseEvents.Double_Click) == MouseEvents.Double_Click)
            {
                if (!this.DoubleClickRegisters.Contains(control))
                {
                    this.DoubleClickRegisters.Add(control);
                }
            }
            if ((events & MouseEvents.Mouse_Down) == MouseEvents.Mouse_Down)
            {
                if (!this.MouseDownRegisters.Contains(control))
                {
                    this.MouseDownRegisters.Add(control);
                }
            }
            if ((events & MouseEvents.Mouse_Move) == MouseEvents.Mouse_Move)
            {
                if (!this.MouseMoveRegisters.Contains(control))
                {
                    this.MouseMoveRegisters.Add(control);
                }
            }
            if ((events & MouseEvents.Mouse_Up) == MouseEvents.Mouse_Up)
            {
                if (!this.MouseUpRegisters.Contains(control))
                {
                    this.MouseUpRegisters.Add(control);
                }
            }
            if ((events & MouseEvents.Wheel_Move) == MouseEvents.Wheel_Move)
            {
                if (!this.WheelMoveRegisters.Contains(control))
                {
                    this.WheelMoveRegisters.Add(control);
                }
            }
        }

        /// <summary>
        /// Unregister a control to receive the given mouse events
        /// </summary>
        /// <param name="control"></param>
        /// <param name="events"></param>
        public void UnregisterMouseCommand(ActionElementControl control, MouseEvents events)
        {
            if ((events & MouseEvents.Double_Click) == MouseEvents.Double_Click)
            {
                if (this.DoubleClickRegisters.Contains(control))
                {
                    this.DoubleClickRegisters.Remove(control);
                }
            }
            if ((events & MouseEvents.Mouse_Down) == MouseEvents.Mouse_Down)
            {
                if (this.MouseDownRegisters.Contains(control))
                {
                    this.MouseDownRegisters.Remove(control);
                }
            }
            if ((events & MouseEvents.Mouse_Move) == MouseEvents.Mouse_Move)
            {
                if (this.MouseMoveRegisters.Contains(control))
                {
                    this.MouseMoveRegisters.Remove(control);
                }
            }
            if ((events & MouseEvents.Mouse_Up) == MouseEvents.Mouse_Up)
            {
                if (this.MouseUpRegisters.Contains(control))
                {
                    this.MouseUpRegisters.Remove(control);
                }
            }
            if ((events & MouseEvents.Wheel_Move) == MouseEvents.Wheel_Move)
            {
                if (this.WheelMoveRegisters.Contains(control))
                {
                    this.WheelMoveRegisters.Remove(control);
                }
            }
        }

        /// <summary>
        /// Registers a toggle action with the manager.
        /// </summary>
        /// <param name="type"></param>
        /// <param name="uiElement"></param>
        /// <param name="triggerEvent"></param>
        /// <param name="toggleOnAction"></param>
        /// <param name="toggleOffAction"></param>
        public void RegisterSecondaryToggleAction(FrameworkElement uiElement, Action toggleOnAction, Action toggleOffAction)
        {
            System.Reflection.PropertyInfo[] properties = uiElement.GetType().GetProperties();
            Boolean validProperty = false;
            foreach (System.Reflection.PropertyInfo property in properties)
            {
                if (property.Name == "IsChecked")
                {
                    validProperty = true;
                    break;
                }
            }

            if (validProperty == true)
            {
                ToggleAction newAction = new ToggleAction(ToggleTypes.Secondary, uiElement, toggleOnAction, toggleOffAction, this, false);
                this.RegisteredToggleActions.Add(newAction);
            }
        }

        /// <summary>
        /// Registers a primary toggle action with the manager. This action is sorted in the manager so that any other primary actions
        /// can be manipulated correctly.
        /// </summary>
        /// <param name="type"></param>
        /// <param name="uiElement"></param>
        /// <param name="triggerEvent"></param>
        /// <param name="toggleOnAction"></param>
        /// <param name="toggleOffAction"></param>
        public void RegisterPrimaryToggleAction(FrameworkElement uiElement, Action toggleOnAction, Action toggleOffAction)
        {
            System.Reflection.PropertyInfo[] properties = uiElement.GetType().GetProperties();
            Boolean validProperty = false;
            foreach (System.Reflection.PropertyInfo property in properties)
            {
                if (property.Name == "IsChecked")
                {
                    validProperty = true;
                    break;
                }
            }

            if (validProperty == true)
            {
                ToggleAction newAction = new ToggleAction(ToggleTypes.Primary, uiElement, toggleOnAction, toggleOffAction, this, false);
                this.RegisteredToggleActions.Add(newAction);
            }
        }

        /// <summary>
        /// Registers a primary toggle action with the manager that is to be the default primary toggle. 
        /// This action is sorted in the manager so that any other primary actions
        /// can be manipulated correctly.
        /// </summary>
        /// <param name="uiElement"></param>
        /// <param name="triggerEvent"></param>
        /// <param name="toggleOnAction"></param>
        /// <param name="toggleOffAction"></param>
        public void RegisterDefaultPrimaryToggleAction(FrameworkElement uiElement, Action toggleOnAction, Action toggleOffAction)
        {
            System.Reflection.PropertyInfo[] properties = uiElement.GetType().GetProperties();
            Boolean validProperty = false;
            foreach (System.Reflection.PropertyInfo property in properties)
            {
                if (property.Name == "IsChecked")
                {
                    validProperty = true;
                    break;
                }
            }

            if (validProperty == true)
            {
                foreach (ToggleAction action in this.RegisteredToggleActions)
                {
                    action.IsDefault = false;
                }

                ToggleAction newAction = new ToggleAction(ToggleTypes.Primary, uiElement, toggleOnAction, toggleOffAction, this, true);
                this.RegisteredToggleActions.Add(newAction);
            }
        }

        #endregion // Register Function(s)

    }

}
