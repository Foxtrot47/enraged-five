﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;

namespace RSG.Base.Windows.Converters
{
    /// <summary>
    /// This takes two values as input (low and high) and uses the parameter to interpolate between them.
    /// </summary>
    public abstract class InterpolationConverterBase<T> : IMultiValueConverter
    {
        public Object Convert(Object[] values, Type targetType, Object parameter, System.Globalization.CultureInfo culture)
        {
            T low;
            T high;
            double offset;

            try
            {
                low = (T)values[0];
                high = (T)values[1];
                offset = Double.Parse((String)parameter);
            }
            catch
            {
                RSG.Base.Logging.Log.Log__Error("There has been an error in the InterpolationConverter multibinding.");
                return 0.0f;
            }

            return GetValue(low, high, offset);
        }

        public Object[] ConvertBack(Object value, Type[] targetTypes, Object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        protected abstract double GetValue(T low, T high, double offset);
    } // InterpolationConverterBase<T>


    /// <summary>
    /// 
    /// </summary>
    public class UIntInterpolationConverter : InterpolationConverterBase<uint>
    {
        protected override double GetValue(uint low, uint high, double offset)
        {
            return low + ((high - low) * offset);
        }
    } // UIntInterpolationConverter
}
