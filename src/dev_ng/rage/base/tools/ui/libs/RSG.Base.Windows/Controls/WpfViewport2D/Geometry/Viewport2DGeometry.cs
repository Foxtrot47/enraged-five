﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Math;
using RSG.Base.Windows.Controls.WpfViewport2D;

namespace RSG.Base.Windows.Controls.WpfViewport2D.Geometry
{
    public enum etCoordSpace
    {
        World,  // Geometry defined in World-Space
    }

    /// <summary>
    /// 
    /// </summary>
    public abstract class Viewport2DGeometry : INotifyPropertyChanged
    {
        #region Events

        /// <summary>
        /// Property changed event fired when the name changes so that the 
        /// binding can be dynamic.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Create the OnPropertyChanged method to raise the event
        /// </summary>
        /// <param name="name"></param>
        protected void OnPropertyChanged(String name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

        #endregion

        #region Constants
        
        protected static readonly String DEFAULT_NAME = "Untitled_Geometry2D";
        
        #endregion // Constants

        #region Properties and Associated Member Data

        /// <summary>
        /// 
        /// </summary>
        public String Name
        {
            get { return m_name; }
            set { m_name = value; OnPropertyChanged("Name"); }
        }
        private String m_name;

        /// <summary>
        /// 
        /// </summary>
        public etCoordSpace CoordinateSpace
        {
            get { return m_eCoordinateSpace; }
            set { m_eCoordinateSpace = value; OnPropertyChanged("CoordinateSpace"); }
        }
        private etCoordSpace m_eCoordinateSpace;

        /// <summary>
        /// This object is the object used when the geometry is picked
        /// in the viewport.
        /// </summary>
        public Object PickData
        {
            get { return m_pickData; }
            set { m_pickData = value; }
        }
        private Object m_pickData;

        /// <summary>
        /// This object is the object used when the geometry is picked
        /// in the viewport.
        /// </summary>
        public Object ToolTip
        {
            get { return m_tooltip; }
            set { m_tooltip = value; }
        }
        private Object m_tooltip;

        /// <summary>
        /// If this is not empty or null it will be shown in
        /// the viewport at the center of the geometry
        /// </summary>
        public String UserText
        {
            get { return m_userText; }
            set { m_userText = value; OnPropertyChanged("UserText"); }
        }
        private String m_userText;

        /// <summary>
        /// Represents whether the geometry is selected and therefore needs to be rendered differently
        /// </summary>
        public Boolean IsSelected
        {
            get { return m_isSelected; }
            set { m_isSelected = value; OnPropertyChanged("IsSelected"); }
        }
        private Boolean m_isSelected;

        #endregion // Properties and Associated Member Data

        #region Constructors

        /// <summary>
        /// Default constructor
        /// </summary>
        public Viewport2DGeometry(etCoordSpace space)
        {
            this.Name = DEFAULT_NAME;
            this.CoordinateSpace = space;
            this.PickData = null;
            this.UserText = String.Empty;
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="space"></param>
        /// <param name="name"></param>
        public Viewport2DGeometry(etCoordSpace space, String name)
        {
            this.Name = name;
            this.CoordinateSpace = space;
            this.PickData = null;
            this.UserText = String.Empty;
        }

        #endregion // Constructors

        /// <summary>
        /// Return bounding box for 2D geometry in world-space.
        /// </summary>
        /// <returns></returns>
        public abstract BoundingBox2f GetWorldBoundingBox(Viewport2D viewport);

        /// <summary>
        /// Determines if the geometry has been picked given the screen
        /// position of the pick
        /// </summary>
        /// <param name="screenPosition"></param>
        /// <returns></returns>
        public abstract Boolean HasBeenPicked(Point viewPosition, Viewport2D viewport);

    }
}
