﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace RSG.Base.Windows.Controls.WpfViewport2D.Controls
{
    /// <summary>
    /// Interaction logic for EmbeddedPanControl.xaml
    /// </summary>
    public partial class EmbeddedPanControl :
        ViewportUserControl
    {
        public EmbeddedPanControl()
        {
            InitializeComponent();
        }

        #region Event Helpers

        /// <summary>
        /// Gets called when the action element sends a mouse double click event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public override void OnManagerMouseDoubleClick(Object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
        }

        /// <summary>
        /// Gets called when the action element sends a mouse button down event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public override void OnManagerMouseDown(Object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
        }

        /// <summary>
        /// Gets called when the action element sends a mouse button up event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public override void OnManagerMouseUp(Object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
        }

        /// <summary>
        /// Gets called when the action element sends a mouse wheel move event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public override void OnManagerMouseWheelMove(Object sender, System.Windows.Input.MouseWheelEventArgs e)
        {
        }

        /// <summary>
        /// Gets called when the action element sends a mouse move event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public override void OnManagerMouseMove(Object sender, System.Windows.Input.MouseEventArgs e)
        {
        }

        /// <summary>
        /// Gets called when the action element sends a key up event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public override void OnManagerKeyUp(Object sender, System.Windows.Input.KeyEventArgs e)
        {
        }

        /// <summary>
        /// Gets called when the action element sends a key down event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public override void OnManagerKeyDown(Object sender, System.Windows.Input.KeyEventArgs e)
        {
        }

        /// <summary>
        /// This gets called whenever a property on the action element changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public override void OnManagerPropertyChanged(Object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            base.OnManagerPropertyChanged(sender, e);
        }

        #endregion Event Helpers

        public override void RegisterActionsWithManager(Actions.ActionManager manager)
        {
            manager.RegisterCommandAction(this.PanCenter, Button.ClickEvent,
                new Action
                (
                    delegate()
                    {
                        if (manager.ActionElement is Viewport2D)
                        {
                            (manager.ActionElement as Viewport2D).Center();
                        }
                    }
                )
            );

            manager.RegisterCommandAction(this.PanRight, Button.ClickEvent,
                new Action
                (
                    delegate()
                    {
                        if (manager.ActionElement is Viewport2D)
                        {
                            (manager.ActionElement as Viewport2D).Pan(new Vector(-500.0, 0.0));
                        }
                    }
                )
            );

            manager.RegisterCommandAction(this.PanLeft, Button.ClickEvent,
                new Action
                (
                    delegate()
                    {
                        if (manager.ActionElement is Viewport2D)
                        {
                            (manager.ActionElement as Viewport2D).Pan(new Vector(500.0, 0.0));
                        }
                    }
                )
            );

            manager.RegisterCommandAction(this.PanUp, Button.ClickEvent,
                new Action
                (
                    delegate()
                    {
                        if (manager.ActionElement is Viewport2D)
                        {
                            (manager.ActionElement as Viewport2D).Pan(new Vector(0.0, 500.0));
                        }
                    }
                )
            );

            manager.RegisterCommandAction(this.PanDown, Button.ClickEvent,
                new Action
                (
                    delegate()
                    {
                        if (manager.ActionElement is Viewport2D)
                        {
                            (manager.ActionElement as Viewport2D).Pan(new Vector(0.0, -500.0));
                        }
                    }
                )
            );

            base.RegisterActionsWithManager(manager);
        }

    }
}
