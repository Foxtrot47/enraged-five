﻿using System;
using System.ComponentModel;
using System.Collections.ObjectModel;
using System.Threading;
using System.Windows.Threading;
using RSG.Base.Logging;
using System.Windows.Documents;
using System.Windows.Media;

namespace RSG.Base.Windows.Controls
{
    public class SingleMessage
    {
        public String TimeStamp { get; private set; }

        public String Type { get; private set; }

        public String Message { get; private set; }

        public Boolean InfoMessage { get; private set; }
        public Boolean DebugMessage { get; private set; }
        public Boolean WarningMessage { get; private set; }
        public Boolean ErrorMessage { get; private set; }

        public SingleMessage(String timeStamp, String type, String message)
        {
            TimeStamp = timeStamp;
            Type = type;
            Message = message;

            InfoMessage = false;
            DebugMessage = false;
            WarningMessage = false;
            ErrorMessage = false;

            if (type == "Info")
            {
                InfoMessage = true;
            }
            else if (type == "Debug")
            {
                DebugMessage = true;
            }
            else if (type == "Warning")
            {
                WarningMessage = true;
            }
            else if (type == "Error")
            {
                ErrorMessage = true;
            }
        }
    }

    /// <summary>
    /// Logger similar to the old application logger however stores the data locally in a property
    /// so that it can be bounded to.
    /// </summary>
    public class BoundedApplicationLogger
    {
        private const String InfoPrefix = "Info";
        private const String DebugPrefix = "Debug";
        private const String WarningPrefix = "Warning";
        private const String ErrorPrefix = "Error";

        public event EventHandler MessagesChanged;

        #region Properties

        public ObservableCollection<SingleMessage> Runs
        {
            get { return m_runs; }
            set { m_runs = value; }
        }
        private ObservableCollection<SingleMessage> m_runs;

        private Dispatcher Dispatcher
        {
            get;
            set;
        }

        #endregion // Properties

        #region Constructor(s)

        /// <summary>
        /// Default 
        /// </summary>
        public BoundedApplicationLogger()
        {
            Log.StaticLog.LogMessage += this.LogMessage;
#if DEBUG
            Log.StaticLog.LogDebug += this.LogDebug;
#endif // DEBUG
            Log.StaticLog.LogWarning += this.LogWarning;
            Log.StaticLog.LogError += this.LogError;

            this.Runs = new ObservableCollection<SingleMessage>();
            this.Dispatcher = Dispatcher.CurrentDispatcher;
        }

        #endregion // Constructor(s)

        #region Event Handlers

        /// <summary>
        /// Log message event handler
        /// </summary>        
        public virtual void LogMessage(Object sender, LogMessageEventArgs e)
        {
            this.Dispatcher.BeginInvoke(
                new Action(
                    delegate()
                    {
                        String[] delimiters = new String[] { "\n", "\r" };
                        String[] messages = e.Message.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                        foreach (String message in messages)
                        {
                            this.Runs.Add(new SingleMessage(DateTime.Now.ToString(), InfoPrefix, message));
                        }
                        if (MessagesChanged != null)
                            MessagesChanged(this, EventArgs.Empty);
                    }
                ));
        }

        /// <summary>
        /// Log debug message event handler
        /// </summary>
        public virtual void LogDebug(Object sender, LogMessageEventArgs e)
        {
#if DEBUG
            this.Dispatcher.BeginInvoke(
                new Action(
                    delegate()
                    {
                        String[] delimiters = new String[] { "\n", "\r" };
                        String[] messages = e.Message.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                        foreach (String message in messages)
                        {
                            this.Runs.Add(new SingleMessage(DateTime.Now.ToString(), DebugPrefix, message));
                        }
                        if (MessagesChanged != null)
                            MessagesChanged(this, EventArgs.Empty);
                    }
                ));
#endif // DEBUG
        }

        /// <summary>
        /// Log warning message event handler
        /// </summary>
        public virtual void LogWarning(Object sender, LogMessageEventArgs e)
        {
            this.Dispatcher.BeginInvoke(
                new Action(
                    delegate()
                    {
                        String[] delimiters = new String[] { "\n", "\r" };
                        String[] messages = e.Message.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                        foreach (String message in messages)
                        {
                            this.Runs.Add(new SingleMessage(DateTime.Now.ToString(), WarningPrefix, message));
                        }
                        if (MessagesChanged != null)
                            MessagesChanged(this, EventArgs.Empty);
                    }
                ));
        }

        /// <summary>
        /// Log error message event handler
        /// </summary>
        public virtual void LogError(Object sender, LogMessageEventArgs e)
        {
            this.Dispatcher.BeginInvoke(
                new Action(
                    delegate()
                    {
                        String[] delimiters = new String[] { "\n", "\r" };
                        String[] messages = e.Message.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                        foreach (String message in messages)
                        {
                            this.Runs.Add(new SingleMessage(DateTime.Now.ToString(), ErrorPrefix, message));
                        }
                        if (MessagesChanged != null)
                            MessagesChanged(this, EventArgs.Empty);
                    }
                ));
        }

        #endregion // Event Handlers

    } // BoundedApplicationLogger
} // RSG.Base.Windows.Controls
