﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;

namespace RSG.Base.Windows.Extensions
{
    /// <summary>
    /// System.Windows.Media.Color extensions
    /// </summary>
    public static class ColorExtensions
    {
        /// <summary>
        /// Convert a Color to it's integer representation
        /// </summary>
        /// <param name="colour"></param>
        /// <returns></returns>
        public static uint ToInteger(this Color colour)
        {
            return (uint)(((colour.A << 24) | (colour.R << 16) | (colour.G << 8) | colour.B) & 0xffffffffL);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="colour"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static Color FromInteger(uint value)
        {
            Color colour = new Color();
            colour.A = (byte)((value >> 24) & 0xff);
            colour.R = (byte)((value >> 16) & 0xff);
            colour.G = (byte)((value >> 8) & 0xff);
            colour.B = (byte)(value & 0xff);
            return colour;
        }
    } // ColorExtensions
}
