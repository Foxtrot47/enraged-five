﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Automation.Peers;
using System.Windows.Automation.Provider;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Threading;

namespace RSG.Base.Windows.Controls
{

    /// <summary>
    /// TimeoutButton is a regular button which can have an optional timeout
    /// value which means the button is clicked after the timeout.
    /// </summary>
    public class TimeoutButton : Button
    {
        #region Properties
        /// <summary>
        /// Timeout property; defines the timeout value (default: no timeout 
        /// regular button, 0 seconds).
        /// </summary>
        public static DependencyProperty TimeoutProperty = DependencyProperty.Register(
            "Timeout", typeof(uint), typeof(TimeoutButton),
            new FrameworkPropertyMetadata(default(uint), OnTimeoutChanged)
            {
                BindsTwoWayByDefault = true,
                DefaultUpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged
            });

        public uint Timeout
        {
            get { return (uint)GetValue(TimeoutProperty); }
            set { SetValue(TimeoutProperty, value); }
        }

        private static void OnTimeoutChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            TimeoutButton button = o as TimeoutButton;
            if (button != null)
                button.OnTimeoutChanged((uint)e.OldValue, (uint)e.NewValue);
        }

        protected virtual void OnTimeoutChanged(uint oldValue, uint newValue)
        {
        }

        /// <summary>
        /// Countdown Property; defines the current countdown time.
        /// </summary>
        public static DependencyProperty CountdownProperty = DependencyProperty.Register(
            "Countdown", typeof(uint), typeof(TimeoutButton),
            new FrameworkPropertyMetadata(default(uint), OnCountdownChanged)
            {
                BindsTwoWayByDefault = true,
                DefaultUpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged
            });

        protected uint Countdown
        {
            get { return (uint)GetValue(CountdownProperty); }
            set { SetValue(CountdownProperty, value); }
        }

        private static void OnCountdownChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            TimeoutButton button = o as TimeoutButton;
            if (button != null)
                button.OnCountdownChanged((uint)e.OldValue, (uint)e.NewValue);
        }

        protected virtual void OnCountdownChanged(uint oldValue, uint newValue)
        {
        }
        #endregion // Properties

        #region Member Data
        /// <summary>
        /// Timer; fired every second until timeout expires.
        /// </summary>
        private System.Timers.Timer Timer;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public TimeoutButton()
            : base()
        {
            this.Timer = new System.Timers.Timer();
            this.Timer.Interval = 1000; // second resolution.
            this.Timer.AutoReset = true;
            this.Timer.Elapsed += TimerElapsed;
        }
        #endregion // Constructor(s)

        #region Public Methods
        /// <summary>
        /// Start the timeout button.
        /// </summary>
        public void Start()
        {
            this.Countdown = this.Timeout;
            this.Timer.Start();
        }

        /// <summary>
        /// Stop the timeout button.
        /// </summary>
        public void Stop()
        {
            this.Timer.Stop();
        }
        #endregion // Public Methods

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TimerElapsed(Object sender, EventArgs e)
        {
            if (this.Dispatcher.CheckAccess())
            {           
                HandleAutomatedClick();
            }
            else
            {
                this.Dispatcher.Invoke(DispatcherPriority.Normal,
                    (Action)delegate() { HandleAutomatedClick(); } );
            }
        }

        /// <summary>
        /// Handle automated click, or tick the countdown.
        /// </summary>
        private void HandleAutomatedClick()
        {
            if (this.Countdown <= 0)
            {
                // http://stackoverflow.com/questions/728432/how-to-programmatically-click-a-button-in-wpf
                ButtonAutomationPeer peer = new ButtonAutomationPeer(this);
                IInvokeProvider invokeProvider = peer.GetPattern(PatternInterface.Invoke) as IInvokeProvider;
                invokeProvider.Invoke();

                this.Timer.Stop();
                this.Timer.Elapsed -= TimerElapsed;
            }
            else
            {
                --this.Countdown;
            }
        }
        #endregion // Private Methods
    }



} // RSG.Base.Windows.Controls namespace
