﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Reflection;

namespace RSG.Base.Windows.DragDrop.Utilities
{
    /// <summary>
    /// 
    /// </summary>
    public class TypeUtil
    {
        public static IEnumerable CreateDynamicallyTypedList(IEnumerable source)
        {
            Type type = GetCommonBaseClass(source);
            Type listType = typeof(List<>).MakeGenericType(type);
            MethodInfo addMethod = listType.GetMethod("Add");
            Object list = listType.GetConstructor(Type.EmptyTypes).Invoke(null);

            foreach (Object o in source)
            {
                addMethod.Invoke(list, new[] { o });
            }

            return (IEnumerable)list;
        }

        public static Type GetCommonBaseClass(IEnumerable e)
        {
            List<Type> types = new List<Type>();
            foreach (object o in e)
            {
                if (o == null)
                    continue;

                types.Add(o.GetType());
            }

            return GetCommonBaseClass(types.ToArray());
        }

        public static Type GetCommonBaseClass(Type[] types)
        {
            if (types.Length == 0)
                return typeof(Object);

            Type ret = types[0];

            for (int i = 1; i < types.Length; ++i)
            {
                if (types[i].IsAssignableFrom(ret))
                    ret = types[i];
                else
                {
                    // This will always terminate when ret == typeof(object)
                    while (!ret.IsAssignableFrom(types[i]))
                        ret = ret.BaseType;
                }
            }

            return ret;
        }
    } // TypeUtil
} // RSG.Base.Windows.DragDrop.Utilities
