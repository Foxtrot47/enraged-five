﻿using System;
using System.Collections;
using System.Windows;
using System.Windows.Controls.Primitives;

namespace RSG.Base.Windows.Controls.MultiSelect
{
    /// <summary>
    /// A sync behaviour for a multiselector.
    /// </summary>
    public static class MultiSelectorBehaviours
    {
        #region Select Items Source

        public static readonly DependencyProperty SelectedItemsSourceProperty = DependencyProperty.RegisterAttached(
            "SelectedItemsSource",
            typeof(IList),
            typeof(MultiSelectorBehaviours),
            new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.AffectsRender, OnSelectedItemsSourceChanged)
            );

        public static void SetSelectedItemsSource(DependencyObject element, IList value)
        {
            element.SetValue(SelectedItemsSourceProperty, value);
        }
        public static IList GetSelectedItemsSource(DependencyObject element)
        {
            return (IList)element.GetValue(SelectedItemsSourceProperty);
        }

        /// <summary>
        /// Gets called whenever the synchronised selected items gets changed
        /// </summary>
        private static void OnSelectedItemsSourceChanged(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs e)
        {
            if (e.OldValue != null)
            {
                SynchronisationManager synchroniser = GetSynchronisationManager(dependencyObject);
                synchroniser.StopSynchronising();

                SetSynchronisationManager(dependencyObject, null);
            }

            IList list = e.NewValue as IList;

            // check that this property is an IList, and that it is being set on a ListBox
            if (list != null)
            {
                SynchronisationManager synchronizer = GetSynchronisationManager(dependencyObject);
                if (synchronizer == null)
                {
                    synchronizer = new SynchronisationManager(dependencyObject);
                    SetSynchronisationManager(dependencyObject, synchronizer);
                }

                synchronizer.StartSynchronisingList();
            }
        }

        #endregion // Select Items Source

        #region Synchronisation Manager

        /// <summary>
        /// Gets the synchronisation manager from the dependency property
        /// </summary>
        private static SynchronisationManager GetSynchronisationManager(DependencyObject dependencyObject)
        {
            return (SynchronisationManager)dependencyObject.GetValue(SynchronisationManagerProperty);
        }

        /// <summary>
        /// Sets the synchronisation manager from the dependency property
        /// </summary>
        private static void SetSynchronisationManager(DependencyObject dependencyObject, SynchronisationManager value)
        {
            dependencyObject.SetValue(SynchronisationManagerProperty, value);
        }

        private static readonly DependencyProperty SynchronisationManagerProperty = DependencyProperty.RegisterAttached(
            "SynchronisationManager", 
            typeof(SynchronisationManager), 
            typeof(MultiSelectorBehaviours), 
            new PropertyMetadata(null));


        #endregion // Synchronisation Manager


    } // MultiSelectorBehaviours


    /// <summary>
    /// A synchronization manager.
    /// </summary>
    class SynchronisationManager
    {
        #region Properties

        private readonly DependencyObject MultiSelector;
        private ListSynchroniser Synchroniser;

        #endregion // Properties

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="SynchronisationManager"/> class.
        /// </summary>
        internal SynchronisationManager(DependencyObject selector)
        {
            MultiSelector = selector;
        }

        #endregion // Constructors

        #region Public Functions

        /// <summary>
        /// Starts synchronising the list.
        /// </summary>
        public void StartSynchronisingList()
        {
            IList list = MultiSelectorBehaviours.GetSelectedItemsSource(MultiSelector);

            if (list != null)
            {
                Synchroniser = new ListSynchroniser(list, GetSelectedItemsCollection(MultiSelector));
                Synchroniser.StartSynchronising();
            }
        }

        /// <summary>
        /// Stops synchronizing the list.
        /// </summary>
        public void StopSynchronising()
        {
            if (Synchroniser != null)
            {
                Synchroniser.StopSynchronising();
            }
        }

        public static IList GetSelectedItemsCollection(DependencyObject selector)
        {
            if (selector is MultiSelector)
            {
                return (selector as MultiSelector).SelectedItems;
            }
            else if (selector is RSG.Base.Windows.Controls.MultiSelect.TreeView)
            {
                return (selector as RSG.Base.Windows.Controls.MultiSelect.TreeView).SelectedItems;
            }
            else if (selector is System.Windows.Controls.ListBox)
            {
                return (selector as System.Windows.Controls.ListBox).SelectedItems;
            }
            else
            {
                throw new InvalidOperationException("Target object has no SelectedItems property to bind.");
            }
        }

        #endregion Public Functions

    } // SynchronisationManager

} // RSG.Base.Windows.Controls.MultiSelect
