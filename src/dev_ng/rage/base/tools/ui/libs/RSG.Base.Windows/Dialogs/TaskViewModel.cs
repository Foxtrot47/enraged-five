﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Editor;
using System.Threading;
using RSG.Base.Tasks;

namespace RSG.Base.Windows.Dialogs
{
    /// <summary>
    /// 
    /// </summary>
    public class TaskViewModel : ViewModelBase
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public ITask Task
        {
            get;
            private set;
        }

        /// <summary>
        /// Name of the task.
        /// </summary>
        public String Name
        {
            get { return m_name; }
            set
            {
                SetPropertyValue(value, m_name, () => this.Name,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_name = (String)newValue;
                        }
                    ));
            }
        }
        private string m_name;

        /// <summary>
        /// How far through processing the task it is.
        /// (Between 0 and 1).
        /// </summary>
        public double Progress
        {
            get { return m_progress; }
            set
            {
                SetPropertyValue(value, m_progress, () => this.Progress,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_progress = (double)newValue;
                        }
                    ));
            }
        }
        private double m_progress;

        /// <summary>
        /// 
        /// </summary>
        public TaskStatus Status
        {
            get { return m_status; }
            set
            {
                SetPropertyValue(value, m_status, () => this.Status,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_status = (TaskStatus)newValue;
                        }
                    ));
            }
        }
        private TaskStatus m_status;

        /// <summary>
        /// 
        /// </summary>
        public bool IsIndeterminate
        {
            get { return m_isIndeterminate; }
            set
            {
                SetPropertyValue(value, m_isIndeterminate, () => this.IsIndeterminate,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_isIndeterminate = (bool)newValue;
                        }
                    ));
            }
        }
        private bool m_isIndeterminate;

        /// <summary>
        /// What the task is currently doing.
        /// </summary>
        public string CurrentAction
        {
            get { return m_currentAction; }
            set
            {
                SetPropertyValue(value, m_currentAction, () => this.CurrentAction,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_currentAction = (string)newValue;
                        }
                    ));
            }
        }
        private string m_currentAction;
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="task"></param>
        public TaskViewModel(ITask task)
        {
            Task = task;
            Name = task.Name;
            Progress = 0.0;
            CurrentAction = null;

            // Hook up the event handler for progress notifications
            if (Task.Progress is EventProgress<TaskProgress>)
            {
                ((EventProgress<TaskProgress>)Task.Progress).ProgressChanged += new EventHandler<TaskProgress>(TaskProgressChanged);
            }
        }
        #endregion // Constructor(s)

        #region Public Methods
        /// <summary>
        /// 
        /// </summary>
        public void ExecuteTask(ITaskContext context)
        {
            Task.Reset();
            Task.ExecuteAsync(context);

            IsIndeterminate = !Task.ReportsProgress;
        }
        #endregion // Public Methods

        #region Event Handlers
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void TaskProgressChanged(object sender, TaskProgress e)
        {
            if (e.Relative)
            {
                Progress += e.Progress;
            }
            else
            {
                Progress = e.Progress;
            }
            CurrentAction = e.Message;
            Status = Task.Status;

            if (e.Progress >= 1.0)
            {
                IsIndeterminate = false;
            }
        }
        #endregion // Event Handlers
    } // ProgressTaskViewModel
}
