﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace RSG.Base.Windows.Helpers
{
    /// <summary>
    /// http://www.codeproject.com/Articles/25964/A-more-generic-way-of-sorting-a-WPF-ListView-with
    /// </summary>
    public interface IListViewCustomComparer : IComparer
    {
        /// <summary>
        /// Gets or sets the sort by column name.
        /// </summary>
        /// <value>The sort by.</value>
        String SortBy { get; set; }

        /// <summary>
        /// Gets or sets the sort direction.
        /// </summary>
        /// <value>The sort direction.</value>
        ListSortDirection SortDirection { get; set; }
    }
}
