﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Data;

namespace RSG.Base.Windows.Controls
{
    /// <summary>
    /// Represents a Windows spin box (also known as an up-down control) that displays numeric values.
    /// </summary>
    /// References:
    ///   http://bot.codeplex.com/
    ///   
    [Description("Represents a up-down control (also known as a spin box) that displays numeric values.")]
    public class NumericUpDown : Control
    {
        #region Constants
        private const Decimal DefaultMinValue = 0;
        private const Decimal DefaultMaxValue = 100;
        private const Decimal DefaultValue = DefaultMinValue;
        private const Decimal DefaultIncrement = 1;
        private const int DefaultDecimalPlaces = 0;
        #endregion // Constants

        #region Events
        /// <summary>
        /// Identifies the ValueChanged routed event.
        /// </summary>
        public static readonly RoutedEvent ValueChangedEvent = EventManager.RegisterRoutedEvent(
            "ValueChanged", RoutingStrategy.Bubble,
            typeof(RoutedPropertyChangedEventHandler<decimal>), typeof(NumericUpDown));

        /// <summary>
        /// Occurs when the Value property changes.
        /// </summary>
        public event RoutedPropertyChangedEventHandler<decimal> ValueChanged
        {
            add { AddHandler(ValueChangedEvent, value); }
            remove { RemoveHandler(ValueChangedEvent, value); }
        }
        #endregion // Events

        #region Properties
        #region Property : Minimum
        /// <summary>
        /// Gets or sets the minimum allowed value for the up-down control.
        /// </summary>
        public Decimal Minimum 
        {
            get { return (Decimal)GetValue(MinimumProperty); }
            set { SetValue(MinimumProperty, value); }
        }
        
        public static readonly DependencyProperty MinimumProperty =
            DependencyProperty.Register("Minimum", typeof(Decimal), typeof(NumericUpDown),
                new FrameworkPropertyMetadata(DefaultMinValue, 
                    new PropertyChangedCallback(OnMinimumChanged), 
                    new CoerceValueCallback(CoerceMinimum)));

        private static void OnMinimumChanged(DependencyObject element, DependencyPropertyChangedEventArgs e)
        {
            element.CoerceValue(MaximumProperty);
            element.CoerceValue(ValueProperty);
        }

        private static Object CoerceMinimum(DependencyObject element, Object value)
        {
            Decimal minimum = (Decimal)value;
            NumericUpDown control = (NumericUpDown)element;
            return (Decimal.Round(minimum, control.DecimalPlaces));
        }
        #endregion // Property : Minimum

        #region Property : Maximum
        /// <summary>
        /// Gets or sets the maximum allowed value for the up-down control.
        /// </summary>
        public Decimal Maximum 
        {
            get { return (Decimal)GetValue(MaximumProperty); }
            set { SetValue(MaximumProperty, value); }
        }

        public static readonly DependencyProperty MaximumProperty =
            DependencyProperty.Register("Maximum", typeof(Decimal), typeof(NumericUpDown),
                new FrameworkPropertyMetadata(DefaultMaxValue,
                    new PropertyChangedCallback(OnMaximumChanged),
                    new CoerceValueCallback(CoerceMaximum)));

        private static void OnMaximumChanged(DependencyObject element, DependencyPropertyChangedEventArgs e)
        {
            element.CoerceValue(ValueProperty);
        }

        private static Object CoerceMaximum(DependencyObject element, Object value)
        {
            NumericUpDown control = (NumericUpDown)element;
            Decimal newMaximum = (Decimal)value;
            return Decimal.Round(System.Math.Max(newMaximum, control.Minimum), control.DecimalPlaces);
        }
        #endregion // Property : Maximum

        #region Property : Increment
        /// <summary>
        /// Gets or sets the value to increment or decrement the up-down control value.
        /// </summary>
        public Decimal Increment
        {
            get { return (Decimal)GetValue(ChangeProperty); }
            set { SetValue(ChangeProperty, value); }
        }

        public static readonly DependencyProperty ChangeProperty =
            DependencyProperty.Register(
                "Increment", typeof(Decimal), typeof(NumericUpDown),
                new FrameworkPropertyMetadata(DefaultIncrement, new PropertyChangedCallback(OnChangeChanged), new CoerceValueCallback(CoerceChange)),
            new ValidateValueCallback(ValidateChange)
            );

        private static bool ValidateChange(object value)
        {
            Decimal change = (Decimal)value;
            return change > 0;
        }

        private static void OnChangeChanged(DependencyObject element, DependencyPropertyChangedEventArgs args)
        {
        }

        private static object CoerceChange(DependencyObject element, object value)
        {
            Decimal newChange = (Decimal)value;
            NumericUpDown control = (NumericUpDown)element;

            Decimal coercedNewChange = Decimal.Round(newChange, control.DecimalPlaces);

            //If Increment is .1 and DecimalPlaces is changed from 1 to 0, we want Increment to go to 1, not 0.
            //Put another way, Increment should always be rounded to DecimalPlaces, but never smaller than the 
            //previous Increment
            if (coercedNewChange < newChange)
            {
                coercedNewChange = smallestForDecimalPlaces(control.DecimalPlaces);
            }

            return coercedNewChange;
        }

        private static Decimal smallestForDecimalPlaces(int decimalPlaces)
        {
            Debug.Assert(decimalPlaces >= 0, "Invalid number of decimal places.");
            Decimal d = 1;
            for (int i = 0; i < decimalPlaces; i++)
            {
                d /= 10;
            }

            return d;
        }

        #endregion

        #region Property : DecimalPlaces
        /// <summary>
        /// Gets or sets the number of Decimal places to display in the up-down control.
        /// </summary>
        public int DecimalPlaces
        {
            get { return (int)GetValue(DecimalPlacesProperty); }
            set { SetValue(DecimalPlacesProperty, value); }
        }

        public static readonly DependencyProperty DecimalPlacesProperty =
            DependencyProperty.Register(
                "DecimalPlaces", typeof(int), typeof(NumericUpDown),
                new FrameworkPropertyMetadata(DefaultDecimalPlaces,
                    new PropertyChangedCallback(OnDecimalPlacesChanged)
                ), new ValidateValueCallback(ValidateDecimalPlaces)
            );

        private static void OnDecimalPlacesChanged(DependencyObject element, DependencyPropertyChangedEventArgs args)
        {
            NumericUpDown control = (NumericUpDown)element;
            control.CoerceValue(ChangeProperty);
            control.CoerceValue(MinimumProperty);
            control.CoerceValue(MaximumProperty);
            control.CoerceValue(ValueProperty);
            control.updateValueString();
        }

        private static bool ValidateDecimalPlaces(object value)
        {
            int decimalPlaces = (int)value;
            return decimalPlaces >= 0;
        }

        #endregion

        #region Property : Value
        /// <summary>
        /// Gets or sets the current value for the up-down control.
        /// </summary>
        /// <exception cref="ArgumentOutOfRange">
        ///   Raises ArgumentOutOfRange exception.
        /// </exception>
        public Decimal Value
        {
            get { return (Decimal)GetValue(ValueProperty); }
            set { SetValue(ValueProperty, value); }
        }

        /// <summary>
        /// Identifies the Value dependency property.
        /// </summary>
        public static readonly DependencyProperty ValueProperty =
            DependencyProperty.Register(
                "Value", typeof(Decimal), typeof(NumericUpDown),
                new FrameworkPropertyMetadata(DefaultValue,
                    new PropertyChangedCallback(OnValueChanged),
                    new CoerceValueCallback(CoerceValue)
                )
            );

        private static void OnValueChanged(DependencyObject obj, DependencyPropertyChangedEventArgs args)
        {
            NumericUpDown control = (NumericUpDown)obj;

            Decimal oldValue = (Decimal)args.OldValue;
            Decimal newValue = (Decimal)args.NewValue;
            
            RoutedPropertyChangedEventArgs<Decimal> e = new RoutedPropertyChangedEventArgs<Decimal>(
                oldValue, newValue, ValueChangedEvent);

            control.OnValueChanged(e);

            control.updateValueString();
        }

        /// <summary>
        /// Raises the ValueChanged event.
        /// </summary>
        /// <param name="args">Arguments associated with the ValueChanged event.</param>
        protected virtual void OnValueChanged(RoutedPropertyChangedEventArgs<Decimal> args)
        {
            RaiseEvent(args);
        }

        private static object CoerceValue(DependencyObject element, object value)
        {
            Decimal newValue = (Decimal)value;
            NumericUpDown control = (NumericUpDown)element;

            newValue = System.Math.Max(control.Minimum, System.Math.Min(control.Maximum, newValue));
            newValue = Decimal.Round(newValue, control.DecimalPlaces);

            return newValue;
        }
        #endregion // Property : Value

        #region Property : ValueString
        /// <summary>
        /// 
        /// </summary>
        public String ValueString
        {
            get
            {
                return (String)GetValue(ValueStringProperty);
            }
        }

        private static readonly DependencyPropertyKey ValueStringPropertyKey =
            DependencyProperty.RegisterAttachedReadOnly("ValueString", typeof(String), typeof(NumericUpDown), 
                new FrameworkPropertyMetadata() { BindsTwoWayByDefault = true, DefaultUpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged });

        public static readonly DependencyProperty ValueStringProperty = ValueStringPropertyKey.DependencyProperty;

        private void updateValueString()
        {
            _numberFormatInfo.NumberDecimalDigits = this.DecimalPlaces;
            string newValueString = this.Value.ToString("f", _numberFormatInfo);
            this.SetValue(ValueStringPropertyKey, newValueString);
        }

        private NumberFormatInfo _numberFormatInfo = new NumberFormatInfo();
        #endregion // Property : ValueString
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Static constructor.
        /// </summary>
        static NumericUpDown()
        {
            InitializeCommands();
            DefaultStyleKeyProperty.OverrideMetadata(typeof(NumericUpDown), 
                new FrameworkPropertyMetadata(typeof(NumericUpDown)));
        }

        /// <summary>
        /// Default constructor.
        /// </summary>
        public NumericUpDown()
            : base()
        {
        }
        #endregion // Constructor(s)

        /// <summary>
        /// This is a class handler for MouseLeftButtonDown event.
        /// The purpose of this handle is to move input focus to NumericUpDown when user pressed
        /// mouse left button on any part of slider that is not focusable.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private static void OnMouseLeftButtonDown(Object sender, MouseButtonEventArgs e)
        {
            NumericUpDown control = (NumericUpDown)sender;

            // When someone click on a part in the NumericUpDown and it's not focusable
            // NumericUpDown needs to take the focus in order to process keyboard correctly
            if (!control.IsKeyboardFocusWithin)
            {
                e.Handled = control.Focus() || e.Handled;
            }
        }

        #region Commands

        public static RoutedCommand IncreaseCommand
        {
            get
            {
                return _increaseCommand;
            }
        }
        public static RoutedCommand DecreaseCommand
        {
            get
            {
                return _decreaseCommand;
            }
        }

        private static void InitializeCommands()
        {
            _increaseCommand = new RoutedCommand("IncreaseCommand", typeof(NumericUpDown));
            CommandManager.RegisterClassCommandBinding(typeof(NumericUpDown), new CommandBinding(_increaseCommand, OnIncreaseCommand));
            CommandManager.RegisterClassInputBinding(typeof(NumericUpDown), new InputBinding(_increaseCommand, new KeyGesture(Key.Up)));

            _decreaseCommand = new RoutedCommand("DecreaseCommand", typeof(NumericUpDown));
            CommandManager.RegisterClassCommandBinding(typeof(NumericUpDown), new CommandBinding(_decreaseCommand, OnDecreaseCommand));
            CommandManager.RegisterClassInputBinding(typeof(NumericUpDown), new InputBinding(_decreaseCommand, new KeyGesture(Key.Down)));
        }

        private static void OnIncreaseCommand(object sender, ExecutedRoutedEventArgs e)
        {
            NumericUpDown control = sender as NumericUpDown;
            if (control.Value == control.Maximum)
                return;

            if (control != null)
            {
                control.OnIncrease();
            }
        }
        private static void OnDecreaseCommand(object sender, ExecutedRoutedEventArgs e)
        {
            NumericUpDown control = sender as NumericUpDown;
            if (control.Value == control.Minimum)
                return;

            if (control != null)
            {
                control.OnDecrease();
            }
        }

        protected virtual void OnIncrease()
        {
            this.Value += Increment;
        }
        protected virtual void OnDecrease()
        {
            this.Value -= Increment;
        }

        private static RoutedCommand _increaseCommand;
        private static RoutedCommand _decreaseCommand;
        #endregion
    }

} // RSG.Base.Windows.Controls namespace
