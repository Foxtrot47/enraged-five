﻿using System;
using System.Windows.Data;
using System.Windows.Media;
using RSG.Base.Math;

namespace RSG.Base.Windows.Converters
{
    public class ColourToVectorConverter : IValueConverter
    {
        #region IValueConverter Members

        /// <summary>
        /// Converts a SolidColorBrush to a Color.
        /// </summary>
        public Object Convert(Object value, Type targetType, Object parameter, System.Globalization.CultureInfo culture)
        {
            if (value is Vector3f)
            {
                Vector3f vector = (Vector3f)value;
                byte r = System.Math.Max((byte)0, System.Math.Min((byte)255, (byte)(vector.X * 255)));
                byte g = System.Math.Max((byte)0, System.Math.Min((byte)255, (byte)(vector.Y * 255)));
                byte b = System.Math.Max((byte)0, System.Math.Min((byte)255, (byte)(vector.Z * 255)));
                return Color.FromArgb(255, r, g, b);
            }

            return value;
        }

        /// <summary>
        /// Converts a Color to a Vector.
        /// </summary>
        public Object ConvertBack(Object value, Type targetType, Object parameter, System.Globalization.CultureInfo culture)
        {
            if (value is Color)
            {
                Color c = (Color)value;
                float inverseScalar = 1.0f / 255.0f;
                Vector3f vector = new Vector3f(c.R * inverseScalar, c.G * inverseScalar, c.B * inverseScalar);
                return vector;
            }

            return value;
        }

        #endregion
    }
}
