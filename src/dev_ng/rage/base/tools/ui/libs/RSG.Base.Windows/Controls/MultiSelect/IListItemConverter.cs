﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Base.Windows.Controls.MultiSelect
{
    /// <summary>
    /// Converts items in the Source list to items in the Target list, and back again.
    /// </summary>
    public interface IListItemConverter
    {
        /// <summary>
        /// Converts the specified master list item.
        /// </summary>
        Object Convert(Object sourceListItem);

        /// <summary>
        /// Converts the specified target list item.
        /// </summary>
        Object ConvertBack(Object targetListItem);
    }
}
