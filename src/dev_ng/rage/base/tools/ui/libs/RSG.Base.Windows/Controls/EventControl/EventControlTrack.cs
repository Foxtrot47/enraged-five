﻿using System.Windows.Controls;
using System.Windows;
using System.Windows.Threading;
using System.ComponentModel;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Automation.Peers;
using System.Windows.Media;
using System.Collections;
using System;
using System.Windows.Data;
using System.Diagnostics;
using System.Collections.Generic;

namespace RSG.Base.Windows.Controls
{
    /// <summary>
    /// </summary>
    public class EventControlTrack : ItemsControl
    {
        #region Fields
        /// <summary>
        /// Identifies the <see cref="CurrentUnit"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty CurrentUnitProperty;

        /// <summary>
        /// Identifies the <see cref="ShowUnitTracker"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty ShowUnitTrackerProperty;

        /// <summary>
        /// Identifies the <see cref="PixelsPerUnit"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty PixelsPerUnitProperty;

        /// <summary>
        /// Identies the <see cref="SnapMovementToUnit"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty SnapMovementToUnitProperty;

        /// <summary>
        /// Identies the <see cref="SnapResizeToUnit"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty SnapResizeToUnitProperty;

        /// <summary>
        /// Identifies the <see cref="Content"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty ContentProperty;

        /// <summary>
        /// Identifies the <see cref="ContentTemplate"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty ContentTemplateProperty;

        /// <summary>
        /// Identies the <see cref="TrackUnitStartOffset"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty TrackUnitStartOffsetProperty;

        ///// <summary>
        ///// Identies the <see cref="MaximumUnitValue"/> dependency property.
        ///// </summary>
        //public static readonly DependencyProperty MaximumUnitValueProperty;
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises static members of the <see cref="EventControlTrack"/> class.
        /// </summary>
        static EventControlTrack()
        {
            ContentTemplateProperty =
                DependencyProperty.Register(
                "ContentTemplate",
                typeof(DataTemplate),
                typeof(EventControlTrack),
                new FrameworkPropertyMetadata(
                    null));

            ContentProperty =
                DependencyProperty.Register(
                "Content",
                typeof(object),
                typeof(EventControlTrack),
                new FrameworkPropertyMetadata(
                    null,
                    new PropertyChangedCallback(EventControlTrack.OnContentChanged)));

            CurrentUnitProperty =
                DependencyProperty.Register(
                "CurrentUnit",
                typeof(double),
                typeof(EventControlTrack),
                new FrameworkPropertyMetadata(
                    0.0));

            ShowUnitTrackerProperty =
                DependencyProperty.Register(
                "ShowUnitTracker",
                typeof(bool),
                typeof(EventControlTrack),
                new FrameworkPropertyMetadata(
                    false));

            PixelsPerUnitProperty =
                EventControl.PixelsPerUnitProperty.AddOwner(
                typeof(EventControlTrack),
                new FrameworkPropertyMetadata(
                    1.0,
                    FrameworkPropertyMetadataOptions.Inherits));

            SnapMovementToUnitProperty =
                EventControl.SnapMovementToUnitProperty.AddOwner(
                typeof(EventControlTrack),
                new FrameworkPropertyMetadata(
                    false,
                    FrameworkPropertyMetadataOptions.Inherits));

            SnapResizeToUnitProperty =
                EventControl.SnapResizeToUnitProperty.AddOwner(
                typeof(EventControlTrack),
                new FrameworkPropertyMetadata(
                    false,
                    FrameworkPropertyMetadataOptions.Inherits));

            TrackUnitStartOffsetProperty =
                EventControl.TrackUnitStartOffsetProperty.AddOwner(
                typeof(EventControlTrack),
                new FrameworkPropertyMetadata(
                    0.0,
                    FrameworkPropertyMetadataOptions.Inherits));

            DefaultStyleKeyProperty.OverrideMetadata(
                typeof(EventControlTrack),
                new FrameworkPropertyMetadata(typeof(EventControlTrack)));
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="EventControlTrack"/> class.
        /// </summary>
        public EventControlTrack()
        {
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the content of a <see cref="EventControlTrack" />.
        /// </summary>
        public object Content
        {
            get
            {
                return base.GetValue(EventControlTrack.ContentProperty);
            }
            set
            {
                base.SetValue(EventControlTrack.ContentProperty, value);
            }
        }

        /// <summary>
        /// Gets or sets the data template used to display the content of the <see cref="EventControlTrack" />.
        /// </summary>
        public DataTemplate ContentTemplate
        {
            get
            {
                return (DataTemplate)base.GetValue(EventControlTrack.ContentTemplateProperty);
            }
            set
            {
                base.SetValue(ContentControl.ContentTemplateProperty, value);
            }
        }

        /// <summary>
        /// Gets or sets the unit position that is currently being marked by the track marker.
        /// </summary>
        public double CurrentUnit
        {
            get { return (double)this.GetValue(CurrentUnitProperty); }
            set { this.SetValue(CurrentUnitProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the unit tracker is shown at the current
        /// unit poisition.
        /// </summary>
        public bool ShowUnitTracker
        {
            get { return (bool)this.GetValue(ShowUnitTrackerProperty); }
            set { this.SetValue(ShowUnitTrackerProperty, value); }
        }

        /// <summary>
        /// Gets or sets the number of pixels that will be render for each unit.
        /// </summary>
        public double PixelsPerUnit
        {
            get { return (double)this.GetValue(PixelsPerUnitProperty); }
            set { this.SetValue(PixelsPerUnitProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the movement of events are limited
        /// to unit boundaries.
        /// </summary>
        public bool SnapMovementToUnit
        {
            get { return (bool)this.GetValue(SnapMovementToUnitProperty); }
            set { this.SetValue(SnapMovementToUnitProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the resizing of events are limited
        /// to unit boundaries.
        /// </summary>
        public bool SnapResizeToUnit
        {
            get { return (bool)this.GetValue(SnapResizeToUnitProperty); }
            set { this.SetValue(SnapResizeToUnitProperty, value); }
        }

        /// <summary>
        /// Gets or sets the first visible unit on the event tracks.
        /// </summary>
        public double TrackUnitStartOffset
        {
            get { return (double)this.GetValue(TrackUnitStartOffsetProperty); }
            set { this.SetValue(TrackUnitStartOffsetProperty, value); }
        }

        ///// <summary>
        ///// Gets or sets the maximum unit value the control can display, this effects
        ///// the manipulation of individual events.
        ///// </summary>
        //public double MaximumUnitValue
        //{
        //    get { return (double)this.GetValue(MaximumUnitValueProperty); }
        //    set { this.SetValue(MaximumUnitValueProperty, value); }
        //}
        #endregion

        #region Methods
        protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
        {
            base.OnMouseLeftButtonDown(e);
            foreach (EventTrackItem selected in this.GetSelectedItems())
            {
                selected.IsSelected = false;
            }
        }

        /// <summary>
        /// Creates or identifies the element used to display a specified item.
        /// </summary>
        /// <returns>
        /// A new instance of the type <see cref="EventControlTrack"/>.
        /// </returns>
        protected override DependencyObject GetContainerForItemOverride()
        {
            return new EventTrackItem();
        }

        /// <summary>
        /// Determines if the specified item is (or is eligible to be) its own container.
        /// </summary>
        /// <param name="item">
        /// The item to check.
        /// </param>
        /// <returns>
        /// True if the item is (or is eligible to be) its own container; otherwise, false.
        /// </returns>
        protected override bool IsItemItsOwnContainerOverride(object item)
        {
            return item is EventTrackItem;
        }

        private static void OnContentChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            EventControlTrack control = (EventControlTrack)d;
            control.OnContentChanged(e.OldValue, e.NewValue);
        }

        /// <summary>
        /// Called when the <see cref="Content" /> property changes.
        /// </summary>
        /// <param name="oldContent">
        /// The old value of the <see cref="Content" /> property.
        /// </param>
        /// <param name="newContent">
        /// The new value of the <see cref="Content" /> property.
        /// </param>
        protected virtual void OnContentChanged(object oldContent, object newContent)
        {
        }

        private List<EventTrackItem> GetSelectedItems()
        {
            List<EventTrackItem> result = new List<EventTrackItem>();
            DependencyObject parent = VisualTreeHelper.GetParent(this);
            EventControl control = parent as EventControl;
            while (parent != null && control == null)
            {
                parent = VisualTreeHelper.GetParent(parent);
                control = parent as EventControl;
            }

            if (control != null)
            {
                for (int i = 0; i < control.Items.Count; i++)
                {
                    var container = control.ItemContainerGenerator.ContainerFromIndex(i);
                    EventControlTrack track = container as EventControlTrack;
                    if (track == null)
                    {
                        continue;
                    }

                    for (int j = 0; j < track.Items.Count; j++)
                    {
                        container = track.ItemContainerGenerator.ContainerFromIndex(j);
                        EventTrackItem item = container as EventTrackItem;
                        if (item != null && item.IsSelected)
                        {
                            result.Add(item);
                        }
                    }
                }
            }

            return result;
        }
        #endregion
    }
}
