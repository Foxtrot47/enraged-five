﻿using System;
using System.ComponentModel;
using RSG.Base.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using RSG.Base.Windows.Controls.WpfViewport2D.Geometry;
using RSG.Base.Windows.Controls.WpfViewport2D.Actions;
using RSG.Base.Math;

namespace RSG.Base.Windows.Controls.WpfViewport2D.Geometry
{
    public class Viewport2DShape : Viewport2DMultiLine
    {
        #region Properties
        /// <summary>
        /// Fill colour
        /// </summary>
        public Color FillColour
        {
            get { return m_fillColour; }
            set 
            {
                if (m_fillColour == value)
                    return;

                m_fillColour = value;
                OnPropertyChanged("FillColour");
            }
        }
        private Color m_fillColour;

        /// <summary>
        /// Fill flag, fills the shape in the given fill brush
        /// </summary>
        public Boolean Fill
        {
            get { return m_fill; }
            set { m_fill = value; }
        }
        private Boolean m_fill;

        /// <summary>
        /// Enumerator for the x coordinates
        /// </summary>
        private IEnumerable<float> XCoordinates
        {
            get
            {
                foreach (Vector2f point in Points)
                {
                    yield return point.X;
                }
            }
        }

        /// <summary>
        /// Enumerator for the y coordinates
        /// </summary>
        private IEnumerable<float> YCoordinates
        {
            get
            {
                foreach (Vector2f point in Points)
                {
                    yield return point.Y;
                }
            }
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="space"></param>
        /// <param name="image"></param>
        /// <param name="pos"></param>
        /// <param name="size"></param>
        public Viewport2DShape(etCoordSpace space, String name, Vector2f[] points, Color outlineColor, int outlineWidth, Color fillColour)
            : base(space, name, points, true, outlineColor, outlineWidth)
        {
            this.Fill = true;
            this.FillColour = fillColour;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="space"></param>
        /// <param name="image"></param>
        /// <param name="pos"></param>
        /// <param name="size"></param>
        public Viewport2DShape(etCoordSpace space, String name, Vector2f[] points, Color outlineColor, int outlineWidth)
            : base(space, name, points, true, outlineColor, outlineWidth)
        {
            this.Fill = false;
        }
        #endregion // Constructor(s)

        #region Overrides
        /// <summary>
        /// Determines if the geometry has been picked given the screen
        /// position of the pick
        /// </summary>
        /// <param name="screenPosition"></param>
        /// <returns></returns>
        public override Boolean HasBeenPicked(Point viewPosition, Viewport2D viewport)
        {
            List<float> xCoordinates = new List<float>(this.XCoordinates);
            List<float> yCoordinates = new List<float>(this.YCoordinates);
            Point worldPosition = viewport.ViewToWorld(viewPosition);
            double x = worldPosition.X;
            double y = worldPosition.Y;

            Boolean oddNodes = false;
            for (int i = 0, j = this.Points.Length - 1; i < this.Points.Length; i++)
            {
                if (yCoordinates[i] < y && yCoordinates[j] >= y || yCoordinates[j] < y && yCoordinates[i] >= y)
                {
                    if (xCoordinates[i] + (y - yCoordinates[i]) / (yCoordinates[j] - yCoordinates[i]) * (xCoordinates[j] - xCoordinates[i]) < x)
                    {
                        oddNodes =! oddNodes;
                    }
                }
                j = i;
            }

            return oddNodes;
        }
        #endregion // Overrides
    } // Viewport2DShape
} // RSG.Base.Windows.Controls.WpfViewport2D.Geometry

