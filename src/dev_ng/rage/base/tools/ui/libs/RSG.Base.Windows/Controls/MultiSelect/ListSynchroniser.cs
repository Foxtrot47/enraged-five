﻿using System;
using System.Collections;
using System.Collections.Specialized;
using System.Linq;
using System.Windows;

namespace RSG.Base.Windows.Controls.MultiSelect
{
    /// <summary>
    /// Keeps two lists synchronised with each other
    /// </summary>
    class ListSynchroniser : IWeakEventListener
    {
        #region Properties

        private static readonly IListItemConverter DefaultConverter = new DoNothingListItemConverter();

        private readonly IList SourceList;
        private readonly IListItemConverter SourceTargetConverter;
        private readonly IList TargetList;

        private delegate void ChangeListAction(IList list, NotifyCollectionChangedEventArgs e, Converter<Object, Object> converter);

        #endregion // Properties

        #region Constructors

        public ListSynchroniser(IList sourceList, IList targetList)
        {
            this.SourceList = sourceList;
            this.TargetList = targetList;
            SourceTargetConverter = DefaultConverter;
        }

        #endregion Constructors

        #region Public Functions

        /// <summary>
        /// Starts synchronizing the lists.
        /// </summary>
        public void StartSynchronising()
        {
            ListenForChangeEvents(SourceList);
            ListenForChangeEvents(TargetList);

            // Update the target dependency object list from the Source list
            SetListValuesFromSource(SourceList, TargetList, ConvertFromSourceToTarget);

            // In some cases the target list might have its own view on which items should included:
            // so update the target list from the source list
            // (This is the case with a ListBox SelectedItems collection: only items from the ItemsSource can be included in SelectedItems)
            if (!TargetAndSourceCollectionsAreEqual())
            {
                SetListValuesFromSource(TargetList, SourceList, ConvertFromTargetToSource);
            }
        }

        /// <summary>
        /// Stop synchronizing the lists.
        /// </summary>
        public void StopSynchronising()
        {
            StopListeningForChangeEvents(SourceList);
            StopListeningForChangeEvents(TargetList);
        }

        #endregion // Public Functions

        #region Protected Functions

        /// <summary>
        /// Listens for change events on a list.
        /// </summary>
        protected void ListenForChangeEvents(IList list)
        {
            if (list is INotifyCollectionChanged)
            {
                CollectionChangedEventManager.AddListener(list as INotifyCollectionChanged, this);
            }
        }

        /// <summary>
        /// Stops listening for change events.
        /// </summary>
        protected void StopListeningForChangeEvents(IList list)
        {
            if (list is INotifyCollectionChanged)
            {
                CollectionChangedEventManager.RemoveListener(list as INotifyCollectionChanged, this);
            }
        }

        #endregion // Protected Functions

        #region Private Functions

        /// <summary>
        /// Adds the new items in the change event arguments to the given list using the converter, it
        /// also uses the other parameters in the change event structure to determine where to insert the
        /// new items
        /// </summary>
        private void AddItems(IList list, NotifyCollectionChangedEventArgs e, Converter<Object, Object> converter)
        {
            if (e.NewItems != null)
            {
                int count = e.NewItems.Count;

                for (int i = 0; i < count; i++)
                {
                    int insertionPoint = e.NewStartingIndex + i;
                    if (insertionPoint > list.Count)
                    {
                        list.Add(converter(e.NewItems[i]));
                    }
                    else
                    {
                        list.Insert(insertionPoint, converter(e.NewItems[i]));
                    }
                }
            }
        }

        /// <summary>
        /// Removes the oldItems in the event arguments from the given list
        /// </summary>
        private void RemoveItems(IList list, NotifyCollectionChangedEventArgs e, Converter<Object, Object> converter)
        {
            if (e.OldItems != null)
            {
                int count = e.OldItems.Count;

                // for the number of items being removed, remove the item from the Old Starting Index
                // (this will cause following items to be shifted down to fill the hole).
                for (int i = 0; i < count; i++)
                {
                    list.RemoveAt(e.OldStartingIndex);
                }
            }
        }

        /// <summary>
        /// Moves items in the given list using the change event arguments
        /// </summary>
        private void MoveItems(IList list, NotifyCollectionChangedEventArgs e, Converter<Object, Object> converter)
        {
            RemoveItems(list, e, converter);
            AddItems(list, e, converter);
        }

        /// <summary>
        /// Replaces items in the given list using the change event arguments
        /// </summary>
        private void ReplaceItems(IList list, NotifyCollectionChangedEventArgs e, Converter<Object, Object> converter)
        {
            RemoveItems(list, e, converter);
            AddItems(list, e, converter);
        }

        /// <summary>
        /// Converters the items in the list using the converters "Convert" method if one exists.
        /// </summary>
        private Object ConvertFromSourceToTarget(Object sourceListItem)
        {
            return SourceTargetConverter == null ? sourceListItem : SourceTargetConverter.Convert(sourceListItem);
        }

        /// <summary>
        /// Converters the items in the list using the converters "ConvertBack" method if one exists.
        /// </summary>
        private Object ConvertFromTargetToSource(Object targetListItem)
        {
            return SourceTargetConverter == null ? targetListItem : SourceTargetConverter.ConvertBack(targetListItem);
        }

        /// <summary>
        /// Handles the collection changed event from a IList source
        /// </summary>
        private void HandleCollectionChanged(Object sender, NotifyCollectionChangedEventArgs e)
        {
            IList eventSender = sender as IList;

            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    this.PerformActionOnAllLists(AddItems, eventSender, e);
                    break;
                case NotifyCollectionChangedAction.Move:
                    this.PerformActionOnAllLists(MoveItems, eventSender, e);
                    break;
                case NotifyCollectionChangedAction.Remove:
                    this.PerformActionOnAllLists(RemoveItems, eventSender, e);
                    break;
                case NotifyCollectionChangedAction.Replace:
                    this.PerformActionOnAllLists(ReplaceItems, eventSender, e);
                    break;
                case NotifyCollectionChangedAction.Reset:
                    this.UpdateListsFromEventSender(sender as IList);
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// Performs the given action on the other list that sent the event (EventOnSource <=> PerformOnAction)
        /// </summary>
        private void PerformActionOnAllLists(ChangeListAction action, IList sourceList, NotifyCollectionChangedEventArgs collectionChangedArgs)
        {
            // Need to make sure we know which list (source/target) the event came from
            if (sourceList == SourceList)
            {
                PerformActionOnList(TargetList, action, collectionChangedArgs, ConvertFromSourceToTarget);
            }
            else
            {
                PerformActionOnList(SourceList, action, collectionChangedArgs, ConvertFromTargetToSource);
            }
        }

        /// <summary>
        /// Perform the given action on the given list, making sure that we have stopped listening to it before doing anything
        /// </summary>
        private void PerformActionOnList(IList list, ChangeListAction action, NotifyCollectionChangedEventArgs collectionChangedArgs, Converter<Object, Object> converter)
        {
            StopListeningForChangeEvents(list);
            action(list, collectionChangedArgs, converter);
            ListenForChangeEvents(list);
        }

        /// <summary>
        /// Makes sure that the given targetList is synchronised to the source list without sending events itself
        /// </summary>
        /// <param name="sourceList"></param>
        /// <param name="targetList"></param>
        /// <param name="converter"></param>
        private void SetListValuesFromSource(IList sourceList, IList targetList, Converter<Object, Object> converter)
        {
            if (sourceList.Count == 0 && targetList.Count == 0)
                return;

            this.StopListeningForChangeEvents(targetList);
            ArrayList Delegates = null;
            if (targetList is RSG.Base.Collections.IEventClearer)
            {
                Delegates = (targetList as RSG.Base.Collections.IEventClearer).RemoveAllHandlersOnCollectionChangedEvent();
            }

            targetList.Clear();

            foreach (Object o in sourceList)
            {
                targetList.Add(converter(o));
            }

            if (Delegates != null)
            {
                (targetList as RSG.Base.Collections.IEventClearer).AddHandlersOnCollectionChangedEvent(Delegates);
                (targetList as RSG.Base.Collections.IBeginEndCollection).EndUpdate();
            }
            this.ListenForChangeEvents(targetList);
        }

        /// <summary>
        /// Returns true if the source list and target list are the same
        /// </summary>
        /// <returns></returns>
        private Boolean TargetAndSourceCollectionsAreEqual()
        {
            return SourceList.Cast<Object>().SequenceEqual(TargetList.Cast<Object>().Select(item => ConvertFromTargetToSource(item)));
        }

        /// <summary>
        /// Makes sure that all synchronized lists have the same values as the source list.
        /// </summary>
        private void UpdateListsFromEventSender(IList eventSender)
        {
            if (eventSender == SourceList)
            {
                SetListValuesFromSource(SourceList, TargetList, ConvertFromSourceToTarget);
            }
            else
            {
                SetListValuesFromSource(TargetList, SourceList, ConvertFromTargetToSource);
            }
        }

        #endregion // Private Functions

        #region IWeakEventListener

        /// <summary>
        /// Receives events from the centralized event manager.
        /// </summary>
        /// <param name="managerType">The type of the <see cref="T:System.Windows.WeakEventManager"/> calling this method.</param>
        /// <param name="sender">Object that originated the event.</param>
        /// <param name="e">Event data.</param>
        /// <returns>
        /// true if the listener handled the event. It is considered an error by the <see cref="T:System.Windows.WeakEventManager"/> handling in WPF to register 
        /// a listener for an event that the listener does not handle. Regardless, the method should return false if it receives an event that it does not recognize or handle.
        /// </returns>
        public Boolean ReceiveWeakEvent(Type managerType, Object sender, EventArgs e)
        {
            if (sender is IList && e is NotifyCollectionChangedEventArgs)
            {
                this.HandleCollectionChanged(sender as IList, e as NotifyCollectionChangedEventArgs);
                return true;
            }

            return false;
        }

        #endregion // IWeakEventListener

        /// <summary>
        /// An implementation that does nothing in the conversions.
        /// </summary>
        internal class DoNothingListItemConverter : IListItemConverter
        {
            /// <summary>
            /// Converts the specified master list item.
            /// </summary>
            public object Convert(object sourceListItem)
            {
                return sourceListItem;
            }

            /// <summary>
            /// Converts the specified target list item.
            /// </summary>
            public object ConvertBack(object targetListItem)
            {
                return targetListItem;
            }
        }

    }
}
