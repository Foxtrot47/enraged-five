﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.ComponentModel;
using System.Collections;
using System.Windows.Media;
using System.Collections.Specialized;

namespace RSG.Base.Windows.Controls
{
    public class MultiSelectTreeViewItem : TreeViewItem
    {
        #region Properties

        #region IsSelected_

        /// <summary>
        /// Using a DependencyProperty as the backing store for IsSelected.  This enables animation, styling, binding, etc...
        /// </summary>
        public Boolean IsSelected_
        {
            get { return (Boolean)GetValue(IsSelectedProperty_); }
            set { SetValue(IsSelectedProperty_, value); }
        }
        public static readonly DependencyProperty IsSelectedProperty_ = DependencyProperty.RegisterAttached("IsSelected_", typeof(Boolean), typeof(MultiSelectTreeViewItem),
            new FrameworkPropertyMetadata(false) { PropertyChangedCallback = RealSelectedChanged });

        /// <summary>
        /// Called every time the Multi Select enabled property is changed, this just
        /// sets up the callback handlers for the key down and clicked events
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        private static void RealSelectedChanged(DependencyObject sender, DependencyPropertyChangedEventArgs args)
        {
            //if ((Boolean)args.OldValue == false && (Boolean)args.NewValue == true)
            //{
            //    MultiSelectTreeView tree = GetTree(sender as MultiSelectTreeViewItem);
            //    if (tree != null && tree.AnchorItem == null && (sender as MultiSelectTreeViewItem).HasHeader)
            //    {
            //        tree.AnchorItem = (sender as MultiSelectTreeViewItem).Header;
            //    }
            //    if ((sender as MultiSelectTreeViewItem).HasHeader)
            //    {
            //        if (!tree.SelectedItems.Contains((sender as MultiSelectTreeViewItem).Header))
            //        {
            //            tree.SelectedItems.Add((sender as MultiSelectTreeViewItem).Header);
            //        }
            //    }
            //}

            //if ((Boolean)args.OldValue == true && (Boolean)args.NewValue == false)
            //{
            //    MultiSelectTreeView tree = GetTree(sender as MultiSelectTreeViewItem);
            //    if (tree != null)
            //    {
            //        if (tree.SelectedItems.Contains((sender as MultiSelectTreeViewItem).Header))
            //        {
            //            tree.SelectedItems.Remove((sender as MultiSelectTreeViewItem).Header);
            //            if (tree.AnchorItem == (sender as MultiSelectTreeViewItem).Header)
            //            {
            //                if (tree.SelectedItems.Count >= 1)
            //                {
            //                    tree.AnchorItem = tree.SelectedItems[0];
            //                }
            //                else
            //                {
            //                    tree.AnchorItem = null;
            //                }
            //            }
            //        }
            //    }
            //}
        }

        #endregion // MultiSelectEnabled

        #endregion // Properties

        #region Pivate Function(s)

        private static MultiSelectTreeView GetTree(MultiSelectTreeViewItem item)
        {
            // If item is disconnected just return null
            DependencyObject test = VisualTreeHelper.GetParent(item);
            if (test == null)
            {
                return null;
            }

            DependencyObject parent = VisualTreeHelper.GetParent(item);
            MultiSelectTreeView tree = parent as MultiSelectTreeView;

            while (parent != null && tree == null)
            {
                parent = VisualTreeHelper.GetParent(parent);
                tree = parent as MultiSelectTreeView;
            }

            return tree;
        }

        #endregion // Pivate Function(s)

        #region Override(s)

        /// <summary>
        /// Makes sure that it is this class that is used for the tree view
        /// </summary>
        /// <returns></returns>
        protected override DependencyObject GetContainerForItemOverride()
        {
            return new MultiSelectTreeViewItem();
        }

        /// <summary>
        /// Makes sure that it is this class that is used for the tree view
        /// </summary>
        /// <returns></returns>
        protected override bool IsItemItsOwnContainerOverride(Object item)
        {
            return item is MultiSelectTreeViewItem;
        }

        /// <summary>
        /// If we are using multiselected we should not be using the inherited selection function
        /// </summary>
        /// <param name="e"></param>
        protected override void OnSelected(RoutedEventArgs e)
        {
            this.IsSelected = false;
        }

        protected override void OnUnselected(RoutedEventArgs e)
        {
        }

        protected override void OnCollapsed(RoutedEventArgs e)
        {
            MultiSelectTreeView tree = GetTree(this);
            if (tree != null)
            {
                tree.SelectedItems.BeginUpdate();
                Object anchor = tree.GetValue(MultiSelectTreeView.AnchorItemProperty);
                Boolean resetAnchor = false;

                for (int i = 0; i < this.Items.Count; i++)
                {
                    MultiSelectTreeViewItem item = this.ItemContainerGenerator.ContainerFromIndex(i) as MultiSelectTreeViewItem;

                    if (item == null)
                        continue;

                    if (item.IsSelected_ == true && item.Header == anchor)
                    {
                        resetAnchor = true;
                    }
                    tree.SelectedItems.Remove(item.Header);
                    item.IsSelected_ = false;
                }

                if (resetAnchor)
                {
                    if (tree.SelectedItems.Count > 0)
                    {
                        tree.SetValue(MultiSelectTreeView.AnchorItemProperty, tree.SelectedItems[tree.SelectedItems.Count - 1]);
                    }
                    else
                    {
                        this.IsSelected_ = true;
                        tree.SelectedItems.Add(this.Header);
                        tree.SetValue(MultiSelectTreeView.AnchorItemProperty, this.Header);
                    }
                }
                tree.SelectedItems.EndUpdate();
            }

            base.OnCollapsed(e);
        }

        protected override void OnHeaderChanged(object oldHeader, object newHeader)
        {
            MultiSelectTreeView tree = GetTree(this);
            if (tree != null)
            {
                if (oldHeader == null && (newHeader != null && newHeader.GetType().FullName != "MS.Internal.NamedObject"))
                {
                    if (this.IsSelected_ == true)
                    {
                        if (!tree.SelectedItems.Contains(newHeader))
                        {
                            tree.SelectedItems.Add(newHeader);
                        }
                        if (tree.AnchorItem == null)
                        {
                            tree.AnchorItem = newHeader;
                        }
                    }
                }

                Object anchor = tree.GetValue(MultiSelectTreeView.AnchorItemProperty);
                Boolean resetAnchor = false;
                if (newHeader.GetType().FullName == "MS.Internal.NamedObject")
                {
                    if (this.IsSelected_ == true && oldHeader == anchor)
                    {
                        resetAnchor = true;
                    }

                    if (resetAnchor)
                    {
                        if (tree.SelectedItems.Count > 0)
                        {
                            tree.SetValue(MultiSelectTreeView.AnchorItemProperty, tree.SelectedItems[tree.SelectedItems.Count - 1]);
                        }
                        else
                        {
                            this.IsSelected_ = true;
                            tree.SelectedItems.Add(this.Header);
                            tree.SetValue(MultiSelectTreeView.AnchorItemProperty, this.Header);
                        }
                    }
                }
            }

            base.OnHeaderChanged(oldHeader, newHeader);
        }

        protected override void OnItemsSourceChanged(IEnumerable oldValue, IEnumerable newValue)
        {
            base.OnItemsSourceChanged(oldValue, newValue);
        }

        protected override void OnItemsChanged(System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            base.OnItemsChanged(e);
        }

        #endregion // Override(s)

        #region Constructor

        public MultiSelectTreeViewItem()
            : base()
        {
        }

        #endregion // Constructor

    } // MultiSelectTreeViewItem
} // RSG.Base.Windows.Controls
