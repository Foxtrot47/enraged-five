﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Interop;

namespace RSG.Base.Windows.Helpers
{
    /// <summary>
    /// WPF window helper methods.
    /// </summary>
    public static class WpfWindowHelper
    {
        #region Constants

        const UInt32 SWP_NOSIZE = 0x0001;
        const UInt32 SWP_NOMOVE = 0x0002;

        static readonly IntPtr HWND_BOTTOM = new IntPtr(1);

        #endregion

        #region Methods

        /// <summary>
        /// Send the selected window to the back of the window stack.
        /// </summary>
        /// <param name="window">Window.</param>
        public static void SendToBack(System.Windows.Window window)
        {
            var hWnd = new WindowInteropHelper(window).Handle;
            RSG.Base.Win32.API.SetWindowPos(hWnd, HWND_BOTTOM, 0, 0, 0, 0, SWP_NOSIZE | SWP_NOMOVE);
        }

        #endregion
    }
}
