﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace RSG.Base.Windows.Helpers
{
    /// <summary>
    /// http://www.codeproject.com/Articles/25964/A-more-generic-way-of-sorting-a-WPF-ListView-with
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class ListViewCustomComparer<T> : IComparer, IListViewCustomComparer where T : class
    {
        #region [ Fields ]

        private String sortBy = String.Empty;
        private ListSortDirection direction = ListSortDirection.Ascending;

        #endregion

        #region [ Properties ]

        /// <summary>
        /// Gets or sets the sort by data column name.
        /// </summary>
        /// <value>The sort by.</value>
        public String SortBy
        {
            get { return sortBy; }
            set { sortBy = value; }
        }

        /// <summary>
        /// Gets or sets the sort direction.
        /// </summary>
        /// <value>The sort direction.</value>
        public ListSortDirection SortDirection
        {
            get { return direction; }
            set { direction = value; }
        }

        #endregion

        #region [ Methods ]

        /// <summary>
        /// Compares two objects and returns a value indicating whether one is less than, equal to, or greater than the other.
        /// </summary>
        /// <param name="x">The first object to compare.</param>
        /// <param name="y">The second object to compare.</param>
        /// <returns>
        /// Value Condition Less than zero <paramref name="x"/> is less than <paramref name="y"/>. Zero <paramref name="x"/> equals <paramref name="y"/>. Greater than zero <paramref name="x"/> is greater than <paramref name="y"/>.
        /// </returns>
        /// <exception cref="T:System.ArgumentException">Neither <paramref name="x"/> nor <paramref name="y"/> implements the <see cref="T:System.IComparable"/> interface.-or- <paramref name="x"/> and <paramref name="y"/> are of different types and neither one can handle comparisons with the other. </exception>
        public Int32 Compare(Object x, Object y)
        {

            T item1 = x as T;
            T item2 = y as T;

            if (item1 == null || item2 == null)
            {
                System.Diagnostics.Trace.Write("either x or y is null in compare(x,y)");
                return 0;
            }

            return Compare(item1, item2);
        }

        /// <summary>
        /// Compares the specified x to y.
        /// </summary>
        /// <param name="x">The x.</param>
        /// <param name="y">The y.</param>
        /// <returns></returns>
        public abstract Int32 Compare(T x, T y);

        #endregion
    }

}
