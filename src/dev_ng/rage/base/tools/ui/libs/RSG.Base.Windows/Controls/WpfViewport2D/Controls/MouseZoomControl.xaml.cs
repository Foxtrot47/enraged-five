﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace RSG.Base.Windows.Controls.WpfViewport2D.Controls
{
    /// <summary>
    /// Interaction logic for MouseZoomTool.xaml
    /// </summary>
    public partial class MouseZoomControl :
        ViewportUserControl
    {
        #region Properties

        /// <summary>
        /// The delta the mouse wheel has to move before the zoom
        /// increase/decreases by the full step amount
        /// </summary>
        public float MouseDelta
        {
            get { return m_mouseDelta; }
            set { m_mouseDelta = value; OnPropertyChanged("MouseDelta"); }
        }
        private float m_mouseDelta;

        /// <summary>
        /// The zoom factor increase step to be added when the + button is pressed
        /// </summary>
        public float ZoomFactorIncrease
        {
            get { return m_zoomFactorIncrease; }
            set { m_zoomFactorIncrease = value; OnPropertyChanged("ZoomFactorIncrease"); }
        }
        private float m_zoomFactorIncrease;

        /// <summary>
        /// The zoom factor decrease step to be added when the - button is pressed
        /// </summary>
        public float ZoomFactorDecrease
        {
            get { return m_zoomFactorDecrease; }
            set { m_zoomFactorDecrease = value; OnPropertyChanged("ZoomFactorDecrease"); }
        }
        private float m_zoomFactorDecrease;

        #endregion // Properties

        #region Constructor
        
        /// <summary>
        /// Default constructor
        /// </summary>
        public MouseZoomControl()
        {
            InitializeComponent();

            this.MouseDelta = 120.0f;
            this.ZoomFactorIncrease = 1.0f;
            this.ZoomFactorDecrease = 1.0f;
        }

        #endregion // Constructor

        #region Event Helpers

        /// <summary>
        /// Gets called when the action element sends a mouse wheel move event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public override void OnManagerMouseWheelMove(Object sender, System.Windows.Input.MouseWheelEventArgs e)
        {
            if (sender is Viewport2D)
            {
                Viewport2D viewport = sender as Viewport2D;
                if (e.Delta > 0)
                {
                    // Zooming in
                    float zoomStep = (e.Delta / (float)this.MouseDelta) * ZoomFactorIncrease;
                    float newZoomFactor = viewport.ZoomFactor - zoomStep;
                    viewport.ZoomFactor = newZoomFactor;
                }
                else if (e.Delta < 0)
                {
                    float zoomStep = (-e.Delta / this.MouseDelta) * ZoomFactorDecrease;
                    float newZoomFactor = viewport.ZoomFactor + zoomStep;
                    viewport.ZoomFactor = newZoomFactor;
                }
            }
        }

        #endregion Event Helpers

        #region Registration

        public override void RegisterActionsWithManager(Actions.ActionManager manager)
        {
            manager.RegisterMouseCommand(this, Actions.MouseEvents.Wheel_Move);
        }

        #endregion // Registration

    }
}
