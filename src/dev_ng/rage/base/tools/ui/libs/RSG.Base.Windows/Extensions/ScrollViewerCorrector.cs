﻿using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Controls;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Base.Windows.Extensions
{
    public class ScrollViewerCorrector
    {
        public static bool GetFixScrolling(DependencyObject obj)
        {
            return (bool)obj.GetValue(FixScrollingProperty);
        }

        public static void SetFixScrolling(DependencyObject obj, Boolean value)
        {
            obj.SetValue(FixScrollingProperty, value);
        }

        public static readonly DependencyProperty FixScrollingProperty =
            DependencyProperty.RegisterAttached("FixScrolling", typeof(Boolean), typeof(ScrollViewerCorrector), 
            new FrameworkPropertyMetadata(false, ScrollViewerCorrector.OnFixScrollingPropertyChanged));

        public static void OnFixScrollingPropertyChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            ScrollViewer viewer = sender as ScrollViewer;
            if (viewer == null)
                throw new ArgumentException("The dependency property can only be attached to a ScrollViewer", "sender");

            if ((Boolean)e.NewValue == true)
            {
                viewer.PreviewMouseWheel += HandlePreviewMouseWheel;
            }
            else if ((Boolean)e.NewValue == false)
            {
                viewer.PreviewMouseWheel -= HandlePreviewMouseWheel;
            }
        }

        private static List<MouseWheelEventArgs> _reentrantList = new List<MouseWheelEventArgs>();

        private static void HandlePreviewMouseWheel(Object sender, MouseWheelEventArgs e)
        {
            var scrollControl = sender as ScrollViewer;
            if (scrollControl == null)
                return;

            if (!e.Handled && sender != null && !_reentrantList.Contains(e))
            {
                var previewEventArg = new MouseWheelEventArgs(e.MouseDevice, e.Timestamp, e.Delta)
                {
                    RoutedEvent = UIElement.PreviewMouseWheelEvent,
                    Source = sender
                };

                var originalSource = e.OriginalSource as UIElement;
                if (originalSource == null)
                    return;

                _reentrantList.Add(previewEventArg);
                originalSource.RaiseEvent(previewEventArg);
                _reentrantList.Remove(previewEventArg);

                // at this point if no one else handled the event in our children, we do our job

                if (!previewEventArg.Handled)
                {
                    Boolean test1 = e.Delta > 0 && scrollControl.VerticalOffset != 0;
                    Boolean test2 = e.Delta <= 0 && scrollControl.VerticalOffset < scrollControl.ScrollableHeight;
                    if ((test1) || (test2))
                    {
                        e.Handled = true;
                        var eventArg = new MouseWheelEventArgs(e.MouseDevice, e.Timestamp, e.Delta);
                        eventArg.RoutedEvent = UIElement.MouseWheelEvent;
                        eventArg.Source = sender;
                        var parent = (UIElement)((FrameworkElement)sender).Parent;
                        parent.RaiseEvent(eventArg);
                        scrollControl.RaiseEvent(eventArg);
                    }
                }
            }
        }

    }
}
