﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Mail;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Interop;
using RSG.Base.Logging;
using RSG.Base.Win32;
using System.ComponentModel;
using System.Windows.Media.Animation;
using RSG.Interop.Microsoft.Office.Outlook;
using RSG.Interop.Microsoft.Office;

namespace RSG.Base.Windows
{
    /// <summary>
    /// Exception stack trace dialog window.
    /// </summary>
    public partial class ExceptionStackTraceDlg : Window, INotifyPropertyChanged
    {
        #region Constants
        /// <summary>
        /// SMTP email server host.
        /// </summary>
        private readonly String EMAIL_SERVER = "rsgediexg1.rockstar.t2.corp"; 
        
        /// <summary>
        /// Devstar Help URL.
        /// </summary>
        private const String URL_HELP = "https://devstar.rockstargames.com/wiki/index.php/Dev:Exception_Dialog";
        #endregion // Constants

        #region Events
        /// <summary>
        /// Event raised when the user clicks on the "Log Bug" button.
        /// </summary>
        public event EventHandler<LogBugEventArgs> LogBug;

        /// <summary>
        /// Event raised when a property in the Model has changed.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion // Events

        #region Properties
        /// <summary>
        /// Application Name
        /// </summary>
        public String ApplicationName
        {
            get;
            private set;
        }

        /// <summary>
        /// Application Version
        /// </summary>
        public String ApplicationVersion
        {
            get;
            private set;
        }
        
        /// <summary>
        /// Application Author
        /// </summary>
        public String ApplicationAuthor
        {
            get;
            private set;
        }
        
        /// <summary>
        /// Application Email
        /// </summary>
        public String ApplicationEmail
        {
            get;
            private set;
        }

        /// <summary>
        /// User email address.
        /// </summary>
        public String UserEmail
        {
            get;
            private set;
        }

        /// <summary>
        /// Details provided by the user regarding the exception
        /// </summary>
        public String UserDetails
        {
            get;
            set;
        }

        /// <summary>
        /// Summary title string for user.
        /// </summary>
        public String SummaryTitle
        {
            get;
            private set;
        }

        /// <summary>
        /// Summary text string for user.
        /// </summary>
        public String SummaryText
        {
            get;
            private set;
        }

        /// <summary>
        /// Details regarding the exception (generally includes the stack trace)
        /// </summary>
        public String ExceptionDetails
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public String ExceptionSummary
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public bool CanEmail
        {
            get
            {
                return (m_canEmail && this.ApplicationEmail.Length > 0);
            }
            private set
            {
                m_canEmail = value;
                OnPropertyChanged("CanEmail");
            }
        }
        private bool m_canEmail = true;

        /// <summary>
        /// 
        /// </summary>
        public bool CanLogBug
        {
            get
            {
                return (null != this.LogBug);
            }
        }
        #endregion // Properties and Associated Member Data

        #region Constructor
        /// <summary>
        /// Constructor; specifying Exception object.
        /// </summary>
        /// <param name="ex">Exception object.</param>
        /// <param name="userAddress">User email address.</param>
        public ExceptionStackTraceDlg(Exception ex, String userAddress)
            : this(ex, userAddress, String.Empty, String.Empty)
        {
        }

        /// <summary>
        /// Constructor; specifying Exception and author.
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="author"></param>
        public ExceptionStackTraceDlg(Exception ex, String userAddress, String author)
            : this(ex, userAddress, author, String.Empty)
        {
        }

        /// <summary>
        /// Constructor; specifying Exception, author and email address (enables
        /// email features).
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="author"></param>
        /// <param name="email"></param>
        public ExceptionStackTraceDlg(Exception ex, String userAddress, String author, String email)
        {
            InitializeComponent();
            Initialise(ex, userAddress, author, email);
            this.DataContext = this;
        }

        /// <summary>
        /// Constructor; specifying string data (for use by external scripts).
        /// </summary>
        /// <param name="message"></param>
        /// <param name="stack"></param>
        /// <param name="script"></param>
        /// <param name="version"></param>
        /// <param name="userEmail"></param>
        /// <param name="author"></param>
        /// <param name="email"></param>
        public ExceptionStackTraceDlg(String message, String exceptionType, String stack, String script, 
            String version, String userEmail, String author, String email)
        {
            InitializeComponent();
            this.DataContext = this;

            // Custom initialisation; since no Exception object.
            this.ApplicationName = script;
            this.ApplicationVersion = version;
            this.ApplicationAuthor = author;
            this.ApplicationEmail = email;
            this.UserEmail = userEmail;
            this.UserDetails = null;

            String[] parts = stack.Split(new Char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
            String summary = String.Format("{0} exception: {1}",
                exceptionType, message);

            this.SummaryText = summary;
            this.SummaryTitle = summary;
            this.ExceptionSummary = String.Format("{0} exception in {1}, version: {2} at {3}.",
                exceptionType, script, version, DateTime.Now);
            this.ExceptionDetails = String.Join(Environment.NewLine, parts);
            this.ExceptionDetails = ExceptionDetails + "\r\n\r\n--------------------\r\n" + String.Format("from {0}@{1}", Environment.UserName, Environment.MachineName);
        }
        #endregion // Constructors

        #region Private Methods
        /// <summary>
        /// Common initialisation method (invoked from constructors)
        /// </summary>
        /// <param name="ex">Exception object.</param>
        /// <param name="userAddress">User email address.</param>
        /// <param name="author">Author alias.</param>
        /// <param name="email">Author email address.</param>
        private void Initialise(Exception ex, String userAddress, String author, String email)
        {
            this.ApplicationName = System.Windows.Forms.Application.ProductName;
            this.ApplicationVersion = System.Windows.Forms.Application.ProductVersion;
            this.ApplicationAuthor = author;
            this.ApplicationEmail = email;
            this.UserEmail = userAddress;
            this.UserDetails = null;

            ExceptionFormatter formatter = new ExceptionFormatter(ex);

            String summary = String.Format("{0} exception: {1}",
                ex.GetType().ToString(), ex.Message);

            this.SummaryText = summary;
            this.SummaryTitle = summary;
            this.ExceptionSummary = formatter.FormatHeading();
            this.ExceptionDetails = String.Join("\n", formatter.Format());

            this.ExceptionDetails = ExceptionDetails + "\r\n\r\n--------------------\r\n" + String.Format("from {0}@{1}", Environment.UserName, Environment.MachineName);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="prop"></param>
        private void OnPropertyChanged(string prop)
        {
            if( PropertyChanged != null )
            {
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
            }
        }

        #region Event Handlers
        /// <summary>
        /// Window Loaded event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_Loaded(Object sender, RoutedEventArgs e)
        {
        }

        /// <summary>
        /// "Log Bug" button click handler.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnLogBug_Click(Object sender, RoutedEventArgs e)
        {
            if (null != this.LogBug)
            {
                String user = "*Default Tools*";
                String summary = ExceptionSummary;
                String desc = ExceptionDetails;
                LogBug(this, new LogBugEventArgs(summary, desc, user));
            }
        }

        /// <summary>
        /// Save button click handler.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSave_Click(Object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
            dlg.Filter = "Text file (.txt)|*.txt";
            if (false == dlg.ShowDialog())
                return;

            try
            {
                System.IO.StreamWriter txtFile =
                    new System.IO.StreamWriter(dlg.FileName, false);
                txtFile.Write(ExceptionDetails);
                txtFile.Flush();
                txtFile.Close();
            }
            catch (System.IO.IOException ex)
            {
                MessageBox.Show(
                    String.Format("Failed to create text file stream (IOException): {0}.", ex.Message),
                    "IO Exception Saving Exception Information",
                    MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
        }

        /// <summary>
        /// Close button click handler.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(Object sender, RoutedEventArgs e)
        {
            Close();
        }

        /// <summary>
        /// Email button click handler.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnEmail_Click(Object sender, RoutedEventArgs e)
        {
            try
            {
                using (IApplication app = OutlookApplication.Create())
                {
                    object obj = app.CreateItem(OlItemType.olMailItem);
                    using (IMailItem newMail = (IMailItem)COMWrapper.Wrap(obj, typeof(IMailItem)))
                    {
                        // Fill in the default information.
                        newMail.Recipients.Add(this.ApplicationEmail);
                        newMail.Recipients.ResolveAll();

                        newMail.Subject = ExceptionSummary;

                        newMail.Body = "User Information:\n";
                        if (UserDetails != null)
                        {
                            newMail.Body += UserDetails;
                        }
                        else
                        {
                            newMail.Body += "Please enter any relevant details about how this exception was generated and steps to reproduce prior to ";
                            newMail.Body += "this information or raising a bug to the application author(s) If you do not add additional details here ";
                            newMail.Body += "it may be difficult for the author(s) to replicate and fix the issue.";
                        }
                        newMail.Body += "\n\nException Details:";
                        newMail.Body += ExceptionDetails;
                        newMail.Display(false);
                    }
                }
            }
            catch (System.Exception ex)
            {
                MessageBox.Show("A problem occurred while attempting to create a new email using Outlook.  Please contact " +
                    "the RSGEDI Tools mailing list including any information about what you were doing and might find relevant.\r\n\r\n" +
                    ex.Message);
            }
        }
        #endregion // Event Handlers
        #endregion // Private Methods
        
        #region Dialog Help Button and Handler
        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        /// See http://stackoverflow.com/questions/1009983/help-button.
        /// 
        protected override void OnSourceInitialized(EventArgs e)
        {
            base.OnSourceInitialized(e);

            IntPtr hwnd = new System.Windows.Interop.WindowInteropHelper(this).Handle;
            uint styles = API.GetWindowLong(hwnd, API.GWL_STYLE);
            styles &= 0xFFFFFFFF ^ (API.WS_MINIMIZEBOX | API.WS_MAXIMIZEBOX);
            API.SetWindowLong(hwnd, API.GWL_STYLE, styles);
            styles = API.GetWindowLong(hwnd, API.GWL_EXSTYLE);
            styles |= API.WS_EX_CONTEXTHELP;
            API.SetWindowLong(hwnd, API.GWL_EXSTYLE, styles);
            API.SetWindowPos(hwnd, IntPtr.Zero, 0, 0, 0, 0, API.SWP_NOMOVE | API.SWP_NOSIZE | API.SWP_NOZORDER | API.SWP_FRAMECHANGED);
            ((HwndSource)PresentationSource.FromVisual(this)).AddHook(HelpHook);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="hwnd"></param>
        /// <param name="msg"></param>
        /// <param name="wParam"></param>
        /// <param name="lParam"></param>
        /// <param name="handled"></param>
        /// <returns></returns>
        private IntPtr HelpHook(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
        {
            if (API.WM_SYSCOMMAND == msg && ((int)wParam & 0xFFF0) == API.SC_CONTEXTHELP)
            {
                API.ShellExecute(IntPtr.Zero, "open", URL_HELP, String.Empty, String.Empty, 0);
                handled = true;
            }
            return IntPtr.Zero;
        }
        #endregion // Dialog Help Button and Handler
    }

    #region Event Argument Classes
    /// <summary>
    /// Log bug event handler argument class.
    /// </summary>
    public class LogBugEventArgs : EventArgs
    {
        #region Properties
        /// <summary>
        /// Summary text.
        /// </summary>
        public String Summary
        {
            get;
            private set;
        }

        /// <summary>
        /// Owner default (e.g. "*Default Tools*").
        /// </summary>
        public String DefaultOwner
        {
            get;
            private set;
        }

        /// <summary>
        /// Description text information.
        /// </summary>
        public String Description
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="summary"></param>
        /// <param name="owner"></param>
        /// <param name="ex"></param>
        public LogBugEventArgs(String summary, String description, String owner)
        {
            this.Summary = summary;
            this.DefaultOwner = owner;
            this.Description = description;
        }
        #endregion // Constructor(s)
    }
    #endregion // Event Argument Classes


    /// <summary>
    /// Changes the value of a GridLength property from its original value to a
    /// specified new value.
    /// </summary>
    public class GridLengthChanger : AnimationTimeline
    {
        #region Fields
        /// <summary>
        /// Identifies the <see cref="NewValue"/> dependency property.
        /// </summary>
        public static DependencyProperty NewValueProperty;
        #endregion

        #region Constructor
        /// <summary>
        /// Initialises static members of the
        /// <see cref="RSG.Base.Windows.GridLengthChanger"/> class.
        /// </summary>
        static GridLengthChanger()
        {
            NewValueProperty =
                DependencyProperty.Register(
                "NewValue",
                typeof(GridLength),
                typeof(GridLengthChanger));
        }

        /// <summary>
        /// Initialises a new instance of the
        /// <see cref="RSG.Base.Windows.GridLengthChanger"/> class.
        /// </summary>
        public GridLengthChanger()
        {
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the value the GridLength property will be changed into.
        /// </summary>
        public GridLength NewValue
        {
            get { return (GridLength)this.GetValue(NewValueProperty); }
            set { this.SetValue(NewValueProperty, value); }
        }

        /// <summary>
        /// Gets the type of property that can be animated by this animation.
        /// </summary>
        public override Type TargetPropertyType
        {
            get { return typeof(GridLength); }
        }

        /// <summary>
        /// Gets the length of time for which this timeline plays, not counting repetitions.
        /// </summary>
        private new Duration Duration
        {
            get { return new Duration(); }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Gets the current value of the animation. In this case just returns the new value as
        /// the changer always has a animation time of 0.
        /// </summary>
        /// <param name="defaultOrigin">
        /// The parameter is not used.
        /// </param>
        /// <param name="defaultDestination">
        /// The parameter is not used.
        /// </param>
        /// <param name="clock">
        /// The parameter is not used.
        /// </param>
        /// <returns>
        /// The new value set for this changer.
        /// </returns>
        public override object GetCurrentValue(
            object defaultOrigin, object defaultDestination, AnimationClock clock)
        {
            return (GridLength)this.GetValue(GridLengthChanger.NewValueProperty);
        }

        /// <summary>
        /// Creates a new instance of this class as a
        /// <see cref="System.Windows.Freezable"/> object.
        /// </summary>
        /// <returns>
        /// The new instance.
        /// </returns>
        protected override Freezable CreateInstanceCore()
        {
            return new GridLengthChanger();
        }
        #endregion
    } // RSG.Base.Windows.GridLengthChanger {Class}
} // RSG.Base.Windows namespace
