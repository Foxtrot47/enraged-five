﻿#pragma checksum "..\..\..\..\Controls\WpfViewport2D\Viewport2D.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "A2B2BE56CFA08FA82CBFD5776953D73BCEE32E825584BDD45E6100C4ACB48D76"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using RSG.Base.Windows.Controls.WpfViewport2D;
using RSG.Base.Windows.Controls.WpfViewport2D.Geometry;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace RSG.Base.Windows.Controls.WpfViewport2D {
    
    
    /// <summary>
    /// Viewport2D
    /// </summary>
    public partial class Viewport2D : RSG.Base.Windows.Controls.WpfViewport2D.ActionElement, System.Windows.Markup.IComponentConnector {
        
        
        #line 363 "..\..\..\..\Controls\WpfViewport2D\Viewport2D.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListBox GeometryListBox;
        
        #line default
        #line hidden
        
        
        #line 367 "..\..\..\..\Controls\WpfViewport2D\Viewport2D.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ItemsControl TopPanel;
        
        #line default
        #line hidden
        
        
        #line 371 "..\..\..\..\Controls\WpfViewport2D\Viewport2D.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ItemsControl LeftPanel;
        
        #line default
        #line hidden
        
        
        #line 375 "..\..\..\..\Controls\WpfViewport2D\Viewport2D.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ItemsControl RightPanel;
        
        #line default
        #line hidden
        
        
        #line 379 "..\..\..\..\Controls\WpfViewport2D\Viewport2D.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ItemsControl BottomLeftPanel;
        
        #line default
        #line hidden
        
        
        #line 383 "..\..\..\..\Controls\WpfViewport2D\Viewport2D.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ItemsControl BottomRightPanel;
        
        #line default
        #line hidden
        
        
        #line 387 "..\..\..\..\Controls\WpfViewport2D\Viewport2D.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ItemsControl MiddlePanel;
        
        #line default
        #line hidden
        
        
        #line 391 "..\..\..\..\Controls\WpfViewport2D\Viewport2D.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ItemsControl ToolTipPanel;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/RSG.Base.Windows;component/controls/wpfviewport2d/viewport2d.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\..\Controls\WpfViewport2D\Viewport2D.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            
            #line 312 "..\..\..\..\Controls\WpfViewport2D\Viewport2D.xaml"
            ((System.Windows.Controls.Grid)(target)).PreviewMouseMove += new System.Windows.Input.MouseEventHandler(this.Grid_PreviewMouseMove);
            
            #line default
            #line hidden
            
            #line 312 "..\..\..\..\Controls\WpfViewport2D\Viewport2D.xaml"
            ((System.Windows.Controls.Grid)(target)).MouseLeave += new System.Windows.Input.MouseEventHandler(this.Grid_MouseLeave);
            
            #line default
            #line hidden
            return;
            case 2:
            this.GeometryListBox = ((System.Windows.Controls.ListBox)(target));
            return;
            case 3:
            this.TopPanel = ((System.Windows.Controls.ItemsControl)(target));
            return;
            case 4:
            this.LeftPanel = ((System.Windows.Controls.ItemsControl)(target));
            return;
            case 5:
            this.RightPanel = ((System.Windows.Controls.ItemsControl)(target));
            return;
            case 6:
            this.BottomLeftPanel = ((System.Windows.Controls.ItemsControl)(target));
            return;
            case 7:
            this.BottomRightPanel = ((System.Windows.Controls.ItemsControl)(target));
            return;
            case 8:
            this.MiddlePanel = ((System.Windows.Controls.ItemsControl)(target));
            return;
            case 9:
            this.ToolTipPanel = ((System.Windows.Controls.ItemsControl)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

