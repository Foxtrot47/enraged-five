﻿using System;
using System.ComponentModel;
using RSG.Base.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using RSG.Base.Windows.Controls.WpfViewport2D.Geometry;
using RSG.Base.Windows.Controls.WpfViewport2D.Actions;
using RSG.Base.Math;
using System.Timers;
using System.Windows.Threading;
using System.Globalization;
using System.IO;

namespace RSG.Base.Windows.Controls.WpfViewport2D
{
    /// <summary>
    /// Interaction logic for Viewport2D.xaml
    /// </summary>
    public partial class Viewport2D : ActionElement
    {
        #region Properties

        #region Control Panels

        /// <summary>
        /// The list of controls that are present in the top panel of the view port.
        /// </summary>
        public ObservableCollection<UIElement> TopPanelControls
        {
            get { return (ObservableCollection<UIElement>)GetValue(TopPanelControlsProperty); }
            set { SetValue(TopPanelControlsProperty, value); }
        }
        public static DependencyProperty TopPanelControlsProperty = DependencyProperty.Register("TopPanelControls",
            typeof(ObservableCollection<UIElement>), typeof(Viewport2D));

        /// <summary>
        /// The list of controls that are present in the left panel of the view port.
        /// </summary>
        public ObservableCollection<UIElement> LeftPanelControls
        {
            get { return (ObservableCollection<UIElement>)GetValue(LeftPanelControlsProperty); }
            set { SetValue(LeftPanelControlsProperty, value); }
        }
        public static DependencyProperty LeftPanelControlsProperty = DependencyProperty.Register("LeftPanelControls",
            typeof(ObservableCollection<UIElement>), typeof(Viewport2D));

        /// <summary>
        /// The list of controls that are present in the right panel of the view port.
        /// </summary>
        public ObservableCollection<UIElement> RightPanelControls
        {
            get { return (ObservableCollection<UIElement>)GetValue(RightPanelControlsProperty); }
            set { SetValue(RightPanelControlsProperty, value); }
        }
        public static DependencyProperty RightPanelControlsProperty = DependencyProperty.Register("RightPanelControls",
            typeof(ObservableCollection<UIElement>), typeof(Viewport2D));

        /// <summary>
        /// The list of controls that are present in the bottom panel of the view port.
        /// </summary>
        public ObservableCollection<UIElement> BottomLeftPanelControls
        {
            get { return (ObservableCollection<UIElement>)GetValue(BottomLeftPanelControlsProperty); }
            set { SetValue(BottomLeftPanelControlsProperty, value); }
        }
        public static DependencyProperty BottomLeftPanelControlsProperty = DependencyProperty.Register("BottomLeftPanelControls",
            typeof(ObservableCollection<UIElement>), typeof(Viewport2D));

        /// <summary>
        /// The list of controls that are present in the bottom panel of the view port.
        /// </summary>
        public ObservableCollection<UIElement> BottomRightPanelControls
        {
            get { return (ObservableCollection<UIElement>)GetValue(BottomRightPanelControlsProperty); }
            set { SetValue(BottomRightPanelControlsProperty, value); }
        }
        public static DependencyProperty BottomRightPanelControlsProperty = DependencyProperty.Register("BottomRightPanelControls",
            typeof(ObservableCollection<UIElement>), typeof(Viewport2D));

        /// <summary>
        /// The list of controls that are present in the middle panel of the view port.
        /// </summary>
        public ObservableCollection<UIElement> MiddlePanelControls
        {
            get { return (ObservableCollection<UIElement>)GetValue(MiddlePanelControlsProperty); }
            set { SetValue(MiddlePanelControlsProperty, value); }
        }
        public static DependencyProperty MiddlePanelControlsProperty = DependencyProperty.Register("MiddlePanelControls",
            typeof(ObservableCollection<UIElement>), typeof(Viewport2D));

        /// <summary>
        /// The list of controls to be presented when the tool tip appears.
        /// </summary>
        public ObservableCollection<UIElement> ToolTipControls
        {
            get { return (ObservableCollection<UIElement>)GetValue(ToolTipControlsProperty); }
            set { SetValue(ToolTipControlsProperty, value); }
        }
        public static DependencyProperty ToolTipControlsProperty = DependencyProperty.Register("ToolTipControls",
            typeof(ObservableCollection<UIElement>), typeof(Viewport2D));

        #endregion // Control Panels

        #region Geometry

        /// <summary>
        /// The list of geometry that the main canvas is bound to and therefore the main 
        /// list of geometry that is drawn
        /// </summary>
        public ObservableCollection<Viewport2DGeometry> GeometrySource
        {
            get { return (ObservableCollection<Viewport2DGeometry>)GetValue(GeometrySourceProperty); }
            set 
            { 
                SetValue(GeometrySourceProperty, value);
            }
        }
        public static DependencyProperty GeometrySourceProperty = DependencyProperty.Register("GeometrySource",
            typeof(ObservableCollection<Viewport2DGeometry>), typeof(Viewport2D), new PropertyMetadata(null, new PropertyChangedCallback(OnGeometrySourceChanged)));

        private static void OnGeometrySourceChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {

        }

        #endregion // Geometry

        /// <summary>
        /// This is the world origin in world coordinates
        /// </summary>
        public Point WorldOrigin
        {
            get { return m_worldOrigin; }
            set { m_worldOrigin = value; OnPropertyChanged("WorldOrigin"); }
        }
        private Point m_worldOrigin;

        /// <summary>
        /// This is the position of the viewports center in world coordinates, basically
        /// this is the point that the viewport is looking at in world coordinates
        /// </summary>
        public Point WorldLookAt
        {
            get { return m_worldLookAt; }
            set
            {
                m_worldLookAt = value; 
                OnPropertyChanged("WorldLookAt");
                CalculateShapeTransform();
            }
        }
        private Point m_worldLookAt;

        /// <summary>
        /// This is the zoom factor, the integer value represents
        /// the number of world units per pixel rendered. The default
        /// zoom factor is 1.0 which means one world unit per pixel. 
        /// </summary>
        public float ZoomFactor
        {
            get { return m_zoomFactor; }
            set 
            { 
                // Clamp the zoom factor to the min/max values
                float clampedValue = value;
                if (clampedValue > MaximumZoomFactor)
                {
                    clampedValue = MaximumZoomFactor;
                }
                if (clampedValue < MinimumZoomFactor)
                {
                    clampedValue = MinimumZoomFactor;
                }

                // Change the zoom factor only if it's changed
                if (m_zoomFactor != clampedValue)
                {
                    m_zoomFactor = clampedValue;
                    OnPropertyChanged("ZoomFactor");
                    CalculateShapeTransform();
                    CalculateTextScale();
                }
            }
        }
        private float m_zoomFactor;

        /// <summary>
        /// The maximum zoom factor this control can set
        /// </summary>
        public float MaximumZoomFactor
        {
            get { return m_maximumZoomFactor; }
            set
            {
                m_maximumZoomFactor = value;
                OnPropertyChanged("MaximumZoomFactor");
            }
        }
        private float m_maximumZoomFactor;

        /// <summary>
        /// The minimum zoom factor this control can set
        /// </summary>
        public float MinimumZoomFactor
        {
            get { return m_mimimumZoomFactor; }
            set
            {
                m_mimimumZoomFactor = value;
                OnPropertyChanged("MinimumZoomFactor");
            }
        }
        private float m_mimimumZoomFactor;

        /// <summary>
        /// The tranform matrix to multiple the shape points by
        /// </summary>
        public Matrix33f ShapeTransform
        {
            get { return m_ShapeTransform; }
            set 
            { 
                m_ShapeTransform = value;
                OnPropertyChanged("ShapeTransform");
            }
        }
        private Matrix33f m_ShapeTransform;

        /// <summary>
        /// Represents the scaling that the text needs to be set at
        /// </summary>
        public float TextScale
        {
            get { return m_textScale; }
            set
            {
                m_textScale = value;
                OnPropertyChanged("TextScale");
            }
        }
        private float m_textScale = 1.0f;

        /// <summary>
        /// Caluclates the shape transform matrix by using the zoom factor, loot at point and render size
        /// </summary>
        private void CalculateShapeTransform()
        {
            float halfWorldUnitsWide = ((float)(RenderSize.Width) * this.ZoomFactor) * 0.5f;
            float halfWorldUnitsHigh = ((float)(RenderSize.Height) * this.ZoomFactor) * 0.5f;
            Vector2f topLeft = new Vector2f((float)WorldLookAt.X - halfWorldUnitsWide, (float)WorldLookAt.Y + halfWorldUnitsHigh);

            float inverseZoom = 1.0f / this.ZoomFactor;
            this.ShapeTransform = new Matrix33f(new Vector3f(inverseZoom, 0.0f, 0.0f),
                                                     new Vector3f(0.0f, -(inverseZoom), 0.0f),
                                                     new Vector3f((-topLeft.X) * inverseZoom, (topLeft.Y * inverseZoom), 1.0f));
        }

        /// <summary>
        /// Calculates what the text should be scaled at
        /// </summary>
        private void CalculateTextScale()
        {
            float zoomDifference = this.ZoomFactor - 1.0f;
            zoomDifference = zoomDifference < 1.0f ? 1.0f : zoomDifference;

            zoomDifference = (float)System.Math.Pow(zoomDifference, 0.5);
            this.TextScale = 1.0f / zoomDifference;
        }

        private Boolean ZoomToFitOnSizeChanged = false;
        
        #region RenderOptions

        /// <summary>
        /// Determines whether the user text of the geometry objects are rendered
        /// </summary>
        public Boolean RenderUserText
        {
            get { return (Boolean)GetValue(RenderUserTextProperty); }
            set { SetValue(RenderUserTextProperty, value); OnPropertyChanged("RenderUserText"); }
        }
        public static DependencyProperty RenderUserTextProperty = DependencyProperty.Register("RenderUserText", typeof(Boolean), typeof(Viewport2D));   

        #endregion

        #endregion // Properties

        #region Constructor

        public Viewport2D()
        {
            this.SnapsToDevicePixels = true;
            InitializeComponent();

            this.TopPanelControls = new ObservableCollection<UIElement>();
            this.TopPanelControls.CollectionChanged += EmbeddedControlsChanged;
            this.LeftPanelControls = new ObservableCollection<UIElement>();
            this.LeftPanelControls.CollectionChanged += EmbeddedControlsChanged;
            this.RightPanelControls = new ObservableCollection<UIElement>();
            this.RightPanelControls.CollectionChanged += EmbeddedControlsChanged;
            this.BottomLeftPanelControls = new ObservableCollection<UIElement>();
            this.BottomLeftPanelControls.CollectionChanged += EmbeddedControlsChanged;
            this.BottomRightPanelControls = new ObservableCollection<UIElement>();
            this.BottomRightPanelControls.CollectionChanged += EmbeddedControlsChanged;
            this.MiddlePanelControls = new ObservableCollection<UIElement>();
            this.MiddlePanelControls.CollectionChanged += EmbeddedControlsChanged;
            this.ToolTipControls = new ObservableCollection<UIElement>();
            this.ToolTipControls.CollectionChanged += EmbeddedControlsChanged;

            this.WorldLookAt = new Point(0.0, 0.0);
            this.ZoomFactor = 1.0f;
            this.MaximumZoomFactor = 100.0f;
            this.MinimumZoomFactor = 0.1f;

            this.RenderUserText = true;
        }

        #endregion // Constructor

        #region Function(s)

        void EmbeddedControlsChanged(Object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null)
            {
                foreach (var newControl in e.NewItems)
                {
                    if (newControl is ActionElementControl)
                    {
                        this.RegisterViewportControl(newControl as ActionElementControl);
                    }
                }
            }
            if (e.OldItems != null)
            {
                foreach (var oldControl in e.OldItems)
                {
                    if (oldControl is ActionElementControl)
                    {
                        this.UnRegisterViewportControl(oldControl as ActionElementControl);
                    }
                }
            }
        }

        public void RegisterViewportControl(ActionElementControl control)
        {
            this.RegisterActionElementControl(control);
        }

        public void UnRegisterViewportControl(ActionElementControl control)
        {
            this.UnRegisterActionElementControl(control);
        }

        /// <summary>
        /// Need to fire the on property changed event for the render size so that the rendering 
        /// converters will pick it up.
        /// </summary>
        /// <param name="sizeInfo"></param>
        protected override void OnRenderSizeChanged(SizeChangedInfo sizeInfo)
        {
            base.OnRenderSizeChanged(sizeInfo);
            OnPropertyChanged("RenderSize");
            CalculateShapeTransform();

            if (ZoomToFitOnSizeChanged == true)
            {
                ZoomToFitOnSizeChanged = false;
                this.ZoomFactor = GetZoomFactorToFit();
            }
        }

        /// <summary>
        /// Save the geometry that is currently displayed out to a file
        /// </summary>
        /// <param name="filepath"></param>
        public void SaveGeometryToImage(string filepath, BoundingBox2f imageBounds)
        {
            const float c_saveToImageScale = 0.33f;
            int levelWidth = (int)(imageBounds.Max.X - imageBounds.Min.X);
            int levelHeight = (int)(imageBounds.Max.Y - imageBounds.Min.Y);

            int width = (int)(levelWidth * c_saveToImageScale);
            int height = (int)(levelHeight * c_saveToImageScale);

            RenderTargetBitmap renderTarget = new RenderTargetBitmap(width, height, 96d, 96d, PixelFormats.Default);

            float previousZoom = ZoomFactor;
            Point previousWorldLookAt = WorldLookAt;

            ZoomFactor = (float)(1.0 / c_saveToImageScale);
            WorldLookAt = new Point(WorldOrigin.X - (levelWidth / 2.0), WorldOrigin.Y + (levelHeight / 2.0));

            float halfWorldUnitsWide = ((float)(RenderSize.Width) * this.ZoomFactor) * 0.5f;
            float halfWorldUnitsHigh = ((float)(RenderSize.Height) * this.ZoomFactor) * 0.5f;

            WorldLookAt = new Point(WorldLookAt.X + halfWorldUnitsWide, WorldLookAt.Y - halfWorldUnitsHigh);

            //int listBoxChildrenCount = VisualTreeHelper.GetChildrenCount(GeometryListBox);
            GeometryListBox.Measure(new Size(width, height));
            GeometryListBox.Arrange(new Rect(new Size(width, height)));
            GeometryListBox.UpdateLayout();
            
            int listBoxChildrenCount = GeometryListBox.Items.Count;
            for (int i = 0; i < listBoxChildrenCount; ++i)
            {
                ListBoxItem listBoxItem = (ListBoxItem)(GeometryListBox.ItemContainerGenerator.ContainerFromIndex(i));

                renderTarget.Render(listBoxItem);
            }

            // Save out the chart to a png file
            PngBitmapEncoder png = new PngBitmapEncoder();
            png.Frames.Add(BitmapFrame.Create(renderTarget));

            using (Stream fileStream = File.Create(filepath))
            {
                png.Save(fileStream);
                fileStream.Flush();
            }

            ZoomFactor = previousZoom;
            WorldLookAt = previousWorldLookAt;
        }

        #endregion // Function(s)

        #region ViewManipulation

        /// <summary>
        /// Calculates the extreme world bounds
        /// </summary>
        /// <returns></returns>
        public BoundingBox2f CalculateWorldBounds()
        {
            BoundingBox2f wbbox = new BoundingBox2f();

            if (this.GeometrySource != null)
            {
                foreach (Viewport2DGeometry geometry in this.GeometrySource)
                {
                    BoundingBox2f bbox = geometry.GetWorldBoundingBox(this);
                    wbbox.Expand(bbox);
                }
            }
            return (wbbox);
        }

        /// <summary>
        /// Gets the zoom factor needed to fit the world bounds into the viewport
        /// </summary>
        /// <returns></returns>
        public float GetZoomFactorToFit()
        {
            return GetZoomFactorToFit(CalculateWorldBounds());
        }

        /// <summary>
        /// Calculates the zoom factor to fit a particular bounding box
        /// </summary>
        /// <param name="bbox"></param>
        /// <returns></returns>
        public float GetZoomFactorToFit(BoundingBox2f bbox)
        {
            // Ensure the rendersize of the viewport is valid
            if (this.RenderSize.Width == 0 || this.RenderSize.Height == 0)
            {
                ZoomToFitOnSizeChanged = true;
                return 1.0f;
            }

            // Ensure the bounding box passed in has a valid width/height
            if (bbox.Width == 0.0f || bbox.Height == 0.0f)
            {
                return 1.0f;
            }

            // Work out what the width zoom factor needs to be
            float worldWidth = bbox.Max.X - bbox.Min.X;
            float widthZoom = worldWidth / (float)this.RenderSize.Width;

            // Work out what the height zoom factor needs to be
            float worldHeight = bbox.Max.Y - bbox.Min.Y;
            float heightZoom = worldHeight / (float)this.RenderSize.Height;

            return heightZoom > widthZoom ? heightZoom : widthZoom;
        }

        /// <summary>
        /// Returns the center position to the world origin
        /// </summary>
        public void Center()
        {
            this.WorldLookAt = this.WorldOrigin;
        }

        /// <summary>
        /// Pan viewport by the given displacement in world units
        /// </summary>
        /// <param name="displacement"></param>
        public void Pan(Vector displacement)
        {
            Point newCenter = this.WorldLookAt + displacement;
            this.WorldLookAt = newCenter;
        }

        #endregion // ViewManipulation

        #region Coordinate Manipulation

        /// <summary>
        /// Transforms the given position in viewport coordinates into
        /// world coordinates
        /// </summary>
        public Point ViewToWorld(Point view)
        {
            Point viewCenter = new Point(this.RenderSize.Width / 2.0, this.RenderSize.Height / 2.0);
            Vector centerDisplacement = view - viewCenter;

            Point worldPoint = new Point(this.WorldLookAt.X + (centerDisplacement.X * this.ZoomFactor), this.WorldLookAt.Y - (centerDisplacement.Y * this.ZoomFactor));
            return worldPoint;
        }

        /// <summary>
        /// Transforms the given position in world coordinates into
        /// viewport coordinates
        /// </summary>
        public Point WorldToView(Point world)
        {
            // Get the top left corner of the viewport in world coordinates based on the zoom level
            double halfWorldUnitsWide = ((this.RenderSize.Width) * this.ZoomFactor) / 2.0;
            double halfWorldUnitsHigh = ((this.RenderSize.Height) * this.ZoomFactor) / 2.0;
            Point topLeft = new Point(this.WorldLookAt.X - halfWorldUnitsWide, this.WorldLookAt.Y + halfWorldUnitsHigh);

            Vector offset = world - topLeft;

            return new Point(offset.X / this.ZoomFactor, offset.Y / this.ZoomFactor);
        }

        #endregion // Coordinate Manipulation


        private Viewport2DGeometry _previousOverGeometry = null;
        private DispatcherTimer _tooltipTimer = null;
        private FrameworkElement _tooltipElement = null;
        private void Grid_PreviewMouseMove(object sender, MouseEventArgs e)
        {
            _tooltipElement = sender as FrameworkElement;
            Point viewPosition = Mouse.GetPosition(this);
            System.Windows.Controls.ToolTip tt = new ToolTip();
            foreach (Viewport2DGeometry geometry in this.GeometrySource.Reverse())
            {
                if (geometry.HasBeenPicked(viewPosition, this))
                {
                    if (geometry != _previousOverGeometry)
                    {
                        _previousOverGeometry = geometry;
                        if (_tooltipTimer != null)
                        {
                            _tooltipTimer.Stop();
                            _tooltipTimer = null;
                        }

                        tt = _tooltipElement.ToolTip as System.Windows.Controls.ToolTip;
                        if (tt != null)
                            tt.IsOpen = false;

                        if (geometry.ToolTip != null)
                        {
                            _tooltipTimer = new DispatcherTimer(new TimeSpan(0, 0, 0, 0, 500), DispatcherPriority.Normal, _tooltipTimer_Tick, this.Dispatcher);
                            _tooltipTimer.Start();
                        }
                        return;
                    }
                    else
                    {
                        return;
                    }
                }
            }

            if (_tooltipTimer != null)
            {
                _tooltipTimer.Stop();
                _tooltipTimer = null;
            }
            _previousOverGeometry = null;
            tt = _tooltipElement.ToolTip as System.Windows.Controls.ToolTip;
            if (tt != null)
                tt.IsOpen = false;
        }

        void _tooltipTimer_Tick(object sender, EventArgs e)
        {
            System.Windows.Controls.ToolTip tt = new System.Windows.Controls.ToolTip();
            
            tt.Content = _previousOverGeometry.ToolTip;
            _tooltipElement.ToolTip = tt;
            if (tt.Content != null)
                tt.IsOpen = true;

            if (_tooltipTimer != null)
            {
                _tooltipTimer.Stop();
                _tooltipTimer = null;
            }
        }

        private void Grid_MouseLeave(object sender, MouseEventArgs e)
        {
            if (_tooltipTimer != null)
            {
                _tooltipTimer.Stop();
                _tooltipTimer = null;
            }
            System.Windows.Controls.ToolTip tt = _tooltipElement.ToolTip as System.Windows.Controls.ToolTip;
            if (tt != null)
                tt.IsOpen = false;

            _tooltipElement.ToolTip = null;
            _previousOverGeometry = null;
        }
    }

    /// <summary>
    /// This takes a few size parameters and returns the translation that needs to be applied to the
    /// object to render it in the viewport in screen coordinates
    /// </summary>
    /// <values>
    /// [0] - Point - The position of the image (top-left corner) in world space
    /// [1] - Point - The center of the view port in world coordinates
    /// [2] - Size - The current render size of the viewport
    /// [3] - double - The zoom factor (world units per pixel)
    /// [4] - Size - The size of the image geometry we are rendering
    /// [4] - BitmapImage - The source image for this that we need to use to determine the scale factor
    /// </values>
    /// <parameters>
    /// If the parameter is X we are trying to get the translation in the x-axis
    /// If the parameter is Y we are trying to get the translation in the y-axis
    /// </parameters>
    public class PositionConverter : IMultiValueConverter
    {
        public Object Convert(Object[] values, Type targetType, Object parameter, System.Globalization.CultureInfo culture)
        {
            Point imagePosition;
            Point viewportCenter;
            Size viewportSize;
            float zoomFactor = 0.0f;
            Size geometrySize;
            BitmapSource image;
            try
            {
                imagePosition = (Point)values[0];
                viewportCenter = (Point)values[1];
                viewportSize = (Size)values[2];
                zoomFactor = (float)values[3];
                geometrySize = (Size)values[4];
                image = (BitmapSource)values[5];
            }
            catch
            {
                RSG.Base.Logging.Log.Log__Error("There has been an error in the scale multibinding");
                return 1.0;
            }


            // Get the top left corner of the viewport in world coordinates based on the zoom level
            double halfWorldUnitsWide = ((viewportSize.Width) * zoomFactor) / 2.0;
            double halfWorldUnitsHigh = ((viewportSize.Height) * zoomFactor) / 2.0;
            Point topLeft = new Point(viewportCenter.X - halfWorldUnitsWide, viewportCenter.Y + halfWorldUnitsHigh);

            double offset = 0;
            if (parameter as String == "X")
            {
                offset = (imagePosition.X - topLeft.X) / zoomFactor;
            }
            else
            {
                offset = (-(imagePosition.Y - topLeft.Y)) / zoomFactor;
            }

            return offset;
        }

        public Object[] ConvertBack(Object value, Type[] targetTypes, Object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
        
    } // PositionConverter

    /// <summary>
    /// Takes a zoom factor in the ration between world units and pixels rendered and turns it into a 
    /// scale factor that wpf transforms can work with
    /// </summary>
    /// <values>
    /// [0] - double - The zoom factor (world units per pixel)
    /// [1] - Point - The center of the view port in world coordinates
    /// [2] - Point - The center in world space of the zoom (The point in which to zoom into)
    /// [3] - Size - The current render size of the viewport
    /// [4] - Size - The size of the image geometry we are rendering
    /// [5] - BitmapImage - The source image for this that we need to use to determine the scale factor
    /// </values>
    public class ScaleConverter : IMultiValueConverter
    {
        public Object Convert(Object[] values, Type targetType, Object parameter, System.Globalization.CultureInfo culture)
        {
            float zoomFactor = 0.0f;
            Point viewportCenter;
            Size viewportSize;
            Size geometrySize;
            BitmapSource image;
            try
            {
                zoomFactor = (float)values[0];
                viewportCenter = (Point)values[1];
                viewportSize = (Size)values[2];
                geometrySize = (Size)values[3];
                image = (BitmapSource)values[4];
            }
            catch
            {
                RSG.Base.Logging.Log.Log__Error("There has been an error in the scale multibinding");
                return 1.0;
            }

            if (image != null)
            {
                if (parameter as String == "X")
                {
                    // Determine how width the image is based on the geometry size and the zoom factor
                    double newImageWidth = geometrySize.Width / zoomFactor;
                    // Determine the width scale factor based on the new and original image size
                    double widthScale = newImageWidth / image.Width;

                    return widthScale;
                }
                else
                {
                    // Determine how high the image is based on the geometry size and the zoom factor
                    double newImageHeight = geometrySize.Height / zoomFactor;
                    // Determine the height scale factor based on the new and original image size
                    double heightScale = newImageHeight / image.Height;

                    return heightScale;
                }
            }

            return 1.0;
        }

        public Object[] ConvertBack(Object value, Type[] targetTypes, Object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

    } // ScaleConverter

    /// <summary>
    /// This takes a few size parameters and returns the translation that needs to be applied to the
    /// object to render it in the viewport in screen coordinates
    /// </summary>
    /// <values>
    /// [0] - Point - The viewport look at position in world space
    /// [1] - Size - The current render size of the viewport
    /// [2] - double - The zoom factor (world units per pixel)
    /// [3] - Geometry - The geometry data for the path we are rendering
    /// </values>
    public class ShapePositionConverter : IMultiValueConverter
    {
        public Object Convert(Object[] values, Type targetType, Object parameter, System.Globalization.CultureInfo culture)
        {
            Vector2f[] points;
            Matrix33f transform;
            try
            {
                transform = (Matrix33f)values[0];
                points = (Vector2f[])values[1];
            }
            catch
            {
                RSG.Base.Logging.Log.Log__Error("There has been an error in the shape position multibinding");
                return new PointCollection();
            }

            PointCollection pointCollection = new PointCollection();
            for (int i = 0; i < points.Length; i++)
            {
                Vector3f vector = new Vector3f(points[i].X, points[i].Y, 1.0f);
                transform.Transform(vector);

                pointCollection.Add(new Point(vector.X, vector.Y));
            }
            return pointCollection;
        }

        public Object[] ConvertBack(Object value, Type[] targetTypes, Object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

    } // ShapePositionConverter

    /// <summary>
    /// This takes a few size parameters and returns the translation that needs to be applied to the
    /// object to render it in the viewport in screen coordinates. For the user text it need to
    /// be translated to the center of the geometry
    /// </summary>
    /// <values>
    /// [0] - Point - The viewport look at position in world space
    /// [1] - Size - The current render size of the viewport
    /// [2] - double - The zoom factor (world units per pixel)
    /// [3] - Geometry - The geometry data for the path we are rendering
    /// [4] - Size - The initial render size of the text box
    /// </values>
    /// <parameters>
    /// If the parameter is X we are trying to get the translation in the x-axis
    /// If the parameter is Y we are trying to get the translation in the y-axis
    /// </parameters>
    public class UserTextPositionConverter : IMultiValueConverter
    {
        public Object Convert(Object[] values, Type targetType, Object parameter, System.Globalization.CultureInfo culture)
        {
            float zoomFactor = 0.0f;
            Size textSize;
            PointCollection points;
            try
            {
                zoomFactor = (float)values[0];
                textSize = (Size)values[1];

                if (values[2] is Point)
                {
                    points = new PointCollection(new Point[] { (Point)values[2] });
                }
                else if (values[2] is Vector2f)
                {
                    Vector2f vec = (Vector2f)values[2];
                    points = new PointCollection(new Point[] { new Point(vec.X, vec.Y) });
                }
                else
                {
                    points = (PointCollection)values[2];
                }
            }
            catch
            {
                RSG.Base.Logging.Log.Log__Error("There has been an error in the scale multibinding");
                return 1.0;
            }

            Point centerPoint = new Point();
            foreach (Point point in points)
            {
                centerPoint.X += point.X;
                centerPoint.Y += point.Y;
            }
            centerPoint.X /= points.Count;
            centerPoint.Y /= points.Count;

            //List<Point> screenPoints = new List<Point>();

            //Point startPoint = geometry.Figures[0].StartPoint;
            //screenPoints.Add(geometry.Figures[0].StartPoint);

            //for (int i = 0; i < geometry.Figures[0].Segments.Count; i++)
            //{
            //    screenPoints.Add((geometry.Figures[0].Segments[i] as LineSegment).Point);
            //}

            //Point centerPoint = new Point();
            //Vector2f test = new Vector2f();
            //foreach (Point point in screenPoints)
            //{
            //    centerPoint.X += point.X;
            //    centerPoint.Y += point.Y;
            //}
            //centerPoint.X /= screenPoints.Count;
            //centerPoint.Y /= screenPoints.Count;

            double zoomDifference = zoomFactor - 1.0;
            zoomDifference = zoomDifference < 1.0 ? 1.0 : zoomDifference;

            zoomDifference = System.Math.Pow(zoomDifference, 0.5);
            double scaleFactor = 1.0 / zoomDifference;

            double offset = 0;
            if (parameter as String == "X")
            {
                offset = centerPoint.X - ((textSize.Width * scaleFactor) * 0.5f);
            }
            else
            {
                offset = centerPoint.Y - ((textSize.Height * scaleFactor) * 0.5f);
            }

            return offset;
        }

        public Object[] ConvertBack(Object value, Type[] targetTypes, Object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

    } // UserTextPositionConverter

    /// <summary>
    /// This takes a few size parameters and returns the translation that needs to be applied to the
    /// object to render it in the viewport in screen coordinates. For the user text it need to
    /// be translated to the center of the geometry
    /// </summary>
    /// <values>
    /// [0] - Point - The viewport look at position in world space
    /// [1] - Size - The current render size of the viewport
    /// [2] - double - The zoom factor (world units per pixel)
    /// [3] - Geometry - The geometry data for the path we are rendering
    /// [4] - Size - The initial render size of the text box
    /// </values>
    public class UserTextScaleConverter : IMultiValueConverter
    {
        public Object Convert(Object[] values, Type targetType, Object parameter, System.Globalization.CultureInfo culture)
        {
            float zoomFactor = 0.0f;
            try
            {
                zoomFactor = (float)values[0];
            }
            catch
            {
                RSG.Base.Logging.Log.Log__Error("There has been an error in the scale multibinding");
                return 1.0;
            }

            double zoomDifference = zoomFactor - 1.0;
            zoomDifference = zoomDifference < 1.0 ? 1.0 : zoomDifference;

            zoomDifference = System.Math.Pow(zoomDifference, 0.5);
            double scaleFactor = 1.0 / zoomDifference;

            return scaleFactor;
        }

        public Object[] ConvertBack(Object value, Type[] targetTypes, Object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

    } // ScaleConverter

    /// <summary>
    /// This takes a few size parameters and returns the scale factor that needs to be applied to a
    /// circle to render it onto the viewport.
    /// </summary>
    /// <values>
    /// [2] - float - The zoom factor (world units per pixel)
    /// [4] - float - The radius of the circle we wish to scale
    /// </values>
    public class RadiusScaleConverter : IMultiValueConverter
    {
        public Object Convert(Object[] values, Type targetType, Object parameter, System.Globalization.CultureInfo culture)
        {
            float zoomFactor = 0.0f;
            float radius = 0.0f;
            try
            {
                zoomFactor = (float)values[0];
                radius = (float)values[1];
            }
            catch
            {
                RSG.Base.Logging.Log.Log__Error("There has been an error in the scale multibinding");
                return 1.0;
            }

            double scale = radius / (zoomFactor * radius);
            return scale;
        }

        public Object[] ConvertBack(Object value, Type[] targetTypes, Object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    } // RadiusScaleConverter

        /// <summary>
    /// This takes a few size parameters and returns the translation that needs to be applied to the
    /// object to render it in the viewport in screen coordinates
    /// </summary>
    /// <values>
    /// [0] - Vector2f - The center of the circle
    /// [1] - Point - The center of the view port in world coordinates
    /// [2] - Size - The current render size of the viewport
    /// [3] - float - The zoom factor (world units per pixel)
    /// </values>
    /// <parameters>
    /// If the parameter is X we are trying to get the translation in the x-axis
    /// If the parameter is Y we are trying to get the translation in the y-axis
    /// </parameters>
    public class CirclePositionConverter : IMultiValueConverter
    {
        public Object Convert(Object[] values, Type targetType, Object parameter, System.Globalization.CultureInfo culture)
        {
            Vector2f circleCenter;
            float circleWidth = 0.0f;
            Point viewportCenter;
            Size viewportSize;
            float zoomFactor = 0.0f;
            bool offsetToTopLeft = true;
            try
            {
                circleCenter = (Vector2f)values[0];
                circleWidth = (float)values[1];
                viewportCenter = (Point)values[2];
                viewportSize = (Size)values[3];
                zoomFactor = (float)values[4];

                if (values.Length > 5)
                {
                    offsetToTopLeft = Boolean.Parse((string)values[5]);
                }
            }
            catch
            {
                RSG.Base.Logging.Log.Log__Error("There has been an error in the position multibinding");
                return 1.0;
            }


            // Get the top left corner of the viewport in world coordinates based on the zoom level
            double halfWorldUnitsWide = ((viewportSize.Width) * zoomFactor) / 2.0;
            double halfWorldUnitsHigh = ((viewportSize.Height) * zoomFactor) / 2.0;
            Point topLeft = new Point(viewportCenter.X - halfWorldUnitsWide, viewportCenter.Y + halfWorldUnitsHigh);

            double offset = 0;
            double scale =  (circleWidth / (zoomFactor * circleWidth));
            if (parameter as String == "X")
            {
                offset = (((circleCenter.X - topLeft.X) / zoomFactor));
                if (offsetToTopLeft)
                {
                    offset -= ((circleWidth * scale) * 0.5);
                }
            }
            else
            {
                offset = ((-(circleCenter.Y - topLeft.Y)) / zoomFactor); ;
                if (offsetToTopLeft)
                {
                    offset -= ((circleWidth * scale) * 0.5);
                }
            }

            return offset;
        }

        public Object[] ConvertBack(Object value, Type[] targetTypes, Object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
        
    } // CirclePositionConverter

    /// <summary>
    /// Boolean to Visibility converter.
    /// </summary>
    [ValueConversion(typeof(Color), typeof(Color))]
    public class ColorAlphaConverter : IValueConverter
    {
        #region IValueConverter Members
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object Convert(Object value, Type targetType, Object parameter, CultureInfo culture)
        {
            Color col = (Color)value;
            col.A = Byte.Parse(parameter as string);
            return col;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object ConvertBack(Object value, Type targetType, Object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
        #endregion // IValueConverter Members
    }
    
}
