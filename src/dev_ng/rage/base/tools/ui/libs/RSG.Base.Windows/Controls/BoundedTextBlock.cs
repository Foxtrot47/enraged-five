﻿using System;
using System.Windows;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows.Documents;

namespace RSG.Base.Windows.Controls
{
    /// <summary>
    /// A text block that includes an additional property runs which can have a collection
    /// of runs bounded to it.
    /// </summary>
    public class BoundedTextBlock : TextBlock
    {
        public ObservableCollection<Inline> Runs
        {
            get { return (ObservableCollection<Inline>)GetValue(RunsProperty); }
            set { SetValue(RunsProperty, value); }
        }
        public static readonly DependencyProperty RunsProperty =
            DependencyProperty.Register("Runs", typeof(ObservableCollection<Inline>), typeof(BoundedTextBlock),
            new PropertyMetadata(null, OnPropertyChanged));

        /// <summary>
        /// Get called when the runs property changes. When this happens need to construct the textblock inlines to match.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private static void OnPropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            BoundedTextBlock textBlock = (sender as BoundedTextBlock);
        }

        public BoundedTextBlock()
            : base()
        {
            this.Runs = new ObservableCollection<Inline>();
        }
    }
}
