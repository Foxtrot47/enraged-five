﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Base.Windows.Helpers
{
    /// <summary>
    /// Base class for item changed notifications
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class ItemChangedEventArgs<T> : EventArgs
    {
        public T NewItem { get; private set; }
        public T OldItem { get; private set; }

        public ItemChangedEventArgs(T oldItem, T newItem)
        {
            NewItem = newItem;
            OldItem = oldItem;
        }
    } // ItemChangedEventArgs<T>
}
