﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Globalization;
using System.Windows.Input;
using System.Windows.Data;
using System.Windows.Markup;

namespace RSG.Base.Windows.Controls
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class UpDownInput : Control
    {
        #region Properties

        #region CultureInfo

        public static readonly DependencyProperty CultureInfoProperty = DependencyProperty.Register("CultureInfo", typeof(CultureInfo), 
            typeof(UpDownInput), new UIPropertyMetadata(CultureInfo.CurrentCulture));
        public CultureInfo CultureInfo
        {
            get { return (CultureInfo)GetValue(CultureInfoProperty); }
            set { SetValue(CultureInfoProperty, value); }
        }

        #endregion //CultureInfo

        #region IsEditable

        public static readonly DependencyProperty IsEditableProperty = DependencyProperty.Register("IsEditable", typeof(bool), 
            typeof(UpDownInput), new PropertyMetadata(true));
        public bool IsEditable
        {
            get { return (bool)GetValue(IsEditableProperty); }
            set { SetValue(IsEditableProperty, value); }
        }

        #endregion //IsEditable

        #region Text

        public static readonly DependencyProperty TextProperty = DependencyProperty.Register("Text", typeof(String),
            typeof(UpDownInput), new FrameworkPropertyMetadata(default(string), OnTextChanged) { BindsTwoWayByDefault = true, DefaultUpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged });
        public string Text
        {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }

        private static void OnTextChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            UpDownInput input = o as UpDownInput;
            if (input != null)
                input.OnTextChanged((String)e.OldValue, (String)e.NewValue);
        }

        protected virtual void OnTextChanged(String oldValue, String newValue)
        {

        }

        #endregion //Text

        #region DisplayText

        public static readonly DependencyProperty DisplayTextProperty = DependencyProperty.Register("DisplayText", typeof(String),
            typeof(UpDownInput), new FrameworkPropertyMetadata(default(string), OnDisplayTextChanged) { BindsTwoWayByDefault = true, DefaultUpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged });
        public string DisplayText
        {
            get { return (string)GetValue(DisplayTextProperty); }
            set { SetValue(DisplayTextProperty, value); }
        }

        private static void OnDisplayTextChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            UpDownInput input = o as UpDownInput;
            if (input != null)
                input.OnDisplayTextChanged((String)e.OldValue, (String)e.NewValue);
        }

        protected virtual void OnDisplayTextChanged(String oldValue, String newValue)
        {

        }

        #endregion //Text

        #region TextAlignment

        public static readonly DependencyProperty TextAlignmentProperty = DependencyProperty.Register("TextAlignment", 
            typeof(TextAlignment), typeof(UpDownInput), new UIPropertyMetadata(TextAlignment.Left));
        public TextAlignment TextAlignment
        {
            get { return (TextAlignment)GetValue(TextAlignmentProperty); }
            set { SetValue(TextAlignmentProperty, value); }
        }

        #endregion //TextAlignment

        #endregion //Properties
    } // UpDownInput

    /// <summary>
    /// Represents spin directions that are valid.
    /// </summary>
    [Flags]
    public enum ValidSpinDirections
    {
        /// <summary>
        /// Can not increase nor decrease.
        /// </summary>
        None = 0,

        /// <summary>
        /// Can increase.
        /// </summary>
        Increase = 1,

        /// <summary>
        /// Can decrease.
        /// </summary>
        Decrease = 2
    } // ValidSpinDirections

    /// <summary>
    /// Represents spin directions that could be initiated by the end-user.
    /// </summary>
    public enum SpinDirection
    {
        /// <summary>
        /// Represents a spin initiated by the end-user in order to Increase a value.
        /// </summary>
        Increase = 0,

        /// <summary>
        /// Represents a spin initiated by the end-user in order to Decrease a value.
        /// </summary>
        Decrease = 1
    } // SpinDirection

    /// <summary>
    /// Provides data for the Spinner.Spin event.
    /// </summary>
    public class SpinEventArgs : RoutedEventArgs
    {
        /// <summary>
        /// Gets the SpinDirection for the spin that has been initiated by the 
        /// end-user.
        /// </summary>
        public SpinDirection Direction { get; private set; }

        /// <summary>
        /// Initializes a new instance of the SpinEventArgs class.
        /// </summary>
        /// <param name="direction">Spin direction.</param>
        public SpinEventArgs(SpinDirection direction)
            : base()
        {
            Direction = direction;
        }
    } // SpinEventArgs

    /// <summary>
    /// Base class for controls that represents controls that can spin.
    /// </summary>
    public abstract class Spinner : Control
    {
        #region Properties

        /// <summary>
        /// Identifies the ValidSpinDirection dependency property.
        /// </summary>
        public static readonly DependencyProperty ValidSpinDirectionProperty = DependencyProperty.Register("ValidSpinDirection", 
            typeof(ValidSpinDirections), typeof(Spinner), new PropertyMetadata(ValidSpinDirections.Increase | ValidSpinDirections.Decrease, OnValidSpinDirectionPropertyChanged));
        public ValidSpinDirections ValidSpinDirection
        {
            get { return (ValidSpinDirections)GetValue(ValidSpinDirectionProperty); }
            set { SetValue(ValidSpinDirectionProperty, value); }
        }

        /// <summary>
        /// ValidSpinDirectionProperty property changed handler.
        /// </summary>
        /// <param name="d">ButtonSpinner that changed its ValidSpinDirection.</param>
        /// <param name="e">Event arguments.</param>
        private static void OnValidSpinDirectionPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            Spinner source = (Spinner)d;
            ValidSpinDirections oldvalue = (ValidSpinDirections)e.OldValue;
            ValidSpinDirections newvalue = (ValidSpinDirections)e.NewValue;
            source.OnValidSpinDirectionChanged(oldvalue, newvalue);
        }

        #endregion //Properties

        #region Public Methods

        /// <summary>
        /// Occurs when spinning is initiated by the end-user.
        /// </summary>
        public event EventHandler<SpinEventArgs> Spin;

        #endregion // Public Methods

        #region Protected Methods

        /// <summary>
        /// Initializes a new instance of the Spinner class.
        /// </summary>
        protected Spinner() { }

        /// <summary>
        /// Raises the OnSpin event when spinning is initiated by the end-user.
        /// </summary>
        /// <param name="e">Spin event args.</param>
        protected virtual void OnSpin(SpinEventArgs e)
        {
            ValidSpinDirections valid = e.Direction == SpinDirection.Increase ? ValidSpinDirections.Increase : ValidSpinDirections.Decrease;

            //Only raise the event if spin is allowed.
            if ((ValidSpinDirection & valid) == valid)
            {
                EventHandler<SpinEventArgs> handler = Spin;
                if (handler != null)
                {
                    handler(this, e);
                }
            }
        }

        /// <summary>
        /// Called when valid spin direction changed.
        /// </summary>
        /// <param name="oldValue">The old value.</param>
        /// <param name="newValue">The new value.</param>
        protected virtual void OnValidSpinDirectionChanged(ValidSpinDirections oldValue, ValidSpinDirections newValue) { }

        #endregion // Protected Methods
    } // Spinner

    /// <summary>
    /// Represents a spinner control that includes two Buttons.
    /// </summary>
    [ContentProperty("Content")]
    public class ButtonSpinner : Spinner
    {
        #region Properties

        #region AllowSpin

        public static readonly DependencyProperty AllowSpinProperty = DependencyProperty.Register("AllowSpin", typeof(bool), 
            typeof(ButtonSpinner), new UIPropertyMetadata(true));
        public bool AllowSpin
        {
            get { return (bool)GetValue(AllowSpinProperty); }
            set { SetValue(AllowSpinProperty, value); }
        }

        #endregion //AllowSpin

        #region Content

        /// <summary>
        /// Identifies the Content dependency property.
        /// </summary>
        public static readonly DependencyProperty ContentProperty = DependencyProperty.Register("Content", typeof(object), 
            typeof(ButtonSpinner), new PropertyMetadata(null, OnContentPropertyChanged));
        public object Content
        {
            get { return GetValue(ContentProperty) as object; }
            set { SetValue(ContentProperty, value); }
        }

        /// <summary>
        /// ContentProperty property changed handler.
        /// </summary>
        /// <param name="d">ButtonSpinner that changed its Content.</param>
        /// <param name="e">Event arguments.</param>
        private static void OnContentPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ButtonSpinner source = d as ButtonSpinner;
            source.OnContentChanged(e.OldValue, e.NewValue);
        }

        #endregion //Content

        #region DecreaseButton

        private ButtonBase _decreaseButton;
        /// <summary>
        /// Gets or sets the DecreaseButton template part.
        /// </summary>
        private ButtonBase DecreaseButton
        {
            get { return _decreaseButton; }
            set
            {
                if (_decreaseButton != null)
                {
                    _decreaseButton.Click -= OnButtonClick;
                }

                _decreaseButton = value;

                if (_decreaseButton != null)
                {
                    _decreaseButton.Click += OnButtonClick;
                }
            }
        }

        #endregion //DecreaseButton

        #region IncreaseButton

        private ButtonBase _increaseButton;
        /// <summary>
        /// Gets or sets the IncreaseButton template part.
        /// </summary>
        private ButtonBase IncreaseButton
        {
            get { return _increaseButton; }
            set
            {
                if (_increaseButton != null)
                {
                    _increaseButton.Click -= OnButtonClick;
                }

                _increaseButton = value;

                if (_increaseButton != null)
                {
                    _increaseButton.Click += OnButtonClick;
                }
            }
        }

        #endregion //IncreaseButton

        #region ShowButtonSpinner

        public static readonly DependencyProperty ShowButtonSpinnerProperty = DependencyProperty.Register("ShowButtonSpinner", 
            typeof(bool), typeof(ButtonSpinner), new UIPropertyMetadata(true));
        public bool ShowButtonSpinner
        {
            get { return (bool)GetValue(ShowButtonSpinnerProperty); }
            set { SetValue(ShowButtonSpinnerProperty, value); }
        }

        #endregion //ShowButtonSpinner

        #endregion //Properties

        #region Constructors

        static ButtonSpinner()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(ButtonSpinner), new FrameworkPropertyMetadata(typeof(ButtonSpinner)));
        }

        #endregion //Constructors

        #region Base Class Overrides

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            IncreaseButton = GetTemplateChild("IncreaseButton") as ButtonBase;
            DecreaseButton = GetTemplateChild("DecreaseButton") as ButtonBase;

            SetButtonUsage();
        }

        /// <summary>
        /// Cancel LeftMouseButtonUp events originating from a button that has
        /// been changed to disabled.
        /// </summary>
        /// <param name="e">The data for the event.</param>
        protected override void OnMouseLeftButtonUp(MouseButtonEventArgs e)
        {
            base.OnMouseLeftButtonUp(e);

            Point mousePosition;
            if (IncreaseButton != null && IncreaseButton.IsEnabled == false)
            {
                mousePosition = e.GetPosition(IncreaseButton);
                if (mousePosition.X > 0 && mousePosition.X < IncreaseButton.ActualWidth &&
                    mousePosition.Y > 0 && mousePosition.Y < IncreaseButton.ActualHeight)
                {
                    e.Handled = true;
                }
            }

            if (DecreaseButton != null && DecreaseButton.IsEnabled == false)
            {
                mousePosition = e.GetPosition(DecreaseButton);
                if (mousePosition.X > 0 && mousePosition.X < DecreaseButton.ActualWidth &&
                    mousePosition.Y > 0 && mousePosition.Y < DecreaseButton.ActualHeight)
                {
                    e.Handled = true;
                }
            }
        }

        /// <summary>
        /// Called when valid spin direction changed.
        /// </summary>
        /// <param name="oldValue">The old value.</param>
        /// <param name="newValue">The new value.</param>
        protected override void OnValidSpinDirectionChanged(ValidSpinDirections oldValue, ValidSpinDirections newValue)
        {
            SetButtonUsage();
        }

        #endregion //Base Class Overrides

        #region Event Handlers

        /// <summary>
        /// Handle click event of IncreaseButton and DecreaseButton template parts,
        /// translating Click to appropriate Spin event.
        /// </summary>
        /// <param name="sender">Event sender, should be either IncreaseButton or DecreaseButton template part.</param>
        /// <param name="e">Event args.</param>
        private void OnButtonClick(object sender, RoutedEventArgs e)
        {
            if (AllowSpin)
            {
                SpinDirection direction = sender == IncreaseButton ? SpinDirection.Increase : SpinDirection.Decrease;
                OnSpin(new SpinEventArgs(direction));
            }
        }

        #endregion //Event Handlers

        #region Methods

        /// <summary>
        /// Occurs when the Content property value changed.
        /// </summary>
        /// <param name="oldValue">The old value of the Content property.</param>
        /// <param name="newValue">The new value of the Content property.</param>
        protected virtual void OnContentChanged(object oldValue, object newValue) { }

        /// <summary>
        /// Disables or enables the buttons based on the valid spin direction.
        /// </summary>
        private void SetButtonUsage()
        {
            // buttonspinner adds buttons that spin, so disable accordingly.
            if (IncreaseButton != null)
            {
                IncreaseButton.IsEnabled = ((ValidSpinDirection & ValidSpinDirections.Increase) == ValidSpinDirections.Increase);
            }

            if (DecreaseButton != null)
            {
                DecreaseButton.IsEnabled = ((ValidSpinDirection & ValidSpinDirections.Decrease) == ValidSpinDirections.Decrease);
            }
        }

        #endregion //Methods
    }

    /// <summary>
    /// 
    /// </summary>
    public class UpDownButton : Control
    {
        #region CornerRadius

        public static readonly DependencyProperty CornerRadiusProperty = DependencyProperty.Register("CornerRadius", typeof(CornerRadius), typeof(UpDownButton), new UIPropertyMetadata(default(CornerRadius), new PropertyChangedCallback(OnCornerRadiusChanged)));
        public CornerRadius CornerRadius
        {
            get { return (CornerRadius)GetValue(CornerRadiusProperty); }
            set { SetValue(CornerRadiusProperty, value); }
        }

        private static void OnCornerRadiusChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            UpDownButton buttonChrome = o as UpDownButton;
            if (buttonChrome != null)
                buttonChrome.OnCornerRadiusChanged((CornerRadius)e.OldValue, (CornerRadius)e.NewValue);
        }

        protected virtual void OnCornerRadiusChanged(CornerRadius oldValue, CornerRadius newValue)
        {
            //we always want the InnerBorderRadius to be one less than the CornerRadius
            CornerRadius newInnerCornerRadius = new CornerRadius(System.Math.Max(0, newValue.TopLeft - 1),
                                                                 System.Math.Max(0, newValue.TopRight - 1),
                                                                 System.Math.Max(0, newValue.BottomRight - 1),
                                                                 System.Math.Max(0, newValue.BottomLeft - 1));

            InnerCornerRadius = newInnerCornerRadius;
        }

        #endregion //CornerRadius

        #region InnerCornerRadius

        public static readonly DependencyProperty InnerCornerRadiusProperty = DependencyProperty.Register("InnerCornerRadius", 
            typeof(CornerRadius), typeof(UpDownButton), new UIPropertyMetadata(default(CornerRadius), new PropertyChangedCallback(OnInnerCornerRadiusChanged)));
        public CornerRadius InnerCornerRadius
        {
            get { return (CornerRadius)GetValue(InnerCornerRadiusProperty); }
            set { SetValue(InnerCornerRadiusProperty, value); }
        }

        private static void OnInnerCornerRadiusChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            UpDownButton buttonChrome = o as UpDownButton;
            if (buttonChrome != null)
                buttonChrome.OnInnerCornerRadiusChanged((CornerRadius)e.OldValue, (CornerRadius)e.NewValue);
        }

        protected virtual void OnInnerCornerRadiusChanged(CornerRadius oldValue, CornerRadius newValue)
        {
        }

        #endregion //InnerCornerRadius

        #region RenderChecked

        public static readonly DependencyProperty RenderCheckedProperty = DependencyProperty.Register("RenderChecked", 
            typeof(bool), typeof(UpDownButton), new UIPropertyMetadata(false, OnRenderCheckedChanged));
        public bool RenderChecked
        {
            get { return (bool)GetValue(RenderCheckedProperty); }
            set { SetValue(RenderCheckedProperty, value); }
        }

        private static void OnRenderCheckedChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            UpDownButton buttonChrome = o as UpDownButton;
            if (buttonChrome != null)
                buttonChrome.OnRenderCheckedChanged((bool)e.OldValue, (bool)e.NewValue);
        }

        protected virtual void OnRenderCheckedChanged(bool oldValue, bool newValue)
        {
        }

        #endregion //RenderChecked

        #region RenderEnabled

        public static readonly DependencyProperty RenderEnabledProperty = DependencyProperty.Register("RenderEnabled", 
            typeof(bool), typeof(UpDownButton), new UIPropertyMetadata(true, OnRenderEnabledChanged));
        public bool RenderEnabled
        {
            get { return (bool)GetValue(RenderEnabledProperty); }
            set { SetValue(RenderEnabledProperty, value); }
        }

        private static void OnRenderEnabledChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            UpDownButton buttonChrome = o as UpDownButton;
            if (buttonChrome != null)
                buttonChrome.OnRenderEnabledChanged((bool)e.OldValue, (bool)e.NewValue);
        }

        protected virtual void OnRenderEnabledChanged(bool oldValue, bool newValue)
        {
        }

        #endregion //RenderEnabled

        #region RenderFocused

        public static readonly DependencyProperty RenderFocusedProperty = DependencyProperty.Register("RenderFocused", typeof(bool), 
            typeof(UpDownButton), new UIPropertyMetadata(false, OnRenderFocusedChanged));
        public bool RenderFocused
        {
            get { return (bool)GetValue(RenderFocusedProperty); }
            set { SetValue(RenderFocusedProperty, value); }
        }

        private static void OnRenderFocusedChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            UpDownButton buttonChrome = o as UpDownButton;
            if (buttonChrome != null)
                buttonChrome.OnRenderFocusedChanged((bool)e.OldValue, (bool)e.NewValue);
        }

        protected virtual void OnRenderFocusedChanged(bool oldValue, bool newValue)
        {
        }

        #endregion //RenderFocused

        #region RenderMouseOver

        public static readonly DependencyProperty RenderMouseOverProperty = DependencyProperty.Register("RenderMouseOver", 
            typeof(bool), typeof(UpDownButton), new UIPropertyMetadata(false, OnRenderMouseOverChanged));
        public bool RenderMouseOver
        {
            get { return (bool)GetValue(RenderMouseOverProperty); }
            set { SetValue(RenderMouseOverProperty, value); }
        }

        private static void OnRenderMouseOverChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            UpDownButton buttonChrome = o as UpDownButton;
            if (buttonChrome != null)
                buttonChrome.OnRenderMouseOverChanged((bool)e.OldValue, (bool)e.NewValue);
        }

        protected virtual void OnRenderMouseOverChanged(bool oldValue, bool newValue)
        {
        }

        #endregion //RenderMouseOver

        #region RenderNormal

        public static readonly DependencyProperty RenderNormalProperty = DependencyProperty.Register("RenderNormal", 
            typeof(bool), typeof(UpDownButton), new UIPropertyMetadata(true, OnRenderNormalChanged));
        public bool RenderNormal
        {
            get { return (bool)GetValue(RenderNormalProperty); }
            set { SetValue(RenderNormalProperty, value); }
        }

        private static void OnRenderNormalChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            UpDownButton buttonChrome = o as UpDownButton;
            if (buttonChrome != null)
                buttonChrome.OnRenderNormalChanged((bool)e.OldValue, (bool)e.NewValue);
        }

        protected virtual void OnRenderNormalChanged(bool oldValue, bool newValue)
        {
            // TODO: Add your property changed side-effects. Descendants can override as well.
        }

        #endregion //RenderNormal

        #region RenderPressed

        public static readonly DependencyProperty RenderPressedProperty = DependencyProperty.Register("RenderPressed", typeof(bool), typeof(UpDownButton), new UIPropertyMetadata(false, OnRenderPressedChanged));
        public bool RenderPressed
        {
            get { return (bool)GetValue(RenderPressedProperty); }
            set { SetValue(RenderPressedProperty, value); }
        }

        private static void OnRenderPressedChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            UpDownButton buttonChrome = o as UpDownButton;
            if (buttonChrome != null)
                buttonChrome.OnRenderPressedChanged((bool)e.OldValue, (bool)e.NewValue);
        }

        protected virtual void OnRenderPressedChanged(bool oldValue, bool newValue)
        {
        }

        #endregion //RenderPressed

        #region Contsructors

        static UpDownButton()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(UpDownButton), new FrameworkPropertyMetadata(typeof(UpDownButton)));
        }

        #endregion //Contsructors
    }

    /// <summary>
    /// Base class for the up down controls
    /// </summary>
    public abstract class UpDownBase<T> : UpDownInput
    {
        #region Contants

        /// <summary>
        /// Name constant for Text template part.
        /// </summary>
        internal const string ElementTextName = "TextBox";

        /// <summary>
        /// Name constant for Spinner template part.
        /// </summary>
        internal const string ElementSpinnerName = "Spinner";

        #endregion // Contants

        #region Helper Properties

        /// <summary>
        /// Flags if the Text and Value properties are in the process of being sync'd
        /// </summary>
        private bool _isSyncingTextAndValueProperties;

        #endregion // Helper Propertie

        #region Properties

        protected Spinner Spinner { get; private set; }
        protected TextBox TextBox { get; private set; }

        #region AllowSpin

        public static readonly DependencyProperty AllowSpinProperty = DependencyProperty.Register("AllowSpin", typeof(bool), typeof(UpDownBase<T>), new UIPropertyMetadata(true));
        public bool AllowSpin
        {
            get { return (bool)GetValue(AllowSpinProperty); }
            set { SetValue(AllowSpinProperty, value); }
        }

        #endregion //AllowSpin

        #region ShowButtonSpinner

        public static readonly DependencyProperty ShowButtonSpinnerProperty = DependencyProperty.Register("ShowButtonSpinner", 
            typeof(bool), typeof(UpDownBase<T>), new UIPropertyMetadata(true));
        public bool ShowButtonSpinner
        {
            get { return (bool)GetValue(ShowButtonSpinnerProperty); }
            set { SetValue(ShowButtonSpinnerProperty, value); }
        }

        #endregion //ShowButtonSpinner

        #region Value

        public static readonly DependencyProperty ValueProperty = DependencyProperty.Register("Value", typeof(T), 
            typeof(UpDownBase<T>), new FrameworkPropertyMetadata(default(T), FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, OnValueChanged));
        public T Value
        {
            get { return (T)GetValue(ValueProperty); }
            set { SetValue(ValueProperty, value); }
        }

        private static void OnValueChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            UpDownBase<T> upDownBase = o as UpDownBase<T>;
            if (upDownBase != null)
                upDownBase.OnValueChanged((T)e.OldValue, (T)e.NewValue);
        }

        protected virtual void OnValueChanged(T oldValue, T newValue)
        {
            ValidateValue(newValue);

            SyncTextAndValueProperties(ValueProperty, string.Empty);

            SetValidSpinDirection();

            RoutedPropertyChangedEventArgs<object> args = new RoutedPropertyChangedEventArgs<object>(oldValue, newValue);
            args.RoutedEvent = ValueChangedEvent;
            RaiseEvent(args);
        }

        #endregion //Value


        #region ContainsError

        /// <summary>
        /// 
        /// </summary>
        public static readonly DependencyProperty ContainsErrorProperty = DependencyProperty.Register("ContainsError",
            typeof(bool), typeof(UpDownBase<T>), new PropertyMetadata(false));
        public bool ContainsError
        {
            get { return (bool)GetValue(ContainsErrorProperty); }
            set { SetValue(ContainsErrorProperty, value); }
        }

        #endregion // DecimalPlaces

        #endregion //Properties

        #region Constructors

        /// <summary>
        /// Default constructor
        /// </summary>
        internal UpDownBase() { }

        #endregion //Constructors

        #region Base Class Overrides

        protected override void OnAccessKey(AccessKeyEventArgs e)
        {
            if (TextBox != null)
                TextBox.Focus();

            base.OnAccessKey(e);
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            TextBox = GetTemplateChild(ElementTextName) as TextBox;
            Spinner = GetTemplateChild(ElementSpinnerName) as Spinner;
            if (Spinner == null)
            {
                return;
            }

            Spinner.Spin += OnSpinnerSpin;
            SetValidSpinDirection();
        }

        protected override void OnGotFocus(RoutedEventArgs e)
        {
            if (TextBox != null)
                TextBox.Focus();
        }

        protected override void OnPreviewKeyDown(KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Up:
                    {
                        if (AllowSpin)
                            DoIncrement();
                        e.Handled = true;
                        break;
                    }
                case Key.Down:
                    {
                        if (AllowSpin)
                            DoDecrement();
                        e.Handled = true;
                        break;
                    }
                case Key.Enter:
                    {
                        if (IsEditable)
                        {
                            var binding = BindingOperations.GetBindingExpression(TextBox, System.Windows.Controls.TextBox.TextProperty);
                            binding.UpdateSource();
                        }
                        break;
                    }
            }
        }

        protected override void OnMouseWheel(MouseWheelEventArgs e)
        {
            base.OnMouseWheel(e);

            if (!e.Handled && AllowSpin && TextBox.IsFocused)
            {
                if (e.Delta < 0)
                {
                    DoDecrement();
                }
                else if (0 < e.Delta)
                {
                    DoIncrement();
                }

                e.Handled = true;
            }
        }

        //protected override void OnTextChanged(String oldValue, String newValue)
        //{
        //    SyncTextAndValueProperties(UpDownInput.TextProperty, newValue);
        //}

        protected override void OnDisplayTextChanged(string oldValue, string newValue)
        {
            SyncTextAndValueProperties(UpDownInput.DisplayTextProperty, newValue);
        }
        #endregion //Base Class Overrides

        #region Event Handlers

        /// <summary>
        /// Processes the spin event if the up down control allows it
        /// </summary>
        private void OnSpinnerSpin(Object sender, SpinEventArgs e)
        {
            if (AllowSpin)
                OnSpin(e);
        }

        #endregion //Event Handlers

        #region Events

        /// <summary>
        /// 
        /// </summary>
        /// <remarks>Due to a bug, you cannot create event handlers for generic T args in XAML, so use Object instead.</remarks>
        public static readonly RoutedEvent ValueChangedEvent = EventManager.RegisterRoutedEvent("ValueChanged", RoutingStrategy.Bubble, 
            typeof(RoutedPropertyChangedEventHandler<Object>), typeof(UpDownBase<T>));
        public event RoutedPropertyChangedEventHandler<Object> ValueChanged
        {
            add { AddHandler(ValueChangedEvent, value); }
            remove { RemoveHandler(ValueChangedEvent, value); }
        }

        #endregion //Events

        #region Methods

        /// <summary>
        /// Called when the spinner event gets fired
        /// </summary>
        protected virtual void OnSpin(SpinEventArgs e)
        {
            if (e == null)
                throw new ArgumentNullException("e");

            if (e.Direction == SpinDirection.Increase)
                DoIncrement();
            else
                DoDecrement();
        }

        /// <summary>
        /// Performs an increment if conditions allow it.
        /// </summary>
        private void DoDecrement()
        {
            if (Spinner == null || (Spinner.ValidSpinDirection & ValidSpinDirections.Decrease) == ValidSpinDirections.Decrease)
            {
                OnDecrement();
            }
        }

        /// <summary>
        /// Performs a decrement if conditions allow it.
        /// </summary>
        private void DoIncrement()
        {
            if (Spinner == null || (Spinner.ValidSpinDirection & ValidSpinDirections.Increase) == ValidSpinDirections.Increase)
            {
                OnIncrement();
            }
        }

        protected void SyncTextAndValueProperties(DependencyProperty p, string text)
        {
            //prevents recursive syncing properties
            if (_isSyncingTextAndValueProperties)
                return;

            _isSyncingTextAndValueProperties = true;

            //this only occures when the user typed in the value
            if (UpDownInput.DisplayTextProperty == p)
            {
                Value = ConvertTextToValue(text);
            }
            else if (ValueProperty == p)
            {
                DisplayText = ConvertValueToText();
                this.ContainsError = false;
            }

            Text = ConvertValueToText();
            if (TextBox != null)
                TextBox.InvalidateProperty(TextBox.TextProperty);
            _isSyncingTextAndValueProperties = false;
        }

        #region Abstract

        /// <summary>
        /// Coerces the value.
        /// </summary>
        protected abstract T CoerceValue(T value);

        /// <summary>
        /// Converts the formatted text to a value.
        /// </summary>
        protected abstract T ConvertTextToValue(string text);

        /// <summary>
        /// Converts the value to formatted text.
        /// </summary>
        /// <returns></returns>
        protected abstract string ConvertValueToText();

        /// <summary>
        /// Called by OnSpin when the spin direction is SpinDirection.Increase.
        /// </summary>
        protected abstract void OnIncrement();

        /// <summary>
        /// Called by OnSpin when the spin direction is SpinDirection.Descrease.
        /// </summary>
        protected abstract void OnDecrement();

        /// <summary>
        /// Sets the valid spin directions.
        /// </summary>
        protected abstract void SetValidSpinDirection();

        /// <summary>
        /// Validates the value and keeps it between the Min and Max values.
        /// </summary>
        /// <param name="value">The value.</param>
        protected abstract void ValidateValue(T value);

        #endregion //Abstract

        #endregion //Methods

    } // UpDownBase<T>

    /// <summary>
    /// Base class to any UpDown Control that uses a numeric
    /// type
    /// </summary>
    public abstract class NumericUpDownBase<T> : UpDownBase<T>
    {
        #region Properties

        #region DefaultValue

        public static readonly DependencyProperty DefaultValueProperty = DependencyProperty.Register("DefaultValue", typeof(T),
            typeof(NumericUpDownBase<T>), new UIPropertyMetadata(default(T)));
        public T DefaultValue
        {
            get { return (T)GetValue(DefaultValueProperty); }
            set { SetValue(DefaultValueProperty, value); }
        }

        #endregion //DefaultValue

        #region FormatString

        public static readonly DependencyProperty FormatStringProperty = DependencyProperty.Register("FormatString", typeof(String),
            typeof(NumericUpDownBase<T>), new UIPropertyMetadata(String.Empty, OnFormatStringChanged));
        public string FormatString
        {
            get { return (string)GetValue(FormatStringProperty); }
            set { SetValue(FormatStringProperty, value); }
        }

        private static void OnFormatStringChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            NumericUpDownBase<T> numericUpDown = o as NumericUpDownBase<T>;
            if (numericUpDown != null)
                numericUpDown.OnFormatStringChanged((string)e.OldValue, (string)e.NewValue);
        }

        protected virtual void OnFormatStringChanged(String oldValue, String newValue)
        {
            Text = ConvertValueToText();
        }

        #endregion //FormatString

        #region Increment

        public static readonly DependencyProperty IncrementProperty = DependencyProperty.Register("Increment", typeof(T),
            typeof(NumericUpDownBase<T>), new PropertyMetadata(default(T)));
        public T Increment
        {
            get { return (T)GetValue(IncrementProperty); }
            set { SetValue(IncrementProperty, value); }
        }

        #endregion

        #region Maximum

        public static readonly DependencyProperty MaximumProperty = DependencyProperty.Register("Maximum", typeof(T),
            typeof(NumericUpDownBase<T>), new UIPropertyMetadata(default(T), OnMaximumChanged));
        public T Maximum
        {
            get { return (T)GetValue(MaximumProperty); }
            set { SetValue(MaximumProperty, value); }
        }

        private static void OnMaximumChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            NumericUpDownBase<T> numericUpDown = o as NumericUpDownBase<T>;
            if (numericUpDown != null)
                numericUpDown.OnMaximumChanged((T)e.OldValue, (T)e.NewValue);
        }

        protected virtual void OnMaximumChanged(T oldValue, T newValue)
        {
            SetValidSpinDirection();
        }

        #endregion //Maximum

        #region Minimum

        public static readonly DependencyProperty MinimumProperty = DependencyProperty.Register("Minimum", typeof(T),
            typeof(NumericUpDownBase<T>), new UIPropertyMetadata(default(T), OnMinimumChanged));
        public T Minimum
        {
            get { return (T)GetValue(MinimumProperty); }
            set { SetValue(MinimumProperty, value); }
        }

        private static void OnMinimumChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            NumericUpDownBase<T> numericUpDown = o as NumericUpDownBase<T>;
            if (numericUpDown != null)
                numericUpDown.OnMinimumChanged((T)e.OldValue, (T)e.NewValue);
        }

        protected virtual void OnMinimumChanged(T oldValue, T newValue)
        {
            SetValidSpinDirection();
        }

        #endregion //Minimum

        #region SelectAllOnGotFocus

        public static readonly DependencyProperty SelectAllOnGotFocusProperty = DependencyProperty.Register("SelectAllOnGotFocus",
            typeof(bool), typeof(NumericUpDownBase<T>), new PropertyMetadata(false));
        public bool SelectAllOnGotFocus
        {
            get { return (bool)GetValue(SelectAllOnGotFocusProperty); }
            set { SetValue(SelectAllOnGotFocusProperty, value); }
        }

        #endregion //SelectAllOnGotFocus

        #region DecimalPlaces

        /// <summary>
        /// A value of minus 1 means that there are no really decimal point provider and the numeric value should get
        /// outputted however is the default.
        /// </summary>
        public static readonly DependencyProperty DecimalPlacesProperty = DependencyProperty.Register("DecimalPlaces",
            typeof(int?), typeof(NumericUpDownBase<T>), new PropertyMetadata(null, OnDecimalPlacesChanged));
        public int? DecimalPlaces
        {
            get { return (int?)GetValue(DecimalPlacesProperty); }
            set { SetValue(DecimalPlacesProperty, value); }
        }

        private static void OnDecimalPlacesChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            NumericUpDownBase<T> numericUpDown = o as NumericUpDownBase<T>;
            if (numericUpDown != null)
                numericUpDown.OnDecimalPlacesChanged((int?)e.OldValue, (int?)e.NewValue);
        }

        protected virtual void OnDecimalPlacesChanged(int? oldValue, int? newValue)
        {
        }

        #endregion // DecimalPlaces

        #endregion //Properties

        #region Base Class Overrides

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            if (SelectAllOnGotFocus && TextBox != null)
            {
                //in order to select all the text we must handle both the keybord (tabbing) and mouse (clicking) events
                TextBox.GotKeyboardFocus += OnTextBoxGotKeyBoardFocus;
                TextBox.PreviewMouseLeftButtonDown += OnTextBoxPreviewMouseLeftButtonDown;
            }
        }

        #endregion //Base Class Overrides

        #region Event Handlers

        private void OnTextBoxGotKeyBoardFocus(object sender, RoutedEventArgs e)
        {
            TextBox.SelectAll();
        }

        void OnTextBoxPreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (!TextBox.IsKeyboardFocused)
            {
                e.Handled = true;
                TextBox.Focus();
            }
        }

        #endregion //Event Handlers

        #region Parse Methods

        protected static decimal ParseDecimal(String text, IFormatProvider cultureInfo, DecimalUpDown sender)
        {
            decimal result;
            Decimal.TryParse(text, NumberStyles.Any, cultureInfo, out result);
            return result;
        }

        protected static double ParseDouble(String text, IFormatProvider cultureInfo)
        {
            double result;
            Double.TryParse(text, NumberStyles.Any, cultureInfo, out result);
            return result;
        }
        
        protected static float ParseFloat(String text, IFormatProvider cultureInfo, FloatUpDown sender)
        {
            float value = 0.0f;
            value = float.Parse(text, NumberStyles.Any, cultureInfo);
            if (sender.DecimalPlaces.HasValue == true && sender.DecimalPlaces.Value >= 0)
            {
                String formatString = "{0:N" + sender.DecimalPlaces + "}";
                String decimalPlaceString = String.Format(formatString, value);
                return float.Parse(decimalPlaceString, NumberStyles.Any, cultureInfo);
            }
            else
            {
                return value;
            }
        }

        protected static int ParseInt(String text, IFormatProvider cultureInfo)
        {
            return Int32.Parse(text, NumberStyles.Any, cultureInfo);
        }

        protected static uint ParseUInt(String text, IFormatProvider cultureInfo)
        {
            return UInt32.Parse(text, NumberStyles.Any, cultureInfo);
        }

        protected static byte ParseByte(String text, IFormatProvider cultureInfo)
        {
            return byte.Parse(text, NumberStyles.Any, cultureInfo);
        }

        protected static decimal ParsePercent(String text, IFormatProvider cultureInfo)
        {
            NumberFormatInfo info = NumberFormatInfo.GetInstance(cultureInfo);

            text = text.Replace(info.PercentSymbol, null);

            decimal result = Decimal.Parse(text, NumberStyles.Any, info);
            result = result / 100;

            return result;
        }

        #endregion // Parse Methods
    } // NumericUpDownBase<T>

    /// <summary>
    /// UpDown Spinner Control for type double?
    /// </summary>
    public class FloatUpDown : NumericUpDownBase<float?>
    {
        #region Constructors

        static FloatUpDown()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(FloatUpDown), new FrameworkPropertyMetadata(typeof(FloatUpDown)));
            DefaultValueProperty.OverrideMetadata(typeof(FloatUpDown), new FrameworkPropertyMetadata(default(float)));
            IncrementProperty.OverrideMetadata(typeof(FloatUpDown), new FrameworkPropertyMetadata(0.1f));
            MaximumProperty.OverrideMetadata(typeof(FloatUpDown), new FrameworkPropertyMetadata(float.MaxValue));
            MinimumProperty.OverrideMetadata(typeof(FloatUpDown), new FrameworkPropertyMetadata(float.MinValue));
        }

        #endregion //Constructors

        #region Base Class Overrides

        protected override float? CoerceValue(float? value)
        {
            if (value < Minimum)
                return Minimum;
            else if (value > Maximum)
                return Maximum;
            else
                return value;
        }

        protected override void OnIncrement()
        {
            if (Value.HasValue)
            {
                float? value = this.Value + Increment;
                Value = CoerceValue(value);
            }
            else
            {
                Value = CoerceValue(DefaultValue);
            }
        }

        protected override void OnDecrement()
        {
            if (Value.HasValue)
            {
                float? value = this.Value - Increment;
                Value = CoerceValue(value);
            }
            else
            {
                Value = CoerceValue(DefaultValue);
            }
        }

        protected override float? ConvertTextToValue(String text)
        {
            float? result = 0.0f;

            if (String.IsNullOrEmpty(text))
            {
                this.ContainsError = true;
                return CoerceValue(result);
            }

            try
            {
                float parsed = FormatString.Contains("P") ? (float)Decimal.ToDouble(ParsePercent(text, CultureInfo)) : ParseFloat(text, CultureInfo, this);
                result = CoerceValue(parsed);
                if (parsed != result)
                    this.ContainsError = true;
                else
                    this.ContainsError = false;
                return result;
            }
            catch
            {
                this.ContainsError = true;
                Text = ConvertValueToText();
                return Value;
            }
        }

        protected override string ConvertValueToText()
        {
            if (Value == null)
                return string.Empty;

            if (this.DecimalPlaces.HasValue == true && this.DecimalPlaces.Value >= 0)
            {
                String formatString = "{0:N" + this.DecimalPlaces + "}";
                return String.Format(formatString, Value.Value);
            }
            else
            {
                return Value.Value.ToString(FormatString, CultureInfo);
            }
        }

        protected override void SetValidSpinDirection()
        {
            ValidSpinDirections validDirections = ValidSpinDirections.None;

            if (Value < Maximum || !Value.HasValue)
                validDirections = validDirections | ValidSpinDirections.Increase;

            if (Value > Minimum || !Value.HasValue)
                validDirections = validDirections | ValidSpinDirections.Decrease;

            if (Spinner != null)
                Spinner.ValidSpinDirection = validDirections;
        }

        protected override void ValidateValue(float? value)
        {
            if (value < Minimum)
                Value = Minimum;
            else if (value > Maximum)
                Value = Maximum;
        }

        protected override void OnKeyDown(KeyEventArgs e)
        {
            bool isNumPadNumeric = (e.Key >= Key.NumPad0 && e.Key <= Key.NumPad9) || e.Key == Key.Decimal;
            bool isNumeric = (e.Key >= Key.D0 && e.Key <= Key.D9) || e.Key == Key.OemPeriod || e.Key == Key.OemMinus;

            if ((isNumeric || isNumPadNumeric) && Keyboard.Modifiers != ModifierKeys.None)
            {
                e.Handled = true;
                return;
            }

            bool isControl = ((Keyboard.Modifiers != ModifierKeys.None && Keyboard.Modifiers != ModifierKeys.Shift)
                || e.Key == Key.Back || e.Key == Key.Delete || e.Key == Key.Insert
                || e.Key == Key.Down || e.Key == Key.Left || e.Key == Key.Right || e.Key == Key.Up
                || e.Key == Key.Tab
                || e.Key == Key.PageDown || e.Key == Key.PageUp
                || e.Key == Key.Enter || e.Key == Key.Return || e.Key == Key.Escape
                || e.Key == Key.Home || e.Key == Key.End);

            e.Handled = !isControl && !isNumeric && !isNumPadNumeric;

            if (!e.Handled)
                base.OnKeyDown(e);
        }

        #endregion //Base Class Overrides
    } // FloatUpDown

    /// <summary>
    /// UpDown Spinner Control for type double?
    /// </summary>
    public class IntegerUpDown : NumericUpDownBase<int?>
    {
        #region Constructors

        static IntegerUpDown()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(IntegerUpDown), new FrameworkPropertyMetadata(typeof(IntegerUpDown)));
            DefaultValueProperty.OverrideMetadata(typeof(IntegerUpDown), new FrameworkPropertyMetadata(0));
            IncrementProperty.OverrideMetadata(typeof(IntegerUpDown), new FrameworkPropertyMetadata(1));
            MaximumProperty.OverrideMetadata(typeof(IntegerUpDown), new FrameworkPropertyMetadata(int.MaxValue));
            MinimumProperty.OverrideMetadata(typeof(IntegerUpDown), new FrameworkPropertyMetadata(int.MinValue));
        }

        #endregion //Constructors

        #region Base Class Overrides

        protected override int? CoerceValue(int? value)
        {
            if (value < Minimum)
                return Minimum;
            else if (value > Maximum)
                return Maximum;
            else
                return value;
        }

        protected override void OnIncrement()
        {
            if (Value.HasValue)
            {
                int? value = this.Value + Increment;
                Value = CoerceValue(value);
            }
            else
            {
                Value = CoerceValue(DefaultValue);
            }
        }

        protected override void OnDecrement()
        {
            if (Value.HasValue)
            {
                int? value = this.Value - Increment;
                Value = CoerceValue(value);
            }
            else
            {
                Value = CoerceValue(DefaultValue);
            }
        }

        protected override int? ConvertTextToValue(String text)
        {
            int? result = 0;

            if (String.IsNullOrEmpty(text))
            {
                this.ContainsError = true;
                return CoerceValue(result);
            }

            try
            {
                int? parsed = FormatString.Contains("P") ? Decimal.ToInt32(ParsePercent(text, CultureInfo)) : ParseInt(text, CultureInfo);
                result = CoerceValue(parsed);
                if (parsed != result)
                    this.ContainsError = true;
                else
                    this.ContainsError = false;
            }
            catch
            {
                this.ContainsError = true;
                Text = ConvertValueToText();
                return Value;
            }

            return result;
        }

        protected override string ConvertValueToText()
        {
            if (Value == null)
                return string.Empty;

            return Value.Value.ToString(FormatString, CultureInfo);
        }

        protected override void SetValidSpinDirection()
        {
            ValidSpinDirections validDirections = ValidSpinDirections.None;

            if (Value < Maximum || !Value.HasValue)
                validDirections = validDirections | ValidSpinDirections.Increase;

            if (Value > Minimum || !Value.HasValue)
                validDirections = validDirections | ValidSpinDirections.Decrease;

            if (Spinner != null)
                Spinner.ValidSpinDirection = validDirections;
        }

        protected override void ValidateValue(int? value)
        {
            if (value < Minimum)
                Value = Minimum;
            else if (value > Maximum)
                Value = Maximum;
        }

        protected override void OnKeyDown(KeyEventArgs e)
        {
            bool isNumPadNumeric = (e.Key >= Key.NumPad0 && e.Key <= Key.NumPad9) || e.Key == Key.Decimal;
            bool isNumeric = (e.Key >= Key.D0 && e.Key <= Key.D9) || e.Key == Key.OemMinus;

            if ((isNumeric || isNumPadNumeric) && Keyboard.Modifiers != ModifierKeys.None)
            {
                e.Handled = true;
                return;
            }

            bool isControl = ((Keyboard.Modifiers != ModifierKeys.None && Keyboard.Modifiers != ModifierKeys.Shift)
                || e.Key == Key.Back || e.Key == Key.Delete || e.Key == Key.Insert
                || e.Key == Key.Down || e.Key == Key.Left || e.Key == Key.Right || e.Key == Key.Up
                || e.Key == Key.Tab
                || e.Key == Key.PageDown || e.Key == Key.PageUp
                || e.Key == Key.Enter || e.Key == Key.Return || e.Key == Key.Escape
                || e.Key == Key.Home || e.Key == Key.End);

            e.Handled = !isControl && !isNumeric && !isNumPadNumeric;

            if (!e.Handled)
                base.OnKeyDown(e);
        }
        #endregion //Base Class Overrides
    } // IntegerUpDown

    /// <summary>
    /// UpDown Spinner Control for type double?
    /// </summary>
    public class UnsignedIntegerUpDown : NumericUpDownBase<uint?>
    {
        #region Constructors

        static UnsignedIntegerUpDown()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(UnsignedIntegerUpDown), new FrameworkPropertyMetadata(typeof(UnsignedIntegerUpDown)));
            DefaultValueProperty.OverrideMetadata(typeof(UnsignedIntegerUpDown), new FrameworkPropertyMetadata(default(uint)));
            IncrementProperty.OverrideMetadata(typeof(UnsignedIntegerUpDown), new FrameworkPropertyMetadata((uint)1));
            MaximumProperty.OverrideMetadata(typeof(UnsignedIntegerUpDown), new FrameworkPropertyMetadata(uint.MaxValue));
            MinimumProperty.OverrideMetadata(typeof(UnsignedIntegerUpDown), new FrameworkPropertyMetadata(uint.MinValue));
        }

        #endregion //Constructors

        #region Base Class Overrides

        protected override uint? CoerceValue(uint? value)
        {
            if (value < Minimum)
                return Minimum;
            else if (value > Maximum)
                return Maximum;
            else
                return value;
        }

        protected override void OnIncrement()
        {
            if (Value.HasValue)
            {
                uint? value = this.Value + Increment;
                Value = CoerceValue(value);
            }
            else
            {
                Value = CoerceValue(DefaultValue);
            }
        }

        protected override void OnDecrement()
        {
            if (Value.HasValue)
            {
                uint? value = this.Value - Increment;
                Value = CoerceValue(value);
            }
            else
            {
                Value = CoerceValue(DefaultValue);
            }
        }

        protected override uint? ConvertTextToValue(String text)
        {
            uint? result = 0;

            if (String.IsNullOrEmpty(text))
            {
                this.ContainsError = true;
                return CoerceValue(result);
            }

            try
            {
                uint? parsed = FormatString.Contains("P") ? Decimal.ToUInt32(ParsePercent(text, CultureInfo)) : ParseUInt(text, CultureInfo);
                result = CoerceValue(parsed);
                if (parsed != result)
                    this.ContainsError = true;
                else
                    this.ContainsError = false;
            }
            catch
            {
                this.ContainsError = true;
                Text = ConvertValueToText();
                return Value;
            }

            return result;
        }

        protected override string ConvertValueToText()
        {
            if (Value == null)
                return string.Empty;

            return Value.Value.ToString(FormatString, CultureInfo);
        }

        protected override void SetValidSpinDirection()
        {
            ValidSpinDirections validDirections = ValidSpinDirections.None;

            if (Value < Maximum || !Value.HasValue)
                validDirections = validDirections | ValidSpinDirections.Increase;

            if (Value > Minimum || !Value.HasValue)
                validDirections = validDirections | ValidSpinDirections.Decrease;

            if (Spinner != null)
                Spinner.ValidSpinDirection = validDirections;
        }

        protected override void ValidateValue(uint? value)
        {
            if (value < Minimum)
                Value = Minimum;
            else if (value > Maximum)
                Value = Maximum;
        }

        protected override void OnKeyDown(KeyEventArgs e)
        {
            bool isNumPadNumeric = (e.Key >= Key.NumPad0 && e.Key <= Key.NumPad9) || e.Key == Key.Decimal;
            bool isNumeric = (e.Key >= Key.D0 && e.Key <= Key.D9) || e.Key == Key.OemMinus;

            if ((isNumeric || isNumPadNumeric) && Keyboard.Modifiers != ModifierKeys.None)
            {
                e.Handled = true;
                return;
            }

            bool isControl = ((Keyboard.Modifiers != ModifierKeys.None && Keyboard.Modifiers != ModifierKeys.Shift)
                || e.Key == Key.Back || e.Key == Key.Delete || e.Key == Key.Insert
                || e.Key == Key.Down || e.Key == Key.Left || e.Key == Key.Right || e.Key == Key.Up
                || e.Key == Key.Tab
                || e.Key == Key.PageDown || e.Key == Key.PageUp
                || e.Key == Key.Enter || e.Key == Key.Return || e.Key == Key.Escape
                || e.Key == Key.Home || e.Key == Key.End);

            e.Handled = !isControl && !isNumeric && !isNumPadNumeric;

            if (!e.Handled)
                base.OnKeyDown(e);
        }
        #endregion //Base Class Overrides
    } // UnsignedIntegerUpDown

    /// <summary>
    /// UpDown Spinner Control for type double?
    /// </summary>
    public class DecimalUpDown : NumericUpDownBase<decimal?>
    {
        #region Constructors

        static DecimalUpDown()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(DecimalUpDown), new FrameworkPropertyMetadata(typeof(DecimalUpDown)));
            DefaultValueProperty.OverrideMetadata(typeof(DecimalUpDown), new FrameworkPropertyMetadata(default(decimal)));
            IncrementProperty.OverrideMetadata(typeof(DecimalUpDown), new FrameworkPropertyMetadata(1m));
            MaximumProperty.OverrideMetadata(typeof(DecimalUpDown), new FrameworkPropertyMetadata(decimal.MaxValue));
            MinimumProperty.OverrideMetadata(typeof(DecimalUpDown), new FrameworkPropertyMetadata(decimal.MinValue));
        }

        #endregion //Constructors

        #region Base Class Overrides

        protected override decimal? CoerceValue(decimal? value)
        {
            if (value < Minimum)
                return Minimum;
            else if (value > Maximum)
                return Maximum;
            else
                return value;
        }

        protected override void OnIncrement()
        {
            if (Value.HasValue)
            {
                decimal? value = this.Value + Increment;
                Value = CoerceValue(value);
            }
            else
            {
                Value = CoerceValue(DefaultValue);
            }
        }

        protected override void OnDecrement()
        {
            if (Value.HasValue)
            {
                decimal? value = this.Value - Increment;
                Value = CoerceValue(value);
            }
            else
            {
                Value = CoerceValue(DefaultValue);
            }
        }

        protected override decimal? ConvertTextToValue(String text)
        {
            decimal? result = new decimal(0.0);

            if (String.IsNullOrEmpty(text))
            {
                this.ContainsError = true;
                return CoerceValue(result);
            }

            try
            {
                decimal? parsed = FormatString.Contains("P") ? (decimal)Decimal.ToDouble(ParsePercent(text, CultureInfo)) : ParseDecimal(text, CultureInfo, this);
                result = CoerceValue(parsed);
                if (parsed != result)
                {
                    this.ContainsError = true;
                }
                else
                {
                    this.ContainsError = false;
                }

                return result;
            }
            catch
            {
                this.ContainsError = true;
                Text = ConvertValueToText();
                return Value;
            }
        }

        protected override string ConvertValueToText()
        {
            if (Value == null)
                return string.Empty;

            if (this.DecimalPlaces.HasValue == true && this.DecimalPlaces.Value >= 0)
            {
                String formatString = "{0:N" + this.DecimalPlaces + "}";
                return String.Format(formatString, Value.Value);
            }
            else
            {
                return Value.Value.ToString(FormatString, CultureInfo);
            }
        }

        protected override void SetValidSpinDirection()
        {
            ValidSpinDirections validDirections = ValidSpinDirections.None;

            if (Value < Maximum || !Value.HasValue)
                validDirections = validDirections | ValidSpinDirections.Increase;

            if (Value > Minimum || !Value.HasValue)
                validDirections = validDirections | ValidSpinDirections.Decrease;

            if (Spinner != null)
                Spinner.ValidSpinDirection = validDirections;
        }

        protected override void ValidateValue(decimal? value)
        {
            if (value < Minimum)
                Value = Minimum;
            else if (value > Maximum)
                Value = Maximum;
        }

        protected override void OnKeyDown(KeyEventArgs e)
        {
            bool isNumPadNumeric = (e.Key >= Key.NumPad0 && e.Key <= Key.NumPad9) || e.Key == Key.Decimal;
            bool isNumeric = (e.Key >= Key.D0 && e.Key <= Key.D9) || e.Key == Key.OemPeriod || e.Key == Key.OemMinus;

            if ((isNumeric || isNumPadNumeric) && Keyboard.Modifiers != ModifierKeys.None)
            {
                e.Handled = true;
                return;
            }

            bool isControl = ((Keyboard.Modifiers != ModifierKeys.None && Keyboard.Modifiers != ModifierKeys.Shift)
                || e.Key == Key.Back || e.Key == Key.Delete || e.Key == Key.Insert
                || e.Key == Key.Down || e.Key == Key.Left || e.Key == Key.Right || e.Key == Key.Up
                || e.Key == Key.Tab
                || e.Key == Key.PageDown || e.Key == Key.PageUp
                || e.Key == Key.Enter || e.Key == Key.Return || e.Key == Key.Escape
                || e.Key == Key.Home || e.Key == Key.End);

            e.Handled = !isControl && !isNumeric && !isNumPadNumeric;

            if (e.Key == Key.OemPeriod || e.Key == Key.Decimal)
            {
                if (this.Value == null)
                {
                    e.Handled = true;
                    return;
                }

                if (string.IsNullOrEmpty(this.Value.ToString()))
                {
                    e.Handled = true;
                    return;
                }

                if (this.Value.ToString().Contains('.'))
                {
                    e.Handled = true;
                    return;
                }
            }

            if (!e.Handled)
                base.OnKeyDown(e);
        }
        #endregion //Base Class Overrides
    } // DecimalUpDown

    /// <summary>
    /// 
    /// </summary>
    public class DoubleUpDown : NumericUpDownBase<double?>
    {
        #region Constructors

        static DoubleUpDown()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(DoubleUpDown), new FrameworkPropertyMetadata(typeof(DoubleUpDown)));
            DefaultValueProperty.OverrideMetadata(typeof(DoubleUpDown), new FrameworkPropertyMetadata(default(double)));
            IncrementProperty.OverrideMetadata(typeof(DoubleUpDown), new FrameworkPropertyMetadata(1d));
            MaximumProperty.OverrideMetadata(typeof(DoubleUpDown), new FrameworkPropertyMetadata(double.MaxValue));
            MinimumProperty.OverrideMetadata(typeof(DoubleUpDown), new FrameworkPropertyMetadata(double.MinValue));
        }

        #endregion //Constructors

        #region Base Class Overrides

        protected override double? CoerceValue(double? value)
        {
            if (value < Minimum)
                return Minimum;
            else if (value > Maximum)
                return Maximum;
            else
                return value;
        }

        protected override void OnIncrement()
        {
            if (Value.HasValue)
            {
                double? value = this.Value + Increment;
                Value = CoerceValue(value);
            }
            else
            {
                Value = CoerceValue(DefaultValue);
            }
        }

        protected override void OnDecrement()
        {
            if (Value.HasValue)
            {
                double? value = this.Value - Increment;
                Value = CoerceValue(value);
            }
            else
            {
                Value = CoerceValue(DefaultValue);
            }
        }

        protected override double? ConvertTextToValue(string text)
        {
            double? result = null;

            if (String.IsNullOrEmpty(text))
            {
                this.ContainsError = true;
                return CoerceValue(result);
            }

            try
            {
                double? parsed = FormatString.Contains("P") ? Decimal.ToDouble(ParsePercent(text, CultureInfo)) : ParseDouble(text, CultureInfo);
                result = CoerceValue(parsed);
                if (parsed != result)
                    this.ContainsError = true;
                else
                    this.ContainsError = false;
            }
            catch
            {
                this.ContainsError = true;
                Text = ConvertValueToText();
                return Value;
            }

            return result;
        }

        protected override string ConvertValueToText()
        {
            if (Value == null)
                return string.Empty;

            return Value.Value.ToString(FormatString, CultureInfo);
        }

        protected override void SetValidSpinDirection()
        {
            ValidSpinDirections validDirections = ValidSpinDirections.None;

            if (Value < Maximum || !Value.HasValue)
                validDirections = validDirections | ValidSpinDirections.Increase;

            if (Value > Minimum || !Value.HasValue)
                validDirections = validDirections | ValidSpinDirections.Decrease;

            if (Spinner != null)
                Spinner.ValidSpinDirection = validDirections;
        }

        protected override void ValidateValue(double? value)
        {
            if (value < Minimum)
                Value = Minimum;
            else if (value > Maximum)
                Value = Maximum;
        }

        protected override void OnKeyDown(KeyEventArgs e)
        {
            bool isNumPadNumeric = (e.Key >= Key.NumPad0 && e.Key <= Key.NumPad9) || e.Key == Key.Decimal;
            bool isNumeric = (e.Key >= Key.D0 && e.Key <= Key.D9) || e.Key == Key.OemPeriod || e.Key == Key.OemMinus;

            if ((isNumeric || isNumPadNumeric) && Keyboard.Modifiers != ModifierKeys.None)
            {
                e.Handled = true;
                return;
            }

            bool isControl = ((Keyboard.Modifiers != ModifierKeys.None && Keyboard.Modifiers != ModifierKeys.Shift)
                || e.Key == Key.Back || e.Key == Key.Delete || e.Key == Key.Insert
                || e.Key == Key.Down || e.Key == Key.Left || e.Key == Key.Right || e.Key == Key.Up
                || e.Key == Key.Tab
                || e.Key == Key.PageDown || e.Key == Key.PageUp
                || e.Key == Key.Enter || e.Key == Key.Return || e.Key == Key.Escape
                || e.Key == Key.Home || e.Key == Key.End);

            e.Handled = !isControl && !isNumeric && !isNumPadNumeric;

            if (!e.Handled)
                base.OnKeyDown(e);
        }
        #endregion //Base Class Overrides
    } // DoubleUpDown

    /// <summary>
    /// UpDown Spinner Control for type double?
    /// </summary>
    public class UnsignedInteger8UpDown : NumericUpDownBase<byte?>
    {
        #region Constructors

        static UnsignedInteger8UpDown()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(UnsignedIntegerUpDown), new FrameworkPropertyMetadata(typeof(UnsignedIntegerUpDown)));
            DefaultValueProperty.OverrideMetadata(typeof(UnsignedIntegerUpDown), new FrameworkPropertyMetadata(default(byte)));
            IncrementProperty.OverrideMetadata(typeof(UnsignedIntegerUpDown), new FrameworkPropertyMetadata((byte)1));
            MaximumProperty.OverrideMetadata(typeof(UnsignedIntegerUpDown), new FrameworkPropertyMetadata(byte.MaxValue));
            MinimumProperty.OverrideMetadata(typeof(UnsignedIntegerUpDown), new FrameworkPropertyMetadata(byte.MinValue));
        }

        #endregion //Constructors

        #region Base Class Overrides

        protected override byte? CoerceValue(byte? value)
        {
            if (value < Minimum)
                return Minimum;
            else if (value > Maximum)
                return Maximum;
            else
                return value;
        }

        protected override void OnIncrement()
        {
            if (Value.HasValue)
            {
                byte? value = (byte?)(this.Value + Increment);
                Value = CoerceValue(value);
            }
            else
            {
                Value = CoerceValue(DefaultValue);
            }
        }

        protected override void OnDecrement()
        {
            if (Value.HasValue)
            {
                byte? value = (byte?)(this.Value - Increment);
                Value = CoerceValue(value);
            }
            else
            {
                Value = CoerceValue(DefaultValue);
            }
        }

        protected override byte? ConvertTextToValue(String text)
        {
            byte? result = 0;

            if (String.IsNullOrEmpty(text))
            {
                this.ContainsError = true;
                return CoerceValue(result);
            }

            try
            {
                byte? parsed = FormatString.Contains("P") ? Decimal.ToByte(ParsePercent(text, CultureInfo)) : ParseByte(text, CultureInfo);
                result = CoerceValue(parsed);
                if (parsed != result)
                    this.ContainsError = true;
                else
                    this.ContainsError = false;
            }
            catch
            {
                this.ContainsError = true;
                Text = ConvertValueToText();
                return Value;
            }

            return result;
        }

        protected override string ConvertValueToText()
        {
            if (Value == null)
                return string.Empty;

            return Value.Value.ToString(FormatString, CultureInfo);
        }

        protected override void SetValidSpinDirection()
        {
            ValidSpinDirections validDirections = ValidSpinDirections.None;

            if (Value < Maximum || !Value.HasValue)
                validDirections = validDirections | ValidSpinDirections.Increase;

            if (Value > Minimum || !Value.HasValue)
                validDirections = validDirections | ValidSpinDirections.Decrease;

            if (Spinner != null)
                Spinner.ValidSpinDirection = validDirections;
        }

        protected override void ValidateValue(byte? value)
        {
            if (value < Minimum)
                Value = Minimum;
            else if (value > Maximum)
                Value = Maximum;
        }

        protected override void OnKeyDown(KeyEventArgs e)
        {
            bool isNumPadNumeric = (e.Key >= Key.NumPad0 && e.Key <= Key.NumPad9) || e.Key == Key.Decimal;
            bool isNumeric = (e.Key >= Key.D0 && e.Key <= Key.D9) || e.Key == Key.OemMinus;

            if ((isNumeric || isNumPadNumeric) && Keyboard.Modifiers != ModifierKeys.None)
            {
                e.Handled = true;
                return;
            }

            bool isControl = ((Keyboard.Modifiers != ModifierKeys.None && Keyboard.Modifiers != ModifierKeys.Shift)
                || e.Key == Key.Back || e.Key == Key.Delete || e.Key == Key.Insert
                || e.Key == Key.Down || e.Key == Key.Left || e.Key == Key.Right || e.Key == Key.Up
                || e.Key == Key.Tab
                || e.Key == Key.PageDown || e.Key == Key.PageUp
                || e.Key == Key.Enter || e.Key == Key.Return || e.Key == Key.Escape
                || e.Key == Key.Home || e.Key == Key.End);

            e.Handled = !isControl && !isNumeric && !isNumPadNumeric;

            if (!e.Handled)
                base.OnKeyDown(e);
        }
        #endregion //Base Class Overrides
    } // UnsignedIntegerUpDown
} // RSG.Base.Windows.Controls
