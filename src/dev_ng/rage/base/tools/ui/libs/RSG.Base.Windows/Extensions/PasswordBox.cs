﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;

namespace RSG.Base.Windows.Extensions
{
    /// <summary>
    /// Code adapted from:
    /// http://blog.functionalfun.net/2008/06/wpf-passwordbox-and-data-binding.html
    /// </summary>
    public static class PasswordBoxExtensions
    {
        public static readonly DependencyProperty BindablePassword =
          DependencyProperty.RegisterAttached("BindablePassword", typeof(string), typeof(PasswordBoxExtensions), new PropertyMetadata(string.Empty, OnBindablePasswordChanged));

        private static readonly DependencyProperty UpdatingPassword =
            DependencyProperty.RegisterAttached("UpdatingPassword", typeof(bool), typeof(PasswordBoxExtensions), new PropertyMetadata(false));

        private static void OnBindablePasswordChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            // Only handle this event when the property is attached to a PasswordBox
            PasswordBox box = d as PasswordBox;
            if (box == null)
            {
                return;
            }

            // avoid recursive updating by ignoring the box's changed event
            box.PasswordChanged -= HandlePasswordChanged;

            string newPassword = (string)e.NewValue;
            SetBindablePassword(box, newPassword);
            
            if (!GetUpdatingPassword(box))
            {
                box.Password = newPassword;
            }

            box.PasswordChanged += HandlePasswordChanged;
        }

        private static void HandlePasswordChanged(object sender, RoutedEventArgs e)
        {
            PasswordBox box = sender as PasswordBox;

            // Set flag to prevent recursive calls
            SetUpdatingPassword(box, true);
            SetBindablePassword(box, box.Password);
            SetUpdatingPassword(box, false);
        }

        public static string GetBindablePassword(DependencyObject dp)
        {
            return (string)dp.GetValue(BindablePassword);
        }

        public static void SetBindablePassword(DependencyObject dp, string value)
        {
            dp.SetValue(BindablePassword, value);
        }

        private static bool GetUpdatingPassword(DependencyObject dp)
        {
            return (bool)dp.GetValue(UpdatingPassword);
        }

        private static void SetUpdatingPassword(DependencyObject dp, bool value)
        {
            dp.SetValue(UpdatingPassword, value);
        }
    } // PasswordBoxExtensions
} // RSG.Base.Windows.Extensions
