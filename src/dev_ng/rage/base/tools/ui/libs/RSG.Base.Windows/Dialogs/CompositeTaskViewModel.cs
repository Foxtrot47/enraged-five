﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Tasks;
using RSG.Base.Collections;

namespace RSG.Base.Windows.Dialogs
{
    /// <summary>
    /// 
    /// </summary>
    public class CompositeTaskViewModel : TaskViewModel
    {
        #region Properties
        /// <summary>
        /// Sub task view models.
        /// </summary>
        public ObservableCollection<TaskViewModel> SubTasks
        {
            get { return m_subTasks; }
            set
            {
                SetPropertyValue(value, m_subTasks, () => this.SubTasks,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_subTasks = (ObservableCollection<TaskViewModel>)newValue;
                        }
                    ));
            }
        }
        private ObservableCollection<TaskViewModel> m_subTasks;

        /// <summary>
        /// Flag indicating whether we should show the sub tasks.
        /// </summary>
        public bool ShowDetailedView
        {
            get { return m_showDetailedView; }
            set
            {
                SetPropertyValue(value, m_showDetailedView, () => this.ShowDetailedView,
                    new RSG.Base.Editor.Command.PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_showDetailedView = (bool)newValue;
                        }
                    ));
            }
        }
        private bool m_showDetailedView;
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="task"></param>
        public CompositeTaskViewModel(CompositeTask task)
            : base(task)
        {
            // Create the sub task vms
            SubTasks = new ObservableCollection<TaskViewModel>();
            SubTasks.BeginUpdate();
            foreach (ITask subTask in task.SubTasks)
            {
                if (subTask is CompositeTask)
                {
                    SubTasks.Add(new CompositeTaskViewModel((CompositeTask)subTask));
                }
                else
                {
                    SubTasks.Add(new TaskViewModel(subTask));
                }
            }
            SubTasks.EndUpdate();

            ShowDetailedView = false;
        }
        #endregion // Constructor(s)
    } // CompositeTaskViewModel
}
