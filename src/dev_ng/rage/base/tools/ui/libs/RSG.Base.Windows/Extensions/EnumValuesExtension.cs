﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Markup;

namespace RSG.Base.Windows.Extensions
{
    /// <summary>
    /// Class that can be used in a WPF itemsource binding to expose enum values
    /// </summary>
    [MarkupExtensionReturnType(typeof(object[]))]
    public class EnumValuesExtension : MarkupExtension
    {
        public EnumValuesExtension()
        {
        }

        public EnumValuesExtension(Type enumType)
        {
            this.EnumType = enumType;
        }

        [ConstructorArgument("enumType")]
        public Type EnumType { get; set; }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            if (this.EnumType == null)
            {
                throw new ArgumentException("The enum type is not set");
            }
            if (!this.EnumType.IsEnum)
            {
                throw new ArgumentException("The enum type must derive from type Enum.");
            }
 
            return Enum.GetValues(this.EnumType);
        }
    } // EnumValuesExtension
}
