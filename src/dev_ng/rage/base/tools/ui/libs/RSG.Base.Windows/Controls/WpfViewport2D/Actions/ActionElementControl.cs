﻿using System;
using System.ComponentModel;
using RSG.Base.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace RSG.Base.Windows.Controls.WpfViewport2D.Actions
{
    /// <summary>
    /// 
    /// </summary>
    public class ActionElementControl :
        UserControl
    {
        /// <summary>
        /// A list of action managers that this action element control
        /// is attached to meaning that it is listening for specific events
        /// from all managers
        /// </summary>
        public ObservableCollection<ActionManager> ActionManagers
        {
            get { return m_actionManagers; }
            set { m_actionManagers = value; }
        }
        private ObservableCollection<ActionManager> m_actionManagers;

        /// <summary>
        /// Default Constructor
        /// </summary>
        public ActionElementControl()
        {
            this.ActionManagers = new ObservableCollection<ActionManager>();
            this.ActionManagers.CollectionChanged += ActionManagersChanged;
        }

        /// <summary>
        /// Adds this control to the given action manager if it already hasn't been done before
        /// </summary>
        public void RegisterControlWithManager(ActionManager manager)
        {
            if (!this.ActionManagers.Contains(manager))
            {
                this.ActionManagers.Add(manager);
                manager.KeyUp += OnManagerKeyUp;
                manager.KeyDown += OnManagerKeyDown;
                manager.PropertyChanged += OnManagerPropertyChanged;
            }
        }

        /// <summary>
        /// Adds this control to the given action manager if it already hasn't been done before
        /// </summary>
        public void UnRegisterControlWithManager(ActionManager manager)
        {
            if (this.ActionManagers.Contains(manager))
            {
                manager.KeyUp -= OnManagerKeyUp;
                manager.KeyDown -= OnManagerKeyDown;
                manager.PropertyChanged -= OnManagerPropertyChanged;

                this.ActionManagers.Remove(manager);
            }
        }

        #region Event Helpers

        /// <summary>
        /// Gets called when the action element sends a mouse double click event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnManagerMouseDoubleClick(Object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
        }

        /// <summary>
        /// Gets called when the action element sends a mouse button down event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnManagerMouseDown(Object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
        }

        /// <summary>
        /// Gets called when the action element sends a mouse button up event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnManagerMouseUp(Object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
        }

        /// <summary>
        /// Gets called when the action element sends a mouse wheel move event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnManagerMouseWheelMove(Object sender, System.Windows.Input.MouseWheelEventArgs e)
        {
        }

        /// <summary>
        /// Gets called when the action element sends a mouse move event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnManagerMouseMove(Object sender, System.Windows.Input.MouseEventArgs e)
        {
        }

        /// <summary>
        /// Gets called when the action element sends a key up event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnManagerKeyUp(Object sender, System.Windows.Input.KeyEventArgs e)
        {
        }

        /// <summary>
        /// Gets called when the action element sends a key down event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnManagerKeyDown(Object sender, System.Windows.Input.KeyEventArgs e)
        {
        }

        /// <summary>
        /// This gets called whenever a property on the action element changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnManagerPropertyChanged(Object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
        }

        #endregion Event Helpers

        /// <summary>
        /// Registers the actions with the manager new manager or removes it from the 
        /// removed manager.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void ActionManagersChanged(Object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null)
            {
                foreach (var newControl in e.NewItems)
                {
                    if (newControl is ActionManager)
                    {
                        this.RegisterActionsWithManager(newControl as ActionManager);
                    }
                }
            }
            if (e.OldItems != null)
            {
                foreach (var oldControl in e.OldItems)
                {
                    if (oldControl is ActionElementControl)
                    {
                        this.UnRegisterActionsWithManager(oldControl as ActionManager);
                    }
                }
            }
        }

        public virtual void RegisterActionsWithManager(ActionManager manager)
        {

        }

        public virtual void UnRegisterActionsWithManager(ActionManager manager)
        {

        }

    }
}
