﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;
using System.Windows.Data;

namespace RSG.Base.Windows.Extensions
{
    /// <summary>
    /// Original code from:
    /// http://stackoverflow.com/questions/563195/wpf-textbox-databind-on-enterkey-press
    /// </summary>
    public static class InputBindingsManager
    {
        public static readonly DependencyProperty UpdateBindingOnEnterProperty = DependencyProperty.RegisterAttached(
            "UpdateBindingOnEnter", typeof(DependencyProperty), typeof(InputBindingsManager), new PropertyMetadata(null, OnUpdateBindingOnEnterPropertyChanged));

        public static void SetUpdateBindingOnEnter(DependencyObject dp, DependencyProperty value)
        {
            dp.SetValue(UpdateBindingOnEnterProperty, value);
        }

        public static DependencyProperty GetUpdateBindingOnEnter(DependencyObject dp)
        {
            return (DependencyProperty)dp.GetValue(UpdateBindingOnEnterProperty);
        }

        private static void OnUpdateBindingOnEnterPropertyChanged(DependencyObject dp, DependencyPropertyChangedEventArgs e)
        {
            UIElement element = dp as UIElement;
            if (element == null)
            {
                return;
            }

            if (e.OldValue != null)
            {
                element.PreviewKeyDown -= HandlePreviewKeyDown;
            }
            if (e.NewValue != null)
            {
                element.PreviewKeyDown += new KeyEventHandler(HandlePreviewKeyDown);
            }
        }

        private static void HandlePreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                DoUpdateSource(e.Source);
            }
        }

        private static void DoUpdateSource(object source)
        {
            DependencyProperty property =
                GetUpdateBindingOnEnter(source as DependencyObject);
            if (property == null)
            {
                return;
            }

            UIElement elt = source as UIElement;
            if (elt == null)
            {
                return;
            }

            BindingExpression binding = BindingOperations.GetBindingExpression(elt, property);
            if (binding != null)
            {
                binding.UpdateSource();
            }
        }
    } // InputBindingsManager
}
