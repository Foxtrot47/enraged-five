﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Base.Windows.DragDrop
{
    /// <summary>
    /// 
    /// </summary>
    public interface IDragSource
    {
        Object StartDrag(DragInfo dragInfo);
    } // IDragSource
} // RSG.Base.Windows.DragDrop
