﻿using System;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace RSG.Base.Windows.Controls
{
    /// <summary>
    /// Represents a panel the arranges its children along the x axis based on a
    /// attached start property and a units per pixel property.
    /// </summary>
    public class EventPanel : Panel
    {
        public static readonly DependencyProperty StartProperty =
            DependencyProperty.RegisterAttached(
            "Start",
            typeof(double),
            typeof(EventPanel),
            new FrameworkPropertyMetadata(
                double.NaN,
                new PropertyChangedCallback(EventPanel.OnPositioningChanged)),
                new ValidateValueCallback(EventPanel.IsDoubleFiniteOrNaN));

        public static readonly DependencyProperty TopProperty =
            DependencyProperty.RegisterAttached(
            "Top",
            typeof(double),
            typeof(EventPanel),
            new FrameworkPropertyMetadata(
                double.NaN,
                new PropertyChangedCallback(EventPanel.OnPositioningChanged)),
                new ValidateValueCallback(EventPanel.IsDoubleFiniteOrNaN));

        public static readonly DependencyProperty HasFixedLengthProperty =
            DependencyProperty.RegisterAttached(
            "HasFixedLength",
            typeof(bool),
            typeof(EventPanel),
            new FrameworkPropertyMetadata(
                false,
                new PropertyChangedCallback(EventPanel.OnPositioningChanged)));

        public static readonly DependencyProperty UnitLengthProperty =
            DependencyProperty.RegisterAttached(
            "UnitLength",
            typeof(double),
            typeof(EventPanel),
            new FrameworkPropertyMetadata(
                1.0,
                new PropertyChangedCallback(EventPanel.OnLengthChanged)),
                new ValidateValueCallback(EventPanel.IsDoublePositiveOrZero));

        public static readonly DependencyProperty PixelsPerUnitProperty =
            DependencyProperty.Register(
            "PixelsPerUnit",
            typeof(double),
            typeof(EventPanel),
            new FrameworkPropertyMetadata(
                1.0,
                new PropertyChangedCallback(EventPanel.OnPixelsPerUnitChanged)),
                new ValidateValueCallback(EventPanel.IsDoublePositiveAndGreaterThanZero));

        /// <summary>
        /// Identies the <see cref="TrackUnitStartOffset"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty TrackUnitStartOffsetProperty =
                DependencyProperty.Register(
                "TrackUnitStartOffset",
                typeof(double),
                typeof(EventPanel),
                new FrameworkPropertyMetadata(
                    0.0,
                    FrameworkPropertyMetadataOptions.Inherits,
                    new PropertyChangedCallback(EventPanel.OnTrackUnitStartOffsetChanged)),
                    new ValidateValueCallback(EventPanel.IsDoublePositiveOrZero));

        ///// <summary>
        ///// Identies the <see cref="MaximumUnitValue"/> dependency property.
        ///// </summary>
        //public static readonly DependencyProperty MaximumUnitValueProperty;

        static EventPanel()
        {
            //EventPanel.MaximumUnitValueProperty =
            //    DependencyProperty.RegisterAttached(
            //    "MaximumUnitValue",
            //    typeof(double),
            //    typeof(EventPanel),
            //    new FrameworkPropertyMetadata(
            //        100.0,
            //        null),
            //        new ValidateValueCallback(EventPanel.IsDoublePositiveOrZero));
        }

        public double PixelsPerUnit
        {
            get { return (double)this.GetValue(EventPanel.PixelsPerUnitProperty); }
            set { this.SetValue(EventPanel.PixelsPerUnitProperty, value); }
        }

        /// <summary>
        /// Gets or sets the first visible unit on the event tracks.
        /// </summary>
        public double TrackUnitStartOffset
        {
            get { return (double)this.GetValue(EventPanel.TrackUnitStartOffsetProperty); }
            set { this.SetValue(EventPanel.TrackUnitStartOffsetProperty, value); }
        }

        public static bool GetHasFixedLength(UIElement element)
        {
            if (element == null)
            {
                throw new ArgumentNullException("element");
            }

            return (bool)element.GetValue(EventPanel.HasFixedLengthProperty);
        }

        public static void SetHasFixedLength(UIElement element, bool hasFixedLength)
        {
            if (element == null)
            {
                throw new ArgumentNullException("element");
            }

            element.SetValue(EventPanel.HasFixedLengthProperty, hasFixedLength);
        }

        public static double GetStart(UIElement element)
        {
            if (element == null)
            {
                throw new ArgumentNullException("element");
            }

            return (double)element.GetValue(EventPanel.StartProperty);
        }

        public static void SetStart(UIElement element, double start)
        {
            if (element == null)
            {
                throw new ArgumentNullException("element");
            }

            element.SetValue(EventPanel.StartProperty, start);
        }

        public static double GetTop(UIElement element)
        {
            if (element == null)
            {
                throw new ArgumentNullException("element");
            }

            return (double)element.GetValue(EventPanel.TopProperty);
        }

        public static void SetTop(UIElement element, double top)
        {
            if (element == null)
            {
                throw new ArgumentNullException("element");
            }

            element.SetValue(EventPanel.TopProperty, top);
        }

        public static double GetUnitLength(UIElement element)
        {
            if (element == null)
            {
                throw new ArgumentNullException("element");
            }

            return (double)element.GetValue(EventPanel.UnitLengthProperty);
        }

        public static void SetUnitLength(UIElement element, double length)
        {
            if (element == null)
            {
                throw new ArgumentNullException("element");
            }

            element.SetValue(EventPanel.UnitLengthProperty, length);
        }

        private static void OnPositioningChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            UIElement uIElement = d as UIElement;
            if (uIElement != null)
            {
                EventPanel panel = VisualTreeHelper.GetParent(uIElement) as EventPanel;
                if (panel != null)
                {
                    panel.InvalidateArrange();
                }
            }
        }

        private static void OnLengthChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            UIElement uIElement = d as UIElement;
            if (uIElement != null)
            {
                EventPanel panel = VisualTreeHelper.GetParent(uIElement) as EventPanel;
                if (panel != null)
                {
                    panel.InvalidateMeasure();
                }
            }
        }

        private static void OnPixelsPerUnitChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            EventPanel panel = d as EventPanel;
            if (panel != null)
            {
                panel.InvalidateArrange();
            }
        }

        private static void OnTrackUnitStartOffsetChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            EventPanel panel = d as EventPanel;
            if (panel != null)
            {
                panel.InvalidateArrange();
            }
        }

        protected override Size MeasureOverride(Size constraint)
        {
            Size size = default(Size);
            Size availableSize = new Size(double.PositiveInfinity, double.PositiveInfinity);
            foreach (UIElement uIElement in base.InternalChildren)
            {
                if (uIElement != null)
                {
                    bool hasFixedWidth = EventPanel.GetHasFixedLength(uIElement);
                    if (hasFixedWidth == true)
                    {
                        availableSize.Width = EventPanel.GetUnitLength(uIElement);
                        uIElement.Measure(availableSize);
                    }
                    else
                    {
                        availableSize.Width = EventPanel.GetUnitLength(uIElement) * this.PixelsPerUnit;
                        availableSize.Width = System.Math.Max(availableSize.Width, 1.0);
                        uIElement.Measure(availableSize);

                        Size desiredSize = uIElement.DesiredSize;
                        size.Height = System.Math.Max(size.Height, desiredSize.Height + EventPanel.GetTop(uIElement));
                    }

                }
            }
            return size;
        }

        protected override Size ArrangeOverride(Size arrangeSize)
        {
            foreach (UIElement uIElement in base.InternalChildren)
            {
                if (uIElement == null)
                {
                    continue;
                }

                double x = 0.0;
                double y = 0.0;
                double start = EventPanel.GetStart(uIElement);
                if (!double.IsNaN(start))
                {
                    x = start * this.PixelsPerUnit;
                    x = x - this.TrackUnitStartOffset;
                }

                double top = EventPanel.GetTop(uIElement);
                if (!double.IsNaN(top))
                {
                    y = top;
                }

                double width = EventPanel.GetUnitLength(uIElement);
                width = width * this.PixelsPerUnit;
                width = System.Math.Max(width, 1.0);

                uIElement.Arrange(new Rect(new Point(x, y), new Size(width, uIElement.DesiredSize.Height)));
            }

            return arrangeSize;
        }

        /// <summary>
        /// Evaluates the equalivalent double value of the specified object.
        /// </summary>
        /// <param name="o">
        /// The object to test.
        /// </param>
        /// <returns>
        /// True if the specified object is equalivalent to to a double finit value or the
        /// double nan value.
        /// </returns>
        internal static bool IsDoubleFiniteOrNaN(object o)
        {
            double d = (double)o;
            return !double.IsInfinity(d);
        }

        /// <summary>
        /// Evaluates the equalivalent double value of the specified object.
        /// </summary>
        /// <param name="o">
        /// The object to test.
        /// </param>
        /// <returns>
        /// True if the specified object is equalivalent to to a positive double finite value
        /// or zero.
        /// </returns>
        internal static bool IsDoublePositiveOrZero(object o)
        {
            double d = (double)o;
            return !double.IsNaN(d) && d >= 0;
        }

        /// <summary>
        /// Evaluates the equalivalent double value of the specified object.
        /// </summary>
        /// <param name="o">
        /// The object to test.
        /// </param>
        /// <returns>
        /// True if the specified object is equalivalent to to a double finite value or the
        /// double nan value.
        /// </returns>
        internal static bool IsDoublePositiveAndGreaterThanZero(object o)
        {
            double d = (double)o;
            return !double.IsNaN(d) && d > 0;
        }

        ///// <summary>
        ///// Gets or sets the maximum unit value the control can display, this effects
        ///// the manipulation of individual events.
        ///// </summary>
        //public double MaximumUnitValue
        //{
        //    get { return (double)this.GetValue(MaximumUnitValueProperty); }
        //    set { this.SetValue(MaximumUnitValueProperty, value); }
        //}
    }
}
