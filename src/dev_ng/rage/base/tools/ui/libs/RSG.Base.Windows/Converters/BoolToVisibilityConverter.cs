﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using System.Windows;
using System.Globalization;

namespace RSG.Base.Windows.Converters
{

    /// <summary>
    /// Boolean to Visibility converter.
    /// </summary>
    [ValueConversion(typeof(bool), typeof(Visibility))]
    public class BoolToVisibilityConverter : IValueConverter
    {
        #region Properties 
        /// <summary>
        /// Invert the boolean to visibility converter logic (default: false).
        /// </summary>
        public bool InvertVisibility
        {
            get;
            set;
        }

        /// <summary>
        /// Whether the visibility should be set to collapse when not visible
        /// </summary>
        public bool Collapse
        {
            get;
            set;
        }
        #endregion // Properties

        #region IValueConverter Members
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object Convert(Object value, Type targetType, Object parameter, CultureInfo culture)
        {
            bool visible = System.Convert.ToBoolean(value, culture);
            if (this.InvertVisibility)
                visible = !visible;
            return (visible ? Visibility.Visible : (Collapse ? Visibility.Collapsed : Visibility.Hidden));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object ConvertBack(Object value, Type targetType, Object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
        #endregion // IValueConverter Members
    }

} // RSG.Base.Windows.Converters namespace
