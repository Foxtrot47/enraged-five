﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using System.Globalization;

namespace RSG.Base.Windows.Converters
{
    [ValueConversion(typeof(String), typeof(Uri))]
    public class StringToUriConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            string formattedString = String.Format(parameter.ToString(), values);

            Uri result;
            Uri.TryCreate(formattedString, UriKind.RelativeOrAbsolute, out result);
            return result;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException("This converter cannot be used in two-way binding.");
        }
    } // StringToUriConverter
} // RSG.Base.Windows.Converters
