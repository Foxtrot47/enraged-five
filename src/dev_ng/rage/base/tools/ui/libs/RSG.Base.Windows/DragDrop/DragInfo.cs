﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using RSG.Base.Windows.DragDrop.Utilities;

namespace RSG.Base.Windows.DragDrop
{
    /// <summary>
    /// A data object that is the object used to carry data in
    /// during a drag and drop operation
    /// </summary>
    public class DragInfo
    {
        #region Members

        private Object m_data;
        private DragDropEffects m_effects;
        private Point m_startPosition;
        private MouseButton m_mouseButton;
        private IEnumerable m_sourceCollection;
        private Object m_sourceItem;
        private IEnumerable m_sourceItems;
        private UIElement m_visualSource;
        private UIElement m_visualSourceItem;
        private List<UIElement> m_visualSourceItems;

        #endregion // Members

        #region Properties

        public Object Data
        {
            get { return m_data; }
            set { m_data = value; }
        }

        /// <summary>
        /// Specifies the possible effects of this drag-and-drop operation.
        /// </summary>
        public DragDropEffects Effects
        {
            get { return m_effects; }
            set { m_effects = value; }
        }

        /// <summary>
        /// The mouse position at the time whent he drag started.
        /// This is not relative to any UIElement.
        /// </summary>
        public Point DragStartPosition
        {
            get { return m_startPosition; }
            private set { m_startPosition = value; } 
        }

        /// <summary>
        /// This mouse button that was changed to start this
        /// drag event
        /// </summary>
        public MouseButton MouseButton
        {
            get { return m_mouseButton; }
            private set { m_mouseButton = value; }
        }
        
        /// <summary>
        /// The collection that the drag event came from
        /// </summary>
        public IEnumerable SourceCollection
        {
            get { return m_sourceCollection; }
            private set { m_sourceCollection = value; }
        }

        /// <summary>
        /// The data item the drag event came from
        /// </summary>
        public Object SourceItem
        {
            get { return m_sourceItem; }
            private set { m_sourceItem = value; }
        }

        /// <summary>
        /// The collection of items that are part of the drag operation
        /// </summary>
        public IEnumerable SourceItems
        {
            get { return m_sourceItems; }
            private set { m_sourceItems = value; }
        }

        /// <summary>
        /// The ui element that sent the initial drag event
        /// </summary>
        public UIElement VisualSource
        {
            get { return m_visualSource; }
            private set { m_visualSource = value; }
        }

        /// <summary>
        /// The item in the visual source that sent the drag event
        /// </summary>
        public UIElement VisualSourceItem
        {
            get { return m_visualSourceItem; }
            private set { m_visualSourceItem = value; }
        }

        /// <summary>
        /// The collection of items that are part of the drag operation
        /// </summary>
        public List<UIElement> VisualSourceItems
        {
            get { return m_visualSourceItems; }
            private set { m_visualSourceItems = value; }
        }

        #endregion // Properties

        #region Constructors

        /// <summary>
        /// 
        /// </summary>
        public DragInfo(Object sender, MouseButtonEventArgs e)
        {
            this.DragStartPosition = e.GetPosition(null);
            this.Effects = DragDropEffects.None;
            this.MouseButton = e.ChangedButton;
            this.VisualSource = sender as UIElement;

            if (sender is ItemsControl)
            {
                ItemsControl itemsControl = (ItemsControl)sender;
                UIElement item = itemsControl.GetItemContainer((UIElement)e.OriginalSource);

                if (item != null)
                {
                    ItemsControl itemParent = ItemsControl.ItemsControlFromItemContainer(item);

                    this.SourceCollection = itemParent.ItemsSource ?? itemParent.Items;
                    this.SourceItem = itemParent.ItemContainerGenerator.ItemFromContainer(item);
                    this.SourceItems = itemsControl.GetSelectedItems();
                    this.VisualSourceItems = new List<UIElement>();
                    foreach (Object sourceItem in this.SourceItems)
                    {
                        this.VisualSourceItems.Add(itemParent.ItemContainerGenerator.ContainerFromItem(sourceItem) as UIElement);
                    }

                    // Some controls (Basically the TreeView!) haven't updated their
                    // SelectedItem by this point. Check to see if there is 1 or less items in 
                    // the SourceItems collection, and if so, override the SelectedItems
                    // with the clicked item.
                    if (SourceItems.Cast<Object>().Count() <= 1)
                    {
                        this.SourceItems = Enumerable.Repeat(SourceItem, 1);
                        this.VisualSourceItems = new List<UIElement>();
                        foreach (Object sourceItem in this.SourceItems)
                        {
                            this.VisualSourceItems.Add(itemParent.ItemContainerGenerator.ContainerFromItem(sourceItem) as UIElement);
                        }
                    }

                    this.VisualSourceItem = item;
                }
                else
                {
                    SourceCollection = itemsControl.ItemsSource ?? itemsControl.Items;
                }
            }

            if (this.SourceItems == null)
            {
                this.SourceItems = Enumerable.Empty<Object>();
            }
        }

        #endregion // Constructors
    } // DragInfo
} // RSG.Base.Windows.DragDrop
