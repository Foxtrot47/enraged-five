﻿using System;
using System.Windows.Data;
using System.Windows.Media;

namespace RSG.Base.Windows.Converters
{
    public class ColourToIntegerConverter : IValueConverter
    {
        #region IValueConverter Members

        /// <summary>
        /// Converts a unsigned integer value to a Color.
        /// </summary>
        public Object Convert(Object value, Type targetType, Object parameter, System.Globalization.CultureInfo culture)
        {
            if (value is uint)
            {
                uint i = (uint)value;
                return Color.FromArgb((byte)((i >> 24) & 0xFF),
                   (byte)((i >> 16) & 0xFF),
                   (byte)((i >> 8) & 0xFF),
                   (byte)(i & 0xFF));
            }
            
            return value;
        }

        /// <summary>
        /// Converts a Color to a unsigned integer value.
        /// </summary>
        public Object ConvertBack(Object value, Type targetType, Object parameter, System.Globalization.CultureInfo culture)
        {
            if (value is Color)
            {
                Color c = (Color)value;
                return (uint)(((c.A << 24) | (c.R << 16) | (c.G << 8) | c.B) & 0xffffffffL);
            }

            return value;
        }

        #endregion
    }
}
