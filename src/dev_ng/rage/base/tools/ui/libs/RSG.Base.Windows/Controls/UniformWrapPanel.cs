﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;

namespace RSG.Base.Windows.Controls
{
    /// <summary>
    /// A wrap panel that also makes sure the items are all the same size (width and height)
    /// </summary>
    public class UniformWrapPanel : WrapPanel
    {
        /// <summary>
        /// Set this to true to make it so that all of the items have the same width and height and that the biggest item will definitely fit
        /// </summary>
        public Boolean IsAutoUniform
        {
            get { return (Boolean)GetValue(IsAutoUniformProperty); }
            set { SetValue(IsAutoUniformProperty, value); }
        }
        public static readonly DependencyProperty IsAutoUniformProperty = DependencyProperty.Register("IsAutoUniform", typeof(Boolean), typeof(UniformWrapPanel),
            new FrameworkPropertyMetadata(true, new PropertyChangedCallback(IsAutoUniformChanged)));

        /// <summary>
        /// If this changes we can force the panel to redraw itself and call the MeasureOverride function again.
        /// </summary>
        private static void IsAutoUniformChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            if (sender is UniformWrapPanel)
            {
                ((UniformWrapPanel)sender).InvalidateVisual();
            }
        }

        /// <summary>
        /// The main function for the panel, this makes sure that is the IsAutoUniform is true
        /// it gets the item width and item height for the panel
        /// </summary>
        protected override Size MeasureOverride(Size availableSize)
        {
            if (Children.Count > 0 && IsAutoUniform == true)
            {
                if (Orientation == Orientation.Horizontal)
                {
                    double totalWidth = availableSize.Width;
                    ItemWidth = double.MaxValue;
                    foreach (UIElement element in Children)
                    {
                        element.Measure(availableSize);
                        Size next = element.DesiredSize;

                        if (!(Double.IsInfinity(next.Width) || Double.IsNaN(next.Width)))
                        {
                            ItemWidth = System.Math.Min(next.Width, ItemWidth);
                        }
                    }
                }
                else
                {
                    double totalHeight = availableSize.Height;
                    ItemHeight = 0.0;
                    foreach (UIElement element in Children)
                    {
                        element.Measure(availableSize);
                        Size next = element.DesiredSize;

                        if (!(Double.IsInfinity(next.Height) || Double.IsNaN(next.Height)))
                        {
                            ItemHeight = System.Math.Max(next.Height, ItemHeight);
                        }
                    }
                }
            }
            return base.MeasureOverride(availableSize);
        }

        /// <summary>
        /// Arranges the children so that they are in a uniform grid with equal spacing between them
        /// </summary>
        protected override Size ArrangeOverride(Size finalSize)
        {
            if (Children.Count > 0 && IsAutoUniform == true)
            {
                int itemDepth = (int)System.Math.Floor(finalSize.Width / ItemWidth);
                int rowCount = (int)System.Math.Ceiling(this.Children.Count / (double)itemDepth);
                double leftOverSpace = finalSize.Width - (itemDepth * ItemWidth);
                double margin = leftOverSpace  / itemDepth;
                double rowOffset = ItemWidth + margin;

                // Sort the elements into rows
                Dictionary<int, List<UIElement>> sortedRowElements = new Dictionary<int, List<UIElement>>();
                foreach (UIElement element in Children)
                {
                    int elementIndex = Children.IndexOf(element);
                    int rowIndex = (int)System.Math.Floor((double)(elementIndex / itemDepth));
                    int columnIndex = elementIndex - (rowIndex * itemDepth);

                    if (!sortedRowElements.ContainsKey(rowIndex))
                    {
                        sortedRowElements.Add(rowIndex, new List<UIElement>());
                    }
                    sortedRowElements[rowIndex].Add(element);
                }

                // Get the greatest item height per row
                List<double> rowHeights = new List<double>();
                int index = 0;
                for (int i = 0; i < sortedRowElements.Count; i++)
                {
                    List<UIElement> rowElements = sortedRowElements[i];
                    rowHeights.Add(0.0);
                    foreach (UIElement rowElement in rowElements)
                    {
                        Size next = rowElement.DesiredSize;

                        if (!(Double.IsInfinity(next.Height) || Double.IsNaN(next.Height)))
                        {
                            rowHeights[index] = System.Math.Max(next.Height, rowHeights[index]);
                        }
                    }

                    index++;
                }

                foreach (UIElement element in Children)
                {
                    int elementIndex = Children.IndexOf(element);
                    int columnIndex = (int)System.Math.Floor((double)(elementIndex / itemDepth));
                    int rowIndex = elementIndex - (columnIndex * itemDepth);
                    
                    double elementPointX = rowIndex * rowOffset;
                    double elementPointY = 0.0;
                    for (int i = 0; i < columnIndex; i++)
                    {
                        elementPointY += rowHeights[i];
                    }
                    element.Arrange(new Rect(new Point(elementPointX, elementPointY), new Size(element.DesiredSize.Width, element.DesiredSize.Height)));

                }
                return finalSize;
            }
            return base.ArrangeOverride(finalSize);


        }

    }
}
