﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using System.Windows.Interop;
using System.Runtime.InteropServices;


namespace RSG.Base.Windows.Dialogs
{
    /// <summary>
    /// A WPF-based MessageBox; with support for timing out a particular
    /// dialog result button press.
    /// </summary>
    public partial class CustomMessageBox : Window
    {
        #region Properties
        /// <summary>
        /// Dialog result; describing which button was clicked/timed-out.
        /// </summary>
        public MessageBoxResult Result
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        internal CustomMessageBox(CustomMessageBoxViewModel vm)
        {
            InitializeComponent();

            this.DataContext = vm;
            this.DisplayButtons(vm.Button);
            this.DisplayImage(vm.Image);
            this.SetupTimeout(vm.DefaultResult, vm.Timeout);
        }
        #endregion // Constructor(s)

        #region Static Controller Methods
        /// <summary>
        /// Show message box.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="title"></param>
        /// <param name="buttons"></param>
        /// <param name="icon"></param>
        /// <param name="defaultResult"></param>
        /// <returns></returns>
        public static MessageBoxResult Show(String message, String title, 
            MessageBoxButton buttons, MessageBoxImage icon,
            MessageBoxResult defaultResult)
        {
            return (CustomMessageBox.Show(null, message, title, buttons, icon,
                defaultResult, 0));
        }

        /// <summary>
        /// Show message box.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="title"></param>
        /// <param name="buttons"></param>
        /// <param name="icon"></param>
        /// <param name="defaultResult"></param>
        /// <param name="defaultResultTimeout"></param>
        /// <returns></returns>
        public static MessageBoxResult Show(String message, String title, 
            MessageBoxButton buttons, MessageBoxImage icon,
            MessageBoxResult defaultResult, uint defaultResultTimeout)
        {
            return (CustomMessageBox.Show(null, message, title, buttons, icon,
                defaultResult, defaultResultTimeout));
        }

        /// <summary>
        /// Show message box.
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="message"></param>
        /// <param name="title"></param>
        /// <param name="buttons"></param>
        /// <param name="icon"></param>
        /// <param name="defaultResult"></param>
        /// <returns></returns>
        public static MessageBoxResult Show(Window owner, String message,
            String title, MessageBoxButton buttons, MessageBoxImage icon,
            MessageBoxResult defaultResult)
        {
            return (CustomMessageBox.Show(owner, message, title, buttons, icon,
                defaultResult, 0));
        }

        /// <summary>
        /// Show message box.
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="message"></param>
        /// <param name="title"></param>
        /// <param name="buttons"></param>
        /// <param name="icon"></param>
        /// <param name="defaultResult"></param>
        /// <param name="timeout">Timeout in seconds.</param>
        /// <returns></returns>
        public static MessageBoxResult Show(Window owner, String message,
            String title, MessageBoxButton buttons, MessageBoxImage image,
            MessageBoxResult defaultResult, uint timeout)
        {
            CustomMessageBoxViewModel vm = new CustomMessageBoxViewModel(title,
                message, buttons, image, defaultResult, timeout);
            CustomMessageBox messageBox = new CustomMessageBox(vm);
            bool? result = messageBox.ShowDialog();
            if (result.HasValue)
            {
                return (messageBox.Result);
            }
            return (defaultResult);
        }
        #endregion // Static Controller Methods

        #region Private Methods
        private void Button_OK_Click(Object sender, RoutedEventArgs e)
        {
            Result = MessageBoxResult.OK;
            this.Close();
        }

        private void Button_Cancel_Click(Object sender, RoutedEventArgs e)
        {
            Result = MessageBoxResult.Cancel;
            this.Close();
        }

        private void Button_Yes_Click(Object sender, RoutedEventArgs e)
        {
            Result = MessageBoxResult.Yes;
            this.Close();
        }

        private void Button_No_Click(Object sender, RoutedEventArgs e)
        {
            Result = MessageBoxResult.No;
            this.Close();
        }     

        /// <summary>
        /// Toggle display of correct buttons.
        /// </summary>
        /// <param name="button"></param>
        private void DisplayButtons(MessageBoxButton button)
        {
            switch (button)
            {
                case MessageBoxButton.OKCancel:
                    // Hide all but OK, Cancel
                    Button_OK.Visibility = System.Windows.Visibility.Visible;
                    Button_OK.Focus();
                    Button_Cancel.Visibility = System.Windows.Visibility.Visible;

                    Button_Yes.Visibility = System.Windows.Visibility.Collapsed;
                    Button_No.Visibility = System.Windows.Visibility.Collapsed;
                    break;
                case MessageBoxButton.YesNo:
                    // Hide all but Yes, No
                    Button_Yes.Visibility = System.Windows.Visibility.Visible;
                    Button_Yes.Focus();
                    Button_No.Visibility = System.Windows.Visibility.Visible;

                    Button_OK.Visibility = System.Windows.Visibility.Collapsed;
                    Button_Cancel.Visibility = System.Windows.Visibility.Collapsed;
                    break;
                case MessageBoxButton.YesNoCancel:
                    // Hide only OK
                    Button_Yes.Visibility = System.Windows.Visibility.Visible;
                    Button_Yes.Focus();
                    Button_No.Visibility = System.Windows.Visibility.Visible;
                    Button_Cancel.Visibility = System.Windows.Visibility.Visible;

                    Button_OK.Visibility = System.Windows.Visibility.Collapsed;
                    break;
                default:
                    // Hide all but OK
                    Button_OK.Visibility = System.Windows.Visibility.Visible;
                    Button_OK.Focus();

                    Button_Yes.Visibility = System.Windows.Visibility.Collapsed;
                    Button_No.Visibility = System.Windows.Visibility.Collapsed;
                    Button_Cancel.Visibility = System.Windows.Visibility.Collapsed;
                    break;
            }
        }

        /// <summary>
        /// Display correct image.
        /// </summary>
        /// <param name="image"></param>
        private void DisplayImage(MessageBoxImage image)
        {
            Icon icon;

            switch (image)
            {
                case MessageBoxImage.Exclamation:       // Enumeration value 48 - also covers "Warning"
                    icon = SystemIcons.Exclamation;
                    break;
                case MessageBoxImage.Error:             // Enumeration value 16, also covers "Hand" and "Stop"
                    icon = SystemIcons.Hand;
                    break;
                case MessageBoxImage.Information:       // Enumeration value 64 - also covers "Asterisk"
                    icon = SystemIcons.Information;
                    break;
                case MessageBoxImage.Question:
                    icon = SystemIcons.Question;
                    break;
                default:
                    icon = SystemIcons.Information;
                    break;
            }

            Image_MessageBox.Source = icon.ToImageSource();
            Image_MessageBox.Visibility = System.Windows.Visibility.Visible;
        }

        /// <summary>
        /// Setup button timeout.
        /// </summary>
        /// <param name="result"></param>
        /// <param name="timeout"></param>
        private void SetupTimeout(MessageBoxResult result, uint timeout)
        {
            switch (result)
            {
                case MessageBoxResult.OK:
                    Button_OK.Timeout = timeout;
                    Button_OK.Start();
                    break;
                case MessageBoxResult.Cancel:
                    Button_Cancel.Timeout = timeout;
                    Button_Cancel.Start();
                    break;
                case MessageBoxResult.Yes:
                    Button_Yes.Timeout = timeout;
                    Button_Yes.Start();
                    break;
                case MessageBoxResult.No:
                    Button_No.Timeout = timeout;
                    Button_No.Start();
                    break;
                case MessageBoxResult.None:
                default:
                    // Do nothing.
                    break;
            }
        }
        #endregion // Private Methods
    }
    
    /// <summary>
    /// 
    /// </summary>
    internal static class IconExtensions
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="icon"></param>
        /// <returns></returns>
        internal static ImageSource ToImageSource(this Icon icon)
        {
            ImageSource imageSource = Imaging.CreateBitmapSourceFromHIcon(
                icon.Handle,
                Int32Rect.Empty,
                BitmapSizeOptions.FromEmptyOptions());

            return imageSource;
        }
    }

} // RSG.Base.Windows.Dialogs namespace
