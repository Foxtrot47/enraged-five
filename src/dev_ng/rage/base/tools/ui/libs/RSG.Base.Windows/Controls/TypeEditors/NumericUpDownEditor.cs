﻿using System;
using System.Activities.Presentation.Converters;
using System.Activities.Presentation.Model;
using System.Activities.Presentation.PropertyEditing;
using System.IO;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace RSG.Base.Windows.Controls.TypeEditors
{

    /// <summary>
    /// 
    /// </summary>
    public class NumericUpDownEditor : PropertyValueEditor
    {
        #region Constants
        /// <summary>
        /// 
        /// </summary>
        private const String TEMPLATE_NORMAL =
            @"
            <DataTemplate 
                    xmlns='http://schemas.microsoft.com/winfx/2006/xaml/presentation'
                    xmlns:x='http://schemas.microsoft.com/winfx/2006/xaml'
                    xmlns:pe='clr-namespace:System.Activities.Presentation.PropertyEditing;assembly=System.Activities.Presentation'
                    xmlns:self='clr-namespace:RSG.Base.Windows.Controls;assembly=RSG.Base.Windows'>

                <self:NumericUpDown Value='{Binding Value}' />
            </DataTemplate>
            ";
        #endregion // Constants

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public NumericUpDownEditor()
            : base()
        {
            // Load templates
            using (var sr = new MemoryStream(Encoding.UTF8.GetBytes(TEMPLATE_NORMAL)))
            {
                this.InlineEditorTemplate = XamlReader.Load(sr) as DataTemplate;
            }       
        }
        #endregion // Constructor(s)
    }

} // RSG.Base.Windows.Controls.TypeEditors namespace
