﻿using System;
using System.ComponentModel;
using RSG.Base.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using RSG.Base.Windows.Controls.WpfViewport2D.Actions;

namespace RSG.Base.Windows.Controls.WpfViewport2D
{
    /// <summary>
    /// A base class to user controls that want to be attached
    /// to a action manager.
    /// </summary>
    public class ActionElement : 
        System.Windows.Controls.UserControl, INotifyPropertyChanged
    {
        #region Events

        /// <summary>
        /// Property changed event fired when the name changes so that the 
        /// binding can be dynamic.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Create the OnPropertyChanged method to raise the event
        /// </summary>
        /// <param name="name"></param>
        protected void OnPropertyChanged(String name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

        #endregion // Events

        /// <summary>
        /// The action manager that is attached to this action element
        /// </summary>
        public ActionManager ActionManager
        {
            get { return m_actionManager; }
            set { m_actionManager = value; }
        }
        private ActionManager m_actionManager;

        /// <summary>
        /// Default Constructor, creates the action manager
        /// and hooks up the necessary events
        /// </summary>
        public ActionElement()
        {
            this.ActionManager = new ActionManager();
            this.ActionManager.RegisterActionManagerWithActionElement(this);
        }

        public void RegisterActionElementControl(ActionElementControl control)
        {
            if (this.ActionManager != null)
            {
                this.ActionManager.RegisterActionElementControl(control);
            }
        }

        public void UnRegisterActionElementControl(ActionElementControl control)
        {
            if (this.ActionManager != null)
            {
                this.ActionManager.UnRegisterActionElementControl(control);
            }
        }

    }
}
