﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Windows;
using System.Windows.Controls;
using System.Linq;
using System.Text;
using RSG.Base.Windows.DragDrop.Utilities;

namespace RSG.Base.Windows.DragDrop
{
    /// <summary>
    /// 
    /// </summary>
    public class DropInfo
    {
        #region Members

        private Object m_data;
        private DragInfo m_dragInfo;
        private DragDropEffects m_effects;
        private int m_insertIndex;
        private IEnumerable m_targetCollection;
        private Object m_targetItem;
        private UIElement m_visualTarget;
        private UIElement m_visualTargetItem;
        private Orientation m_visualTargetOrientation;

        #endregion // Members

        #region Properties

        /// <summary>
        /// The actual data that is being dragged
        /// </summary>
        public Object Data
        {
            get { return m_data; }
            private set { m_data = value; }
        }

        /// <summary>
        /// The drag info structure
        /// </summary>
        public DragInfo DragInfo
        {
            get { return m_dragInfo; }
            private set { m_dragInfo = value; }
        }

        /// <summary>
        /// Specifies the possible effects of this drag-and-drop operation.
        /// </summary>
        public DragDropEffects Effects
        {
            get { return m_effects; }
            set { m_effects = value; }
        }

        /// <summary>
        /// The index at which the dragged data is being inserted at
        /// </summary>
        public int InsertIndex
        {
            get { return m_insertIndex; }
            private set { m_insertIndex = value; }
        }

        /// <summary>
        /// The collection the the drag data is being dropped into
        /// </summary>
        public IEnumerable TargetCollection
        {
            get { return m_targetCollection; }
            private set { m_targetCollection = value; }
        }

        /// <summary>
        /// The target item that is getting the dragged event
        /// </summary>
        public Object TargetItem
        {
            get { return m_targetItem; }
            private set { m_targetItem = value; }
        }

        /// <summary>
        /// The visual ui element that owns the items source that
        /// the dragged data is getting dropped onto
        /// </summary>
        public UIElement VisualTarget
        {
            get { return m_visualTarget; }
            private set { m_visualTarget = value; }
        }

        /// <summary>
        ///  The ui element that is getting the dragged event
        /// </summary>
        public UIElement VisualTargetItem
        {
            get { return m_visualTargetItem; }
            private set { m_visualTargetItem = value; }
        }

        /// <summary>
        /// The orientation of the panel that is
        /// getting the dragged event
        /// </summary>
        public Orientation VisualTargetOrientation
        {
            get { return m_visualTargetOrientation; }
            private set { m_visualTargetOrientation = value; }
        }

        #endregion // Properties
        
        #region Constructors

        /// <summary>
        /// 
        /// </summary>
        public DropInfo(Object sender, DragEventArgs e, DragInfo dragInfo, string format)
        {
            this.Data = (e.Data.GetDataPresent(format)) ? e.Data.GetData(format) : e.Data;
            this.DragInfo = dragInfo;
            this.VisualTarget = sender as UIElement;

            if (sender is ItemsControl)
            {
                ItemsControl itemsControl = (ItemsControl)sender;
                UIElement item = itemsControl.GetItemContainerAt(e.GetPosition(itemsControl));

                VisualTargetOrientation = itemsControl.GetItemsPanelOrientation();

                if (item != null)
                {
                    ItemsControl itemParent = ItemsControl.ItemsControlFromItemContainer(item);

                    InsertIndex = itemParent.ItemContainerGenerator.IndexFromContainer(item);
                    TargetCollection = itemParent.ItemsSource ?? itemParent.Items;
                    TargetItem = itemParent.ItemContainerGenerator.ItemFromContainer(item);
                    VisualTargetItem = item;

                    if (VisualTargetOrientation == Orientation.Vertical)
                    {
                        if (e.GetPosition(item).Y > item.RenderSize.Height / 2) InsertIndex++;
                    }
                    else
                    {
                        if (e.GetPosition(item).X > item.RenderSize.Width / 2) InsertIndex++;
                    }
                }
                else
                {
                    TargetCollection = itemsControl.ItemsSource ?? itemsControl.Items;
                    InsertIndex = itemsControl.Items.Count;
                }
            }
        }

        #endregion // Constructors
    } // DropInfo
} // RSG.Base.Windows.DragDrop
