﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Timers;
using System.Windows.Threading;

namespace RSG.Base.Windows.Controls
{
    public class CountdownButton : Button
    {
        /// <summary>
        /// 
        /// </summary>
        private int _countDownValue;

        /// <summary>
        /// 
        /// </summary>
        private int _currentValue;

        /// <summary>
        /// 
        /// </summary>
        private DispatcherTimer _timer;

//         /// <summary>
//         /// 
//         /// </summary>
//         private DateTime _initTime;

        /// <summary>
        /// 
        /// </summary>
        private string _label;

        public string Label
        {
            set { _label = value; }
            get { return _label; }
        }

        /// <summary>
        /// 
        /// </summary>
        public event EventHandler TimeElapsed;

        public int CountDownValue
        {
            get { return _countDownValue; }
            set
            {
                _countDownValue = value;
                _currentValue = value;
                base.Content = _label + " (" + (value.ToString()) +")";
                _timer = new DispatcherTimer();
                _timer.Interval = TimeSpan.FromSeconds(1);
                _timer.Tick += _timer_tick;
                _timer.Start();
            }
        }

//         public int CurrentValue
//         {
//             get { return Integer.Parse(_timer.ToString())/1000; }
//         }

        public bool Active
        {
            get { return _timer.IsEnabled; }
            set { _timer.IsEnabled = value; }
        }

//         public CountdownButton()
//             : base()
//         {
//         }
// 
        void _timer_tick(object sender, EventArgs e)
        {
            _currentValue --;
            base.Content = _label + " (" + (_currentValue.ToString()) + ")";
            if (_currentValue<=0)
            {
                EventHandler handler = TimeElapsed as EventHandler;
                if (null != handler)
                    handler(sender, e);
                _timer.Stop();
            }
        }

    }
}
