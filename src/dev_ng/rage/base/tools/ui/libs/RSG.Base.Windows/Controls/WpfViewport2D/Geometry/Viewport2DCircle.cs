﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using RSG.Base.Windows.Controls.WpfViewport2D.Geometry;
using RSG.Base.Windows.Controls.WpfViewport2D.Actions;
using RSG.Base.Math;

namespace RSG.Base.Windows.Controls.WpfViewport2D.Geometry
{
    public class Viewport2DCircle
        : Viewport2DGeometry
    {
        #region Properties

        /// <summary>
        /// Represents the center of the circle
        /// </summary>
        public Vector2f Center
        {
            get { return m_center; }
            set { m_center = value; }
        }
        private Vector2f m_center;

        /// <summary>
        /// Represents the radius of the circle
        /// </summary>
        public float Radius
        {
            get { return m_radius; }
            set { m_radius = value; }
        }
        private float m_radius;

        /// <summary>
        /// Represents the width of the ellipse
        /// </summary>
        public float Width
        {
            get { return Radius * 2.0f; }
        }

        /// <summary>
        /// Represents the height of the ellipse
        /// </summary>
        public float Height
        {
            get { return Radius * 2.0f; }
        }

        /// <summary>
        /// Outline colour
        /// </summary>
        public Color OutlineColour
        {
            get { return m_outlineColour; }
            set { m_outlineColour = value; }
        }
        private Color m_outlineColour;

        /// <summary>
        /// Fill colour
        /// </summary>
        public Color FillColour
        {
            get { return m_fillColour; }
            set { m_fillColour = value; }
        }
        private Color m_fillColour;

        /// <summary>
        /// Outline width
        /// </summary>
        public int OutlineWidth
        {
            get { return m_outlineWidth; }
            set { m_outlineWidth = value; }
        }
        private int m_outlineWidth;

        /// <summary>
        /// Fill flag, fills the shape in the given fill brush
        /// </summary>
        public Boolean Fill
        {
            get { return m_fill; }
            set { m_fill = value; }
        }
        private Boolean m_fill;

        /// <summary>
        /// Outline flag, repressents whether the shape should be drawn with a outline
        /// </summary>
        public Boolean Outline
        {
            get { return m_outline; }
            set { m_outline = value; }
        }
        private Boolean m_outline;

        #endregion // Properties

        #region Constructor(s)

        /// <summary>
        /// Constructor that constructs a circle that only has a outline
        /// </summary>
        public Viewport2DCircle(etCoordSpace space, String name, Vector2f center, float radius, Color outlineColour, int outlineWidth)
            : base(space, name)
        {
            this.Center = center;
            this.Radius = radius;
            this.OutlineColour = outlineColour;
            this.OutlineWidth = outlineWidth;
            this.Fill = false;
            this.Outline = true;
        }

        /// <summary>
        /// Constructor that constructs a circle that only has a fill color
        /// </summary>
        public Viewport2DCircle(etCoordSpace space, String name, Vector2f center, float radius, Color fillColour)
            : base(space, name)
        {
            this.Center = center;
            this.Radius = radius;
            this.Fill = true;
            this.Outline = false;
            this.FillColour = fillColour;
        }

        #endregion // Constructor(s)

        #region Overrides

        /// <summary>
        /// Return bounding box for 2D geometry in world-space.
        /// </summary>
        public override BoundingBox2f GetWorldBoundingBox(Viewport2D viewport)
        {
            BoundingBox2f bbox = new BoundingBox2f();
            if (etCoordSpace.World == this.CoordinateSpace)
            {
                bbox.Expand(this.Center);
                bbox.Expand(new Vector2f(this.Center.X, this.Center.Y + this.Radius));
                bbox.Expand(new Vector2f(this.Center.X, this.Center.Y - this.Radius));
                bbox.Expand(new Vector2f(this.Center.X + this.Radius, this.Center.Y));
                bbox.Expand(new Vector2f(this.Center.X - this.Radius, this.Center.Y));
            }
            else
            {
                throw new NotImplementedException("Invalid coordinate space.");
            }
            return bbox;
        }

        /// <summary>
        /// Determines if the geometry has been picked given the screen
        /// position of the pick
        /// </summary>
        /// <param name="screenPosition"></param>
        /// <returns></returns>
        public override Boolean HasBeenPicked(Point viewPosition, Viewport2D viewport)
        {
            if (this.Fill == true)
            {
                Point world = viewport.ViewToWorld(viewPosition);
                Vector2f worldPosition = new Vector2f((float)world.X, (float)world.Y);

                Vector2f displacement = worldPosition - this.Center;
                double distance = displacement.Magnitude();
                if (distance < this.Radius)
                {
                    return true;
                }
            }
            return false;
        }

        #endregion // Overrides

    }
}
