using System;
using System.Windows.Controls;

namespace RSG.Base.Windows.Extensions
{

    public static class ControlExtensions
    {
        /// <summary>
        /// InvokeAction checks what thread you call it from, and if it's the same as the widget's thread
        /// it will call the action immediately, otherwise it will Invoke the action, so that it gets processed
        /// by the widget's thread eventually.
        /// 
        /// A typical use might be something like this:
        /// widget.InvokeAction(() => widget.Text = "abc");
        /// Call it from any thread, and you won't get an error about invalid access.
        /// </summary>
        /// <param name="widget"></param>
        /// <param name="fn"></param>
        public static void InvokeAction(this Control widget, Action fn)
        {
            if (!widget.Dispatcher.CheckAccess())
            {
                widget.Dispatcher.Invoke(fn);
            }
            else
            {
                fn();
            }
        }

        /// <summary>
        /// InvokeAction checks what thread you call it from, and if it's the same as the widget's thread
        /// it will call the action immediately (and synchronously!), otherwise it will BeginInvoke the action, 
        /// so that it gets processed by the widget's thread eventually.
        /// 
        /// A typical use might be something like this:
        /// widget.BeginInvokeAction(() => widget.Text = "abc");
        /// Call it from any thread, and you won't get an error about invalid access.
        /// 
        /// Use this for "fire-and-forget" actions.
        /// </summary>
        /// <param name="widget"></param>
        /// <param name="fn"></param>
        public static void BeginInvokeAction(this Control widget, Action fn)
        {
            if (!widget.Dispatcher.CheckAccess())
            {
                widget.Dispatcher.BeginInvoke(fn);
            }
            else
            {
                fn();
            }
        }
    }
}