﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using RSG.Base.Math;

namespace RSG.Base.Windows.Controls.WpfViewport2D.Geometry
{
    /// <summary>
    /// 
    /// </summary>
    public class Viewport2DImage : Viewport2DGeometry
    {
        #region Properties and Associated Member Data

        /// <summary>
        /// Image position.
        /// </summary>
        public Point Position
        {
            get { return m_vPosition; }
            set { m_vPosition = value; }
        }
        private Point m_vPosition;

        /// <summary>
        /// Image viewport size in current coordinate space.
        /// </summary>
        public Size Size
        {
            get { return m_vSize; }
            set { m_vSize = value; }
        }
        private Size m_vSize;

        /// <summary>
        /// Image data.
        /// </summary>
        public BitmapImage Image
        {
            get { return m_Image; }
            set { m_Image = value; }
        }
        private BitmapImage m_Image;

        /// <summary>
        /// How the image should be scaled
        /// </summary>
        public BitmapScalingMode BitmapScalingMode
        {
            get;
            set;
        }

        #endregion // Properties and Associated Member Data

        #region Constructor(s)

        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="space"></param>
        /// <param name="image"></param>
        /// <param name="pos"></param>
        /// <param name="size"></param>
        public Viewport2DImage(etCoordSpace space, String name, String filename, Vector2f pos, Vector2f size)
            : base(space, name)
        {
            this.Position = new Point(pos.X, pos.Y);
            this.Size = new Size(size.X, size.Y);
            this.Image = null;
            this.BitmapScalingMode = BitmapScalingMode.Unspecified;

            try
            {
                if (!String.IsNullOrEmpty(filename))
                {
                    BitmapImage source = new BitmapImage();
                    source.BeginInit();
                    source.UriSource = new Uri(filename, UriKind.Absolute);
                    source.EndInit();

                    int stride = 4 * source.PixelWidth;
                    byte[] pixels = new byte[stride * source.PixelHeight];
                    source.CopyPixels(pixels, stride, 0);

                    BitmapSource bitmapSource = BitmapSource.Create(source.PixelWidth, source.PixelHeight, 96.0, 96.0, source.Format, null, pixels, stride);

                    JpegBitmapEncoder encoder = new JpegBitmapEncoder();
                    MemoryStream memoryStream = new MemoryStream();

                    encoder.Frames.Add(BitmapFrame.Create(bitmapSource));
                    encoder.Save(memoryStream);

                    this.Image = new BitmapImage();
                    this.Image.BeginInit();
                    this.Image.StreamSource = new MemoryStream(memoryStream.ToArray());
                    this.Image.EndInit();
     
                    memoryStream.Close();
                }
            }
            catch
            {
                this.Image = null;
            }
        }

        /// <summary>
        /// Same constructor as before but takes a stream for the image instead of a filename
        /// </summary>
        /// <param name="space"></param>
        /// <param name="name"></param>
        /// <param name="imageStream"></param>
        /// <param name="pos"></param>
        /// <param name="size"></param>
        public Viewport2DImage(etCoordSpace space, String name, Stream imageStream, Vector2f pos, Vector2f size)
            : base(space, name)
        {
            this.Position = new Point(pos.X, pos.Y);
            this.Size = new Size(size.X, size.Y);
            this.Image = null;
            this.BitmapScalingMode = BitmapScalingMode.Unspecified;

            try
            {
                if (imageStream != null)
                {
                    BitmapImage source = new BitmapImage();
                    source.BeginInit();
                    source.StreamSource = imageStream;
                    source.EndInit();

                    int stride = 4 * source.PixelWidth;
                    byte[] pixels = new byte[stride * source.PixelHeight];
                    source.CopyPixels(pixels, stride, 0);

                    BitmapSource bitmapSource = BitmapSource.Create(source.PixelWidth, source.PixelHeight, 96.0, 96.0, source.Format, null, pixels, stride);

                    JpegBitmapEncoder encoder = new JpegBitmapEncoder();
                    MemoryStream memoryStream = new MemoryStream();

                    encoder.Frames.Add(BitmapFrame.Create(bitmapSource));
                    encoder.Save(memoryStream);
                    
                    this.Image = new BitmapImage();
                    this.Image.BeginInit();
                    this.Image.StreamSource = new MemoryStream(memoryStream.ToArray());
                    this.Image.EndInit();

                    memoryStream.Close();
                }
            }
            catch
            {
                this.Image = null;
            }
        }

        /// <summary>
        /// Same constructor as before but takes an input bitmap image
        /// </summary>
        /// <param name="space"></param>
        /// <param name="name"></param>
        /// <param name="imageStream"></param>
        /// <param name="pos"></param>
        /// <param name="size"></param>
        public Viewport2DImage(etCoordSpace space, String name, BitmapImage image, Vector2f pos, Vector2f size)
            : base(space, name)
        {
            this.Position = new Point(pos.X, pos.Y);
            this.Size = new Size(size.X, size.Y);
            this.Image = image;
            this.BitmapScalingMode = BitmapScalingMode.Unspecified;
        }

        #endregion // Constructor(s)

        #region Overrides

        /// <summary>
        /// Return bounding box for 2D geometry in world-space.
        /// </summary>
        public override BoundingBox2f GetWorldBoundingBox(Viewport2D viewport)
        {
            BoundingBox2f bbox = new BoundingBox2f();
            if (etCoordSpace.World == this.CoordinateSpace)
            {
                bbox.Expand(new Vector2f((float)this.Position.X, (float)this.Position.Y));
                bbox.Expand(new Vector2f((float)(this.Position.X + this.Size.Width), (float)(this.Position.Y - this.Size.Height)));
            }
            else
            {
                throw new NotImplementedException("Invalid coordinate space.");
            }

            return bbox;
        }

        /// <summary>
        /// Determines if the geometry has been picked given the screen
        /// position of the pick
        /// </summary>
        /// <param name="screenPosition"></param>
        /// <returns></returns>
        public override Boolean HasBeenPicked(Point viewPosition, Viewport2D viewport)
        {
            BoundingBox2f bbox = new BoundingBox2f();
            if (etCoordSpace.World == this.CoordinateSpace)
            {
                bbox.Expand(new Vector2f((float)this.Position.X, (float)this.Position.Y));
                bbox.Expand(new Vector2f((float)(this.Position.X + this.Size.Width), (float)(this.Position.Y - this.Size.Height)));

                Point worldPosition = viewport.ViewToWorld(viewPosition);

                return bbox.Contains(new Vector2f((float)worldPosition.X, (float)worldPosition.Y));
            }
            else
            {
                throw new NotImplementedException("Invalid coordinate space.");
            }      
        }

        #endregion // Overrides

    } // Viewport2DImage
} // RSG.Base.Windows.Controls.WpfViewport2D.Geometry
