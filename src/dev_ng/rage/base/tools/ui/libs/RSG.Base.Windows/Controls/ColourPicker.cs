﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Data;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Media;
using System.Reflection;
using System.Windows.Shapes;

namespace RSG.Base.Windows.Controls
{
    /// <summary>
    /// 
    /// </summary>
    public struct HsvColor
    {
        public double H;
        public double S;
        public double V;

        public HsvColor(double h, double s, double v)
        {
            H = h;
            S = s;
            V = v;
        }
    } // HsvColor

    /// <summary>
    /// 
    /// </summary>
    public static class ColorUtilities
    {
        public static readonly Dictionary<string, Color> KnownColors = GetKnownColors();

        public static string GetColorName(this Color color)
        {
            string colorName = KnownColors.Where(kvp => kvp.Value.Equals(color)).Select(kvp => kvp.Key).FirstOrDefault();

            if (String.IsNullOrEmpty(colorName))
                colorName = color.ToString();

            return colorName;
        }

        private static Dictionary<string, Color> GetKnownColors()
        {
            var colorProperties = typeof(Colors).GetProperties(BindingFlags.Static | BindingFlags.Public);
            return colorProperties.ToDictionary(p => p.Name, p => (Color)p.GetValue(null, null));
        }

        /// <summary>
        /// Converts an RGB color to an HSV color.
        /// </summary>
        /// <param name="r"></param>
        /// <param name="b"></param>
        /// <param name="g"></param>
        /// <returns></returns>
        public static HsvColor ConvertRgbToHsv(int r, int g, int b)
        {
            double delta, min;
            double h = 0, s, v;

            min = System.Math.Min(System.Math.Min(r, g), b);
            v = System.Math.Max(System.Math.Max(r, g), b);
            delta = v - min;

            if (v == 0.0)
            {
                s = 0;
            }
            else
                s = delta / v;

            if (s == 0)
                h = 0.0;

            else
            {
                if (r == v)
                    h = (g - b) / delta;
                else if (g == v)
                    h = 2 + (b - r) / delta;
                else if (b == v)
                    h = 4 + (r - g) / delta;

                h *= 60;
                if (h < 0.0)
                    h = h + 360;

            }

            return new HsvColor { H = h, S = s, V = v / 255 };
        }

        /// <summary>
        ///  Converts an HSV color to an RGB color.
        /// </summary>
        /// <param name="h"></param>
        /// <param name="s"></param>
        /// <param name="v"></param>
        /// <returns></returns>
        public static Color ConvertHsvToRgb(double h, double s, double v)
        {
            double r = 0, g = 0, b = 0;

            if (s == 0)
            {
                r = v;
                g = v;
                b = v;
            }
            else
            {
                int i;
                double f, p, q, t;

                if (h == 360)
                    h = 0;
                else
                    h = h / 60;

                i = (int)System.Math.Truncate(h);
                f = h - i;

                p = v * (1.0 - s);
                q = v * (1.0 - (s * f));
                t = v * (1.0 - (s * (1.0 - f)));

                switch (i)
                {
                    case 0:
                        {
                            r = v;
                            g = t;
                            b = p;
                            break;
                        }
                    case 1:
                        {
                            r = q;
                            g = v;
                            b = p;
                            break;
                        }
                    case 2:
                        {
                            r = p;
                            g = v;
                            b = t;
                            break;
                        }
                    case 3:
                        {
                            r = p;
                            g = q;
                            b = v;
                            break;
                        }
                    case 4:
                        {
                            r = t;
                            g = p;
                            b = v;
                            break;
                        }
                    default:
                        {
                            r = v;
                            g = p;
                            b = q;
                            break;
                        }
                }

            }

            return Color.FromArgb(255, (byte)(r * 255), (byte)(g * 255), (byte)(b * 255));
        }

        /// <summary>
        /// Generates a list of colors with hues ranging from 0 360 and a saturation and value of 1. 
        /// </summary>
        /// <returns></returns>
        public static List<Color> GenerateHsvSpectrum()
        {
            List<Color> colorsList = new List<Color>(8);

            for (int i = 0; i < 29; i++)
            {
                colorsList.Add(ColorUtilities.ConvertHsvToRgb(i * 12, 1, 1));
            }

            colorsList.Add(ColorUtilities.ConvertHsvToRgb(0, 1, 1));

            return colorsList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="startHue"></param>
        /// <param name="endHue"></param>
        /// <returns></returns>
        public static IEnumerable<Color> GenerateHsvSpectrum(double startHue, double endHue)
        {
            int start = (int)(startHue * 36);
            int end = (int)(endHue * 36);

            for (int i = start; i <= end; ++i)
            {
                yield return ColorUtilities.ConvertHsvToRgb(i * 10, 1, 1);
            }
        }
    } // ColorUtilities

    /// <summary>
    /// 
    /// </summary>
    public class ColorSpectrumSlider : Slider
    {
        #region Private Members

        private Rectangle _spectrumDisplay;
        private LinearGradientBrush _pickerBrush;

        #endregion //Private Members

        #region Constructors

        static ColorSpectrumSlider()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(ColorSpectrumSlider), new FrameworkPropertyMetadata(typeof(ColorSpectrumSlider)));
        }

        #endregion //Constructors

        #region Dependency Properties

        public static readonly DependencyProperty SelectedColorProperty = DependencyProperty.Register("SelectedColor", typeof(Color), typeof(ColorSpectrumSlider), new FrameworkPropertyMetadata(System.Windows.Media.Colors.Transparent, FrameworkPropertyMetadataOptions.AffectsRender | FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, new PropertyChangedCallback(OnSelectedColorChanged)));
        public Color SelectedColor
        {
            get { return (Color)GetValue(SelectedColorProperty); }
            set { SetValue(SelectedColorProperty, value); }
        }

        public bool isUpdatingValue = false;
        /// <summary>
        /// 
        /// </summary>
        private static void OnSelectedColorChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            ColorSpectrumSlider slider = (ColorSpectrumSlider)sender;
            Color newColor = (Color)e.NewValue;

            if (!slider.isUpdatingValue)
            {
                slider.Value = (int)ColorUtilities.ConvertRgbToHsv(newColor.R, newColor.G, newColor.B).H;
            }
        }

        public static readonly DependencyProperty StartHueProperty = DependencyProperty.Register("StartHue", typeof(double), typeof(ColorSpectrumSlider), new FrameworkPropertyMetadata(0.0, FrameworkPropertyMetadataOptions.AffectsRender, new PropertyChangedCallback(OnStartHueChanged)));
        public double StartHue
        {
            get { return (double)GetValue(StartHueProperty); }
            set { SetValue(StartHueProperty, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        private static void OnStartHueChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            ColorSpectrumSlider slider = (ColorSpectrumSlider)sender;
            double value = (double)e.NewValue;
            slider.Minimum = value * 360;
        }

        public static readonly DependencyProperty EndHueProperty = DependencyProperty.Register("EndHue", typeof(double), typeof(ColorSpectrumSlider), new FrameworkPropertyMetadata(1.0, FrameworkPropertyMetadataOptions.AffectsRender, new PropertyChangedCallback(OnEndHueChanged)));
        public double EndHue
        {
            get { return (double)GetValue(EndHueProperty); }
            set { SetValue(EndHueProperty, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        private static void OnEndHueChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            ColorSpectrumSlider slider = (ColorSpectrumSlider)sender;
            double value = (double)e.NewValue;
            slider.Maximum = value * 360;
        }

        #endregion //Dependency Properties

        #region Base Class Overrides

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            _spectrumDisplay = (Rectangle)GetTemplateChild("PART_SpectrumDisplay");
            CreateSpectrum();
            OnValueChanged(Double.NaN, Value);
        }

        protected override void OnValueChanged(double oldValue, double newValue)
        {
            isUpdatingValue = true;
            base.OnValueChanged(oldValue, newValue);

            SelectedColor = ColorUtilities.ConvertHsvToRgb(newValue, 1, 1);
            isUpdatingValue = false;
        }

        #endregion //Base Class Overrides

        #region Methods

        private void CreateSpectrum()
        {
            _pickerBrush = new LinearGradientBrush();

            if (Orientation == Orientation.Vertical)
            {
                _pickerBrush.StartPoint = new Point(0.5, 1.0);
                _pickerBrush.EndPoint = new Point(0.5, 0.0);
            }
            else
            {
                _pickerBrush.StartPoint = new Point(0.0, 0.5);
                _pickerBrush.EndPoint = new Point(1.0, 0.5);
            }
            _pickerBrush.ColorInterpolationMode = ColorInterpolationMode.SRgbLinearInterpolation;

            List<Color> colorsList = ColorUtilities.GenerateHsvSpectrum(StartHue, EndHue).ToList();

            double stopIncrement = 1.0 / colorsList.Count;

            int i;
            for (i = 0; i < colorsList.Count; i++)
            {
                _pickerBrush.GradientStops.Add(new GradientStop(colorsList[i], i * stopIncrement));
            }

            _pickerBrush.GradientStops[i - 1].Offset = 1.0;
            _spectrumDisplay.Fill = _pickerBrush;
        }

        #endregion //Methods
    } // ColorSpectrumSlider

    /// <summary>
    /// 
    /// </summary>
    public class ColorCanvas : Control
    {
        #region Private Members

        private TranslateTransform _colorShadeSelectorTransform = new TranslateTransform();
        private Canvas _colorShadingCanvas;
        private Canvas _colorShadeSelector;
        private ColorSpectrumSlider _spectrumSlider;
        private Point? _currentColorPosition;
        private bool _supressPropertyChanged;
        private bool _dragInProgress = false;

        #endregion //Private Members

        #region Properties

        #region SelectedColor

        public static readonly DependencyProperty SelectedColorProperty = DependencyProperty.Register("SelectedColor", typeof(Color), 
            typeof(ColorCanvas), new FrameworkPropertyMetadata(Colors.Black, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, OnSelectedColorChanged));
        public Color SelectedColor
        {
            get { return (Color)GetValue(SelectedColorProperty); }
            set { SetValue(SelectedColorProperty, value); }
        }

        private static void OnSelectedColorChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            ColorCanvas colorCanvas = o as ColorCanvas;
            if (colorCanvas != null)
                colorCanvas.OnSelectedColorChanged((Color)e.OldValue, (Color)e.NewValue);
        }

        protected virtual void OnSelectedColorChanged(Color oldValue, Color newValue)
        {
            RoutedPropertyChangedEventArgs<Color> args = new RoutedPropertyChangedEventArgs<Color>(oldValue, newValue);
            args.RoutedEvent = SelectedColorChangedEvent;
            RaiseEvent(args);
        }

        #endregion //SelectedColor

        #region InternalColor

        public static readonly DependencyProperty InternalColorProperty = DependencyProperty.Register("InternalColor", typeof(Color),
            typeof(ColorCanvas), new FrameworkPropertyMetadata(Colors.Black, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, OnInternalColorChanged));
        public Color InternalColor
        {
            get { return (Color)GetValue(InternalColorProperty); }
            protected set { SetValue(InternalColorProperty, value); }
        }

        private static void OnInternalColorChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            ColorCanvas colorCanvas = o as ColorCanvas;
            if (colorCanvas != null)
                colorCanvas.OnInternalColorChanged((Color)e.OldValue, (Color)e.NewValue);
        }

        protected virtual void OnInternalColorChanged(Color oldValue, Color newValue)
        {
            HexadecimalString = newValue.ToString();
            UpdateRGBValues(newValue);
            UpdateColorShadeSelectorPosition(newValue);

            if (!_dragInProgress)
            {
                SelectedColor = newValue;
            }
        }

        #endregion // Internal Color

        #region ARGB

        #region A

        public static readonly DependencyProperty AProperty = DependencyProperty.Register("A", typeof(byte), typeof(ColorCanvas), new UIPropertyMetadata((byte)255, OnAChanged));
        public byte A
        {
            get { return (byte)GetValue(AProperty); }
            set { SetValue(AProperty, value); }
        }

        private static void OnAChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            ColorCanvas colorCanvas = o as ColorCanvas;
            if (colorCanvas != null)
                colorCanvas.OnAChanged((byte)e.OldValue, (byte)e.NewValue);
        }

        protected virtual void OnAChanged(byte oldValue, byte newValue)
        {
            if (!_supressPropertyChanged)
                UpdateInternalColor();
        }

        #endregion //A

        #region R

        public static readonly DependencyProperty RProperty = DependencyProperty.Register("R", typeof(byte), typeof(ColorCanvas), new UIPropertyMetadata((byte)0, OnRChanged));
        public byte R
        {
            get { return (byte)GetValue(RProperty); }
            set { SetValue(RProperty, value); }
        }

        private static void OnRChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            ColorCanvas colorCanvas = o as ColorCanvas;
            if (colorCanvas != null)
                colorCanvas.OnRChanged((byte)e.OldValue, (byte)e.NewValue);
        }

        protected virtual void OnRChanged(byte oldValue, byte newValue)
        {
            if (!_supressPropertyChanged)
                UpdateInternalColor();
        }

        #endregion //R

        #region G

        public static readonly DependencyProperty GProperty = DependencyProperty.Register("G", typeof(byte), typeof(ColorCanvas), new UIPropertyMetadata((byte)0, OnGChanged));
        public byte G
        {
            get { return (byte)GetValue(GProperty); }
            set { SetValue(GProperty, value); }
        }

        private static void OnGChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            ColorCanvas colorCanvas = o as ColorCanvas;
            if (colorCanvas != null)
                colorCanvas.OnGChanged((byte)e.OldValue, (byte)e.NewValue);
        }

        protected virtual void OnGChanged(byte oldValue, byte newValue)
        {
            if (!_supressPropertyChanged)
                UpdateInternalColor();
        }

        #endregion //G

        #region B

        public static readonly DependencyProperty BProperty = DependencyProperty.Register("B", typeof(byte), typeof(ColorCanvas), new UIPropertyMetadata((byte)0, OnBChanged));
        public byte B
        {
            get { return (byte)GetValue(BProperty); }
            set { SetValue(BProperty, value); }
        }

        private static void OnBChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            ColorCanvas colorCanvas = o as ColorCanvas;
            if (colorCanvas != null)
                colorCanvas.OnBChanged((byte)e.OldValue, (byte)e.NewValue);
        }

        protected virtual void OnBChanged(byte oldValue, byte newValue)
        {
            if (!_supressPropertyChanged)
                UpdateInternalColor();
        }

        #endregion //B

        #endregion // ARGB

        #region HexadecimalString

        public static readonly DependencyProperty HexadecimalStringProperty = DependencyProperty.Register("HexadecimalString", typeof(String), 
            typeof(ColorCanvas), new UIPropertyMetadata("#FFFFFFFF", OnHexadecimalStringChanged));
        public string HexadecimalString
        {
            get { return (string)GetValue(HexadecimalStringProperty); }
            set { SetValue(HexadecimalStringProperty, value); }
        }

        private static void OnHexadecimalStringChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            ColorCanvas colorCanvas = o as ColorCanvas;
            if (colorCanvas != null)
                colorCanvas.OnHexadecimalStringChanged((string)e.OldValue, (string)e.NewValue);
        }

        protected virtual void OnHexadecimalStringChanged(String oldValue, String newValue)
        {
            if (InternalColor.ToString().Equals(newValue))
                return;

            // Make sure that the string is a valid hexidecimal
            String hexString = String.Copy(newValue);
            if (!hexString.StartsWith("#"))
            {
                hexString = "#" + hexString;
            }
            if (hexString.Length != 9)
            {
                int insertCount = 9 - hexString.Length;
                for (int i = 0; i < insertCount; i++)
                {
                    hexString = hexString.Insert(1, "0");
                }
            }
            if (hexString != newValue)
            {
                HexadecimalString = hexString;
                return;
            }
            Color oldColour = this.InternalColor;
            UpdateInternalColor((Color)ColorConverter.ConvertFromString(newValue));
            if (InternalColor.Equals(oldColour))
            {
                HexadecimalString = oldValue;
            }
        }

        #endregion //HexadecimalString

        #endregion // Properties

        #region Constructors

        static ColorCanvas()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(ColorCanvas), new FrameworkPropertyMetadata(typeof(ColorCanvas)));
        }

        #endregion //Constructors

        #region Base Class Overrides

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            _colorShadingCanvas = (Canvas)GetTemplateChild("PART_ColorShadingCanvas");
            _colorShadeSelector = (Canvas)GetTemplateChild("PART_ColorShadeSelector");
            _spectrumSlider = (ColorSpectrumSlider)GetTemplateChild("PART_SpectrumSlider");

            // Needs to be done after we have the local references set up, but before the color shader selector's rendertransform is set.
            InternalColor = SelectedColor;

            _colorShadingCanvas.MouseLeftButtonDown += ColorShadingCanvas_MouseLeftButtonDown;
            _colorShadingCanvas.MouseLeftButtonUp += ColorShadingCanvas_MouseLeftButtonUp;
            _colorShadingCanvas.MouseMove += ColorShadingCanvas_MouseMove;
            _colorShadingCanvas.SizeChanged += ColorShadingCanvas_SizeChanged;

            _colorShadeSelector.RenderTransform = _colorShadeSelectorTransform;

            _spectrumSlider.ValueChanged += SpectrumSlider_ValueChanged;
            AddSliderEventHandlers(_spectrumSlider);

            Slider slider = (Slider)GetTemplateChild("PART_RSlider");
            AddSliderEventHandlers(slider);
            slider = (Slider)GetTemplateChild("PART_GSlider");
            AddSliderEventHandlers(slider);
            slider = (Slider)GetTemplateChild("PART_BSlider");
            AddSliderEventHandlers(slider);
            slider = (Slider)GetTemplateChild("PART_OpacitySlider");
            AddSliderEventHandlers(slider);
            
            UpdateRGBValues(InternalColor);
            UpdateColorShadeSelectorPosition(InternalColor);
        }

        protected override void OnPreviewKeyDown(KeyEventArgs e)
        {
            //hitting enter on textbox will update value of underlying source
            if (e.Key == Key.Enter && e.OriginalSource is TextBox)
            {
                BindingExpression be = ((TextBox)e.OriginalSource).GetBindingExpression(TextBox.TextProperty);
                be.UpdateSource();
            }
        }

        #endregion //Base Class Overrides

        #region Event Handlers

        void ColorShadingCanvas_MouseLeftButtonDown(Object sender, MouseButtonEventArgs e)
        {
            Point p = e.GetPosition(_colorShadingCanvas);
            UpdateColorShadeSelectorPositionAndCalculateColor(p, true);
            _colorShadingCanvas.CaptureMouse();
            _dragInProgress = true;
            e.Handled = true;
        }

        void ColorShadingCanvas_MouseLeftButtonUp(Object sender, MouseButtonEventArgs e)
        {
            _colorShadingCanvas.ReleaseMouseCapture();
            _dragInProgress = false;
            SelectedColor = InternalColor;
        }

        void ColorShadingCanvas_MouseMove(Object sender, MouseEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                Point p = e.GetPosition(_colorShadingCanvas);
                UpdateColorShadeSelectorPositionAndCalculateColor(p, true);
                Mouse.Synchronize();
            }
        }

        void ColorShadingCanvas_SizeChanged(Object sender, SizeChangedEventArgs e)
        {
            if (_currentColorPosition != null)
            {
                Point _newPoint = new Point
                {
                    X = ((Point)_currentColorPosition).X * e.NewSize.Width,
                    Y = ((Point)_currentColorPosition).Y * e.NewSize.Height
                };

                UpdateColorShadeSelectorPositionAndCalculateColor(_newPoint, false);
            }
        }

        void SpectrumSlider_ValueChanged(Object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (_currentColorPosition != null)
            {
                CalculateColor((Point)_currentColorPosition);
            }
        }

        private void Slider_DragStarted(object sender, DragStartedEventArgs e)
        {
            _dragInProgress = true;
        }

        private void Slider_DragCompleted(object sender, DragCompletedEventArgs e)
        {
            _dragInProgress = false;
            SelectedColor = InternalColor;
        }

        #endregion //Event Handlers

        #region Events

        public static readonly RoutedEvent SelectedColorChangedEvent = EventManager.RegisterRoutedEvent("SelectedColorChanged", 
            RoutingStrategy.Bubble, typeof(RoutedPropertyChangedEventHandler<Color>), typeof(ColorCanvas));
        public event RoutedPropertyChangedEventHandler<Color> SelectedColorChanged
        {
            add { AddHandler(SelectedColorChangedEvent, value); }
            remove { RemoveHandler(SelectedColorChangedEvent, value); }
        }

        #endregion //Events

        #region Methods

        private void AddSliderEventHandlers(Slider slider)
        {
            slider.AddHandler(Thumb.DragStartedEvent, new DragStartedEventHandler(Slider_DragStarted));
            slider.AddHandler(Thumb.DragCompletedEvent, new DragCompletedEventHandler(Slider_DragCompleted));
        }

        private void UpdateInternalColor()
        {
            InternalColor = Color.FromArgb(A, R, G, B);
        }

        private void UpdateInternalColor(Color color)
        {
            InternalColor = Color.FromArgb(color.A, color.R, color.G, color.B);
        }

        private void UpdateRGBValues(Color color)
        {
            _supressPropertyChanged = true;

            A = color.A;
            R = color.R;
            G = color.G;
            B = color.B;

            _supressPropertyChanged = false;
        }

        private void UpdateColorShadeSelectorPositionAndCalculateColor(Point p, bool calculateColor)
        {
            if (p.Y < 0)
                p.Y = 0;

            if (p.X < 0)
                p.X = 0;

            if (p.X > _colorShadingCanvas.ActualWidth)
                p.X = _colorShadingCanvas.ActualWidth;

            if (p.Y > _colorShadingCanvas.ActualHeight)
                p.Y = _colorShadingCanvas.ActualHeight;

            _colorShadeSelectorTransform.X = p.X - (_colorShadeSelector.Width / 2);
            _colorShadeSelectorTransform.Y = p.Y - (_colorShadeSelector.Height / 2);

            p.X = p.X / _colorShadingCanvas.ActualWidth;
            p.Y = p.Y / _colorShadingCanvas.ActualHeight;

            _currentColorPosition = p;

            if (calculateColor)
                CalculateColor(p);
        }

        private void UpdateColorShadeSelectorPosition(Color color)
        {
            if (_spectrumSlider == null || _colorShadingCanvas == null)
                return;

            _currentColorPosition = null;

            HsvColor hsv = ColorUtilities.ConvertRgbToHsv(color.R, color.G, color.B);

            if (!(color.R == color.G && color.R == color.B))
                _spectrumSlider.Value = hsv.H;

            Point p = new Point(hsv.S, 1 - hsv.V);

            _currentColorPosition = p;

            _colorShadeSelectorTransform.X = (p.X * _colorShadingCanvas.Width) - 5;
            _colorShadeSelectorTransform.Y = (p.Y * _colorShadingCanvas.Height) - 5;
        }

        private void CalculateColor(Point p)
        {
            HsvColor hsv = new HsvColor(_spectrumSlider.Value, 1, 1) { S = p.X, V = 1 - p.Y };
            var currentColor = ColorUtilities.ConvertHsvToRgb(hsv.H, hsv.S, hsv.V);
            currentColor.A = A;
            InternalColor = currentColor;
            HexadecimalString = InternalColor.ToString();
        }

        #endregion //Methods
    } // ColorCanvas

    /// <summary>
    /// 
    /// </summary>
    public class ColorItem
    {
        public Color Color { get; set; }
        public String Name { get; set; }

        public ColorItem(Color color, String name)
        {
            Color = color;
            Name = name;
        }
    } // ColorItem

    /// <summary>
    /// 
    /// </summary>
    public class ColorPicker : Control
    {
        #region Members

        private Popup _colorPickerCanvasPopup;
        private ListBox _availableColors;
        private ListBox _standardColors;
        private ListBox _recentColors;

        #endregion //Members

        #region Properties

        #region AvailableColors

        public static readonly DependencyProperty AvailableColorsProperty = DependencyProperty.Register("AvailableColors", typeof(ObservableCollection<ColorItem>), typeof(ColorPicker), new UIPropertyMetadata(CreateAvailableColors()));
        public ObservableCollection<ColorItem> AvailableColors
        {
            get { return (ObservableCollection<ColorItem>)GetValue(AvailableColorsProperty); }
            set { SetValue(AvailableColorsProperty, value); }
        }

        #endregion //AvailableColors

        #region ButtonStyle

        public static readonly DependencyProperty ButtonStyleProperty = DependencyProperty.Register("ButtonStyle", typeof(Style), typeof(ColorPicker));
        public Style ButtonStyle
        {
            get { return (Style)GetValue(ButtonStyleProperty); }
            set { SetValue(ButtonStyleProperty, value); }
        }

        #endregion //ButtonStyle

        #region DisplayColorAndName

        public static readonly DependencyProperty DisplayColorAndNameProperty = DependencyProperty.Register("DisplayColorAndName", 
            typeof(bool), typeof(ColorPicker), new UIPropertyMetadata(false));
        public bool DisplayColorAndName
        {
            get { return (bool)GetValue(DisplayColorAndNameProperty); }
            set { SetValue(DisplayColorAndNameProperty, value); }
        }

        #endregion //DisplayColorAndName

        #region IsOpen

        public static readonly DependencyProperty IsOpenProperty = DependencyProperty.Register("IsOpen", typeof(bool), typeof(ColorPicker), new UIPropertyMetadata(false));
        public bool IsOpen
        {
            get { return (bool)GetValue(IsOpenProperty); }
            set { SetValue(IsOpenProperty, value); }
        }

        #endregion //IsOpen

        #region RecentColors

        public static readonly DependencyProperty RecentColorsProperty = DependencyProperty.Register("RecentColors", 
            typeof(ObservableCollection<ColorItem>), typeof(ColorPicker), new UIPropertyMetadata(null));
        public ObservableCollection<ColorItem> RecentColors
        {
            get { return (ObservableCollection<ColorItem>)GetValue(RecentColorsProperty); }
            set { SetValue(RecentColorsProperty, value); }
        }

        #endregion //RecentColors

        #region SelectedColor

        public static readonly DependencyProperty SelectedColorProperty = DependencyProperty.Register("SelectedColor", 
            typeof(Color), typeof(ColorPicker), new FrameworkPropertyMetadata(Colors.Black, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, 
                new PropertyChangedCallback(OnSelectedColorPropertyChanged)));
        public Color SelectedColor
        {
            get { return (Color)GetValue(SelectedColorProperty); }
            set { SetValue(SelectedColorProperty, value); }
        }

        private static void OnSelectedColorPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ColorPicker colorPicker = (ColorPicker)d;
            if (colorPicker != null)
                colorPicker.OnSelectedColorChanged((Color)e.OldValue, (Color)e.NewValue);
        }

        private void OnSelectedColorChanged(Color oldValue, Color newValue)
        {
            SelectedColorText = newValue.GetColorName();

            RoutedPropertyChangedEventArgs<Color> args = new RoutedPropertyChangedEventArgs<Color>(oldValue, newValue);
            args.RoutedEvent = ColorPicker.SelectedColorChangedEvent;
            RaiseEvent(args);
        }

        #endregion //SelectedColor

        #region SelectedColorText

        public static readonly DependencyProperty SelectedColorTextProperty = DependencyProperty.Register("SelectedColorText", typeof(string), typeof(ColorPicker), new UIPropertyMetadata("Black"));
        public string SelectedColorText
        {
            get { return (string)GetValue(SelectedColorTextProperty); }
            protected set { SetValue(SelectedColorTextProperty, value); }
        }

        #endregion //SelectedColorText

        #region AdvancedMode

        public static readonly DependencyProperty AdvancedModeProperty = DependencyProperty.Register("AdvancedMode",
            typeof(bool), typeof(ColorPicker), new UIPropertyMetadata(true));
        public bool AdvancedMode
        {
            get { return (bool)GetValue(AdvancedModeProperty); }
            set { SetValue(AdvancedModeProperty, value); }
        }

        #endregion // AdvancedMode

        #region ShowAdvancedButton

        public static readonly DependencyProperty ShowAdvancedButtonProperty = DependencyProperty.Register("ShowAdvancedButton", 
            typeof(bool), typeof(ColorPicker), new UIPropertyMetadata(true));
        public bool ShowAdvancedButton
        {
            get { return (bool)GetValue(ShowAdvancedButtonProperty); }
            set { SetValue(ShowAdvancedButtonProperty, value); }
        }

        #endregion //ShowAdvancedButton

        #region ShowAvailableColors

        public static readonly DependencyProperty ShowAvailableColorsProperty = DependencyProperty.Register("ShowAvailableColors", 
            typeof(bool), typeof(ColorPicker), new UIPropertyMetadata(true));
        public bool ShowAvailableColors
        {
            get { return (bool)GetValue(ShowAvailableColorsProperty); }
            set { SetValue(ShowAvailableColorsProperty, value); }
        }

        #endregion //ShowAvailableColors

        #region ShowRecentColors

        public static readonly DependencyProperty ShowRecentColorsProperty = DependencyProperty.Register("ShowRecentColors", 
            typeof(bool), typeof(ColorPicker), new UIPropertyMetadata(false));
        public bool ShowRecentColors
        {
            get { return (bool)GetValue(ShowRecentColorsProperty); }
            set { SetValue(ShowRecentColorsProperty, value); }
        }

        #endregion //DisplayRecentColors

        #region ShowStandardColors

        public static readonly DependencyProperty ShowStandardColorsProperty = DependencyProperty.Register("ShowStandardColors", 
            typeof(bool), typeof(ColorPicker), new UIPropertyMetadata(true));
        public bool ShowStandardColors
        {
            get { return (bool)GetValue(ShowStandardColorsProperty); }
            set { SetValue(ShowStandardColorsProperty, value); }
        }

        #endregion //DisplayStandardColors

        #region StandardColors

        public static readonly DependencyProperty StandardColorsProperty = DependencyProperty.Register("StandardColors", 
            typeof(ObservableCollection<ColorItem>), typeof(ColorPicker), new UIPropertyMetadata(CreateStandardColors()));
        public ObservableCollection<ColorItem> StandardColors
        {
            get { return (ObservableCollection<ColorItem>)GetValue(StandardColorsProperty); }
            set { SetValue(StandardColorsProperty, value); }
        }

        #endregion //StandardColors

        #endregion //Properties

        #region Constructors

        static ColorPicker()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(ColorPicker), new FrameworkPropertyMetadata(typeof(ColorPicker)));
        }

        public ColorPicker()
        {
            RecentColors = new ObservableCollection<ColorItem>();
            Keyboard.AddKeyDownHandler(this, OnKeyDown);
            Mouse.AddPreviewMouseDownOutsideCapturedElementHandler(this, OnMouseDownOutsideCapturedElement);
        }

        #endregion //Constructors

        #region Base Class Overrides

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            _colorPickerCanvasPopup = (Popup)GetTemplateChild("PART_ColorPickerPalettePopup");
            //_colorPickerCanvasPopup.Opened += ColorPickerCanvasPopup_Opened;

            _availableColors = (ListBox)GetTemplateChild("PART_AvailableColors");
            if (_availableColors != null)
                _availableColors.SelectionChanged += Color_SelectionChanged;

            _standardColors = (ListBox)GetTemplateChild("PART_StandardColors");
            if (_standardColors != null)
                _standardColors.SelectionChanged += Color_SelectionChanged;

            _recentColors = (ListBox)GetTemplateChild("PART_RecentColors");
            if (_recentColors != null)
                _recentColors.SelectionChanged += Color_SelectionChanged;
        }

        #endregion //Base Class Overrides

        #region Event Handlers

        void ColorPickerCanvasPopup_Opened(object sender, EventArgs e)
        {

        }

        private void OnKeyDown(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Escape:
                case Key.Tab:
                    {
                        CloseColorPicker();
                        break;
                    }
            }
        }

        private void OnMouseDownOutsideCapturedElement(object sender, MouseButtonEventArgs e)
        {
            CloseColorPicker();
        }

        private void Color_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ListBox lb = (ListBox)sender;

            if (e.AddedItems.Count > 0)
            {
                var colorItem = (ColorItem)e.AddedItems[0];
                SelectedColor = colorItem.Color;
                UpdateRecentColors(colorItem);
                CloseColorPicker();
                lb.SelectedIndex = -1; //for now I don't care about keeping track of the selected color
            }
        }

        #endregion //Event Handlers

        #region Events

        public static readonly RoutedEvent SelectedColorChangedEvent = EventManager.RegisterRoutedEvent("SelectedColorChanged", RoutingStrategy.Bubble, typeof(RoutedPropertyChangedEventHandler<Color>), typeof(ColorPicker));
        public event RoutedPropertyChangedEventHandler<Color> SelectedColorChanged
        {
            add { AddHandler(SelectedColorChangedEvent, value); }
            remove { RemoveHandler(SelectedColorChangedEvent, value); }
        }

        #endregion //Events

        #region Methods

        private void CloseColorPicker()
        {
            if (IsOpen)
                IsOpen = false;
            ReleaseMouseCapture();
        }

        private void UpdateRecentColors(ColorItem colorItem)
        {
            if (!RecentColors.Contains(colorItem))
                RecentColors.Add(colorItem);

            if (RecentColors.Count > 10) //don't allow more than ten, maybe make a property that can be set by the user.
                RecentColors.RemoveAt(0);
        }

        private static ObservableCollection<ColorItem> CreateStandardColors()
        {
            ObservableCollection<ColorItem> _standardColors = new ObservableCollection<ColorItem>();
            _standardColors.Add(new ColorItem(Colors.Transparent, "Transparent"));
            _standardColors.Add(new ColorItem(Colors.White, "White"));
            _standardColors.Add(new ColorItem(Colors.Gray, "Gray"));
            _standardColors.Add(new ColorItem(Colors.Black, "Black"));
            _standardColors.Add(new ColorItem(Colors.Red, "Red"));
            _standardColors.Add(new ColorItem(Colors.Green, "Green"));
            _standardColors.Add(new ColorItem(Colors.Blue, "Blue"));
            _standardColors.Add(new ColorItem(Colors.Yellow, "Yellow"));
            _standardColors.Add(new ColorItem(Colors.Orange, "Orange"));
            _standardColors.Add(new ColorItem(Colors.Purple, "Purple"));
            return _standardColors;
        }

        private static ObservableCollection<ColorItem> CreateAvailableColors()
        {
            ObservableCollection<ColorItem> _standardColors = new ObservableCollection<ColorItem>();

            foreach (var item in ColorUtilities.KnownColors)
            {
                if (!String.Equals(item.Key, "Transparent"))
                {
                    var colorItem = new ColorItem(item.Value, item.Key);
                    if (!_standardColors.Contains(colorItem))
                        _standardColors.Add(colorItem);
                }
            }

            return _standardColors;
        }

        #endregion //Methods
    } // ColorPicker

    /// <summary>
    /// 
    /// </summary>
    public class ColourPickerButton : Control
    {
        #region CornerRadius

        public static readonly DependencyProperty CornerRadiusProperty = DependencyProperty.Register("CornerRadius", 
            typeof(CornerRadius), typeof(ColourPickerButton), new UIPropertyMetadata(default(CornerRadius), 
                new PropertyChangedCallback(OnCornerRadiusChanged)));
        public CornerRadius CornerRadius
        {
            get { return (CornerRadius)GetValue(CornerRadiusProperty); }
            set { SetValue(CornerRadiusProperty, value); }
        }

        private static void OnCornerRadiusChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            ColourPickerButton buttonChrome = o as ColourPickerButton;
            if (buttonChrome != null)
                buttonChrome.OnCornerRadiusChanged((CornerRadius)e.OldValue, (CornerRadius)e.NewValue);
        }

        protected virtual void OnCornerRadiusChanged(CornerRadius oldValue, CornerRadius newValue)
        {
            //we always want the InnerBorderRadius to be one less than the CornerRadius
            CornerRadius newInnerCornerRadius = new CornerRadius(System.Math.Max(0, newValue.TopLeft - 1),
                                                                 System.Math.Max(0, newValue.TopRight - 1),
                                                                 System.Math.Max(0, newValue.BottomRight - 1),
                                                                 System.Math.Max(0, newValue.BottomLeft - 1));

            InnerCornerRadius = newInnerCornerRadius;
        }

        #endregion //CornerRadius

        #region InnerCornerRadius

        public static readonly DependencyProperty InnerCornerRadiusProperty = DependencyProperty.Register("InnerCornerRadius", 
            typeof(CornerRadius), typeof(ColourPickerButton), new UIPropertyMetadata(default(CornerRadius), 
                new PropertyChangedCallback(OnInnerCornerRadiusChanged)));
        public CornerRadius InnerCornerRadius
        {
            get { return (CornerRadius)GetValue(InnerCornerRadiusProperty); }
            set { SetValue(InnerCornerRadiusProperty, value); }
        }

        private static void OnInnerCornerRadiusChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            ColourPickerButton buttonChrome = o as ColourPickerButton;
            if (buttonChrome != null)
                buttonChrome.OnInnerCornerRadiusChanged((CornerRadius)e.OldValue, (CornerRadius)e.NewValue);
        }

        protected virtual void OnInnerCornerRadiusChanged(CornerRadius oldValue, CornerRadius newValue)
        {
        }

        #endregion //InnerCornerRadius

        #region RenderChecked

        public static readonly DependencyProperty RenderCheckedProperty = DependencyProperty.Register("RenderChecked", typeof(bool), 
            typeof(ColourPickerButton), new UIPropertyMetadata(false, OnRenderCheckedChanged));
        public bool RenderChecked
        {
            get { return (bool)GetValue(RenderCheckedProperty); }
            set { SetValue(RenderCheckedProperty, value); }
        }

        private static void OnRenderCheckedChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            ColourPickerButton buttonChrome = o as ColourPickerButton;
            if (buttonChrome != null)
                buttonChrome.OnRenderCheckedChanged((bool)e.OldValue, (bool)e.NewValue);
        }

        protected virtual void OnRenderCheckedChanged(bool oldValue, bool newValue)
        {
        }

        #endregion //RenderChecked

        #region RenderEnabled

        public static readonly DependencyProperty RenderEnabledProperty = DependencyProperty.Register("RenderEnabled", typeof(bool), typeof(ColourPickerButton), new UIPropertyMetadata(true, OnRenderEnabledChanged));
        public bool RenderEnabled
        {
            get { return (bool)GetValue(RenderEnabledProperty); }
            set { SetValue(RenderEnabledProperty, value); }
        }

        private static void OnRenderEnabledChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            ColourPickerButton buttonChrome = o as ColourPickerButton;
            if (buttonChrome != null)
                buttonChrome.OnRenderEnabledChanged((bool)e.OldValue, (bool)e.NewValue);
        }

        protected virtual void OnRenderEnabledChanged(bool oldValue, bool newValue)
        {
        }

        #endregion //RenderEnabled

        #region RenderFocused

        public static readonly DependencyProperty RenderFocusedProperty = DependencyProperty.Register("RenderFocused", typeof(bool), typeof(ColourPickerButton), new UIPropertyMetadata(false, OnRenderFocusedChanged));
        public bool RenderFocused
        {
            get { return (bool)GetValue(RenderFocusedProperty); }
            set { SetValue(RenderFocusedProperty, value); }
        }

        private static void OnRenderFocusedChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            ColourPickerButton buttonChrome = o as ColourPickerButton;
            if (buttonChrome != null)
                buttonChrome.OnRenderFocusedChanged((bool)e.OldValue, (bool)e.NewValue);
        }

        protected virtual void OnRenderFocusedChanged(bool oldValue, bool newValue)
        {
        }

        #endregion //RenderFocused

        #region RenderMouseOver

        public static readonly DependencyProperty RenderMouseOverProperty = DependencyProperty.Register("RenderMouseOver", typeof(bool), typeof(ColourPickerButton), new UIPropertyMetadata(false, OnRenderMouseOverChanged));
        public bool RenderMouseOver
        {
            get { return (bool)GetValue(RenderMouseOverProperty); }
            set { SetValue(RenderMouseOverProperty, value); }
        }

        private static void OnRenderMouseOverChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            ColourPickerButton buttonChrome = o as ColourPickerButton;
            if (buttonChrome != null)
                buttonChrome.OnRenderMouseOverChanged((bool)e.OldValue, (bool)e.NewValue);
        }

        protected virtual void OnRenderMouseOverChanged(bool oldValue, bool newValue)
        {
        }

        #endregion //RenderMouseOver

        #region RenderNormal

        public static readonly DependencyProperty RenderNormalProperty = DependencyProperty.Register("RenderNormal",
            typeof(bool), typeof(ColourPickerButton), new UIPropertyMetadata(true, OnRenderNormalChanged));
        public bool RenderNormal
        {
            get { return (bool)GetValue(RenderNormalProperty); }
            set { SetValue(RenderNormalProperty, value); }
        }

        private static void OnRenderNormalChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            ColourPickerButton buttonChrome = o as ColourPickerButton;
            if (buttonChrome != null)
                buttonChrome.OnRenderNormalChanged((bool)e.OldValue, (bool)e.NewValue);
        }

        protected virtual void OnRenderNormalChanged(bool oldValue, bool newValue)
        {
        }

        #endregion //RenderNormal

        #region RenderPressed

        public static readonly DependencyProperty RenderPressedProperty = DependencyProperty.Register("RenderPressed", typeof(bool), typeof(ColourPickerButton), new UIPropertyMetadata(false, OnRenderPressedChanged));
        public bool RenderPressed
        {
            get { return (bool)GetValue(RenderPressedProperty); }
            set { SetValue(RenderPressedProperty, value); }
        }

        private static void OnRenderPressedChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            ColourPickerButton buttonChrome = o as ColourPickerButton;
            if (buttonChrome != null)
                buttonChrome.OnRenderPressedChanged((bool)e.OldValue, (bool)e.NewValue);
        }

        protected virtual void OnRenderPressedChanged(bool oldValue, bool newValue)
        {
        }

        #endregion //RenderPressed

        #region Constructors

        static ColourPickerButton()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(ColourPickerButton), new FrameworkPropertyMetadata(typeof(ColourPickerButton)));
        }

        #endregion // Constructors
    } // ColourPickerButton

} // RSG.Base.Windows.Controls
