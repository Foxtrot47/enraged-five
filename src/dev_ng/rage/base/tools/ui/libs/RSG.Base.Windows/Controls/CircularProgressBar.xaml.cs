﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace RSG.Base.Windows.Controls
{
    /// <summary>
    /// Interaction logic for CircularProgressBar.xaml
    /// </summary>
    public partial class CircularProgressBar : UserControl
    {
        #region Member Data
        /// <summary>
        /// Timer for updating the animation
        /// </summary>
        private readonly DispatcherTimer m_animationTimer;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Default Constructor
        /// </summary>
        public CircularProgressBar()
        {
            InitializeComponent();

            m_animationTimer = new DispatcherTimer(DispatcherPriority.ContextIdle, Dispatcher);
            m_animationTimer.Interval = new TimeSpan(0, 0, 0, 0, 75);
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Called to start the animation
        /// </summary>
        private void Start()
        {
            m_animationTimer.Tick += HandleAnimationTick;
            m_animationTimer.Start();
        }

        /// <summary>
        /// Called to stop the animation
        /// </summary>
        private void Stop()
        {
            m_animationTimer.Stop();
            m_animationTimer.Tick -= HandleAnimationTick;
        }

        /// <summary>
        /// Tick called by the animation timer for updating the rotation
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HandleAnimationTick(object sender, EventArgs e)
        {
            SpinnerRotate.Angle = (SpinnerRotate.Angle + 36) % 360;
        }

        /// <summary>
        /// Called on load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HandleLoaded(object sender, RoutedEventArgs e)
        {
            const double offset = System.Math.PI;
            const double step = System.Math.PI * 2 / 10.0;

            SetPosition(C0, offset, 0.0, step);
            SetPosition(C1, offset, 1.0, step);
            SetPosition(C2, offset, 2.0, step);
            SetPosition(C3, offset, 3.0, step);
            SetPosition(C4, offset, 4.0, step);
            SetPosition(C5, offset, 5.0, step);
            SetPosition(C6, offset, 6.0, step);
            SetPosition(C7, offset, 7.0, step);
            SetPosition(C8, offset, 8.0, step);
        }

        /// <summary>
        /// Helper for setting the position of one of the ellipses
        /// </summary>
        /// <param name="ellipse"></param>
        /// <param name="offset"></param>
        /// <param name="posOffSet"></param>
        /// <param name="step"></param>
        private void SetPosition(Ellipse ellipse, double offset, double posOffSet, double step)
        {
            ellipse.SetValue(Canvas.LeftProperty, 50.0 + System.Math.Sin(offset + posOffSet * step) * 50.0);
            ellipse.SetValue(Canvas.TopProperty, 50 + System.Math.Cos(offset + posOffSet * step) * 50.0);
        }

        /// <summary>
        /// Called when the control is unloaded
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HandleUnloaded(object sender, RoutedEventArgs e)
        {
            Stop();
        }

        /// <summary>
        /// Called when the visibility of the control changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HandleVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            bool isVisible = (bool)e.NewValue;

            if (isVisible)
            {
                Start();
            }
            else
            {
                Stop();
            }
        }
        #endregion
    }
}
