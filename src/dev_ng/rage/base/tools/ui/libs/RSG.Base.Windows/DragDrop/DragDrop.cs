﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Reflection;
using System.Windows.Input;
using System.Windows.Media;
using RSG.Base.Windows.DragDrop.Utilities;

namespace RSG.Base.Windows.DragDrop
{
    public static class DragDrop
    {
        #region Members

        private static DragInfo m_dragInfo;
        private static DataFormat m_Format = DataFormats.GetDataFormat("RSG.Base.Windows.DragDrop");
        private static Boolean m_selectionHandled;
        private static Boolean m_dragStarted;
        private static Boolean m_refired;
        private static MouseButtonEventArgs m_cachedMouseEvent;
        private static IDragSource m_DefaultDragHandler;
        private static IDropTarget m_DefaultDropHandler;

        #endregion // Members

        #region Properties

        /// <summary>
        /// The information that is associated with the current
        /// drag and drop operation
        /// </summary>
        private static DragInfo DragInfoStructure
        {
            get { return m_dragInfo; }
            set { m_dragInfo = value; }
        }

        /// <summary>
        /// The default drag handler in case a user doesn't want any specific
        /// drag and drop operations
        /// </summary>
        public static IDragSource DefaultDragHandler
        {
            get
            {
                if (m_DefaultDragHandler == null)
                {
                    m_DefaultDragHandler = new DefaultDragHandler();
                }

                return m_DefaultDragHandler;
            }
            set
            {
                m_DefaultDragHandler = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public static IDropTarget DefaultDropHandler
        {
            get
            {
                if (m_DefaultDropHandler == null)
                {
                    m_DefaultDropHandler = new DefaultDropHandler();
                }

                return m_DefaultDropHandler;
            }
            set
            {
                m_DefaultDropHandler = value;
            }
        }

        #endregion // Properties

        #region Dependency Properties

        public static readonly DependencyProperty IsDragSourceProperty = DependencyProperty.RegisterAttached(
            "IsDragSource", 
            typeof(Boolean), 
            typeof(DragDrop),
            new UIPropertyMetadata(false, IsDragSourceChanged));

        public static readonly DependencyProperty DragHandlerProperty = DependencyProperty.RegisterAttached(
            "DragHandler", 
            typeof(IDragSource), 
            typeof(DragDrop));

        public static readonly DependencyProperty IsDropTargetProperty = DependencyProperty.RegisterAttached(
            "IsDropTarget", 
            typeof(Boolean), 
            typeof(DragDrop),
            new UIPropertyMetadata(false, IsDropTargetChanged));

        public static readonly DependencyProperty DropHandlerProperty = DependencyProperty.RegisterAttached(
            "DropHandler", 
            typeof(IDropTarget), 
            typeof(DragDrop));

        public static readonly DependencyProperty CanEditDraggedItemsProperty = DependencyProperty.RegisterAttached(
            "CanEditDraggedItems",
            typeof(Boolean),
            typeof(DragDrop),
            new UIPropertyMetadata(false));

        #endregion // Dependency Properties

        #region Public Functions

        /// <summary>
        ///  Get whether the ui element is a drag source
        /// </summary>
        public static Boolean GetIsDragSource(UIElement target)
        {
            return (bool)target.GetValue(IsDragSourceProperty);
        }
        
        /// <summary>
        ///  Set whether the ui element is a drag source
        /// </summary>
        public static void SetIsDragSource(UIElement target, Boolean value)
        {
            target.SetValue(IsDragSourceProperty, value);
        }

        /// <summary>
        ///  Get whether the ui element that is dragged can be edited
        /// </summary>
        public static Boolean GetCanEditDraggedItems(UIElement target)
        {
            return (bool)target.GetValue(CanEditDraggedItemsProperty);
        }

        /// <summary>
        ///  Sets whether the ui element that is dragged can be edited
        /// </summary>
        public static void SetCanEditDraggedItems(UIElement target, Boolean value)
        {
            target.SetValue(CanEditDraggedItemsProperty, value);
        }

        /// <summary>
        ///  Get the drag handler for the UI Element
        /// </summary>
        public static IDragSource GetDragHandler(UIElement target)
        {
            if (target == null)
                return null;

            return target.GetValue(DragHandlerProperty) as IDragSource;
        }

        /// <summary>
        /// Set the drag handler for the UI Element
        /// </summary>
        public static void SetDragHandler(UIElement target, IDragSource value)
        {
            target.SetValue(DragHandlerProperty, value);
        }
        
        /// <summary>
        ///  Get whether the ui element is a drop target
        /// </summary>
        public static bool GetIsDropTarget(UIElement target)
        {
            return (bool)target.GetValue(IsDropTargetProperty);
        }

        /// <summary>
        ///  Set whether the ui element is a drop target
        /// </summary>
        public static void SetIsDropTarget(UIElement target, bool value)
        {
            target.SetValue(IsDropTargetProperty, value);
        }

        /// <summary>
        ///  Get the drop handler for the UI Element
        /// </summary>
        public static IDropTarget GetDropHandler(UIElement target)
        {
            return (IDropTarget)target.GetValue(DropHandlerProperty);
        }

        /// <summary>
        /// Set the drop handler for the UI Element
        /// </summary>
        public static void SetDropHandler(UIElement target, IDropTarget value)
        {
            target.SetValue(DropHandlerProperty, value);
        }

        #endregion // Public Functions

        #region Property Changed Callbacks

        /// <summary>
        /// 
        /// </summary>
        private static void IsDragSourceChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            UIElement uiElement = (UIElement)d;

            if ((Boolean)e.NewValue == true)
            {
                uiElement.PreviewMouseLeftButtonDown += DragSource_PreviewMouseLeftButtonDown;
                uiElement.PreviewMouseLeftButtonUp += DragSource_PreviewMouseLeftButtonUp;
                uiElement.PreviewMouseMove += DragSource_PreviewMouseMove;
            }
            else
            {
                uiElement.PreviewMouseLeftButtonDown -= DragSource_PreviewMouseLeftButtonDown;
                uiElement.PreviewMouseLeftButtonUp -= DragSource_PreviewMouseLeftButtonUp;
                uiElement.PreviewMouseMove -= DragSource_PreviewMouseMove;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private static void IsDropTargetChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            UIElement uiElement = (UIElement)d;

            if ((bool)e.NewValue == true)
            {
                uiElement.AllowDrop = true;
                uiElement.PreviewDragEnter += DropTarget_PreviewDragEnter;
                uiElement.PreviewDragLeave += DropTarget_PreviewDragLeave;
                uiElement.PreviewDragOver += DropTarget_PreviewDragOver;
                uiElement.Drop += DropTarget_PreviewDrop;
            }
            else
            {
                uiElement.AllowDrop = false;
                uiElement.PreviewDragEnter -= DropTarget_PreviewDragEnter;
                uiElement.PreviewDragLeave -= DropTarget_PreviewDragLeave;
                uiElement.PreviewDragOver -= DropTarget_PreviewDragOver;
                uiElement.Drop -= DropTarget_PreviewDrop;
            }
        }

        #endregion // Property Changed Callbacks

        #region Drag Source Event Handlers
        
        /// <summary>
        /// Determine whether the mouse event has happen on a scroll bar
        /// </summary>
        private static Boolean HitTestScrollBar(Object sender, MouseButtonEventArgs e)
        {
            HitTestResult hit = VisualTreeHelper.HitTest((Visual)sender, e.GetPosition((IInputElement)sender));
            if (hit != null)
                return hit.VisualHit.GetVisualAncestor<System.Windows.Controls.Primitives.ScrollBar>() != null;
            else
                return false;
        }

        /// <summary>
        /// Gets called when a UI Element that is a drag source gets a preview
        /// left button down event
        /// </summary>
        private static void DragSource_PreviewMouseLeftButtonDown(Object sender, MouseButtonEventArgs e)
        {
            var sourceType = e.OriginalSource.GetType().FullName;
            if (m_refired == true || sourceType == "System.Windows.Controls.TextBoxView")
                return;

            // Ignore the click if the user has clicked on a scrollbar.
            if (HitTestScrollBar(sender, e))
            {
                DragDrop.DragInfoStructure = null;
                return;
            }

            DragDrop.DragInfoStructure = new DragInfo(sender, e);

            // If the sender is a list box that allows multiple selections, ensure that clicking on an 
            // already selected item does not change the selection, otherwise dragging multiple items 
            // is made impossible.
            ItemsControl itemsControl = sender as ItemsControl;

            if (DragDrop.DragInfoStructure.VisualSourceItem != null && itemsControl != null && itemsControl.CanSelectMultipleItems())
            {
                IEnumerable selectedItems = itemsControl.GetSelectedItems();

                if (selectedItems.Cast<Object>().Contains(DragDrop.DragInfoStructure.SourceItem))
                {
                    // TODO: Re-raise the supressed event if the user didn't initiate a drag.
                    bool canEdit = GetCanEditDraggedItems((UIElement)sender);

                    if (canEdit)
                        e.Handled = false;
                    else
                        e.Handled = true;

                    m_cachedMouseEvent = e;
                    m_selectionHandled = true;
                }
            }
        }

        /// <summary>
        /// Gets called when a UI Element that is a drag source gets a preview
        /// left button up event
        /// </summary>
        private static void DragSource_PreviewMouseLeftButtonUp(Object sender, MouseButtonEventArgs e)
        {
            if (DragDrop.DragInfoStructure != null)
            {
                DragDrop.DragInfoStructure = null;
            }
            if (m_selectionHandled == true)
            {
                if (m_dragStarted == false)
                {
                    // Fire left mouse button down event again.
                    m_cachedMouseEvent.Handled = false;
                    m_refired = true;
                    MouseDevice mouseDevice = Mouse.PrimaryDevice;
                    MouseButtonEventArgs mouseButtonEventArgs = new MouseButtonEventArgs(m_cachedMouseEvent.MouseDevice, m_cachedMouseEvent.Timestamp, MouseButton.Right);
                    mouseButtonEventArgs.RoutedEvent = Mouse.PreviewMouseDownEvent;
                    mouseButtonEventArgs.Source = m_cachedMouseEvent.Source;
                    (m_cachedMouseEvent.OriginalSource as UIElement).RaiseEvent(m_cachedMouseEvent);
                }
                m_refired = false;
                m_selectionHandled = false;
                m_dragStarted = false;

                ItemsControl itemsControl = sender as ItemsControl;
                if (itemsControl != null && itemsControl.CanSelectMultipleItems())
                {
                    DependencyObject selected = (e.OriginalSource as UIElement).GetVisualAncestor(itemsControl.ItemContainerGenerator);
                    if (selected != null)
                    {
                        itemsControl.ClearSelection();
                        PropertyInfo property = selected.GetType().GetProperty("IsSelected");
                        if (property != null)
                        {
                            property.SetValue(selected, true, null);
                        }
                        else
                        {
                            itemsControl.SelectItem(selected);
                        }
                    }
                }
            }

            m_cachedMouseEvent = null;
        }

        /// <summary>
        /// Gets called when a UI Element that is a drag source gets a preview
        /// mouse move event
        /// </summary>
        static void DragSource_PreviewMouseMove(Object sender, MouseEventArgs e)
        {
            if (DragDrop.DragInfoStructure != null)
            {
                Point dragStart = DragDrop.DragInfoStructure.DragStartPosition;
                Point position = e.GetPosition(null);

                if (System.Math.Abs(position.X - dragStart.X) > SystemParameters.MinimumHorizontalDragDistance ||
                    System.Math.Abs(position.Y - dragStart.Y) > SystemParameters.MinimumVerticalDragDistance)
                {
                    m_dragStarted = true;
                    // Each visual item that is currently getting dragged can have it's own drag handler
                    // to get the data from
                    List<Object> draggedData = new List<Object>();
                    if (DragDrop.DragInfoStructure.VisualSourceItems != null)
                    {
                        foreach (UIElement draggedElement in DragDrop.DragInfoStructure.VisualSourceItems)
                        {
                            IDragSource dragHandler = GetDragHandler(draggedElement);
                            if (dragHandler != null)
                                draggedData.Add(dragHandler.StartDrag(DragDrop.DragInfoStructure));
                        }

                        if (draggedData.Count == 0)
                        {
                            DragDrop.DragInfoStructure.Data = DefaultDragHandler.StartDrag(DragDrop.DragInfoStructure);
                        }
                        else if (draggedData.Count == 1)
                        {
                            DragDrop.DragInfoStructure.Data = draggedData[0];
                        }
                        else
                        {
                            DragDrop.DragInfoStructure.Data = TypeUtil.CreateDynamicallyTypedList(draggedData);
                        }

                        if (DragDrop.DragInfoStructure.Effects != DragDropEffects.None && DragDrop.DragInfoStructure.Data != null)
                        {
                            DataObject data = new DataObject(m_Format.Name, DragDrop.DragInfoStructure.Data);
                            System.Windows.DragDrop.DoDragDrop(DragDrop.DragInfoStructure.VisualSource, data, DragDropEffects.All);
                            DragDrop.DragInfoStructure = null;
                            m_selectionHandled = false;
                        }
                    }
                }
            }
        }

        #endregion // Drag Source Event Handlers

        #region Drop Target Event Handlers

        /// <summary>
        /// 
        /// </summary>
        private static void Scroll(DependencyObject o, DragEventArgs e)
        {
            ScrollViewer scrollViewer = o.GetVisualDescendent<ScrollViewer>();

            if (scrollViewer != null)
            {
                Point position = e.GetPosition(scrollViewer);
                double scrollMargin = System.Math.Min(scrollViewer.FontSize * 2, scrollViewer.ActualHeight / 2);

                if (position.X >= scrollViewer.ActualWidth - scrollMargin &&
                    scrollViewer.HorizontalOffset < scrollViewer.ExtentWidth - scrollViewer.ViewportWidth)
                {
                    scrollViewer.LineRight();
                }
                else if (position.X < scrollMargin && scrollViewer.HorizontalOffset > 0)
                {
                    scrollViewer.LineLeft();
                }
                else if (position.Y >= scrollViewer.ActualHeight - scrollMargin &&
                    scrollViewer.VerticalOffset < scrollViewer.ExtentHeight - scrollViewer.ViewportHeight)
                {
                    scrollViewer.LineDown();
                }
                else if (position.Y < scrollMargin && scrollViewer.VerticalOffset > 0)
                {
                    scrollViewer.LineUp();
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private static void DropTarget_PreviewDragEnter(Object sender, DragEventArgs e)
        {
            DropTarget_PreviewDragOver(sender, e);
        }

        /// <summary>
        /// 
        /// </summary>
        static void DropTarget_PreviewDragLeave(Object sender, DragEventArgs e)
        {
            DropTarget_PreviewDragOver(sender, e);
        }

        /// <summary>
        /// 
        /// </summary>
        static void DropTarget_PreviewDragOver(Object sender, DragEventArgs e)
        {
            e.Effects = DragDropEffects.None;
            var originalSource = (e.OriginalSource as DependencyObject).GetVisualAncestor(sender.GetType());
            if (sender == originalSource)
            {
                e.Effects = DragDropEffects.None;
                DropInfo dropInfo = new DropInfo(sender, e, DragDrop.DragInfoStructure, m_Format.Name);
                IDropTarget dropHandler = GetDropHandler((UIElement)sender);

                if (dropHandler != null)
                {
                    dropHandler.DragOver(dropInfo);
                }
                else
                {
                    DefaultDropHandler.DragOver(dropInfo);
                }

                e.Effects = dropInfo.Effects;

                Scroll((DependencyObject)sender, e);
                e.Handled = true;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        static void DropTarget_PreviewDrop(object sender, DragEventArgs e)
        {
            DropInfo dropInfo = new DropInfo(sender, e, DragDrop.DragInfoStructure, m_Format.Name);
            IDropTarget dropHandler = GetDropHandler((UIElement)sender);
            
            if (dropHandler != null)
            {
                dropHandler.Drop(dropInfo);
            }
            else
            {
                DefaultDropHandler.Drop(dropInfo);
            }

            e.Handled = true;
        }

        #endregion // Drop Target Event Handlers
    } // DragDrop
} // RSG.Base.Windows.DragDrop
