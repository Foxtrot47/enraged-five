﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace RSG.Base.Windows.Controls.WpfViewport2D.Controls
{
    /// <summary>
    /// Custom event args for viewport click events
    /// </summary>
    public class MouseMoveEventArgs : EventArgs
    {
        public Point ViewPosition
        {
            get;
            protected set;
        }

        public Point WorldPosition
        {
            get;
            protected set;
        }

        public MouseMoveEventArgs(Point viewPosition, Point worldPosition)
            : base()
        {
            ViewPosition = viewPosition;
            WorldPosition = worldPosition;
        }
    }

    /// <summary>
    /// Event handler for handling clicks on the viewport
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public delegate void ViewportMouseMoveEventHandler(object sender, MouseMoveEventArgs e);

    /// <summary>
    /// Interaction logic for MouseLocationControl.xaml
    /// </summary>
    public partial class MouseLocationControl : ViewportUserControl
    {
        #region Events
        /// <summary>
        /// Event fired when the mouse moves within the viewport.
        /// </summary>
        public event ViewportMouseMoveEventHandler MouseMoved;

        private void OnMouseMoved(Point viewPosition, Point worldPosition)
        {
            if (MouseMoved != null)
            {
                MouseMoved(this, new MouseMoveEventArgs(viewPosition, worldPosition));
            }
        }
        #endregion // Events

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public MouseLocationControl()
        {
            InitializeComponent();
        }
        #endregion // Constructor(s)

        #region Event Helpers
        /// <summary>
        /// 
        /// </summary>
        public override void OnManagerMouseMove(Object sender, MouseEventArgs e)
        {
            if (sender is Viewport2D)
            {
                Point mouseViewPoint = e.MouseDevice.GetPosition(sender as Viewport2D);
                Point mouseWorldPoint = (sender as Viewport2D).ViewToWorld(mouseViewPoint);
                CoordTextBlock.Text = String.Format("({0:0.###}, {1:0.###})", mouseWorldPoint.X, mouseWorldPoint.Y);

                // Fire off the mouse move event.
                OnMouseMoved(mouseViewPoint, mouseWorldPoint);
            }
        }
        #endregion // Event Helpers

        #region Registration
        /// <summary>
        /// 
        /// </summary>
        /// <param name="manager"></param>
        public override void RegisterActionsWithManager(Actions.ActionManager manager)
        {
            manager.RegisterMouseCommand(this, Actions.MouseEvents.Mouse_Move);
        }
        #endregion // Registration
    }
}
