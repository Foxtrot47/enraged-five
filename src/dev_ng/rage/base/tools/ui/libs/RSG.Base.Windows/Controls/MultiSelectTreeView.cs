﻿using System;
using System.Collections.Generic;
using RSG.Base.Collections;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.ComponentModel;
using System.Collections;
using System.Windows.Media;

namespace RSG.Base.Windows.Controls
{
    public class MultiSelectTreeView : TreeView
    {
        #region Events

        public event EventHandler SelectedItemsChanged;

        #endregion // Events

        #region Properties

        #region MultiSelectEnabled

        /// <summary>
        /// Using a DependencyProperty as the backing store for EnableMultiSelect.  This enables animation, styling, binding, etc...
        /// </summary>
        public Boolean MultiSelectEnabled
        {
            get { return (Boolean)GetValue(EnableMultiSelectProperty); }
            set { SetValue(EnableMultiSelectProperty, value); }
        }
        public static readonly DependencyProperty EnableMultiSelectProperty = DependencyProperty.RegisterAttached("EnableMultiSelect", typeof(Boolean), typeof(MultiSelectTreeView),
            new FrameworkPropertyMetadata(false) { PropertyChangedCallback = EnableMultiSelectChanged, BindsTwoWayByDefault = true });

        /// <summary>
        /// Called every time the Multi Select enabled property is changed, this just
        /// sets up the callback handlers for the key down and clicked events
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        static void EnableMultiSelectChanged(DependencyObject sender, DependencyPropertyChangedEventArgs args)
        {
            TreeView tree = (TreeView)sender;
            var wasEnable = (bool)args.OldValue;
            var isEnabled = (bool)args.NewValue;
            if (wasEnable)
            {
                tree.RemoveHandler(MultiSelectTreeViewItem.MouseDownEvent, new MouseButtonEventHandler(ItemClicked));
            }
            if (isEnabled)
            {
                tree.AddHandler(MultiSelectTreeViewItem.MouseDownEvent, new MouseButtonEventHandler(ItemClicked), true);
            }
        }

        #endregion // MultiSelectEnabled

        #region SelectedItems

        /// <summary>
        /// Using a DependencyProperty as the backing store for SelectedItems.  This enables animation, styling, binding, etc...
        /// </summary>
        public ObservableCollection<Object> SelectedItems
        {
            get { return (ObservableCollection<Object>)GetValue(SelectedItemsProperty); }
            set { SetValue(SelectedItemsProperty, value); }
        }
        public static readonly DependencyProperty SelectedItemsProperty = DependencyProperty.RegisterAttached("SelectedItems", typeof(ObservableCollection<Object>), typeof(MultiSelectTreeView),
            new PropertyMetadata(null, new PropertyChangedCallback(SelectedItemPropertyChanged)));

        static void SelectedItemPropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            MultiSelectTreeView treeView = sender as MultiSelectTreeView;

            if (e.NewValue == null)
            {
                treeView.SelectedItems.CollectionChanged -= treeView.SelectedItems_CollectionChanged;
            }
            else if (e.OldValue == null && e.NewValue != null)
            {
                treeView.SelectedItems.CollectionChanged += treeView.SelectedItems_CollectionChanged;
            }

            treeView.FireSelectedItemsChangedEvent();
        }

        void SelectedItems_CollectionChanged(Object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            FireSelectedItemsChangedEvent();
        }

        void FireSelectedItemsChangedEvent()
        {
            if (this.SelectedItemsChanged != null)
                this.SelectedItemsChanged(this, EventArgs.Empty);
        }

        #endregion // SelectedItems

        #region AnchorItem

        /// <summary>
        /// Using a DependencyProperty as the backing store for AnchorItem.  This enables animation, styling, binding, etc...
        /// </summary>
        public Object AnchorItem
        {
            get { return (Object)GetValue(AnchorItemProperty); }
            set { SetValue(AnchorItemProperty, value); }
        }
        public static readonly DependencyProperty AnchorItemProperty = DependencyProperty.RegisterAttached("AnchorItem", typeof(Object), typeof(MultiSelectTreeView),
            new PropertyMetadata(null));

        #endregion // AnchorItem

        #endregion // Properties

        #region Private Function(s)

        /// <summary>
        /// Called if this treeview has multi selection enabled and a key is pressed down
        /// </summary>
        /// <param name="sender">The sender of the event</param>
        /// <param name="e">Additional arguments for the event</param>

        protected override void OnKeyDown(KeyEventArgs e)
        {
            if (e.Key == Key.A && (Keyboard.Modifiers & ModifierKeys.Control) == ModifierKeys.Control)
            {
                List<MultiSelectTreeViewItem> expandedItems = new List<MultiSelectTreeViewItem>(GetExpandedTreeViewItems(this));
                this.SelectedItems.BeginUpdate();
                this.SelectedItems.Clear();
                this.AnchorItem = null;
                foreach (MultiSelectTreeViewItem item in expandedItems)
                {
                    item.IsSelected_ = true;
                    if (!this.SelectedItems.Contains(item.Header))
                        this.SelectedItems.Add(item.Header);
                    this.AnchorItem = item.Header;
                    item.Focus();
                }
                this.SelectedItems.EndUpdate();
            }
            else if (e.Key == Key.Up)
            {
                List<MultiSelectTreeViewItem> expandedItems = new List<MultiSelectTreeViewItem>(GetExpandedTreeViewItems(this));
                int originalIndex = expandedItems.IndexOf(e.OriginalSource as MultiSelectTreeViewItem);
                if (originalIndex != 0)
                {
                    if ((Keyboard.Modifiers & ModifierKeys.Control) == ModifierKeys.Control)
                    {
                        if (expandedItems[originalIndex - 1].IsSelected_ == true)
                        {
                            if (expandedItems[originalIndex].IsSelected_ == true)
                            {
                                expandedItems[originalIndex].IsSelected_ = false;
                                this.SelectedItems.Remove(expandedItems[originalIndex].Header);
                                expandedItems[originalIndex - 1].Focus();
                            }
                            else
                            {
                                expandedItems[originalIndex - 1].IsSelected_ = false;
                                this.SelectedItems.Remove(expandedItems[originalIndex - 1].Header);
                            }
                        }
                        else
                        {
                            expandedItems[originalIndex - 1].IsSelected_ = true;
                            if (this.SelectedItems.Count == 0)
                            {
                                this.AnchorItem = expandedItems[originalIndex - 1].Header;
                            }
                            if (!this.SelectedItems.Contains(expandedItems[originalIndex - 1].Header))
                                this.SelectedItems.Add(expandedItems[originalIndex - 1].Header);
                            expandedItems[originalIndex - 1].Focus();
                        }
                    }
                    else
                    {
                        MakeSingleSelection(this, expandedItems[originalIndex - 1]);
                        expandedItems[originalIndex - 1].Focus();
                    }
                }
                else
                {
                    if ((Keyboard.Modifiers & ModifierKeys.Control) != ModifierKeys.Control)
                    {
                        MakeSingleSelection(this, expandedItems[originalIndex]);
                        expandedItems[originalIndex].Focus();
                    }
                }
            }
            else if (e.Key == Key.Down)
            {
                List<MultiSelectTreeViewItem> expandedItems = new List<MultiSelectTreeViewItem>(GetExpandedTreeViewItems(this));
                int originalIndex = expandedItems.IndexOf(e.OriginalSource as MultiSelectTreeViewItem);
                if (originalIndex != expandedItems.Count - 1)
                {
                    if ((Keyboard.Modifiers & ModifierKeys.Control) == ModifierKeys.Control)
                    {
                        if (expandedItems[originalIndex + 1].IsSelected_ == true)
                        {
                            if (expandedItems[originalIndex].IsSelected_ == true)
                            {
                                expandedItems[originalIndex].IsSelected_ = false;
                                this.SelectedItems.Remove(expandedItems[originalIndex + 1].Header);
                                expandedItems[originalIndex + 1].Focus();
                            }
                            else
                            {
                                expandedItems[originalIndex + 1].IsSelected_ = false;
                                this.SelectedItems.Remove(expandedItems[originalIndex + 1].Header);
                            }
                        }
                        else
                        {
                            expandedItems[originalIndex + 1].IsSelected_ = true;
                            if (this.SelectedItems.Count == 0)
                            {
                                this.AnchorItem = expandedItems[originalIndex - 1].Header;
                            }
                            if (!this.SelectedItems.Contains(expandedItems[originalIndex + 1].Header))
                                this.SelectedItems.Add(expandedItems[originalIndex + 1].Header);
                            expandedItems[originalIndex + 1].Focus();
                        }
                    }
                    else
                    {
                        MakeSingleSelection(this, expandedItems[originalIndex + 1]);
                        expandedItems[originalIndex + 1].Focus();
                    }
                }
                else
                {
                    if ((Keyboard.Modifiers & ModifierKeys.Control) != ModifierKeys.Control)
                    {
                        MakeSingleSelection(this, expandedItems[originalIndex]);
                        expandedItems[originalIndex].Focus();
                    }
                }
            }
        }

        public static void MakeSingleSelection(MultiSelectTreeView tree, MultiSelectTreeViewItem item)
        {
            tree.SelectedItems.BeginUpdate();
            tree.ClearSelection();
            foreach (MultiSelectTreeViewItem selectedItem in GetExpandedTreeViewItems(tree))
            {
                if (selectedItem == null)
                    continue;

                if (selectedItem == item && selectedItem.IsSelected_ == false)
                {
                    selectedItem.IsSelected_ = true;
                    if (!tree.SelectedItems.Contains(selectedItem.Header))
                        tree.SelectedItems.Add(selectedItem.Header);
                }
            }
            UpdateAnchorAndActionItem(tree, item);
            tree.SelectedItems.EndUpdate();
        }

        public static void UpdateAnchorAndActionItem(MultiSelectTreeView tree, MultiSelectTreeViewItem item)
        {
            tree.AnchorItem = item.Header;
        }

        /// <summary>
        /// Called if this treeview has multi selection enabled and a item is clicked
        /// </summary>
        /// <param name="sender">The sender of the event</param>
        /// <param name="e">Additional arguments for the event</param>
        public static void ItemClicked(Object sender, MouseButtonEventArgs e)
        {
            MultiSelectTreeViewItem item = FindTreeViewItem(e.OriginalSource);
            if (item == null)
                return;

            MultiSelectTreeView tree = sender as MultiSelectTreeView;
            if (tree == null)
                return;

            var mouseButton = e.ChangedButton;
            if (mouseButton == MouseButton.Left)
            {
                if ((Keyboard.Modifiers & ModifierKeys.Control) == ModifierKeys.Control)
                {
                    if (item.IsSelected_ == true)
                    {
                        item.IsSelected_ = false;
                        tree.SelectedItems.Remove(item.Header);
                        if (tree.SelectedItems.Count == 0)
                        {
                            tree.AnchorItem = null;
                        }
                    }
                    else
                    {
                        item.IsSelected_ = true;
                        if (tree.SelectedItems.Count == 0)
                        {
                            tree.AnchorItem = item.Header;
                        }

                        if (!tree.SelectedItems.Contains(item.Header))
                            tree.SelectedItems.Add(item.Header);
                        item.Focus();
                    }
                }
                else
                {
                    if (item.IsSelected_ == false || tree.SelectedItems.Count > 1)
                    {
                        MakeSingleSelection(tree, item);
                    }
                }
            }
        }

        /// <summary>
        /// Finds the tree view item from a object by walking up the visual tree
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static MultiSelectTreeViewItem FindTreeViewItem(Object obj)
        {
            DependencyObject dpObj = obj as DependencyObject;

            if (dpObj == null || (dpObj is System.Windows.Controls.Primitives.ToggleButton))
                return null;

            if (dpObj is MultiSelectTreeViewItem)
                return (MultiSelectTreeViewItem)dpObj;

            return FindTreeViewItem(VisualTreeHelper.GetParent(dpObj));
        }

        /// <summary>
        /// Returns a list of all the expanded treeview items, this is so we can use ctrl+A to select them all
        /// </summary>
        /// <param name="tree"></param>
        /// <returns></returns>
        public static IEnumerable<MultiSelectTreeViewItem> GetExpandedTreeViewItems(ItemsControl tree)
        {
            for (int i = 0; i < tree.Items.Count; i++)
            {
                var item = (MultiSelectTreeViewItem)tree.ItemContainerGenerator.ContainerFromIndex(i);

                if (item == null)
                    continue;

                yield return item;
                if (item.IsExpanded)
                {
                    foreach (var subItem in GetExpandedTreeViewItems(item))
                    {
                        yield return subItem;
                    }
                }
            }
        }

        #endregion // Private Function(s)

        #region Public Function(s)

        public void ClearSelection()
        {
            foreach (MultiSelectTreeViewItem expanded in GetExpandedTreeViewItems(this))
            {
                expanded.IsSelected_ = false;
            }
            this.SelectedItems.Clear();
            this.AnchorItem = null;
        }

        #endregion // Public Function(s)

        #region Override(s)

        /// <summary>
        /// Makes sure that this custom tree view uses the custom tree view items
        /// </summary>
        /// <returns></returns>
        protected override DependencyObject GetContainerForItemOverride()
        {
            return new MultiSelectTreeViewItem();
        }

        /// <summary>
        /// Makes sure that this custom tree view uses the custom tree view items
        /// </summary>
        /// <returns></returns>
        protected override bool IsItemItsOwnContainerOverride(object item)
        {
            return item is MultiSelectTreeViewItem;
        }

        #endregion // Override(s)

        #region Constructor

        public MultiSelectTreeView()
            : base()
        {
            this.SelectedItems = new ObservableCollection<Object>();
        }

        #endregion // Constructor

    } // MultiSelectTreeView
} // RSG.Base.Windows.Controls
