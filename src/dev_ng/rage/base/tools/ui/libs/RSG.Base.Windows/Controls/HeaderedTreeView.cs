﻿using System;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Controls;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Base.Windows.Controls
{
    /// <summary>
    /// A control that shows a tree view with a items source
    /// with a header on the top of it that has an expander on it.
    /// This is basically a restyled-simplied tree view item.
    /// </summary>
    [TemplateVisualState(Name = "Expanded", GroupName = "ExpansionStates")]
    [TemplateVisualState(Name = "Collapsed", GroupName = "ExpansionStates")]
    [TemplatePart(Name = "Part_TreeView", Type = typeof(TreeView))]
    public class HeaderedTreeView : HeaderedItemsControl
    {
        #region Events
        public event RoutedPropertyChangedEventHandler<object> SelectedItemChanged;
        #endregion

        #region Dependency Properties

        /// <summary>
        /// Dependency property for the IsExpanded property which determines
        /// whether the tree view is currently visible or not
        /// </summary>
        public static DependencyProperty IsExpandedProperty = DependencyProperty.Register(
            "IsExpanded",
            typeof(Boolean),
            typeof(HeaderedTreeView),
            new FrameworkPropertyMetadata(false, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault | FrameworkPropertyMetadataOptions.AffectsArrange
                , OnExpansionStateChanged));

        /// <summary>
        /// Dependency property for the inner border background colour.
        /// </summary>
        public static DependencyProperty InnerBackgroundProperty = DependencyProperty.Register(
            "InnerBackground",
            typeof(Brush),
            typeof(HeaderedTreeView),
            new FrameworkPropertyMetadata(Brushes.Transparent));

        /// <summary>
        /// Dependency property for the selected item in the items source
        /// </summary>
        public static DependencyProperty SelectedItemProperty = DependencyProperty.Register(
            "SelectedItem",
            typeof(Object),
            typeof(HeaderedTreeView),
            new PropertyMetadata(null));

        #endregion // Dependency Properties

        #region Properties

        /// <summary>
        /// Determines whether the tree view is currently visible or not
        /// </summary>
        public Boolean IsExpanded
        {
            get { return (Boolean)GetValue(IsExpandedProperty); }
            set 
            { 
                SetValue(IsExpandedProperty, value);
            }
        }

        /// <summary>
        /// Represents the brush to use for the inner background
        /// </summary>
        public Brush InnerBackground
        {
            get { return (Brush)GetValue(InnerBackgroundProperty); }
            set { SetValue(InnerBackgroundProperty, value); }
        }

        /// <summary>
        /// Dependency property for the selected item in the items source
        /// </summary>
        public Object SelectedItem
        {
            get { return (Object)GetValue(SelectedItemProperty); }
            set { SetValue(SelectedItemProperty, value); }
        }

        #endregion // Properties

        #region Constructors

        /// <summary>
        /// Static constructor called once
        /// </summary>
        static HeaderedTreeView()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(HeaderedTreeView), new FrameworkPropertyMetadata(typeof(HeaderedTreeView)));
        }

        /// <summary>
        /// Default constructor
        /// </summary>
        public HeaderedTreeView()
        {
        }

        #endregion // Constructors

        #region Override Methods

        /// <summary>
        /// Get a reference to the tree view and start listening to the 
        /// selected item change property
        /// </summary>
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            TreeView treeView = GetTemplateChild("Part_TreeView") as TreeView;
            if (treeView != null)
                treeView.SelectedItemChanged += new RoutedPropertyChangedEventHandler<Object>(OnSelectedItemChanged);
        }

        /// <summary>
        /// Gets called when the selection on the tree view is changed
        /// </summary>
        void OnSelectedItemChanged(Object sender, RoutedPropertyChangedEventArgs<Object> e)
        {
            this.SelectedItem = (sender as TreeView).SelectedItem;
            if (this.SelectedItemChanged != null)
                this.SelectedItemChanged(this, e);
        }

        #endregion // Override Methods

        #region Visual State Changes

        /// <summary>
        /// Changes the visual state based on the expasion state
        /// </summary>
        private static void OnExpansionStateChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            if ((Boolean)e.NewValue == false)
            {
                VisualStateManager.GoToState(sender as HeaderedTreeView, "Collapsed", false);
            } 
            else
            {
                VisualStateManager.GoToState(sender as HeaderedTreeView, "Expanded", false);
            }
        }

        #endregion // #region Visual State Changes
    } // HeaderedTreeView

    /// <summary>
    /// Converts a level index [0 - n] to a indent width attribute for a custom tree view
    /// </summary>
    public class ConvertLevelToIndent : IValueConverter
    {
        #region IValueConverter

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return (int)value * 16;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotSupportedException("Not supported - ConvertBack should never be called in a OneWay Binding.");
        }

        #endregion // IValueConverter
    } // ConvertLevelToIndent
} // RSG.Base.Windows.Controls
