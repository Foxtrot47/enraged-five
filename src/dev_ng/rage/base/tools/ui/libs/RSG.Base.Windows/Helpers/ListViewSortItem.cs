﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows.Controls;

namespace RSG.Base.Windows.Helpers
{
    /// <summary>
    /// http://www.codeproject.com/Articles/25964/A-more-generic-way-of-sorting-a-WPF-ListView-with
    /// </summary>
    public class ListViewSortItem
    {
        #region [ Constructor ]

        /// <summary>
        /// Initializes a new instance of the <see cref="ListViewSortItem"/> class.
        /// </summary>
        /// <param name="comparer">The comparer.</param>
        /// <param name="lastColumnHeaderClicked">The last column header clicked.</param>
        /// <param name="lastSortDirection">The last sort direction.</param>
        public ListViewSortItem(IListViewCustomComparer comparer, GridViewColumnHeader lastColumnHeaderClicked, ListSortDirection lastSortDirection)
        {
            Comparer = comparer;
            LastColumnHeaderClicked = lastColumnHeaderClicked;
            LastSortDirection = lastSortDirection;
        }

        #endregion

        #region [ Properties ]

        /// <summary>
        /// Gets or sets the comparer.
        /// </summary>
        /// <value>The comparer.</value>
        public IListViewCustomComparer Comparer { get; private set; }

        /// <summary>
        /// Gets or sets the last column header clicked.
        /// </summary>
        /// <value>The last column header clicked.</value>
        public GridViewColumnHeader LastColumnHeaderClicked { get; set; }

        /// <summary>
        /// Gets or sets the last sort direction.
        /// </summary>
        /// <value>The last sort direction.</value>
        public ListSortDirection LastSortDirection { get; set; }

        #endregion
    }

}
