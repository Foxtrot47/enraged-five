﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using System.Globalization;
using RSG.Base.Attributes;
using System.Reflection;

namespace RSG.Base.Windows.Converters
{
    /// <summary>
    /// Extracts an enum values friendly name.
    /// Useful for when a combobox or other items presenter is bound directly against an enums values.
    /// </summary>
    [ValueConversion(typeof(object), typeof(string))]
    public class EnumFriendlyNameConverter : IValueConverter
    {
        #region IValueConverter Members
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object Convert(Object value, Type targetType, Object parameter, CultureInfo culture)
        {
            Type type = value.GetType();

            if (type.IsEnum)
            {
                FieldInfo fi = type.GetField(value.ToString());
                FriendlyNameAttribute[] attributes = fi.GetCustomAttributes(typeof(FriendlyNameAttribute), false) as FriendlyNameAttribute[];

                return (attributes.Length > 0 ? attributes[0].FriendlyName : null);
            }
            else
            {
                throw new NotSupportedException();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object ConvertBack(Object value, Type targetType, Object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
        #endregion // IValueConverter Members
    } // EnumFriendlyNameConverter
}
