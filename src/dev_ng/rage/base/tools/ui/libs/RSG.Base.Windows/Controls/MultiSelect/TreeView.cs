﻿using System;
using System.Windows;
using System.Windows.Input;
using System.Collections;
using System.Collections.Generic;
using System.Windows.Automation.Peers;
using RSG.Base.Collections;

namespace RSG.Base.Windows.Controls.MultiSelect
{
    #region TreeView

    /// <summary>
    /// Defines a multiselect tree view which has a base class of a list box
    /// </summary>
    [StyleTypedProperty(Property = "ItemContainerStyle", StyleTargetType = typeof(TreeViewItem))]
    public class TreeView : System.Windows.Controls.TreeView
    {
        #region Public Events

        public event System.Windows.Controls.SelectionChangedEventHandler SelectionChanged;

        internal List<Object> RemovedItems = new List<Object>();
        internal List<Object> AddedItems = new List<Object>();
        internal void OnSelectionChanged()
        {
            System.Windows.Controls.SelectionChangedEventHandler handler = this.SelectionChanged;
            if (handler != null)
                this.SelectionChanged(this, new System.Windows.Controls.SelectionChangedEventArgs(System.Windows.Controls.ListBox.SelectionChangedEvent, RemovedItems, AddedItems));
        }
        
        #endregion // Public Events

        #region Containers

        /// <summary>
        /// Makes sure that this custom tree view uses the custom tree view items
        /// </summary>
        /// <returns></returns>
        protected override DependencyObject GetContainerForItemOverride()
        {
            return new TreeViewItem();
        }

        /// <summary>
        /// Makes sure that this custom tree view uses the custom tree view items
        /// </summary>
        /// <returns></returns>
        protected override Boolean IsItemItsOwnContainerOverride(Object item)
        {
            return item is TreeViewItem;
        }

        #endregion

        #region Public Properties

        public new Object SelectedItem
        {
            get { return (Object)GetValue(SelectedItemProperty); }
            set { SetValue(SelectedItemProperty, value); }
        }
        public new static DependencyProperty SelectedItemProperty = DependencyProperty.Register("SelectedItem",
         typeof(Object), typeof(TreeView), new FrameworkPropertyMetadata(null));     

        public System.Windows.Controls.SelectionMode SelectionMode
        {
            get { return (System.Windows.Controls.SelectionMode)GetValue(SelectionModeProperty); }
            set { SetValue(SelectionModeProperty, value); }
        }
        public static DependencyProperty SelectionModeProperty = DependencyProperty.Register("SelectionMode",
         typeof(System.Windows.Controls.SelectionMode), typeof(TreeView), new FrameworkPropertyMetadata(System.Windows.Controls.SelectionMode.Single));     

        public IList SelectedItems
        {
            get { return m_selectedItems; }
            set { m_selectedItems = value; }
        }
        public IList m_selectedItems = new RSG.Base.Collections.ObservableCollection<Object>();

        internal TreeViewItem AnchorItem = null;

        internal Dictionary<Object, TreeViewItem> TreeViewSelectedItems
        {
            get { return m_treeViewSelectedItems; }
            set { m_treeViewSelectedItems = value; }
        }
        internal Dictionary<Object, TreeViewItem> m_treeViewSelectedItems = new Dictionary<Object, TreeViewItem>();
        public TreeViewItem SelectedTreeViewItem = null;

        #endregion

        #region Constructors

        public TreeView()
        {
            this.Focusable = true;
            FocusManager.SetIsFocusScope(this, true);
        }

        #endregion // Constructors

        #region Iternal Functions

        internal void BeginUpdate()
        {
            if (this.SelectedItems is IBeginEndCollection)
                (this.SelectedItems as IBeginEndCollection).BeginUpdate();

            RemovedItems = new List<Object>();
            AddedItems = new List<Object>();
        }

        internal void EndUpdate()
        {
            if (this.SelectedItems is IBeginEndCollection)
                (this.SelectedItems as IBeginEndCollection).EndUpdate();

            this.OnSelectionChanged();
        }

        /// <summary>
        /// Returns a list of all the expanded treeview items, this is so we can use ctrl+A to select them all
        /// </summary>
        internal static IEnumerable<TreeViewItem> GetExpandedTreeViewItems(System.Windows.Controls.ItemsControl control)
        {
            for (int i = 0; i < control.Items.Count; i++)
            {
                TreeViewItem item = null;
                try
                {
                    item = control.ItemContainerGenerator.ContainerFromIndex(i) as TreeViewItem;
                }
                catch
                {
                    continue;
                }

                if (item == null)
                    continue;

                yield return item;

                if (item.IsExpanded)
                {
                    foreach (var subItem in GetExpandedTreeViewItems(item))
                    {
                        yield return subItem;
                    }
                }
            }
        }

        /// <summary>
        /// Returns a list of all the expanded treeview items, this is so we can use ctrl+A to select them all
        /// </summary>
        internal static IEnumerable<Object> GetExpandedTreeViewData(System.Windows.Controls.ItemsControl control)
        {
            for (int i = 0; i < control.Items.Count; i++)
            {
                TreeViewItem item = null;
                try
                {
                    if (control.ItemContainerGenerator.Status == System.Windows.Controls.Primitives.GeneratorStatus.ContainersGenerated)
                    {
                        item = control.ItemContainerGenerator.ContainerFromIndex(i) as TreeViewItem;
                    }
                }
                catch
                {
                    continue;
                }

                if (item == null)
                    continue;

                yield return item.HeaderData;

                if (item.IsExpanded)
                {
                    foreach (var subItem in GetExpandedTreeViewItems(item))
                    {
                        yield return subItem.HeaderData;
                    }
                }
            }
        }

        /// <summary>
        /// Gets called whenever any of the children in this treeview selection state changes, this is responible for making sure the
        /// anchor item is properly set with the correct focus
        /// </summary>
        internal void ChangeSingleSelection(Object data, TreeViewItem item, Boolean selected)
        {
            if (selected == true)
            {
                AnchorItem = item;
            }

            if (this.SelectionMode == System.Windows.Controls.SelectionMode.Single)
            {
                this.SelectedItem = selected == true ? data : null;
                this.TreeViewSelectedItems.Clear();
                this.TreeViewSelectedItems.Add(item.HeaderData, item);
            }
            else
            {
                this.SelectedItem = null;
                this.SelectedTreeViewItem = null;
                if (selected == true)
                {
                    this.AddToSelectedItems(data);
                    if (item.HeaderData != null)
                    {
                        if (!this.TreeViewSelectedItems.ContainsKey(item.HeaderData))
                            this.TreeViewSelectedItems.Add(item.HeaderData, item);

                        this.SelectedTreeViewItem = this.TreeViewSelectedItems[this.SelectedItems[0]];
                    }

                    this.SelectedItem = this.SelectedItems[0];
                }
                else
                {
                    this.RemoveFromSelectedItems(data);
                    if (this.TreeViewSelectedItems.ContainsKey(item.HeaderData))
                        this.TreeViewSelectedItems.Remove(item.HeaderData); 

                    if (this.SelectedItems.Count > 0)
                        this.SelectedItem = this.SelectedItems[0];

                    if (this.TreeViewSelectedItems.Count > 0)
                        this.SelectedTreeViewItem = this.TreeViewSelectedItems[this.SelectedItems[0]];
                }
            }
        }

        internal void ClearSelectionApartFrom(IList<TreeViewItem> exceptions, Boolean notifyChange)
        {
            if (notifyChange == true)
                this.BeginUpdate();

            List<TreeViewItem> tempItemList = new List<TreeViewItem>();
            foreach (TreeViewItem item in this.TreeViewSelectedItems.Values)
            {
                if (exceptions == null || (exceptions != null && !exceptions.Contains(item)))
                    tempItemList.Add(item);
            }
            foreach (TreeViewItem item in tempItemList)
            {
                item.IsSelected = false;
                // Since the header could be disconnected need to update the binding in code
                System.Windows.Data.BindingExpression expression = item.GetBindingExpression(TreeViewItem.IsSelectedProperty);
                if (expression != null && expression.ParentBindingBase != null)
                {
                    System.Windows.Data.Binding binding = (expression.ParentBindingBase as System.Windows.Data.Binding);
                    if (binding.Mode == System.Windows.Data.BindingMode.TwoWay || binding.Mode == System.Windows.Data.BindingMode.OneWayToSource)
                    {
                        PropertyPath path = binding.Path;
                        System.Reflection.PropertyInfo info = item.HeaderData.GetType().GetProperty(path.Path);
                        if (info != null)
                        {
                            info.SetValue(item.HeaderData, false, null);
                        }
                    }
                }
            }

            if (notifyChange == true)
                this.EndUpdate();
        }

        #endregion // Iternal Functions

        #region Private Functions

        /// <summary>
        /// Used to add a selected item to the selected items list
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        private Boolean AddToSelectedItems(Object data)
        {
            if (!this.SelectedItems.Contains(data))
            {
                this.SelectedItems.Add(data);
                return true;
            }
            return false;
        }

        /// <summary>
        /// Used to remove a selected item to the selected items list
        /// </summary>
        private Boolean RemoveFromSelectedItems(Object data)
        {
            if (this.SelectedItems.Contains(data))
            {
                this.SelectedItems.Remove(data);
                return true;
            }
            return false;
        }

        /// <summary>
        /// Gets called when the children of this item control get changed in some way. We need to make sure that
        /// if any changes happen these changes get represented in the selected items property
        /// </summary>
        /// <param name="e"></param>
        protected override void OnItemsChanged(System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Remove)
            {
                this.BeginUpdate();

                foreach (var item in e.OldItems)
                {
                    if (this.SelectedItems.Contains(item))
                    {
                        this.SelectedItems.Remove(item);
                    }
                }

                if (this.Items.Count >= 1)
                {
                    if (this.SelectionMode == System.Windows.Controls.SelectionMode.Single)
                    {
                        TreeViewItem item = (TreeViewItem)this.ItemContainerGenerator.ContainerFromIndex(0);
                        item.IsSelected = true;
                    }
                    else
                    {
                        Boolean selectFirstItem = true;
                        foreach (TreeViewItem item in TreeView.GetExpandedTreeViewItems(this))
                        {
                            if (item.IsSelected == true)
                            {
                                selectFirstItem = false;
                                break;
                            }
                        }
                        if (selectFirstItem == true)
                        {
                            TreeViewItem item = (TreeViewItem)this.ItemContainerGenerator.ContainerFromIndex(0);
                            item.IsSelected = true;
                        }
                    }
                }

                this.EndUpdate();
            }

            base.OnItemsChanged(e);
        }

        #endregion // Private Functions

        #region Public Functions

        /// <summary>
        /// Sets the selected items to count 0 and makes sure that all treeviewitems
        /// selection state is set to false
        /// </summary>
        public void ClearSelection()
        {
            if (this.SelectedItems.Count == 0 || this.TreeViewSelectedItems.Count == 0)
                return;

            if (this.SelectedItems.Count != this.TreeViewSelectedItems.Count)
                return;

            this.BeginUpdate();
            List<TreeViewItem> tempItemList = new List<TreeViewItem>();
            foreach (TreeViewItem item in this.TreeViewSelectedItems.Values)
            {
                tempItemList.Add(item);
            }
            List<Object> tempDataList = new List<Object>();
            foreach (Object item in this.SelectedItems)
            {
                tempDataList.Add(item);
            }
            for (int i = 0; i < tempItemList.Count; i++)
            {
                TreeViewItem item = tempItemList[i];
                Object data = tempDataList[i];

                item.IsSelected = false;
                // Since the header is disconnected need to update the binding in code
                System.Windows.Data.BindingExpression expression = item.GetBindingExpression(TreeViewItem.IsSelectedProperty);
                if (expression != null && expression.ParentBindingBase != null)
                {
                    System.Windows.Data.Binding binding = (expression.ParentBindingBase as System.Windows.Data.Binding);
                    if (binding.Mode == System.Windows.Data.BindingMode.TwoWay || binding.Mode == System.Windows.Data.BindingMode.OneWayToSource)
                    {
                        PropertyPath path = binding.Path;
                        System.Reflection.PropertyInfo info = data.GetType().GetProperty(path.Path);
                        if (info != null)
                        {
                            info.SetValue(data, false, null);
                        }
                    }
                }
            }

            this.SelectedItems.Clear();
            this.TreeViewSelectedItems.Clear();
            tempItemList.Clear();
            tempDataList.Clear();

            this.EndUpdate();
        }
       
        #endregion // Public Functions
    }

    #endregion // TreeView

    #region TreeViewItem

    /// <summary>
    /// Defines a tree view item for the multiselect treeview
    /// </summary>
    public class TreeViewItem : System.Windows.Controls.TreeViewItem
    {
        #region Public Events

        /// <summary>
        /// Gets fired when the drag area for the item gets a PreviewMouseLeftButtonDown event
        /// </summary>
        public static readonly RoutedEvent DragAreaMouseDownEvent = EventManager.RegisterRoutedEvent(
            "DragTap", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(TreeViewItem));

        public event RoutedEventHandler DragAreaMouseDown
        {
            add { AddHandler(DragAreaMouseDownEvent, value); }
            remove { RemoveHandler(DragAreaMouseDownEvent, value); }
        }

        #endregion // Public Events

        #region Tree

        /// <summary> 
        /// Walks up the parent chain of TreeViewItems to the top TreeView.  
        /// </summary> 
        internal TreeView ParentTreeView
        {
            get
            {
                System.Windows.Controls.ItemsControl parent = ParentItemsControl;
                while (parent != null)
                {
                    TreeView tv = parent as TreeView;
                    if (tv != null)
                    {
                        return tv;
                    }

                    parent = System.Windows.Controls.ItemsControl.ItemsControlFromItemContainer(parent);
                }

                return null;
            }
        }

        /// <summary>  
        /// Returns the immediate parent TreeViewItem. Null if the parent is a TreeView. 
        /// </summary>  
        internal TreeViewItem ParentTreeViewItem
        {
            get
            {
                return ParentItemsControl as TreeViewItem;
            }
        }

        /// <summary>  
        /// Returns the immediate parent ItemsControl.  
        /// </summary> 
        internal System.Windows.Controls.ItemsControl ParentItemsControl
        {
            get
            {
                return System.Windows.Controls.ItemsControl.ItemsControlFromItemContainer(this);
            }
        }

        TreeView StoredTreeView = null;

        #endregion // Tree

        #region Containers

        /// <summary> 
        /// Returns true if the item is or should be its own container.  
        /// </summary>
        protected override bool IsItemItsOwnContainerOverride(Object item)
        {
            return item is TreeViewItem;
        }

        /// <summary>  
        /// Create or identify the element used to display the given item. 
        /// </summary>
        protected override DependencyObject GetContainerForItemOverride()
        {
            return new TreeViewItem();
        }

        private TreeViewItem GetNextSibling()
        {
            System.Windows.Controls.ItemsControl parent = ParentItemsControl;
            if (parent != null)
            {
                if (this.Items.Count > 0 && this.IsExpanded == true)
                {
                    return this.ItemContainerGenerator.ContainerFromIndex(0) as TreeViewItem;
                }
                else 
                {
                    int nextIndex = parent.ItemContainerGenerator.IndexFromContainer(this) + 1;
                    while (parent != null && parent.Items.Count <= nextIndex)
                    {
                        System.Windows.Controls.ItemsControl nextparent = System.Windows.Controls.ItemsControl.ItemsControlFromItemContainer(parent);
                        if (nextparent != null)
                        {
                            nextIndex = nextparent.ItemContainerGenerator.IndexFromContainer(parent) + 1;
                        }
                        parent = nextparent;
                    }

                    if (parent != null)
                    {
                        DependencyObject next = parent.ItemContainerGenerator.ContainerFromIndex(nextIndex);
                        return next as TreeViewItem;
                    }
                }
            }
            return null;
        }

        private TreeViewItem GetPreviousSibling()
        {
            System.Windows.Controls.ItemsControl parent = ParentItemsControl;
            if (parent != null)
            {
                int previousIndex = parent.ItemContainerGenerator.IndexFromContainer(this) - 1;
                if (parent.Items.Count > previousIndex && previousIndex >= 0)
                {
                    DependencyObject next = parent.ItemContainerGenerator.ContainerFromIndex(previousIndex);
                    if (next is TreeViewItem)
                    {
                        if ((next as TreeViewItem).IsExpanded && (next as TreeViewItem).Items.Count > 0)
                        {
                            return (next as TreeViewItem).ItemContainerGenerator.ContainerFromIndex((next as TreeViewItem).Items.Count - 1) as TreeViewItem;
                        }
                    }

                    return next as TreeViewItem;
                }
                else
                {
                    if (parent is TreeViewItem)
                    {
                        return parent as TreeViewItem;
                    }
                }
            }
            return null;
        }

        TreeViewItem StoredParent = null;
        internal int StoredContainerIndex = -1;
        List<TreeViewItem> StoredChildren = new List<TreeViewItem>();
        TreeViewItem RemovedItem = null;

        #endregion

        #region Public Properties

        /// <summary>  
        /// The DependencyProperty for the <SEE cref="IsSelected" /> property. 
        /// Default Value: false  
        /// </summary>  
        public new static readonly DependencyProperty IsSelectedProperty =
        DependencyProperty.Register("IsSelected", typeof(Boolean), typeof(TreeViewItem), new FrameworkPropertyMetadata(
                            false, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, new PropertyChangedCallback(OnIsSelectedChanged)));

        /// <summary>  
        /// Specifies whether this item is selected or not. 
        /// </summary> 
        public new Boolean IsSelected
        {
            get { return (Boolean)GetValue(IsSelectedProperty); }
            set { SetValue(IsSelectedProperty, value); }
        }

        private static void OnIsSelectedChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            TreeViewItem item = (TreeViewItem)sender;
            bool isSelected = (bool)e.NewValue;

            item.Select(isSelected);

            if (isSelected)
            {
                item.OnSelected(new RoutedEventArgs(SelectedEvent, item));
            }
            else
            {
                item.OnUnselected(new RoutedEventArgs(UnselectedEvent, item));
            }
        }

        /// <summary>  
        /// The DependencyProperty for the <SEE cref="IsFocusedSync" /> property. 
        /// Default Value: false  
        /// </summary> 
        public static readonly DependencyProperty IsFocusedSyncProperty =
        DependencyProperty.Register("IsFocusedSync", typeof(Boolean), typeof(TreeViewItem), new FrameworkPropertyMetadata(
                            false, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, new PropertyChangedCallback(OnIsFocusedSyncChanged)));

        /// <summary>  
        /// Specifies whether this item has focus or not
        /// </summary> 
        public Boolean IsFocusedSync
        {
            get { return (Boolean)GetValue(IsFocusedSyncProperty); }
            set { SetValue(IsFocusedSyncProperty, value); }
        }

        private static void OnIsFocusedSyncChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            TreeViewItem item = (TreeViewItem)sender;

            if ((Boolean)e.OldValue == false && (Boolean)e.NewValue == true)
            {
                Keyboard.Focus(item);
            }
        }

        internal Object HeaderData;

        #endregion // Public Properties

        #region Constructors

        /// <summary>
        /// Override the default style
        /// </summary>
        static TreeViewItem()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(TreeViewItem), new FrameworkPropertyMetadata(typeof(TreeViewItem)));
        }

        /// <summary>
        /// Creates an instance of this control.  
        /// </summary>
        public TreeViewItem()
        {
            this.Loaded += new RoutedEventHandler(OnLoaded);
            this.ItemContainerGenerator.StatusChanged += new EventHandler(ItemContainerGenerator_StatusChanged);
        }

        void ItemContainerGenerator_StatusChanged(Object sender, EventArgs e)
        {
            if (this.ItemContainerGenerator.Status == System.Windows.Controls.Primitives.GeneratorStatus.ContainersGenerated)
            {
                StoredChildren.Clear();
                for (int i = 0; i < this.Items.Count; i++)
                {
                    TreeViewItem childContainer = this.ItemContainerGenerator.ContainerFromIndex(i) as TreeViewItem;
                    if (childContainer != null)
                    {
                        StoredChildren.Add(childContainer);
                        childContainer.StoredContainerIndex = i;
                    }
                }
            }
        }
        
        void OnLoaded(Object sender, RoutedEventArgs e)
        {
            this.StoredTreeView = ParentTreeView;
            this.StoredParent = ParentTreeViewItem;

            // The header could have set the is selected property through binding and therefore we need to
            // add this into the tree selection if selected
            this.Select(this.IsSelected);

            this.Loaded -= new RoutedEventHandler(OnLoaded);
        }

        #endregion // Constructors

        #region Protected Functions

        protected override void OnHeaderChanged(Object oldHeader, Object newHeader)
        {
            if (newHeader != null)
            {
                Type type = newHeader.GetType();
                if (!(type.FullName == "MS.Internal.NamedObject" && type.IsPublic == false))
                {
                    this.HeaderData = newHeader;
                }
                else
                {
                    // If the header becomes disconnected it means that it is being deleted from the tree.
                    // So can no longer be selected, so if it is currently selected unselect it removing it from the tree selection
                    // and make sure it's parent is selected
                    if (this.IsSelected == true)
                    {

                        if (this.StoredTreeView != null)
                            this.StoredTreeView.BeginUpdate();

                        this.Select(false);
                        System.Windows.Data.BindingExpression expression = this.GetBindingExpression(TreeViewItem.IsSelectedProperty);
                        if (expression != null && expression.ParentBindingBase != null && this.HeaderData != null)
                        {
                            System.Windows.Data.Binding binding = (expression.ParentBindingBase as System.Windows.Data.Binding);
                            if (binding.Mode == System.Windows.Data.BindingMode.TwoWay || binding.Mode == System.Windows.Data.BindingMode.OneWayToSource)
                            {
                                PropertyPath path = binding.Path;
                                System.Reflection.PropertyInfo info = this.HeaderData.GetType().GetProperty(path.Path);
                                if (info != null)
                                {
                                    info.SetValue(this.HeaderData, false, null);
                                }
                            }
                        }


                        if (this.StoredParent != null)
                        {
                            Type parentType = this.StoredParent.Header.GetType();
                            if (!(parentType.FullName == "MS.Internal.NamedObject" && parentType.IsPublic == false))
                            {
                                if (this.StoredParent.StoredChildren.Count - 1 <= 0)
                                {
                                    this.StoredParent.IsSelected = true;
                                }
                                else
                                {
                                    this.StoredParent.RemovedItem = this;
                                    base.OnHeaderChanged(oldHeader, newHeader);
                                    return;
                                }
                            }
                        }

                        if (this.StoredTreeView != null)
                            this.StoredTreeView.EndUpdate();
                    }
                }
            }
            base.OnHeaderChanged(oldHeader, newHeader);
        }

        /// <summary>
        /// Gets the parts of the template and adds event handlers to them
        /// </summary>
        public override void OnApplyTemplate()
        {
            DependencyObject d = GetTemplateChild("PART_Drag");

            if (d != null)
            {
                (d as System.Windows.UIElement).PreviewMouseLeftButtonDown += OnDragAreaMouseDown;
            }

            base.OnApplyTemplate();
        }

        /// <summary>
        /// Gets called whenever the drag area in the control template gets a preview mouse down event
        /// </summary>
        private void OnDragAreaMouseDown(Object sender, MouseButtonEventArgs e)
        {
            RoutedEventArgs newEventArgs = new RoutedEventArgs(TreeViewItem.DragAreaMouseDownEvent);
            RaiseEvent(newEventArgs);
        }

        /// <summary>
        /// Gets called whenever this item recieves keyboard focus
        /// </summary>
        protected new void OnGotFocus(RoutedEventArgs e)
        {
            this.IsFocusedSync = true;
            base.OnGotFocus(e);
        }

        /// <summary>
        /// Gets called when the control loses focus
        /// </summary>
        protected override void OnLostFocus(RoutedEventArgs e)
        {
            this.IsFocusedSync = false;
            base.OnLostFocus(e);
        }

        /// <summary>
        /// Called when the the item gets a 
        /// </summary>
        protected override void OnMouseDown(MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
            {
                TreeViewItem source = null;
                if (!(e.OriginalSource is TreeViewItem) && (e.OriginalSource is UIElement))
                {
                    DependencyObject parentVisual = System.Windows.Media.VisualTreeHelper.GetParent(e.OriginalSource as UIElement);
                    source = parentVisual as TreeViewItem;
                    while (parentVisual != null && source == null)
                    {
                        parentVisual = System.Windows.Media.VisualTreeHelper.GetParent(parentVisual);
                        source = parentVisual as TreeViewItem;
                    }
                    if (source == null)
                        return;
                }

                TreeView tree = ParentTreeView;
                System.Windows.Controls.ItemsControl parent = ParentItemsControl;
                if ((tree != null) && (parent != null) && source == this)
                {
                    switch (tree.SelectionMode)
                    {
                        case System.Windows.Controls.SelectionMode.Single:
                            source.HandleMouseDownSingleSelection(tree, parent);
                            break;
                        case System.Windows.Controls.SelectionMode.Multiple:
                            source.HandleMouseDownMultipleSelection(tree, parent);
                            break;

                        case System.Windows.Controls.SelectionMode.Extended:
                            source.HandleMouseDownExtendedSelection(tree, parent);
                            break;

                        default:
                            break;
                    }
                }
            }
            else if (e.ChangedButton == MouseButton.Right)
            {
                TreeViewItem source = null;
                if (!(e.OriginalSource is TreeViewItem) && (e.OriginalSource is UIElement))
                {
                    DependencyObject parentVisual = System.Windows.Media.VisualTreeHelper.GetParent(e.OriginalSource as UIElement);
                    source = parentVisual as TreeViewItem;
                    while (parentVisual != null && source == null)
                    {
                        parentVisual = System.Windows.Media.VisualTreeHelper.GetParent(parentVisual);
                        source = parentVisual as TreeViewItem;
                    }
                    if (source == null)
                        return;
                }

                TreeView tree = ParentTreeView;
                System.Windows.Controls.ItemsControl parent = ParentItemsControl;
                if ((tree != null) && (parent != null) && source == this)
                {
                    source.HandleMouseDownSingleSelection(tree, parent);
                }
            }
            base.OnMouseDown(e);
        }

        /// <summary>
        /// Gets called whenever this item gets a mouse up event
        /// </summary>
        protected override void OnMouseUp(MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
            {
                TreeViewItem source = null;
                if (!(e.OriginalSource is TreeViewItem) && (e.OriginalSource is UIElement))
                {
                    DependencyObject parentVisual = System.Windows.Media.VisualTreeHelper.GetParent(e.OriginalSource as UIElement);
                    source = parentVisual as TreeViewItem;
                    while (parentVisual != null && source == null)
                    {
                        parentVisual = System.Windows.Media.VisualTreeHelper.GetParent(parentVisual);
                        source = parentVisual as TreeViewItem;
                    }
                    if (source == null)
                        return;
                }

                TreeView tree = ParentTreeView;
                System.Windows.Controls.ItemsControl parent = ParentItemsControl;
                if ((tree != null) && (parent != null) && source == this)
                {
                    switch (tree.SelectionMode)
                    {
                        case System.Windows.Controls.SelectionMode.Extended:
                            source.HandleMouseUpExtendedSelection(tree, parent);
                            break;

                        default:
                            break;
                    }
                }
            }
            base.OnMouseDown(e);
        }

        /// <summary>
        /// Handles the implementation of what happens when a mouse button has been pressed with single selection mode activated
        /// </summary>
        protected void HandleMouseDownSingleSelection(TreeView tree, System.Windows.Controls.ItemsControl parent)
        {
            // If this item is already selected we leave it selected
            if (this.IsSelected == false)
            {
                tree.BeginUpdate();

                // If we are using a single selection mode we need to make sure that all other
                // items are deselected before selecting this one.
                foreach (TreeViewItem item in TreeView.GetExpandedTreeViewItems(tree))
                {
                    item.IsSelected = false;
                }

                this.IsSelected = true;

                tree.EndUpdate();
            }
        }

        /// <summary>
        /// Handles the implementation of what happens when a mouse button has been pressed with mulitple selection mode activated
        /// </summary>
        protected void HandleMouseDownMultipleSelection(TreeView tree, System.Windows.Controls.ItemsControl parent)
        {
            // We toggle the selection of this item
            tree.BeginUpdate();

            this.IsSelected = !this.IsSelected;

            tree.EndUpdate();
        }

        /// <summary>
        /// Handles the implementation of what happens when a mouse button has been pressed with extended selection mode activated
        /// </summary>
        protected void HandleMouseDownExtendedSelection(TreeView tree, System.Windows.Controls.ItemsControl parent)
        {
            // This requires extra work to decide if the item should be selected or unselected.

            if (((System.Windows.Input.Keyboard.Modifiers & ModifierKeys.Shift) == ModifierKeys.Shift) && tree.AnchorItem != null)
            {
                if (this == tree.AnchorItem)
                    return;

                tree.BeginUpdate();

                Boolean startCollecting = false;
                List<TreeViewItem> collection = new List<TreeViewItem>();
                collection.Add(this);
                foreach (TreeViewItem item in TreeView.GetExpandedTreeViewItems(tree))
                {
                    if (startCollecting == true)
                    {
                        if (item == this || item == tree.AnchorItem)
                        { 
                            startCollecting = false;
                        }
                        else
                        {
                            if (!collection.Contains(item))
                                collection.Add(item);
                        }
                    }
                    else
                    {
                        if (item == this || item == tree.AnchorItem)
                        {
                            startCollecting = true;
                        }
                    }   
                }
                // Make sure we keep the same snchor item but move over the focus
                TreeViewItem anchor = tree.AnchorItem;
                collection.Add(tree.AnchorItem);
                tree.ClearSelectionApartFrom(collection, false);
                foreach (TreeViewItem item in collection)
                {
                    item.IsSelected = true;
                }
                tree.AnchorItem = anchor;

                tree.EndUpdate();
            }
            // If the control modifier is down we don't do anything as because in this case the selection happens on mouse up
            else if (((System.Windows.Input.Keyboard.Modifiers & ModifierKeys.Control) == ModifierKeys.Control) && tree.AnchorItem != null)
            {

            }
            // If no modifiers are pressed then we just do a normal single selection
            else if ((System.Windows.Input.Keyboard.Modifiers & ModifierKeys.None) == ModifierKeys.None)
            {
                // If it is currently selected we unselect it with the mouse up event
                if (this.IsSelected == true)
                    return;

                if (this.IsSelected == false || tree.SelectedItems.Count > 1)
                {
                    tree.BeginUpdate();

                    tree.ClearSelectionApartFrom(null, false);
                    this.IsSelected = true;                    

                    tree.EndUpdate();
                }
            }
        }

        /// <summary>
        /// Handles the implementation of what happens when a mouse button has been released with extended selection mode activated
        /// </summary>
        protected void HandleMouseUpExtendedSelection(TreeView tree, System.Windows.Controls.ItemsControl parent)
        {
            if (((System.Windows.Input.Keyboard.Modifiers & ModifierKeys.Control) == ModifierKeys.Control))
            {
                tree.BeginUpdate();

                this.IsSelected = !this.IsSelected;

                tree.EndUpdate();
            }
            else if (this.IsSelected == true && tree.SelectedItems.Count > 1)
            {
                if (((System.Windows.Input.Keyboard.Modifiers & ModifierKeys.Shift) == ModifierKeys.Shift))
                    return;

                tree.BeginUpdate();

                tree.ClearSelectionApartFrom(null, false);
                this.IsSelected = true;

                tree.EndUpdate();
            }
        }

        /// <summary>
        /// Gets called when this item is collapsed, in this case need to make sure that
        /// the child items are deselected and that this one is selected if needed
        /// </summary>
        /// <param name="e"></param>
        protected override void OnCollapsed(RoutedEventArgs e)
        {
            TreeView tree = ParentTreeView;
            System.Windows.Controls.ItemsControl parent = ParentItemsControl;
            if ((tree != null) && (parent != null))
            {
                switch (tree.SelectionMode)
                {
                    case System.Windows.Controls.SelectionMode.Single:
                        HandleSingleCollapsed(tree, parent);
                        break;
                    case System.Windows.Controls.SelectionMode.Multiple:
                        HandleMultipleCollapsed(tree, parent);
                        break;

                    case System.Windows.Controls.SelectionMode.Extended:
                        HandleMultipleCollapsed(tree, parent);
                        break;

                    default:
                        break;
                }
            }

            base.OnCollapsed(e);
        }

        /// <summary>
        /// If one of its children is selected need to deselect it and select self
        /// </summary>
        protected void HandleSingleCollapsed(TreeView tree, System.Windows.Controls.ItemsControl parent)
        {
            tree.BeginUpdate();

            foreach (TreeViewItem child in TreeViewItem.GetSelectedItems(this))
            {
                child.IsSelected = false;
            }
            Boolean selectThis = true;
            foreach (TreeViewItem item in TreeView.GetExpandedTreeViewItems(tree))
            {
                if (item.IsSelected == true)
                {
                    selectThis = false;
                    break;
                }
            }

            if (selectThis == true)
            {
                this.IsSelected = true;
            }

            tree.EndUpdate();
        }

        /// <summary>
        /// If one or more of its children is selected need to deselect it and select self iff there is nothing else selected
        /// </summary>
        protected void HandleMultipleCollapsed(TreeView tree, System.Windows.Controls.ItemsControl parent)
        {
            Boolean selectedChildren = false;
            foreach (TreeViewItem child in TreeViewItem.GetSelectedItems(this))
            {
                selectedChildren = true;
                break;
            }

            if (selectedChildren == true)
            {
                tree.BeginUpdate();

                foreach (TreeViewItem child in TreeViewItem.GetSelectedItems(this))
                {
                    child.IsSelected = false;
                }

                // If nothing else if selected select this item
                Boolean selectThis = true;
                foreach (TreeViewItem item in TreeView.GetExpandedTreeViewItems(tree))
                {
                    if (item.IsSelected == true)
                    {
                        selectThis = false;
                        break;
                    }
                }

                if (selectThis == true)
                {
                    this.IsSelected = true;
                }

                tree.EndUpdate();
            }
        }

        /// <summary>
        /// Gets called whenever the item gets a key down event
        /// </summary>
        protected override void OnKeyDown(KeyEventArgs e)
        {
            TreeView tree = ParentTreeView;
            System.Windows.Controls.ItemsControl parent = ParentItemsControl;
            if ((tree != null) && (parent != null) && e.Source == this)
            {
                switch (tree.SelectionMode)
                {
                    case System.Windows.Controls.SelectionMode.Single:
                        (e.Source as TreeViewItem).HandleKeyDownSingleSelection(tree, parent, e);
                        break;
                    case System.Windows.Controls.SelectionMode.Multiple:
                        (e.Source as TreeViewItem).HandleKeyDownMultipleSelection(tree, parent, e);
                        break;

                    case System.Windows.Controls.SelectionMode.Extended:
                        (e.Source as TreeViewItem).HandleKeyDownExtendedSelection(tree, parent, e);
                        break;

                    default:
                        break;
                }
            }

            if (e.Source == this)
                base.OnKeyDown(e);
        }

        /// <summary>
        /// Handles the implementation of what happens when a keyboard key has been pressed with single selection mode activated
        /// </summary>
        protected void HandleKeyDownSingleSelection(TreeView tree, System.Windows.Controls.ItemsControl parent, KeyEventArgs e)
        {
            if (e.Key == Key.Up || e.Key == Key.Down)
            {
                TreeViewItem previousItem = null;
                TreeViewItem nextItem = null;
                Boolean foundItem = false;
                foreach (TreeViewItem item in TreeView.GetExpandedTreeViewItems(tree))
                {
                    if (foundItem == true && nextItem == null)
                    {
                        nextItem = item;
                    }

                    if (item == this)
                    {
                        foundItem = true;
                    }

                    if (foundItem == false)
                    {
                        previousItem = item;
                    }
                }
                if (e.Key == Key.Up)
                {
                    if (foundItem == true && previousItem != null)
                    {
                        tree.BeginUpdate();

                        this.IsSelected = false;
                        previousItem.IsSelected = true;

                        tree.EndUpdate();
                    }
                }
                else
                {
                    if (foundItem == true && nextItem != null)
                    {
                        tree.BeginUpdate();

                        this.IsSelected = false;
                        nextItem.IsSelected = true;

                        tree.EndUpdate();
                    }
                }
            }
            else if (e.Key == Key.Left)
            {
                // Collapses item or selects the parent item if one is available
                if (this.HasItems)
                {
                    if (this.IsExpanded == true)
                    {
                        this.IsExpanded = false;
                    }
                    else
                    {
                        if (parent is TreeViewItem)
                        {
                            tree.BeginUpdate();

                            this.IsSelected = false;
                            (parent as TreeViewItem).IsSelected = true;

                            tree.EndUpdate();
                        }
                    }
                }
                else
                {
                    if (parent is TreeViewItem)
                    {
                        tree.BeginUpdate();

                        this.IsSelected = false;
                        (parent as TreeViewItem).IsSelected = true;

                        tree.EndUpdate();
                    }
                }
            }
            else if (e.Key == Key.Right)
            {
                // Expands item or selects first child is one is available
                if (this.HasItems)
                {
                    if (this.IsExpanded == false)
                    {
                        this.IsExpanded = true;
                    }
                    else
                    {
                        var item = this.ItemContainerGenerator.ContainerFromIndex(0);
                        if (item is TreeViewItem)
                        {
                            tree.BeginUpdate();

                            this.IsSelected = false;
                            (item as TreeViewItem).IsSelected = true;

                            tree.EndUpdate();
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Handles the implementation of what happens when a keyboard key has been pressed with single selection mode activated
        /// </summary>
        protected void HandleKeyDownMultipleSelection(TreeView tree, System.Windows.Controls.ItemsControl parent, KeyEventArgs e)
        {
            return;
        }

        /// <summary>
        /// Handles the implementation of what happens when a keyboard key has been pressed with single selection mode activated
        /// </summary>
        protected void HandleKeyDownExtendedSelection(TreeView tree, System.Windows.Controls.ItemsControl parent, KeyEventArgs e)
        {
            if (e.Key == Key.Up || e.Key == Key.Down)
            {
                if (((System.Windows.Input.Keyboard.Modifiers & ModifierKeys.Shift) == ModifierKeys.Shift) && tree.AnchorItem != null)
                {
                    TreeViewItem focusItem = null;
                    if (e.Key == Key.Up)
                    {
                        focusItem = this.GetPreviousSibling();
                    }
                    else
                    {
                        focusItem = this.GetNextSibling();
                    }
                    if (focusItem != null)
                    {
                        if (focusItem == tree.AnchorItem)
                        {
                            tree.BeginUpdate();

                            foreach (TreeViewItem item in TreeView.GetExpandedTreeViewItems(tree))
                            {
                                item.IsSelected = false;
                            }

                            tree.AnchorItem.IsSelected = true;

                            tree.EndUpdate();
                            return;
                        }

                        tree.BeginUpdate();

                        Boolean startCollecting = false;
                        List<TreeViewItem> collection = new List<TreeViewItem>();
                        foreach (TreeViewItem item in TreeView.GetExpandedTreeViewItems(tree))
                        {
                            if (startCollecting == true)
                            {
                                if (item == focusItem || item == tree.AnchorItem)
                                {
                                    startCollecting = false;
                                }
                                else
                                {
                                    if (!collection.Contains(item))
                                        collection.Add(item);
                                }
                            }
                            else
                            {
                                if (item == focusItem || item == tree.AnchorItem)
                                {
                                    startCollecting = true;
                                }
                            }
                        }
                        // Make sure we keep the same snchor item but move over the focus
                        TreeViewItem anchor = tree.AnchorItem;
                        collection.Add(tree.AnchorItem);
                        collection.Add(focusItem);
                        tree.ClearSelectionApartFrom(collection, false);
                        focusItem.IsSelected = true;
                        tree.AnchorItem = anchor;

                        tree.EndUpdate();
                    }
                }
                else if (((System.Windows.Input.Keyboard.Modifiers & ModifierKeys.Control) == ModifierKeys.Control) && tree.AnchorItem != null)
                {
                    if (parent is TreeViewItem)
                    {
                        int currentIndex = (parent as TreeViewItem).ItemContainerGenerator.IndexFromContainer(this);
                        if (e.Key == Key.Up)
                        {
                            if (currentIndex > 0)
                            {
                                TreeViewItem item = (parent as TreeViewItem).ItemContainerGenerator.ContainerFromIndex(currentIndex - 1) as TreeViewItem;
                                if (item != null)
                                {
                                    tree.ClearSelectionApartFrom(null, false);
                                    item.IsSelected = true;
                                    item.Focus();
                                    e.Handled = true;
                                }
                            }
                        }
                        else
                        {
                            if ((parent as TreeViewItem).Items.Count > currentIndex + 1)
                            {
                                TreeViewItem item = (parent as TreeViewItem).ItemContainerGenerator.ContainerFromIndex(currentIndex + 1) as TreeViewItem;
                                if (item != null)
                                {
                                    tree.ClearSelectionApartFrom(null, false);
                                    item.IsSelected = true;
                                    item.Focus();
                                    e.Handled = true;
                                }
                            }
                        }  
                    }
                }
                else
                {
                    if (e.Key == Key.Up)
                    {
                        TreeViewItem nextFocus = this.GetPreviousSibling();

                        if (nextFocus != null)
                        {
                            tree.BeginUpdate();

                            tree.ClearSelectionApartFrom(null, false);
                            nextFocus.IsSelected = true;

                            tree.EndUpdate();
                        }
                    }
                    else
                    {
                        TreeViewItem nextFocus = this.GetNextSibling();
                        if (nextFocus != null)
                        {
                            tree.BeginUpdate();

                            tree.ClearSelectionApartFrom(null, false);
                            nextFocus.IsSelected = true;

                            tree.EndUpdate();
                        }
                    }
                }
            }
            else if (e.Key == Key.Left)
            {
                // Collapses item or selects the parent item if one is available
                if (!this.IsFocused)
                {
                    this.Focus();
                }
                else if (this.HasItems)
                {
                    if (this.IsExpanded == true)
                    {
                        this.IsExpanded = false;
                        e.Handled = true;
                    }
                    else
                    {
                        if (parent is TreeViewItem)
                        {
                            tree.BeginUpdate();

                            tree.ClearSelectionApartFrom(null, false);
                            (parent as TreeViewItem).IsSelected = true;
                            (parent as TreeViewItem).Focus();
                            e.Handled = true;

                            tree.EndUpdate();
                        }
                    }
                }
                else
                {
                    if (parent is TreeViewItem)
                    {
                        tree.BeginUpdate();

                        tree.ClearSelectionApartFrom(null, false);
                        (parent as TreeViewItem).IsSelected = true;
                        (parent as TreeViewItem).Focus();
                        e.Handled = true;

                        tree.EndUpdate();
                    }
                }
            }
            else if (e.Key == Key.Right)
            {
                // Expands item or selects first child is one is available
                if (!this.IsFocused)
                {
                    this.Focus();
                }
                else if (this.HasItems)
                {
                    this.IsExpanded = true;
                    e.Handled = true;
                } 
            }

        }

        /// <summary>
        /// When it is selected make sure it gets keyboard focus
        /// </summary>
        protected override void OnSelected(RoutedEventArgs e)
        {
            TreeViewItem parent = ParentItemsControl as TreeViewItem;
            if (parent != null)
            {
                parent.IsExpanded = true;
            }
            this.BringIntoView();

            base.OnSelected(e);
        }

        /// <summary>
        /// When this item is expanded make sure that the parent item is also expanded
        /// </summary>
        protected override void OnExpanded(RoutedEventArgs e)
        {
            TreeViewItem parent = ParentItemsControl as TreeViewItem;
            if (parent != null)
            {
                parent.IsExpanded = true;
            }
            this.BringIntoView();

            base.OnExpanded(e);
        }

        /// <summary>
        /// Gets called when the children of this item control get changed in some way. We need to make sure that
        /// if any changes happen these changes get represented in the selected items property
        /// </summary>
        /// <param name="e"></param>
        protected override void OnItemsChanged(System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (this.RemovedItem != null)
            {
                this.StoredChildren.Remove(this.RemovedItem);

                if (this.StoredChildren.Count > 0)
                {
                    if (this.RemovedItem.StoredContainerIndex >= this.StoredChildren.Count)
                    {
                        this.StoredChildren[this.StoredChildren.Count - 1].IsSelected = true;
                    }
                    else
                    {
                        this.StoredChildren[this.RemovedItem.StoredContainerIndex].IsSelected = true;
                    }
                }
                else
                {
                    this.IsSelected = true;
                }


                this.RemovedItem = null;
                if (this.StoredTreeView != null)
                    this.StoredTreeView.EndUpdate();
            }
            base.OnItemsChanged(e);
        }

        #endregion // Protected Functions

        #region Private Functions

        /// <summary>
        /// Gets called whenever the selection state of this item is changed
        /// </summary>
        /// <param name="selected"></param>
        private void Select(Boolean selected)
        {
            TreeView tree = ParentTreeView;
            System.Windows.Controls.ItemsControl parent = ParentItemsControl;
            if (tree != null && parent != null)
            {
                Object data = parent.ItemContainerGenerator.ItemFromContainer(this);
                if (data == null)
                {
                    data = this;
                }
                else if (data == DependencyProperty.UnsetValue)
                {
                    // Currently disconnected so use the stored header data
                    data = this.HeaderData;
                }
                tree.ChangeSingleSelection(data, this, selected);
            }
            else if (this.StoredTreeView != null)
            {
                Object data = this.HeaderData;
                if (data == null)
                {
                    data = this;
                }
                this.StoredTreeView.ChangeSingleSelection(data, this, selected);
            }
        }

        /// <summary>
        /// Returns a list of all the selected treeview items, this is so we can deselect them all on a collapse
        /// </summary>
        private static IEnumerable<TreeViewItem> GetSelectedItems(System.Windows.Controls.ItemsControl control)
        {
            for (int i = 0; i < control.Items.Count; i++)
            {
                var item = (TreeViewItem)control.ItemContainerGenerator.ContainerFromIndex(i);

                if (item == null)
                    continue;

                if (item.IsSelected == true)
                {
                    yield return item;
                }

                if (item.IsExpanded)
                {
                    foreach (var subItem in GetSelectedItems(item))
                    {
                        yield return subItem;
                    }
                }
            }
        }

        private static Object GetContainerOrItem(TreeViewItem item)
        {
            TreeView tree = item.ParentTreeView;
            System.Windows.Controls.ItemsControl parent = item.ParentItemsControl;
            if (tree != null && parent != null)
            {
                Object data = parent.ItemContainerGenerator.ItemFromContainer(item);
                if (data == null)
                {
                    data = item;
                }
                return data;
            }
            return null;
        }

        #endregion // Private Functions

    }

    #endregion // TreeViewItem

}  
