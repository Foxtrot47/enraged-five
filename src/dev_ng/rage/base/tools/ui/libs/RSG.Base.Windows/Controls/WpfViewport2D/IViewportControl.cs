﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Base.Windows.Controls.WpfViewport2D
{
    public interface IViewportControl
    {
        Viewport2D Viewport { get; set; }

        void SetViewport(Viewport2D viewport);
    }
}
