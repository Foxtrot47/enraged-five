﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows.Data;

namespace RSG.Base.Windows.Converters
{

    /// <summary>
    /// 
    /// </summary>
    public class StringCollectionToStringOneLineConverter : IValueConverter
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public Object Convert(Object value, Type targetType, Object parameter, CultureInfo culture)
        {
            if (!(value is IEnumerable<String>))
                return (null);

            IEnumerable<String> list = (IEnumerable<String>)value;
            int count = list.Count();
            if (0 == count)
                return (String.Empty);
            else if (1 == count)
                return (list.First());
            else
                return (String.Format("{0}, ...", list.First()));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public Object ConvertBack(Object value, Type targetType, Object parameter, CultureInfo culture)
        {
            throw new NotImplementedException("This converter cannot be used in two-way binding.");
        }
    }

} // RSG.Base.Windows.Converters namespace
