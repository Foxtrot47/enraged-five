﻿using System.Windows.Controls;
using System.Windows;
using System.Windows.Controls.Primitives;
using System;
using System.Windows.Input;
using System.Windows.Media;
using System.Collections.Generic;

namespace RSG.Base.Windows.Controls
{
    /// <summary>
    /// </summary>
    public class EventTrackItem : ContentControl
    {
        #region Fields
        private Thumb leftResizer;
        private Thumb rightResizer;

        /// <summary>
        /// Identies the <see cref="SnapMovementToUnit"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty SnapMovementToUnitProperty;

        /// <summary>
        /// Identies the <see cref="SnapResizeToUnit"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty SnapResizeToUnitProperty;

        /// <summary>
        /// Identies the <see cref="IsSelected"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty IsSelectedProperty;
        #endregion

        #region Constructors
        static EventTrackItem()
        {
            SnapMovementToUnitProperty =
                EventControlTrack.SnapMovementToUnitProperty.AddOwner(
                typeof(EventTrackItem),
                new FrameworkPropertyMetadata(
                false,
                FrameworkPropertyMetadataOptions.Inherits));

            SnapResizeToUnitProperty =
                EventControlTrack.SnapResizeToUnitProperty.AddOwner(
                typeof(EventTrackItem),
                new FrameworkPropertyMetadata(
                    false,
                    FrameworkPropertyMetadataOptions.Inherits));

            IsSelectedProperty =
                DependencyProperty.Register(
                "IsSelected",
                typeof(bool),
                typeof(EventTrackItem),
                new PropertyMetadata(false));

            DefaultStyleKeyProperty.OverrideMetadata(
                typeof(EventTrackItem),
                new FrameworkPropertyMetadata(typeof(EventTrackItem)));
        }

        public EventTrackItem()
        {
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets a value indicating whether the movement of events are limited
        /// to unit boundaries.
        /// </summary>
        public bool SnapMovementToUnit
        {
            get { return (bool)this.GetValue(SnapMovementToUnitProperty); }
            set { this.SetValue(SnapMovementToUnitProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the resizing of events are limited
        /// to unit boundaries.
        /// </summary>
        public bool SnapResizeToUnit
        {
            get { return (bool)this.GetValue(SnapResizeToUnitProperty); }
            set { this.SetValue(SnapResizeToUnitProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this track item is currently selected.
        /// </summary>
        public bool IsSelected
        {
            get { return (bool)this.GetValue(IsSelectedProperty); }
            set { this.SetValue(IsSelectedProperty, value); }
        } 
        #endregion

        #region Methods
        /// <summary>
        /// Invoked whenever the application applies the template to this control.
        /// </summary>
        public override void OnApplyTemplate()
        {
            DependencyObject parent = VisualTreeHelper.GetParent(this);
            bool foundPanel = false;
            while (parent != null)
            {
                if (parent is EventPanel)
                {
                    foundPanel = true;
                    break;
                }

                parent = VisualTreeHelper.GetParent(parent);
            }
            if (!foundPanel)
            {
                throw new NotSupportedException("Unable to define a EventTrackItem with a visual parent other than a EventPanel");
            }

            base.OnApplyTemplate();

            Thumb mover = this.GetTemplateChild("PART_Mover") as Thumb;
            if (mover != null)
            {
                mover.DragStarted += MoverDragStarted;
                mover.DragDelta += MoverDragDelta;
                mover.DragCompleted += MoverDragCompleted;
            }

            this.leftResizer = this.GetTemplateChild("PART_LeftResizer") as Thumb;
            if (leftResizer != null)
            {
                leftResizer.DragStarted += ResizerDragStarted;
                leftResizer.DragDelta += ResizerDragDelta;
                leftResizer.DragCompleted += ResizerDragCompleted;
            }

            this.rightResizer = this.GetTemplateChild("PART_RightResizer") as Thumb;
            if (rightResizer != null)
            {
                rightResizer.DragStarted += ResizerDragStarted;
                rightResizer.DragDelta += ResizerDragDelta;
                rightResizer.DragCompleted += ResizerDragCompleted;
            }
        }

        protected override void OnMouseRightButtonDown(MouseButtonEventArgs e)
        {
            base.OnMouseRightButtonDown(e);
            foreach (EventTrackItem selected in this.GetSelectedItems())
            {
                if (selected != this)
                {
                    selected.IsSelected = false;
                }
            }

            this.IsSelected = true;
        }

        protected override void OnMouseUp(MouseButtonEventArgs e)
        {
            base.OnMouseDown(e);
        }

        private bool selectedAtDragStart = false;
        private bool movement = false;
        private void MoverDragStarted(object sender, DragStartedEventArgs e)
        {
            this.SetValue(Panel.ZIndexProperty, 1);
            e.Handled = true;
            this.selectedAtDragStart = this.IsSelected;
            this.movement = false;
        }

        private void MoverDragDelta(object sender, DragDeltaEventArgs e)
        {
            if (e.HorizontalChange == 0.0)
            {
                return;
            }

            this.movement = true;
            this.IsSelected = true;
            EventPanel panel = GetEventPanel();
            if (panel == null)
            {
                throw new NotSupportedException("Unable to define a EventTrackItem with a visual parent other than a EventPanel");
            }

            EventControl eventControl = GetEventControl();
            List<EventTrackItem> selected = this.GetSelectedItems();

            e.Handled = true;
            foreach (EventTrackItem item in selected)
            {
                double maximumUnit = eventControl == null ? double.MaxValue : eventControl.MaximumUnitValue;
                maximumUnit -= EventPanel.GetUnitLength(item);
                double unitDelta = e.HorizontalChange / panel.PixelsPerUnit;
                double newStart = EventPanel.GetStart(item) + unitDelta;
                if (this.SnapMovementToUnit)
                {
                    newStart = EventPanel.GetStart(item) + unitDelta;
                    if (e.HorizontalChange < 0)
                    {
                        newStart = System.Math.Ceiling(newStart);
                    }
                    else
                    {
                        newStart = System.Math.Floor(newStart);
                    }
                }

                if (newStart == EventPanel.GetStart(item))
                {
                    return;
                }

                newStart = System.Math.Max(newStart, 0.0);
                newStart = System.Math.Min(newStart, maximumUnit);
                EventPanel.SetStart(item, newStart);
            }           
        }

        private void MoverDragCompleted(object sender, DragCompletedEventArgs e)
        {
            this.SetValue(Panel.ZIndexProperty, 0);
            e.Handled = false;
            if (!selectedAtDragStart)
            {
                this.IsSelected = true;
            }
            else
            {
                if (this.movement)
                {
                    this.IsSelected = true;
                }
                else
                {
                    this.IsSelected = false;
                }
            }
        }

        private void ResizerDragStarted(object sender, DragStartedEventArgs e)
        {
            this.SetValue(Panel.ZIndexProperty, 1);
            e.Handled = true;
        }

        private void ResizerDragDelta(object sender, DragDeltaEventArgs e)
        {
            if (e.HorizontalChange == 0.0)
            {
                return;
            }

            EventPanel panel = GetEventPanel();
            if (panel == null)
            {
                throw new NotSupportedException("Unable to define a EventTrackItem with a visual parent other than a EventPanel");
            }

            EventControl eventControl = GetEventControl();

            double maximumUnit = eventControl == null ? double.MaxValue : eventControl.MaximumUnitValue;

            e.Handled = true;
            double unitDelta = e.HorizontalChange / panel.PixelsPerUnit;
            double newStart, oldStart;
            newStart = oldStart = EventPanel.GetStart(this);

            double newEnd, oldEnd;
            newEnd = oldEnd = EventPanel.GetStart(this) + EventPanel.GetUnitLength(this);

            if (this.SnapResizeToUnit)
            {
                if (sender == leftResizer)
                {
                    newStart = System.Math.Max(newStart + unitDelta, 0.0);
                    if (e.HorizontalChange < 0)
                    {
                        newStart = System.Math.Ceiling(newStart);
                    }
                    else
                    {
                        newStart = System.Math.Max(System.Math.Floor(newStart), oldStart);
                    }
                }
                else
                {
                    newEnd += unitDelta;
                    if (e.HorizontalChange < 0)
                    {
                        newEnd = System.Math.Ceiling(newEnd);
                    }
                    else
                    {
                        newEnd = System.Math.Max(System.Math.Floor(newEnd), oldEnd);
                    }
                }
            }
            else
            {
                if (sender == leftResizer)
                {
                    newStart = System.Math.Max(newStart + unitDelta, 0.0);
                }
                else
                {
                    newEnd += unitDelta;
                }
            }

            if (newEnd != oldEnd)
            {
                double newLength = System.Math.Max(newEnd - newStart, 0.0);
                if (newLength + newStart > maximumUnit)
                {
                    newLength = maximumUnit - newStart;
                }

                EventPanel.SetUnitLength(this, newLength);
            }

            if (newStart != oldStart)
            {
                double newLength = 0.0;
                if (newStart > newEnd)
                {
                    newStart = newEnd;
                }
                else
                {
                    newLength = System.Math.Max(oldEnd - newStart, 0.0);
                }

                System.Diagnostics.Debug.Print("Start {0}, Length {1}", newStart, newLength);
                EventPanel.SetStart(this, newStart);
                EventPanel.SetUnitLength(this, newLength);
            }
        }

        private void ResizerDragCompleted(object sender, DragCompletedEventArgs e)
        {
            this.SetValue(Panel.ZIndexProperty, 0);
            e.Handled = true;
        } 

        private EventPanel GetEventPanel()
        {
            DependencyObject parent = VisualTreeHelper.GetParent(this);
            EventPanel panel = parent as EventPanel;
            while (parent != null && panel == null)
            {
                parent = VisualTreeHelper.GetParent(parent);
                panel = parent as EventPanel;
            }

            return panel;
        }

        private EventControl GetEventControl()
        {
            DependencyObject parent = VisualTreeHelper.GetParent(this);
            EventControl control = parent as EventControl;
            while (parent != null && control == null)
            {
                parent = VisualTreeHelper.GetParent(parent);
                control = parent as EventControl;
            }

            return control;
        }

        private List<EventTrackItem> GetSelectedItems()
        {
            List<EventTrackItem> result = new List<EventTrackItem>();
            DependencyObject parent = VisualTreeHelper.GetParent(this);
            EventControl control = parent as EventControl;
            while (parent != null && control == null)
            {
                parent = VisualTreeHelper.GetParent(parent);
                control = parent as EventControl;
            }

            if (control != null)
            {
                for (int i = 0; i < control.Items.Count; i++)
                {
                    var container = control.ItemContainerGenerator.ContainerFromIndex(i);
                    EventControlTrack track = container as EventControlTrack;
                    if (track == null)
                    {
                        continue;
                    }

                    for (int j = 0; j < track.Items.Count; j++)
                    {
                        container = track.ItemContainerGenerator.ContainerFromIndex(j);
                        EventTrackItem item = container as EventTrackItem;
                        if (item != null && item.IsSelected)
                        {
                            result.Add(item);
                        }
                    }
                }
            }

            return result;
        }
        #endregion
    }
}