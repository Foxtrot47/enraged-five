﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Media;

using System.Text;
using System.Collections.ObjectModel;

namespace RSG.Base.Windows.Controls
{
    /// <summary>
    /// A wrap panel that virualizes it items so that only the items that
    /// need rendering are created.
    /// </summary>
    public class VirtualizingWrapPanel : VirtualizingPanel, IScrollInfo
    {
        #region Members

        private UIElementCollection m_children;
        private ItemsControl m_itemsControl;
        private IItemContainerGenerator m_generator;
        private Dictionary<UIElement, Rect> m_calculatedChildAreas = new Dictionary<UIElement, Rect>();
        private WrapPanelManager m_panelManager;
        private int m_visibleSections;
        private Size m_previousAvailableSize;
        private Point m_scrollOffset = new Point(0, 0);
        private Size m_extentSize = new Size(0, 0);
        private Size m_contentViewport = new Size(0, 0);
        private int firstIndex = 0;
        private Size m_pixelMeasuredViewport = new Size(0, 0);
        private double m_previousItemSize = 0;

        #endregion // Members

        #region Properties

        #region Dependency Properties

        /// <summary>
        /// The size of each item (width if horizontial orientation
        /// and height if vertical orientation)
        /// </summary>
        [TypeConverter(typeof(LengthConverter))]
        public double ItemSize
        {
            get
            {
                return (double)base.GetValue(ItemSizeProperty);
            }
            set
            {
                base.SetValue(ItemSizeProperty, value);
            }
        }
        public static readonly DependencyProperty ItemSizeProperty = DependencyProperty.Register(
         "ItemSize",
         typeof(double),
         typeof(VirtualizingWrapPanel),
         new FrameworkPropertyMetadata(100.0, FrameworkPropertyMetadataOptions.AffectsArrange | FrameworkPropertyMetadataOptions.AffectsMeasure));

        /// <summary>
        /// Whether or not to wrap the the items length wise
        /// or depth
        /// </summary>
        public Orientation Orientation
        {
            get { return (Orientation)GetValue(OrientationProperty); }
            set { SetValue(OrientationProperty, value); }
        }
        public static readonly DependencyProperty OrientationProperty = StackPanel.OrientationProperty.AddOwner(
            typeof(VirtualizingWrapPanel),
            new FrameworkPropertyMetadata(Orientation.Horizontal));

        /// <summary>
        /// The amount of padding this stack panel has
        /// </summary>
        public Thickness Padding
        {
            get
            {
                return (Thickness)base.GetValue(PaddingProperty);
            }
            set
            {
                base.SetValue(PaddingProperty, value);
            }
        }
        public static readonly DependencyProperty PaddingProperty = DependencyProperty.Register(
         "Padding",
         typeof(Thickness),
         typeof(VirtualizingWrapPanel),
         new FrameworkPropertyMetadata(new Thickness(0.0), FrameworkPropertyMetadataOptions.AffectsArrange | FrameworkPropertyMetadataOptions.AffectsMeasure));

        #endregion // Dependency Properties

        /// <summary>
        /// The maximum size each item can take up. Depending on orientation one
        /// dimension will always be auto.
        /// </summary>
        private Size ChildSlotSize
        {
            get
            {
                if (Orientation == Orientation.Vertical)
                    return new Size(Double.PositiveInfinity, ItemSize);
                else
                    return new Size(ItemSize, Double.PositiveInfinity);
            }
        }

        /// <summary>
        /// The element child for this panel
        /// </summary>
        private UIElementCollection ElementChildren
        {
            get { return m_children; }
            set { m_children = value; }
        }

        /// <summary>
        /// The items control that this panel is host to
        /// </summary>
        private ItemsControl ItemsControl
        {
            get { return m_itemsControl; }
            set { m_itemsControl = value; }
        }

        /// <summary>
        /// The item container generator that is responsible
        /// for generating the ui elements for this panel
        /// </summary>
        private IItemContainerGenerator Generator
        {
            get { return m_generator; }
            set { m_generator = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        private Dictionary<UIElement, Rect> CalculatedChildAreas
        {
            get { return m_calculatedChildAreas; }
            set { m_calculatedChildAreas = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        private WrapPanelManager PanelManager
        {
            get { return m_panelManager; }
            set { m_panelManager = value; }
        }

        /// <summary>
        /// The number of sections that are currently visible
        /// to the user
        /// </summary>
        private int VisibleSections
        {
            get { return m_visibleSections; }
            set { m_visibleSections = value; }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Gets the index of the section that is is first
        /// to be visible on the screen
        /// </summary>
        public int GetFirstVisibleSection()
        {
            int section;
            var maxSection = 0;
            if (this.PanelManager != null)
            {
                maxSection = this.PanelManager.SectionCount - 1;
            }
            if (Orientation == Orientation.Horizontal)
            {
                section = (int)System.Math.Floor(m_scrollOffset.Y);
            }
            else
            {
                section = (int)System.Math.Floor(m_scrollOffset.X);
            }
            if (section > maxSection)
            {
                section = maxSection;
            }
            return section;
        }

        /// <summary>
        /// Gets the index for the item that has the minimum index and is
        /// visible on the screen
        /// </summary>
        public int GetFirstVisibleIndex()
        {
            int section = GetFirstVisibleSection();
            if (this.PanelManager != null)
            {
                return section * this.PanelManager.ItemsPerSection;
            }
            return 0;
        }

        /// <summary>
        /// Makes sure that the only children that are generated are the ones that we actually
        /// want. This is how the virtualising takes place. Previously created items that are no longer
        /// needed are removed and destroyed.
        /// </summary>
        public void CleanUpItems(int minDesiredGenerated, int maxDesiredGenerated)
        {
            for (int i = this.ElementChildren.Count - 1; i >= 0; i--)
            {
                GeneratorPosition childGeneratorPos = new GeneratorPosition(i, 0);
                int itemIndex = this.Generator.IndexFromGeneratorPosition(childGeneratorPos);
                if (itemIndex < minDesiredGenerated || itemIndex > maxDesiredGenerated)
                {
                    this.Generator.Remove(childGeneratorPos, 1);
                    RemoveInternalChildRange(i, 1);
                }
            }
        }

        /// <summary>
        /// Resets the scrolling information by making sure the offsets are 
        /// reset to zero
        /// </summary>
        private void ResetScrollInfo()
        {
            m_scrollOffset.X = 0;
            m_scrollOffset.Y = 0;
        }

        private void ComputeExtentAndViewport(Size viewportSize, int fullyVisibleSections, Size visibleExtent)
        {
            if (Orientation == Orientation.Horizontal)
            {
                if (this.VisibleSections == this.PanelManager.SectionCount)
                {
                    m_contentViewport.Height = viewportSize.Height;
                }
                else
                {
                    m_contentViewport.Height = fullyVisibleSections;
                }
                m_contentViewport.Width = viewportSize.Width;
            }
            else
            {
                m_contentViewport.Width = this.VisibleSections;
                m_contentViewport.Height = viewportSize.Height;
            }

            if (Orientation == Orientation.Horizontal)
            {
                if (this.VisibleSections == this.PanelManager.SectionCount)
                {
                    m_extentSize.Height = visibleExtent.Height;
                }
                else
                {
                    m_extentSize.Height = this.PanelManager.SectionCount;
                }
                m_extentSize.Width = visibleExtent.Width;
            }
            else
            {
                m_extentSize.Width = this.PanelManager.SectionCount + ViewportWidth - 1;
            }
            m_owner.InvalidateScrollInfo();
        }

        /// <summary>
        /// Gets the item index for the item that is directly below or right of the
        /// given item index
        /// </summary>
        private int GetNextSectionClosestIndex(int itemIndex)
        {
            var item = this.PanelManager[itemIndex];
            if (item.SectionIndex < this.PanelManager.SectionCount - 1)
            {
                ItemManager result = this.PanelManager.
                    Where(x => x.SectionIndex == item.SectionIndex + 1).
                    OrderBy(x => System.Math.Abs(x.IndexInSection - item.IndexInSection)).
                    First();

                return result.OverallIndex;
            }
            else
            {
                return itemIndex;
            }
        }

        /// <summary>
        /// Gets the item index for the item that is directly above or left of the
        /// given item index
        /// </summary>
        private int GetLastSectionClosestIndex(int itemIndex)
        {
            ItemManager item = this.PanelManager[itemIndex];
            if (item.SectionIndex > 0)
            {
                ItemManager result = this.PanelManager.
                    Where(x => x.SectionIndex == item.SectionIndex - 1).
                    OrderBy(x => System.Math.Abs(x.IndexInSection - item.IndexInSection)).
                    First();

                return result.OverallIndex;
            }
            else
            {
                return itemIndex;
            }
        }

        /// <summary>
        /// Gets called when the user pressed the down key and moves focus to the appropriate
        /// item container if it can
        /// </summary>
        private void NavigateDown()
        {
            ItemContainerGenerator generator = this.ItemContainerGenerator.GetItemContainerGeneratorForPanel(this);
            UIElement selectedItem = (UIElement)Keyboard.FocusedElement;
            int selectedItemIndex = generator.IndexFromContainer(selectedItem);

            int itemDepth = 0;
            while (selectedItemIndex == -1)
            {
                selectedItem = (UIElement)VisualTreeHelper.GetParent(selectedItem);
                selectedItemIndex = generator.IndexFromContainer(selectedItem);
                itemDepth++;
            }

            DependencyObject nextSelection = null;
            if (this.Orientation == Orientation.Vertical)
            {
            }
            else
            {
                if (this.PanelManager[selectedItemIndex].SectionIndex == this.PanelManager.SectionCount - 1)
                    return;

                int nextIndex = GetNextSectionClosestIndex(selectedItemIndex);
                nextSelection = generator.ContainerFromIndex(nextIndex);
                while (nextSelection == null)
                {
                    SetVerticalOffset(VerticalOffset + 1);
                    UpdateLayout();
                    nextSelection = generator.ContainerFromIndex(nextIndex);
                }
            }

            while (itemDepth != 0)
            {
                nextSelection = VisualTreeHelper.GetChild(nextSelection, 0);
                itemDepth--;
            }
            (nextSelection as UIElement).Focus();
        }

        /// <summary>
        /// Gets called when the user pressed the left key and moves focus to the appropriate
        /// item container if it can (this gets created if needed)
        /// </summary>
        private void NavigateLeft()
        {
            ItemContainerGenerator generator = this.ItemContainerGenerator.GetItemContainerGeneratorForPanel(this);
            UIElement selectedItem = (UIElement)Keyboard.FocusedElement;
            int selectedItemIndex = generator.IndexFromContainer(selectedItem);

            int itemDepth = 0;
            while (selectedItemIndex == -1)
            {
                selectedItem = (UIElement)VisualTreeHelper.GetParent(selectedItem);
                selectedItemIndex = generator.IndexFromContainer(selectedItem);
                itemDepth++;
            }

            DependencyObject nextSelection = null;
            if (this.Orientation == Orientation.Vertical)
            {
            }
            else
            {
                if (selectedItemIndex == 0)
                    return;

                nextSelection = generator.ContainerFromIndex(selectedItemIndex - 1);
                while (nextSelection == null)
                {
                    SetVerticalOffset(VerticalOffset - 1);
                    UpdateLayout();
                    nextSelection = generator.ContainerFromIndex(selectedItemIndex - 1);
                }
            }
            while (itemDepth != 0)
            {
                nextSelection = VisualTreeHelper.GetChild(nextSelection, 0);
                itemDepth--;
            }
            (nextSelection as UIElement).Focus();
        }

        /// <summary>
        /// Gets called when the user pressed the right key and moves focus to the appropriate
        /// item container if it can (this gets created if needed)
        /// </summary>
        private void NavigateRight()
        {
            ItemContainerGenerator generator = this.ItemContainerGenerator.GetItemContainerGeneratorForPanel(this);
            UIElement selectedItem = (UIElement)Keyboard.FocusedElement;
            int selectedItemIndex = generator.IndexFromContainer(selectedItem);

            int itemDepth = 0;
            while (selectedItemIndex == -1)
            {
                selectedItem = (UIElement)VisualTreeHelper.GetParent(selectedItem);
                selectedItemIndex = generator.IndexFromContainer(selectedItem);
                itemDepth++;
            }

            DependencyObject nextSelection = null;
            if (this.Orientation == Orientation.Vertical)
            {
            }
            else
            {
                if (selectedItemIndex == this.ItemsControl.Items.Count - 1)
                    return;

                nextSelection = generator.ContainerFromIndex(selectedItemIndex + 1);
                while (nextSelection == null)
                {
                    SetVerticalOffset(VerticalOffset + 1);
                    UpdateLayout();
                    nextSelection = generator.ContainerFromIndex(selectedItemIndex + 1);
                }
            }
            while (itemDepth != 0)
            {
                nextSelection = VisualTreeHelper.GetChild(nextSelection, 0);
                itemDepth--;
            }
            (nextSelection as UIElement).Focus();
        }

        /// <summary>
        /// Gets called when the user pressed the up key and moves focus to the appropriate
        /// item container if it can
        /// </summary>
        private void NavigateUp()
        {
            ItemContainerGenerator generator = this.ItemContainerGenerator.GetItemContainerGeneratorForPanel(this);
            UIElement selectedItem = (UIElement)Keyboard.FocusedElement;
            int selectedItemIndex = generator.IndexFromContainer(selectedItem);

            int itemDepth = 0;
            while (selectedItemIndex == -1)
            {
                selectedItem = (UIElement)VisualTreeHelper.GetParent(selectedItem);
                selectedItemIndex = generator.IndexFromContainer(selectedItem);
                itemDepth++;
            }

            DependencyObject nextSelection = null;
            if (this.Orientation == Orientation.Vertical)
            {
            }
            else
            {
                if (this.PanelManager[selectedItemIndex].SectionIndex == 0)
                    return;

                int nextIndex = GetLastSectionClosestIndex(selectedItemIndex);
                nextSelection = generator.ContainerFromIndex(nextIndex);
                while (nextSelection == null)
                {
                    SetVerticalOffset(VerticalOffset - 1);
                    UpdateLayout();
                    nextSelection = generator.ContainerFromIndex(nextIndex);
                }
            }

            while (itemDepth != 0)
            {
                nextSelection = VisualTreeHelper.GetChild(nextSelection, 0);
                itemDepth--;
            }
            (nextSelection as UIElement).Focus();
        }

        #endregion

        #region Override

        /// <summary>
        /// Gets called when the user presses any key will an item in the panel has focus
        /// This is used to control selected navigation in the panel
        /// </summary>
        protected override void OnKeyDown(KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Down:
                    NavigateDown();
                    e.Handled = true;
                    break;
                case Key.Left:
                    NavigateLeft();
                    e.Handled = true;
                    break;
                case Key.Right:
                    NavigateRight();
                    e.Handled = true;
                    break;
                case Key.Up:
                    NavigateUp();
                    e.Handled = true;
                    break;
                default:
                    base.OnKeyDown(e);
                    break;
            }
        }

        /// <summary>
        /// Make sure that when the items change the panel manager and scroll info
        /// are reset.
        /// </summary>
        protected override void OnItemsChanged(Object sender, ItemsChangedEventArgs args)
        {
            base.OnItemsChanged(sender, args);
            this.PanelManager = null;
            ResetScrollInfo();
        }

        /// <summary>
        /// Gets called once when the panel is first initialised
        /// </summary>
        protected override void OnInitialized(EventArgs e)
        {
            base.OnInitialized(e);
            this.ItemsControl = ItemsControl.GetItemsOwner(this);
            this.ElementChildren = InternalChildren;
            this.Generator = ItemContainerGenerator;
        }

        /// <summary>
        /// Gets called when any child data items need to get measured so that the
        /// panel can arrange them and render them
        /// </summary>
        protected override Size MeasureOverride(Size availableSize)
        {
            if (this.ItemsControl == null || this.ItemsControl.Items.Count == 0)
                return availableSize;

            Boolean createScrollInfo = true;
            if (this.PanelManager == null || m_previousAvailableSize != availableSize)
            {
                m_previousAvailableSize = availableSize;
                this.PanelManager = new WrapPanelManager(this, availableSize, this.ItemsControl.Items.Count);
            }
            if (this.m_previousItemSize != this.ItemSize)
            {
                this.m_previousItemSize = this.ItemSize;
                this.CleanUpItems(-1, -1);
                this.PanelManager = new WrapPanelManager(this, availableSize, this.ItemsControl.Items.Count);
            }

            this.CalculatedChildAreas.Clear();
            int firstVisibleSection = this.GetFirstVisibleSection();
            int firstVisibleIndex = this.GetFirstVisibleIndex();
            this.VisibleSections = 0;
            Size visibleExtent = new Size(0.0, 0.0);
            if (this.Orientation == Orientation.Horizontal)
                visibleExtent.Width = ((this.ItemSize + this.PanelManager.ItemMargin) * this.PanelManager.ItemsPerSection) + this.Padding.Left;
            else
                visibleExtent.Height = (this.ItemSize + this.PanelManager.ItemMargin) * this.PanelManager.ItemsPerSection;

            m_pixelMeasuredViewport = availableSize;

            GeneratorPosition startingContainer = this.Generator.GeneratorPositionFromIndex(firstVisibleIndex);
            using (this.Generator.StartAt(startingContainer, GeneratorDirection.Forward, true))
            {
                Boolean stop = false;
                Boolean isHorizontal = Orientation == Orientation.Horizontal;
                int currentIndex = firstVisibleSection * this.PanelManager.ItemsPerSection;
                int childContainerIndex = (startingContainer.Offset == 0) ? startingContainer.Index : startingContainer.Index + 1;
                int fullyVisibleSections = 0;
                double startingX = this.Padding.Left - this.HorizontalOffset;
                double startingY = 0;
                if (firstVisibleIndex == 0)
                {
                    if (this.VerticalOffset == 0)
                    {
                        startingY = this.Padding.Top;
                    }
                }

                double currentX = startingX;
                double currentY = startingY;

                for (int i = 0; i < this.PanelManager.SectionCount; i++)
                {
                    double sectionSize = 0;
                    if (isHorizontal)
                        currentX = startingX;
                    else
                        currentY = startingY;
                    if (currentIndex >= this.ItemsControl.Items.Count)
                        break;

                    for (int j = 0; j < this.PanelManager.ItemsPerSection; j++)
                    {
                        if (currentIndex >= this.ItemsControl.Items.Count)
                            break;

                        Boolean newlyRealized;
                        UIElement child = this.Generator.GenerateNext(out newlyRealized) as UIElement;
                        if (newlyRealized)
                        {
                            // Figure out if we need to insert the child at the end or somewhere in the middle
                            if (childContainerIndex >= this.ElementChildren.Count)
                            {
                                base.AddInternalChild(child);
                            }
                            else
                            {
                                base.InsertInternalChild(childContainerIndex, child);
                            }
                            this.Generator.PrepareItemContainer(child);
                            child.Measure(ChildSlotSize);
                        }
                        else
                        {
                            // The child has already been created, let's be sure it's in the right spot
                            Debug.Assert(child == this.ElementChildren[childContainerIndex], "Wrong child was generated");

                        }
                        Size childContainerSize = child.DesiredSize;
                        Rect childContainerRect = new Rect(new Point(currentX, currentY), childContainerSize);
                        if (isHorizontal)
                        {
                            sectionSize = System.Math.Max(sectionSize, childContainerSize.Height);
                            childContainerRect.X = currentX;
                            childContainerRect.Y = currentY;

                            currentX += this.ItemSize + this.PanelManager.ItemMargin;
                        }
                        else
                        {
                            sectionSize = System.Math.Max(sectionSize, childContainerSize.Width);
                            childContainerRect.X = currentX;
                            childContainerRect.Y = currentY;

                            currentY += this.ItemSize + this.PanelManager.ItemMargin;
                        }

                        this.CalculatedChildAreas.Add(child, childContainerRect);

                        currentIndex++;
                        childContainerIndex++;
                    }

                    this.VisibleSections++;
                    if (isHorizontal)
                    {
                        currentY += sectionSize + 1;
                        if (currentY >= availableSize.Height)
                            stop = true;
                        else
                            fullyVisibleSections++;

                        visibleExtent.Height = currentY;
                    }
                    else
                    {
                        currentX += sectionSize + 1;
                        if (currentX >= availableSize.Width)
                            stop = true;
                        else
                            fullyVisibleSections++;


                        visibleExtent.Width = currentX;
                    }

                    if (stop)
                        break;
                }

                this.CleanUpItems(firstVisibleIndex, currentIndex - 1);
                if (createScrollInfo == true)
                    this.ComputeExtentAndViewport(availableSize, fullyVisibleSections, visibleExtent);
            }
            return availableSize;
        }

        /// <summary>
        /// Gets called when the child ui elements need to get set in place on
        /// the panel
        /// </summary>
        protected override Size ArrangeOverride(Size finalSize)
        {
            if (this.ElementChildren != null)
            {
                foreach (UIElement child in this.ElementChildren)
                {
                    if (this.CalculatedChildAreas.ContainsKey(child))
                    {
                        var layoutInfo = this.CalculatedChildAreas[child];
                        child.Arrange(layoutInfo);
                    }
                }
            }
            return finalSize;
        }

        #endregion

        #region IScrollInfo Members

        private bool m_canHScroll = false;
        public bool CanHorizontallyScroll
        {
            get { return m_canHScroll; }
            set { m_canHScroll = value; }
        }

        private bool m_canVScroll = false;
        public bool CanVerticallyScroll
        {
            get { return m_canVScroll; }
            set { m_canVScroll = value; }
        }

        public double ExtentHeight
        {
            get { return m_extentSize.Height; }
        }

        public double ExtentWidth
        {
            get { return m_extentSize.Width; }
        }

        public double HorizontalOffset
        {
            get { return m_scrollOffset.X; }
        }

        private ScrollViewer m_owner;
        public ScrollViewer ScrollOwner
        {
            get { return m_owner; }
            set { m_owner = value; }
        }

        public double VerticalOffset
        {
            get { return m_scrollOffset.Y; }
        }

        public double ViewportHeight
        {
            get { return m_contentViewport.Height; }
        }

        public double ViewportWidth
        {
            get { return m_contentViewport.Width; }
        }

        public void LineDown()
        {
            if (Orientation == Orientation.Horizontal)
            {
                SetVerticalOffset(VerticalOffset + 1);
            }
        }

        public void LineLeft()
        {
            if (Orientation == Orientation.Horizontal)
                SetHorizontalOffset(HorizontalOffset - 20);
            else
                SetHorizontalOffset(HorizontalOffset - 1);
        }

        public void LineRight()
        {
            if (Orientation == Orientation.Horizontal)
                SetHorizontalOffset(HorizontalOffset + 20);
            else
                SetHorizontalOffset(HorizontalOffset + 1);
        }

        public void LineUp()
        {
            if (Orientation == Orientation.Vertical)
                SetVerticalOffset(VerticalOffset - 20);
            else
                SetVerticalOffset(VerticalOffset - 1);
        }

        /// <summary>
        /// Makes sure that the given visual is completely visible
        /// basically scroll it into view.
        /// </summary>
        public Rect MakeVisible(Visual visual, Rect rectangle)
        {
            ItemContainerGenerator generator = this.Generator.GetItemContainerGeneratorForPanel(this);
            UIElement element = visual as UIElement;
            if (element != null)
            {
                int itemIndex = generator.IndexFromContainer(element);
                while (itemIndex == -1)
                {
                    element = (UIElement)VisualTreeHelper.GetParent(element);
                    itemIndex = generator.IndexFromContainer(element);
                }
                int section = this.PanelManager[itemIndex].SectionIndex;
                Rect elementRect = this.CalculatedChildAreas[element];
                if (Orientation == Orientation.Horizontal)
                {
                    double viewportHeight = m_pixelMeasuredViewport.Height;
                    if (elementRect.Bottom > viewportHeight)
                    {
                        m_scrollOffset.Y += 1;
                        InvalidateMeasure();
                    }
                    else if (elementRect.Top < 0)
                    {
                        m_scrollOffset.Y -= 1;
                        InvalidateMeasure();
                    }
                }
                else
                {
                    double viewportWidth = m_pixelMeasuredViewport.Width;
                    if (elementRect.Right > viewportWidth)
                    {
                        m_scrollOffset.X += 1;
                        InvalidateMeasure();
                    }
                    else if (elementRect.Left < 0)
                    {
                        m_scrollOffset.X -= 1;
                        InvalidateMeasure();
                    }
                }
                return elementRect;
            }
            else
            {
                return rectangle;
            }
        }

        public void MouseWheelDown()
        {
            PageDown();
        }

        public void MouseWheelLeft()
        {
            PageLeft();
        }

        public void MouseWheelRight()
        {
            PageRight();
        }

        public void MouseWheelUp()
        {
            PageUp();
        }

        public void PageDown()
        {
            SetVerticalOffset(VerticalOffset + m_contentViewport.Height * 0.8);
        }

        public void PageLeft()
        {
            SetHorizontalOffset(HorizontalOffset - m_contentViewport.Width * 0.8);
        }

        public void PageRight()
        {
            SetHorizontalOffset(HorizontalOffset + m_contentViewport.Width * 0.8);
        }

        public void PageUp()
        {
            SetVerticalOffset(VerticalOffset - m_contentViewport.Height * 0.8);
        }

        public void SetHorizontalOffset(double offset)
        {
            if (offset < 0 || m_contentViewport.Width >= m_extentSize.Width)
            {
                offset = 0;
            }
            else
            {
                if (offset + m_contentViewport.Width >= m_extentSize.Width)
                {
                    offset = m_extentSize.Width - m_contentViewport.Width;
                }
            }

            m_scrollOffset.X = offset;

            if (this.ScrollOwner != null)
                this.ScrollOwner.InvalidateScrollInfo();

            InvalidateMeasure();
            firstIndex = GetFirstVisibleIndex();
        }

        public void SetVerticalOffset(double offset)
        {
            if (offset < 0 || m_contentViewport.Height >= m_extentSize.Height)
            {
                offset = 0;
            }
            else
            {
                if (offset + m_contentViewport.Height >= m_extentSize.Height)
                {
                    offset = m_extentSize.Height - m_contentViewport.Height;
                }
            }

            m_scrollOffset.Y = offset;

            if (this.ScrollOwner != null)
                this.ScrollOwner.InvalidateScrollInfo();

            InvalidateMeasure();
            firstIndex = GetFirstVisibleIndex();
        }

        #endregion // IScrollInfo Members

        #region Helper Classes

        /// <summary>
        /// Looks after a single item in the wrap panel
        /// </summary>
        class ItemManager
        {
            #region Member

            private int m_sectionIndex;
            private int m_indexInSection;
            private int m_overallIndex;

            #endregion // Member

            #region Properties

            /// <summary>
            /// The section index this item belongs to
            /// </summary>
            public int SectionIndex
            {
                get { return m_sectionIndex; }
                set { m_sectionIndex = value; }
            }

            /// <summary>
            /// The index of this item in the owning section
            /// </summary>
            public int IndexInSection
            {
                get { return m_indexInSection; }
                set { m_indexInSection = value; }
            }

            /// <summary>
            /// The index of the item in the overall collection
            /// </summary>
            public int OverallIndex
            {
                get { return m_overallIndex; }
                set { m_overallIndex = value; }
            }

            #endregion // Properties

            #region Constructor

            /// <summary>
            /// Default Constructor
            /// </summary>
            public ItemManager(WrapPanelManager panel, int overallIndex)
            {
                this.OverallIndex = overallIndex;
                this.SectionIndex = (int)System.Math.Floor(overallIndex / (double)panel.ItemsPerSection);
                this.IndexInSection = overallIndex - (this.SectionIndex * panel.ItemsPerSection);
            }

            #endregion // Constructor
        } // ItemManager

        /// <summary>
        /// Looks after the wrap panel and all it's sections
        /// </summary>
        class WrapPanelManager : IEnumerable<ItemManager>
        {
            #region Members

            private int m_itemsPerSection;
            private int m_sectionCount;
            private int m_itemMargin;
            private List<ItemManager> m_newItems;

            #endregion // Members

            #region Properties

            /// <summary>
            /// The number of items that can fit into a single
            /// section
            /// </summary>
            public int ItemsPerSection
            {
                get { return m_itemsPerSection; }
                set { m_itemsPerSection = value; }
            }

            /// <summary>
            /// The number of sections that current exist in this
            /// panel.
            /// </summary>
            public int SectionCount
            {
                get { return m_sectionCount; }
                set { m_sectionCount = value; }
            }

            /// <summary>
            /// The size of the margin that appears between two items
            /// in a single section
            /// </summary>
            public int ItemMargin
            {
                get { return m_itemMargin; }
                set { m_itemMargin = value; }
            }

            /// <summary>
            /// The collection of managed item abstractions
            /// </summary>
            private List<ItemManager> Items
            {
                get { return m_newItems; }
                set { m_newItems = value; }
            }

            /// <summary>
            /// Array indexer into the item abstractions
            /// </summary>
            public ItemManager this[int index]
            {
                get
                {
                    return Items[index];
                }
            }

            #endregion // Properties

            #region Constructor

            /// <summary>
            /// Default constructor, creates the manager using the
            /// items count
            /// </summary>
            public WrapPanelManager(VirtualizingWrapPanel panel, Size availableSize, int itemCount)
            {
                double size = 0;
                if (panel.Orientation == Orientation.Vertical)
                {
                    size = System.Math.Floor(availableSize.Height);
                }
                else // Orientation.Horizontal
                {
                    size = System.Math.Floor(availableSize.Width) - panel.Padding.Left - panel.Padding.Right;
                }
                int maxItemsPerSection = System.Math.Max((int)System.Math.Floor(size / panel.ItemSize), 1);
                int leftSpace = System.Math.Max((int)System.Math.Floor(size - (maxItemsPerSection * panel.ItemSize)), 0);
                if (leftSpace < maxItemsPerSection && maxItemsPerSection > 1)
                {
                    maxItemsPerSection--;
                    leftSpace = System.Math.Max((int)System.Math.Floor(size - (maxItemsPerSection * panel.ItemSize)), 0);
                }
                if (itemCount <= maxItemsPerSection)
                {
                    this.ItemsPerSection = itemCount;
                    this.SectionCount = 1;
                    this.ItemMargin = 1;
                }
                else
                {
                    this.ItemsPerSection = maxItemsPerSection;
                    this.SectionCount = (int)System.Math.Ceiling((double)itemCount / maxItemsPerSection);
                    this.ItemMargin = (int)System.Math.Floor(leftSpace / (double)maxItemsPerSection);
                }

                this.Items = new List<ItemManager>(itemCount);
                for (int i = 0; i < itemCount; i++)
                {
                    this.Items.Add(new ItemManager(this, i));
                }
            }

            #endregion // Constructor

            #region IEnumerable<ItemManager> Members

            public IEnumerator<ItemManager> GetEnumerator()
            {
                return Items.GetEnumerator();
            }

            IEnumerator IEnumerable.GetEnumerator()
            {
                return GetEnumerator();
            }

            #endregion
        } // WrapPanelManager

        #endregion // Helper Classes
    } // VirtualizingWrapPanel
} // RSG.Base.Windows.Controls
