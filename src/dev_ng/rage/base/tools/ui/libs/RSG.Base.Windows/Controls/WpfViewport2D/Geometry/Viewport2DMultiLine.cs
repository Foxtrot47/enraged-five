﻿using System;
using System.ComponentModel;
using RSG.Base.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using RSG.Base.Windows.Controls.WpfViewport2D.Geometry;
using RSG.Base.Windows.Controls.WpfViewport2D.Actions;
using RSG.Base.Math;

namespace RSG.Base.Windows.Controls.WpfViewport2D.Geometry
{
    /// <summary>
    /// Represents a set of lines
    /// </summary>
    public class Viewport2DMultiLine : Viewport2DGeometry
    {
        #region Properties
        /// <summary>
        /// Line points (world-space)
        /// </summary>
        public Vector2f[] Points
        {
            get { return m_points; }
            set { m_points = value; }
        }
        private Vector2f[] m_points;

        /// <summary>
        /// Center of the points
        /// </summary>
        public Vector2f Center
        {
            get { return m_center; }
            set { m_center = value; }
        }
        private Vector2f m_center;

        /// <summary>
        /// Closed flag, automatically close the multiline.
        /// </summary>
        public Boolean Closed
        {
            get { return m_closed; }
            set { m_closed = value; }
        }
        private Boolean m_closed;

        /// <summary>
        /// Line width
        /// </summary>
        public int LineWidth
        {
            get { return m_lineWidth; }
            set { m_lineWidth = value; }
        }
        private int m_lineWidth;

        /// <summary>
        /// Line colour
        /// </summary>
        public Color LineColour
        {
            get { return m_lineColour; }
            set { m_lineColour = value; }
        }
        private Color m_lineColour;
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="space"></param>
        /// <param name="name"></param>
        /// <param name="points"></param>
        public Viewport2DMultiLine(etCoordSpace space, String name, Vector2f[] points)
            : this(space, name, points, false, Colors.Black, 1)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="space"></param>
        /// <param name="name"></param>
        /// <param name="points"></param>
        /// <param name="closed"></param>
        /// <param name="lineWidth"></param>
        /// <param name="lineColour"></param>
        public Viewport2DMultiLine(etCoordSpace space, String name, Vector2f[] points, bool closed, Color lineColour, int lineWidth)
            : base(space, name)
        {
            // Set up the points/center
            m_points = points;
            m_center = new Vector2f(0.0f, 0.0f);
            if (m_points.Length > 0)
            {
                foreach (Vector2f point in m_points)
                {
                    m_center += point;
                }
                m_center /= m_points.Length;
            }

            m_closed = closed;
            m_lineWidth = lineWidth;
            m_lineColour = lineColour;
        }
        #endregion // Constructor(s)

        /// <summary>
        /// Return bounding box for 2D geometry in world-space.
        /// </summary>
        public override BoundingBox2f GetWorldBoundingBox(Viewport2D viewport)
        {
            BoundingBox2f bbox = new BoundingBox2f();
            if (etCoordSpace.World == this.CoordinateSpace)
            {
                foreach (Vector2f point in Points)
                {
                    bbox.Expand(point);
                }
            }
            else
            {
                throw new NotImplementedException("Invalid coordinate space.");
            }
            return bbox;
        }

        /// <summary>
        /// Determines if the geometry has been picked given the screen
        /// position of the pick
        /// </summary>
        /// <param name="screenPosition"></param>
        /// <returns></returns>
        public override Boolean HasBeenPicked(Point screenPosition, Viewport2D viewport)
        {
            // Line don't currently support picking
            return false;
        }

    }
}
