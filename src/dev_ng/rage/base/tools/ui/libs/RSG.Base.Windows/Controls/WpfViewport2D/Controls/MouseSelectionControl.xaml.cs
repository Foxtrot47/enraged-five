﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using RSG.Base.Collections;
using RSG.Base.Windows.Controls.WpfViewport2D.Geometry;

namespace RSG.Base.Windows.Controls.WpfViewport2D.Controls
{
    /// <summary>
    /// Custom event args for viewport click events
    /// </summary>
    public class ViewportClickEventArgs : EventArgs
    {
        public Point ViewPosition
        {
            get;
            protected set;
        }

        public Point WorldPosition
        {
            get;
            protected set;
        }

        public ViewportClickEventArgs(Point viewPosition, Point worldPosition)
            : base()
        {
            ViewPosition = viewPosition;
            WorldPosition = worldPosition;
        }
    }

    /// <summary>
    /// Event handler for handling clicks on the viewport
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public delegate void ViewportClickEventHandler(object sender, ViewportClickEventArgs e);

    /// <summary>
    /// Interaction logic for MouseSelectionControl.xaml
    /// </summary>
    public partial class MouseSelectionControl :
        ViewportUserControl
    {
        #region Public Events

        public event NotifyCollectionChangedEventHandler SelectionChanged;

        private void OnSelectionChanged(NotifyCollectionChangedAction action, List<Object> oldItems, List<Object> newItems)
        {
            if (oldItems == null)
            {
                oldItems = new List<Object>();
            }
            if (newItems == null)
            {
                newItems = new List<Object>();
            }
            NotifyCollectionChangedEventArgs newArgs = new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Replace, oldItems, newItems);

            if (this.SelectionChanged != null)
                this.SelectionChanged(this, newArgs);
        }

        /// <summary>
        /// Event fired when the viewport is clicked
        /// </summary>
        public event ViewportClickEventHandler ViewportClicked;

        private void OnViewportClicked(Point viewPosition, Point worldPosition)
        {
            if (ViewportClicked != null)
            {
                ViewportClicked(this, new ViewportClickEventArgs(viewPosition, worldPosition));
            }
        }
        #endregion // Public Events

        #region Properties

        /// <summary>
        /// The collection of geometry that is currently selected
        /// </summary>
        public ObservableCollection<Viewport2DGeometry> SelectedGeometry
        {
            get { return m_selectedGeometry; }
            set { m_selectedGeometry = value; }
        }
        private ObservableCollection<Viewport2DGeometry> m_selectedGeometry;

        /// <summary>
        /// The collection of pick data objects that are currently selected
        /// </summary>
        public ObservableCollection<Object> SelectedGeometryItems
        {
            get { return (ObservableCollection<Object>)GetValue(SelectedGeometryItemsProperty); }
            set 
            { 
                Object oldValue = (ObservableCollection<Object>)GetValue(SelectedGeometryItemsProperty);
                SetValue(SelectedGeometryItemsProperty, value); 
                
                OnPropertyChanged(new DependencyPropertyChangedEventArgs(SelectedGeometryItemsProperty, oldValue, value));
            }
        }
        public static DependencyProperty SelectedGeometryItemsProperty = DependencyProperty.Register("SelectedGeometryItems", typeof(ObservableCollection<Object>),
                                                                                            typeof(MouseSelectionControl));

        private Viewport2DGeometry LastPicked
        {
            get { return m_lastPicked; }
            set { m_lastPicked = value; }
        }
        private Viewport2DGeometry m_lastPicked;

        #endregion // Properties

        #region Constructor

        /// <summary>
        /// Default constructor
        /// </summary>
        public MouseSelectionControl()
        {
            InitializeComponent();
            this.SelectedGeometry = new ObservableCollection<Viewport2DGeometry>();
            this.SelectedGeometryItems = new ObservableCollection<Object>();

            this.SelectedGeometry.CollectionChanged += new System.Collections.Specialized.NotifyCollectionChangedEventHandler(SelectedGeometry_CollectionChanged);
        }

        void SelectedGeometry_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            //throw new NotImplementedException();
        }

        #endregion // Constructor

        #region Event Helpers

        /// <summary>
        /// After clicking the left mouse button we go through all of the geometry in the viewport
        /// in reverse and determine what if anything was hit first
        /// </summary>
        public override void OnManagerMouseDown(Object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
            {
                if (!System.Windows.Input.Keyboard.IsKeyDown(Key.LeftCtrl))
                {
                    //Get the view position of the mouse click
                    Point viewPosition = e.GetPosition(sender as Viewport2D);

                    // See if we have picked anything in the viewport
                    Boolean picked = false;
                    foreach (Viewport2DGeometry geometry in (sender as Viewport2D).GeometrySource.Reverse())
                    {
                        if (!(geometry is Viewport2DImage))
                        {
                            if (geometry.HasBeenPicked(viewPosition, (sender as Viewport2D)))
                            {
                                this.SelectedGeometry.BeginUpdate();
                                this.SelectedGeometryItems.BeginUpdate();

                                List<Object> oldItems = new List<Object>();
                                foreach (Viewport2DGeometry currentGeometry in this.SelectedGeometry)
                                {
                                    currentGeometry.IsSelected = false;
                                    oldItems.Add(currentGeometry);
                                }
                                this.SelectedGeometry.Clear();
                                this.SelectedGeometryItems.Clear();
                                geometry.IsSelected = true;
                                this.SelectedGeometry.Add(geometry);
                                if (geometry.PickData != null)
                                    this.SelectedGeometryItems.Add(geometry.PickData);

                                this.SelectedGeometry.EndUpdate();
                                this.SelectedGeometryItems.EndUpdate();
                                List<Object> newItems = new List<Object>() { geometry };

                                if (oldItems.Count > 0)
                                {
                                    if (oldItems.Count == 1)
                                    {
                                        this.OnSelectionChanged(NotifyCollectionChangedAction.Add, oldItems, newItems);
                                    }
                                    else
                                    {
                                        this.OnSelectionChanged(NotifyCollectionChangedAction.Reset, oldItems, newItems);
                                    }
                                }
                                else
                                {
                                    this.OnSelectionChanged(NotifyCollectionChangedAction.Add, oldItems, newItems);
                                }

                                System.Diagnostics.Debug.Print("Geometry has been picked - \"{0}\" - {1} items selected", geometry.Name, this.SelectedGeometry.Count);
                                picked = true;
                                break;
                            }
                        }
                    }
                    if (picked == false)
                    {
                        List<Object> oldItems = new List<Object>();
                        foreach (Viewport2DGeometry currentGeometry in this.SelectedGeometry)
                        {
                            currentGeometry.IsSelected = false;
                            oldItems.Add(currentGeometry);
                        }
                        this.SelectedGeometry.Clear();
                        this.SelectedGeometryItems.Clear();

                        if (oldItems.Count > 0)
                        {
                            this.OnSelectionChanged(NotifyCollectionChangedAction.Remove, oldItems, null);
                        }
                        System.Diagnostics.Debug.Print("No geometry has been picked - {0} items selected", this.SelectedGeometry.Count);

                        Point worldPosition = (sender as Viewport2D).ViewToWorld(viewPosition);
                        OnViewportClicked(viewPosition, worldPosition);
                    }
                }
                else
                {
                    //Get the view position of the mouse click
                    Point viewPosition = e.GetPosition(sender as Viewport2D);

                    foreach (Viewport2DGeometry geometry in (sender as Viewport2D).GeometrySource.Reverse())
                    {
                        if (!(geometry is Viewport2DImage))
                        {
                            if (geometry.HasBeenPicked(viewPosition, (sender as Viewport2D)))
                            {
                                this.LastPicked = geometry;
                                break;
                            }
                        }
                    }
                }
            }
            else if (e.ChangedButton == MouseButton.Right)
            {
                List<Object> oldItems = new List<Object>();
                foreach (Viewport2DGeometry currentGeometry in this.SelectedGeometry)
                {
                    currentGeometry.IsSelected = false;
                    oldItems.Add(currentGeometry);
                }
                this.SelectedGeometry.Clear();
                this.SelectedGeometryItems.Clear();

                if (oldItems.Count > 0)
                {
                    this.OnSelectionChanged(NotifyCollectionChangedAction.Remove, oldItems, null);
                }
                System.Diagnostics.Debug.Print("Selection has been cleared - {0} items selected", this.SelectedGeometry.Count);
            }
        }

        /// <summary>
        /// If we are using multi selection (left ctrl down) we need to look for the mouse up
        /// event to select containers
        /// </summary>
        public override void OnManagerMouseUp(Object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
            {
                if (System.Windows.Input.Keyboard.IsKeyDown(Key.LeftCtrl) || this.SelectedGeometry.Count > 1)
                {
                    //Get the view position of the mouse click
                    Point viewPosition = e.GetPosition(sender as Viewport2D);

                    // See if we have picked anything in the viewport
                    foreach (Viewport2DGeometry geometry in (sender as Viewport2D).GeometrySource.Reverse())
                    {
                        if (!(geometry is Viewport2DImage) && geometry == this.LastPicked)
                        {
                            if (geometry.HasBeenPicked(viewPosition, (sender as Viewport2D)))
                            {
                                if (this.SelectedGeometry.Contains(geometry))
                                {
                                    geometry.IsSelected = false;
                                    this.SelectedGeometry.Remove(geometry);
                                    if (geometry.PickData != null)
                                        this.SelectedGeometryItems.Remove(geometry.PickData);

                                    List<Object> oldItems = new List<Object>() { geometry };
                                    this.OnSelectionChanged(NotifyCollectionChangedAction.Remove, oldItems, null);
                                }
                                else
                                {
                                    geometry.IsSelected = true;
                                    this.SelectedGeometry.Add(geometry);
                                    if (geometry.PickData != null)
                                        this.SelectedGeometryItems.Add(geometry.PickData);

                                    List<Object> newItems = new List<Object>() { geometry };
                                    this.OnSelectionChanged(NotifyCollectionChangedAction.Add, null, newItems);
                                }

                                System.Diagnostics.Debug.Print("Geometry has been picked with multiselection - \"{0}\" - {1} items selected", geometry.Name, this.SelectedGeometry.Count);
                                break;
                            }
                        }
                    }
                }
            }
        }

        #endregion Event Helpers

        #region Registration

        public override void RegisterActionsWithManager(Actions.ActionManager manager)
        {
            manager.RegisterMouseCommand(this, Actions.MouseEvents.Mouse_Down | Actions.MouseEvents.Mouse_Up);
        }

        #endregion // Registration

    }
}
