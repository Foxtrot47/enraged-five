﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Reflection;
using System.Collections;
using System.Windows.Controls.Primitives;

namespace RSG.Base.Windows.DragDrop.Utilities
{
    /// <summary>
    /// 
    /// </summary>
    public static class ItemsControlExtensions
    {
        /// <summary>
        /// Returns the item container for a items control
        /// </summary>
        public static UIElement GetItemContainer(this ItemsControl itemsControl, UIElement child)
        {
            Type itemType = GetItemContainerType(itemsControl);

            if (itemType != null)
            {
                return (UIElement)child.GetVisualAncestor(itemType);
            }

            return null;
        }

        /// <summary>
        /// Not really fully safe as we have to walk the visual tree and find the items
        /// presenter that should have the children in it.
        /// </summary>
        public static Type GetItemContainerType(this ItemsControl itemsControl)
        {
            if (itemsControl.Items.Count > 0)
            {
                IEnumerable<ItemsPresenter> itemsPresenters = itemsControl.GetVisualDescendents<ItemsPresenter>();

                foreach (ItemsPresenter itemsPresenter in itemsPresenters)
                {
                    if (VisualTreeHelper.GetChildrenCount(itemsPresenter) != 0)
                    {
                        DependencyObject panel = VisualTreeHelper.GetChild(itemsPresenter, 0);
                        DependencyObject itemContainer = VisualTreeHelper.GetChild(panel, 0);

                        // Ensure that this actually *is* an item container by checking it with
                        // ItemContainerGenerator.
                        if (itemContainer != null && itemsControl.ItemContainerGenerator.IndexFromContainer(itemContainer) != -1)
                        {
                            return itemContainer.GetType();
                        }
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Gets the selected items for a specific items control
        /// </summary>
        public static IEnumerable GetSelectedItems(this ItemsControl itemsControl)
        {
            if (itemsControl is MultiSelector)
            {
                return ((MultiSelector)itemsControl).SelectedItems;
            }
            else if (itemsControl is ListBox)
            {
                ListBox listBox = (ListBox)itemsControl;

                if (listBox.SelectionMode == SelectionMode.Single)
                {
                    return Enumerable.Repeat(listBox.SelectedItem, 1);
                }
                else
                {
                    return listBox.SelectedItems;
                }
            }
            else if (itemsControl is RSG.Base.Windows.Controls.MultiSelect.TreeView)
            {
                return (itemsControl as RSG.Base.Windows.Controls.MultiSelect.TreeView).SelectedItems;
            }
            else if (itemsControl is TreeView)
            {
                return Enumerable.Repeat(((TreeView)itemsControl).SelectedItem, 1);
            }
            else if (itemsControl is Selector)
            {
                return Enumerable.Repeat(((Selector)itemsControl).SelectedItem, 1);
            }
            else
            {
                return Enumerable.Empty<object>();
            }
        }

        /// <summary>
        /// Determines whether a items control supports single selection or multi
        /// selection
        /// </summary>
        public static bool CanSelectMultipleItems(this ItemsControl itemsControl)
        {
            if (itemsControl is MultiSelector)
            {
                // The CanSelectMultipleItems property is protected. Use reflection to
                // get it's value anyway.
                return (bool)itemsControl.GetType()
                    .GetProperty("CanSelectMultipleItems", BindingFlags.Instance | BindingFlags.NonPublic)
                    .GetValue(itemsControl, null);
            }
            else if (itemsControl is ListBox)
            {
                return ((ListBox)itemsControl).SelectionMode != SelectionMode.Single;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public static void ClearSelection(this ItemsControl itemsControl)
        {
            if (itemsControl is MultiSelector)
            {
                (itemsControl as MultiSelector).UnselectAll();
            }
            else if (itemsControl is ListBox)
            {
                (itemsControl as ListBox).UnselectAll();
            }
            else if (itemsControl is Selector)
            {
                (itemsControl as Selector).SelectedItem = null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public static void SelectItem(this ItemsControl itemsControl, DependencyObject selection)
        {
            if (itemsControl is MultiSelector)
            {
                (itemsControl as MultiSelector).SelectedItem = selection;
            }
            else if (itemsControl is ListBox)
            {
                (itemsControl as ListBox).SelectedItem = selection;
            }
            else if (itemsControl is Selector)
            {
                (itemsControl as Selector).SelectedItem = null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public static UIElement GetItemContainerAt(this ItemsControl itemsControl, Point position)
        {
            IInputElement inputElement = itemsControl.InputHitTest(position);
            UIElement uiElement = inputElement as UIElement;

            if (uiElement != null)
            {
                return GetItemContainer(itemsControl, uiElement);
            }

            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        public static Orientation GetItemsPanelOrientation(this ItemsControl itemsControl)
        {
            ItemsPresenter itemsPresenter = itemsControl.GetVisualDescendent<ItemsPresenter>();
            if (VisualTreeHelper.GetChildrenCount(itemsPresenter) != 0)
            {
                DependencyObject itemsPanel = VisualTreeHelper.GetChild(itemsPresenter, 0);
                PropertyInfo orientationProperty = itemsPanel.GetType().GetProperty("Orientation", typeof(Orientation));
                if (orientationProperty != null)
                {
                    return (Orientation)orientationProperty.GetValue(itemsPanel, null);
                }
                else
                {
                    if (itemsPanel is WrapPanel || itemsPanel is RSG.Base.Windows.Controls.VirtualizingWrapPanel)
                    {
                        return Orientation.Horizontal;
                    }
                }
            }

            return Orientation.Vertical;
        }
    } // ItemsControlExtensions
} // RSG.Base.Windows.DragDrop.Utilities
