﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using RSG.Base.Editor;
using RSG.Base.Editor.Command;

namespace RSG.Base.Windows.Dialogs
{

    /// <summary>
    /// 
    /// </summary>
    internal sealed class CustomMessageBoxViewModel : ViewModelBase
    {
        #region Properties
        /// <summary>
        /// Message box title/caption.
        /// </summary>
        public String Title
        {
            get { return m_sTitle; }
            private set
            {
                SetPropertyValue(value, () => this.Title,
                    new PropertySetDelegate(delegate(Object newValue) { this.m_sTitle = (String)newValue; }));
            }
        }
        private String m_sTitle;

        /// <summary>
        /// Message box message string.
        /// </summary>
        public String Message
        {
            get { return m_sMessage; }
            private set
            {
                SetPropertyValue(value, () => this.Message,
                    new PropertySetDelegate(delegate(Object newValue) { this.m_sMessage = (String)newValue; }));
            }
        }
        private String m_sMessage;

        /// <summary>
        /// Message box buttons.
        /// </summary>
        public MessageBoxButton Button
        {
            get { return m_Button; }
            private set
            {
                SetPropertyValue(value, () => this.Button,
                    new PropertySetDelegate(delegate(Object newValue) { this.m_Button = (MessageBoxButton)newValue; }));
            }
        }
        private MessageBoxButton m_Button;

        /// <summary>
        /// Message box default result (timeout).
        /// </summary>
        public MessageBoxResult DefaultResult
        {
            get { return m_DefaultResult; }
            private set
            {
                SetPropertyValue(value, () => this.DefaultResult,
                    new PropertySetDelegate(delegate(Object newValue) { this.m_DefaultResult = (MessageBoxResult)newValue; }));
            }
        }
        private MessageBoxResult m_DefaultResult;

        /// <summary>
        /// Message box image.
        /// </summary>
        public MessageBoxImage Image
        {
            get { return m_Image; }
            private set
            {
                SetPropertyValue(value, () => this.Image,
                    new PropertySetDelegate(delegate(Object newValue) { this.m_Image = (MessageBoxImage)newValue; }));
            }
        }
        private MessageBoxImage m_Image;

        /// <summary>
        /// Message box timeout (seconds, default: 0, no timeout).
        /// </summary>
        public uint Timeout
        {
            get { return m_secTimeout; }
            private set
            {
                SetPropertyValue(value, () => this.Timeout,
                    new PropertySetDelegate(delegate(Object newValue) { this.m_secTimeout = (uint)newValue; }));
            }
        }
        private uint m_secTimeout;
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="title"></param>
        /// <param name="message"></param>
        /// <param name="buttons"></param>
        /// <param name="image"></param>
        /// <param name="defaultResult"></param>
        /// <param name="timeout">Timeout in seconds</param>
        /// 
        public CustomMessageBoxViewModel(String title, String message, 
            MessageBoxButton button, MessageBoxImage image,
            MessageBoxResult defaultResult, uint timeout)
        {
            this.Title = title;
            this.Message = message;
            this.Button = button;
            this.Image = image;
            this.DefaultResult = defaultResult;
            this.Timeout = timeout;
        }
        #endregion // Constructor(s)
    }

} // RSG.Base.Windows.Dialogs namespace
