﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using RSG.Base.Plugins;

namespace RSG.Base.Windows
{
    /// <summary>
    /// 
    /// </summary>
    public partial class PluginDlg : Window
    {
        #region Member Data
        /// <summary>
        /// 
        /// </summary>
        private IPlugin[] Plugins;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public PluginDlg(IPlugin[] plugins)
        {
            InitializeComponent();
            this.Plugins = plugins;
        }
        #endregion // Constructor(s)
    }

} // RSG.Base.Windows namespace
