﻿using System;
using System.Windows.Data;
using System.Windows.Media;

namespace RSG.Base.Windows.Converters
{
    /// <summary>
    /// 
    /// </summary>
    public class ColourToSolidBrushConverter : IValueConverter
    {
        #region IValueConverter Members

        /// <summary>
        /// Converts a Color to a SolidColorBrush.
        /// </summary>
        public Object Convert(Object value, Type targetType, Object parameter, System.Globalization.CultureInfo culture)
        {
            if (value != null)
                return new SolidColorBrush((Color)value);

            return value;
        }

        /// <summary>
        /// Converts a SolidColorBrush to a Color.
        /// </summary>
        public Object ConvertBack(Object value, Type targetType, Object parameter, System.Globalization.CultureInfo culture)
        {
            if (value != null)
                return ((SolidColorBrush)value).Color;

            return value;
        }

        #endregion
    }
}
