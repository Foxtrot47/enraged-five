﻿#region Using directives

using System;
using System.Collections.Generic;
using System.Text;

#endregion

namespace UILibrary
{
    internal class CommandHistory
    {
        private int currentPosn = -1;
        private string lastCommand;
        private List<string> commandHistory = new List<string>();

        internal CommandHistory()
        {
        }

        internal void Add(string command)
        {
            if ( command != lastCommand )
            {
                commandHistory.Add( command );
                lastCommand = command;
                currentPosn = commandHistory.Count;
            }
        }

        internal bool DoesPreviousCommandExist()
        {
            return currentPosn > -1;
        }

        internal bool DoesNextCommandExist()
        {
            return currentPosn < commandHistory.Count - 1;
        }

        internal string GetPreviousCommand()
        {
            if ( currentPosn > 0 )
            {
                --currentPosn;
                lastCommand = commandHistory[currentPosn];
            }

            return this.LastCommand;
        }

        internal string GetNextCommand()
        {
            if ( currentPosn < commandHistory.Count - 1 )
            {
                ++currentPosn;
                lastCommand = commandHistory[currentPosn];
            }

            return this.LastCommand;
        }

        internal string LastCommand
        {
            get 
            {
                return lastCommand;
            }
        }

        internal string[] GetCommandHistory()
        {
            return commandHistory.ToArray();
        }
    }
}
