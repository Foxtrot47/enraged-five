using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace UILibrary
{
	/// <summary>
	/// Summary description for CommandWindow.
	/// </summary>
	public class CommandWindow : System.Windows.Forms.Form
	{
		private System.Windows.Forms.ListBox m_ListBox;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public CommandWindow()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		public void AddCommands(string[] commands)
		{
			m_ListBox.Items.Clear();
			int index=0;
			m_ListBox.BeginUpdate();
			foreach (string command in commands)
			{
				m_ListBox.Items.Add(index + ": " + command);
				index++;
			}
			m_ListBox.EndUpdate();
			
			m_ListBox.SelectedIndex=m_ListBox.Items.Count-1;
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.m_ListBox = new System.Windows.Forms.ListBox();
			this.SuspendLayout();
			// 
			// m_ListBox
			// 
			this.m_ListBox.Dock = System.Windows.Forms.DockStyle.Fill;
			this.m_ListBox.Location = new System.Drawing.Point(0, 0);
			this.m_ListBox.Name = "m_ListBox";
			this.m_ListBox.Size = new System.Drawing.Size(504, 173);
			this.m_ListBox.TabIndex = 0;
			this.m_ListBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.m_ListBox_KeyDown);
			this.m_ListBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.m_ListBox_KeyUp);
			// 
			// CommandWindow
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(504, 184);
			this.Controls.Add(this.m_ListBox);
			this.Name = "CommandWindow";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Commands";
			this.ResumeLayout(false);

		}
		#endregion

		private void HandleKeyPress()
		{
			m_SelectedCommand="";
			string selectedCommand=m_ListBox.SelectedItem as string;
			try
			{
				selectedCommand=selectedCommand.Substring(selectedCommand.IndexOf(":")+2);
				m_SelectedCommand=selectedCommand;
			}
			catch (System.Exception)
			{
			}
			Close();
		}

		private void m_ListBox_KeyUp(object sender, System.Windows.Forms.KeyEventArgs e)
		{
			m_IsArrowKey=false;
			if (e.KeyCode==Keys.Enter)
			{
				HandleKeyPress();
			}
			else if (e.KeyCode==Keys.Escape)
			{
				m_SelectedCommand="";
				Close();
			}
		}

		bool m_IsArrowKey = false;
		public bool IsArrowKey
		{
			get
			{
				return m_IsArrowKey;
			}
		}

		string m_SelectedCommand="";

		private void m_ListBox_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
		{
			m_IsArrowKey=e.KeyCode==Keys.Left || e.KeyCode==Keys.Right;
		
			// prevent list box from using the arrow keys:
			if (m_IsArrowKey)
			{
				e.Handled=true;
				HandleKeyPress();
			}
		}
	
		public string SelectedCommand
		{
			get
			{
				return m_SelectedCommand;
			}
		}
	}
}
