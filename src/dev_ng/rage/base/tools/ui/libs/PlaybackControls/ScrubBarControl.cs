using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace PlaybackControls
{
    public partial class ScrubBarControl : UserControl
    {
        public ScrubBarControl()
        {
            InitializeComponent();

            InitializeAdditionalComponents();
        }

        private void InitializeAdditionalComponents()
        {
            this.SetStyle( ControlStyles.DoubleBuffer, true );
            this.SetStyle( ControlStyles.AllPaintingInWmPaint, true );
            this.SetStyle( ControlStyles.UserPaint, true );

            this.toolTip.SetToolTip( this.valueSliderPanel, "Click and drag to scrub the current frame." );

            CalculateMarkerLineDrawInfo();
            UpdateSliderPosition();
        }

        #region Constants
        private const int c_markerPad = 4;
        private const int c_minMarkerLineSpacing = 60;
        #endregion

        #region Variables
        private decimal m_playbackStart = 0.0M;
        private decimal m_playbackEnd = 100.0M;
        private decimal m_value = 0.0M;
        private decimal m_mouseDownValue = 0.0M;

        private decimal m_markerLineStep;
        private decimal m_markerLineSpacing;

        private bool m_mouseDown = false;
        private Point m_mousePos = new Point( 0, 0 );
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the Start of the Playback range.  Initializes the current Value to PlaybackStart (and invokes the ValueChanged
        /// event when changing its value).
        /// </summary>
        [DefaultValue( 0 )]
        public decimal PlaybackStart
        {
            get
            {
                return m_playbackStart;
            }
            set
            {
                m_playbackStart = value;

                //Clamp the position of the marker to the start of the playback range
                if (this.Value < this.PlaybackStart)
                {
                    this.Value = PlaybackStart;
                    OnValueChanged();
                }
               
                UpdateSliderPosition();
                CalculateMarkerLineDrawInfo();
                Invalidate( this.ClientRectangle );
            }
        }

        /// <summary>
        /// Gets or sets the End of the Playback range.  Initializes the current Value to PlaybackStart (and invokes the ValueChanged
        /// event when changing its value).
        /// </summary>
        [DefaultValue( 100 )]
        public decimal PlaybackEnd
        {
            get
            {
                return m_playbackEnd;
            }
            set
            {
                m_playbackEnd = value;

                //Clamp the position of the marker to the end of the playback range
                if (this.Value > this.PlaybackEnd)
                {
                    this.Value = this.PlaybackEnd;

                    OnValueChanged();
                }

                UpdateSliderPosition();
                CalculateMarkerLineDrawInfo();
                Invalidate( this.ClientRectangle );
            }
        }

        /// <summary>
        /// Gets or sets the current value.
        /// </summary>
        [DefaultValue( 0 )]
        public decimal Value
        {
            get
            {
                return m_value;
            }
            set
            {
                if ( m_value != value )
                {
                    m_value = value;

                    UpdateSliderPosition();                    
                }
            }
        }
        #endregion

        #region Events
        public event EventHandler ValueChanging;
        public event EventHandler ValueChanged;
        #endregion

        #region Event Dispatchers
        protected void OnValueChanging()
        {
            if ( this.ValueChanging != null )
            {
                this.ValueChanging( this, new EventArgs() );
            }
        }

        protected void OnValueChanged()
        {
            if ( this.ValueChanged != null )
            {
                this.ValueChanged( this, new EventArgs() );
            }
        }
        #endregion

        #region Overrides
        protected override void OnResize( EventArgs e )
        {
            CalculateMarkerLineDrawInfo();
            UpdateSliderPosition();

            base.OnResize( e );
        }

        protected override void OnPaint( PaintEventArgs pe )
        {
            if ( this.PlaybackStart == this.PlaybackEnd )
            {
                return;
            }

            Brush markerTextBrush = new SolidBrush( this.Enabled ? SystemColors.ControlText : SystemColors.GrayText );
            Pen markerLinePen = new Pen( markerTextBrush, 1.0f );
            
            //Draw the marker lines
            int markerLineHeight = (this.ClientSize.Height) / 2;
            decimal curVal = this.PlaybackStart;

            for ( decimal i = this.PlaybackStart; i <= this.PlaybackEnd; i += m_markerLineStep )
            {
                decimal X = CalcPositionFromValue( i );

                pe.Graphics.DrawLine( markerLinePen, (int)X, 0, (int)X, (float)markerLineHeight );

                float frameNumStringSize = pe.Graphics.MeasureString( i.ToString( "f1" ), this.Font ).Width;

                if ( (X + Convert.ToDecimal( frameNumStringSize ) + 2) > GetScrubAreaEnd() )
                {
                    pe.Graphics.DrawString( i.ToString( "f1" ), this.Font, markerTextBrush, 
                        (float)Convert.ToDouble( X ) - frameNumStringSize - 2.0f, 1 );
                }
                else
                {
                    pe.Graphics.DrawString( i.ToString( "f1" ), this.Font, markerTextBrush,
                        (float)Convert.ToDouble( X ) + 2.0f, 1 );
                }
            }

            //Draw the current value 			
            string valueString = m_value.ToString( "f1" );
            float valueNoteSize = pe.Graphics.MeasureString( valueString, this.Font ).Width;

            Font boldFont = new Font( this.Font, FontStyle.Bold );

            if ( (this.Width - (this.valueSliderPanel.Location.X + this.valueSliderPanel.Width + 1)) < valueNoteSize )
            {
                //The frame note doesn't fit on the right side of the frame slider, so flip it over to the left side
                pe.Graphics.DrawString( valueString, boldFont, markerTextBrush,
                    (this.valueSliderPanel.Location.X - valueNoteSize - 1),
                    (this.Height - this.Font.Height - 1) );
            }
            else
            {
                //Draw the frame note on the right side of the frame slider.
                pe.Graphics.DrawString( valueString, boldFont, markerTextBrush,
                    (this.valueSliderPanel.Location.X + this.valueSliderPanel.Width + 1),
                    (this.Height - this.Font.Height - 1) );
            }

            markerTextBrush.Dispose();
            markerLinePen.Dispose();
            boldFont.Dispose();
        }
        #endregion

        #region Event Handlers
        private void ScrubBarControl_MouseMove( object sender, MouseEventArgs e )
        {
            Point screenPoint = Control.MousePosition;

            if ( m_mouseDown )
            {
                Point clientPoint = this.PointToClient( screenPoint );

                if ( clientPoint.X < c_markerPad )
                {
                    clientPoint.X = c_markerPad;
                }
                else if ( clientPoint.X > (this.Width - c_markerPad) )
                {
                    clientPoint.X = (this.Width - c_markerPad);
                }

                this.Value = CalcValueFromPosition( clientPoint.X );

                OnValueChanging();
            }

            m_mousePos = screenPoint;
        }
        
        private void ScrubBarControl_MouseDown(object sender, MouseEventArgs e)
        {
            Point screenPoint = Control.MousePosition;
            Point clientPoint = this.PointToClient(screenPoint);

            decimal downValue = CalcValueFromPosition(clientPoint.X);
            if(downValue != this.Value)
            {
                this.Value = downValue;
                OnValueChanged();
            }
            m_mouseDown = true;
        }

        private void ScrubBarControl_MouseLeave(object sender, EventArgs e)
        {
            if (m_mouseDown)
            {
                m_mouseDown = false;

                if (this.Value != m_mouseDownValue)
                {
                    OnValueChanged();
                }
            }
        }

        private void valueSliderPanel_MouseDown( object sender, MouseEventArgs e )
        {
            m_mouseDown = true;
            m_mousePos = Control.MousePosition;
            m_mouseDownValue = this.Value;
        }

        private void valueSliderPanel_MouseMove( object sender, MouseEventArgs e )
        {
            ScrubBarControl_MouseMove( sender, e );
        }

        private void valueSliderPanel_MouseUp( object sender, MouseEventArgs e )
        {
            ScrubBarControl_MouseLeave( sender, EventArgs.Empty );
        }
        #endregion

        #region Private Functions
        private decimal GetScrubAreaWidth()
        {
            return Convert.ToDecimal( this.ClientSize.Width - (2 * c_markerPad) );
        }

        private decimal GetScrubAreaBegin()
        {
            return Convert.ToDecimal( c_markerPad );
        }

        private decimal GetScrubAreaEnd()
        {
            return Convert.ToDecimal( this.ClientSize.Width - c_markerPad );
        }

        private decimal CalcPositionFromValue( decimal inValue )
        {
            if ( this.PlaybackStart == this.PlaybackEnd )
            {
                return 0.0M;
            }

            decimal pixelsPerStep = GetScrubAreaWidth() / (this.PlaybackEnd - this.PlaybackStart);

            return (((inValue - this.PlaybackStart) * pixelsPerStep) + GetScrubAreaBegin());
        }

        private decimal CalcValueFromPosition( decimal inPosition )
        {
            if ( this.PlaybackStart == this.PlaybackEnd )
            {
                return 0.0M;
            }

            decimal pixelsPerStep = GetScrubAreaWidth() / (this.PlaybackEnd - this.PlaybackStart);

            decimal val = (((inPosition - GetScrubAreaBegin()) / pixelsPerStep) + this.PlaybackStart);
            if ( val < this.PlaybackStart )
            {
                val = this.PlaybackStart;
            }
            else if ( val > this.PlaybackEnd )
            {
                val = this.PlaybackEnd;
            }

            return val;
        }

        private void UpdateSliderPosition()
        {
            decimal sliderLoc = CalcPositionFromValue( m_value );

            this.valueSliderPanel.Location = new Point( (int)sliderLoc, this.valueSliderPanel.Location.Y );

            Invalidate( this.ClientRectangle );
        }

        private void CalculateMarkerLineDrawInfo()
        {
            m_markerLineStep = 1.0M;
            m_markerLineSpacing = 0;

            if ( (this.PlaybackStart == this.PlaybackEnd) || (GetScrubAreaWidth() <= 0) )
            {
                return;
            }

            while ( m_markerLineSpacing < c_minMarkerLineSpacing )
            {
                if ( (this.PlaybackEnd - this.PlaybackStart) < 1.0M )
                {
                    m_markerLineStep = (this.PlaybackEnd - this.PlaybackStart);
                    return;
                }

                int numLines = (int)((this.PlaybackEnd - this.PlaybackStart) / m_markerLineStep);

                m_markerLineSpacing = (GetScrubAreaWidth() / numLines);

                if ( m_markerLineSpacing < c_minMarkerLineSpacing )
                {
                    if ( m_markerLineStep == 1 )
                    {
                        m_markerLineStep = 2;
                    }
                    else if ( m_markerLineStep == 2 )
                    {
                        m_markerLineStep = 5;
                    }
                    else
                    {
                        m_markerLineStep += 5;
                    }
                }
                else
                {
                    break;
                }
            }
        }
        #endregion
    }
}
