namespace PlaybackControls
{
    partial class PlaybackRangeSliderControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && (components != null) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.playbackStartButton = new System.Windows.Forms.Button();
            this.playbackEndButton = new System.Windows.Forms.Button();
            this.toolTip = new System.Windows.Forms.ToolTip( this.components );
            this.SuspendLayout();
            // 
            // playbackStartButton
            // 
            this.playbackStartButton.BackColor = System.Drawing.SystemColors.Control;
            this.playbackStartButton.Location = new System.Drawing.Point( 4, 2 );
            this.playbackStartButton.Name = "playbackStartButton";
            this.playbackStartButton.Size = new System.Drawing.Size( 16, 14 );
            this.playbackStartButton.TabIndex = 0;
            this.playbackStartButton.UseVisualStyleBackColor = false;
            this.playbackStartButton.MouseMove += new System.Windows.Forms.MouseEventHandler( this.playbackStartButton_MouseMove );
            this.playbackStartButton.MouseDown += new System.Windows.Forms.MouseEventHandler( this.playbackStartButton_MouseDown );
            this.playbackStartButton.MouseUp += new System.Windows.Forms.MouseEventHandler( this.playbackStartButton_MouseUp );
            // 
            // playbackEndButton
            // 
            this.playbackEndButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.playbackEndButton.BackColor = System.Drawing.SystemColors.Control;
            this.playbackEndButton.Location = new System.Drawing.Point( 279, 2 );
            this.playbackEndButton.Name = "playbackEndButton";
            this.playbackEndButton.Size = new System.Drawing.Size( 16, 14 );
            this.playbackEndButton.TabIndex = 1;
            this.playbackEndButton.UseVisualStyleBackColor = false;
            this.playbackEndButton.MouseMove += new System.Windows.Forms.MouseEventHandler( this.playbackEndButton_MouseMove );
            this.playbackEndButton.MouseDown += new System.Windows.Forms.MouseEventHandler( this.playbackEndButton_MouseDown );
            this.playbackEndButton.MouseUp += new System.Windows.Forms.MouseEventHandler( this.playbackEndButton_MouseUp );
            // 
            // PlaybackRangeSliderControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlDark;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add( this.playbackEndButton );
            this.Controls.Add( this.playbackStartButton );
            this.Name = "PlaybackRangeSliderControl";
            this.Size = new System.Drawing.Size( 298, 18 );
            this.ResumeLayout( false );

        }

        #endregion

        private System.Windows.Forms.Button playbackStartButton;
        private System.Windows.Forms.Button playbackEndButton;
        private System.Windows.Forms.ToolTip toolTip;
    }
}
