using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace PlaybackControls
{
    public partial class PlaybackControl : UserControl
    {
        public PlaybackControl()
        {
            InitializeComponent();
        }

        #region Properties
        [DefaultValue( true )]
        public bool CanRewindOrGoToStart
        {
            get
            {
                return this.vcrControl.CanRewindOrGoToStart;
            }
            set
            {
                this.vcrControl.CanRewindOrGoToStart = value;
            }
        }

        [DefaultValue( true )]
        public bool CanGoToPreviousOrStepBackward
        {
            get
            {
                return this.vcrControl.CanGoToPreviousOrStepBackward;
            }
            set
            {
                this.vcrControl.CanGoToPreviousOrStepBackward = value;
            }
        }

        [DefaultValue( true )]
        public bool CanPlayBackwards
        {
            get
            {
                return this.vcrControl.CanPlayBackwards;
            }
            set
            {
                this.vcrControl.CanPlayBackwards = value;
            }
        }

        [DefaultValue( true )]
        public bool CanPause
        {
            get
            {
                return this.vcrControl.CanPause;
            }
            set
            {
                this.vcrControl.CanPause = value;
            }
        }

        [DefaultValue( true )]
        public bool CanPlayForwards
        {
            get
            {
                return this.vcrControl.CanPlayForwards;
            }
            set
            {
                this.vcrControl.CanPlayForwards = value;
            }
        }

        [DefaultValue( true )]
        public bool CanGoToNextOrStepForward
        {
            get
            {
                return this.vcrControl.CanGoToNextOrStepForward;
            }
            set
            {
                this.vcrControl.CanGoToNextOrStepForward = value;
            }
        }

        [DefaultValue( true )]
        public bool CanFastForwardOrGoToEnd
        {
            get
            {
                return this.vcrControl.CanFastForwardOrGoToEnd;
            }
            set
            {
                this.vcrControl.CanFastForwardOrGoToEnd = value;
            }
        }

        [DefaultValue( 0 )]
        public decimal MinimumFrame
        {
            get
            {
                return this.playbackRangeControl.MinimumFrame;
            }
            set
            {
                this.playbackRangeControl.MinimumFrame = value;
            }
        }

        [DefaultValue( 10000 )]
        public decimal MaximumFrame
        {
            get
            {
                return this.playbackRangeControl.MaximumFrame;
            }
            set
            {
                this.playbackRangeControl.MaximumFrame = value;
            }
        }

        [DefaultValue( true )]
        public bool StartFrameEnabled
        {
            get
            {
                return this.playbackRangeControl.StartFrameEnabled;
            }
            set
            {
                this.playbackRangeControl.StartFrameEnabled = value;
            }
        }

        [DefaultValue( true )]
        public bool EndFrameEnabled
        {
            get
            {
                return this.playbackRangeControl.EndFrameEnabled;
            }
            set
            {
                this.playbackRangeControl.EndFrameEnabled = value;
            }
        }

        [DefaultValue( 0 )]
        public decimal StartFrame
        {
            get
            {
                return this.playbackRangeControl.StartFrame;
            }
            set
            {
                this.playbackRangeControl.StartFrame = value;
            }
        }

        [DefaultValue( 100 )]
        public decimal EndFrame
        {
            get
            {
                return this.playbackRangeControl.EndFrame;
            }
            set
            {
                this.playbackRangeControl.EndFrame = value;
            }
        }

        [DefaultValue( 0 )]
        public decimal PlaybackStart
        {
            get
            {
                return this.playbackRangeControl.PlaybackStart;
            }
            set
            {
                this.playbackRangeControl.PlaybackStart = value;
                this.scrubBarControl.PlaybackStart = value;
            }
        }

        [DefaultValue( 100 )]
        public decimal PlaybackEnd
        {
            get
            {
                return this.playbackRangeControl.PlaybackEnd;
            }
            set
            {
                this.playbackRangeControl.PlaybackEnd = value;
                this.scrubBarControl.PlaybackEnd = value;
            }
        }

        [DefaultValue( 0 )]
        public decimal Value
        {
            get
            {
                return this.scrubBarControl.Value;
            }
            set
            {
                this.scrubBarControl.Value = value;
            }
        }

        [DefaultValue( VCRControl.ButtonStyle.VCR )]
        public VCRControl.ButtonStyle VCRButtonStyle
        {
            get
            {
                return this.vcrControl.Style;
            }
            set
            {
                this.vcrControl.Style = value;
            }
        }
        #endregion

        #region Events
        public event EventHandler StartFrameChanged
        {
            add
            {
                this.playbackRangeControl.StartFrameChanged += value;
            }
            remove
            {
                this.playbackRangeControl.StartFrameChanged -= value;
            }
        }

        public event EventHandler EndFrameChanged
        {
            add
            {
                this.playbackRangeControl.EndFrameChanged += value;
            }
            remove
            {
                this.playbackRangeControl.EndFrameChanged -= value;
            }
        }

        public event EventHandler PlaybackStartChanged
        {
            add
            {
                this.playbackRangeControl.PlaybackStartChanged += value;
            }
            remove
            {
                this.playbackRangeControl.PlaybackStartChanged -= value;
            }
        }

        public event EventHandler PlaybackEndChanged
        {
            add
            {
                this.playbackRangeControl.PlaybackEndChanged += value;
            }
            remove
            {
                this.playbackRangeControl.PlaybackEndChanged -= value;
            }
        }

        public event EventHandler ValueChanging
        {
            add
            {
                this.scrubBarControl.ValueChanging += value;
            }
            remove
            {
                this.scrubBarControl.ValueChanging -= value;
            }
        }

        public event EventHandler ValueChanged
        {
            add
            {
                this.scrubBarControl.ValueChanged += value;
            }
            remove
            {
                this.scrubBarControl.ValueChanged -= value;
            }
        }

        public event EventHandler RewindOrGoToStartButtonClick
        {
            add
            {
                this.vcrControl.RewindOrGoToStartButtonClick += value;
            }
            remove
            {
                this.vcrControl.RewindOrGoToStartButtonClick -= value;
            }
        }

        public event EventHandler GoToPreviousOrStepBackwardButtonClick
        {
            add
            {
                this.vcrControl.GoToPreviousOrStepBackwardButtonClick += value;
            }
            remove
            {
                this.vcrControl.GoToPreviousOrStepBackwardButtonClick -= value;
            }
        }

        public event EventHandler PlayBackwardsButtonClick
        {
            add
            {
                this.vcrControl.PlayBackwardsButtonClick += value;
            }
            remove
            {
                this.vcrControl.PlayBackwardsButtonClick -= value;
            }
        }

        public event EventHandler PauseButtonClick
        {
            add
            {
                this.vcrControl.PauseButtonClick += value;
            }
            remove
            {
                this.vcrControl.PauseButtonClick -= value;
            }
        }

        public event EventHandler PlayForwardsButtonClick
        {
            add
            {
                this.vcrControl.PlayForwardsButtonClick += value;
            }
            remove
            {
                this.vcrControl.PlayForwardsButtonClick -= value;
            }
        }

        public event EventHandler GoToNextOrStepForwardButtonClick
        {
            add
            {
                this.vcrControl.GoToNextOrStepForwardButtonClick += value;
            }
            remove
            {
                this.vcrControl.GoToNextOrStepForwardButtonClick -= value;
            }
        }

        public event EventHandler FastForwardOrGoToEndButtonClick
        {
            add
            {
                this.vcrControl.FastForwardOrGoToEndButtonClick += value;
            }
            remove
            {
                this.vcrControl.FastForwardOrGoToEndButtonClick -= value;
            }
        }
        #endregion

        #region Event Handlers
        private void playbackRangeControl_EndChanged( object sender, EventArgs e )
        {
            this.scrubBarControl.PlaybackEnd = this.playbackRangeControl.PlaybackEnd;
        }

        private void playbackRangeControl_MaximumChanged( object sender, EventArgs e )
        {
            this.scrubBarControl.PlaybackEnd = this.playbackRangeControl.PlaybackEnd;
        }

        private void playbackRangeControl_MinimumChanged( object sender, EventArgs e )
        {
            this.scrubBarControl.PlaybackStart = this.playbackRangeControl.PlaybackStart;
        }

        private void playbackRangeControl_StartChanged( object sender, EventArgs e )
        {
            this.scrubBarControl.PlaybackStart = this.playbackRangeControl.PlaybackStart;
        }
        #endregion
    }
}