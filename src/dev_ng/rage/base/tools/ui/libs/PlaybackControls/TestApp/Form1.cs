using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace TestApp
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void playbackControl_EndFrameChanged( object sender, EventArgs e )
        {
            this.richTextBox.AppendText( "EndFrameChanged" + Environment.NewLine );
        }

        private void playbackControl_FastForwardOrGoToEndButtonClick( object sender, EventArgs e )
        {
            this.richTextBox.AppendText( "FastForwardOrGoToEndButtonClick" + Environment.NewLine );
        }

        private void playbackControl_GoToNextOrStepForwardButtonClick( object sender, EventArgs e )
        {
            this.richTextBox.AppendText( "GoToNextOrStepForwardButtonClick" + Environment.NewLine );
        }

        private void playbackControl_GoToPreviousOrStepBackwardButtonClick( object sender, EventArgs e )
        {
            this.richTextBox.AppendText( "GoToPreviousOrStepBackwardButtonClick" + Environment.NewLine );
        }

        private void playbackControl_PauseButtonClick( object sender, EventArgs e )
        {
            this.richTextBox.AppendText( "PauseButtonClick" + Environment.NewLine );
        }

        private void playbackControl_PlaybackEndChanged( object sender, EventArgs e )
        {
            this.richTextBox.AppendText( "PlaybackEndChanged" + Environment.NewLine );
        }

        private void playbackControl_PlaybackStartChanged( object sender, EventArgs e )
        {
            this.richTextBox.AppendText( "PlaybackStartChanged" + Environment.NewLine );
        }

        private void playbackControl_PlayBackwardsButtonClick( object sender, EventArgs e )
        {
            this.richTextBox.AppendText( "PlayBackwardsButtonClick" + Environment.NewLine );
        }

        private void playbackControl_PlayForwardsButtonClick( object sender, EventArgs e )
        {
            this.richTextBox.AppendText( "PlayForwardsButtonClick" + Environment.NewLine );
        }

        private void playbackControl_RewindOrGoToStartButtonClick( object sender, EventArgs e )
        {
            this.richTextBox.AppendText( "RewindOrGoToStartButtonClick" + Environment.NewLine );
        }

        private void playbackControl_StartFrameChanged( object sender, EventArgs e )
        {
            this.richTextBox.AppendText( "StartFrameChanged" + Environment.NewLine );
        }

        private void playbackControl_ValueChanged( object sender, EventArgs e )
        {
            this.richTextBox.AppendText( "ValueChanged" + Environment.NewLine );
        }
    }
}