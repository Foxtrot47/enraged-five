namespace PlaybackControls
{
    partial class ScrubBarControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && (components != null) )
            {
                components.Dispose();
            }

            base.Dispose( disposing );
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.valueSliderPanel = new System.Windows.Forms.Panel();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.SuspendLayout();
            // 
            // valueSliderPanel
            // 
            this.valueSliderPanel.BackColor = System.Drawing.SystemColors.ControlText;
            this.valueSliderPanel.Location = new System.Drawing.Point(192, 0);
            this.valueSliderPanel.Name = "valueSliderPanel";
            this.valueSliderPanel.Size = new System.Drawing.Size(3, 32);
            this.valueSliderPanel.TabIndex = 0;
            this.valueSliderPanel.MouseMove += new System.Windows.Forms.MouseEventHandler(this.valueSliderPanel_MouseMove);
            this.valueSliderPanel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.valueSliderPanel_MouseDown);
            this.valueSliderPanel.MouseUp += new System.Windows.Forms.MouseEventHandler(this.valueSliderPanel_MouseUp);
            // 
            // ScrubBarControl
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.BackColor = System.Drawing.SystemColors.ControlDark;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.valueSliderPanel);
            this.Name = "ScrubBarControl";
            this.Size = new System.Drawing.Size(438, 30);
            this.MouseLeave += new System.EventHandler(this.ScrubBarControl_MouseLeave);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.ScrubBarControl_MouseMove);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ScrubBarControl_MouseDown);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel valueSliderPanel;
        private System.Windows.Forms.ToolTip toolTip;
    }
}
