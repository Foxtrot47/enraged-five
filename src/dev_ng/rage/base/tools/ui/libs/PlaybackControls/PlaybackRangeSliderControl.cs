using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace PlaybackControls
{
    public partial class PlaybackRangeSliderControl : UserControl
    {
        public PlaybackRangeSliderControl()
        {
            InitializeComponent();

            InitializeAdditionalComponents();
        }

        private void InitializeAdditionalComponents()
        {
            SetStyle( ControlStyles.DoubleBuffer, true );
            SetStyle( ControlStyles.AllPaintingInWmPaint, true );
            SetStyle( ControlStyles.UserPaint, true );

            this.toolTip.SetToolTip( this.playbackStartButton, "Click and drag to set the Playback Start Frame." );
            this.toolTip.SetToolTip( this.playbackEndButton, "Click and drag to set the Playback End Frame." );

            m_startMouseDown = false;
            m_endMouseDown = false;
            m_mousePos = new Point( 0, 0 );

            UpdateMarkerPositions();
        }

        #region Constants
        private const int c_markerPad = 8;
        #endregion

        #region Variables
        private decimal m_startFrame = 0.0M;
        private decimal m_endFrame = 100.0M;
        private decimal m_playbackStart = 0.0M;
        private decimal m_playbackEnd = 100.0M;

        private Boolean m_startMouseDown;
        private Boolean m_endMouseDown;
        private Point m_mousePos;
        #endregion

        #region Properties
        /// <summary>
        /// When setting the StartFrame time, always set EndFrame first.  Ensures that StartFrame is at least 0.01 less than EndFrame,
        /// but never less than 0.  Ensures that PlaybackStart is never less than StartFrame (will dispatch PlaybackStartChanged if that is 
        /// adjusted).  Ensures that PlaybackEnd is at least 1% of the range above PlaybackStart (will dispatch PlaybackEndChanged if that is adjusted).
        /// </summary>
        [DefaultValue( 0 )]
        public decimal StartFrame
        {
            get
            {
                return m_startFrame;
            }
            set
            {
                m_startFrame = value;

                if ( m_startFrame >= m_endFrame )
                {
                    m_startFrame = m_endFrame - 1.0M;
                }

                if ( m_startFrame < 0.0M )
                {
                    m_startFrame = 0.0M;
                }

                if ( m_playbackStart < m_startFrame )
                {
                    m_playbackStart = m_startFrame;

                    //Don't send out the changed events while the value is still changing as
                    //this causes the interactivity of the control do drop
                    if ( !m_startMouseDown )
                    {
                        OnPlaybackStartChanged();
                    }
                }

                if ( m_playbackEnd <= m_playbackStart )
                {
                    m_playbackEnd = m_playbackStart + ((this.EndFrame - this.StartFrame) * 0.01M);

                    //Don't send out the changed events while the value is still changing as
                    //this causes the interactivity of the control do drop
                    if ( !m_endMouseDown )
                    {
                        OnPlaybackEndChanged();
                    }
                }

                UpdateMarkerPositions();
            }
        }

        /// <summary>
        /// When setting the EndFrame time, call this before StartFrame.  Ensures that EndFrame is at least 0.01 greater than StartFrame.
        /// Ensures that PlaybackEnd is never greater than EndFrame (will dispatch PlaybackEndChanged if that is adjusted).  Ensures that
        /// PlaybackStart is at least 1% of the range less than PlaybackEnd (will dispatch PlaybackStartChanged if that is adjusted).
        /// </summary>
        [DefaultValue( 100 )]
        public decimal EndFrame
        {
            get
            {
                return m_endFrame;
            }
            set
            {
                m_endFrame = value;

                if ( m_endFrame <= m_startFrame )
                {
                    m_endFrame = m_startFrame + 1.0M;
                }

                if ( m_playbackEnd > m_endFrame )
                {
                    m_playbackEnd = m_endFrame;

                    //Don't send out the changed events while the value is still changing as
                    //this causes the interactivity of the control do drop
                    if ( !m_endMouseDown )
                    {
                        OnPlaybackEndChanged();
                    }
                }

                if ( m_playbackStart >= m_playbackEnd )
                {
                    m_playbackStart = m_playbackEnd - ((this.EndFrame - this.StartFrame) * 0.01M);

                    //Don't send out the changed events while the value is still changing as
                    //this causes the interactivity of the control do drop
                    if ( !m_startMouseDown )
                    {
                        OnPlaybackStartChanged();
                    }
                }

                UpdateMarkerPositions();
            }
        }

        /// <summary>
        /// When setting PlaybackStart, always set PlaybackEnd first.  Ensures that PlaybackStart is at least 1% of the range less than PlaybackEnd.  Ensures that
        /// PlaybackStart is not less than StartFrame.
        /// </summary>
        [DefaultValue( 0 )]        
        public decimal PlaybackStart
        {
            get
            {
                return m_playbackStart;
            }
            set
            {
                if ( this.PlaybackStart != value )
                {
                    m_playbackStart = value;
                    
                    if ( m_playbackStart >= m_playbackEnd )
                    {
                        m_playbackStart = m_playbackEnd - ((this.EndFrame - this.StartFrame) * 0.01M);
                    }

                    if ( m_playbackStart < m_startFrame )
                    {
                        m_playbackStart = m_startFrame;
                    }

                    UpdateMarkerPositions();
                }
            }
        }

        /// <summary>
        /// When setting PlaybackEnd, always set before PlaybackStart.  Ensures that PlaybackEnd is at least 1% of the range greater than PlaybackStart.  Ensures that
        /// PlaybackEnd is not greater than StartFrame.
        /// </summary>
        [DefaultValue( 100 )]
        public decimal PlaybackEnd
        {
            get
            {
                return m_playbackEnd;
            }
            set
            {
                if ( m_playbackEnd != value )
                {
                    m_playbackEnd = value;

                    if ( m_playbackEnd <= m_playbackStart )
                    {
                        m_playbackEnd = m_playbackStart + ((this.EndFrame - this.StartFrame) * 0.01M);
                    }

                    if ( m_playbackEnd > m_endFrame )
                    {
                        m_playbackEnd = m_endFrame;
                    }

                    UpdateMarkerPositions();
                }
            }
        }
        #endregion

        #region Events
        /// <summary>
        /// Dispatched when PlaybackStart is changed by the User or when EndFrame or StartFrame clamps the value.
        /// </summary>
        public event EventHandler PlaybackStartChanged;

        /// <summary>
        /// Dispatched when PlaybackEnd is changed by the User or when EndFrame or StartFrame clamps the value.
        /// </summary>
        public event EventHandler PlaybackEndChanged;
        #endregion

        #region Event Dispatchers
        protected void OnPlaybackStartChanged()
        {
            if ( this.PlaybackStartChanged != null )
            {
                this.PlaybackStartChanged( this, EventArgs.Empty );
            }
        }

        protected void OnPlaybackEndChanged()
        {
            if ( this.PlaybackEndChanged != null )
            {
                this.PlaybackEndChanged( this, EventArgs.Empty );
            }
        }
        #endregion

        #region Event Handlers
        private void playbackStartButton_MouseDown( object sender, MouseEventArgs e )
        {
            m_startMouseDown = true;
            m_mousePos = Control.MousePosition;
            this.playbackStartButton.BackColor = SystemColors.Highlight;
        }

        private void playbackStartButton_MouseMove( object sender, MouseEventArgs e )
        {
            Point screenPoint = Control.MousePosition;

            if ( m_startMouseDown )
            {
                Point clientPoint = this.PointToClient(screenPoint);

                //Check that the button isn't moving out of the control
                float halfButtonWidth = (float)this.playbackStartButton.Width / 2.0f;
                float newLocation = (float)clientPoint.X + halfButtonWidth;

                if (newLocation - halfButtonWidth < c_markerPad)
                {
                    newLocation = c_markerPad + halfButtonWidth;
                }
                else if ((newLocation + halfButtonWidth) > (this.Width - c_markerPad))
                {
                    newLocation = this.Width - c_markerPad - halfButtonWidth;
                }

                //Check that the start button isn't overlapping the end button
                if ((newLocation + halfButtonWidth) >= this.playbackEndButton.Location.X)
                {
                    newLocation = this.playbackEndButton.Location.X - halfButtonWidth - 1;
                }

                this.PlaybackStart = CalcValueFromPosition( (decimal)newLocation);
            }

            m_mousePos = screenPoint;
        }

        private void playbackStartButton_MouseUp( object sender, MouseEventArgs e )
        {
            if ( m_startMouseDown )
            {
                m_startMouseDown = false;
                this.playbackStartButton.BackColor = SystemColors.Control;

                OnPlaybackStartChanged();
            }
        }

        private void playbackEndButton_MouseDown( object sender, MouseEventArgs e )
        {
            m_endMouseDown = true;
            m_mousePos = Control.MousePosition;
            this.playbackEndButton.BackColor = SystemColors.Highlight;
        }

        private void playbackEndButton_MouseMove( object sender, MouseEventArgs e )
        {
            Point screenPoint = Control.MousePosition;

            if ( m_endMouseDown )
            {
                Point clientPoint = this.PointToClient(screenPoint);

                //Check that the button isn't moving out of the control
                float halfButtonWidth = (float)this.playbackEndButton.Width / 2.0f;
                
                float newLocation = (float)clientPoint.X + halfButtonWidth;

                if (newLocation + halfButtonWidth < c_markerPad)
                {
                    newLocation = c_markerPad + halfButtonWidth;
                }
                else if ((newLocation + halfButtonWidth) > (this.Width - c_markerPad))
                {
                    newLocation = this.Width - c_markerPad - halfButtonWidth;
                }

                //Check that the start button isn't overlapping the end button
                if (newLocation <= (this.playbackStartButton.Location.X + halfButtonWidth))
                {
                    newLocation = this.playbackStartButton.Location.X + halfButtonWidth + 1;
                }

                this.PlaybackEnd = CalcValueFromPosition( (decimal)newLocation );
            }

            m_mousePos = screenPoint;
        }

        private void playbackEndButton_MouseUp( object sender, MouseEventArgs e )
        {
            if ( m_endMouseDown )
            {
                m_endMouseDown = false;
                this.playbackEndButton.BackColor = SystemColors.Control;

                OnPlaybackEndChanged();
            }
        }
        #endregion

        #region Overrides
        protected override void OnResize( EventArgs e )
        {
            UpdateMarkerPositions();
            base.OnResize( e );
        }

        protected override void OnPaint( PaintEventArgs e )
        {
            Brush buttonRectBrush = new SolidBrush( SystemColors.Control );
            Brush buttonTextBrush = new SolidBrush( this.Enabled ? SystemColors.ControlText : SystemColors.GrayText );

            // Draw the enclosing rectangle around the start and end buttons
            int rX = this.playbackStartButton.Location.X - 2;
            int rY = this.playbackStartButton.Location.Y - 2;
            int rW = (this.playbackEndButton.Location.X + this.playbackEndButton.Width + 2) - rX;
            int rH = this.playbackStartButton.Height + 4;
            e.Graphics.FillRectangle( buttonRectBrush, rX, rY, rW, rH );

            // Draw the start and end value text
            int startTextWidth = (int)(e.Graphics.MeasureString( this.PlaybackStart.ToString( "f1" ), this.Font ).Width);
            int endTextWidth = (int)(e.Graphics.MeasureString( this.PlaybackEnd.ToString( "f1" ), this.Font ).Width);

            int sX = this.playbackStartButton.Location.X + this.playbackStartButton.Width + 1;
            int sY = this.playbackStartButton.Location.Y + (this.playbackStartButton.Height - this.Font.Height);

            int eX = this.playbackEndButton.Location.X - endTextWidth - 1;
            int eY = this.playbackEndButton.Location.Y + (this.playbackEndButton.Height - this.Font.Height);

            //Only draw the text if there is enough room
            if ( (startTextWidth + endTextWidth) < (this.playbackEndButton.Location.X - (this.playbackStartButton.Location.X + this.playbackStartButton.Width)) )
            {
                e.Graphics.DrawString( this.PlaybackStart.ToString( "f1" ), this.Font, buttonTextBrush, sX, sY );
                e.Graphics.DrawString( this.PlaybackEnd.ToString( "f1" ), this.Font, buttonTextBrush, eX, eY );
            }

            buttonRectBrush.Dispose();
            buttonTextBrush.Dispose();

            base.OnPaint( e );            
        }
        #endregion

        #region Private Functions
        private decimal GetScrubAreaBegin()
        {
            return Convert.ToDecimal( c_markerPad + this.playbackStartButton.Width );
        }

        private decimal GetScrubAreaEnd()
        {
            return Convert.ToDecimal( this.ClientRectangle.Width - c_markerPad - this.playbackEndButton.Width );
        }

        private decimal GetScrubAreaWidth()
        {
            return (GetScrubAreaEnd() - GetScrubAreaBegin());
        }

        private decimal CalcPositionFromValue( decimal val )
        {
            if ( this.StartFrame == this.EndFrame )
            {
                return 0;
            }

            decimal phase = (val - this.StartFrame) / (this.EndFrame - this.StartFrame);

            return (phase * GetScrubAreaWidth()) + GetScrubAreaBegin();
        }

        private decimal CalcValueFromPosition( decimal pos )
        {
            if ( this.PlaybackStart == this.PlaybackEnd )
            {
                return this.PlaybackStart;
            }

            decimal phase = (pos - GetScrubAreaBegin()) / GetScrubAreaWidth();

            return (phase * (this.EndFrame - this.StartFrame)) + this.StartFrame;
        }

        private void UpdateMarkerPositions()
        {
            int startLoc = (int)CalcPositionFromValue( this.PlaybackStart );
            int endLoc = (int)CalcPositionFromValue( this.PlaybackEnd );

            this.playbackStartButton.Location = new Point( startLoc - this.playbackStartButton.Width, this.playbackStartButton.Location.Y );
            this.playbackEndButton.Location = new Point( endLoc, this.playbackEndButton.Location.Y );

            Invalidate( this.ClientRectangle );
        }
        #endregion
    }
}
