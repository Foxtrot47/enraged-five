namespace PlaybackControls
{
    partial class PlaybackControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && (components != null) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.numericUpDown2 = new System.Windows.Forms.NumericUpDown();
            this.vcrControl = new PlaybackControls.VCRControl();
            this.playbackRangeControl = new PlaybackControls.PlaybackRangeControl();
            this.scrubBarControl = new PlaybackControls.ScrubBarControl();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).BeginInit();
            this.SuspendLayout();
            // 
            // numericUpDown2
            // 
            this.numericUpDown2.Location = new System.Drawing.Point( 78, 42 );
            this.numericUpDown2.Name = "numericUpDown2";
            this.numericUpDown2.Size = new System.Drawing.Size( 120, 20 );
            this.numericUpDown2.TabIndex = 2;
            // 
            // vcrControl
            // 
            this.vcrControl.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.vcrControl.Location = new System.Drawing.Point( 712, 39 );
            this.vcrControl.Name = "vcrControl";
            this.vcrControl.Size = new System.Drawing.Size( 205, 24 );
            this.vcrControl.TabIndex = 2;
            // 
            // playbackRangeControl
            // 
            this.playbackRangeControl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.playbackRangeControl.EndFrame = new decimal( new int[] {
            100,
            0,
            0,
            0} );
            this.playbackRangeControl.Location = new System.Drawing.Point( 4, 38 );
            this.playbackRangeControl.MaximumFrame = new decimal( new int[] {
            100,
            0,
            0,
            0} );
            this.playbackRangeControl.MinimumFrame = new decimal( new int[] {
            0,
            0,
            0,
            0} );
            this.playbackRangeControl.Name = "playbackRangeControl";
            this.playbackRangeControl.PlaybackEnd = new decimal( new int[] {
            100,
            0,
            0,
            0} );
            this.playbackRangeControl.PlaybackStart = new decimal( new int[] {
            0,
            0,
            0,
            0} );
            this.playbackRangeControl.Size = new System.Drawing.Size( 702, 25 );
            this.playbackRangeControl.StartFrame = new decimal( new int[] {
            0,
            0,
            0,
            0} );
            this.playbackRangeControl.TabIndex = 1;
            this.playbackRangeControl.EndFrameChanged += new System.EventHandler( this.playbackRangeControl_MaximumChanged );
            this.playbackRangeControl.StartFrameChanged += new System.EventHandler( this.playbackRangeControl_MinimumChanged );
            this.playbackRangeControl.PlaybackStartChanged += new System.EventHandler( this.playbackRangeControl_StartChanged );
            this.playbackRangeControl.PlaybackEndChanged += new System.EventHandler( this.playbackRangeControl_EndChanged );
            // 
            // scrubBarControl
            // 
            this.scrubBarControl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.scrubBarControl.BackColor = System.Drawing.SystemColors.ControlDark;
            this.scrubBarControl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.scrubBarControl.Location = new System.Drawing.Point( 4, 4 );
            this.scrubBarControl.Name = "scrubBarControl";
            this.scrubBarControl.PlaybackEnd = new decimal( new int[] {
            100,
            0,
            0,
            0} );
            this.scrubBarControl.PlaybackStart = new decimal( new int[] {
            0,
            0,
            0,
            0} );
            this.scrubBarControl.Size = new System.Drawing.Size( 913, 32 );
            this.scrubBarControl.TabIndex = 0;
            this.scrubBarControl.Value = new decimal( new int[] {
            0,
            0,
            0,
            65536} );
            // 
            // PlaybackControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add( this.vcrControl );
            this.Controls.Add( this.playbackRangeControl );
            this.Controls.Add( this.scrubBarControl );
            this.Name = "PlaybackControl";
            this.Size = new System.Drawing.Size( 920, 70 );
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).EndInit();
            this.ResumeLayout( false );

        }

        #endregion

        private ScrubBarControl scrubBarControl;
        private System.Windows.Forms.NumericUpDown numericUpDown2;
        private PlaybackRangeControl playbackRangeControl;
        private VCRControl vcrControl;
    }
}
