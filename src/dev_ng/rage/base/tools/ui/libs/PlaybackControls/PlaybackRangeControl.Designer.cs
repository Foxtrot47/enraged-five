namespace PlaybackControls
{
    partial class PlaybackRangeControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing )
            {
                if ( components != null )
                {
                    components.Dispose();
                }
            }

            base.Dispose( disposing );
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.startFrameNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.playbackStartNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.playbackEndNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.endFrameNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.playbackRangeSliderControl = new PlaybackControls.PlaybackRangeSliderControl();
            this.toolTip = new System.Windows.Forms.ToolTip( this.components );
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            ((System.ComponentModel.ISupportInitialize)(this.startFrameNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.playbackStartNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.playbackEndNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.endFrameNumericUpDown)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // startFrameNumericUpDown
            // 
            this.startFrameNumericUpDown.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.startFrameNumericUpDown.Location = new System.Drawing.Point( 3, 3 );
            this.startFrameNumericUpDown.Maximum = new decimal( new int[] {
            9999,
            0,
            0,
            131072} );
            this.startFrameNumericUpDown.Name = "startFrameNumericUpDown";
            this.startFrameNumericUpDown.Size = new System.Drawing.Size( 62, 20 );
            this.startFrameNumericUpDown.TabIndex = 1;
            this.startFrameNumericUpDown.ValueChanged += new System.EventHandler( this.startFrameNumericUpDown_ValueChanged );
            this.startFrameNumericUpDown.Leave += new System.EventHandler( this.startFrameNumericUpDown_Leave );
            // 
            // playbackStartNumericUpDown
            // 
            this.playbackStartNumericUpDown.DecimalPlaces = 1;
            this.playbackStartNumericUpDown.Location = new System.Drawing.Point( 71, 3 );
            this.playbackStartNumericUpDown.Maximum = new decimal( new int[] {
            9999,
            0,
            0,
            131072} );
            this.playbackStartNumericUpDown.Name = "playbackStartNumericUpDown";
            this.playbackStartNumericUpDown.Size = new System.Drawing.Size( 62, 20 );
            this.playbackStartNumericUpDown.TabIndex = 2;
            this.playbackStartNumericUpDown.ValueChanged += new System.EventHandler( this.playbackStartNumericUpDown_ValueChanged );
            this.playbackStartNumericUpDown.Leave += new System.EventHandler( this.playbackStartNumericUpDown_Leave );
            // 
            // playbackEndNumericUpDown
            // 
            this.playbackEndNumericUpDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.playbackEndNumericUpDown.DecimalPlaces = 1;
            this.playbackEndNumericUpDown.Location = new System.Drawing.Point( 481, 3 );
            this.playbackEndNumericUpDown.Minimum = new decimal( new int[] {
            1,
            0,
            0,
            0} );
            this.playbackEndNumericUpDown.Name = "playbackEndNumericUpDown";
            this.playbackEndNumericUpDown.Size = new System.Drawing.Size( 62, 20 );
            this.playbackEndNumericUpDown.TabIndex = 3;
            this.playbackEndNumericUpDown.Value = new decimal( new int[] {
            100,
            0,
            0,
            0} );
            this.playbackEndNumericUpDown.ValueChanged += new System.EventHandler( this.playbackEndNumericUpDown_ValueChanged );
            this.playbackEndNumericUpDown.Leave += new System.EventHandler( this.playbackEndNumericUpDown_Leave );
            // 
            // endFrameNumericUpDown
            // 
            this.endFrameNumericUpDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.endFrameNumericUpDown.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.endFrameNumericUpDown.Location = new System.Drawing.Point( 549, 3 );
            this.endFrameNumericUpDown.Minimum = new decimal( new int[] {
            1,
            0,
            0,
            0} );
            this.endFrameNumericUpDown.Name = "endFrameNumericUpDown";
            this.endFrameNumericUpDown.Size = new System.Drawing.Size( 62, 20 );
            this.endFrameNumericUpDown.TabIndex = 4;
            this.endFrameNumericUpDown.Value = new decimal( new int[] {
            100,
            0,
            0,
            0} );
            this.endFrameNumericUpDown.ValueChanged += new System.EventHandler( this.endFrameNumericUpDown_ValueChanged );
            this.endFrameNumericUpDown.Leave += new System.EventHandler( this.endFrameNumericUpDown_Leave );
            // 
            // playbackRangeSliderControl
            // 
            this.playbackRangeSliderControl.BackColor = System.Drawing.SystemColors.ControlDark;
            this.playbackRangeSliderControl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.playbackRangeSliderControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.playbackRangeSliderControl.EndFrame = new decimal( new int[] {
            1000,
            0,
            0,
            65536} );
            this.playbackRangeSliderControl.Location = new System.Drawing.Point( 139, 3 );
            this.playbackRangeSliderControl.Name = "playbackRangeSliderControl";
            this.playbackRangeSliderControl.PlaybackEnd = new decimal( new int[] {
            1000,
            0,
            0,
            65536} );
            this.playbackRangeSliderControl.PlaybackStart = new decimal( new int[] {
            0,
            0,
            0,
            65536} );
            this.playbackRangeSliderControl.Size = new System.Drawing.Size( 336, 19 );
            this.playbackRangeSliderControl.StartFrame = new decimal( new int[] {
            0,
            0,
            0,
            65536} );
            this.playbackRangeSliderControl.TabIndex = 5;
            this.playbackRangeSliderControl.PlaybackStartChanged += new System.EventHandler( this.playbackRangeSliderControl_PlaybackStartChanged );
            this.playbackRangeSliderControl.PlaybackEndChanged += new System.EventHandler( this.playbackRangeSliderControl_PlaybackEndChanged );
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 5;
            this.tableLayoutPanel1.ColumnStyles.Add( new System.Windows.Forms.ColumnStyle( System.Windows.Forms.SizeType.Absolute, 68F ) );
            this.tableLayoutPanel1.ColumnStyles.Add( new System.Windows.Forms.ColumnStyle( System.Windows.Forms.SizeType.Absolute, 68F ) );
            this.tableLayoutPanel1.ColumnStyles.Add( new System.Windows.Forms.ColumnStyle( System.Windows.Forms.SizeType.Percent, 100F ) );
            this.tableLayoutPanel1.ColumnStyles.Add( new System.Windows.Forms.ColumnStyle( System.Windows.Forms.SizeType.Absolute, 68F ) );
            this.tableLayoutPanel1.ColumnStyles.Add( new System.Windows.Forms.ColumnStyle( System.Windows.Forms.SizeType.Absolute, 68F ) );
            this.tableLayoutPanel1.Controls.Add( this.startFrameNumericUpDown, 0, 0 );
            this.tableLayoutPanel1.Controls.Add( this.endFrameNumericUpDown, 4, 0 );
            this.tableLayoutPanel1.Controls.Add( this.playbackRangeSliderControl, 2, 0 );
            this.tableLayoutPanel1.Controls.Add( this.playbackEndNumericUpDown, 3, 0 );
            this.tableLayoutPanel1.Controls.Add( this.playbackStartNumericUpDown, 1, 0 );
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point( 0, 0 );
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add( new System.Windows.Forms.RowStyle( System.Windows.Forms.SizeType.Percent, 100F ) );
            this.tableLayoutPanel1.Size = new System.Drawing.Size( 614, 25 );
            this.tableLayoutPanel1.TabIndex = 6;
            // 
            // PlaybackRangeControl
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.Controls.Add( this.tableLayoutPanel1 );
            this.Name = "PlaybackRangeControl";
            this.Size = new System.Drawing.Size( 614, 25 );
            ((System.ComponentModel.ISupportInitialize)(this.startFrameNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.playbackStartNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.playbackEndNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.endFrameNumericUpDown)).EndInit();
            this.tableLayoutPanel1.ResumeLayout( false );
            this.ResumeLayout( false );

        }

        #endregion

        private System.Windows.Forms.NumericUpDown startFrameNumericUpDown;
        private System.Windows.Forms.NumericUpDown playbackStartNumericUpDown;
        private System.Windows.Forms.NumericUpDown playbackEndNumericUpDown;
        private System.Windows.Forms.NumericUpDown endFrameNumericUpDown;
        private PlaybackRangeSliderControl playbackRangeSliderControl;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;

    }
}
