namespace PlaybackControls
{
    partial class VCRControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && (components != null) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.rewindOrGoToStartButton = new System.Windows.Forms.Button();
            this.goToPreviousOrStepBackwardButton = new System.Windows.Forms.Button();
            this.playBackwardsButton = new System.Windows.Forms.Button();
            this.pauseButton = new System.Windows.Forms.Button();
            this.playForwardsButton = new System.Windows.Forms.Button();
            this.goToNextOrStepForwardButton = new System.Windows.Forms.Button();
            this.fastForwardOrGoToEndButton = new System.Windows.Forms.Button();
            this.toolTip = new System.Windows.Forms.ToolTip( this.components );
            this.SuspendLayout();
            // 
            // rewindOrGoToStartButton
            // 
            this.rewindOrGoToStartButton.Image = global::PlaybackControls.Properties.Resources.rewind;
            this.rewindOrGoToStartButton.Location = new System.Drawing.Point( 0, 0 );
            this.rewindOrGoToStartButton.Name = "rewindOrGoToStartButton";
            this.rewindOrGoToStartButton.Size = new System.Drawing.Size( 24, 24 );
            this.rewindOrGoToStartButton.TabIndex = 0;
            this.rewindOrGoToStartButton.UseVisualStyleBackColor = true;
            // 
            // goToPreviousOrStepBackwardButton
            // 
            this.goToPreviousOrStepBackwardButton.Image = global::PlaybackControls.Properties.Resources.stepbackwards;
            this.goToPreviousOrStepBackwardButton.Location = new System.Drawing.Point( 30, 0 );
            this.goToPreviousOrStepBackwardButton.Name = "goToPreviousOrStepBackwardButton";
            this.goToPreviousOrStepBackwardButton.Size = new System.Drawing.Size( 24, 24 );
            this.goToPreviousOrStepBackwardButton.TabIndex = 1;
            this.goToPreviousOrStepBackwardButton.UseVisualStyleBackColor = true;
            // 
            // playBackwardsButton
            // 
            this.playBackwardsButton.Image = global::PlaybackControls.Properties.Resources.playbackwards;
            this.playBackwardsButton.Location = new System.Drawing.Point( 60, 0 );
            this.playBackwardsButton.Name = "playBackwardsButton";
            this.playBackwardsButton.Size = new System.Drawing.Size( 24, 24 );
            this.playBackwardsButton.TabIndex = 2;
            this.playBackwardsButton.UseVisualStyleBackColor = true;
            // 
            // pauseButton
            // 
            this.pauseButton.Image = global::PlaybackControls.Properties.Resources.pause;
            this.pauseButton.Location = new System.Drawing.Point( 90, 0 );
            this.pauseButton.Name = "pauseButton";
            this.pauseButton.Size = new System.Drawing.Size( 24, 24 );
            this.pauseButton.TabIndex = 3;
            this.pauseButton.UseVisualStyleBackColor = true;
            // 
            // playForwardsButton
            // 
            this.playForwardsButton.Image = global::PlaybackControls.Properties.Resources.playforwards;
            this.playForwardsButton.Location = new System.Drawing.Point( 120, 0 );
            this.playForwardsButton.Name = "playForwardsButton";
            this.playForwardsButton.Size = new System.Drawing.Size( 24, 24 );
            this.playForwardsButton.TabIndex = 4;
            this.playForwardsButton.UseVisualStyleBackColor = true;
            // 
            // goToNextOrStepForwardButton
            // 
            this.goToNextOrStepForwardButton.Image = global::PlaybackControls.Properties.Resources.stepforwards;
            this.goToNextOrStepForwardButton.Location = new System.Drawing.Point( 150, 0 );
            this.goToNextOrStepForwardButton.Name = "goToNextOrStepForwardButton";
            this.goToNextOrStepForwardButton.Size = new System.Drawing.Size( 24, 24 );
            this.goToNextOrStepForwardButton.TabIndex = 5;
            this.goToNextOrStepForwardButton.UseVisualStyleBackColor = true;
            // 
            // fastForwardOrGoToEndButton
            // 
            this.fastForwardOrGoToEndButton.Image = global::PlaybackControls.Properties.Resources.fastforward;
            this.fastForwardOrGoToEndButton.Location = new System.Drawing.Point( 180, 0 );
            this.fastForwardOrGoToEndButton.Name = "fastForwardOrGoToEndButton";
            this.fastForwardOrGoToEndButton.Size = new System.Drawing.Size( 24, 24 );
            this.fastForwardOrGoToEndButton.TabIndex = 6;
            this.fastForwardOrGoToEndButton.UseVisualStyleBackColor = true;
            // 
            // VCRControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add( this.fastForwardOrGoToEndButton );
            this.Controls.Add( this.goToNextOrStepForwardButton );
            this.Controls.Add( this.playForwardsButton );
            this.Controls.Add( this.pauseButton );
            this.Controls.Add( this.playBackwardsButton );
            this.Controls.Add( this.goToPreviousOrStepBackwardButton );
            this.Controls.Add( this.rewindOrGoToStartButton );
            this.Name = "VCRControl";
            this.Size = new System.Drawing.Size( 205, 24 );
            this.ResumeLayout( false );

        }

        #endregion

        private System.Windows.Forms.Button rewindOrGoToStartButton;
        private System.Windows.Forms.Button goToPreviousOrStepBackwardButton;
        private System.Windows.Forms.Button playBackwardsButton;
        private System.Windows.Forms.Button pauseButton;
        private System.Windows.Forms.Button playForwardsButton;
        private System.Windows.Forms.Button goToNextOrStepForwardButton;
        private System.Windows.Forms.Button fastForwardOrGoToEndButton;
        private System.Windows.Forms.ToolTip toolTip;
    }
}
