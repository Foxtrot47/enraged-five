using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace PlaybackControls
{
    public partial class PlaybackRangeControl : UserControl
    {
        public PlaybackRangeControl()
        {
            InitializeComponent();

            InitializeAdditionalComponents();
        }

        private void InitializeAdditionalComponents()
        {
            this.toolTip.SetToolTip( this.startFrameNumericUpDown, "Start Frame" );
            this.toolTip.SetToolTip( this.playbackStartNumericUpDown, "Playback Start Frame" );
            this.toolTip.SetToolTip( this.playbackEndNumericUpDown, "Playback End Frame" );
            this.toolTip.SetToolTip( this.endFrameNumericUpDown, "End Frame" );
        }

        #region Constants
        private const int c_markerPad = 8;
        #endregion

        #region Variables
        private bool m_dispatchEvents = true;
        #endregion

        #region Properties
        /// <summary>
        /// When setting MinimumFrame, always set MaximumFrame first.  Ensures that MinimumFrame is at least 1 less than MaximumFrame.
        /// Ensures that MinimumFrame is not less than 0.
        /// </summary>
        [DefaultValue( 0 )]
        public decimal MinimumFrame
        {
            get
            {
                return this.startFrameNumericUpDown.Minimum;
            }
            set
            {
                if ( this.startFrameNumericUpDown.Minimum != value )
                {
                    if ( value >= this.MaximumFrame )
                    {
                        this.startFrameNumericUpDown.Minimum = this.MaximumFrame - 1;
                    }
                    else if ( value >= 0.0M )
                    {
                        this.startFrameNumericUpDown.Minimum = value;
                    }
                    else
                    {
                        this.startFrameNumericUpDown.Minimum = 0.0M;
                    }

                    this.playbackStartNumericUpDown.Minimum = this.MinimumFrame;
                }
            }
        }
        
        /// <summary>
        /// When setting MaximumFrame, call this before MinimumFrame.  Ensures that MaxiumumFrame is at least 1 more than MinimumFrame.
        /// </summary>
        [DefaultValue( 10000 )]
        public decimal MaximumFrame
        {
            get
            {
                return this.endFrameNumericUpDown.Maximum;
            }
            set
            {
                if ( this.endFrameNumericUpDown.Maximum != value )
                {
                    if ( value <= this.MinimumFrame )
                    {
                        this.endFrameNumericUpDown.Maximum = this.MinimumFrame + 1;
                    }
                    else
                    {
                        this.endFrameNumericUpDown.Maximum = value;
                    }

                    this.playbackEndNumericUpDown.Maximum = this.endFrameNumericUpDown.Maximum;
                }
            }
        }

        [DefaultValue( true )]
        public bool StartFrameEnabled
        {
            get
            {
                return !this.startFrameNumericUpDown.ReadOnly;
            }
            set
            {
                this.startFrameNumericUpDown.ReadOnly = !value;
                this.startFrameNumericUpDown.Enabled = value;
            }
        }

        [DefaultValue( true )]
        public bool EndFrameEnabled
        {
            get
            {
                return !this.endFrameNumericUpDown.ReadOnly;
            }
            set
            {
                this.endFrameNumericUpDown.ReadOnly = !value;
                this.endFrameNumericUpDown.Enabled = value;
            }
        }

        /// <summary>
        /// When setting the StartFrame time, always set EndFrame first.  Ensures that StartFrame is at least 0.01 less than EndFrame, 
        /// but never less than 0. Ensures that PlaybackStart is never less than StartFrame (will dispatch PlaybackStartChanged if that is 
        /// adjusted).  Ensures that PlaybackEnd is at least 1% of the range above PlaybackStart (will dispatch PlaybackEndChanged if that is adjusted).
        /// </summary>        
        [DefaultValue( 0 )]
        public decimal StartFrame
        {
            get
            {
                return this.startFrameNumericUpDown.Value;
            }
            set
            {
                if ( this.StartFrame != value )
                {
                    SetStartFrame( value );
                }
            }
        }

        /// <summary>
        /// When setting the EndFrame time, call this before StartFrame.  Ensures that EndFrame is at least 0.01 greater than StartFrame.
        /// Ensures that PlaybackEnd is never greater than EndFrame (will dispatch PlaybackEndChanged if that is adjusted).  Ensures that
        /// PlaybackStart is at least 1% of the range less than PlaybackEnd (will dispatch PlaybackStartChanged if that is adjusted).
        /// </summary>
        [DefaultValue( 100 )]
        public decimal EndFrame
        {
            get
            {
                return this.endFrameNumericUpDown.Value;
            }
            set
            {
                if ( this.EndFrame != value )
                {
                    SetEndFrame( value );
                }
            }
        }

        /// <summary>
        /// When setting PlaybackStart, always set PlaybackEnd first.  Ensures that PlaybackStart is at least 1% of the range less than PlaybackEnd.  Ensures that
        /// PlaybackStart is not less than StartFrame.
        /// </summary>
        [DefaultValue( 0 )]
        public decimal PlaybackStart
        {
            get
            {
                return this.playbackStartNumericUpDown.Value; 
            }
            set
            {
                if ( this.PlaybackStart != value )
                {
                    SetPlaybackStart( value );
                }
            }
        }

        /// <summary>
        /// When setting PlaybackEnd, always set before PlaybackStart.  Ensures that PlaybackEnd is at least 1% of the range greater than PlaybackStart.  Ensures that
        /// PlaybackEnd is not greater than StartFrame.
        /// </summary>
        [DefaultValue( 100 )]
        public decimal PlaybackEnd
        {
            get
            {
                return this.playbackEndNumericUpDown.Value;
            }
            set
            {
                if ( this.playbackEndNumericUpDown.Value != value )
                {
                    SetPlaybackEnd( value );
                }
            }
        }
        #endregion

        #region Events
        /// <summary>
        /// Dispatched when StartFrame is changed by the User.
        /// </summary>
        public event EventHandler StartFrameChanged;

        /// <summary>
        /// Dispatched when EndFrame is changed by the User.
        /// </summary>
        public event EventHandler EndFrameChanged;

        /// <summary>
        /// Dispatched when PlaybackStart is changed by the User or when EndFrame or StartFrame clamps the value.
        /// </summary>
        public event EventHandler PlaybackStartChanged;

        /// <summary>
        /// Dispatched when PlaybackEnd is changed by the User or when EndFrame or StartFrame clamps the value.
        /// </summary>
        public event EventHandler PlaybackEndChanged;
        #endregion

        #region Event Dispatchers
        protected void OnStartFrameChanged()
        {
            if ( this.StartFrameChanged != null )
            {
                this.StartFrameChanged( this, EventArgs.Empty );
            }
        }

        protected void OnEndFrameChanged()
        {
            if ( this.EndFrameChanged != null )
            {
                this.EndFrameChanged( this, EventArgs.Empty );
            }
        }

        protected void OnPlaybackStartChanged()
        {
            if ( this.PlaybackStartChanged != null )
            {
                this.PlaybackStartChanged( this, EventArgs.Empty );
            }
        }

        protected void OnPlaybackEndChanged()
        {
            if ( this.PlaybackEndChanged != null )
            {
                this.PlaybackEndChanged( this, EventArgs.Empty );
            }
        }
        #endregion

        #region Event Handlers
        private void startFrameNumericUpDown_Leave( object sender, EventArgs e )
        {
            if ( m_dispatchEvents )
            {
                if ( this.startFrameNumericUpDown.Value != this.playbackRangeSliderControl.StartFrame )
                {
                    SetStartFrame( this.startFrameNumericUpDown.Value );
                }
         
                OnStartFrameChanged();
            }
        }

        private void startFrameNumericUpDown_ValueChanged( object sender, EventArgs e )
        {
            if ( m_dispatchEvents )
            {
                SetStartFrame( this.startFrameNumericUpDown.Value );
               
                OnStartFrameChanged();
            }
        }

        private void playbackStartNumericUpDown_Leave( object sender, EventArgs e )
        {
            if ( m_dispatchEvents )
            {
                if ( this.playbackStartNumericUpDown.Value != this.playbackRangeSliderControl.PlaybackStart )
                {
                    SetPlaybackStart( this.playbackStartNumericUpDown.Value );
                }

                OnPlaybackStartChanged();
            }
        }

        private void playbackStartNumericUpDown_ValueChanged( object sender, EventArgs e )
        {
            if ( m_dispatchEvents )
            {
                SetPlaybackStart( this.playbackStartNumericUpDown.Value );
             
                OnPlaybackStartChanged();
            }
        }

        private void playbackEndNumericUpDown_Leave( object sender, EventArgs e )
        {
            if ( m_dispatchEvents )
            {
                if ( this.playbackEndNumericUpDown.Value != this.playbackRangeSliderControl.PlaybackEnd )
                {
                    SetPlaybackEnd( this.playbackEndNumericUpDown.Value );
                }
                
                OnPlaybackEndChanged();
            }
        }

        private void playbackEndNumericUpDown_ValueChanged( object sender, EventArgs e )
        {
            if ( m_dispatchEvents )
            {
                SetPlaybackEnd( this.playbackEndNumericUpDown.Value );
                
                OnPlaybackEndChanged();
            }
        }

        private void endFrameNumericUpDown_Leave( object sender, EventArgs e )
        {
            if ( m_dispatchEvents )
            {
                if ( this.endFrameNumericUpDown.Value != this.playbackRangeSliderControl.EndFrame )
                {
                    SetEndFrame( this.endFrameNumericUpDown.Value );
                }
                
                OnEndFrameChanged();
            }
        }

        private void endFrameNumericUpDown_ValueChanged( object sender, EventArgs e )
        {
            if ( m_dispatchEvents )
            {
                SetEndFrame( this.endFrameNumericUpDown.Value );

                OnEndFrameChanged();
            }
        }

        private void playbackRangeSliderControl_PlaybackEndChanged( object sender, EventArgs e )
        {
            this.PlaybackEnd = this.playbackRangeSliderControl.PlaybackEnd;
            
            if ( m_dispatchEvents )
            {
                OnPlaybackEndChanged();
            }
        }

        private void playbackRangeSliderControl_PlaybackStartChanged( object sender, EventArgs e )
        {
            this.PlaybackStart = this.playbackRangeSliderControl.PlaybackStart;
            
            if ( m_dispatchEvents )
            {
                OnPlaybackStartChanged();
            }
        }
        #endregion

        #region Private Functions
        private void SetStartFrame( decimal val )
        {
            m_dispatchEvents = false;

            // The PlaybackRangeSliderControl will do all the clamping for us
            this.playbackRangeSliderControl.StartFrame = val;

            this.startFrameNumericUpDown.Minimum = this.playbackRangeSliderControl.StartFrame;
            this.startFrameNumericUpDown.Value = this.playbackRangeSliderControl.StartFrame;

            this.endFrameNumericUpDown.Minimum = this.StartFrame + ((this.EndFrame - this.StartFrame) * 0.01M);

            // See if PlaybackEnd was clamped
            if ( this.PlaybackEnd != this.playbackRangeSliderControl.PlaybackEnd )
            {
                this.PlaybackEnd = this.playbackRangeSliderControl.PlaybackEnd;

                OnPlaybackEndChanged();
            }

            // See if PlaybackStart was clamped
            if ( this.PlaybackStart != this.playbackRangeSliderControl.PlaybackStart )
            {
                this.PlaybackStart = this.playbackRangeSliderControl.PlaybackStart;

                OnPlaybackStartChanged();
            }

            m_dispatchEvents = true;
        }

        private void SetEndFrame( decimal val )
        {
            m_dispatchEvents = false;

            // The PlaybackRangeSliderControl will do all the clamping for us
            this.playbackRangeSliderControl.EndFrame = val;

            this.endFrameNumericUpDown.Maximum = this.playbackRangeSliderControl.EndFrame;
            this.endFrameNumericUpDown.Value = this.playbackRangeSliderControl.EndFrame;

            this.startFrameNumericUpDown.Maximum = this.EndFrame - ((this.EndFrame - this.StartFrame) * 0.01M);

            // See if PlaybackEnd was clamped
            if ( this.PlaybackEnd != this.playbackRangeSliderControl.PlaybackEnd )
            {
                this.PlaybackEnd = this.playbackRangeSliderControl.PlaybackEnd;

                OnPlaybackEndChanged();
            }

            // See if PlaybackStart was clamped
            if ( this.PlaybackStart != this.playbackRangeSliderControl.PlaybackStart )
            {
                this.PlaybackStart = this.playbackRangeSliderControl.PlaybackStart;

                OnPlaybackStartChanged();
            }

            m_dispatchEvents = true;
        }

        private void SetPlaybackStart( decimal val )
        {
            m_dispatchEvents = false;

            // The PlaybackRangeSliderControl will do all the clamping for us
            this.playbackRangeSliderControl.PlaybackStart = val;

			this.playbackStartNumericUpDown.Minimum = this.startFrameNumericUpDown.Minimum;
            this.playbackStartNumericUpDown.Value = this.playbackRangeSliderControl.PlaybackStart;

            this.playbackEndNumericUpDown.Minimum = this.PlaybackStart + ((this.EndFrame - this.StartFrame) * 0.01M);

            m_dispatchEvents = true;
        }

        private void SetPlaybackEnd( decimal val )
        {
            m_dispatchEvents = false;

            // The PlaybackRangeSliderControl will do all the clamping for us
            this.playbackRangeSliderControl.PlaybackEnd = val;

			this.playbackEndNumericUpDown.Maximum = this.endFrameNumericUpDown.Maximum;
			this.playbackEndNumericUpDown.Value = this.playbackRangeSliderControl.PlaybackEnd;

			this.playbackStartNumericUpDown.Maximum = this.PlaybackEnd - ((this.EndFrame - this.StartFrame) * 0.01M);
			
			this.playbackEndNumericUpDown.Value = this.playbackRangeSliderControl.PlaybackEnd;

            m_dispatchEvents = true;
        }
        #endregion
    }
}
