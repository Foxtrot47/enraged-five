using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace PlaybackControls
{
    public partial class VCRControl : UserControl
    {
        public VCRControl()
        {
            InitializeComponent();

            InitializeAdditionalComponents();
        }

        private void InitializeAdditionalComponents()
        {
            this.toolTip.SetToolTip( this.playBackwardsButton, "Play Backwards" );
            this.toolTip.SetToolTip( this.pauseButton, "Pause" );
            this.toolTip.SetToolTip( this.playForwardsButton, "Play Forwards" );

            SetButtonStyle( this.Style );
        }

        #region Enums
        public enum ButtonStyle
        {
            VCR,
            Animation
        }
        #endregion

        #region Variables
        private ButtonStyle m_buttonStyle;
        #endregion

        #region Properties
        /// <summary>
        /// Sets the style of the buttons.
        /// For VCR, the buttons are Rewind, Previous, Play Backwards, Pause, Play Forwards, Next, and Fast Forward.
        /// For Animation, the buttons are Go To Start, Step Backward, Play Backwards, Pause, Play Forwards, Step Forward, and Go To End.
        /// </summary>
        [DefaultValue( ButtonStyle.VCR )]
        public ButtonStyle Style
        {
            get
            {
                return m_buttonStyle;
            }
            set
            {
                if ( m_buttonStyle != value )
                {
                    SetButtonStyle( value );
                }
            }
        }

        [DefaultValue( true )]
        public bool CanRewindOrGoToStart
        {
            get
            {
                return this.rewindOrGoToStartButton.Enabled;
            }
            set
            {
                this.rewindOrGoToStartButton.Enabled = value;
            }
        }

        [DefaultValue( true )]
        public bool CanGoToPreviousOrStepBackward
        {
            get
            {
                return this.goToPreviousOrStepBackwardButton.Enabled;
            }
            set
            {
                this.goToPreviousOrStepBackwardButton.Enabled = value;
            }
        }

        [DefaultValue( true )]
        public bool CanPlayBackwards
        {
            get
            {
                return this.playBackwardsButton.Enabled;
            }
            set
            {
                this.playBackwardsButton.Enabled = value;
            }
        }

        [DefaultValue( true )]
        public bool CanPause
        {
            get
            {
                return this.pauseButton.Enabled;
            }
            set
            {
                this.pauseButton.Enabled = value;
            }
        }

        [DefaultValue( true )]
        public bool CanPlayForwards
        {
            get
            {
                return this.playForwardsButton.Enabled;
            }
            set
            {
                this.playForwardsButton.Enabled = value;
            }
        }

        [DefaultValue( true )]
        public bool CanGoToNextOrStepForward
        {
            get
            {
                return this.goToNextOrStepForwardButton.Enabled;
            }
            set
            {
                this.goToNextOrStepForwardButton.Enabled = value;
            }
        }

        [DefaultValue( true )]
        public bool CanFastForwardOrGoToEnd
        {
            get
            {
                return this.fastForwardOrGoToEndButton.Enabled;
            }
            set
            {
                this.fastForwardOrGoToEndButton.Enabled = value;
            }
        }
        #endregion

        #region Events
        public event EventHandler RewindOrGoToStartButtonClick
        {
            add
            {
                this.rewindOrGoToStartButton.Click += value;
            }
            remove
            {
                this.rewindOrGoToStartButton.Click -= value;
            }
        }

        public event EventHandler GoToPreviousOrStepBackwardButtonClick
        {
            add
            {
                this.goToPreviousOrStepBackwardButton.Click += value;
            }
            remove
            {
                this.goToPreviousOrStepBackwardButton.Click -= value;
            }
        }

        public event EventHandler PlayBackwardsButtonClick
        {
            add
            {
                this.playBackwardsButton.Click += value;
            }
            remove
            {
                this.playBackwardsButton.Click -= value;
            }
        }

        public event EventHandler PauseButtonClick
        {
            add
            {
                this.pauseButton.Click += value;
            }
            remove
            {
                this.pauseButton.Click -= value;
            }
        }

        public event EventHandler PlayForwardsButtonClick
        {
            add
            {
                this.playForwardsButton.Click += value;
            }
            remove
            {
                this.playForwardsButton.Click -= value;
            }
        }

        public event EventHandler GoToNextOrStepForwardButtonClick
        {
            add
            {
                this.goToNextOrStepForwardButton.Click += value;
            }
            remove
            {
                this.goToNextOrStepForwardButton.Click -= value;
            }
        }

        public event EventHandler FastForwardOrGoToEndButtonClick
        {
            add
            {
                this.fastForwardOrGoToEndButton.Click += value;
            }
            remove
            {
                this.fastForwardOrGoToEndButton.Click -= value;
            }
        }
        #endregion

        #region Private Functions
        private void SetButtonStyle( ButtonStyle style )
        {
            m_buttonStyle = style;

            if ( this.Style == ButtonStyle.VCR )
            {
                this.toolTip.SetToolTip( this.rewindOrGoToStartButton, "Rewind" );
                this.rewindOrGoToStartButton.Image = global::PlaybackControls.Properties.Resources.rewind;

                this.toolTip.SetToolTip( this.goToPreviousOrStepBackwardButton, "Previous" );
                this.toolTip.SetToolTip( this.goToNextOrStepForwardButton, "Next" );

                this.toolTip.SetToolTip( this.fastForwardOrGoToEndButton, "Fast Forward" );
                this.fastForwardOrGoToEndButton.Image = global::PlaybackControls.Properties.Resources.fastforward;
            }
            else
            {
                this.toolTip.SetToolTip( this.rewindOrGoToStartButton, "Go To Start" );
                this.rewindOrGoToStartButton.Image = global::PlaybackControls.Properties.Resources.gotostart;

                this.toolTip.SetToolTip( this.goToPreviousOrStepBackwardButton, "Step Backward" );
                this.toolTip.SetToolTip( this.goToNextOrStepForwardButton, "Step Forward" );

                this.toolTip.SetToolTip( this.fastForwardOrGoToEndButton, "Go To End" );
                this.fastForwardOrGoToEndButton.Image = global::PlaybackControls.Properties.Resources.gotoend;
            }
        }
        #endregion
    }
}
