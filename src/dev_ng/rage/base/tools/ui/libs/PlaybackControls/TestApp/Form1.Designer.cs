namespace TestApp
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && (components != null) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.playbackControl = new PlaybackControls.PlaybackControl();
            this.splitContainer = new System.Windows.Forms.SplitContainer();
            this.richTextBox = new System.Windows.Forms.RichTextBox();
            this.propertyGrid = new System.Windows.Forms.PropertyGrid();
            this.splitContainer.Panel1.SuspendLayout();
            this.splitContainer.Panel2.SuspendLayout();
            this.splitContainer.SuspendLayout();
            this.SuspendLayout();
            // 
            // playbackControl
            // 
            this.playbackControl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.playbackControl.EndFrame = new decimal( new int[] {
            100,
            0,
            0,
            0} );
            this.playbackControl.Location = new System.Drawing.Point( 12, 12 );
            this.playbackControl.MaximumFrame = new decimal( new int[] {
            100,
            0,
            0,
            0} );
            this.playbackControl.MinimumFrame = new decimal( new int[] {
            0,
            0,
            0,
            0} );
            this.playbackControl.Name = "playbackControl";
            this.playbackControl.PlaybackEnd = new decimal( new int[] {
            100,
            0,
            0,
            0} );
            this.playbackControl.PlaybackStart = new decimal( new int[] {
            0,
            0,
            0,
            0} );
            this.playbackControl.Size = new System.Drawing.Size( 1211, 70 );
            this.playbackControl.StartFrame = new decimal( new int[] {
            0,
            0,
            0,
            0} );
            this.playbackControl.TabIndex = 0;
            this.playbackControl.Value = new decimal( new int[] {
            0,
            0,
            0,
            65536} );
            this.playbackControl.StartFrameChanged += new System.EventHandler( this.playbackControl_StartFrameChanged );
            this.playbackControl.RewindOrGoToStartButtonClick += new System.EventHandler( this.playbackControl_RewindOrGoToStartButtonClick );
            this.playbackControl.PlaybackStartChanged += new System.EventHandler( this.playbackControl_PlaybackStartChanged );
            this.playbackControl.PlaybackEndChanged += new System.EventHandler( this.playbackControl_PlaybackEndChanged );
            this.playbackControl.EndFrameChanged += new System.EventHandler( this.playbackControl_EndFrameChanged );
            this.playbackControl.GoToPreviousOrStepBackwardButtonClick += new System.EventHandler( this.playbackControl_GoToPreviousOrStepBackwardButtonClick );
            this.playbackControl.ValueChanged += new System.EventHandler( this.playbackControl_ValueChanged );
            this.playbackControl.PlayForwardsButtonClick += new System.EventHandler( this.playbackControl_PlayForwardsButtonClick );
            this.playbackControl.GoToNextOrStepForwardButtonClick += new System.EventHandler( this.playbackControl_GoToNextOrStepForwardButtonClick );
            this.playbackControl.PlayBackwardsButtonClick += new System.EventHandler( this.playbackControl_PlayBackwardsButtonClick );
            this.playbackControl.FastForwardOrGoToEndButtonClick += new System.EventHandler( this.playbackControl_FastForwardOrGoToEndButtonClick );
            this.playbackControl.PauseButtonClick += new System.EventHandler( this.playbackControl_PauseButtonClick );
            // 
            // splitContainer
            // 
            this.splitContainer.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer.Location = new System.Drawing.Point( 12, 88 );
            this.splitContainer.Name = "splitContainer";
            // 
            // splitContainer.Panel1
            // 
            this.splitContainer.Panel1.Controls.Add( this.richTextBox );
            // 
            // splitContainer.Panel2
            // 
            this.splitContainer.Panel2.Controls.Add( this.propertyGrid );
            this.splitContainer.Size = new System.Drawing.Size( 1211, 265 );
            this.splitContainer.SplitterDistance = 706;
            this.splitContainer.TabIndex = 1;
            // 
            // richTextBox
            // 
            this.richTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richTextBox.Location = new System.Drawing.Point( 0, 0 );
            this.richTextBox.Name = "richTextBox";
            this.richTextBox.Size = new System.Drawing.Size( 706, 265 );
            this.richTextBox.TabIndex = 0;
            this.richTextBox.Text = "";
            // 
            // propertyGrid
            // 
            this.propertyGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.propertyGrid.Location = new System.Drawing.Point( 0, 0 );
            this.propertyGrid.Name = "propertyGrid";
            this.propertyGrid.SelectedObject = this.playbackControl;
            this.propertyGrid.Size = new System.Drawing.Size( 501, 265 );
            this.propertyGrid.TabIndex = 0;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size( 1235, 365 );
            this.Controls.Add( this.splitContainer );
            this.Controls.Add( this.playbackControl );
            this.Name = "Form1";
            this.Text = "Form1";
            this.splitContainer.Panel1.ResumeLayout( false );
            this.splitContainer.Panel2.ResumeLayout( false );
            this.splitContainer.ResumeLayout( false );
            this.ResumeLayout( false );

        }

        #endregion

        private PlaybackControls.PlaybackControl playbackControl;
        private System.Windows.Forms.SplitContainer splitContainer;
        private System.Windows.Forms.RichTextBox richTextBox;
        private System.Windows.Forms.PropertyGrid propertyGrid;
    }
}

