﻿using System;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media.Imaging;

namespace RSG.PropertyGrid.Converters
{
    /// <summary>
    /// 
    /// </summary>
    public class ValueSourceToImagePathConverter : IValueConverter
    {
        #region IValueConverter Members

        public Object Convert(Object value, Type targetType, Object parameter, System.Globalization.CultureInfo culture)
        {
            BaseValueSource bvs = (BaseValueSource)value;

            string uriPrefix = "/RSG.PropertyGrid;component/Resource/Images/";
            string imageName = "AdvancedProperties";

            switch (bvs)
            {
                case BaseValueSource.Inherited:
                case BaseValueSource.DefaultStyle:
                case BaseValueSource.ImplicitStyleReference:
                    imageName = "Inheritance";
                    break;
                case BaseValueSource.DefaultStyleTrigger:
                    break;
                case BaseValueSource.Style:
                    imageName = "Style";
                    break;

                case BaseValueSource.Local:
                    imageName = "Local";
                    break;
            }


            return new BitmapImage(new Uri(String.Format("{0}{1}.png", uriPrefix, imageName), UriKind.RelativeOrAbsolute));
        }

        public Object ConvertBack(Object value, Type targetType, Object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    } // ValueSourceToImagePathConverter
} // RSG.PropertyGrid.Converters
