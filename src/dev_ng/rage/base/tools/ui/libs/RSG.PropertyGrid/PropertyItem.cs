﻿using System;
using System.Linq;
using System.ComponentModel;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Markup.Primitives;
using RSG.PropertyGrid.Commands;

namespace RSG.PropertyGrid
{
    /// <summary>
    /// 
    /// </summary>
    public class PropertyItem : Control
    {
        #region Members

        private PropertyGrid m_propertyGrid;
        private DependencyPropertyDescriptor m_dpDescriptor;
        private MarkupObject m_markupObject;
        private PropertyDescriptor m_propertyDescriptor;
        private Object m_instance;
        private List<Object> m_instances;

        #endregion // Members

        #region Properties

        #region Dependency Properties

        #region Category

        public static readonly DependencyProperty CategoryProperty = DependencyProperty.Register(
            "Category", 
            typeof(String), 
            typeof(PropertyItem), 
            new UIPropertyMetadata(String.Empty));
        public String Category
        {
            get { return (String)GetValue(CategoryProperty); }
            set { SetValue(CategoryProperty, value); }
        }

        #endregion //Category

        #region Editor

        public static readonly DependencyProperty EditorProperty = DependencyProperty.Register(
            "Editor",
            typeof(FrameworkElement), 
            typeof(PropertyItem), 
            new UIPropertyMetadata(null));
        public FrameworkElement Editor
        {
            get { return (FrameworkElement)GetValue(EditorProperty); }
            set { SetValue(EditorProperty, value); }
        }

        #endregion //Editor

        #region IsSelected

        public static readonly DependencyProperty IsSelectedProperty = DependencyProperty.Register(
            "IsSelected", 
            typeof(Boolean), 
            typeof(PropertyItem), 
            new UIPropertyMetadata(false, OnIsSelectedChanged));
        public Boolean IsSelected
        {
            get { return (Boolean)GetValue(IsSelectedProperty); }
            set { SetValue(IsSelectedProperty, value); }
        }

        private static void OnIsSelectedChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            PropertyItem propertyItem = o as PropertyItem;
            if (propertyItem != null)
                propertyItem.OnIsSelectedChanged((Boolean)e.OldValue, (Boolean)e.NewValue);
        }

        protected virtual void OnIsSelectedChanged(Boolean oldValue, Boolean newValue)
        {
            if (newValue)
                PropertyGrid.SelectedProperty = this;
        }

        #endregion //IsSelected

        #region Value

        private Boolean m_initialSet = false;
        private Boolean m_resolving = false;
        public static readonly DependencyProperty ValueProperty = DependencyProperty.Register(
            "Value",
            typeof(Object),
            typeof(PropertyItem),
            new UIPropertyMetadata(null, OnValueChanged));
        public Object Value
        {
            get { return (Object)GetValue(ValueProperty); }
            set { SetValue(ValueProperty, value); }
        }

        private static void OnValueChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            PropertyItem propertyItem = o as PropertyItem;
            if (propertyItem.m_resolving == true)
                return;


            if (propertyItem != null)
                propertyItem.OnValueChanged((Object)e.OldValue, (Object)e.NewValue);
        }

        protected virtual void OnValueChanged(Object oldValue, Object newValue)
        {
            if (m_initialSet == false)
            {
                foreach (Object instance in this.PropertyGrid.ObjectStack)
                {
                    PropertyDescriptor.SetValue(instance, newValue);
                }
            }
        }

        #endregion //Value

        #region DisplayName

        public static readonly DependencyProperty DisplayNameProperty = DependencyProperty.Register(
            "DisplayName",
            typeof(String),
            typeof(PropertyItem),
            new UIPropertyMetadata(String.Empty));

        public String DisplayName
        {
            get { return (String)GetValue(DisplayNameProperty); }
            set { SetValue(DisplayNameProperty, value); }
        }

        #endregion //DisplayName

        #endregion // Dependency Properties

        /// <summary>
        /// A reference to the property grid that owns this
        /// item
        /// </summary>
        public PropertyGrid PropertyGrid
        {
            get { return m_propertyGrid; }
            private set { m_propertyGrid = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public DependencyPropertyDescriptor Descriptor
        {
            get { return m_dpDescriptor; }
            set { m_dpDescriptor = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public MarkupObject MarkupObject
        {
            get { return m_markupObject; }
            set { m_markupObject = value; }
        }

        /// <summary>
        /// The propertry descriptor object where most of the state
        /// properties come from
        /// </summary>
        public PropertyDescriptor PropertyDescriptor
        {
            get { return m_propertyDescriptor; }
            private set
            {
                m_propertyDescriptor = value;
                this.DisplayName = m_propertyDescriptor.DisplayName;
                Category = m_propertyDescriptor.Category;
                this.Descriptor = DependencyPropertyDescriptor.FromProperty(m_propertyDescriptor);
            }
        }

        /// <summary>
        /// The objects that are tied to this property
        /// </summary>
        public Object Instance
        {
            get { return m_instance; }
            private set
            {
                m_instance = value;
                this.MarkupObject = MarkupWriter.GetMarkupObjectFor(m_instance);
            }
        }

        /// <summary>
        /// The objects that are tied to this property
        /// </summary>
        public List<Object> Instances
        {
            get { return m_instances; }
            private set
            {
                m_instances = value;
                if (m_instances.Count > 0)
                    this.MarkupObject = MarkupWriter.GetMarkupObjectFor(m_instances[0]);
            }
        }

        /// <summary>
        /// The description for the property
        /// </summary>
        public String Description 
        { 
            get { return PropertyDescriptor.Description; }
        }

        /// <summary>
        /// Gets if the property is data bound
        /// </summary>
        public Boolean IsDataBound
        {
            get
            {
                var dependencyObject = Instance as DependencyObject;
                if (dependencyObject != null && m_dpDescriptor != null)
                    return BindingOperations.GetBindingExpressionBase(dependencyObject, m_dpDescriptor.DependencyProperty) != null;

                return false;
            }
        }

        public Boolean IsDynamicResource
        {
            get
            {
                var markupProperty = m_markupObject.Properties.Where(p => p.Name == PropertyDescriptor.Name).FirstOrDefault();
                if (markupProperty != null)
                    return markupProperty.Value is DynamicResourceExtension;
                return false;
            }
        }

        public Boolean HasResourceApplied
        {
            get
            {
                var markupProperty = m_markupObject.Properties.Where(p => p.Name == PropertyDescriptor.Name).FirstOrDefault();
                if (markupProperty != null)
                    return markupProperty.Value is Style;

                return false;
            }
        }

        /// <summary>
        /// Returns true if the property set method cannot be
        /// reached
        /// </summary>
        public Boolean IsReadOnly
        { 
            get { return PropertyDescriptor.IsReadOnly; }
        }

        /// <summary>
        /// Returns true if the property set method can be
        /// reached
        /// </summary>
        public Boolean IsWriteable
        { 
            get { return !IsReadOnly; }
        }

        /// <summary>
        /// The type that the property is. Used to
        /// determine which editor to display
        /// </summary>
        public Type PropertyType
        { 
            get { return PropertyDescriptor.PropertyType; }
        }

        /// <summary>
        /// The command that is used when the user wants to
        /// reset the property
        /// </summary>
        public ICommand ResetValueCommand
        { 
            get;
            private set;
        }

        /// <summary>
        /// Gets the value source.
        /// </summary>
        public BaseValueSource ValueSource
        {
            get
            {
                var dependencyObject = Instance as DependencyObject;
                if (m_dpDescriptor != null && dependencyObject != null)
                    return DependencyPropertyHelper.GetValueSource(dependencyObject, m_dpDescriptor.DependencyProperty).BaseValueSource;

                return BaseValueSource.Unknown;
            }
        }

        #endregion // Properties

        #region Constructor

        /// <summary>
        /// Default constructor
        /// </summary>
        static PropertyItem()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(PropertyItem), new FrameworkPropertyMetadata(typeof(PropertyItem)));
        }

        /// <summary>
        /// Creates a property item for a property decription and a collection of instances
        /// </summary>
        public PropertyItem(List<Object> instances, PropertyDescriptor property, PropertyGrid propertyGrid)
        {
            PropertyDescriptor = property;
            PropertyGrid = propertyGrid;
            this.m_initialSet = true;
            this.Instances = new List<object>();
            if (instances.Count == 1)
            {
                Instance = instances[0];
                Instances.Add(instances[0]);
                this.Value = property.GetValue(instances[0]);
            }
            else
            {
                Object value = property.GetValue(instances[0]);
                Boolean sameValue = true;
                for (int i = 1; i < instances.Count; i++)
                {
                    Instances.Add(instances[i]);
                    Object instanceValue = property.GetValue(instances[i]);
                    if (!Object.Equals(instanceValue, value))
                    {
                        sameValue = false;
                    } 
                }
                if (sameValue == true)
                {
                    this.Value = value;
                }
                else
                {
                    this.Value = null;
                }
            }
            this.m_initialSet = false;

            foreach (object item in instances)
            {
                if (!(item is INotifyPropertyChanged))
                    continue;

                (item as INotifyPropertyChanged).PropertyChanged += new PropertyChangedEventHandler(PropertyItem_PropertyChanged);
            }

            CommandBindings.Add(new CommandBinding(PropertyItemCommands.ResetValue, ExecuteResetValueCommand, CanExecuteResetValueCommand));

            AddHandler(Mouse.PreviewMouseDownEvent, new MouseButtonEventHandler(PropertyItemPreviewMouseDown), true);
        }

        #endregion // Constructor

        #region Event Handlers

        /// <summary>
        /// Makes sure that this item gets selected and focused on mouse down.
        /// </summary>
        void PropertyItemPreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            IsSelected = true;

            if (!(e.Source is ComboBox))
                Focus();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void InstanceValueChanged(object sender, EventArgs e)
        {

        }

        void PropertyItem_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName != this.PropertyDescriptor.Name)
                return;

            ResolveValues();
        }
        #endregion  // Event Handlers

        #region Reset Commands
        
        /// <summary>
        /// Executes when the user chooses to reset the property
        /// </summary>
        private void ExecuteResetValueCommand(Object sender, ExecutedRoutedEventArgs e)
        {
            if (PropertyDescriptor.CanResetValue(Instance))
                PropertyDescriptor.ResetValue(Instance);
        }

        /// <summary>
        /// Gets called to determine if the user can reset the property value
        /// </summary>
        private void CanExecuteResetValueCommand(Object sender, CanExecuteRoutedEventArgs e)
        {
            Boolean canExecute = false;

            if (PropertyDescriptor.CanResetValue(Instance) && !PropertyDescriptor.IsReadOnly)
            {
                canExecute = true;
            }

            e.CanExecute = canExecute;
        }

        #endregion // Reset Commands

        #region Methods
        private void ResolveValues()
        {
            if (this.Dispatcher.CheckAccess())
            {
                this.m_resolving = true;
                if (Instances.Count == 1)
                {
                    this.Value = PropertyDescriptor.GetValue(Instances[0]);
                }
                else
                {
                    Object value = PropertyDescriptor.GetValue(Instances[0]);
                    Boolean sameValue = true;
                    for (int i = 1; i < Instances.Count; i++)
                    {
                        Instances.Add(Instances[i]);
                        Object instanceValue = PropertyDescriptor.GetValue(Instances[i]);
                        if (!Object.Equals(instanceValue, value))
                        {
                            sameValue = false;
                        }
                    }
                    if (sameValue == true)
                    {
                        this.Value = value;
                    }
                    else
                    {
                        this.Value = null;
                    }
                }
                this.m_resolving = false;
            }
            else
            {
                this.Dispatcher.Invoke(new Action(ResolveValues));
            }
        }
        #endregion
    } // PropertyItem
} // RSG.PropertyGrid
