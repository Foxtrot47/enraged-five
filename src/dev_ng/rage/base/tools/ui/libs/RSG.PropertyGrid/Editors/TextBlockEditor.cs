﻿using System.Windows.Controls;

namespace RSG.PropertyGrid.Editors
{
    /// <summary>
    /// Repesents a text block editor that is used for read only
    /// properties.
    /// </summary>
    public class TextBlockEditor : TypeEditor<TextBlock>
    {
        #region Overriden Functions

        /// <summary>
        /// Makes sure the value property is 'tagged' to the text dependency property
        /// </summary>
        protected override void SetValueDependencyProperty()
        {
            ValueProperty = TextBlock.TextProperty;
        }

        /// <summary>
        /// Sets any of the specific control styles for this editor
        /// </summary>
        protected override void SetControlProperties()
        {
            Editor.Margin = new System.Windows.Thickness(5, 0, 0, 0);
        }
        #endregion // Overridden Functions
    } // TextBlockEditor
} // RSG.PropertyGrid.Editors
