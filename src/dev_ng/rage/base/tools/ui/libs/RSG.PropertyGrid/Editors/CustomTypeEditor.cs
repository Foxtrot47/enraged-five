﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.PropertyGrid.Editors
{
    /// <summary>
    /// Specifies that a custom editor should be used for the given target
    /// type. (Gets created before the default ones so can override default editors
    /// for common types).
    /// </summary>
    public class CustomTypeEditor : ICustomTypeEditor
    {
        #region Members

        private IList<String> m_properties = new List<String>();

        #endregion // Members

        #region Properties

        /// <summary>
        /// The actual editor to use for this type
        /// </summary>
        public ITypeEditor Editor
        { 
            get; 
            set;
        }

        /// <summary>
        /// The properties for the custom type editor
        /// </summary>
        public IList<String> Properties
        {
            get { return m_properties; }
            set { m_properties = value; }
        }

        /// <summary>
        /// The type which the property needs to be to use this type
        /// </summary>
        public Type TargetType
        { 
            get;
            set;
        }

        #endregion // Properties
    } // CustomTypeEditor
} // RSG.PropertyGrid.Editors
