﻿using System.Windows;

namespace RSG.PropertyGrid.Editors
{
    public interface ITypeEditor
    {
        #region Methods

        void Attach(PropertyItem propertyItem);

        FrameworkElement ResolveEditor();

        #endregion // Methods
    } // ITypeEditor
} // RSG.PropertyGrid.Editors
