﻿using System;
using System.Windows;
using System.Windows.Data;

namespace RSG.PropertyGrid.Editors
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class TypeEditor<T> : ITypeEditor where T : FrameworkElement, new()
    {
        #region Properties

        /// <summary>
        /// The actual editor that will be used (the actual control)
        /// </summary>
        protected T Editor
        { 
            get;
            set;
        }

        /// <summary>
        /// The valus property is the property 'tagged' on the control
        /// to be the property to update
        /// </summary>
        protected DependencyProperty ValueProperty 
        { 
            get;
            set;
        }

        #endregion //Properties

        #region Constructors

        /// <summary>
        /// Default constructor
        /// </summary>
        public TypeEditor()
        {
            Editor = new T();
            SetValueDependencyProperty();
            SetControlProperties();
        }

        #endregion //Constructors

        #region ITypeEditor Members

        /// <summary>
        /// Attaches a property item to this editor
        /// </summary>
        public virtual void Attach(PropertyItem propertyItem)
        {
            ResolveValueBinding(propertyItem);
        }

        /// <summary>
        /// Can be overriden to do special things with the editor if need be before showing it
        /// </summary>
        public virtual FrameworkElement ResolveEditor()
        {
            return Editor;
        }

        #endregion // ITypeEditor Members

        #region Virtual Methods

        /// <summary>
        /// Can be overriden to surply a value converter to the
        /// binding between the property and the editor
        /// </summary>
        protected virtual IValueConverter CreateValueConverter()
        {
            return null;
        }

        /// <summary>
        /// Makes sure that the binding between the property and the editor is set
        /// and valid
        /// </summary>
        protected virtual void ResolveValueBinding(PropertyItem propertyItem)
        {
            var binding = new Binding("Value");
            binding.Source = propertyItem;
            binding.ValidatesOnExceptions = true;
            binding.ValidatesOnDataErrors = true;
            binding.Mode = propertyItem.IsWriteable ? BindingMode.TwoWay : BindingMode.OneWay;
            binding.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
            binding.Converter = CreateValueConverter();
            BindingOperations.SetBinding(Editor, ValueProperty, binding);
        }

        /// <summary>
        /// Sets any properties on the editor itself
        /// </summary>
        protected virtual void SetControlProperties()
        {
        }

        /// <summary>
        /// Sets the dependency property on the editor that is bound to the property
        /// value
        /// </summary>
        protected abstract void SetValueDependencyProperty();

        #endregion // Virtual Methods
    } // TypeEditor
} // RSG.PropertyGrid.Editors
