﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace RSG.PropertyGrid
{
    /// <summary>
    /// 
    /// </summary>
    public class PropertyCollection : ObservableCollection<Object>
    {
        #region Constructors

        public PropertyCollection()
        {

        }

        public PropertyCollection(List<Object> list)
            : base(list)
        {

        }

        public PropertyCollection(IEnumerable<Object> collection)
            : base(collection)
        {

        }

        #endregion // Constructors
    } // PropertyCollection
} // RSG.PropertyGrid
