﻿using System;
using System.Windows.Input;

namespace RSG.PropertyGrid.Commands
{
    /// <summary>
    /// 
    /// </summary>
    public static class PropertyItemCommands
    {
        private static RoutedCommand _resetValueCommand = new RoutedCommand();
        public static RoutedCommand ResetValue
        {
            get
            {
                return _resetValueCommand;
            }
        }
    } // PropertyItemCommands
} // RSG.PropertyGrid.Commands
