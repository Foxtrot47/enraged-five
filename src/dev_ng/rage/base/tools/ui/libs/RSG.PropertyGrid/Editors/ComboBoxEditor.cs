﻿using System.Collections.Generic;
using System.Windows.Controls;

namespace RSG.PropertyGrid.Editors
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class ComboBoxEditor : TypeEditor<ComboBox>
    {
        #region Overriden Functions

        protected override void SetValueDependencyProperty()
        {
            ValueProperty = ComboBox.SelectedItemProperty;
        }

        public override void Attach(PropertyItem propertyItem)
        {
            SetItemsSource(propertyItem);
            base.Attach(propertyItem);
        }

        protected abstract IList<object> CreateItemsSource(PropertyItem propertyItem);

        private void SetItemsSource(PropertyItem propertyItem)
        {
            Editor.ItemsSource = CreateItemsSource(propertyItem);
        }
        #endregion // Overriden Functions
    } // ComboBoxEditor
} // RSG.PropertyGrid.Editors
