﻿using System;
using System.Windows.Data;

namespace RSG.PropertyGrid.Converters
{
    /// <summary>
    /// 
    /// </summary>
    public class InverseBoolConverter : IValueConverter
    {
        #region IValueConverter Members

        public Object Convert(Object value, Type targetType, Object parameter, System.Globalization.CultureInfo culture)
        {
            return !(bool)value;
        }

        public Object ConvertBack(Object value, Type targetType, Object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    } // InverseBoolConverter
} //RSG.PropertyGrid.Converters
