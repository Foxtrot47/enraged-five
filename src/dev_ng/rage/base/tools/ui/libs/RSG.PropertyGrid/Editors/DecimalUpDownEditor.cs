﻿using System;
using RSG.Base.Windows.Controls;

namespace RSG.PropertyGrid.Editors
{
    /// <summary>
    /// 
    /// </summary>
    public class DecimalUpDownEditor : TypeEditor<DecimalUpDown>
    {
        #region Overriden Functions

        /// <summary>
        /// Makes sure the value property is 'tagged' to the text dependency property
        /// </summary>
        protected override void SetControlProperties()
        {
            Editor.TextAlignment = System.Windows.TextAlignment.Left;
            Editor.BorderThickness = new System.Windows.Thickness(0.0);
        }

        /// <summary>
        /// Sets any of the specific control styles for this editor
        /// </summary>
        protected override void SetValueDependencyProperty()
        {
            ValueProperty = DecimalUpDown.ValueProperty;
        }

        #endregion // Overriden Functions
    } // DoubleUpDownEditor
} // RSG.PropertyGrid.Editors
