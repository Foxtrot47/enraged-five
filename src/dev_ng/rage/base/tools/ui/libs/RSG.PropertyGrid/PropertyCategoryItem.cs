﻿using System;
using System.Collections.Generic;
using System.Windows.Controls;
using System.Windows;

namespace RSG.PropertyGrid
{
    /// <summary>
    /// 
    /// </summary>
    public class PropertyCategoryItem : Control
    {
        #region Memebers

        private List<PropertyItem> m_properties = new List<PropertyItem>();

        #endregion // Members

        #region Properties

        #region Dependency Properties

        public static readonly DependencyProperty CategoryProperty = DependencyProperty.Register(
            "Category",
            typeof(String), 
            typeof(PropertyCategoryItem), 
            new UIPropertyMetadata(String.Empty, new PropertyChangedCallback(OnCategoryChanged), new CoerceValueCallback(OnCoerceCategory)));

        private static Object OnCoerceCategory(DependencyObject o, Object value)
        {
            PropertyCategoryItem propertyCategoryItem = o as PropertyCategoryItem;
            if (propertyCategoryItem != null)
                return propertyCategoryItem.OnCoerceCategory((string)value);
            else
                return value;
        }

        private static void OnCategoryChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            PropertyCategoryItem propertyCategoryItem = o as PropertyCategoryItem;
            if (propertyCategoryItem != null)
                propertyCategoryItem.OnCategoryChanged((String)e.OldValue, (String)e.NewValue);
        }

        #endregion // Dependency Properties

        public String Category
        {
            get
            {
                return (String)GetValue(CategoryProperty);
            }
            set
            {
                SetValue(CategoryProperty, value);
            }
        }

        public List<PropertyItem> Properties
        {
            get { return m_properties; }
            set { m_properties = value; }
        }

        #endregion // Properties

        #region Constructor

        /// <summary>
        /// static constructor
        /// </summary>
        static PropertyCategoryItem()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(PropertyCategoryItem), new FrameworkPropertyMetadata(typeof(PropertyCategoryItem)));
        }

        #endregion // Constructor

        #region Virtual Methods

        protected virtual String OnCoerceCategory(String value)
        {
            return value;
        }

        protected virtual void OnCategoryChanged(String oldValue, String newValue)
        {
        }

        #endregion // Virtual Methods
    } // PropertyCategoryItem
} // RSG.PropertyGrid

