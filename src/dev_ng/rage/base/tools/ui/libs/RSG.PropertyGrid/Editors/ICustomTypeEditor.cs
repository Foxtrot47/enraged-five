﻿using System;
using System.Collections.Generic;

namespace RSG.PropertyGrid.Editors
{
    /// <summary>
    /// 
    /// </summary>
    public interface ICustomTypeEditor
    {
        #region Properties

        ITypeEditor Editor { get; set; }

        IList<String> Properties { get; set; }

        Type TargetType { get; set; }

        #endregion // Properties
    } // ICustomTypeEditor
} // RSG.PropertyGrid.Editors
