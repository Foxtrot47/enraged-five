﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows;

namespace RSG.PropertyGrid.Editors
{
    /// <summary>
    /// Enumerable type editor.
    /// </summary>
    public class EnumerableEditor : ITypeEditor
    {
        #region Public enumeration

        /// <summary>
        /// Display enumeration.
        /// </summary>
        public enum Display
        {
            /// <summary>
            /// Show only the item count.
            /// </summary>
            ItemCount,

            /// <summary>
            /// Allow the user to drop-down the items.
            /// </summary>
            DropDown
        }

        #endregion

        #region Public properties

        /// <summary>
        /// The valus property is the property 'tagged' on the control
        /// to be the property to update
        /// </summary>
        protected DependencyProperty ValueProperty
        {
            get;
            set;
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="display">Display type.</param>
        public EnumerableEditor(Display display)
        {
            if (display == Display.ItemCount)
            {
                editor = new TextBlock();
                ValueProperty = TextBlock.TextProperty;
            }
            else
            {
                editor = new ComboBox();
                ValueProperty = ComboBox.SelectedItemProperty;
            }

            this.display = display;
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Attach the property item to the editor.
        /// </summary>
        /// <param name="propertyItem">Property item.</param>
        public void Attach(PropertyItem propertyItem)
        {
            var binding = new Binding("Value");

            if (display == Display.ItemCount)
            {
                binding.Source = CreateProxy(propertyItem);
            }
            else
            {
                ((ComboBox)editor).ItemsSource = propertyItem.Value as System.Collections.IEnumerable;
                ((ComboBox)editor).ToolTip = IEnumerableHelper.ToString(propertyItem.Value as System.Collections.IEnumerable);
            }

            binding.ValidatesOnExceptions = true;
            binding.ValidatesOnDataErrors = true;
            binding.Mode = BindingMode.OneWay;
            binding.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
            binding.Converter = null;
            BindingOperations.SetBinding(editor, ValueProperty, binding);
        }

        /// <summary>
        /// Returns the instance of the visual editor.
        /// </summary>
        /// <returns></returns>
        public FrameworkElement ResolveEditor()
        {
            return editor;
        }

        #endregion

        #region Private helper methods

        /// <summary>
        /// Create a proxy property item for the visual editor. This will be used to convert the property from an IEnumerable 
        /// to a display showing the number of items.
        /// </summary>
        /// <param name="propertyItem">Property item.</param>
        /// <returns>Proxy property item containing modified property descriptors.</returns>
        private PropertyItem CreateProxy(PropertyItem propertyItem)
        {
            return new PropertyItem(propertyItem.Instances, new PropDesc(propertyItem.PropertyDescriptor.Name), propertyItem.PropertyGrid);
        }

        #endregion

        #region Property descriptor class for extending the properties

        /// <summary>
        /// Property descriptor class for extending the properties
        /// </summary>
        class PropDesc : System.ComponentModel.PropertyDescriptor
        {
            #region Constructor

            /// <summary>
            /// Constructor.
            /// </summary>
            /// <param name="name">Name of the property.</param>
            public PropDesc(string name)
                : base(name, null)
            {

            }

            #endregion

            public override bool CanResetValue(object component)
            {
                return true;
            }

            /// <summary>
            /// The component type.
            /// </summary>
            public override System.Type ComponentType
            {
                get { return typeof(string); }
            }

            /// <summary>
            /// Get the text representation of the component.
            /// </summary>
            /// <param name="component">Component.</param>
            /// <returns>The property's text representation.</returns>
            public override object GetValue(object component)
            {
                return IEnumerableHelper.ToString(component, Name);
            }

            /// <summary>
            /// Value is read-only.
            /// </summary>
            public override bool IsReadOnly
            {
                get { return true; }
            }

            /// <summary>
            /// Property type.
            /// </summary>
            public override System.Type PropertyType
            {
                get { return typeof(string); }
            }

            public override void ResetValue(object component)
            {

            }

            public override void SetValue(object component, object value)
            {

            }

            public override bool ShouldSerializeValue(object component)
            {
                return true;
            }
        }
        
        #endregion

        #region Static helper class to get the item count from an IEnumerable

        /// <summary>
        /// Enumerable helper class to 'convert' IEnumerables to text descriptions.
        /// </summary>
        static class IEnumerableHelper
        {
            /// <summary>
            /// Convertes a component with an IEnumerable propertyName to text.
            /// </summary>
            /// <param name="component">Component.</param>
            /// <param name="propertyName">IEnumerable property name.</param>
            /// <returns>The string version of the property's value.</returns>
            public static string ToString(object component, string propertyName)
            {
                System.Reflection.PropertyInfo pi = component.GetType().GetProperty(propertyName);
                var propValue = pi.GetValue(component, null);
                if (propValue == null)
                {
                    return "{null}";
                }

                if (propValue.GetType().GetInterface("ICollection") != null)
                {
                    var collection = propValue as System.Collections.ICollection;
                    return String.Format("{0} item(s)", collection.Count);
                }
                else if (propValue.GetType().GetInterface("IEnumerable") != null)
                {
                    var enumerable = propValue as System.Collections.IEnumerable;
                    return ToString(enumerable);
                }

                return "(Collection)";
            }

            /// <summary>
            /// Returns the number of items in the enumerable collection.
            /// </summary>
            /// <param name="enumerable">Enumerable.</param>
            /// <returns>The number of items in the IEnumerable instance.</returns>
            public static string ToString(System.Collections.IEnumerable enumerable)
            {
                if (enumerable == null)
                {
                    return "{null}";
                }

                int count = 0;
                foreach (object o in enumerable)
                {
                    count++;
                }

                return String.Format("{0} item(s)", count);
            }
        }


        #endregion

        #region Private member fields

        private FrameworkElement editor;
        private Display display;

        #endregion
    }
}
