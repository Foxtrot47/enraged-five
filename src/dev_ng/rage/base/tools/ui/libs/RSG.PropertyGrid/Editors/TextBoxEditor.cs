﻿using System;
using System.Windows.Controls;

namespace RSG.PropertyGrid.Editors
{
    /// <summary>
    /// Represents a text box editor that is used for
    /// any property that is a string and isn't read only.
    /// This editor is also used be default when no editor can
    /// be found
    /// </summary>
    public class TextBoxEditor : TypeEditor<TextBox>
    {
        #region Overriden Functions

        /// <summary>
        /// Makes sure the value property is 'tagged' to the text dependency property
        /// </summary>
        protected override void SetValueDependencyProperty()
        {
            ValueProperty = TextBox.TextProperty;
        }

        /// <summary>
        /// Sets any properties on the editor itself
        /// </summary>
        protected override void SetControlProperties()
        {
            Editor.BorderThickness = new System.Windows.Thickness(0.0);
            Editor.BorderBrush = System.Windows.Media.Brushes.Transparent;
        }
        #endregion // Overridden Functions
    } // TextBoxEditor
} // RSG.PropertyGrid.Editors