﻿using System;
using System.Windows;

namespace RSG.PropertyGrid.Utils
{
    /// <summary>
    /// Means that the context menu will be shown on a left mouse button click
    /// </summary>
    public class ContextMenuUtilities
    {
        public static readonly DependencyProperty OpenOnMouseLeftButtonClickProperty = DependencyProperty.RegisterAttached(
            "OpenOnMouseLeftButtonClick",
            typeof(Boolean), 
            typeof(ContextMenuUtilities),
            new FrameworkPropertyMetadata(false, OpenOnMouseLeftButtonClickChanged));

        public static void SetOpenOnMouseLeftButtonClick(FrameworkElement element, Boolean value)
        {
            element.SetValue(OpenOnMouseLeftButtonClickProperty, value);
        }
        public static Boolean GetOpenOnMouseLeftButtonClick(FrameworkElement element)
        {
            return (Boolean)element.GetValue(OpenOnMouseLeftButtonClickProperty);
        }

        public static void OpenOnMouseLeftButtonClickChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            FrameworkElement control = (FrameworkElement)sender;
            if ((Boolean)e.NewValue == true)
            {
                control.PreviewMouseLeftButtonDown += PreviewMouseLeftButtonDown;
            }
            else
            {
                control.PreviewMouseLeftButtonDown -= PreviewMouseLeftButtonDown;
            }
        }

        private static void PreviewMouseLeftButtonDown(Object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            FrameworkElement control = sender as FrameworkElement;
            if (control != null && control.ContextMenu != null)
            {
                control.ContextMenu.PlacementTarget = control;
                control.ContextMenu.IsOpen = true;
            }
        }
    } // ContextMenuUtilities
} // RSG.PropertyGrid.Utils
