﻿using System;
using System.Collections.ObjectModel;
using RSG.PropertyGrid.Editors;

namespace RSG.PropertyGrid
{
    public class CustomTypeEditorCollection : ObservableCollection<ICustomTypeEditor>
    {
        #region Properties

        public ICustomTypeEditor this[string propertyName]
        {
            get
            {
                foreach (var item in Items)
                {
                    if (item.Properties.Contains(propertyName))
                        return item;
                }

                return null;
            }
        }

        public ICustomTypeEditor this[Type targetType]
        {
            get
            {
                foreach (var item in Items)
                {
                    if (item.TargetType == targetType)
                        return item;
                }

                return null;
            }
        }

        #endregion // Properties
    } // CustomTypeEditorCollection
} // RSG.PropertyGrid
