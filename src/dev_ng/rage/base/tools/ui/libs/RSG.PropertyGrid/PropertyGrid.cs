﻿using System;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Controls.Primitives;
using System.ComponentModel;
using System.Windows.Input;
using RSG.PropertyGrid.Editors;

namespace RSG.PropertyGrid
{
    /// <summary>
    /// 
    /// </summary>
    public class PropertyGrid : Control
    {
        #region Members

        private Thumb m_dragThumb;
        private List<PropertyItem> m_propertyItemsCache;
        private List<Object> m_objectStack = new List<Object>();

        #endregion //Members

        #region Properties

        #region Depency Properties

        #region CustomTypeEditors

        public static readonly DependencyProperty CustomTypeEditorsProperty = DependencyProperty.Register(
            "CustomTypeEditors", 
            typeof(CustomTypeEditorCollection),
            typeof(PropertyGrid),
            new UIPropertyMetadata(new CustomTypeEditorCollection()));

        public CustomTypeEditorCollection CustomTypeEditors
        {
            get { return (CustomTypeEditorCollection)GetValue(CustomTypeEditorsProperty); }
            set { SetValue(CustomTypeEditorsProperty, value); }
        }

        #endregion //CustomTypeEditors

        #region IsCategorized

        public static readonly DependencyProperty IsCategorizedProperty = DependencyProperty.Register(
            "IsCategorized", 
            typeof(Boolean), 
            typeof(PropertyGrid),
            new UIPropertyMetadata(true, OnIsCategorizedChanged));

        public bool IsCategorized
        {
            get { return (Boolean)GetValue(IsCategorizedProperty); }
            set { SetValue(IsCategorizedProperty, value); }
        }

        private static void OnIsCategorizedChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            PropertyGrid propertyGrid = o as PropertyGrid;
            if (propertyGrid != null)
                propertyGrid.OnIsCategorizedChanged((Boolean)e.OldValue, (Boolean)e.NewValue);
        }

        protected virtual void OnIsCategorizedChanged(Boolean oldValue, Boolean newValue)
        {
            InitializePropertyGrid(newValue);
        }

        #endregion //IsCategorized

        #region NameColumnWidth

        public static readonly DependencyProperty NameColumnWidthProperty = DependencyProperty.Register(
            "NameColumnWidth", 
            typeof(double), 
            typeof(PropertyGrid), new UIPropertyMetadata(150.0));

        public double NameColumnWidth
        {
            get { return (double)GetValue(NameColumnWidthProperty); }
            set { SetValue(NameColumnWidthProperty, value); }
        }

        #endregion //NameColumnWidth

        #region Properties

        public static readonly DependencyProperty PropertiesProperty = DependencyProperty.Register(
            "Properties", 
            typeof(PropertyCollection), 
            typeof(PropertyGrid), 
            new UIPropertyMetadata(null));

        public PropertyCollection Properties
        {
            get { return (PropertyCollection)GetValue(PropertiesProperty); }
            private set { SetValue(PropertiesProperty, value); }
        }

        #endregion // Properties
        
        #region CommonBaseType

        public static readonly DependencyProperty CommonBaseTypeProperty = DependencyProperty.Register(
            "CommonBaseType",
            typeof(Type), 
            typeof(PropertyGrid),
            new UIPropertyMetadata(null, OnCommonBaseTypeChanged));

        public Type CommonBaseType
        {
            get { return (Type)GetValue(CommonBaseTypeProperty); }
            private set { SetValue(CommonBaseTypeProperty, value); }
        }

        private static void OnCommonBaseTypeChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            PropertyGrid propertyGrid = o as PropertyGrid;
            if (propertyGrid != null)
                propertyGrid.OnCommonBaseTypeChanged((Type)e.OldValue, (Type)e.NewValue);
        }

        protected virtual void OnCommonBaseTypeChanged(Type oldValue, Type newValue)
        {
            CommonBaseTypeName = newValue.Name;
        }

        #endregion // CommonBaseType

        #region CommonBaseTypeName

        public static readonly DependencyProperty CommonBaseTypeNameProperty = DependencyProperty.Register(
            "CommonBaseTypeName", 
            typeof(String), 
            typeof(PropertyGrid), 
            new UIPropertyMetadata(string.Empty));

        public String CommonBaseTypeName
        {
            get { return (String)GetValue(CommonBaseTypeNameProperty); }
            private set { SetValue(CommonBaseTypeNameProperty, value); }
        }

        #endregion // CommonBaseTypeName

        #region SelectedProperty

        public static readonly DependencyProperty SelectedPropertyProperty = DependencyProperty.Register(
            "SelectedProperty", 
            typeof(PropertyItem), 
            typeof(PropertyGrid), 
            new UIPropertyMetadata(null, OnSelectedPropertyChanged));

        public PropertyItem SelectedProperty
        {
            get { return (PropertyItem)GetValue(SelectedPropertyProperty); }
            internal set { SetValue(SelectedPropertyProperty, value); }
        }

        private static void OnSelectedPropertyChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            PropertyGrid propertyGrid = o as PropertyGrid;
            if (propertyGrid != null)
                propertyGrid.OnSelectedPropertyChanged((PropertyItem)e.OldValue, (PropertyItem)e.NewValue);
        }

        protected virtual void OnSelectedPropertyChanged(PropertyItem oldValue, PropertyItem newValue)
        {
            if (oldValue != null)
                oldValue.IsSelected = false;

            if (newValue != null)
                newValue.IsSelected = true;
        }

        #endregion //SelectedProperty

        #region DisplaySummary

        public static readonly DependencyProperty DisplaySummaryProperty = DependencyProperty.Register(
            "DisplaySummary", 
            typeof(Boolean), 
            typeof(PropertyGrid),
            new UIPropertyMetadata(true));

        public Boolean DisplaySummary
        {
            get { return (Boolean)GetValue(DisplaySummaryProperty); }
            set { SetValue(DisplaySummaryProperty, value); }
        }

        #endregion //DisplaySummary

        #endregion // Depency Properties

        /// <summary>
        /// The drag thumb from the control template if it exists, this
        /// drag thumb is used to change the width of the name column
        /// </summary>
        private Thumb DragThumb
        {
            get { return m_dragThumb; }
            set { m_dragThumb = value; }
        }

        /// <summary>
        /// The property items that have been got through reflection
        /// from the object/s that are pushed to this property grid
        /// </summary>
        private List<PropertyItem> PropertyItemsCache
        {
            get { return m_propertyItemsCache; }
            set { m_propertyItemsCache = value; }
        }

        /// <summary>
        /// The objects that have currently been pushed to this property grid
        /// and can have their properties shown.
        /// </summary>
        internal List<Object> ObjectStack
        {
            get { return m_objectStack; }
            set { m_objectStack = value; }
        }

        #endregion // Properties

        #region Constructors

        /// <summary>
        /// Static constructor, makes sure the style of this control
        /// is resest.
        /// </summary>
        static PropertyGrid()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(PropertyGrid), new FrameworkPropertyMetadata(typeof(PropertyGrid)));
        }

        #endregion //Constructors

        #region Overrides

        /// <summary>
        /// Takes a reference to the drag thumb part of the control
        /// template and starts listening to the drag event for the thumb
        /// </summary>
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            this.DragThumb = GetTemplateChild("PART_DragThumb") as Thumb;

            if (this.DragThumb != null)
                this.DragThumb.DragDelta += DragThumbDragDelta;
        }

        #endregion // Overrides

        #region Event Handlers

        /// <summary>
        /// Resizes the name column width based on the delta drag movement
        /// </summary>
        void DragThumbDragDelta(Object sender, DragDeltaEventArgs e)
        {
            NameColumnWidth = Math.Max(0, NameColumnWidth + e.HorizontalChange);
        }

        #endregion // Event Handlers

        #region Private Methods

        /// <summary>
        /// Initialises the property grid by setting the properties and
        /// the drag thumb margin
        /// </summary>
        private void InitializePropertyGrid(Boolean isCategorised)
        {
            LoadProperties(isCategorised);
            SetDragThumbMargin(isCategorised);
        }

        /// <summary>
        /// Loads the properties form the pushed object/s and sets them in the
        /// right order based on if this is currently in a categorised state
        /// </summary>
        private void LoadProperties(Boolean isCategorised)
        {
            if (isCategorised)
                Properties = GetCategorisedProperties(this.PropertyItemsCache);
            else
                Properties = GetAlphabetisedProperties(this.PropertyItemsCache);
        }

        /// <summary>
        /// Gets the properties from the source object/s in the categorsed order
        /// </summary>
        private static PropertyCollection GetCategorisedProperties(List<PropertyItem> propertyItems)
        {
            PropertyCollection propertyCollection = new PropertyCollection();

            CollectionViewSource src = new CollectionViewSource { Source = propertyItems };
            src.GroupDescriptions.Add(new PropertyGroupDescription("Category"));
            src.SortDescriptions.Add(new SortDescription("Category", ListSortDirection.Ascending));
            src.SortDescriptions.Add(new SortDescription("Name", ListSortDirection.Ascending));

            if (src.View != null)
            {
                foreach (CollectionViewGroup item in src.View.Groups)
                {
                    PropertyCategoryItem propertyCategoryItem = new PropertyCategoryItem { Category = item.Name.ToString() };
                    foreach (var propertyitem in item.Items)
                    {
                        propertyCategoryItem.Properties.Add((PropertyItem)propertyitem);
                    }
                    propertyCollection.Add(propertyCategoryItem);
                }
            }

            return propertyCollection;
        }

        /// <summary>
        /// Gets the properties from the source object/s in the alphabetised order
        /// </summary>
        private static PropertyCollection GetAlphabetisedProperties(List<PropertyItem> propertyItems)
        {
            PropertyCollection propertyCollection = new PropertyCollection();

            if (propertyItems == null)
                return propertyCollection;

            CollectionViewSource src = new CollectionViewSource { Source = propertyItems };
            src.SortDescriptions.Add(new SortDescription("Name", ListSortDirection.Ascending));

            foreach (var item in ((ListCollectionView)(src.View)))
            {
                propertyCollection.Add((PropertyItem)item);
            }

            return propertyCollection;
        }
        
        /// <summary>
        /// Gets all the properties from a specific object and creates the property items from them
        /// </summary>
        private List<PropertyItem> GetTypeProperties(Type type, List<Object> instances)
        {
            List<PropertyItem> propertyItems = new List<PropertyItem>();

            if (type == null)
                return propertyItems;

            type.GetProperties(BindingFlags.Public);
            var properties = TypeDescriptor.GetProperties(type, new Attribute[] { new PropertyFilterAttribute(PropertyFilterOptions.All) });

            try
            {
                // Get all properties of the type
                propertyItems.AddRange(properties.Cast<PropertyDescriptor>().
                    Where(p => p.IsBrowsable && p.Name != "GenericParameterAttributes").
                    Select(property => CreatePropertyItem(property, instances, this)));
            }
            catch (Exception ex)
            {
                RSG.Base.Logging.Log.Log__Exception(ex, "Exception caught while getting the property of an object for the property grid");
            }

            return propertyItems;
        }

        /// <summary>
        /// Creates a single property item using the property descriptor and the actual object.
        /// This function is response of taking the property type and choosing a editor for it
        /// </summary>
        private PropertyItem CreatePropertyItem(PropertyDescriptor property, List<Object> instances, PropertyGrid grid)
        {
            PropertyItem propertyItem = new PropertyItem(instances, property, grid);

            ITypeEditor editor = null;

            // check for custom editor
            if (CustomTypeEditors.Count > 0)
            {
                // first check if the custom editor is type based
                ICustomTypeEditor customEditor = CustomTypeEditors[propertyItem.PropertyType];
                if (customEditor == null)
                {
                    customEditor = CustomTypeEditors[propertyItem.Name];
                }

                if (customEditor != null)
                    editor = customEditor.Editor;
            }

            try
            {
                //no custom editor found
                if (editor == null)
                {
                    if (propertyItem.PropertyType.GetInterface("IEnumerable") != null && propertyItem.PropertyType!=typeof(String))
                        editor = new EnumerableEditor(EnumerableEditor.Display.DropDown);
                    else if (propertyItem.IsReadOnly)
                        editor = new TextBlockEditor();
                    else if (propertyItem.PropertyType == typeof(Boolean))
                        editor = new CheckBoxEditor();
                    else if (propertyItem.PropertyType == typeof(float) || propertyItem.PropertyType == typeof(float?))
                        editor = new FloatUpDownEditor();
                    else if (propertyItem.PropertyType == typeof(decimal) || propertyItem.PropertyType == typeof(decimal?))
                        editor = new DecimalUpDownEditor();
                    else if (propertyItem.PropertyType == typeof(double) || propertyItem.PropertyType == typeof(double?))
                        editor = new DoubleUpDownEditor();
                    else if (propertyItem.PropertyType == typeof(int) || propertyItem.PropertyType == typeof(int?))
                        editor = new IntegerUpDownEditor();
                    else if (propertyItem.PropertyType == typeof(uint) || propertyItem.PropertyType == typeof(uint?))
                        editor = new UnsignedIntegerUpDownEditor();
                    else if (propertyItem.PropertyType == typeof(Color))
                        editor = new ColorEditor();
                    else if (propertyItem.PropertyType.IsEnum)
                        editor = new EnumComboBoxEditor();
                    else
                        editor = new TextBoxEditor();
                }
            }
            catch (Exception ex)
            {
                RSG.Base.Logging.Log.Log__Exception(ex, "Exception caught while setting the type editor");
            }

            if (editor != null)
            {
                editor.Attach(propertyItem);
                propertyItem.Editor = editor.ResolveEditor();
            }

            return propertyItem;
        }

        ///// <summary>
        ///// Sets the name binding for the given object
        ///// </summary>
        //private void SetSelectedObjectNameBinding(Object selectedObject)
        //{
        //    if (selectedObject is FrameworkElement)
        //    {
        //        var binding = new Binding("Name");
        //        binding.Source = selectedObject;
        //        binding.Mode = BindingMode.OneWay;
        //        BindingOperations.SetBinding(this, PropertyGrid.SelectedObjectNameProperty, binding);
        //    }
        //}

        /// <summary>
        /// Sets the drag thumb margin if it has been set in the control template.
        /// This is so it automatically changes when the order/categorization changes
        /// </summary>
        private void SetDragThumbMargin(Boolean isCategorised)
        {
            if (this.DragThumb == null)
                return;

            if (isCategorised)
                this.DragThumb.Margin = new Thickness(6, 0, 0, 0);
            else
                this.DragThumb.Margin = new Thickness(-1, 0, 0, 0);
        }
        
        /// <summary>
        /// Gets the common base class for a set of objects
        /// </summary>
        private static Type GetCommonBaseClass(IEnumerable e)
        {
            Type[] types = e.Cast<Object>().Select(o => o.GetType()).ToArray<Type>();
            return GetCommonBaseClass(types);
        }

        /// <summary>
        /// Gets the common base class for a set of types
        /// </summary>
        private static Type GetCommonBaseClass(Type[] types)
        {
            if (types.Length == 0)
                return typeof(Object);

            Type ret = types[0];

            for (int i = 1; i < types.Length; ++i)
            {
                if (types[i].IsAssignableFrom(ret))
                    ret = types[i];
                else
                {
                    // This will always terminate when ret == typeof(object)
                    while (!ret.IsAssignableFrom(types[i]))
                        ret = ret.BaseType;
                }
            }

            return ret;
        }

        #endregion // Private Methods

        #region Public Methods

        /// <summary>
        /// Pushes the given object onto the object stack and
        /// resolves the common properties for all the objects on the stack
        /// </summary>
        public void Push(Object item)
        {
            this.PushRange(new List<object>() { item });
        }

        /// <summary>
        /// Pushes the given collection onto the object stack and
        /// resolves the common properties for all the objects on the stack
        /// </summary>
        public void PushRange(IEnumerable<Object> collection)
        {
            if (collection != null)
            {
                bool modified = false;
                foreach (Object obj in collection)
                {
                    if (!ObjectStack.Contains(obj))
                    {
                        ObjectStack.Add(obj);
                        modified = true;
                    }
                }
                if (modified == true)
                {
                    Type type = GetCommonBaseClass(ObjectStack);
                    if (type != null)
                    {
                        this.CommonBaseType = type;
                        this.PropertyItemsCache = GetTypeProperties(type, collection.ToList());
                    }
                }
                InitializePropertyGrid(IsCategorized);
            }
        }

        /// <summary>
        /// Pops the given object off the object stack and
        /// resolves the common properties for all the objects on the stack
        /// </summary>
        public void Pop(Object item)
        {
            this.PopRange(new List<object>() { item });
        }

        /// <summary>
        /// Pops the given collection off the object stack and
        /// resolves the common properties for all the objects on the stack
        /// </summary>
        public void PopRange(IEnumerable<Object> collection)
        {
            if (collection != null)
            {
                bool modified = false;
                foreach (Object obj in collection)
                {
                    if (ObjectStack.Contains(obj))
                    {
                        ObjectStack.Remove(obj);
                        modified = true;
                    }
                }
                if (modified == true)
                {
                    Type type = GetCommonBaseClass(ObjectStack);
                    if (type != null)
                    {
                        this.CommonBaseType = type;
                        this.PropertyItemsCache = GetTypeProperties(type, collection.ToList());
                    }
                }
                InitializePropertyGrid(IsCategorized);
            }           
        }

        /// <summary>
        /// Clears the object stack
        /// </summary>
        public void Clear()
        {
            this.ObjectStack.Clear();
            if (this.PropertyItemsCache != null)
                this.PropertyItemsCache.Clear();
            if (this.Properties != null)
                this.Properties.Clear();
            this.SelectedProperty = null;
        }

        #endregion // Public Methods
    } // PropertyGrid
} // RSG.PropertyGrid
