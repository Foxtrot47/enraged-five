﻿using System;
using RSG.Base.Windows.Controls;

namespace RSG.PropertyGrid.Editors
{
    /// <summary>
    /// 
    /// </summary>
    public class ColorEditor : TypeEditor<ColorPicker>
    {
        #region Overriden Functions

        /// <summary>
        /// Sets any of the specific control styles for this editor
        /// </summary>
        protected override void SetValueDependencyProperty()
        {
            ValueProperty = ColorPicker.SelectedColorProperty;
        }

        #endregion // Overriden Functions
    } // ColorEditor
} // RSG.PropertyGrid.Editors
