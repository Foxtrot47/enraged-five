﻿using System.Windows;
using System.Windows.Controls;

namespace RSG.PropertyGrid.Editors
{
    /// <summary>
    /// 
    /// </summary>
    public class CheckBoxEditor : TypeEditor<CheckBox>
    {
        #region Overriden Functions

        /// <summary>
        /// Makes sure the value property is 'tagged' to the text dependency property
        /// </summary>
        protected override void SetControlProperties()
        {
            Editor.Margin = new Thickness(5, 0, 0, 0);
        }

        /// <summary>
        /// Sets any of the specific control styles for this editor
        /// </summary>
        protected override void SetValueDependencyProperty()
        {
            ValueProperty = CheckBox.IsCheckedProperty;
        }

        #endregion // Overriden Functions
    } // CheckBoxEditor
} // RSG.PropertyGrid.Editors
