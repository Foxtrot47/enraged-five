﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Drawing.Drawing2D;

namespace RSG.TrackViewer
{
    /// <summary>
    /// Interaction logic for SelectionLayer.xaml
    /// </summary>
    public partial class SelectionLayer : ContentControl
    {
        public SelectionLayer()
        {
            InitializeComponent();
            Origin = new Point(0.0, 0.0);
            Extent = new Point(0.0, 0.0);
            RectLine = new Pen(Brushes.Black, 0.5);
            RectLine.DashStyle = DashStyles.Dash;
        }

        //public Brush BackgroundBrush;
        Pen RectLine; 

        public Point Origin
        {
            get
            {
                return _Origin;
            }
            set
            {
                if (_Origin != value)
                {
                    _Origin = value;
                }
            }
        }
        Point _Origin;

        public Point Extent
        {
            get
            {
                return _Extent;
            }
            set
            {
                if (_Extent != value)
                {
                    _Extent = value;
                }
            }
        }
        Point _Extent;

        public void Reset()
        {
            Origin = new Point(0.0, 0.0);
            Extent = new Point(0.0, 0.0);
        }

        protected override void OnRender(DrawingContext dc)
        {
            Rect drawRect = new Rect(Origin, Extent);
            dc.DrawRectangle(null, RectLine, drawRect);
        }
    }

}
