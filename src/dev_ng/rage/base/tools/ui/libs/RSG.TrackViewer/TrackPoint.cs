﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.ComponentModel;

namespace RSG.TrackViewer
{
    public abstract class TrackPoint : INotifyPropertyChanged
    {
        internal TrackPoint(double x, double y)
        {
            X = x;
            Y = y;
            IsSelected = false;
            
        }

        
        public event PropertyChangedEventHandler PropertyChanged;

        public void Notify(string propName)
        {
           
            if (this.PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propName));
            }
        }
        

        public double X 
        {
            get
            {
                return _X;
            }
            set
            {
                if (_X != value )
                {
                    if (!IsLockedX)
                    {
                        _X = value;
                        Notify("X");
                    }
                    
                }
            }
        }
        double _X;

        public double Y
        {
            get
            {
                return _Y;
            }
            set
            {
                if (_Y != value)
                {
                    if (!IsLockedY)
                    {
                        _Y = value;
                        Notify("Y");
                    }
                   
                }
            }
        }
        double _Y;

        public bool IsSelected { get; set; }
        public bool IsLocked 
        {
            get
            {
                return _IsLocked;
            }
            set
            {
                _IsLocked = IsLockedX = IsLockedY = value;
            }
        }
        bool _IsLocked;
        public bool IsLockedX { get; set; }
        public bool IsLockedY { get; set; }
    }

    public class FloatTrackPoint : TrackPoint
    {
        public FloatTrackPoint(double x, double y) : base(x,y)
        {

        }

        public FloatTrackPoint(TrackPoint p)
            : base(p.X, p.Y)
        {
           
        }

        protected Point Position { get; private set; }
    }

    public class BoolTrackPoint : TrackPoint
    {
        internal BoolTrackPoint(double y, bool val)
            : base(0.0, y)
        {

        }

    }

    public class StringTrackPoint : TrackPoint
    {
        internal StringTrackPoint(double y, String val)
            : base(0.0, y)
        {

        }

    }
}
