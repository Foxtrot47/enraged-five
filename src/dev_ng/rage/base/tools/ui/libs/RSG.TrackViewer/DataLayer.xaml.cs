﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;
using RSG.TrackViewer.Data;

using RSG.TrackViewer.ViewModel;


namespace RSG.TrackViewer
{
    /// <summary>
    /// Responsible for rendering the view of the data and handling mouse interaction with the view
    /// </summary>
    public partial class DataLayer : ContentControl, INotifyPropertyChanged
    {
        private double m_minPointGapX = 0.01;

        public DataLayer()
        {
            InitializeComponent();
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void Notify(string propName)
        {

            if (this.PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propName));
            }
        }

        #region Properties

        public static readonly DependencyProperty HorizTimelineProperty = DependencyProperty.Register(
        "HorizTimeline",
        typeof(WindowedTimeLine),
        typeof(DataLayer),
        new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.AffectsArrange));
        public WindowedTimeLine HorizTimeline
        {
            get
            {
                return (WindowedTimeLine)GetValue(HorizTimelineProperty);
            }
            set
            {
                SetValue(HorizTimelineProperty, value);
            }
        }

        public static readonly DependencyProperty VertTimelineProperty = DependencyProperty.Register(
        "VertTimeline",
        typeof(WindowedTimeLine),
        typeof(DataLayer),
        new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.AffectsArrange));
        public WindowedTimeLine VertTimeline
        {
            get
            {
                return (WindowedTimeLine)GetValue(VertTimelineProperty);
            }
            set
            {
                SetValue(VertTimelineProperty, value);
            }
        }

        public Brush BackgroundBrush = Brushes.Black;

        TrackManager TM = TrackManager.Instance;
        CommonTrackData CommonData = TrackManager.Instance.CommonData;

        const int MAX_COLORS = 10;
        static System.Windows.Media.Brush[] sColors = new System.Windows.Media.Brush[MAX_COLORS] { System.Windows.Media.Brushes.Red, System.Windows.Media.Brushes.Green, System.Windows.Media.Brushes.Blue, System.Windows.Media.Brushes.Pink, System.Windows.Media.Brushes.AntiqueWhite,
                                                                    System.Windows.Media.Brushes.Black, System.Windows.Media.Brushes.Brown, System.Windows.Media.Brushes.Cornsilk, System.Windows.Media.Brushes.Orange, System.Windows.Media.Brushes.Olive};
      
        #endregion // Properties

        #region Private Methods
        /// <summary>
        /// Set a points selected flag to true if it is visible on a point that lies within range of p
        /// TODO: Move to TrackManager
        /// </summary>
        /// <param name="p">Point we are using to determine a hit</param>
        /// <param name="clearSelection">Whether to clear the existing list of selected points</param>
        public bool SetSelected(Point p, bool clearSelection)
        {
            if(clearSelection)
                CommonData.SelectedPoints.Clear();
            bool selectionChange = false;
            foreach (TrackGroupViewModel tg in TM.TrackGroupCollectionViewModel.TrackGroupViewModels)
            {
                foreach (TrackViewModel t in tg.TrackViewModels)
                {
                    if (t.Visible || tg.Visible)
                    {
                        foreach (TrackPoint tp in t.Track.Points)
                        {
                            if (Math.Abs(tp.X - p.X) < 0.01 && Math.Abs(tp.Y - p.Y) < 0.01 && !selectionChange)
                            {
                                if (null == CommonData.SelectedPoints.FirstOrDefault((delegate(TrackPoint pt) { return pt.X == tp.X && pt.Y == tp.Y; })))
                                {
                                   
                                    CommonData.SelectedPoints.Add(tp);
                                }
                                CommonData.SelectedPoint.PropertyChanged -= new PropertyChangedEventHandler(TrackPoint_PropertyChanged);
                                CommonData.SelectedPoint = tp;
                                CommonData.SelectedPoint.PropertyChanged += new PropertyChangedEventHandler(TrackPoint_PropertyChanged);
                                selectionChange = true;
                            }
                        }
                    }
                }
            }
            return selectionChange;
        }

        /// <summary>
        /// Set a points selected flag to true if it is visible on all points within a rectangle
        /// TODO: Move to TrackManager
        /// </summary>
        /// <param name="r">Rectangle perimiter for selection</param>
        public bool SetSelected(Rect r)
        {
            CommonData.SelectedPoints.Clear();
            bool selectionChange = false;
            foreach (TrackGroupViewModel tg in TM.TrackGroupCollectionViewModel.TrackGroupViewModels)
            {
                foreach (TrackViewModel t in tg.TrackViewModels)
                {
                    if (t.Visible || tg.Visible)
                    {
                        foreach (TrackPoint tp in t.Track.Points)
                        {
                            if (r.Contains(new Point(tp.X, tp.Y)))
                            {
                                
                                CommonData.SelectedPoints.Add(tp);
                                CommonData.SelectedPoint.PropertyChanged -= new PropertyChangedEventHandler(TrackPoint_PropertyChanged);
                                CommonData.SelectedPoint = tp;
                                CommonData.SelectedPoint.PropertyChanged += new PropertyChangedEventHandler(TrackPoint_PropertyChanged);
                                selectionChange = true;
                            }
                            
                        }
                    }
                }
            }
            return selectionChange;
        }

        /// <summary>
        /// Update all selected points by calculating delta from primary selected point
        /// TODO: Move to TrackManager
        /// </summary>
        /// <param name="p">Point we using to update position of any selected points</param>
        public void UpdatePositionForSelection(Point p)
        {
            if (CommonData.SelectedPoint.IsLocked == false)
            {
                // Calculate out delta relative to the selected point
                double deltaX = p.X - CommonData.SelectedPoint.X;
                double deltaY = p.Y - CommonData.SelectedPoint.Y;
                foreach (TrackPoint tp in CommonData.SelectedPoints)
                {
                    double newX = tp.X + deltaX;
                    double newY = tp.Y + deltaY;
                    if (!tp.IsLocked)
                    {
                        if (!tp.IsLockedX)
                        {
                            if (newX <= 1.0 && newX > 0.0)
                                tp.X = newX;
                            if (CommonData.SelectedPoint.X == 1.0)
                                tp.X = CommonData.SelectedPoint.X;
                        }
                        if (!tp.IsLockedY)
                        {
                            if (newY <= 1.0 && newY >= 0.0)
                                tp.Y = newY;
                            if (CommonData.SelectedPoint.Y == 1.0)
                                tp.Y = CommonData.SelectedPoint.Y;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Sort all tracks so their order reflects their position along the X axis
        /// TODO: Move to TrackManager
        /// </summary>
        public void SortTracks()
        {
            foreach (TrackGroupViewModel tg in TM.TrackGroupCollectionViewModel.TrackGroupViewModels)
            {
                foreach (TrackViewModel t in tg.TrackViewModels)
                {
                    if (t.Visible || tg.Visible)
                    {
                        t.Track.SortPoints();
                    }
                }
            }
        }

        
        #endregion // Public Methods

        #region Events

        void TrackPoint_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            SortTracks();
            InvalidateVisual();
        }

        protected override void OnRender(DrawingContext dc)
        {
            Rect rect = new Rect();
            rect.Height = 4.0;
            rect.Width = 4.0;
            Rect selRect = new Rect();
            selRect.Height = 8.0;
            selRect.Width = 8.0;
            Point lastPoint = new Point(0.0, 0.0);

            Pen keyPen;
            Pen curvePen;

            foreach (TrackGroupViewModel trackGroupModel in TM.TrackGroupCollectionViewModel.TrackGroupViewModels)
            {
                int idx = 0;
                foreach (TrackViewModel trackView in trackGroupModel.TrackViewModels)
                {
                    if (trackView != null)
                    {
                        if (trackView.Visible && trackView.Enabled || (trackGroupModel.Visible && trackView.Enabled))
                        {
                            keyPen = new Pen(sColors[idx], 0.5);
                            curvePen = new Pen(sColors[idx], 1.0);

                            int pointidx = 0;

                            TrackPoint selectedPoint = CommonData.SelectedPoint;

                            // This block of code was causing the data model to
                            // to change and resulting in the situation where two points
                            // couldn't occupy the same x value space.

                            //foreach (TrackPoint tp in trackView.Track.Points)
                            //{
                            //    //  Make sure two points aren't too close on X axis
                            //    if (selectedPoint != null && tp != selectedPoint)
                            //    {
                            //        if ((float)(tp.X - selectedPoint.X) < m_minPointGapX && (float)(tp.X - selectedPoint.X) > -m_minPointGapX)
                            //        {
                            //            if (tp.X > selectedPoint.X)
                            //            {
                            //                selectedPoint.X = tp.X - m_minPointGapX;
                            //            }
                            //            else
                            //            {
                            //                selectedPoint.X = tp.X + m_minPointGapX;
                            //            }
                            //        }
                            //    }
                            //}

                            foreach (TrackPoint tp in trackView.Track.Points)
                            {
                                double horizDisp = HorizTimeline.PhaseToDisplay(tp.X);
                                double vertDisp = VertTimeline.PhaseToDisplay(tp.Y);

                                double horiz = HorizTimeline.PhaseToPixel(tp.X);
                                double vert = VertTimeline.PhaseToPixel(tp.Y);

                                Rect currRect;

                                if (TM.CommonData.SelectedPoints.Contains(tp))
                                    currRect = selRect;
                                else
                                    currRect = rect;

                                currRect.X = horiz - (currRect.Height / 2.0);
                                currRect.Y = vert - (currRect.Width / 2.0);
                                dc.DrawRectangle(BackgroundBrush, keyPen, currRect);

                                if (trackGroupModel.RenderLines)
                                {
                                    if (pointidx != 0)
                                        dc.DrawLine(curvePen, (new Point(horiz, vert)), lastPoint);

                                    if (pointidx == (trackView.Track.Points.Count - 1))
                                    {
                                        dc.DrawLine(curvePen, (new Point(horiz, vert)), new Point(HorizTimeline.PhaseToPixel(1.0), vert));
                                    }
                                }

                                lastPoint.X = horiz;
                                lastPoint.Y = vert;
                                pointidx++;
                            }
                        }
                    }
                    idx++;
                }
            }
        }

        #endregion // Event
    }
}
