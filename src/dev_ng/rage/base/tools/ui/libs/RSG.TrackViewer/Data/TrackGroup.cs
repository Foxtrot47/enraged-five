﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.Serialization;
using System.Xml;


namespace RSG.TrackViewer.Data
{
    [Serializable]
    public class TrackGroup : ISerializable 
    {
        #region Constructors
        public TrackGroup(string name)
        {
            Name = name;
            Tracks = new ObservableCollection<Track>();
            RenderLines = true;
            Enabled = true;
        }
        #endregion // Constructors

        #region Properties
        public bool Visible
        {
            get
            {
                return _Visible;
            }
            set
            {
                if (_Visible != value)
                {
                    _Visible = value;
                }
            }
        }
        bool _Visible;

        /// <summary>
        /// Enabled flag.
        /// </summary>
        public bool Enabled
        {
            get
            {
                return _Enabled;
            }
            set
            {
                if (_Enabled != value)
                {
                    _Enabled = value;
                }
            }
        }
        bool _Enabled;

        public bool RenderLines
        {
            get
            {
                return _RenderLines;
            }
            set
            {
                if (_RenderLines != value)
                {
                    _RenderLines = value;
                }
            }
        }
        bool _RenderLines;

        public string Name { get; private set; }
        public override string ToString()
        {
            if (Name != String.Empty)
            {
                return Name;
            }
            return base.ToString();
        }

        public string ShortName
        {
            get
            {
                return ToString();
            }
        }

        public ObservableCollection<Track> Tracks { get; private set; }

        #endregion // Properties

        #region Public Functions
        public void Add(Track t)
        {
            Tracks.Add(t);
        }

        public void Remove(Track t)
        {
            Tracks.Remove(t);
        }
        #endregion Public Functions

        #region Event Handlers
        public event EventHandler TracksChanged;
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string prop)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
            }
        }

        public void FireTracksChanged()
        {
            if (TracksChanged != null)
            {
                TracksChanged(this, EventArgs.Empty);
            }
        }
        #endregion

        #region Serialisation
        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("Name", Name);
            info.AddValue("Visible", Visible);
            info.AddValue("Tracks", Tracks);
            info.AddValue("Enabled", Enabled);
            info.AddValue("RenderLines", RenderLines);
        }

        public TrackGroup(SerializationInfo info, StreamingContext context)
        {
            
            Name = info.GetString("Name");
            Visible = info.GetBoolean("Visible");
            Tracks = (ObservableCollection<Track>)info.GetValue("Tracks", typeof(ObservableCollection<Track>));
            Enabled = info.GetBoolean("Enabled");
            RenderLines = info.GetBoolean("RenderLines");
        }
        #endregion // Serialisation


        
        /// <summary>
        /// Initialises a new instance of the <see cref="TrackGroup"/> class
        /// using the specified System.Xml.XmlReader as a data provider.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides access to the xml data used to initialise
        /// this instance.
        /// </param>
        /// <exception cref="System.ArgumentNullException">
        /// Thrown if the <paramref name="reader"/> or <paramref name="database"/>
        /// parameter is null.
        /// </exception>
        public TrackGroup(XmlReader reader)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }

            this.Tracks = new ObservableCollection<Track>();
            this.RenderLines = true;
            this.Enabled = true;
            this.Deserialise(reader);
        }

        /// <summary>
        /// Serialises this instance to the given System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that this instance should be serialised to.
        /// </param>
        public void Serialise(XmlWriter writer)
        {

            if (writer == null)
            {
                return;
            }

            writer.WriteStartElement("TrackGroup");
            writer.WriteAttributeString("Name", this.Name);
            writer.WriteAttributeString("Visible", this.Visible.ToString());
            writer.WriteAttributeString("Enabled", this.Enabled.ToString());
            writer.WriteAttributeString("RenderLines", this.RenderLines.ToString());

            if (this.Tracks != null)
            {
                writer.WriteStartElement("Tracks");
                foreach (Track track in this.Tracks)
                {
                    track.Serialise(writer);                
                }
                writer.WriteEndElement();
            }

            writer.WriteEndElement();
        }

        /// <summary>
        /// Uses the specified System.Xml.XmlReader to initialise the fields for this instance.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides the data used to initialise this instance.
        /// </param>
        private void Deserialise(XmlReader reader)
        {
            if (reader == null)
            {
                return;
            }
            
            this.Name = reader.GetAttribute("Name");
            this._Visible = bool.Parse(reader.GetAttribute("Visible"));
            this.Enabled = bool.Parse(reader.GetAttribute("Enabled"));
            this.RenderLines = bool.Parse(reader.GetAttribute("RenderLines"));

            if (reader.IsEmptyElement)
            {
                reader.Skip();
                return;
            }

            reader.ReadStartElement();
            while (reader.MoveToContent() != XmlNodeType.EndElement)
            {
                if (!reader.IsStartElement())
                {
                    reader.Read();
                    continue;
                }

                if (string.CompareOrdinal(reader.Name, "Tracks") == 0)
                {
                    if (!reader.IsEmptyElement)
                    {
                        reader.ReadStartElement();
                        while (reader.MoveToContent() != XmlNodeType.EndElement)
                        {
                            this.Tracks.Add(new Track(reader));
                        }

                        reader.Skip();
                    }
                    else
                    {
                        reader.Skip();
                    }
                }
                else
                {
                    reader.Read();
                }
            }

            reader.Skip();
        }
    }

    public class TrackGroupCollection
    {

        #region Constructors
        public TrackGroupCollection()
        {  
            Children = new ObservableCollection<TrackGroup>();
        }
        #endregion

        #region Properties
       
        public ObservableCollection<TrackGroup> Children { get; set; }
      
        #endregion Properties

        #region Public Functions
        public void Add(TrackGroup tg)
        {
            Children.Add( tg );
        }

        public void Remove(TrackGroup tg)
        {
            Children.Remove( tg );
        }

        public TrackGroup GetTrackGroup(int idx)
        {
            Debug.Assert(idx < Children.Count, "TrackGroupCollection: Index out of range");
            return Children[idx];
        }

        #endregion // Public Functions

    }
}
