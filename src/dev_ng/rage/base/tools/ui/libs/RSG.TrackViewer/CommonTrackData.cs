﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.ComponentModel;
using System.Diagnostics;

using RSG.TrackViewer.Data;

namespace RSG.TrackViewer
{
    /// <summary>
    /// Common data class for sharing across layers
    /// </summary>
    public class CommonTrackData : INotifyPropertyChanged
    {
        #region Contructors
        public CommonTrackData(WindowedTimeLine VertTimeline, WindowedTimeLine HorizTimeline)
        {
            SelectedPoints = new ObservableCollection<TrackPoint>();
            SelectedPoint = new FloatTrackPoint(0.0, 0.0);
            SelectedPoints.Add(SelectedPoint);

            _VertTimeline = VertTimeline;
            _HorizTimeline = HorizTimeline;
        }
        #endregion // Contructors

        #region Properties
        WindowedTimeLine _VertTimeline;
        WindowedTimeLine _HorizTimeline;

        public event PropertyChangedEventHandler PropertyChanged;

        public void Notify(string propName)
        {

            if (this.PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propName));
            }
        }

        public double XMin 
        {
            get
            {
                if (_HorizTimeline == null)
                    return 0.0;
                else
                    return _HorizTimeline.DisplayMin;
            }
            set
            {
                if (value != _HorizTimeline.DisplayMin)
                {
                    _HorizTimeline.DisplayMin = value;
                    Notify("XMin");
                    Notify("SelectedPoint");
                }
            }
        }

        public double XMax
        {
            get
            {
                if (_HorizTimeline == null)
                    return 1.0;
                else
                    return _HorizTimeline.DisplayMax;
            }
            set
            {
                if (value != _HorizTimeline.DisplayMax)
                {
                    _HorizTimeline.DisplayMax = value;
                    Notify("XMax");
                    Notify("SelectedPoint");
                }
            }
        }


        public double YMin
        {
            get
            {
                if (_VertTimeline == null)
                    return 0.0;
                else
                    return _VertTimeline.DisplayMin;
            }
            set
            {
                if (value != _VertTimeline.DisplayMin)
                {
                    _VertTimeline.DisplayMin = value;
                    Notify("YMin");
                    Notify("SelectedPoint");
                }
            }
        }

        public double YMax
        {
            get
            {
                if (_VertTimeline == null)
                    return 1.0;
                else
                    return _VertTimeline.DisplayMax;
            }
            set
            {
                if (value != _VertTimeline.DisplayMax)
                {
                    _VertTimeline.DisplayMax = value;
                    Notify("YMax");
                    Notify("SelectedPoint");
                }
            }
        }

        /// <summary>
        /// Dirty flag
        /// </summary>
        public bool IsDirty
        {
            get
            {
                return _IsDirty;
            }
            set
            {
                if (_IsDirty != value)
                    _IsDirty = value;
                Notify("IsDirty");
            }
        }
        bool _IsDirty;

        public ObservableCollection<TrackPoint> SelectedPoints { get; private set; }
        public TrackPoint SelectedPoint
        {
            get
            {
                return _SelectedPoint;
            }
            set
            {
                if (_SelectedPoint != value)
                {
                    _SelectedPoint = value;
                    Notify("SelectedPoint");
                }
            }
        }
        TrackPoint _SelectedPoint;
        #endregion // Properties

        #region Methods
        public void SetXRange(float min, float max)
        {
            Debug.Assert(max >= min, "Min is greater than max");
            XMin = min;
            XMax = max;
        }

        public void SetYRange(float min, float max)
        {
            Debug.Assert(max >= min, "Min is greater than max");
            YMin = min;
            YMax = max;
        }
        #endregion // Methods

    }
}
