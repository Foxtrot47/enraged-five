﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace RSG.TrackViewer
{
    /// <summary>
    /// Interaction logic for TrackGroupControl.xaml
    /// </summary>
    public partial class TrackGroupControl : UserControl
    {
        public TrackGroupControl()
        {
            InitializeComponent();
        }

        protected override void OnPreviewMouseDown(MouseButtonEventArgs e)
        {
        
        }
    }
}
