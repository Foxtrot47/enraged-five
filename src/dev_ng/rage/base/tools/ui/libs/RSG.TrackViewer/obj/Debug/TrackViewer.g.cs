﻿#pragma checksum "..\..\TrackViewer.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "3D622B44BF6ED7373B032D4EEDCC19C853EC3B0E373BB4804E48B359B7A1D2BE"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using RSG.Base;
using RSG.TrackViewer;
using RSG.TrackViewer.ViewModel;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace RSG.TrackViewer {
    
    
    /// <summary>
    /// TrackViewer
    /// </summary>
    public partial class TrackViewer : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 58 "..\..\TrackViewer.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid ParentGrid;
        
        #line default
        #line hidden
        
        
        #line 80 "..\..\TrackViewer.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal RSG.TrackViewer.Timespan _HorizTimespan;
        
        #line default
        #line hidden
        
        
        #line 83 "..\..\TrackViewer.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal RSG.TrackViewer.Timespan _VertTimespan;
        
        #line default
        #line hidden
        
        
        #line 86 "..\..\TrackViewer.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal RSG.TrackViewer.Grid _Grid;
        
        #line default
        #line hidden
        
        
        #line 89 "..\..\TrackViewer.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal RSG.TrackViewer.KeyDataPresenter _KeyDataPres;
        
        #line default
        #line hidden
        
        
        #line 92 "..\..\TrackViewer.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Primitives.ScrollBar VertScrollbar;
        
        #line default
        #line hidden
        
        
        #line 93 "..\..\TrackViewer.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Primitives.ScrollBar HorizScrollbar;
        
        #line default
        #line hidden
        
        
        #line 96 "..\..\TrackViewer.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal RSG.TrackViewer.DataLayer _DataLayer;
        
        #line default
        #line hidden
        
        
        #line 102 "..\..\TrackViewer.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ScrollViewer TreeScroller;
        
        #line default
        #line hidden
        
        
        #line 104 "..\..\TrackViewer.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TreeView TrackTreeView;
        
        #line default
        #line hidden
        
        
        #line 137 "..\..\TrackViewer.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.GridSplitter gridSplitter1;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/RSG.TrackViewer;component/trackviewer.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\TrackViewer.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal System.Delegate _CreateDelegate(System.Type delegateType, string handler) {
            return System.Delegate.CreateDelegate(delegateType, this, handler);
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            
            #line 17 "..\..\TrackViewer.xaml"
            ((System.Windows.Input.CommandBinding)(target)).CanExecute += new System.Windows.Input.CanExecuteRoutedEventHandler(this.DeleteCanExecute);
            
            #line default
            #line hidden
            
            #line 17 "..\..\TrackViewer.xaml"
            ((System.Windows.Input.CommandBinding)(target)).Executed += new System.Windows.Input.ExecutedRoutedEventHandler(this.DeleteExecuted);
            
            #line default
            #line hidden
            return;
            case 2:
            
            #line 18 "..\..\TrackViewer.xaml"
            ((System.Windows.Input.CommandBinding)(target)).CanExecute += new System.Windows.Input.CanExecuteRoutedEventHandler(this.ZoomCanExecute);
            
            #line default
            #line hidden
            
            #line 18 "..\..\TrackViewer.xaml"
            ((System.Windows.Input.CommandBinding)(target)).Executed += new System.Windows.Input.ExecutedRoutedEventHandler(this.ZoomExecuted);
            
            #line default
            #line hidden
            return;
            case 3:
            this.ParentGrid = ((System.Windows.Controls.Grid)(target));
            return;
            case 4:
            this._HorizTimespan = ((RSG.TrackViewer.Timespan)(target));
            return;
            case 5:
            this._VertTimespan = ((RSG.TrackViewer.Timespan)(target));
            return;
            case 6:
            this._Grid = ((RSG.TrackViewer.Grid)(target));
            return;
            case 7:
            this._KeyDataPres = ((RSG.TrackViewer.KeyDataPresenter)(target));
            return;
            case 8:
            this.VertScrollbar = ((System.Windows.Controls.Primitives.ScrollBar)(target));
            
            #line 92 "..\..\TrackViewer.xaml"
            this.VertScrollbar.Scroll += new System.Windows.Controls.Primitives.ScrollEventHandler(this.VertScroll_Changed);
            
            #line default
            #line hidden
            return;
            case 9:
            this.HorizScrollbar = ((System.Windows.Controls.Primitives.ScrollBar)(target));
            
            #line 93 "..\..\TrackViewer.xaml"
            this.HorizScrollbar.Scroll += new System.Windows.Controls.Primitives.ScrollEventHandler(this.HorizScroll_Changed);
            
            #line default
            #line hidden
            return;
            case 10:
            this._DataLayer = ((RSG.TrackViewer.DataLayer)(target));
            return;
            case 11:
            this.TreeScroller = ((System.Windows.Controls.ScrollViewer)(target));
            return;
            case 12:
            this.TrackTreeView = ((System.Windows.Controls.TreeView)(target));
            
            #line 104 "..\..\TrackViewer.xaml"
            this.TrackTreeView.AddHandler(System.Windows.Controls.TreeViewItem.SelectedEvent, new System.Windows.RoutedEventHandler(this.InvalidateDataRepresentation));
            
            #line default
            #line hidden
            return;
            case 13:
            this.gridSplitter1 = ((System.Windows.Controls.GridSplitter)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

