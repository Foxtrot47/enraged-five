﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Controls.Primitives;

using RSG.TrackViewer.Data;
using RSG.TrackViewer.ViewModel;

namespace RSG.TrackViewer
{
    /// <summary>
    /// Main user control for the TrackViewer.  Glues the other controls together via XAML and the TrackManager singleton
    /// </summary>
    public partial class TrackViewer : UserControl
    {
        public TrackViewer()
        {
            InitializeComponent();
            
            _KeyDataPres.Init(TM.CommonData);

            // Deal with scrollbar virtualisation
            SetThumbLength(VertScrollbar, 1.0);
            SetThumbLength(HorizScrollbar, 1.0);
            HorizScrollOldValue = GetThumbCenter(HorizScrollbar);
            VertScrollOldValue = GetThumbCenter(VertScrollbar);

            TM.TrackGroupCollectionViewModel = new TrackGroupCollectionViewModel();

            this.DataContext = TM;
            this.TrackTreeView.Loaded += new RoutedEventHandler(TrackTreeView_Loaded);
            this.Focusable = true;
        }



        #region Properties

        public TrackManager TM = TrackManager.Instance;
        
        // true only while left ctrl key is pressed
        bool CtrlPressed
        {
            get
            {
                return System.Windows.Input.Keyboard.IsKeyDown(Key.LeftCtrl);
            }
        }

        // a set of all selected items
        Dictionary<TreeViewItem, string> SelectedItems =
            new Dictionary<TreeViewItem, string>();

        double HorizScrollOldValue { get; set; }
        double VertScrollOldValue { get; set; }

        #endregion // Members    
        #region Event Handlers

        bool PointHit = false;
        bool PointAdded = false;
        bool LeftButtonHeld = false;
        protected override void OnMouseDown(MouseButtonEventArgs e)
        {
            
            base.OnMouseDown(e);
            this.Focus();

            Point absPos = e.GetPosition(_DataLayer);
            if (absPos.X <= _DataLayer.ActualWidth && absPos.Y <= _DataLayer.ActualHeight)
            {
                if (e.ClickCount == 2)
                {
                    foreach (TrackGroupViewModel tg in TM.TrackGroupCollectionViewModel.TrackGroupViewModels)
                    {
                        foreach (TrackViewModel t in tg.TrackViewModels)
                        {
                            if ((t.Visible || tg.Visible) && !PointAdded)
                            {

                                t.Track.AddPoint(new FloatTrackPoint(TM.HorizTimeline.PixelToPhase(absPos.X), TM.VertTimeline.PixelToPhase(absPos.Y)));
                                PointAdded = true;
                            }
                        }
                    }
                    _DataLayer.SortTracks();
                    _DataLayer.InvalidateVisual();
                }
                else
                {
                    switch (e.ChangedButton)
                    {
                        // What drugs was I taking when I wrote this?
                        case MouseButton.Left:
                            Point phasePoint = new Point(_DataLayer.HorizTimeline.PixelToPhase(absPos.X), _DataLayer.VertTimeline.PixelToPhase(absPos.Y));
                            if (TM.CommonData.SelectedPoints.Count() > 1 && _DataLayer.SetSelected(phasePoint, false))
                            {
                                _DataLayer.InvalidateVisual();
                                PointHit = true;
                            }
                            else if (TM.CommonData.SelectedPoints.Count <= 1 && _DataLayer.SetSelected(phasePoint, true))
                            {
                                _DataLayer.InvalidateVisual();
                                PointHit = true;
                            }
                            else
                            {
                                _DataLayer.InvalidateVisual();
                                _DataLayer._SelLayer.Origin = e.GetPosition(_DataLayer);
                                PointHit = false;
                            }
                            LeftButtonHeld = false;
                            break;
                        case MouseButton.Middle:

                            break;
                        case MouseButton.Right:

                            break;
                    }
                }
            }
            e.Handled = true;
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);
            Point absPos = e.GetPosition(_DataLayer);
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                if (PointHit)
                {
                    if (LeftButtonHeld)
                    {
                        Point phasePoint = new Point(TM.HorizTimeline.PixelToPhase(absPos.X), TM.VertTimeline.PixelToPhase(absPos.Y));
                        _DataLayer.UpdatePositionForSelection(phasePoint);
                        _DataLayer.SortTracks();
                        _DataLayer.InvalidateVisual();
                    }
                    else
                    {
                        LeftButtonHeld = true;
                    }
                }
                else if(!PointAdded)
                {
                    _DataLayer._SelLayer.Extent = absPos;
                    _DataLayer._SelLayer.InvalidateVisual();
                }
            }
            e.Handled = true;
        }


        protected override void OnMouseUp(MouseButtonEventArgs e)
        {
            base.OnMouseUp(e);
            Point absPos = e.GetPosition(_DataLayer);
            //if (absPos.X <= _DataLayer.ActualWidth && absPos.Y <= _DataLayer.ActualHeight)
            {
                switch (e.ChangedButton)
                {
                    case MouseButton.Left:
                        if (!PointHit && !PointAdded)
                        {

                            _DataLayer._SelLayer.Extent = e.GetPosition(_DataLayer);
                            Point phaseOrigin = new Point(TM.HorizTimeline.PixelToPhase(_DataLayer._SelLayer.Origin.X), TM.VertTimeline.PixelToPhase(_DataLayer._SelLayer.Origin.Y));
                            Point phaseExtent = new Point(TM.HorizTimeline.PixelToPhase(_DataLayer._SelLayer.Extent.X), TM.VertTimeline.PixelToPhase(_DataLayer._SelLayer.Extent.Y));
                            if (_DataLayer.SetSelected(new Rect(phaseOrigin, phaseExtent)))
                                InvalidateVisual();
                            _DataLayer._SelLayer.Reset();
                            _DataLayer._SelLayer.InvalidateVisual();
                            _DataLayer.InvalidateVisual();

                        }
                        break;
                    case MouseButton.Middle:

                        break;
                    case MouseButton.Right:

                        break;
                }
            }
            
            LeftButtonHeld = false;
            PointHit = false;
            PointAdded = false;
            TM.CommonData.IsDirty = true;
            e.Handled = true;
        }

        protected override void OnPreviewMouseUp(MouseButtonEventArgs e)
        {
            base.OnPreviewMouseUp(e);

            if (_DataLayer._SelLayer.Origin != _DataLayer._SelLayer.Extent)
            {
                Point absPos = e.GetPosition(_DataLayer);
                //if (absPos.X <= _DataLayer.ActualWidth && absPos.Y <= _DataLayer.ActualHeight)
                {
                    switch (e.ChangedButton)
                    {
                        case MouseButton.Left:
                        if (!PointHit && !PointAdded)
                        {

                            _DataLayer._SelLayer.Extent = e.GetPosition(_DataLayer);
                            Point phaseOrigin = new Point(TM.HorizTimeline.PixelToPhase(_DataLayer._SelLayer.Origin.X), TM.VertTimeline.PixelToPhase(_DataLayer._SelLayer.Origin.Y));
                            Point phaseExtent = new Point(TM.HorizTimeline.PixelToPhase(_DataLayer._SelLayer.Extent.X), TM.VertTimeline.PixelToPhase(_DataLayer._SelLayer.Extent.Y));
                            if (_DataLayer.SetSelected(new Rect(phaseOrigin, phaseExtent)))
                                InvalidateVisual();
                            _DataLayer._SelLayer.Reset();
                            _DataLayer._SelLayer.InvalidateVisual();
                            _DataLayer.InvalidateVisual();

                        }
                        break;
                        case MouseButton.Middle:

                        break;
                        case MouseButton.Right:

                        break;
                    }
                }

                LeftButtonHeld = false;
                PointHit = false;
                PointAdded = false;
                TM.CommonData.IsDirty = true;
                e.Handled = true;
            }
        }


        private void DeleteCanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
            e.Handled = true;
        }

        private void DeleteExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            foreach (TrackPoint tp in TM.CommonData.SelectedPoints)
            {
                if(!tp.IsLocked && !tp.IsLockedX && !tp.IsLockedY)
                    TM.FindAndDeletePoint(tp);
            }

            _DataLayer.InvalidateVisual();
            e.Handled = true;
        }

        private void ZoomCanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
            e.Handled = true;
        }

        private void ZoomExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            FramePoints(false);

            e.Handled = true;
        }

        private void TrackTreeView_Loaded(object sender, RoutedEventArgs e)
        {
            TreeViewItem item = ((TreeView)e.Source).ItemContainerGenerator.ContainerFromIndex(0) as TreeViewItem;
            if (item != null)
            {
                item.IsSelected = true;
            }
        }

        /// <summary>
        /// Handler used to support multi-selection.  Not full implemented at this stage
        /// </summary>
        void TrackTreeView_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            /*
            TreeViewItem treeViewItem = TrackTreeView.SelectedItem as TreeViewItem;
            if (treeViewItem == null)
                return;

            // prevent the WPF tree item selection 
            treeViewItem.IsSelected = false;

            treeViewItem.Focus();

            if (!CtrlPressed)
            {
                List<TreeViewItem> selectedTreeViewItemList = new List<TreeViewItem>();
                foreach (TreeViewItem treeViewItem1 in SelectedItems.Keys)
                {
                    selectedTreeViewItemList.Add(treeViewItem1);
                }

                foreach (TreeViewItem treeViewItem1 in selectedTreeViewItemList)
                {
                    Deselect(treeViewItem1);
                }
            }

            ChangeSelectedState(treeViewItem);
             * */
        }

        /// <summary>
        /// Invalidate the data layer to force re-draw
        /// </summary>
        void InvalidateDataRepresentation(object sender, EventArgs e)
        {
            _DataLayer.InvalidateVisual();
        }

        static bool sMouseWheel = false;
        private void VertScroll_Changed(object sender, ScrollEventArgs e)
        {
            if (!sMouseWheel)
            {
                double delta = VertScrollOldValue - e.NewValue;
                double min = (TM.VertTimeline.DisplayViewMin / (TM.VertTimeline.DisplayMax - TM.VertTimeline.DisplayMin)) - delta;
                double max = (TM.VertTimeline.DisplayViewMax / (TM.VertTimeline.DisplayMax - TM.VertTimeline.DisplayMin)) - delta;

                VertScrollOldValue = e.NewValue;

                ClampAndSetPhaseView(min, max, Axis.Y, false);
            }
            else
                sMouseWheel = false;
        }

        private void HorizScroll_Changed(object sender, ScrollEventArgs e)
        {
            if (!sMouseWheel)
            {
                double delta = HorizScrollOldValue - e.NewValue;
                delta *= (TM.HorizTimeline.DisplayViewMax - TM.HorizTimeline.DisplayViewMin);
                double min = (TM.HorizTimeline.DisplayViewMin / (TM.HorizTimeline.DisplayMax - TM.HorizTimeline.DisplayMin)) - delta;
                double max = (TM.HorizTimeline.DisplayViewMax / (TM.HorizTimeline.DisplayMax - TM.HorizTimeline.DisplayMin)) - delta;

                HorizScrollOldValue = e.NewValue;

                ClampAndSetPhaseView(min, max, Axis.X, false);
            }
            else
                sMouseWheel = false;
        }

        
        protected override void OnPreviewMouseWheel(MouseWheelEventArgs e)
        {
            sMouseWheel = true;

            base.OnPreviewMouseWheel(e);

            double newHorizMin = TM.HorizTimeline.PhaseViewMin;
            double newHorizMax = TM.HorizTimeline.PhaseViewMax;

            double newVertMin = TM.VertTimeline.PhaseViewMin;
            double newVertMax = TM.VertTimeline.PhaseViewMax;

            double scale = 0.0;
            if (0 < e.Delta)
            {
                scale = 0.9;
            }
            else
            {
                scale = 1.1;
            }

            double oldHorizHalfRange = TM.HorizTimeline.PhaseViewRange * 0.5;
            double newHorizHalfRange = oldHorizHalfRange * scale;

            double oldVertHalfRange = TM.VertTimeline.PhaseViewRange * 0.5;
            double newVertHalfRange = oldVertHalfRange * scale;

            double phaseHorizViewMid = TM.HorizTimeline.PhaseViewMin + oldHorizHalfRange;
            double phaseVertViewMid = TM.VertTimeline.PhaseViewMin + oldVertHalfRange;

            newHorizMin = phaseHorizViewMid - newHorizHalfRange;
            newHorizMax = phaseHorizViewMid + newHorizHalfRange;

            newVertMin = phaseVertViewMid - newVertHalfRange;
            newVertMax = phaseVertViewMid + newVertHalfRange;

            if (newHorizHalfRange > (TM.HorizTimeline.PhaseMax - TM.HorizTimeline.PhaseMin) * 0.45)
            { // if we're within 90% of all the way zoomed out, zoom all the way out
                newHorizMin = TM.HorizTimeline.PhaseMin;
                newHorizMax = TM.HorizTimeline.PhaseMax;
            }

            if (newVertHalfRange > (TM.VertTimeline.PhaseMax - TM.VertTimeline.PhaseMin) * 0.45)
            { // if we're within 90% of all the way zoomed out, zoom all the way out
                newVertMin = TM.VertTimeline.PhaseMin;
                newVertMax = TM.VertTimeline.PhaseMax;
            }
 
            ClampAndSetPhaseView(newHorizMin, newHorizMax, Axis.X, true);
            ClampAndSetPhaseView(newVertMin, newVertMax, Axis.Y, true);

            e.Handled = true;
        }
        

        #endregion // Event handlers
        #region Public Methods
        /// <summary>
        /// Frame the viewport around points that are currently visible in available tracks
        /// </summary>
        public void FramePoints(bool selected)
        {
            double xmin = 1.0;
            double xmax = 0.0;
            double ymin = 1.0;
            double ymax = 0.0;

            foreach (TrackGroupViewModel tg in TM.TrackGroupCollectionViewModel.TrackGroupViewModels)
            {
                foreach (TrackViewModel t in tg.TrackViewModels)
                {
                    if (t.Visible || tg.Visible)
                    {
                        foreach (TrackPoint tp in t.Track.Points)
                        {
                            if (tp.X > xmax)
                                xmax = tp.X;
                            if (tp.X < xmin)
                                xmin = tp.X;
                            if (tp.Y > ymax)
                                ymax = tp.Y;
                            if (tp.Y < ymin)
                                ymin = tp.Y;
                        }
                    }
                }
            }

            if (xmin >= xmax)
            {
                xmin = 0.0;
                xmax = 1.0;
            }

            if (ymin >= ymax)
            {
                ymin = 0.0;
                ymax = 1.0;
            }

            ClampAndSetPhaseView(xmin, xmax, Axis.X, true);
            ClampAndSetPhaseView(ymin, ymax, Axis.Y, true);

        }
        #endregion

        #region Timeline Functions

        // Makes sure start and stop fit within the view range, adjusts them if they don't,
        // and sets the view.
        void ClampAndSetPhaseView(double start, double stop, Axis axis, bool resize)
        {
            WindowedTimeLine timeline = null;
            if (axis == Axis.X)
                timeline = TM.HorizTimeline;
            else if (axis == Axis.Y)
                timeline = TM.VertTimeline;

            double diff = stop - start;
            if (start < timeline.PhaseMin)
            {
                start = timeline.PhaseMin;
                stop = start + diff;
            }

            if (stop > timeline.PhaseMax)
            {
                stop = timeline.PhaseMax;
                start = stop - diff;
            }

            start = Math.Max(timeline.PhaseMin, start);
            stop = Math.Min(timeline.PhaseMax, stop);

            timeline.SetPhaseView(start, stop);

            if (resize)
            {
                if (axis == Axis.X)
                {
                    SetThumbLength(HorizScrollbar, (stop-start));
                    SetThumbCenter(HorizScrollbar, ((timeline.PhaseViewMax - timeline.PhaseViewMin) / 2.0) + timeline.PhaseViewMin);
                }
                else if (axis == Axis.Y)
                {
                    SetThumbLength(VertScrollbar, (stop - start));
                    SetThumbCenter(VertScrollbar, ((timeline.PhaseViewMax - timeline.PhaseViewMin) / 2.0) + timeline.PhaseViewMin);
                }
            }

            if (axis == Axis.X)
                _HorizTimespan.InvalidateVisual();
            else if (axis == Axis.Y)
                _VertTimespan.InvalidateVisual();

           
            _Grid.InvalidateVisual();
            _DataLayer.InvalidateVisual();
        }

        #endregion
        #region Command Handlers

        private void navigationCommandZoom_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            if (e.Parameter == null)
            {
                e.CanExecute = false;
                return;
            }

            string scaleString = (string)e.Parameter;
            if (scaleString == "Fit")
            {
                e.CanExecute = TM.HorizTimeline.PhaseViewMin != TM.HorizTimeline.PhaseMin || TM.HorizTimeline.PhaseViewMax != TM.HorizTimeline.PhaseMax ||
                                TM.VertTimeline.PhaseViewMin != TM.VertTimeline.PhaseMin || TM.VertTimeline.PhaseViewMax != TM.VertTimeline.PhaseMax;
                return;
            }

            double scale = Double.Parse(scaleString);

            double newHorizRange = TM.HorizTimeline.PhaseViewRange * scale;

            if ((scale < 1 && newHorizRange < 0.001) ||
                (scale > 1 && TM.HorizTimeline.PhaseViewRange >= TM.HorizTimeline.PhaseMax - TM.HorizTimeline.PhaseMin))
            {
                e.CanExecute = false;
                return;
            }

            double newVertRange = TM.VertTimeline.PhaseViewRange * scale;

            if ((scale < 1 && newVertRange < 0.001) ||
                (scale > 1 && TM.VertTimeline.PhaseViewRange >= TM.VertTimeline.PhaseMax - TM.VertTimeline.PhaseMin))
            {
                e.CanExecute = false;
                return;
            }

            // Can't execute when there's no data to zoom
            if (!TrackTreeView.HasItems)
            {
                e.CanExecute = false;
                return;
            }

            e.CanExecute = true;
        }

        private void navigationCommandZoom_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            double newHorizMin = TM.HorizTimeline.PhaseViewMin;
            double newHorizMax = TM.HorizTimeline.PhaseViewMax;

            double newVertMin = TM.VertTimeline.PhaseViewMin;
            double newVertMax = TM.VertTimeline.PhaseViewMax;

            string scaleString = (string)e.Parameter;
            if (scaleString == "Fit")
            {
                newHorizMin = TM.HorizTimeline.PhaseMin;
                newHorizMax = TM.HorizTimeline.PhaseMax;

                newVertMin = TM.VertTimeline.PhaseMin;
                newVertMax = TM.VertTimeline.PhaseMax;
            }
            else
            {
                double scale = Double.Parse(scaleString);

                double oldHorizHalfRange = TM.HorizTimeline.PhaseViewRange * 0.5;
                double newHorizHalfRange = oldHorizHalfRange * scale;

                double oldVertHalfRange = TM.VertTimeline.PhaseViewRange * 0.5;
                double newVertHalfRange = oldVertHalfRange * scale;

                double phaseHorizViewMid = TM.HorizTimeline.PhaseViewMin + oldHorizHalfRange;
                double phaseVertViewMid = TM.VertTimeline.PhaseViewMin + oldVertHalfRange;

                newHorizMin = phaseHorizViewMid - newHorizHalfRange;
                newHorizMax = phaseHorizViewMid + newHorizHalfRange;

                newVertMin = phaseVertViewMid - newVertHalfRange;
                newVertMax = phaseVertViewMid + newVertHalfRange;

                if (newHorizHalfRange > (TM.HorizTimeline.PhaseMax - TM.HorizTimeline.PhaseMin) * 0.45)
                { // if we're within 90% of all the way zoomed out, zoom all the way out
                    newHorizMin = TM.HorizTimeline.PhaseMin;
                    newHorizMax = TM.HorizTimeline.PhaseMax;
                }

                if (newVertHalfRange > (TM.VertTimeline.PhaseMax - TM.VertTimeline.PhaseMin) * 0.45)
                { // if we're within 90% of all the way zoomed out, zoom all the way out
                    newVertMin = TM.VertTimeline.PhaseMin;
                    newVertMax = TM.VertTimeline.PhaseMax;
                }
 
                /*
                double oldHalfRange = _Timeline.PhaseViewRange * 0.5;
                double newHalfRange = oldHalfRange * scale;

                double phaseViewMid = _Timeline.PhaseViewMin + oldHalfRange;

                newMin = phaseViewMid - newHalfRange;
                newMax = phaseViewMid + newHalfRange;

                if (newHalfRange > (_Timeline.PhaseMax - _Timeline.PhaseMin) * 0.45)
                { // if we're within 90% of all the way zoomed out, zoom all the way out
                    newMin = _Timeline.PhaseMin;
                    newMax = _Timeline.PhaseMax;
                }
                 * */
            }

            ClampAndSetPhaseView(newHorizMin, newHorizMax, Axis.X, true);
            ClampAndSetPhaseView(newVertMin, newVertMax, Axis.Y, true);
            //ClampAndSetPhaseView(newMin, newMax);
            //UpdateIntervalsForNewPhaseView();
        }
        #endregion // Command Handlers
        #region Scrollbar Utilies
        public static double GetThumbCenter(ScrollBar s)
        {
            double thumbLength = GetThumbLength(s);
            double trackLength = s.Maximum - s.Minimum;

            return thumbLength / 2 + s.Minimum + (s.Value - s.Minimum) *
                (trackLength - thumbLength) / trackLength;
        }

        public static void SetThumbCenter(ScrollBar s, double thumbCenter)
        {
            double thumbLength = GetThumbLength(s);
            double trackLength = s.Maximum - s.Minimum;

            if (thumbCenter >= s.Maximum - thumbLength / 2)
            {
                s.Value = s.Maximum;
            }
            else if (thumbCenter <= s.Minimum + thumbLength / 2)
            {
                s.Value = s.Minimum;
            }
            else if (thumbLength >= trackLength)
            {
                s.Value = s.Minimum;
            }
            else
            {
                s.Value = s.Minimum + trackLength *
                    ((thumbCenter - s.Minimum - thumbLength / 2)
                    / (trackLength - thumbLength));
            }
        }

        public static double GetThumbLength(ScrollBar s)
        {
            double trackLength = s.Maximum - s.Minimum;
            return trackLength * s.ViewportSize /
                (trackLength + s.ViewportSize);
        }

        public static void SetThumbLength(ScrollBar s, double thumbLength)
        {
            double trackLength = s.Maximum - s.Minimum;

            if (thumbLength < 0)
            {
                s.ViewportSize = 0;
            }
            else if (thumbLength < trackLength)
            {
                s.ViewportSize = trackLength * thumbLength / (trackLength - thumbLength);
            }
            else
            {
                s.ViewportSize = double.MaxValue;
            }
        }
        #endregion // Scrollbar Utilies
       
        public float _Width = 0;
        public float _Height = 0;
    }
}
