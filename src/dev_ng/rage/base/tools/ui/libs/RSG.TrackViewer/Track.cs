﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Media;
using System.Windows.Controls;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace RSG.TrackViewer
{
    public class Track //: TreeViewItem
    {
        #region Constructors
        public Track(string name)
        {
            Points = new List<TrackPoint>();
            AllowDuplicates = false;
            Visible = false;
            KeyPenWidth = 0.5f;
            CurvePenWidth = 1.0f;
            Colour = Brushes.Red;
            Name = name;
            //Header = name;
        }

        internal Track(List<TrackPoint> points)
        {
            Points = points;
            AllowDuplicates = false;
            Visible = false;
        }
        #endregion // Constructors

        #region Properties

        
        public bool Displayed { get; set; }
        public float KeyPenWidth { get; set; }
        public float CurvePenWidth { get; set; }
        public Brush Colour { get; set; }
        public string Name 
        { 
            get
            {
                return _Name;
            }
            set
            {
                if (_Name != value)
                    _Name = value;
            }
        }
        string _Name;
        public override string ToString()
        {
            if (Name != String.Empty)
            {
                return Name;
            }
            return base.ToString();
        }

        public string ShortName
        {
            get
            {
                return ToString();
            }
        }


        public List<TrackPoint> Points { get; private set; }
        
        bool AllowDuplicates { get; set; }

        public bool Visible 
        {
            get
            {
                return _Visible;
            }
            set
            {
                if (_Visible != value)
                {
                    _Visible = value;
                }
            }
        }
        bool _Visible;

        #endregion // Properties

        #region Public Functions
        
        /// <summary>
        /// NumPoints
        /// </summary>
        public int NumPoints()
        {
            return Points.Count;
        }

        /// <summary>
        /// GetPoint
        /// </summary>
        public TrackPoint GetPoint(int idx)
        {
            return Points[idx];
        }

        /// <summary>
        /// Get certain Y value for given X value (usually time)
        /// </summary>
        public float GetYValAt(int index, float val)
        {
            if (index >= Points.Count || (index - 1 < 0))
            {
                TrackPoint keyframe = Points[Points.Count - 1];
                return (float)keyframe.Y;
            }
            else
            {
                TrackPoint keyframe = Points[index];
                if (keyframe.X == val)
                {
                    return (float)keyframe.Y;
                }
                else
                {
                    // find neighbors, lerp
                    TrackPoint prev = Points[index - 1];

                    float dx = (float)(keyframe.X - prev.X);
                    float dy = (float)(keyframe.Y - prev.Y);

                    float t = (float)((val - prev.X) / dx);
                    return ((float)(t * dy + prev.Y));
                }
            }
        }

        /// <summary>
        /// Add a point to a track.  Dont use this to construct the point list
        /// if you know the point list at initialisation since it will force a 
        /// re-draw everytime a point is added.  Use the constuctor that takes a list of points
        /// </summary>
        /// <param name="point"></param>
        public bool AddPoint(TrackPoint point)
        {
            if (!AllowDuplicates)
            {
                if (null == Points.Find(delegate(TrackPoint p) { return p.X == point.X && p.Y == point.Y; }))
                    Points.Add(point);
                else
                    return false;
            }
            else
                Points.Add(point);

            return true;
        }

        /// <summary>
        /// Remove a point from the point list (thanks captain obvious)
        /// </summary>
        /// <param name="point"></param>
        public bool RemovePoint(TrackPoint point)
        {
            return Points.Remove(point);
        }

        /// <summary>
        /// Remove a point from the point list (thanks captain obvious)
        /// </summary>
        /// <param name="point"></param>
        public void RemovePointAt(int idx)
        {
            Points.RemoveAt(idx);
        }

        /// <summary>
        /// Sort function, currently assumes sorting along the X (usually time) axis.
        /// </summary>
        public void SortPoints()
        {
            Points.Sort(CompareTrackPointByX);
        }
        #endregion // Public Functions

        #region Private Functions
        /// <summary>
        /// Compare function for points in a track (used in a sort)
        /// </summary>
        private static int CompareTrackPointByX( TrackPoint p1, TrackPoint p2 )
        {
            if (p1 == null)
            {
                if (p2 == null)
                    return 0;
                else
                    return -1;
            }
            else
            {
                if (p2 == null)
                    return 1;
                else
                {
                    if (p1.X > p2.X)
                        return 1;
                    else if (p1.X < p2.X)
                        return -1;
                    else
                        return 0;
                }
            }
        }
        #endregion // Public Functions
    }
}
