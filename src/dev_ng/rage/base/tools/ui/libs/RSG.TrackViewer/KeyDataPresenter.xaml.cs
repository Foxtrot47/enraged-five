﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;

namespace RSG.TrackViewer
{
    /// <summary>
    /// Class is for presenting the selected keyframes data as readable data
    /// </summary>
    public partial class KeyDataPresenter : UserControl
    {
        public KeyDataPresenter()
        {
            InitializeComponent();
        }

        public void Init(CommonTrackData data)
        {
            XCoord.PreviewKeyUp += new KeyEventHandler(CoordTextBox_KeyUp);
            YCoord.PreviewKeyUp += new KeyEventHandler(CoordTextBox_KeyUp);
            keyDataGrid.DataContext = data;

            ((PointConverterDataContainer)Resources["_PointConverterDataContainerX"]).CommonData = data;
            ((PointConverterDataContainer)Resources["_PointConverterDataContainerX"]).OpAxis = Axis.X;
            ((PointConverterDataContainer)Resources["_PointConverterDataContainerY"]).CommonData = data;
            ((PointConverterDataContainer)Resources["_PointConverterDataContainerY"]).OpAxis = Axis.Y;
        }

        #region Event Handlers
        protected override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);
            e.Handled = true;
        }

        protected override void OnMouseDown(MouseButtonEventArgs e)
        {
            base.OnMouseDown(e);
            e.Handled = true;
        }

        protected override void OnMouseUp(MouseButtonEventArgs e)
        {
            base.OnMouseUp(e);
            e.Handled = true;
        }

        private void CoordTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                TextBox textBox = e.OriginalSource as TextBox;
                BindingExpression be = textBox.GetBindingExpression(TextBox.TextProperty);
                be.UpdateSource();
            }
        }
        #endregion
    }
}
