﻿using System;
using System.Collections.Generic;

using RSG.TrackViewer.Data;

namespace RSG.TrackViewer.ViewModel
{
    public class TrackViewModel : BaseViewModel
    {
        #region Contructors

        public TrackViewModel(Track track, TrackGroupViewModel parent)
        {
            _track = track;
            Visible = false;
            Parent = parent;
            _track.Changing += new EventHandler(TrackModel_Changed);
        }

        void TrackModel_Changed(object sender, EventArgs e)
        {
            base.OnPropertyChanged("Enabled");
        }


        #endregion // Connstructors

        #region Fields

        readonly Track _track;
        public TrackGroupViewModel Parent;
        #endregion // Fields

        #region Properties
        public Track Track
        {
            get
            {
                return _track;
            }
        }


        public bool Enabled
        {
            get
            {
                return _track.IsActive;
            }

            set
            {
                if (value == _track.IsActive)
                    return;

                _track.IsActive = value;

                base.OnPropertyChanged("Enabled");
            }
        }

        public bool Visible
        {
            get
            {
                return _Visible;
            }

            set
            {
                if (value == _Visible)
                    return;

                _Visible = value;

                base.OnPropertyChanged("Visible");
            }
        }
        bool _Visible;

        public string Name
        {
            get
            {
                return _track.Name;
            }

        }

        public string ShortName
        {
            get
            {
                return _track.ShortName;
            }
            
        }
        #endregion // Track Properties

        #region Methods
        public void FireNameChangedEvent()
        {
            this.OnPropertyChanged("ShortName");
        }
        #endregion
    }
}
