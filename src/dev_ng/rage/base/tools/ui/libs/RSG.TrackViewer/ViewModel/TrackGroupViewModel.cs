﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using RSG.TrackViewer.Data;


namespace RSG.TrackViewer.ViewModel
{
    public class TrackGroupCollectionViewModel : BaseViewModel
    {
        #region Constructors

        public TrackGroupCollectionViewModel()
        {
            _tgc = new TrackGroupCollection();
            TrackGroupViewModels = new ObservableCollection<TrackGroupViewModel>();
            _tgc.Children.CollectionChanged += new NotifyCollectionChangedEventHandler(Children_CollectionChanged);
        }

        #endregion // Constructors

        #region Fields

        TrackGroupCollection _tgc;

        #endregion

        #region Properties

        public ObservableCollection<TrackGroupViewModel> TrackGroupViewModels
        {
            get
            {
                return _TrackGroupViewModels;
            }
            set
            {
                if (_TrackGroupViewModels == value)
                    return;

                _TrackGroupViewModels = value;
            }
        }
        ObservableCollection<TrackGroupViewModel> _TrackGroupViewModels;
        
        #endregion

        #region Public Methods

        public void SetTrackGroupCollection(TrackGroupCollection tgc)
        {
            _tgc = tgc;
            _tgc.Children.CollectionChanged += new NotifyCollectionChangedEventHandler(Children_CollectionChanged);
        }

        #endregion

        #region Event Handlers
        void Children_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null)
            {
                foreach (TrackGroup trackGroup in e.NewItems)
                {
                    TrackGroupViewModels.Add(new TrackGroupViewModel(trackGroup));
                }
            }
        }
        #endregion // Event Handlers
    }

    public class TrackGroupViewModel : BaseViewModel
    {
        #region Constructors
        public TrackGroupViewModel(TrackGroup trackGroup)
        {
            _trackGroup = trackGroup;
            TrackViewModels = new ObservableCollection<TrackViewModel>();
            foreach(Track track in trackGroup.Tracks)
                TrackViewModels.Add(new TrackViewModel(track, this));
            _trackGroup.Tracks.CollectionChanged += new NotifyCollectionChangedEventHandler(Tracks_CollectionChanged);
            _trackGroup.TracksChanged += new EventHandler(_trackGroup_TracksChanged);
        }
        #endregion // Constructors

        #region Fields

        readonly TrackGroup _trackGroup;

        #endregion

        #region TrackGroupViewModel Properties

        public ObservableCollection<TrackViewModel> TrackViewModels
        {
            get
            {
                return _TrackViewModels;
            }
            set
            {
                if (_TrackViewModels == value)
                    return;

                _TrackViewModels = value;
            }
        }
        ObservableCollection<TrackViewModel> _TrackViewModels;

        public string ShortName
        {
            get
            {
                return _trackGroup.ShortName;
            }
        }

        public bool Visible
        {
            get
            {
                return _trackGroup.Visible;
            }
            set
            {
                if (value == _trackGroup.Visible)
                    return;

                _trackGroup.Visible = value;
                base.OnPropertyChanged("Visible");
            }
        }

        public bool Enabled
        {
            get
            {
                return _trackGroup.Enabled;
            }
            set
            {
                if (value == _trackGroup.Enabled)
                    return;

                _trackGroup.Enabled = value;
                base.OnPropertyChanged("Enabled");
            }

        }

        public bool RenderLines
        {
            get
            {
                return _trackGroup.RenderLines;
            }
            set
            {
                if (value == _trackGroup.RenderLines)
                    return;

                _trackGroup.RenderLines = value;
                base.OnPropertyChanged("RenderLines");
            }

        }

        #endregion

        #region Event Handlers
        void Tracks_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Add)
            {
                if (e.NewItems != null)
                {
                    foreach (Track track in e.NewItems)
                    {
                        TrackViewModels.Add(new TrackViewModel(track, this));
                    }
                }
            }
            else if (e.Action == NotifyCollectionChangedAction.Remove)
            {
                if (e.OldItems != null)
                {
                    foreach (Track oldItem in e.OldItems)
                    {
                        TrackViewModel vm = null;
                        foreach (TrackViewModel trackvm in TrackViewModels)
                        {
                            if (trackvm.Track == oldItem)
                            {
                                vm = trackvm;
                                break;
                            }
                        }
                        if (vm != null)
                            TrackViewModels.Remove(vm);
                    }
                }
            }
            else if (e.NewItems != null)
            {
                foreach (Track track in e.NewItems)
                {
                    TrackViewModels.Add(new TrackViewModel(track, this));
                }
            }
        }

        void _trackGroup_TracksChanged(object sender, EventArgs e)
        {
            foreach (TrackViewModel trackvm in TrackViewModels)
            {
                trackvm.FireNameChangedEvent();
            }
        }
        #endregion // Event Handlers
    }

}
