﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace RSG.TrackViewer
{
    /// <summary>
    /// Interaction logic for Grid.xaml
    /// </summary>
    public partial class Grid : ContentControl
    {
        public Grid()
        {
            InitializeComponent();
            BackgroundBrush = Brushes.Black.Clone();
            BackgroundBrush.Opacity = 0.65;
            Loaded += new RoutedEventHandler(Grid_Loaded);
            WidthPlus = Width + 500;
        }

        void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            InvalidateVisual();
        }

        #region Properties

        public static readonly DependencyProperty HorizTimelineProperty = DependencyProperty.Register(
        "HorizTimeline",
        typeof(WindowedTimeLine),
        typeof(Grid),
        new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.AffectsArrange));
        public WindowedTimeLine HorizTimeline 
        {
            get
            {
                return (WindowedTimeLine)GetValue(HorizTimelineProperty);
            }
            set
            {
                SetValue(HorizTimelineProperty, value);
            } 
        }

        public static readonly DependencyProperty VertTimelineProperty = DependencyProperty.Register(
        "VertTimeline",
        typeof(WindowedTimeLine),
        typeof(Grid),
        new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.AffectsArrange));
        public WindowedTimeLine VertTimeline 
        {
            get
            {
                return (WindowedTimeLine)GetValue(VertTimelineProperty);
            }
            set
            {
                SetValue(VertTimelineProperty, value);
            }
        }

        Brush BackgroundBrush;
        Pen KeyLine = new Pen(Brushes.Gray, 0.5);
        public double WidthPlus { get; private set; }

        #endregion // Properties

        protected override void OnRender(DrawingContext dc)
        {
            HorizTimeline.PixelLength = RenderSize.Width;

            double horizInterval = HorizTimeline.TickInterval;
            double horizStart = HorizTimeline.GetIntervalViewStart(horizInterval);
            double horizEnd = HorizTimeline.GetIntervalViewEnd(horizInterval);

            double tick = horizStart;
            while (tick <= (horizEnd + horizInterval))
            {
                double xPos = HorizTimeline.DisplayToPixel(tick);
                dc.DrawLine(KeyLine, new Point(xPos, 0), new Point(xPos, RenderSize.Height));
                tick += horizInterval;
            }

            VertTimeline.PixelLength = RenderSize.Height;

            double vertInterval = VertTimeline.TickInterval;
            double vertStart = VertTimeline.GetIntervalViewStart(vertInterval);
            double vertEnd = VertTimeline.GetIntervalViewEnd(vertInterval);

            tick = vertStart;
            while (tick <= vertEnd)
            {
                double yPos = VertTimeline.DisplayToPixel(tick);
                dc.DrawLine(KeyLine, new Point(0, yPos), new Point(RenderSize.Width, yPos));
                tick += vertInterval;
            }
        }


    }
}
