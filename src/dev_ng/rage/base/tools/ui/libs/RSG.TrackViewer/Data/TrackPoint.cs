﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Runtime.Serialization;

namespace RSG.TrackViewer.Data
{
    /// <summary>
    /// A single track point contains position and some basic information about behaviour
    /// </summary>
    [Serializable]
    public abstract class TrackPoint : INotifyPropertyChanged, ISerializable
    {
        #region Contructors
        internal TrackPoint(double x, double y)
        {
            X = x;
            Y = y;
        }
        #endregion

        #region Properties
        /// <summary>
        /// X Component of our point
        /// </summary>
        public double X 
        {
            get
            {
                return _X;
            }
            set
            {
                if (_X != value )
                {
                    if (!IsLockedX)
                    {
                        _X = value;
                        Notify("X");
                    }
                    
                }
            }
        }
        double _X;

        /// <summary>
        /// Y Component of our point
        /// </summary>
        public double Y
        {
            get
            {
                return _Y;
            }
            set
            {
                if (_Y != value)
                {
                    if (!IsLockedY)
                    {
                        _Y = value;
                        Notify("Y");
                    }
                   
                }
            }
        }
        double _Y;

        /// <summary>
        /// Locked points are immovable and indestructable
        /// </summary>
        public bool IsLocked 
        {
            get
            {
                return _IsLocked;
            }
            set
            {
                _IsLocked = IsLockedX = IsLockedY = value;
            }
        }
        bool _IsLocked;

        /// <summary>
        /// Point is locked in the X axis only
        /// </summary>
        public bool IsLockedX { get; set; }

        /// <summary>
        /// Point is locked in the Y axis only
        /// </summary>
        public bool IsLockedY { get; set; }
        #endregion // Properties

        #region Events

        public event PropertyChangedEventHandler PropertyChanged;
        public void Notify(string propName)
        {

            if (this.PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propName));
            }
        }
        
        #endregion // Events

        #region Serialisation

        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("X", X);
            info.AddValue("Y", Y);
            info.AddValue("IsLocked", IsLocked);
            info.AddValue("IsLockedX", IsLockedX);
            info.AddValue("IsLockedY", IsLockedY);
        }

        public TrackPoint(SerializationInfo info, StreamingContext context)
        {
            
            X = info.GetDouble("X");
            Y = info.GetDouble("Y");
            IsLocked = info.GetBoolean("IsLocked");
            IsLockedX = info.GetBoolean("IsLockedX");
            IsLockedY = info.GetBoolean("IsLockedY");
        }
        #endregion

        #region Methods
        public TrackPoint Clone()
        {
            TrackPoint clone = null;
            this.Clone(out clone);
            clone._IsLocked = this._IsLocked;
            clone.IsLockedX = this.IsLockedX;
            clone.IsLockedY = this.IsLockedY;

            return clone;
        }

        protected abstract void Clone(out TrackPoint clone);
        #endregion
    }

    /// <summary>
    /// A single track point contains position and some basic information about behaviour
    /// </summary>
    [Serializable]
    public class FloatTrackPoint : TrackPoint
    {
        public FloatTrackPoint(double x, double y) : base(x,y)
        {

        }

        public FloatTrackPoint(TrackPoint p)
            : base(p.X, p.Y)
        {
           
        }

        public FloatTrackPoint(SerializationInfo info, StreamingContext context)
            :base(info, context)
        {

        }

        protected Point Position { get; private set; }

        protected override void Clone(out TrackPoint clone)
        {
            clone = new FloatTrackPoint(this);
        }
    }

    /// <summary>
    /// Bool point may not be necessary
    /// </summary>
    public class BoolTrackPoint : TrackPoint
    {
        internal BoolTrackPoint(double y, bool val)
            : base(0.0, y)
        {

        }

        protected override void Clone(out TrackPoint clone)
        {
            clone = new BoolTrackPoint(this.Y, false);
        }

    }

    /// <summary>
    /// String points for storing tagged updata in a timeline
    /// </summary>
    public class StringTrackPoint : TrackPoint
    {
        internal StringTrackPoint(double y, String val)
            : base(0.0, y)
        {

        }

        protected override void Clone(out TrackPoint clone)
        {
            clone = new StringTrackPoint(this.Y, string.Empty);
        }
    }
}
