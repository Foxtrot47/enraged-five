﻿using System;
using System.Windows.Data;
using System.Collections.Generic;
using System.Globalization;

namespace RSG.TrackViewer
{

    [ValueConversion(typeof(float), typeof(float))]
    public class PointConverter : IValueConverter
    {
        #region IValueConverter Members
        public object Convert(object value, Type targetType,
            object parameter, CultureInfo culture)
        {

            if (parameter != null)
            {
                PointConverterDataContainer pd = ((PointConverterDataContainer)parameter);
                if (pd.OpAxis == Axis.X)
                    return ((double)value * ((double)pd.CommonData.XMax - (double)pd.CommonData.XMin)) + (double)pd.CommonData.XMin;
                else
                    return ((double)value * ((double)pd.CommonData.YMax - (double)pd.CommonData.YMin)) + (double)pd.CommonData.YMin;
            }
            else
                return (double)value;
        }
        
        public object ConvertBack(object value, Type targetType,
            object parameter, CultureInfo culture)
        {
            if (parameter != null)
            {
                PointConverterDataContainer pd = ((PointConverterDataContainer)parameter);
                if (pd.OpAxis == Axis.X)
                    return (double)(((Decimal.ToDouble((Decimal)value)) / (pd.CommonData.XMax - pd.CommonData.XMin)) + pd.CommonData.XMin);
                else
                    return (double)(((Decimal.ToDouble((Decimal)value)) / (pd.CommonData.YMax - pd.CommonData.YMin)) + pd.CommonData.YMin);
            }
            else
                return (double)value;
        }
        #endregion
    }

    public class PointConverterDataContainer
    {
        public CommonTrackData CommonData { get; set; }
        public Axis OpAxis { get; set; }
    }
}
