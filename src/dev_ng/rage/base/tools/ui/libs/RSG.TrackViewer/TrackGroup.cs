﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Diagnostics;

namespace RSG.TrackViewer
{
    public class TrackGroup : /*TreeViewItem,*/ INotifyPropertyChanged 
    {
        #region Constructors
        public TrackGroup(string name)
        {
            Name = name;
            //Header = name;
            Tracks = new ObservableCollection<Track>();
            RenderLines = true;
        }
        #endregion // Constructors

        #region Properties
        public bool Visible
        {
            get
            {
                return _Visible;
            }
            set
            {
                if (_Visible != value)
                {
                    _Visible = value;
                }
            }
        }
        bool _Visible;

        public bool RenderLines
        {
            get
            {
                return _RenderLines;
            }
            set
            {
                if (_RenderLines != value)
                {
                    _RenderLines = value;
                }
            }
        }
        bool _RenderLines;

        public string Name { get; private set; }
        public override string ToString()
        {
            if (Name != String.Empty)
            {
                return Name;
            }
            return base.ToString();
        }

        public string ShortName
        {
            get
            {
                return ToString();
            }
        }

        public ObservableCollection<Track> Tracks { get; private set; }

        #endregion // Properties

        #region Public Functions
        public bool AddTrack(Track t)
        {
            Tracks.Add(t);
            return false;
        }
        #endregion Public Functions

        #region Event Handlers
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string prop)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
            }
        }
        #endregion
    }

    public class TrackGroupCollection : INotifyPropertyChanged
    {

        #region Constructors
        public TrackGroupCollection()
        {
            /*
            SelectedPoints = new List<TrackPoint>();
            SelectedPoint = new FloatTrackPoint(0.0, 0.0);
            SelectedPoints.Add(SelectedPoint);
            */
            Children = new ObservableCollection<TrackGroup>();
        }
        #endregion

        #region Properties
        public event PropertyChangedEventHandler PropertyChanged;

        public void Notify(string propName)
        {

            if (this.PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propName));
            }
        }

        public ObservableCollection<TrackGroup> Children { get; private set; }
        /*
        public List<TrackPoint> SelectedPoints { get; set; }
        public TrackPoint SelectedPoint
        {
            get
            {
                return _SelectedPoint;
            }
            set
            {
                if (_SelectedPoint != value)
                {
                    _SelectedPoint = value;
                    Notify("SelectedPoint");
                }
            }
        }
        TrackPoint _SelectedPoint;
        */
        #endregion Properties

        #region Public Functions
        public void AddTrackGroup(TrackGroup tg)
        {
            Children.Add(tg);
        }

        public TrackGroup GetTrackGroup(int idx)
        {
            Debug.Assert(idx < Children.Count, "TrackGroupCollection: Index out of range");
            return Children[idx];
        }

        public bool FindAndDeletePoint(TrackPoint tp)
        {
            foreach (TrackGroup child in Children)
            {
                foreach (Track track in child.Tracks)
                {
                    if (track.RemovePoint(tp))
                        return true;
                }
            }

            return false;
        }
        #endregion // Public Functions

    }
}
