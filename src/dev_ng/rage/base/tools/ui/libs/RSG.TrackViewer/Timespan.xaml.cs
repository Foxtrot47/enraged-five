﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Globalization;

namespace RSG.TrackViewer
{
    
    /// <summary>
    /// Interaction logic for TimeStrip.xaml
    /// </summary>
    public partial class Timespan : ContentControl
    {
        
        #region Member Variables

        public static readonly DependencyProperty WinTimelineProperty = DependencyProperty.Register(
        "WinTimeline",
        typeof(WindowedTimeLine),
        typeof(Timespan),
        new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.AffectsArrange));

        public WindowedTimeLine WinTimeline 
        {
            get
            {
                return (WindowedTimeLine)GetValue(WinTimelineProperty);
            }
            set
            {
                SetValue(WinTimelineProperty, value);
                
            }
        }
        /*
        public WindowedTimeLine WinTimeline 
        { 
            get
            {
                return _WinTimeline;
            }
            set
            {
                if (_WinTimeline != value)
                    _WinTimeline = value;
            }
        }
        WindowedTimeLine _WinTimeline;
        */
        public int TimespanAxis
        {
            get
            {
                return _TimespanAxis;
            }

            set
            {
                _TimespanAxis = value;
            }
        }
        int _TimespanAxis;

        #endregion // Member Variables

        public Timespan()
        {
            InitializeComponent();
            Loaded += new RoutedEventHandler(TimeStrip_Loaded);
            this.DataContextChanged += new DependencyPropertyChangedEventHandler(DataContext_Changed);
            TimespanAxis = 0;
            
        }

        public void DataContext_Changed(object sender, DependencyPropertyChangedEventArgs e)
        {
            // yuck - the data context is all wrong here.
            (this.DataContext as TrackManager).VertTimeline.RangeChanged += new EventHandler(TimeLineRange_Changed);
            (this.DataContext as TrackManager).HorizTimeline.RangeChanged += new EventHandler(TimeLineRange_Changed);
        }
        
        public void TimeLineRange_Changed(object sender, EventArgs e)
        {
            InvalidateVisual();
        }
        
        void TimeStrip_Loaded(object sender, RoutedEventArgs e)
        {
            InvalidateVisual();
        }

        protected override void OnRender(DrawingContext dc)
        {
            double secondaryLength = 0.0;
            switch (TimespanAxis)
            {
                case 0://Axis.X:
                    WinTimeline.PixelLength = RenderSize.Width;
                    secondaryLength = RenderSize.Height;
                    break;
                case 1://Axis.Y:
                    WinTimeline.PixelLength = RenderSize.Height;
                    secondaryLength = RenderSize.Width;
                    break;
            }

            // Draw the tick marks
            Brush tickBrush = this.Foreground;
            Pen tickPen = new Pen(tickBrush, 1.0);

            double topOfTick = secondaryLength - 3;
            double bottomOfTick = secondaryLength;

            double topOfMajorTick = secondaryLength - 5;
            double bottomOfMajorTick = secondaryLength;

            double bottomOfLabel = secondaryLength - 5;

            double range = WinTimeline.DisplayViewRange;
            double tickInterval = WinTimeline.TickInterval;
            double startTick = WinTimeline.GetIntervalViewStart(tickInterval);
            double endTick = WinTimeline.GetIntervalViewEnd(tickInterval);

            double tick = startTick;
            while (tick <= endTick)
            {
                switch (TimespanAxis)
                {
                    case 0://Axis.X:
                        double xPos = WinTimeline.DisplayToPixel(tick);
                        dc.DrawLine(tickPen, new Point(xPos, topOfTick), new Point(xPos, bottomOfTick));
                        tick += tickInterval;
                        break;
                    case 1://Axis.Y:
                        double yPos = WinTimeline.DisplayToPixel(tick);
                        dc.DrawLine(tickPen, new Point(topOfTick, yPos), new Point(bottomOfTick, yPos));
                        tick += tickInterval;
                        break;
                }
            }

            // Now draw the labels
            double labelInterval = WinTimeline.LabelInterval;

            // pick a good format string for the interval
            string formatString = "{0:0.#}";
            if (labelInterval < 0.01f)
            {
                formatString = "{0:0.000}";
            }
            else if (labelInterval < 0.1f)
            {
                formatString = "{0:0.00}";
            }

            // so now we know we want to draw labels every 'interval' units
            // (we could recompute this just when the size or interval changes too)

            double startLabel = WinTimeline.GetIntervalViewStart(labelInterval);
            double endLabel = WinTimeline.GetIntervalViewEnd(labelInterval);

            Brush labelBrush = this.Foreground;

            // Draw the labels

            Typeface typeface = new Typeface(this.FontFamily, this.FontStyle, this.FontWeight, this.FontStretch);

            double label = startLabel;
            while (label <= endLabel + labelInterval) // Draw one extra label, just in case the incremental adds don't work out
            {
                switch (TimespanAxis)
                {
                    case 0://Axis.X:
                        double xPos = WinTimeline.DisplayToPixel(label);

                        string numberStr = String.Format(formatString, label);

                        FormattedText text = new FormattedText(numberStr, CultureInfo.InvariantCulture, FlowDirection.LeftToRight, typeface, this.FontSize * 1.2, labelBrush);

                        double width = text.Width;

                        dc.DrawText(text, new Point(xPos - width * 0.5, bottomOfLabel - text.Height));
                        dc.DrawLine(tickPen, new Point(xPos, topOfMajorTick), new Point(xPos, bottomOfMajorTick));
                        label += labelInterval;
                        break;
                    case 1:
                        double yPos = WinTimeline.DisplayToPixel(label);

                        string numberStr1 = String.Format(formatString, label);

                        FormattedText text1 = new FormattedText(numberStr1, CultureInfo.InvariantCulture, FlowDirection.LeftToRight, typeface, this.FontSize * 1.2, labelBrush);

                        double height = text1.Height;

                        dc.DrawText(text1, new Point(bottomOfLabel - text1.Width, (yPos - height * 0.5)));
                        dc.DrawLine(tickPen, new Point(yPos, topOfMajorTick), new Point(yPos, bottomOfMajorTick));
                        label += labelInterval;
                        break;
                }

            }
        }



    }

    public enum Axis
    {
        X,
        Y,
        Z
    }

       
        
}
