﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.ComponentModel;

using RSG.TrackViewer.Data;
using RSG.TrackViewer.ViewModel;

namespace RSG.TrackViewer
{
    /// <summary>
    /// Singleton class for managing the trackviewers collections of track data
    /// </summary>
    public class TrackManager
    {
        #region Contructors
        private TrackManager() 
        {
            VertTimeline = new WindowedTimeLine(true, Axis.Y);
            HorizTimeline = new WindowedTimeLine(false, Axis.X);
            CommonData = new CommonTrackData(VertTimeline, HorizTimeline);
        }

        /*void TrackGroups_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null)
            {
                foreach (TrackGroup trackGroup in e.NewItems)
                {
                    TrackGroupCollectionViewModels.TrackGroupViewModels.Add(new TrackGroupViewModel(trackGroup));
                }
            }
        }*/

        public event PropertyChangedEventHandler PropertyChanged;

        public void Notify(string propName)
        {

            if (this.PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propName));
            }
        }

        #endregion // Contructors
        #region Properties
       
        /// <summary>
        /// A single object containg all of the data that will be rendered on the data layer
        /// </summary>
        public TrackGroupCollectionViewModel TrackGroupCollectionViewModel
        {
            get
            {
                return _TrackGroupCollectionViewModel;
            }
            set
            {
                if (value != _TrackGroupCollectionViewModel)
                    _TrackGroupCollectionViewModel = value;
            }
        }
        TrackGroupCollectionViewModel _TrackGroupCollectionViewModel;

        public WindowedTimeLine VertTimeline
        {
            get
            {
                return _VertTimeline;
            }
            set
            {
                if (value != _VertTimeline)
                    _VertTimeline = value;
            }
        }
        WindowedTimeLine _VertTimeline;

        public WindowedTimeLine HorizTimeline
        {
            get
            {
                return _HorizTimeline;
            }
            set
            {
                if (value != _HorizTimeline)
                    _HorizTimeline = value;
            }
        }
        WindowedTimeLine _HorizTimeline;

        /// <summary>
        /// Common data packet
        /// </summary>
        public CommonTrackData CommonData
        {
            get
            {
                return _CommonData;
            }
            private set
            {
                if (_CommonData != value)
                    _CommonData = value;
            }
        }
        CommonTrackData _CommonData;

        /// <summary>
        /// Our TrackManager singleton
        /// </summary>
        public static TrackManager Instance
        {
            get
            {
                if (_Instance == null)
                {
                    _Instance = new TrackManager();
                }
                return _Instance;
            }
        }
        private static TrackManager _Instance;

        
        #endregion // Properties

        #region Public Functions
        /// <summary>
        /// Locate a point and delete it
        /// </summary>
        ///  /// <param name="tp">Point we want to delete</param>
        public bool FindAndDeletePoint(TrackPoint tp)
        {
            foreach (TrackGroupViewModel child in TrackGroupCollectionViewModel.TrackGroupViewModels)
            {
                foreach (TrackViewModel t in child.TrackViewModels)
                {
                    if (t.Track.RemovePoint(tp))
                        return true;
                }
            }
            return false;
        }
        #endregion // Public Functions
    }
}
