﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Runtime.Serialization;
using System.ComponentModel;
using System.Xml;
using System.Globalization;

namespace RSG.TrackViewer.Data
{
    /// <summary>
    /// A Track is a collection of TrackPoints that can be sued to generate a curve
    /// </summary>
    [Serializable]
    public class Track : ISerializable
    {
        #region Constructors
        public Track(string name)
        {
            Points = new List<TrackPoint>();
            AllowDuplicates = false;
            Name = name;
            IsActive = true;
            ClampMinimum = 0.0;
            ClampMaximum = 1.0;
        }

        public Track(Track other)
        {
            AllowDuplicates = other.AllowDuplicates;
            Name = other.Name;
            IsActive = other.IsActive;
            ClampMinimum = other.ClampMinimum;
            ClampMaximum = other.ClampMaximum;

            Points = new List<TrackPoint>();
            foreach (TrackPoint opoint in other.Points)
            {
                Points.Add(opoint.Clone());
            }
        }

        internal Track(List<TrackPoint> points)
        {
            Points = points;
            AllowDuplicates = false;
            IsActive = true;
            ClampMinimum = 0.0;
            ClampMaximum = 1.0;
        }
        #endregion // Constructors

        #region Properties
        /// <summary>
        /// The tracks name
        /// </summary>
        public string Name 
        { 
            get
            {
                return _Name;
            }
            set
            {
                if (_Name != value)
                    _Name = value;
            }
        }
        string _Name;
        
        public override string ToString()
        {
            if (Name != String.Empty)
            {
                return Name;
            }
            return base.ToString();
        }

        public string ShortName
        {
            get
            {
                return ToString();
            }
        }

        /// <summary>
        /// The list of points.  This will likely need to change to an observable collection when I start
        /// toying with bezier curves.
        /// </summary>
        public List<TrackPoint> Points { get; private set; }
        
        /// <summary>
        /// Whether or not we wont to allow two points to exist in the same space
        /// </summary>
        bool AllowDuplicates { get; set; }

        /// <summary>
        /// Defines whether or not the track is active.  The View sees this as an indication as to whether or not
        /// to render the track in the viewport.  NOTE:  This is not the smae as Visible which si defined in the ViewModel
        /// of TrackViewer
        /// </summary>
        public bool IsActive
        {
            get
            {
                return _IsActive;
            }
            set
            {
                if (_IsActive != value)
                {
                    _IsActive = value;
                    OnChanged();
                }
            }
        }
        bool _IsActive;

        /// <summary>
        /// Minimum clamp
        /// </summary>
        public double ClampMinimum
        {
            get
            {
                return _ClampMinimum;
            }
            set
            {
                if (_ClampMinimum != value)
                {
                    _ClampMinimum = value;
                }
            }
        }
        double _ClampMinimum;

        public double ClampMaximum
        {
            get
            {
                return _ClampMaximum;
            }
            set
            {
                if (_ClampMaximum != value)
                {
                    _ClampMaximum = value;
                }
            }
        }
        double _ClampMaximum;

        /// <summary>
        /// Minimum clamp
        /// </summary>
        public double OutputMinimum
        {
            get
            {
                return _OutputMinimum;
            }
            set
            {
                if (_OutputMinimum != value)
                {
                    _OutputMinimum = value;
                }
            }
        }
        double _OutputMinimum;

        public double OutputMaximum
        {
            get
            {
                return _OutputMaximum;
            }
            set
            {
                if (_OutputMaximum != value)
                {
                    _OutputMaximum = value;
                }
            }
        }
        double _OutputMaximum;
        #endregion // Properties

        #region Public Functions
        
        /// <summary>
        /// NumPoints
        /// </summary>
        public int NumPoints()
        {
            return Points.Count;
        }

        /// <summary>
        /// GetPoint
        /// </summary>
        public TrackPoint GetPoint(int idx)
        {
            return Points[idx];
        }

        /// <summary>
        /// Get certain Y value for given X value 
        /// </summary>
        public float GetYValAt(int index, float val)
        {
            if (index >= Points.Count || (index - 1 < 0))
            {
                TrackPoint keyframe = Points[Points.Count - 1];
                return (float)keyframe.Y;
            }
            else
            {
                TrackPoint keyframe = Points[index];
                if (keyframe.X == val)
                {
                    return (float)keyframe.Y;
                }
                else
                {
                    // find neighbors, lerp
                    TrackPoint prev = Points[index - 1];

                    float dx = (float)(keyframe.X - prev.X);
                    float dy = (float)(keyframe.Y - prev.Y);

                    float t = (float)((val - prev.X) / dx);
                    return ((float)(t * dy + prev.Y));
                }
            }
        }

        /// <summary>
        /// Add a point to a track.  Dont use this to construct the point list
        /// if you know the point list at initialisation since it will force a 
        /// re-draw everytime a point is added.  Use the constuctor that takes a list of points
        /// </summary>
        /// <param name="point"></param>
        public bool AddPoint(TrackPoint point)
        {
            if (!AllowDuplicates)
            {
                if (null == Points.Find(delegate(TrackPoint p) { return p.X == point.X && p.Y == point.Y; }))
                    Points.Add(point);
                else
                    return false;
            }
            else
                Points.Add(point);

            return true;
        }

        /// <summary>
        /// Remove a point from the point list (thanks captain obvious)
        /// </summary>
        /// <param name="point"></param>
        public bool RemovePoint(TrackPoint point)
        {
            return Points.Remove(point);
        }

        /// <summary>
        /// Remove a point from the point list (thanks captain obvious)
        /// </summary>
        /// <param name="point"></param>
        public void RemovePointAt(int idx)
        {
            Points.RemoveAt(idx);
        }

        /// <summary>
        /// Sort function, currently assumes sorting along the X (usually time) axis.
        /// </summary>
        public void SortPoints()
        {
            Points.Sort(CompareTrackPointByX);
        }
        #endregion // Public Functions

        #region Private Functions
        /// <summary>
        /// Compare function for points in a track (used in a sort)
        /// </summary>
        private static int CompareTrackPointByX( TrackPoint p1, TrackPoint p2 )
        {
            if (p1 == null)
            {
                if (p2 == null)
                    return 0;
                else
                    return -1;
            }
            else
            {
                if (p2 == null)
                    return 1;
                else
                {
                    if (p1.X > p2.X)
                        return 1;
                    else if (p1.X < p2.X)
                        return -1;
                    else
                        return 0;
                }
            }
        }
        #endregion // Public Functions

        #region Events
        public event EventHandler Changing;
        public void OnChanged()
        {
            if (Changing != null)
            {
                Changing(this, EventArgs.Empty);
            }
        }
        #endregion // Events

        #region Serialisation

        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("Name", Name);
            info.AddValue("Points", Points);
            info.AddValue("ClampMinimum", ClampMinimum);
            info.AddValue("ClampMaximum", ClampMaximum);
            info.AddValue("IsActive", IsActive);
           
        }

        public Track(SerializationInfo info, StreamingContext context)
        {
            
            Name = info.GetString("Name");
            Points = (List<TrackPoint>)info.GetValue("Points", typeof(List<TrackPoint>));
            
            ClampMinimum = info.GetDouble("ClampMinimum");
            ClampMaximum = info.GetDouble("ClampMaximum");
            try
            {
                IsActive = info.GetBoolean("IsActive");
            }
            catch (Exception)
            {

            }
            
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="Track"/> class
        /// using the specified System.Xml.XmlReader as a data provider.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides access to the xml data used to initialise
        /// this instance.
        /// </param>
        /// <exception cref="System.ArgumentNullException">
        /// Thrown if the <paramref name="reader"/> or <paramref name="database"/>
        /// parameter is null.
        /// </exception>
        public Track(XmlReader reader)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }

            Points = new List<TrackPoint>();
            AllowDuplicates = false;
            this.Deserialise(reader);
        }

        #endregion

        /// <summary>
        /// Serialises this instance to the given System.Xml.XmlWriter.
        /// </summary>
        /// <param name="writer">
        /// The System.Xml.XmlWriter that this instance should be serialised to.
        /// </param>
        public void Serialise(XmlWriter writer)
        {
            if (writer == null)
            {
                return;
            }

            writer.WriteStartElement("Track");

            writer.WriteAttributeString("Name", this.Name);
            writer.WriteAttributeString("Min", this.ClampMinimum.ToString());
            writer.WriteAttributeString("Max", this.ClampMaximum.ToString());
            writer.WriteAttributeString("OutputMin", this.OutputMinimum.ToString());
            writer.WriteAttributeString("OutputMax", this.OutputMaximum.ToString());
            writer.WriteAttributeString("IsActive", this.IsActive.ToString());
            writer.WriteAttributeString("AllowDuplicates", this.AllowDuplicates.ToString());

            if (this.Points != null)
            {
                writer.WriteStartElement("Points");
                foreach (TrackPoint point in this.Points)
                {
                    if (point is StringTrackPoint)
                    {
                        writer.WriteStartElement("StringPoint");
                    }
                    else if (point is BoolTrackPoint)
                    {
                        writer.WriteStartElement("BoolPoint");
                    }
                    else
                    {
                        writer.WriteStartElement("FloatPoint");
                    }

                    writer.WriteAttributeString(
                        "x", point.X.ToString("G17"));
                    writer.WriteAttributeString(
                        "y", point.Y.ToString("G17"));
                    writer.WriteAttributeString(
                        "locked", point.IsLocked.ToString(CultureInfo.InvariantCulture));
                    writer.WriteAttributeString(
                        "lockedX", point.IsLockedX.ToString(CultureInfo.InvariantCulture));
                    writer.WriteAttributeString(
                        "lockedY", point.IsLockedY.ToString(CultureInfo.InvariantCulture));
                    writer.WriteEndElement();
                }

                writer.WriteEndElement();    
            }

            writer.WriteEndElement();
        }

        /// <summary>
        /// Uses the specified System.Xml.XmlReader to initialise the fields for this instance.
        /// </summary>
        /// <param name="reader">
        /// The System.Xml.XmlReader that provides the data used to initialise this instance.
        /// </param>
        private void Deserialise(XmlReader reader)
        {
            if (reader == null)
            {
                return;
            }

            this.Name = reader.GetAttribute("Name");
            this._IsActive = bool.Parse(reader.GetAttribute("IsActive"));
            this._ClampMinimum = double.Parse(reader.GetAttribute("Min"));
            this._ClampMaximum = double.Parse(reader.GetAttribute("Max"));
            this.AllowDuplicates = bool.Parse(reader.GetAttribute("AllowDuplicates"));

            string outputMin = reader.GetAttribute("OutputMin");
            if (outputMin != null)
            {
                this._OutputMinimum = double.Parse(outputMin);
            }

            string outputMax = reader.GetAttribute("OutputMax");
            if (outputMax != null)
            {
                this._OutputMaximum = double.Parse(outputMax);
            }

            if (reader.IsEmptyElement)
            {
                reader.Skip();
                return;
            }

            reader.ReadStartElement();
            while (reader.MoveToContent() != XmlNodeType.EndElement)
            {
                if (!reader.IsStartElement())
                {
                    reader.Read();
                    continue;
                }

                if (string.CompareOrdinal(reader.Name, "Points") == 0)
                {
                    if (!reader.IsEmptyElement)
                    {
                        reader.ReadStartElement();
                        while (reader.MoveToContent() != XmlNodeType.EndElement)
                        {
                            double x = double.Parse(reader.GetAttribute("x"));
                            double y = double.Parse(reader.GetAttribute("y"));
                            bool locked = bool.Parse(reader.GetAttribute("locked"));
                            bool lockedX = bool.Parse(reader.GetAttribute("lockedX"));
                            bool lockedY = bool.Parse(reader.GetAttribute("lockedY"));
                            if (reader.Name == "StringPoint")
                            {
                                this.Points.Add(new StringTrackPoint(y, string.Empty));
                            }
                            else if (reader.Name == "BoolPoint")
                            {
                                this.Points.Add(new BoolTrackPoint(y, false));
                            }
                            else
                            {
                                this.Points.Add(new FloatTrackPoint(x, y));
                            }

                            this.Points[this.Points.Count - 1].IsLocked = locked;
                            this.Points[this.Points.Count - 1].IsLockedX = lockedX;
                            this.Points[this.Points.Count - 1].IsLockedY = lockedY;
                            reader.Skip();
                        }

                        reader.Skip();
                    }
                    else
                    {
                        reader.Skip();
                    }
                }
                else
                {
                    reader.Read();
                }
            }

            reader.Skip();
        }
    }
}
