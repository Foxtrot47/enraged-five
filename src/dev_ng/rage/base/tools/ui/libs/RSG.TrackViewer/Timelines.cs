﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.TrackViewer
{
    /// <summary>
    /// Keey our two timelines in the one struct
    /// </summary>
    struct Timelines
    {
        /// <summary>
        /// Horizontal timeline component
        /// </summary>
        public WindowedTimeLine Horiz
        {
            get
            {
                return _Horiz;
            }
            set
            {
                if(value != _Horiz)
                    _Horiz = value;
            }
        }
        WindowedTimeLine _Horiz;

        /// <summary>
        /// Vertical timeline component
        /// </summary>
        public WindowedTimeLine Vert
        {
            get
            {
                return _Vert;
            }
            set
            {
                if (value != _Vert)
                    _Vert = value;
            }
        }
        WindowedTimeLine _Vert;
    }
}
