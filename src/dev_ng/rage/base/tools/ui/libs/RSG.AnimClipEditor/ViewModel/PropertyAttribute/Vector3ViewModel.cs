﻿using System;

using RSG.ManagedRage.ClipAnimation;

namespace RSG.AnimClipEditor.ViewModel.PropertyAttribute
{
    public class Vector3ViewModel : PropertyAttributeBaseViewModel
    {
        #region Constructors
        public Vector3ViewModel(MPropertyAttributeVector3 mpropattr, ClipViewModel clip)
            : base(mpropattr, clip)
        {

        }
        #endregion

        #region Properties
        public int Value
        {
            get
            {
                throw new Exception(this.Type.ToString() + " type not implemented");
            }
        }
        #endregion // Properties
    }
}
