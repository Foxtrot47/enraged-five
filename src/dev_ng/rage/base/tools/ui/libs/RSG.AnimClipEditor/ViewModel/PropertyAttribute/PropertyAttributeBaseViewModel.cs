﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using RSG.ManagedRage.ClipAnimation;
using System.ComponentModel;

namespace RSG.AnimClipEditor.ViewModel
{
    public abstract class PropertyAttributeBaseViewModel
    {
        #region Fields
        ClipViewModel clip;
        #endregion

        #region Constructors
        public PropertyAttributeBaseViewModel(MPropertyAttribute mpropertyattr, ClipViewModel clip)
        {
            this.clip = clip;
            _PropertyAttribute = mpropertyattr;
        }
        #endregion // Constructors

        #region Properties
        public MPropertyAttribute PropertyAttribute
        {
            get
            {
                return _PropertyAttribute;
            }
        }
        MPropertyAttribute _PropertyAttribute;

        public string Name
        {
            get
            {
                String attrName =  _PropertyAttribute.GetName();
                attrName = attrName.Replace("_DO_NOT_RESOURCE", "");
                return attrName;
            }
        }

        public string UIName
        {
            get
            {
                if (string.IsNullOrWhiteSpace(uiName))
                {
                    String attrName = _PropertyAttribute.GetName();
                    attrName = attrName.Replace("_DO_NOT_RESOURCE", "");
                    return attrName;
                }
                else
                {
                    return uiName;
                }
            }
            set { this.uiName = value; }
        }
        private string uiName;

        public string UIDescription
        {
            get
            {
                if (string.IsNullOrWhiteSpace(uiDescription))
                {
                    return null;
                }
                else
                {
                    return uiDescription;
                }
            }
            set { this.uiDescription = value; }
        }
        private string uiDescription;

        public int Type
        {
            get
            {
                return (int)_PropertyAttribute.GetType();
            }
        }

        public string[] FavoriteValues
        {
            get
            {
                return _FavoriteValues;
            }
            set
            {
                _FavoriteValues = value;
            }
        }
        string[] _FavoriteValues;
        #endregion // Properties

        #region INotifyPropertyChanged Members
        public event EventHandler ValueChanged;

        protected void FireValueChange()
        {
            if (ValueChanged != null)
            {
                ValueChanged(this, EventArgs.Empty);
            }

            if (this.clip != null)
            {
                this.clip.DirtyState = true;
            }
        }
        #endregion

        #region methods
        public virtual bool TestString(string value)
        {
            return false;
        }

        public virtual void SetFromString(string value)
        {
        }

        public virtual string GetValueAsString()
        {
            return string.Empty;
        }

        public virtual void SetFromOther(PropertyAttributeBaseViewModel other)
        {
        }
        #endregion
    }
}
