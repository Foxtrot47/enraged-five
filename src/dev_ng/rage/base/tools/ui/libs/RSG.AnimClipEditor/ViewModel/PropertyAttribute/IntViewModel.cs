﻿using System;

using RSG.ManagedRage.ClipAnimation;

namespace RSG.AnimClipEditor.ViewModel.PropertyAttribute
{
    public class IntViewModel : PropertyAttributeBaseViewModel
    {
        #region Constructors
        public IntViewModel(MPropertyAttributeInt mpropattr, ClipViewModel clip)
            : base(mpropattr, clip)
        {

        }

        public IntViewModel(MPropertyAttributeInt mpropattr, string initialValue, ClipViewModel clip)
            : base(mpropattr, clip)
        {
            SetFromString(initialValue);
        }
        #endregion

        #region Properties
        public int Value
        {
            get
            {
                return (int)(this.PropertyAttribute as MPropertyAttributeInt).GetInt();
            }
            set
            {
                if (value != (int)(this.PropertyAttribute as MPropertyAttributeInt).GetInt())
                {
                    (this.PropertyAttribute as MPropertyAttributeInt).SetInt(Convert.ToInt32(value));
                    FireValueChange();
                }
            }
        }
        #endregion // Properties

        #region methods
        public override bool TestString(string value)
        {
            int testingValue;
            if (!int.TryParse(value, out testingValue))
                return false;

            return testingValue == Value;
        }

        public override void SetFromString(string value)
        {
            int newValue;
            if (int.TryParse(value, out newValue))
            {
                this.Value = newValue;
            }
        }

        public override string GetValueAsString()
        {
            return this.Value.ToString().ToLower();
        } 

        public override void SetFromOther(PropertyAttributeBaseViewModel other)
        {
            if (!(other is IntViewModel))
                return;

            this.Value = (other as IntViewModel).Value;
        }
        #endregion
    }
}
