﻿using System;

using RSG.ManagedRage.ClipAnimation;

namespace RSG.AnimClipEditor.ViewModel.PropertyAttribute
{
    public class Matrix34ViewModel : PropertyAttributeBaseViewModel
    {
        #region Constructors
        public Matrix34ViewModel(MPropertyAttributeMatrix34 mpropattr, ClipViewModel clip)
            : base(mpropattr, clip)
        {

        }
        #endregion

        #region Properties
        int Value
        {
            get
            {
                throw new Exception(this.Type.ToString() + " type not implemented");
            }
        }
        #endregion // Properties
    }
}
