﻿using System;

using RSG.ManagedRage.ClipAnimation;

namespace RSG.AnimClipEditor.ViewModel.PropertyAttribute
{
    public class FloatViewModel : PropertyAttributeBaseViewModel
    {
        #region Constructors
        public FloatViewModel(MPropertyAttributeFloat mpropattr, ClipViewModel clip)
            : base(mpropattr, clip)
        {
            this.Min = float.MinValue;
            this.Max = float.MaxValue;
        }

        public FloatViewModel(MPropertyAttributeFloat mpropattr, string initialValue, ClipViewModel clip)
            : base(mpropattr, clip)
        {
            SetFromString(initialValue);
            this.Min = float.MinValue;
            this.Max = float.MaxValue;
        }
        #endregion

        #region Properties
        public float Value
        {
            get
            {
                var value = (this.PropertyAttribute as MPropertyAttributeFloat).GetFloat();
                return (float)Convert.ToSingle(value);
            }
            set
            {
                var currentValue = (this.PropertyAttribute as MPropertyAttributeFloat).GetFloat();
                if (value != (float)Convert.ToSingle(currentValue))
                {
                    (this.PropertyAttribute as MPropertyAttributeFloat).SetFloat(Convert.ToSingle(value));
                    FireValueChange();
                }
            }
        }

        public bool IsBlendIn
        {
            get { return this.isBlendIn; }
            set
            {
                if (value == this.isBlendIn)
                {
                    return;
                }

                bool oldValue = this.isBlendIn;
                this.isBlendIn = value;
                if (value)
                {
                    this.Min = 0.0f;
                    this.Max = 1.0f;
                }
                else
                {
                    this.Min = float.MinValue;
                    this.Max = float.MaxValue;
                }
            }
        }
        private bool isBlendIn;

        public bool IsBlendOut
        {
            get { return this.isBlendOut; }
            set
            {
                if (value == this.isBlendOut)
                {
                    return;
                }

                this.isBlendOut = value;
                if (value)
                {
                    this.Min = 0.0f;
                    this.Max = 1.0f;
                }
                else
                {
                    this.Min = float.MinValue;
                    this.Max = float.MaxValue;
                }
            }
        }
        private bool isBlendOut;

        public float Min
        {
            get;
            set;
        }

        public float Max
        {
            get;
            set;
        }
        #endregion // Properties

        #region methods
        public override bool TestString(string value)
        {
            float testingValue;
            if (!float.TryParse(value, out testingValue))
                return false;

            return testingValue == Value;
        }

        public override void SetFromString(string value)
        {
            float newValue;
            if (float.TryParse(value, out newValue))
            {
                this.Value = newValue;
            }
            else
            {
                this.Value = 0.0f;
            }
        }

        public override string GetValueAsString()
        {
            return this.Value.ToString().ToLower();
        } 

        public override void SetFromOther(PropertyAttributeBaseViewModel other)
        {
            if (!(other is FloatViewModel))
                return;

            this.Value = (other as FloatViewModel).Value;
        }
        #endregion
    }
}
