﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Collections.Specialized;

using RSG.ManagedRage.ClipAnimation;
using RSG.AnimClipEditor.ViewModel;
using RSG.AnimClipEditor.ViewModel.PropertyAttribute;
using RSG.AnimationMetadata;
using System.Windows.Media;

namespace RSG.AnimClipEditor.Util
{
    static class EventFactory
    {
        #region Properties

        #endregion // Properties
        
        #region Public Methods
        public static ObservableCollection<ClipEventViewModel> GenerateClipEvents(MClip mclip, ClipViewModel cvm)
        {
            ObservableCollection<ClipEventViewModel> events = new ObservableCollection<ClipEventViewModel>();
            foreach (MTag mtag in mclip.GetTags())
            {
                ClipEventViewModel clipevent = null;
                foreach (ClipEventViewModel ce in events)
                {
                    if (ce.Type == mtag.GetName())
                    {
                        clipevent = ce;
                        break;
                    }

                }
                if (null != clipevent)
                {
                    clipevent.Tags.Add(CreateTagViewModel(mtag, cvm));
                }
                else
                {
                    Console.WriteLine(mtag.GetName());
                    ObservableCollection<TagViewModel> tags = new ObservableCollection<TagViewModel>();
                    tags.Add(CreateTagViewModel(mtag, cvm));

                    ClipEventViewModel newclipevent = new ClipEventViewModel(mtag.GetName(), tags, cvm);

                    events.Add(newclipevent);
                }
            }
            foreach (ClipEventViewModel cevm in events)
            {
                cevm.Tags.CollectionChanged += new NotifyCollectionChangedEventHandler(cvm.Tags_CollectionChanged);
            }

            return events;
        }

        public static ObservableCollection<PropertyViewModel> GenerateClipProperties(MClip mclip, ClipViewModel cvm)
        {
            ObservableCollection<PropertyViewModel> clipProperties = new ObservableCollection<PropertyViewModel>();
            foreach (MProperty mprop in mclip.GetProperties())
            {
                clipProperties.Add(CreatePropertyViewModelFromMProperty(mprop, true, cvm));
            }
            return clipProperties;
        }
        #endregion // Public Methods

        #region Private Methods

        public static TagViewModel CreateTagViewModel(MTag mtag, ClipViewModel cvm)
        {
            TagViewModel tvm = new TagViewModel(mtag, cvm);

            tvm.Background = new SolidColorBrush(Colors.Gray);
            tvm.DefaultBackground = new SolidColorBrush(Colors.Gray);
            tvm.InitialFrameLength = 0.0f;
            if (ClipTagDefinitions.PropertyDescriptions.ContainsKey(mtag.GetProperty().GetName()))
            {
                ClipPropertyDefinition propertyDef = ClipTagDefinitions.PropertyDescriptions[mtag.GetProperty().GetName()];
                tvm.Background = new SolidColorBrush(propertyDef.DefaultTagColour.Value);
                tvm.DefaultBackground = new SolidColorBrush(propertyDef.DefaultTagColour.Value);
                tvm.ColourChangers = propertyDef.PropertyGroups;
                tvm.MinimumFrameLength = propertyDef.MinimumFrameLength;
                tvm.MaximumFrameLength = propertyDef.MaximumFrameLength;
                tvm.InitialFrameLength = propertyDef.InitialFrameLength;
            }
            tvm.Property = CreatePropertyViewModelFromMProperty(mtag.GetProperty(), false, cvm);
            tvm.PropertyChanged += new PropertyChangedEventHandler(cvm.Selection_Changed);
            tvm.DetermineColourFromPropertyValues();

            return tvm;
        }

        public static TagViewModel CreateTagViewModel(string clipeventType, float start, float end, ClipViewModel cvm)
        {
            if (!ClipTagDefinitions.PropertyDescriptions.ContainsKey(clipeventType))
                return null;

            ClipPropertyDefinition propertyDef = ClipTagDefinitions.PropertyDescriptions[clipeventType];
            List<MPropertyAttribute> propertyAttributes = new List<MPropertyAttribute>();
            List<PropertyAttributeBaseViewModel> propertyViewModels = new List<PropertyAttributeBaseViewModel>();

            foreach (ClipAttributeDefinition attributeDef in propertyDef.AttributeDefs)
            {
                switch (attributeDef.Type)
                {
                    case (int)MPropertyAttribute.eType.kTypeBool:
                        {
                            string[] names = attributeDef.Name.Split(';');
                            if (names.Length > 1)
                            {
                                foreach (string name in names)
                                {
                                    MPropertyAttributeBool propattr = new MPropertyAttributeBool(name, false);
                                    propertyAttributes.Add(propattr);
                                    bool initialValue = false;
                                    if (attributeDef.InitialValue == name)
                                        initialValue = true;
                                    propertyViewModels.Add(new BoolViewModel(propattr, cvm)
                                    {
                                        FavoriteValues = attributeDef.Favorites,
                                        UIName = attributeDef.UIName,
                                        UIDescription = attributeDef.UIDescription,
                                        IsApartOfRadioGroup = true,
                                        RadioButtonGroupName = attributeDef.Name,
                                        Value = initialValue,
                                    });
                                }
                            }
                            else
                            {
                                MPropertyAttributeBool propattr = new MPropertyAttributeBool(attributeDef.Name, false);
                                propertyAttributes.Add(propattr);
                                propertyViewModels.Add(new BoolViewModel(propattr, attributeDef.InitialValue, cvm)
                                {
                                    FavoriteValues = attributeDef.Favorites,
                                    UIName = attributeDef.UIName,
                                    UIDescription = attributeDef.UIDescription,
                                });
                            }
                            break;
                        }
                    case (int)MPropertyAttribute.eType.kTypeFloat:
                        {
                            MPropertyAttributeFloat propattr = new MPropertyAttributeFloat(attributeDef.Name, 0.0f);
                            propertyAttributes.Add(propattr);
                            propertyViewModels.Add(new FloatViewModel(propattr, attributeDef.InitialValue, cvm)
                            {
                                FavoriteValues = attributeDef.Favorites,
                                UIName = attributeDef.UIName,
                                UIDescription = attributeDef.UIDescription,
                                IsBlendIn = attributeDef.IsBlendIn,
                                IsBlendOut = attributeDef.IsBlendOut,
                            });
                            break;
                        }
                    case (int)MPropertyAttribute.eType.kTypeHashString:
                        {
                            MPropertyAttributeHashString propattr = new MPropertyAttributeHashString(attributeDef.Name, 0);
                            propertyAttributes.Add(propattr);
                            propertyViewModels.Add(new HashStringViewModel(propattr, attributeDef.InitialValue, cvm)
                            {
                                FavoriteValues = attributeDef.Favorites,
                                UIName = attributeDef.UIName,
                                UIDescription = attributeDef.UIDescription,
                            });
                            break;
                        }
                    case (int)MPropertyAttribute.eType.kTypeInt:
                        {
                            MPropertyAttributeInt propattr = new MPropertyAttributeInt(attributeDef.Name, 0);
                            propertyAttributes.Add(propattr);
                            propertyViewModels.Add(new IntViewModel(propattr, attributeDef.InitialValue, cvm)
                            {
                                FavoriteValues = attributeDef.Favorites,
                                UIName = attributeDef.UIName,
                                UIDescription = attributeDef.UIDescription,
                            });
                            break;
                        }
                    case (int)MPropertyAttribute.eType.kTypeString:
                        {
                            MPropertyAttributeString propattr = new MPropertyAttributeString(attributeDef.Name, "CHANGEME");
                            propertyAttributes.Add(propattr);
                            propertyViewModels.Add(new StringViewModel(propattr, attributeDef.InitialValue, cvm)
                            {
                                FavoriteValues = attributeDef.Favorites,
                                UIName = attributeDef.UIName,
                                UIDescription = attributeDef.UIDescription,
                            });
                            break;
                        }
                    case (int)MPropertyAttribute.eType.kTypeBitSet:
                    case (int)MPropertyAttribute.eType.kTypeCustom:
                    case (int)MPropertyAttribute.eType.kTypeData:
                    case (int)MPropertyAttribute.eType.kTypeMatrix34:
                    case (int)MPropertyAttribute.eType.kTypeNone:
                    case (int)MPropertyAttribute.eType.kTypeNum:
                    case (int)MPropertyAttribute.eType.kTypeQuaternion:
                    case (int)MPropertyAttribute.eType.kTypeSituation:
                    case (int)MPropertyAttribute.eType.kTypeVector3:
                    case (int)MPropertyAttribute.eType.kTypeVector4:
                    default:
                        break;
                }
            }

            MProperty newProperty = new MProperty(clipeventType, propertyAttributes);
            MTag newTag = new MTag(clipeventType, start, end, newProperty);
            TagViewModel tvm = new TagViewModel(newTag, cvm);
            tvm.ColourChangers = propertyDef.PropertyGroups;
            tvm.MinimumFrameLength = propertyDef.MinimumFrameLength;
            tvm.MaximumFrameLength = propertyDef.MaximumFrameLength;
            tvm.InitialFrameLength = propertyDef.InitialFrameLength;
            tvm.Background = new SolidColorBrush(propertyDef.DefaultTagColour.Value);
            tvm.DefaultBackground = new SolidColorBrush(propertyDef.DefaultTagColour.Value);

            PropertyViewModel pvm = new PropertyViewModel(newProperty);
            foreach (PropertyAttributeBaseViewModel vm in propertyViewModels)
                pvm.PropertyAttributes.Add(vm);

            tvm.Property = pvm;
            tvm.PropertyChanged += new PropertyChangedEventHandler(cvm.Selection_Changed);
            tvm.DetermineColourFromPropertyValues();
            return tvm;
        }

        static PropertyViewModel CreatePropertyViewModelFromMProperty(MProperty mproperty, bool clipProperties, ClipViewModel clip)
        {
            ClipPropertyDefinition propertyDef = null;
            if (clipProperties)
            {
                string name = mproperty.GetName();
                ClipTagDefinitions.ClipPropertyDescriptions.TryGetValue(name, out propertyDef);
            }
            else
            {
                string name = mproperty.GetName();
                ClipTagDefinitions.PropertyDescriptions.TryGetValue(name, out propertyDef);
            }

            PropertyViewModel pvm = new PropertyViewModel(mproperty);
            //  Set enabled (allow_edit) here
            if (propertyDef != null)
            {
                pvm.Enabled = propertyDef.Enabled;
            }

            SortedList<int, List<PropertyAttributeBaseViewModel>> orderedProperties = new SortedList<int, List<PropertyAttributeBaseViewModel>>();
            orderedProperties.Add(int.MaxValue, new List<PropertyAttributeBaseViewModel>());
            foreach (MPropertyAttribute propattr in mproperty.GetPropertyAttributes())
            {
                switch ((int)propattr.GetType())
                {
                    case (int)MPropertyAttribute.eType.kTypeBitSet:
                        {
                            int index = int.MaxValue;
                            BitSetViewModel vm = new BitSetViewModel(propattr as MPropertyAttributeBitSet, clip);
                            if (propertyDef != null)
                            {
                                for (int i = 0; i < propertyDef.AttributeDefs.Count; i++)
                                {
                                    ClipAttributeDefinition attributeDef = propertyDef.AttributeDefs[i];
                                    if (propattr.GetName() != attributeDef.Name)
                                    {
                                        continue;
                                    }
                                    
                                    vm.FavoriteValues = attributeDef.Favorites;
                                    vm.UIName = attributeDef.UIName;
                                    vm.UIDescription = attributeDef.UIDescription;
                                    index = i;
                                    break;
                                }
                            }

                            if (!orderedProperties.ContainsKey(index))
                            {
                                orderedProperties.Add(index, new List<PropertyAttributeBaseViewModel>());
                            }

                            orderedProperties[index].Add(vm);
                            break;
                        }
                    case (int)MPropertyAttribute.eType.kTypeBool:
                        {
                            int index = int.MaxValue;
                            BoolViewModel vm = new BoolViewModel(propattr as MPropertyAttributeBool, clip);
                            if (propertyDef != null)
                            {
                                for (int i = 0; i < propertyDef.AttributeDefs.Count; i++)
                                {
                                    ClipAttributeDefinition attributeDef = propertyDef.AttributeDefs[i];
                                    if (propattr.GetName() == attributeDef.Name)
                                    {
                                        vm.FavoriteValues = attributeDef.Favorites;
                                        vm.UIName = attributeDef.UIName;
                                        vm.UIDescription = attributeDef.UIDescription;
                                        index = i;
                                        break;
                                    }
                                    else if (!string.IsNullOrWhiteSpace(attributeDef.Name))
                                    {
                                        string[] splitNames = attributeDef.Name.Split(';');
                                        if (splitNames.Contains(propattr.GetName()))
                                        {
                                            vm.IsApartOfRadioGroup = true;
                                            vm.RadioButtonGroupName = attributeDef.Name;
                                            index = i;
                                            break;
                                        }
                                    }
                                }
                            }

                            if (!orderedProperties.ContainsKey(index))
                            {
                                orderedProperties.Add(index, new List<PropertyAttributeBaseViewModel>());
                            }

                            orderedProperties[index].Add(vm);
                            break;
                        }
                    case (int)MPropertyAttribute.eType.kTypeData:
                        {
                            int index = int.MaxValue;
                            DataViewModel vm = new DataViewModel(propattr as MPropertyAttributeData, clip);
                            if (propertyDef != null)
                            {
                                for (int i = 0; i < propertyDef.AttributeDefs.Count; i++)
                                {
                                    ClipAttributeDefinition attributeDef = propertyDef.AttributeDefs[i];
                                    if (propattr.GetName() != attributeDef.Name)
                                    {
                                        continue;
                                    }
                                    
                                    vm.FavoriteValues = attributeDef.Favorites;
                                    vm.UIName = attributeDef.UIName;
                                    vm.UIDescription = attributeDef.UIDescription;
                                    index = i;
                                    break;
                                }
                            }

                            if (!orderedProperties.ContainsKey(index))
                            {
                                orderedProperties.Add(index, new List<PropertyAttributeBaseViewModel>());
                            }

                            orderedProperties[index].Add(vm);
                            break;
                        }
                    case (int)MPropertyAttribute.eType.kTypeFloat:
                        {
                            int index = int.MaxValue;
                            FloatViewModel vm = new FloatViewModel(propattr as MPropertyAttributeFloat, clip);
                            if (propertyDef != null)
                            {
                                for (int i = 0; i < propertyDef.AttributeDefs.Count; i++)
                                {
                                    ClipAttributeDefinition attributeDef = propertyDef.AttributeDefs[i];
                                    if (propattr.GetName() != attributeDef.Name)
                                    {
                                        continue;
                                    }
                                    
                                    vm.FavoriteValues = attributeDef.Favorites;
                                    vm.UIName = attributeDef.UIName;
                                    vm.UIDescription = attributeDef.UIDescription;
                                    vm.IsBlendIn = attributeDef.IsBlendIn;
                                    vm.IsBlendOut = attributeDef.IsBlendOut;
                                    index = i;
                                    break;
                                }
                            }

                            if (!orderedProperties.ContainsKey(index))
                            {
                                orderedProperties.Add(index, new List<PropertyAttributeBaseViewModel>());
                            }

                            orderedProperties[index].Add(vm);
                            break;
                        }
                    case (int)MPropertyAttribute.eType.kTypeHashString:
                        {
                            int index = int.MaxValue;
                            HashStringViewModel vm = new HashStringViewModel(propattr as MPropertyAttributeHashString, clip);
                            if (propertyDef != null)
                            {
                                for (int i = 0; i < propertyDef.AttributeDefs.Count; i++)
                                {
                                    ClipAttributeDefinition attributeDef = propertyDef.AttributeDefs[i];
                                    if (propattr.GetName() != attributeDef.Name)
                                    {
                                        continue;
                                    }

                                    vm.FavoriteValues = attributeDef.Favorites;
                                    vm.UIName = attributeDef.UIName;
                                    vm.UIDescription = attributeDef.UIDescription;
                                    index = i;
                                    break;
                                }
                            }

                            if (!orderedProperties.ContainsKey(index))
                            {
                                orderedProperties.Add(index, new List<PropertyAttributeBaseViewModel>());
                            }

                            orderedProperties[index].Add(vm);
                            break;
                        }
                    case (int)MPropertyAttribute.eType.kTypeInt:
                        {
                            int index = int.MaxValue;
                            IntViewModel vm = new IntViewModel(propattr as MPropertyAttributeInt, clip);
                            if (propertyDef != null)
                            {
                                for (int i = 0; i < propertyDef.AttributeDefs.Count; i++)
                                {
                                    ClipAttributeDefinition attributeDef = propertyDef.AttributeDefs[i];
                                    if (propattr.GetName() != attributeDef.Name)
                                    {
                                        continue;
                                    }
                                    
                                    vm.FavoriteValues = attributeDef.Favorites;
                                    vm.UIName = attributeDef.UIName;
                                    vm.UIDescription = attributeDef.UIDescription;
                                    index = i;
                                    break;
                                }
                            }

                            if (!orderedProperties.ContainsKey(index))
                            {
                                orderedProperties.Add(index, new List<PropertyAttributeBaseViewModel>());
                            }

                            orderedProperties[index].Add(vm);
                            break;
                        }
                    case (int)MPropertyAttribute.eType.kTypeMatrix34:
                        {
                            int index = int.MaxValue;
                            Matrix34ViewModel vm = new Matrix34ViewModel(propattr as MPropertyAttributeMatrix34, clip);
                            if (propertyDef != null)
                            {
                                for (int i = 0; i < propertyDef.AttributeDefs.Count; i++)
                                {
                                    ClipAttributeDefinition attributeDef = propertyDef.AttributeDefs[i];
                                    if (propattr.GetName() != attributeDef.Name)
                                    {
                                        continue;
                                    }
                                    
                                    vm.FavoriteValues = attributeDef.Favorites;
                                    vm.UIName = attributeDef.UIName;
                                    vm.UIDescription = attributeDef.UIDescription;
                                    index = i;
                                    break;
                                }
                            }

                            if (!orderedProperties.ContainsKey(index))
                            {
                                orderedProperties.Add(index, new List<PropertyAttributeBaseViewModel>());
                            }

                            orderedProperties[index].Add(vm);
                            break;
                        }
                    case (int)MPropertyAttribute.eType.kTypeQuaternion:
                        {
                            int index = int.MaxValue;
                            QuaternionViewModel vm = new QuaternionViewModel(propattr as MPropertyAttributeQuaternion, clip);
                            if (propertyDef != null)
                            {
                                for (int i = 0; i < propertyDef.AttributeDefs.Count; i++)
                                {
                                    ClipAttributeDefinition attributeDef = propertyDef.AttributeDefs[i];
                                    if (propattr.GetName() != attributeDef.Name)
                                    {
                                        continue;
                                    }
                                    
                                    vm.FavoriteValues = attributeDef.Favorites;
                                    vm.UIName = attributeDef.UIName;
                                    vm.UIDescription = attributeDef.UIDescription;
                                    index = i;
                                    break;
                                }
                            }

                            if (!orderedProperties.ContainsKey(index))
                            {
                                orderedProperties.Add(index, new List<PropertyAttributeBaseViewModel>());
                            }

                            orderedProperties[index].Add(vm);
                            break;
                        }
                    case (int)MPropertyAttribute.eType.kTypeSituation:
                        {
                            int index = int.MaxValue;
                            SituationViewModel vm = new SituationViewModel(propattr as MPropertyAttributeSituation, clip);
                            if (propertyDef != null)
                            {
                                for (int i = 0; i < propertyDef.AttributeDefs.Count; i++)
                                {
                                    ClipAttributeDefinition attributeDef = propertyDef.AttributeDefs[i];
                                    if (propattr.GetName() != attributeDef.Name)
                                    {
                                        continue;
                                    }
                                    
                                    vm.FavoriteValues = attributeDef.Favorites;
                                    vm.UIName = attributeDef.UIName;
                                    vm.UIDescription = attributeDef.UIDescription;
                                    index = i;
                                    break;
                                }
                            }

                            if (!orderedProperties.ContainsKey(index))
                            {
                                orderedProperties.Add(index, new List<PropertyAttributeBaseViewModel>());
                            }

                            orderedProperties[index].Add(vm);
                            break;
                        }
                    case (int)MPropertyAttribute.eType.kTypeString:
                        {
                            int index = int.MaxValue;
                            StringViewModel vm = new StringViewModel(propattr as MPropertyAttributeString, clip);
                            if (propertyDef != null)
                            {
                                for (int i = 0; i < propertyDef.AttributeDefs.Count; i++)
                                {
                                    ClipAttributeDefinition attributeDef = propertyDef.AttributeDefs[i];
                                    if (propattr.GetName() != attributeDef.Name)
                                    {
                                        continue;
                                    }
                                    
                                    vm.FavoriteValues = attributeDef.Favorites;
                                    vm.UIName = attributeDef.UIName;
                                    vm.UIDescription = attributeDef.UIDescription;
                                    index = i;
                                    break;
                                }
                            }

                            if (!orderedProperties.ContainsKey(index))
                            {
                                orderedProperties.Add(index, new List<PropertyAttributeBaseViewModel>());
                            }

                            orderedProperties[index].Add(vm);
                            break;
                        }
                    case (int)MPropertyAttribute.eType.kTypeVector3:
                        {
                            int index = int.MaxValue;
                            Vector3ViewModel vm = new Vector3ViewModel(propattr as MPropertyAttributeVector3, clip);
                            if (propertyDef != null)
                            {
                                for (int i = 0; i < propertyDef.AttributeDefs.Count; i++)
                                {
                                    ClipAttributeDefinition attributeDef = propertyDef.AttributeDefs[i];
                                    if (propattr.GetName() != attributeDef.Name)
                                    {
                                        continue;
                                    }
                                    
                                    vm.FavoriteValues = attributeDef.Favorites;
                                    vm.UIName = attributeDef.UIName;
                                    vm.UIDescription = attributeDef.UIDescription;
                                    index = i;
                                    break;
                                }
                            }

                            if (!orderedProperties.ContainsKey(index))
                            {
                                orderedProperties.Add(index, new List<PropertyAttributeBaseViewModel>());
                            }

                            orderedProperties[index].Add(vm);
                            break;
                        }
                    case (int)MPropertyAttribute.eType.kTypeVector4:
                        {
                            int index = int.MaxValue;
                            Vector4ViewModel vm = new Vector4ViewModel(propattr as MPropertyAttributeVector4, clip);
                            if (propertyDef != null)
                            {
                                for (int i = 0; i < propertyDef.AttributeDefs.Count; i++)
                                {
                                    ClipAttributeDefinition attributeDef = propertyDef.AttributeDefs[i];
                                    if (propattr.GetName() != attributeDef.Name)
                                    {
                                        continue;
                                    }
                                    
                                    vm.FavoriteValues = attributeDef.Favorites;
                                    vm.UIName = attributeDef.UIName;
                                    vm.UIDescription = attributeDef.UIDescription;
                                    index = i;
                                    break;
                                }
                            }

                            if (!orderedProperties.ContainsKey(index))
                            {
                                orderedProperties.Add(index, new List<PropertyAttributeBaseViewModel>());
                            }

                            orderedProperties[index].Add(vm);
                            break;
                        }
                    default:
                        break;
                }
            }

            foreach (List<PropertyAttributeBaseViewModel> propertyList in orderedProperties.Values)
            {
                foreach (PropertyAttributeBaseViewModel property in propertyList)
                {
                    pvm.PropertyAttributes.Add(property);
                }
            }

            return pvm;
        }
        #endregion // Private Methods
    }
}
