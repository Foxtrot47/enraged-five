﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using RSG.AnimClipEditor.ViewModel;

namespace RSG.AnimClipEditor.View
{
    /// <summary>
    /// Interaction logic for TagTabHeader.xaml
    /// </summary>
    public partial class TagTabHeader : Label
    {
        public TagTabHeader()
        {
            InitializeComponent();
        }

        protected override void OnPreviewMouseDoubleClick(MouseButtonEventArgs e)
        {

            base.OnPreviewMouseDoubleClick(e);
            ClipEventViewModel ce = this.DataContext as ClipEventViewModel;
            ce.IsExpanded = !ce.IsExpanded;
            foreach (TagViewModel tag in ce.Tags)
            {
                tag.IsVisible = ce.IsExpanded;
            }
            e.Handled = true;
        }
    }
}
