﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using RSG.AnimationMetadata;
using RSG.AnimClipEditor.DataModel;
using RSG.AnimClipEditor.Util;
using RSG.AnimClipEditor.View;
using RSG.AnimClipEditor.ViewModel.PropertyAttribute;
using RSG.Base.ConfigParser;
using RSG.ManagedRage.ClipAnimation;
using RSG.PlaybackControl;
using System.Collections;
using RSG.Base.Configuration;
using RSG.Base.Windows.Helpers;
using RSG.Base.Logging;

using Microsoft.Win32;
using System.Timers;
using System.Threading;

namespace RSG.AnimClipEditor.ViewModel
{
    /// <summary>
    /// Highest level view model component to be appleid to the data context of the clip editor user control
    /// </summary>
    public class ClipViewModel : BaseViewModel
    {
        #region Fields
        /// <summary>
        /// A static boolean property to determine if the properties for all the clip
        /// events are currently hidden or shown.
        /// </summary>
        internal static bool staticShowWrapper;

        /// <summary>
        /// The private instance of the ragWrapper that is used to push events to a running
        /// instance of the game.
        /// </summary>
        private static RagAnimationWrapper ragAnimationWrapper;

        /// <summary>
        /// The private field used for the <see cref="CurrentFramePosition"/> property.
        /// </summary>
        private double currentFramePosition;

        /// <summary>
        /// The private field used for the <see cref="SnapToFrame"/> property.
        /// </summary>
        private bool snapToFrame;

        /// <summary>
        /// The private field used for the <see cref="ShowDurationInMilliseconds"/> property.
        /// </summary>
        static private bool showDurationInMilliseconds = false;

        /// <summary>
        /// The private field used for the <see cref="IdentifyTags"/> property.
        /// </summary>
        static private bool identifyTags = false;

        /// <summary>
        /// The private field used for the <see cref="Loaded"/> property.
        /// </summary>
        private bool loaded;

        /// <summary>
        /// The private field used for the <see cref="SelectedTagTracks"/> property.
        /// </summary>
        private List<ClipEventViewModel> selectedTagTracks;

        /// <summary>
        /// The private field used for the <see cref="SelectedTags"/> property.
        /// </summary>
        private ObservableCollection<TagViewModel> selectedTags;

        /// <summary>
        /// The private field used for the <see cref="ConnectCommand"/> property.
        /// </summary>
        private SimpleCommand connectCommand;

        /// <summary>
        /// The private field used for the <see cref="DisconnectCommand"/> property.
        /// </summary>
        private SimpleCommand disconnectCommand;

        /// <summary>
        /// The private field used for the <see cref="SaveCommand"/> property.
        /// </summary>
        private SimpleCommand saveCommand;

        /// <summary>
        /// The private field used for the <see cref="PreviewCommand"/> property.
        /// </summary>
        private SimpleCommand previewCommand;

        /// <summary>
        /// The private field used for the <see cref="PauseCommand"/> property.
        /// </summary>
        private SimpleCommand pauseCommand;

        /// <summary>
        /// The private field used for the <see cref="PlayCommand"/> property.
        /// </summary>
        private SimpleCommand playCommand;

        /// <summary>
        /// The private field used for the <see cref="RewindCommand"/> property.
        /// </summary>
        private SimpleCommand rewindCommand;

        /// <summary>
        /// The private field used for the <see cref="StepForwardsCommand"/> property.
        /// </summary>
        private SimpleCommand stepForwardsCommand;

        /// <summary>
        /// The private field used for the <see cref="StepBackwardsCommand"/> property.
        /// </summary>
        private SimpleCommand stepBackwardsCommand;

        /// <summary>
        /// The private field used for the <see cref="RefreshCommand"/> property.
        /// </summary>
        private SimpleCommand refreshCommand;

        /// <summary>
        /// The private field used for the <see cref="AddTagCommand"/> property.
        /// </summary>
        private SimpleCommand addTagCommand;

        /// <summary>
        /// The private field used for the <see cref="AddTagShortcutCommand"/> property.
        /// </summary>
        private SimpleCommand addTagShortcutCommand;

        /// <summary>
        /// The private field used for the <see cref="MoveToNextTagCommand"/> property.
        /// </summary>
        private SimpleCommand moveToNextTagCommand;

        /// <summary>
        /// The private field used for the <see cref="MoveToPreviousTagCommand"/> property.
        /// </summary>
        private SimpleCommand moveToPreviousTagCommand;

        /// <summary>
        /// The private field used for the <see cref="TagTracks"/> property.
        /// </summary>
        private ObservableCollection<ClipEventViewModel> tagTracks;

        /// <summary>
        /// The private field used for the <see cref="Model"/> property.
        /// </summary>
        private readonly ClipModel model;

        /// <summary>
        /// Private timer used to test the connection to RAG and update the current frame
        /// position based on the value in RAG.
        /// </summary>
        private DispatcherTimer connectionTimer;

        /// <summary>
        /// Private timer used to test the connection to RAG and update the current frame
        /// position based on the value in RAG.
        /// </summary>
        private DispatcherTimer playTimer;

        /// <summary>
        /// The private field used for the <see cref="FirstVisibleFrame"/> property.
        /// </summary>
        private double firstVisibleFrame;

        /// <summary>
        /// The private field used for the <see cref="LastVisibleFrame"/> property.
        /// </summary>
        private double lastVisibleFrame;

        /// <summary>
        /// The private field used for the <see cref="TagTrackVisiblePixelWidth"/> property.
        /// </summary>
        private double tagTrackVisiblePixelWidth;

        /// <summary>
        /// The private field used for the <see cref="TagTrackWidth"/> property.
        /// </summary>
        private double tagTrackWidth;

        /// <summary>
        /// The private field used for the <see cref="PixelsPerFrame"/> property.
        /// </summary>
        private double pixelsPerFrame;

        /// <summary>
        /// The private field used for the <see cref="DirtyState"/> property.
        /// </summary>
        private bool dirtyState;

        /// <summary>
        /// The private field used to determine whether the position has come from a RAG read
        /// or from the MoVE tool.
        /// </summary>
        private bool positionHasComeFromRAG;

        /// <summary>
        /// The private reference to the view clip editor.
        /// </summary>
        internal ClipEditor clipEditor;
        #endregion

        #region Constructors
        static ClipViewModel()
        {
            ragAnimationWrapper = new RagAnimationWrapper(false, null);
        }

        /// <summary>
        /// Initialises a new instance of the
        /// <see cref="RSG.AnimClipEditor.ViewModel.ClipViewModel"/> class.
        /// </summary>
        /// <param name="clipModel">
        /// The model used to create this view model instance. This cannot be null.
        /// </param>
        public ClipViewModel(ClipModel clipModel)
        {
            if (clipModel == null)
                throw new ArgumentNullException(@"clipModel");

            this.model = clipModel;
            model.ManagedClip.OnLoad += ManagedClip_OnLoad;
            if ((System.Boolean)model.ManagedClip.GetHasLoaded())
            {
                this.TimeData = new TimeVal(0.0, ConvertUtils.PhaseToFrame((float)model.ManagedClip.GetDuration()), 1.0);
            }

            this.selectedTags = new ObservableCollection<TagViewModel>();
            this.selectedTags.CollectionChanged += new NotifyCollectionChangedEventHandler(SelectedTags_CollectionChanged);
            this.DefinedTags = new ObservableCollection<ClipViewModel.PreDefinedTagDefinition>();
            
            this.tagTracks = EventFactory.GenerateClipEvents(clipModel.ManagedClip, this);
            this.tagTracks.CollectionChanged += tagTracks_CollectionChanged;
            _ClipPropertyAttributes = EventFactory.GenerateClipProperties(clipModel.ManagedClip, this);
            _ClipPropertyAttributes.CollectionChanged += (s, e) => { this.OnPropertyChanged("ClipPropertyDefinitions"); };

            this.snapToFrame = true;
            ragAnimationWrapper.ConnectionStatusChanged += this.OnConnectionStatusChanged;

            Application.Current.Exit += Current_Exit;
        }

        void Current_Exit(object sender, ExitEventArgs e)
        {
            this.Disconnect(null);
        }

        void tagTracks_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            this.DirtyState = true;
        }

        private void ManagedClip_OnLoad(object sender, EventArgs e)
        {
            this.TimeData = new TimeVal(0.0, ConvertUtils.PhaseToFrame((float)(sender as MClip).GetDuration()), 1.0);
            this.Reload();
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets a value indicating whether this clip has been edited by the user at
        /// all and is in a dirty state.
        /// </summary>
        public bool DirtyState
        {
            get
            {
                return this.dirtyState;
            }

            internal set
            {
                this.dirtyState = value;
                this.OnPropertyChanged("DirtyState");
                this.OnPropertyChanged("SaveButtonContent");
            }
        }

        /// <summary>
        /// Gets the string content of the save button.
        /// </summary>
        public string SaveButtonContent
        {
            get
            {
                if (this.DirtyState == false)
                {
                    return "Save";
                }
                else
                {
                    return "Save *";
                }
            }
        }

        /// <summary>
        /// Gets the clip model that was used to initialise this view model. 
        /// </summary>
        public ClipModel Model
        {
            get { return model; }
        }

        /// <summary>
        /// Gets a list of tag tracks that are currently being used on this clip.
        /// </summary>
        public ObservableCollection<ClipEventViewModel> TagTracks
        {
            get { return tagTracks; }
        }

        public TimeVal TimeData
        {
            get
            {
                return _TimeData;
            }
            set
            {
                _TimeData = value;
            }
        }
        private TimeVal _TimeData;

        public TagViewModel SelectedTag
        {
            get
            {
                return _SelectedTag;
            }
            set
            {
                if (value != _SelectedTag)
                {
                    _SelectedTag = value;
                    this.OnPropertyChanged("SelectedTag");

                    if (value != null)
                    {
                        foreach (ClipEventViewModel cevm in TagTracks)
                        {
                            if (cevm.Tags.Contains(_SelectedTag))
                            {
                                cevm.IsSelected = true;
                                break;
                            }
                        }
                    }
                }
            }
        }
        TagViewModel _SelectedTag;

        public TagViewModel CombinedSelectedTag
        {
            get
            {
                return _CombinedSelectedTag;
            }
        }
        TagViewModel _CombinedSelectedTag;

        public RelayCommand EnterCommand
        {
            get
            {
                if (enterCommand == null)
                {
                    enterCommand = new RelayCommand(null);
                }

                return this.enterCommand;
            }
        }
        RelayCommand enterCommand;

        public ObservableCollection<PropertyViewModel> ClipPropertyAttributes 
        {
            get 
            {
                return _ClipPropertyAttributes;
            }
        }
        ObservableCollection<PropertyViewModel> _ClipPropertyAttributes;

        public Dictionary<string, ClipPropertyDefinition> PropertyDefinitions
        {
            get
            {
                return ClipTagDefinitions.PropertyDescriptions;
            }
        }

        public List<string> ClipPropertyDefinitions
        {
            get
            {
                var properties = ClipTagDefinitions.ClipPropertyDescriptions;
                var validProperties = new List<string>();
                foreach (var property in properties)
                {
                    validProperties.Add(property.Key);
                }

                return validProperties;
            }
        }

        /// <summary>
        /// Gets or sets a list containing the tag view models that are currently selected
        /// in the view.
        /// </summary>
        public ObservableCollection<TagViewModel> SelectedTags
        {
            get
            {
                return selectedTags;
            }

            set
            {
                if (value == selectedTags)
                    return;

                selectedTags = value;
                this.OnPropertyChanged("SelectedTags");
            }
        }

        /// <summary>
        /// Gets or sets a list containing the tag track view models that are currently
        /// selected in the view.
        /// </summary>
        public List<ClipEventViewModel> SelectedTagTracks
        {
            get
            {
                return selectedTagTracks;
            }

            set
            {
                if (value == selectedTagTracks)
                    return;

                selectedTagTracks = value;
                this.OnPropertyChanged("SelectedTagTracks");
            }
        }

        public ObservableCollection<TagViewModel> SelectedTagTrackTags
        {
            get
            {
                if (SelectedTagTracks != null && SelectedTagTracks.Count > 0)
                {
                    //  Sort the list on position number
                    List<TagViewModel> tagList = SelectedTagTracks[0].Tags.OrderBy(i => i.PositionNumber).ToList();
                    ObservableCollection<TagViewModel> sorted = new ObservableCollection<TagViewModel>(tagList);

                    return sorted;
                }

                return null;
            }
        }

        public void FirePropertyChanged(string property)
        {
            OnPropertyChanged(property);
        }

        /// <summary>
        /// Gets a collection of predefined tag definitions for the selected tag track. 
        /// </summary>
        public ObservableCollection<PreDefinedTagDefinition> DefinedTags
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the unmanaged clip the clip editor is
        /// representing has been fully loaded.
        /// </summary>
        public bool Loaded
        {
            get
            {
                return loaded;
            }

            set
            {
                loaded = value;
                OnPropertyChanged("Loaded");
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the duration of a single tag is shown
        /// in frame counts or milliseconds.
        /// </summary>
        public bool ShowDurationInMilliseconds
        {
            get
            {
                return showDurationInMilliseconds;
            }

            set
            {
                showDurationInMilliseconds = value;
                OnPropertyChanged("ShowDurationInMilliseconds");

                //  Need to update the tag descriptions too
                foreach (ClipEventViewModel cevm in TagTracks)
                {
                    foreach (TagViewModel tvm in cevm.Tags)
                    {
                        tvm.FirePropertyChanged("Description");
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the positional Id of a single tag is shown
        /// </summary>
        public bool IdentifyTags
        {
            get
            {
                return identifyTags;
            }

            set
            {
                identifyTags = value;
                OnPropertyChanged("IdentifyTags");
            }
        }

        /// <summary>
        /// Gets the maximum frame number for the loaded clip using the unmanaged clip
        /// duration and a 30 frames per second multiplier.
        /// </summary>
        public double MaximumFrameCount
        {
            get
            {
                float duration = (float)this.Model.ManagedClip.GetDuration();
                var timeValue = new TimeVal(0.0, ConvertUtils.PhaseToFrame(duration), 1.0);

                return timeValue.End;
            }
        }

        /// <summary>
        /// Gets or sets the current frame current pointed at by the user interface.
        /// </summary>
        public double CurrentFramePosition
        {
            get
            {
                return this.currentFramePosition;
            }

            set
            {
                if (this.currentFramePosition == value)
                {
                    return;
                }

                this.currentFramePosition = value;
                if (!this.IsPlaying && !this.positionHasComeFromRAG)
                {
                    ragAnimationWrapper.CurrentFramePosition = (float)value;
                }

                this.OnPropertyChanged("CurrentFramePosition");
                this.OnPropertyChanged("CurrentPhasePosition");
                this.OnPropertyChanged("CurrentMillisecondsPosition");

                while (this.CurrentFramePosition > this.LastVisibleFrame)
                {
                    double newFirstFrame = this.FirstVisibleFrame + 1;
                    this.FirstVisibleFrame = newFirstFrame;
                }

                while (this.CurrentFramePosition < this.FirstVisibleFrame)
                {
                    double newFirstFrame = this.FirstVisibleFrame - 1;
                    if (newFirstFrame < 0.0)
                    {
                        newFirstFrame = 0.0;
                    }

                    this.FirstVisibleFrame = newFirstFrame;
                }

                this.OnPropertyChanged("FrameMarkerCanvasLeft");
            }
        }

        public double FrameMarkerCanvasLeft
        {
            get
            {
                double firstFrame = this.FirstVisibleFrame;
                double frameStart = this.CurrentFramePosition;
                double frameOffset = frameStart - firstFrame;
                double value = frameOffset * this.PixelsPerFrame;
                return value + 156;
            }
        }

        /// <summary>
        /// Gets the current phase position based on the current frame position.
        /// </summary>
        public double CurrentPhasePosition
        {
            get
            {
                if (this.MaximumFrameCount == 0.0)
                    return 0.0;

                return this.currentFramePosition / this.MaximumFrameCount;
            }
        }

        /// <summary>
        /// Gets the current milliseconds position based on the current frame position.
        /// </summary>
        public double CurrentMillisecondsPosition
        {
            get
            {
                return (int)Math.Ceiling(this.currentFramePosition * 33.33);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the user can move the current frame to a
        /// non-integer value.
        /// </summary>
        public bool SnapToFrame
        {
            get
            {
                return snapToFrame;
            }

            set
            {
                if (value == snapToFrame)
                    return;

                snapToFrame = value;
                if (value)
                {
                    this.CurrentFramePosition = Math.Round(this.CurrentFramePosition);
                }

                OnPropertyChanged("SnapToFrame");
            }
        }

        /// <summary>
        /// Gets a value indicating whether the clip editor is currently connected to a running
        /// instance of Rag.
        /// </summary>
        public bool IsConnected
        {
            get { return ragAnimationWrapper.IsConnected; }
        }

        /// <summary>
        /// Gets the command used to connect to a running instance of Rag.
        /// </summary>
        public SimpleCommand ConnectCommand
        {
            get
            {
                if (this.connectCommand == null)
                {
                    this.connectCommand = new SimpleCommand(this.EstablishConnection, this.IsLoaded);
                }
                return this.connectCommand;
            }
        }

        /// <summary>
        /// Gets the command used to disconnect from a running instance of Rag.
        /// </summary>
        public SimpleCommand DisconnectCommand
        {
            get
            {
                if (this.disconnectCommand == null)
                {
                    this.disconnectCommand = new SimpleCommand(this.Disconnect, this.IsLoaded);
                }
                return this.disconnectCommand;
            }
        }

        /// <summary>
        /// Gets the command used to save the clip to the associated file.
        /// </summary>
        public SimpleCommand SaveCommand
        {
            get
            {
                if (this.saveCommand == null)
                {
                    this.saveCommand = new SimpleCommand(this.SaveClip, this.IsLoaded);
                }

                return this.saveCommand;
            }
        }

        /// <summary>
        /// Gets the command used to preview the clip by rebuilding the zip file and putting
        /// it inside the preview folder.
        /// </summary>
        public SimpleCommand PreviewCommand
        {
            get
            {
                if (this.previewCommand == null)
                {
                    this.previewCommand = new SimpleCommand(this.PreviewClip, this.IsLoaded);
                }
                return this.previewCommand;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the clip is currently being played by the
        /// user in either a forwards or backwards direction.
        /// </summary>
        public bool IsPlaying
        {
            get { return this.PlayRate != 0.0f;  }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the clip is currently being played by the
        /// user in a forwards direction.
        /// </summary>
        public bool IsPlayingForwards
        {
            get
            {
                return this.PlayRate > 0.0f;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the clip is currently being played by the
        /// user in a backwards direction.
        /// </summary>
        public bool IsPlayingBackwards
        {
            get
            {
                return this.PlayRate < 0.0f;
            }
        }

        /// <summary>
        /// Gets or sets the play rate multiplier that is currently being applied to the clip
        /// during playback.
        /// </summary>
        public double PlayRate
        {
            get
            {
                return ragAnimationWrapper.GetPlayRate();
            }
        }

        /// <summary>
        /// Gets the command used to pause the clip.
        /// </summary>
        public SimpleCommand PauseCommand
        {
            get
            {
                if (this.pauseCommand == null)
                {
                    this.pauseCommand = new SimpleCommand(this.Pause, this.IsLoaded);
                }
                return this.pauseCommand;
            }
        }

        /// <summary>
        /// Gets the command used to play the clip.
        /// </summary>
        public SimpleCommand PlayCommand
        {
            get
            {
                if (this.playCommand == null)
                {
                    this.playCommand = new SimpleCommand(this.Play, this.CanPlay);
                }
                return this.playCommand;
            }
        }

        /// <summary>
        /// Gets the command used to rewind the clip.
        /// </summary>
        public SimpleCommand RewindCommand
        {
            get
            {
                if (this.rewindCommand == null)
                {
                    this.rewindCommand = new SimpleCommand(this.Rewind, this.CanRewind);
                }
                return this.rewindCommand;
            }
        }

        /// <summary>
        /// Gets the command used to step forward by a single frame.
        /// </summary>
        public SimpleCommand StepForwardsCommand
        {
            get
            {
                if (this.stepForwardsCommand == null)
                {
                    this.stepForwardsCommand = new SimpleCommand(this.StepForward, this.CanStep);
                }
                return this.stepForwardsCommand;
            }
        }

        /// <summary>
        /// Gets the command used to step backwards by a single frame.
        /// </summary>
        public SimpleCommand StepBackwardsCommand
        {
            get
            {
                if (this.stepBackwardsCommand == null)
                {
                    this.stepBackwardsCommand = new SimpleCommand(this.StepBackward, this.CanStep);
                }
                return this.stepBackwardsCommand;
            }
        }

        /// <summary>
        /// Gets the command used to refresh the loaded clip in memory.
        /// </summary>
        public SimpleCommand RefreshCommand
        {
            get
            {
                if (this.refreshCommand == null)
                {
                    this.refreshCommand = new SimpleCommand(this.RefreshClip, this.CanRefreshClip);
                }
                return this.refreshCommand;
            }
        }

        /// <summary>
        /// Gets the command used to add a tag at the current position for all selected tracks.
        /// </summary>
        public SimpleCommand AddTagCommand
        {
            get
            {
                if (this.addTagCommand == null)
                {
                    this.addTagCommand = new SimpleCommand(this.AddTag, this.CanAddTag);
                }
                return this.addTagCommand;
            }
        }

        /// <summary>
        /// Gets the shortcut command used to add a tag at the current position for a single
        /// selected track.
        /// </summary>
        public SimpleCommand AddTagShortcutCommand
        {
            get
            {
                if (this.addTagShortcutCommand == null)
                {
                    this.addTagShortcutCommand = new SimpleCommand(this.AddTagShortcut, this.CanAddTagShortcut);
                }
                return this.addTagShortcutCommand;
            }
        }

        /// <summary>
        /// Gets the command used to move to the next tag on the selected tracks.
        /// </summary>
        public SimpleCommand MoveToNextTagCommand
        {
            get
            {
                if (this.moveToNextTagCommand == null)
                {
                    this.moveToNextTagCommand = new SimpleCommand(this.MoveToNextTag);
                }
                return this.moveToNextTagCommand;
            }
        }

        /// <summary>
        /// Gets the command used to move to the previous tag on the selected tracks.
        /// </summary>
        public SimpleCommand MoveToPreviousTagCommand
        {
            get
            {
                if (this.moveToPreviousTagCommand == null)
                {
                    this.moveToPreviousTagCommand = new SimpleCommand(this.MoveToPreviousTag);
                }
                return this.moveToPreviousTagCommand;
            }
        }

        /// <summary>
        /// Gets the first frame number that is visible to the user in the tag tracks.
        /// </summary>
        public double FirstVisibleFrame
        {
            get { return firstVisibleFrame; }
            set
            {
                firstVisibleFrame = value;
                OnPropertyChanged("FirstVisibleFrame");
                double visibleFrames = this.TagTrackVisiblePixelWidth / this.pixelsPerFrame;
                this.LastVisibleFrame = this.FirstVisibleFrame + visibleFrames;
                OnPropertyChanged("FrameScrollOffsetInPixels");
            }
        }

        /// <summary>
        /// Gets the last frame number that is visible to the user in the tag tracks.
        /// </summary>
        public double LastVisibleFrame
        {
            get { return this.lastVisibleFrame; }
            set
            {
                this.lastVisibleFrame = value;
                OnPropertyChanged("LastVisibleFrame");
            }
        }

        /// <summary>
        /// Gets the number of pixels a single frame takes up in the tag track.
        /// </summary>
        public double PixelsPerFrame
        {
            get
            {
                return this.pixelsPerFrame;
            }

            set
            {
                this.pixelsPerFrame = value;
                this.TagTrackWidth = this.pixelsPerFrame * MaximumFrameCount;
                double visibleFrames = this.TagTrackVisiblePixelWidth / this.pixelsPerFrame;
                visibleFrames = Math.Round(visibleFrames, 0, MidpointRounding.AwayFromZero);
                this.LastVisibleFrame = this.FirstVisibleFrame + visibleFrames;
                this.OnPropertyChanged("PixelsPerFrame");
                this.OnPropertyChanged("FullTrackWidthInPixels");
                this.OnPropertyChanged("FrameScrollOffsetInPixels");
                this.OnPropertyChanged("FrameMarkerCanvasLeft");
            }
        }

        /// <summary>
        /// Gets the width of the tag track in pixels.
        /// </summary>
        public double TagTrackVisiblePixelWidth
        {
            get
            {
                return this.tagTrackVisiblePixelWidth;
            }

            set
            {
                if (this.tagTrackVisiblePixelWidth == value)
                {
                    return;
                }

                this.tagTrackVisiblePixelWidth = value;
                if (this.pixelsPerFrame <= 1 && this.MaximumFrameCount != 0)
                {
                    this.PixelsPerFrame = value / this.MaximumFrameCount;
                }

                this.OnPropertyChanged("FrameMarkerCanvasLeft");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public double FullTrackWidthInPixels
        {
            get
            {
                return (this.PixelsPerFrame * this.MaximumFrameCount) + 10.0;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public Thickness FrameScrollOffsetInPixels
        {
            get
            {
                return new Thickness(
                    -this.FirstVisibleFrame * this.PixelsPerFrame, 0.0, 0.0, 0.0);
            }
        }

        /// <summary>
        /// Gets the total width of the tag track.
        /// </summary>
        public double TagTrackWidth
        {
            get { return this.tagTrackWidth; }
            set { this.tagTrackWidth = value; OnPropertyChanged("TagTrackWidth"); }
        }

        /// <summary>
        /// Gets the application's folder path in My Documents
        /// </summary>
        public static string ApplicationFolderPath
        {
            get
            {
                string path = Path.Combine(
                    Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments),
                    "RAGE.MOVE");

                if (!Directory.Exists(path))
                    Directory.CreateDirectory(path);

                return path;
            }
        }
        #endregion // Properties

        #region Public Methods

        public void AddClipEvent( ClipEventViewModel newCevm )
        {
            bool exists = false;
            foreach ( ClipEventViewModel cevm in TagTracks )
            {
                if ( cevm.Type == newCevm.Type )
                {
                    exists = true;
                    break;
                }
            }
            if (!exists)
            {
                TagTracks.Add(newCevm);
                this.DirtyState = true;
            }
        }

        public void DeleteSelectedTags()
        {
            if (SelectedTags == null && SelectedTagTracks == null)
                return;

            if (SelectedTags != null && SelectedTags.Count > 0)
            {
                foreach (TagViewModel tag in this.SelectedTags)
                {
                    foreach (ClipEventViewModel cevm in TagTracks)
                    {
                        if (cevm.Tags.Contains(tag))
                        {
                            cevm.Tags.Remove(tag);
                            this.DirtyState = true;
                            this.Model.ManagedClip.RemoveTag(tag.Tag);
                            break;
                        }
                    }
                }
                SelectedTag = null;
                SelectedTags.Clear();
            }
            else if (SelectedTagTracks != null && SelectedTagTracks.Count > 0)
            {
                var selectedTracks = (from e in SelectedTagTracks select e).ToList();
                foreach (var selection in selectedTracks)
                {
                    if (selection.Tags.Count == 0)
                    {
                        this.DirtyState = true;
                        TagTracks.Remove(selection);
                    }
                }
            }

            //  Update the tag list
            OnPropertyChanged("SelectedTagTrackTags");
            //  Update the position numbers
            UpdatePositions();
        }

        public void DeleteTagTrack(ClipEventViewModel track)
        {
            if (track == null || !TagTracks.Contains(track))
                return;

            if (track.Tags != null && track.Tags.Count > 0)
            {
                foreach (var tag in track.Tags)
                {
                    if (this.SelectedTags.Contains(tag))
                    {
                        this.DirtyState = true;
                        this.SelectedTags.Remove(tag);
                    }

                    this.Model.ManagedClip.RemoveTag(tag.Tag);
                }
                track.Tags.Clear();
            }
            
            TagTracks.Remove(track);
        }

        public void DeselectAllTags(TagViewModel except)
        {
            foreach (ClipEventViewModel cevm in TagTracks)
            {
                foreach (TagViewModel tvm in cevm.Tags)
                {
                    if (object.ReferenceEquals(except, tvm))
                    {
                        continue;
                    }

                    tvm.IsSelected = false;
                }

            }
            SelectedTag = null;
        }

        public void DeselectAllTags()
        {
            foreach (ClipEventViewModel cevm in TagTracks)
            {
                foreach (TagViewModel tvm in cevm.Tags)
                {
                    tvm.IsSelected = false;
                }
                
            }
            SelectedTag = null;
        }

        public void AddTagToSelected(float frameNumber, TagTab tab)
        {
            if (SelectedTagTracks == null || SelectedTagTracks.Count > 0)
                return;

            foreach (var selection in this.SelectedTagTracks)
            {
                TagViewModel tvm = EventFactory.CreateTagViewModel(selection.Type, frameNumber, frameNumber, this);
                float endAsFrame = tvm.StartAsFrame + tvm.InitialFrameLength;
                if (endAsFrame > this.MaximumFrameCount)
                {
                    float startAsFrame = (float)this.MaximumFrameCount - tvm.InitialFrameLength;
                    tvm.StartAsFrame = Math.Max(startAsFrame, 0.0f);
                    tvm.EndAsFrame = (float)this.MaximumFrameCount;
                }
                else
                {
                    tvm.EndAsFrame = endAsFrame;
                }
                
                tvm.GlobalWidthInPixels = tab.ActualWidth;
                selection.Tags.Add(tvm);
                this.DirtyState = true;
            }
        }
        #endregion

        #region Event Handlers
        public void Selection_Changed(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "IsSelected")
            {
                if ((sender as TagViewModel).IsSelected)
                {
                    SelectedTag = sender as TagViewModel;
                }
            }
        }

        public void Tags_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (TagViewModel tvm in e.NewItems)
                    {
                        this.Model.ManagedClip.AddTag(tvm.Tag);
                    }
                    break;
                case NotifyCollectionChangedAction.Remove:

                    break;
            }
        }

        public void UpdatePositions()
        {
            foreach (ClipEventViewModel cevm in TagTracks)
            {
                Dictionary<TagViewModel, double> ordered = new Dictionary<TagViewModel, double>();
                int position = 1;

                //  Add the tags to a dictionary for sorting
                foreach (TagViewModel tvm in cevm.Tags)
                {
                    ordered.Add(tvm, tvm.Start);
                }
                //  Sort dictionary
                ordered = ordered.OrderBy(i => i.Value).ToDictionary(i => i.Key, i => i.Value);
                //  Set the position value for each tag in the sorted dictionary
                KeyValuePair<TagViewModel, double> lastPair = new KeyValuePair<TagViewModel, double>(null, 0.0);
                foreach (KeyValuePair<TagViewModel, double> pair in ordered)
                {
                    pair.Key.IsPositionOccupied = false;

                    if (lastPair.Key != null && lastPair.Value == pair.Value)
                    {
                        pair.Key.PositionNumber = lastPair.Key.PositionNumber;
                        //  Set the position occupied but try to keep the selected tag at the front, on the bottom
                        if (pair.Key.IsSelected)
                        {
                            pair.Key.IsPositionOccupied = false;
                            lastPair.Key.IsPositionOccupied = true;
                        }
                        else
                        {
                            pair.Key.IsPositionOccupied = true;
                        }
                    }
                    else
                    {
                        pair.Key.PositionNumber = position;
                    }
                    lastPair = pair;
                    position++;
                }
            }
        }

        #endregion

        #region Commands
        private static Dictionary<string, List<TagViewModel>> Copied = new Dictionary<string, List<TagViewModel>>();
        
        /// <summary>
        /// 
        /// </summary>
        public SimpleCommand CopySelectedTracksCommand
        {
            get
            {
                if (this.copySelectedTracksCommand == null)
                {
                    this.copySelectedTracksCommand = new SimpleCommand(CopySelectedTracksExecute, CopySelectedTracksCanExecute);
                }
                return this.copySelectedTracksCommand;
            }
        }
        private SimpleCommand copySelectedTracksCommand;

        /// <summary>
        /// 
        /// </summary>
        public SimpleCommand CopyTracksCommand
        {
            get
            {
                if (this.copyTracksCommand == null)
                {
                    this.copyTracksCommand = new SimpleCommand(CopyTracksExecute, CopyTracksCanExecute);
                }
                return this.copyTracksCommand;
            }
        }
        private SimpleCommand copyTracksCommand;

        /// <summary>
        /// 
        /// </summary>
        public SimpleCommand ExportAudioTrackCommand
        {
            get
            {
                if (this.exportAudioTrackCommand == null)
                {
                    this.exportAudioTrackCommand = new SimpleCommand(ExportAudioTrackExecute, ExportAudioTrackCanExecute);
                }
                return this.exportAudioTrackCommand;
            }
        }
        private SimpleCommand exportAudioTrackCommand;

        /// <summary>
        /// 
        /// </summary>
        public SimpleCommand ImportAudioTrackCommand
        {
            get
            {
                if (this.importAudioTrackCommand == null)
                {
                    this.importAudioTrackCommand = new SimpleCommand(ImportAudioTrackExecute, ImportAudioTrackCanExecute);
                }
                return this.importAudioTrackCommand;
            }
        }
        private SimpleCommand importAudioTrackCommand;

        /// <summary>
        /// 
        /// </summary>
        public SimpleCommand CopyTagsOnSelectedTracksCommand
        {
            get
            {
                if (this.copyTagsOnSelectedTracksCommand == null)
                {
                    this.copyTagsOnSelectedTracksCommand = new SimpleCommand(CopyTagsOnSelectedTracksExecute, CopyTagsOnSelectedTracksCanExecute);
                }
                return this.copyTagsOnSelectedTracksCommand;
            }
        }
        private SimpleCommand copyTagsOnSelectedTracksCommand;

        /// <summary>
        /// 
        /// </summary>
        public SimpleCommand CopySelectedTagsOnSelectedTracksCommand
        {
            get
            {
                if (this.copySelectedTagsOnSelectedTracksCommand == null)
                {
                    this.copySelectedTagsOnSelectedTracksCommand = new SimpleCommand(CopySelectedTagsOnSelectedTracksExecute, CopySelectedTagsOnSelectedTracksCanExecute);
                }
                return this.copySelectedTagsOnSelectedTracksCommand;
            }
        }
        private SimpleCommand copySelectedTagsOnSelectedTracksCommand;

        /// <summary>
        /// 
        /// </summary>
        public SimpleCommand PasteOverwriteCommand
        {
            get
            {
                if (this.pasteOverwriteCommand == null)
                {
                    this.pasteOverwriteCommand = new SimpleCommand(PasteOverwriteExecute, PasteCanExecute);
                }
                return this.pasteOverwriteCommand;
            }
        }
        private SimpleCommand pasteOverwriteCommand;

        /// <summary>
        /// 
        /// </summary>
        public SimpleCommand PasteMergeCommand
        {
            get
            {
                if (this.pasteMergeCommand == null)
                {
                    this.pasteMergeCommand = new SimpleCommand(PasteMergeExecute, PasteCanExecute);
                }
                return this.pasteMergeCommand;
            }
        }
        private SimpleCommand pasteMergeCommand;
        #endregion

        #region CopySelectedTracksCommand
        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameter"></param>
        private bool CopySelectedTracksCanExecute(object parameter)
        {
            if (this.SelectedTagTracks == null || this.SelectedTagTracks.Count == 0)
                return false;

            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameter"></param>
        private void CopySelectedTracksExecute(object parameter)
        {
            if (ClipViewModel.Copied == null)
                Copied = new Dictionary<string, List<TagViewModel>>();
            else
                Copied.Clear();

            foreach (ClipEventViewModel selection in this.SelectedTagTracks)
            {
                List<TagViewModel> list = new List<TagViewModel>();
                foreach (TagViewModel tag in selection.Tags)
                {
                    list.Add(tag);
                }

                Copied.Add(selection.Type, list);
            }
        }
        #endregion

        #region CopyTracksCommand
        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameter"></param>
        private bool CopyTracksCanExecute(object parameter)
        {
            if (this.TagTracks == null || this.TagTracks.Count == 0)
                return false;

            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameter"></param>
        private void CopyTracksExecute(object parameter)
        {
            if (ClipViewModel.Copied == null)
                Copied = new Dictionary<string, List<TagViewModel>>();
            else
                Copied.Clear();

            foreach (ClipEventViewModel track in this.TagTracks)
            {
                List<TagViewModel> list = new List<TagViewModel>();
                foreach (TagViewModel tag in track.Tags)
                {
                    list.Add(tag);
                }

                Copied.Add(track.Type, list);
            }
        }
        #endregion

        #region ExportTracksCommand
        private string audioCSVHeader = "#,Name,Start,End,Length";
        /// <summary>
        /// 
        /// </summary>
        /// <param name="milliseconds"></param>
        /// <returns></returns>
        private string MillisecondsToExportFormat(int milliseconds)
        {
            TimeSpan t = TimeSpan.FromMilliseconds(milliseconds);
            string exportFormat = string.Format("{0}:{1:D2}.{2}", t.Minutes, t.Seconds, t.Milliseconds);

            return exportFormat;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameter"></param>
        private bool ExportAudioTrackCanExecute(object parameter)
        {
            if (this.TagTracks == null || this.TagTracks.Count == 0)
                return false;

            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameter"></param>
        private void ExportAudioTrackExecute(object parameter)
        {
            //  Create the application folder
            if (!Directory.Exists(ApplicationFolderPath))
            {
                Directory.CreateDirectory(ApplicationFolderPath);
            }

            //  Find the audio track
            foreach (ClipEventViewModel track in this.TagTracks)
            {
                if (track.Type.Equals("Audio", StringComparison.OrdinalIgnoreCase))
                {
                    //  Create a writer to write out the file
                    string animName = Path.GetFileNameWithoutExtension(this.Model.ManagedClip.GetFilename());
                    string filename = Path.Combine(ApplicationFolderPath, animName + "_Audio_Track.csv");
                    TextWriter writer = new StreamWriter(filename);
                    //  Write the header
                    writer.WriteLine(audioCSVHeader);
                    int position = 1;
                    //  Write the tags out
                    foreach (TagViewModel tag in track.Tags)
                    {
                        writer.WriteLine("R" + position + "," +
                            tag.Property.PropertyAttributes[0].GetValueAsString() + "," +
                            MillisecondsToExportFormat(tag.StartAsMillisecond) + "," +
                            MillisecondsToExportFormat((int)tag.EndAsMillisecond) + "," +
                            MillisecondsToExportFormat((int)tag.LengthAsMilliseconds));
                        position++;
                    }
                    //  Close the file
                    writer.Close();
                    //  Export completed ok
                    LogFactory.ApplicationLog.Message("Audio track exported to '" + filename + "'");
                    return;
                }
            }

            //  There wasn't an audio track, add a message to the log
            LogFactory.ApplicationLog.Error("There is no Audio track!");
        }
        #endregion

        #region ImportTracksCommand
        /// <summary>
        /// 
        /// </summary>
        /// <param name="milliseconds"></param>
        /// <returns></returns>
        private int MillisecondsFromExportFormat(string formattedString)
        {
            //  The format we use only has minutes:seconds.milliseconds so just add hours at the front for easy parsing
            string stringToParse = "00:" + formattedString;
            TimeSpan t = TimeSpan.Parse(stringToParse);

            return (int)t.TotalMilliseconds;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameter"></param>
        private bool ImportAudioTrackCanExecute(object parameter)
        {
            if (this.TagTracks == null || this.TagTracks.Count == 0)
                return false;

            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameter"></param>
        private void ImportAudioTrackExecute(object parameter)
        {
            //  Find the audio track
            foreach (ClipEventViewModel track in this.TagTracks)
            {
                if (track.Type.Equals("Audio", StringComparison.OrdinalIgnoreCase))
                {
                    OpenFileDialog dlg = new OpenFileDialog();

                    dlg.InitialDirectory = ApplicationFolderPath;
                    dlg.Filter = "CSV Files (*.csv)|*.csv|All Files (*.*)|*.*";
                    dlg.Title = "Open";
                    dlg.ShowDialog();

                    if (!String.IsNullOrEmpty(dlg.FileName))
                    {
                        //  Create the text reader to read the file
                        TextReader reader = new StreamReader(dlg.FileName);
                        //  Read the first line which should be the header
                        string header = reader.ReadLine();

                        if (header.Equals(audioCSVHeader, StringComparison.OrdinalIgnoreCase))
                        {
                            //  Read through the lines for each tag
                            string line = reader.ReadLine();
                            while (line != null && line.Length > 0)
                            {
                                //  Split the values on comma
                                string [] values = line.Split(',');
                                string audioId = values[1];
                                int start = MillisecondsFromExportFormat(values[2]);
                                int end = MillisecondsFromExportFormat(values[3]);
                                int length = MillisecondsFromExportFormat(values[4]);

                                //  Create a new tag of the Audio type
                                TagViewModel tvm = EventFactory.CreateTagViewModel(track.Type, 0, 0, this);
                                if (tvm != null)
                                {
                                    tvm.GlobalWidthInPixels = track.TabTagWidth;
                                    //  Set the Audio Id string
                                    tvm.Property.PropertyAttributes[0].SetFromString(audioId);
                                    //  Set the end before the start or it doesn't work
                                    tvm.EndAsMillisecond = end;
                                    tvm.StartAsMillisecond = start;
                                    //  Add the tag to the list
                                    track.Tags.Add(tvm);
                                }

                                //  Read the next line
                                line = reader.ReadLine();
                            }

                            //  Close the file
                            reader.Close();
                            //  Export completed ok
                            LogFactory.ApplicationLog.Message("Audio track imported from '" + dlg.FileName + "' successfully.");
                        }
                        else
                        {
                            LogFactory.ApplicationLog.Error("File '{0}' is not correct Audio SCV file format.", dlg.FileName);
                            return;
                        }
                    }
                    return;
                }
            }

            //  There wasn't an audio track, add a message to the log
            LogFactory.ApplicationLog.Error("There is no Audio track!");
        }
        #endregion

        #region CopyTagsOnSelectedTracks
        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameter"></param>
        private bool CopyTagsOnSelectedTracksCanExecute(object parameter)
        {
            if (this.SelectedTagTracks == null || this.SelectedTagTracks.Count == 0)
                return false;

            foreach (ClipEventViewModel cevm in this.SelectedTagTracks)
            {
                if (cevm.Tags.Count > 0)
                    return true;
            }

            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameter"></param>
        private void CopyTagsOnSelectedTracksExecute(object parameter)
        {
            if (ClipViewModel.Copied == null)
                Copied = new Dictionary<string, List<TagViewModel>>();
            else
                Copied.Clear();

            foreach (ClipEventViewModel track in this.SelectedTagTracks)
            {
                Copied.Add(track.Type, new List<TagViewModel>());
                foreach (TagViewModel tag in track.Tags)
                {
                    Copied[track.Type].Add(tag);
                }
            }
        }
        #endregion

        #region CopyTagsOnTracks
        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameter"></param>
        private bool CopySelectedTagsOnSelectedTracksCanExecute(object parameter)
        {
            if (this.SelectedTagTracks == null || this.SelectedTagTracks.Count == 0)
                return false;

            foreach (ClipEventViewModel cevm in this.SelectedTagTracks)
            {
                foreach (TagViewModel tag in cevm.Tags)
                {
                    if (tag.IsSelected)
                        return true;
                }
            }

            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameter"></param>
        private void CopySelectedTagsOnSelectedTracksExecute(object parameter)
        {
            if (ClipViewModel.Copied == null)
                Copied = new Dictionary<string, List<TagViewModel>>();
            else
                Copied.Clear();

            foreach (ClipEventViewModel track in this.SelectedTagTracks)
            {
                Copied.Add(track.Type, new List<TagViewModel>());
                foreach (TagViewModel tag in track.Tags)
                {
                    if (tag.IsSelected)
                    {
                        Copied[track.Type].Add(tag);
                    }
                }
            }
        }
        #endregion

        #region PasteCommands
        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameter"></param>
        private bool PasteCanExecute(object parameter)
        {
            if (ClipViewModel.Copied == null || ClipViewModel.Copied.Count == 0)
                return false;

            foreach (KeyValuePair<string, List<TagViewModel>> kvp in ClipViewModel.Copied)
            {
                if (kvp.Value == null)
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameter"></param>
        private void PasteOverwriteExecute(object parameter)
        {
            Paste(false);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameter"></param>
        private void PasteMergeExecute(object parameter)
        {
            Paste(true);
        }
        #endregion

        #region Private Methods
        internal void AddClipPropertyExecute(string propertyName)
        {
            if (!ClipTagDefinitions.ClipPropertyDescriptions.ContainsKey(propertyName))
                return;

            List<MPropertyAttribute> attributes = new List<MPropertyAttribute>();
            List<PropertyAttributeBaseViewModel> propertyViewModels = new List<PropertyAttributeBaseViewModel>();
            ClipPropertyDefinition definition = ClipTagDefinitions.ClipPropertyDescriptions[propertyName];
            foreach (ClipAttributeDefinition attribute in definition.AttributeDefs)
            {
                switch (attribute.Type)
                {
                    case (int)MPropertyAttribute.eType.kTypeBool:
                        {
                            string[] names = attribute.Name.Split(';');
                            if (names.Length > 1)
                            {
                                foreach (string name in names)
                                {
                                    MPropertyAttributeBool propattr = new MPropertyAttributeBool(name, false);
                                    attributes.Add(propattr);

                                    bool initialValue = false;
                                    if (attribute.InitialValue == name)
                                        initialValue = true;

                                    propertyViewModels.Add(new BoolViewModel(propattr, this)
                                    {
                                        FavoriteValues = attribute.Favorites,
                                        UIName = attribute.UIName,
                                        UIDescription = attribute.UIDescription,
                                        IsApartOfRadioGroup = true,
                                        RadioButtonGroupName = attribute.Name,
                                        Value = initialValue,
                                    });
                                }
                            }
                            else
                            {
                                MPropertyAttributeBool propattr = new MPropertyAttributeBool(attribute.Name, false);
                                attributes.Add(propattr);
                                propertyViewModels.Add(new BoolViewModel(propattr, attribute.InitialValue, this)
                                {
                                    FavoriteValues = attribute.Favorites,
                                    UIName = attribute.UIName,
                                    UIDescription = attribute.UIDescription,
                                });
                            }
                        }
                        break;
                    case (int)MPropertyAttribute.eType.kTypeFloat:
                        {
                            MPropertyAttributeFloat propattr = new MPropertyAttributeFloat(attribute.Name, 0.0);
                            attributes.Add(propattr);
                            propertyViewModels.Add(new FloatViewModel(propattr, this)
                            {
                                FavoriteValues = attribute.Favorites,
                                UIName = attribute.UIName,
                                UIDescription = attribute.UIDescription,
                                IsBlendIn = attribute.IsBlendIn,
                                IsBlendOut = attribute.IsBlendOut,
                            });
                        }
                        break;
                    case (int)MPropertyAttribute.eType.kTypeHashString:
                        {
                            MPropertyAttributeHashString propattr = new MPropertyAttributeHashString(attribute.Name, 0);
                            attributes.Add(propattr);
                            propertyViewModels.Add(new HashStringViewModel(propattr, this)
                            {
                                FavoriteValues = attribute.Favorites,
                                UIName = attribute.UIName,
                                UIDescription = attribute.UIDescription,
                            });
                        }
                        break;
                    case (int)MPropertyAttribute.eType.kTypeInt:
                        {
                            MPropertyAttributeInt propattr = new MPropertyAttributeInt(attribute.Name, 0);
                            attributes.Add(propattr);
                            propertyViewModels.Add(new IntViewModel(propattr, this)
                            {
                                FavoriteValues = attribute.Favorites,
                                UIName = attribute.UIName,
                                UIDescription = attribute.UIDescription,
                            });
                        }
                        break;
                    case (int)MPropertyAttribute.eType.kTypeString:
                        {
                            MPropertyAttributeString propattr = new MPropertyAttributeString(attribute.Name, "CHANGEME");
                            attributes.Add(propattr);
                            propertyViewModels.Add(new StringViewModel(propattr, this)
                            {
                                FavoriteValues = attribute.Favorites,
                                UIName = attribute.UIName,
                                UIDescription = attribute.UIDescription,
                            });
                        }
                        break;
                    default:
                        break;
                }
            }
            if (attributes.Count <= 0)
                return;

            MProperty property = new MProperty(propertyName, attributes);
            Model.ManagedClip.GetProperties().Add(property);

            PropertyViewModel pvm = new PropertyViewModel(property);
            foreach (PropertyAttributeBaseViewModel vm in propertyViewModels)
                pvm.PropertyAttributes.Add(vm);

            this.ClipPropertyAttributes.Add(pvm);
        }

        internal void RemoveClipPropertyExecute(object parameter)
        {
            PropertyViewModel vm = parameter as PropertyViewModel;
            if (vm == null)
                return;

            MProperty property = vm.Property;
            if (property == null)
                return;

            Model.ManagedClip.GetProperties().Remove(property);
            this.ClipPropertyAttributes.Remove(vm);
        }

        private void SelectedTags_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (this.CombinedSelectedTag != null)
            {
                this.CombinedSelectedTag.PropertyChanged -= PositionPropertyChanged;
                foreach (PropertyAttributeBaseViewModel pavm in this.CombinedSelectedTag.Property.PropertyAttributes)
                {
                    pavm.ValueChanged += new EventHandler(AttributeValueChanged);
                }
            }

            if (this.SelectedTags.Count <= 0)
            {
                _CombinedSelectedTag = null;
                OnPropertyChanged("CombinedSelectedTag");
                return;
            }
            if (this.SelectedTags.Count == 1)
            {
                _CombinedSelectedTag = this.SelectedTags[0];
                OnPropertyChanged("CombinedSelectedTag");
                return;
            }

            // Have multiple tags selected but they should all come for a single event to edit.
            ClipEventViewModel theEvent = null;
            List<ClipEventViewModel> allEvents = new List<ClipEventViewModel>();
            bool sameEvent = true;
            bool allDifferentEvents = true;
            foreach (TagViewModel tag in this.SelectedTags)
            {
                bool found = false;
                foreach (ClipEventViewModel cevm in TagTracks)
                {
                    if (cevm.Tags.Contains(tag))
                    {
                        found = true;
                        if (allEvents.Contains(cevm))
                            allDifferentEvents = false;

                        allEvents.Add(cevm);
                        if (theEvent != null)
                        {
                            if (!object.ReferenceEquals(theEvent, cevm))
                            {
                                sameEvent = false;
                                break;
                            }
                        }

                        theEvent = cevm;
                        break;
                    }
                }
                if (!found)
                {
                    _CombinedSelectedTag = null;
                    OnPropertyChanged("CombinedSelectedTag");
                    return;
                }
                if (!sameEvent)
                    break;
            }
            if (!sameEvent && !allDifferentEvents)
            {
                _CombinedSelectedTag = null;
                OnPropertyChanged("CombinedSelectedTag");
                return;
            }
            else if (!sameEvent && allDifferentEvents)
            {
                // Can edit the position if all different
                TagViewModel positionCombined = EventFactory.CreateTagViewModel(theEvent.Type, 0.0f, 0.0f, this);
                positionCombined.Property.PropertyAttributes.Clear();
                positionCombined.CanEditAttributes = false;
                positionCombined.PropertyChanged += PositionPropertyChanged;
                this._CombinedSelectedTag = positionCombined;
                OnPropertyChanged("CombinedSelectedTag");
                return;
            }

            // Check to see if any attributes are available 
            if (this.SelectedTags[0].Property.PropertyAttributesCount <= 0)
            {
                _CombinedSelectedTag = null;
                OnPropertyChanged("CombinedSelectedTag");
                return;
            }

            // Ok we can multi edit the attributes
            TagViewModel combined = EventFactory.CreateTagViewModel(theEvent.Type, 0.0f, 0.0f, this);
            foreach (PropertyAttributeBaseViewModel pavm in combined.Property.PropertyAttributes)
            {
                pavm.ValueChanged += AttributeValueChanged;
            }
            combined.CanEditPosition = false;
            this._CombinedSelectedTag = combined;
            OnPropertyChanged("CombinedSelectedTag");
        }

        private void PositionPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Start")
            {
                foreach (TagViewModel tag in this.SelectedTags)
                {
                    tag.Start = (sender as TagViewModel).Start;
                }
            }
            else if (e.PropertyName == "End")
            {
                foreach (TagViewModel tag in this.SelectedTags)
                {
                    tag.End = (sender as TagViewModel).End;
                }
            }
        }

        private void AttributeValueChanged(object sender, EventArgs e)
        {
            if (sender is HashStringViewModel)
            {
                foreach (TagViewModel tag in this.SelectedTags)
                {
                    foreach (PropertyAttributeBaseViewModel property in tag.Property.PropertyAttributes)
                    {
                        if (property is HashStringViewModel && property.Name == (sender as HashStringViewModel).Name)
                        {
                            (property as HashStringViewModel).Value = (sender as HashStringViewModel).Value;
                        }
                    }
                }
            }
            else if (sender is StringViewModel)
            {
                foreach (TagViewModel tag in this.SelectedTags)
                {
                    foreach (PropertyAttributeBaseViewModel property in tag.Property.PropertyAttributes)
                    {
                        if (property is StringViewModel && property.Name == (sender as StringViewModel).Name)
                        {
                            (property as StringViewModel).Value = (sender as StringViewModel).Value;
                        }
                    }
                }
            }
            else if (sender is BoolViewModel)
            {
                foreach (TagViewModel tag in this.SelectedTags)
                {
                    foreach (PropertyAttributeBaseViewModel property in tag.Property.PropertyAttributes)
                    {
                        if (property is BoolViewModel && property.Name == (sender as BoolViewModel).Name)
                        {
                            (property as BoolViewModel).Value = (sender as BoolViewModel).Value;
                        }
                    }
                }
            }
            else if (sender is FloatViewModel)
            {
                foreach (TagViewModel tag in this.SelectedTags)
                {
                    foreach (PropertyAttributeBaseViewModel property in tag.Property.PropertyAttributes)
                    {
                        if (property is FloatViewModel && property.Name == (sender as FloatViewModel).Name)
                        {
                            (property as FloatViewModel).Value = (sender as FloatViewModel).Value;
                        }
                    }
                }
            }
            else if (sender is IntViewModel)
            {
                foreach (TagViewModel tag in this.SelectedTags)
                {
                    foreach (PropertyAttributeBaseViewModel property in tag.Property.PropertyAttributes)
                    {
                        if (property is IntViewModel && property.Name == (sender as IntViewModel).Name)
                        {
                            (property as IntViewModel).Value = (sender as IntViewModel).Value;
                        }
                    }
                }
            }
        }

        private void Paste(bool merge)
        {
            this.Paste(merge, false);
        }

        public void Paste(bool merge, bool byFrame)
        {
            List<string> uncopiedTracks = new List<string>();
            Dictionary<string, List<TagViewModel>> validCopied = new Dictionary<string, List<TagViewModel>>();
            foreach (KeyValuePair<string, List<TagViewModel>> kvp in ClipViewModel.Copied)
            {
                if (ClipTagDefinitions.PropertyDescriptions.ContainsKey(kvp.Key))
                {
                    validCopied.Add(kvp.Key, kvp.Value);
                    continue;
                }

                uncopiedTracks.Add(kvp.Key);
            }

            if (uncopiedTracks.Count > 0)
            {
                string msg = "Unable to copy the following tracks due to them not being defined inside the clip_tag_definitions.xml file. ";
                msg += "You need to add a definition for the track inside the file and restart MoVE to support the track.\n\n";
                foreach (string uncopiedTrack in uncopiedTracks)
                {
                    msg += uncopiedTrack + "\n";
                }

                msg += "\nDo you wish to continue with the paste and ignore the above tracks?";

                MessageBoxResult result = MessageBox.Show(msg, "Warning", MessageBoxButton.YesNo, MessageBoxImage.Warning);
                if (result == MessageBoxResult.No)
                {
                    return;
                }
            }

            foreach (KeyValuePair<string, List<TagViewModel>> kvp in validCopied)
            {
                ClipEventViewModel clip = this.GetTrack(kvp.Key);
                if (clip == null)
                {
                    clip = new ClipEventViewModel(kvp.Key, new ObservableCollection<TagViewModel>(), this);
                    clip.Tags.CollectionChanged += this.Tags_CollectionChanged;
                    this.TagTracks.Add(clip);
                }

                if (!merge)
                {
                    foreach (TagViewModel tag in clip.Tags)
                    {
                        this.Model.ManagedClip.RemoveTag(tag.Tag);
                    }

                    clip.Tags.Clear();
                }
                
                foreach (TagViewModel tag in kvp.Value as List<TagViewModel>)
                {
                    if (byFrame)
                    {
                        if (tag.StartAsFrame < this.TimeData.Start)
                            continue;
                        if (tag.EndAsFrame > this.TimeData.End)
                            continue;
                    }

                    float start = tag.Start;
                    float end = tag.End;
                    if (byFrame)
                    {
                        // Frame to phase.
                        double phasePerFrame = 1 / clip.ClipViewModel.MaximumFrameCount;
                        start = (float)(phasePerFrame * tag.StartAsFrame);
                        end = (float)(phasePerFrame * tag.EndAsFrame);
                    }

                    TagViewModel newTag = EventFactory.CreateTagViewModel(kvp.Key, start, end, this);
                    foreach (PropertyAttributeBaseViewModel property in tag.Property.PropertyAttributes)
                    {
                        foreach (PropertyAttributeBaseViewModel newProperty in newTag.Property.PropertyAttributes)
                        {
                            if (newProperty.Name != property.Name)
                            {
                                continue;
                            }

                            newProperty.SetFromOther(property);
                            break;
                        }
                    }

                    clip.Tags.Add(newTag);
                }
            }

            this.DirtyState = true;
        }

        private ClipEventViewModel GetTrack(string typeName)
        {
            foreach (ClipEventViewModel cevm in this.TagTracks)
            {
                if (cevm.Type == typeName)
                    return cevm;
            }
            return null;
        }
        #endregion

        #region Methods
        public void OnTagSelected()
        {
            if (this.clipEditor != null)
            {
                this.clipEditor.OnTagSelected();
            }
        }

        /// <summary>
        /// Reloads the clip view model due to a major property change, for example the
        /// filename has changed.
        /// </summary>
        public void Reload()
        {
            this.tagTracks.Clear();
            foreach (ClipEventViewModel vm in EventFactory.GenerateClipEvents(model.ManagedClip, this))
            {
                this.tagTracks.Add(vm);
            }

            this.Loaded = (bool)Model.ManagedClip.GetHasLoaded();
            string filename = this.Model.ManagedClip.GetFilename();
            if (!string.IsNullOrWhiteSpace(filename))
            {
                ragAnimationWrapper.EnsureAnimationIsLoaded(filename);
            }

            this.OnPropertyChanged("MaximumFrameCount");

            if (this.pixelsPerFrame <= 1 && this.MaximumFrameCount != 0)
            {
                this.PixelsPerFrame = this.TagTrackVisiblePixelWidth / this.MaximumFrameCount;
            }

            this.DirtyState = false;
        }

        /// <summary>
        /// Disposes of this view model by detaching the event handlers and re activating the
        /// ped through the rag wrapper if requested.
        /// </summary>
        /// <param name="reactivatePed">
        /// True if the connection 
        /// </param>
        public void Dispose(bool reactivatePed)
        {
            this.Pause(null);
            this.CurrentFramePosition = 0.0;

            if (connectionTimer != null)
            {
                connectionTimer.Tick -= new EventHandler(OnConnectionTimerTick);
                connectionTimer.Stop();
                connectionTimer = null;
            }

            if (playTimer != null)
            {
                playTimer.Tick -= new EventHandler(OnPlayTimerTick);
                playTimer.Stop();
                playTimer = null;
            }

            if (!reactivatePed)
            {
                return;
            }

            this.Disconnect(null);
        }

        public void PushRAGValues()
        {
            if (!IsConnected)
            {
                this.EstablishConnection(null);
            }

            string filename = this.Model.ManagedClip.GetFilename();
            if (!string.IsNullOrWhiteSpace(filename))
            {
                ragAnimationWrapper.EnsureAnimationIsLoaded(filename);
            }

            if (this.connectionTimer != null)
            {
                connectionTimer.Tick -= new EventHandler(OnConnectionTimerTick);
                connectionTimer.Stop();
                connectionTimer = null;
            }

            connectionTimer = new DispatcherTimer();
            connectionTimer.Interval = TimeSpan.FromMilliseconds(5000);
            connectionTimer.Tick += new EventHandler(OnConnectionTimerTick);
            connectionTimer.Start();

            //  Create the RAG connection test timer
            if (this.playTimer != null)
            {
                playTimer.Tick -= new EventHandler(OnPlayTimerTick);
                playTimer.Stop();
                playTimer = null;
            }

            playTimer = new DispatcherTimer();
            playTimer.Interval = TimeSpan.FromMilliseconds(1);
            playTimer.Tick += new EventHandler(OnPlayTimerTick);
            playTimer.Start();
        }

        /// <summary>
        /// Determines whether the add tag shortcut command can be executed by the user.
        /// </summary>
        /// <param name="parameter">
        /// The command parameter passed in from the view.
        /// </param>
        /// <returns>
        /// True if the add tag shortcut command can be executed by the user; otherwise, false.
        /// </returns>
        private bool CanAddTagShortcut(object parameter)
        {
            if (this.SelectedTagTracks == null || this.SelectedTagTracks.Count != 1)
                return false;

            return true;
        }

        /// <summary>
        /// Gets called when the add tag shortcut command gets executed. Adds the predefined
        /// tag specified by the command parameter to the one and only tag track.
        /// </summary>
        /// <param name="parameter">
        /// The command parameter passed in from the view.
        /// </param>
        private void AddTagShortcut(object parameter)
        {
            int index = 0;
            if (!int.TryParse(parameter.ToString(), out index))
                return;

            if (this.DefinedTags.Count <= index)
                return;

            this.AddTag(this.DefinedTags[index].Group, this.SelectedTagTracks[0]);
        }

        /// <summary>
        /// Adds a new tag at the current frame position for each track in the specified
        /// enumerable collection.
        /// </summary>
        /// <param name="tracks">
        /// A iterator around the tracks that need a new tag adding.
        /// </param>
        internal void AddTag(IEnumerable<ClipEventViewModel> tracks)
        {
            if (tracks == null)
                return;

            double frame = this.CurrentFramePosition;
            if (this.SnapToFrame)
                frame = Math.Round(frame);

            float start = 0.0f;
            if (this.MaximumFrameCount != 0.0)
                start = (float)(frame / this.MaximumFrameCount);

            foreach (ClipEventViewModel track in tracks)
            {
                double globalWidthInPixels = track.TabTagWidth;

                TagViewModel tvm = EventFactory.CreateTagViewModel(track.Type, start, start, this);
                if (tvm != null)
                {
                    tvm.GlobalWidthInPixels = globalWidthInPixels;
                    float endAsFrame = tvm.StartAsFrame + tvm.InitialFrameLength;
                    if (endAsFrame > this.MaximumFrameCount)
                    {
                        float startAsFrame = (float)this.MaximumFrameCount - tvm.InitialFrameLength;
                        tvm.StartAsFrame = Math.Max(startAsFrame, 0.0f);
                        tvm.EndAsFrame = (float)this.MaximumFrameCount;
                    }
                    else
                    {
                        tvm.EndAsFrame = endAsFrame;
                    }

                    track.Tags.Add(tvm);
                }
                else
                {
                    ShowErrorMessage("No description for type: " + track.Type);
                }
            }
        }

        /// <summary>
        /// Adds a new tag at the current frame position for the specified track using the
        /// specified definition to set its values.
        /// </summary>
        /// <param name="group">
        /// The property group definition that is used to set the values on the tag.
        /// </param>
        /// <param name="track">
        /// The track the tag should be added to.
        /// </param>
        internal void AddTag(ClipPropertyDefinition.PropertyGroup group, ClipEventViewModel track)
        {
            if (track == null || group == null)
                return;

            double frame = this.CurrentFramePosition;
            if (this.SnapToFrame)
                frame = Math.Round(frame);

            float start = 0.0f;
            if (this.MaximumFrameCount != 0.0)
                start = (float)(frame / this.MaximumFrameCount);

            double globalWidthInPixels = track.TabTagWidth;

            TagViewModel tvm = EventFactory.CreateTagViewModel(track.Type, start, start, this);
            if (tvm != null)
            {
                float endAsFrame = tvm.StartAsFrame + tvm.InitialFrameLength;
                if (endAsFrame > this.MaximumFrameCount)
                {
                    float startAsFrame = (float)this.MaximumFrameCount - tvm.InitialFrameLength;
                    tvm.StartAsFrame = Math.Max(startAsFrame, 0.0f);
                    tvm.EndAsFrame = (float)this.MaximumFrameCount;
                }
                else
                {
                    tvm.EndAsFrame = endAsFrame;
                }

                tvm.GlobalWidthInPixels = globalWidthInPixels;
                foreach (var groupProperty in group.PropertyValues)
                {
                    foreach (var attribute in tvm.Property.PropertyAttributes)
                    {
                        if (attribute.Name == groupProperty.Key)
                        {
                            attribute.SetFromString(groupProperty.Value);
                        }
                    }
                }

                track.Tags.Add(tvm);
            }
            else
            {
                ShowErrorMessage("No description for type: " + track.Type);
            }
        }

        /// <summary>
        /// Determines whether the clip has been loaded successfully and therefore whether the
        /// commands that are dependent on this can be executed by the user.
        /// </summary>
        /// <param name="parameter">
        /// The command parameter passed in from the view.
        /// </param>
        /// <returns>
        /// True if the commands that are based on a valid loaded clip can be executed by the
        /// user; otherwise, false.
        /// </returns>
        private bool IsLoaded(object parameter)
        {
            return this.loaded;
        }

        /// <summary>
        /// Gets called when the connect command gets executed. Tries to establish a connection
        /// to a running instance of Rag.
        /// </summary>
        /// <param name="parameter">
        /// The command parameter passed in from the view.
        /// </param>
        public void EstablishConnection(object parameter)
        {
            Cursor previousCursor = Application.Current.MainWindow.Cursor;
            try
            {
                Application.Current.MainWindow.Cursor = Cursors.Wait;
                ragAnimationWrapper.EnsureConnection();
            }
            catch (Exception ex)
            {
                RSG.Base.Logging.Log.StaticLog.Exception(ex, "Error during rag connection");
            }
            finally
            {
                Application.Current.MainWindow.Cursor = previousCursor;
            }

            if (this.IsConnected)
            {
                string filename = this.Model.ManagedClip.GetFilename();
                if (!string.IsNullOrWhiteSpace(filename))
                {
                    Thread.Sleep(1000);
                    ragAnimationWrapper.EnsureAnimationIsLoaded(filename);
                    ragAnimationWrapper.EnsureAnimationIsPaused();
                    ragAnimationWrapper.CurrentFramePosition = (float)this.currentFramePosition;
                }

                //  Create the RAG connection test timer
                if (this.connectionTimer != null)
                {
                    connectionTimer.Tick -= new EventHandler(OnConnectionTimerTick);
                    connectionTimer.Stop();
                    connectionTimer = null;
                }

                connectionTimer = new DispatcherTimer();
                connectionTimer.Interval = TimeSpan.FromMilliseconds(5000);
                connectionTimer.Tick += new EventHandler(OnConnectionTimerTick);
                connectionTimer.Start();

                //  Create the RAG connection test timer
                if (this.playTimer != null)
                {
                    playTimer.Tick -= new EventHandler(OnPlayTimerTick);
                    playTimer.Stop();
                    playTimer = null;
                }

                playTimer = new DispatcherTimer();
                playTimer.Interval = TimeSpan.FromMilliseconds(1);
                playTimer.Tick += new EventHandler(OnPlayTimerTick);
                playTimer.Start();
                return;
            }

            MessageBox.Show(
                "Unable to establish a connection to a running instance of Rag, due to an unknown error.",
                "Connection Error",
                MessageBoxButton.OK,
                MessageBoxImage.Error);
        }

        void OnPlayTimerTick(object sender, EventArgs e)
        {
            bool isCurrentlyPlaying = this.IsPlaying;

            if (isCurrentlyPlaying)
            {
                double valFromRAG = ragAnimationWrapper.GetFramePositionFromRAG();

                if (valFromRAG >= 0.0)
                {
                    this.positionHasComeFromRAG = true;
                    this.CurrentFramePosition = valFromRAG;
                    this.positionHasComeFromRAG = false;
                }
            }

            if (previouslyPlaying != isCurrentlyPlaying)
            {
                this.OnPropertyChanged("IsPlayingForwards");
                this.OnPropertyChanged("IsPlayingBackwards");
                this.OnPropertyChanged("IsPlaying");

                if (playTimer != null)
                {
                    if (!previouslyPlaying && isCurrentlyPlaying)
                    {
                        playTimer.Interval = TimeSpan.FromMilliseconds(0.01);
                    }
                    else if (previouslyPlaying && !isCurrentlyPlaying)
                    {
                        playTimer.Interval = TimeSpan.FromMilliseconds(1);
                    }
                }
            }

            previouslyPlaying = isCurrentlyPlaying;
        }

        void OnConnectionTimerTick(object sender, EventArgs e)
        {
            if (!IsConnected)
            {
                Disconnect(null);

                MessageBox.Show(
                    "Lost connection to a running instance of Rag, due to an unknown error.",
                    "Connection Error",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);

                this.OnPropertyChanged("IsConnected");
            }
        }

        private bool previouslyPlaying;

        /// <summary>
        /// Gets called when the disconnect command gets executed. Removes any current
        /// connection to a running instance of Rag.
        /// </summary>
        /// <param name="parameter">
        /// The command parameter passed in from the view.
        /// </param>
        public void Disconnect(object parameter)
        {
            this.Pause(null);

            //  Stop the RAG connection test timer
            if (connectionTimer != null)
            {
                connectionTimer.Tick -= new EventHandler(OnConnectionTimerTick);
                connectionTimer.Stop();
                connectionTimer = null;
            }

            if (playTimer != null)
            {
                playTimer.Tick -= new EventHandler(OnPlayTimerTick);
                playTimer.Stop();
                playTimer = null;
            }

            ragAnimationWrapper.Disconnect();
        }

        /// <summary>
        /// Gets called when the save command gets executed. Saves the clip into the
        /// associated file path.
        /// </summary>
        /// <param name="parameter">
        /// The command parameter passed in from the view.
        /// </param>
        private void SaveClip(object parameter)
        {
            this.SaveClip();
        }

        /// <summary>
        /// Saves the clip to its associated file.
        /// </summary>
        /// <returns>
        /// True if the clip was successfully saved; otherwise, false.
        /// </returns>
        public bool SaveClip()
        {
            if (this.Model == null || this.Model.ManagedClip == null)
            {
                return false;
            }

            string filename = this.Model.ManagedClip.GetFilename();
            if (string.IsNullOrWhiteSpace(filename))
            {
                this.ShowErrorMessage("Cannot save clips that don't reference a local file.");
                return false;
            }

            if (File.Exists(filename))
            {
                // Check to make sure the file isn't read only.
                FileInfo fileInfo = new FileInfo(filename);
                if (fileInfo.IsReadOnly)
                {
                    string msg = string.Format("Unable to save clip due to '{0}' being read-only.", filename);
                    this.ShowErrorMessage(msg);
                    return false;
                }
            }

            try
            {
                if ((bool)this.Model.ManagedClip.Save())
                {
                    List<string> errors = new List<string>();
                    foreach (ClipEventViewModel eventViewModel in this.TagTracks)
                    {
                        foreach (TagViewModel tag in eventViewModel.Tags)
                        {
                            string error = string.Format("A tag in the {0} track has a ", eventViewModel.Type);

                            if (tag.StartAsFrame < 0.0)
                            {
                                errors.Add(error + "start value less than zero.");
                            }

                            if (tag.EndAsFrame > this.MaximumFrameCount)
                            {
                                errors.Add(error + "end value greater than the frame count.");
                            }

                            if (tag.StartAsFrame > tag.EndAsFrame)
                            {
                                errors.Add(error + "end value less than the start value.");
                            }
                        }
                    }

                    if (errors.Count > 0)
                    {
                        string msg = "Successfully saved the .clip file, however, it has the following warnings in it.";
                        foreach (string error in errors)
                        {
                            msg += "\n" + error;
                        }

                        MessageBox.Show(msg, "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    }
                    else
                    {
                        MessageBox.Show("Successfully saved the .clip file.", "Successful", MessageBoxButton.OK, MessageBoxImage.None);
                    }

                    this.DirtyState = false;
                    return true;
                }
            }
            catch (Exception ex)
            {
                RSG.Base.Logging.Log.StaticLog.Exception(ex, "Unhandled exception occurred while saving the clip file.");
            }

            this.ShowErrorMessage(@"Failed to save clip due to unexpected error.");
            return false;
        }

        /// <summary>
        /// Gets called when the preview command gets executed. Previews the clip by rebuilding
        /// the zip file and placing it inside the preview folder.
        /// </summary>
        /// <param name="parameter">
        /// The command parameter passed in from the view.
        /// </param>
        private void PreviewClip(object parameter)
        {
            if (this.Model == null || this.Model.ManagedClip == null)
                return;

            try
            {
                string filename = this.Model.ManagedClip.GetFilename();
                if (string.IsNullOrWhiteSpace(filename))
                {
                    this.ShowErrorMessage("Cannot preview clips that don't reference a local file.");
                    return;
                }

                if (!this.SaveClip())
                    return;

                string rubyExecutablePathname = "";
                string commandLine = "";
                IConfig config = ConfigFactory.CreateConfig();
                rubyExecutablePathname = config.ToolsRuby;

                // Determine the target scene name
                string clipDirectoryName = System.IO.Path.GetDirectoryName(filename);
                string clipDirectoryNameNormalised = clipDirectoryName.ToUpper().Replace('\\', '/');
                string baseAnimArtDirectoryName = System.IO.Path.Combine(config.Project.DefaultBranch.Art, "anim/export_mb");
                string baseAnimArtDirectoryNameNormalised = baseAnimArtDirectoryName.ToUpper().Replace('\\', '/');
                if (!clipDirectoryNameNormalised.Contains(baseAnimArtDirectoryNameNormalised))
                {
                    string targetScenename = System.IO.Path.GetFileName(clipDirectoryName);
                    commandLine = string.Format("{0} --project={1} --rootsrcdir={2} --animsrcdir={2} --scenename={3} --preview",
                        System.IO.Path.Combine(config.ToolsLib, "pipeline/resourcing/gateways/pack_file.rb"),
                        config.Project.Name,
                        clipDirectoryName,
                        targetScenename);
                }
                else
                {
                    string relativeClipDirectoryName = clipDirectoryNameNormalised.Replace(baseAnimArtDirectoryNameNormalised, "");
                    string[] parentDirectoryNames = relativeClipDirectoryName.Split('/');
                    var targetScenenameBuilder = new StringBuilder();
                    foreach (string parentDirectoryName in parentDirectoryNames)
                    {
                        targetScenenameBuilder.Append(parentDirectoryName);
                    }
                    string targetScenename = targetScenenameBuilder.ToString();

                    commandLine = string.Format("{0} --project={1} --rootsrcdir={2} --animsrcdir={2} --scenename={3} --preview",
                        System.IO.Path.Combine(config.ToolsLib, "pipeline/resourcing/gateways/pack_file.rb"),
                        config.Project.Name,
                        clipDirectoryName,
                        targetScenename);
                }

                Process process = Process.Start(rubyExecutablePathname, commandLine);
                process.WaitForExit();

                if (process.ExitCode != 0)
                {
                    this.ShowErrorMessage("An error occurred in pack_file.rb.");
                }
            }
            catch (Exception)
            {
                this.ShowErrorMessage("An unexpected error occurred when trying to preview.");
            }
        }

        /// <summary>
        /// Determines whether the refresh command can be executed by the user.
        /// </summary>
        /// <param name="parameter">
        /// The command parameter passed in from the view.
        /// </param>
        /// <returns>
        /// True if the refresh command can be executed by the user; otherwise, false.
        /// </returns>
        private bool CanRefreshClip(object parameter)
        {
            return this.IsConnected;
        }

        /// <summary>
        /// Gets called when the refresh command gets executed. Refreshes the clip in memory
        /// by triggering a reload.
        /// </summary>
        /// <param name="parameter">
        /// The command parameter passed in from the view.
        /// </param>
        private void RefreshClip(object parameter)
        {
            ragAnimationWrapper.Refresh();
        }

        /// <summary>
        /// Called whenever the connection status of the rag animation wrapper changes.
        /// </summary>
        /// <param name="sender">
        /// The source of the event.
        /// </param>
        /// <param name="e">
        /// The System.EventArgs attached to this event.
        /// </param>
        private void OnConnectionStatusChanged(object sender, EventArgs e)
        {
            if (this.IsConnected)
            {

            }
            else
            {
                this.Pause(null);
            }

            this.OnPropertyChanged("IsConnected");
        }

        /// <summary>
        /// Shows a error message box with some standard parameters and the specified
        /// error message.
        /// </summary>
        /// <param name="message">
        /// The message to show the user on the error message box.
        /// </param>
        private void ShowErrorMessage(string message)
        {
            MessageBox.Show(message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
        }
        
        /// <summary>
        /// Gets called when the pause command gets executed.
        /// </summary>
        /// <param name="parameter">
        /// The command parameter passed in from the view.
        /// </param>
        private void Pause(object parameter)
        {
            ragAnimationWrapper.EnsureAnimationIsPaused();
            if (this.SnapToFrame)
            {
                this.CurrentFramePosition = Math.Round(this.CurrentFramePosition);
            }

            this.OnPropertyChanged("IsPlayingBackwards");
            this.OnPropertyChanged("IsPlayingForwards");
        }

        /// <summary>
        /// Determines whether the play command can be executed by the user.
        /// </summary>
        /// <param name="parameter">
        /// The command parameter passed in from the view.
        /// </param>
        /// <returns>
        /// True if the play command can be executed by the user; otherwise, false.
        /// </returns>
        private bool CanPlay(object parameter)
        {
            return this.loaded && this.IsConnected;
        }

        /// <summary>
        /// Gets called when the play command gets executed.
        /// </summary>
        /// <param name="parameter">
        /// The command parameter passed in from the view.
        /// </param>
        private void Play(object parameter)
        {
            ragAnimationWrapper.PlayAnimation();
        }

        /// <summary>
        /// Determines whether the rewind command can be executed by the user.
        /// </summary>
        /// <param name="parameter">
        /// The command parameter passed in from the view.
        /// </param>
        /// <returns>
        /// True if the rewind command can be executed by the user; otherwise, false.
        /// </returns>
        private bool CanRewind(object parameter)
        {
            return this.loaded && this.IsConnected;
        }

        /// <summary>
        /// Gets called when the rewind command gets executed.
        /// </summary>
        /// <param name="parameter">
        /// The command parameter passed in from the view.
        /// </param>
        private void Rewind(object parameter)
        {
            ragAnimationWrapper.RewindAnimation();
        }

        /// <summary>
        /// Determines whether the step forwards or step backwards commands can be executed
        /// by the user.
        /// </summary>
        /// <param name="parameter">
        /// The command parameter passed in from the view.
        /// </param>
        /// <returns>
        /// True if the step forwards and step backwards commands can be executed by the
        /// user; otherwise, false.
        /// </returns>
        private bool CanStep(object parameter)
        {
            return this.loaded && this.PlayRate == 0.0f;
        }

        /// <summary>
        /// Gets called when the step forward command gets executed.
        /// </summary>
        /// <param name="parameter">
        /// The command parameter passed in from the view.
        /// </param>
        private void StepForward(object parameter)
        {
            //if (this.IsConnected)
            //{
            //    ragAnimationWrapper.PressStepForwardButton();
            //    return;
            //}

            double position = Math.Floor(this.CurrentFramePosition + 1);
            if (position > this.MaximumFrameCount)
            {
                position = 0.0;
            }

            this.CurrentFramePosition = position;
        }

        /// <summary>
        /// Gets called when the step backwards command gets executed.
        /// </summary>
        /// <param name="parameter">
        /// The command parameter passed in from the view.
        /// </param>
        private void StepBackward(object parameter)
        {
            //if (this.IsConnected)
            //{
            //    ragAnimationWrapper.PressStepBackwardButton();
            //    return;
            //}

            double position = Math.Ceiling(this.CurrentFramePosition - 1);
            if (position < 0.0)
            {
                position = this.MaximumFrameCount;
            }

            this.CurrentFramePosition = position;
        }

        /// <summary>
        /// Determines whether the add tag command can be executed by the user.
        /// </summary>
        /// <param name="parameter">
        /// The command parameter passed in from the view.
        /// </param>
        /// <returns>
        /// True if the refresh command can be executed by the user; otherwise, false.
        /// </returns>
        private bool CanAddTag(object parameter)
        {
            if (this.SelectedTagTracks == null || this.SelectedTagTracks.Count <= 0)
                return false;

            return true;
        }

        /// <summary>
        /// Gets called when the add tag command gets executed. Adds a new tag at the current
        /// frame position for each selected track.
        /// </summary>
        /// <param name="parameter">
        /// The command parameter passed in from the view.
        /// </param>
        private void AddTag(object parameter)
        {
            this.AddTag(this.SelectedTagTracks);
        }

        /// <summary>
        /// Gets called when the move to next tag command gets executed.
        /// </summary>
        /// <param name="parameter">
        /// The command parameter passed in from the view.
        /// </param>
        private void MoveToNextTag(object parameter)
        {
            if (this.SelectedTagTracks == null || this.SelectedTagTracks.Count == 0)
                return;

            List<TagViewModel> tags = new List<TagViewModel>();
            foreach (ClipEventViewModel track in this.SelectedTagTracks)
                foreach (TagViewModel tag in track.Tags)
                    tags.Add(tag);

            SortedList<float, List<TagViewModel>> sortedTags = new SortedList<float,List<TagViewModel>>();
            bool selected = false;
            foreach (TagViewModel tag in tags)
            {
                float tagStart = tag.StartAsFrame;
                if (!sortedTags.ContainsKey(tagStart))
                    sortedTags.Add(tagStart, new List<TagViewModel>());

                sortedTags[tagStart].Add(tag);
                if (tag.IsSelected)
                    selected = true;
            }

            if (!selected)
            {
                this.DeselectAllTags();
                foreach (var tagList in sortedTags.Values)
                {
                    foreach (TagViewModel tag in tagList)
                    {
                        tag.IsSelected = true;
                        return;
                    }
                }
                return;
            }

            TagViewModel lastSelection = null;
            TagViewModel nextSelection = null;
            foreach (var tagList in sortedTags.Values)
            {
                foreach (TagViewModel tag in tagList)
                {
                    if (tag.IsSelected)
                    {
                        lastSelection = tag;
                        nextSelection = null;
                    }
                    else if (lastSelection != null && nextSelection == null)
                    {
                        nextSelection = tag;
                    }
                }
            }

            if (nextSelection == null)
            {
                this.DeselectAllTags();
                foreach (var tagList in sortedTags.Values)
                {
                    foreach (TagViewModel tag in tagList)
                    {
                        tag.IsSelected = true;
                        return;
                    }
                }
                return;
            }
            else
            {
                this.DeselectAllTags();
                nextSelection.IsSelected = true;
            }
        }

        /// <summary>
        /// Gets called when the move to previous tag command gets executed.
        /// </summary>
        /// <param name="parameter">
        /// The command parameter passed in from the view.
        /// </param>
        private void MoveToPreviousTag(object parameter)
        {
            if (this.SelectedTagTracks == null || this.SelectedTagTracks.Count == 0)
                return;

            List<TagViewModel> tags = new List<TagViewModel>();
            foreach (ClipEventViewModel track in this.SelectedTagTracks)
                foreach (TagViewModel tag in track.Tags)
                    tags.Add(tag);

            SortedList<float, List<TagViewModel>> sortedTags = new SortedList<float, List<TagViewModel>>();
            bool selected = false;
            foreach (TagViewModel tag in tags)
            {
                float tagStart = tag.StartAsFrame;
                if (!sortedTags.ContainsKey(tagStart))
                    sortedTags.Add(tagStart, new List<TagViewModel>());

                sortedTags[tagStart].Add(tag);
                if (tag.IsSelected)
                    selected = true;
            }

            if (!selected)
            {
                this.DeselectAllTags();
                foreach (var tagList in sortedTags.Values.Reverse())
                {
                    foreach (TagViewModel tag in (tagList as IList<TagViewModel>).Reverse())
                    {
                        tag.IsSelected = true;
                        return;
                    }
                }
                return;
            }

            TagViewModel lastSelection = null;
            TagViewModel nextSelection = null;
            foreach (var tagList in sortedTags.Values.Reverse())
            {
                foreach (TagViewModel tag in (tagList as IList<TagViewModel>).Reverse())
                {
                    if (tag.IsSelected)
                    {
                        lastSelection = tag;
                        nextSelection = null;
                    }
                    else if (lastSelection != null && nextSelection == null)
                    {
                        nextSelection = tag;
                    }
                }
            }

            if (nextSelection == null)
            {
                this.DeselectAllTags();
                foreach (var tagList in sortedTags.Values.Reverse())
                {
                    foreach (TagViewModel tag in (tagList as IList<TagViewModel>).Reverse())
                    {
                        tag.IsSelected = true;
                        return;
                    }
                }
                return;
            }
            else
            {
                this.DeselectAllTags();
                nextSelection.IsSelected = true;
            }
        }
        #endregion

        #region Classes
        /// <summary>
        /// Defines a data defined tag that can be used to add tags on to the selected track.
        /// </summary>
        public class PreDefinedTagDefinition
        {
            #region Fields
            /// <summary>
            /// The private field for the <see cref="Name"/> property.
            /// </summary>
            private string name;

            /// <summary>
            /// The private field for the <see cref="ButtonVisibility"/> property.
            /// </summary>
            private string buttonVisibility;

            /// <summary>
            /// The private field for the <see cref="Command"/> property.
            /// </summary>
            private SimpleCommand command;

            /// <summary>
            /// The clip view model that owns this pre defined tag definition.
            /// </summary>
            private ClipViewModel clipViewModel;

            /// <summary>
            /// The group that this definition is wrapping.
            /// </summary>
            private ClipPropertyDefinition.PropertyGroup group;

            private int index;
            #endregion

            #region Constructors
            /// <summary>
            /// Initialises a new instance of the <see cref="PreDefinedTagDefinition"/> class.
            /// </summary>
            /// <param name="group">
            /// The group used to initialise this instance.
            /// </param>
            /// <param name="clipViewModel">
            /// Clip view model instance to add the tags to.
            /// </param>
            public PreDefinedTagDefinition(ClipPropertyDefinition.PropertyGroup group, ClipViewModel clipViewModel, int index)
            {
                this.clipViewModel = clipViewModel;
                this.group = group;
                this.name = group.Name;
                this.buttonVisibility = group.ButtonVisibility;
                this.index = index;
            }

            /// <summary>
            /// Initialises a new instance of the <see cref="PreDefinedTagDefinition"/> class.
            /// </summary>
            /// <param name="clipViewModel">
            /// Clip view model instance to add the tags to.
            /// </param>
            public PreDefinedTagDefinition(string name, ClipViewModel clipViewModel)
            {
                this.clipViewModel = clipViewModel;
                this.name = name;
                this.buttonVisibility = "Visible";
            }
            #endregion

            #region Properties
            /// <summary>
            /// Gets or sets the user defined name for this particular tag definition.
            /// </summary>
            public string Name
            {
                get { return this.name; }
                set { this.name = value; }
            }

            /// <summary>
            /// Gets or sets the user defined button visibility for this particular tag definition.
            /// </summary>
            public string ButtonVisibility
            {
                get { return this.buttonVisibility; }
                set { this.buttonVisibility = value; }
            }

            /// <summary>
            /// Gets the command that will be executed when the user selects to add a tag
            /// with this definition.
            /// </summary>
            public SimpleCommand Command
            {
                get
                {
                    if (this.command == null)
                    {
                        this.command = new SimpleCommand(this.OnExecute, this.CanExecute);
                    }

                    return command;
                }
            }

            /// <summary>
            /// Gets the group that is being wrapped by this definition.
            /// </summary>
            public ClipPropertyDefinition.PropertyGroup Group
            {
                get { return this.group; }
            }

            /// <summary>
            /// Gets a user friendly description of this instance to show as a tooltip.
            /// </summary>
            public string Description
            {
                get
                {
                    if (this.group != null)
                    {
                        string desc = string.Format("{0} ({1}, NumPad {1})", this.Name, index);
                        foreach (var value in Group.PropertyValues)
                        {
                            desc += string.Format("\n{0}: {1}", value.Key, value.Value);
                        }

                        return desc;
                    }
                    else
                    {
                        string desc = string.Format("{0} (Enter)", this.Name);

                        return desc;
                    }
                }
            }
            #endregion

            #region Methods
            /// <summary>
            /// Determines whether the command can be executed by the user.
            /// </summary>
            /// <param name="parameter">
            /// The command parameter passed in from the view.
            /// </param>
            /// <returns>
            /// True if the command can be executed by the user; otherwise, false.
            /// </returns>
            private bool CanExecute(object parameter)
            {
                if (this.clipViewModel == null || this.clipViewModel.SelectedTagTracks == null)
                    return false;

                if (this.clipViewModel.SelectedTagTracks.Count == 1)
                    return true;

                return false;
            }

            /// <summary>
            /// Gets called when the user executes this command.
            /// </summary>
            /// <param name="parameter">
            /// The command parameter passed in from the view.
            /// </param>
            private void OnExecute(object parameter)
            {
                ClipEventViewModel track = this.clipViewModel.SelectedTagTracks[0];

                if (this.group == null)
                    this.clipViewModel.AddTag(Enumerable.Repeat<ClipEventViewModel>(track, 1));
                else
                    this.clipViewModel.AddTag(this.group, track);
            }
            #endregion
        }
        #endregion
    }
}
