﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls.Primitives;
using System.Windows;
using System.Windows.Media;
using System.Windows.Controls;
using RSG.AnimClipEditor.ViewModel;
using RSG.AnimClipEditor.ViewModel.PropertyAttribute;

namespace RSG.AnimClipEditor.Controls
{
    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class BlendOutMoveThumb : Thumb
    {
        public BlendOutMoveThumb()
        {
            DragDelta += new DragDeltaEventHandler(BlendOutMoveThumb_DragDelta);
        }

        void BlendOutMoveThumb_DragDelta(object sender, DragDeltaEventArgs e)
        {
            if (e.HorizontalChange == 0.0)
            {
                return;
            }

            DependencyObject parent = VisualTreeHelper.GetParent(sender as DependencyObject);
            Canvas canvas = parent as Canvas;
            while (parent != null && canvas == null)
            {
                parent = VisualTreeHelper.GetParent(parent);
                canvas = parent as Canvas;
            }

            TagViewModel tag = this.DataContext as TagViewModel;
            if (canvas == null || tag == null)
            {
                return;
            }

            double phaseDifference = -(e.HorizontalChange / Math.Round(canvas.ActualWidth));
            double newPhase = tag.BlendOut + phaseDifference;
            if (newPhase > 1.0)
            {
                newPhase = 1.0;
            }
            if (newPhase < 0.0)
            {
                newPhase = 0.0;
            }

            foreach (PropertyAttributeBaseViewModel item in tag.Property.PropertyAttributes)
            {
                FloatViewModel floatAttribute = item as FloatViewModel;
                if (floatAttribute == null || !floatAttribute.IsBlendOut)
                {
                    continue;
                }

                floatAttribute.Value = (float)newPhase;
            }
        }
    }
}
