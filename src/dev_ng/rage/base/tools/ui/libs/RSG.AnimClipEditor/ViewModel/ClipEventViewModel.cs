﻿using System;
using System.Windows;
using System.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Media;
using System.Windows.Input;

using RSG.AnimClipEditor.Util;
using RSG.ManagedRage.ClipAnimation;
using RSG.AnimationMetadata;
using RSG.PlaybackControl;
using System.Collections;
using System.ComponentModel;

namespace RSG.AnimClipEditor.ViewModel
{
    public class ClipEventViewModel : BaseViewModel
    {
        const int MAX_COLORS = 10;
        static Brush[] sColors = new Brush[MAX_COLORS] { Brushes.Red, Brushes.Green, Brushes.Blue, Brushes.Pink, Brushes.AntiqueWhite,
                                                                    Brushes.Black, Brushes.Brown, Brushes.Cornsilk, Brushes.Orange, Brushes.Olive};
        public static int sEventCount = 0;

        #region Constructor
        public ClipEventViewModel(String type, ObservableCollection<TagViewModel> tags, ClipViewModel cvm)
        {
            this.ClipViewModel = cvm;
            _Type = type;
            _Tags = tags;
            _Tags.CollectionChanged += _Tags_CollectionChanged;
            _IsExpanded = true;
            _Height = 40;

            if (ClipTagDefinitions.PropertyDescriptions.ContainsKey(type))
            {
                ClipPropertyDefinition propertyDef = ClipTagDefinitions.PropertyDescriptions[type];
                _Colour = new SolidColorBrush(propertyDef.BackgroundColour.Value);
            }
            else
            {
                _Colour = Brushes.Black;
            }
        }

        void _Tags_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            this.ClipViewModel.DirtyState = true;
        }

        public ClipEventViewModel(String type, ClipViewModel cvm)
            : this(type, new ObservableCollection<TagViewModel>(), cvm)
        {
        }
        #endregion // Constructor

        #region Properties
        public ObservableCollection<TagViewModel> Tags
        {
            get
            {
                return _Tags;
            }
        }
        ObservableCollection<TagViewModel> _Tags;

        public String Type
        {
            get
            {
                return _Type;
            }
        }
        String _Type;

        public Brush Colour
        {
            get
            {
                return _Colour;
            }
        }
        Brush _Colour;

        public bool Expanded
        {
            get
            {
                return _Expanded;
            }
            set
            {
                if (_Expanded != value)
                    _Expanded = value;
            }
        }
        bool _Expanded;

        public bool IsSelected
        {
            get
            {
                return _IsSelected;
            }
            set
            {
                if (_IsSelected != value)
                {
                    _IsSelected = value;
                    OnPropertyChanged("IsSelected");
                }
            }
        }
        bool _IsSelected;

        public int Height
        {
            get
            {
                return _Height;
            }
            set
            {
                if (Height != value)
                {
                    _Height = value;
                    OnPropertyChanged("Height");
                }
            }
        }
        int _Height;

        public double Width
        {
            get { return this.ClipViewModel.TagTrackWidth; }
        }

        public bool FirstVisibleFrameIsZero
        {
            get
            {
                return this.ClipViewModel.FirstVisibleFrame == 0.0;
            }
        }

        public bool LastVisibleFrameIsMaximum
        {
            get
            {
                double visibleFrames =
                    this.ClipViewModel.TagTrackVisiblePixelWidth /
                    this.ClipViewModel.PixelsPerFrame;

                double lastFrame = this.ClipViewModel.FirstVisibleFrame + visibleFrames;
                return lastFrame >= this.ClipViewModel.MaximumFrameCount;
            }
        }
     
        public bool IsExpanded
        {
            get
            {
                return _IsExpanded;
            }
            set
            {
                _IsExpanded = value;
                if (_IsExpanded)
                    Height = 40;
                else
                    Height = 20;
            }
        }
        bool _IsExpanded;

        public double TabTagWidth
        {
            get;
            set;
        }

        public ClipViewModel ClipViewModel
        {
            get
            {
                return this.clipViewModel;
            }

            set
            {
                if (this.clipViewModel != null)
                {
                    this.clipViewModel.PropertyChanged -= this.OnClipViewModelPropertyChanged;
                }

                this.clipViewModel = value;
                if (value != null)
                {
                    this.clipViewModel.PropertyChanged += this.OnClipViewModelPropertyChanged;
                }
            }
        }

        private void OnClipViewModelPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "TagTrackWidth")
            {
                this.OnPropertyChanged("Width");
                this.OnPropertyChanged("LastVisibleFrameIsMaximum");
            }
            else if (e.PropertyName == "FirstVisibleFrame")
            {
                this.OnPropertyChanged("FirstVisibleFrameIsZero");
                this.OnPropertyChanged("LastVisibleFrameIsMaximum");
            }
        }
        private ClipViewModel clipViewModel;

        #endregion // Properties

        #region Commands
        public SimpleCommand DeleteCommand
        {
            get
            {
                if (this.deleteCommand == null)
                {
                    this.deleteCommand = new SimpleCommand(this.OnDeleteCommand, this.CanDeleteCommand);
                }
                return deleteCommand;
            }
        }
        private SimpleCommand deleteCommand;

        /// <summary>
        /// 
        /// </summary>
        public SimpleCommand CopyAllTags
        {
            get
            {
                if (this.copyAllTags == null)
                {
                    this.copyAllTags = new SimpleCommand(CopyAllTagsExecute, CopyAllTagsCanExecute);
                }
                return this.copyAllTags;
            }
        }
        private SimpleCommand copyAllTags;

        /// <summary>
        /// 
        /// </summary>
        public SimpleCommand CopySelectedTags
        {
            get
            {
                if (this.copySelectedTags == null)
                {
                    this.copySelectedTags = new SimpleCommand(CopySelectedTagsExecute, CopySelectedTagsCanExecute);
                }
                return this.copySelectedTags;
            }
        }
        private SimpleCommand copySelectedTags;

        /// <summary>
        /// 
        /// </summary>
        public SimpleCommand CopyWholeEventWithTags
        {
            get
            {
                if (this.copyWholeEventWithTags == null)
                {
                    this.copyWholeEventWithTags = new SimpleCommand(CopyWholeEventWithTagsExecute, CopyWholeEventWithTagsCanExecute);
                }
                return this.copyWholeEventWithTags;
            }
        }
        private SimpleCommand copyWholeEventWithTags;

        /// <summary>
        /// 
        /// </summary>
        public SimpleCommand PasteTagsWithMerge
        {
            get
            {
                if (this.pasteTagsWithMerge == null)
                {
                    this.pasteTagsWithMerge = new SimpleCommand(PasteTagsWithMergeExecute, PasteTagsWithMergeCanExecute);
                }
                return this.pasteTagsWithMerge;
            }
        }
        private SimpleCommand pasteTagsWithMerge;

        /// <summary>
        /// 
        /// </summary>
        public SimpleCommand PasteTagsWithOverwrite
        {
            get
            {
                if (this.pasteTagsWithOverwrite == null)
                {
                    this.pasteTagsWithOverwrite = new SimpleCommand(PasteTagsWithOverwriteExecute, PasteTagsWithOverwriteCanExecute);
                }
                return this.pasteTagsWithOverwrite;
            }
        }
        private SimpleCommand pasteTagsWithOverwrite;

        /// <summary>
        /// 
        /// </summary>
        public SimpleCommand PasteWholeEventWithTags
        {
            get
            {
                if (this.pasteWholeEventWithTags == null)
                {
                    this.pasteWholeEventWithTags = new SimpleCommand(PasteWholeEventWithTagsExecute, PasteWholeEventWithTagsCanExecute);
                }
                return this.pasteWholeEventWithTags;
            }
        }
        private SimpleCommand pasteWholeEventWithTags;
        #endregion

        internal static Dictionary<string, object> Copied = new Dictionary<string,object>();

        #region CopyAllTags
        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameter"></param>
        private void CopyAllTagsExecute(object parameter)
        {
            Copied.Clear();
            Copied.Add(string.Format("{0} Tags", this.Type), this.Tags);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameter"></param>
        private bool CopyAllTagsCanExecute(object parameter)
        {
            return this.Tags != null && this.Tags.Count > 0;
        }
        #endregion

        #region CopySelectedTags
        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameter"></param>
        private void CopySelectedTagsExecute(object parameter)
        {
            Copied.Clear();
            Copied.Add(string.Format("{0} Tags", this.Type), this.Tags.Where(t => t.IsSelected));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameter"></param>
        private bool CopySelectedTagsCanExecute(object parameter)
        {
            return this.Tags != null && this.Tags.Where(t => t.IsSelected).Count() > 0;
        }
        #endregion

        #region CopyWholeEventWithTags
        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameter"></param>
        private void CopyWholeEventWithTagsExecute(object parameter)
        {
            Copied.Clear();
            Copied.Add(string.Format("{0}", this.Type), this);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameter"></param>
        private bool CopyWholeEventWithTagsCanExecute(object parameter)
        {
            return true;
        }
        #endregion

        #region PasteTagsWithMerge
        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameter"></param>
        private void PasteTagsWithMergeExecute(object parameter)
        {
            object data = Copied[string.Format("{0} Tags", this.Type)];
            if (!(data is IEnumerable<TagViewModel>))
                return;
            
            foreach (TagViewModel tag in (data as IEnumerable<TagViewModel>).ToList())
            {
                if (tag.StartAsFrame < this.ClipViewModel.TimeData.Start)
                    continue;
                if (tag.EndAsFrame > this.ClipViewModel.TimeData.End)
                    continue;

                TagViewModel newTag = EventFactory.CreateTagViewModel(this.Type, tag.Start, tag.End, this.ClipViewModel);
                foreach (PropertyAttributeBaseViewModel property in tag.Property.PropertyAttributes)
                {
                    foreach (PropertyAttributeBaseViewModel newProperty in newTag.Property.PropertyAttributes)
                    {
                        if (newProperty.Name != property.Name)
                        {
                            continue;
                        }

                        newProperty.SetFromOther(property);
                        break;
                    }
                }

                this.Tags.Add(newTag);
            }
            Copied.Remove(string.Format("{0} Tags", this.Type));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameter"></param>
        private bool PasteTagsWithMergeCanExecute(object parameter)
        {
            return Copied.ContainsKey(string.Format("{0} Tags", this.Type));
        }
        #endregion

        #region PasteTagsWithOverwrite
        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameter"></param>
        private void PasteTagsWithOverwriteExecute(object parameter)
        {
            object data = Copied[string.Format("{0} Tags", this.Type)];
            if (!(data is IEnumerable<TagViewModel>))
                return;

            var copiedList = (data as IEnumerable<TagViewModel>).ToList();
            this.Tags.Clear();
            foreach (TagViewModel tag in copiedList)
            {
                if (tag.StartAsFrame < this.ClipViewModel.TimeData.Start)
                    continue;
                if (tag.EndAsFrame > this.ClipViewModel.TimeData.End)
                    continue;

                TagViewModel newTag = EventFactory.CreateTagViewModel(this.Type, tag.Start, tag.End, this.ClipViewModel);
                foreach (PropertyAttributeBaseViewModel property in tag.Property.PropertyAttributes)
                {
                    foreach (PropertyAttributeBaseViewModel newProperty in newTag.Property.PropertyAttributes)
                    {
                        if (newProperty.Name != property.Name)
                        {
                            continue;
                        }

                        newProperty.SetFromOther(property);
                        break;
                    }
                }

                this.Tags.Add(newTag);
            }
            Copied.Remove(string.Format("{0} Tags", this.Type));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameter"></param>
        private bool PasteTagsWithOverwriteCanExecute(object parameter)
        {
            return Copied.ContainsKey(string.Format("{0} Tags", this.Type));
        }
        #endregion

        #region PasteWholeEventWithTags
        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameter"></param>
        private void PasteWholeEventWithTagsExecute(object parameter)
        {
            object data = Copied.FirstOrDefault();
            if (!(data is KeyValuePair<string, object>))
                return;

            string type = ((KeyValuePair<string, object>)data).Key;
            data = ((KeyValuePair<string, object>)data).Value;
            if (!(data is ClipEventViewModel))
                return;

            ClipEventViewModel clip = data as ClipEventViewModel;

            ClipEventViewModel newClip = new ClipEventViewModel(type, new ObservableCollection<TagViewModel>(), this.ClipViewModel);
            newClip.Tags.CollectionChanged += this.ClipViewModel.Tags_CollectionChanged;
            this.ClipViewModel.TagTracks.Add(newClip);

            ObservableCollection<TagViewModel> tags = new ObservableCollection<TagViewModel>();
            foreach (TagViewModel tag in (data as ClipEventViewModel).Tags)
            {
                if (tag.StartAsFrame < this.ClipViewModel.TimeData.Start)
                    continue;
                if (tag.EndAsFrame > this.ClipViewModel.TimeData.End)
                    continue;

                TagViewModel newTag = EventFactory.CreateTagViewModel(type, tag.Start, tag.End, this.ClipViewModel);
                for (int i = 0; i < tag.Property.PropertyAttributes.Count; i++)
                {
                    newTag.Property.PropertyAttributes[i].SetFromOther(tag.Property.PropertyAttributes[i]);
                }

                newClip.Tags.Add(newTag);
            }
            Copied.Clear();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameter"></param>
        private bool PasteWholeEventWithTagsCanExecute(object parameter)
        {
            object data = Copied.FirstOrDefault();
            if (!(data is KeyValuePair<string, object>))
                return false;

            data = ((KeyValuePair<string, object>)data).Value;
            if (!(data is ClipEventViewModel))
                return false;

            foreach (var clipEvent in this.ClipViewModel.TagTracks)
            {
                if (clipEvent.Type == (data as ClipEventViewModel).Type)
                    return false;
            }
            return true;
        }
        #endregion

        #region Methods
        private bool CanDeleteCommand(object parameter)
        {
            return true;
        }

        private void OnDeleteCommand(object parameter)
        {
            this.ClipViewModel.DeleteTagTrack(this);
        }
        #endregion
    }
}
