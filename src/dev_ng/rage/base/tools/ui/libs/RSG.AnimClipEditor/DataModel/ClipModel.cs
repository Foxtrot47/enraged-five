﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using RSG.ManagedRage.ClipAnimation;
using RSG.AnimClipEditor.Util;


namespace RSG.AnimClipEditor.DataModel
{

    public class ClipModel
    {
        #region Constructors
        public ClipModel(MClip managedClip)
        {
            _ManagedClip = managedClip;
        }
        #endregion // Constructors

        #region Properties
        public MClip ManagedClip
        {
            get
            {
                return _ManagedClip;
            }
        }
        MClip _ManagedClip;

        /*public List<ClipEvent> Events
        {
            get
            {
                return _Events;
            }
        }
        List<ClipEvent> _Events;*/

        public int Duration
        {
            get
            {
                return _Duration;
            }
        }
        int _Duration = 0;
        #endregion // Properties

        #region Public Methods
        /// <summary>
        /// Organise the soup of tags into a dicitonary using the tag type name as the key
        /// </summary>
        /*public void OrganiseTags()
        {
            foreach (MTag mtag in ManagedClip.GetTags())
            {
                ClipEvent clipevent = Events.Find( delegate(ClipEvent ce) { return ce.Type == mtag.GetName(); });
                if (null != clipevent)
                {
                    clipevent.Tags.Add(mtag);
                }
                else
                {
                    Console.WriteLine(mtag.GetName());
                    List<MTag> tags = new List<MTag>();
                    tags.Add(mtag);
                    ClipEvent newclipevent = new ClipEvent(mtag.GetName(), tags);
                    Events.Add(newclipevent);
                }
            }
        }*/
        #endregion // Public Methods

    }
}
