﻿using System;

using RSG.ManagedRage.ClipAnimation;
using RSG.Base.Windows.Helpers;
namespace RSG.AnimClipEditor.ViewModel.PropertyAttribute
{
    public class HashStringViewModel : PropertyAttributeBaseViewModel
    {
        #region Constructors
        public HashStringViewModel(MPropertyAttributeHashString mpropattr, ClipViewModel clip)
            : base(mpropattr, clip)
        {
            enterCommand = new RelayCommand(null);
        }

        public HashStringViewModel(MPropertyAttributeHashString mpropattr, string initialValue, ClipViewModel clip)
            : base(mpropattr, clip)
        {
            SetFromString(initialValue);
            enterCommand = new RelayCommand(null);
        }
        #endregion

        #region Properties
        public string Value
        {
            get
            {
                return Convert.ToString((this.PropertyAttribute as MPropertyAttributeHashString).GetHashString());
            }
            set
            {
                if (value == null)
                    return;

                if (value != Convert.ToString((this.PropertyAttribute as MPropertyAttributeHashString).GetHashString()))
                {
                    (this.PropertyAttribute as MPropertyAttributeHashString).SetHashString(Convert.ToString(value));
                    FireValueChange();
                }
            }
        }

        public string FavoriteSelectedValue
        {
            get
            {
                return _FavoriteSelectedValue;
            }
            set
            {
                if (value != _FavoriteSelectedValue)
                {
                    _FavoriteSelectedValue = value;
                    this.Value = _FavoriteSelectedValue;
                }
            }
        }
        string _FavoriteSelectedValue;

        public RelayCommand EnterCommand
        {
            get
            {
                return this.enterCommand;
            }
        }
        RelayCommand enterCommand;
        #endregion // Properties

        #region methods
        public override bool TestString(string value)
        {
            return value == Value;
        }

        public override void SetFromString(string value)
        {
            if (value == null)
                this.Value = string.Empty;
            else
                this.Value = value;
        }

        public override string GetValueAsString()
        {
            if (string.IsNullOrWhiteSpace(this.Value))
                return "Empty";

            return this.Value;
        } 

        public override void SetFromOther(PropertyAttributeBaseViewModel other)
        {
            if (!(other is HashStringViewModel))
                return;

            this.Value = (other as HashStringViewModel).Value;
        }
        #endregion
    }
}
