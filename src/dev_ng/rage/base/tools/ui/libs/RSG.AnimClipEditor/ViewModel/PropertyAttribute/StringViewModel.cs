﻿using System;

using RSG.ManagedRage.ClipAnimation;
using RSG.Base.Windows.Helpers;

namespace RSG.AnimClipEditor.ViewModel.PropertyAttribute
{
    public class StringViewModel : PropertyAttributeBaseViewModel
    {
        #region Constructors
        public StringViewModel(MPropertyAttributeString mpropattr, ClipViewModel clip)
            : base(mpropattr, clip)
        {
            enterCommand = new RelayCommand(null);
        }

        public StringViewModel(MPropertyAttributeString mpropattr, string initialValue, ClipViewModel clip)
            : base(mpropattr, clip)
        {
            SetFromString(initialValue);
            enterCommand = new RelayCommand(null);
        }
        #endregion

        #region Properties
        public string Value
        {
            get
            {
                return (string)(this.PropertyAttribute as MPropertyAttributeString).GetString();
            }
            set
            {
                if (value == null)
                    return;

                if (value != (string)(this.PropertyAttribute as MPropertyAttributeString).GetString())
                {
                    (this.PropertyAttribute as MPropertyAttributeString).SetString(Convert.ToString(value));
                    FireValueChange();
                }
            }
        }

        public string FavoriteSelectedValue
        {
            get
            {
                return _FavoriteSelectedValue;
            }
            set
            {
                if (value != _FavoriteSelectedValue)
                {
                    _FavoriteSelectedValue = value;
                    this.Value = _FavoriteSelectedValue;
                }
            }
        }
        string _FavoriteSelectedValue;

        public RelayCommand EnterCommand
        {
            get
            {
                return this.enterCommand;
            }
        }
        RelayCommand enterCommand;
        #endregion // Properties

        #region methods
        public override bool TestString(string value)
        {
            return value == Value;
        }

        public override void SetFromString(string value)
        {
            if (value == null)
                this.Value = string.Empty;
            else
                this.Value = value;
        }

        public override string GetValueAsString()
        {
            if (string.IsNullOrWhiteSpace(this.Value))
                return "Empty";

            return this.Value;
        } 

        public override void SetFromOther(PropertyAttributeBaseViewModel other)
        {
            if (!(other is StringViewModel))
                return;

            this.Value = (other as StringViewModel).Value;
        }

        #endregion
    }
}
