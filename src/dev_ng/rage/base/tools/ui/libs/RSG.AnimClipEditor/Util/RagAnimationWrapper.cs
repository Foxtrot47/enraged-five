﻿using System;
using RSG.Base.Net;
using System.IO;
using System.Linq;
using System.Threading;
using RSG.Rag.Contracts.Data;
using System.Collections.Generic;
using RSG.Rag.Clients;
using System.ServiceModel;
using RSG.Base.Logging;

namespace RSG.AnimClipEditor.Util
{
    /// <summary>
    /// Provides methods that wrap the rag animation bank functionality to provide easy
    /// communication to a running instance of Rag.
    /// </summary>
    internal class RagAnimationWrapper
    {
        #region Fields
        /// <summary>
        /// The root widget path for all animation operations.
        /// </summary>
        private const string AnimationGroupName = @"Animation";

        /// <summary>
        /// The path to the Toggle Animation Bank widget.
        /// </summary>
        private const string AnimationBankWidgetName = AnimationGroupName + @"/Toggle Animation bank";

        /// <summary>
        /// The root widget path for all animation operations in the preview animation
        /// sub group.
        /// </summary>
        private const string PreviewAnimGroupName = AnimationGroupName + @"/Preview Anims";

        /// <summary>
        /// The root widget path for all animation operations in the clip selector sub group.
        /// </summary>
        private const string ClipSelectorGroupName = AnimationGroupName + @"/Clip Selector";

        /// <summary>
        /// The path to the Frame widget in the preview animation sub group.
        /// </summary>
        private const string FrameWidgetName = PreviewAnimGroupName + @"/Frame";

        /// <summary>
        /// The path to the Rate widget in the preview animation sub group.
        /// </summary>
        private const string RateWidgetName = PreviewAnimGroupName + @"/Rate";

        /// <summary>
        /// The path to the Load Clip widget in the preview animation sub group.
        /// </summary>
        private const string LoadAnimWidgetName = PreviewAnimGroupName + @"/Load preview network";
        
        /// <summary>
        /// The path to the Remove Clip widget in the preview animation sub group.
        /// </summary>
        private const string RemoveAnimWidgetName = PreviewAnimGroupName + @"/Remove Anim";

        /// <summary>
        /// The path to the Reload Clip widget in the preview animation sub group.
        /// </summary>
        private const string ReloadAnimWidgetName = PreviewAnimGroupName + @"/Reload Anim";

        /// <summary>
        /// The path to the Reactivate Ped widget in the preview animation sub group.
        /// </summary>
        private const string ReactivatePedWidgetName = PreviewAnimGroupName + @"/Reactivate Ped";
        
        /// <summary>
        /// The path to the Clip Context widget in the clip selector sub group.
        /// </summary>
        private const string ClipContextWidgetName = ClipSelectorGroupName + @"/Clip Context";

        /// <summary>
        /// The path to the Local File widget in the clip selector sub group.
        /// </summary>
        private const string LocalFileWidgetName = ClipSelectorGroupName + @"/Local File:";

        /// <summary>
        /// 
        /// </summary>
        private GameConnection _gameConnection;

        /// <summary>
        /// The private field for the <see cref="Filename"/> property.
        /// </summary>
        private string filename;
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of a
        /// <see cref="RSG.AnimClipEditor.Util.RagAnimationWrapper"/> class.
        /// </summary>
        /// <param name="attemptConnection">
        /// A value indicating whether a connection to the Rag console should be attempted
        /// on initialisation.
        /// </param>
        /// <param name="filename">
        /// The filename of the clip that is being edited.
        /// </param>
        public RagAnimationWrapper(bool attemptConnection, string filename)
        {
            this.filename = filename;
        }
        #endregion

        #region Events
        /// <summary>
        /// Occurs when a changed is made to the status of the connection to the connected
        /// instance of Rag.
        /// </summary>
        internal event EventHandler ConnectionStatusChanged;
        #endregion

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        internal GameConnection GameConnection
        {
            get { return _gameConnection; }
            private set
            {
                if (_gameConnection != value)
                {
                    _gameConnection = value;
                    OnStatusChanged();
                }
            }
        }

        /// <summary>
        /// Gets a value that indicating whether this wrapper is currently connected to a
        /// running instance of Rag.
        /// </summary>
        internal bool IsConnected
        {
            get { return _gameConnection != null; }
        }

        /// <summary>
        /// Gets or sets the current frame position that the loaded clip currently has in the
        /// rag ui.
        /// </summary>
        internal float CurrentFramePosition
        {
            get
            {
                if (!this.IsConnected)
                    return 0.0f;

                WidgetClient client = RagClientFactory.CreateWidgetClient(_gameConnection);
                try
                {
                    return client.ReadFloatWidget(FrameWidgetName);
                }
                catch (TimeoutException e)
                {
                    LogFactory.ApplicationLog.ToolException(e, "Operation timed out.");
                    GameConnection = null;
                    return 0.0f;
                }
                catch (CommunicationException e)
                {
                    LogFactory.ApplicationLog.ToolException(e, "Communication problem.");
                    GameConnection = null;
                    return 0.0f;
                }
                finally
                {
                    if (client.State == CommunicationState.Faulted)
                    {
                        client.Abort();
                    }
                    else
                    {
                        client.Close();
                    }
                }
            }
            set
            {
                if (!this.IsConnected)
                    return;

                WidgetClient client = RagClientFactory.CreateWidgetClient(_gameConnection);
                try
                {
                    client.WriteFloatWidget(FrameWidgetName, value);
                }
                catch (TimeoutException e)
                {
                    LogFactory.ApplicationLog.ToolException(e, "Operation timed out.");
                    GameConnection = null;
                }
                catch (CommunicationException e)
                {
                    LogFactory.ApplicationLog.ToolException(e, "Communication problem.");
                    GameConnection = null;
                }
                finally
                {
                    if (client.State == CommunicationState.Faulted)
                    {
                        client.Abort();
                    }
                    else
                    {
                        client.Close();
                    }
                }
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Ensures that this wrapped is currently connected to a running instance of Rag if
        /// that is possible.
        /// </summary>
        /// <returns>
        /// True if successfully connected to Rag; otherwise, false.
        /// </returns>
        internal bool EnsureConnection()
        {
            if (this.IsConnected)
            {
                return true;
            }

            try
            {
                IEnumerable<GameConnection> connections = RagClientFactory.GetGameConnections();
                GameConnection = connections.FirstOrDefault(connection => connection.DefaultConnection);
            }
            catch (Exception)
            {
                GameConnection = null;
            }

            return this.IsConnected;
        }

        /// <summary>
        /// Disconnects to the instance of Rag that this instance is currently connected to.
        /// </summary>
        internal void Disconnect()
        {
            if (!this.IsConnected)
            {
                return;
            }

            this.ReactivatePed();
            GameConnection = null;
        }

        /// <summary>
        /// Attempts to open the animation bank in Rag using the current connection if one
        /// has been established.
        /// </summary>
        /// <returns>
        /// True if the animation bank was opened successfully; otherwise, false.
        /// </returns>
        private bool EnsureAnimationBankIsOpen(WidgetClient client)
        {
            if (client.WidgetExists(ClipSelectorGroupName))
            {
                return true;
            }

            client.AddBankReference(AnimationGroupName);
            client.PressButton(AnimationBankWidgetName);
            client.AddBankReference(ClipSelectorGroupName);

            int milliseconds = 0;
            while (true)
            {
                if (client.WidgetExists(ClipSelectorGroupName))
                {
                    return true;
                }

                Thread.Sleep(1);
                if (milliseconds++ >= 1000)
                {
                    break;
                }
            }

            return false;
        }

        /// <summary>
        /// Refreshes the currently loaded animation by pressing the 'Reload Anim' button in
        /// the connected rag ui.
        /// </summary>
        internal void Refresh()
        {
            if (!this.IsConnected)
                return;

            WidgetClient client = RagClientFactory.CreateWidgetClient(_gameConnection);
            try
            {
                if (!this.EnsureAnimationBankIsOpen(client))
                {
                    return;
                }

                Refresh(client);
            }
            catch (TimeoutException e)
            {
                LogFactory.ApplicationLog.ToolException(e, "Operation timed out.");
                GameConnection = null;
            }
            catch (CommunicationException e)
            {
                LogFactory.ApplicationLog.ToolException(e, "Communication problem.");
                GameConnection = null;
            }
            finally
            {
                if (client.State == CommunicationState.Faulted)
                {
                    client.Abort();
                }
                else
                {
                    client.Close();
                }
            }
        }

        private void Refresh(WidgetClient client)
        {
            client.PressButton(ReloadAnimWidgetName);
        }

        /// <summary>
        /// Ensures that the associated clip file has been loaded and is correctly setup. 
        /// </summary>
        internal bool EnsureAnimationIsLoaded(string filename)
        {
            if (!this.IsConnected)
                return false;

            WidgetClient client = RagClientFactory.CreateWidgetClient(_gameConnection);
            try
            {
                if (!this.EnsureAnimationBankIsOpen(client))
                {
                    return false;
                }

                if (!this.TryUpdateStringWidget(ClipContextWidgetName, "Local Path", client))
                {
                    return false;
                }

                if (!this.TryUpdateStringWidget(LocalFileWidgetName, Path.GetFullPath(filename), client))
                {
                    return false;
                }

                client.PressButton(LoadAnimWidgetName);
                Refresh(client);
                return true;
            }
            catch (TimeoutException e)
            {
                LogFactory.ApplicationLog.ToolException(e, "Operation timed out.");
                GameConnection = null;
            }
            catch (CommunicationException e)
            {
                LogFactory.ApplicationLog.ToolException(e, "Communication problem.");
                GameConnection = null;
            }
            finally
            {
                if (client.State == CommunicationState.Faulted)
                {
                    client.Abort();
                }
                else
                {
                    client.Close();
                }
            }
            return false;
        }

        /// <summary>
        /// Ensures that the associated clip file has been loaded and that the clip is
        /// paused inside of RAG. 
        /// </summary>
        internal bool EnsureAnimationIsPaused()
        {
            if (!this.IsConnected)
                return false;

            WidgetClient client = RagClientFactory.CreateWidgetClient(_gameConnection);
            try
            {
                if (!this.EnsureAnimationBankIsOpen(client))
                {
                    return true;
                }

                client.WriteStringWidget("Animation/Preview Anims/Playback", "pause");
                client.WriteFloatWidget(RateWidgetName, 0.0f);
                return true;
            }
            catch (TimeoutException e)
            {
                LogFactory.ApplicationLog.ToolException(e, "Operation timed out.");
                GameConnection = null;
            }
            catch (CommunicationException e)
            {
                LogFactory.ApplicationLog.ToolException(e, "Communication problem.");
                GameConnection = null;
            }
            finally
            {
                if (client.State == CommunicationState.Faulted)
                {
                    client.Abort();
                }
                else
                {
                    client.Close();
                }
            }
            return true;
        }

        /// <summary>
        /// Ensures that the associated clip file has been loaded and that the clip is
        /// playing inside of RAG. 
        /// </summary>
        internal bool PlayAnimation()
        {
            if (!this.IsConnected)
                return true;

            WidgetClient client = RagClientFactory.CreateWidgetClient(_gameConnection);
            try
            {
                if (!this.EnsureAnimationBankIsOpen(client))
                {
                    return true;
                }

                client.WriteStringWidget("Animation/Preview Anims/Playback", "playforwards");
                client.WriteFloatWidget(RateWidgetName, 1.0f);
            }
            catch (TimeoutException e)
            {
                LogFactory.ApplicationLog.ToolException(e, "Operation timed out.");
                GameConnection = null;
            }
            catch (CommunicationException e)
            {
                LogFactory.ApplicationLog.ToolException(e, "Communication problem.");
                GameConnection = null;
            }
            finally
            {
                if (client.State == CommunicationState.Faulted)
                {
                    client.Abort();
                }
                else
                {
                    client.Close();
                }
            }
            return true;
        }

        /// <summary>
        /// Ensures that the associated clip file has been loaded and that the clip is
        /// rewinding inside of RAG. 
        /// </summary>
        internal bool RewindAnimation()
        {
            if (!this.IsConnected)
                return true;

            WidgetClient client = RagClientFactory.CreateWidgetClient(_gameConnection);
            try
            {
                if (!this.EnsureAnimationBankIsOpen(client))
                {
                    return true;
                }

                client.WriteStringWidget("Animation/Preview Anims/Playback", "playbackwards");
                client.WriteFloatWidget(RateWidgetName, -1.0f);
            }
            catch (TimeoutException e)
            {
                LogFactory.ApplicationLog.ToolException(e, "Operation timed out.");
                GameConnection = null;
            }
            catch (CommunicationException e)
            {
                LogFactory.ApplicationLog.ToolException(e, "Communication problem.");
                GameConnection = null;
            }
            finally
            {
                if (client.State == CommunicationState.Faulted)
                {
                    client.Abort();
                }
                else
                {
                    client.Close();
                }
            }
            return true;
        }

        /// <summary>
        /// Gets the frame position from inside of RAG.
        /// </summary>
        internal double GetFramePositionFromRAG()
        {
            if (!this.IsConnected)
                return 0.0;

            WidgetClient client = RagClientFactory.CreateWidgetClient(_gameConnection);
            try
            {
                return (double)client.ReadFloatWidget(FrameWidgetName);
            }
            catch (TimeoutException e)
            {
                LogFactory.ApplicationLog.ToolException(e, "Operation timed out.");
                GameConnection = null;
            }
            catch (CommunicationException e)
            {
                LogFactory.ApplicationLog.ToolException(e, "Communication problem.");
                GameConnection = null;
            }
            finally
            {
                if (client.State == CommunicationState.Faulted)
                {
                    client.Abort();
                }
                else
                {
                    client.Close();
                }
            }
            return 0.0;
        }

        /// <summary>
        /// Reactivates the ped by pressing the 'Reactivate Ped' button in the connected
        /// rag ui.
        /// </summary>
        private void ReactivatePed()
        {
            if (!this.IsConnected)
                return;

            WidgetClient client = RagClientFactory.CreateWidgetClient(_gameConnection);
            try
            {
                if (!this.EnsureAnimationBankIsOpen(client))
                    return;

                client.PressButton(ReactivatePedWidgetName);
            }
            catch (TimeoutException e)
            {
                LogFactory.ApplicationLog.ToolException(e, "Operation timed out.");
                GameConnection = null;
            }
            catch (CommunicationException e)
            {
                LogFactory.ApplicationLog.ToolException(e, "Communication problem.");
                GameConnection = null;
            }
            finally
            {
                if (client.State == CommunicationState.Faulted)
                {
                    client.Abort();
                }
                else
                {
                    client.Close();
                }
            }
        }

        /// <summary>
        /// Gets the rate of playback in RAG.
        /// </summary>
        /// <returns>
        /// 
        /// </returns>
        internal float GetPlayRate()
        {
            if (!this.IsConnected)
                return 0.0f;

            WidgetClient client = RagClientFactory.CreateWidgetClient(_gameConnection);
            try
            {
                if (!this.EnsureAnimationBankIsOpen(client))
                    return 0.0f;

                return client.ReadFloatWidget(RateWidgetName);
            }
            catch (TimeoutException e)
            {
                LogFactory.ApplicationLog.ToolException(e, "Operation timed out.");
                GameConnection = null;
            }
            catch (CommunicationException e)
            {
                LogFactory.ApplicationLog.ToolException(e, "Communication problem.");
                GameConnection = null;
            }
            finally
            {
                if (client.State == CommunicationState.Faulted)
                {
                    client.Abort();
                }
                else
                {
                    client.Close();
                }
            }
            return 0.0f;
        }

        /// <summary>
        /// Fires the <see cref="ConnectionStatusChanged"/> event off to anyone that
        /// is listening.
        /// </summary>
        private void OnStatusChanged()
        {
            EventHandler handler = this.ConnectionStatusChanged;
            if (handler == null)
                return;

            this.ConnectionStatusChanged(this, EventArgs.Empty);
        }

        /// <summary>
        /// Tries to set the specified widgets string value to the specified new value and
        /// returns a boolean value representing whether the update was successful or not.
        /// </summary>
        /// <param name="widgetPath">
        /// The full widget path to the button that should be pushed.
        /// </param>
        /// <param name="newValue">
        /// The new string value to set the specified widget to.
        /// </param>
        /// <returns>
        /// True if the specified widget path was successfully updated to the specified
        /// new value; otherwise, false.
        /// </returns>
        private bool TryUpdateStringWidget(string widgetPath, string newValue, WidgetClient client)
        {
            client.WriteStringWidget(widgetPath, newValue);
            client.SendSyncCommand();
            string selectedValue = client.ReadStringWidget(widgetPath);
            return selectedValue == newValue;
        }
        #endregion
    }
}
