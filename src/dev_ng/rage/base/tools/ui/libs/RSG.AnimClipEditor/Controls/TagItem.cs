﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using RSG.AnimClipEditor.ViewModel;

namespace RSG.AnimClipEditor.Controls
{
    /// <summary>
    /// Interaction logic for TagItem
    /// </summary>
    public class TagItem : ContentControl
    {

        public TagItem()
        {
            this.Focusable = false;
            //this.Loaded += new RoutedEventHandler(this.TagItem_Loaded);
        }
        
        static TagItem()
        {
            FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(typeof(TagItem), new FrameworkPropertyMetadata(typeof(TagItem)));
        }

        protected override void OnPreviewMouseDown(MouseButtonEventArgs e)
        {
            base.OnPreviewMouseDown(e);
            
            TagViewModel tvm = this.DataContext as TagViewModel;
            if ((Keyboard.Modifiers &
            (ModifierKeys.Shift | ModifierKeys.Control)) != ModifierKeys.None)
            {
                tvm.IsSelected = !tvm.IsSelected;
            }
            else
            {
                tvm.Clip.DeselectAllTags(tvm);
                tvm.IsSelected = true;
            }
        }

    }
}
