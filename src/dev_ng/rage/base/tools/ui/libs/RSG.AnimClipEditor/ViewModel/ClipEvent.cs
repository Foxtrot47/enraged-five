﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Media;
using System.Windows.Input;

using RSG.AnimClipEditor.Util;
using RSG.ManagedRage.ClipAnimation;

namespace RSG.AnimClipEditor.ViewModel
{
    public class ClipEvent : BaseViewModel
    {
        const int MAX_COLORS = 10;
        static Brush[] sColors = new Brush[MAX_COLORS] { Brushes.Red, Brushes.Green, Brushes.Blue, Brushes.Pink, Brushes.AntiqueWhite,
                                                                    Brushes.Black, Brushes.Brown, Brushes.Cornsilk, Brushes.Orange, Brushes.Olive};
        static int sEventCount = 0;

        #region Constructor
        public ClipEvent(String type, ObservableCollection<TagViewModel> tags)
        {
            _Type = type;
            _Tags = tags;
            _IsExpanded = true;
            _Height = 60;
            
            // TODO: Sort this shit out
            if (sEventCount < MAX_COLORS)
                _Colour = sColors[sEventCount];
            else
                _Colour = Brushes.Black;
            sEventCount++;
        }
        #endregion // Constructor

        #region Properties
        public ObservableCollection<TagViewModel> Tags
        {
            get
            {
                return _Tags;
            }
        }
        ObservableCollection<TagViewModel> _Tags;

        public TimeVal TimeData
        {
            get
            {
                return CoreManager.GlobalTimeVal;
            }
        }
        

        public String Type
        {
            get
            {
                return _Type;
            }
        }
        String _Type;

        public Brush Colour
        {
            get
            {
                return _Colour;
            }
        }
        Brush _Colour;

        public bool Expanded
        {
            get
            {
                return _Expanded;
            }
            set
            {
                if (_Expanded != value)
                    _Expanded = value;
            }
        }
        bool _Expanded;
       
        public int Height
        {
            get
            {
                return _Height;
            }
            set
            {
                if (Height != value)
                {
                    _Height = value;
                    OnPropertyChanged("Height");
                }
            }
        }
        int _Height;
        
        public bool IsExpanded
        {
            get
            {
                return _IsExpanded;
            }
            set
            {
                _IsExpanded = value;
                if (_IsExpanded)
                    Height = 60;
                else
                    Height = 20;
            }
        }
        bool _IsExpanded;

        #endregion // Properties

        #region Public Methods
        /*public void AddTag(MTag tag)
        {
            _Tags.Add(tag);
        }*/
        #endregion

        #region
        
        #endregion
    }
}
