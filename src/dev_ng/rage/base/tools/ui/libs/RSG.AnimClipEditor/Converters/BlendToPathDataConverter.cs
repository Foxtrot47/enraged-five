﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows;
using RSG.AnimClipEditor.ViewModel;

namespace RSG.AnimClipEditor.Converters
{

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class BlendToPathDataConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            double actualWidth = (double)values[0];
            double actualHeight = (double)values[1];
            GeometryGroup geometryGroup = new GeometryGroup();
            if (actualWidth == 0.0 || actualHeight == 0.0)
            {
                return geometryGroup;
            }

            float blendInPhase = (float)values[2];
            float blendOutPhase = (float)values[3];
            actualHeight = Math.Round(actualHeight);
            actualWidth = Math.Round(actualWidth);
            double blendInOffset = actualWidth * blendInPhase;
            double blendOutOffset = actualWidth - (actualWidth * blendOutPhase);

            LineGeometry blendIn = new LineGeometry()
            {
                StartPoint = new Point(0.0, actualHeight),
                EndPoint = new Point(blendInOffset, 1.0),
            };
            geometryGroup.Children.Add(blendIn);

            if (blendOutOffset - blendInOffset > 0)
            {
                LineGeometry blendMax = new LineGeometry()
                {
                    StartPoint = new Point(blendInOffset, 1.0),
                    EndPoint = new Point(blendOutOffset, 1.0),
                };
                geometryGroup.Children.Add(blendMax);
            }

            LineGeometry blendOut = new LineGeometry()
            {
                StartPoint = new Point(blendOutOffset, 1.0),
                EndPoint = new Point(actualWidth, actualHeight),
            };
            geometryGroup.Children.Add(blendOut);
            return geometryGroup;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    
    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class BlendInToCanvasConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            double actualWidth = (double)values[0];
            if (actualWidth == 0.0)
            {
                return 0.0;
            }

            float blendInPhase = (float)values[1];
            actualWidth = Math.Round(actualWidth);
            return (actualWidth * blendInPhase) - 3.0f;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class BlendOutToCanvasConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            double actualWidth = (double)values[0];
            if (actualWidth == 0.0)
            {
                return 0.0;
            }

            float blendOutPhase = (float)values[1];
            actualWidth = Math.Round(actualWidth);
            return actualWidth - (actualWidth * blendOutPhase) - 3.0f;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }   
}
