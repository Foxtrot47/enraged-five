﻿using System;
using System.Net;
using System.Windows;
using System.Linq;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections;
using System.Collections.Specialized;

namespace RSG.AnimClipEditor.View
{
    public class GridUtils
    {
        #region ItemsPerColumn attached property
        /// <summary>
        /// Identifies the ItemsPerRow attached property. 
        /// </summary>
        public static readonly DependencyProperty ItemsPerColumnProperty =
            DependencyProperty.RegisterAttached("ItemsPerColumn", typeof(int), typeof(GridUtils),
                new PropertyMetadata(0, new PropertyChangedCallback(OnItemsPerColumnPropertyChanged)));

        /// <summary>
        /// Gets the value of the ItemsPerRow property
        /// </summary>
        public static int GetItemsPerColumn(DependencyObject d)
        {
            return (int)d.GetValue(ItemsPerColumnProperty);
        }

        /// <summary>
        /// Sets the value of the ItemsPerRow property
        /// </summary>
        public static void SetItemsPerColumn(DependencyObject d, int value)
        {
            d.SetValue(ItemsPerColumnProperty, value);
        }

        /// <summary>
        /// Handles property changed event for the ItemsPerRow property, constructing
        /// the required ItemsPerRow elements on the grid which this property is attached to.
        /// </summary>
        private static void OnItemsPerColumnPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            RearrangeGrid(d as Grid);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="grid"></param>
        private static void RearrangeGrid(Grid grid)
        {
            // construct the required row definitions
            grid.LayoutUpdated += (s, e2) =>
            {
                var childCount = grid.Children.Count;
                int columnDifference = childCount - grid.ColumnDefinitions.Count;

                // if new items have been added, create the required grid rows
                // and assign the row index to each child
                if (columnDifference != 0)
                {
                    grid.ColumnDefinitions.Clear();
                    for (int row = 0; row < childCount; row++)
                    {
                        grid.ColumnDefinitions.Add(new ColumnDefinition());
                    }
                }

                // set the row property for each chid
                for (int i = 0; i < childCount; i++)
                {
                    var child = grid.Children[i] as FrameworkElement;
                    Grid.SetColumn(child, i);
                }
            };
        }
        #endregion

        #region ItemsSource attached property
        /// <summary>
        /// Identified the ItemsSource attached property
        /// </summary>
        public static readonly DependencyProperty ItemsSourceProperty =
            DependencyProperty.RegisterAttached("ItemsSource", typeof(IEnumerable), typeof(GridUtils),
                new PropertyMetadata(null, new PropertyChangedCallback(OnItemsSourcePropertyChanged)));

        /// <summary>
        /// Gets the value of the ItemsSource property
        /// </summary>
        public static IEnumerable GetItemsSource(DependencyObject d)
        {
            return (IEnumerable)d.GetValue(ItemsSourceProperty);
        }

        /// <summary>
        /// Sets the value of the ItemsSource property
        /// </summary>
        public static void SetItemsSource(DependencyObject d, IEnumerable value)
        {
            d.SetValue(ItemsSourceProperty, value);
        }

        /// <summary>
        /// Handles property changed event for the ItemsSource property.
        /// </summary>
        private static void OnItemsSourcePropertyChanged(DependencyObject d,
          DependencyPropertyChangedEventArgs e)
        {
            ItemsControl control = d as ItemsControl;

            // set the ItemsSource of the ItemsControl that this property is attached to
            control.ItemsSource = e.NewValue as IEnumerable;

            INotifyCollectionChanged notifyCollection = e.NewValue as INotifyCollectionChanged;
            if (notifyCollection != null)
            {
                // if a collection changed event occurs, reset the ItemsControl's
                // ItemsSource, rebuilding the UI 
                notifyCollection.CollectionChanged += (s, e2) =>
                {
                    control.ItemsSource = null;
                    control.ItemsSource = e.NewValue as IEnumerable;
                };
            }
        } 
        #endregion
    }
}
