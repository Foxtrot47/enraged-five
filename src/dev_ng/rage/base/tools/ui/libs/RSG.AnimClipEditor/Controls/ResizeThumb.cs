﻿namespace RSG.AnimClipEditor.Controls
{
    using System;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Controls.Primitives;
    using RSG.AnimClipEditor.ViewModel;

    /// <summary>
    /// Represents a System.Windows.Primitives.Thumb control that can be used to resize a
    /// clip tag visual on its track.
    /// </summary>
    public class ResizeThumb : Thumb
    {
        #region Constructors
        /// <summary>
        /// Intialises a new instance of the <see cref="ResizeThumb"/> class.
        /// </summary>
        public ResizeThumb()
        {
            this.DragDelta += this.OnDragDelta;
        }
        #endregion // Constructors

        #region Methods
        /// <summary>
        /// Gets called when the thumb is dragged by a positive or negative amount greater
        /// than zero.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.Windows.Primitives.DragDeltaEventArgs data used for this event.
        /// </param>
        private void OnDragDelta(object sender, DragDeltaEventArgs e)
        {
            FrameworkElement element = sender as FrameworkElement;
            if (element == null)
            {
                e.Handled = true;
                return;
            }

            TagViewModel tvm = element.DataContext as TagViewModel;
            double canvasLeft = 0.0;
            double frameWidthInPixels = tvm.Clip.PixelsPerFrame;
            DependencyObject parent = System.Windows.Media.VisualTreeHelper.GetParent(this);
            Border border = parent as Border;
            while (parent != null && border == null)
            {
                TagItem tagItem = parent as TagItem;
                if (tagItem != null)
                {
                    canvasLeft = Canvas.GetLeft(tagItem);
                }

                parent = System.Windows.Media.VisualTreeHelper.GetParent(parent);
                border = parent as Border;
                if (border != null)
                {
                    if (border.CornerRadius != new CornerRadius(14, 14, 14, 14))
                    {
                        border = null;
                    }
                }
            }

            if (border == null)
            {
                return;
            }

            switch (HorizontalAlignment)
            {
                case HorizontalAlignment.Left:
                    {
                        double xpoint = System.Windows.Input.Mouse.GetPosition(border).X;
                        double pixelDifferent = xpoint - canvasLeft;
                        double frameDifference = pixelDifferent / frameWidthInPixels;
                        double startFrame = 0.0;
                        if (tvm.Clip.SnapToFrame)
                        {
                            startFrame = Math.Round(tvm.StartAsFrame + frameDifference);
                        }
                        else
                        {
                            startFrame = tvm.StartAsFrame + frameDifference;
                        }

                        if (startFrame > tvm.EndAsFrame)
                        {
                            startFrame = tvm.EndAsFrame;
                        }
                        else if (startFrame < 0.0f)
                        {
                            startFrame = 0.0;
                        }

                        tvm.StartAsFrame = (float)startFrame;

                        if (tvm.StartAsFrame > tvm.Clip.LastVisibleFrame)
                        {
                            double oldFirstFrame = tvm.Clip.FirstVisibleFrame;
                            while (tvm.StartAsFrame > tvm.Clip.LastVisibleFrame)
                            {
                                double newFirstFrame = tvm.Clip.FirstVisibleFrame + 1;
                                tvm.Clip.FirstVisibleFrame = newFirstFrame;
                            }

                            double firstFrameDiff = tvm.Clip.FirstVisibleFrame - oldFirstFrame;
                            tvm.Clip.CurrentFramePosition += firstFrameDiff;
                        }
                        else if (tvm.StartAsFrame < tvm.Clip.FirstVisibleFrame)
                        {
                            double oldFirstFrame = tvm.Clip.FirstVisibleFrame;
                            while (tvm.StartAsFrame < tvm.Clip.FirstVisibleFrame)
                            {
                                double newFirstFrame = tvm.Clip.FirstVisibleFrame - 1;
                                if (newFirstFrame < 0.0)
                                {
                                    newFirstFrame = 0.0;
                                }

                                tvm.Clip.FirstVisibleFrame = newFirstFrame;
                            }

                            double firstFrameDiff = tvm.Clip.FirstVisibleFrame - oldFirstFrame;
                            tvm.Clip.CurrentFramePosition += firstFrameDiff;
                        }

                        break;
                    }
                case HorizontalAlignment.Right:
                    {   
                        double xpoint = System.Windows.Input.Mouse.GetPosition(border).X;
                        double pixelWidth = xpoint - canvasLeft;
                        double frameWidth = pixelWidth / frameWidthInPixels;
                        double endFrame = 0.0;
                        if (tvm.Clip.SnapToFrame)
                        {
                            endFrame = Math.Round(tvm.StartAsFrame + frameWidth);
                        }
                        else
                        {
                            endFrame = tvm.StartAsFrame + frameWidth;
                        }

                        if (endFrame > tvm.Clip.MaximumFrameCount)
                        {
                            endFrame = tvm.Clip.MaximumFrameCount;
                        }

                        tvm.EndAsFrame = (float)endFrame;

                        if (tvm.EndAsFrame > tvm.Clip.LastVisibleFrame)
                        {
                            double oldFirstFrame = tvm.Clip.FirstVisibleFrame;
                            while (tvm.EndAsFrame > tvm.Clip.LastVisibleFrame)
                            {
                                double newFirstFrame = tvm.Clip.FirstVisibleFrame + 1;

                                tvm.Clip.FirstVisibleFrame = newFirstFrame;
                            }

                            double firstFrameDiff = tvm.Clip.FirstVisibleFrame - oldFirstFrame;
                            tvm.Clip.CurrentFramePosition += firstFrameDiff;
                        }
                        else if (tvm.EndAsFrame < tvm.Clip.FirstVisibleFrame)
                        {
                            double oldFirstFrame = tvm.Clip.FirstVisibleFrame;
                            while (tvm.EndAsFrame < tvm.Clip.FirstVisibleFrame)
                            {
                                double newFirstFrame = tvm.Clip.FirstVisibleFrame - 1;
                                if (newFirstFrame < 0.0)
                                {
                                    newFirstFrame = 0.0;
                                }

                                tvm.Clip.FirstVisibleFrame = newFirstFrame;
                            }

                            double firstFrameDiff = tvm.Clip.FirstVisibleFrame - oldFirstFrame;
                            tvm.Clip.CurrentFramePosition += firstFrameDiff;
                        }

                        break;
                    }
                default:
                    break;
            }           

            e.Handled = true;
        }
        #endregion
    } // RSG.AnimClipEditor.Controls.ResizeThumb {Class}
} // RSG.AnimClipEditor.Controls {Namespace}
