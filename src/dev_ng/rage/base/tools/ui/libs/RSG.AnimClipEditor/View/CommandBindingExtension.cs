﻿using System;
using System.Reflection;
using System.Windows;
using System.Windows.Input;
using System.Windows.Markup;
using RSG.PlaybackControl;

namespace RSG.AnimClipEditor.View
{
    /// <summary>
    /// Mark-up extension for the command parameter on any object. Used to bind view model
    /// commands to the command property of a key binding.
    /// </summary>
    [MarkupExtensionReturnType(typeof(ICommand))]
    public class CommandBindingExtension : MarkupExtension
    {
        #region Fields
        /// <summary>
        /// The object we setting the command on.
        /// </summary>
        private object targetObject;

        /// <summary>
        /// The property we are setting through this binding.
        /// </summary>
        private object targetProperty;

        /// <summary>
        /// A value indicating whether the event handler for the data context changing has
        /// been set.
        /// </summary>
        private bool dataContextChangeHandlerSet;
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the
        /// <see cref="UniversalLogViewer.CommandBindingExtension"/> class.
        /// </summary>
        public CommandBindingExtension()
        {
        }

        /// <summary>
        /// Initialises a new instance of the
        /// <see cref="UniversalLogViewer.CommandBindingExtension"/> class binding to the
        /// given command name.
        /// </summary>
        /// <param name="commandName">
        /// The name of the command to bind to.
        /// </param>
        public CommandBindingExtension(string commandName)
        {
            this.CommandName = commandName;
        }
        #endregion

        #region Properties
        [ConstructorArgument("commandName")]
        public string CommandName
        {
            get;
            set;
        }
        #endregion

        #region Methods
        /// <summary>
        /// When implemented in a derived class, returns an object that is set as the
        /// value of the target property for this mark-up extension.
        /// </summary>
        /// <param name="serviceProvider">
        /// Object that can provide services for the mark-up extension.
        /// </param>
        /// <returns>
        /// The object value to set on the property where the extension is applied.
        /// </returns>
        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            this.SetTargetProperties(serviceProvider);
            if (string.IsNullOrEmpty(CommandName))
                return SimpleCommand.EmptyCommand;

            FrameworkElement rootElement = GetRootElement(serviceProvider);
            if (rootElement == null)
                return SimpleCommand.EmptyCommand;

            object dataContext = rootElement.DataContext;
            if (!dataContextChangeHandlerSet)
            {
                rootElement.DataContextChanged += this.OnDataContextChanged;
                dataContextChangeHandlerSet = true;
            }

            ICommand command = GetCommand(dataContext, CommandName);
            if (command != null)
                return command;
            else
                return SimpleCommand.EmptyCommand;
        }

        /// <summary>
        /// Sets the target object and target property from the specified service provider.
        /// </summary>
        /// <param name="serviceProvider">
        /// The service provider to get the target object and property from.
        /// </param>
        private void SetTargetProperties(IServiceProvider serviceProvider)
        {
            Type serviceType = typeof(IProvideValueTarget);
            object targetService = serviceProvider.GetService(serviceType);
            if (!(targetService is IProvideValueTarget))
                return;

            targetObject = (targetService as IProvideValueTarget).TargetObject;
            targetProperty = (targetService as IProvideValueTarget).TargetProperty;
        }

        /// <summary>
        /// Gets the root framework element on the specified service provider object.
        /// </summary>
        /// <param name="serviceProvider">
        /// The instance of the service provider to get the root framework element from.
        /// </param>
        /// <returns>
        /// The root framework element for the specified service provider. 
        /// </returns>
        private FrameworkElement GetRootElement(IServiceProvider serviceProvider)
        {
            string fieldName = @"_xamlContext";
            BindingFlags flags = BindingFlags.Instance | BindingFlags.NonPublic;
            FieldInfo field = serviceProvider.GetType().GetField(fieldName, flags);
            if (field == null)
                return null;

            object context = field.GetValue(serviceProvider);

            fieldName = @"_rootInstance";
            field = context.GetType().GetField(fieldName, flags);
            if (field == null)
                return null;

            return field.GetValue(context) as FrameworkElement;
        }

        /// <summary>
        /// Gets called whenever the data context on the root target element changes.
        /// </summary>
        /// <param name="sender">
        /// The object that sent this event.
        /// </param>
        /// <param name="e">
        /// The event data associated with this event.
        /// </param>
        private void OnDataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            FrameworkElement rootElement = sender as FrameworkElement;
            if (rootElement == null)
                return;

            object dataContext = rootElement.DataContext;
            if (dataContext == null)
                return;

            ICommand command = GetCommand(dataContext, CommandName);
            if (command == null)
                return;

            AssignCommand(command);
        }

        /// <summary>
        /// Gets the command by looking at the property value on the specified data context
        /// with the specified name.
        /// </summary>
        /// <param name="dataContext">
        /// The object to get the command from.
        /// </param>
        /// <param name="commandName">
        /// The name of the property on the data context that specifies the command.
        /// </param>
        /// <returns>
        /// The value of the command set on the property with the specified name on the
        /// specified data context object.
        /// </returns>
        private ICommand GetCommand(object dataContext, string commandName)
        {
            if (dataContext == null)
                return null;

            PropertyInfo property = dataContext.GetType().GetProperty(commandName);
            if (property == null)
                return null;

            ICommand command = property.GetValue(dataContext, null) as ICommand;
            if (command != null)
                return command;

            return null;
        }

        /// <summary>
        /// Sets the specified command onto the associated target object.
        /// </summary>
        /// <param name="command">
        /// The command that needs to be set on the target object.
        /// </param>
        private void AssignCommand(ICommand command)
        {
            if (this.targetObject == null || this.targetProperty == null)
                return;

            if (targetProperty is DependencyProperty)
            {
                DependencyObject targetObject = this.targetObject as DependencyObject;
                DependencyProperty property = this.targetProperty as DependencyProperty;
                targetObject.SetValue(property, command);
            }
            else
            {
                PropertyInfo property = this.targetProperty as PropertyInfo;
                property.SetValue(this.targetObject, command, null);
            }
        }
        #endregion
    }
}
