﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.AnimClipEditor.Util
{
    public static class ConvertUtils
    {
        #region Contants
        static double FRAMES_PER_SECOND = 30.0;
        #endregion 
        #region Public Functions
        public static double PhaseToPixel(float phaseValue, double pixelRange)
        {
            double pixelVal = phaseValue * pixelRange;
            return pixelVal;
        }

        public static double PixelToPhase(double pixelRange, double pixelValue)
        {
            double phaseVal = (pixelValue / pixelRange);
            return phaseVal;
        }

        public static double PhaseToFrame(float phaseValue)
        {
            return Math.Round(phaseValue * FRAMES_PER_SECOND);
        }

        public static double FrameToPhase(float frameValue)
        {
            return frameValue / FRAMES_PER_SECOND;
        }
        #endregion // Public Functions
    }
}
