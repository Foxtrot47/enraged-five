﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using RSG.ManagedRage.ClipAnimation;

namespace RSG.AnimClipEditor.ViewModel
{
    public class PropertyViewModel
    {
        #region Constructors
        public PropertyViewModel(MProperty mproperty)
        {
            _Property = mproperty;
            _PropertyAttributes = new List<PropertyAttributeBaseViewModel>();
        }
        #endregion // Constructors

        #region Properties
        public MProperty Property
        {
            get
            {
                return _Property;
            }
        }
        MProperty _Property;

        public string Name 
        {
            get
            {
                return _Property.GetName();
            }
        }

        private string Enabled_ = null;
        public string Enabled
        {
            get
            {
                return Enabled_;
            }
            set
            {
                Enabled_ = value;
            }
        }

        public List<PropertyAttributeBaseViewModel> PropertyAttributes
        {
            get
            {
                return _PropertyAttributes;
            }
        }
        List<PropertyAttributeBaseViewModel> _PropertyAttributes;

        public int PropertyAttributesCount
        {
            get
            {
                return PropertyAttributes.Count;
            }
        }
        #endregion
    }
}
