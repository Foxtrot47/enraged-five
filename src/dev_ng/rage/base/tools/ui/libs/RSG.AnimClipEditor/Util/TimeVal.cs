﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.AnimClipEditor.Util
{
    /// <summary>
    /// Stors information required for a time slider
    /// </summary>
    public class TimeVal
    {
        #region Constructors
        /// <summary>
        /// Constructor takes minimum data required to render our time slider
        /// </summary>
        public TimeVal(double start, double end, double interval)
        {
            _Start = start;
            _End = end;
            _Interval = interval;
        }
        #endregion

        #region Properties
        public double Start
        {
            get
            {
                return _Start;
            }
        }
        double _Start;

        public double End
        {
            get
            {
                return _End;
            }
        }
        double _End;

        public double Interval
        {
            get
            {
                return _Interval;
            }
        }
        double _Interval;

        public double Value
        {
            get
            {
                return _Value;
            }
            set
            {
                if(_Value != value)
                    _Value = value;
            }
        }
        double _Value;
        #endregion // Properties

    }
}
