﻿using System;

using RSG.ManagedRage.ClipAnimation;

namespace RSG.AnimClipEditor.ViewModel.PropertyAttribute
{
    public class BitSetViewModel : PropertyAttributeBaseViewModel, IPropertyAttribute
    {
        #region Constructors
        public BitSetViewModel(MPropertyAttributeBitSet mpropattr, ClipViewModel clip)
            : base(mpropattr, clip)
        {

        }
        #endregion

        #region Properties
        int Value
        {
            get
            {
                throw new Exception(this.Type.ToString() + " type not implemented");
            }
        }
        #endregion // Properties
    }
}
