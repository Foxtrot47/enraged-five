﻿using System;

using RSG.ManagedRage.ClipAnimation;

namespace RSG.AnimClipEditor.ViewModel.PropertyAttribute
{
    public class BoolViewModel : PropertyAttributeBaseViewModel
    {
        #region Constructors
        public BoolViewModel(MPropertyAttributeBool mpropattr, ClipViewModel clip)
            : base(mpropattr, clip)
        {
        }

        public BoolViewModel(MPropertyAttributeBool mpropattr, string initialValue, ClipViewModel clip)
            : base(mpropattr, clip)
        {
            SetFromString(initialValue);
        }
        #endregion

        #region Properties
        public bool Value
        {
            get
            {
                return (bool)(this.PropertyAttribute as MPropertyAttributeBool).GetBool();
            }
            set
            {
                if (value != (bool)(this.PropertyAttribute as MPropertyAttributeBool).GetBool())
                {
                    (this.PropertyAttribute as MPropertyAttributeBool).SetBool(Convert.ToBoolean(value));
                    FireValueChange();
                }
            }
        }

        public bool IsApartOfRadioGroup
        {
            get;
            set;
        }

        public string RadioButtonGroupName
        {
            get;
            set;
        }
        #endregion // Properties

        #region methods
        public override bool TestString(string value)
        {
            bool testingValue;
            if (!bool.TryParse(value, out testingValue))
                return false;

            return testingValue == Value;
        }

        public override void SetFromString(string value)
        {
            bool newValue;
            if (bool.TryParse(value, out newValue))
            {
                this.Value = newValue;
            }
        }

        public override string GetValueAsString()
        {
            return this.Value.ToString().ToLower();
        } 

        public override void SetFromOther(PropertyAttributeBaseViewModel other)
        {
            if (!(other is BoolViewModel))
                return;

            this.Value = (other as BoolViewModel).Value;
        }
        #endregion
    }
}
