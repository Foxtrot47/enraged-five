﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using RSG.AnimClipEditor.ViewModel;
using RSG.ManagedRage.ClipAnimation;
using RSG.PlaybackControl;
using System.Collections;
using System.Collections.ObjectModel;
using RSG.AnimationMetadata;
using RSG.AnimClipEditor.DataModel;
using System.ComponentModel;

namespace RSG.AnimClipEditor.View
{
    /// <summary>
    /// Interaction logic for UserControl1.xaml
    /// </summary>
    public partial class ClipEditor : ContentControl
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the
        /// <see cref="RSG.AnimClipEditor.View"/> class.
        /// </summary>
        /// <param name="content"></param>
        /// <param name="bankName"></param>
        /// <param name="clipName"></param>
        /// <param name="mclip"></param>
        public ClipEditor(MClip mclip)
        {
            ClipModel clipmodel = new ClipModel(mclip);
            this.DataContext = new ClipViewModel(clipmodel);

            bool loaded = (bool)mclip.GetHasLoaded();
            ClipViewModel cvm = (this.DataContext as ClipViewModel);
            cvm.Loaded = loaded;
            cvm.clipEditor = this;

            InitializeComponent();
            if (ClipViewModel.staticShowWrapper)
            {
                this.MainGrid.RowDefinitions[4].Height = new GridLength(200.0);
                this.ShowHideButton.Content = "Hide Properties";
                this.ShowHideButton.ToolTip = "Hide Properties";
            }
            else
            {
                this.MainGrid.RowDefinitions[4].Height = new GridLength(0.0);
                this.ShowHideButton.Content = "Show Properties";
                this.ShowHideButton.ToolTip = "Show Properties";
            }

            this.DataContextChanged += OnDataContextChanged;
            this.TimeLine.SizeChanged += this.OnTimeLineSizeChanged;

            this.TagTrackList.SelectionChanged += this.OnTagClicked;
        }
        #endregion

        #region Methods
        internal void OnTagSelected()
        {
            this.TagTrackList.Focus();
        }

        /// <summary>
        /// Gets called whenever the actual width or actual height of the timeline slider
        /// control changes.
        /// </summary>
        /// <param name="sender">
        /// The object that handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.Windows.SizeChangedEventArgs data for the event.
        /// </param>
        private void OnTimeLineSizeChanged(object sender, SizeChangedEventArgs e)
        {
            ClipViewModel cvm = this.DataContext as ClipViewModel;
            if (e.WidthChanged == false || cvm == null)
            {
                return;
            }

            cvm.TagTrackVisiblePixelWidth = this.TimeLine.ActualWidth - 20.0;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void OnDataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            ClipViewModel cvm = (this.DataContext as ClipViewModel);
            bool loaded = (bool)cvm.Model.ManagedClip.GetHasLoaded();
            cvm.Loaded = loaded;
            cvm.TagTrackVisiblePixelWidth = this.TimeLine.ActualWidth - 20.0;
        }

        private void Delete_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
            e.Handled = true;
        }

        private void Delete_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            ClipViewModel cvm = (this.DataContext as ClipViewModel);
            cvm.DeleteSelectedTags();
            e.Handled = true;
        }

        void dataContext_TagAddedCommandFired(object sender, EventArgs e)
        {
            //ClipViewModel cvm = (this.DataContext as ClipViewModel);
            //TagTab tab = GetVisualDescendent<TagTab>(this);
            //if (tab == null)
            //    return;

            //float phase = (this.PlaybackControl.DataContext as PlaybackControlViewModel).Phase;

            //cvm.AddTagToSelected(phase, tab);
        }

        private bool AddTagTrack(string name)
        {
            if (string.IsNullOrWhiteSpace(name))
                return false;

            ClipViewModel viewModel = this.DataContext as ClipViewModel;
            if (viewModel == null)
                return false;

            ClipEventViewModel newTrackViewModel = new ClipEventViewModel(name, viewModel);
            newTrackViewModel.Tags.CollectionChanged += viewModel.Tags_CollectionChanged;
            viewModel.AddClipEvent(newTrackViewModel);
            return true;
        }

        private bool AddClipProperty(string name)
        {
            if (string.IsNullOrWhiteSpace(name))
                return false;

            ClipViewModel viewModel = this.DataContext as ClipViewModel;
            if (viewModel == null)
                return false;

            viewModel.AddClipPropertyExecute(name);
            return true;
        }
        #endregion

        private void OnTagClicked(object sender, SelectionChangedEventArgs e)
        {
            ListBox listBox = sender as ListBox;
            ClipViewModel cvm = this.DataContext as ClipViewModel;
            if (cvm == null)
                return;

            listBox.Focus();
            if (listBox.SelectedItems.Count > 0)
            {
                //  Changed selection
                TagViewModel tvm = listBox.SelectedItems[0] as TagViewModel;

                if (tvm != null)
                {
                    foreach (TagViewModel tag in cvm.SelectedTagTrackTags)
                    {
                        tag.IsSelected = (tag == tvm);
                    }
                }
            }
        }

        private void ListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            (sender as ListBox).Focus();
            ClipViewModel cvm = this.DataContext as ClipViewModel;
            if (cvm == null)
                return;

            if (cvm.SelectedTagTracks == null)
                cvm.SelectedTagTracks = new List<ClipEventViewModel>();
            else
                cvm.SelectedTagTracks.Clear();

            foreach (var selection in (sender as ListBox).SelectedItems)
            {
                if (selection is ClipEventViewModel)
                    cvm.SelectedTagTracks.Add(selection as ClipEventViewModel);
            }

            if (cvm.DefinedTags == null)
                cvm.DefinedTags = new ObservableCollection<ClipViewModel.PreDefinedTagDefinition>();
            else
                cvm.DefinedTags.Clear();

            if (cvm.SelectedTagTracks.Count != 1)
                return;

            //  Update the tags list view
            cvm.FirePropertyChanged("SelectedTagTrackTags");

            string type = cvm.SelectedTagTracks[0].Type;
            if (!ClipTagDefinitions.PropertyDescriptions.ContainsKey(type))
                return;

            ClipPropertyDefinition tagDefinition = ClipTagDefinitions.PropertyDescriptions[type];
            if (tagDefinition != null && tagDefinition.PropertyGroups.Count > 0)
            {
                int index = 1;
                foreach (var group in tagDefinition.PropertyGroups)
                {
                    cvm.DefinedTags.Add(new ClipViewModel.PreDefinedTagDefinition(group, cvm, index++));
                }
            }
            else
            {
                cvm.DefinedTags.Add(new ClipViewModel.PreDefinedTagDefinition("Default", cvm));
            }
        }

        private void DeleteClipPropertyExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            ClipViewModel cvm = (this.DataContext as ClipViewModel);
            if (cvm == null)
                return;

            List<PropertyViewModel> properties = new List<PropertyViewModel>();
            foreach (PropertyViewModel selection in this.ClipPropertiesListBox.SelectedItems)
                properties.Add(selection);

            foreach (PropertyViewModel property in properties)
                cvm.RemoveClipPropertyExecute(property);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (this.MainGrid.RowDefinitions[4].Height.Value != 0.0)
            {
                this.MainGrid.RowDefinitions[4].Height = new GridLength(0.0);
                this.ShowHideButton.Content = "Show Properties";
                this.ShowHideButton.ToolTip = "Show Properties";
                ClipViewModel.staticShowWrapper = false;
            }
            else
            {
                this.MainGrid.RowDefinitions[4].Height = new GridLength(200.0, GridUnitType.Star);
                this.ShowHideButton.Content = "Hide Properties";
                this.ShowHideButton.ToolTip = "Hide Properties";
                ClipViewModel.staticShowWrapper = true;
            }
        }

        private void MenuItem_SubmenuOpened(object sender, RoutedEventArgs e)
        {
            MenuItem target = sender as MenuItem;
            ClipViewModel viewModel = this.DataContext as ClipViewModel;
            if (target == null || viewModel == null)
                return;

            target.Items.Clear();
            foreach (var track in viewModel.PropertyDefinitions)
            {
                MenuItem newChild = new MenuItem();
                newChild.Header = track.Key;
                newChild.Click += new RoutedEventHandler(TrackMenuItemClicked);
                foreach (ClipEventViewModel trackViewModel in viewModel.TagTracks)
                {
                    if (string.CompareOrdinal(track.Key, trackViewModel.Type) == 0)
                    {
                        newChild.IsEnabled = false;
                        break;
                    }
                }

                target.Items.Add(newChild);
            }
        }

        private void TrackMenuItemClicked(object sender, RoutedEventArgs e)
        {
            MenuItem menuItem = sender as MenuItem;
            if (menuItem == null)
                return;

            e.Handled = this.AddTagTrack(menuItem.Header as string);
            if (!e.Handled)
                return;

            TagTracksList.UnselectAll();
            int newIndex = TagTracksList.Items.Count - 1;

            TagTracksList.SelectedIndex = newIndex;
            TagTracksList.Focus();
            TagTracksList.ScrollIntoView(TagTracksList.SelectedItem);
        }

        private void ClipPropertiesSubmenuOpened(object sender, RoutedEventArgs e)
        {
            MenuItem target = sender as MenuItem;
            ClipViewModel viewModel = this.DataContext as ClipViewModel;
            if (target == null || viewModel == null)
                return;

            target.Items.Clear();
            foreach (string property in viewModel.ClipPropertyDefinitions)
            {
                MenuItem newChild = new MenuItem();
                newChild.Header = property;
                newChild.Click += new RoutedEventHandler(ClipPropertyMenuItemClicked);
                foreach (PropertyViewModel pvm in viewModel.ClipPropertyAttributes)
                {
                    if (string.CompareOrdinal(property, pvm.Name) == 0)
                    {
                        newChild.IsEnabled = false;
                        break;
                    }
                }

                target.Items.Add(newChild);
            }
        }

        private void ClipPropertyMenuItemClicked(object sender, RoutedEventArgs e)
        {
            MenuItem menuItem = sender as MenuItem;
            if (menuItem == null)
                return;

            e.Handled = this.AddClipProperty(menuItem.Header as string);
            if (!e.Handled)
                return;

            ClipPropertiesListBox.UnselectAll();
            int newIndex = ClipPropertiesListBox.Items.Count - 1;

            ClipPropertiesListBox.SelectedIndex = newIndex;
            ClipPropertiesListBox.Focus();
            ClipPropertiesListBox.ScrollIntoView(ClipPropertiesListBox.SelectedItem);
        }

        private void TracksDoubleClicked(object sender, MouseButtonEventArgs e)
        {
            if (!(e.Source is ListBoxItem) || !(e.OriginalSource is Border))
            {
                return;
            }

            Border border = e.OriginalSource as Border;
            if (border.CornerRadius != new CornerRadius(14, 14, 14, 14))
            {
                return;
            }

            ClipEventViewModel track = border.DataContext as ClipEventViewModel;
            ClipViewModel clipViewModel = this.DataContext as ClipViewModel;
            if (track == null || clipViewModel == null)
                return;

            double width = border.ActualWidth - 10.0;
            double pixel = Math.Max(Mouse.GetPosition(border).X, 0.0);
            double frame = pixel / clipViewModel.PixelsPerFrame;

            if (this.TimeLine.IsSnapToTickEnabled)
            {
                frame = Math.Min(Math.Floor(frame), clipViewModel.MaximumFrameCount);
            }

            this.TimeLine.Value = frame;
            clipViewModel.AddTag(Enumerable.Repeat<ClipEventViewModel>(track, 1));
            e.Handled = true;
        }

        private void Slider_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ClipViewModel clipViewModel = this.DataContext as ClipViewModel;
            if (clipViewModel == null)
            {
                return;
            }

            if (clipViewModel.MaximumFrameCount != 0)
            {
                clipViewModel.PixelsPerFrame = clipViewModel.TagTrackVisiblePixelWidth / clipViewModel.MaximumFrameCount;
            }
        }
    }
}
