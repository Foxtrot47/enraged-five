﻿using System;

using RSG.ManagedRage.ClipAnimation;

namespace RSG.AnimClipEditor.ViewModel.PropertyAttribute
{
    public class QuaternionViewModel : PropertyAttributeBaseViewModel
    {
        #region Constructors
        public QuaternionViewModel(MPropertyAttributeQuaternion mpropattr, ClipViewModel clip)
            : base(mpropattr, clip)
        {

        }
        #endregion

        #region Properties
        public int Value
        {
            get
            {
                throw new Exception(this.Type.ToString() + " type not implemented");
            }
        }
        #endregion // Properties
    }
}
