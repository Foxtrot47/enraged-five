﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls.Primitives;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows;

using RSG.AnimClipEditor.ViewModel;
using RSG.AnimClipEditor.Util;
using System.Windows.Media;
using RSG.AnimClipEditor.View;

namespace RSG.AnimClipEditor.Controls
{
    public class MoveThumb : Thumb
    {
        float startPosition;
        double totalDelta;
        Nullable<int> direction;
        bool hadMoved;
        
        public MoveThumb()
        {
            DragDelta += new DragDeltaEventHandler(this.MoveThumb_DragDelta);
            DragStarted += new DragStartedEventHandler(this.MoveThumb_DragStarted);
            DragCompleted += new DragCompletedEventHandler(this.MoveThumb_DragCompleted);
        }

        protected override void OnInitialized(EventArgs e)
        {
            UpdatePositions(this);

            base.OnInitialized(e);
        }

        private void MoveThumb_DragDelta(object sender, DragDeltaEventArgs e)
        {
            if (e.HorizontalChange == 0.0)
                return;

            DependencyObject parent = VisualTreeHelper.GetParent(sender as DependencyObject);
            ClipEditor clipEditor = parent as ClipEditor;
            while (parent != null && clipEditor == null)
            {
                parent = VisualTreeHelper.GetParent(parent);
                clipEditor = parent as ClipEditor;
            }

            List<TagViewModel> viewModels = new List<TagViewModel>();
            if (clipEditor != null)
            {
                ClipViewModel clipViewModel = clipEditor.DataContext as ClipViewModel;
                viewModels.AddRange(clipViewModel.SelectedTags);
            }
            else
            {
                TagViewModel tvm;
                if (this.DataContext is Control)
                {
                    Control item = this.DataContext as Control;
                    tvm = item.DataContext as TagViewModel;
                }
                else
                {
                    tvm = this.DataContext as TagViewModel;
                }

                viewModels.Add(tvm);
            }

            foreach (TagViewModel tvm in viewModels)
            {
                this.HandleDelta(tvm, e);
            }

            UpdatePositions(sender);

            e.Handled = true;
        }

        private void HandleDelta(TagViewModel tvm, DragDeltaEventArgs e)
        {
            if (tvm == null)
            {
                return;
            }

            double frameDelta = e.HorizontalChange / tvm.Clip.PixelsPerFrame;
            totalDelta = frameDelta;
            this.hadMoved = frameDelta != 0.0;
            if (direction != null)
            {
                if (direction.Value > 0 && e.HorizontalChange < 0.0)
                {
                    totalDelta = 0.0;
                    direction = -1;
                }
                else if (direction.Value < 0 && e.HorizontalChange > 0.0)
                {
                    totalDelta = 0.0;
                    direction = 1;
                }
            }
            else
            {
                direction = e.HorizontalChange > 0.0 ? 1 : -1;
            }

            if (tvm.Clip.SnapToFrame)
            {
                float newStart = tvm.StartAsFrame + (float)totalDelta;
                if (e.HorizontalChange < 0)
                {
                    float roundedStart = (float)Math.Ceiling(newStart);
                    if (roundedStart != tvm.StartAsFrame)
                    {
                        totalDelta = 0;
                        tvm.MoveToNewFramePosition(roundedStart);
                    }
                }
                else
                {
                    float roundedStart = (float)Math.Floor(newStart);
                    if (roundedStart != tvm.StartAsFrame)
                    {
                        totalDelta = 0;
                        tvm.MoveToNewFramePosition(roundedStart);
                    }
                }
            }
            else
            {
                float newStart = tvm.StartAsFrame + (float)frameDelta;
                tvm.MoveToNewFramePosition(newStart);
            }
        }

        private void MoveThumb_DragStarted(object sender, DragStartedEventArgs e)
        {
            this.hadMoved = false;
            TagViewModel tvm;
            if (this.DataContext is Control)
            {
                Control item = this.DataContext as Control;
                tvm = item.DataContext as TagViewModel;
            }
            else
            {
                tvm = this.DataContext as TagViewModel;
            }
            if (tvm == null)
                return;


            if ((Keyboard.Modifiers & (ModifierKeys.Shift | ModifierKeys.Control)) != ModifierKeys.None)
            {
                tvm.IsSelected = !tvm.IsSelected;
            }
            else
            {
                if (tvm.IsSelected == false)
                {
                    tvm.Clip.DeselectAllTags(tvm);
                    tvm.IsSelected = true;
                }
            }

            this.startPosition = tvm.StartAsFrame;
            this.totalDelta = 0;
            this.direction = null;
            e.Handled = true;
        }

        private void MoveThumb_DragCompleted(object sender, DragCompletedEventArgs e)
        {
            TagViewModel tvm;
            if (this.DataContext is Control)
            {
                Control item = this.DataContext as Control;
                tvm = item.DataContext as TagViewModel;
            }
            else
            {
                tvm = this.DataContext as TagViewModel;
            }

            if ((Keyboard.Modifiers & (ModifierKeys.Shift | ModifierKeys.Control)) != ModifierKeys.None)
            {
            }
            else
            {
                if (tvm != null && !this.hadMoved)
                {
                    tvm.Clip.DeselectAllTags(tvm);
                    tvm.IsSelected = true;
                }
            }

            e.Handled = true;
        }

        private void UpdatePositions(object sender)
        {
            DependencyObject parent = VisualTreeHelper.GetParent(sender as DependencyObject);
            ClipEditor clipEditor = parent as ClipEditor;
            while (parent != null && clipEditor == null)
            {
                parent = VisualTreeHelper.GetParent(parent);
                clipEditor = parent as ClipEditor;
            }

            ClipViewModel clipViewModel = clipEditor.DataContext as ClipViewModel;

            clipViewModel.UpdatePositions();
        }
        
    }
}
