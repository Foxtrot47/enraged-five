﻿using System;
using System.Windows.Data;
using System.Collections.Generic;
using System.Globalization;
using System.Windows.Controls;


using RSG.AnimClipEditor.Util;
using RSG.AnimClipEditor.ViewModel;

namespace RSG.AnimClipEditor.Converters
{
    [ValueConversion(typeof(double), typeof(double))]
    public class PhaseToPixelConverter : IValueConverter
    {
        #region IValueConverter Members
        public object Convert(object value, Type targetType,
            object parameter, CultureInfo culture)
        {

            if (parameter != null)
            {
                
                return value;
            }
            else
                return value;
        }

        public object ConvertBack(object value, Type targetType,
            object parameter, CultureInfo culture)
        {
            if (parameter != null)
            {
                return value;
            }
            else
                return value;
        }
        #endregion
    }

    [ValueConversion(typeof(double), typeof(double))]
    public class PhaseToPixelMultiConverter : IMultiValueConverter
    {
        #region IMultiValueConverter Members
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            TagViewModel tagViewModel = values[3] as TagViewModel;
            if (tagViewModel == null)
            {
                return 0.0;
            }

            ClipViewModel clipViewModel = tagViewModel.Clip;
            if (clipViewModel == null)
            {
                return 0.0;
            }

            return tagViewModel.StartAsFrame * clipViewModel.PixelsPerFrame;




            //Border ctrl = values[1] as Border;
            //double w = ctrl.Width;
            //if ((double)values[1] == 0.0)
            //{
                
            //    return (814.0 * (float)values[0]);
            //}
            //else
            //{
            //    double value = ConvertUtils.PhaseToPixel((float)values[0], (double)values[1] - 10.0);
            //    return value;
            //}
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
        #endregion
    }

}
