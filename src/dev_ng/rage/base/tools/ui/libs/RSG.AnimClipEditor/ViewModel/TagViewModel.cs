﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;

using RSG.ManagedRage.ClipAnimation;
using RSG.AnimClipEditor.Util;
using System.Windows.Media;
using RSG.AnimationMetadata;
using RSG.PlaybackControl;
using RSG.AnimClipEditor.ViewModel.PropertyAttribute;


namespace RSG.AnimClipEditor.ViewModel
{
    public class TagViewModel : BaseViewModel
    {
        #region Fields
        MTag _Tag;
        double _ViewWidth;
        bool _IsSelected;
        bool _IsVisble;
        Brush _Background;
        double _GlobalWidthInPixels;
        private ClipViewModel clip;
        PropertyViewModel _Property;
        bool _CanEditPosition;
        bool _CanEditAttributes;
        private bool checkLength;
        #endregion

        #region Constructor
        public TagViewModel(MTag tag, ClipViewModel clip)
        {
            _Tag = tag;
            IsSelected = false;
            IsVisible = true;
            Clip = clip;
            CanEditAttributes = true;
            CanEditPosition = true;
            Background = new SolidColorBrush(Colors.Gray);
            this.MinimumFrameLength = 0;
            this.MaximumFrameLength = int.MaxValue;
            this.checkLength = true;

            if (this.StartAsFrame < 0.0f)
            {
                this.StartAsFrame = 0.0f;
            }

            if (this.EndAsFrame > this.Clip.MaximumFrameCount)
            {
                this.EndAsFrame = (float)this.Clip.MaximumFrameCount;
            }
        }
        #endregion // Constructor

        #region Properties
        public MTag Tag
        {
            get
            {
                return _Tag;
            }
        }

        public string Name
        {
            get
            {
                return _Tag.GetName();
            }
        }

        public float Start
        {
            get
            {
                return (float)_Tag.GetStart();
            }
            set
            {
                float currentValue = (float)_Tag.GetStart();
                if (currentValue == value)
                    return;

                this.Clip.DirtyState = true;
                float newValue = value;
                float newValueAsFrame = (float)(value * (this.Clip.TimeData.End - this.Clip.TimeData.Start));

                if (checkLength)
                {
                    int lengthConstraint = this.MaximumFrameLength - this.MinimumFrameLength;
                    if (lengthConstraint >= 0)
                    {
                        float lengthAfterChange = EndAsFrame - newValueAsFrame;
                        if (lengthAfterChange < this.MinimumFrameLength)
                        {
                            newValueAsFrame = this.EndAsFrame - this.MinimumFrameLength;
                        }
                        if (lengthAfterChange > this.MaximumFrameLength)
                        {
                            newValueAsFrame = this.EndAsFrame - this.MaximumFrameLength;
                        }
                    }
                    newValue = (float)(newValueAsFrame / (this.Clip.TimeData.End - this.Clip.TimeData.Start));
                }

                if (newValue <= End)
                {
                    _Tag.SetStart((float)newValue);
                }
                else
                {
                    _Tag.SetStart(End);
                }
                OnPropertyChanged("Start");
                OnPropertyChanged("StartAsFrame");
                OnPropertyChanged("WidthInPixels");
                OnPropertyChanged("LengthAsFrames");
                OnPropertyChanged("LengthInFrames");
                OnPropertyChanged("LengthInMilliseconds");
                OnPropertyChanged("Description");
            }
        }

        public float End
        {
            get
            {
                return (float)_Tag.GetEnd();
            }
            set
            {
                float currentValue = (float)_Tag.GetEnd();
                if (currentValue == value)
                    return;

                this.Clip.DirtyState = true;
                float newValue = value;
                float newValueAsFrame = (float)(value * (this.Clip.TimeData.End - this.Clip.TimeData.Start));

                if (checkLength)
                {
                    int lengthConstraint = this.MaximumFrameLength - this.MinimumFrameLength;
                    if (lengthConstraint >= 0)
                    {
                        float lengthAfterChange = newValueAsFrame - StartAsFrame;
                        if (lengthAfterChange < this.MinimumFrameLength)
                        {
                            newValueAsFrame = this.StartAsFrame + this.MinimumFrameLength;
                        }
                        if (lengthAfterChange > this.MaximumFrameLength)
                        {
                            newValueAsFrame = this.StartAsFrame + this.MaximumFrameLength;
                        }
                    }
                    newValue = (float)(newValueAsFrame / (this.Clip.TimeData.End - this.Clip.TimeData.Start));
                }

                if (newValue >= Start)
                {
                    _Tag.SetEnd((float)newValue);
                }
                else
                {
                    _Tag.SetEnd(Start);
                }
                OnPropertyChanged("End");
                OnPropertyChanged("EndAsFrame");
                OnPropertyChanged("WidthInPixels");
                OnPropertyChanged("LengthAsFrames");
                OnPropertyChanged("LengthInFrames");
                OnPropertyChanged("LengthInMilliseconds");
                OnPropertyChanged("Description");
            }
        }
        
        public float StartAsFrame
        {
            get
            {
                float value = (float)(Convert.ToDouble(_Tag.GetStart()) * (this.Clip.TimeData.End - this.Clip.TimeData.Start));
                return value;
            }
            set
            {
                float newStart = (float)(value / (this.Clip.TimeData.End - this.Clip.TimeData.Start));
                if( newStart <= End )
                {
                    Start = newStart;
                }
                else 
                {
                    Start = End;
                }
            }
        }

        public int StartAsMillisecond
        {
            get
            {
                double value = StartAsFrame;
                return (int)Math.Ceiling(value * 33.33);
            }
            set
            {
                float startFrame = value / 33.33f;
                float newStart = (float)(startFrame / (this.Clip.TimeData.End - this.Clip.TimeData.Start));
                if (newStart <= End)
                {
                    Start = newStart;
                }
                else
                {
                    Start = End;
                }
            }
        }

        public float EndAsFrame
        {
            get
            {
                float value = (float)(Convert.ToDouble(_Tag.GetEnd()) * (this.Clip.TimeData.End - this.Clip.TimeData.Start));
                return value;
            }
            set
            {
                float newEnd = (float)(value / (this.Clip.TimeData.End - this.Clip.TimeData.Start));
                if (newEnd >= Start)
                {
                    End = newEnd;
                }
                else
                {
                    End = Start;
                }
            }
        }

        public float EndAsMillisecond
        {
            get
            {
                double value = EndAsFrame;
                return (int)Math.Ceiling(value * 33.33);
            }
            set
            {
                float endFrame = value / 33.33f;
                float newEnd = (float)(endFrame / (this.Clip.TimeData.End - this.Clip.TimeData.Start));
                if (newEnd >= Start)
                {
                    End = newEnd;
                }
                else
                {
                    End = Start;
                }
            }
        }

        public int MinimumMillisecond
        {
            get { return (int)Math.Ceiling(this.Clip.TimeData.Start * 33.33); }
        }

        public int MaximumMillisecond
        {
            get { return (int)Math.Ceiling(this.Clip.TimeData.End * 33.33); }
        }

        public double ViewWidth
        {
            get
            {
                return _ViewWidth;
            }
            set
            {
                if (value != _ViewWidth)
                {
                    _ViewWidth = value;
                }
            }
        }

        public string Description
        {
            get
            {
                string desc = string.Format("{0:0.000}(fr {1:0.000})", Start, StartAsFrame);

                //  Change to milliseconds if needed
                if (this.Clip.ShowDurationInMilliseconds)
                {
                    desc = string.Format("{0:0.000}(ms {1:0.000})", Start, StartAsMillisecond);
                }

                foreach (var property in this.Property.PropertyAttributes)
                {
                    desc += string.Format("\n{0}: {1}", property.UIName, property.GetValueAsString());
                }

                return desc;
            }
        }

        // TODO: Make this a dependency property so I can use it in a trigger
        public bool IsSelected
        {
            get
            {
                return _IsSelected;
            }
            set
            {
                if (value == this._IsSelected)
                {
                    return;
                }

                if (_IsSelected == true && !value)
                {
                    if (this.Clip.SelectedTag == this)
                        this.Clip.SelectedTag = null;

                    this.Clip.SelectedTags.Remove(this);
                }
                if (_IsSelected == false && value)
                {
                    this.Clip.SelectedTags.Add(this);
                }

                _IsSelected = value;
                OnPropertyChanged("IsSelected");
                if (value && this.Clip != null)
                {
                    this.Clip.OnTagSelected();
                }
            }
        }

        private bool m_IsPositionOccupied;
        public bool IsPositionOccupied
        {
            get { return m_IsPositionOccupied; }
            set 
            { 
                m_IsPositionOccupied = value;
                OnPropertyChanged("IsPositionOccupied");
                OnPropertyChanged("PositionNumber");
            }
        }

        private int m_positionNumber;
        public int PositionNumber
        {
            get { return m_positionNumber; }
            set
            {
                m_positionNumber = value;
                OnPropertyChanged("PositionNumber");
                this.Clip.FirePropertyChanged("SelectedTagTrackTags");
            }
        }

        public bool IsVisible
        {
            get
            {
                return _IsVisble;
            }
            set
            {
                _IsVisble = value;
                OnPropertyChanged("IsVisible");
            }
        }

        public Brush Background
        {
            get
            {
                return _Background;
            }
            set
            {
                _Background = value;
                OnPropertyChanged("Background");
            }
        }

        public SolidColorBrush DefaultBackground
        {
            get;
            set;
        }

        public float InitialFrameLength
        {
            get;
            set;
        }

        public float LengthAsFrames
        {
            get { return EndAsFrame - StartAsFrame; }
            set
            {
                float newEnd = (float)((StartAsFrame + value) / (this.Clip.TimeData.End - this.Clip.TimeData.Start));
                if (newEnd >= Start)
                {
                    End = newEnd;
                }
                else
                {
                    End = Start;
                }

                OnPropertyChanged("LengthAsFrames");
            }
        }

        public float LengthAsMilliseconds
        {
            get 
            {
                double lengthInMillisecondsRounded = Math.Ceiling(LengthAsFrames * 33.33);
                return (int)lengthInMillisecondsRounded;
            }
            set
            {
                float endFrame = (StartAsMillisecond + value) / 33.33f;
                float newEnd = (float)(endFrame / (this.Clip.TimeData.End - this.Clip.TimeData.Start));
                if (newEnd >= Start)
                {
                    End = newEnd;
                }
                else
                {
                    End = Start;
                }

                OnPropertyChanged("LengthAsMilliseconds");
            }
        }

        public string LengthInFrames
        {
            get
            {
                double lengthInFramesRounded = Math.Round(LengthAsFrames, 2);
                return lengthInFramesRounded.ToString();
            } 
        }

        public string LengthInMilliseconds
        {
            get
            {
                double lengthInMillisecondsRounded = Math.Ceiling(LengthAsFrames * 33.33);
                return (lengthInMillisecondsRounded).ToString();
            }
        }

        public double WidthInPixels
        {
            get
            {
                return GlobalWidthInPixels * (End - Start);
            }
           
        }

        public int MinimumFrameLength
        {
            get { return minimumFrameLength; }
            set
            {
                if (value > this.Clip.TimeData.End)
                    return;

                minimumFrameLength = value;
                if (LengthAsFrames < minimumFrameLength)
                {
                    double newEndAsFrame = this.StartAsFrame + this.minimumFrameLength;
                    double newStartAsFrame = this.StartAsFrame;
                    while (newEndAsFrame > this.Clip.TimeData.End)
                    {
                        newEndAsFrame--;
                        newStartAsFrame--;
                    }

                    float newEndValue = (float)(newEndAsFrame / (this.Clip.TimeData.End - this.Clip.TimeData.Start));
                    float newStartValue = (float)(newStartAsFrame / (this.Clip.TimeData.End - this.Clip.TimeData.Start));

                    this.checkLength = false;
                    this.Start = newStartValue;
                    this.End = newEndValue;
                    this.checkLength = true;
                }
            }
        }
        private int minimumFrameLength;

        public int MaximumFrameLength
        {
            get { return maximumFrameLength; }
            set
            {
                maximumFrameLength = value;
                double lengthInFramesValue = EndAsFrame - StartAsFrame;
                if (lengthInFramesValue > maximumFrameLength)
                {
                    double newEndAsFrame = this.StartAsFrame + this.maximumFrameLength;
                    float newValue = (float)(newEndAsFrame / (this.Clip.TimeData.End - this.Clip.TimeData.Start));
                    this.checkLength = false;
                    this.End = newValue;
                    this.checkLength = true;
                }
            }
        }
        private int maximumFrameLength;

        // TODO: Move this into the ClipEventModel
        public double GlobalWidthInPixels
        {
            get
            {
                return _GlobalWidthInPixels;
            }
            set
            {
                _GlobalWidthInPixels = value;
                 OnPropertyChanged("WidthInPixels");
            }
        }

        public PropertyViewModel Property
        {
            get
            {
                return _Property;
            }
            set
            {
                if (value != _Property)
                {
                    _Property = value;
                    if (_Property != null)
                    {
                        foreach (var item in _Property.PropertyAttributes)
                        {
                            FloatViewModel floatItem = item as FloatViewModel;
                            if (floatItem != null)
                            {
                                if (floatItem.IsBlendOut)
                                {
                                    floatItem.ValueChanged += this.OnBlendOutValueChanged;
                                }
                                else if (floatItem.IsBlendIn)
                                {
                                    floatItem.ValueChanged += this.OnBlendInValueChanged;
                                }
                            }

                            item.ValueChanged += new EventHandler(PropertyValueChanged);
                        }
                    }
                }
            }
        }

        public float BlendIn
        {
            get
            {
                if (_Property != null)
                {
                    foreach (PropertyAttributeBaseViewModel item in _Property.PropertyAttributes)
                    {
                        FloatViewModel floatAttribute = item as FloatViewModel;
                        if (floatAttribute == null || !floatAttribute.IsBlendIn)
                        {
                            continue;
                        }

                        return floatAttribute.Value;
                    }
                }

                return 0.0f;
            }
        }

        public float BlendOut
        {
            get
            {
                if (_Property != null)
                {
                    foreach (PropertyAttributeBaseViewModel item in _Property.PropertyAttributes)
                    {
                        FloatViewModel floatAttribute = item as FloatViewModel;
                        if (floatAttribute == null || !floatAttribute.IsBlendOut)
                        {
                            continue;
                        }

                        return floatAttribute.Value;
                    }
                }

                return 0.0f;
            }
        }

        public bool IsDurationTag
        {
            get
            {
                bool hasBlendIn = false;
                bool hasBlendOut = false;
                if (_Property != null)
                {
                    foreach (PropertyAttributeBaseViewModel item in _Property.PropertyAttributes)
                    {
                        FloatViewModel floatAttribute = item as FloatViewModel;
                        if (floatAttribute == null)
                        {
                            continue;
                        }

                        if (hasBlendIn == false)
                        {
                            hasBlendIn = floatAttribute.IsBlendIn;
                        }

                        if (hasBlendOut == false)
                        {
                            hasBlendOut = floatAttribute.IsBlendOut;
                        }
                    }
                }

                return hasBlendIn && hasBlendOut;
            }
        }

        public List<ClipPropertyDefinition.PropertyGroup> ColourChangers
        {
            get;
            set;
        }

        public void FirePropertyChanged(string property)
        {
            OnPropertyChanged(property);
        }

        private void PropertyValueChanged(object sender, EventArgs e)
        {
            DetermineColourFromPropertyValues();
            OnPropertyChanged("Description");
        }

        private void OnBlendOutValueChanged(object sender, EventArgs e)
        {
            OnPropertyChanged("BlendOut");
        }

        private void OnBlendInValueChanged(object sender, EventArgs e)
        {
            OnPropertyChanged("BlendIn");
        }

        public ClipViewModel Clip
        {
            get { return clip; }
            set { clip = value; }
        }

        public bool CanEditPosition
        {
            get
            {
                return _CanEditPosition;
            }
            set
            {
                _CanEditPosition = value;
                OnPropertyChanged("CanEditPosition");
            }
        }

        public bool CanEditAttributes
        {
            get
            {
                return _CanEditAttributes;
            }
            set
            {
                _CanEditAttributes = value;
                OnPropertyChanged("CanEditAttributes");
            }
        }

        public void DetermineColourFromPropertyValues()
        {
            if (ColourChangers == null || ColourChangers.Count == 0)
                return;

            SolidColorBrush newBackground = this.DefaultBackground;
            foreach (var changer in this.ColourChangers)
            {
                bool passed = true;
                foreach (var propertyValue in changer.PropertyValues)
                {
                    string name = propertyValue.Key;
                    string value = propertyValue.Value;

                    if (!TestProperty(name, value))
                    {
                        passed = false;
                        break;
                    }
                }
                if (passed)
                {
                    newBackground = new SolidColorBrush(changer.Colour.Value);
                    break;
                }
            }

            this.Background = newBackground;
        }

        private bool TestProperty(string name, string value)
        {
            foreach (var property in Property.PropertyAttributes)
            {
                if (property.Name != name)
                    continue;

                if (property.TestString(value))
                    return true;

                return false;
            }
            return false;
        }

        public void MoveToNewStartPosition(float newPosition)
        {
            if (newPosition < 0.0)
                return;

            float currentLength = End - Start;
            if (newPosition < this.Start)
            {
                this.checkLength = false;
                this.Start = newPosition;
                float newEnd = this.Start + currentLength;
                this.End = newEnd <= 1.0 ? newEnd : 1.0f;
                this.checkLength = true;
            }
            else
            {
                float newEnd = newPosition + currentLength;
                newEnd = newEnd <= 1.0 ? newEnd : 1.0f;

                this.checkLength = false;
                this.End = newEnd;
                this.checkLength = true;

                this.Start = newPosition;
            }
        }

        public void MoveToNewFramePosition(float newPosition)
        {
            if (newPosition < 0.0f)
            {
                newPosition = 0.0f;
            }

            if (newPosition > (float)Clip.MaximumFrameCount)
            {
                newPosition = (float)Clip.MaximumFrameCount;
            }

            float currentLength = EndAsFrame - StartAsFrame;
            if (newPosition < this.StartAsFrame)
            {
                this.checkLength = false;
                this.StartAsFrame = newPosition;
                float newEnd = this.StartAsFrame + currentLength;
                this.EndAsFrame = newEnd <= (float)Clip.MaximumFrameCount ? newEnd : (float)Clip.MaximumFrameCount;
                this.checkLength = true;
            }
            else
            {
                float newEnd = newPosition + currentLength;
                newEnd = newEnd <= (float)Clip.MaximumFrameCount ? newEnd : (float)Clip.MaximumFrameCount;

                this.checkLength = false;
                this.EndAsFrame = newEnd;
                this.checkLength = true;

                this.StartAsFrame = newPosition;
            }

            if (this.StartAsFrame > this.Clip.LastVisibleFrame)
            {
                double oldFirstFrame = this.Clip.FirstVisibleFrame;
                while (this.StartAsFrame > this.Clip.LastVisibleFrame)
                {
                    double newFirstFrame = this.Clip.FirstVisibleFrame + 1;
                    //double visibleFrames = this.Clip.TagTrackVisiblePixelWidth / this.Clip.PixelsPerFrame;
                    //double newLastFrame = newFirstFrame + visibleFrames;
                    //if (newLastFrame > this.Clip.MaximumFrameCount)
                    //{
                    //    newFirstFrame = this.Clip.MaximumFrameCount - visibleFrames;
                    //}

                    this.Clip.FirstVisibleFrame = newFirstFrame;
                }

                double firstFrameDiff = this.Clip.FirstVisibleFrame - oldFirstFrame;
                this.Clip.CurrentFramePosition += firstFrameDiff;
            }
            else if (this.StartAsFrame < this.Clip.FirstVisibleFrame)
            {
                double oldFirstFrame = this.Clip.FirstVisibleFrame;
                while (this.StartAsFrame < this.Clip.FirstVisibleFrame)
                {
                    double newFirstFrame = this.Clip.FirstVisibleFrame - 1;
                    if (newFirstFrame < 0.0)
                    {
                        newFirstFrame = 0.0;
                    }

                    this.Clip.FirstVisibleFrame = newFirstFrame;
                }

                double firstFrameDiff = this.Clip.FirstVisibleFrame - oldFirstFrame;
                this.Clip.CurrentFramePosition += firstFrameDiff;
            }
        }
        #endregion

        #region Methods

        //void OnMouseLeftButtonUp(MouseButtonEventArgs e)
        //{

        //}
        #endregion
    }
}
