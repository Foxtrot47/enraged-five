﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using RSG.ManagedRage.ClipAnimation;

using RSG.AnimationMetadata;

using RSG.AnimClipEditor.ViewModel;
using RSG.AnimClipEditor.Util;
using RSG.AnimClipEditor;

namespace RSG.AnimClipEditor.View
{
    /// <summary>
    /// Interaction logic for TagTab.xaml
    /// </summary>
    public partial class TagTab : ContentControl 
    {
        public TagTab()
        {
            InitializeComponent();
             
        }

        protected override void OnPreviewMouseDown(MouseButtonEventArgs e)
        {
            base.OnPreviewMouseDown(e);
        }
        
        protected override void OnMouseDoubleClick(MouseButtonEventArgs e)
        {
            base.OnMouseDoubleClick(e);
            //ClipEventViewModel clipevent = this.DataContext as ClipEventViewModel;
            //if (clipevent.IsExpanded)
            //{
            //    Point pos = e.GetPosition(this);
            //    double globalWidthInPixels = this.ActualWidth;

            //    float start = (float)(pos.X / globalWidthInPixels);
            //    if (CoreManager.ClipViewModel.SnapToFrame)
            //    {
                    
            //    }

            //    float end = start;
            //    TagViewModel tvm = EventFactory.CreateTagViewModel(clipevent.Type, start, end, CoreManager.ClipViewModel);
            //    if (tvm != null)
            //    {
            //        tvm.GlobalWidthInPixels = globalWidthInPixels;
            //        clipevent.Tags.Add(tvm);
            //    }
            //    else
            //    {
            //        MessageBox.Show("No description for type: " + clipevent.Type);
            //    }
            //}
            //e.Handled = false;
            
        }
    }
}
