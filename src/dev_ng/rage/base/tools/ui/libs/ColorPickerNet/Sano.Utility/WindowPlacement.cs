using System;

namespace Sano.Utility.NativeMethods {

	public struct WindowPlacement {

		public int length;
		public int flags;
		public int showCmd;
		public NativePoint ptMinPosition;
		public NativePoint ptMaxPosition;
		public NativeRect rcNormalPosition;

	} // WindowPlacement

} // Sano.Utility.NativeMethods