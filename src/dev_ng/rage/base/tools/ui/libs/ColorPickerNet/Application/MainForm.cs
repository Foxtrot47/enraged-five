using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Diagnostics;
using System.Threading;
using System.Runtime.InteropServices;

using Sano.PersonalProjects.ColorPicker.Controls;
using Sano.Utility;
using Sano.Utility.NativeMethods;

namespace Sano.PersonalProjects.ColorPicker.Main {
	/// <summary>
	/// Summary description for MainForm.
	/// </summary>
	public class MainForm : Form {

		private ColorPanel colorPanel1;
		private MenuItem menuItem1;
		private MenuItem aboutMenuItem;
		private MainMenu mainMenu;
		private MenuItem miHomepage;
		private System.Windows.Forms.NotifyIcon notifyIcon;
		private System.Windows.Forms.ContextMenu notifyIconContextMenu;
		private System.Windows.Forms.MenuItem cmExit;
		private System.Windows.Forms.MenuItem openApplicationMenuItem;
		private System.Windows.Forms.MenuItem menuItem4;
		private System.Windows.Forms.MenuItem mnuClose;
		private System.Windows.Forms.MenuItem menuItem5;
		private System.Windows.Forms.MenuItem minimizeToTrayMenuItem;
		private System.Windows.Forms.MenuItem saveSettingsOnApplicationCloseMenuItem;
		private System.Windows.Forms.MenuItem menuItem2;
		private System.Windows.Forms.MenuItem menuItem3;
		private System.Windows.Forms.MenuItem miReportABug;
		private System.Windows.Forms.MenuItem menuItem6;
		private System.ComponentModel.IContainer components;

		public MainForm() {
			InitializeComponent();
		}

		#region Dispose
		
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing ) {
			if( disposing ) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#endregion Dispose

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			this.components = new System.ComponentModel.Container();
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(MainForm));
			this.mainMenu = new System.Windows.Forms.MainMenu();
			this.menuItem4 = new System.Windows.Forms.MenuItem();
			this.mnuClose = new System.Windows.Forms.MenuItem();
			this.menuItem5 = new System.Windows.Forms.MenuItem();
			this.minimizeToTrayMenuItem = new System.Windows.Forms.MenuItem();
			this.saveSettingsOnApplicationCloseMenuItem = new System.Windows.Forms.MenuItem();
			this.menuItem1 = new System.Windows.Forms.MenuItem();
			this.aboutMenuItem = new System.Windows.Forms.MenuItem();
			this.miHomepage = new System.Windows.Forms.MenuItem();
			this.miReportABug = new System.Windows.Forms.MenuItem();
			this.notifyIcon = new System.Windows.Forms.NotifyIcon(this.components);
			this.notifyIconContextMenu = new System.Windows.Forms.ContextMenu();
			this.openApplicationMenuItem = new System.Windows.Forms.MenuItem();
			this.menuItem3 = new System.Windows.Forms.MenuItem();
			this.menuItem2 = new System.Windows.Forms.MenuItem();
			this.cmExit = new System.Windows.Forms.MenuItem();
			this.colorPanel1 = new Sano.PersonalProjects.ColorPicker.Controls.ColorPanel();
			this.menuItem6 = new System.Windows.Forms.MenuItem();
			this.SuspendLayout();
			// 
			// mainMenu
			// 
			this.mainMenu.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					 this.menuItem4,
																					 this.menuItem5,
																					 this.menuItem1});
			// 
			// menuItem4
			// 
			this.menuItem4.Index = 0;
			this.menuItem4.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					  this.mnuClose,
																					  this.menuItem6});
			this.menuItem4.Text = "&File";
			// 
			// mnuClose
			// 
			this.mnuClose.Index = 0;
			this.mnuClose.Text = "Close";
			this.mnuClose.Click += new System.EventHandler(this.mnuClose_Click);
			// 
			// menuItem5
			// 
			this.menuItem5.Index = 1;
			this.menuItem5.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					  this.minimizeToTrayMenuItem,
																					  this.saveSettingsOnApplicationCloseMenuItem});
			this.menuItem5.Text = "&Settings";
			// 
			// minimizeToTrayMenuItem
			// 
			this.minimizeToTrayMenuItem.Index = 0;
			this.minimizeToTrayMenuItem.Text = "Minimize application to system tray";
			this.minimizeToTrayMenuItem.Click += new System.EventHandler(this.minimizeToTrayMenuItem_Click);
			// 
			// saveSettingsOnApplicationCloseMenuItem
			// 
			this.saveSettingsOnApplicationCloseMenuItem.Index = 1;
			this.saveSettingsOnApplicationCloseMenuItem.Text = "Save settings on application exit";
			this.saveSettingsOnApplicationCloseMenuItem.Click += new System.EventHandler(this.saveSettingsOnApplicationCloseMenuItem_Click);
			// 
			// menuItem1
			// 
			this.menuItem1.Index = 2;
			this.menuItem1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					  this.aboutMenuItem,
																					  this.miHomepage,
																					  this.miReportABug});
			this.menuItem1.Text = "&Help";
			// 
			// aboutMenuItem
			// 
			this.aboutMenuItem.Index = 0;
			this.aboutMenuItem.Text = "About ColorPicker.NET";
			this.aboutMenuItem.Click += new System.EventHandler(this.aboutMenuItem_Click);
			// 
			// miHomepage
			// 
			this.miHomepage.Index = 1;
			this.miHomepage.Text = "Homepage";
			this.miHomepage.Click += new System.EventHandler(this.miHomepage_Click);
			// 
			// miReportABug
			// 
			this.miReportABug.Index = 2;
			this.miReportABug.Text = "Report a Bug";
			this.miReportABug.Click += new System.EventHandler(this.miReportABug_Click);
			// 
			// notifyIcon
			// 
			this.notifyIcon.ContextMenu = this.notifyIconContextMenu;
			this.notifyIcon.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon.Icon")));
			this.notifyIcon.Text = "ColorPicker.NET";
			this.notifyIcon.DoubleClick += new System.EventHandler(this.notifyIcon_DoubleClick);
			// 
			// notifyIconContextMenu
			// 
			this.notifyIconContextMenu.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																								  this.openApplicationMenuItem,
																								  this.menuItem3,
																								  this.menuItem2,
																								  this.cmExit});
			this.notifyIconContextMenu.Popup += new System.EventHandler(this.notifyIconContextMenu_Popup);
			// 
			// openApplicationMenuItem
			// 
			this.openApplicationMenuItem.Index = 0;
			this.openApplicationMenuItem.Text = "Open ColorPicker.NET";
			this.openApplicationMenuItem.Click += new System.EventHandler(this.openApplicationMenuItem_Click);
			// 
			// menuItem3
			// 
			this.menuItem3.Index = 1;
			this.menuItem3.Text = "-";
			// 
			// menuItem2
			// 
			this.menuItem2.Index = 2;
			this.menuItem2.Text = "Homepage";
			// 
			// cmExit
			// 
			this.cmExit.Index = 3;
			this.cmExit.Text = "Exit";
			this.cmExit.Click += new System.EventHandler(this.cmExit_Click);
			// 
			// colorPanel1
			// 
			this.colorPanel1.AllowDrop = true;
			this.colorPanel1.CurrentColor = System.Drawing.Color.FromArgb(((System.Byte)(0)), ((System.Byte)(0)), ((System.Byte)(0)));
			this.colorPanel1.Location = new System.Drawing.Point(2, 14);
			this.colorPanel1.Name = "colorPanel1";
			this.colorPanel1.Size = new System.Drawing.Size(572, 272);
			this.colorPanel1.TabIndex = 0;
			this.colorPanel1.UseSwatches = true;
			// 
			// menuItem6
			// 
			this.menuItem6.Index = 1;
			this.menuItem6.Text = "TestThingy";
			this.menuItem6.Click += new System.EventHandler(this.menuItem6_Click);
			// 
			// MainForm
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(576, 297);
			this.Controls.Add(this.colorPanel1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximizeBox = false;
			this.Menu = this.mainMenu;
			this.Name = "MainForm";
			this.Text = "ColorPicker.NET";
			this.ResumeLayout(false);

		}
		#endregion

		#region Program entry point

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() {
			
			bool createdNew;

			Mutex m = new Mutex( true, "ColorPicker.NET", out createdNew );

			if ( !createdNew ) {

				// the window class name was found using spy++
				IntPtr hWnd = User32.FindWindow( "WindowsForms10.Window.8.app3", "ColorPicker.NET" );

				if ( hWnd != IntPtr.Zero ) {

					WindowPlacement placement = new WindowPlacement();
					placement.length = Marshal.SizeOf( placement );

					User32.GetWindowPlacement( hWnd, ref placement );

					placement.showCmd = ShowWindowMessages.SW_RESTORE;
					
					User32.SetWindowPlacement(hWnd, ref placement);
					User32.SetForegroundWindow(hWnd); 

				}

			} else {

				Application.Run(new MainForm());
				GC.KeepAlive( m );

			}
			
		}
		
		#endregion Program entry point

		private void aboutMenuItem_Click(object sender, System.EventArgs e) {
		
			using ( AboutForm af = new AboutForm() ) {
				af.ShowDialog( this );
			}

		}

		private void miHomepage_Click(object sender, System.EventArgs e) {
			LoadHomepage();
		}

		private static void LoadHomepage() {

			if ( Internet.IsMachineConnectedToInternet() ) {
				System.Diagnostics.Process.Start( "http://sano.dotnetgeeks.net/colorpicker" );		
			} else {
				MessageBox.Show( "Your machine is not connected to the Internet.", "Connection Error", MessageBoxButtons.OK );
			}

		}

		protected override void OnResize(EventArgs e) {
		
			if ( this.WindowState == FormWindowState.Minimized && minimizeToTrayMenuItem.Checked ) {
				this.Hide();
			}

		}

		private void notifyIcon_DoubleClick(object sender, System.EventArgs e) {

			if ( this.WindowState == FormWindowState.Minimized ) {

				this.Show();
				this.WindowState = FormWindowState.Normal;
			
			} else {
				this.BringToFront();
			}
		
		}

		private void openApplicationMenuItem_Click(object sender, System.EventArgs e) {
			notifyIcon_DoubleClick( openApplicationMenuItem, e );
		}

		private void cmExit_Click(object sender, System.EventArgs e) {
			this.Close();
		}

		private void mnuClose_Click(object sender, System.EventArgs e) {
			this.Close();
		}

		/// <summary>
		/// Handles the Click event raised by the minimizeToTrayMenuItem object 
		/// and updates the appropriate application settings.
		/// </summary>
		/// <param name="sender">The object that raised the event.</param>
		/// <param name="e">An EventArgs containing the event data.</param>

		private void minimizeToTrayMenuItem_Click(object sender, System.EventArgs e) {
		
			MenuItem menuItem = ( MenuItem ) sender;

			if ( !menuItem.Checked ) {

				this.notifyIcon.Visible = true;
				menuItem.Checked = true;
			
			} else {
			
				this.notifyIcon.Visible = false;
				menuItem.Checked = false;
			
			}

			ApplicationConfiguration.Settings.MinimizeApplicationToSystemTray = menuItem.Checked;

		}

		/// <summary>
		/// Handles the Click event raised by the 
		/// saveSettingsOnApplicationCloseMenuItem object and updates the 
		/// appropriate application settings.
		/// </summary>
		/// <param name="sender">The object that raised the event.</param>
		/// <param name="e">An EventArgs containing the event data.</param>

		private void saveSettingsOnApplicationCloseMenuItem_Click(object sender, System.EventArgs e) {
			
			MenuItem menuItem = ( MenuItem ) sender;

			if ( !menuItem.Checked ) {
				
				menuItem.Checked = true;
				ApplicationConfiguration.Settings.SaveSettingsOnApplicationClose = true;

			} else {

				menuItem.Checked = false;
				ApplicationConfiguration.Settings.SaveSettingsOnApplicationClose = false;

			}

		}

		/// <summary>
		/// Overrides the OnLoad method to restore previously saved application
		/// configuration settings.
		/// </summary>
		/// <param name="e">An EventArgs containing the event data.</param>
		
		protected override void OnLoad(EventArgs e) {
			
			ApplicationConfiguration.Settings.ReadSettingsFromFile();

			if ( ApplicationConfiguration.Settings.SaveSettingsOnApplicationClose ) {

				if ( ApplicationConfiguration.Settings.MinimizeApplicationToSystemTray ) {

					this.notifyIcon.Visible = true;
					minimizeToTrayMenuItem.Checked = true;

				}

				if ( ApplicationConfiguration.Settings.SaveSettingsOnApplicationClose ) {
					saveSettingsOnApplicationCloseMenuItem.Checked = true;
				}

			}

			// hides menu items that spawn browser windows if the machine is
			// not connected to the internet.
			if ( !Internet.IsMachineConnectedToInternet() ) {
				
				miReportABug.Enabled = false;
				miHomepage.Enabled = false;
				
			}			

			base.OnLoad (e);

		}

		/// <summary>
		/// Overrides the OnClosing method to save the application settings if 
		/// the user has chosen this option.
		/// </summary>
		/// <param name="e">A CancelEventArgs containing the event data.</param>

		protected override void OnClosing(CancelEventArgs e) {
			
			ApplicationConfiguration.Settings.WriteSettingsToFile();
			
			base.OnClosing (e);

		}

		private void notifyIconContextMenu_Popup(object sender, System.EventArgs e) {

			if ( this.WindowState != FormWindowState.Minimized ) {
				openApplicationMenuItem.Text = "Show ColorPicker.NET";
			} else {
				openApplicationMenuItem.Text = "Open ColorPicker.NET";
			}
		
		}

		private void miReportABug_Click(object sender, System.EventArgs e) {
		
			if ( Internet.IsMachineConnectedToInternet() ) {
				System.Diagnostics.Process.Start( @"http://www.gotdotnet.com/workspaces/bugtracker/home.aspx?id=489fa843-1954-4fe4-8fe3-cb58fb758850" );		
			} else {
				MessageBox.Show( "Your machine is not connected to the Internet.", "Connection Error", MessageBoxButtons.OK );
			}
			
		}

		private void menuItem6_Click(object sender, System.EventArgs e)
		{
			ColorPickerDialog dlg = new ColorPickerDialog();
			dlg.CurrentColor = colorPanel1.CurrentColor;
			dlg.ColorChanged += new ColorSelectedEventHandler(OnThingyChanged);
			if (dlg.ShowDialog() == DialogResult.OK) {
				MessageBox.Show("Woo!");
			}
		}

		private void OnThingyChanged(object sender, ColorSelectedEventArgs e) {
			colorPanel1.CurrentColor = e.Color;
		}

	} // MainForm

} // Sano.PersonalProjects.ColorPicker.Main
