
using System;
using System.Windows.Forms;

namespace Sano.PersonalProjects.ColorPicker.Main {

	public class OptionsForm : Form {

		// controls
		private GroupBox groupBox1;
		private CheckBox chkMinimizeToSystemTray;
		private Button btnOk;
		private Button btnCancel;
		private CheckBox chkSaveValuesOnAppClose;

		public OptionsForm() {
			
			InitializeComponent();

		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>

		private void InitializeComponent() {
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.chkSaveValuesOnAppClose = new System.Windows.Forms.CheckBox();
			this.chkMinimizeToSystemTray = new System.Windows.Forms.CheckBox();
			this.btnOk = new System.Windows.Forms.Button();
			this.btnCancel = new System.Windows.Forms.Button();
			this.groupBox1.SuspendLayout();
			this.SuspendLayout();
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.chkSaveValuesOnAppClose);
			this.groupBox1.Controls.Add(this.chkMinimizeToSystemTray);
			this.groupBox1.Location = new System.Drawing.Point(17, 19);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(248, 85);
			this.groupBox1.TabIndex = 2;
			this.groupBox1.TabStop = false;
			// 
			// chkSaveValuesOnAppClose
			// 
			this.chkSaveValuesOnAppClose.Location = new System.Drawing.Point(16, 48);
			this.chkSaveValuesOnAppClose.Name = "chkSaveValuesOnAppClose";
			this.chkSaveValuesOnAppClose.Size = new System.Drawing.Size(208, 21);
			this.chkSaveValuesOnAppClose.TabIndex = 3;
			this.chkSaveValuesOnAppClose.Text = "Save values when application closes";
			// 
			// chkMinimizeToSystemTray
			// 
			this.chkMinimizeToSystemTray.Location = new System.Drawing.Point(16, 16);
			this.chkMinimizeToSystemTray.Name = "chkMinimizeToSystemTray";
			this.chkMinimizeToSystemTray.Size = new System.Drawing.Size(208, 24);
			this.chkMinimizeToSystemTray.TabIndex = 2;
			this.chkMinimizeToSystemTray.Text = "Minimize application to system tray";
			// 
			// btnOk
			// 
			this.btnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.btnOk.Location = new System.Drawing.Point(112, 120);
			this.btnOk.Name = "btnOk";
			this.btnOk.TabIndex = 3;
			this.btnOk.Text = "OK";
			this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
			// 
			// btnCancel
			// 
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.Location = new System.Drawing.Point(190, 120);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.TabIndex = 4;
			this.btnCancel.Text = "Cancel";
			// 
			// OptionsForm
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(282, 162);
			this.Controls.Add(this.btnOk);
			this.Controls.Add(this.btnCancel);
			this.Controls.Add(this.groupBox1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
			this.Name = "OptionsForm";
			this.Text = "Options";
			this.groupBox1.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion Windows Form Designer generated code

		private void btnOk_Click(object sender, System.EventArgs e) {
		
			ApplicationConfiguration.Settings.MinimizeApplicationToSystemTray = chkMinimizeToSystemTray.Checked;
			ApplicationConfiguration.Settings.SaveSettingsOnApplicationClose = chkSaveValuesOnAppClose.Checked;

		}

	} // OptionsForm

} // Sano.PersonalProjects.ColorPicker.Main