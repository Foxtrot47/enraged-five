
using System;
using System.Drawing;

namespace Sano.PersonalProjects.ColorPicker.Controls {
	
	/// <summary>
	/// This class is responsible for storing all of the ColorPanel related
	/// user preferences.
	/// </summary>

	[Serializable]
	public class ColorPanelSettings {
		
		// private data member
		private string m_activeColorBox;
		private Color m_topColorBoxColor;
		private Color m_bottomColorBoxColor;
		private string m_selectedColorSpaceComponent;
		
		/// <summary>
		/// Gets or sets the active color box.
		/// </summary>

		public string ActiveColorBox {
			get { return m_activeColorBox; }
			set { m_activeColorBox = value; }
		}
	
		/// <summary>
		/// Gets or sets the stored color of the top color box.
		/// </summary>
		
		public Color TopColorBoxColor {
			get { return m_topColorBoxColor; }
			set { m_topColorBoxColor = value; }
		}
				
		/// <summary>
		/// Gets or sets the stored color of the bottom color box.
		/// </summary>

		public Color BottomColorBoxColor {
			get { return m_bottomColorBoxColor; }
			set { m_bottomColorBoxColor = value; }
		}
		
		/// <summary>
		/// Gets or sets the display character of the selected color space
		/// component.
		/// </summary>

		public string SelectedColorSpaceComponent {
			get { return m_selectedColorSpaceComponent; }
			set { m_selectedColorSpaceComponent = value; }
		}

	} // ColorPanelSettings

} // Sano.PersonalProjects.ColorPicker.Controls