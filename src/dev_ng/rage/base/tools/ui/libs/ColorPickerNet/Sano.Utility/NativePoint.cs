
using System;

namespace Sano.Utility.NativeMethods {

	public struct NativePoint {
	
		public int x;
		public int y;
	
	} // NativePoint

} // Sano.Utility.NativeMethods