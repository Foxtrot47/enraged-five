
using System;

namespace Sano.PersonalProjects.ColorPicker.Controls {

	/// <summary>
	/// This is a helper class that evaluates the validity of structures that
	/// are expected to be annotated with the ColorSpaceStructureAttribute.
	/// As of right now, only HSB and RGB have been bestowned with this honor.
	/// </summary>

	internal sealed class ColorSpaceStructureEvaluator {
		 
		/// <summary>
		/// Private constructor to ensure that the compiler does not 
		/// automatically generate a public constructor.
		/// </summary>

		private ColorSpaceStructureEvaluator() { }

		/// <summary>
		/// Evaluates the structure parameter to see if it is a HSB structure.
		/// </summary>
		/// <param name="structure">A purported structure object that is to be evaluated.</param>
		/// <returns>True if structure is a HSB structure; otherwise, false.</returns>

		internal static bool IsHsb( object structure ) {

			bool isHsb = false;

			ColorSpaceStructureAttribute css = GetColorSpaceStructureAttribute( structure );
			
			if ( css != null && css.ColorSpace == ColorSpaces.HueSaturationBrightness ) {
				isHsb = true;
			}

			return isHsb;

		}

		/// <summary>
		/// Evaluates the structure parameter to see if it is a RGB structure.
		/// </summary>
		/// <param name="structure">A purported structure object that is to be evaluated.</param>
		/// <returns>True if structure is a RGB structure; otherwise, false.</returns>

		internal static bool IsRgb( object structure ) {

			bool isRgb = false;

			ColorSpaceStructureAttribute css = GetColorSpaceStructureAttribute( structure );
			
			if ( css != null && css.ColorSpace == ColorSpaces.RedGreenBlue ) {
				isRgb = true;
			}

			return isRgb;

		}

		/// <summary>
		/// Checks to see if the structure has been annotated with the 
		/// ColorSpaceStructureAttribute.
		/// </summary>
		/// <param name="structure">The structure that is to be checked.</param>
		/// <returns>True if the structure is annotatedw ith the 
		/// ColorSpaceStructureAttribute; otherwise, false.</returns>

		private static ColorSpaceStructureAttribute GetColorSpaceStructureAttribute( object structure ) {

			ColorSpaceStructureAttribute css = null;

			foreach( Attribute attribute in structure.GetType().GetCustomAttributes( true )  ) {

				css = attribute as ColorSpaceStructureAttribute;
				
				if ( css != null ) {

					// no need to check for other attributes
					break;
				
				}
			
			}

			return css;

		}

	} // ColorSpaceStructureEvaluator

} // Sano.PersonalProjects.ColorPicker.Controls