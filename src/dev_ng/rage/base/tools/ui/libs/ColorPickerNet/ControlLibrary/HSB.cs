
using System;

namespace Sano.PersonalProjects.ColorPicker.Controls {

	// This class stores all relevant HSB data.

	[ColorSpaceStructure( ColorSpaces.HueSaturationBrightness )]
	internal sealed class HSB {
		
		// private data fields
		private int m_hue;
		private int m_saturation;
		private int m_brightness;

		/// <summary>
		/// Gets or sets the hue coordinate.
		/// </summary>

		internal int Hue {
			get { return m_hue; }
			set { m_hue = value; }
		}

		/// <summary>
		/// Gets or sets the saturation coordinate.
		/// </summary>

		internal int Saturation {
			get { return m_saturation; }
			set { m_saturation = value; }
		}

		/// <summary>
		/// Gets or sets the brightness coordinate.
		/// </summary>

		internal int Brightness {
			get { return m_brightness; }
			set { m_brightness = value; }
		}

		/// <summary>
		/// Constructor. Provides initial values for the coordinates in the
		/// HSB color space.
		/// </summary>
		/// <param name="hue">The hue coordinate.</param>
		/// <param name="saturation">The saturation coordinate.</param>
		/// <param name="brightness">The brightness coordinate.</param>

		internal HSB( int hue, int saturation, int brightness ) {
			
			m_hue = hue;
			m_saturation = saturation;
			m_brightness = brightness;

		}

		/// <summary>
		/// Returns a string representation of the current object.
		/// </summary>
		/// <returns>A string representation of the current object.</returns>

		public override string ToString() {
			return String.Format( "Hue: {0}; Saturation: {1}; Brightness: {2} ", Hue, Saturation, Brightness );
		}

		/// <summary>
		/// Determines whether or not two objects are equal.
		/// </summary>
		/// <param name="obj">The object that is to be compared to the current 
		/// object.</param>
		/// <returns>True if the two objects are equal; otherwise false.</returns>

		public override bool Equals( object obj ) {
			
			bool equal = false;
			HSB hsb = obj as HSB;

			if ( hsb != null ) {
				
				if ( this.Hue == hsb.Hue && this.Saturation == hsb.Saturation && this.Brightness == hsb.Brightness ) {
					equal = true;
				}
			
			}

			return equal;

		}

		/// <summary>
		/// Returns the hash code.
		/// </summary>
		/// <returns>The hash code.</returns>

		public override int GetHashCode() {
			return base.GetHashCode ();
		}

	} // HSB

} // Sano.PersonalProjects.ColorPicker.Controls
