using System;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Data;
using System.Threading;
using System.Windows.Forms;


// TODO: use Offset() to offset point coordinates instead of subtracting SAMPLE_CIRCLE_RADIUS.

namespace Sano.PersonalProjects.ColorPicker.Controls {

	/// <summary>
	/// Responsible for managing all functionality related to the color field.
	/// </summary>

	internal class ColorFieldPanel : UserControl {

		// private data fields		
		private bool m_isMouseDown;
		private Color m_color;
		private ColorSpace m_colorSpace;
		private int m_selectedComponentValue;
		private Point m_currentPoint;
		private Point m_tempPoint;

		// constants
		private const int SAMPLE_CIRCLE_RADIUS = 5;

		// events
		[EditorBrowsable(EditorBrowsableState.Always)]
		public event ColorSelectedEventHandler ColorSelected;

		/// <summary>
		/// Constructor.
		/// </summary>

		internal ColorFieldPanel() {

			InitializeComponent();
			this.SetStyle( ControlStyles.AllPaintingInWmPaint | ControlStyles.DoubleBuffer | ControlStyles.UserPaint, true );

		}

		public Color CurrentlySelectedColor {
			get { 
				
				Color color = this.CalculateSelectedColor( m_currentPoint );			
				return color;
		
			}

		}

		private void InvalidateNewAndOldRegions( Point oldPoint, Point newPoint ) {

			Rectangle oldRegion = new Rectangle( 
				oldPoint.X - SAMPLE_CIRCLE_RADIUS,
				oldPoint.Y - SAMPLE_CIRCLE_RADIUS,
				SAMPLE_CIRCLE_RADIUS * 2,
				SAMPLE_CIRCLE_RADIUS * 2 );
				
			// give the region a little buffer
			oldRegion.Inflate( 4, 4 );
			this.Invalidate( oldRegion );

			Rectangle newRegion = new Rectangle(
				newPoint.X - SAMPLE_CIRCLE_RADIUS,
				newPoint.Y - SAMPLE_CIRCLE_RADIUS,
				SAMPLE_CIRCLE_RADIUS * 2, 
				SAMPLE_CIRCLE_RADIUS * 2 );
				
			// give the region a little buffer
			newRegion.Inflate( 4, 4 );				
			this.Invalidate( newRegion );

		}
		
		protected override void OnMouseDown( MouseEventArgs e ) {

			if ( e.Button == MouseButtons.Left && ( e.X >= 0 && e.X <= 255 || e.Y >= 0 && e.Y <= 255 ) ) {
			
				m_isMouseDown = true;				
				
				Point newPoint = new Point( e.X , e.Y );
				this.InvalidateNewAndOldRegions( m_currentPoint, newPoint );
				m_currentPoint = newPoint;
				m_tempPoint = m_currentPoint;
		
				OnColorSelected( new ColorSelectedEventArgs( CalculateSelectedColor( newPoint ) ) ); 
			
			} else {
				m_isMouseDown = false;
			}

		}

		protected override void OnMouseUp(MouseEventArgs e) {
			m_isMouseDown = false;
		}

		protected override void OnMouseMove(MouseEventArgs e) {

			base.OnMouseMove( e );

			if ( m_isMouseDown ) 
            {
				int x, y = 0;

                x = Math.Max(e.X, 0);
                x = Math.Min(x, 255);

                y = Math.Max(e.Y, 0);
                y = Math.Min(y, 255);

				Point newPoint = new Point( x, y );

                InvalidateNewAndOldRegions(m_currentPoint, newPoint);

                OnColorSelected(new ColorSelectedEventArgs(CalculateSelectedColor(new Point(m_currentPoint.X, m_currentPoint.Y))));

                m_currentPoint = newPoint;
			} 

		}
		
		protected override void OnLoad(EventArgs e) {
			m_currentPoint = CalculatePoint();
		}

		protected override void OnMouseEnter(EventArgs e) {
			//this.Cursor = new Cursor( Sano.Utility.Resources.GetFileResource( "ColorFieldPanelCursor.cur" ) );
		}

		protected override void OnMouseLeave(EventArgs e) {
			
			Cursor.Show();
			this.ParentForm.Cursor = Cursors.Default;

		}

		protected void OnColorSelected( ColorSelectedEventArgs e ) {

			if ( ColorSelected != null ) {
				ColorSelected( this, e ); 
			}

		}

		protected override void OnPaint( PaintEventArgs e ) {
			
			// make sure this painting occurs only during run time.
			if ( !this.DesignMode ) {

				Graphics g = e.Graphics;
				UpdateCurrentColor( g );
				
				int circumference = 0;

				Point circlePoint = new Point( 
					m_currentPoint.X - SAMPLE_CIRCLE_RADIUS,
					m_currentPoint.Y - SAMPLE_CIRCLE_RADIUS );
						
				circumference = SAMPLE_CIRCLE_RADIUS * 2;

				Color currentColor = this.CurrentlySelectedColor;
				int average = ( currentColor.R + currentColor.G + currentColor.B ) / 3;
				
				if ( average > 150 ) {
					g.DrawEllipse( Pens.Black, new Rectangle( circlePoint, new Size( circumference, circumference ) ) );				
				} else {
					g.DrawEllipse( Pens.White, new Rectangle( circlePoint, new Size( circumference, circumference ) ) );
				}
							
			}
					
		}
		
		private Point CalculatePoint() {

			int x = 0;
			int y = 0;
				
			if ( m_colorSpace is HsbColorSpace ) {

				HSB hsb = ( HSB ) m_colorSpace.Structure;

				if ( m_colorSpace.SelectedComponent.DisplayCharacter == 'H' ) {

					x = ( int ) Math.Round( hsb.Saturation * 2.55 );
					y = 255 - ( int ) Math.Round( hsb.Brightness * 2.55 );
					
				} else if ( m_colorSpace.SelectedComponent.DisplayCharacter == 'S' ) {

					x = ( int ) Math.Ceiling( hsb.Hue * ( ( double ) 255 / 360 ) );
					y = ( int ) ( 255 - ( hsb.Brightness * 2.55 ) );
					
				} else if ( m_colorSpace.SelectedComponent.DisplayCharacter == 'B' ) {

					x = ( int ) Math.Ceiling( hsb.Hue * ( ( double ) 255 / 360 ) );
					y = 255 - ( int ) Math.Round( hsb.Saturation * 2.55 );
					
				}

			} else if ( m_colorSpace is RgbColorSpace ) {

				Color c = ( ( RgbColorSpace ) m_colorSpace ).GetColor(); 

				if ( m_colorSpace.SelectedComponent.DisplayCharacter == 'R' ) {

					x = c.B;
					y = 255 - c.G;
					
				} else if ( m_colorSpace.SelectedComponent.DisplayCharacter == 'G' ) {
					
					x = c.B;
					y = 255 - c.R;
					
				} else if ( m_colorSpace.SelectedComponent.DisplayCharacter == 'B' ) {

					x = c.R;
					y = 255 - c.G;	

				}
				
			}
			
			return new Point( x, y);
		
		}

		private void UpdateCurrentColor( Graphics g ) {
				
			if ( m_colorSpace is HsbColorSpace ) {

				HSB hsb = ( HSB ) m_colorSpace.Structure;

				if ( m_colorSpace.SelectedComponent.DisplayCharacter == 'H' ) {

					Color c = m_color;
					if ( c.Equals( Color.FromArgb( 0, 0, 0, 0 ) ) | c.Equals( Color.FromArgb( 0, 0, 0 ) ) ) {
						c = Color.FromArgb( 255, 0, 0 );
					}

					ColorRenderingHelper.DrawHueColorField( g, c );
					
				} else if ( m_colorSpace.SelectedComponent.DisplayCharacter == 'S' ) {
					ColorRenderingHelper.DrawSaturationColorField( g, m_selectedComponentValue );						
				} else if ( m_colorSpace.SelectedComponent.DisplayCharacter == 'B' ) {						
					ColorRenderingHelper.DrawBrightnessColorField( g, m_selectedComponentValue );
				}

			} else if ( m_colorSpace is RgbColorSpace ) {

				Color c = ( ( RgbColorSpace ) m_colorSpace ).GetColor(); 

				if ( m_colorSpace.SelectedComponent.DisplayCharacter == 'R' ) {						
					ColorRenderingHelper.DrawRedColorField( g, m_selectedComponentValue );											
				} else if ( m_colorSpace.SelectedComponent.DisplayCharacter == 'G' ) {
					ColorRenderingHelper.DrawGreenColorField( g, m_selectedComponentValue );											
				} else if ( m_colorSpace.SelectedComponent.DisplayCharacter == 'B' ) {
					ColorRenderingHelper.DrawBlueColorField( g, m_selectedComponentValue );						
				}
				
			}
						
		}
		
		public void UpdateColor( ColorSpace colorSpace, Color color, bool resetPreviouslyPickedPoint ) {
			
			m_color = color;
			UpdateColor( colorSpace, resetPreviouslyPickedPoint );

		}

		public void UpdateColor( ColorSpace colorSpace, int selectedComponentValue, bool resetPreviouslyPickedPoint ) {
			
			m_color = Color.Empty;
			m_selectedComponentValue = selectedComponentValue;
			UpdateColor( colorSpace, resetPreviouslyPickedPoint );

		}

		private void UpdateColor( ColorSpace colorSpace, bool resetPreviouslyPickedPoint ) {

			m_colorSpace = colorSpace;

			if ( resetPreviouslyPickedPoint ) {
				m_currentPoint = CalculatePoint();
			}

			this.Invalidate();

		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			this.Name = "ColorFieldPanel";
			this.Size = new System.Drawing.Size(256, 256);
		}
		#endregion

		private Color CalculateSelectedColor( Point p ) {

			object selectedColor = null;
			
			if ( m_colorSpace is HsbColorSpace ) {

				HSB hsb = ( HSB ) m_colorSpace.Structure;

				if ( m_colorSpace.SelectedComponent.DisplayCharacter == 'H' ) {

					int brightness = ( int ) ( ( ( double ) 255 - p.Y ) / 2.55 );
					int saturation = ( int ) ( ( double ) p.X / 2.55 );

					selectedColor = new HSB( hsb.Hue, saturation, brightness );
				
				} else if ( m_colorSpace.SelectedComponent.DisplayCharacter == 'S' ) {

					int hue = ( int ) ( p.X * ( ( double ) 360 / 255 ) );
					int brightness = ( int ) ( ( ( double ) 255 - p.Y ) / 2.55 );

					if ( hue == 360 ) {
						hue = 0;
					}

					selectedColor = new HSB( hue, hsb.Saturation, brightness );
				
				} else if ( m_colorSpace.SelectedComponent.DisplayCharacter == 'B' ) {

					int hue = ( int ) ( p.X * ( ( double ) 360 / 255 ) );
					int saturation = ( int ) ( ( ( double ) 255 - p.Y ) / 2.55 );

					if ( hue == 360 ) {
						hue = 0;
					}
					
					selectedColor = new HSB( hue, saturation, hsb.Brightness );

				}

			}  else if ( m_colorSpace is RgbColorSpace ) {

				RGB rgb = ( RGB ) m_colorSpace.Structure; 

				if ( m_colorSpace.SelectedComponent.DisplayCharacter == 'R' ) {					
					selectedColor = new RGB( rgb.Red, 255 - p.Y, p.X );
				} else if ( m_colorSpace.SelectedComponent.DisplayCharacter == 'G' ) {
					selectedColor = new RGB( 255 - p.Y, rgb.Green, p.X );
				} else if ( m_colorSpace.SelectedComponent.DisplayCharacter == 'B' ) {
					selectedColor = new RGB( p.X, 255 - p.Y, rgb.Blue);
				}
			
			}
			
//			RGB crgb;
			HSB hsbSelectedColor; 
//			if ( ( hsbSelectedColor = selectedColor as HSB ) != null ) {
//				crgb = ColorConverter.HsbToRgb( hsbSelectedColor );
//			} else {
//				crgb = ( RGB ) selectedColor;
//			}
//
			Color c = Color.Empty;
			if ( ( hsbSelectedColor = selectedColor as HSB ) != null ) {
				c = ColorConverter.HsbToColor( hsbSelectedColor );
			} else {
				c = ColorConverter.RgbToColor( ( RGB ) selectedColor );
			}


			return c; //ColorConverter.RgbToColor( crgb );

		}

	} // ColorFieldPanel

} // Sano.PersonalProjects.ColorPicker.Controls
