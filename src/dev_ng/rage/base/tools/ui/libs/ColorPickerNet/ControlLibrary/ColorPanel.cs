using System;
using System.Collections;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Data;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;

using System.Diagnostics;

namespace Sano.PersonalProjects.ColorPicker.Controls {

	// TODO: change data types of RGB from int16 to byte
	
	public class ColorPanel : UserControl {

		// controls
		private ColorFieldPanel colorFieldPanel;
		private HexTextBox hexTextBox;
		private HsbColorSpace hsbColorSpace;
		private Label hexLabel;
		private ColorBox topColorBox;
		private ColorBox bottomColorBox;
		private ColorBox activeColorBox;
		private ColorSlider colorSlider1;
		private RgbColorSpace rgbColorSpace;
		private ColorSwatchPanel colorSwatchPanel;
		
		// member fields
		private bool m_isLeftMouseButtonDown;
		private Bitmap m_hueSliderBitmap;
		private Rectangle m_selectedColorRegion;
		private Rectangle m_selectedColorColorRegion;
		private ColorSpace m_currentColorSpace;
		private BitVector32 m_panelState;
        
		// rectangles for drawing.
		private readonly Rectangle m_colorFieldOuterRegion = new Rectangle( 12, 4, 265, 265 );
		private readonly Rectangle m_colorFieldInnerRegion = new Rectangle( 15, 7, 257, 257 );
		private readonly Rectangle m_valueFieldOuterRegion = new Rectangle( 295, 4, 27, 265 );
		private readonly Rectangle m_valueFieldInnerRegion = new Rectangle( 298, 7, 19, 257 );
		private readonly Rectangle m_swatchRegion = new Rectangle( 464, 4, 164, 263 );

		// constants
		private const int PANELSTATE_isLeftMouseDown = 0x1;								// 1
		private const int PANELSTATE_isLeftMouseDownAndMoving = 0x8;					// 8
		private const int ARROW_HEIGHT = 10;
		private Sano.PersonalProjects.ColorPicker.Controls.ColorSlider alphaSlider;
		private System.Windows.Forms.Label alphaLabel;
		private System.Windows.Forms.TextBox alphaTextBox;
		private const int ARROW_WIDTH = 6;
		
		/// <summary>
		/// Constructor. Initializes all of the components and member fields 
		/// and configures the control for double buffering support.
		/// </summary>

		public ColorPanel() {

			InitializeComponent();
			InitializeCustomComponents();

			m_hueSliderBitmap = new Bitmap( 18, 256 );
			using ( Graphics g = Graphics.FromImage( m_hueSliderBitmap ) ) {
				ColorRenderingHelper.DrawHueColorSlider( g, new Rectangle( new Point( 0, 0 ), m_hueSliderBitmap.Size ) );
			}

			activeColorBox = topColorBox;
			activeColorBox.IsActive = true;
			this.hsbColorSpace.SetDefaultSelection(); 
			
			alphaTextBox.Text = alphaSlider.Value.ToString();

		}

		/// <summary>
		/// Overrides the base class' OnLoad method to initialize several 
		/// different data fields and control properties.
		/// </summary>
		/// <param name="e">An EventArgs that contains the event data.</param>

		protected override void OnLoad(EventArgs e) {
			base.OnLoad (e);

			m_selectedColorRegion = new Rectangle( 344, 8, 88, 40 );
			m_selectedColorColorRegion = new Rectangle( 346, 10, 85, 37 );
			m_panelState = new BitVector32( 0 );

			this.SetStyle( ControlStyles.DoubleBuffer | ControlStyles.UserPaint, true );
			this.AllowDrop = true;
			this.bottomColorBox.BackColor = this.rgbColorSpace.GetColor();

		}
		
		/// <summary>
		/// Returns the hex value of the colors in the RGB color space.
		/// </summary>

		public string HexValue {
			get { return rgbColorSpace.ConvertToHex().ToString(); }
		}

		/// <summary>
		/// Overrides the panel's OnPaint method to performs all of the painting 
		/// operations.
		/// </summary>
		/// <param name="e">A PaintEventArgs that contains the event data.</param>

		protected override void OnPaint(PaintEventArgs e) {

			base.OnPaint (e);

			// using defines a scope at the end of which the graphics object is disposed.
			using ( Graphics g = e.Graphics ) {
								
				ControlPaint.DrawBorder3D( g, m_colorFieldOuterRegion );
				g.DrawRectangle( Pens.Black, m_colorFieldInnerRegion );

				ControlPaint.DrawBorder3D( g, m_valueFieldOuterRegion );
				g.DrawRectangle( Pens.Black, m_valueFieldInnerRegion );

				ControlPaint.DrawBorder3D( g, new Rectangle( 
					topColorBox.Location.X - 3,
					topColorBox.Location.Y - 3,
					topColorBox.Width + 7,
					( topColorBox.Height * 2 ) + 9 ) );
				
			}
			
		}
		
		/// <summary>
		/// Determines the color slider left triangle pointer invalidation 
		/// region.
		/// </summary>
		/// <param name="arrowY">Current cursor y-value.</param>
		/// <returns>A rectangle object representing the area to be 
		/// invalidated.</returns>

		private Rectangle GetLeftTrianglePointerInvalidationRegion( int arrowY ) {

			int leftPadding = ARROW_WIDTH + 2;
			int x = m_valueFieldOuterRegion.Left - leftPadding;
			int y = arrowY - ( ARROW_HEIGHT / 2 ) - 1;
			int width = ARROW_WIDTH + 2;
			int height = ARROW_HEIGHT + 3;
			
			return new Rectangle( x, y, width, height );

		}
		
		/// <summary>
		/// Determines the color slider right triangle pointer invalidation 
		/// region.
		/// </summary>
		/// <param name="arrowY">Current cursor y-value</param>
		/// <returns>A rectangle object representing the area to be 
		/// invalidated.</returns>
		
		private Rectangle GetRightTrianglePointerInvalidationRegion( int arrowY ) {

			int x = m_valueFieldOuterRegion.Right;
			int y = arrowY - ( ARROW_HEIGHT / 2 ) - 1;
			int width = ARROW_WIDTH + 2;
			int height = ARROW_HEIGHT + 3;
			
			return new Rectangle( x, y, width, height );

		}

		protected override void OnMouseDown(MouseEventArgs e) {
			
			base.OnMouseDown( e );

			if ( e.Button == MouseButtons.Left ) {
			
				// TODO: constrict X position. too much flexibility is given there.
				
				// RS TODO: Is this function even necessary? What's it for?

				m_isLeftMouseButtonDown = true;
	
				UpdateColorPanels( false, false, true, true );

				if ( m_currentColorSpace.SelectedComponent.DisplayCharacter == 'H' ) {
					activeColorBox.BackColor = colorFieldPanel.CurrentlySelectedColor;
				}

			}
		
		}

		protected override void OnMouseUp(MouseEventArgs e) {
			
			base.OnMouseUp( e );

			if ( m_isLeftMouseButtonDown ) {
				UpdateColorField( false );
			}

			m_isLeftMouseButtonDown = false;

		}
		
		/// <summary>
		/// Handles the SelectedColorSpaceComponentChanged event raised by the
		/// color spaces. When this occurs, the color slider arrow regions and 
		/// the color panels are updated.
		/// </summary>
		/// <param name="sender">The ColorSpace object that raised the event.</param>
		/// <param name="e">An EventArgs containing the event data.</param>

		private void SelectedColorSpaceComponentChanged( ColorSpace sender, EventArgs e ) {

			if ( sender is RgbColorSpace ) {
				hsbColorSpace.ResetComponents();
			} else if ( sender is HsbColorSpace ) {
				rgbColorSpace.ResetComponents();
			}

			m_currentColorSpace = sender;
			
			UpdateColorSliderArrowRegions();			
			UpdateColorPanels( true, true, true, true );

		}

		/// <summary>
		/// Handles the ComponentValueChanged event that the ColorSpace raises 
		/// when the value of one of its components is changed by way of a
		/// keyboard user input. The color spaces are synced up and the color
		/// panels updated.
		/// </summary>
		/// <param name="sender">The ColorSpace object that raised the event.</param>
		/// <param name="e">An EventArgs object containing the event data.</param>

		private void ColorSpaceComponentValueChanged( ColorSpace sender, EventArgs e ) {

		if ( sender is RgbColorSpace ) {
				hsbColorSpace.Structure = ColorConverter.RgbToHsb( ( RGB ) rgbColorSpace.Structure );
			} else if ( sender is HsbColorSpace ) {
				rgbColorSpace.Structure = ColorConverter.HsbToRgb( ( HSB ) hsbColorSpace.Structure ); //, ( 255 - ( this.m_currentColorSliderArrowYLocation - m_colorSliderInnerRegion.Top ) ) );
			}

			UpdateColorSliderArrowRegions();
			UpdateColorPanels( true, true, true, true );
			activeColorBox.BackColor = ColorConverter.RgbToColor( ( RGB ) rgbColorSpace.Structure );

		}

		/// <summary>
		/// Processes the selected color.
		/// </summary>
		/// <param name="color">A Color object containing the color that was
		/// recently selected.</param>

		private void ColorSelected( Color color, bool updateSliderPosition ) {
			
			// make sure the color that was just clicked isn't the color that
			// is currently displayed (performance enhancement).

			if ( !ColorConverter.ColorToRgb( color ).Equals( rgbColorSpace.Structure ) ||
				(m_UseAlpha && (color.A != alphaSlider.Value))) {
				
				RGB rgb = ColorConverter.ColorToRgb( color );

				rgbColorSpace.Structure = rgb;
				hsbColorSpace.Structure = ColorConverter.RgbToHsb( rgb );
				
				if ( updateSliderPosition ) {
					UpdateColorSliderArrowRegions();
				}

				if (m_UseAlpha)
				{
					alphaSlider.Value = color.A;
					alphaTextBox.Text = alphaSlider.Value.ToString();				
				}

				this.UpdateColorPanels( false, true, true, true );
			}

		}

		[Browsable(true)]
		public Color CurrentColor {
			get {
				if (m_UseAlpha)
				{
					Color c = rgbColorSpace.GetColor();
					return Color.FromArgb(alphaSlider.Value, c);
				}
				else
				{
					return rgbColorSpace.GetColor();
				}
			}
			set {
				ColorSelected(value, true);
			}
		}

		[Browsable(true)]
		public event ColorSelectedEventHandler ColorChanged;

		protected void OnColorChanged() {
			if (ColorChanged != null) {
				ColorSelectedEventArgs e = new ColorSelectedEventArgs(CurrentColor);
				ColorChanged(this, e);
			}
		}

		private void colorSwatchPanel_ColorSwatchSelected( object sender, ColorSelectedEventArgs e ) {
			ColorSelected( e.Color, true );
		}

		private void colorFieldPanel_ColorSelected( object sender, ColorSelectedEventArgs e ) {
				
			RGB rgb = ColorConverter.ColorToRgb( e.Color );

			rgbColorSpace.Structure = rgb;
			HSB hsb = ColorConverter.RgbToHsb( rgb );

			// this is a hack that works around the conversion problem that 
			// sees the hue and saturation values get reset to 0 when the 
			// rgb values are 0, 0 and 0 respectively. revisit the conversion
			// formula at a later date and see if the problem can be 
			// pinpointed.
			
			if ( m_currentColorSpace.SelectedComponent.DisplayCharacter == 'H' ) {

				int hue = ( ( HSB ) hsbColorSpace.Structure ).Hue;
				hsbColorSpace.Structure = new HSB( hue, hsb.Saturation, hsb.Brightness );

			} else if ( m_currentColorSpace.SelectedComponent.DisplayCharacter == 'S' ) {

				int saturation = ( ( HSB ) hsbColorSpace.Structure ).Saturation;
				hsbColorSpace.Structure = new HSB( hsb.Hue, saturation , hsb.Brightness );
			
			} else {
				hsbColorSpace.Structure = hsb;			
			}
			
			hexTextBox.Text = this.HexValue;
			activeColorBox.BackColor = ColorConverter.RgbToColor( rgb );

			UpdateColorPanels(true, false, true, false);

		}

		private void InitializeCustomComponents() {

			this.SuspendLayout();

//			// colorSwatchPanel
//			colorSwatchPanel = new ColorSwatchPanel();
//			colorSwatchPanel.Location = new System.Drawing.Point(464, 4);
//			colorSwatchPanel.Name = "colorSwatchPanel";
//			colorSwatchPanel.Size = new System.Drawing.Size(96, 264);
//			colorSwatchPanel.TabIndex = 53;
//			colorSwatchPanel.ColorSwatchSelected += new ColorSwatchSelectedHandler(colorSwatchPanel_ColorSwatchSelected);
//			Controls.Add( colorSwatchPanel );
			
			this.ResumeLayout( false );

		}
		
		private void ValueChanged( int newValue ) {
				
			int mValue = newValue;

			switch ( m_currentColorSpace.SelectedComponent.Unit ) {
		
				case ComponentUnit.Percentage:
					mValue = ( int ) Math.Ceiling(  mValue / ( ( ( double ) 255 ) / 100 ) );
					break;
		
				case ComponentUnit.Degree:
					mValue = ( int ) Math.Ceiling( mValue / ( ( ( double ) 255 ) / 360 ) );
					if ( mValue == 360 ) {
						mValue = 0;
					}
					break;
				
			}

			m_currentColorSpace.SelectedComponent.Value = mValue;

			if ( m_currentColorSpace is RgbColorSpace ) {
				hsbColorSpace.Structure = ColorConverter.RgbToHsb( ( RGB ) rgbColorSpace.Structure );
			} else if ( m_currentColorSpace is HsbColorSpace ) {
				rgbColorSpace.Structure = ColorConverter.HsbToRgb( ( HSB ) hsbColorSpace.Structure ); 
			}
											
			UpdateColorPanels( false, false, true, true );
			activeColorBox.BackColor = ColorConverter.RgbToColor( ( RGB ) rgbColorSpace.Structure );

		} // ValueChanged

		private void UpdateColorSliderArrowRegions() {
			colorSlider1.Value = CalculateColorSliderArrowPosition( this.m_currentColorSpace.SelectedComponent );
		}
		
		/// <summary>
		/// Calculates the color slider value.
		/// </summary>
		/// <param name="csc">The active color space component.</param>
		/// <returns>The color slider value.</returns>

		private int CalculateColorSliderArrowPosition( ColorSpaceComponent csc ) {
			
			int mValue = csc.Value;
			
			switch ( csc.Unit ) {
			
				case ComponentUnit.Percentage:
					mValue = ( int ) Math.Ceiling( ( ( ( double ) 255 ) / 100 ) * mValue );
					break;
				
				case ComponentUnit.Degree:
					mValue = ( int ) Math.Ceiling( ( ( ( double ) 255 ) / 360 ) * mValue );
					break;
					
			}

			return mValue; 

		}
		
		/// <summary>
		/// Updates the color panels and the hex value.
		/// </summary>
		/// <param name="updateSlider">A boolean value indicating whether or 
		/// not the color slider should be updated.</param>
		/// <param name="resetPreviouslyPickedPointOnColorField">A boolean 
		/// value indicating whether or not the previously picked point on the 
		/// color field should be reset.</param>
		/// <param name="updateHexValue">A boolean value indicating whether or
		/// not the hex value should be updated.</param>

		private void UpdateColorPanels( bool updateSlider, bool resetPreviouslyPickedPointOnColorField, bool updateHexValue, bool updateField ) {
						
			if ( activeColorBox == null ) {
				activeColorBox = topColorBox;
				activeColorBox.IsActive = true;
			}
			
			activeColorBox.BackColor = ColorConverter.RgbToColor( ( RGB ) rgbColorSpace.Structure );

			if ( updateSlider ) {
				this.colorSlider1.UpdateColor( m_currentColorSpace );
			}

			this.alphaSlider.UpdateColor(m_currentColorSpace);

			if (updateField) {
				this.UpdateColorField( resetPreviouslyPickedPointOnColorField );
			}

			if ( updateHexValue ) {
				this.UpdateHexValue();
			}

			OnColorChanged();
			
		}
		
		/// <summary>
		/// Updates the hexadecimal text value.
		/// </summary>

		private void UpdateHexValue() {
			hexTextBox.Text = this.HexValue;
			hexTextBox.SelectionStart = this.HexValue.Length;
		}

		/// <summary>
		/// Updates the color field panel.
		/// </summary>
		/// <param name="resetPreviouslyPickedPoint">A boolean value indicating
		/// whether or not the previously picked point should be reset.</param>

		private void UpdateColorField( bool resetPreviouslyPickedPoint ) {

			int sValue = this.m_currentColorSpace.SelectedComponent.Value;
			char component = m_currentColorSpace.SelectedComponent.DisplayCharacter;
						
			if ( m_currentColorSpace is HsbColorSpace ) {
				
				if ( component == 'H' ) {
					
//					Debug.WriteLine( 255 - colorSlider1.Value );
					Color color = m_hueSliderBitmap.GetPixel( 10, 255 - colorSlider1.Value );
					//Color color = ColorConverter.HsbToColor( ( HSB ) m_currentColorSpace.Structure ) ;
					colorFieldPanel.UpdateColor( m_currentColorSpace, color, resetPreviouslyPickedPoint );

				} else if ( component == 'S' || component == 'B' ) {
					colorFieldPanel.UpdateColor( m_currentColorSpace, sValue, resetPreviouslyPickedPoint );
				}
							
			} else if ( m_currentColorSpace is RgbColorSpace ) {
				colorFieldPanel.UpdateColor( m_currentColorSpace, sValue, resetPreviouslyPickedPoint );
			}

		}

		/// <summary>
		/// Handles the KeyUp event raised by the hexTextBox object.
		/// </summary>
		
		private void hexTextBox_KeyUp( object sender, KeyEventArgs e ) {
			UpdateColorSpacesWithNewHexValue();		
		}

		/// <summary>
		/// Handles the KeyDown event raised by the hexTextBox object.
		/// </summary>
		/// <param name="sender"> The object that raised the event.</param>
		/// <param name="e">A KeyEventArgs containing the event data.</param>

		private void hexTextBox_KeyDown(object sender, KeyEventArgs e) {
			UpdateColorSpacesWithNewHexValue();
		}

		/// <summary>
		/// Updates the color spaces with the new hex value and makes sure that
		/// the necessary components are updated (color field, color slider, 
		/// etc).
		/// </summary>

		private void UpdateColorSpacesWithNewHexValue() {

			RGB rgb = ColorConverter.HexToRgb( hexTextBox.Text );							

			rgbColorSpace.Structure = rgb;
			hsbColorSpace.Structure = ColorConverter.RgbToHsb( rgb );
			
			UpdateColorSliderArrowRegions();

			// reset the location of the arrows in the value region.
			this.UpdateColorPanels( true, true, false, true );

		}

		private void UpdateAlphaSliderWithNewValue() {
			try 
			{
				int newValue = System.Int32.Parse(alphaTextBox.Text);
				if (newValue >= 0 && newValue < 256)
				{
					alphaSlider.Value = newValue;
				}
			}
			catch 
			{
				// leave value alone
			}
		}

		#region Component Designer generated code
		
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>

		private void InitializeComponent() {
			this.hexLabel = new System.Windows.Forms.Label();
			this.bottomColorBox = new Sano.PersonalProjects.ColorPicker.Controls.ColorBox();
			this.topColorBox = new Sano.PersonalProjects.ColorPicker.Controls.ColorBox();
			this.colorFieldPanel = new Sano.PersonalProjects.ColorPicker.Controls.ColorFieldPanel();
			this.rgbColorSpace = new Sano.PersonalProjects.ColorPicker.Controls.RgbColorSpace();
			this.hsbColorSpace = new Sano.PersonalProjects.ColorPicker.Controls.HsbColorSpace();
			this.hexTextBox = new Sano.PersonalProjects.ColorPicker.Controls.HexTextBox();
			this.colorSlider1 = new Sano.PersonalProjects.ColorPicker.Controls.ColorSlider();
			this.colorSwatchPanel = new Sano.PersonalProjects.ColorPicker.Controls.ColorSwatchPanel();
			this.alphaSlider = new Sano.PersonalProjects.ColorPicker.Controls.ColorSlider();
			this.alphaLabel = new System.Windows.Forms.Label();
			this.alphaTextBox = new System.Windows.Forms.TextBox();
			this.SuspendLayout();
			// 
			// hexLabel
			// 
			this.hexLabel.Location = new System.Drawing.Point(350, 245);
			this.hexLabel.Name = "hexLabel";
			this.hexLabel.Size = new System.Drawing.Size(31, 23);
			this.hexLabel.TabIndex = 40;
			this.hexLabel.Text = "HEX:";
			this.hexLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// bottomColorBox
			// 
			this.bottomColorBox.AllowDrop = true;
			this.bottomColorBox.BackColor = System.Drawing.Color.Black;
			this.bottomColorBox.Location = new System.Drawing.Point(352, 39);
			this.bottomColorBox.Name = "bottomColorBox";
			this.bottomColorBox.Size = new System.Drawing.Size(85, 30);
			this.bottomColorBox.TabIndex = 51;
			this.bottomColorBox.TabStop = false;
			this.bottomColorBox.DragDrop += new System.Windows.Forms.DragEventHandler(this.ColorBox_DragDrop);
			this.bottomColorBox.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ColorBox_MouseDown);
			// 
			// topColorBox
			// 
			this.topColorBox.AllowDrop = true;
			this.topColorBox.Location = new System.Drawing.Point(352, 7);
			this.topColorBox.Name = "topColorBox";
			this.topColorBox.Size = new System.Drawing.Size(85, 30);
			this.topColorBox.TabIndex = 42;
			this.topColorBox.TabStop = false;
			this.topColorBox.DragDrop += new System.Windows.Forms.DragEventHandler(this.ColorBox_DragDrop);
			this.topColorBox.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ColorBox_MouseDown);
			// 
			// colorFieldPanel
			// 
			this.colorFieldPanel.BackColor = System.Drawing.SystemColors.ControlLight;
			this.colorFieldPanel.Location = new System.Drawing.Point(16, 8);
			this.colorFieldPanel.Name = "colorFieldPanel";
			this.colorFieldPanel.Size = new System.Drawing.Size(256, 256);
			this.colorFieldPanel.TabIndex = 52;
			this.colorFieldPanel.ColorSelected += new Sano.PersonalProjects.ColorPicker.Controls.ColorSelectedEventHandler(this.colorFieldPanel_ColorSelected);
			// 
			// rgbColorSpace
			// 
			this.rgbColorSpace.Location = new System.Drawing.Point(338, 153);
			this.rgbColorSpace.Name = "rgbColorSpace";
			this.rgbColorSpace.Size = new System.Drawing.Size(102, 96);
			this.rgbColorSpace.TabIndex = 48;
			this.rgbColorSpace.ComponentValueChanged += new Sano.PersonalProjects.ColorPicker.Controls.ColorSpaceEventHandler(this.ColorSpaceComponentValueChanged);
			this.rgbColorSpace.SelectedComponentChanged += new Sano.PersonalProjects.ColorPicker.Controls.ColorSpaceEventHandler(this.SelectedColorSpaceComponentChanged);
			// 
			// hsbColorSpace
			// 
			this.hsbColorSpace.Location = new System.Drawing.Point(338, 73);
			this.hsbColorSpace.Name = "hsbColorSpace";
			this.hsbColorSpace.Size = new System.Drawing.Size(102, 96);
			this.hsbColorSpace.TabIndex = 47;
			this.hsbColorSpace.ComponentValueChanged += new Sano.PersonalProjects.ColorPicker.Controls.ColorSpaceEventHandler(this.ColorSpaceComponentValueChanged);
			this.hsbColorSpace.SelectedComponentChanged += new Sano.PersonalProjects.ColorPicker.Controls.ColorSpaceEventHandler(this.SelectedColorSpaceComponentChanged);
			// 
			// hexTextBox
			// 
			this.hexTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.hexTextBox.Location = new System.Drawing.Point(382, 246);
			this.hexTextBox.MaxLength = 6;
			this.hexTextBox.Name = "hexTextBox";
			this.hexTextBox.Size = new System.Drawing.Size(55, 20);
			this.hexTextBox.TabIndex = 49;
			this.hexTextBox.Text = "";
			this.hexTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.hexTextBox_KeyDown);
			this.hexTextBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.hexTextBox_KeyUp);
			// 
			// colorSlider1
			// 
			this.colorSlider1.IsAlphaSlider = false;
			this.colorSlider1.Location = new System.Drawing.Point(290, 0);
			this.colorSlider1.Name = "colorSlider1";
			this.colorSlider1.Size = new System.Drawing.Size(48, 272);
			this.colorSlider1.TabIndex = 53;
			this.colorSlider1.Value = 7;
			this.colorSlider1.ValueChanged += new Sano.PersonalProjects.ColorPicker.Controls.ValueChangedEventHandler(this.colorSlider1_ValueChanged);
			// 
			// colorSwatchPanel
			// 
			this.colorSwatchPanel.AllowDrop = true;
			this.colorSwatchPanel.Location = new System.Drawing.Point(502, 5);
			this.colorSwatchPanel.Name = "colorSwatchPanel";
			this.colorSwatchPanel.Size = new System.Drawing.Size(96, 264);
			this.colorSwatchPanel.TabIndex = 53;
			this.colorSwatchPanel.ColorSwatchSelected += new Sano.PersonalProjects.ColorPicker.Controls.ColorSwatchSelectedHandler(this.colorSwatchPanel_ColorSwatchSelected);
			// 
			// alphaSlider
			// 
			this.alphaSlider.IsAlphaSlider = true;
			this.alphaSlider.Location = new System.Drawing.Point(448, 0);
			this.alphaSlider.Name = "alphaSlider";
			this.alphaSlider.Size = new System.Drawing.Size(48, 272);
			this.alphaSlider.TabIndex = 54;
			this.alphaSlider.Value = 255;
			this.alphaSlider.ValueChanged += new Sano.PersonalProjects.ColorPicker.Controls.ValueChangedEventHandler(this.alphaSlider_ValueChanged);
			// 
			// alphaLabel
			// 
			this.alphaLabel.Location = new System.Drawing.Point(364, 245);
			this.alphaLabel.Name = "alphaLabel";
			this.alphaLabel.Size = new System.Drawing.Size(15, 23);
			this.alphaLabel.TabIndex = 55;
			this.alphaLabel.Text = "A:";
			this.alphaLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// alphaTextBox
			// 
			this.alphaTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.alphaTextBox.Location = new System.Drawing.Point(386, 246);
			this.alphaTextBox.MaxLength = 3;
			this.alphaTextBox.Name = "alphaTextBox";
			this.alphaTextBox.Size = new System.Drawing.Size(40, 20);
			this.alphaTextBox.TabIndex = 56;
			this.alphaTextBox.Text = "0";
			this.alphaTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.alphaTextBox_KeyDown);
			this.alphaTextBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.alphaTextBox_KeyUp);
			// 
			// ColorPanel
			// 
			this.Controls.Add(this.alphaTextBox);
			this.Controls.Add(this.alphaLabel);
			this.Controls.Add(this.alphaSlider);
			this.Controls.Add(this.colorSwatchPanel);
			this.Controls.Add(this.colorSlider1);
			this.Controls.Add(this.hexTextBox);
			this.Controls.Add(this.rgbColorSpace);
			this.Controls.Add(this.hsbColorSpace);
			this.Controls.Add(this.bottomColorBox);
			this.Controls.Add(this.hexLabel);
			this.Controls.Add(this.topColorBox);
			this.Controls.Add(this.colorFieldPanel);
			this.Name = "ColorPanel";
			this.Size = new System.Drawing.Size(608, 272);
			this.ResumeLayout(false);

		}

		#endregion Component Designer generated code

		private void ColorBox_MouseDown(object sender, MouseEventArgs e) {

			if ( e.Button == MouseButtons.Right ) {

				if ( sender != activeColorBox ) {

					activeColorBox.IsActive = false;
					activeColorBox = ( ( ColorBox ) sender );
					activeColorBox.IsActive = true;

					rgbColorSpace.Structure = ColorConverter.ColorToRgb( activeColorBox.BackColor );
					hsbColorSpace.Structure = ColorConverter.ColorToHsb( activeColorBox.BackColor );
					
					colorSlider1.Value = CalculateColorSliderArrowPosition( this.m_currentColorSpace.SelectedComponent );
					this.UpdateColorPanels( true, true, true, true );

				}

			}

		}

		private void colorSlider1_ValueChanged(object sender, Sano.PersonalProjects.ColorPicker.Controls.ValueChangedEventArgs e) {
			this.ValueChanged( e.Value );
		}

		private void ColorBox_DragDrop(object sender, System.Windows.Forms.DragEventArgs e) {

			// should only update fields if color box that the color is 
			// dropped above is active.
						
			if ( ( ( ColorBox ) sender ).IsActive ) {

				object oColor = e.Data.GetData( typeof( Color ) );
				Color color = Color.Empty;
							
				if ( oColor != null ) {
					color = ( Color ) oColor;	
				}
							 
				if ( color != Color.Empty ) {
							
					rgbColorSpace.Structure = ColorConverter.ColorToRgb( color );
					hsbColorSpace.Structure = ColorConverter.ColorToHsb( color );
									
					colorSlider1.Value = CalculateColorSliderArrowPosition( this.m_currentColorSpace.SelectedComponent );
					this.UpdateColorPanels( true, true, true, true );
								
				}
			
			}
					
		}

		public bool UseSwatches {
			get {
				return m_UseSwatches;
			}
			set {
				if (m_UseSwatches != value) {
					m_UseSwatches = value;
					if (m_UseSwatches) 
					{
						this.Width = colorSwatchPanel.Location.X + colorSwatchPanel.Width + 3;
						colorSwatchPanel.Show();
						topColorBox.DragDrop += new DragEventHandler(ColorBox_DragDrop);
						bottomColorBox.DragDrop += new DragEventHandler(ColorBox_DragDrop);
					}
					else 
					{
						this.Width = colorSwatchPanel.Location.X - 3;
						colorSwatchPanel.Hide();
						topColorBox.DragDrop -= new DragEventHandler(ColorBox_DragDrop);
						bottomColorBox.DragDrop -= new DragEventHandler(ColorBox_DragDrop);
					}
				}
			}
		}
		private bool m_UseSwatches = true;

		public bool UseHex {
			get {
				return m_UseHex;
			}
			set {
				if (m_UseHex != value) {
					m_UseHex = value;
					this.hexTextBox.Visible = m_UseHex;
					this.hexLabel.Visible = m_UseHex;
					if (m_UseHex && UseAlpha)
					{
						UseAlpha = false;
					}
				}
			}
		}
		private bool m_UseHex = true;

		public bool UseAlpha {
			get {
				return m_UseAlpha;
			}
			set {
				if (m_UseAlpha != value) {
					m_UseAlpha = value;
					alphaLabel.Visible = m_UseAlpha;
					alphaTextBox.Visible = m_UseAlpha;
					alphaSlider.Visible = m_UseAlpha;
					if (m_UseAlpha) {
						if (UseHex) {
							UseHex = false;
						}
						this.Width -= alphaSlider.Width;
					}
					else {
						this.Width += alphaSlider.Width;
					}
				}
			}
		}
		private bool m_UseAlpha = false;

		private void alphaTextBox_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
		{
			UpdateAlphaSliderWithNewValue();
		}

		private void alphaTextBox_KeyUp(object sender, System.Windows.Forms.KeyEventArgs e)
		{
			UpdateAlphaSliderWithNewValue();
		}

		private void alphaSlider_ValueChanged(object sender, Sano.PersonalProjects.ColorPicker.Controls.ValueChangedEventArgs e)
		{
			alphaTextBox.Text = alphaSlider.Value.ToString();
			OnColorChanged();		
		}

	} // ColorPanel

} // Sano.PersonalProjects.ColorPicker.Controls
