
using Sano.Utility;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Globalization;
using System.IO;
using System.Windows.Forms;
using System.Diagnostics;

namespace Sano.PersonalProjects.ColorPicker.Controls {

	internal class ColorSwatchPanel : UserControl {

		private System.ComponentModel.IContainer components;
		
		// events
		public event ColorSwatchSelectedHandler ColorSwatchSelected;  

		// controls
		private ToolTip colorTip;
		private DragForm dragFormSwatch;
		private ContextMenu contextMenu;
		private MenuItem renameSwatchMenuItem;
		private MenuItem deleteSwatchMenuItem;

		// readonly members
		private readonly Rectangle m_swatchRegion = new Rectangle( 0, 0, 164, 263 );
		private readonly string m_customSwatchesFile = "CustomSwatches.xml";
		private readonly Size m_swatchSize = new Size( SWATCH_WIDTH, SWATCH_HEIGHT );
		
		// constants
		private const int PADDING = 2;
		private const int SWATCH_WIDTH = 10;
		private const int SWATCH_HEIGHT = 10;
		private const int OUTER_PADDING = 6;

		// member fields
		private List<ColorSwatch> m_swatches;
		private bool m_isLeftMouseButtonDown;
		private bool m_paintActiveEmptySwatch;
		private Bitmap m_swatchBitmap;
		private ColorSwatch[] m_swatchArray;	
		private ColorSwatch m_lastSwatch;
		private int m_currentSwatchIndex;
		private int m_startX; 
		private int m_startY; 
		private int m_swatchOuterRegionWidth;
		private int m_swatchOuterRegionHeight;
		private int m_totalNumberOfSwatches;
		private int m_numberOfEmptySwatches;
		private int m_numberOfHorizontalSwatches;
		private int m_numberOfVerticalSwatches;
		private int m_nextEmptySwatchIndex;

		/// <summary>
		/// Constructor. 
		/// </summary>

		internal ColorSwatchPanel() {
			
			InitializeComponent();

			m_startX = m_swatchRegion.X + OUTER_PADDING;
			m_startY = m_swatchRegion.Y + OUTER_PADDING;

			dragFormSwatch = new DragForm();

			this.AllowDrop = true;

		} 
		
		protected override void OnLoad(EventArgs e) {

			base.OnLoad( e );

			if ( File.Exists( m_customSwatchesFile ) ) {				
				m_swatches = ColorSwatchXml.ReadSwatches( m_customSwatchesFile, false );
			} else {

				CreateCustomSwatchesFile();			
				m_swatches = ColorSwatchXml.ReadSwatches( "ColorSwatches.xml", true );

			}

			m_swatchOuterRegionWidth = this.Width - ( ( this.Width - ( 2 * OUTER_PADDING ) ) ) % ( SWATCH_WIDTH + PADDING );
			m_swatchOuterRegionHeight = this.Height - ( ( this.Height - ( 2 * OUTER_PADDING ) ) ) % ( SWATCH_HEIGHT + PADDING );

			m_numberOfHorizontalSwatches = (  m_swatchOuterRegionWidth - ( 2 * OUTER_PADDING ) ) / ( SWATCH_WIDTH + PADDING );
			m_numberOfVerticalSwatches = ( m_swatchOuterRegionHeight - ( 2 * OUTER_PADDING ) ) / ( SWATCH_HEIGHT + PADDING );

			m_swatchArray = new ColorSwatch[ m_numberOfHorizontalSwatches * m_numberOfVerticalSwatches ];
			
		}

		/// <summary>
		/// Creates the custom swatches file that is stored on the user's local
		/// drive.
		/// </summary>

		private void CreateCustomSwatchesFile() {

			StreamReader fileStreamRead = new StreamReader( Resources.GetFileResource( "ColorSwatches.xml" ) );
				
			if ( fileStreamRead != null ) {

				StreamWriter fileStreamWrite = null;
					
				try {
		
					fileStreamWrite = new StreamWriter( m_customSwatchesFile );
					fileStreamWrite.Write( fileStreamRead.ReadToEnd() );
					fileStreamWrite.Flush();
	
				} finally {
						
					fileStreamRead.Close();

					if ( fileStreamWrite != null ) {
						fileStreamWrite.Close();
					}

				}

			}

		}

		/// <summary>
		/// Draws a color swatch.
		/// </summary>
		/// <param name="g">The graphics object (device context) that the 
		/// swatch is to be drawn on.</param>
		/// <param name="swatchIndex">The swatch index.</param>

		private void DrawColorSwatch( Graphics g, int swatchIndex ) {

			PaintSwatch( g, swatchIndex, m_swatchArray[ swatchIndex ].Color );
					
			g.DrawRectangle( 
				Pens.Black, 
				m_swatchArray[ swatchIndex ].Location.X, 
				m_swatchArray[ swatchIndex ].Location.Y, 
				SWATCH_HEIGHT, 
				SWATCH_WIDTH );

		}

		/// <summary>
		/// Draws an empty color swatch.
		/// </summary>
		/// <param name="g">The graphics object (device context) that the 
		/// swatch is to be drawn on.</param>
		/// <param name="swatchIndex">The swatch index.</param>

		private void DrawEmptyColorSwatch( Graphics g, int swatchIndex ) {

			PaintSwatch( g, swatchIndex, this.BackColor );

			g.DrawRectangle( 
				Pens.DarkGray,
				m_swatchArray[ swatchIndex ].Location.X, 
				m_swatchArray[ swatchIndex ].Location.Y, 
				SWATCH_HEIGHT, 
				SWATCH_WIDTH );
		
		}

		/// <summary>
		/// Paint the swatch with the desired color.
		/// </summary>
		/// <param name="g">The graphics object (device context) that the 
		/// swatch is to be drawn on.</param>
		/// <param name="swatchIndex">The swatch index.</param>
		/// <param name="color">The color with which the swatch is to be 
		/// filled.</param>

		private void PaintSwatch( Graphics g, int swatchIndex, Color color ) {

			using ( SolidBrush sb = new SolidBrush( color ) ) {
				
				g.FillRectangle( 
					sb, 
					m_swatchArray[ swatchIndex ].Location.X, 
					m_swatchArray[ swatchIndex ].Location.Y, 
					SWATCH_HEIGHT, 
					SWATCH_WIDTH );
			
			}

		}

		/// <summary>
		/// Builds the swatch bitmap. Instead of attempting to persistently 
		/// paint on top of the control, I decided to use a bitmap instead.
		/// This also makes things a lot easier when updating the swatches.
		/// </summary>
		/// <returns>A reference to a bitmap object containing painted 
		/// swatches.</returns>
		
		private Bitmap BuildSwatchBitmap() {

			int x = m_startX;
			int y = m_startY;			
			bool isFull = false;

			int swatchColumnNum = 0;
			int swatchRowNum = 0;
			int count = 0;
			
			ColorSwatch[] swatches = m_swatches.ToArray();

			Bitmap swatchBitmap = new Bitmap( m_swatchOuterRegionWidth, m_swatchOuterRegionHeight );

			using ( Graphics g = Graphics.FromImage( swatchBitmap ) ) {

				for ( int i=0; i < swatches.Length; i++ ) {
					
					swatches[i].Location = new Point( x, y );
					swatches[i].Size = m_swatchSize;

					m_swatchArray[ count++ ] = swatches[ i ];

					DrawColorSwatch( g, i );

					UpdatePositions( ref x, ref y, ref swatchColumnNum, ref swatchRowNum );
				
					if ( y + SWATCH_HEIGHT > m_swatchOuterRegionHeight ) {
						isFull = true;
						break;
					}

					m_totalNumberOfSwatches++;
			
				}

				if ( !isFull ) {
					
					m_numberOfEmptySwatches = 0;

					while ( true ) {
						
						if ( ++m_numberOfEmptySwatches == 1 ) {
							m_nextEmptySwatchIndex = m_totalNumberOfSwatches;
						}
											
						m_swatchArray[ count ] = new ColorSwatch( new Point( x, y ), m_swatchSize );
						DrawEmptyColorSwatch( g, count );
						count++;

						UpdatePositions( ref x, ref y, ref swatchColumnNum, ref swatchRowNum );
						m_totalNumberOfSwatches++;
						
						if ( y + SWATCH_HEIGHT > m_swatchOuterRegionHeight ) {
							isFull = true;
							break;
						}


					}

				}
			
			}

			return swatchBitmap;

		}

		/// <summary>
		/// Helper method that updates swatch positions. 
		/// </summary>
		/// <param name="x">X-coordinate (passed by reference).</param>
		/// <param name="y">Y-coordinate (passed by reference).</param>
		/// <param name="swatchColumnNum">The current swatch column (passed by 
		/// reference).</param>
		/// <param name="swatchRowNum">The current swatch row (passed by 
		/// reference).</param>
		
		private void UpdatePositions( ref int x, ref int y, ref int swatchColumnNum, ref int swatchRowNum ) {
			
			if ( x + 2 * ( SWATCH_WIDTH + PADDING ) > m_swatchOuterRegionWidth ) {

				x = m_startX;
				y += SWATCH_HEIGHT + PADDING;

				swatchColumnNum = 0;
				swatchRowNum++;

			} else {
				swatchColumnNum++;
				x += SWATCH_WIDTH + PADDING;
			}
	
		}
	
		/// <summary>
		/// Overrides the control's Paint method to perform the necessary 
		/// swatch-related painting.
		/// </summary>
		/// <param name="pe">A PaintEventArgs containing the event data.</param>

		protected override void OnPaint(PaintEventArgs pe) {
			
			ControlPaint.DrawBorder3D( pe.Graphics, new Rectangle( 0, 0, m_swatchOuterRegionWidth, m_swatchOuterRegionHeight ) );
		
			if ( m_swatchBitmap == null ) {
				m_swatchBitmap = BuildSwatchBitmap();
			}
			pe.Graphics.DrawImage( m_swatchBitmap, new Rectangle( new Point( 0, 0 ), new Size( m_swatchBitmap.Width, m_swatchBitmap.Height ) ) );
				
			if ( m_paintActiveEmptySwatch ) {

				Rectangle rect = m_swatchArray[ m_nextEmptySwatchIndex ].Region;
				pe.Graphics.DrawRectangle( Pens.Yellow, rect );
			
			}
		
		}

		/// <summary>
		/// Checks to see if the cursor is within the outer portion of the 
		/// swatch grid.
		/// </summary>
		/// <param name="x">Cursor x-coordinate.</param>
		/// <param name="y">Cursor y-coordinate.</param>
		/// <returns>True if the cursor is within the swatch grid borders; 
		/// otherwise, false.</returns>

		private bool IsCursorWithinSwatchGridBorders( int x, int y ) {

			int width = m_swatchOuterRegionWidth - ( 2 * OUTER_PADDING );
			int height = m_swatchOuterRegionHeight - ( 2 * OUTER_PADDING );
			Rectangle r = new Rectangle( m_startX, m_startY, width, height );
			Rectangle c = new Rectangle( x, y, 1, 1 );
			
			return c.IntersectsWith( r );

		}

		/// <summary>
		/// Checks to see if the cursor is intersecting with the color swatch.
		/// </summary>
		/// <param name="swatch">The color swatch.</param>
		/// <param name="x">Cursor x-coordinate.</param>
		/// <param name="y">Cursor y-coordinate.</param>
		/// <returns>True if the cursor is intersecting with the color swatch; 
		/// otherwise, false.</returns>

		private static bool DoesCursorIntersectWithSwatch( ColorSwatch swatch, int x, int y ) {

			Point cursorPoint = new Point( x, y );
			Rectangle cursorRectangle = new Rectangle( cursorPoint, new Size( 1, 1 ) );
			
			return cursorRectangle.IntersectsWith( swatch.Region );

		}

		/// <summary>
		/// Retrieves the swatch column index based on the x-coordinate.
		/// </summary>
		/// <param name="x">The x-coordinate.</param>
		/// <returns>An integer representing the swatch column in the swatch 
		/// grid.</returns>

		private static int GetSwatchColumnIndex( int x ) {
			return ( ( x - OUTER_PADDING ) / ( SWATCH_WIDTH + PADDING ) );
		}

		/// <summary>
		/// Retrieves the swatch row index based on the y-coordinate.
		/// </summary>
		/// <param name="x">The y-coordinate.</param>
		/// <returns>An integer representing the swatch row in the swatch grid.</returns>

		private static int GetSwatchRowIndex( int y ) {
			return ( ( y - OUTER_PADDING ) / ( SWATCH_HEIGHT + PADDING ) );
		}

		/// <summary>
		/// Gets the color swatch at the given coordinate in the swatch grid.
		/// </summary>
		/// <param name="x">Cursor x-coordinate.</param>
		/// <param name="y">Cursor y-coordinate.</param>
		/// <returns>A reference to the ColorSwatch object residing at the 
		/// given coordinate.</returns>

		private ColorSwatch GetColorSwatch( int x, int y ) {
			return m_swatchArray[ GetColorSwatchIndex( x, y ) ];
		}
		
		/// <summary>
		/// Retrieves the color swatch index in the color swatch array based on
		/// the cursor coordinates.
		/// </summary>
		/// <param name="x">Cursor x-coordinate.</param>
		/// <param name="y">Cursor y-coordinate.</param>
		/// <returns>The color swatch index at the given coordinate 
		/// coordinates.</returns>
		
		private int GetColorSwatchIndex( int x, int y ) {
			return ( GetSwatchRowIndex( y ) * m_numberOfHorizontalSwatches ) + GetSwatchColumnIndex( x ); 
		}

		/// <summary>
		/// Overrides the OnMouseDown method.
		/// 
		/// TODO: explain what happens here.
		/// 
		/// </summary>
		/// <param name="e">A MouseEventArgs containing the event data.</param>
		
		protected override void OnMouseDown(MouseEventArgs e) {
			
			base.OnMouseDown (e);

			if ( IsCursorWithinSwatchGridBorders( e.X, e.Y ) ) {

				ColorSwatch swatch = GetColorSwatch( e.X, e.Y ); 

				if ( e.Button == MouseButtons.Left ) {
					
					if ( DoesCursorIntersectWithSwatch( swatch, e.X, e.Y ) && swatch.Color != Color.Empty ) {

// --- DISABLED FOR v0.1 release
//						// update form properties
//						dragFormSwatch.UpdateLocation( this.PointToScreen( swatch.Location ) );
//
//						// calculate the difference between the current cursor position and
//						// the upper left corner of the picCurrentColor picture box. this
//						// allows us to persist the distance between the cursor and the 
//						// upper left corner of the drag form as it is being dragged.
//						dragFormSwatch.CursorXDifference =  Cursor.Position.X - dragFormSwatch.Location.X;
//						dragFormSwatch.CursorYDifference = Cursor.Position.Y - dragFormSwatch.Location.Y;
//
//						dragFormSwatch.BackColor = swatch.Color;
//						dragFormSwatch.ChangeSize( swatch.Size + new Size( 1, 1 ) );
//						dragFormSwatch.ShowForm();
//
//						// update the panel state to show that the left mouse button has 
//						// been pressed.
//						m_isLeftMouseButtonDown = true;
//
//						// store the swatch that is about to be dragged
//						m_currentSwatchIndex = GetColorSwatchIndex( e.X, e.Y );
// -- END
						
						this.Cursor = Cursors.Hand;

					}
							
				} else if ( e.Button == MouseButtons.Right ) {
					m_currentSwatchIndex = GetColorSwatchIndex( e.X, e.Y ); 
				}

			}

		}

		/// <summary>
		/// Overrides the OnMouseMove method.
		/// 
		/// If the left mouse button is down, meaning that a swatch is being 
		/// dragged, the form location is updated with the new coordinates. 
		/// Otherwise, if there is an intersection, a tooltip is displayed.
		///
		/// </summary>
		/// <param name="e">A MouseEventArgs containing the event data.</param>

		protected override void OnMouseMove( MouseEventArgs e ) {
			
			if ( m_isLeftMouseButtonDown ) {

				dragFormSwatch.Location = this.PointToScreen( new Point( e.X - dragFormSwatch.CursorXDifference, e.Y - dragFormSwatch.CursorYDifference) );							
			
			} else if ( IsCursorWithinSwatchGridBorders( e.X, e.Y ) ) {
				
				ColorSwatch swatch = GetColorSwatch( e.X, e.Y ); 

				if ( DoesCursorIntersectWithSwatch( swatch, e.X, e.Y ) && swatch.Color != Color.Empty ) { 

					// hides the tooltip when moving from swatch to swatch.
					if ( !m_lastSwatch.Equals( swatch ) ) {
						this.colorTip.Active = false;
					}

					if ( swatch.Description != null && !swatch.Description.Equals( colorTip.GetToolTip( this ) ) ) {
						this.colorTip.SetToolTip( this, swatch.Description );
					}

					this.colorTip.Active = true;
					m_lastSwatch = swatch;
					this.Cursor = Cursors.Hand;
				
				} else {

					this.Cursor = Cursors.Default;
					this.colorTip.Active = false;
				
				}

			} else {
				this.Cursor = Cursors.Default;
			}

		}
		/// <summary>
		/// Overrides the OnMouseUp method.
		/// 
		/// If the cursor is within a swatch region, if the left mouse button 
		/// was released, the ColorSwatchSelected event is raised to notify 
		/// listeners that a color has been selected. If the right mouse button
		/// was released, a context menu is displayed giving the user the 
		/// option of renaming or deleting the color. If the left mouse button
		/// was released outside of the color swatch grid borders, then the
		/// swatch is deleted.  
		/// 
		/// </summary>
		/// <param name="e">A MouseEventArgs containing the event data.</param>

		protected override void OnMouseUp(MouseEventArgs e) {

			dragFormSwatch.Hide();

			if ( IsCursorWithinSwatchGridBorders( e.X, e.Y ) ) {

				ColorSwatch swatch = GetColorSwatch( e.X, e.Y );
				
				if ( DoesCursorIntersectWithSwatch( swatch, e.X, e.Y ) && swatch.Color != Color.Empty ) {
					
					if ( e.Button == MouseButtons.Right ) {
						this.contextMenu.Show( this, new Point( e.X, e.Y ) );	
					} else {
						OnColorSwatchSelected( new ColorSelectedEventArgs( swatch.Color ) ); 								
					}

				}

			} else if ( m_isLeftMouseButtonDown ) {
				
//				DeleteSwatch();
				m_isLeftMouseButtonDown = false;

			} 
				
		}

		/// <summary>
		/// Raises the ColorSwatchSelected event.
		/// </summary>
		/// <param name="e">A ColorSelectedEventArgs containing the event data,</param>

		protected virtual void OnColorSwatchSelected( ColorSelectedEventArgs e ) {
			
			if ( ColorSwatchSelected != null ) {
				ColorSwatchSelected( this, e );
			}

		}

		/// <summary>
		/// Overrides the OnMouseLeave method to hide the dragged swatch form
		/// if focus is taken away from the application.
		/// </summary>
		/// <param name="e">An EventArgs containing the event data.</param>

		protected override void OnMouseLeave(EventArgs e) {

			dragFormSwatch.Hide();
			base.OnMouseLeave (e);
		
		}

		/// <summary>
		/// Overrides the OnDragEnter method to toggle the empty swatch state
		/// (indicate which swatch will be filled by the color that is 
		/// currently being dragged over the control).
		/// </summary>
		/// <param name="drgevent">A DragEventArgs containing the event data.</param>
		
		protected override void OnDragEnter(DragEventArgs drgevent) {
			
			ToggleEmptySwatchState( true );
			base.OnDragEnter (drgevent);
		
		}

		/// <summary>
		/// Overrides the OnDragLeave method to toggle the empty swatch state
		/// (hides the empty swatch indicator explained in the OnDragEnter 
		/// summary).
		/// </summary>
		/// <param name="e">An EventArgs containing the event data.</param>

		protected override void OnDragLeave(EventArgs e) {
			
			ToggleEmptySwatchState( false );
			base.OnDragLeave (e);
		
		}
	
		/// <summary>
		/// Overrides the OnDragOver method to set the drag effect to 
		/// DragDropEffects.Copy.
		/// </summary>
		/// <param name="e">A DragEventArgs containing the event data.</param>

		protected override void OnDragOver(DragEventArgs e) {
		
			e.Effect = DragDropEffects.Move;
			base.OnDragOver( e );

		} 

		/// <summary>
		/// Overrides the OnDragDrop method to handle the drop. The new color 
		/// is sent to the AddColor method for processing and the drag is 
		/// cancelled.
		/// </summary>
		/// <param name="e">A DragEventArgs containing the event data.</param>

		protected override void OnDragDrop(DragEventArgs e) {
			
			Color c = ( Color ) e.Data.GetData( typeof( Color ) );
			AddColor( c );
			e.Effect = DragDropEffects.None;
				
			base.OnDragDrop( e );

		}

		/// <summary>
		/// Adds the new color to the swatch collection.
		/// </summary>
		/// <param name="c">The color to add to the collection.</param>
		
		private void AddColor( Color c ) {

			bool writeSwatchesToFile = false;
			int swatchToInvalidateIndex = m_nextEmptySwatchIndex;

			if ( DoesColorAlreadyExist( c ) ) {
				MessageBox.Show( this.Parent, "Color already exists.", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error );	
			} else if ( m_numberOfEmptySwatches <= 0 ) {
				MessageBox.Show( this.Parent, "There are no empty swatches available.", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error );	
			} else {

				using( AddNewColorSwatchForm colorForm = new AddNewColorSwatchForm( c ) ) {
				
					colorForm.StartPosition = FormStartPosition.CenterParent;
					colorForm.ShowInTaskbar = false;
				
					if ( colorForm.ShowDialog() == DialogResult.OK ) {

						if ( m_swatchBitmap != null ) {
								
							int x = m_swatchArray[ m_nextEmptySwatchIndex ].Location.X;
							int y = m_swatchArray[ m_nextEmptySwatchIndex ].Location.Y;
					
							using ( Graphics g = Graphics.FromImage( m_swatchBitmap ) ) {
																	
								using ( SolidBrush sb = new SolidBrush( c ) ) {
									g.FillRectangle( sb, x, y, 10, 10 );
								}
											
								g.DrawRectangle( Pens.Black, x, y, 10, 10 );
									
							}
					
							m_numberOfEmptySwatches--;

							m_swatchArray[ m_nextEmptySwatchIndex ].Color = c;
							m_swatchArray[ m_nextEmptySwatchIndex ].Description = colorForm.ColorDescription;
							
							Debug.WriteLine( "Added: " + colorForm.ColorDescription );
							//m_swatches.Add( m_swatchArray[ m_nextEmptySwatchIndex ] );

							m_nextEmptySwatchIndex++;							
							writeSwatchesToFile = true;
								
						}
					
					} 

				}
			
			}

			// it's faster to write the swatches to the file after the modal 
			// dialog has been closed, and it also looks better (bits of the
			// dialog don't hang around while the swatches are being written).
			if ( writeSwatchesToFile ) {				
				ColorSwatchXml.WriteSwatches( String.Format( new CultureInfo( "en-US" ), @"{0}\{1}", Application.StartupPath, m_customSwatchesFile ), m_swatchArray );
			}

			m_paintActiveEmptySwatch = false;
			
			// this is placed here because we don't want to hide the 
			// highlighted box until after the color information has been 
			// entered.
			this.InvalidateSwatch( m_swatchArray[ swatchToInvalidateIndex ].Location );
		
		}

		/// <summary>
		/// Check to see if the color already exists.
		/// </summary>
		/// <param name="c">The color to evaluate.</param>
		/// <returns>True if the color already exists, false otherwise.</returns>
		
		private bool DoesColorAlreadyExist( Color c ) {
			
			bool doesColorAlreadyExist = false;

//			ColorSwatch[] swatches = m_swatches.ToArray() );

			for ( int i=0; i < m_swatchArray.Length; i++ ) {

				if ( m_swatchArray[ i ].Color.Equals( c ) ) {
					
					doesColorAlreadyExist = true;		
					break;

				}

			}

			return doesColorAlreadyExist;

		}

		/// <summary>
		/// Deletes a swatch from the swatch array and updates the swatch 
		/// bitmap.
		/// </summary>
		
		private void DeleteSwatch() {

			if ( m_currentSwatchIndex != -1 ) {
					
				using ( Graphics g = Graphics.FromImage( m_swatchBitmap ) ) {
								
					int max = m_currentSwatchIndex + ( m_totalNumberOfSwatches - 1 ) - m_currentSwatchIndex - m_numberOfEmptySwatches;
					int swatchIndex = 0;

					for ( swatchIndex = m_currentSwatchIndex; swatchIndex <= max; swatchIndex++ ) {

						if ( ( swatchIndex + 1 ) < ( m_swatchArray.Length - 1 ) ) {
							
							m_swatchArray[ swatchIndex ].Color = m_swatchArray[ swatchIndex + 1 ].Color;
							m_swatchArray[ swatchIndex ].Description = m_swatchArray[ swatchIndex + 1 ].Description;
						
						} else {
						
							m_swatchArray[ swatchIndex ].Color = Color.Empty;
							m_swatchArray[ swatchIndex ].Description = String.Empty;

						}
						
						if ( m_swatchArray[ swatchIndex ].Color == Color.Empty ) {
								
							DrawEmptyColorSwatch( g, swatchIndex );
							m_nextEmptySwatchIndex = swatchIndex;

						} else {
							DrawColorSwatch( g, swatchIndex );
						}

						Rectangle invalidationRectangle = m_swatchArray[ swatchIndex ].Region; 
						invalidationRectangle.Inflate( 1, 1 );
						this.Invalidate( invalidationRectangle );

					}

					m_numberOfEmptySwatches++;
												
				}

				m_currentSwatchIndex = -1;
				ColorSwatchXml.WriteSwatches( m_customSwatchesFile, m_swatchArray );

			}
		
		}

		/// <summary>
		/// Toggles the state of the very first empty swatch. If the isActive
		/// parameter is true, then the m_paintActiveEmptySwatch flag is set
		/// and the swatch region is invalidated, which results in a colored 
		/// border being drawn around the swatch.
		/// </summary>
		/// <param name="isActive">A boolean indicating whether or not the 
		/// empty swatch is to be activated.</param>

		private void ToggleEmptySwatchState( bool isActive ) {
			
			if ( m_numberOfEmptySwatches > 0 ) {
		
				m_paintActiveEmptySwatch = isActive;
				InvalidateSwatch();

			} else {
				m_paintActiveEmptySwatch = false;
			}
		
		}

		/// <summary>
		/// Invalidates the very first empty swatch.
		/// </summary>

		private void InvalidateSwatch() {
			InvalidateSwatch( m_swatchArray[ m_nextEmptySwatchIndex ].Location );
		}

		/// <summary>
		/// Invalidates the swatch at the given point.
		/// </summary>
		/// <param name="swatchPoint">The point at which the swatch resides.</param>
		
		private void InvalidateSwatch( Point swatchPoint ) {
			
			Rectangle invalidationRegion = new Rectangle( swatchPoint, m_swatchSize );
			invalidationRegion.Inflate( 1, 1 );
			
			this.Invalidate( invalidationRegion );

		}
		
		/// <summary>
		/// Handles the Click event raised by the deleteSwatchMenuItem object 
		/// to delete the current swatch.
		/// </summary>
		/// <param name="sender">The MenuItem that raised this event.</param>
		/// <param name="e">An EventArgs containing the event data.</param>

		private void deleteSwatchMenuItem_Click(object sender, EventArgs e) {
			DeleteSwatch();
		}

		/// <summary>
		/// Handles the Click event raised by the deleteSwatchMenuItem object 
		/// to rename the current swatch.
		/// </summary>
		/// <param name="sender">The MenuItem that raised this event.</param>
		/// <param name="e">An EventArgs containing the event data.</param>

		private void renameSwatchMenuItem_Click(object sender, EventArgs e) {

			using( AddNewColorSwatchForm colorForm = new AddNewColorSwatchForm( m_swatchArray[ m_currentSwatchIndex ] ) ) {
				
				colorForm.StartPosition = FormStartPosition.CenterParent;
				colorForm.ShowInTaskbar = false;
				
				if ( colorForm.ShowDialog() == DialogResult.OK ) {					

					m_swatchArray[ m_currentSwatchIndex ].Description = colorForm.ColorDescription;
					ColorSwatchXml.WriteSwatches( m_customSwatchesFile, m_swatchArray );
					
				} 

			}

		}


		#region Dispose

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing ) {
			if( disposing ) {
				if( components != null )
					components.Dispose();
			}
			base.Dispose( disposing );
		}

		#endregion Dispose

		#region Component Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			
			this.components = new System.ComponentModel.Container();
			this.colorTip = new System.Windows.Forms.ToolTip(this.components);
			this.deleteSwatchMenuItem = new MenuItem( "Delete swatch" );
			this.deleteSwatchMenuItem.Click +=new EventHandler(deleteSwatchMenuItem_Click);

			this.renameSwatchMenuItem = new MenuItem( "Rename swatch" );
			this.renameSwatchMenuItem.Click += new EventHandler(renameSwatchMenuItem_Click);
			this.contextMenu = new System.Windows.Forms.ContextMenu( new MenuItem[] { renameSwatchMenuItem, deleteSwatchMenuItem } );

			// 
			// colorTip
			// 
			this.colorTip.Active = false;

		}
		#endregion

	} // ColorSwatchPanel

} // Sano.PersonalProjects.ColorPicker.Controls
