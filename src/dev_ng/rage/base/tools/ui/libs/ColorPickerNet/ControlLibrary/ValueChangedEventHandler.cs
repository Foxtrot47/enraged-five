
using System;

namespace Sano.PersonalProjects.ColorPicker.Controls {
	internal delegate void ValueChangedEventHandler( object sender, ValueChangedEventArgs e );
} // Sano.PersonalProjects.ColorPicker.Controls