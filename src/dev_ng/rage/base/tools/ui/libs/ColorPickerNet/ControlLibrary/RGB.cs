
using System;

namespace Sano.PersonalProjects.ColorPicker.Controls {

	// This class stores all relevant RGB data.

	[ColorSpaceStructure( ColorSpaces.RedGreenBlue ) ]
	internal sealed class RGB {
		
		// private data fields
		private int m_red;
		private int m_green;
		private int m_blue;

		/// <summary>
		/// Gets or sets the red coordinate.
		/// </summary>

		internal int Red {
			get { return m_red; }
			set { m_red = value; }
		}

		/// <summary>
		/// Gets or sets the green coordinate.
		/// </summary>

		internal int Green {
			get { return m_green; }
			set { m_green = value; }
		}

		/// <summary>
		/// Gets or sets the blue coordinate.
		/// </summary>

		internal int Blue {
			get { return m_blue; }
			set { m_blue = value; }
		}

		/// <summary>
		/// Constructor. Provides initial values for the coordinates in the
		/// RGB color space.
		/// </summary>
		/// <param name="red">The red coordinate.</param>
		/// <param name="green">The green coordinate.</param>
		/// <param name="blue">The blue coordinate.</param>

		internal RGB( int red, int green, int blue ) {
			
			m_red = red;
			m_green = green;
			m_blue = blue;

		}

		/// <summary>
		/// Returns a string representation of the current object.
		/// </summary>
		/// <returns>A string representation of the current object.</returns>

		public override string ToString() {
			return String.Format( "Red: {0}; Green: {1}; Blue: {2} ", Red, Green, Blue );
		}

		/// <summary>
		/// Determines whether or not two objects are equal.
		/// </summary>
		/// <param name="obj">The object that is to be compared to the current 
		/// object.</param>
		/// <returns>True if the two objects are equal; otherwise false.</returns>

		public override bool Equals( object obj ) {
			
			bool equal = false;
			RGB rgb = obj as RGB;

			if ( rgb != null ) {
				
				if ( this.Red == rgb.Red && this.Blue == rgb.Blue && this.Green == rgb.Green ) {
					equal = true;
				}
			
			}

			return equal;

		}

		/// <summary>
		/// Returns the hash code.
		/// </summary>
		/// <returns>The hash code.</returns>

		public override int GetHashCode() {
			return base.GetHashCode ();
		}

	} // RGB

} // Sano.PersonalProjects.ColorPicker.Controls
