
using System;

namespace Sano.PersonalProjects.ColorPicker.Controls {

	// This attribute is used in place of a marker interface (an interface
	// without members).

	internal sealed class ColorSpaceStructureAttribute : Attribute {

		private ColorSpaces m_colorSpace;
		
		/// <summary>
		/// Returns the color space that is represented by this attribute.
		/// </summary>

		public ColorSpaces ColorSpace {
			get { return m_colorSpace; }
		}

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="colorSpace">The ColorSpaces item that this attribute
		/// represents.</param>

		internal ColorSpaceStructureAttribute( ColorSpaces colorSpace ) { 
			m_colorSpace = colorSpace;
		}

	} // ColorSpaceAttribute

} // Sano.PersonalProjects.ColorPicker.Controls