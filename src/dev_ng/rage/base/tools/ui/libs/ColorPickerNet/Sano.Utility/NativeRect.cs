
using System;

namespace Sano.Utility.NativeMethods {

	public struct NativeRect {
	
		public int left;
		public int top;
		public int right;
		public int bottom;
	
	} // NativeRect

} // Sano.Utility.NativeMethods