
using System;
using System.Runtime.InteropServices;

namespace Sano.Utility {

	public class Internet {

		[DllImport("wininet.dll", SetLastError = true)]
		private extern static bool InternetGetConnectedState( out int Description, int ReservedValue ) ;

		public static bool IsMachineConnectedToInternet() {
			
			int desc;
			return InternetGetConnectedState( out desc, 0 );
			
		}

	} // Internet
	
} // Sano.Utility
