
using System;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace Sano.PersonalProjects.ColorPicker.Controls {

	internal class ColorSlider : UserControl {
		
		public event ValueChangedEventHandler ValueChanged;

		// private data members
		private bool m_isLeftMouseButtonDown;
		private bool m_isLeftMouseButtonDownAndMoving;
		private int m_currentArrowYLocation;
		private Rectangle m_leftArrowRegion;
		private Rectangle m_rightArrowRegion;

		// readonly
		private readonly Rectangle m_colorRegion = new Rectangle( 13, 7, 18, 256 );
		private readonly Rectangle m_outerRegion = new Rectangle( 10, 4, 26, 264 );

		// constants
		private const int POINTER_HEIGHT = 10;
		private const int POINTER_WIDTH = 6;

		public int Value 
        {			
			get 
            {
                int val = (255 - m_currentArrowYLocation) + m_colorRegion.Top;
                return Math.Max( 0, Math.Min( 255, val ) );
            }
			set 
            {
                int val = Math.Max( 0, Math.Min( 255, value ) );
				m_currentArrowYLocation = ( int )( m_colorRegion.Top + ( 255 - val ) );
				this.InvalidateArrowRegions( m_currentArrowYLocation );								
			}			
		}

		public bool IsAlphaSlider {
			get { return m_IsAlphaSlider; }
			set { m_IsAlphaSlider = value;	}
		}
		private bool m_IsAlphaSlider;

		/// <summary>
		/// Constructor.
		/// </summary>
		
		internal ColorSlider() {			
			
			m_currentArrowYLocation = m_colorRegion.Top;
			this.SetStyle( ControlStyles.DoubleBuffer | ControlStyles.AllPaintingInWmPaint | ControlStyles.UserPaint, true );

		}

		protected override void OnPaint(PaintEventArgs e) {

			base.OnPaint (e);

			Graphics g = e.Graphics;

			CreateLeftTrianglePointer( g, m_currentArrowYLocation );
			CreateRightTrianglePointer( g, m_currentArrowYLocation ); 

			if (m_IsAlphaSlider) {
				UpdateColorRegionAlpha(g);
			}
			else {
				UpdateColorRegion( g );
			}

			ControlPaint.DrawBorder3D( g, m_outerRegion );
			g.DrawRectangle( Pens.Black, m_colorRegion );

		}
		
		protected override void OnMouseDown(MouseEventArgs e) {
					
			if ( e.Button == MouseButtons.Left ) {
							
				m_isLeftMouseButtonDown = true;
				CheckCursorYRegion( e.Y );		
				
			}

			base.OnMouseDown (e);
				
		}

		protected override void OnMouseMove(MouseEventArgs e) {

			if ( m_isLeftMouseButtonDown ) {	

				m_isLeftMouseButtonDownAndMoving = true;
				CheckCursorYRegion( e.Y );			
			
			}
			
			base.OnMouseMove (e);

		}

		protected override void OnMouseUp(MouseEventArgs e) {

			m_isLeftMouseButtonDown = false;
			m_isLeftMouseButtonDownAndMoving = false;

			base.OnMouseUp (e);

		}

		/// <summary>
		/// Calculates the points needed to draw the left triangle pointer for
		/// the value strip.
		/// </summary>
		/// <param name="g">Graphics object.</param>
		/// <param name="y">Current cursor y-value.</param>

		private void CreateLeftTrianglePointer( Graphics g, int y ) {

			Point[] points = { 
								 new Point( m_outerRegion.Left - POINTER_WIDTH - 1, y - ( POINTER_HEIGHT / 2 ) ), 
								 new Point( m_outerRegion.Left - 2, y ), 
								 new Point( m_outerRegion.Left - POINTER_WIDTH - 1, y + ( POINTER_HEIGHT / 2 ) ) };
			
			g.DrawPolygon( Pens.Black, points );
		
		}

		/// <summary>
		/// Calculates the points needed to draw the right triangle pointer for
		/// the color slider.
		/// </summary>
		/// <param name="g">Graphics object.</param>
		/// <param name="y">Current cursor y-value.</param>
		
		private void CreateRightTrianglePointer( Graphics g, int y ) {

			Point[] points = { 
								 new Point( m_outerRegion.Right - 1 + POINTER_WIDTH, y - ( POINTER_HEIGHT / 2 ) ), 
								 new Point( m_outerRegion.Right, y ), 
								 new Point( m_outerRegion.Right - 1 + POINTER_WIDTH, y + ( POINTER_HEIGHT / 2 ) ) };
			
			g.DrawPolygon( Pens.Black, points );
			
		}
		
		/// <summary>
		/// Determines the color slider left triangle pointer invalidation 
		/// region.
		/// </summary>
		/// <param name="arrowY">Current cursor y-value.</param>
		/// <returns>A rectangle object representing the area to be 
		/// invalidated.</returns>

		private Rectangle GetLeftTrianglePointerInvalidationRegion( int arrowY ) {

			int leftPadding = POINTER_WIDTH + 2;
			int x = m_outerRegion.Left - leftPadding;
			int y = arrowY - ( POINTER_HEIGHT / 2 ) - 1;
			int width = POINTER_WIDTH + 2;
			int height = POINTER_HEIGHT + 3;
			
			return new Rectangle( x, y, width, height );

		}
		
		/// <summary>
		/// Determines the color slider right triangle pointer invalidation 
		/// region.
		/// </summary>
		/// <param name="arrowY">Current cursor y-value</param>
		/// <returns>A rectangle object representing the area to be 
		/// invalidated.</returns>
		
		private Rectangle GetRightTrianglePointerInvalidationRegion( int arrowY ) {

			int x = m_outerRegion.Right;
			int y = arrowY - ( POINTER_HEIGHT / 2 ) - 1;
			int width = POINTER_WIDTH + 2;
			int height = POINTER_HEIGHT + 3;
			
			return new Rectangle( x, y, width, height );

		}

		private void CheckCursorYRegion( int y ) {

			int mValue = y;

			if ( m_isLeftMouseButtonDown && !m_isLeftMouseButtonDownAndMoving ) {

				if ( y < m_colorRegion.Top || y > m_colorRegion.Bottom ) {
					return;
				}

			} else if ( m_isLeftMouseButtonDownAndMoving ) {

				if ( y < m_colorRegion.Top ) {
					mValue = m_colorRegion.Top;
				} else if ( y >= m_colorRegion.Bottom ) {
					mValue = m_colorRegion.Bottom - 1;
				} 

			} else {
				return;
			}
								 
			m_currentArrowYLocation = mValue;

			InvalidateArrowRegions( mValue );

			if ( ValueChanged != null ) 
            {
                int val = 255 - (mValue - m_colorRegion.Top);
                ValueChanged( this, new ValueChangedEventArgs( Math.Max( 0, Math.Min( 255, val ) ) ) );
			}
		}

		private void InvalidateArrowRegions( int y ) {

			this.Invalidate( m_leftArrowRegion );
			this.Invalidate( m_rightArrowRegion );
				
			m_leftArrowRegion = this.GetLeftTrianglePointerInvalidationRegion( y );
			m_rightArrowRegion = this.GetRightTrianglePointerInvalidationRegion( y );

			this.Invalidate( m_leftArrowRegion );
			this.Invalidate( m_rightArrowRegion );

		}

		private ColorSpace m_colorSpace;

		public void UpdateColor( ColorSpace colorSpace ) {
			
			m_colorSpace = colorSpace;
//			// added 06-07-05
//			this.Value = m_colorSpace.SelectedComponent.Value;
			this.Invalidate( m_colorRegion );

		}


		private void UpdateColorRegionAlpha(Graphics g) {
			RGB rgb;

			if (m_colorSpace != null) 
			{
				if (ColorSpaceStructureEvaluator.IsRgb(m_colorSpace.Structure))
				{
					rgb = (RGB) m_colorSpace.Structure;
				}
				else 
				{
					rgb = ColorConverter.HsbToRgb((HSB)m_colorSpace.Structure);
				}
			

				using (HatchBrush hb = new HatchBrush(HatchStyle.LargeCheckerBoard, Color.White, Color.FromArgb(255,204,204,204))) 
				{
					g.FillRectangle(hb, m_colorRegion);
				}

				Color startColor = Color.FromArgb(0, rgb.Red, rgb.Green, rgb.Blue);
				Color endColor = Color.FromArgb(255, rgb.Red, rgb.Green, rgb.Blue);

				using (LinearGradientBrush lgb = new LinearGradientBrush(m_colorRegion, startColor, endColor, 270f)) 
				{
					g.FillRectangle(lgb, m_colorRegion);
				}
			}
		}

		/// <summary>
		/// Updates the color slider.
		/// </summary>
		/// 
        private void UpdateColorRegion( Graphics g ) {

			RGB rgb;
			HSB hsb;

			if ( m_colorSpace != null ) { 

				if ( ColorSpaceStructureEvaluator.IsRgb( m_colorSpace.Structure ) ) {
				
					rgb = ( RGB ) m_colorSpace.Structure;
					hsb = ColorConverter.RgbToHsb( rgb );

				} else {

					hsb = ( HSB ) m_colorSpace.Structure;
					rgb = ColorConverter.HsbToRgb( hsb );

				}
				
				if ( m_colorSpace is RgbColorSpace ) {
						
					char dChar = m_colorSpace.SelectedComponent.DisplayCharacter;
					int red = rgb.Red;
					int green = rgb.Green;
					int blue = rgb.Blue;
						
					Color startColor;
					Color endColor;

					switch ( dChar ) {
							
						case 'R':
							startColor = Color.FromArgb( 0, green, blue );
							endColor = Color.FromArgb( 255, green, blue );
							break;

						case 'G':
							startColor = Color.FromArgb( red, 0, blue );
							endColor = Color.FromArgb( red, 255, blue );
							break;

						default:
							startColor = Color.FromArgb( red, green, 0 );
							endColor = Color.FromArgb( red, green, 255 );
							break;

					}

					using ( LinearGradientBrush lgb = new LinearGradientBrush( m_colorRegion, startColor, endColor, 270f ) ) {
						g.FillRectangle( lgb, m_colorRegion );
					}
					
				} else if ( m_colorSpace is HsbColorSpace ) {
					
					if ( m_colorSpace.SelectedComponent.DisplayCharacter == 'H' ) { 

						using ( LinearGradientBrush brBrush = new LinearGradientBrush( m_colorRegion, Color.Blue, Color.Red, 90f, false ) ) {

							Color[] colorArray = { Color.Red, Color.Magenta, Color.Blue, Color.Cyan, Color.FromArgb( 0, 255, 0 ), Color.Yellow, Color.Red };
							float[] posArray = { 0.0f, 0.1667f, 0.3372f, 0.502f, 0.6686f, 0.8313f, 1.0f };

							ColorBlend colorBlend = new ColorBlend();
							colorBlend.Colors = colorArray;
							colorBlend.Positions = posArray;
							brBrush.InterpolationColors = colorBlend;

							g.FillRectangle( brBrush, m_colorRegion );
								
						}

					} else if ( m_colorSpace.SelectedComponent.DisplayCharacter == 'B' ) {
							
						RGB sRgb = ColorConverter.HsbToRgb( new HSB( hsb.Hue, hsb.Saturation, 100 ) );
						RGB eRgb = ColorConverter.HsbToRgb( new HSB( hsb.Hue, hsb.Saturation, 0 ) );
							
						using ( LinearGradientBrush lgb = new LinearGradientBrush( m_colorRegion, Color.FromArgb( sRgb.Red, sRgb.Green, sRgb.Blue ), Color.FromArgb( eRgb.Red, eRgb.Green, eRgb.Blue ), 90f ) ) {
							g.FillRectangle( lgb, m_colorRegion );
						}

					} else {

						RGB sRgb = ColorConverter.HsbToRgb( new HSB( hsb.Hue, 100, hsb.Brightness ) );
						RGB eRgb = ColorConverter.HsbToRgb( new HSB( hsb.Hue, 0, hsb.Brightness ) );

						using ( LinearGradientBrush lgb = new LinearGradientBrush( m_colorRegion, Color.FromArgb( sRgb.Red, sRgb.Green, sRgb.Blue ), Color.FromArgb( eRgb.Red, eRgb.Green, eRgb.Blue ), 90f ) ) {
							g.FillRectangle( lgb, m_colorRegion );
						}

					}

				}

			}
		
		}

	} // ColorSlider

} // Sano.PersonalProjects.ColorPicker.Controls