
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using System.Diagnostics;

namespace Sano.PersonalProjects.ColorPicker.Controls {

	internal class ColorBox : UserControl {

		// controls
		private DragForm dragForm;

		// private data members
		private bool m_isActive;
		private RGB m_rgbColor;

		/// <summary>
		/// Gets or sets a boolean value representing whether or not this 
		/// instance is the active color box.
		/// </summary>

		[Browsable(false)]
		internal bool IsActive {
			get { return m_isActive; }
			set { 

				m_isActive = value; 
				this.Invalidate();
			
			}
		}
		
//		/// <summary>
//		/// Gets or sets the RGB color value of the background color of the 
//		/// control.
//		/// </summary>
//		
//		internal RGB RgbColor {
//			get { return m_rgbColor; }
//			set { 
//				
//				m_rgbColor = value; 
//				Debug.WriteLine
//				this.BackColor = ColorConverter.RgbToColor( m_rgbColor );
//			
//			}
//		}

		/// <summary>
		/// Constructor.
		/// </summary>

		internal ColorBox() : base() {
			this.AllowDrop = true;
			m_rgbColor = new RGB( 0, 0, 0 );
		}

		/// <summary>
		/// Overrides the painting instructions set forth by the base class.
		/// </summary>
		/// <param name="e">A PaintEventArgs containing the event data.</param>

		protected override void OnPaint(PaintEventArgs e) {
			
			using ( Graphics g = e.Graphics ) {

				if ( this.IsActive ) {
					
					using ( Pen p = new Pen( Brushes.Black, 2f ) ) {
						g.DrawRectangle( p, 1, 1, this.Width - 2, this.Height - 2 );
					}
				
				} else {
					g.DrawRectangle( Pens.Black, 0, 0, this.Width - 1, this.Height - 1 );
				}
			
			}

		}

		/// <summary>
		/// Overrides the base class' OnLoad method and instantiates a new
		/// DragForm object that will be used to create the visual drag effect
		/// when adding the currently selected color to the color swatch panel.
		/// </summary>
		/// <param name="e">An EventArgs that contains the event data.</param>

		protected override void OnLoad( EventArgs e ) {
			dragForm = new DragForm();
		}

		/// <summary>
		/// Overrides the OnMouseDown event method to implement custom event 
		/// logic. The DragForm instance is updated and the drag-and-drop 
		/// operation is initiated.
		/// </summary>
		/// <param name="e">A MouseEventArgs that contains the event data.</param>

		protected override void OnMouseDown(MouseEventArgs e) {

			base.OnMouseDown( e );

			if ( e.Button == MouseButtons.Left ) {

				// update form properties
				dragForm.UpdateLocation( this.Parent.PointToScreen( this.Location ) );

				// calculate the difference between the current cursor position and
				// the upper left corner of the this picture box. this
				// allows us to persist the distance between the cursor and the 
				// upper left corner of the drag form as it is being dragged.
				
				dragForm.CursorXDifference =  Cursor.Position.X - dragForm.Location.X;
				dragForm.CursorYDifference = Cursor.Position.Y - dragForm.Location.Y;

				dragForm.BackColor = this.BackColor;
				dragForm.ChangeSize( this.Size );
				dragForm.ShowForm();

				// initiate drag
				this.DoDragDrop( this.BackColor, DragDropEffects.Move );
			
			}
		
		}

		/// <summary>
		/// Handles the GetFeedback behavior established by the base class. We 
		/// don't want to use the default cursors (I think they are intrusive). 
		/// Setting the UseDefaultCursors property of the GiveFeedbackEventArgs
		/// object and setting the cursor to the hand cursor takes care of this
		/// for us.
		/// </summary>
		/// <param name="sender"> The object that raised the event.</param>
		/// <param name="e">A GiveFeedbackEventArgs containing the event data.</param>

		protected override void OnGiveFeedback( GiveFeedbackEventArgs e ) {
		
			e.UseDefaultCursors = false;
			Cursor.Current = Cursors.Hand;

			base.OnGiveFeedback( e );
							
		}

		/// <summary>
		/// Overrides the default QueryContinueDrag behavior established by the
		/// base class. If the Action is anything other than Cancel or Drop then 
		/// the location of the transparent form is updated.
		/// </summary>
		/// <param name="sender">The object that raised the event.</param>
		/// <param name="e">A QueryContinueDragEventArgs containing the event
		/// data.</param>

		protected override void OnQueryContinueDrag( QueryContinueDragEventArgs e ) {

			if ( e.Action == DragAction.Cancel || e.Action == DragAction.Drop ) {
				dragForm.Hide();
			} else {
				dragForm.Location = new Point( Cursor.Position.X - dragForm.CursorXDifference, Cursor.Position.Y - dragForm.CursorYDifference );
			}
			
			base.OnQueryContinueDrag( e );
		
		}

		/// <summary>
		/// Overrides the default OnDragOver behavior established by the base class.
		/// Checks to see if the dragged data is of type Color and contains a 
		/// different color definition than the background of the current instance.
		/// </summary>
		/// <param name="e">A DragEventArgs containing the event data.</param>

		protected override void OnDragOver( DragEventArgs e ) {
			
			object dragData = e.Data.GetData( typeof( Color ) );

			if ( dragData != null && !( ( Color ) dragData ).Equals( this.BackColor ) ) {
				e.Effect = DragDropEffects.Move;
			}
			
			base.OnDragOver( e );

		}

		/// <summary>
		/// Overrides the default OnDragDrop behavior established by the base 
		/// class. The background color of the current instance gets updated
		/// with the value of the dragged color object.
		/// </summary>
		/// <param name="e">A DragEventArgs containing the event data.</param>

		protected override void OnDragDrop( DragEventArgs e ) {

			base.OnDragDrop( e );
			
			this.BackColor = ( Color ) e.Data.GetData( typeof( Color ) );
			e.Effect = DragDropEffects.None;
			
		}

		#region Dispose

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>

		protected override void Dispose(bool disposing) {

			if ( disposing ) {
			
				if ( dragForm != null ) {
					dragForm.Dispose();
				}

			}

			base.Dispose (disposing);
		
		}

		#endregion Dispose

	} // CurrentColorBox

} // Sano.PersonalProjects.ColorPicker.Controls