
using System;
using System.IO;
using System.Xml.Serialization;
using Sano.PersonalProjects.ColorPicker.Controls;

namespace Sano.PersonalProjects.ColorPicker.Main {

	/// <summary>
	/// Holds all application related preferences.
	/// </summary>

	[Serializable]
	public struct ApplicationData {

		// private data fields
		private bool m_minimizeApplicationToSystemTray;
		private bool m_saveSettingsOnApplicationClose;
		private ColorPanelSettings m_colorPanelSettings;

		/// <summary>
		/// Gets or sets a value that determines whether or not the application
		/// is to be minimized to the system tray.
		/// </summary>

		public bool MinimizeApplicationToSystemTray {
			get { return m_minimizeApplicationToSystemTray; }
			set { m_minimizeApplicationToSystemTray = value; }
		}

		/// <summary>
		/// Gets or sets a value that determines whether or not the application
		/// settings are to be saved to disk when the application is closed.
		/// </summary>

		public bool SaveSettingsOnApplicationClose {
			get { return m_saveSettingsOnApplicationClose; }
			set { m_saveSettingsOnApplicationClose = value; }
		}

		public ColorPanelSettings ColorPanelSettings {
			get { return m_colorPanelSettings; }
			set { m_colorPanelSettings = value; }
		}

	} // ApplicationData

} // Sano.PersonalProjects.ColorPicker.Main