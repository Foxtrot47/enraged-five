
using System;
using System.Runtime.InteropServices;

namespace Sano.Utility.NativeMethods {

	public class User32 {
		
		[DllImport("User32.dll", SetLastError = true)]
		private static extern bool StretchBlt( 
			IntPtr destDC,
			int dX, 
			int dY, 
			int dWidth,
			int dHeight,
			IntPtr sourceDC,
			int sX, 
			int sY,  
			int sWidth,
			int sHeight,
			int opCode );

		[DllImport("user32.dll", SetLastError = true)]
		public static extern IntPtr GetDC( IntPtr windowHandle );
		
		[DllImport("user32.dll", SetLastError = true)]
		public static extern IntPtr GetWindowDC( IntPtr windowHandle );
		
		[DllImport("user32.dll", SetLastError = true)]
		public static extern int ReleaseDC( IntPtr windowHandle, IntPtr dc );
		
		[DllImport("user32.dll", SetLastError = true)]
		public static extern bool ShowWindow( IntPtr handle, int message );

		[DllImport("user32.dll", SetLastError = true)]
		public static extern IntPtr FindWindow( string className, string windowName );

		[DllImport("user32.dll", SetLastError = true)]
		public  static extern IntPtr SendMessage( IntPtr hWnd, uint Msg, IntPtr wParam, IntPtr lParam );

		[DllImport("user32.dll", SetLastError = true)]
		public  static extern bool SetForegroundWindow( IntPtr hWnd );

		[DllImport("user32.dll", SetLastError = true)]
		public static extern bool SetWindowPlacement( IntPtr hWnd, [In] ref WindowPlacement lpwndpl );

		[DllImport("user32.dll", SetLastError = true)]
		public  static extern bool GetWindowPlacement( IntPtr hWnd, [In] ref WindowPlacement lpwndpl );

		/// <summary>
		/// Private constructor to ensure that the compiler does not 
		/// automatically generate a public constructor.
		/// </summary>

		private User32() { }

	} // User32

} // Sano.Utility.NativeMethods