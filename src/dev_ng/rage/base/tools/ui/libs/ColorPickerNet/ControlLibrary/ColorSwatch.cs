
using System;
using System.Drawing;
using System.Windows.Forms;

namespace Sano.PersonalProjects.ColorPicker.Controls {
	
	internal struct ColorSwatch {

		// private data fields
		private Color m_color;
		private string m_description;
		private Point m_location;
		private Size m_size;

		/// <summary>
		/// Gets or sets the color swatch description.
		/// </summary>

		internal string Description {
			get { return m_description; }
			set { m_description = value; }
		}

		/// <summary>
		/// Gets or sets the color of this swatch.
		/// </summary>

		internal Color Color {
			get { return m_color; }
			set { m_color = value; }
		}

		/// <summary>
		/// Gets or sets the location of the color swatch in the swatch grid.
		/// </summary>

		internal Point Location {
			get { return m_location; }
			set { m_location = value; }
		}

		/// <summary>
		/// Gets or sets the color swatch size.
		/// </summary>

		internal Size Size {
			get { return m_size; }
			set { m_size = value; }
		}

		/// <summary>
		/// Color swatch region.
		/// </summary>

		internal Rectangle Region {
			get { return new Rectangle( this.Location, this.Size ); }
		}

		/// <summary>
		/// Constructor. Conveys values to the second overloaded constructor.
		/// </summary>
		/// <param name="location">The location of the color swatch in the 
		/// swatch grid.</param>
		/// <param name="size">The size of the swatch.</param>

		internal ColorSwatch( Point location, Size size ) : 
			this( Color.Empty, String.Empty, location, size ) {
		
		}

		internal ColorSwatch( Color color, string description ) : 
			this( color, description, new Point( 0, 0 ), new Size( 10, 10 ) ) {

		}

		internal ColorSwatch( Color color, string description, Point location, Size size ) {
			
			m_color = color;
			m_description = description;
			m_location = location; 
			m_size = size;

		}

		/// <summary>
		/// Determines whether or not two objects are equal.
		/// </summary>
		/// <param name="obj">The object to compare with the current object.</param>
		/// <returns>True if the objects are equal; otherwise, false.</returns>
		
		public override bool Equals( object obj ) {
			
			ColorSwatch swatch = ( ColorSwatch ) obj;
			bool isEquals = false;

			if ( this.Color == swatch.Color && this.Description != null && this.Description.Equals( swatch.Description ) ) {
				isEquals = true;
			}

			return isEquals;
			
		}

		/// <summary>
		/// Overloads the equality operator.
		/// </summary>
		/// <returns>True if the two color swatchs are equal; otherwise false.</returns>

		public static bool operator == ( ColorSwatch swatch1, ColorSwatch swatch2 ) {
			return swatch1.Equals( swatch2 );
		}

		/// <summary>
		/// Overloads the inequality operator.
		/// </summary>
		/// <returns>True if the two color swatches are inequal; otherwise false.</returns>

		public static bool operator != ( ColorSwatch swatch1, ColorSwatch swatch2 ) {
			return !swatch1.Equals( swatch2 );
		}

		/// <summary>
		/// Returns the hash code.
		/// </summary>
		/// <returns>The hash code.</returns>

		public override int GetHashCode() {
			return base.GetHashCode ();
		}

		/// <summary>
		/// Overrides the ToString method to return a string representation of 
		/// the current object. 
		/// </summary>
		/// <returns>A string representation of the current object.</returns>

		public override string ToString() {
			return String.Format( "Description: {0}, Color: {1}", this.Description, this.Color );
		}

	} // ColorSwatch

} // Sano.PersonalProjects.ColorPicker.Controls