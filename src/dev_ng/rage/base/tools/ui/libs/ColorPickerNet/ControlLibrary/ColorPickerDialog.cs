using System;
using System.Runtime.InteropServices;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace Sano.PersonalProjects.ColorPicker.Controls
{
	/// <summary>
	/// Summary description for ColorPickerDialog.
	/// </summary>
	public class ColorPickerDialog : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Button okButton;
		private System.Windows.Forms.Button cancelButton;
        private Sano.PersonalProjects.ColorPicker.Controls.ColorPanel colorPanel1;
        private CheckBox eyeDropperCheckbox;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public ColorPickerDialog()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		public bool UseAlpha
		{
			get {
				return colorPanel1.UseAlpha;
			}
			set {
				colorPanel1.UseAlpha = value;
			}
		}

		public bool UseHex
		{
			get {
				return colorPanel1.UseHex;
			}
			set {
				colorPanel1.UseHex = value;
			}
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ColorPickerDialog));
            this.okButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.colorPanel1 = new Sano.PersonalProjects.ColorPicker.Controls.ColorPanel();
            this.eyeDropperCheckbox = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // okButton
            // 
            this.okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.okButton.Location = new System.Drawing.Point(328, 280);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(75, 23);
            this.okButton.TabIndex = 1;
            this.okButton.Text = "OK";
            // 
            // cancelButton
            // 
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.Location = new System.Drawing.Point(416, 280);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 2;
            this.cancelButton.Text = "Cancel";
            // 
            // colorPanel1
            // 
            this.colorPanel1.CurrentColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.colorPanel1.Location = new System.Drawing.Point(0, 0);
            this.colorPanel1.Name = "colorPanel1";
            this.colorPanel1.Size = new System.Drawing.Size(499, 272);
            this.colorPanel1.TabIndex = 4;
            this.colorPanel1.UseAlpha = true;
            this.colorPanel1.UseHex = false;
            this.colorPanel1.UseSwatches = false;
            this.colorPanel1.ColorChanged += new Sano.PersonalProjects.ColorPicker.Controls.ColorSelectedEventHandler(this.colorPanel1_ColorChanged);
            // 
            // eyeDropperCheckbox
            // 
            this.eyeDropperCheckbox.Appearance = System.Windows.Forms.Appearance.Button;
            this.eyeDropperCheckbox.AutoSize = true;
            this.eyeDropperCheckbox.Image = global::Sano.PersonalProjects.ColorPicker.Controls.Properties.Resources.EyeDropper;
            this.eyeDropperCheckbox.Location = new System.Drawing.Point(12, 278);
            this.eyeDropperCheckbox.Name = "eyeDropperCheckbox";
            this.eyeDropperCheckbox.Size = new System.Drawing.Size(22, 22);
            this.eyeDropperCheckbox.TabIndex = 5;
            this.eyeDropperCheckbox.UseVisualStyleBackColor = true;
            this.eyeDropperCheckbox.MouseDown += new System.Windows.Forms.MouseEventHandler(this.eyeDropperCheckbox_MouseDown);
            this.eyeDropperCheckbox.MouseUp += new System.Windows.Forms.MouseEventHandler(this.eyeDropperCheckbox_MouseUp);
            this.eyeDropperCheckbox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.eyeDropperCheckbox_KeyDown);
            // 
            // ColorPickerDialog
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(498, 311);
            this.Controls.Add(this.eyeDropperCheckbox);
            this.Controls.Add(this.colorPanel1);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.okButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ColorPickerDialog";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "Color Picker";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.ColorPickerDialog_Load);
            this.Closed += new System.EventHandler(this.ColorPickerDialog_Closed);
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion

		public event ColorSelectedEventHandler ColorChanged;
		public event EventHandler SimpleColorChanged;

		public Color CurrentColor {
			get {
				return colorPanel1.CurrentColor;
			}
			set {
				colorPanel1.CurrentColor = value;
			}
		}

		private Color m_OriginalColor;

		private void ColorPickerDialog_Load(object sender, System.EventArgs e)
		{
			m_OriginalColor = CurrentColor;
		}

		private void ColorPickerDialog_Closed(object sender, System.EventArgs e)
		{
			if (this.DialogResult != DialogResult.OK) {
				CurrentColor = m_OriginalColor;
			}
		}

		private void colorPanel1_ColorChanged(object sender, Sano.PersonalProjects.ColorPicker.Controls.ColorSelectedEventArgs e)
		{
			if (ColorChanged != null) {
				ColorChanged(this, e);
			}
			if (SimpleColorChanged != null) {
				SimpleColorChanged(this, System.EventArgs.Empty);
			}
		}

        private Color eyeDropperOriginalColor = new Color();
        private System.Timers.Timer eyeDropperTimer = new System.Timers.Timer();

        private void StartEyeDropping()
        {
            eyeDropperCheckbox.CheckState = CheckState.Checked;
            eyeDropperCheckbox.Capture = true;
            eyeDropperTimer.Enabled = true;
            eyeDropperTimer.SynchronizingObject = this;
            eyeDropperTimer.Elapsed += eyeDropperTimer_Elapsed;
            eyeDropperOriginalColor = CurrentColor;
            eyeDropperCheckbox.Cursor = Cursors.No;
        }

        private void StopEyeDropping()
        {
            eyeDropperCheckbox.CheckState = CheckState.Unchecked;
            eyeDropperTimer.Enabled = false;
            eyeDropperCheckbox.Capture = false;
            eyeDropperCheckbox.Cursor = Cursors.Default;
        }

        private void CancelEyeDropping()
        {
            StopEyeDropping();
            CurrentColor = eyeDropperOriginalColor;
        }

        // From http://www.bobpowell.net/eyedropper.htm
        
        [DllImport("Gdi32.dll")]
        public static extern int GetPixel(
        System.IntPtr hdc,    // handle to DC
        int nXPos,  // x-coordinate of pixel
        int nYPos   // y-coordinate of pixel
        );

        [DllImport("User32.dll")]
        public static extern IntPtr GetDC(IntPtr wnd);

        [DllImport("User32.dll")]
        public static extern void ReleaseDC(IntPtr dc);

        private void eyeDropperTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            Point p = Control.MousePosition;
            if (this.Bounds.Contains(p))
            {
                eyeDropperCheckbox.Cursor = Cursors.No;
                return; // don't capture colors in our own form
            }
            eyeDropperCheckbox.Cursor = Cursors.Cross;
            IntPtr dc = GetDC(IntPtr.Zero);
            CurrentColor = ColorTranslator.FromWin32(GetPixel(dc, p.X, p.Y));
            ReleaseDC(dc);
        }

        private void eyeDropperCheckbox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                CancelEyeDropping();
            }
        }

        private void eyeDropperCheckbox_MouseUp(object sender, MouseEventArgs e)
        {
                StopEyeDropping();
        }

        private void eyeDropperCheckbox_MouseDown(object sender, MouseEventArgs e)
        {
                StartEyeDropping();
        }

	}
}
