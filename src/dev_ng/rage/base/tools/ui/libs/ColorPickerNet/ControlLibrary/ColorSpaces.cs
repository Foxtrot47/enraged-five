
using System;

namespace Sano.PersonalProjects.ColorPicker.Controls {

	internal enum ColorSpaces {
		
		HueSaturationBrightness,
		RedGreenBlue
	
	} // ColorSpaces

} // Sano.PersonalProjects.ColorPicker.Controls