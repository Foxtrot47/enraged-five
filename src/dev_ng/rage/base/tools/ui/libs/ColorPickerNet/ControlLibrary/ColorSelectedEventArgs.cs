
using System;
using System.Drawing;
using System.Windows.Forms;

namespace Sano.PersonalProjects.ColorPicker.Controls {
	
	public class ColorSelectedEventArgs : EventArgs {
		
		private Color m_color;
		
		public Color Color {
			get { return m_color; }
		}

		internal ColorSelectedEventArgs( Color color ) {
			m_color = color;
		}

	}

}