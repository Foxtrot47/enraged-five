
using Sano.PersonalProjects.ColorPicker.Controls;
using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml.Serialization;

namespace Sano.PersonalProjects.ColorPicker.Main {

	/// <summary>
	/// This class is responsible for managing all of the application 
	/// configuration data, including serializing the data to and from disk.
	/// </summary>

	public sealed class ApplicationConfiguration {
		
		// private member fields 
		private ApplicationData m_applicationData;
		private ColorPanelSettings m_colorPanelSettings;
			
		// readonly members
		private readonly string m_configurationFile = "settings.cpn";

		// static instance of ApplicationConfiguration - this enforces the
		// Singleton design pattern and allows us to ensure that there is
		// only one instance of ApplicationConfiguration used throughout the
		// application's lifetime.

		public static ApplicationConfiguration Settings = new ApplicationConfiguration();

		/// <summary>
		/// Private constructor. Initializes an instance of m_applicationData.
		/// </summary>

		private ApplicationConfiguration() {
			m_applicationData = new ApplicationData();
			m_colorPanelSettings = new ColorPanelSettings();
		}

		/// <summary>
		/// Gets or sets a value that determines whether or not the application
		/// is to be minimized to the system tray.
		/// </summary>

		public bool MinimizeApplicationToSystemTray {
			get { return m_applicationData.MinimizeApplicationToSystemTray; }
			set { m_applicationData.MinimizeApplicationToSystemTray = value; }
		}

		/// <summary>
		/// Gets or sets a value that determines whether or not the application
		/// settings are to be saved to disk when the application is closed.
		/// </summary>

		public bool SaveSettingsOnApplicationClose {
			get { return m_applicationData.SaveSettingsOnApplicationClose; }
			set { m_applicationData.SaveSettingsOnApplicationClose = value; }
		}

		/// <summary>
		/// Gets or sets the ColorPanelSettings object that tracks the user's
		/// preferences for the color panel portion of the application.
		/// </summary>

		public ColorPanelSettings ColorPanelSettings {
			get { return m_applicationData.ColorPanelSettings; }
			set { m_applicationData.ColorPanelSettings = value; }
		}

		/// <summary>
		/// Writes the configuration settings to file.
		/// </summary>

		internal void WriteSettingsToFile() {
			
			BinaryFormatter formatter = new BinaryFormatter();
			Stream stream = null; 

			try {

				stream = new FileStream( m_configurationFile, FileMode.OpenOrCreate, FileAccess.Write, FileShare.None );
				formatter.Serialize( stream, m_applicationData );
				
			} finally {

				if ( stream != null ) {
					stream.Close();
				}

			}
		
		}

		/// <summary>
		/// Reads the configuration settings from file.
		/// </summary>

		internal void ReadSettingsFromFile() {
			
			if ( File.Exists( m_configurationFile ) ) {

				BinaryFormatter formatter = new BinaryFormatter();
				Stream stream = null;

				try {
					
					stream = new FileStream( m_configurationFile, FileMode.Open, FileAccess.Read, FileShare.None );
					m_applicationData = ( ApplicationData ) formatter.Deserialize( stream );

				} finally {

					if ( stream != null ) {
						stream.Close();
					}

				}
			
			}
		
		}

	} // ApplicationConfiguration

} // Sano.PersonalProjects.ColorPicker.Main