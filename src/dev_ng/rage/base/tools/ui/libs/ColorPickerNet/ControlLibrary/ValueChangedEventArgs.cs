
using System;
using System.Windows.Forms;

namespace Sano.PersonalProjects.ColorPicker.Controls {

	/// <summary>
	/// This class serves as a mini data structure that holds a changed value.
	/// </summary>

	internal class ValueChangedEventArgs : EventArgs {

		// private data field
		private int m_value;

		/// <summary>
		/// Gets the value.
		/// </summary>

		internal int Value {
			get { return m_value; }
		}

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="newValue">The new value.</param>

		internal ValueChangedEventArgs( int newValue ) {
			m_value = newValue;
		}

	} // ValueChangedEventArgs

} // Sano.PersonalProjects.ColorPicker.Controls