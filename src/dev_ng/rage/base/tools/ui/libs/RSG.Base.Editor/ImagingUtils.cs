﻿using System;
using System.Windows;
using System.Windows.Media.Imaging;

namespace RSG.Base.Editor
{

    /// <summary>
    /// 
    /// </summary>
    internal static class ImagingUtils
    {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static BitmapSource LoadBitmap(System.Drawing.Bitmap source)
        {
            if (null == source)
                return (null);

            BitmapSource bs = null;
            IntPtr ip = source.GetHbitmap();
            try
            {
                bs = System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(ip,
                   IntPtr.Zero, Int32Rect.Empty,
                   System.Windows.Media.Imaging.BitmapSizeOptions.FromEmptyOptions());
            }
            finally
            {
                RSG.Base.Win32.API.DeleteObject(ip);
            }

            return bs;
        }
    }

} // RSG.Base.Editor namespace
