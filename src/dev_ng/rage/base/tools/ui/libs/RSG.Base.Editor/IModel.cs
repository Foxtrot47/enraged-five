﻿using System;
using System.ComponentModel;
using RSG.Base.Editor.Command;

namespace RSG.Base.Editor
{
    /// <summary>
    /// Model interface.
    /// </summary>
    public interface IModel :
        INotifyPropertyChanging,
        INotifyPropertyChanged
    {
        #region Events

        /// <summary>
        /// Event raised when model's Dirty state has changed.
        /// </summary>
        event ModifiedStateChangedEventHandler ModifiedStateChanged;
        
        #endregion // Events

        #region Properties

        /// <summary>
        /// Used to tell whether there has been a property change in the
        /// model 
        /// </summary>
        Boolean IsModified { get; set; }

        /// <summary>
        /// Gets a value indicting whether undo redo batching is being applied to the
        /// model
        /// </summary>
        Boolean IsBatching { get; }

        #endregion // Properties

        #region Methods

        void SendPropertyChangedEvent(EditorPropertyChangedEventArgs e);

        void SendCompositePropertyChangedEvent(EditorCompositeChangedEventArgs e);

        void SendCollectionChangedEvent(EditorCollectionChangedEventArgs e);

        void SendModifiedStateChangedEvent(ModifiedStateChangedEventArgs e);

        #endregion // Methods
    }

    /// <summary>
    /// Model event argument class.
    /// </summary>
    public class ModelEventArgs : EventArgs
    {
        #region Properties
        /// <summary>
        /// Associated Model object.
        /// </summary>
        public IModel Model
        {
            get;
            protected set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="m"></param>
        public ModelEventArgs(IModel m)
        {
            this.Model = m;
        }
        #endregion // Constructor(s)
    }

    internal interface IModelUndoRedoProperties
    {
        #region Methods

        void StartUndoRedoBlock(String tag);

        void FinishUndoRedoBlock();

        #endregion // Methods
    } // IModelUndoRedoProperties

    public interface IHierarchicalModel : IModel
    {
        #region Properties

        IModel ParentModel { get; set; }

        #endregion // Properties
    } // IModelUndoRedoProperties
} // RSG.Base.Editor namespace
