﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace RSG.Base.Editor.Command
{
    /// <summary>
    /// Interaction logic for CommandStackButton.xaml
    /// </summary>
    public partial class CommandStackButton : UserControl
    {
        public CommandStackButton()
        {
            InitializeComponent();
        }
    }
}
