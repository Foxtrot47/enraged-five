﻿using System;

namespace RSG.Base.Editor.Command
{

    /// <summary>
    /// UndoableProperty attribute flags properties that have undo/redo 
    /// commands created for their property changed events.
    /// </summary>
    /// For all properties in a model/viewmodel that are undoable specify this
    /// attribute.
    /// 
    [AttributeUsage(AttributeTargets.Property)]
    public class UndoableProperty : Attribute
    {
    }

    /// <summary>
    /// UndoableCollectionProperty attribute flags collection properties that
    /// have undo/redo commands created for their property changed events.
    /// </summary>
    /// For all collection properties in a model/viewmodel that are undoable
    /// specify this attribute.
    /// 
    [AttributeUsage(AttributeTargets.Property)]
    public class UndoableCollectionProperty : Attribute
    {
    }

    /// <summary>
    /// CollapsableProperty attribute flags properties for when multiple undo/
    /// redo commands are next to each other in the command stack are collapsed
    /// into a single undo/redo command.
    /// </summary>
    /// This is especially useful for String properties where they are edited
    /// in a TextBox; multiple key presses are collapsed together into one
    /// undoable/redoable command.  This makes for happier users.
    /// 
    public delegate bool CollapsablePredict<in T>(T oldValue, T newValue);

    [AttributeUsage(AttributeTargets.Property)]
    public class CollapsableProperty : Attribute
    {
        public CollapsableProperty()
        {
        }

        public virtual Boolean Collapse(Object oldValue, Object newValue)
        {
            return true;
        }
    }

    /// <summary>
    /// NotDirtyProperty attribute flags properties for when the property changing
    /// doesn't need to cause the dirty state of the model to change.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class NotDirtyProperty : Attribute
    {
    }

    /// <summary>
    /// DontRegister when set means that the model doesn't get registered for property changes to use for
    /// the command manager.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class DontRegister : Attribute
    {
    }

} // RSG.Base.Editor.Command namespace
