﻿using System;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace RSG.Base.Editor.Command
{
    #region Base Interfaces

    /// <summary>
    /// Base undo/redo command interface.
    /// </summary>
    public interface IUndoRedoCommand
    {
        #region Properties

        /// <summary>
        /// Command tag string for UI display.
        /// </summary>
        String Tag { get; }

        /// <summary>
        /// The modified state the model was in when 
        /// this command was created
        /// </summary>
        Boolean ModifiedState { get; set; }

        #endregion // Properties
    }

    #endregion // Base Interfaces

    /// <summary>
    /// Atomic undo/redo command.
    /// </summary>
    /// An atomic undo/redo command representing a single command that can be
    /// done/undone at once.
    /// 
    public interface IAtomicUndoRedoCommand : IUndoRedoCommand
    {
        #region Properties
        /// <summary>
        /// Original source object that caused the undo command.
        /// </summary>
        IModel Sender { get; }
        
        /// <summary>
        /// Previous value prior to the changed event.
        /// </summary>
        Object PreviousValue { get; }
        
        /// <summary>
        /// New (current) value after the changed event.
        /// </summary>
        Object NewValue { get; }

        /// <summary>
        /// The expression that represents the method that originally called 
        /// the property change
        /// </summary>
        PropertyInfo Property { get; }

        /// <summary>
        /// The actual delegate that is used to set the property to a new value.
        /// </summary>
        PropertySetDelegate Setter { get; }
        #endregion // Properties
    }

    /// <summary>
    /// Composite undo/redo command.
    /// </summary>
    /// A composite undo/redo command representing a chain of commands that are
    /// done/undone at once.
    /// 
    public interface ICompositeUndoRedoCommand : IUndoRedoCommand
    {
        #region Properties
        /// <summary>
        /// Child undo/redo commands making up this composite command.
        /// </summary>
        Stack<IUndoRedoCommand> Children { get; }
        #endregion // Properties
    }

    /// <summary>
    /// Delegate that is used to set a property to a new value.
    /// </summary>
    /// <param name="value"></param>
    public delegate void PropertySetDelegate(Object value);

} // RSG.Base.Editor.Command namespace
