﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Base.Editor.Command
{
    /// <summary>
    /// 
    /// </summary>
    public class ModelPropertyMetadata<T>
    {
        public ModelPropertyMetadata(ModelPropertyChangedCallback<T> propertyChangedCallback)
        {
            this.PropertyChangedCallback = propertyChangedCallback;
        }

        public ModelPropertyChangedCallback<T> PropertyChangedCallback { get; private set; }
    } // ModelPropertyMetadata
} // RSG.Base.Editor.Command
