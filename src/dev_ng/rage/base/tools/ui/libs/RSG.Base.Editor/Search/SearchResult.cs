﻿using System;

namespace RSG.Base.Editor.Search
{

    /// <summary>
    /// 
    /// </summary>
    public class SearchResult
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public Object ModelObject { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public SearchResult()
        {
        }

        /// <summary>
        /// Constructor that takes a model that is used to set the object 
        /// reference with the matching property in it.
        /// </summary>
        /// <param name="model"></param>
        public SearchResult(Object model)
        {
            this.ModelObject = model;
        }
        #endregion // Constructor(s)
    }

} // RSG.Base.Editor.Search namespace
