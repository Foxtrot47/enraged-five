﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace RSG.Base.Editor.Collections
{
    public interface INotifyCollectionChanging
    {
        /// <summary>
        /// Occurs when the collection is about to change.
        /// </summary>
        event NotifyCollectionChangedEventHandler CollectionChanging;
    }

    public interface IModelNotifyingCollection : INotifyCollectionChanged, INotifyCollectionChanging
    {
        #region Properties

        String PropertyName { get; set; }
        
        #endregion // Properties
    }

    internal interface IModelCollection
    {
        #region Properties

        /// <summary>
        /// If true no events are fired
        /// </summary>
        Boolean InSilentMode { get; set; }

        #endregion // Properties
    }

    public interface IModelObservableCollection
    {
        #region Functions

        /// <summary>
        /// 
        /// </summary>
        void AddRange(int index, IEnumerable collection);

        /// <summary>
        /// 
        /// </summary>
        void RemoveRange(int index, IEnumerable collection);

        #endregion // Public Functions
    }

    public class ModelCollectionSilentBlock : IDisposable
    {
        private IModelCollection Collection { get; set; }

        public ModelCollectionSilentBlock(IModelNotifyingCollection collection)
        {
            if (collection is IModelCollection)
            {
                Collection = collection as IModelCollection;
                Collection.InSilentMode = true;
            }
        }

        public void Dispose()
        {
            if (this.Collection != null)
                Collection.InSilentMode = false;
        }
    }
    
    public class ModelObservableCollection<T> : Collection<T>, 
        INotifyPropertyChanged, INotifyPropertyChanging,
        IModelNotifyingCollection, IModelCollection, IModelObservableCollection
    {
        #region Public Events
        
        public event NotifyCollectionChangedEventHandler CollectionChanged;
        public event NotifyCollectionChangedEventHandler CollectionChanging;
        public event PropertyChangedEventHandler PropertyChanged;
        public event PropertyChangingEventHandler PropertyChanging;
        
        #endregion // Public Events

        #region Members

        private Boolean m_inSilentMode;
        private String m_propertyName;

        #endregion // Members

        #region Properties
        
        /// <summary>
        /// If true no events are fired
        /// </summary>
        Boolean IModelCollection.InSilentMode
        {
            get { return m_inSilentMode; }
            set { m_inSilentMode = value; }
        }

        /// <summary>
        /// The property name of this collection if
        /// it is valid to have one
        /// </summary>
        public String PropertyName
        {
            get { return m_propertyName; }
            set { m_propertyName = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public T Get(int index)
        {
            return this.Items[index];
        }

        #endregion // Properties

        #region Constructor(s)

        /// <summary>
        /// Default constructor
        /// </summary>
        public ModelObservableCollection(ModelBase attachedModel, String propertyName)
        {
            this.PropertyName = propertyName;
            attachedModel.RegisterModelCollection(this);
        }

        /// <summary>
        /// Construct a collection using a already defined list of items
        /// </summary>
        /// <param name="items"></param>
        public ModelObservableCollection(IList<T> items, ModelBase attachedModel, String propertyName)
        {
            this.PropertyName = propertyName;
            attachedModel.RegisterModelCollection(this);

            foreach (var item in items)
            {
                this.Add(item);
            }
        }

        #endregion // Constructor(s)

        #region Public Functions

        /// <summary>
        /// 
        /// </summary>
        public void AddRange(int index, IEnumerable collection)
        {
            List<T> addCollection = new List<T>();
            foreach (var i in collection)
                if (i is T)
                    addCollection.Add((T)i);

            if (addCollection.Count > 0)
            {
                HandleCollectionChanging(NotifyCollectionChangedAction.Add, addCollection, null, index);

                int currentIndex = index;
                foreach (T i in addCollection)
                {
                    this.Items.Insert(currentIndex, i);
                }

                HandleCollectionChanged(NotifyCollectionChangedAction.Add, addCollection, null, index);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void RemoveRange(int index, IEnumerable collection)
        {
            List<T> removedList = new List<T>();
            foreach (var i in collection)
            {
                if (i is T)
                {
                    T removedItem = (T)i;
                    foreach (T item in this.Items)
                    {
                        if (ReferenceEquals(item, removedItem))
                        {
                            removedList.Add(removedItem);
                        }
                    }
                }
            }

            if (removedList.Count > 0)
            {
                HandleCollectionChanging(NotifyCollectionChangedAction.Remove, null, removedList, index);

                foreach (T item in removedList)
                {
                    Items.Remove(item);
                }

                HandleCollectionChanged(NotifyCollectionChangedAction.Remove, null, removedList, index);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void AddRange(int index, IEnumerable<T> collection)
        {
            List<T> addList = new List<T>();
            foreach (T i in collection)
                    addList.Add(i);

            HandleCollectionChanging(NotifyCollectionChangedAction.Add, addList, null, index);

            int currentIndex = index;
            foreach (T i in collection)
                this.Items.Insert(currentIndex++, i);

            HandleCollectionChanged(NotifyCollectionChangedAction.Add, addList, null, index);
        }

        /// <summary>
        /// 
        /// </summary>
        public void RemoveRange(int index, IEnumerable<T> collection)
        {
            List<T> removedList = new List<T>();
            foreach (var i in collection)
            {
                if (i is T)
                {
                    T removedItem = (T)i;
                    foreach (T item in this.Items)
                    {
                        if (ReferenceEquals(item, removedItem))
                        {
                            removedList.Add(removedItem);
                        }
                    }
                }
            }

            if (removedList.Count > 0)
            {
                HandleCollectionChanging(NotifyCollectionChangedAction.Remove, null, removedList, index);

                for (int i = removedList.Count - 1; i >= 0; i--)
                {
                    Items.RemoveAt(index + i);
                }

                HandleCollectionChanged(NotifyCollectionChangedAction.Remove, null, removedList, index);
            }
        }

        #endregion // Public Functions

        #region Override Functions

        /// <summary>
        /// Override the clear items function to make sure that the correct events get called for it.
        /// </summary>
        protected override void ClearItems()
        {
            List<T> removedList = new List<T>();
            foreach (T i in this.Items)
                    removedList.Add(i);

            HandleCollectionChanging(NotifyCollectionChangedAction.Remove, null, removedList, 0);

            base.ClearItems();

            HandleCollectionChanged(NotifyCollectionChangedAction.Remove, null, removedList, 0);
        }

        /// <summary>
        /// Override the insert item function to make sure that the correct events get called for it.
        /// </summary>
        protected override void InsertItem(int index, T item)
        {
            HandleCollectionChanging(NotifyCollectionChangedAction.Add, new List<T>() { item }, null, index);

            base.InsertItem(index, item);

            HandleCollectionChanged(NotifyCollectionChangedAction.Add, new List<T>() { item }, null, index);
        }

        /// <summary>
        /// Override the remove item function to make sure that the correct events get called for it.
        /// </summary>
        protected override void RemoveItem(int index)
        {
            T removedItem = this[index];
            HandleCollectionChanging(NotifyCollectionChangedAction.Remove, null, new List<T>() { removedItem }, index);

            base.RemoveItem(index);

            HandleCollectionChanged(NotifyCollectionChangedAction.Remove, null, new List<T>() { removedItem }, index);
        }

        #endregion // Override Function(s)

        #region Private Event Helpers

        private void HandleCollectionChanging(NotifyCollectionChangedAction action, IList newItems, IList oldItems, int index)
        {
            FirePropertyChangingNotifications();
            FireCollectionChangingNotifications(action, newItems, oldItems, index);
        }

        private void HandleCollectionChanged(NotifyCollectionChangedAction action, IList newItems, IList oldItems, int index)
        {
            FirePropertyChangedNotifications();
            FireCollectionChangedNotifications(action, newItems, oldItems, index);
        }

        private void FireCollectionChangingNotifications(NotifyCollectionChangedAction action, IList newItems, IList oldItems, int index)
        {
            NotifyCollectionChangedEventArgs args = null;
            switch (action)
            {
                case NotifyCollectionChangedAction.Add:
                    args = new NotifyCollectionChangedEventArgs(action, newItems, index);
                    break;
                case NotifyCollectionChangedAction.Remove:
                    args = new NotifyCollectionChangedEventArgs(action, oldItems, index);
                    break;
            }

            if (args != null)
                OnCollectionChanging(args);
        }

        private void FireCollectionChangedNotifications(NotifyCollectionChangedAction action, IList newItems, IList oldItems, int index)
        {
            NotifyCollectionChangedEventArgs args = null;
            switch (action)
            {
                case NotifyCollectionChangedAction.Add:
                    args = new NotifyCollectionChangedEventArgs(action, newItems as IList, index);
                    break;
                case NotifyCollectionChangedAction.Remove:
                    args = new NotifyCollectionChangedEventArgs(action, oldItems as IList, index);
                    break;
            }

            if (args != null)
                OnCollectionChanged(args);
        }

        private void FirePropertyChangingNotifications()
        {
            OnPropertyChanging("Count");
            OnPropertyChanging("Item");
            OnPropertyChanging("Items");
        }

        private void FirePropertyChangedNotifications()
        {
            OnPropertyChanged("Count");
            OnPropertyChanged("Item");
            OnPropertyChanged("Items");
        }

        private void OnCollectionChanging(NotifyCollectionChangedEventArgs args)
        {
            if (CollectionChanging != null && (this as IModelCollection).InSilentMode == false)
                CollectionChanging(this, args);
        }

        private void OnCollectionChanged(NotifyCollectionChangedEventArgs args)
        {
            if (CollectionChanged != null && (this as IModelCollection).InSilentMode == false)
                CollectionChanged(this, args);
        }

        private void OnPropertyChanging(String propertyName)
        {
            if (PropertyChanging != null && (this as IModelCollection).InSilentMode == false)
                PropertyChanging(this, new PropertyChangingEventArgs(propertyName));
        }

        private void OnPropertyChanged(String propertyName)
        {
            if (PropertyChanged != null && (this as IModelCollection).InSilentMode == false)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion // Private Event Helpers
    } // ModelObservableCollection
} // RSG.Base.Editor.Collections
