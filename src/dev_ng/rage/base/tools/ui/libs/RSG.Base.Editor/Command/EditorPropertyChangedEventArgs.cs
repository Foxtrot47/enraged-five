﻿using System;
using System.Reflection;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using RSG.Base.Editor.Collections;

namespace RSG.Base.Editor.Command
{
    /// <summary>
    /// 
    /// </summary>
    public class EditorPropertyChangedEventArgs : PropertyChangedEventArgs
    {        
        #region Members

        private IModel m_model;
        private IModel m_originalModel;
        private Object m_newValue;
        private Object m_oldValue;
        private PropertyInfo m_property;
        private PropertySetDelegate m_setter;
        private Boolean m_modifiedState;
        private Boolean m_createUndoCommand;
        private Boolean m_isCollectionChange;
        private Boolean m_handled;

        #endregion // Members

        #region Properties

        public IModel Model
        {
            get { return m_model; }
            internal set { m_model = value; }
        }

        public IModel OriginalModel
        {
            get { return m_originalModel; }
            private set { m_originalModel = value; }
        }

        public Object NewValue
        {
            get { return m_newValue; }
            private set { m_newValue = value; }
        }

        public Object OldValue
        {
            get { return m_oldValue; }
            private set { m_oldValue = value; }
        }

        public PropertyInfo Property
        {
            get { return m_property; }
            private set { m_property = value; }
        }

        public PropertySetDelegate Setter
        {
            get { return m_setter; }
            private set { m_setter = value; }
        }

        public Boolean ModifiedState
        {
            get { return m_modifiedState; }
            set { m_modifiedState = value; }
        }

        public Boolean CreateUndoCommand
        {
            get { return m_createUndoCommand; }
            private set { m_createUndoCommand = value; }
        }

        public Boolean IsCollectionChange
        {
            get { return m_isCollectionChange; }
            set { m_isCollectionChange = value; }
        }

        public Boolean Handled
        {
            get { return m_handled; }
            set { m_handled = value; }
        }

        #endregion // Properties

        #region Constructors

        /// <summary>
        /// Default Constructor
        /// </summary>
        public EditorPropertyChangedEventArgs(String propertyName, IModel model, IModel original, Object oldValue, Object newValue, 
            PropertyInfo property, PropertySetDelegate setter, Boolean modifiedState, Boolean createUndoCommand, Boolean isCollectionChange)
            : base(propertyName)
        {
            this.Model = model;
            this.OriginalModel = original;
            this.NewValue = newValue;
            this.OldValue = oldValue;
            this.Property = property;
            this.Setter = setter;
            this.ModifiedState = modifiedState;
            this.CreateUndoCommand = createUndoCommand;
            this.IsCollectionChange = isCollectionChange;
            this.Handled = false;
        }

        #endregion // Constructors
    } // EditorPropertyChangedEventArgs

    /// <summary>
    /// 
    /// </summary>
    public class EditorCollectionChangedEventArgs : EditorPropertyChangedEventArgs
    {
        #region Members

        private IModelObservableCollection m_collection;
        private int m_startingIndex;

        #endregion // Members

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        public IModelObservableCollection Collection
        {
            get { return m_collection; }
            set { m_collection = value; }
        }

        public int StartingIndex
        {
            get { return m_startingIndex; }
            set { m_startingIndex = value; }
        }

        #endregion // Properties

        #region Constructors

        /// <summary>
        /// Default Constructor
        /// </summary>
        public EditorCollectionChangedEventArgs(String propertyName, IModel model, IModel original, Object oldValue, Object newValue,
            PropertyInfo property, Boolean modifiedState, Boolean createUndoCommand, IModelObservableCollection collection, int startingIndex)
            : base(propertyName, model, original, oldValue, newValue, property, null, modifiedState, createUndoCommand, true)
        {
            this.Collection = collection;
            this.StartingIndex = startingIndex;
        }

        #endregion // Constructors
    } // EditorCollectionChangedEventArgs

    /// <summary>
    /// 
    /// </summary>
    public class EditorCompositeChangedEventArgs : EditorPropertyChangedEventArgs
    {
        #region Members

        private List<EditorPropertyChangedEventArgs> m_eventCollection;
        private String m_tag;

        #endregion // Members

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        public List<EditorPropertyChangedEventArgs> EventCollection
        {
            get { return m_eventCollection; }
            set { m_eventCollection = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public String Tag
        {
            get { return m_tag; }
            set { m_tag = value; }
        }

        #endregion // Properties

        #region Constructors

        /// <summary>
        /// Default Constructor
        /// </summary>
        public EditorCompositeChangedEventArgs(IModel model, IModel original, String tag, Boolean modifiedState, Boolean createUndoCommand, IList<EditorPropertyChangedEventArgs> events)
            : base(String.Empty, model, original, null, null, null, null, modifiedState, createUndoCommand, false)
        {
            this.EventCollection = new List<EditorPropertyChangedEventArgs>(events);
            this.Tag = tag;
        }

        #endregion // Constructors
    } // EditorCompositeChangedEventArgs
} // RSG.Base.Editor.Command
