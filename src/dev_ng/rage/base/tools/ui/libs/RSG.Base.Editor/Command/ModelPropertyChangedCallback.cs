﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Base.Editor.Command
{
    public delegate void ModelPropertyChangedCallback<T>(IModel model, ModelPropertyChangedEventArgs<T> e);
}
