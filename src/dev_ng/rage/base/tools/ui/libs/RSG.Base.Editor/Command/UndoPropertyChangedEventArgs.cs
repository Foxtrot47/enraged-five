﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;

namespace RSG.Base.Editor.Command
{
    /// <summary>
    /// 
    /// </summary>
    public class UndoPropertyChangedEventArgs
    {
        #region Members

        private Object m_newValue;
        private Object m_oldValue;
        private MemberExpression m_method;
        private PropertySetDelegate m_setter;

        #endregion // Members

        #region Properties

        public Object NewValue
        {
            get { return m_newValue; }
            private set { m_newValue = value; }
        }

        public Object OldValue
        {
            get { return m_oldValue; }
            private set { m_oldValue = value; }
        }

        public MemberExpression Method
        {
            get { return m_method; }
            private set { m_method = value; }
        }

        public PropertySetDelegate Setter
        {
            get { return m_setter; }
            private set { m_setter = value; }
        }

        public Boolean ModifiedState
        {
            get;
            set;
        }

        #endregion // Properties

        #region Constructors

        /// <summary>
        /// Default Constructor
        /// </summary>
        public UndoPropertyChangedEventArgs(Object oldValue, Object newValue, MemberExpression method, PropertySetDelegate setter, Boolean modifiedState)
        {
            this.NewValue = newValue;
            this.OldValue = oldValue;
            this.Method = method;
            this.Setter = setter;
            this.ModifiedState = modifiedState;
        }

        #endregion // Constructors
    } // UndoPropertyChangedEventArgs
} // RSG.Base.Editor.Command
