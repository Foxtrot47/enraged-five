﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows;
using System.Windows.Threading;
using RSG.Base.Editor.Command;
using System.Collections.ObjectModel;

namespace RSG.Base.Editor
{

    /// <summary>
    /// Abstract base class for ViewModels.
    /// </summary>
    /// <seealso cref="ModelBase" />
    /// <seealso cref="IViewModel" />
    /// 
    public abstract class ViewModelBase : 
        ModelBase, 
        IViewModel
    {
        #region Properties and Associated Member Data
        /// <summary>
        /// Selection property for this ViewModel.
        /// </summary>
        [Browsable(false)]
        public bool IsSelected
        {
            get { return m_bIsSelected; }
            set
            {
                this.SetPropertyValue(ref this.m_bIsSelected, value, "IsSelected");
            }
        }
        private bool m_bIsSelected = false;

        /// <summary>
        /// Access to the current thread dispatcher.
        /// </summary>
        /// This allows subclasses to easily perform work in a background thread
        /// and synchronise the results with the UI.  A new dispatcher is assigned
        /// if no WPF application is running.
        protected Dispatcher Dispatcher
        {
            get { return m_Dispatcher; }
        }
        private readonly Dispatcher m_Dispatcher;
        #endregion // Properties and Associated Member Data

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        protected ViewModelBase()
        {
            if (null != Application.Current)
                this.m_Dispatcher = Application.Current.Dispatcher;
            else
                this.m_Dispatcher = Dispatcher.CurrentDispatcher;
        }
        #endregion // Constructor(s)
    }

    /// <summary>
    /// Abstract base class for hierarchical ViewModels.
    /// </summary>
    public abstract class HierarchicalViewModelBase :
        ViewModelBase,
        IHierarchicalViewModel
    {
        #region Properties and Associated Member Data
        /// <summary>
        /// Selection property for this ViewModel.
        /// </summary>
        public new bool IsSelected
        {
            get { return m_bIsSelected; }
            set
            {
                if (this.SetPropertyValue(ref this.m_bIsSelected, value, "IsSelected"))
                {
                    if (value)
                    {
                        HierarchicalViewModelBase parent = this.Parent as HierarchicalViewModelBase;
                        while (parent != null)
                        {
                            parent.IsExpanded = true;
                            parent = parent.Parent as HierarchicalViewModelBase;
                        }
                    }
                }
            }
        }
        private bool m_bIsSelected = false;

        private bool BeenExpanded = false;

        /// <summary>
        /// Expanded property for this hierarchical ViewModel.
        /// </summary>
        public bool IsExpanded
        {
            get { return m_bIsExpanded; }
            set
            {
                if (this.SetPropertyValue(ref this.m_bIsExpanded, value, "IsExpanded"))
                {
                    if (value)
                    {
                        if (this.BeenExpanded == false)
                        {
                            this.BeenExpanded = true;
                            OnFirstExpanded();
                            OnExpanded();
                        }
                        else
                        {
                            OnExpanded();
                        }
                    }
                    else
                    {
                        OnCollapsed();
                    }
                }
            }
        }
        private bool m_bIsExpanded = false;

        /// <summary>
        /// Parent property for this hierarchical ViewModel.
        /// </summary>
        public IViewModel Parent
        {
            get { return m_Parent; }
            set
            {
                this.SetPropertyValue(ref this.m_Parent, value, "Parent");
            }
        }
        private IViewModel m_Parent = null;

        /// <summary>
        /// 
        /// </summary>
        public ICollection<IViewModel> Children
        {
            get { return m_Children; }
            set
            {
                this.SetPropertyValue(ref this.m_Children, value, "Children");
            }
        }
        private ICollection<IViewModel> m_Children = 
            new ObservableCollection<IViewModel>();
        #endregion // Properties and Associated Member Data

        #region Virtual Functions

        protected virtual void OnFirstExpanded()
        {
        }

        protected virtual void OnExpanded()
        {
        }

        protected virtual void OnCollapsed()
        {
        }

        public virtual IViewModel GetChildWithString(String name)
        {
            return null;
        }

        #endregion // Virtual Functions
    }

} // RSG.Base.Editor namespace
