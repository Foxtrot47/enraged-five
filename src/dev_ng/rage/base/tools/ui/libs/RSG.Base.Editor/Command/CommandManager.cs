﻿using System;
using System.Windows;
using System.ComponentModel;
using System.Collections;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using RSG.Base.Collections;

namespace RSG.Base.Editor.Command
{
    /// <summary>
    /// 
    /// </summary>
    public class CommandManager
    {
        #region Enumerations

        /// <summary>
        /// 
        /// </summary>
        protected enum CompositeUndoRedoState
        {
            Idle,       // Idle, no composite commands.
            Creating    // Creating composite commands.
        }

        #endregion // Enumerations

        #region Members

        private ObservableStack<IUndoRedoCommand> m_undoStack;
        private ObservableStack<IUndoRedoCommand> m_redoStack;

        #endregion // Members

        #region Properties

        /// <summary>
        /// Stack of done commands; ready to be undone.
        /// </summary>
        public ObservableStack<IUndoRedoCommand> UndoStack
        {
            get { return m_undoStack; }
            protected set { m_undoStack = value; }
        }

        /// <summary>
        /// Stack of undone commands; ready to be redone.
        /// </summary>
        public ObservableStack<IUndoRedoCommand> RedoStack
        {
            get { return m_redoStack; }
            protected set { m_redoStack = value; }
        }

        /// <summary>
        /// Marked true if just saved
        /// </summary>
        public Boolean Saved
        {

            get;
            set;
        }

        /// <summary>
        /// Set to true if currently performing a undo or redo command
        /// </summary>
        private Boolean PerformingCommand
        {
            get;
            set;
        }

        #endregion // Properties

        #region Member Data

        /// <summary>
        /// Collection of registered models.
        /// </summary>
        protected Collection<IModel> RegisteredModels;

        /// <summary>
        /// Collection of registered models.
        /// </summary>
        private static Dictionary<CommandManager, List<IModel>> RegisteredManagers;

        #region Composite Undo/Redo Command Support

        /// <summary>
        /// Current composite state.
        /// </summary>
        protected CompositeUndoRedoState CompositeState;

        /// <summary>
        /// Current composite tag string.
        /// </summary>
        protected String CompositeTag = String.Empty;

        /// <summary>
        /// Composite command stack.
        /// </summary>
        protected Stack<IUndoRedoCommand> CompositeUndoStack;

        #endregion // Composite Undo/Redo Command Support

        #endregion // Member Data

        #region Constructor(s)

        /// <summary>
        /// Default constructor.
        /// </summary>
        public CommandManager()
        {
            this.UndoStack = new ObservableStack<IUndoRedoCommand>();
            this.RedoStack = new ObservableStack<IUndoRedoCommand>();
            this.CompositeUndoStack = new Stack<IUndoRedoCommand>();
            this.RegisteredModels = new Collection<IModel>();
            this.CompositeState = CompositeUndoRedoState.Idle;
        }

        #endregion // Constructor(s)

        #region Controller Methods

        #region Undo/Redo

        /// <summary>
        /// 
        /// </summary>
        public void Undo()
        {
            PerformingCommand = true;
            if (this.UndoStack.Count > 0)
            {
                IUndoRedoCommand command = this.UndoStack.Pop();
                UndoCommand(command, true);                
            }
            PerformingCommand = false;
        }

        private void UndoCommand(IUndoRedoCommand command, Boolean createRedoCommand)
        {
            if (command is AtomicUndoRedoCommand)
            {
                AtomicUndoRedoCommand atomicCommand = command as AtomicUndoRedoCommand;
                if (atomicCommand.Sender is ModelBase)
                {
                    (atomicCommand.Sender as ModelBase).SetPropertyValue(atomicCommand.PreviousValue, atomicCommand.NewValue, atomicCommand.Property, atomicCommand.Setter);
                }
                else
                {
                    atomicCommand.Setter(atomicCommand.PreviousValue);
                }

                if (createRedoCommand == true)
                {
                    atomicCommand.Sender.IsModified = command.ModifiedState;
                    this.RedoStack.Push(AtomicUndoRedoCommand.CreateRedoCommand(atomicCommand));
                    if (Saved == true)
                    {
                        Saved = false;
                        this.RedoStack.Peek().ModifiedState = !atomicCommand.ModifiedState;
                    }
                }
            }
            else if (command is AtomicCollectionUndoRedoCommand)
            {
                AtomicCollectionUndoRedoCommand collectionCommand = command as AtomicCollectionUndoRedoCommand;

                if (collectionCommand.OldItems != null)
                    collectionCommand.Collection.AddRange(collectionCommand.StartingIndex, collectionCommand.OldItems);

                if (collectionCommand.NewItems != null)
                    collectionCommand.Collection.RemoveRange(collectionCommand.StartingIndex, collectionCommand.NewItems);

                if (collectionCommand.Sender is ModelBase)
                {
                    (collectionCommand.Sender as ModelBase).IsModified = command.ModifiedState;
                }

                if (createRedoCommand == true)
                {
                    this.RedoStack.Push(AtomicCollectionUndoRedoCommand.CreateRedoCommand(collectionCommand));
                    if (Saved == true)
                    {
                        Saved = false;
                        this.RedoStack.Peek().ModifiedState = !collectionCommand.ModifiedState;
                    }
                }
            }
            else if (command is CompositeUndoRedoCommand)
            {
                CompositeUndoRedoCommand compositeCommand = command as CompositeUndoRedoCommand;

                foreach (IUndoRedoCommand childCommand in compositeCommand.Children.Reverse())
                {
                    this.UndoCommand(childCommand, false);
                }
                compositeCommand.Sender.IsModified = command.ModifiedState;

                if (createRedoCommand == true)
                {
                    this.RedoStack.Push(CompositeUndoRedoCommand.CreateRedoCommand(compositeCommand));
                    if (Saved == true)
                    {
                        Saved = false;
                        this.RedoStack.Peek().ModifiedState = !compositeCommand.ModifiedState;
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void Redo()
        {
            PerformingCommand = true;

            if (this.RedoStack.Count > 0)
            {
                IUndoRedoCommand command = this.RedoStack.Pop();
                RedoCommand(command, true);
            }
            PerformingCommand = false;
        }

        private void RedoCommand(IUndoRedoCommand command, Boolean createUndoCommand)
        {
            if (command is AtomicUndoRedoCommand)
            {
                AtomicUndoRedoCommand atomicCommand = command as AtomicUndoRedoCommand;
                if (atomicCommand.Sender is ModelBase)
                {
                    (atomicCommand.Sender as ModelBase).SetPropertyValue(atomicCommand.PreviousValue, atomicCommand.NewValue, atomicCommand.Property, atomicCommand.Setter);
                }
                else
                {
                    atomicCommand.Setter(atomicCommand.PreviousValue);
                }

                if (createUndoCommand == true)
                {
                    atomicCommand.Sender.IsModified = command.ModifiedState;
                    this.UndoStack.Push(AtomicUndoRedoCommand.CreateUndoCommand(atomicCommand));
                    if (Saved == true)
                    {
                        Saved = false;
                        this.UndoStack.Peek().ModifiedState = !atomicCommand.ModifiedState;
                    }
                }
            }
            else if (command is AtomicCollectionUndoRedoCommand)
            {
                AtomicCollectionUndoRedoCommand collectionCommand = command as AtomicCollectionUndoRedoCommand;

                if (collectionCommand.OldItems != null)
                    collectionCommand.Collection.AddRange(collectionCommand.StartingIndex, collectionCommand.OldItems);

                if (collectionCommand.NewItems != null)
                    collectionCommand.Collection.RemoveRange(collectionCommand.StartingIndex, collectionCommand.NewItems);

                if (collectionCommand.Sender is ModelBase)
                {
                    (collectionCommand.Sender as ModelBase).IsModified = command.ModifiedState;
                }

                if (createUndoCommand == true)
                {
                    this.UndoStack.Push(AtomicCollectionUndoRedoCommand.CreateUndoCommand(collectionCommand));
                    if (Saved == true)
                    {
                        Saved = false;
                        this.UndoStack.Peek().ModifiedState = !collectionCommand.ModifiedState;
                    }
                }
            }
            else if (command is CompositeUndoRedoCommand)
            {
                CompositeUndoRedoCommand compositeCommand = command as CompositeUndoRedoCommand;
                foreach (IUndoRedoCommand childCommand in compositeCommand.Children)
                {
                    this.RedoCommand(childCommand, false);
                }
                compositeCommand.Sender.IsModified = command.ModifiedState;

                if (createUndoCommand == true)
                {
                    this.UndoStack.Push(CompositeUndoRedoCommand.CreateUndoCommand(compositeCommand));
                    if (Saved == true)
                    {
                        Saved = false;
                        this.UndoStack.Peek().ModifiedState = !compositeCommand.ModifiedState;
                    }
                }
            }
        }

        #endregion // Undo/Redo

        #region Model Registration Methods

        /// <summary>
        /// Register a ViewModel with this command manager.
        /// </summary>
        /// Any undoable property change will be managed by this command 
        /// manager.
        /// <param name="m">Model object</param>
        public void RegisterModel(IModel m)
        {
            Debug.Assert(!RegisteredModels.Contains(m),
                "Application Error: Model already registered.  Aborting.");

            if (RegisteredModels.Contains(m))
                return;

            if (RegisteredManagers == null)
                RegisteredManagers = new Dictionary<CommandManager, List<IModel>>();

            if (!RegisteredManagers.ContainsKey(this))
                RegisteredManagers.Add(this, new List<IModel>());

            RegisteredModels.Add(m);
            RegisteredManagers[this].Add(m);
            m.PropertyChanged += OnPropertyChanged;
        }
        
        /// <summary>
        /// Register a ViewModel with this command manager.
        /// </summary>
        /// Any undoable property change will be managed by this command 
        /// manager.
        /// <param name="m">Model object</param>
        public void RegisterModels(List<IModel> models)
        {
            foreach (IModel model in models)
            {
                RegisterModel(model);
            }
        }

        /// <summary>
        /// Unregister a ViewModel with this command manager.
        /// </summary>
        /// Any undoable property change will no longer be managed by this
        /// command manager.
        /// <param name="m">Model object</param>
        public void UnregisterModel(IModel m)
        {
            Debug.Assert(RegisteredModel(m),
                "Application Error: Model not registered.  Aborting.");

            if (!RegisteredModel(m))
                return;

            RegisteredModels.Remove(m);
            RegisteredManagers[this].Remove(m);
            m.PropertyChanged -= OnPropertyChanged;
        }

        /// <summary>
        /// Register a ViewModel with this command manager.
        /// </summary>
        /// Any undoable property change will be managed by this command 
        /// manager.
        /// <param name="m">Model object</param>
        public void UnRegisterModels(List<IModel> models)
        {
            foreach (IModel model in models)
            {
                UnregisterModel(model);
            }
        }

        /// <summary>
        /// Unregisteres all the models currently registered with this command manager
        /// </summary>
        public void UnregisterAllModels()
        {
            while (this.RegisteredModels.Count > 0)
            {
                IModel model = this.RegisteredModels.First();
                UnregisterModel(model);
            }
        }

        public static CommandManager CommandManagerForModel(IModel model)
        {
            foreach (KeyValuePair<CommandManager, List<IModel>> kvp in RegisteredManagers)
            {
                if (kvp.Value.Contains(model))
                    return kvp.Key;
            }

            return null;
        }

        #endregion // Model Registration Methods

        #endregion // Controller Methods

        #region Private Functions

        /// <summary>
        /// Returns true if the model has already been collected in the
        /// model registeration
        /// </summary>
        private Boolean RegisteredModel(IModel model)
        {
            foreach (IModel collectedModel in RegisteredModels)
            {
                if (ReferenceEquals(model, collectedModel))
                    return true;
            }
            return false;
        }

        #endregion // Private Functions

        #region Event Callbacks

        /// <summary>
        /// 
        /// </summary>
        private void OnPropertyChanged(Object sender, PropertyChangedEventArgs e)
        {
            IUndoRedoCommand command = null;
            if (e is EditorPropertyChangedEventArgs)
            {
                command = CreateUndoRedoCommand((e as EditorPropertyChangedEventArgs).OriginalModel, e);
            }
            else
            {
                command = CreateUndoRedoCommand(sender as IModel, e);
            }
            if (command != null)
            {
                if (this.IsBatching)
                {
                    if (FirstBatchSender == null)
                    {
                        if (e is EditorPropertyChangedEventArgs)
                        {
                            FirstBatchSender = (e as EditorPropertyChangedEventArgs).OriginalModel;
                        }
                        else
                        {
                            FirstBatchSender = sender as IModel;
                        }
                    }
                    this.UndoRedoBlock.Add(command);
                }
                else
                {
                    this.UndoStack.Push(command);
                    this.RedoStack.Clear();
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private IUndoRedoCommand CreateUndoRedoCommand(IModel parent, PropertyChangedEventArgs e)
        {
            if (this.PerformingCommand == true)
                return null;

            if (e is EditorPropertyChangedEventArgs)
            {
                EditorPropertyChangedEventArgs args = e as EditorPropertyChangedEventArgs;
                args.Handled = true;
                if (args.CreateUndoCommand == false)
                    return null;

                if (args.IsCollectionChange == false)
                {
                    if (args is EditorCompositeChangedEventArgs)
                    {
                        EditorCompositeChangedEventArgs compositeCommand = args as EditorCompositeChangedEventArgs;

                        List<IUndoRedoCommand> childCommands = new List<IUndoRedoCommand>();
                        foreach (PropertyChangedEventArgs childArgs in compositeCommand.EventCollection)
                        {
                            childCommands.Add(CreateUndoRedoCommand(parent, childArgs));
                        }
                        CompositeUndoRedoCommand command = new CompositeUndoRedoCommand(parent, compositeCommand.Tag, compositeCommand.ModifiedState, childCommands);
                        return command;
                    }
                    else
                    {
                        PropertyInfo propertyInfo = args.Property;
                        if (this.UndoStack.Count > 0)
                        {
                            IUndoRedoCommand peek = this.UndoStack.Peek();
                            if (peek is AtomicUndoRedoCommand)
                            {
                                AtomicUndoRedoCommand atomicCommand = peek as AtomicUndoRedoCommand;
                                if ((propertyInfo == atomicCommand.Property) && ReferenceEquals(parent, atomicCommand.Sender))
                                {
                                    Boolean collapse = false;
                                    Object[] attributes = propertyInfo.GetCustomAttributes(typeof(CollapsableProperty), true);
                                    if (attributes.Length > 0)
                                    {
                                        collapse = true;
                                        foreach (CollapsableProperty attribute in attributes)
                                        {
                                            if (!attribute.Collapse(args.OldValue, args.NewValue))
                                            {
                                                collapse = false;
                                                break;
                                            }
                                        }
                                    }
                                    if (collapse == true)
                                    {
                                        (peek as AtomicUndoRedoCommand).NewValue = args.NewValue;
                                        this.UndoStack.Pop();
                                        return peek;
                                    }
                                }
                            }
                        }

                        AtomicUndoRedoCommand command = new AtomicUndoRedoCommand(parent, args.Property, args.Setter, args.OldValue, args.NewValue, args.ModifiedState);
                        return command;
                    }
                }
                else
                {
                    if (e is EditorCollectionChangedEventArgs)
                    {
                        EditorCollectionChangedEventArgs collectionArgs = e as EditorCollectionChangedEventArgs;

                        AtomicCollectionUndoRedoCommand command = new AtomicCollectionUndoRedoCommand(parent, collectionArgs.OldValue as IList,
                            collectionArgs.NewValue as IList, collectionArgs.StartingIndex, collectionArgs.ModifiedState, collectionArgs.Collection);
                        
                        return command;
                    }
                }
            }
            return null;
        }

        #endregion // Event Callbacks

        #region Batching

        private bool IsBatching
        {
            get;
            set;
        }

        private List<IUndoRedoCommand> UndoRedoBlock = new List<IUndoRedoCommand>();
        private event EventHandler UndoRedoBlockFinished;
        private IModel FirstBatchSender;

        internal void StartUndoRedoBlock()
        {
            UndoRedoBlock.Clear();
            IsBatching = true;
            FirstBatchSender = null;
        }

        internal void FinishUndoRedoBlock()
        {
            if (UndoRedoBlock.Count > 0)
            {

                CompositeUndoRedoCommand command = new CompositeUndoRedoCommand(FirstBatchSender, string.Empty, UndoRedoBlock[0].ModifiedState, this.UndoRedoBlock);
                this.UndoStack.Push(command);
                this.RedoStack.Clear();
            }

            IsBatching = false;
            FirstBatchSender = null;
        }
        #endregion
    } // CommandManager
} // RSG.Base.Editor.Command namespace