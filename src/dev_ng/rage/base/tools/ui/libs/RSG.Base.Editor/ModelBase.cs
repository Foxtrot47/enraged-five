﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq.Expressions;
using System.Reflection;
using RSG.Base.Editor.Command;
using RSG.Base.Editor.Search;
using RSG.Base.Editor.Collections;
using System.Runtime.Serialization;

namespace RSG.Base.Editor
{
    public class ModifiedStateChangedEventArgs : EventArgs
    {        
        #region Members

        private IModel m_model;
        private IModel m_originalModel;
        private Boolean m_handled;

        #endregion // Members

        #region Properties

        public IModel Model
        {
            get { return m_model; }
            internal set { m_model = value; }
        }

        public IModel OriginalModel
        {
            get { return m_originalModel; }
            private set { m_originalModel = value; }
        }

        public Boolean Handled
        {
            get { return m_handled; }
            set { m_handled = value; }
        }

        #endregion // Properties

        #region Constructors

        public ModifiedStateChangedEventArgs(IModel model, IModel original)
        {
            this.Model = model;
            this.OriginalModel = original;
        }
        
        #endregion Constructors
    }

    public delegate void ModifiedStateChangedEventHandler(IModel model, ModifiedStateChangedEventArgs e);

    /// <summary>
    /// Abstract base model class.
    /// </summary>
    /// <seealso cref="ViewModelBase" />
    /// <seealso cref="IModel" />
    /// <seealso cref="ISearchable" />
    /// 
    /// This is the abstract base model class; it supports undo/redo on 
    /// properties and its recommended it is used as the base class for all
    /// parts of a model requiring such functionality.
    /// 
    /// This abstract base class also supports simple property value searching,
    /// through the ISearchable interface.
    /// 
    /// Subclasses should have property set accessors that invoke the protected
    /// 'SetPropertyValue' method.
    /// 
    [DataContract]
    [Serializable]
    public abstract class ModelBase : 
        IModel,
        ISearchable,
        IModelUndoRedoProperties
    {
        #region Events

        /// <summary>
        /// Event raised when a property in the Model is about to change.
        /// </summary>
        [field: NonSerialized]
        public event PropertyChangingEventHandler PropertyChanging;

        /// <summary>
        /// Event raised when a property in the Model has changed.
        /// </summary>
        [field: NonSerialized]
        public event PropertyChangedEventHandler PropertyChanged;
        
        /// <summary>
        /// 
        /// </summary>
        [field: NonSerialized]
        public event ModifiedStateChangedEventHandler ModifiedStateChanged;

        #endregion // Events

        #region Fields
        /// <summary>
        /// The private set containing all of this types property names, that can be used to
        /// validate the <see cref="SetPropertyValue{T}"/> method.
        /// </summary>
        private static Dictionary<Type, HashSet<string>> propertyNames = new Dictionary<Type, HashSet<string>>();

        /// <summary>
        /// A generic object used to sync the creation of the <see cref="propertyNames"/> field
        /// across threads.
        /// </summary>
        private static object syncObject = new object();

        /// <summary>
        /// The private field used for the <see cref="IsModified"/> property.
        /// </summary>
        private bool m_isModified;

        /// <summary>
        /// The private field used for the <see cref="IsModified"/> property.
        /// </summary>
        private bool m_isBatching;
        #endregion

        #region Properties

        /// <summary>
        /// Used to tell whether there has been any property changes in the
        /// model
        /// </summary>
        [Browsable(false)]
        public Boolean IsModified
        {
            get { return m_isModified; }
            set 
            {
                if (value == m_isModified)
                    return;

                m_isModified = value;
                OnModifiedStateChanged();
            }
        }

        [Browsable(false)]
        public Boolean IsBatching
        {
            get
            {
                bool isBatching = this.m_isBatching;
                if (!isBatching && this is IHierarchicalModel)
                {
                    IModel parent = (this as IHierarchicalModel).ParentModel;
                    while (parent != null)
                    {
                        if (parent is IModel)
                        {
                            if ((parent as IModel).IsBatching)
                            {
                                isBatching = true;
                                break;
                            }
                        }

                        if (parent is IHierarchicalModel)
                        {
                            parent = (parent as IHierarchicalModel).ParentModel;
                        }
                        else
                        {
                            parent = null;
                        }
                    }

                    //IModel parent = (this as HierarchicalModelBase).ParentModel;
                    //if (parent != null && parent is ModelBase)
                    //    return (parent as ModelBase).IsBatching;
                }
                return isBatching;

            }
            private set { m_isBatching = value; }
        }

        /// <summary>
        /// A dictionary of all the observable collections and there current value
        /// </summary>
        private Dictionary<String, KeyValuePair<PropertyInfo, IModelNotifyingCollection>> NotifyingCollections
        {
            get
            {
                if (m_notifyingCollections == null)
                {
                    m_notifyingCollections = new Dictionary<String, KeyValuePair<PropertyInfo, IModelNotifyingCollection>>();
                }

                return m_notifyingCollections;
            }
            //set { m_notifyingCollections = value; }
        }
        [NonSerialized]
        private Dictionary<String, KeyValuePair<PropertyInfo, IModelNotifyingCollection>> m_notifyingCollections;

        #endregion // Properties

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="RSG.Base.Editor.ModelBase"/> class.
        /// </summary>
        public ModelBase()
        {
        }
        #endregion

        #region Event Callbacks

        internal void RegisterModelCollection<T>(ModelObservableCollection<T> collection)
        {
            if (!this.NotifyingCollections.ContainsKey(collection.PropertyName))
            {
                collection.CollectionChanged += OnCollectionChanged;
                collection.CollectionChanging += OnCollectionChanging;
                this.NotifyingCollections.Add(collection.PropertyName, new KeyValuePair<PropertyInfo, IModelNotifyingCollection>(null, collection));
            }
            else if (this.NotifyingCollections[collection.PropertyName].Value != null)
            {
                this.NotifyingCollections[collection.PropertyName].Value.CollectionChanged -= OnCollectionChanged;
                this.NotifyingCollections[collection.PropertyName].Value.CollectionChanging += OnCollectionChanging;

                PropertyInfo propertyInfo = this.NotifyingCollections[collection.PropertyName].Key;
                this.NotifyingCollections[collection.PropertyName] = new KeyValuePair<PropertyInfo, IModelNotifyingCollection>(propertyInfo, collection);
                collection.CollectionChanged += OnCollectionChanged;
                collection.CollectionChanging += OnCollectionChanging;
            }
        }

        private Boolean modifiedStateBeforeCollectionChange = false;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void OnCollectionChanging(Object sender, NotifyCollectionChangedEventArgs e)
        {
            modifiedStateBeforeCollectionChange = this.IsModified;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void OnCollectionChanged(Object sender, NotifyCollectionChangedEventArgs e)
        {
            if (sender is IModelNotifyingCollection && sender is IModelObservableCollection)
            {
                IModelNotifyingCollection collection = sender as IModelNotifyingCollection;
                if (this.NotifyingCollections.ContainsKey(collection.PropertyName))
                {
                    if (this.NotifyingCollections[collection.PropertyName].Key == null)
                    {
                        IModelNotifyingCollection storedCollection = this.NotifyingCollections[collection.PropertyName].Value;

                        this.NotifyingCollections[collection.PropertyName] = 
                            new KeyValuePair<PropertyInfo, IModelNotifyingCollection>(this.GetType().GetProperty(collection.PropertyName), storedCollection);
                    }
                    if (this.NotifyingCollections[collection.PropertyName].Key == null)
                        return;

                    PropertyInfo propertyInfo = this.NotifyingCollections[collection.PropertyName].Key;
                    Object[] undoRedo = propertyInfo.GetCustomAttributes(typeof(UndoableProperty), true);
                    Boolean createUndoCommand = false;
                    if (undoRedo.Length > 0)
                    {
                        createUndoCommand = true;
                    }

                    if (this.IsBatching == false)
                    {
                        if (e.Action == NotifyCollectionChangedAction.Add)
                        {
                            this.SendCollectionChangedEvent(collection.PropertyName, this, this, e.OldItems, e.NewItems,
                                    propertyInfo, modifiedStateBeforeCollectionChange, createUndoCommand, sender as IModelObservableCollection, e.NewStartingIndex);
                        }
                        else
                        {
                            this.SendCollectionChangedEvent(collection.PropertyName, this, this, e.OldItems, e.NewItems,
                                    propertyInfo, modifiedStateBeforeCollectionChange, createUndoCommand, sender as IModelObservableCollection, e.OldStartingIndex);
                        }
                    }
                    else
                    {
                        if (e.Action == NotifyCollectionChangedAction.Add)
                        {
                            this.UndoRedoBlock.Add(new EditorCollectionChangedEventArgs(collection.PropertyName, this, this, e.OldItems, e.NewItems,
                                    propertyInfo, modifiedStateBeforeCollectionChange, createUndoCommand, sender as IModelObservableCollection, e.NewStartingIndex));
                        }
                        else
                        {
                            this.UndoRedoBlock.Add(new EditorCollectionChangedEventArgs(collection.PropertyName, this, this, e.OldItems, e.NewItems,
                                    propertyInfo, modifiedStateBeforeCollectionChange, createUndoCommand, sender as IModelObservableCollection, e.OldStartingIndex));
                        }
                    }
                }
            }
        }

        #endregion // Event Callbacks

        #region Methods
        /// <summary>
        /// Sets the given field to the given value making sure the correct property changed
        /// event is fired.
        /// </summary>
        /// <typeparam name="T">
        /// The type the property that is getting set is.
        /// </typeparam>
        /// <param name="field">
        /// A reference to the property that is getting set.
        /// </param>
        /// <param name="value">
        /// The value to set the property to.
        /// </param>
        /// <param name="names">
        /// A array of property names that are changed due to this one change.
        /// </param>
        /// <returns>
        /// True if the specified field was set to the new value; otherwise, false. If false
        /// this usually indicates that the field is already set to the specified value.
        /// </returns>
        protected bool SetPropertyValue<T>(ref T field, T value, params string[] names)
        {
            this.ValidateSetPropertyValue(names);

            if (EqualityComparer<T>.Default.Equals(field, value))
            {
                return false;
            }

            T oldValue = field;
            field = value;

            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (names == null || handler == null || names.Length == 0)
            {
                return true;
            }

            foreach (string name in names)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }

            return true;
        }

        /// <summary>
        /// Property set helper method.
        /// </summary>
        /// This updates a property and makes sure that the current property 
        /// change events are fired.  It also looks into the attributes of the 
        /// property and if the property in undoable fires off the correct undo 
        /// events so that any undo managers listening will be able to create 
        /// the undo stack object.
        ///
        /// <typeparam name="T">The type of the property that has changed</typeparam>
        /// <param name="newValue">The new value of the property</param>
        /// <param name="oldValueExpression">The expression of the property so we can get the old value</param>
        /// <param name="setter">The delegate function that will be called to actually update the property</param>
        /// <returns></returns>
        [Obsolete]
        protected virtual void SetPropertyValue<T>(T newValue, Expression<Func<T>> oldValueExpression, PropertySetDelegate setter)
        {
            Func<T> getter = oldValueExpression.Compile();
            T oldValue = getter();
            this.SetPropertyValue(newValue, oldValue, oldValueExpression, setter);
        }

        /// <summary>
        /// Property set helper method.
        /// </summary>
        /// This updates a property and makes sure that the current property 
        /// change events are fired.  It also looks into the attributes of the 
        /// property and if the property in undoable fires off the correct undo 
        /// events so that any undo managers listening will be able to create 
        /// the undo stack object.
        ///
        /// <typeparam name="T">The type of the property that has changed</typeparam>
        /// <param name="newValue">The new value of the property</param>
        /// <param name="oldValueExpression">The expression of the property so we can get the old value</param>
        /// <param name="setter">The delegate function that will be called to actually update the property</param>
        /// <returns></returns>
        [Obsolete]
        protected void SetPropertyValue<T>(T newValue, T oldValue, Expression<Func<T>> memberExpression, PropertySetDelegate setter)
        {
            SetPropertyValue(newValue, oldValue, memberExpression, setter, null);
        }

        /// <summary>
        /// Property set helper method.
        /// </summary>
        /// This updates a property and makes sure that the current property 
        /// change events are fired.  It also looks into the attributes of the 
        /// property and if the property in undoable fires off the correct undo 
        /// events so that any undo managers listening will be able to create 
        /// the undo stack object.
        ///
        /// <typeparam name="T">The type of the property that has changed</typeparam>
        /// <param name="newValue">The new value of the property</param>
        /// <param name="oldValueExpression">The expression of the property so we can get the old value</param>
        /// <param name="setter">The delegate function that will be called to actually update the property</param>
        /// <returns></returns>
        [Obsolete]
        protected void SetPropertyValue<T>(T newValue, T oldValue, Expression<Func<T>> memberExpression, PropertySetDelegate setter, ModelPropertyMetadata<T> metadata)
        {
            if ((newValue == null && oldValue == null) || 
                (newValue != null && newValue.Equals(oldValue)) || 
                (oldValue != null && oldValue.Equals(newValue)))
                return;

            var method = (memberExpression.Body as MemberExpression);
            var propertyInfo = (method.Member as PropertyInfo);
            this.SetPropertyValue<T>(newValue, oldValue, propertyInfo, setter, metadata);
        }

        /// <summary>
        /// 
        /// </summary>
        [Obsolete]
        internal void SetPropertyValue<T>(T newValue, T oldValue, PropertyInfo propertyInfo, PropertySetDelegate setter)
        {
            SetPropertyValue(newValue, oldValue, propertyInfo, setter, null);
        }

        /// <summary>
        /// 
        /// </summary>
        [Obsolete]
        internal void SetPropertyValue<T>(T newValue, T oldValue, PropertyInfo propertyInfo, PropertySetDelegate setter, ModelPropertyMetadata<T> metadata)
        {
            if ((newValue == null && oldValue == null) ||
                (newValue != null && newValue.Equals(oldValue)) ||
                (oldValue != null && oldValue.Equals(newValue)))
                return;
            
            Object[] undoRedo = propertyInfo.GetCustomAttributes(typeof(UndoableProperty), true);
            Boolean createUndoCommand = false;
            if (undoRedo.Length > 0)
            {
                createUndoCommand = true;
            }

            OnPropertyChanging(propertyInfo.Name);
            setter(newValue);
            Boolean modifiedStateBeforeChange = this.IsModified;

            if (this.IsBatching == false)
            {
                SendPropertyChangedEvent(oldValue, newValue, propertyInfo, setter, createUndoCommand, modifiedStateBeforeChange);
            }
            else
            {
                this.UndoRedoBlock.Add(new EditorPropertyChangedEventArgs(propertyInfo.Name, this, this, oldValue, newValue,
                        propertyInfo, setter, modifiedStateBeforeChange, createUndoCommand, false));
            }

            if (metadata != null && metadata.PropertyChangedCallback != null)
                metadata.PropertyChangedCallback(this, new ModelPropertyChangedEventArgs<T>(oldValue, newValue));
        }

        /// <summary>
        /// Used to manually fire off a property change based on a property name.
        /// This should be avoided if it can be
        /// </summary>
        /// <param name="name"></param>
        protected virtual void NotifyPropertyChanged(String name)
        {
            OnPropertyChanging(name);
            OnPropertyChanged(name);
        }

        protected void SendPropertyChangedEvent<T>(T oldValue, T newValue, PropertyInfo propertyInfo, PropertySetDelegate setter, Boolean createUndoCommand, Boolean modifiedStateBeforeChange)
        {
            EditorPropertyChangedEventArgs e = new EditorPropertyChangedEventArgs(propertyInfo.Name, this, this, oldValue, newValue,
                    propertyInfo, setter, modifiedStateBeforeChange, createUndoCommand, false);

            SendPropertyChangedEvent(e);
        }
        
        protected void SendCompositePropertyChangedEvent()
        {
            EditorCompositeChangedEventArgs e = new EditorCompositeChangedEventArgs(this, this, CompositeTag, ModifiedStateBeforeBlockChanges, true, this.UndoRedoBlock);
            SendCompositePropertyChangedEvent(e);
        }
        
        protected void SendCollectionChangedEvent(String propertyName, IModel model, IModel original, Object oldValue, Object newValue,
            PropertyInfo property, Boolean modifiedState, Boolean createUndoCommand, IModelObservableCollection collection, int startingIndex)
        {
            EditorCollectionChangedEventArgs e = new EditorCollectionChangedEventArgs(propertyName, model, original, oldValue, newValue,
                property, modifiedState, createUndoCommand, collection, startingIndex);

            SendCollectionChangedEvent(e);
        }

        protected void OnModifiedStateChanged()
        {
            ModifiedStateChangedEventArgs e = new ModifiedStateChangedEventArgs(this, this);
            SendModifiedStateChangedEvent(e);
        }

        public virtual void SendPropertyChangedEvent(EditorPropertyChangedEventArgs e)
        {
            if (this.PropertyChanged != null)
                this.PropertyChanged(this, e);
        }

        public virtual void SendCompositePropertyChangedEvent(EditorCompositeChangedEventArgs e)
        {
            if (this.PropertyChanged != null)
                this.PropertyChanged(this, e);
        }

        public virtual void SendCollectionChangedEvent(EditorCollectionChangedEventArgs e)
        {
            if (this.PropertyChanged != null)
                this.PropertyChanged(this, e);
        }

        public virtual void SendModifiedStateChangedEvent(ModifiedStateChangedEventArgs e)
        {
            if (this.ModifiedStateChanged != null)
                this.ModifiedStateChanged(this, e);
        }

        /// <summary>
        /// Validates the list of string names past into the <see cref="SetPropertyValue{T}"/>
        /// method against a valid list of property names and the name of the calling method
        /// found via the stack trace.
        /// </summary>
        /// <param name="names">
        /// The list of names to validate.
        /// </param>
        [Conditional("DEBUG")]
        private void ValidateSetPropertyValue(params string[] names)
        {
            if (names == null || names.Length == 0)
            {
                return;
            }

            HashSet<string> set = null;
            Type type = this.GetType();
            if (!propertyNames.TryGetValue(type, out set))
            {
                lock (syncObject)
                {
                    if (!propertyNames.TryGetValue(type, out set))
                    {
                        set = new HashSet<string>();
                        PropertyInfo[] properties = this.GetType().GetProperties();
                        for (int i = 0; i < properties.Length; i++)
                        {
                            set.Add(properties[i].Name);
                        }
                        propertyNames.Add(type, set);
                    }
                }
            }

            string msg = "Incorrect property name detected.";
            string detailed = "The specified name '{0}' doesn't match any of the valid names.";
            // Check that each specified name is valid.
            foreach (string name in names)
            {
                Debug.Assert(
                    propertyNames[type].Contains(names[0]),
                    msg,
                    detailed,
                    names[0]);
            }
        }
        #endregion

        #region ISearchable Methods

        /// <summary>
        /// Cycles through the properties in this object and determines if any of them match the given search parameters. This 
        /// function also goes through all the children objects that are of type Isearchable and does the same thing with them.
        /// </summary>
        /// <param name="searchField">The name of the propery (fixed name or display name) that we are looking for.</param>
        /// <param name="searchType">The type of the search, whether it be a numeric search or a string search etc.</param>
        /// <param name="comparisonOptions">What sort of comparison should be done to determine if a property is the correct value</param>
        /// <param name="searchOptions">Any additional options that this search should have</param>
        /// <param name="searchText">The actual string that we are searching for, this could be a string represetation of a numeric value or a boolean value</param>
        /// <returns>A list of models that have properties in them that match the search parameters.</returns>
        public List<SearchResult> FindAll(String searchField, SearchType searchType, ComparisonOptions comparisonOptions, SearchOptions searchOptions, String searchText)
        {
            throw new NotImplementedException();
        }
        #endregion // ISearchable Methods

        #region Private Helper Methods

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        public void OnPropertyChanging(PropertyChangingEventArgs e)
        {
            PropertyChangingEventHandler handler = this.PropertyChanging;
            if (null != handler)
                handler(this, e);
        }

        /// <summary>
        /// Method to allow easy raising of the OnPropertyChanging event.
        /// </summary>
        /// <param name="propertyName"></param>
        public void OnPropertyChanging(String propertyName)
        {
            OnPropertyChanging(new PropertyChangingEventArgs(propertyName));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        public virtual void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (null != handler)
                handler(this, e);
        }

        /// <summary>
        /// Method to allow easy raising of the OnPropertyChanged event.
        /// </summary>
        /// <param name="propertyName"></param>
        public virtual void OnPropertyChanged(String propertyName)
        {
            OnPropertyChanged(new PropertyChangedEventArgs(propertyName));
        }

        #endregion // Private Helper Methods

        #region IModelUndoRedoProperties

        [NonSerialized]
        private List<EditorPropertyChangedEventArgs> UndoRedoBlock = new List<EditorPropertyChangedEventArgs>();
        [NonSerialized]
        private Boolean ModifiedStateBeforeBlockChanges = false;
        [NonSerialized]
        private String CompositeTag = String.Empty;

        void IModelUndoRedoProperties.StartUndoRedoBlock(String tag)
        {
            UndoRedoBlock.Clear();
            ModifiedStateBeforeBlockChanges = this.IsModified;
            this.CompositeTag = tag;
            this.IsBatching = true;
        }

        void IModelUndoRedoProperties.FinishUndoRedoBlock()
        {
            if (this.UndoRedoBlock.Count > 0)
            {
                this.SendCompositePropertyChangedEvent();
            }

            this.IsBatching = false;
        }

        #endregion // IModelUndoRedoProperties
    } // ModelBase

    public abstract class HierarchicalModelBase : ModelBase, IHierarchicalModel
    {
        #region Members

        protected IModel m_parentModel;

        #endregion // Members

        #region Properties

        [DontRegister()]
        public IModel ParentModel
        {
            get { return m_parentModel; }
            set
            {
                IModel oldValue = m_parentModel;
                m_parentModel = value;

                OnParentModelChanged(oldValue, value);
            }
        }

        #endregion // Properties

        #region Constructor

        public HierarchicalModelBase(IModel parent)
        {
            this.ParentModel = parent;
        }

        #endregion // Constructor

        #region Override

        public override void SendPropertyChangedEvent(EditorPropertyChangedEventArgs e)
        {
            base.SendPropertyChangedEvent(e);

            if (e.Handled == false && ParentModel != null)
            {
                e.Model = ParentModel;
                ParentModel.SendPropertyChangedEvent(e);
            }
        }

        public override void SendCompositePropertyChangedEvent(EditorCompositeChangedEventArgs e)
        {
            base.SendCompositePropertyChangedEvent(e);

            if (e.Handled == false && ParentModel != null)
            {
                e.Model = ParentModel;
                ParentModel.SendCompositePropertyChangedEvent(e);
            }
        }

        public override void SendCollectionChangedEvent(EditorCollectionChangedEventArgs e)
        {
            base.SendCollectionChangedEvent(e);

            if (e.Handled == false && ParentModel != null)
            {
                e.Model = ParentModel;
                ParentModel.SendCollectionChangedEvent(e);
            }
        }

        public override void SendModifiedStateChangedEvent(ModifiedStateChangedEventArgs e)
        {
            base.SendModifiedStateChangedEvent(e);

            if (e.Handled == false && ParentModel != null)
            {
                e.Model = ParentModel;
                ParentModel.SendModifiedStateChangedEvent(e);
            }
        }

        #endregion // Override

        #region Virtual Functions

        protected virtual void OnParentModelChanged(IModel oldValue, IModel newValue)
        {
        }

        #endregion // Virtual Functions
    } // HierarchicalModelBase

    public abstract class ModelBaseDictionary<TKey, TValue> : Dictionary<TKey, TValue>,
        IModel,
        ISearchable,
        IModelUndoRedoProperties
    {
        #region Events

        /// <summary>
        /// Event raised when a property in the Model is about to change.
        /// </summary>
        public event PropertyChangingEventHandler PropertyChanging;

        /// <summary>
        /// Event raised when a property in the Model has changed.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;
        
        /// <summary>
        /// 
        /// </summary>
        public event ModifiedStateChangedEventHandler ModifiedStateChanged;

        #endregion // Events

        #region Members

        private Boolean m_isModified;
        private Boolean m_isBatching;

        #endregion // Members

        #region Properties

        /// <summary>
        /// Used to tell whether there has been any property changes in the
        /// model
        /// </summary>
        [Browsable(false)]
        public Boolean IsModified
        {
            get { return m_isModified; }
            set 
            {
                if (value == m_isModified)
                    return;

                m_isModified = value;
                OnModifiedStateChanged();
            }
        }

        public Boolean IsBatching
        {
            get { return m_isBatching; }
            private set { m_isBatching = value; }
        }

        /// <summary>
        /// A dictionary of all the observable collections and there current value
        /// </summary>
        private Dictionary<String, KeyValuePair<PropertyInfo, IModelNotifyingCollection>> NotifyingCollections
        {
            get { return m_notifyingCollections; }
            set { m_notifyingCollections = value; }
        }
        private Dictionary<String, KeyValuePair<PropertyInfo, IModelNotifyingCollection>> m_notifyingCollections;

        #endregion // Properties

        #region Constructor(s)
        
        /// <summary>
        /// Default constructor.
        /// </summary>
        public ModelBaseDictionary()
        {
            this.NotifyingCollections = new Dictionary<String, KeyValuePair<PropertyInfo, IModelNotifyingCollection>>();
        }

        #endregion // Constructor(s)

        #region Event Callbacks

        internal void RegisterModelCollection<T>(ModelObservableCollection<T> collection)
        {
            if (!this.NotifyingCollections.ContainsKey(collection.PropertyName))
            {
                collection.CollectionChanged += OnCollectionChanged;
                collection.CollectionChanging += OnCollectionChanging;
                this.NotifyingCollections.Add(collection.PropertyName, new KeyValuePair<PropertyInfo, IModelNotifyingCollection>(null, collection));
            }
            else if (this.NotifyingCollections[collection.PropertyName].Value != null)
            {
                this.NotifyingCollections[collection.PropertyName].Value.CollectionChanged -= OnCollectionChanged;
                this.NotifyingCollections[collection.PropertyName].Value.CollectionChanging += OnCollectionChanging;

                PropertyInfo propertyInfo = this.NotifyingCollections[collection.PropertyName].Key;
                this.NotifyingCollections[collection.PropertyName] = new KeyValuePair<PropertyInfo, IModelNotifyingCollection>(propertyInfo, collection);
                collection.CollectionChanged += OnCollectionChanged;
                collection.CollectionChanging += OnCollectionChanging;
            }
        }

        private Boolean modifiedStateBeforeCollectionChange = false;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void OnCollectionChanging(Object sender, NotifyCollectionChangedEventArgs e)
        {
            modifiedStateBeforeCollectionChange = this.IsModified;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void OnCollectionChanged(Object sender, NotifyCollectionChangedEventArgs e)
        {
            if (sender is IModelNotifyingCollection && sender is IModelObservableCollection)
            {
                IModelNotifyingCollection collection = sender as IModelNotifyingCollection;
                if (this.NotifyingCollections.ContainsKey(collection.PropertyName))
                {
                    if (this.NotifyingCollections[collection.PropertyName].Key == null)
                    {
                        IModelNotifyingCollection storedCollection = this.NotifyingCollections[collection.PropertyName].Value;

                        this.NotifyingCollections[collection.PropertyName] = 
                            new KeyValuePair<PropertyInfo, IModelNotifyingCollection>(this.GetType().GetProperty(collection.PropertyName), storedCollection);
                    }
                    if (this.NotifyingCollections[collection.PropertyName].Key == null)
                        return;

                    PropertyInfo propertyInfo = this.NotifyingCollections[collection.PropertyName].Key;
                    Object[] undoRedo = propertyInfo.GetCustomAttributes(typeof(UndoableProperty), true);
                    Boolean createUndoCommand = false;
                    if (undoRedo.Length > 0)
                    {
                        createUndoCommand = true;
                    }

                    if (this.IsBatching == false)
                    {
                        if (this.PropertyChanged != null)
                        {
                            if (e.Action == NotifyCollectionChangedAction.Add)
                            {
                                this.SendCollectionChangedEvent(collection.PropertyName, this, this, e.OldItems, e.NewItems,
                                        propertyInfo, modifiedStateBeforeCollectionChange, createUndoCommand, sender as IModelObservableCollection, e.NewStartingIndex);
                            }
                            else
                            {
                                this.SendCollectionChangedEvent(collection.PropertyName, this, this, e.OldItems, e.NewItems,
                                        propertyInfo, modifiedStateBeforeCollectionChange, createUndoCommand, sender as IModelObservableCollection, e.OldStartingIndex);
                            }
                        }
                    }
                    else
                    {
                        if (e.Action == NotifyCollectionChangedAction.Add)
                        {
                            this.UndoRedoBlock.Add(new EditorCollectionChangedEventArgs(collection.PropertyName, this, this, e.OldItems, e.NewItems,
                                    propertyInfo, modifiedStateBeforeCollectionChange, createUndoCommand, sender as IModelObservableCollection, e.NewStartingIndex));
                        }
                        else
                        {
                            this.UndoRedoBlock.Add(new EditorCollectionChangedEventArgs(collection.PropertyName, this, this, e.OldItems, e.NewItems,
                                    propertyInfo, modifiedStateBeforeCollectionChange, createUndoCommand, sender as IModelObservableCollection, e.OldStartingIndex));
                        }
                    }
                }
            }
        }

        #endregion // Event Callbacks

        #region Protected Methods

        /// <summary>
        /// Property set helper method.
        /// </summary>
        /// This updates a property and makes sure that the current property 
        /// change events are fired.  It also looks into the attributes of the 
        /// property and if the property in undoable fires off the correct undo 
        /// events so that any undo managers listening will be able to create 
        /// the undo stack object.
        ///
        /// <typeparam name="T">The type of the property that has changed</typeparam>
        /// <param name="newValue">The new value of the property</param>
        /// <param name="oldValueExpression">The expression of the property so we can get the old value</param>
        /// <param name="setter">The delegate function that will be called to actually update the property</param>
        /// <returns></returns>
        protected virtual void SetPropertyValue<T>(T newValue, Expression<Func<T>> oldValueExpression, PropertySetDelegate setter)
        {
            Func<T> getter = oldValueExpression.Compile();
            T oldValue = getter();
            this.SetPropertyValue(newValue, oldValue, oldValueExpression, setter);
        }

        /// <summary>
        /// Property set helper method.
        /// </summary>
        /// This updates a property and makes sure that the current property 
        /// change events are fired.  It also looks into the attributes of the 
        /// property and if the property in undoable fires off the correct undo 
        /// events so that any undo managers listening will be able to create 
        /// the undo stack object.
        ///
        /// <typeparam name="T">The type of the property that has changed</typeparam>
        /// <param name="newValue">The new value of the property</param>
        /// <param name="oldValueExpression">The expression of the property so we can get the old value</param>
        /// <param name="setter">The delegate function that will be called to actually update the property</param>
        /// <returns></returns>
        protected void SetPropertyValue<T>(T newValue, T oldValue, Expression<Func<T>> memberExpression, PropertySetDelegate setter)
        {
            SetPropertyValue(newValue, oldValue, memberExpression, setter, null);
        }

        /// <summary>
        /// Property set helper method.
        /// </summary>
        /// This updates a property and makes sure that the current property 
        /// change events are fired.  It also looks into the attributes of the 
        /// property and if the property in undoable fires off the correct undo 
        /// events so that any undo managers listening will be able to create 
        /// the undo stack object.
        ///
        /// <typeparam name="T">The type of the property that has changed</typeparam>
        /// <param name="newValue">The new value of the property</param>
        /// <param name="oldValueExpression">The expression of the property so we can get the old value</param>
        /// <param name="setter">The delegate function that will be called to actually update the property</param>
        /// <returns></returns>
        protected void SetPropertyValue<T>(T newValue, T oldValue, Expression<Func<T>> memberExpression, PropertySetDelegate setter, ModelPropertyMetadata<T> metadata)
        {
            if ((newValue == null && oldValue == null) || 
                (newValue != null && newValue.Equals(oldValue)) || 
                (oldValue != null && oldValue.Equals(newValue)))
                return;

            var method = (memberExpression.Body as MemberExpression);
            var propertyInfo = (method.Member as PropertyInfo);
            this.SetPropertyValue<T>(newValue, oldValue, propertyInfo, setter, metadata);
        }

        /// <summary>
        /// 
        /// </summary>
        internal void SetPropertyValue<T>(T newValue, T oldValue, PropertyInfo propertyInfo, PropertySetDelegate setter)
        {
            SetPropertyValue(newValue, oldValue, propertyInfo, setter, null);
        }

        /// <summary>
        /// 
        /// </summary>
        internal void SetPropertyValue<T>(T newValue, T oldValue, PropertyInfo propertyInfo, PropertySetDelegate setter, ModelPropertyMetadata<T> metadata)
        {
            if ((newValue == null && oldValue == null) ||
                (newValue != null && newValue.Equals(oldValue)) ||
                (oldValue != null && oldValue.Equals(newValue)))
                return;
            
            Object[] undoRedo = propertyInfo.GetCustomAttributes(typeof(UndoableProperty), true);
            Boolean createUndoCommand = false;
            if (undoRedo.Length > 0)
            {
                createUndoCommand = true;
            }

            OnPropertyChanging(propertyInfo.Name);
            setter(newValue);
            Boolean modifiedStateBeforeChange = this.IsModified;

            if (this.IsBatching == false)
            {
                SendPropertyChangedEvent(oldValue, newValue, propertyInfo, setter, createUndoCommand, modifiedStateBeforeChange);
            }
            else
            {
                this.UndoRedoBlock.Add(new EditorPropertyChangedEventArgs(propertyInfo.Name, this, this, oldValue, newValue,
                        propertyInfo, setter, modifiedStateBeforeChange, createUndoCommand, false));
            }

            if (metadata != null && metadata.PropertyChangedCallback != null)
                metadata.PropertyChangedCallback(this, new ModelPropertyChangedEventArgs<T>(oldValue, newValue));
        }

        /// <summary>
        /// Used to manually fire off a property change based on a property name.
        /// This should be avoided if it can be
        /// </summary>
        /// <param name="name"></param>
        protected virtual void NotifyPropertyChanged(String name)
        {
            OnPropertyChanging(name);
            OnPropertyChanged(name);
        }

        protected void SendPropertyChangedEvent<T>(T oldValue, T newValue, PropertyInfo propertyInfo, PropertySetDelegate setter, Boolean createUndoCommand, Boolean modifiedStateBeforeChange)
        {
            EditorPropertyChangedEventArgs e = new EditorPropertyChangedEventArgs(propertyInfo.Name, this, this, oldValue, newValue,
                    propertyInfo, setter, modifiedStateBeforeChange, createUndoCommand, false);

            SendPropertyChangedEvent(e);
        }

        public virtual void SendPropertyChangedEvent(EditorPropertyChangedEventArgs e)
        {
            if (this.PropertyChanged != null)
                this.PropertyChanged(this, e);
        }

        protected void SendCompositePropertyChangedEvent()
        {
            EditorCompositeChangedEventArgs e = new EditorCompositeChangedEventArgs(this, this, CompositeTag, ModifiedStateBeforeBlockChanges, true, this.UndoRedoBlock);
            SendCompositePropertyChangedEvent(e);
        }

        public virtual void SendCompositePropertyChangedEvent(EditorCompositeChangedEventArgs e)
        {
            if (this.PropertyChanged != null)
                this.PropertyChanged(this, e);
        }

        protected void SendCollectionChangedEvent(String propertyName, IModel model, IModel original, Object oldValue, Object newValue,
            PropertyInfo property, Boolean modifiedState, Boolean createUndoCommand, IModelObservableCollection collection, int startingIndex)
        {
            EditorCollectionChangedEventArgs e = new EditorCollectionChangedEventArgs(propertyName, model, original, oldValue, newValue,
                property, modifiedState, createUndoCommand, collection, startingIndex);

            SendCollectionChangedEvent(e);
        }

        public virtual void SendCollectionChangedEvent(EditorCollectionChangedEventArgs e)
        {
            if (this.PropertyChanged != null)
                this.PropertyChanged(this, e);
        }

        protected void OnModifiedStateChanged()
        {
            ModifiedStateChangedEventArgs e = new ModifiedStateChangedEventArgs(this, this);
            SendModifiedStateChangedEvent(e);
        }

        public virtual void SendModifiedStateChangedEvent(ModifiedStateChangedEventArgs e)
        {
            if (this.ModifiedStateChanged != null)
                this.ModifiedStateChanged(this, e);
        }

        #endregion // Protected Methods

        #region ISearchable Methods

        /// <summary>
        /// Cycles through the properties in this object and determines if any of them match the given search parameters. This 
        /// function also goes through all the children objects that are of type Isearchable and does the same thing with them.
        /// </summary>
        /// <param name="searchField">The name of the propery (fixed name or display name) that we are looking for.</param>
        /// <param name="searchType">The type of the search, whether it be a numeric search or a string search etc.</param>
        /// <param name="comparisonOptions">What sort of comparison should be done to determine if a property is the correct value</param>
        /// <param name="searchOptions">Any additional options that this search should have</param>
        /// <param name="searchText">The actual string that we are searching for, this could be a string represetation of a numeric value or a boolean value</param>
        /// <returns>A list of models that have properties in them that match the search parameters.</returns>
        public List<SearchResult> FindAll(String searchField, SearchType searchType, ComparisonOptions comparisonOptions, SearchOptions searchOptions, String searchText)
        {
            throw new NotImplementedException();
        }
        #endregion // ISearchable Methods

        #region Private Helper Methods

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        public void OnPropertyChanging(PropertyChangingEventArgs e)
        {
            PropertyChangingEventHandler handler = this.PropertyChanging;
            if (null != handler)
                handler(this, e);
        }

        /// <summary>
        /// Method to allow easy raising of the OnPropertyChanging event.
        /// </summary>
        /// <param name="propertyName"></param>
        public void OnPropertyChanging(String propertyName)
        {
            OnPropertyChanging(new PropertyChangingEventArgs(propertyName));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        public virtual void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (null != handler)
                handler(this, e);
        }

        /// <summary>
        /// Method to allow easy raising of the OnPropertyChanged event.
        /// </summary>
        /// <param name="propertyName"></param>
        public virtual void OnPropertyChanged(String propertyName)
        {
            OnPropertyChanged(new PropertyChangedEventArgs(propertyName));
        }

        #endregion // Private Helper Methods

        #region IModelUndoRedoProperties

        private List<EditorPropertyChangedEventArgs> UndoRedoBlock = new List<EditorPropertyChangedEventArgs>();
        private Boolean ModifiedStateBeforeBlockChanges = false;
        private String CompositeTag = String.Empty;

        void IModelUndoRedoProperties.StartUndoRedoBlock(String tag)
        {
            UndoRedoBlock.Clear();
            ModifiedStateBeforeBlockChanges = this.IsModified;
            this.CompositeTag = tag;
            this.IsBatching = true;
        }

        void IModelUndoRedoProperties.FinishUndoRedoBlock()
        {
            if (this.UndoRedoBlock.Count > 0)
            {
                this.SendCompositePropertyChangedEvent();
            }

            this.IsBatching = false;
        }

        #endregion // IModelUndoRedoProperties
    }

    public abstract class HierarchicalModelBaseDictionary<TKey, TValue> : ModelBaseDictionary<TKey, TValue>,
        IHierarchicalModel
    {
        #region Members

        protected IModel m_parentModel;

        #endregion // Members

        #region Properties

        [DontRegister()]
        public IModel ParentModel
        {
            get;
            set;
        }

        #endregion // Properties

        #region Constructor

        public HierarchicalModelBaseDictionary(IModel parent)
        {
            this.ParentModel = parent;
        }

        #endregion // Constructor

        #region Override

        public override void SendPropertyChangedEvent(EditorPropertyChangedEventArgs e)
        {
            base.SendPropertyChangedEvent(e);

            if (e.Handled == false && ParentModel is IModel)
            {
                e.Model = ParentModel;
                (ParentModel as IModel).SendPropertyChangedEvent(e);
            }
        }

        public override void SendCompositePropertyChangedEvent(EditorCompositeChangedEventArgs e)
        {
            base.SendCompositePropertyChangedEvent(e);

            if (e.Handled == false && ParentModel is IModel)
            {
                e.Model = ParentModel;
                (ParentModel as IModel).SendCompositePropertyChangedEvent(e);
            }
        }

        public override void SendCollectionChangedEvent(EditorCollectionChangedEventArgs e)
        {
            base.SendCollectionChangedEvent(e);

            if (e.Handled == false && ParentModel is IModel)
            {
                e.Model = ParentModel;
                (ParentModel as IModel).SendCollectionChangedEvent(e);
            }
        }

        public override void SendModifiedStateChangedEvent(ModifiedStateChangedEventArgs e)
        {
            base.SendModifiedStateChangedEvent(e);

            if (e.Handled == false && ParentModel is IModel)
            {
                e.Model = ParentModel;
                (ParentModel as IModel).SendModifiedStateChangedEvent(e);
            }
        }

        #endregion // Override
    } // HierarchicalModelBaseDictionary

} // RSG.Base.Editor namespace
