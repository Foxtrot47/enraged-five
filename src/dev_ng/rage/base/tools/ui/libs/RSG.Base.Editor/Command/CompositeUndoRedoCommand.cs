﻿using System;
using System.Collections.Generic;

namespace RSG.Base.Editor.Command
{

    /// <summary>
    /// Composite undo/redo command.
    /// </summary>
    /// A composite undo/redo command representing a chain of commands that are
    /// done/undone at once.
    /// 
    /// The composite command's Tag is manually specified in the constructor.
    /// 
    public class CompositeUndoRedoCommand : ICompositeUndoRedoCommand
    {
        #region Properties

        /// <summary>
        /// Child undo/redo commands making up this composite command.
        /// </summary>
        public Stack<IUndoRedoCommand> Children
        {
            get;
            protected set;
        }

        /// <summary>
        /// Composite tag; typically for UI display.
        /// </summary>
        public String Tag
        {
            get;
            protected set;
        }

        /// <summary>
        /// The modified state the model was in when 
        /// this command was created
        /// </summary>
        public Boolean ModifiedState
        {
            get;
            set;
        }

        /// <summary>
        /// Original source object that caused the undo command.
        /// </summary>
        public IModel Sender
        {
            get;
            protected set;
        }

        #endregion // Properties

        #region Constructor(s)

        /// <summary>
        /// Default constructor.
        /// </summary>
        public CompositeUndoRedoCommand(IModel model, String tag, Boolean modifiedState, List<IUndoRedoCommand> children)
        {
            this.Tag = (tag.Clone() as String);
            this.Sender = model;
            this.ModifiedState = modifiedState;
            this.Children = new Stack<IUndoRedoCommand>();
            List<IUndoRedoCommand> reversedChildren = new List<IUndoRedoCommand>(children);
            reversedChildren.Reverse();
            bool collection = false;
            foreach (IUndoRedoCommand child in reversedChildren)
            {
                this.Children.Push(child);
                if (child is AtomicCollectionUndoRedoCommand)
                {
                    collection = true;
                }
            }

            if (collection == false)
            {
            
            }          
        }

        /// <summary>
        /// Default constructor.
        /// </summary>
        private CompositeUndoRedoCommand(IModel model, String tag, Boolean modifiedState, Stack<IUndoRedoCommand> children)
        {
            this.Tag = (tag.Clone() as String);
            this.Sender = model;
            this.ModifiedState = modifiedState;
            this.Children = children;
        }

        #endregion // Constructor(s)

        #region Protected Methods

        /// <summary>
        /// Creates the redo representation from the undo command
        /// </summary>
        internal static CompositeUndoRedoCommand CreateRedoCommand(CompositeUndoRedoCommand command)
        {
            List<IUndoRedoCommand> commands = new List<IUndoRedoCommand>();
            foreach (IUndoRedoCommand childCommand in command.Children)
            {
                if (childCommand is AtomicUndoRedoCommand)
                    commands.Add(AtomicUndoRedoCommand.CreateRedoCommand(childCommand as AtomicUndoRedoCommand));
                else if (childCommand is AtomicCollectionUndoRedoCommand)
                    commands.Add(AtomicCollectionUndoRedoCommand.CreateRedoCommand(childCommand as AtomicCollectionUndoRedoCommand));
                else if (childCommand is CompositeUndoRedoCommand)
                    commands.Add(CompositeUndoRedoCommand.CreateRedoCommand(childCommand as CompositeUndoRedoCommand));
            }

            CompositeUndoRedoCommand redoCommand = new CompositeUndoRedoCommand(command.Sender,
                        command.Tag,
                        !command.ModifiedState,
                        commands);

            return redoCommand;
        }

        /// <summary>
        /// Creates the redo representation from the undo command
        /// </summary>
        internal static CompositeUndoRedoCommand CreateUndoCommand(CompositeUndoRedoCommand command)
        {
            List<IUndoRedoCommand> commands = new List<IUndoRedoCommand>();
            foreach (IUndoRedoCommand childCommand in command.Children)
            {
                if (childCommand is AtomicUndoRedoCommand)
                    commands.Add(AtomicUndoRedoCommand.CreateUndoCommand(childCommand as AtomicUndoRedoCommand));
                else if (childCommand is AtomicCollectionUndoRedoCommand)
                    commands.Add(AtomicCollectionUndoRedoCommand.CreateUndoCommand(childCommand as AtomicCollectionUndoRedoCommand));
                else if (childCommand is CompositeUndoRedoCommand)
                    commands.Add(CompositeUndoRedoCommand.CreateUndoCommand(childCommand as CompositeUndoRedoCommand));
            }

            CompositeUndoRedoCommand undoCommand = new CompositeUndoRedoCommand(command.Sender,
                        command.Tag,
                        !command.ModifiedState,
                        commands);

            return undoCommand;
        }

        #endregion // Protected Methods
    }

} // RSG.Base.Editor.Command namespace
