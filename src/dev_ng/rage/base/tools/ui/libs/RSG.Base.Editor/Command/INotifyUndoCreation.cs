﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Base.Editor.Command
{
    internal interface INotifyUndoCreation
    {
        event UndoPropertyChangedCallback UndoPropertyChanged;
    } // INotifyUndoCreation
} // RSG.Base.Editor.Command
