﻿using System;

namespace RSG.Base.Editor.Search
{

    /// <summary>
    /// NonSearchableProperty attribute flags properties that cannot be
    /// searched by the user.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class NonSearchablePropertyAttribute : Attribute
    {
        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public NonSearchablePropertyAttribute()
        {
        }
        #endregion // Constructor(s)
    }

} // RSG.Base.Editor.Search namespace
