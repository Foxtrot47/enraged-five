﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace RSG.Base.Editor
{

    /// <summary>
    /// Our core ViewModel interface.
    /// </summary>
    /// http://kentb.blogspot.com/2009/04/mvvm-infrastructure-viewmodel.html
    /// http://blogs.msdn.com/b/atverma/archive/2010/07/28/inotifypropertychanging-and-inotifypropertychanged-in-real-life-using-expression-trees.aspx
    /// 
    public interface IViewModel : 
        INotifyPropertyChanging,    // Pre-change notification
        INotifyPropertyChanged      // Post-change notification
    {
        #region Properties
        /// <summary>
        /// Selection property for this ViewModel.
        /// </summary>
        bool IsSelected { get; set; }
        #endregion // Properties
    }
    
    /// <summary>
    /// Our core hierarchical ViewModel interface.
    /// </summary>
    public interface IHierarchicalViewModel : 
        IViewModel
    {
        #region Properties
        /// <summary>
        /// Expanded property for this hierarchical ViewModel.
        /// </summary>
        bool IsExpanded { get; set; }

        /// <summary>
        /// Parent property for this hierarchical ViewModel.
        /// </summary>
        IViewModel Parent { get; set; }

        /// <summary>
        /// Children for this hierarchical ViewModel.
        /// </summary>
        ICollection<IViewModel> Children { get; set; }
        #endregion // Properties

        #region Methods

        IViewModel GetChildWithString(String name);

        #endregion // Methods
    }

    /// <summary>
    /// ViewModel event argument class.
    /// </summary>
    public class ViewModelEventArgs : EventArgs
    {
        #region Properties
        /// <summary>
        /// Associated Model object.
        /// </summary>
        public IViewModel ViewModel
        {
            get;
            protected set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="m"></param>
        public ViewModelEventArgs(IViewModel m)
        {
            this.ViewModel = m;
        }
        #endregion // Constructor(s)
    }

} // RSG.Base.Editor namespace
