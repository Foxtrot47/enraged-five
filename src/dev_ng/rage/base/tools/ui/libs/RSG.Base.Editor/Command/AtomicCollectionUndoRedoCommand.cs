﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Editor.Collections;

namespace RSG.Base.Editor.Command
{
    public class AtomicCollectionUndoRedoCommand : IUndoRedoCommand
    {
        #region Properties

        /// <summary>
        /// Command tag string for UI display.
        /// </summary>
        public String Tag
        {
            get;
            protected set;
        }

        /// <summary>
        /// Original source object that caused the undo command.
        /// </summary>
        public IModel Sender
        {
            get;
            protected set;
        }

        /// <summary>
        /// The modified state the model was in when 
        /// this command was created
        /// </summary>
        public Boolean ModifiedState
        {
            get;
            set;
        }

        /// <summary>
        /// The collection that was changed in the command
        /// </summary>
        public IModelObservableCollection Collection
        {
            get;
            private set;
        }

        /// <summary>
        /// Previous value prior to the changed event.
        /// </summary>
        public IList OldItems
        {
            get;
            protected set;
        }

        /// <summary>
        /// New (current) value after the changed event.
        /// </summary>
        public IList NewItems
        {
            get;
            internal set;
        }

        /// <summary>
        /// Index to start the changes from
        /// </summary>
        public int StartingIndex
        {
            get;
            set;
        }

        #endregion // Properties

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        public AtomicCollectionUndoRedoCommand(IModel sender, IList oldItems, IList newItems, 
            int startingIndex, Boolean modifiedState, IModelObservableCollection collection)
        {
            this.Tag = GenerateTag();
            this.Sender = sender;
            this.OldItems = oldItems;
            this.NewItems = newItems;
            this.ModifiedState = modifiedState;
            this.Collection = collection;
            this.StartingIndex = startingIndex;
        }

        #endregion // Constructor

        #region Protected Methods

        /// <summary>
        /// GenerateTag method
        /// </summary>
        protected virtual String GenerateTag()
        {
            return "Collection changed";
        }

        /// <summary>
        /// Creates the redo representation from the undo command
        /// </summary>
        internal static AtomicCollectionUndoRedoCommand CreateRedoCommand(AtomicCollectionUndoRedoCommand command)
        {
            AtomicCollectionUndoRedoCommand redoCommand = new AtomicCollectionUndoRedoCommand(command.Sender,
                        command.NewItems,
                        command.OldItems,
                        command.StartingIndex,
                        !command.ModifiedState,
                        command.Collection);

            return redoCommand;
        }

        /// <summary>
        /// Creates the redo representation from the undo command
        /// </summary>
        internal static AtomicCollectionUndoRedoCommand CreateUndoCommand(AtomicCollectionUndoRedoCommand command)
        {
            AtomicCollectionUndoRedoCommand undoCommand = new AtomicCollectionUndoRedoCommand(command.Sender,
                        command.NewItems,
                        command.OldItems,
                        command.StartingIndex,
                        !command.ModifiedState,
                        command.Collection);

            return undoCommand;
        }

        #endregion // Protected Methods
    } // AtomicCollectionUndoRedoCommand
} // RSG.Base.Editor.Command
