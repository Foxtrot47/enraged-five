﻿using System;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Base.Editor.Command
{
    /// <summary>
    /// 
    /// </summary>
    public class ModelPropertyChangedEventArgs<T>
    {
        /// <summary>
        /// Default constructor
        /// </summary>
        public ModelPropertyChangedEventArgs(T oldValue, T newValue)
        {
            this.OldValue = oldValue;
            this.NewValue = newValue;
        }

        /// <summary>
        /// Gets the value of the property after the change.
        /// </summary>
        public T NewValue { get; private set; }

        /// <summary>
        /// Gets the value of the property before the change.
        /// </summary>
        public T OldValue { get; private set; }

    } // ModelPropertyChangedEventArgs
} // RSG.Base.Editor.Command
