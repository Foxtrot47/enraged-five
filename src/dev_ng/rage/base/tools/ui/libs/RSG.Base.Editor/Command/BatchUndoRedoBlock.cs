﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Base.Editor.Command
{
    public class BatchUndoRedoBlock : IDisposable
    {
        #region Members

        private IModelUndoRedoProperties m_model;
        private CommandManager m_manager;
        #endregion // Members

        #region Properties

        private IModelUndoRedoProperties Model
        {
            get { return m_model; }
            set { m_model = value; }
        }

        #endregion // Properties

        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public BatchUndoRedoBlock(CommandManager manager)
        {
            if (manager != null)
            {
                manager.StartUndoRedoBlock();
                m_manager = manager;
            }
        }
        
        /// <summary>
        /// Default constructor
        /// </summary>
        public BatchUndoRedoBlock(IModel model)
        {
            if (model is IModelUndoRedoProperties)
            {
                this.Model = model as IModelUndoRedoProperties;
                this.Model.StartUndoRedoBlock(String.Empty);
            }
        }

        /// <summary>
        /// Default constructor
        /// </summary>
        public BatchUndoRedoBlock(IModel model, String tag)
        {
            if (model is IModelUndoRedoProperties)
            {
                this.Model = model as IModelUndoRedoProperties;
                this.Model.StartUndoRedoBlock(tag);
            }
        }

        #endregion // Constructor

        #region IDisposable Implementation

        /// <summary>
        /// Called when the object is getting disposed of.
        /// </summary>
        public void Dispose()
        {
            if (this.Model != null)
                this.Model.FinishUndoRedoBlock();

            if (this.m_manager != null)
                m_manager.FinishUndoRedoBlock();
        }

        #endregion // IDisposable Implementation
    } // IgnoreUndoRedoBlock
} // RSG.Base.Editor.Command
