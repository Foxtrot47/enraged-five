﻿using System;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Windows.Controls;

namespace RSG.Base.Editor.Validation
{

    /// <summary>
    /// Validates a String against a regular expression.
    /// </summary>
    /// References:
    ///   http://www.wpftutorial.net/DataValidation.html
    ///
    public class RegexValidationRule : ValidationRule
    {
        #region Properties and Associated Member Data
        /// <summary>
        /// 
        /// </summary>
        public String Pattern
        {
            get { return m_sPattern; }
            set
            {
                m_sPattern = value;
                m_regEx = new Regex(m_sPattern, RegexOptions.IgnoreCase);
            }
        }
        private String m_sPattern;
        private Regex m_regEx;

        /// <summary>
        /// 
        /// </summary>
        public String ErrorText
        {
            get;
            set;
        }
        #endregion // Properties and Associated Member Data

        #region Controller Methods
        /// <summary>
        /// Validate.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="cultureInfo"></param>
        /// <returns></returns>
        public override ValidationResult Validate(Object value, CultureInfo cultureInfo)
        {
            if (String.IsNullOrEmpty(value as String) || !m_regEx.Match(value.ToString()).Success)
            {
                return new ValidationResult(false, this.ErrorText);
            }
            else
            {
                return new ValidationResult(true, null);
            }
        }
        #endregion // Controller Methods
    }

} // RSG.Base.Editor.Validation namespace
