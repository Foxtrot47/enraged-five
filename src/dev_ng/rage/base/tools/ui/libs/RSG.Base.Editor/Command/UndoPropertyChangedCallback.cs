﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Base.Editor.Command
{
    public delegate void UndoPropertyChangedCallback(IModel model, UndoPropertyChangedEventArgs e);
}
