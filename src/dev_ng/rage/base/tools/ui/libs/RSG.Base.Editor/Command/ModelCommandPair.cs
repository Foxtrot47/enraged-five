﻿using System;
using System.Diagnostics;

namespace RSG.Base.Editor.Command
{

    ///// <summary>
    ///// Helper class to manage a Model and CommandManager pair.
    ///// </summary>
    //public sealed class ModelCommandManagerPair
    //{
    //    #region Properties
    //    /// <summary>
    //    /// 
    //    /// </summary>
    //    public CommandManager CommandManager
    //    {
    //        get;
    //        private set;
    //    }

    //    /// <summary>
    //    /// 
    //    /// </summary>
    //    public IModel Model
    //    {
    //        get;
    //        private set;
    //    }
    //    #endregion // Properties

    //    #region Constructor(s)
    //    /// <summary>
    //    /// Constructor.
    //    /// </summary>
    //    /// <param name="model"></param>
    //    public ModelCommandManagerPair(IModel model)
    //    {
    //        Debug.Assert(model is IModelCommandManager,
    //            "Application Error: model must implement IModelCommandManager.");
    //        if (!(model is IModelCommandManager))
    //            throw new ArgumentException("Model must implement IModelCommandManager.");

    //        this.Model = model;
    //        this.CommandManager = new CommandManager();
    //        this.CommandManager.RegisterModel(this.Model);
    //    }

    //    /// <summary>
    //    /// Constructor.
    //    /// </summary>
    //    /// <param name="model"></param>
    //    /// <param name="cmdMgr"></param>
    //    public ModelCommandManagerPair(IModel model, CommandManager cmdMgr)
    //    {
    //        Debug.Assert(model is IModelCommandManager,
    //            "Application Error: model must implement IModelCommandManager.");
    //        if (!(model is IModelCommandManager))
    //            throw new ArgumentException("Model must implement IModelCommandManager.");
            
    //        this.Model = model;
    //        this.CommandManager = cmdMgr;
    //        this.CommandManager.RegisterModel(this.Model);
    //    }
    //    #endregion // Constructor(s)
    //}

} // RSG.Base.Editor.Command
