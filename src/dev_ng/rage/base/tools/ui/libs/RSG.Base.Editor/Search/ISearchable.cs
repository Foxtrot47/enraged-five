﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Base.Editor.Search
{

    #region Enumerations
    /// <summary>
    /// Comparison options a ISearchable Model/ViewModel supports
    /// </summary>
    public enum ComparisonOptions
    {
        None = 0,
        EqualTo = 1,
        NotEqualTo = 2,
        StartsWith = 3,
        EndsWith = 4,
        Contains = 5,
        LessThan = 6,
        GreaterThen = 7
    } // ComparisonOptions

    /// <summary>
    /// Search types that an ISearchable Model/ViewModel can support
    /// </summary>
    public enum SearchType
    {
        Search_None_Type,
        Search_Boolean_Type,
        Search_String_Type,
        Search_Enum_Type,
        Search_Numeric_Type
    } // SearchType

    /// <summary>
    /// Searching option flags that can be pasted into the searching 
    /// functions of a ISearchable model object to determine the 
    /// searching behaviour.
    /// </summary>
    [Flags()]
    public enum SearchOptions
    {
        None = 0x0,
        IgnoreCase = 0x1,
    } // SearchOptions
    #endregion // Enumerations

    /// <summary>
    /// 
    /// </summary>
    public interface ISearchable
    {
        #region Methods
        /// <summary>
        /// Cycles through the properties in this object and determines if any of them match the given search parameters. This 
        /// function also goes through all the children objects that are of type Isearchable and does the same thing with them.
        /// </summary>
        /// <param name="searchField">The name of the propery (fixed name or display name) that we are looking for.</param>
        /// <param name="searchType">The type of the search, whether it be a numeric search or a string search etc.</param>
        /// <param name="comparisonOptions">What sort of comparison should be done to determine if a property is the correct value</param>
        /// <param name="searchOptions">Any additional options that this search should have</param>
        /// <param name="searchText">The actual string that we are searching for, this could be a string represetation of a numeric value or a boolean value</param>
        /// <returns>A list of models that have properties in them that match the search parameters.</returns>
        List<SearchResult> FindAll(String searchField, SearchType searchType, ComparisonOptions comparisonOptions, SearchOptions searchOptions, String searchText);
        #endregion // Methods
    }

} // RSG.Base.Editor.Search namespace
