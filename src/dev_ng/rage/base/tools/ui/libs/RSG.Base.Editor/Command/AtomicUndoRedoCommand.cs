﻿using System;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace RSG.Base.Editor.Command
{

    /// <summary>
    /// Atomic undo/redo command.
    /// </summary>
    /// An atomic undo/redo command representing a single command that can be
    /// done/undone at once.
    /// 
    /// The command's Tag is automatically generated from the property member
    /// information.
    /// 
    public class AtomicUndoRedoCommand : IAtomicUndoRedoCommand
    {
        #region Properties

        /// <summary>
        /// Command tag string for UI display.
        /// </summary>
        public String Tag
        {
            get;
            protected set;
        }

        /// <summary>
        /// Original source object that caused the undo command.
        /// </summary>
        public IModel Sender
        {
            get;
            protected set;
        }

        /// <summary>
        /// Previous value prior to the changed event.
        /// </summary>
        public Object PreviousValue
        {
            get;
            protected set;
        }

        /// <summary>
        /// New (current) value after the changed event.
        /// </summary>
        public Object NewValue
        {
            get;
            internal set;
        }

        /// <summary>
        /// The expression that represents the method that originally called 
        /// the property change.
        /// </summary>
        public PropertyInfo Property
        {
            get;
            protected set;
        }

        /// <summary>
        /// Delegate used to set the property to its new value.
        /// </summary>
        public PropertySetDelegate Setter
        {
            get;
            protected set;
        }

        /// <summary>
        /// The modified state the model was in when 
        /// this command was created
        /// </summary>
        public Boolean ModifiedState
        {
            get;
            set;
        }

        #endregion // Properties

        #region Constructor(s)

        /// <summary>
        /// 
        /// </summary>
        public AtomicUndoRedoCommand(IModel sender, PropertyInfo property, 
            PropertySetDelegate setter, Object previous, Object current, Boolean modifiedState)
        {
            this.Sender = sender;
            this.Property = property;
            this.PreviousValue = previous;
            this.NewValue = current;
            this.Setter = setter;
            this.ModifiedState = modifiedState;

            this.Tag = GenerateTag();
        }

        #endregion // Constructor(s)

        #region Protected Methods

        /// <summary>
        /// GenerateTag method
        /// </summary>
        protected virtual String GenerateTag()
        {
            return (String.Format("{0} changed", this.Property.Name));        
        }

        /// <summary>
        /// Creates the redo representation from the undo command
        /// </summary>
        internal static AtomicUndoRedoCommand CreateRedoCommand(AtomicUndoRedoCommand command)
        {
            AtomicUndoRedoCommand redoCommand = new AtomicUndoRedoCommand(command.Sender,
                        command.Property,
                        command.Setter,
                        command.NewValue,
                        command.PreviousValue,
                        !command.ModifiedState);

            return redoCommand;
        }

        /// <summary>
        /// Creates the redo representation from the undo command
        /// </summary>
        internal static AtomicUndoRedoCommand CreateUndoCommand(AtomicUndoRedoCommand command)
        {
            AtomicUndoRedoCommand undoCommand = new AtomicUndoRedoCommand(command.Sender,
                        command.Property,
                        command.Setter,
                        command.NewValue,
                        command.PreviousValue,
                        !command.ModifiedState);

            return undoCommand;
        }

        #endregion // Protected Methods
    }

} // RSG.Base.Editor.Command
