using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace ptxShaderVars
{
    public partial class varKeyframe : UserControl
    {
        public varKeyframe()
        {
            InitializeComponent();
        }

		public string m_RuleName;
		public int m_RuleType;
		public uint m_PropertyId;
        private uint m_NetId;

        public uint NetID
        {
            get { return m_NetId; }
            set { m_NetId = value; }
        }
	


        private String m_HeaderText = "";
        public Object m_DataObject = null;
        public String HeaderText
        {
            get { return m_HeaderText; }
            set { m_HeaderText = value; SetTitle(); }
        }
        private void SetTitle()
        {
            m_Label.Text = m_HeaderText;
        }

        private void m_EditButton_Click(object sender, EventArgs e)
        {
            OnShowEditWindow();
        }

        public event EventHandler ShowEditWindow;

        public void OnShowEditWindow()
        {
            if (ShowEditWindow != null)
            {
                ShowEditWindow.Invoke(this, EventArgs.Empty);
            }
        }

    }
}
