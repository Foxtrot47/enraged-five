namespace RageUserControls
{
    partial class ButtonEX
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.m_LabelText = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // m_LabelText
            // 
            this.m_LabelText.BackColor = System.Drawing.SystemColors.Control;
            this.m_LabelText.CausesValidation = false;
            this.m_LabelText.Location = new System.Drawing.Point(2, 2);
            this.m_LabelText.Name = "m_LabelText";
            this.m_LabelText.Size = new System.Drawing.Size(102, 38);
            this.m_LabelText.TabIndex = 0;
            this.m_LabelText.Text = "Button Text";
            this.m_LabelText.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.m_LabelText.Click += new System.EventHandler(this.ButtonEX_Click);
            // 
            // ButtonEX
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.m_LabelText);
            this.Name = "ButtonEX";
            this.Size = new System.Drawing.Size(107, 40);
            this.Click += new System.EventHandler(this.ButtonEX_Click);
            this.Resize += new System.EventHandler(this.ButtonEX_Resize);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.ButtonEX_Paint);
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.Label m_LabelText;
    }
}
