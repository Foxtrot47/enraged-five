namespace ptxShaderVars
{
    partial class varBool
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.m_Check = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // m_Check
            // 
            this.m_Check.AutoSize = true;
            this.m_Check.Location = new System.Drawing.Point(4, 4);
            this.m_Check.Name = "m_Check";
            this.m_Check.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.m_Check.Size = new System.Drawing.Size(80, 17);
            this.m_Check.TabIndex = 1;
            this.m_Check.Text = "checkBox1";
            this.m_Check.UseVisualStyleBackColor = true;
            this.m_Check.CheckedChanged += new System.EventHandler(this.m_Check_CheckedChanged);
            // 
            // varBool
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.m_Check);
            this.Name = "varBool";
            this.Size = new System.Drawing.Size(217, 24);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox m_Check;
    }
}
