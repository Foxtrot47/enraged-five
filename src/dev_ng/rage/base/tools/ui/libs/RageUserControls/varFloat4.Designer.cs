namespace ptxShaderVars
{
    partial class varFloat4
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label3 = new System.Windows.Forms.Label();
            this.m_ptxSlider2 = new ParticleEditor.ptxSlider();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.m_ptxSlider1 = new ParticleEditor.ptxSlider();
            this.m_ptxSlider = new ParticleEditor.ptxSlider();
            this.m_Label = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.m_ptxSlider3 = new ParticleEditor.ptxSlider();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(204, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(15, 13);
            this.label3.TabIndex = 20;
            this.label3.Text = "z:";
            // 
            // m_ptxSlider2
            // 
            this.m_ptxSlider2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.m_ptxSlider2.Decimals = 3;
            this.m_ptxSlider2.Increment = 0.1F;
            this.m_ptxSlider2.Location = new System.Drawing.Point(223, 20);
            this.m_ptxSlider2.MaxValue = 100F;
            this.m_ptxSlider2.MinValue = 0F;
            this.m_ptxSlider2.Multiline = true;
            this.m_ptxSlider2.Name = "m_ptxSlider2";
            this.m_ptxSlider2.SetValue = 0F;
            this.m_ptxSlider2.Size = new System.Drawing.Size(74, 20);
            this.m_ptxSlider2.TabIndex = 19;
            this.m_ptxSlider2.Text = "0.000";
            this.m_ptxSlider2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.m_ptxSlider2.Value = 0F;
            this.m_ptxSlider2.OnValueChanged += new ParticleEditor.ptxSlider.ptxSliderEventDelegate(this.m_ptxSlider_OnValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(103, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(15, 13);
            this.label2.TabIndex = 18;
            this.label2.Text = "y:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(15, 13);
            this.label1.TabIndex = 17;
            this.label1.Text = "x:";
            // 
            // m_ptxSlider1
            // 
            this.m_ptxSlider1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.m_ptxSlider1.Decimals = 3;
            this.m_ptxSlider1.Increment = 0.1F;
            this.m_ptxSlider1.Location = new System.Drawing.Point(122, 20);
            this.m_ptxSlider1.MaxValue = 100F;
            this.m_ptxSlider1.MinValue = 0F;
            this.m_ptxSlider1.Multiline = true;
            this.m_ptxSlider1.Name = "m_ptxSlider1";
            this.m_ptxSlider1.SetValue = 0F;
            this.m_ptxSlider1.Size = new System.Drawing.Size(74, 20);
            this.m_ptxSlider1.TabIndex = 16;
            this.m_ptxSlider1.Text = "0.000";
            this.m_ptxSlider1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.m_ptxSlider1.Value = 0F;
            this.m_ptxSlider1.OnValueChanged += new ParticleEditor.ptxSlider.ptxSliderEventDelegate(this.m_ptxSlider_OnValueChanged);
            // 
            // m_ptxSlider
            // 
            this.m_ptxSlider.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.m_ptxSlider.Decimals = 3;
            this.m_ptxSlider.Increment = 0.1F;
            this.m_ptxSlider.Location = new System.Drawing.Point(23, 20);
            this.m_ptxSlider.MaxValue = 100F;
            this.m_ptxSlider.MinValue = 0F;
            this.m_ptxSlider.Multiline = true;
            this.m_ptxSlider.Name = "m_ptxSlider";
            this.m_ptxSlider.SetValue = 0F;
            this.m_ptxSlider.Size = new System.Drawing.Size(74, 20);
            this.m_ptxSlider.TabIndex = 15;
            this.m_ptxSlider.Text = "0.000";
            this.m_ptxSlider.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.m_ptxSlider.Value = 0F;
            this.m_ptxSlider.OnValueChanged += new ParticleEditor.ptxSlider.ptxSliderEventDelegate(this.m_ptxSlider_OnValueChanged);
            // 
            // m_Label
            // 
            this.m_Label.AutoSize = true;
            this.m_Label.Location = new System.Drawing.Point(4, 4);
            this.m_Label.Name = "m_Label";
            this.m_Label.Size = new System.Drawing.Size(35, 13);
            this.m_Label.TabIndex = 14;
            this.m_Label.Text = "label1";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(304, 22);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(18, 13);
            this.label4.TabIndex = 22;
            this.label4.Text = "w:";
            // 
            // m_ptxSlider3
            // 
            this.m_ptxSlider3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.m_ptxSlider3.Decimals = 3;
            this.m_ptxSlider3.Increment = 0.1F;
            this.m_ptxSlider3.Location = new System.Drawing.Point(323, 20);
            this.m_ptxSlider3.MaxValue = 100F;
            this.m_ptxSlider3.MinValue = 0F;
            this.m_ptxSlider3.Multiline = true;
            this.m_ptxSlider3.Name = "m_ptxSlider3";
            this.m_ptxSlider3.SetValue = 0F;
            this.m_ptxSlider3.Size = new System.Drawing.Size(74, 20);
            this.m_ptxSlider3.TabIndex = 21;
            this.m_ptxSlider3.Text = "0.000";
            this.m_ptxSlider3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.m_ptxSlider3.Value = 0F;
            this.m_ptxSlider3.OnValueChanged += new ParticleEditor.ptxSlider.ptxSliderEventDelegate(this.m_ptxSlider_OnValueChanged);
            // 
            // varFloat4
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.label4);
            this.Controls.Add(this.m_ptxSlider3);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.m_ptxSlider2);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.m_ptxSlider1);
            this.Controls.Add(this.m_ptxSlider);
            this.Controls.Add(this.m_Label);
            this.Name = "varFloat4";
            this.Size = new System.Drawing.Size(410, 45);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label m_Label;
        private System.Windows.Forms.Label label4;
        private ParticleEditor.ptxSlider m_ptxSlider2;
        private ParticleEditor.ptxSlider m_ptxSlider1;
        private ParticleEditor.ptxSlider m_ptxSlider;
        private ParticleEditor.ptxSlider m_ptxSlider3;
    }
}
