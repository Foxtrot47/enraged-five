using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace RageUserControls
{
    public partial class ButtonEX : UserControl
    {
        Bitmap	m_OffScreenStateUpBmp;
        Bitmap  m_OffScreenStateDownBmp;
        Color   m_ColorDown = Color.FromKnownColor(KnownColor.LightBlue);
        Color m_ColorUp = Color.FromKnownColor(KnownColor.Control);
        bool m_State = true;

        #region Properties
        
        [Description("Initial state of the control(up or down)."), Category("Appearance")]
        public bool ButtonState
        {
            get { return m_State;}
            set { m_State = value; Invalidate(); }
        }

        [Description("The text associated with the control."), Category("Appearance")]
        public String ButtonText
        {
            get { return m_LabelText.Text; }
            set { m_LabelText.Text = value; Invalidate(); }
        }
        [Description("Determines the position of the text within the control."), Category("Appearance")]
        public ContentAlignment TextAlign
        {
            get { return m_LabelText.TextAlign; }
            set { m_LabelText.TextAlign = value; }
        }
        [Description("Color of depressed state."), Category("Appearance")]
        public Color ColorDown
        {
            get { return m_ColorDown; }
            set {m_ColorDown=value;DrawControl();Invalidate();}
        }
        [Description("Color of raised state."), Category("Appearance")]
        public Color ColorUp
        {
            get { return m_ColorUp; }
            set { m_ColorUp = value; DrawControl();Invalidate(); }
        }

        #endregion


        #region EventTypes
        public class ButtonEXEventArgs
        {
            public bool ButtonState;
            internal ButtonEXEventArgs(bool state)
            {
                ButtonState = state;
            }
        }
        #endregion
        #region Events
        public delegate void ButtonEXEventHandler(object sender, ButtonEXEventArgs e);
        public event ButtonEXEventHandler OnButtonStateChangedByUser;
        #endregion

        public ButtonEX()
        {
            InitializeComponent();
            this.BorderStyle = BorderStyle.FixedSingle;
            m_OffScreenStateUpBmp = new Bitmap(Width, Height);
            m_OffScreenStateDownBmp = new Bitmap(Width, Height);
            CreateBitmaps();
            DrawControl();
        }
        
        public void SetColor(Color color)
        {
            m_ColorDown=color;
            m_ColorUp=color;
            DrawControl();
            Invalidate();
        }

        private void CreateBitmaps()
        {

            SolidBrush brdarkdark = new SolidBrush(Color.FromKnownColor(KnownColor.ControlDarkDark));
            SolidBrush brdark = new SolidBrush(Color.FromKnownColor(KnownColor.ControlDark));
            SolidBrush brcontrol = new SolidBrush(Color.FromKnownColor(KnownColor.Control));
            SolidBrush brlight = new SolidBrush(Color.FromKnownColor(KnownColor.ControlLightLight));
            
            //UpState
            Graphics g = Graphics.FromImage(m_OffScreenStateUpBmp);
            g.FillRectangle(brdarkdark,0, 0, m_OffScreenStateUpBmp.Width, m_OffScreenStateUpBmp.Height);
            g.FillRectangle(brlight, 0, 0, (m_OffScreenStateUpBmp.Width - 3), (m_OffScreenStateUpBmp.Height - 3));
            g.FillRectangle(brdark, 1, 1, (m_OffScreenStateUpBmp.Width-4), (m_OffScreenStateUpBmp.Height-4));
            g.FillRectangle(brcontrol, 1, 1, (m_OffScreenStateUpBmp.Width - 5), (m_OffScreenStateUpBmp.Height - 5));
            
            //DownState
            g = Graphics.FromImage(m_OffScreenStateDownBmp);
            g.FillRectangle(brdarkdark, 0, 0, m_OffScreenStateDownBmp.Width, m_OffScreenStateDownBmp.Height);
            g.FillRectangle(brdark, 1, 1, (m_OffScreenStateDownBmp.Width - 4), (m_OffScreenStateDownBmp.Height - 4));
        }
        private void DrawControl()
        {
            m_LabelText.Size = new Size(Width-4, Height-4);

            if (m_State)
            {
                m_LabelText.Location = new Point(1, 1);
                Graphics.FromHwnd(Handle).DrawImage(m_OffScreenStateUpBmp, 0, 0);
                m_LabelText.BackColor = m_ColorUp;
            }
            else
            {
                m_LabelText.Location = new Point(2, 2);
                Graphics.FromHwnd(Handle).DrawImage(m_OffScreenStateDownBmp, 0, 0);
                m_LabelText.BackColor = m_ColorDown;
            }
            
            
        }
        
        private void UserToggle()
        {
            m_State = !m_State;
            DrawControl();
            if (OnButtonStateChangedByUser != null)
                OnButtonStateChangedByUser(this, new ButtonEXEventArgs(m_State));
        }
        
        private void ButtonEX_Paint(object sender, PaintEventArgs e)
        {
            DrawControl();
        }

        private void ButtonEX_Click(object sender, EventArgs e)
        {
            UserToggle();
        }

        private void ButtonEX_Resize(object sender, EventArgs e)
        {
            m_OffScreenStateUpBmp = new Bitmap(Width, Height);
            m_OffScreenStateDownBmp = new Bitmap(Width, Height);
            CreateBitmaps();
            DrawControl();
        }
    }
}
