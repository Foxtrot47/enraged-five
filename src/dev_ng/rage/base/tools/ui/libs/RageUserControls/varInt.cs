using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace ptxShaderVars
{
    public partial class varInt : UserControl
    {
        private string m_HeaderText = "";
        private int m_MinValue = 0;
        private int m_MaxValue = 100;
        private int m_Increment = 0;
        private int m_Value = 0;

        public Object m_DataObject = null;

        #region Properties

        public String HeaderText
        {
            get { return m_HeaderText; }
            set { m_HeaderText = value; SetTitle(); }
        }
        public int MinValue
        {
            get { return m_MinValue; }
            set { m_MinValue = value; SetData(); }
        }
        public int MaxValue
        {
            get { return m_MaxValue; }
            set { m_MaxValue = value; SetData(); }
        }
        public int Increment
        {
            get { return m_Increment; }
            set { m_Increment = value; SetData(); }
        }
        public int Value
        {
            get { return m_Value; }
            set { m_Value = value; SetData(); }
        }
        public delegate void varIntEventDelegate(varInt var);

        [Description("Occurs when user changes the value"), Category("Action")]
        public event varIntEventDelegate OnValueChanged = null;

        #endregion

        public varInt()
        {
            InitializeComponent();
        }

        private void SetTitle()
        {
            m_Label.Text = m_HeaderText;
        }
        private void SetData()
        {
            m_ptxSlider.MinValue = (float)m_MinValue;
            m_ptxSlider.MaxValue = (float)m_MaxValue;
            m_ptxSlider.Increment = (float)m_Increment;
            m_ptxSlider.SetValue = (float)m_Value;
        }
        private void m_ptxSlider_OnValueChanged(ParticleEditor.ptxSlider slider)
        {
            m_Value = (int)m_ptxSlider.Value;
            if (OnValueChanged != null)
                OnValueChanged(this);
        }

    }
}
