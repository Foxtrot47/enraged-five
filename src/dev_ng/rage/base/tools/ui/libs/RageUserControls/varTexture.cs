using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace ptxShaderVars
{
    public partial class varTexture : UserControl
    {
        private String m_BrowseDirectory = "";
        private String m_HeaderText = "";
        private String m_FilePath = "";
        private String m_FileName = "";
        private String m_Extension = "";
        public Object m_DataObject = null;

        #region Properties
        public String HeaderText
        {
            get { return m_HeaderText; }
            set { m_HeaderText = value; SetTitle(); }
        }
        public String FileName
        {
            get { return m_FileName; }
            set { m_FileName = value; SetFileName();}
        }
        public String FilePath
        {
            get { return m_FilePath; }
            set { m_FilePath = value; SetFileName(); }
        }
        public String FileExtension
        {
            get { return m_Extension; }
            set { m_Extension = value; SetFileName(); }
        }
        public String BrowseDirectory
        {
            get { return m_BrowseDirectory; }
            set { m_BrowseDirectory = value; }
        }

        public delegate void varTextureEventDelegate(varTexture var);
        
        [Description("Occurs when user selects a new valid file"), Category("Action")]
        public event varTextureEventDelegate OnSelectFile = null;

        [Description("Occurs when user hits the reset button"), Category("Action")]
        public event varTextureEventDelegate OnResetFile = null;

        #endregion

        public varTexture()
        {
            InitializeComponent();
        }

        public void DisplayTexture(string filename)
        {
        }
        
        private void SetTitle()
        {
            m_Label.Text = m_HeaderText;
        }
        private void SetFileName()
        {
            //Do not show full path
            //if (m_FilePath.Length > 0)
            //    m_TextBox.Text = m_FilePath + System.IO.Path.DirectorySeparatorChar + m_FileName;
            //else
                m_TextBox.Text = m_FileName;
        }

        private void m_ButtonBrowse_Click(object sender, EventArgs e)
        {
            OpenFileDialog diag = new OpenFileDialog();
            diag.InitialDirectory = m_BrowseDirectory;
            diag.Filter = "Textures (*.dds)|*.dds";
            diag.Multiselect = false;
            diag.CheckFileExists = true;
            if (diag.ShowDialog() == DialogResult.OK)
            {
                String filename = diag.FileName;
                String path = System.IO.Path.GetDirectoryName(filename);
                String name = System.IO.Path.GetFileNameWithoutExtension(filename);
                String extension = System.IO.Path.GetExtension(filename);
                BrowseDirectory = path;
                FilePath = path;
                FileName = name;
                FileExtension = extension;
                if (OnSelectFile != null)
                    OnSelectFile(this);
            }
        }

        private void m_ButtonReset_Click(object sender, EventArgs e)
        {
            FileName = "";
            FilePath = "";
            FileExtension = "";
            if (OnResetFile != null)
            {
                OnResetFile(this);
            }
        }
    }
}