namespace ptxShaderVars
{
    partial class varKeyframe
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.m_EditButton = new System.Windows.Forms.Button();
            this.m_Label = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // m_EditButton
            // 
            this.m_EditButton.Location = new System.Drawing.Point(7, 19);
            this.m_EditButton.Name = "m_EditButton";
            this.m_EditButton.Size = new System.Drawing.Size(75, 23);
            this.m_EditButton.TabIndex = 0;
            this.m_EditButton.Text = "Edit";
            this.m_EditButton.UseVisualStyleBackColor = true;
            this.m_EditButton.Click += new System.EventHandler(this.m_EditButton_Click);
            // 
            // m_Label
            // 
            this.m_Label.AutoSize = true;
            this.m_Label.Location = new System.Drawing.Point(4, 4);
            this.m_Label.Name = "m_Label";
            this.m_Label.Size = new System.Drawing.Size(35, 13);
            this.m_Label.TabIndex = 1;
            this.m_Label.Text = "label1";
            // 
            // varKeyframe
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.m_Label);
            this.Controls.Add(this.m_EditButton);
            this.Name = "varKeyframe";
            this.Size = new System.Drawing.Size(435, 45);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button m_EditButton;
        private System.Windows.Forms.Label m_Label;
    }
}
