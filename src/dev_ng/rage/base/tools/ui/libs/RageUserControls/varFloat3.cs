using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace ptxShaderVars
{
    public partial class varFloat3 : UserControl
    {
        public class float3
        {
            public float3() { x = y = z = 0.0f; }
            public float3(float xx, float yy, float zz) { x = xx; y = yy; z = zz; }
            public float x, y, z;
        };

        private string m_HeaderText = "";
        private float m_MinValue = 0.0f;
        private float m_MaxValue = 100.0f;
        private float m_Increment = 0.01f;
        private float3 m_Value = new float3();

        public Object m_DataObject = null;

        #region Properties

        public String HeaderText
        {
            get { return m_HeaderText; }
            set { m_HeaderText = value; SetTitle(); }
        }
        public float MinValue
        {
            get { return m_MinValue; }
            set { m_MinValue = value; SetData(); }
        }
        public float MaxValue
        {
            get { return m_MaxValue; }
            set { m_MaxValue = value; SetData(); }
        }
        public float Increment
        {
            get { return m_Increment; }
            set { m_Increment = value; SetData(); }
        }
        public float3 Value
        {
            get { return m_Value; }
            set { m_Value = value; SetData(); }
        }
        public delegate void varFloat3EventDelegate(varFloat3 var);

        [Description("Occurs when user changes the value"), Category("Action")]
        public event varFloat3EventDelegate OnValueChanged = null;

        #endregion

        public varFloat3()
        {
            InitializeComponent();
        }
        
        private void SetTitle()
        {
            m_Label.Text = m_HeaderText;
        }
        private void SetData()
        {
            m_ptxSlider.MinValue = m_ptxSlider1.MinValue = m_ptxSlider2.MinValue = m_MinValue;
            m_ptxSlider.MaxValue = m_ptxSlider1.MaxValue = m_ptxSlider2.MaxValue = m_MaxValue;
            m_ptxSlider.Increment = m_ptxSlider1.Increment = m_ptxSlider2.Increment = m_Increment;
            m_ptxSlider.SetValue = m_Value.x;
            m_ptxSlider1.SetValue = m_Value.y;
            m_ptxSlider2.SetValue = m_Value.z;
        }

        private void m_ptxSlider_OnValueChanged(ParticleEditor.ptxSlider slider)
        {
            m_Value.x = m_ptxSlider.Value;
            m_Value.y = m_ptxSlider1.Value;
            m_Value.z = m_ptxSlider2.Value;
            if (OnValueChanged != null)
                OnValueChanged(this);
        }


    }
}
