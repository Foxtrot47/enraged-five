using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace ptxShaderVars
{
    public partial class varFloat : UserControl
    {
        private string m_HeaderText = "";
        private float m_MinValue = 0.0f;
        private float m_MaxValue = 100.0f;
        private float m_Increment = 0.01f;
        private float m_Value = 0.0f;
        public Object m_DataObject = null;

        #region Properties
        public String HeaderText
        {
            get { return m_HeaderText; }
            set { m_HeaderText = value; SetTitle(); }
        }
        public float MinValue
        {
            get { return m_MinValue;}
            set { m_MinValue = value; SetData(); }
        }
        public float MaxValue
        {
            get { return m_MaxValue; }
            set { m_MaxValue = value; SetData(); }
        }
        public float Increment
        {
            get { return m_Increment; }
            set { m_Increment = value; SetData(); }
        }
        public float Value 
        {
            get { return m_Value; }
            set { m_Value = value; SetData(); }
        }

        public delegate void varFloatEventDelegate(varFloat var);

        [Description("Occurs when user changes the value"), Category("Action")]
        public event varFloatEventDelegate OnValueChanged = null;

        #endregion

        public varFloat()
        {
            InitializeComponent();
        }

        private void SetTitle()
        {
            m_Label.Text = m_HeaderText;
        }
        
        private void SetData()
        {
            m_ptxSlider.MinValue = m_MinValue;
            m_ptxSlider.MaxValue = m_MaxValue;
            m_ptxSlider.Increment = m_Increment;
            m_ptxSlider.SetValue = m_Value;
        }

        private void m_ptxSlider_OnValueChanged(ParticleEditor.ptxSlider slider)
        {
            m_Value = m_ptxSlider.Value;
            if (OnValueChanged != null)
                OnValueChanged(this);
        }

    }
}
