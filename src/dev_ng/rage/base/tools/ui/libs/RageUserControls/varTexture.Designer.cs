namespace ptxShaderVars
{
    partial class varTexture
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.m_Label = new System.Windows.Forms.Label();
            this.m_TextBox = new System.Windows.Forms.TextBox();
            this.m_ButtonBrowse = new System.Windows.Forms.Button();
            this.m_ButtonReset = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // m_Label
            // 
            this.m_Label.AutoSize = true;
            this.m_Label.Location = new System.Drawing.Point(4, 4);
            this.m_Label.Name = "m_Label";
            this.m_Label.Size = new System.Drawing.Size(35, 13);
            this.m_Label.TabIndex = 1;
            this.m_Label.Text = "label1";
            // 
            // m_TextBox
            // 
            this.m_TextBox.Location = new System.Drawing.Point(7, 20);
            this.m_TextBox.Name = "m_TextBox";
            this.m_TextBox.ReadOnly = true;
            this.m_TextBox.Size = new System.Drawing.Size(280, 20);
            this.m_TextBox.TabIndex = 2;
            // 
            // m_ButtonBrowse
            // 
            this.m_ButtonBrowse.Location = new System.Drawing.Point(293, 17);
            this.m_ButtonBrowse.Name = "m_ButtonBrowse";
            this.m_ButtonBrowse.Size = new System.Drawing.Size(61, 23);
            this.m_ButtonBrowse.TabIndex = 3;
            this.m_ButtonBrowse.Text = "Browse";
            this.m_ButtonBrowse.UseVisualStyleBackColor = true;
            this.m_ButtonBrowse.Click += new System.EventHandler(this.m_ButtonBrowse_Click);
            // 
            // m_ButtonReset
            // 
            this.m_ButtonReset.Location = new System.Drawing.Point(360, 17);
            this.m_ButtonReset.Name = "m_ButtonReset";
            this.m_ButtonReset.Size = new System.Drawing.Size(64, 23);
            this.m_ButtonReset.TabIndex = 4;
            this.m_ButtonReset.Text = "Reset";
            this.m_ButtonReset.UseVisualStyleBackColor = true;
            this.m_ButtonReset.Click += new System.EventHandler(this.m_ButtonReset_Click);
            // 
            // varTexture
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.m_ButtonReset);
            this.Controls.Add(this.m_ButtonBrowse);
            this.Controls.Add(this.m_TextBox);
            this.Controls.Add(this.m_Label);
            this.Name = "varTexture";
            this.Size = new System.Drawing.Size(435, 45);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label m_Label;
        private System.Windows.Forms.TextBox m_TextBox;
        private System.Windows.Forms.Button m_ButtonBrowse;
        private System.Windows.Forms.Button m_ButtonReset;
    }
}
