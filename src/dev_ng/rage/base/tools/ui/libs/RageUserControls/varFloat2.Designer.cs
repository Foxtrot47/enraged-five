namespace ptxShaderVars
{
    partial class varFloat2
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.m_ptxSlider = new ParticleEditor.ptxSlider();
            this.m_Label = new System.Windows.Forms.Label();
            this.m_ptxSlider1 = new ParticleEditor.ptxSlider();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // m_ptxSlider
            // 
            this.m_ptxSlider.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.m_ptxSlider.Decimals = 3;
            this.m_ptxSlider.Increment = 0.1F;
            this.m_ptxSlider.Location = new System.Drawing.Point(23, 20);
            this.m_ptxSlider.MaxValue = 100F;
            this.m_ptxSlider.MinValue = 0F;
            this.m_ptxSlider.Multiline = true;
            this.m_ptxSlider.Name = "m_ptxSlider";
            this.m_ptxSlider.SetValue = 0F;
            this.m_ptxSlider.Size = new System.Drawing.Size(74, 20);
            this.m_ptxSlider.TabIndex = 3;
            this.m_ptxSlider.Text = "0.000";
            this.m_ptxSlider.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.m_ptxSlider.Value = 0F;
            this.m_ptxSlider.OnValueChanged += new ParticleEditor.ptxSlider.ptxSliderEventDelegate(this.m_ptxSlider_OnValueChanged);
            // 
            // m_Label
            // 
            this.m_Label.AutoSize = true;
            this.m_Label.Location = new System.Drawing.Point(4, 4);
            this.m_Label.Name = "m_Label";
            this.m_Label.Size = new System.Drawing.Size(35, 13);
            this.m_Label.TabIndex = 2;
            this.m_Label.Text = "label1";
            // 
            // m_ptxSlider1
            // 
            this.m_ptxSlider1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.m_ptxSlider1.Decimals = 3;
            this.m_ptxSlider1.Increment = 0.1F;
            this.m_ptxSlider1.Location = new System.Drawing.Point(122, 20);
            this.m_ptxSlider1.MaxValue = 100F;
            this.m_ptxSlider1.MinValue = 0F;
            this.m_ptxSlider1.Multiline = true;
            this.m_ptxSlider1.Name = "m_ptxSlider1";
            this.m_ptxSlider1.SetValue = 0F;
            this.m_ptxSlider1.Size = new System.Drawing.Size(74, 20);
            this.m_ptxSlider1.TabIndex = 4;
            this.m_ptxSlider1.Text = "0.000";
            this.m_ptxSlider1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.m_ptxSlider1.Value = 0F;
            this.m_ptxSlider1.OnValueChanged += new ParticleEditor.ptxSlider.ptxSliderEventDelegate(this.m_ptxSlider_OnValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(15, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "x:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(103, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(15, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "y:";
            // 
            // varFloat2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.m_ptxSlider1);
            this.Controls.Add(this.m_ptxSlider);
            this.Controls.Add(this.m_Label);
            this.Name = "varFloat2";
            this.Size = new System.Drawing.Size(205, 45);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label m_Label;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private ParticleEditor.ptxSlider m_ptxSlider;
        private ParticleEditor.ptxSlider m_ptxSlider1;
    }
}
