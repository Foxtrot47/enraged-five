using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace ParticleEditor
{
	public partial class ptxCombo : ComboBox
	{
		#region Properties

		public delegate void ptxComboEventDelegate(ptxCombo combo);
		[Description("Occurs when user changes the value"), Category("Action")]
		public event ptxComboEventDelegate OnSelectedIndexChanged_Custom = null;

		#endregion

		public ptxCombo()
		{
			InitializeComponent();
		}

		private void InitializeComponent()
		{
			this.SuspendLayout();
			// 
			// ptxCombo
			// 
			this.SelectedIndexChanged += new System.EventHandler(this.ptxCombo_SelectedIndexChanged);
			this.ResumeLayout(false);

		}

		private void ptxCombo_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (OnSelectedIndexChanged_Custom != null)
			{
				OnSelectedIndexChanged_Custom(this);
			}
		}
	}
}
