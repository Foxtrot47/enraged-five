namespace ptxShaderVars
{
    partial class varFloat
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.m_Label = new System.Windows.Forms.Label();
            this.m_ptxSlider = new ParticleEditor.ptxSlider();
            this.SuspendLayout();
            // 
            // m_Label
            // 
            this.m_Label.AutoSize = true;
            this.m_Label.Location = new System.Drawing.Point(4, 4);
            this.m_Label.Name = "m_Label";
            this.m_Label.Size = new System.Drawing.Size(35, 13);
            this.m_Label.TabIndex = 0;
            this.m_Label.Text = "label1";
            // 
            // m_ptxSlider
            // 
            this.m_ptxSlider.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.m_ptxSlider.Decimals = 3;
            this.m_ptxSlider.Increment = 0.1F;
            this.m_ptxSlider.Location = new System.Drawing.Point(7, 20);
            this.m_ptxSlider.MaxValue = 100F;
            this.m_ptxSlider.MinValue = 0F;
            this.m_ptxSlider.Multiline = true;
            this.m_ptxSlider.Name = "m_ptxSlider";
            this.m_ptxSlider.SetValue = 0F;
            this.m_ptxSlider.Size = new System.Drawing.Size(74, 20);
            this.m_ptxSlider.TabIndex = 1;
            this.m_ptxSlider.Text = "0.000";
            this.m_ptxSlider.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.m_ptxSlider.Value = 0F;
            this.m_ptxSlider.OnValueChanged += new ParticleEditor.ptxSlider.ptxSliderEventDelegate(this.m_ptxSlider_OnValueChanged);
            // 
            // varFloat
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.m_ptxSlider);
            this.Controls.Add(this.m_Label);
            this.Name = "varFloat";
            this.Size = new System.Drawing.Size(214, 45);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label m_Label;
        private ParticleEditor.ptxSlider m_ptxSlider;
    }
}
