using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace ptxShaderVars
{
    public partial class varBool : UserControl
    {
        private string m_HeaderText = "";
        private bool m_Value;
        public Object m_DataObject = null;

        #region Properties

        public String HeaderText
        {
            get { return m_HeaderText; }
            set { m_HeaderText = value; SetTitle(); }
        }
        public bool Value
        {
            get { return m_Value; }
            set { m_Value = value; SetData(); }
        }
        public delegate void varBoolEventDelegate(varBool var);

        [Description("Occurs when user changes the value"), Category("Action")]
        public event varBoolEventDelegate OnValueChanged = null;

        #endregion

        public varBool()
        {
            InitializeComponent();
        }

        private void SetTitle()
        {
            m_Check.Text = m_HeaderText;
        }
        private void SetData()
        {
            m_Check.Checked = m_Value;
        }

        private void m_Check_CheckedChanged(object sender, EventArgs e)
        {
            m_Value = m_Check.Checked;
            if (OnValueChanged != null)
                OnValueChanged(this);
        }
    }
}
