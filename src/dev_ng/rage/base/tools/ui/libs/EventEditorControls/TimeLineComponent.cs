using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Windows.Forms;

namespace EventEditorControls
{
	public partial class TimeLineComponent : Component
	{
		public TimeLineComponent()
		{
			InitializeComponent();
		}

		public TimeLineComponent(IContainer container)
		{
			container.Add(this);

			InitializeComponent();
		}

		#region Constants
		private const float c_minLabelSpacing = 40.0f;
		private const float c_minTickSpacing = 6.0f;
		#endregion

		#region Variables
		private Control m_header = null;

		private float m_phaseMin = 0.0f;
		private float m_phaseViewMin = 0.0f;
		private float m_phaseViewMax = 1.0f;
		private float m_phaseMax = 1.0f;

		private float m_displayMin = 0.0f;
		private float m_displayMax = 100.0f;

		private float m_labelInterval = 100.0f;
		private float m_tickInterval = 100.0f;

		private static float[] sm_labelIntervals = new float[]
        {
            .001f,
            .005f,
            .01f,
            .05f,
            .1f,
            .5f,
            1,
            2,
            5,
            10,
            20,
            50,
            100,
            200,
            500,
            1000,
            2000,
            5000,
            10000,
          };

		private static float[] sm_tickIntervals = new float[]
        {
            0.001f,
            0.01f,
            0.1f,
            1,
            10,
            100,
            1000,
            10000,
        };
		#endregion

		#region Properties
		private float HeaderWidth
		{
			get
			{
				return (m_header == null) ? 1.0f : (float)m_header.Width;
			}
		}

		public Control Header
		{
			set
			{
				if (m_header != value)
				{
					// is there an old header?
					if (m_header != null)
					{
						m_header.SizeChanged -= new EventHandler(Header_SizeChanged);
					}

					m_header = value;

					if (m_header != null)
					{
						m_header.SizeChanged += new EventHandler(Header_SizeChanged);
					}

					UpdateIntervals();
				}
			}
		}

		/// <summary>
		/// The minimum phase value for the entire timeline
		/// </summary>
		public float PhaseMin
		{
			get
			{
				return m_phaseMin;
			}
		}

		/// <summary>
		/// The maximum phase value for the entire timeline
		/// </summary>
		public float PhaseMax
		{
			get
			{
				return m_phaseMax;
			}
		}

		/// <summary>
		/// The pixel for the minimum phase
		/// </summary>
		public float PixelMin
		{
			get
			{
				return PhaseToPixel(m_phaseMin);
			}
		}

		/// <summary>
		/// The pixel for the maximum phase
		/// </summary>
		public float PixelMax
		{
			get
			{
				return PhaseToPixel(m_phaseMax);
			}
		}

		/// <summary>
		/// The minimum displayed value (frames, seconds, whatever) for the entire timeline
		/// </summary>
		public float DisplayMin
		{
			get
			{
				return m_displayMin;
			}
			set
			{
				if (m_displayMin != value)
				{
					m_displayMin = value;

					UpdateIntervals();
				}
			}
		}

		/// <summary>
		/// The maximum displayed value (frames, seconds, whatever) for the entire timeline
		/// </summary>
		public float DisplayMax
		{
			get
			{
				return m_displayMax;
			}
			set
			{
				if (m_displayMax != value)
				{
					m_displayMax = value;

					UpdateIntervals();
				}
			}
		}

		/// <summary>
		/// The minimum (leftmost) phase value of the time range shown
		/// </summary>
		public float PhaseViewMin
		{
			get
			{
				return m_phaseViewMin;
			}
			set
			{
				if (m_phaseViewMin != value)
				{
					if (value >= m_phaseViewMax)
					{
						throw new ArgumentOutOfRangeException("value", "value must be < PhaseViewMax");
					}

					if (value < m_phaseMin)
					{
						throw new ArgumentOutOfRangeException("value", "value must be >= PhaseMin");
					}

					m_phaseViewMin = value;

					UpdateIntervals();
				}
			}
		}

		/// <summary>
		/// The maximum (rightmost) phase value of the time range shown
		/// </summary>
		public float PhaseViewMax
		{
			get
			{
				return m_phaseViewMax;
			}
			set
			{
				if (m_phaseViewMax != value)
				{
					if (value <= m_phaseViewMin)
					{
						throw new ArgumentOutOfRangeException("value", "value must be > PhaseViewMin");
					}

					if (value > m_phaseMax)
					{
						throw new ArgumentOutOfRangeException("value", "value must be <= PhaseMax");
					}

					m_phaseViewMax = value;

					UpdateIntervals();
				}
			}
		}

		/// <summary>
		/// The range of the phase that is shown
		/// </summary>
		public float PhaseViewRange
		{
			get
			{
				return this.PhaseViewMax - this.PhaseViewMin;
			}
		}

		/// <summary>
		/// The minimum (leftmost) displayed value (frames, seconds, whatever) 
		/// </summary>
		public float DisplayViewMin
		{
			get
			{
				return PhaseToDisplay(m_phaseViewMin);
			}
			set
			{
				if (value >= this.DisplayViewMax)
				{
					throw new ArgumentOutOfRangeException("value", "value must be < DisplayViewMax");
				}

				if (value < this.DisplayMin)
				{
					throw new ArgumentOutOfRangeException("value", "value must be >= DisplayMin");
				}

				this.PhaseViewMin = DisplayToPhase(value);
			}
		}

		/// <summary>
		/// The maximum (rightmost) displayed value (frames, seconds, whatever) 
		/// </summary>
		public float DisplayViewMax
		{
			get
			{
				return PhaseToDisplay(m_phaseViewMax);
			}
			set
			{
				if (value <= this.DisplayViewMin)
				{
					throw new ArgumentOutOfRangeException("value", "value must be > DisplayViewMin");
				}

				if (value > this.DisplayMax)
				{
					throw new ArgumentOutOfRangeException("value", "value must be <= DisplayMax");
				}

				this.PhaseViewMax = DisplayToPhase(value);
			}
		}

		/// <summary>
		/// The range of the values that are shown
		/// </summary>
		public float DisplayViewRange
		{
			get
			{
				return this.DisplayViewMax - this.DisplayViewMin;
			}
		}

		/// <summary>
		/// The interval at which value labels should be displayed
		/// </summary>
		public float LabelInterval
		{
			get
			{
				return m_labelInterval;
			}
		}

		/// <summary>
		/// The interval at which value ticks should be displayed
		/// </summary>
		public float TickInterval
		{
			get
			{
				return m_tickInterval;
			}
		}
		#endregion

		#region Event Handlers
		private void Header_SizeChanged(object sender, EventArgs e)
		{
			UpdateIntervals();
		}
		#endregion

		#region Public Static Functions
		public static float Lerp(float t, float min, float max)
		{
			return t * (max - min) + min;
		}

		public static float Range(float t, float min, float max)
		{
			return (t - min) / (max - min);
		}

		public static float Remap(float t, float inMin, float inMax, float outMin, float outMax)
		{
			return Lerp(Range(t, inMin, inMax), outMin, outMax);
		}
		#endregion

		#region Public Functions
		/// <summary>
		/// Set the minimum and maximum phase for the entire timeline
		/// </summary>
		/// <param name="min"></param>
		/// <param name="max"></param>
		public void SetPhase(float min, float max)
		{
			if (min >= max)
			{
				throw new ArgumentOutOfRangeException("min must be < max");
			}

			m_phaseMin = min;
			m_phaseMax = max;

			UpdateIntervals();
		}

		/// <summary>
		/// Set the minimum and maximum values for the entire timeline
		/// </summary>
		/// <param name="min"></param>
		/// <param name="max"></param>
		public void SetDisplay(float min, float max)
		{
			if (min >= max)
			{
				throw new ArgumentOutOfRangeException("min must be < max");
			}

			m_displayMin = min;
			m_displayMax = max;

			UpdateIntervals();
		}

		/// <summary>
		/// Set the minimum and maximum phase values that are displayed
		/// </summary>
		/// <param name="min"></param>
		/// <param name="max"></param>
		public void SetPhaseView(float min, float max)
		{
			if (min < m_phaseMin)
			{
				throw new ArgumentOutOfRangeException("min", "min must be < PhaseMin");
			}

			if (max > m_phaseMax)
			{
				throw new ArgumentOutOfRangeException("max", "max must be > PhaseMax");
			}

			if (min >= max)
			{
				throw new ArgumentOutOfRangeException("min must be < max");
			}

			m_phaseViewMin = min;
			m_phaseViewMax = max;

			UpdateIntervals();
		}

		/// <summary>
		/// Sets the minimum and maximum displayed values
		/// </summary>
		/// <param name="min"></param>
		/// <param name="max"></param>
		public void SetDisplayView(float min, float max)
		{
			if (min < m_displayMin)
			{
				throw new ArgumentOutOfRangeException("min", "min must be < DisplayMin");
			}

			if (max > m_displayMax)
			{
				throw new ArgumentOutOfRangeException("max", "max must be > DisplayMax");
			}

			if (min >= max)
			{
				throw new ArgumentOutOfRangeException("min must be < max");
			}

			m_phaseViewMin = DisplayToPhase(min);
			m_phaseViewMax = DisplayToPhase(max);

			UpdateIntervals();
		}

		/// <summary>
		/// Convert from phase value to display value
		/// </summary>
		/// <param name="t"></param>
		/// <returns></returns>
		public float PhaseToDisplay(float t)
		{
			return Remap(t, this.PhaseMin, this.PhaseMax, this.DisplayMin, this.DisplayMax);
		}

		/// <summary>
		/// Convert from display value to phase value
		/// </summary>
		/// <param name="t"></param>
		/// <returns></returns>
		public float DisplayToPhase(float t)
		{
			return Remap(t, this.DisplayMin, this.DisplayMax, this.PhaseMin, this.PhaseMax);
		}

		/// <summary>
		/// Convert from phase value to pixel
		/// </summary>
		/// <param name="t"></param>
		/// <returns></returns>
		public float PhaseToPixel(float t)
		{
			return Remap(t, this.PhaseViewMin, this.PhaseViewMax, 0, this.HeaderWidth);
		}

		/// <summary>
		/// Convert from pixel to phase value
		/// </summary>
		/// <param name="t"></param>
		/// <returns></returns>
		public float PixelToPhase(float t)
		{
			return Remap(t, 0, this.HeaderWidth, this.PhaseViewMin, this.PhaseViewMax);
		}

		/// <summary>
		/// Convert from display value to pixel
		/// </summary>
		/// <param name="t"></param>
		/// <returns></returns>
		public float DisplayToPixel(float t)
		{
			return Remap(t, this.DisplayViewMin, this.DisplayViewMax, 0, this.HeaderWidth);
		}

		/// <summary>
		/// Convert from pixel to display value
		/// </summary>
		/// <param name="t"></param>
		/// <returns></returns>
		public float PixelToDisplay(float t)
		{
			return Remap(t, 0, this.HeaderWidth, this.DisplayViewMin, this.DisplayViewMax);
		}

		/// <summary>
		/// Computes the start value based on the given interval and the DisplayViewMin
		/// </summary>
		/// <param name="interval"></param>
		/// <returns></returns>
		public float GetIntervalViewStart(float interval)
		{
			return (float)Math.Floor(this.DisplayViewMin / interval) * interval;
		}

		/// <summary>
		/// Computes the end value based on the given interval and the DisplayViewMax
		/// </summary>
		/// <param name="interval"></param>
		/// <returns></returns>
		public float GetIntervalViewEnd(float interval)
		{
			return (float)Math.Ceiling(DisplayViewMax / interval) * interval;
		}
		#endregion

		#region Private Functions
		/// <summary>
		/// Determines the proper interval based on the display settings and minimum spacing
		/// </summary>
		/// <param name="intervalTable"></param>
		/// <param name="spacing"></param>
		/// <returns></returns>
		private float FindInterval(float[] intervalTable, float spacing)
		{
			float range = this.DisplayViewRange;
			float maxNum = this.HeaderWidth / spacing;
			foreach (float testInter in intervalTable)
			{
				if (range / testInter < maxNum)
				{
					return testInter;
				}
			}

			return range;
		}

		/// <summary>
		/// Updates the label and tick intervals based on the current display settings.
		/// </summary>
		private void UpdateIntervals()
		{
			m_labelInterval = FindInterval(sm_labelIntervals, c_minLabelSpacing);
			m_tickInterval = FindInterval(sm_tickIntervals, c_minTickSpacing);
		}
		#endregion
	}
}
