using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace TestApp
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            this.eventEditorControl1.AddTrackType( "generic" );
            this.eventEditorControl1.AddTrackType( "named" );
        }

        int m_namedEventCount = 1;
        private void eventEditorControl1_EventCreate( object sender, EventEditorControls.EventCreateOnTrackEventArgs e )
        {
            if ( e.TrackControl.TypeName == "named" )
            {
                e.EventControl = this.eventEditorControl1.CreateEvent( String.Format( "Event #{0}", m_namedEventCount++ ), 
                    e.TrackControl.TypeName, e.MinT, e.MaxT, -1.0f, e.TrackControl );
            }
            else
            {
                e.EventControl = this.eventEditorControl1.CreateEvent( e.TrackControl.Text, e.TrackControl.TypeName, e.MinT, e.MaxT, -1.0f,
                    e.TrackControl );
            }
        }

        int m_namedTrackCount = 1;
        private void eventEditorControl1_TrackCreate( object sender, EventEditorControls.TrackCreateEventArgs e )
        {
            if ( e.TypeName == "named" )
            {
                e.TrackControl = this.eventEditorControl1.CreateTrack( String.Format( "Track {0}", m_namedTrackCount++ ), e.TypeName );
            }
            else
            {
                e.TrackControl = this.eventEditorControl1.CreateTrack( e.TypeName, e.TypeName );
            }
        }
    }
}