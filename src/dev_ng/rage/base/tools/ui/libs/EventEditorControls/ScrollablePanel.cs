using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;

namespace EventEditorControls
{
    public partial class ScrollablePanel : Panel
    {
        public ScrollablePanel()
        {
            InitializeComponent();
        }

        #region Constants
        private const int SB_LINEUP = 0;
        private const int SB_LINEDOWN = 1;
        private const int SB_PAGEUP = 2;
        private const int SB_PAGEDOWN = 3;
        private const int SB_THUMBPOSITION = 4;
        private const int SB_THUMBTRACK = 5;
        private const int SB_TOP = 6;
        private const int SB_BOTTOM = 7;
        private const int SB_ENDSCROLL = 8;

        private const int SBM_SETSCROLLINFO = 0x00E9;

        private const int WM_HSCROLL = 0x114;
        private const int WM_VSCROLL = 0x115;
        private const int WM_MOUSEWHEEL = 0x020A;
        private const int WM_NCCALCSIZE = 0x0083;
        private const int WM_PAINT = 0x000F;
        private const int WM_SIZE = 0x0005;

        private const uint SB_HORZ = 0;
        private const uint SB_VERT = 1;
        private const uint SB_CTL = 2;
        private const uint SB_BOTH = 3;

        private const uint ESB_DISABLE_BOTH = 0x3;
        private const uint ESB_ENABLE_BOTH = 0x0;

        private const int MK_LBUTTON = 0x01;
        private const int MK_RBUTTON = 0x02;
        private const int MK_SHIFT = 0x04;
        private const int MK_CONTROL = 0x08;
        private const int MK_MBUTTON = 0x10;
        private const int MK_XBUTTON1 = 0x0020;
        private const int MK_XBUTTON2 = 0x0040;
        #endregion

        #region Variables
        private bool m_enableAutoHorizontal = true;
        private bool m_enableAutoVertical = true;
        private bool m_visibleAutoHorizontal = true;
        private bool m_visibleAutoVertical = true;

        private int m_autoScrollHorizontalMinimum = 0;
        private int m_autoScrollHorizontalMaximum = 100;

        private int m_autoScrollVerticalMinimum = 0;
        private int m_autoScrollVerticalMaximum = 100;

        private bool m_enableRaisingEvents = true;
        #endregion

        #region Properties
        [DefaultValue( 0 )]
        public int AutoScrollHPos
        {
            get
            {
                return GetScrollPos( this.Handle, (int)SB_HORZ );
            }
            set
            {
                int pos = this.AutoScrollHPos;
                if ( value != pos )
                {
                    m_enableRaisingEvents = false;

                    SetScrollPos( this.Handle, (int)SB_HORZ, value, true );
                    
                    if ( value > pos )
                    {
                        int newPos = pos;
                        while ( value > newPos )
                        {
                            PerformScrollHorizontal( ScrollEventType.SmallIncrement );

                            pos = newPos;
                            newPos = this.AutoScrollHPos;

                            if ( newPos == pos )
                            {
                                break;
                            }
                        }
                    }
                    else
                    {
                        int newPos = pos;
                        while ( value < newPos )
                        {
                            PerformScrollHorizontal( ScrollEventType.SmallDecrement );

                            pos = newPos;
                            newPos = this.AutoScrollHPos;

                            if ( newPos == pos )
                            {
                                break;
                            }
                        }
                    }

                    m_enableRaisingEvents = true;
                }
            }
        }

        [DefaultValue( 0 )]
        public int AutoScrollVPos
        {
            get
            {
                return GetScrollPos( this.Handle, (int)SB_VERT );
            }
            set
            {
                int pos = this.AutoScrollVPos;
                if ( value != pos )
                {
                    m_enableRaisingEvents = false;

                    SetScrollPos( this.Handle, (int)SB_VERT, value, true );

                    if ( value > pos )
                    {
                        int newPos = pos;
                        while ( value > newPos )
                        {
                            PerformScrollVertical( ScrollEventType.SmallIncrement );

                            pos = newPos;
                            newPos = this.AutoScrollVPos;

                            if ( newPos == pos )
                            {
                                break;
                            }
                        }
                    }
                    else
                    {
                        int newPos = pos;
                        while ( value < newPos )
                        {
                            PerformScrollVertical( ScrollEventType.SmallDecrement );

                            pos = newPos;
                            newPos = this.AutoScrollVPos;

                            if ( newPos == pos )
                            {
                                break;
                            }
                        }
                    }

                    m_enableRaisingEvents = true;
                }
            }
        }

        [DefaultValue( 0 )]
        public int AutoScrollHorizontalMinimum
        {
            get
            {
                return m_autoScrollHorizontalMinimum;
            }
            set
            {
                m_autoScrollHorizontalMinimum = value;
                
                SetScrollRange( this.Handle, (int)SB_HORZ, m_autoScrollHorizontalMinimum, m_autoScrollHorizontalMaximum, true );
            }
        }

        [DefaultValue( 100 )]
        public int AutoScrollHorizontalMaximum
        {
            get
            {
                return m_autoScrollHorizontalMaximum;
            }
            set
            {
                m_autoScrollHorizontalMaximum = value;
                
                SetScrollRange( this.Handle, (int)SB_HORZ, m_autoScrollHorizontalMinimum, m_autoScrollHorizontalMaximum, true );
            }
        }

        [DefaultValue( 0 )]
        public int AutoScrollVerticalMinimum
        {
            get
            {
                return m_autoScrollVerticalMinimum;
            }
            set
            {
                m_autoScrollVerticalMinimum = value;
            
                SetScrollRange( this.Handle, (int)SB_VERT, m_autoScrollHorizontalMinimum, m_autoScrollHorizontalMaximum, true );
            }
        }

        [DefaultValue( 100 )]
        public int AutoScrollVerticalMaximum
        {
            get
            {
                return m_autoScrollVerticalMaximum;
            }
            set
            {
                m_autoScrollVerticalMaximum = value;
                
                SetScrollRange( this.Handle, (int)SB_VERT, m_autoScrollHorizontalMinimum, m_autoScrollHorizontalMaximum, true );
            }
        }

        [DefaultValue( true )]
        public bool EnableAutoScrollHorizontal
        {
            get
            {
                return m_enableAutoHorizontal;
            }
            set
            {
                m_enableAutoHorizontal = value;

                if ( value )
                {
                    EnableScrollBar( this.Handle, SB_HORZ, ESB_ENABLE_BOTH );
                }
                else
                {
                    EnableScrollBar( this.Handle, SB_HORZ, ESB_DISABLE_BOTH );
                }
            }
        }

        [DefaultValue( true )]
        public bool EnableAutoScrollVertical
        {
            get
            {
                return m_enableAutoVertical;
            }
            set
            {
                m_enableAutoVertical = value;

                if ( value )
                {
                    EnableScrollBar( this.Handle, SB_VERT, ESB_ENABLE_BOTH );
                }
                else
                {
                    EnableScrollBar( this.Handle, SB_VERT, ESB_DISABLE_BOTH );
                }
            }
        }

        [DefaultValue( true )]
        public bool VisibleAutoScrollHorizontal
        {
            get
            {
                return m_visibleAutoHorizontal;
            }
            set
            {
                m_visibleAutoHorizontal = value;
                
                ShowScrollBar( this.Handle, (int)SB_HORZ, value );
            }
        }

        [DefaultValue( true )]
        public bool VisibleAutoScrollVertical
        {
            get
            {
                return m_visibleAutoVertical;
            }
            set
            {
                m_visibleAutoVertical = value;
                
                ShowScrollBar( this.Handle, (int)SB_VERT, value );
            }
        }
        #endregion

        #region Events
        public event ScrollEventHandler ScrollHorizontal;
        public event ScrollEventHandler ScrollVertical;
        public event MouseEventHandler ScrollMouseWheel;
        #endregion

        #region Event Dispatchers
        protected void OnScrollHorizontal( ScrollEventArgs e )
        {
            if ( m_enableRaisingEvents && (this.ScrollHorizontal != null) )
            {
                this.ScrollHorizontal( this, e );
            }
        }

        protected void OnScrollVertical( ScrollEventArgs e )
        {
            if ( m_enableRaisingEvents && (this.ScrollVertical != null) )
            {
                this.ScrollVertical( this, e );
            }
        }

        protected void OnScrollMouseWheel( MouseEventArgs e )
        {
            if ( m_enableRaisingEvents && (this.ScrollMouseWheel != null) )
            {
                this.ScrollMouseWheel( this, e );
            }
        }
        #endregion

        #region Overrides
        protected override void WndProc( ref Message msg )
        {
            if ( msg.HWnd != this.Handle )
            {
                return;
            }

            switch ( msg.Msg )
            {
                case WM_MOUSEWHEEL:
                    {
                        if ( !this.VisibleAutoScrollVertical )
                        {
                            return;
                        }

                        try
                        {
                            int zDelta = HiWord( (int)msg.WParam );
                            int y = HiWord( (int)msg.LParam );
                            int x = LoWord( (int)msg.LParam );
                            MouseButtons button;

                            switch ( LoWord( (int)msg.WParam ) )
                            {
                                case MK_LBUTTON:
                                    button = MouseButtons.Left;
                                    break;
                                case MK_MBUTTON:
                                    button = MouseButtons.Middle;
                                    break;
                                case MK_RBUTTON:
                                    button = MouseButtons.Right;
                                    break;
                                case MK_XBUTTON1:
                                    button = MouseButtons.XButton1;
                                    break;
                                case MK_XBUTTON2:
                                    button = MouseButtons.XButton2;
                                    break;
                                default:
                                    button = MouseButtons.None;
                                    break;
                            }

                            OnScrollMouseWheel( new MouseEventArgs( button, 1, x, y, zDelta ) );
                        }
                        catch ( Exception )
                        {

                        }
                    }
                    break;
                case WM_VSCROLL:
                    {
                        try
                        {
                            ScrollEventType type = GetScrollEventType( msg.WParam );
                            int pos = GetScrollPos( this.Handle, (int)SB_VERT );

                            OnScrollVertical( new ScrollEventArgs( type, pos ) );
                        }
                        catch ( Exception )
                        {
                        
                        }
                    }
                    break;
                case WM_HSCROLL:
                    {
                        try
                        {
                            ScrollEventType type = GetScrollEventType( msg.WParam );
                            int pos = GetScrollPos( this.Handle, (int)SB_HORZ );

                            OnScrollHorizontal( new ScrollEventArgs( type, pos ) );
                        }
                        catch ( Exception )
                        {

                        }
                    }
                    break;

                default:
                    break;
            }

            base.WndProc( ref msg );
        }
        #endregion

        #region Event Handlers
        private void ScrollablePanel_Click( object sender, EventArgs e )
        {
            this.Focus();
        }
        #endregion

        #region Public Functions
        public void PerformScrollHorizontal( ScrollEventType type )
        {
            int param = GetSBFromScrollEventType( type );
            if ( param == -1 )
            {
                return;
            }

            SendMessage( this.Handle, (uint)WM_HSCROLL, (System.UIntPtr)param, (System.IntPtr)0 );
        }

        public void PerformScrollVertical( ScrollEventType type )
        {
            int param = GetSBFromScrollEventType( type );
            if ( param == -1 )
            {
                return;
            }

            SendMessage( this.Handle, (uint)WM_VSCROLL, (System.UIntPtr)param, (System.IntPtr)0 );
        }
        #endregion

        #region Private Functions
        private int GetSBFromScrollEventType( ScrollEventType type )
        {
            int res = -1;
            switch ( type )
            {
                case ScrollEventType.SmallDecrement:
                    res = SB_LINEUP;
                    break;
                case ScrollEventType.SmallIncrement:
                    res = SB_LINEDOWN;
                    break;
                case ScrollEventType.LargeDecrement:
                    res = SB_PAGEUP;
                    break;
                case ScrollEventType.LargeIncrement:
                    res = SB_PAGEDOWN;
                    break;
                case ScrollEventType.ThumbTrack:
                    res = SB_THUMBTRACK;
                    break;
                case ScrollEventType.First:
                    res = SB_TOP;
                    break;
                case ScrollEventType.Last:
                    res = SB_BOTTOM;
                    break;
                case ScrollEventType.ThumbPosition:
                    res = SB_THUMBPOSITION;
                    break;
                case ScrollEventType.EndScroll:
                    res = SB_ENDSCROLL;
                    break;
                default:
                    break;
            }
            return res;
        }

        private ScrollEventType GetScrollEventType( System.IntPtr wParam )
        {
            ScrollEventType res = 0;
            switch ( LoWord( (int)wParam ) )
            {
                case SB_LINEUP:
                    res = ScrollEventType.SmallDecrement;
                    break;
                case SB_LINEDOWN:
                    res = ScrollEventType.SmallIncrement;
                    break;
                case SB_PAGEUP:
                    res = ScrollEventType.LargeDecrement;
                    break;
                case SB_PAGEDOWN:
                    res = ScrollEventType.LargeIncrement;
                    break;
                case SB_THUMBTRACK:
                    res = ScrollEventType.ThumbTrack;
                    break;
                case SB_TOP:
                    res = ScrollEventType.First;
                    break;
                case SB_BOTTOM:
                    res = ScrollEventType.Last;
                    break;
                case SB_THUMBPOSITION:
                    res = ScrollEventType.ThumbPosition;
                    break;
                case SB_ENDSCROLL:
                    res = ScrollEventType.EndScroll;
                    break;
                default:
                    res = ScrollEventType.EndScroll;
                    break;
            }

            return res;
        }
        #endregion

        #region API32 functions
        [DllImport( "user32.dll", CharSet = CharSet.Auto )]
        static public extern int GetSystemMetrics( int code );

        [DllImport( "user32.dll" )]
        static public extern bool EnableScrollBar( System.IntPtr hWnd, uint wSBflags, uint wArrows );

        [DllImport( "user32.dll" )]
        static public extern int SetScrollRange( System.IntPtr hWnd, int nBar, int nMinPos, int nMaxPos, bool bRedraw );

        [DllImport( "user32.dll" )]
        static public extern int SetScrollPos( System.IntPtr hWnd, int nBar, int nPos, bool bRedraw );

        [DllImport( "user32.dll" )]
        static public extern int GetScrollPos( System.IntPtr hWnd, int nBar );

        [DllImport( "user32.dll" )]
        static public extern bool ShowScrollBar( System.IntPtr hWnd, int wBar, bool bShow );

        [DllImport( "user32.dll" )]
        static extern IntPtr SendMessage( IntPtr hWnd, uint Msg, UIntPtr wParam, IntPtr lParam );

        [DllImport( "user32.dll" )]
        static extern int HIWORD( System.IntPtr wParam );

        static int MakeLong( int LoWord, int HiWord )
        {
            return (HiWord << 16) | (LoWord & 0xffff);
        }

        static IntPtr MakeLParam( int LoWord, int HiWord )
        {
            return (IntPtr)((HiWord << 16) | (LoWord & 0xffff));
        }

        static int HiWord( int number )
        {
            if ( (number & 0x80000000) == 0x80000000 )
            {
                return (number >> 16);
            }
            else
            {
                return (number >> 16) & 0xffff;
            }
        }

        static int LoWord( int number )
        {
            return number & 0xffff;
        }
        #endregion
    }
}
