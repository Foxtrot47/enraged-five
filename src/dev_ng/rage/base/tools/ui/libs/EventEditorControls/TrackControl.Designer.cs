namespace EventEditorControls
{
    partial class TrackControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && (components != null) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // TrackControl
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.Name = "TrackControl";
            this.Size = new System.Drawing.Size( 576, 30 );
            this.Paint += new System.Windows.Forms.PaintEventHandler( this.TrackControl_Paint );
            this.MouseMove += new System.Windows.Forms.MouseEventHandler( this.TrackControl_MouseMove );
            this.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler( this.TrackControl_MouseDoubleClick );
            this.ResumeLayout( false );

        }

        #endregion

    }
}
