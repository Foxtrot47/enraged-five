using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace EventEditorControls
{
	public partial class EventEditorControl : UserControl
	{
		public EventEditorControl()
		{
			InitializeComponent();

			SetStyle(ControlStyles.ResizeRedraw, true);
			SetStyle(ControlStyles.AllPaintingInWmPaint, true);
			SetStyle(ControlStyles.UserPaint, true);
			SetStyle(ControlStyles.DoubleBuffer, true);
			SetStyle(ControlStyles.SupportsTransparentBackColor, true);
			SetStyle(ControlStyles.ContainerControl, true);

			SetViewWindow(this.timeLineComponent.DisplayMin, this.timeLineComponent.DisplayMax);
		}

		#region Constants
		public const float MaxZoomLevel = 100.0f;
		#endregion

		#region Variables
		private int m_currentHeight;
		private float m_currentPhase = 0.0f;

		private List<string> m_trackTypes = new List<string>();
		private Dictionary<string, bool> m_trackFilter = new Dictionary<string, bool>();
		#endregion

		#region Properties
		public TimeLineComponent TimeLine
		{
			get
			{
				return this.timeLineComponent;
			}
		}

		[DefaultValue(true)]
		public bool FilterButtonEnabled
		{
			get
			{
				return this.filterButton.Enabled;
			}
			set
			{
				this.filterButton.Enabled = value;
			}
		}

		[DefaultValue(true)]
		public bool CreateButtonEnabled
		{
			get
			{
				return this.createButton.Enabled;
			}
			set
			{
				this.createButton.Enabled = value;
			}
		}

		[Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public float CurrentPhase
		{
			get
			{
				return m_currentPhase;
			}
			set
			{
				if (m_currentPhase != value)
				{
					m_currentPhase = value;

					this.scrubBarControl.Left = (int)this.TimeLine.PhaseToPixel(m_currentPhase);

					this.Invalidate();
				}
			}
		}

		[Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public float CurrentTime
		{
			get
			{
				return this.timeLineComponent.PhaseToDisplay(this.CurrentPhase);
			}
			set
			{
				this.CurrentPhase = this.timeLineComponent.DisplayToPhase(value);
			}
		}

		public List<string> TrackTypes
		{
			get
			{
				return m_trackTypes;
			}
		}

		public Dictionary<string, bool> TrackFilters
		{
			get
			{
				return m_trackFilter;
			}
		}

		public List<TrackControl> TrackControls
		{
			get
			{
				List<TrackControl> trackCtrls = new List<TrackControl>();
				foreach (TrackControl ctrl in this.tracksScrollablePanel.Controls)
				{
					trackCtrls.Add(ctrl);
				}

				return trackCtrls;
			}
		}

		public List<TrackNameControl> TrackNameControls
		{
			get
			{
				List<TrackNameControl> trackNameCtrls = new List<TrackNameControl>();
				foreach (TrackNameControl ctrl in this.trackNamesScrollablePanel.Controls)
				{
					trackNameCtrls.Add(ctrl);
				}

				return trackNameCtrls;
			}
		}

		public List<EventControl> EventControls
		{
			get
			{
				List<EventControl> eventCtrls = new List<EventControl>();
				foreach (TrackControl ctrl in this.tracksScrollablePanel.Controls)
				{
					eventCtrls.AddRange(ctrl.EventControls);
				}

				return eventCtrls;
			}
		}

		[Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public EventEditorControlSettings EventEditorControlSettings
		{
			get
			{
				EventEditorControlSettings settings = new EventEditorControlSettings();
				foreach (KeyValuePair<string, bool> pair in this.TrackFilters)
				{
					if (!pair.Value)
					{
						settings.DisabledTrackCollection.Add(pair.Key);
					}
				}

				return settings;
			}
			set
			{
				if (value != null)
				{
					for (int i = 0; i < this.trackNamesScrollablePanel.Controls.Count; ++i)
					{
						TrackNameControl nameCtrl = this.trackNamesScrollablePanel.Controls[i] as TrackNameControl;
						EnableTrackInternal(i, !value.DisabledTrackCollection.Contains(nameCtrl.TypeName));
					}

					UpdateTrackPosition();
				}
			}
		}
		#endregion

		#region Events
		public event TrackCreateEventHandler TrackCreate;

		public event EventCreateOnTrackEventHandler EventCreate;
		public event EventOnTrackWithCancelEventHandler EventDelete;
		public event EventOnTrackEventHandler EventEdit;
		public event EventOnTrackEventHandler EventMoving;
		public event EventOnTrackEventHandler EventMoved;

		public event EventOnTrackEventHandler SelectionChanged;
		#endregion

		#region Event Dispatchers
		protected void OnTrackCreate(TrackCreateEventArgs e)
		{
			if (this.TrackCreate != null)
			{
				this.TrackCreate(this, e);
			}
			else
			{
				e.TrackControl = CreateTrack(e.Text, e.TypeName);
			}

			if (e.TrackControl != null)
			{
				AddTrack(e.TrackControl);
			}

		}

		protected void OnEventCreate(EventCreateOnTrackEventArgs e)
		{
			if (this.EventCreate != null)
			{
				this.EventCreate(this, e);
			}
			else
			{
				e.EventControl = CreateEvent(e.TrackControl.Text, e.TrackControl.TypeName, e.MinT, e.MaxT, -1.0f, e.TrackControl);
			}

			if (e.EventControl != null)
			{
				AddEvent(e.EventControl, e.TrackControl);
			}
		}

		protected void OnEventDelete(EventOnTrackWithCancelEventArgs e)
		{
			if (this.EventDelete != null)
			{
				this.EventDelete(this, e);
			}

			if (!e.Cancel)
			{
				RemoveEvent(e.EventControl);
			}
		}

		protected void OnEventEdit(EventOnTrackEventArgs e)
		{
			if (this.EventEdit != null)
			{
				this.EventEdit(this, e);
			}
		}

		protected void OnEventMoving(EventOnTrackEventArgs e)
		{
			if (this.EventMoving != null)
			{
				this.EventMoving(this, e);
			}
		}

		protected void OnEventMoved(EventOnTrackEventArgs e)
		{
			if (this.EventMoved != null)
			{
				this.EventMoved(this, e);
			}
		}

		protected void OnSelectionChanged(EventOnTrackEventArgs e)
		{
			if (this.SelectionChanged != null)
			{
				this.SelectionChanged(this, e);
			}
		}
		#endregion

		#region Event Handlers
		private void filterButton_Click(object sender, EventArgs e)
		{
			if (this.filterContextMenuStrip.Items.Count > 0)
			{
				this.filterContextMenuStrip.Show(this.filterButton, new Point(0, this.filterButton.Height));
			}
		}

		private void createButton_Click(object sender, EventArgs e)
		{
			if (this.createContextMenuStrip.Items.Count > 0)
			{
				this.createContextMenuStrip.Show(this.createButton, new Point(0, this.createButton.Height));
			}
		}

		private void filterMenuItem_Click(object sender, EventArgs e)
		{
			ToolStripMenuItem filterMenuItem = sender as ToolStripMenuItem;
			if (filterMenuItem != null)
			{
				EnableTrack(filterMenuItem.Text, filterMenuItem.Checked);
			}
		}

		private void createMenuItem_Click(object sender, EventArgs e)
		{
			ToolStripMenuItem createMenuItem = sender as ToolStripMenuItem;
			if (createMenuItem != null)
			{
				OnTrackCreate(new TrackCreateEventArgs(createMenuItem.Text, createMenuItem.Text));
			}
		}

		private void zoomHScrollBar_Scroll(object sender, ScrollEventArgs e)
		{
			// get the current display range
			float displayRange = this.timeLineComponent.DisplayMax - this.timeLineComponent.DisplayMin;

			// get the current display center
			float displayCenter = (this.timeLineComponent.DisplayViewMin + this.timeLineComponent.DisplayViewMax) / 2.0f;

			// the new window size can be somewhere between the full window size and 20x zoom            
			float newWindowSize = TimeLineComponent.Remap(this.zoomHScrollBar.Value,
				this.zoomHScrollBar.Minimum, this.zoomHScrollBar.Maximum - this.zoomHScrollBar.LargeChange + 1,
				displayRange, displayRange / EventEditorControl.MaxZoomLevel);

			float newMinT = displayCenter - newWindowSize / 2.0f;
			float newMaxT = displayCenter + newWindowSize / 2.0f;

			SetViewWindow(newMinT, newMaxT);
		}

		private void hScrollBar_Scroll(object sender, ScrollEventArgs e)
		{
			// set the new view window
			float newMinT = TimeLineComponent.Remap(this.hScrollBar.Value,
				this.hScrollBar.Minimum, this.hScrollBar.Maximum,
				this.timeLineComponent.DisplayMin, this.timeLineComponent.DisplayMax);
			float newMaxT = TimeLineComponent.Remap(this.hScrollBar.Value + this.hScrollBar.LargeChange,
				this.hScrollBar.Minimum, this.hScrollBar.Maximum,
				this.timeLineComponent.DisplayMin, this.timeLineComponent.DisplayMax);

			SetViewWindow(newMinT, newMaxT);
		}

		private void trackNamesScrollablePanel_ScrollMouseWheel(object sender, MouseEventArgs e)
		{
			this.tracksScrollablePanel.AutoScrollVPos = this.trackNamesScrollablePanel.AutoScrollVPos;
		}

		private void trackNamesScrollablePanel_ScrollVertical(object sender, ScrollEventArgs e)
		{
			this.tracksScrollablePanel.AutoScrollVPos = this.trackNamesScrollablePanel.AutoScrollVPos;
		}

		private void tracksScrollablePanel_ScrollMouseWheel(object sender, MouseEventArgs e)
		{
			this.trackNamesScrollablePanel.AutoScrollVPos = this.tracksScrollablePanel.AutoScrollVPos;
		}

		private void tracksScrollablePanel_ScrollVertical(object sender, ScrollEventArgs e)
		{
			this.trackNamesScrollablePanel.AutoScrollVPos = this.tracksScrollablePanel.AutoScrollVPos;
		}

		private void trackControl_EventCreate(object sender, EventCreateEventArgs e)
		{
			OnEventCreate(new EventCreateOnTrackEventArgs(sender as TrackControl, e.MinT, e.MaxT));
		}

		private void trackControl_EventDelete(object sender, EventControlEventArgs e)
		{
			OnEventDelete(new EventOnTrackWithCancelEventArgs(sender as TrackControl, e.EventControl));
		}

		private void trackControl_EventEdit(object sender, EventControlEventArgs e)
		{
			OnEventEdit(new EventOnTrackEventArgs(sender as TrackControl, e.EventControl));
		}

		private void trackControl_EventMoving(object sender, EventControlEventArgs e)
		{
			this.timeLineHeaderControl.Invalidate();
			this.timeLineHeaderControl.Update();

			OnEventMoving(new EventOnTrackEventArgs(sender as TrackControl, e.EventControl));
		}

		private void trackControl_EventMoved(object sender, EventControlEventArgs e)
		{
			OnEventMoved(new EventOnTrackEventArgs(sender as TrackControl, e.EventControl));
		}

		private void trackControl_EventSelectionChanged(object sender, EventControlEventArgs e)
		{
			OnSelectionChanged(new EventOnTrackEventArgs(sender as TrackControl, e.EventControl));
		}
		#endregion

		#region Public Functions
		public void SetPhaseRange(float min, float max)
		{
			this.TimeLine.SetPhase(min, max);
			this.Invalidate();
		}

		public void SetDisplayRange(float min, float max)
		{
			this.TimeLine.SetDisplay(min, max);
			this.timeLineHeaderControl.Invalidate();
			this.Invalidate();
		}

		public void SetDisplayView(float min, float max)
		{
			SetViewWindow(min, max);
		}

		/// <summary>
		/// Adds a new track type if it doesn't exist already.
		/// </summary>
		/// <param name="typeName"></param>
		public void AddTrackType(string typeName)
		{
			if (!this.TrackFilters.ContainsKey(typeName))
			{
				this.TrackTypes.Add(typeName);
				this.TrackFilters.Add(typeName, true);

				ToolStripMenuItem filterMenuItem = new ToolStripMenuItem(typeName);
				filterMenuItem.Checked = true;
				filterMenuItem.CheckOnClick = true;
				filterMenuItem.Click += new EventHandler(filterMenuItem_Click);

				this.filterContextMenuStrip.Items.Add(filterMenuItem);

				ToolStripMenuItem createMenuItem = new ToolStripMenuItem(typeName);
				createMenuItem.Click += new EventHandler(createMenuItem_Click);

				this.createContextMenuStrip.Items.Add(createMenuItem);
			}
		}

		/// <summary>
		/// Removes the track type
		/// </summary>
		/// <param name="typeName"></param>
		public void RemoveTrackType(string typeName)
		{
			int indexOf = this.TrackTypes.IndexOf(typeName);
			if (indexOf != -1)
			{
				this.TrackTypes.RemoveAt(indexOf);
				this.TrackFilters.Remove(typeName);

				this.filterContextMenuStrip.Items.RemoveAt(indexOf);
				this.createContextMenuStrip.Items.RemoveAt(indexOf);
			}
		}

		/// <summary>
		/// Changes the track type name
		/// </summary>
		/// <param name="oldTypeName"></param>
		/// <param name="newTypeName"></param>
		public void ChangeTrackTypeName(string oldTypeName, string newTypeName)
		{
			if (String.IsNullOrEmpty(newTypeName))
			{
				return;
			}

			int indexOf = this.TrackTypes.IndexOf(oldTypeName);
			if (indexOf != -1)
			{
				this.TrackTypes[indexOf] = newTypeName;

				bool enable;
				if (this.TrackFilters.TryGetValue(oldTypeName, out enable))
				{
					this.TrackFilters.Remove(oldTypeName);

					if (!this.TrackFilters.ContainsKey(newTypeName))
					{
						this.TrackFilters.Add(newTypeName, enable);
					}
				}

				this.filterContextMenuStrip.Items[indexOf].Text = newTypeName;
				this.createContextMenuStrip.Items[indexOf].Text = newTypeName;

				List<TrackControl> trackCtrls = this.TrackControls;
				foreach (TrackControl trackCtrl in trackCtrls)
				{
					if (trackCtrl.TypeName == oldTypeName)
					{
						trackCtrl.TypeName = newTypeName;
					}
				}

				List<TrackNameControl> trackNameCtrls = this.TrackNameControls;
				foreach (TrackNameControl trackNameCtrl in trackNameCtrls)
				{
					if (trackNameCtrl.TypeName == oldTypeName)
					{
						trackNameCtrl.TypeName = newTypeName;
					}
				}
			}
		}

		/// <summary>
		/// Creates a new track, but does not add it.
		/// </summary>
		/// <param name="text"></param>
		/// <param name="typeName"></param>
		/// <returns></returns>
		public TrackControl CreateTrack(string text, string typeName)
		{
			// create the track control
			TrackControl trackCtrl = new TrackControl();
			trackCtrl.Anchor = AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top;
			trackCtrl.BackColor = SystemColors.ControlLight;
			trackCtrl.Height = EventControl.StandardHeight;
			trackCtrl.Text = text;
			trackCtrl.TimeLine = this.timeLineComponent;
			trackCtrl.TypeName = typeName;

			return trackCtrl;
		}

		/// <summary>
		/// Adds the track, creating and adding its associated TrackNameControl as well.
		/// </summary>
		/// <param name="trackCtrl"></param>
		public void AddTrack(TrackControl trackCtrl)
		{
			// create the name control
			TrackNameControl nameCtrl = new TrackNameControl();
			nameCtrl.Anchor = AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top;
			nameCtrl.BackColor = SystemColors.Control;
			nameCtrl.Height = trackCtrl.Height;
			nameCtrl.Text = trackCtrl.Text;
			nameCtrl.Top = m_currentHeight + this.tracksScrollablePanel.AutoScrollPosition.Y;
			nameCtrl.TypeName = trackCtrl.TypeName;
			nameCtrl.Width = this.trackNamesScrollablePanel.Width;

			this.trackNamesScrollablePanel.Controls.Add(nameCtrl);

			trackCtrl.Left = this.tracksScrollablePanel.AutoScrollPosition.X;
			trackCtrl.Top = m_currentHeight + this.tracksScrollablePanel.AutoScrollPosition.Y;
			trackCtrl.TrackNumber = this.tracksScrollablePanel.Controls.Count;
			trackCtrl.Width = this.tracksScrollablePanel.Width;

			// add event handlers
			trackCtrl.EventCreate += new EventCreateEventHandler(trackControl_EventCreate);
			trackCtrl.EventDelete += new EventControlEventHandler(trackControl_EventDelete);
			trackCtrl.EventEdit += new EventControlEventHandler(trackControl_EventEdit);
			trackCtrl.EventMoved += new EventControlEventHandler(trackControl_EventMoved);
			trackCtrl.EventMoving += new EventControlEventHandler(trackControl_EventMoving);
			trackCtrl.SelectionChanged += new EventControlEventHandler(trackControl_EventSelectionChanged);

			this.tracksScrollablePanel.Controls.Add(trackCtrl);

			m_currentHeight += trackCtrl.Height;
		}

		/// <summary>
		/// Removes the first track found with the matching text.
		/// </summary>
		/// <param name="name"></param>
		public void RemoveTrack(string text)
		{
			TrackNameControl nameCtrl = null;
			foreach (TrackNameControl ctrl in this.trackNamesScrollablePanel.Controls)
			{
				if (ctrl.Text == text)
				{
					nameCtrl = ctrl;
					break;
				}
			}

			if (nameCtrl != null)
			{
				RemoveTrack(nameCtrl);
			}
		}

		/// <summary>
		/// Removes the track at the specified index.
		/// </summary>
		/// <param name="trackIndex"></param>
		public void RemoveTrack(int trackIndex)
		{
			if ((trackIndex < 0) || (trackIndex >= this.tracksScrollablePanel.Controls.Count))
			{
				return;
			}

			// remove event handlers
			TrackControl trackCtrl = this.tracksScrollablePanel.Controls[trackIndex] as TrackControl;
			trackCtrl.EventCreate -= new EventCreateEventHandler(trackControl_EventCreate);
			trackCtrl.EventDelete -= new EventControlEventHandler(trackControl_EventDelete);
			trackCtrl.EventEdit -= new EventControlEventHandler(trackControl_EventEdit);
			trackCtrl.EventMoved -= new EventControlEventHandler(trackControl_EventMoved);
			trackCtrl.EventMoving -= new EventControlEventHandler(trackControl_EventMoving);
			trackCtrl.SelectionChanged -= new EventControlEventHandler(trackControl_EventSelectionChanged);

			this.tracksScrollablePanel.Controls.RemoveAt(trackIndex);
			for (int i = trackIndex; i < this.tracksScrollablePanel.Controls.Count; ++i)
			{
				TrackControl ctrl = this.tracksScrollablePanel.Controls[i] as TrackControl;
				ctrl.TrackNumber = i;
			}

			this.trackNamesScrollablePanel.Controls.RemoveAt(trackIndex);

			UpdateTrackPosition();
		}

		/// <summary>
		/// Removes the track control and its associated TrackNameControl.
		/// </summary>
		/// <param name="trackCtrl"></param>
		public void RemoveTrack(TrackControl trackCtrl)
		{
			int indexOf = this.tracksScrollablePanel.Controls.IndexOf(trackCtrl);
			if (indexOf != -1)
			{
				RemoveTrack(indexOf);
			}
		}

		/// <summary>
		/// Removes the TrackNameControl and its associated TrackControl.
		/// </summary>
		/// <param name="nameCtrl"></param>
		public void RemoveTrack(TrackNameControl nameCtrl)
		{
			int indexOf = this.trackNamesScrollablePanel.Controls.IndexOf(nameCtrl);
			if (indexOf != -1)
			{
				RemoveTrack(indexOf);
			}
		}

		/// <summary>
		/// Creates a new EventControl with default parameters, but does not add it.
		/// </summary>
		/// <param name="text"></param>
		/// <param name="typeName"></param>
		/// <param name="trackIndex"></param>
		/// <returns></returns>
		public EventControl CreateEvent(string text, string typeName, int trackIndex)
		{
			if ((trackIndex >= 0) && (trackIndex < this.tracksScrollablePanel.Controls.Count))
			{
				return CreateEvent(text, typeName, this.tracksScrollablePanel.Controls[trackIndex] as TrackControl);
			}

			return null;
		}

		/// <summary>
		/// Creates a new EventControl with default parameters, but does not add it.
		/// </summary>
		/// <param name="text"></param>
		/// <param name="typeName"></param>
		/// <param name="trackCtrl"></param>
		/// <returns></returns>
		public EventControl CreateEvent(string text, string typeName, TrackControl trackCtrl)
		{
			return trackCtrl.CreateEvent(text, typeName, m_currentPhase);
		}

		/// <summary>
		/// Creates a new EventControl, but does not add it.
		/// </summary>
		/// <param name="text"></param>
		/// <param name="typeName"></param>
		/// <param name="minT"></param>
		/// <param name="maxT"></param>
		/// <param name="midT"></param>
		/// <param name="trackIndex"></param>
		/// <returns></returns>
		public EventControl CreateEvent(string text, string typeName, float minT, float maxT, float midT, int trackIndex)
		{
			if ((trackIndex >= 0) && (trackIndex < this.tracksScrollablePanel.Controls.Count))
			{
				return CreateEvent(text, typeName, minT, maxT, midT, this.tracksScrollablePanel.Controls[trackIndex] as TrackControl);
			}

			return null;
		}

		/// <summary>
		/// Creates a new EventControl, but does not add it.
		/// </summary>
		/// <param name="text"></param>
		/// <param name="typeName"></param>
		/// <param name="minT"></param>
		/// <param name="maxT"></param>
		/// <param name="midT"></param>
		/// <param name="trackCtrl"></param>
		/// <returns></returns>
		public EventControl CreateEvent(string text, string typeName, float minT, float maxT, float midT, TrackControl trackCtrl)
		{
			return trackCtrl.CreateEvent(text, typeName, minT, maxT, midT);
		}

		/// <summary>
		/// Adds the EventControl to the specified track index.
		/// </summary>
		/// <param name="eventCtrl"></param>
		/// <param name="trackIndex"></param>
		public void AddEvent(EventControl eventCtrl, int trackIndex)
		{
			if ((trackIndex >= 0) && (trackIndex < this.tracksScrollablePanel.Controls.Count))
			{
				AddEvent(eventCtrl, this.tracksScrollablePanel.Controls[trackIndex] as TrackControl);
			}
		}

		/// <summary>
		/// Adds the EventControl to the specified TrackControl.
		/// </summary>
		/// <param name="eventCtrl"></param>
		/// <param name="trackCtrl"></param>
		public void AddEvent(EventControl eventCtrl, TrackControl trackCtrl)
		{
			trackCtrl.AddEvent(eventCtrl);
		}

		/// <summary>
		/// Removes the EventControl.
		/// </summary>
		/// <param name="eventCtrl"></param>
		public void RemoveEvent(EventControl eventCtrl)
		{
			foreach (TrackControl trackCtrl in this.tracksScrollablePanel.Controls)
			{
				trackCtrl.RemoveEvent(eventCtrl);
			}
		}

		/// <summary>
		/// Removes the EventControl with the given Guid.
		/// </summary>
		/// <param name="guid"></param>
		public void RemoveEvent(Guid guid)
		{
			foreach (TrackControl trackCtrl in this.tracksScrollablePanel.Controls)
			{
				trackCtrl.RemoveEvent(guid);
			}
		}

		/// <summary>
		/// Enables or disables tracks with the given typeName.  Disabled tracks are made invisible.
		/// </summary>
		/// <param name="typeName"></param>
		/// <param name="enable"></param>
		public void EnableTrack(string typeName, bool enable)
		{
			for (int i = 0; i < this.trackNamesScrollablePanel.Controls.Count; ++i)
			{
				TrackNameControl nameCtrl = this.trackNamesScrollablePanel.Controls[i] as TrackNameControl;
				if (nameCtrl.TypeName == typeName)
				{
					EnableTrackInternal(i, enable);
				}
			}

			UpdateTrackPosition();
		}

		/// <summary>
		/// Enables or disables the track at the given index.  Disabled tracks are made invisible.
		/// </summary>
		/// <param name="trackIndex"></param>
		/// <param name="enable"></param>
		public void EnableTrack(int trackIndex, bool enable)
		{
			EnableTrackInternal(trackIndex, enable);

			UpdateTrackPosition();
		}

		/// <summary>
		/// Enables to disables the given TrackControl and its associated TrackNameControl.  Disabled tracks are made invisible.
		/// </summary>
		/// <param name="trackCtrl"></param>
		/// <param name="enable"></param>
		public void EnableTrack(TrackControl trackCtrl, bool enable)
		{
			int indexOf = this.tracksScrollablePanel.Controls.IndexOf(trackCtrl);
			if (indexOf != -1)
			{
				EnableTrack(indexOf, enable);
			}
		}

		/// <summary>
		/// Enables to disables the given TrackNameControl and its associated TrackControl.  Disabled tracks are made invisible.
		/// </summary>
		/// <param name="nameCtrl"></param>
		/// <param name="enable"></param>
		public void EnableTrack(TrackNameControl nameCtrl, bool enable)
		{
			int indexOf = this.trackNamesScrollablePanel.Controls.IndexOf(nameCtrl);
			if (indexOf != -1)
			{
				EnableTrack(indexOf, enable);
			}
		}

		public TrackControl FindTrack(int trackIndex)
		{
			if ((trackIndex >= 0) && (trackIndex < this.tracksScrollablePanel.Controls.Count))
			{
				return this.tracksScrollablePanel.Controls[trackIndex] as TrackControl;
			}

			return null;
		}

		public TrackControl FindTrack(object tag)
		{
			foreach (TrackControl ctrl in this.tracksScrollablePanel.Controls)
			{
				if ((ctrl.Tag != null) && ctrl.Tag.Equals(tag))
				{
					return ctrl;
				}
			}

			return null;
		}

		public EventControl FindEvent(Guid guid)
		{
			foreach (TrackControl ctrl in this.tracksScrollablePanel.Controls)
			{
				EventControl eventCtrl = ctrl.FindEvent(guid);
				if (eventCtrl != null)
				{
					return eventCtrl;
				}
			}

			return null;
		}

		public EventControl FindEvent(object tag)
		{
			foreach (TrackControl ctrl in this.tracksScrollablePanel.Controls)
			{
				EventControl eventCtrl = ctrl.FindEvent(tag);
				if (eventCtrl != null)
				{
					return eventCtrl;
				}
			}

			return null;
		}

		public void Clear()
		{
			this.CurrentTime = 0.0f;

			m_trackTypes.Clear();
			m_trackFilter.Clear();

			this.filterContextMenuStrip.Items.Clear();
			this.createContextMenuStrip.Items.Clear();

			this.trackNamesScrollablePanel.Controls.Clear();
			this.tracksScrollablePanel.Controls.Clear();

			UpdateTrackPosition();
		}

		/// <summary>
		/// Returns a list of all of the selected event objects.
		/// Currently only one item can be selected, but in the future
		/// we can have multiple selection
		/// </summary>
		/// <returns></returns>
		public List<EventControl> FindSelectedEvents()
		{
			List<EventControl> evts = new List<EventControl>();
			foreach (TrackControl track in this.tracksScrollablePanel.Controls)
			{
				foreach (EventControl ctrl in track.EventControls)
				{
					if (ctrl.IsSelected)
					{
						evts.Add(ctrl);
					}
				}
			}
			return evts;
		}
		#endregion

		#region Private Functions
		private void SetViewWindow(float minDisplay, float maxDisplay)
		{
			if ((minDisplay == this.timeLineComponent.DisplayViewMin) && (maxDisplay == this.timeLineComponent.DisplayViewMax))
			{
				return;
			}

			float newMinT = minDisplay;
			float newMaxT = maxDisplay;
			float width = maxDisplay - minDisplay;

			if (newMinT < this.timeLineComponent.DisplayMin)
			{
				newMinT = this.timeLineComponent.DisplayMin;
				newMaxT = this.timeLineComponent.DisplayMin + width;
			}

			if (newMaxT > this.timeLineComponent.DisplayMax)
			{
				newMaxT = this.timeLineComponent.DisplayMax;
				newMinT = this.timeLineComponent.DisplayMax - width;
			}

			if (newMinT < this.timeLineComponent.DisplayMin)
			{
				newMinT = this.timeLineComponent.DisplayMin;
			}

			if (newMaxT > this.timeLineComponent.DisplayMax)
			{
				newMaxT = this.timeLineComponent.DisplayMax;
			}

			this.hScrollBar.Minimum = 0;
			this.hScrollBar.Maximum = 100000;

			this.hScrollBar.Value = (int)TimeLineComponent.Remap(newMinT,
				this.timeLineComponent.DisplayMin, this.timeLineComponent.DisplayMax,
				this.hScrollBar.Minimum, this.hScrollBar.Maximum);

			int endValue = (int)TimeLineComponent.Remap(newMaxT,
				this.timeLineComponent.DisplayMin, this.timeLineComponent.DisplayMax,
				this.hScrollBar.Minimum, this.hScrollBar.Maximum);

			this.hScrollBar.LargeChange = endValue - this.hScrollBar.Value;

			this.timeLineComponent.SetDisplayView(newMinT, newMaxT);

			this.scrubBarControl.Left = (int)this.timeLineComponent.PhaseToPixel(m_currentPhase);

			ResizeInternal();
		}

		private void ResizeInternal()
		{
			int width = this.tracksScrollablePanel.Width;
			this.timeLineHeaderControl.Width = width;

			foreach (TrackControl trackCtrl in this.tracksScrollablePanel.Controls)
			{
				trackCtrl.Width = width;
			}

			foreach (TrackControl trackCtrl in this.tracksScrollablePanel.Controls)
			{
				foreach (EventControl eventCtrl in trackCtrl.EventControls)
				{
					eventCtrl.Left = (int)this.timeLineComponent.PhaseToPixel(eventCtrl.MinT);
					eventCtrl.Width = (int)this.timeLineComponent.PhaseToPixel(eventCtrl.MaxT) - eventCtrl.Left;
					eventCtrl.Invalidate();
				}

				trackCtrl.Invalidate();
			}

			this.timeLineHeaderControl.Invalidate();
		}

		private void UpdateTrackPosition()
		{
			// move the tracks:
			int top = 0;
			foreach (TrackControl trackCtrl in this.tracksScrollablePanel.Controls)
			{
				if (trackCtrl.Visible)
				{
					trackCtrl.Top = top;
					top += trackCtrl.Height;
				}
			}

			top = 0;
			foreach (TrackNameControl nameCtrl in this.trackNamesScrollablePanel.Controls)
			{
				if (nameCtrl.Visible)
				{
					nameCtrl.Top = top;
					top += nameCtrl.Height;
				}
			}

			m_currentHeight = top;
		}

		private void EnableTrackInternal(int trackIndex, bool enable)
		{
			if ((trackIndex >= 0) && (trackIndex < this.trackNamesScrollablePanel.Controls.Count))
			{
				TrackNameControl nameCtrl = this.trackNamesScrollablePanel.Controls[trackIndex] as TrackNameControl;
				nameCtrl.Visible = enable;

				TrackControl trackCtrl = this.tracksScrollablePanel.Controls[trackIndex] as TrackControl;
				trackCtrl.Visible = enable;
			}
		}
		#endregion
	}

	public class TrackCreateEventArgs : EventArgs
	{
		public TrackCreateEventArgs(string text, string typeName)
		{
			m_text = text;
			m_typeName = typeName;
		}

		#region Variables
		private string m_text;
		private string m_typeName;
		private TrackControl m_trackControl;
		#endregion

		#region Properties
		/// <summary>
		/// The Text of the track to create.
		/// </summary>
		public string Text
		{
			get
			{
				return m_text;
			}
		}

		/// <summary>
		/// The TypeName of the track to create.
		/// </summary>
		public string TypeName
		{
			get
			{
				return m_typeName;
			}
		}

		/// <summary>
		/// Users should call EventEditorControl.CreateTrack(...), set any desired properties on the TrackControl that is returned,
		/// and then set this property.  If TrackControl is null, no track will be added.
		/// </summary>
		public TrackControl TrackControl
		{
			get
			{
				return m_trackControl;
			}
			set
			{
				m_trackControl = value;
			}
		}
		#endregion
	}

	public delegate void TrackCreateEventHandler(object sender, TrackCreateEventArgs e);

	public class EventCreateOnTrackEventArgs : EventCreateEventArgs
	{
		public EventCreateOnTrackEventArgs(TrackControl trackCtrl, float minT, float maxT)
			: base(minT, maxT)
		{
			m_trackControl = trackCtrl;
		}

		#region Variables
		private TrackControl m_trackControl;
		private EventControl m_eventControl = null;
		#endregion

		#region Properties
		/// <summary>
		/// The TrackControl on which the Event is being created.
		/// </summary>
		public TrackControl TrackControl
		{
			get
			{
				return m_trackControl;
			}
		}

		/// <summary>
		/// Users should call EventEditorControl.CreateEvent(...), set any desired properties on the EventControl that is returned,
		/// and then set this property.  If EventControl is null, no event will be added.
		/// </summary>
		public EventControl EventControl
		{
			get
			{
				return m_eventControl;
			}
			set
			{
				m_eventControl = value;
			}
		}
		#endregion
	}

	public delegate void EventCreateOnTrackEventHandler(object sender, EventCreateOnTrackEventArgs e);

	public class EventOnTrackEventArgs : EventControlEventArgs
	{
		public EventOnTrackEventArgs(TrackControl trackCtrl, EventControl eventCtrl)
			: base(eventCtrl)
		{
			m_trackControl = trackCtrl;
		}

		#region Variables
		private TrackControl m_trackControl;
		#endregion

		#region Properties
		/// <summary>
		/// The TrackControl on which the EventControl resides.
		/// </summary>
		public TrackControl TrackControl
		{
			get
			{
				return m_trackControl;
			}
		}
		#endregion
	}

	public delegate void EventOnTrackEventHandler(object sender, EventOnTrackEventArgs e);

	public class EventOnTrackWithCancelEventArgs : EventOnTrackEventArgs
	{
		public EventOnTrackWithCancelEventArgs(TrackControl trackCtrl, EventControl eventCtrl)
			: base(trackCtrl, eventCtrl)
		{

		}

		#region Variables
		private bool m_cancel = false;
		#endregion

		#region Properties
		/// <summary>
		/// Set this to true to cancel the operation.
		/// </summary>
		public bool Cancel
		{
			get
			{
				return m_cancel;
			}
			set
			{
				m_cancel = value;
			}
		}
		#endregion
	}

	public delegate void EventOnTrackWithCancelEventHandler(object sender, EventOnTrackWithCancelEventArgs e);
}
