namespace EventEditorControls
{
    partial class EventControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && (components != null) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.SuspendLayout();
            // 
            // toolTip
            // 
            this.toolTip.AutoPopDelay = 5000;
            this.toolTip.InitialDelay = 0;
            this.toolTip.ReshowDelay = 0;
            // 
            // EventControl
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnablePreventFocusChange;
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Name = "EventControl";
            this.Size = new System.Drawing.Size(150, 28);
            this.VisibleChanged += new System.EventHandler(this.EventControl_VisibleChanged);
            this.MouseLeave += new System.EventHandler(this.EventControl_MouseLeave);
            this.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.EventControl_PreviewKeyDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.EventControl_MouseMove);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.EventControl_KeyUp);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.EventControl_MouseDown);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.EventControl_KeyPress);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.EventControl_MouseUp);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.EventControl_KeyDown);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ToolTip toolTip;
    }
}
