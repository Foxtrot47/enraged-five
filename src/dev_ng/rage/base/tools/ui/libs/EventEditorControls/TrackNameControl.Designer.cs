namespace EventEditorControls
{
    partial class TrackNameControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && (components != null) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.trackNameLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // trackNameLabel
            // 
            this.trackNameLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.trackNameLabel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.trackNameLabel.Location = new System.Drawing.Point( 0, 0 );
            this.trackNameLabel.Name = "trackNameLabel";
            this.trackNameLabel.Size = new System.Drawing.Size( 150, 30 );
            this.trackNameLabel.TabIndex = 0;
            this.trackNameLabel.Text = "label1";
            this.trackNameLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.trackNameLabel.Paint += new System.Windows.Forms.PaintEventHandler( this.trackNameLabel_Paint );
            // 
            // TrackNameControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add( this.trackNameLabel );
            this.Name = "TrackNameControl";
            this.Size = new System.Drawing.Size( 150, 30 );
            this.ResumeLayout( false );

        }

        #endregion

        private System.Windows.Forms.Label trackNameLabel;
    }
}
