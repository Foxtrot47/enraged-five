using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace EventEditorControls
{
    public partial class EventControl : UserControl
    {
        public EventControl()
        {
            InitializeComponent();

            this.Text = "#" + sm_count++;

            SetStyle( ControlStyles.UserPaint | ControlStyles.AllPaintingInWmPaint | ControlStyles.OptimizedDoubleBuffer | ControlStyles.Opaque, true );

            this.GotFocus += new EventHandler( EventControl_GotFocus );
            this.LostFocus += new EventHandler( EventControl_LostFocus );

            m_kbdFrameChangeBuffer = new List<char>();
        }

        #region Constants
        public const int StandardHeight = 30;
        #endregion

        #region Variables
        private static int sm_count = 1;
        private static Color sm_topColor = Color.FromArgb( 255, 64, 64, 64 );
        private static Color sm_bottomColor = Color.FromArgb( 255, 128, 128, 128 );
        private static Color sm_midPointColor = Color.FromArgb( 255, 128, 255, 128 );

        private System.Guid m_guid = Guid.NewGuid();
        private TimeLineComponent m_timeLineComponent = new TimeLineComponent();

        private string m_typeName = string.Empty;
        private float m_minT;
        private float m_mid = -1.0f;
        private float m_maxT;

        private float m_originalMinT;
        private float m_originalMin;
        private float m_originalMaxT;

        private float m_originalWidth = 0; // for maintaining width while dragging
        private float m_mouseDownPos = 0; // to track deltas while dragging
        private bool m_resizingRight = false;
        private bool m_resizingLeft = false;
        private bool m_movingMiddle = false;
        private bool m_moving = false;

        private EventControl m_neighborRight = null;
        private EventControl m_neighborLeft = null;

        private bool m_frameSnapOn = false;
        private bool m_currentPhaseSnapOn = false;
        
        private bool m_eventPhaseScrubKeyDown = false;
        private bool m_eventPhaseScrubOn = false;

        private bool        m_kbdFrameAddActive = false;
        private bool        m_kbdFrameSubActive = false;
        private List<char>  m_kbdFrameChangeBuffer;

        #endregion

        #region Properties
        public Guid Guid
        {
            get
            {
                return m_guid;
            }
            set
            {
                m_guid = value;
            }
        }

        public TimeLineComponent TimeLine
        {
            get
            {
                return m_timeLineComponent;
            }
            set
            {
                m_timeLineComponent = value;
            }
        }

        public string TypeName
        {
            get
            {
                return m_typeName;
            }
            set
            {
                m_typeName = value;

                this.toolTip.SetToolTip( this, String.Format( "{0} ({1}): ({2:0.00}->{3:0.00})",
                    this.Text,
                    this.TypeName,
                    this.TimeLine.PhaseToDisplay( this.MinT ),
                    this.TimeLine.PhaseToDisplay( this.MaxT ) ) );

                if ( this.Visible )
                {
                    Invalidate();
                }
            }
        }

        public float MinT
        {
            get
            {
                return m_minT;
            }
        }

        public float MaxT
        {
            get
            {
                return m_maxT;
            }
        }

        public bool HasMid
        {
            get
            {
                return m_mid >= 0.0f;
            }
        }

        public float MidT
        {
            get
            {
                if ( !this.HasMid )
                {
                    throw new InvalidOperationException( "Can't get MidT when there is no midpoint" );
                }
                
                return TimeLineComponent.Lerp( m_mid, m_minT, m_maxT );
            }
            set
            {
                if ( value != -1.0f )
                {
                    if ( value < m_minT )
                    {
                        throw new ArgumentOutOfRangeException( "value", "value must be >= MinT" );
                    }

                    if ( value > m_maxT )
                    {
                        throw new ArgumentOutOfRangeException( "value", "value must be <= MaxT" );
                    }

                    m_mid = TimeLineComponent.Range( value, m_minT, m_maxT );
                }
                else
                {
                    m_mid = value;
                }
            }
        }

        public float MidLerp
        {
            get
            {
                if ( !this.HasMid )
                {
                    throw new InvalidOperationException( "Can't get MidLerp when there is no midpoint" );
                }
                
                return m_mid;
            }
            set
            {
                if ( (value != 1.0f) && ((value < 0.0f) || (value > 1.0f)) )
                {
                    throw new ArgumentOutOfRangeException( "value", "value must be -1.0f, or between 0.0f and 1.0f" );
                }
                
                m_mid = value;
            }
        }

        public float Range
        {
            get
            {
                return m_maxT - m_minT;
            }
        }

        public EventControl NeighborRight
        {
            set
            {
                m_neighborRight = value;
            }
            get
            {
                return m_neighborRight;
            }
        }

        public EventControl NeighborLeft
        {
            set
            {
                m_neighborLeft = value;
            }
            get
            {
                return m_neighborLeft;
            }
        }

        private bool Changing
        {
            get
            {
                return m_resizingLeft || m_resizingRight || m_moving || m_movingMiddle;
            }
        }

        public override string Text
        {
            get
            {
                return base.Text;
            }
            set
            {
                base.Text = value;

                this.toolTip.SetToolTip( this, String.Format( "{0} ({1}): ({2:0.00}->{3:0.00})",
                    this.Text,
                    this.TypeName,
                    this.TimeLine.PhaseToDisplay( this.MinT ),
                    this.TimeLine.PhaseToDisplay( this.MaxT ) ) );

                if ( this.Visible )
                {
                    Invalidate();
                }
            }
        }

        public bool IsSelected
        {
            get
            {
                return this.Focused;
            }
        }

        public bool IsPhaseScrubOn
        {
            get
            {
                return this.m_eventPhaseScrubOn;
            }
        }

        #endregion

        #region Events
        public event EventHandler Moving;
        public event EventHandler Moved;
        public event EventHandler Delete;
        public event EventHandler Edit;
        #endregion

        #region Event Dispatchers
        protected void OnMoving()
        {
            if ( this.Moving != null )
            {
                this.Moving( this, EventArgs.Empty );
            }
        }

        protected void OnMoved()
        {
            if ( this.Moved != null )
            {
                this.Moved( this, EventArgs.Empty );
            }
        }

        protected void OnDelete()
        {
            if ( this.Delete != null )
            {
                this.Delete( this, EventArgs.Empty );
            }
        }

        protected void OnEdit()
        {
            if ( this.Edit != null )
            {
                this.Edit( this, EventArgs.Empty );
            }
        }
        #endregion

        #region Event Handlers
        void EventControl_LostFocus( object sender, EventArgs e )
        {
            this.Invalidate();
        }

        void EventControl_GotFocus( object sender, EventArgs e )
        {
            this.Invalidate();
        }

        private void EventControl_KeyDown( object sender, KeyEventArgs e )
        {
            if ( (e.KeyCode == Keys.Back) || (e.KeyCode == Keys.Delete) )
            {
                OnDelete();
            }
            else if ( e.KeyCode == Keys.Left )
            {
                OnMoving();
                // TODO: Don't hardcode in 10 pixel movements?
                //	UpdateLeftPosition(m_ResizePosX-10,ref m_ResizePosX);
            }
            else if ( e.KeyCode == Keys.Right )
            {
                OnMoving();
                // TODO: Don't hardcode in 10 pixel movements?
                //	UpdateLeftPosition(m_ResizePosX+10,ref m_ResizePosX);
            }
            else if (e.KeyCode == Keys.F)
            {
                m_frameSnapOn = true;
            }
            else if (e.KeyCode == Keys.C)
            {
                m_currentPhaseSnapOn = true;
            }
            else if (e.KeyCode == Keys.S)
            {
                m_eventPhaseScrubKeyDown = true;
            }
        }

        private void EventControl_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F)
            {
                m_frameSnapOn = false;
            }
            else if (e.KeyCode == Keys.C)
            {
                m_currentPhaseSnapOn = false;
            }
            else if (e.KeyCode == Keys.S)
            {
                m_eventPhaseScrubKeyDown = false;
                m_eventPhaseScrubOn = false;
            }
            else if (e.KeyCode == Keys.Add || e.KeyCode == Keys.Oemplus)
            {
                m_kbdFrameAddActive = true;

                //Make sure our key buffer is cleared out
                m_kbdFrameChangeBuffer.Clear();

                Invalidate();
            }
            else if (e.KeyCode == Keys.Subtract || e.KeyCode == Keys.OemMinus )
            {
                m_kbdFrameSubActive = true;

                //Make sure our key buffer is cleared out
                m_kbdFrameChangeBuffer.Clear();

                Invalidate();
            }
            else if (e.KeyCode == Keys.Return)
            {
                //Clear out the newline / linefeed characters at the end of the buffer
                char[] trimChars = { '\n', '\r', ' ' };
                String bufferString = new String(m_kbdFrameChangeBuffer.ToArray());
                bufferString = bufferString.TrimEnd(trimChars);
                
                if(bufferString.Length > 0)
                {
                    float displayDelta = 0.0f;
                    try
                    {
                        //Convert the input buffer to a float
                        displayDelta = (float)System.Double.Parse(bufferString);
                    }
                    catch (Exception)
                    {
                        //Something went wrong with out keyboard input stream.. the
                        //displayDelta will be left at 0.0 and no change will be made to
                        //the event times
                        System.Diagnostics.Debug.Print("Failed to parse : '{0}'", bufferString);
                    }

                    if (displayDelta > 0.0f)
                    {
                        if (m_kbdFrameSubActive)
                        {
                            displayDelta *= -1.0f;
                        }

                        //Get the absolute min and max phase values we can set for the event
                        float minPhase = this.TimeLine.PhaseMin;
                        if (this.NeighborLeft != null)
                        {
                            minPhase = this.NeighborLeft.MaxT;
                        }

                        float maxPhase = this.TimeLine.PhaseMax;
                        if (this.NeighborRight != null)
                        {
                            maxPhase = this.NeighborRight.MinT;
                        }

                        //Update the event's minT/maxT values
                        float phaseDelta = this.TimeLine.DisplayToPhase(displayDelta);
                        float newMinT = this.MinT;
                        float newMaxT = this.MaxT;

                        newMinT = Math.Max((this.MinT + phaseDelta), minPhase);
                        newMaxT = newMinT + m_originalWidth;
                        newMaxT = Math.Min(newMaxT, maxPhase);
                        newMinT = newMaxT - m_originalWidth;

                        SetMinMax(newMinT, newMaxT);

                        //Let everyone else know we moved
                        OnMoved();

                    }//End if (displayDelta > 0.0f)
                } // End if(bufferString.Length > 0)

                //Reset the state of our keyboard input 
                m_kbdFrameAddActive = false;
                m_kbdFrameSubActive = false;
                m_kbdFrameChangeBuffer.Clear();

                //Force a redraw of the control
                Invalidate();
            }
        }

        private void EventControl_KeyPress(object sender, KeyPressEventArgs e)
        {
            //If we are in the keyboard based display delta entry, buffer the input
            if (m_kbdFrameAddActive || m_kbdFrameSubActive)
            {
                if (Char.IsDigit(e.KeyChar) || (e.KeyChar == '.') )
                {
                    m_kbdFrameChangeBuffer.Add(e.KeyChar);
                }
            }
        }

        private void EventControl_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            //We have to check for the escape key-press in the preview event, since the esc keypress
            //message will not be routed to the control because it causes the control to lose focus
            if (e.KeyCode == Keys.Escape)
            {
                if (m_kbdFrameAddActive || m_kbdFrameSubActive)
                {
                    //Canceling the hot-key frame change mode
                    m_kbdFrameAddActive = false;
                    m_kbdFrameSubActive = false;
                    m_kbdFrameChangeBuffer.Clear();

                    Invalidate();
                }
            }
        }

        private void EventControl_MouseDown( object sender, MouseEventArgs e )
        {
            // display the context menu to edit the instance:
            if ( e.Button == MouseButtons.Middle )
            {
                OnEdit();
            }
            // or we move the instance:
            else if ( e.Button == MouseButtons.Left )
            {
                m_originalWidth = this.Range;

                if (m_frameSnapOn)
                {
                    float snappedDownPos = this.TimeLine.PixelToDisplay(this.Left + e.X);
                    snappedDownPos = (float)Math.Round(snappedDownPos, 0, MidpointRounding.ToEven);
                    m_mouseDownPos = this.TimeLine.DisplayToPhase(snappedDownPos);
                }
                else
                {
                    m_mouseDownPos = this.TimeLine.PixelToPhase(this.Left + e.X);
                }

                if ( CanResizeRight( e.X ) )
                {
                    m_resizingRight = true;
                }
                else if ( CanResizeLeft( e.X ) )
                {
                    m_resizingLeft = true;
                }
                else if ( CanMoveMiddle( e.X ) )
                {
                    m_movingMiddle = true;
                }
                else
                {
                    m_moving = true;
                    this.Cursor = Cursors.SizeWE;
                }

                m_originalMinT = m_minT;
                m_originalMin = m_mid;
                m_originalMaxT = m_maxT;

                this.toolTip.ShowAlways = true;
            }
        }

        private void EventControl_MouseLeave( object sender, EventArgs e )
        {
            if ( (m_resizingLeft || m_resizingRight || m_movingMiddle || m_moving)
                && ((m_originalMinT != m_minT) || (m_originalMin != m_mid) || (m_originalMaxT != m_maxT)) )
            {
                OnMoved();
            }

            m_resizingLeft = false;
            m_resizingRight = false;
            m_movingMiddle = false;
            m_moving = false;

            this.Cursor = Cursors.Default;

            this.toolTip.ShowAlways = false;
        }

        private void EventControl_MouseMove( object sender, MouseEventArgs e )
        {
            float prevMin = this.MinT;
            float prevMax = this.MaxT;

            float minPhase = this.TimeLine.PhaseMin;
            if ( this.NeighborLeft != null )
            {
                minPhase = this.NeighborLeft.MaxT;
            }

            float maxPhase = this.TimeLine.PhaseMax;
            if ( this.NeighborRight != null )
            {
                maxPhase = this.NeighborRight.MinT;
            }

            float newMinT = prevMin;
            float newMaxT = prevMax;

            //Get the phase of the current pixel.  Note: e.X is in control-space, so
            //we need to add in the offset of the left border of to the event control
            //to get the correct pixel location to calculate the phase from (parent control space).
            float newPhase = this.TimeLine.PixelToPhase(this.Left + e.X);

            if (m_frameSnapOn)
            {
                float frameSnappedDisplay = (float)Math.Round(this.TimeLine.PhaseToDisplay(newPhase), 0, MidpointRounding.ToEven);
                newPhase = this.TimeLine.DisplayToPhase(frameSnappedDisplay);
            }
            else if (m_currentPhaseSnapOn)
            {
                EventEditorControl evtEditorCtrl = GetParentEventEditor();
                if (evtEditorCtrl != null)
                {
                    float curPhase = evtEditorCtrl.CurrentPhase;

                    //If the phase is near the current phase then snap to it
                    if (Math.Abs(newPhase - curPhase) < 0.05f)
                    {
                        newPhase = curPhase;
                    }
                }
            }
            else if (m_eventPhaseScrubKeyDown)
            {
                EventEditorControl evtEditorCtrl = GetParentEventEditor();
                if (evtEditorCtrl != null)
                {
                    float curPhase = evtEditorCtrl.CurrentPhase;

                    //If the phase is near the current phase then snap to it
                    if (Math.Abs(this.MinT - curPhase) < 0.1f)
                    {
                        m_eventPhaseScrubOn = true;
                    }
                    else
                    {
                        m_eventPhaseScrubOn = false;
                    }
                }
            }

            float moveDelta = newPhase - m_mouseDownPos;            

            if ( m_moving )
            {
                float interpValue = 1.0f - ((prevMax - m_mouseDownPos) / m_originalWidth);

                newMinT = Math.Max(newPhase - (m_originalWidth * interpValue), minPhase);
                newMaxT = newMinT + m_originalWidth;
                newMaxT = Math.Min(newMaxT, maxPhase);
                newMinT = newMaxT - m_originalWidth;

                this.Cursor = Cursors.SizeWE;

                m_mouseDownPos = newPhase;
            }
            else if ( m_resizingRight )
            {
                newMaxT = Math.Min(maxPhase, newPhase);
                newMaxT = Math.Max(newMaxT, prevMin);

                // ensure we are at least 8 pixels wide so that we are visible
                float minimumPixelPhase = this.TimeLine.PixelToPhase( this.TimeLine.PhaseToPixel( this.MinT ) + 8.0f );
                newMaxT = Math.Max( newMaxT, minimumPixelPhase );
 
                // have to update the mousedown pos to prevent feedback
                m_mouseDownPos = newPhase;
            }
            else if ( m_resizingLeft )
            {
                newMinT = Math.Max(minPhase, newPhase);
                newMinT = Math.Min(newMinT, prevMax);

                // ensure we are at least 8 pixels wide so that we are visible
                float minimumPixelPhase = this.TimeLine.PixelToPhase( this.TimeLine.PhaseToPixel( this.MaxT ) - 8.0f );
                newMinT = Math.Min( newMinT, minimumPixelPhase );

                // have to update the mousedown pos to prevent feedback
                m_mouseDownPos = newPhase;
            }
            else if ( m_movingMiddle )
            {
                float newMidT = this.MidT + moveDelta;
                newMidT = Math.Max( newMidT, prevMin );
                newMidT = Math.Min( newMidT, prevMax );
                newMidT = Math.Max( newMidT, this.MinT );
                newMidT = Math.Min( newMidT, this.MaxT );
                this.MidT = newMidT;

                // have to update the mousedown pos to prevent feedback
                m_mouseDownPos = newPhase;

                // this is a hack to get around the fact that ScrollablePanel doesn't work 
                // in the case where you're moving something in the panel and that causes 
                // the scroll to occur.  We let clients know that the event bar is moving,
                // and they check the current auto scroll position of the event panel:
                OnMoving();
            }
            else if ( CanResizeLeft( e.X ) || CanResizeRight( e.X ) || CanMoveMiddle( e.X ) )
            {
                this.Cursor = Cursors.VSplit;
            }
            else
            {
                this.Cursor = Cursors.SizeWE;
            }

            // trigger that we are moving
            if ( (newMinT != prevMin) || (newMaxT != prevMax) )
            {
                SetMinMax( newMinT, newMaxT );
                
                Invalidate();

                // this is a hack to get around the fact that ScrollablePanel doesn't work 
                // in the case where you're moving something in the panel and that causes 
                // the scroll to occur.  We let clients know that the event bar is moving,
                // and they check the current auto scroll position of the event panel:
                OnMoving();
            }
        }

        private void EventControl_MouseUp( object sender, MouseEventArgs e )
        {
            if ( (m_resizingLeft || m_resizingRight || m_movingMiddle || m_moving)
            && ((m_originalMinT != m_minT) || (m_originalMin != m_mid) || (m_originalMaxT != m_maxT)) )
            {
                OnMoved();
            }

            m_resizingLeft = false;
            m_resizingRight = false;
            m_movingMiddle = false;
            m_moving = false;

            this.toolTip.ShowAlways = false;
        }

        private void EventControl_VisibleChanged( object sender, EventArgs e )
        {
            if ( (m_resizingLeft || m_resizingRight || m_movingMiddle || m_moving)
                && ((m_originalMinT != m_minT) || (m_originalMin != m_mid) || (m_originalMaxT != m_maxT)) )
            {
                OnMoved();
            }

            m_resizingLeft = false;
            m_resizingRight = false;
            m_movingMiddle = false;
            m_moving = false;

            this.Enabled = this.Visible;
            this.toolTip.ShowAlways = false;
        }
        #endregion

        #region Overrides
        protected override bool IsInputKey( Keys keyData )
        {
            if ( (keyData == Keys.Left) || (keyData == Keys.Right) )
            {
                return true;
            }

            return base.IsInputKey( keyData );
        }

        protected override void OnPaint( PaintEventArgs e )
        {
            Rectangle outerRect 
                = new Rectangle( this.ClientRectangle.X, this.ClientRectangle.Y, this.ClientRectangle.Width - 1, this.ClientRectangle.Height - 1 );

            if ( outerRect.Width == 0 )
            {
                outerRect.Width = 1;
            }

            using ( GraphicsPath outerFillPath = new GraphicsPath() )
            {
                outerFillPath.AddRectangle( outerRect );
                
                using ( LinearGradientBrush outerBrush 
                    = new LinearGradientBrush( outerRect, SystemColors.Control, SystemColors.ControlDark, LinearGradientMode.Vertical ) )
                {
                    e.Graphics.FillPath( outerBrush, outerFillPath );
                }
            }

            using ( GraphicsPath outerOutlinePath = new GraphicsPath() )
            {
                if ( this.Focused )
                {
                    outerOutlinePath.AddRectangle( new Rectangle( outerRect.Left + 1, outerRect.Top + 1, outerRect.Width - 1, outerRect.Height - 1 ) );
                }
                else
                {
                    outerOutlinePath.AddRectangle( outerRect );
                }

                using ( Pen outlinePen = new Pen( 
                                            this.Focused ?
                                            ((this.m_kbdFrameAddActive || this.m_kbdFrameSubActive) ? SystemColors.HighlightText : SystemColors.Highlight) 
                                                : SystemColors.ControlDark, 
                                            this.Focused ? 2.0f : 1.0f ) )
                {
                    e.Graphics.DrawPath( outlinePen, outerOutlinePath );
                }
            }

            // Draw the midpoint
            if ( this.HasMid )
            {
                int midX = (int)this.TimeLine.PhaseToPixel( this.MidT ) - this.Left;

                int bloomWidth = 6;
                Rectangle bloom = new Rectangle( midX - bloomWidth, this.ClientRectangle.Y + 1, bloomWidth, this.ClientRectangle.Height - 2 );
                
                using ( LinearGradientBrush brush 
                    = new LinearGradientBrush( bloom, Color.Transparent, 
                    Color.FromArgb( 60, SystemColors.ControlLight ), LinearGradientMode.Horizontal ) )
                {
                    e.Graphics.FillRectangle( brush, bloom );
                }

                bloom.Offset( bloomWidth, 0 );
                
                using ( LinearGradientBrush brush
                    = new LinearGradientBrush( bloom, Color.FromArgb( 60, SystemColors.ControlLight ), 
                    Color.Transparent, LinearGradientMode.Horizontal ) )
                {
                    e.Graphics.FillRectangle( brush, bloom );
                }

                e.Graphics.DrawLine(
                    new Pen( SystemColors.ControlLight ), 
                    midX, this.ClientRectangle.Y + 2, midX, this.ClientRectangle.Y + this.ClientRectangle.Height - 3 );
            }

            if ( this.Text.Length > 0 )
            {
                // Paint the text
                if ( this.TypeName.Length > 0 )
                {
                    TextRenderer.DrawText( e.Graphics, String.Format( "{0} ({1})", this.Text, this.TypeName ), this.Font,
                        this.ClientRectangle, SystemColors.ControlText, Color.Transparent,
                        TextFormatFlags.HorizontalCenter | TextFormatFlags.VerticalCenter | TextFormatFlags.EndEllipsis );
                }
                else
                {
                    TextRenderer.DrawText( e.Graphics, this.Text, this.Font,
                        this.ClientRectangle, SystemColors.ControlText, Color.Transparent,
                        TextFormatFlags.HorizontalCenter | TextFormatFlags.VerticalCenter | TextFormatFlags.EndEllipsis );
                }
            }
        }
        #endregion

        #region Public Functions
        public void SetMinMax( float minT, float maxT )
        {
            if ( minT > maxT )
            {
                throw new ArgumentOutOfRangeException( "minT must be <= maxT" );
            }

            m_minT = minT;
            m_maxT = maxT;

            UpdatePosition();
            Invalidate();
        }

        public void UpdatePosition()
        {
            this.Left = (int)this.TimeLine.PhaseToPixel( this.MinT );
            this.Width = (int)(this.TimeLine.PhaseToPixel( this.MaxT ) - this.TimeLine.PhaseToPixel( this.MinT ));

            this.toolTip.SetToolTip( this, String.Format( "{0} ({1}): ({2:0.00}->{3:0.00})",
                this.Text,
                this.TypeName,
                this.TimeLine.PhaseToDisplay( this.MinT ),
                this.TimeLine.PhaseToDisplay( this.MaxT ) ) );
        }
        #endregion

        #region Private Functions
        private bool CanResizeRight( int x )
        {
            return ((x >= this.Width - 1 - 3) && (x <= this.Width - 1)) && !this.Changing;
        }

        private bool CanResizeLeft( int x )
        {
            return ((x >= 0) && (x <= 3)) && !this.Changing;
        }

        private bool CanMoveMiddle( int x )
        {
            if ( !this.HasMid )
            {
                return false;
            }

            int midX = (int)this.TimeLine.PhaseToPixel( this.MidT ) - this.Left;
            return ((x >= midX - 2) && (x <= midX + 2));
        }

        private EventEditorControl GetParentEventEditor()
        {
            Type evtEditorCtrlType = typeof(EventEditorControl);
            Control target = this.Parent;
            while ((target != null) && (target.GetType() != evtEditorCtrlType))
            {
                target = target.Parent;
            }

            if (target != null)
            {
                return (EventEditorControl)target;
            }
            else
            {
                return null;
            }
        }
        #endregion
    }
}
