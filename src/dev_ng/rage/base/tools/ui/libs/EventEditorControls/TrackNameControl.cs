using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace EventEditorControls
{
    public partial class TrackNameControl : UserControl
    {
        public TrackNameControl()
        {
            InitializeComponent();
        }

        #region Properties
        private string m_typeName;
        #endregion

        #region Properties
        public string TypeName
        {
            get
            {
                return m_typeName;
            }
            set
            {
                m_typeName = value;
            }
        }

        public override string Text
        {
            get
            {
                return this.trackNameLabel.Text;
            }
            set
            {
                this.trackNameLabel.Text = value;
            }
        }
        #endregion

        #region Event Handlers
        private void trackNameLabel_Paint( object sender, PaintEventArgs e )
        {
            e.Graphics.DrawLine( new System.Drawing.Pen( Color.DarkGray ), new Point( 0, this.Height - 2 ), new Point( this.Width, this.Height - 2 ) );
            e.Graphics.DrawLine( new System.Drawing.Pen( Color.White ), new Point( 0, this.Height - 1 ), new Point( this.Width, this.Height - 1 ) );
        }
        #endregion
    }
}
