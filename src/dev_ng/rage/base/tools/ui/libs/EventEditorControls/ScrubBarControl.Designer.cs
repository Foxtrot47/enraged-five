namespace EventEditorControls
{
    partial class ScrubBarControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && (components != null) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // ScrubBarControl
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.BackColor = System.Drawing.Color.Red;
            this.CausesValidation = false;
            this.ForeColor = System.Drawing.Color.Red;
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Name = "ScrubBarControl";
            this.Size = new System.Drawing.Size( 1, 384 );
            this.ResumeLayout( false );

        }

        #endregion
    }
}
