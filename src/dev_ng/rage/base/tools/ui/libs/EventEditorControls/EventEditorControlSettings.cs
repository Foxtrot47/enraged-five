using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Text;
using System.Xml.Serialization;

using RSG.Base.Serialization;

namespace EventEditorControls
{
    /// <summary>
    /// Holds the per-user settings data for a EventEditorControl.
    /// </summary>
    public class EventEditorControlSettings : rageSerializableObject
    {
        public EventEditorControlSettings()
        {

        }

        #region Variables
        private rageSerializableStringCollection m_disabledTrackCollection = new rageSerializableStringCollection();
        #endregion

        #region Properties
        public rageSerializableStringCollection DisabledTrackCollection
        {
            get
            {
                return m_disabledTrackCollection;
            }
            set
            {
                if ( value != null )
                {
                    m_disabledTrackCollection = value;
                }
                else
                {
                    m_disabledTrackCollection.Clear();
                }
            }
        }
        #endregion

        #region Overrides
        public override bool Equals( object obj )
        {
            if ( obj == null )
            {
                return false;
            }

            if ( base.Equals( obj ) )
            {
                return true;
            }

            if ( obj is EventEditorControlSettings )
            {
                return Equals( obj as EventEditorControlSettings );
            }

            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override object Clone()
        {
            EventEditorControlSettings settings = new EventEditorControlSettings();
            settings.CopyFrom( this );
            return settings;
        }

        public override void CopyFrom( IRageClonableObject other )
        {
            if ( other == null )
            {
                return;
            }

            if ( base.Equals( (object)other ) )
            {
                return;
            }

            if ( other is EventEditorControlSettings )
            {
                CopyFrom( other as EventEditorControlSettings );
            }
        }

        public override void Reset()
        {
            base.Reset();

            this.DisabledTrackCollection.Reset();
        }
        #endregion

        #region Public Functions
        public void CopyFrom( EventEditorControlSettings other )
        {
            if ( other == null )
            {
                return;
            }

            if ( base.Equals( (object)other ) )
            {
                return;
            }

            base.CopyFrom( other as rageSerializableObject );

            this.DisabledTrackCollection.CopyFrom( other.DisabledTrackCollection );
        }

        public bool Equals( EventEditorControlSettings other )
        {
            if ( other == null )
            {
                return false;
            }

            if ( base.Equals( other as rageSerializableObject ) )
            {
                return true;
            }

            return this.DisabledTrackCollection.Equals( other.DisabledTrackCollection );
        }
        #endregion
    }
}
