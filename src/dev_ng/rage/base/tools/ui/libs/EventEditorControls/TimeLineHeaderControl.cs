using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace EventEditorControls
{
    public partial class TimeLineHeaderControl : UserControl
    {
        public TimeLineHeaderControl()
        {
            InitializeComponent();

            SetStyle( ControlStyles.ResizeRedraw, true );
            SetStyle( ControlStyles.AllPaintingInWmPaint, true );
            SetStyle( ControlStyles.UserPaint, true );
            SetStyle( ControlStyles.DoubleBuffer, true );
            SetStyle( ControlStyles.SupportsTransparentBackColor, true );
            SetStyle( ControlStyles.ContainerControl, true );
        }

        #region Variables
        private TimeLineComponent m_timeLineComponent = new TimeLineComponent();
        #endregion

        #region Properties
        public TimeLineComponent TimeLine
        {
            get
            {
                return m_timeLineComponent;
            }
            set
            {
                m_timeLineComponent = value;

                if ( m_timeLineComponent != null )
                {
                    m_timeLineComponent.Header = this;
                }
            }
        }
        #endregion

        #region Event Handlers
        private void TimeLineHeaderControl_Paint( object sender, PaintEventArgs e )
        {
            // Call the OnPaint method of the base class.
            //base.OnPaint(pe);

            Rectangle rc = new Rectangle( 0, 0, this.Width, this.Height );
            LinearGradientBrush b = new LinearGradientBrush( rc, Color.FromArgb( 192, 192, 192 ), Color.White, LinearGradientMode.Vertical );

            // Draw the background
            e.Graphics.FillRectangle( b, rc );

            // Draw the tick marks
            float range = this.TimeLine.DisplayViewRange;
            Pen tickPen = new Pen( Color.FromArgb( 90, this.ForeColor ) );

            float tickInterval = this.TimeLine.TickInterval;
            float startTick = TimeLine.GetIntervalViewStart( tickInterval );
            float endTick = TimeLine.GetIntervalViewEnd( tickInterval );

            float tick = startTick;
            while ( tick <= endTick )
            {
                int xPos = (int)this.TimeLine.DisplayToPixel( tick );
                e.Graphics.DrawLine( tickPen, new Point( xPos, this.Height - 7 ), new Point( xPos, this.Height ) );
                tick += tickInterval;
            }

            // Now draw the labels
            float labelInterval = this.TimeLine.LabelInterval;

            // pick a good format string for the interval
            string formatString = "{0:0.#}";
            if ( labelInterval < 0.01f )
            {
                formatString = "{0:0.000}";
            }
            else if ( labelInterval < 0.1f )
            {
                formatString = "{0:0.00}";
            }

            // so now we know we want to draw labels every 'interval' units
            // (we could recompute this just when the size or interval changes too)

            float startLabel = this.TimeLine.GetIntervalViewStart( labelInterval );
            float endLabel = this.TimeLine.GetIntervalViewEnd( labelInterval );

            SolidBrush fontBrush = new SolidBrush( this.ForeColor );
            Pen majorTickPen = new Pen( this.ForeColor );

            float label = startLabel;
            while ( label <= endLabel )
            {
                int xPos = (int)this.TimeLine.DisplayToPixel( label );

                string numberStr = String.Format( formatString, label );

                Size size = e.Graphics.MeasureString( numberStr, this.Font ).ToSize();
                e.Graphics.DrawString( numberStr, this.Font, fontBrush, xPos - (size.Width / 2), this.Height - 10 - size.Height );
                e.Graphics.DrawLine( majorTickPen, new Point( xPos, this.Height - 10 ), new Point( xPos, this.Height ) );
                label += labelInterval;
            }
        }
        #endregion
    }
}
