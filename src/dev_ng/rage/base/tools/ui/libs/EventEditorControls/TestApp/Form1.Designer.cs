namespace TestApp
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && (components != null) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.eventEditorControl1 = new EventEditorControls.EventEditorControl();
            this.SuspendLayout();
            // 
            // eventEditorControl1
            // 
            this.eventEditorControl1.CurrentPhase = 0F;
            this.eventEditorControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.eventEditorControl1.Location = new System.Drawing.Point( 0, 0 );
            this.eventEditorControl1.Name = "eventEditorControl1";
            this.eventEditorControl1.Size = new System.Drawing.Size( 1126, 651 );
            this.eventEditorControl1.TabIndex = 0;
            this.eventEditorControl1.EventCreate += new EventEditorControls.EventCreateOnTrackEventHandler( this.eventEditorControl1_EventCreate );
            this.eventEditorControl1.TrackCreate += new EventEditorControls.TrackCreateEventHandler( this.eventEditorControl1_TrackCreate );
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size( 1126, 651 );
            this.Controls.Add( this.eventEditorControl1 );
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout( false );

        }

        #endregion

        private EventEditorControls.EventEditorControl eventEditorControl1;
    }
}

