namespace EventEditorControls
{
    partial class EventEditorControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && (components != null) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.trackNamesScrollablePanel = new EventEditorControls.ScrollablePanel();
            this.createButton = new System.Windows.Forms.Button();
            this.filterButton = new System.Windows.Forms.Button();
            this.scrubBarControl = new EventEditorControls.ScrubBarControl();
            this.tracksScrollablePanel = new EventEditorControls.ScrollablePanel();
            this.timeLineHeaderControl = new EventEditorControls.TimeLineHeaderControl();
            this.timeLineComponent = new EventEditorControls.TimeLineComponent( this.components );
            this.hScrollBar = new System.Windows.Forms.HScrollBar();
            this.zoomHScrollBar = new System.Windows.Forms.HScrollBar();
            this.filterContextMenuStrip = new System.Windows.Forms.ContextMenuStrip( this.components );
            this.createContextMenuStrip = new System.Windows.Forms.ContextMenuStrip( this.components );
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.Location = new System.Drawing.Point( 0, 0 );
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add( this.trackNamesScrollablePanel );
            this.splitContainer1.Panel1.Controls.Add( this.createButton );
            this.splitContainer1.Panel1.Controls.Add( this.filterButton );
            this.splitContainer1.Panel1MinSize = 100;
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add( this.scrubBarControl );
            this.splitContainer1.Panel2.Controls.Add( this.tracksScrollablePanel );
            this.splitContainer1.Panel2.Controls.Add( this.timeLineHeaderControl );
            this.splitContainer1.Panel2.Controls.Add( this.hScrollBar );
            this.splitContainer1.Panel2.Controls.Add( this.zoomHScrollBar );
            this.splitContainer1.Size = new System.Drawing.Size( 1032, 608 );
            this.splitContainer1.SplitterDistance = 139;
            this.splitContainer1.TabIndex = 0;
            // 
            // trackNamesScrollablePanel
            // 
            this.trackNamesScrollablePanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.trackNamesScrollablePanel.AutoScroll = true;
            this.trackNamesScrollablePanel.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.trackNamesScrollablePanel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.trackNamesScrollablePanel.Location = new System.Drawing.Point( 0, 44 );
            this.trackNamesScrollablePanel.Name = "trackNamesScrollablePanel";
            this.trackNamesScrollablePanel.Size = new System.Drawing.Size( 138, 542 );
            this.trackNamesScrollablePanel.TabIndex = 4;
            this.trackNamesScrollablePanel.VisibleAutoScrollHorizontal = false;
            this.trackNamesScrollablePanel.VisibleAutoScrollVertical = false;
            this.trackNamesScrollablePanel.ScrollMouseWheel += new System.Windows.Forms.MouseEventHandler( this.trackNamesScrollablePanel_ScrollMouseWheel );
            this.trackNamesScrollablePanel.ScrollVertical += new System.Windows.Forms.ScrollEventHandler( this.trackNamesScrollablePanel_ScrollVertical );
            // 
            // createButton
            // 
            this.createButton.Location = new System.Drawing.Point( 72, 13 );
            this.createButton.Name = "createButton";
            this.createButton.Size = new System.Drawing.Size( 56, 24 );
            this.createButton.TabIndex = 3;
            this.createButton.Text = "Create...";
            this.createButton.UseVisualStyleBackColor = true;
            this.createButton.Click += new System.EventHandler( this.createButton_Click );
            // 
            // filterButton
            // 
            this.filterButton.Location = new System.Drawing.Point( 8, 13 );
            this.filterButton.Name = "filterButton";
            this.filterButton.Size = new System.Drawing.Size( 56, 24 );
            this.filterButton.TabIndex = 2;
            this.filterButton.Text = "Filter...";
            this.filterButton.UseVisualStyleBackColor = true;
            this.filterButton.Click += new System.EventHandler( this.filterButton_Click );
            // 
            // scrubBarControl
            // 
            this.scrubBarControl.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.scrubBarControl.BackColor = System.Drawing.Color.Red;
            this.scrubBarControl.CausesValidation = false;
            this.scrubBarControl.ForeColor = System.Drawing.Color.Red;
            this.scrubBarControl.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.scrubBarControl.Location = new System.Drawing.Point( 0, 0 );
            this.scrubBarControl.Name = "scrubBarControl";
            this.scrubBarControl.Size = new System.Drawing.Size( 1, 586 );
            this.scrubBarControl.TabIndex = 4;
            // 
            // tracksScrollablePanel
            // 
            this.tracksScrollablePanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tracksScrollablePanel.AutoScroll = true;
            this.tracksScrollablePanel.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.tracksScrollablePanel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.tracksScrollablePanel.Location = new System.Drawing.Point( 0, 44 );
            this.tracksScrollablePanel.Name = "tracksScrollablePanel";
            this.tracksScrollablePanel.Size = new System.Drawing.Size( 886, 541 );
            this.tracksScrollablePanel.TabIndex = 3;
            this.tracksScrollablePanel.VisibleAutoScrollHorizontal = false;
            this.tracksScrollablePanel.VisibleAutoScrollVertical = false;
            this.tracksScrollablePanel.ScrollMouseWheel += new System.Windows.Forms.MouseEventHandler( this.tracksScrollablePanel_ScrollMouseWheel );
            this.tracksScrollablePanel.ScrollVertical += new System.Windows.Forms.ScrollEventHandler( this.tracksScrollablePanel_ScrollVertical );
            // 
            // timeLineHeaderControl
            // 
            this.timeLineHeaderControl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.timeLineHeaderControl.Location = new System.Drawing.Point( 0, 0 );
            this.timeLineHeaderControl.Name = "timeLineHeaderControl";
            this.timeLineHeaderControl.Size = new System.Drawing.Size( 886, 40 );
            this.timeLineHeaderControl.TabIndex = 2;
            this.timeLineHeaderControl.TimeLine = this.timeLineComponent;
            // 
            // timeLineComponent
            // 
            this.timeLineComponent.DisplayMax = 100F;
            this.timeLineComponent.DisplayMin = 0F;
            this.timeLineComponent.DisplayViewMax = 100F;
            this.timeLineComponent.DisplayViewMin = 0F;
            this.timeLineComponent.PhaseViewMax = 1F;
            this.timeLineComponent.PhaseViewMin = 0F;
            // 
            // hScrollBar
            // 
            this.hScrollBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.hScrollBar.LargeChange = 100;
            this.hScrollBar.Location = new System.Drawing.Point( 115, 589 );
            this.hScrollBar.Name = "hScrollBar";
            this.hScrollBar.Size = new System.Drawing.Size( 774, 19 );
            this.hScrollBar.TabIndex = 1;
            this.hScrollBar.Scroll += new System.Windows.Forms.ScrollEventHandler( this.hScrollBar_Scroll );
            // 
            // zoomHScrollBar
            // 
            this.zoomHScrollBar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.zoomHScrollBar.Location = new System.Drawing.Point( 0, 589 );
            this.zoomHScrollBar.Name = "zoomHScrollBar";
            this.zoomHScrollBar.Size = new System.Drawing.Size( 115, 19 );
            this.zoomHScrollBar.TabIndex = 0;
            this.zoomHScrollBar.Scroll += new System.Windows.Forms.ScrollEventHandler( this.zoomHScrollBar_Scroll );
            // 
            // filterContextMenuStrip
            // 
            this.filterContextMenuStrip.Name = "filterContextMenuStrip";
            this.filterContextMenuStrip.ShowCheckMargin = true;
            this.filterContextMenuStrip.ShowImageMargin = false;
            this.filterContextMenuStrip.Size = new System.Drawing.Size( 61, 4 );
            // 
            // createContextMenuStrip
            // 
            this.createContextMenuStrip.Name = "createContextMenuStrip";
            this.createContextMenuStrip.ShowImageMargin = false;
            this.createContextMenuStrip.Size = new System.Drawing.Size( 36, 4 );
            // 
            // EventEditorControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add( this.splitContainer1 );
            this.Name = "EventEditorControl";
            this.Size = new System.Drawing.Size( 1032, 608 );
            this.splitContainer1.Panel1.ResumeLayout( false );
            this.splitContainer1.Panel2.ResumeLayout( false );
            this.splitContainer1.ResumeLayout( false );
            this.ResumeLayout( false );

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.HScrollBar hScrollBar;
        private System.Windows.Forms.HScrollBar zoomHScrollBar;
        private TimeLineHeaderControl timeLineHeaderControl;
        private TimeLineComponent timeLineComponent;
        private System.Windows.Forms.Button createButton;
        private System.Windows.Forms.Button filterButton;
        private ScrollablePanel tracksScrollablePanel;
        private System.Windows.Forms.ContextMenuStrip filterContextMenuStrip;
        private System.Windows.Forms.ContextMenuStrip createContextMenuStrip;
        private ScrubBarControl scrubBarControl;
        private ScrollablePanel trackNamesScrollablePanel;
    }
}
