using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace EventEditorControls
{
    public partial class TrackControl : UserControl
    {
        public TrackControl()
        {
            InitializeComponent();
        }

        #region Variables
        private string m_typeName;
        private TimeLineComponent m_timeLineComponent = new TimeLineComponent();
        private int m_trackNumber;
        private int m_MouseX;
        private int m_MouseY;
        #endregion

        #region Properties
        public string TypeName
        {
            get
            {
                return m_typeName;
            }
            set
            {
                m_typeName = value;
            }
        }

        public TimeLineComponent TimeLine
        {
            get
            {
                return m_timeLineComponent;
            }
            set
            {
                m_timeLineComponent = value;
            }
        }

        public List<EventControl> EventControls
        {
            get
            {
                List<EventControl> eventCtrls = new List<EventControl>();
                foreach ( EventControl ctrl in this.Controls )
                {
                    eventCtrls.Add( ctrl );
                }

                return eventCtrls;
            }
        }

        public int TrackNumber
        {
            get
            {
                return m_trackNumber;
            }
            set
            {
                m_trackNumber = value;
            }
        }
        #endregion

        #region Events
        public event EventCreateEventHandler EventCreate;

        public event EventControlEventHandler EventMoving;
        public event EventControlEventHandler EventMoved;
        public event EventControlEventHandler EventDelete;
        public event EventControlEventHandler EventEdit;

        public event EventControlEventHandler SelectionChanged;
        #endregion

        #region Event Dispatchers
        protected void OnEventCreate( EventCreateEventArgs e )
        {
            if ( this.EventCreate != null )
            {
                this.EventCreate( this, e );
            }
        }

        protected void OnEventMoving( EventControlEventArgs e )
        {
            if ( this.EventMoving != null )
            {
                this.EventMoving( this, e );
            }
        }

        protected void OnEventMoved( EventControlEventArgs e )
        {
            if ( this.EventMoved != null )
            {
                this.EventMoved( this, e );
            }
        }

        protected void OnEventDelete( EventControlEventArgs e )
        {
            if ( this.EventDelete != null )
            {
                this.EventDelete( this, e );
            }
        }

        protected void OnEventEdit( EventControlEventArgs e )
        {
            if ( this.EventEdit != null )
            {
                this.EventEdit( this, e );
            }
        }

        protected void OnSelectionChanged( EventControlEventArgs e )
        {
            if (this.SelectionChanged != null)
            {
                this.SelectionChanged(this, e);
            }
        }
        #endregion

        #region Event Handlers
        private void TrackControl_MouseDoubleClick( object sender, MouseEventArgs e )
        {
            if ( e.Button == MouseButtons.Left )
            {
                float minT = this.TimeLine.PixelToPhase( m_MouseX - 10 );
                float maxT = this.TimeLine.PixelToPhase( m_MouseX + 10 );

                OnEventCreate( new EventCreateEventArgs( minT, maxT ) );
            }
        }

        private void TrackControl_MouseMove( object sender, MouseEventArgs e )
        {
            m_MouseX = e.X;
            m_MouseY = e.Y;
        }

        private void TrackControl_Paint( object sender, PaintEventArgs e )
        {
            e.Graphics.DrawLine( new System.Drawing.Pen( Color.DarkGray ), new Point( 0, this.Height - 2 ), new Point( this.Width, this.Height - 2 ) );
            e.Graphics.DrawLine( new System.Drawing.Pen( Color.White ), new Point( 0, this.Height - 1 ), new Point( this.Width, this.Height - 1 ) );
        }

        private void eventControl_Moving( object sender, EventArgs e )
        {
            OnEventMoving( new EventControlEventArgs( sender as EventControl ) );
        }

        private void eventControl_Moved( object sender, EventArgs e )
        {
            OnEventMoved( new EventControlEventArgs( sender as EventControl ) );
        }

        private void eventControl_Delete( object sender, EventArgs e )
        {
            OnEventDelete( new EventControlEventArgs( sender as EventControl ) );
        }

        private void eventControl_Edit( object sender, EventArgs e )
        {
            OnEventEdit( new EventControlEventArgs( sender as EventControl ) );
        }

        private void eventControl_Selected(object sender, EventArgs e)
        {
            OnSelectionChanged(new EventControlEventArgs(sender as EventControl));
        }

        private void eventControl_Deselected(object sender, EventArgs e)
        {
            OnSelectionChanged(new EventControlEventArgs(null));
        }
        #endregion

        #region Overrides
        protected override void OnSizeChanged( EventArgs e )
        {
            base.OnSizeChanged( e );

            // resize each of the event controls
            foreach ( EventControl ctrl in this.EventControls )
            {
                ctrl.UpdatePosition();
                ctrl.Invalidate();
            }
        }
        #endregion

        #region Public Functions
        /// <summary>
        /// Creates a new EventControl with default parameters, but does not add it.
        /// </summary>
        /// <param name="text"></param>
        /// <param name="typeName"></param>
        /// <param name="currentPhase"></param>
        /// <returns></returns>
        public EventControl CreateEvent( string text, string typeName, float currentPhase )
        {
            float pixel = this.TimeLine.PhaseToPixel( currentPhase );
            return CreateEvent( text, typeName,
                this.TimeLine.PixelToPhase( pixel - 10 ), this.TimeLine.PixelToPhase( pixel + 10 ), -1.0f );
        }

        /// <summary>
        /// Creates a new EventControl, but does not add it.
        /// </summary>
        /// <param name="text"></param>
        /// <param name="typeName"></param>
        /// <param name="minT"></param>
        /// <param name="maxT"></param>
        /// <param name="midT"></param>
        /// <returns></returns>
        public EventControl CreateEvent( string text, string typeName, float minT, float maxT, float midT )
        {
            EventControl eventCtrl = new EventControl();
            eventCtrl.TimeLine = this.TimeLine;
            eventCtrl.SetMinMax( minT, maxT );
            eventCtrl.MidT = midT;
            eventCtrl.Text = text;
            eventCtrl.TypeName = typeName;

            return eventCtrl;
        }

        public void AddEvent( EventControl ctrl )
        {
            List<EventControl> eventCtrls = this.EventControls;

            bool inserted = false;
            for ( int i = 0; i < eventCtrls.Count; ++i )
            {
                if ( ctrl.Left < eventCtrls[i].Left )
                {
                    if ( i > 0 )
                    {
                        ctrl.NeighborLeft = eventCtrls[i - 1] as EventControl;
                    }

                    ctrl.NeighborRight = eventCtrls[i] as EventControl;
                    this.Controls.Add( ctrl );
                    this.Controls.SetChildIndex( ctrl, i );
                    ctrl.TabIndex = i;
                    
                    inserted = true;
                    break;
                }
            }

            if ( !inserted )
            {
                if ( eventCtrls.Count > 0 )
                {
                    ctrl.NeighborLeft = eventCtrls[eventCtrls.Count - 1] as EventControl;
                }

                this.Controls.Add( ctrl );
                ctrl.TabIndex = this.EventControls.Count - 1;
            }

            // let our neighbors know about us
            if ( ctrl.NeighborLeft != null )
            {
                ctrl.NeighborLeft.NeighborRight = ctrl;
            }

            if ( ctrl.NeighborRight != null )
            {
                ctrl.NeighborRight.NeighborLeft = ctrl;
            }

            // add event handlers
            ctrl.Moving += new EventHandler( eventControl_Moving );
            ctrl.Moved += new EventHandler( eventControl_Moved );
            ctrl.Delete += new EventHandler( eventControl_Delete );
            ctrl.Edit += new EventHandler(eventControl_Edit);

            ctrl.Enter += new EventHandler(eventControl_Selected);
            ctrl.Leave += new EventHandler(eventControl_Deselected);
        }

        public void RemoveEvent( EventControl ctrl )
        {
            if ( this.Controls.Contains( ctrl ) )
            {
                this.Controls.Remove( ctrl );
                this.EventControls.Remove( ctrl );

                if ( ctrl.NeighborLeft != null )
                {
                    ctrl.NeighborLeft.NeighborRight = ctrl.NeighborRight;
                }

                if ( ctrl.NeighborRight != null )
                {
                    ctrl.NeighborRight.NeighborLeft = ctrl.NeighborLeft;
                }

                ctrl.NeighborLeft = null;
                ctrl.NeighborRight = null;

                // remove event handlers
                ctrl.Moving -= new EventHandler( eventControl_Moving );
                ctrl.Moved -= new EventHandler( eventControl_Moved );
                ctrl.Delete -= new EventHandler( eventControl_Delete );
                ctrl.Edit -= new EventHandler( eventControl_Edit );

                ctrl.Enter -= new EventHandler(eventControl_Selected);
                ctrl.Leave -= new EventHandler(eventControl_Deselected);
            }
        }

        public void RemoveEvent( Guid guid )
        {
            EventControl eventCtrl = FindEvent( guid );
            if ( eventCtrl != null )
            {
                RemoveEvent( eventCtrl );
            }
        }

        public void RemoveEvent( object tag )
        {
            EventControl eventCtrl = FindEvent( tag );
            if ( eventCtrl != null )
            {
                RemoveEvent( eventCtrl );
            }
        }

        public EventControl FindEvent( Guid guid )
        {
            foreach ( EventControl ctrl in this.Controls )
            {
                if ( ctrl.Guid.Equals( guid ) )
                {
                    return ctrl;
                }
            }

            return null;
        }

        public EventControl FindEvent( object tag )
        {
            foreach ( EventControl ctrl in this.Controls )
            {
                if ( (ctrl.Tag != null) && ctrl.Tag.Equals( tag ) )
                {
                    return ctrl;
                }
            }

            return null;
        }
        #endregion
    }

    public class EventCreateEventArgs : EventArgs
    {
        public EventCreateEventArgs( float minT, float maxT )
        {
            m_minT = minT;
            m_maxT = maxT;
        }

        #region Variables
        private float m_minT;
        private float m_maxT;
        #endregion

        #region Properties
        /// <summary>
        /// The MinT for the EventControl that is being created.
        /// </summary>
        public float MinT
        {
            get
            {
                return m_minT;
            }
        }

        /// <summary>
        /// The MaxT for the EventControl that is being created.
        /// </summary>
        public float MaxT
        {
            get
            {
                return m_maxT;
            }
        }
        #endregion
    }

    public delegate void EventCreateEventHandler( object sender, EventCreateEventArgs e );

    public class EventControlEventArgs : EventArgs
    {
        public EventControlEventArgs( EventControl eventCtrl )
        {
            m_eventControl = eventCtrl;
        }

        #region Variables
        private EventControl m_eventControl;
        #endregion

        #region Properties
        /// <summary>
        /// The EventControl on which the operation is being performed.
        /// </summary>
        public EventControl EventControl
        {
            get
            {
                return m_eventControl;
            }
        }
        #endregion
    }

    public delegate void EventControlEventHandler( object sender, EventControlEventArgs e );
}
