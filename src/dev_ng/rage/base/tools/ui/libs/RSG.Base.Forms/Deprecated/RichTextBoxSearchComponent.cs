using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace RSG.Base.Forms
{
    /// <summary>
    /// This is a helper component for the RichTextBoxSearchReplaceDialog.  Typically, you should not need 
    /// to add one of these to your Form.  Just use the dialog.
    /// </summary>
    public partial class RichTextBoxSearchComponent : Component
    {
        public RichTextBoxSearchComponent()
        {
            InitializeComponent();
        }

        public RichTextBoxSearchComponent( IContainer container )
        {
            container.Add( this );

            InitializeComponent();
        }

        #region Variables
        private RichTextBox m_richTextBox = null;

        private rageSearchReplaceType m_searchType;
        private int m_numFound = 0;
        private rageSearchReplaceResult m_result = rageSearchReplaceResult.Found;
        private string m_resultMessage = null;
        private string m_resultTitle = null;
        #endregion

        #region Delegates
        private delegate rageSearchReplaceResultData FindReplaceOptionsStartOffsetDelegate( rageRichTextBoxSearchReplaceOptions options );
        private delegate List<rageSearchReplaceResultData> FindReplaceOptionsDelegate( rageRichTextBoxSearchReplaceOptions options );
        private delegate void GoToTopOfFileRichTextBoxDelegate( RichTextBox rtb );
        #endregion

        #region Events
        /// <summary>
        /// Dispatched at the start of the search/replace, indicating what the operation is.
        /// </summary>
        public event rageSearchBeginEventHandler SearchBegin;

        /// <summary>
        /// Dispatched at the end of the search, providing the final results of the operation.
        /// </summary>
        public event rageSearchCompleteEventHandler SearchComplete;

        /// <summary>
        /// Dispatched when a match is found during the search/replace operation.
        /// </summary>
        public event rageFoundStringEventHandler FoundString;
        #endregion

        #region Event Dispatchers
        protected void OnSearchBegin( rageSearchReplaceEventArgs e )
        {
            m_searchType = e.Search;

            if ( this.SearchBegin != null )
            {
                if ( m_richTextBox.InvokeRequired )
                {
                    m_richTextBox.Invoke( this.SearchBegin, new object[] { e } );
                }
                else
                {
                    this.SearchBegin( this, e );
                }
            }

            m_result = rageSearchReplaceResult.NotFound;
            m_resultMessage = "The specified text was not found.";
            m_resultTitle = "Search/Replace";

            m_numFound = 0;
        }

        protected void OnSearchComplete( rageSearchCompleteEventArgs e )
        {
            if ( this.SearchComplete != null )
            {
                if ( this.SearchComplete != null )
                {
                    if ( m_richTextBox.InvokeRequired )
                    {
                        m_richTextBox.Invoke( this.SearchComplete, new object[] { e } );
                    }
                    else
                    {
                        this.SearchComplete( this, e );
                    }
                }
            }
        }

        protected void OnFoundString( rageSearchReplaceFoundStringEventArgs e )
        {
            if ( this.FoundString != null )
            {
                if ( m_richTextBox.InvokeRequired )
                {
                    m_richTextBox.Invoke( this.FoundString, new object[] { e } );
                }
                else
                {
                    this.FoundString( this, e );
                }
            }
        }
        #endregion

        #region Invokers
        private rageSearchReplaceResultData FindReplaceNext( SearchRichTextBox searchRTB, rageRichTextBoxSearchReplaceOptions options, 
            bool replace )
        {
            if ( replace )
            {
                if ( searchRTB.RichTextBox.InvokeRequired )
                {
                    FindReplaceOptionsStartOffsetDelegate del = new FindReplaceOptionsStartOffsetDelegate( searchRTB.ReplaceNext );
                    return searchRTB.RichTextBox.Invoke( del, new object[] { options } ) as rageSearchReplaceResultData;
                }
                else
                {
                    return searchRTB.ReplaceNext( options );
                }
            }
            else
            {
                if ( searchRTB.RichTextBox.InvokeRequired )
                {
                    FindReplaceOptionsStartOffsetDelegate del = new FindReplaceOptionsStartOffsetDelegate( searchRTB.FindNext );
                    return searchRTB.RichTextBox.Invoke( del, new object[] { options } ) as rageSearchReplaceResultData;
                }
                else
                {
                    return searchRTB.FindNext( options );
                }
            }
        }

        private List<rageSearchReplaceResultData> FindReplaceAll( SearchRichTextBox searchRTB, rageRichTextBoxSearchReplaceOptions options, 
            bool replace )
        {
            if ( replace )
            {
                if ( searchRTB.RichTextBox.InvokeRequired )
                {
                    FindReplaceOptionsDelegate del = new FindReplaceOptionsDelegate( searchRTB.ReplaceAll );
                    return searchRTB.RichTextBox.Invoke( del, new object[] { options } ) as List<rageSearchReplaceResultData>;
                }
                else
                {
                    return searchRTB.ReplaceAll( options );
                }
            }
            else
            {
                if ( searchRTB.RichTextBox.InvokeRequired )
                {
                    FindReplaceOptionsDelegate del = new FindReplaceOptionsDelegate( searchRTB.FindAll );
                    return searchRTB.RichTextBox.Invoke( del, new object[] { options } ) as List<rageSearchReplaceResultData>;
                }
                else
                {
                    return searchRTB.FindAll( options );
                }
            }
        }
        #endregion

        #region Public Functions
        /// <summary>
        /// Determines what the search text should be based on the given offset in the RichTextBox.  This is typically
        /// used to initialize the "Find What" field when a Find/Replace dialog is shown.
        /// </summary>
        /// <param name="rtb"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public string DetermineSearchText( RichTextBox rtb, int offset )
        {
            SearchRichTextBox searchRTB = new SearchRichTextBox( rtb );
            return searchRTB.GetSelectedText( offset );
        }

        /// <summary>
        /// Performs the Find Next operation on the given RichTextBox with the given search parameters.
        /// </summary>
        /// <param name="rtb"></param>
        /// <param name="options"></param>
        public void FindNext( RichTextBox rtb, rageRichTextBoxSearchReplaceOptions options )
        {
            m_richTextBox = rtb;

            OnSearchBegin( new rageSearchReplaceEventArgs( rageSearchReplaceType.Find ) );

            FindReplaceNextInternal( rtb, options, false );

            OnSearchComplete( new rageSearchCompleteEventArgs( m_numFound, m_result, m_resultMessage, m_resultTitle, m_searchType ) );
        }

        /// <summary>
        /// Performs the Replace Next operation on the given RichTextBox with the given search parameters.
        /// </summary>
        /// <param name="rtb"></param>
        /// <param name="options"></param>
        public void ReplaceNext( RichTextBox rtb, rageRichTextBoxSearchReplaceOptions options )
        {
            m_richTextBox = rtb;

            OnSearchBegin( new rageSearchReplaceEventArgs( rageSearchReplaceType.Replace ) );

            FindReplaceNextInternal( rtb, options, true );

            OnSearchComplete( new rageSearchCompleteEventArgs( m_numFound, m_result, m_resultMessage, m_resultTitle, m_searchType ) );
        }

        /// <summary>
        /// Performs the Find All operation on the given RichTextBox with the given search parameters.
        /// </summary>
        /// <param name="rtb"></param>
        /// <param name="options"></param>
        public void FindAll( RichTextBox rtb, rageRichTextBoxSearchReplaceOptions options )
        {
            m_richTextBox = rtb;

            OnSearchBegin( new rageSearchReplaceEventArgs( rageSearchReplaceType.FindAll ) );

            FindReplaceAllInternal( rtb, options, false );

            OnSearchComplete( new rageSearchCompleteEventArgs( m_numFound, m_result, m_resultMessage, m_resultTitle, m_searchType ) );
        }

        /// <summary>
        /// Performs the Replace All operation on the given RichTextBox with the given search parameters.
        /// </summary>
        /// <param name="rtb"></param>
        /// <param name="options"></param>
        public void ReplaceAll( RichTextBox rtb, rageRichTextBoxSearchReplaceOptions options )
        {
            m_richTextBox = rtb;

            OnSearchBegin( new rageSearchReplaceEventArgs( rageSearchReplaceType.ReplaceAll ) );

            FindReplaceAllInternal( rtb, options, true );

            OnSearchComplete( new rageSearchCompleteEventArgs( m_numFound, m_result, m_resultMessage, m_resultTitle, m_searchType ) );
        }
        #endregion

        #region Private Functions
        private void DetermineResult( rageSearchReplaceResultData resultData )
        {
            m_result = rageSearchReplaceResult.Found;
            m_resultMessage = string.Empty;
            m_resultTitle = "Search/Replace";

            // See if the search went past the starting point
            if ( resultData.PastStartOfSearch )
            {
                m_result = rageSearchReplaceResult.PastStartOfSearch;
                m_resultMessage = "Find reached the starting point of the search.";
            }
            // If no matches were found...			
            else if ( !resultData.Found )
            {
                m_result = rageSearchReplaceResult.NotFound;
                m_resultMessage = "The specified text was not found.";
            }
            // See if the search went past the end of the file
            else if ( resultData.PastEndOfFile )
            {
                m_result = rageSearchReplaceResult.PastEndOfFile;
                m_resultMessage = "Find reached end of document.";
            }
        }

        private void GoToTopOfRichTextBox( RichTextBox rtb )
        {
            if ( rtb.InvokeRequired )
            {
                GoToTopOfFileRichTextBoxDelegate del = new GoToTopOfFileRichTextBoxDelegate( GoToTopOfRichTextBox );
                rtb.Invoke( del, new object[] { rtb } );
            }
            else
            {
                rtb.SelectionLength = 0;
                rtb.SelectionStart = 0;
            }
        }

        private void FindReplaceNextInternal( RichTextBox rtb, rageRichTextBoxSearchReplaceOptions options, bool replace )
        {
            SearchRichTextBox searchRTB = new SearchRichTextBox( rtb );

            rageSearchReplaceResultData resultData = null;
            try
            {
                resultData = FindReplaceNext( searchRTB, options, replace );
            }
            catch ( System.Exception e )
            {
                m_result = rageSearchReplaceResult.Error;
                m_resultMessage = String.Format( "An error occurred:{0}{1}", Environment.NewLine, e.Message );
                m_resultTitle = String.Format( "Search/Replace Error" );
                return;
            }

            DetermineResult( resultData );

            if ( m_result == rageSearchReplaceResult.Found )
            {
                OnFoundString( new rageSearchReplaceFoundStringEventArgs( resultData.Offset, 
                    replace ? options.ReplaceText : options.FindText, m_searchType ) );

                ++m_numFound;
            }
        }

        private void FindReplaceAllInternal( RichTextBox rtb, rageRichTextBoxSearchReplaceOptions options, bool replace )
        {
            SearchRichTextBox searchRTB = new SearchRichTextBox( rtb );

            List<rageSearchReplaceResultData> results = null;
            try
            {
                results = FindReplaceAll( searchRTB, options, replace );
            }
            catch ( System.Exception e )
            {
                m_result = rageSearchReplaceResult.Error;
                m_resultMessage = String.Format( "An error occurred:{0}{1}", Environment.NewLine, e.Message );
                m_resultTitle = String.Format( "Search/Replace Error" );
                return;
            }

            foreach ( rageSearchReplaceResultData resultData in results )
            {
                if ( resultData.Found )
                {
                    OnFoundString( new rageSearchReplaceFoundStringEventArgs( resultData.Offset,
                        replace ? options.ReplaceText : options.FindText, m_searchType ) );

                    ++m_numFound;
                }
            }

            if ( m_numFound > 0 )
            {
                m_result = rageSearchReplaceResult.Found;
                m_resultMessage = string.Empty;
                m_resultTitle = "Search/Replace";
            }
            else
            {
                m_result = rageSearchReplaceResult.NotFound;
                m_resultMessage = "The specified text was not found.";
                m_resultTitle = "Search/Replace";
            }
        }
        #endregion
    }
}
