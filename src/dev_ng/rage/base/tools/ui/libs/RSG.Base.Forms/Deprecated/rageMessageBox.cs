using System;
using System.Text;
using System.Windows.Forms;
using RSG.Base.Logging;

namespace RSG.Base.Forms
{
    /// <summary>
    /// A wrapper around System.Windows.Forms.MessageBox that logs the message to the Console and records the DialogResult.
    /// </summary>
    public struct rageMessageBox
    {
        #region Public Static Functions
        public static void ShowNone( IWin32Window owner, string text, string caption )
        {
            ShowMessage( owner, text, caption, MessageBoxButtons.OK, MessageBoxIcon.None );
        }

        public static void ShowNone( string text, string caption )
        {
            ShowMessage( null, text, caption, MessageBoxButtons.OK, MessageBoxIcon.None );
        }

        public static DialogResult ShowNone( IWin32Window owner, string text, string caption, MessageBoxButtons buttons )
        {
            return ShowMessage( owner, text, caption, buttons, MessageBoxIcon.None );
        }

        public static DialogResult ShowNone( string text, string caption, MessageBoxButtons buttons )
        {
            return ShowMessage( null, text, caption, buttons, MessageBoxIcon.None );
        }

        public static void ShowError( IWin32Window owner, string text, string caption )
        {
            ShowMessage( owner, text, caption, MessageBoxButtons.OK, MessageBoxIcon.Error );
        }

        public static void ShowError( string text, string caption )
        {
            ShowMessage( null, text, caption, MessageBoxButtons.OK, MessageBoxIcon.Error );
        }

        public static DialogResult ShowError( IWin32Window owner, string text, string caption, MessageBoxButtons buttons )
        {
            return ShowMessage( owner, text, caption, buttons, MessageBoxIcon.Error );
        }

        public static DialogResult ShowError( string text, string caption, MessageBoxButtons buttons )
        {
            return ShowMessage( null, text, caption, buttons, MessageBoxIcon.Error );
        }

        public static void ShowWarning( IWin32Window owner, string text, string caption )
        {
            ShowMessage( owner, text, caption, MessageBoxButtons.OK, MessageBoxIcon.Warning );
        }

        public static void ShowWarning( string text, string caption )
        {
            ShowMessage( null, text, caption, MessageBoxButtons.OK, MessageBoxIcon.Warning );
        }

        public static DialogResult ShowWarning( IWin32Window owner, string text, string caption, MessageBoxButtons buttons )
        {
            return ShowMessage( owner, text, caption, buttons, MessageBoxIcon.Warning );
        }

        public static DialogResult ShowWarning( string text, string caption, MessageBoxButtons buttons )
        {
            return ShowMessage( null, text, caption, buttons, MessageBoxIcon.Warning );
        }

        public static void ShowExclamation( IWin32Window owner, string text, string caption )
        {
            ShowMessage( owner, text, caption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation );
        }

        public static void ShowExclamation( string text, string caption )
        {
            ShowMessage( null, text, caption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation );
        }

        public static DialogResult ShowExclamation( IWin32Window owner, string text, string caption, MessageBoxButtons buttons )
        {
            return ShowMessage( owner, text, caption, buttons, MessageBoxIcon.Exclamation );
        }

        public static DialogResult ShowExclamation( string text, string caption, MessageBoxButtons buttons )
        {
            return ShowMessage( null, text, caption, buttons, MessageBoxIcon.Exclamation );
        }

        public static DialogResult ShowQuestion( IWin32Window owner, string text, string caption )
        {
            return ShowMessage( owner, text, caption, MessageBoxButtons.YesNo, MessageBoxIcon.Question );
        }

        public static DialogResult ShowQuestion( string text, string caption )
        {
            return ShowMessage( null, text, caption, MessageBoxButtons.YesNo, MessageBoxIcon.Question );
        }

        public static DialogResult ShowQuestion( IWin32Window owner, string text, string caption, MessageBoxButtons buttons )
        {
            return ShowMessage( owner, text, caption, buttons, MessageBoxIcon.Question );
        }

        public static DialogResult ShowQuestion( string text, string caption, MessageBoxButtons buttons )
        {
            return ShowMessage( null, text, caption, buttons, MessageBoxIcon.Question );
        }

        public static void ShowInformation( IWin32Window owner, string text, string caption )
        {
            ShowMessage( owner, text, caption, MessageBoxButtons.OK, MessageBoxIcon.Information );
        }

        public static void ShowInformation( string text, string caption )
        {
            ShowMessage( null, text, caption, MessageBoxButtons.OK, MessageBoxIcon.Information );
        }

        public static DialogResult ShowInformation( IWin32Window owner, string text, string caption, MessageBoxButtons buttons )
        {
            return ShowMessage( owner, text, caption, buttons, MessageBoxIcon.Information );
        }

        public static DialogResult ShowInformation( string text, string caption, MessageBoxButtons buttons )
        {
            return ShowMessage( null, text, caption, buttons, MessageBoxIcon.Information );
        }
        #endregion

        #region Private Static Functions
        private static DialogResult ShowMessage( IWin32Window owner, string text, string caption, MessageBoxButtons buttons, MessageBoxIcon icon )
        {
            MessageToLogAndConsole( "****************************************************************" );
            MessageToLogAndConsole( "{0}: {1}", icon.ToString(), caption );
            MessageToLogAndConsole( "****************************************************************" );
            MessageToLogAndConsole( text );
            MessageToLogAndConsole( "****************************************************************" );

            StringBuilder msg = new StringBuilder( text );
            msg.AppendLine();
            msg.Append( System.Reflection.Assembly.GetEntryAssembly().GetName().Name );
            msg.Append( " Version " );
            msg.Append( System.Reflection.Assembly.GetEntryAssembly().GetName().Version.ToString() );

            DialogResult result = DialogResult.OK;
            if ( owner != null )
            {
                result = MessageAlertBox.Show(owner, msg.ToString(), caption, buttons, icon);
            }
            else
            {
                result = MessageAlertBox.Show(msg.ToString(), caption, buttons, icon);
            }
            
            MessageToLogAndConsole( "User selected {0}", result.ToString() );
            MessageToLogAndConsole( "****************************************************************" );
            return result;
        }

        /// <summary>
        /// If we've initialized the log factory (and are thus using the new log) log a message to the app log. Otherwise, output to console only.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="args"></param>
        private static void MessageToLogAndConsole(String message, params String[] args)
        {
            if (LogFactory.ApplicationLog != null)
            {
                LogFactory.ApplicationLog.Message(message, args);
            }

            Console.WriteLine(message, args);
        }
        #endregion
    }
}
