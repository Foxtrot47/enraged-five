﻿namespace RSG.Base.Forms
{
    partial class AssetDependencyDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.ListViewGroup listViewGroup1 = new System.Windows.Forms.ListViewGroup("Required Dependencies", System.Windows.Forms.HorizontalAlignment.Left);
            System.Windows.Forms.ListViewGroup listViewGroup2 = new System.Windows.Forms.ListViewGroup("Optional Dependencies", System.Windows.Forms.HorizontalAlignment.Left);
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AssetDependencyDialog));
            this.tblPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.flowPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btnNoSync = new System.Windows.Forms.Button();
            this.btnSyncAndContinue = new RSG.Base.Forms.Controls.TimeButton();
            this.lvAssets = new System.Windows.Forms.ListView();
            this.hdrPath = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tblPanel1.SuspendLayout();
            this.flowPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tblPanel1
            // 
            this.tblPanel1.ColumnCount = 1;
            this.tblPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tblPanel1.Controls.Add(this.flowPanel1, 0, 1);
            this.tblPanel1.Controls.Add(this.lvAssets, 0, 0);
            this.tblPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tblPanel1.Location = new System.Drawing.Point(0, 0);
            this.tblPanel1.Name = "tblPanel1";
            this.tblPanel1.RowCount = 2;
            this.tblPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tblPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tblPanel1.Size = new System.Drawing.Size(646, 484);
            this.tblPanel1.TabIndex = 0;
            // 
            // flowPanel1
            // 
            this.flowPanel1.Controls.Add(this.btnNoSync);
            this.flowPanel1.Controls.Add(this.btnSyncAndContinue);
            this.flowPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.flowPanel1.Location = new System.Drawing.Point(3, 447);
            this.flowPanel1.Name = "flowPanel1";
            this.flowPanel1.Size = new System.Drawing.Size(640, 34);
            this.flowPanel1.TabIndex = 0;
            // 
            // btnNoSync
            // 
            this.btnNoSync.DialogResult = System.Windows.Forms.DialogResult.No;
            this.btnNoSync.Location = new System.Drawing.Point(477, 3);
            this.btnNoSync.Name = "btnNoSync";
            this.btnNoSync.Size = new System.Drawing.Size(160, 28);
            this.btnNoSync.TabIndex = 0;
            this.btnNoSync.Text = "&Continue";
            this.btnNoSync.UseVisualStyleBackColor = true;
            // 
            // btnSyncAndContinue
            // 
            this.btnSyncAndContinue.DialogResult = System.Windows.Forms.DialogResult.Yes;
            this.btnSyncAndContinue.Location = new System.Drawing.Point(311, 3);
            this.btnSyncAndContinue.Name = "btnSyncAndContinue";
            this.btnSyncAndContinue.ShowTimeout = true;
            this.btnSyncAndContinue.Size = new System.Drawing.Size(160, 28);
            this.btnSyncAndContinue.TabIndex = 1;
            this.btnSyncAndContinue.Text = "&Sync and Continue";
            this.btnSyncAndContinue.Timeout = 60;
            this.btnSyncAndContinue.UseVisualStyleBackColor = true;
            // 
            // lvAssets
            // 
            this.lvAssets.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.hdrPath});
            this.lvAssets.Dock = System.Windows.Forms.DockStyle.Fill;
            listViewGroup1.Header = "Required Dependencies";
            listViewGroup1.Name = "grpRequired";
            listViewGroup2.Header = "Optional Dependencies";
            listViewGroup2.Name = "grpOptional";
            this.lvAssets.Groups.AddRange(new System.Windows.Forms.ListViewGroup[] {
            listViewGroup1,
            listViewGroup2});
            this.lvAssets.Location = new System.Drawing.Point(3, 3);
            this.lvAssets.Name = "lvAssets";
            this.lvAssets.Size = new System.Drawing.Size(640, 438);
            this.lvAssets.TabIndex = 1;
            this.lvAssets.UseCompatibleStateImageBehavior = false;
            this.lvAssets.View = System.Windows.Forms.View.Details;
            // 
            // hdrPath
            // 
            this.hdrPath.Text = "Path";
            this.hdrPath.Width = 600;
            // 
            // AssetDependencyDialog
            // 
            this.AcceptButton = this.btnSyncAndContinue;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnNoSync;
            this.ClientSize = new System.Drawing.Size(646, 484);
            this.Controls.Add(this.tblPanel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimizeBox = false;
            this.Name = "AssetDependencyDialog";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Outdated Asset Dependencies";
            this.TopMost = true;
            this.tblPanel1.ResumeLayout(false);
            this.flowPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tblPanel1;
        private System.Windows.Forms.FlowLayoutPanel flowPanel1;
        private System.Windows.Forms.Button btnNoSync;
        private Controls.TimeButton btnSyncAndContinue;
        private System.Windows.Forms.ListView lvAssets;
        private System.Windows.Forms.ColumnHeader hdrPath;
    }
}