namespace RSG.Base.Forms
{
    partial class UnhandledExceptionDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && (components != null) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.errorRichTextBox = new System.Windows.Forms.RichTextBox();
            this.sendButton = new System.Windows.Forms.Button();
            this.dontSendButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // errorRichTextBox
            // 
            this.errorRichTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.errorRichTextBox.Location = new System.Drawing.Point(12, 12);
            this.errorRichTextBox.Name = "errorRichTextBox";
            this.errorRichTextBox.ReadOnly = true;
            this.errorRichTextBox.Size = new System.Drawing.Size(388, 363);
            this.errorRichTextBox.TabIndex = 0;
            this.errorRichTextBox.Text = "";
            // 
            // sendButton
            // 
            this.sendButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.sendButton.AutoSize = true;
            this.sendButton.Location = new System.Drawing.Point(217, 381);
            this.sendButton.Name = "sendButton";
            this.sendButton.Size = new System.Drawing.Size(102, 23);
            this.sendButton.TabIndex = 7;
            this.sendButton.Text = "Send Error Report";
            this.sendButton.UseVisualStyleBackColor = true;
            this.sendButton.Click += new System.EventHandler(this.sendButton_Click);
            // 
            // dontSendButton
            // 
            this.dontSendButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.dontSendButton.AutoSize = true;
            this.dontSendButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.dontSendButton.Location = new System.Drawing.Point(325, 381);
            this.dontSendButton.Name = "dontSendButton";
            this.dontSendButton.Size = new System.Drawing.Size(75, 23);
            this.dontSendButton.TabIndex = 8;
            this.dontSendButton.Text = "Don\'t Send";
            this.dontSendButton.UseVisualStyleBackColor = true;
            // 
            // UnhandledExceptionDialog
            // 
            this.AcceptButton = this.sendButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.dontSendButton;
            this.ClientSize = new System.Drawing.Size(412, 416);
            this.Controls.Add(this.dontSendButton);
            this.Controls.Add(this.sendButton);
            this.Controls.Add(this.errorRichTextBox);
            this.MinimumSize = new System.Drawing.Size(420, 413);
            this.Name = "UnhandledExceptionDialog";
            this.ShowIcon = false;
            this.Text = "Unhandled Exception!";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.UnhandledExceptionForm_Load);
            this.Shown += new System.EventHandler(this.UnhandledExceptionDialog_Shown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox errorRichTextBox;
        private System.Windows.Forms.Button sendButton;
        private System.Windows.Forms.Button dontSendButton;
    }
}