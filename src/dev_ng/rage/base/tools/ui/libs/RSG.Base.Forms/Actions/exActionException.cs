//
// File: exActionException.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of exActionException.cs class
//

using System;
using System.Collections.Generic;
using System.Text;

namespace RSG.Base.Forms.Actions
{

    /// <summary>
    /// Generic action exception class
    /// </summary>
    public class exActionException : Exception
    {
        #region Constructors
        public exActionException()
            : base()
        {
        }

        public exActionException(String Message)
            : base(Message)
        {
        }

        public exActionException(String Message, Exception InnerException)
            : base(Message, InnerException)
        {
        }
        #endregion
    }

} // End of RSG.Base.Actions namespace

// End of file
