//
// File: ColourPickerDlg.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: 
//
// References:
//   http://msdn.microsoft.com/msdnmag/issues/03/07/GDIColorPicker/
//

using System;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using RSG.Base.Drawing;

namespace RSG.Base.Forms
{

    /// <summary>
    /// 
    /// </summary>
    public partial class ColourPickerDlg : Form
    {
        #region Enumerations
        private enum ChangeStyle
        {
            MouseMove,
            RGB,
            HSV,
            None
        }
        #endregion // Enumerations

        #region Properties and Associated Member Data
        /// <summary>
        /// 
        /// </summary>
        public Color Color
        {
            // Get or set the color to be
            // displayed in the color wheel.
            get
            {
                return myColorWheel.Color;
            }

            set
            {
                // Indicate the color change type. Either RGB or HSV
                // will cause the color wheel to update the position
                // of the pointer.
                changeType = ChangeStyle.RGB;
                RGB = new RGB(value.R, value.G, value.B);
                HSV = ColourSpaceConv.RGBtoHSV(RGB);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public Byte Alpha
        {
            get { return (Byte)nudAlpha.Value; }
            set { nudAlpha.Value = value; }
        }
        #endregion // Properties and Associated Member Data

        #region Member Data
        private ChangeStyle changeType = ChangeStyle.None;
        private Point selectedPoint;

        private cColourWheel myColorWheel;
        private RGB RGB;
        private HSV HSV;
        private bool isInUpdate = false;
        #endregion // Member Data

        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public ColourPickerDlg()
        {
            InitializeComponent();
        }
        #endregion // Constructor

        #region UI Event Handlers
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uiColourPickerDlg_Load(Object sender, EventArgs e)
        {
            // Turn on double-buffering, so the form looks better. 
            this.SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            this.SetStyle(ControlStyles.UserPaint, true);
            this.SetStyle(ControlStyles.DoubleBuffer, true);
            
            // These properties are set in design view, as well, but they
            // have to be set to false in order for the Paint
            // event to be able to display their contents.
            // Never hurts to make sure they're invisible.
            pnlSelectedColour.Visible = false;
            pnlBrightness.Visible = false;
            pnlAlpha.Visible = false;
            pnlColour.Visible = false;

            // Calculate the coordinates of the three
            // required regions on the form.
            Rectangle SelectedColorRectangle =
              new Rectangle(pnlSelectedColour.Location, pnlSelectedColour.Size);
            Rectangle BrightnessRectangle =
              new Rectangle(pnlBrightness.Location, pnlBrightness.Size);
            Rectangle ColorRectangle =
              new Rectangle(pnlColour.Location, pnlColour.Size);

            // Create the new ColorWheel class, indicating
            // the locations of the color wheel itself, the
            // brightness area, and the position of the selected color.
            myColorWheel = new cColourWheel(
              ColorRectangle, BrightnessRectangle,
              SelectedColorRectangle);
            myColorWheel.ColorChanged +=
              new cColourWheel.ColorChangedEventHandler(
              this.myColorWheel_ColorChanged);

            // Set the RGB and HSV values 
            // of the NumericUpDown controls.
            SetRGB(RGB);
            SetHSV(HSV);	
        }

        private void HandleMouse(object sender, MouseEventArgs e)
        {
            // If you have the left mouse button down, 
            // then update the selectedPoint value and 
            // force a repaint of the color wheel.
            if (e.Button == MouseButtons.Left)
            {
                changeType = ChangeStyle.MouseMove;
                selectedPoint = new Point(e.X, e.Y);
                this.Invalidate();
            }
        }

        private void uiColourPickerDlg_MouseUp(object sender, MouseEventArgs e)
        {
            myColorWheel.SetMouseUp();
            changeType = ChangeStyle.None;
        }

        private void myColorWheel_ColorChanged(Object sender, cColourWheelChangedEventArgs e)
        {
            SetRGB(e.RGB);
            SetHSV(e.HSV);
        }

        private void uiColourPickerDlg_Paint(Object sender, System.Windows.Forms.PaintEventArgs e)
        {
            // Depending on the circumstances, force a repaint
            // of the color wheel passing different information.
            switch (changeType)
            {
                case ChangeStyle.HSV:
                    myColorWheel.Draw(e.Graphics, HSV);
                    break;
                case ChangeStyle.MouseMove:
                case ChangeStyle.None:
                    myColorWheel.Draw(e.Graphics, selectedPoint);
                    break;
                case ChangeStyle.RGB:
                    myColorWheel.Draw(e.Graphics, RGB);
                    break;
            }
        }
        #endregion // UI Event Handlers

        #region Private Methods
        private void HandleRGBChange(Object sender, EventArgs e)
        {
            // If the R, G, or B values change, use this 
            // code to update the HSV values and invalidate
            // the color wheel (so it updates the pointers).
            // Check the isInUpdate flag to avoid recursive events
            // when you update the NumericUpdownControls.
            if (!isInUpdate)
            {
                changeType = ChangeStyle.RGB;
                RGB = new RGB((Byte)nudRed.Value, (Byte)nudGreen.Value, (Byte)nudBlue.Value);
                SetHSV(ColourSpaceConv.RGBtoHSV(RGB));
                this.Invalidate();
            }
        }

        private void HandleAlphaChange(Object sender, EventArgs e)
        {
#if false
            if (!isInUpdate)
            {
                changeType = ChangeStyle.RGB;
                RGB = new RGB((Byte)nudRed.Value, (Byte)nudGreen.Value, (Byte)nudBlue.Value);
                SetHSV(ColourSpaceConv.RGBtoHSV(RGB));
                this.Invalidate();
            }
#endif
        }

        private void HandleHSVChange(Object sender, EventArgs e)
        {
            // If the H, S, or V values change, use this 
            // code to update the RGB values and invalidate
            // the color wheel (so it updates the pointers).
            // Check the isInUpdate flag to avoid recursive events
            // when you update the NumericUpdownControls.
            if (!isInUpdate)
            {
                changeType = ChangeStyle.HSV;
                HSV = new HSV((Byte)(nudHue.Value), (Byte)(nudSaturation.Value), (Byte)(nudBrightness.Value));
                SetRGB(ColourSpaceConv.HSVtoRGB(HSV));
                this.Invalidate();
            }
        }

        private void SetRGB(RGB RGB)
        {
            // Update the RGB values on the form, but don't trigger
            // the ValueChanged event of the form. The isInUpdate
            // variable ensures that the event procedures
            // exit without doing anything.
            isInUpdate = true;
            RefreshValue(nudRed, RGB.Red);
            RefreshValue(nudBlue, RGB.Blue);
            RefreshValue(nudGreen, RGB.Green);
            isInUpdate = false;
        }

        private void SetHSV(HSV HSV)
        {
            // Update the HSV values on the form, but don't trigger
            // the ValueChanged event of the form. The isInUpdate
            // variable ensures that the event procedures
            // exit without doing anything.
            isInUpdate = true;
            RefreshValue(nudHue, HSV.Hue);
            RefreshValue(nudSaturation, HSV.Saturation);
            RefreshValue(nudBrightness, HSV.value);
            isInUpdate = false;
        }

        private void HandleTextChanged(Object sender, EventArgs e)
        {
            // This step works around a bug -- unless you actively
            // retrieve the value, the min and max settings for the 
            // control aren't honored when you type text. This may
            // be fixed in the 1.1 version, but in VS.NET 1.0, this 
            // step is required.
            Decimal x = ((NumericUpDown)sender).Value;
        }

        private void RefreshValue(NumericUpDown nud, int value)
        {
            // Update the value of the NumericUpDown control, 
            // if the value is different than the current value.
            // Refresh the control, causing an immediate repaint.
            if (nud.Value != value)
            {
                nud.Value = value;
                nud.Refresh();
            }
        }
        #endregion // Private Methods
    }
    
} // End of RSG.Base.Forms namespace

// End of file
