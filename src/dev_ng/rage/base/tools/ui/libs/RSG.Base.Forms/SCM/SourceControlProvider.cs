using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Text;
using System.Xml.Serialization;

using RSG.Base.Command;

namespace RSG.Base.Forms.SCM
{
    [XmlInclude(typeof(rageAlienBrainSourceControlProvider)),
    XmlInclude(typeof(ragePerforceSourceControlProvider))]
    public abstract class rageSourceControlProvider
    {
        protected rageSourceControlProvider( string name )
        {
            m_name = name;
        }

        protected rageSourceControlProvider( string name, string serverOrPort, string projectOrWorkspace, string loginOrUsername )
            : this( name )
        {
            this.ServerOrPort = serverOrPort;
            this.ProjectOrWorkspace = projectOrWorkspace;
            this.LoginOrUsername = loginOrUsername;
        }

        #region Variables
        private string m_name;
        private string m_serverOrPort = string.Empty;
        private string m_projectOrWorkspace = string.Empty;
        private string m_loginOrUsername = string.Empty;
        #endregion

        #region Properties
        public string Name
        {
            get
            {
                return m_name;
            }
        }

        public string ServerOrPort
        {
            get
            {
                return m_serverOrPort;
            }
            set
            {
                m_serverOrPort = value;
            }
        }

        public string ProjectOrWorkspace
        {
            get
            {
                return m_projectOrWorkspace;
            }
            set
            {
                m_projectOrWorkspace = value;
            }
        }

        public string LoginOrUsername
        {
            get
            {
                return m_loginOrUsername;
            }
            set
            {
                m_loginOrUsername = value;
            }
        }

        public abstract bool HasServerOrPort { get; }

        public abstract bool RequiresServerOrPort { get; }

        public abstract bool HasProjectOrWorkspace { get; }

        public abstract bool RequiresProjectOrWorkspace { get; }

        public abstract bool HasLoginOrUsername { get; }

        public abstract bool RequiresLoginOrUsername { get; }
        #endregion

        #region Overrides
        public override bool Equals( object obj )
        {
            if ( obj == null )
            {
                return false;
            }

            rageSourceControlProvider other = obj as rageSourceControlProvider;
            if ( other == null )
            {
                return false;
            }

            return (this.Name == other.Name)
                && (this.LoginOrUsername == other.LoginOrUsername)
                && (this.ProjectOrWorkspace == other.ProjectOrWorkspace)
                && (this.ServerOrPort == other.ServerOrPort);
        }

        public override int GetHashCode()
        {
            StringBuilder s = new StringBuilder( this.Name );
            s.Append( this.LoginOrUsername );
            s.Append( this.ProjectOrWorkspace );
            s.Append( this.ServerOrPort );

            return s.ToString().GetHashCode();
        }

        public override string ToString()
        {
            return this.Name;
        }
        #endregion

        #region Public Functions
        public abstract rageSourceControlProvider Clone();

        public void CopyFrom( rageSourceControlProvider other )
        {
            if ( other.Name == this.Name )
            {
                this.LoginOrUsername = other.LoginOrUsername;
                this.ProjectOrWorkspace = other.ProjectOrWorkspace;
                this.ServerOrPort = other.ServerOrPort;
            }
        }

        public virtual bool Equals( rageSourceControlProvider other )
        {
            if ( other == null )
            {
                return false;
            }

            return (this.Name == other.Name)
                && (this.LoginOrUsername == other.LoginOrUsername)
                && (this.ProjectOrWorkspace == other.ProjectOrWorkspace)
                && (this.ServerOrPort == other.ServerOrPort);
        }

        public virtual void Validate( out rageStatus status )
        {
            List<string> missingItems = new List<string>();

            if ( this.RequiresServerOrPort && String.IsNullOrEmpty( this.ServerOrPort ) )
            {
                missingItems.Add( "Server/Port" );
            }

            if ( this.RequiresProjectOrWorkspace && String.IsNullOrEmpty( this.ProjectOrWorkspace ) )
            {
                missingItems.Add( "Project/Workspace" );
            }

            if ( this.RequiresLoginOrUsername && String.IsNullOrEmpty( this.LoginOrUsername ) )
            {
                missingItems.Add( "Login/Username" );
            }

            if ( missingItems.Count > 0 )
            {
                StringBuilder error = new StringBuilder( "The following items need to be provided before continuing:  " );
                for ( int i = 0; i < missingItems.Count; ++i )
                {
                    if ( i > 0 )
                    {
                        if ( missingItems.Count > 2 )
                        {
                            error.Append( ", " );
                        }

                        if ( i == missingItems.Count - 1 )
                        {
                            if ( missingItems.Count == 2 )
                            {
                                error.Append( " and " );
                            }
                            else
                            {
                                error.Append( "and " );
                            }
                        }
                    }

                    error.Append( missingItems[i] );
                }

                error.Append( '.' );

                status = new rageStatus( error.ToString() );
            }
            else
            {
                status = new rageStatus();
            }
        }

        /// <summary>
        /// Determines if the file is under source control.
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public abstract bool IsSourceControlled( string filename, out rageStatus status );

        /// <summary>
        /// If the file is under source control, determines if we have the latest version of it locally.
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public abstract bool HaveLatestVersion( string filename, out rageStatus status );

        /// <summary>
        /// If the file is under source control, updates the local file with the latest version.
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="status"></param>
        public abstract void Sync( string filename, out rageStatus status );

        /// <summary>
        /// Opens the file for editing.
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="status"></param>
        public abstract void CheckOut( string filename, out rageStatus status );

        /// <summary>
        /// Reverts the file, losing any changes made to it.
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="status"></param>
        public abstract void Revert( string filename, out rageStatus status );

        /// <summary>
        /// Reverts the file if there are no changes to it.
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="status"></param>
        public abstract void RevertIfUnchanged( string filename, out rageStatus status );

        /// <summary>
        /// Adds the file to source control.
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="status"></param>
        public abstract void Add( string filename, out rageStatus status );

        /// <summary>
        /// Adds all of the files in the directory to source control.
        /// </summary>
        /// <param name="directory"></param>
        /// <param name="status"></param>
        public abstract void AddDirectory( string directory, out rageStatus status );

        public virtual void Clear()
        {
            this.LoginOrUsername = string.Empty;
            this.ProjectOrWorkspace = string.Empty;
            this.ServerOrPort = string.Empty;
        }
        #endregion

        #region Private Functions
        /// <summary>
        /// This is a hilarious function to encrypt/decrypt a string. It is a simple XOR, so the function can be used both ways.
        /// </summary>
        /// <param name="sourceString"></param>
        /// <param name="seed"></param>
        /// <returns></returns>
        private string PatheticCrypto( string sourceString, string seed )
        {
            int index = 0;
            string result = "";
            int length = sourceString.Length;
            int seedLen = seed.Length;

            for ( int x = 0; x < length; x++ )
            {
                result += (char)(sourceString[x] ^ (seed[index++] & 15));
                index %= seedLen;
            }

            return result;
        }
        #endregion
    }

    public class ragePerforceSourceControlProvider : rageSourceControlProvider
    {
        public ragePerforceSourceControlProvider()
            : base( "Perforce" )
        {

        }

        public ragePerforceSourceControlProvider( string serverOrPort, string projectOrWorkspace, string loginOrUsername )
            : base( "Perforce", serverOrPort, projectOrWorkspace, loginOrUsername )
        {

        }

        #region Overrides
        public override bool HasServerOrPort { get { return true; } }

        public override bool RequiresServerOrPort { get { return false; } }

        public override bool HasProjectOrWorkspace { get { return true; } }

        public override bool RequiresProjectOrWorkspace { get { return false; } }

        public override bool HasLoginOrUsername { get { return true; } }

        public override bool RequiresLoginOrUsername { get { return false; } }

        public override rageSourceControlProvider Clone()
        {
            return new ragePerforceSourceControlProvider( this.ServerOrPort, this.ProjectOrWorkspace, this.LoginOrUsername );
        }

        public override bool IsSourceControlled( string filename, out rageStatus status )
        {
            ragePerforce.Port = this.ServerOrPort;
            ragePerforce.UserName = this.LoginOrUsername;
            ragePerforce.WorkspaceName = this.ProjectOrWorkspace;
            
            // see if it exists
            if ( !File.Exists( filename ) )
            {
                status = new rageStatus( String.Format( "The file '{0}' does not exist.", filename ) );
                return false;
            }

            return ragePerforce.FileExists( filename, out status );
        }

        public override bool HaveLatestVersion( string filename, out rageStatus status )
        {
            if ( IsSourceControlled( filename, out status ) )
            {
                return ragePerforce.IsLocalFileLatestVersion( filename, out status );
            }

            return true;
        }

        public override void Sync( string filename, out rageStatus status )
        {
            if ( IsSourceControlled( filename, out status ) )
            {
                ragePerforce.SyncFile( filename, out status );
            }
        }

        public override void CheckOut( string filename, out rageStatus status )
        {
            if ( IsSourceControlled( filename, out status ) )
            {
                // see if it's writable
                FileAttributes attributes = File.GetAttributes( filename );
                if ( (attributes & FileAttributes.ReadOnly) != FileAttributes.ReadOnly )
                {
                    status = new rageStatus();
                    return;
                }

                ragePerforce.CheckOutFile( filename, out status );
            }
        }

        public override void Revert( string filename, out rageStatus status )
        {
            if ( IsSourceControlled( filename, out status ) )
            {
                ragePerforce.RevertFile( filename, false, out status );
            }
        }

        public override void RevertIfUnchanged( string filename, out rageStatus status )
        {
            if ( IsSourceControlled( filename, out status ) )
            {
                ragePerforce.RevertFile( filename, true, out status );
            }
        }

        public override void Add( string filename, out rageStatus status )
        {
            if ( !IsSourceControlled( filename, out status ) && status.Success() )
            {
                ragePerforce.AddFile( filename, false, out status );
            }
        }

        public override void AddDirectory( string directory, out rageStatus status )
        {
            if ( !Directory.Exists( directory ) )
            {
                status = new rageStatus( String.Format( "The directory '{0}' does not exist.", directory ) );
                return;
            }

            ragePerforce.AddAllFilesInFolder( directory, false, out status );
        }
        #endregion
    }

    public class rageAlienBrainSourceControlProvider : rageSourceControlProvider
    {
        public rageAlienBrainSourceControlProvider()
            : base("Alienbrain")
        {

        }

        public rageAlienBrainSourceControlProvider(string serverOrPort, string projectOrWorkspace, string loginOrUsername)
            : base("Alienbrain", serverOrPort, projectOrWorkspace, loginOrUsername)
        {

        }

        #region Overrides
        public override bool HasServerOrPort { get { return true; } }

        public override bool RequiresServerOrPort { get { return false; } }

        public override bool HasProjectOrWorkspace { get { return true; } }

        public override bool RequiresProjectOrWorkspace { get { return false; } }

        public override bool HasLoginOrUsername { get { return true; } }

        public override bool RequiresLoginOrUsername { get { return false; } }

        public override rageSourceControlProvider Clone()
        {
            return new rageAlienBrainSourceControlProvider(this.ServerOrPort, this.ProjectOrWorkspace, this.LoginOrUsername);
        }

        public override bool IsSourceControlled(string filename, out rageStatus status)
        {
            status = new rageStatus("unsupported");
            return false;
        }

        public override bool HaveLatestVersion(string filename, out rageStatus status)
        {
            status = new rageStatus("unsupported");
            return true;
        }

        public override void Sync(string filename, out rageStatus status)
        {
            status = new rageStatus("unsupported");
        }

        public override void CheckOut(string filename, out rageStatus status)
        {
            status = new rageStatus("unsupported");
        }

        public override void Revert(string filename, out rageStatus status)
        {
            status = new rageStatus("unsupported");
        }

        public override void RevertIfUnchanged(string filename, out rageStatus status)
        {
            status = new rageStatus("unsupported");
        }

        public override void Add(string filename, out rageStatus status)
        {
            status = new rageStatus("unsupported");
        }

        public override void AddDirectory(string directory, out rageStatus status)
        {
            status = new rageStatus("unsupported");
        }
        #endregion
    }

    public class rageSourceControlProviderSettings
    {
        public rageSourceControlProviderSettings()
        {
            m_providers.Add( new ragePerforceSourceControlProvider() );
            m_providers.Add(new rageAlienBrainSourceControlProvider());
            m_selectedIndex = 0;
        }

        #region Variables
        private List<rageSourceControlProvider> m_providers = new List<rageSourceControlProvider>();
        private bool m_enabled = false;
        private int m_selectedIndex = -1;
        #endregion

        #region Properties
        [XmlIgnore]
        public List<rageSourceControlProvider> Providers
        {
            get
            {
                return m_providers;
            }
            set
            {
                if ( (value == null) || (value.Count == 0) )
                {
                    m_providers.Clear();
                    m_providers.Add( new ragePerforceSourceControlProvider() );
                    m_providers.Add(new rageAlienBrainSourceControlProvider());
                }
                else 
                {
                    m_providers = value;
                }

                this.SelectedIndex = 0;
            }
        }

        [XmlArray( "Providers" ), Browsable( false ), DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public rageSourceControlProvider[] XmlProviders
        {
            get
            {
                return m_providers.ToArray();
            }
            set
            {
                if ( (value != null) && (value.Length > 0) )
                {
                    m_providers.Clear();
                    m_providers.AddRange( value );
                }
            }
        }

        public bool Enabled
        {
            get
            {
                return m_enabled;
            }
            set
            {
                m_enabled = value;
            }
        }

        public int SelectedIndex
        {
            get
            {
                return m_selectedIndex;
            }
            set
            {
                if ( (value >= 0) && (value < m_providers.Count) )
                {
                    m_selectedIndex = value;
                }
                else
                {
                    m_selectedIndex = -1;
                }
            }
        }

        [XmlIgnore, Browsable( false ), DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public rageSourceControlProvider SelectedProvider
        {
            get
            {
                if ( (m_selectedIndex >= 0) && (m_selectedIndex < m_providers.Count) )
                {
                    return m_providers[m_selectedIndex];
                }

                return null;
            }
            set
            {
                if ( value != null )
                {
                    int selectedIndex = m_providers.IndexOf( value );
                    if ( selectedIndex != -1 )
                    {
                        m_selectedIndex = selectedIndex;
                    }
                }
            }
        }
        #endregion

        #region Overrides
        public override bool Equals( object obj )
        {
            if ( obj == null )
            {
                return false;
            }

            if ( obj is rageSourceControlProviderSettings )
            {
                return Equals( obj as rageSourceControlProviderSettings );
            }

            return false;
        }

        public override int GetHashCode()
        {
            StringBuilder s = new StringBuilder( this.Enabled.ToString() );
            s.Append( this.SelectedIndex );
            s.Append( this.Providers.Count );
            
            foreach ( rageSourceControlProvider provider in this.Providers )
            {
                s.Append( provider.GetHashCode() );
            }

            return s.ToString().GetHashCode();
        }
        #endregion

        #region Public Functions
        public bool Equals( rageSourceControlProviderSettings other )
        {
            if ( other == null )
            {
                return false;
            }

            if ( (this.Enabled != other.Enabled) || (this.SelectedIndex != other.SelectedIndex)
                || (this.Providers.Count != other.Providers.Count) )
            {
                return false;
            }

            for ( int i = 0; i < this.Providers.Count; ++i )
            {
                if ( !this.Providers[i].Equals( other.Providers[i] ) )
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Indicates if a file is under the current source control.
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public bool IsSourceControlled( string filename, out rageStatus status )
        {
            if ( !this.Enabled )
            {
                if ( !File.Exists( filename ) )
                {
                    status = new rageStatus( String.Format( "The file '{0}' does not exist.", filename ) );
                }
                else
                {
                    status = new rageStatus();
                }

                return false;
            }

            if ( this.SelectedProvider != null )
            {
                return this.SelectedProvider.IsSourceControlled( filename, out status );
            }

            status = new rageStatus( "No source control provider selected." );
            return false;
        }
        
        /// <summary>
        /// If the file is under source control, determines if we have the latest version of it locally.
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public bool HaveLatestVersion( string filename, out rageStatus status )
        {
            if ( !this.Enabled )
            {
                if ( !File.Exists( filename ) )
                {
                    status = new rageStatus( String.Format( "The file '{0}' does not exist.", filename ) );
                }
                else
                {
                    status = new rageStatus();
                }

                return false;
            }

            if ( this.SelectedProvider != null )
            {
                return this.SelectedProvider.HaveLatestVersion( filename, out status );
            }

            status = new rageStatus( "No source control provider selected." );
            return false;
        }

        /// <summary>
        /// If the file is under source control, updates the local file with the latest version.
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="status"></param>
        public void Sync( string filename, out rageStatus status )
        {
            if ( !this.Enabled )
            {
                if ( !File.Exists( filename ) )
                {
                    status = new rageStatus( String.Format( "The file '{0}' does not exist.", filename ) );
                    return;
                }

                status = new rageStatus();
                return;
            }

            if ( this.SelectedProvider != null )
            {
                this.SelectedProvider.Sync( filename, out status );
            }
            else
            {
                status = new rageStatus( "No source control provider selected." );
            }
        }

        /// <summary>
        /// When source control is enabled, checks out the file using the current provider.  Otherwise, 
        /// ensurse that the file is writable.
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="status"></param>
        public void CheckOut( string filename, out rageStatus status )
        {
            if ( !this.Enabled )
            {
                if ( !File.Exists( filename ) )
                {
                    status = new rageStatus( String.Format( "The file '{0}' does not exist.", filename ) );
                    return;
                }

                // make sure its writable
                try
                {
                    FileAttributes attributes = File.GetAttributes( filename );
                    if ( (attributes & FileAttributes.ReadOnly) == FileAttributes.ReadOnly )
                    {
                        File.SetAttributes( filename, File.GetAttributes( filename ) & (~FileAttributes.ReadOnly) );                        
                    }
                }
                catch ( Exception e )
                {
                    status = new rageStatus( String.Format( "Unable to make '{0}' writable:  {1}", filename, e.Message ) );
                    return;
                }

                status = new rageStatus();
                return;
            }

            if ( this.SelectedProvider != null )
            {
                this.SelectedProvider.CheckOut( filename, out status );
            }
            else
            {
                status = new rageStatus( "No source control provider selected." );
            }
        }

        /// <summary>
        /// When source control is enabled, reverts the file using the current provider, losing
        /// any changes made to it.
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="status"></param>
        public void Revert( string filename, out rageStatus status )
        {
            if ( !this.Enabled )
            {
                status = new rageStatus();
                return;
            }

            if ( this.SelectedProvider != null )
            {
                this.SelectedProvider.Revert( filename, out status );
            }
            else
            {
                status = new rageStatus( "No source control provider selected." );
            }
        }

        /// <summary>
        /// When source control is enabled, reverts the file using the current provider, if there
        /// are no changes to it.
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="status"></param>
        public void RevertIfUnchanged( string filename, out rageStatus status )
        {
            if ( !this.Enabled )
            {
                status = new rageStatus();
                return;
            }

            if ( this.SelectedProvider != null )
            {
                this.SelectedProvider.RevertIfUnchanged( filename, out status );
            }
            else
            {
                status = new rageStatus( "No source control provider selected." );
            }
        }

        /// <summary>
        /// When source control is enabled, adds the file to the current provider.
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="status"></param>
        public void Add( string filename, out rageStatus status )
        {
            if ( !this.Enabled )
            {
                status = new rageStatus();
                return;
            }

            if ( this.SelectedProvider != null )
            {
                this.SelectedProvider.Add( filename, out status );
            }
            else
            {
                status = new rageStatus( "No source control provider selected." );
            }
        }

        /// <summary>
        /// When source control is enabled, adds all filef in the directory to the current provider.
        /// </summary>
        /// <param name="directory"></param>
        /// <param name="status"></param>
        public void AddDirectory( string directory, out rageStatus status )
        {
            if ( !this.Enabled )
            {
                status = new rageStatus();
                return;
            }

            if ( this.SelectedProvider != null )
            {
                this.SelectedProvider.AddDirectory( directory, out status );
            }
            else
            {
                status = new rageStatus( "No source control provider selected." );
            }
        }

        public rageSourceControlProviderSettings Clone()
        {
            rageSourceControlProviderSettings clone = new rageSourceControlProviderSettings();
            clone.CopyFrom( this );
            return clone;
        }

        public void CopyFrom( rageSourceControlProviderSettings other )
        {
            this.Enabled = other.Enabled;

            this.Providers.Clear();
            foreach ( rageSourceControlProvider provider in other.Providers )
            {
                this.Providers.Add( provider.Clone() );
            }

            this.SelectedIndex = other.SelectedIndex;
        }

        public void Validate( out rageStatus status )
        {
            if ( this.Enabled && (this.SelectedProvider != null) )
            {
                this.SelectedProvider.Validate( out status );
                return;
            }

            status = new rageStatus();
        }

        public void Reset()
        {
            foreach ( rageSourceControlProvider provider in this.Providers )
            {
                provider.Clear();
            }

            this.Enabled = false;
            this.SelectedIndex = -1;
        }
        #endregion
    }
}
