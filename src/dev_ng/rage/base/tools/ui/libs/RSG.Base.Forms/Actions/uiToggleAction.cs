//
// File: uiToggleAction.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of uiToggleAction.cs class
//

using System;
using System.Drawing;
using System.Windows.Forms;

using RSG.Base.Collections;
using RSG.Base.Command;

namespace RSG.Base.Forms.Actions
{

    #region Enumerations
    /// <summary>
    /// Toggle Action Type
    /// </summary>
    public enum etToggleActionType
    {
        Primary,
        Secondary
    };
    #endregion // Enumerations

    /// <summary>
    /// Abstract Toggle Action
    /// </summary>
    public abstract class uiToggleAction : uiAction
    {
        #region Properties and Associated Member Data
        /// <summary>
        /// 
        /// </summary>
        public etToggleActionType Type
        {
            get { return m_eType; }
            internal set { m_eType = value; }
        }
        private etToggleActionType m_eType;

        /// <summary>
        /// Activated state of the Toggle Action (default: false)
        /// </summary>
        public bool Toggled
        {
            get { return m_bToggled; }
            set 
            {
                m_bToggled = value;
                UpdateUIElements();
                RaiseToggledEvent(); 
            }
        }
        private bool m_bToggled;
        #endregion // Properties and Associated Member Data

        #region Events
        /// <summary>
        /// 
        /// </summary>
        public event EventHandler<EventArgs> ToggledEvent;
        #endregion // Events

        #region Virtual Event Handlers
        /// <summary>
        /// Mouse down interaction event handler (optional)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void MouseDown(Object sender, MouseEventArgs e)
        {
        }

        /// <summary>
        /// Mouse up interaction event handler (optional)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void MouseUp(Object sender, MouseEventArgs e)
        {
        }

        /// <summary>
        /// Mouse move interaction event handler (optional)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void MouseMove(Object sender, MouseEventArgs e)
        {
        }

        /// <summary>
        /// Mouse wheel interaction event handler (optional)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void MouseWheel(Object sender, MouseEventArgs e)
        {
        }

        /// <summary>
        /// Key Down interaction event handler (optional)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void KeyDown(Object sender, KeyEventArgs e)
        {
        }

        /// <summary>
        /// Key Up interaction event handler (optional)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void KeyUp(Object sender, KeyEventArgs e)
        {
        }
        #endregion // Virtual Event Handlers

        #region Constructors
        /// <summary>
        /// Constructor (text, image and transparent image colour)
        /// </summary>
        /// <param name="sText">Text to display on UI items</param>
        /// <param name="pImage">Image to display on UI items</param>
        /// <param name="transparentColour">Image transparent colour</param>
        public uiToggleAction(String sText, Image pImage, Color transparentColour)
            : base(sText, pImage, transparentColour)
        {
            Initialise();
        }

        /// <summary>
        /// Constructor (text, image, transparent image colour and tooltip)
        /// </summary>
        /// <param name="sText">Text to display on UI items</param>
        /// <param name="pImage">Image to display on UI items</param>
        /// <param name="transparentColour">Image transparent colour</param>
        /// <param name="sToolTip">Tooltip text</param>
        public uiToggleAction(String sText, Image pImage, Color transparentColour, String sToolTip)
            : base(sText, pImage, transparentColour, sToolTip)
        {
            Initialise();
        }

        /// <summary>
        /// Constructor (text, image, transparent image colour, tooltip and shortcut keys)
        /// </summary>
        /// <param name="sText">Text to display on UI items</param>
        /// <param name="pImage">Image to display on UI items</param>
        /// <param name="transparentColour">Image transparent colour</param>
        /// <param name="sToolTip">Tooltip text</param>
        /// <param name="keys">Shortcut key</param>
        public uiToggleAction(String sText, Image pImage, Color transparentColour, String sToolTip, Keys keys)
            : base(sText, pImage, transparentColour, sToolTip, keys)
        {
            Initialise();
        }
        #endregion // Constructors

        #region Controller Methods
        public override void AddTo(ToolStrip ctrl)
        {
            // See if we have already been added to this toolstrip
            foreach (Pair<ToolStrip, ToolStripButton> tsBar in base.ToolStripButtons)
            {
                if (tsBar.First == ctrl)
                    throw new exActionException("This action has already been added to this menu.");
            }

            // Otherwise we can add our action to the toolstrip
            ToolStripButton tbNewButton = new ToolStripButton(base.Text, base.Image, ToggleActionEventHandler);
            tbNewButton.ImageTransparentColor = base.TransparentColour;
            base.ToolStripButtons.Add(new Pair<ToolStrip, ToolStripButton>(ctrl, tbNewButton));
            ctrl.Items.Add(tbNewButton);

            UpdateUIElements();
        }

        public override void AddTo(ToolStripMenuItem ctrl)
        {
            // See if we have already been added to this menu
            foreach (Pair<ToolStripMenuItem, ToolStripMenuItem> miItem in base.MenuItems)
            {
                if (miItem.First == ctrl)
                    throw new exActionException("This action has already been added to this menu.");
            }

            // Otherwise we can add our action to the menu
            ToolStripMenuItem miNewItem = new ToolStripMenuItem(base.Text, base.Image, ToggleActionEventHandler, base.ShortcutKey);
            miNewItem.ImageTransparentColor = base.TransparentColour;
            base.MenuItems.Add(new Pair<ToolStripMenuItem, ToolStripMenuItem>(ctrl, miNewItem));
            ctrl.DropDownItems.Add(miNewItem);
            
            UpdateUIElements();
        }

        public override void AddTo(ContextMenuStrip ctrl)
        {
            // See if we have already been added to this Context Menu
            foreach (Pair<ContextMenuStrip, ToolStripMenuItem> miItem in base.ContextMenus)
            {
                if (miItem.First == ctrl)
                    throw new exActionException("This action has already been added to this context menu.");
            }

            // Otherwise we can add our action to the context menu
            ToolStripMenuItem miNewItem = new ToolStripMenuItem(base.Text, base.Image, ToggleActionEventHandler, base.ShortcutKey);
            miNewItem.ImageTransparentColor = base.TransparentColour;
            base.ContextMenus.Add(new Pair<ContextMenuStrip, ToolStripMenuItem>(ctrl, miNewItem));
            ctrl.Items.Add(miNewItem);

            UpdateUIElements();
        }

        public override void RemoveFrom(ToolStrip ctrl)
        {
            if (0 == base.ToolStripButtons.Count)
                throw new exActionException("This action has not been added to any ToolStrip control");

            foreach (Pair<ToolStrip, ToolStripButton> miBar in base.ToolStripButtons)
            {
                if (miBar.First == ctrl)
                {
                    ctrl.Items.Remove(miBar.Second);
                    base.ToolStripButtons.Remove(miBar);
                }
            }
            
            UpdateUIElements();
        }

        public override void RemoveFrom(ToolStripMenuItem ctrl)
        {
            if (0 == base.MenuItems.Count)
                throw new exActionException("This action has not been added to any Menu control");

            foreach (Pair<ToolStripMenuItem, ToolStripMenuItem> miItem in base.MenuItems)
            {
                if (miItem.First == ctrl)
                {
                    ctrl.DropDownItems.Remove(miItem.Second);
                    base.MenuItems.Remove(miItem);
                }
            }

            UpdateUIElements();
        }

        public override void RemoveFrom(ContextMenuStrip ctrl)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Command Action : Update
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// The default Update method does nothing.  Application actions should
        /// override this behaviour to typically enable/disable the actions
        /// based on number of items selected or type of items selected.
        /// 
        public override void Update(Object sender, cActionUpdateEventArgs e)
        {
        }
        #endregion // Controller Methods

        #region UI Event Handlers
        /// <summary>
        /// Invoke our subclass' Toggled method, then invoke our ToggledEvent event
        /// </summary>
        /// This method is attached to all UI controls that represent this
        /// Command Action.  This method handles invoking the user-defined code
        /// in the subclass instance of the uiCommandAction class.
        /// 
        private void ToggleActionEventHandler(Object sender, EventArgs e)
        {
            this.Toggled = !this.Toggled;

            RaiseToggledEvent();
        }
        #endregion // UI Event Handlers

        #region Private Methods
        /// <summary>
        /// Common initialisation
        /// </summary>
        private void Initialise()
        {
            this.m_bToggled = false;
        }

        /// <summary>
        /// 
        /// </summary>
        private void RaiseToggledEvent()
        {
            // Update UI state
            foreach (Pair<ToolStripMenuItem, ToolStripMenuItem> miItem in MenuItems)
                miItem.Second.Checked = this.Toggled;
            foreach (Pair<ToolStrip, ToolStripButton> tbButton in ToolStripButtons)
                tbButton.Second.Checked = this.Toggled;

            if (null != this.ToggledEvent)
                ToggledEvent(this, new EventArgs());
        }
        #endregion // Private Methods
    }

} // End of RSG.Base.Actions namespace

// End of file
