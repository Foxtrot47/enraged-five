﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Xml.XPath;
using PresentationControls;
using System.IO;
using RSG.Base.Network;
using RSG.Base.Logging.Universal;

namespace RSG.Base.Forms.Controls
{
    public partial class UniversalLogControl : UserControl
    {
        #region Constants
        private static string xPath_rootContext = "//context";
        private static string xPath_allChildren = "*";
        private static readonly String xPath_Error = "//context/error";
        private static readonly String xPath_Warning = "//context/warning";
        #endregion

        #region Members
        #region public
        public String[] comboLabels
        {
            get{return m_comboLabels;}
            set{m_comboLabels = value;}
        }

        /// <summary>
        /// Internally set when errors have been logged.
        /// </summary>
        public bool HasErrors
        {
            get { return m_bHasErrors; }
            protected internal set { m_bHasErrors = value; }
        }
        protected bool m_bHasErrors;

        /// <summary>
        /// Internally set when warnings have been logged.
        /// </summary>
        public bool HasWarnings
        {
            get { return m_bHasWarnings; }
            protected internal set { m_bHasWarnings = value; }
        }
        protected bool m_bHasWarnings;

        #endregion
        #region private
        private List<String> history;
        private String lastHistoryEntry = "";

        private String m_filepath = null;
        private String[] m_comboLabels = { "Debug", "Messages", "Warnings", "Errors" };
        private String[] m_dataTags =    { "debug", "message", "warning", "error" };
        private Color[] m_typeColors =   { Color.White, Color.Green, Color.Yellow, Color.Red };
        private Boolean[] m_scopeSelected = { true, true, true, true };
        #endregion // private
        #endregion // all members

        #region Events
        // custom attributes 
        public class ContextArgs : System.EventArgs
        {
            private string[] message;

            public ContextArgs(string[] m)
            {
                this.message = m;
            }

            public string[] Message()
            {
                return message;
            }
        }        // delegate declaration 
//        public delegate void MessageSelectDelegate(object sender, ContextArgs ca); 

        // event declaration 
//        public event MessageSelectDelegate MessageSelected;
        static public event EventHandler<ContextArgs> MessageSelected;
        #endregion

        #region complex types
        public struct sLogMessage
        {
            public String mType;
            public String mTime;
            public String mMessage;
            public String mContext;
            public bool mCreateNew;
            public sLogMessage(String type, String time, String message, String context, bool createNew)
            {
                mType = type;
                mTime = time;
                mMessage = message;
                mContext = context;
                mCreateNew = createNew;
            }
        }
        #endregion

        #region Constructors
        public UniversalLogControl()
        {
            InitializeComponent();
        }
        public UniversalLogControl(String xmlDoc)
        {
            if (null == xmlDoc)
                xmlDoc = "X:/streamGTA5/maps/maps_unilog.xml";
            InitializeComponent();
            Init(xmlDoc);
        }
        #endregion

        #region Methods

        public void Init(String xmlDoc)
        {
            m_filepath = xmlDoc;
            history = new List<string>();
            AddToHistory(xmlDoc);
            this.m_tabContext.TabPages.Clear();
            ReadXmlDoc();
            HasErrors = HasWarnings = false;

            //FileSystemWatcher watcher = new FileSystemWatcher(); // Declares the FileSystemWatcher object
            //watcher.Path = Path.GetDirectoryName(m_filepath); // We have to specify the path which has to monitor
            //watcher.NotifyFilter = NotifyFilters.LastAccess|NotifyFilters.LastWrite|NotifyFilters.FileName|NotifyFilters.DirectoryName; // This property specifies which are the events to be monitored
            //watcher.Filter = Path.GetFileName(m_filepath); // Only watch text files.
            //// Add event handlers.
            //watcher.Changed += new FileSystemEventHandler(OnLogFileChanged);
            //watcher.Deleted += new FileSystemEventHandler(OnLogFileChanged);
            //// Begin watching.
            //watcher.EnableRaisingEvents = true;

            UpdateSelectedTabFilter();
        }

        private void AddToHistory(String path)
        {
            FileStream historyStream = File.Open("UniversalLogClient_History.txt", FileMode.OpenOrCreate);
            StreamReader sr = new StreamReader(historyStream, true);
            string text = sr.ReadToEnd();
            string[] paths = text.Split('\r');
            //bool isNewPath = true;
            foreach (string p in paths)
            {
                string cp = p;
                if ("\n" == p || "" == p || "\r" == p)
                    continue;
                cp = cp.Trim();
                cp = cp.Replace("\r", "");
                cp = cp.Replace("\n", "");
                if (cp != path)
                    history.Add(cp);
                //else
                //    isNewPath = false;
            }
//            if (!currentPathsAlreadyExistent)
            history.Add(path);
            StreamWriter sw = new StreamWriter(historyStream);
            historyStream.Seek(0, SeekOrigin.Begin);
            foreach(string h in history)
                sw.WriteLine(h);
            sw.Close();

            //
            // Ui update
            //
            if (history.Count > 0)// && (this.adressHistory.Text!=path) || isNewPath)
            {
                history.Reverse();
                this.adressHistory.Items.Clear();
                this.adressHistory.Items.AddRange(history.ToArray());
                this.adressHistory.Text = history[0];
            }
        }

        private void AppendToTargetTab(XPathNavigator node)
        {
            String tabName = node.GetAttribute("name", "");

            XPathNodeIterator nodes = node.Select(xPath_allChildren);
            bool firstNode = true;
            while (nodes.MoveNext())
            {
                XPathNavigator current = nodes.Current;
                Object[] attrs = { current.Name, current.HasAttributes, current.Value };
                //Console.WriteLine("Element {0} attributes? {1} value {2}", attrs);
                sLogMessage msg = new sLogMessage(current.Name, current.GetAttribute("timestamp", ""), current.Value, current.GetAttribute("context", ""), firstNode);
                AppendToTargetTab(tabName, msg);
                firstNode = false;
            }
        }
        private void AppendToTargetTab(String tabName, sLogMessage msg)
        {
            // Create data source and fill with existent data

            DataTable dataTable = null;
            if (!targetDataMap.ContainsKey(tabName))
            {
                dataTable = new DataTable();
                dataTable.Columns.Add("Type", typeof(String));
                dataTable.Columns.Add("Time", typeof(String));
                dataTable.Columns.Add("Message", typeof(String));
                dataTable.Columns.Add("Context", typeof(String));
                targetDataMap.Add(tabName, dataTable);
            }
            else
                dataTable = targetDataMap[tabName];

            if (msg.mCreateNew)
                dataTable.Clear();

            // Create data grid and bind data to it
            TabPage page = m_tabContext.TabPages[tabName];
            if (null == page)
            {
                //
                // datagrid init
                //
                m_tabContext.TabPages.Add(tabName, tabName);
                page = m_tabContext.TabPages[tabName];
                m_tabContext.SelectedTab = page;
                DataGridView dataGridView1 = new System.Windows.Forms.DataGridView();
                //
                // datagrid init
                //
                dataGridView1.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(ChangedSourcerHandler);
                ((System.ComponentModel.ISupportInitialize)(dataGridView1)).BeginInit();
                dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
                dataGridView1.Size = page.Size;
                dataGridView1.Dock = DockStyle.Fill;
//                dataGridView1.AutoSize = true;
//                dataGridView1.Anchor = (AnchorStyles.Bottom | AnchorStyles.Right | AnchorStyles.Top | AnchorStyles.Left);
                dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
                // Set property values appropriate for read-only display and 
                // limited interactivity. 
                dataGridView1.RowHeadersVisible = false;
                dataGridView1.AllowUserToAddRows = false;
                dataGridView1.AllowUserToDeleteRows = false;
                dataGridView1.AllowUserToOrderColumns = true;
                dataGridView1.ReadOnly = true;
                dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
                dataGridView1.MultiSelect = true;
                dataGridView1.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.None;
                dataGridView1.AllowUserToResizeColumns = true;
                dataGridView1.ColumnHeadersHeightSizeMode =
                    DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
                dataGridView1.AllowUserToResizeRows = false;
                dataGridView1.RowHeadersWidthSizeMode =
                    DataGridViewRowHeadersWidthSizeMode.DisableResizing;
                dataGridView1.ClearSelection();
                // Events
                dataGridView1.DataSource = dataTable;
                dataGridView1.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(RowsAddedHandler);
                dataGridView1.SelectionChanged += new EventHandler(RowSelectedHandler);
                //
                // connect datatable
                //
                ((System.ComponentModel.ISupportInitialize)(dataGridView1)).EndInit();
                if (null != page)
                {
                    page.Controls.Add(dataGridView1);
                }
            }

            // fill message data in
            dataTable.Rows.Add(msg.mType, msg.mTime, msg.mMessage, msg.mContext);
        }
        private Color GetColorFromMsgType(string type)
        {
            for (int k = 0; k < m_dataTags.Length; k++)
            {
                if (m_dataTags[k] == type.ToLower())
                {
                    HasErrors = k == 3;
                    HasWarnings = k == 2;
                    return m_typeColors[k];
                }
            }
            return Color.White;
        }

        public void ReadXmlDoc()
        {
            XPathDocument xPathDoc = new XPathDocument(m_filepath);
            XPathNavigator docRoot = xPathDoc.CreateNavigator();
            XPathNodeIterator nodes = docRoot.Select(xPath_rootContext);
            while (nodes.MoveNext())
            {
                AppendToTargetTab(nodes.Current);
            }
            nodes = docRoot.Select(xPath_Error);
            if (nodes.MoveNext())
            {
                HasErrors = true;
            }
            nodes = docRoot.Select(xPath_Warning);
            if (nodes.MoveNext())
            {
                HasWarnings = true;
            }
        }
        #endregion

        #region Events
        private void ResizeHandler(object sender, System.EventArgs e)
        {
            this.m_tabContext.Width = this.Width;
            this.adressHistory.Width = this.Width - this.filterControl.Width - 5;
            this.emailButton.Width = this.Width;
        }

        private void UpdateSelectedTabFilter()
        {
            if (null == m_tabContext.SelectedTab)
                return;
            DataTable dataTable = targetDataMap[m_tabContext.SelectedTab.Name];
            String filterString = "";
            int boxIndex = 0;
            foreach (CheckBoxComboBoxItem i in this.filterControl.CheckBoxItems)
            {
                if (i.CheckState == CheckState.Checked)
                {
                    if ("" != filterString)
                        filterString += " OR ";
                    filterString += "Type LIKE '" + m_dataTags[boxIndex] + "'";
                }
                boxIndex++;
            }
            dataTable.DefaultView.RowFilter = filterString;
        }

        private void SelectTabHandler(object sender, System.Windows.Forms.TabControlEventArgs e)
        {
            UpdateSelectedTabFilter();
        }

        private void ChangedFilterHandler(object sender, System.EventArgs e)
        {
            if (m_tabContext.TabCount>0 && null != m_tabContext.SelectedTab)
            {
                UpdateSelectedTabFilter();
            }
        }
        private void ChangedHistoryHandler(object sender, System.EventArgs e)
        {
            if (null != this.adressHistory.Text &&
                lastHistoryEntry != this.adressHistory.Text)
            {
                lastHistoryEntry = this.adressHistory.Text;
                Init(this.adressHistory.Text);
            }
        }
        private void ChangedHistoryKeyHandler(object sender, System.Windows.Forms.KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Return &&
                null != this.adressHistory.Text &&
                lastHistoryEntry != this.adressHistory.Text)
            {
                lastHistoryEntry = this.adressHistory.Text;
                Init(this.adressHistory.Text);
            }
        }

        private void ChangedSourcerHandler(object sender, System.Windows.Forms.DataGridViewBindingCompleteEventArgs e)
        {
            DataGridView dataGridView1 = (sender as DataGridView);
            if (dataGridView1.ColumnCount > 1)
            {
                dataGridView1.Columns[0].FillWeight = 10;
                dataGridView1.Columns[1].FillWeight = 10;
                dataGridView1.Columns[2].FillWeight = 70;
                dataGridView1.Columns[3].FillWeight = 10;
                dataGridView1.Columns[2].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            }
        }

        private void RowsAddedHandler(object sender, System.Windows.Forms.DataGridViewRowsAddedEventArgs e)
        {
            DataGridView dataGridView1 = (sender as DataGridView);
            foreach (DataGridViewRow dr in dataGridView1.Rows)
            {
                String msg = dr.Cells[2].Value.ToString();
                DataGridViewCell dc = dr.Cells[0];
                //dr.DefaultCellStyle.BackColor = GetColorFromMsgType(dc.Value.ToString());
                Color cellColor = GetColorFromMsgType(dc.Value.ToString());
                dc.Style.BackColor = cellColor;
            }
        }

        private void RowSelectedHandler(object sender, EventArgs e)
        {
            DataGridView dataGridView1 = (sender as DataGridView);
            if (dataGridView1.SelectedRows.Count < 1)
                return;
            List<string> contextList = new List<string>();
            foreach (DataGridViewRow dr in dataGridView1.SelectedRows)
                contextList.Add(dr.Cells[dr.Cells.Count - 1].Value.ToString());
            ContextArgs args = new ContextArgs(contextList.ToArray());
            if (null != MessageSelected)
                MessageSelected(sender, args);
        }

        public void MessageSelectedHandler(object sender, ContextArgs e)
        {
            foreach (string s in e.Message())
                Console.WriteLine(s);
        }

        public void OnLogFileChanged(object sender, FileSystemEventArgs e)
        {
            Refresh();
        }

        internal class cPredClass
        {
            public static string compareString = "";
            public static bool PredFunc(string s) { return s == compareString; }
        };

        public void SendMailHandler(object sender, EventArgs e)
        {
            string[] rec = {"RSGEDI Tools"};
            List<string> att = new List<string>();
            att.Add(m_filepath);
            string messagebody = "See attachment\n\n\n";
            string subject = "Universal Log Report from " + Environment.UserName;
            //if Message Contains a Path, add it to the email.
            foreach (KeyValuePair<string, DataTable> p in targetDataMap)
            {
                int msgindex = p.Value.Columns.IndexOf("Message");
                foreach (DataRow r in p.Value.Rows)
                {
                    String contextString = r.ItemArray[msgindex].ToString();
                    if (contextString.Length >= 260) // windows limit for valid paths... no f***ng comment.
                        continue;
                    try
                    {
                        contextString = Path.GetFullPath(contextString);
                        cPredClass.compareString = contextString;
                        if (File.Exists(contextString) && null == att.Find(cPredClass.PredFunc))
                            att.Add(contextString);
                    }
                    //catch (System.IO.PathTooLongException)
                    //{ }
                    //catch (System.NotSupportedException)
                    //{ }
                    //catch (System.ArgumentException)
                    //{ }
                    catch (System.Exception)
                    {
                        //messagebody += "D'oh! Some error with the file collecting appeared:" + contextString + "\n";
                    }
                }
            }
            OfficeInteraction.CreateEmail(rec, subject, messagebody, att.ToArray());
        }
        #endregion
    }
}
