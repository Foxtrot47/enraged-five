//
// File: uiCommandAction.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of uiCommandAction.cs class
//

using System;
using System.Drawing;
using System.Windows.Forms;

using RSG.Base.Command;
using RSG.Base.Collections;

namespace RSG.Base.Forms.Actions
{
    
    /// <summary>
    /// Abstract Command Action
    /// </summary>
    /// User-defined Command Actions should subclass this and need only implement
    /// the Execute abstract method.  The implementation of this is responsible 
    /// for carrying out the action consequences.
    /// 
    /// When the action has executed the ExecutedEvent event is raised.  Applications
    /// can connect to this to be notified when the action is executed.
    ///
    public abstract class uiCommandAction : uiAction
    {
        #region Events
        /// <summary>
        /// Event raised after the Command Action has executed
        /// </summary>
        public event EventHandler<EventArgs> ExecutedEvent;
        #endregion // Events

        #region Abstract Methods
        /// <summary>
        /// Execute the Command Action
        /// </summary>
        public abstract void Execute();
        #endregion // Abstract Methods

        #region Constructors
        /// <summary>
        /// Constructor (text, image and transparent image colour)
        /// </summary>
        /// <param name="sText">Text to display on UI items</param>
        /// <param name="pImage">Image to display on UI items</param>
        /// <param name="transparentColour">Image transparent colour</param>
        public uiCommandAction(String sText, Image pImage, Color transparentColour)
            : base(sText, pImage, transparentColour)
        {
        }

        /// <summary>
        /// Constructor (text, image, transparent image colour and tooltip)
        /// </summary>
        /// <param name="sText">Text to display on UI items</param>
        /// <param name="pImage">Image to display on UI items</param>
        /// <param name="transparentColour">Image transparent colour</param>
        /// <param name="sToolTip">Tooltip text</param>
        public uiCommandAction(String sText, Image pImage, Color transparentColour, String sToolTip)
            : base(sText, pImage, transparentColour, sToolTip)
        {
        }

        /// <summary>
        /// Constructor (text, image, transparent image colour, tooltip and shortcut keys)
        /// </summary>
        /// <param name="sText">Text to display on UI items</param>
        /// <param name="pImage">Image to display on UI items</param>
        /// <param name="transparentColour">Image transparent colour</param>
        /// <param name="sToolTip">Tooltip text</param>
        /// <param name="keys">Shortcut key</param>
        public uiCommandAction(String sText, Image pImage, Color transparentColour, String sToolTip, Keys keys)
            : base(sText, pImage, transparentColour, sToolTip, keys)
        {
        }
        #endregion // Constructors

        #region uiAction Overridden Methods
        public override void AddTo(ToolStrip ctrl)
        {
            // See if we have already been added to this toolstrip
            foreach (Pair<ToolStrip, ToolStripButton> tsBar in base.ToolStripButtons)
            {
                if (tsBar.First == ctrl)
                    throw new exActionException("This action has already been added to this menu.");
            }

            // Otherwise we can add our action to the toolstrip
            ToolStripButton tbNewButton = new ToolStripButton(base.Text, base.Image, CommandActionEventHandler);
            tbNewButton.ImageTransparentColor = base.TransparentColour;
            base.ToolStripButtons.Add(new Pair<ToolStrip, ToolStripButton>(ctrl, tbNewButton));
            ctrl.Items.Add(tbNewButton);
        }

        public override void AddTo(ToolStripMenuItem ctrl)
        {
            // See if we have already been added to this menu
            foreach (Pair<ToolStripMenuItem, ToolStripMenuItem> miItem in base.MenuItems)
            {
                if (miItem.First == ctrl)
                    throw new exActionException("This action has already been added to this menu.");
            }

            // Otherwise we can add our action to the menu
            ToolStripMenuItem miNewItem = new ToolStripMenuItem(base.Text, base.Image, CommandActionEventHandler, base.ShortcutKey);
            miNewItem.ImageTransparentColor = base.TransparentColour;
            base.MenuItems.Add(new Pair<ToolStripMenuItem, ToolStripMenuItem>(ctrl, miNewItem));
            ctrl.DropDownItems.Add(miNewItem);
        }

        public override void AddTo(ContextMenuStrip ctrl)
        {
            ToolStripMenuItem miNewItem = new ToolStripMenuItem(base.Text, base.Image, CommandActionEventHandler, base.ShortcutKey);
            miNewItem.ImageTransparentColor = base.TransparentColour;
            ctrl.Items.Add(miNewItem);
        }

        public override void RemoveFrom(ToolStrip ctrl)
        {
            if (0 == base.ToolStripButtons.Count)
                throw new exActionException("This action has not been added to any ToolStrip control");

            foreach (Pair<ToolStrip, ToolStripButton> miBar in base.ToolStripButtons)
            {
                if (miBar.First == ctrl)
                {
                    ctrl.Items.Remove(miBar.Second);
                    base.ToolStripButtons.Remove(miBar);
                }
            }
        }

        public override void RemoveFrom(ToolStripMenuItem ctrl)
        {
            if (0 == base.MenuItems.Count)
                throw new exActionException("This action has not been added to any Menu control");

            foreach (Pair<ToolStripMenuItem, ToolStripMenuItem> miItem in base.MenuItems)
            {
                if (miItem.First == ctrl)
                {
                    ctrl.DropDownItems.Remove(miItem.Second);
                    base.MenuItems.Remove(miItem);
                }
            }
        }

        public override void RemoveFrom(ContextMenuStrip ctrl)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Command Action : Update
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// The default Update method does nothing.  Application actions should
        /// override this behaviour to typically enable/disable the actions
        /// based on number of items selected or type of items selected.
        /// 
        public override void Update(Object sender, cActionUpdateEventArgs e)
        {
        }
        #endregion // uiAction Overridden Methods

        #region UI Event Handling
        /// <summary>
        /// Invoke our subclass' Execute method, then invoke our ExecutedEvent event
        /// </summary>
        /// This method is attached to all UI controls that represent this
        /// Command Action.  This method handles invoking the user-defined code
        /// in the subclass instance of the uiCommandAction class.
        /// 
        private void CommandActionEventHandler(Object sender, EventArgs e)
        {
            Execute();

            if (null != ExecutedEvent)
                ExecutedEvent(this, new EventArgs());
        }
        #endregion // UI Event Handling
    }

} // End of RsBase namespace

// End of file
