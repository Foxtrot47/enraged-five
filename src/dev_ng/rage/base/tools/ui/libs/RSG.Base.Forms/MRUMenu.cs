//
// File: cMRUMenu.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of cMRUMenu.cs class
//

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace RSG.Base.Forms
{

    #region MRU Menu Class
    /// <summary>
    /// Most-Recently-Used menu
    /// </summary>
    public class cMRUMenu
    {
        #region Constants
        private readonly int sC_s_cnt_DefaultMaxEntries = 4;
        #endregion

        #region Delegates and Events
        public event EventHandler<cMRUMenuItemClickEventArgs> MRUItemClick;
        #endregion

        #region Properties and Associated Member Data
        /// <summary>
        /// ToolStripMenuItem to attach the MRU list to
        /// </summary>
        /// Child ToolStripMenuItems are attached to this
        public ToolStripMenuItem MenuItem
        {
            get { return m_MenuItem; }
            internal set { m_MenuItem = value; }
        }
        private ToolStripMenuItem m_MenuItem;

        public List<string> RecentlyUsedFiles
        {
            get { return m_RecentlyUsedFiles; }
            internal set { m_RecentlyUsedFiles = value; }
        }
        private List<string> m_RecentlyUsedFiles;

        public int NumberOfEntries
        {
            get { return m_nNumEntries; }
            internal set { m_nNumEntries = value; }
        }
        private int m_nNumEntries;
        #endregion

        #region Constructor
        /// <summary>
        /// 
        /// </summary>
        /// <param name="menuitem"></param>
        public cMRUMenu(ToolStripMenuItem menuitem)
        {
            Initialise(menuitem, sC_s_cnt_DefaultMaxEntries);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="menuitem"></param>
        /// <param name="cnt_MaxEntries"></param>
        public cMRUMenu(ToolStripMenuItem menuitem, int cnt_MaxEntries)
        {
            Initialise(menuitem, cnt_MaxEntries);
        }
        #endregion

        #region Controller Methods
        /// <summary>
        /// Add a file to the Most Recently Used list
        /// </summary>
        /// <param name="sFilename"></param>
        public void AddFile(string sFilename)
        {
            // If we are maxed then remove the first entry added
            if (RecentlyUsedFiles.Count == NumberOfEntries)
                RecentlyUsedFiles.RemoveAt(0);

            // If file already in list remove (so its added as most recent)
            int nPrevIndex = RecentlyUsedFiles.IndexOf(sFilename, 0);
            if (-1 != nPrevIndex)
                RecentlyUsedFiles.RemoveAt(nPrevIndex);

            RecentlyUsedFiles.Add(sFilename);

            Update();
        }

        /// <summary>
        /// Load MRU entries from a string of ";" separated filenames
        /// </summary>
        /// <param name="sFiles"></param>
        public void Load(string sFiles)
        {
            string[] asFiles = sFiles.Split(';');
            foreach (string sFile in asFiles)
            {
                if ((string.Empty != sFile) && (File.Exists(sFile)))
                    RecentlyUsedFiles.Add(sFile);
            }

            Update();
        }

        /// <summary>
        /// MRU entries encoded as a single string using ";" to separate filenames
        /// </summary>
        /// <returns></returns>
        public string Save()
        {
            string sResult = string.Empty;
            foreach (string sFile in RecentlyUsedFiles)
            {
                sResult += sFile + ";";
            }
            return sResult;
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Initialisation method
        /// </summary>
        /// <param name="menuitem"></param>
        /// <param name="lsMRUFiles"></param>
        /// <param name="cnt_MaxEntries"></param>
        private void Initialise(ToolStripMenuItem menuitem, int nMaxEntries)
        {
            if (null == menuitem)
                throw new ArgumentNullException("menuitem");

            MenuItem = menuitem;
            RecentlyUsedFiles = new List<string>();
            NumberOfEntries = nMaxEntries;

            Update();
        }

        /// <summary>
        /// Private method to update our managed menu items
        /// </summary>
        private void Update()
        {
            // Clear all menu items
            MenuItem.DropDownItems.Clear();

            // Recreate
            for (int nIndex = (RecentlyUsedFiles.Count-1); nIndex >= 0; --nIndex)
            {
                string sFile = RecentlyUsedFiles[nIndex];
                cMRUMenuItem item = new cMRUMenuItem(sFile, null, OnMenuItemClick);
                MenuItem.DropDownItems.Add(item);
            }
            // Enable/disable our menuitem depending on if we have items to display
            MenuItem.Enabled = (RecentlyUsedFiles.Count > 0);
        }

        /// <summary>
        /// Local event handler installed for each Menu Item
        /// </summary>
        /// This handler invokes the application defined event handler with event args
        /// that easily specify the full filename clicked.
        /// <param name="sender">Menu item that was clicked</param>
        /// <param name="e">Event arguments</param>
        private void OnMenuItemClick(object sender, EventArgs e)
        {
            if ((sender is cMRUMenuItem) && (null != MRUItemClick))
            {
                cMRUMenuItem item = sender as cMRUMenuItem;

                if (null != MRUItemClick)
                    MRUItemClick(sender, new cMRUMenuItemClickEventArgs(item.Filename));
            }
        }
        #endregion
    }
    #endregion

    #region MRU Menu Item Click EventArgs Class
    public class cMRUMenuItemClickEventArgs : EventArgs
    {
        #region Properties and Associated Member Data
        /// <summary>
        /// Fully qualified filename of the associated menu item
        /// </summary>
        public string Filename
        {
            get { return m_sFilename; }
            internal set { m_sFilename = value; }
        }
        private string m_sFilename;
        #endregion

        #region Constructor
        public cMRUMenuItemClickEventArgs(string sFilename)
        {
            Filename = sFilename;
        }
        #endregion
    };
    #endregion

    #region MRU Menu Item Class
    /// <summary>
    /// ToolStripMenuItem representing a file
    /// </summary>
    internal class cMRUMenuItem : ToolStripMenuItem
    {
        #region Properties and Associated Member Data
        /// <summary>
        /// The fully qualified path name to the file this menu item relates to
        /// </summary>
        /// This can be used by the application to open the file
        public string Filename 
        {
            get { return m_sFilename; }
            internal set { m_sFilename = value; }
        }
        private string m_sFilename;

        /// <summary>
        /// The name string being displayed on this menu item
        /// </summary>
        public string DisplayFilename
        {
            get { return RSG.Base.IO.cFileUtil.ShortenPathname(Filename, MaxShortenPathLength); }
        }

        /// <summary>
        /// Number of characters for shorten path length (default 48, min 40)
        /// </summary>
        public int MaxShortenPathLength
        {
            get { return m_nMaxShortenPathLength; }
            set { m_nMaxShortenPathLength = (value < 40) ? 40 : value; }
        }
        private int m_nMaxShortenPathLength;
        #endregion

        #region Constructors
        public cMRUMenuItem(string sFilename)
            : base(Path.GetFileName(sFilename))
        {
            MaxShortenPathLength = 48;
            Filename = sFilename;
            Text = DisplayFilename;
        }

        public cMRUMenuItem(string sFilename, System.Drawing.Image image)
            : base(Path.GetFileName(sFilename), image)
        {
            MaxShortenPathLength = 48;
            Filename = sFilename;
            Text = DisplayFilename;
        }

        public cMRUMenuItem(string sFilename, System.Drawing.Image image, EventHandler evHandler)
            : base(Path.GetFileName(sFilename), image, evHandler)
        {
            MaxShortenPathLength = 48;
            Filename = sFilename;
            Text = DisplayFilename;
        }

        public cMRUMenuItem(string sFilename, System.Drawing.Image image, EventHandler evHandler, Keys shortcut)
            : base(Path.GetFileName(sFilename), image, evHandler, shortcut)
        {
            MaxShortenPathLength = 48;
            Filename = sFilename;
            Text = DisplayFilename;
        }
        #endregion
    };
    #endregion

} // End of RSG.Base.Forms namespace


// End of file
