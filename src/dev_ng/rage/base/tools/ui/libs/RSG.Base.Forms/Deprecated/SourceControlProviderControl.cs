using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Xml.Serialization;
using System.Windows.Forms;

using RSG.Base.Forms.SCM;

namespace RSG.Base.Forms
{
    [Obsolete("Use RSG.Interop.Perforce/RSG.Editor.Controls.Perforce instead")]
    public partial class SourceControlProviderControl : UserControl
    {
        public SourceControlProviderControl()
        {
            InitializeComponent();
        }

        #region Variables
        private rageSourceControlProvider m_currentProvider = null;
        private bool m_dispatchEvents = true;
        #endregion

        #region Properties
        [Browsable(false), DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public List<rageSourceControlProvider> Providers
        {
            get
            {
                SetProviderFromUI();

                List<rageSourceControlProvider> providers = new List<rageSourceControlProvider>();
                foreach ( rageSourceControlProvider provider in this.providerComboBox.Items )
                {
                    providers.Add( provider );
                }

                return providers;
            }
            set
            {
                m_dispatchEvents = false;

                this.providerComboBox.Items.Clear();

                if ( value != null )
                {
                    foreach ( rageSourceControlProvider provider in value )
                    {
                        this.providerComboBox.Items.Add( provider );
                    }
                }

                m_dispatchEvents = true;
            }
        }

        [Browsable( false ), DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public int SelectedIndex
        {
            get
            {
                return this.providerComboBox.SelectedIndex;
            }
            set
            {
                this.providerComboBox.SelectedIndex = value;
            }
        }

        [Browsable( false ), DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public rageSourceControlProvider SelectedProvider
        {
            get
            {
                SetProviderFromUI();

                return m_currentProvider;
            }
            set
            {
                m_dispatchEvents = false;

                if ( value != null )
                {
                    this.providerComboBox.SelectedIndex = this.providerComboBox.Items.IndexOf( value );
                }
                else
                {
                    this.providerComboBox.SelectedIndex = -1;
                }

                m_dispatchEvents = true;
            }
        }
        #endregion

        #region Event
        public event EventHandler SettingChanged;
        #endregion

        #region Event Dispatchers
        protected void OnSettingChanged()
        {
            if ( !m_dispatchEvents )
            {
                return;
            }

            if ( this.SettingChanged != null )
            {
                if ( this.InvokeRequired )
                {
                    this.Invoke( this.SettingChanged, new object[] { this, EventArgs.Empty } );
                }
                else
                {
                    this.SettingChanged( this, EventArgs.Empty );
                }
            }
        }
        #endregion

        #region Event Handlers
        private void providerComboBox_SelectedIndexChanged( object sender, EventArgs e )
        {
            SetProviderFromUI();

            m_currentProvider = this.providerComboBox.SelectedItem as rageSourceControlProvider;

            SetUIFromProvider();

            OnSettingChanged();
        }
        
        private void TextBox_TextChanged( object sender, EventArgs e )
        {
            OnSettingChanged();
        }
        #endregion

        #region Private Functions
        private void SetUIFromProvider()
        {
            if ( m_currentProvider != null )
            {
                this.loginUsernameTextBox.Enabled = m_currentProvider.HasLoginOrUsername;
                this.loginUsernameTextBox.Text = m_currentProvider.LoginOrUsername;

                this.projectWorkspaceTextBox.Enabled = m_currentProvider.HasProjectOrWorkspace;
                this.projectWorkspaceTextBox.Text = m_currentProvider.ProjectOrWorkspace;

                this.serverPortTextBox.Enabled = m_currentProvider.HasServerOrPort;
                this.serverPortTextBox.Text = m_currentProvider.ServerOrPort;
            }
            else
            {
                this.loginUsernameTextBox.Enabled = false;
                this.loginUsernameTextBox.Text = string.Empty;

                this.projectWorkspaceTextBox.Enabled = false;
                this.projectWorkspaceTextBox.Text = string.Empty;

                this.serverPortTextBox.Enabled = false;
                this.serverPortTextBox.Text = string.Empty;
            }
        }

        private void SetProviderFromUI()
        {
            if ( m_currentProvider != null )
            {
                m_currentProvider.LoginOrUsername = this.loginUsernameTextBox.Text.Trim();
                m_currentProvider.ProjectOrWorkspace = this.projectWorkspaceTextBox.Text.Trim();
                m_currentProvider.ServerOrPort = this.serverPortTextBox.Text.Trim();
            }
        }
        #endregion
    }
}
