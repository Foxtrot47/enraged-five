//
// File: uiToolStripCheckBox.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of uiToolStripCheckBox.cs class
//

using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace RSG.Base.Controls
{

    /// <summary>
    /// ToolStrip CheckBox Control
    /// </summary>
    public class uiToolStripCheckBox : ToolStripControlHost
    {
        #region Events
        /// <summary>
        /// Event raised when CheckBox's Checked status changes
        /// </summary>
        public event EventHandler CheckedChanged;
        #endregion // Events

        #region Properties and Associated Member Data
        /// <summary>
        /// Access to hosted CheckBox Control
        /// </summary>
        public CheckBox CheckBoxControl
        {
            get { return (this.Control as CheckBox); }
        }
        #endregion // Properties and Associated Member Data

        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public uiToolStripCheckBox()
            : base(new CheckBox())
        {
        }
        #endregion // Constructor

        #region ToolStripControlHost Overridden Methods
        protected override void OnSubscribeControlEvents(Control control)
        {
            base.OnSubscribeControlEvents(control);
            CheckBox ctrl = (CheckBox)control;
            
            // Event subscriptions
            ctrl.CheckedChanged += new EventHandler(OnCheckedChanged);
        }

        protected override void  OnUnsubscribeControlEvents(Control control)
        {
 	        base.OnUnsubscribeControlEvents(control);
            CheckBox ctrl = (CheckBox)control;
           
            // Event unsubscriptions
            ctrl.CheckedChanged -= new EventHandler(OnCheckedChanged);
        }
        #endregion // ToolStripControlHost Overridden Methods

        #region Private Methods
        /// <summary>
        /// Raise the CheckedChanged event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnCheckedChanged(Object sender, EventArgs e)
        {
            if (null != this.CheckedChanged)
                CheckedChanged(this, e);
        }
        #endregion // Private Methods
    }

} // End of RSG.Base.Controls namespace

// End of file
