//
// File: uiActionManager.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of uiActionManager class
//

using System;
using System.Collections.Generic;
using Diag = System.Diagnostics;
using System.Windows.Forms;

using RSG.Base.Command;

namespace RSG.Base.Forms.Actions
{

    /// <summary>
    /// Action Manager
    /// </summary>
    /// Typically a View owns an Action Manager object.
    /// 
    public class uiActionManager
    {
        #region Properties and Associated Member Data
        /// <summary>
        /// Managed Command Actions
        /// </summary>
        public List<uiCommandAction> CommandActions
        {
            get { return m_CommandActions; }
            private set { m_CommandActions = value; }
        }
        private List<uiCommandAction> m_CommandActions;

        /// <summary>
        /// Managed Primary Toggle Actions
        /// </summary>
        public List<uiToggleAction> PrimaryToggleActions
        {
            get { return m_PrimaryToggleActions; }
            private set { m_PrimaryToggleActions = value; }
        }
        private List<uiToggleAction> m_PrimaryToggleActions;
        
        /// <summary>
        /// Managed Secondary Toggle Actions
        /// </summary>
        public List<uiToggleAction> SecondaryToggleActions
        {
            get { return m_SecondaryToggleActions; }
            private set { m_SecondaryToggleActions = value; }
        }
        private List<uiToggleAction> m_SecondaryToggleActions;

        /// <summary>
        /// Currently active primary toggle action (can be null)
        /// </summary>
        public uiToggleAction ActivePrimaryToggleAction
        {
            get { return m_ActivePrimaryToggleAction; }
            private set { m_ActivePrimaryToggleAction = value; }
        }
        private uiToggleAction m_ActivePrimaryToggleAction;

        /// <summary>
        /// Default primary toggle action (can be null)
        /// </summary>
        public uiToggleAction DefaultPrimaryToggleAction
        {
            get { return m_DefaultPrimaryToggleAction; }
            set 
            {
                m_DefaultPrimaryToggleAction = value;
                ActivatePrimaryToggleAction(m_DefaultPrimaryToggleAction);
            }
        }
        private uiToggleAction m_DefaultPrimaryToggleAction;
        #endregion // Properties and Associated Member Data

        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public uiActionManager(cCommandManager cmdmgr)
        {
            System.Diagnostics.Debug.Assert(null != cmdmgr);
            if (null == cmdmgr)
                throw new ArgumentNullException("cmdmgr");

            this.CommandActions = new List<uiCommandAction>();
            this.PrimaryToggleActions = new List<uiToggleAction>();
            this.SecondaryToggleActions = new List<uiToggleAction>();
            this.ActivePrimaryToggleAction = null;
        }
        #endregion // Constructor

        #region Controller Methods
        /// <summary>
        /// Add command action
        /// </summary>
        /// <param name="act"></param>
        public void Add(uiCommandAction act)
        {
            this.CommandActions.Add(act);
            act.CommandEvent += this.OnCommand;
            act.ExecutedEvent += this.OnCommandActionExecuted;
        }
        
        /// <summary>
        /// Add toggle action
        /// </summary>
        /// <param name="act"></param>
        public void Add(uiToggleAction act, etToggleActionType type)
        {
            switch (type)
            {
                case etToggleActionType.Primary:
                    this.PrimaryToggleActions.Add(act);
                    break;
                case etToggleActionType.Secondary:
                    this.SecondaryToggleActions.Add(act);
                    break;
            }
            act.Type = type;
            act.CommandEvent += this.OnCommand;
            act.ToggledEvent += this.OnToggleActionToggled;
        }

        /// <summary>
        /// Remove command action
        /// </summary>
        /// <param name="act"></param>
        public void Remove(uiCommandAction act)
        {
            if (!this.CommandActions.Contains(act))
                throw new exActionException("Command action not found.");

            act.CommandEvent -= this.OnCommand;
            act.ExecutedEvent -= this.OnCommandActionExecuted;
            this.CommandActions.Remove(act);
        }

        /// <summary>
        /// Remove toggle action
        /// </summary>
        /// <param name="act"></param>
        public void Remove(uiToggleAction act)
        {
            if (!(this.PrimaryToggleActions.Contains(act) || 
                  this.SecondaryToggleActions.Contains(act)))
                throw new exActionException("Toggle action not found.");

            act.CommandEvent -= this.OnCommand;
            act.ToggledEvent -= this.OnToggleActionToggled;

            // Remove references if active toggle and/or default
            if (act == this.DefaultPrimaryToggleAction)
                this.DefaultPrimaryToggleAction = null;
            if (act == this.ActivePrimaryToggleAction)
                this.ActivePrimaryToggleAction = null;

            if (this.PrimaryToggleActions.Contains(act))
                this.PrimaryToggleActions.Remove(act);
            else
                this.SecondaryToggleActions.Remove(act);
        }
        #endregion // Controller Methods

        #region Private Methods
        /// <summary>
        /// Activate specified primary toggle action
        /// </summary>
        /// <param name="action">uiToggleAction to activate</param>
        private void ActivatePrimaryToggleAction(uiToggleAction action)
        {
            Diag.Debug.Assert(this.PrimaryToggleActions.Contains(action),
                "Primary Toggle Action list does not contain action.  ActionManager error.");

            // Deactivate previously active primary toggle action
            if (null != this.ActivePrimaryToggleAction)
            {
                this.ActivePrimaryToggleAction.UpdateUIElements();
                this.ActivePrimaryToggleAction.ToggledEvent -= this.OnToggleActionToggled;
                this.ActivePrimaryToggleAction.Toggled = false;
                this.ActivePrimaryToggleAction.ToggledEvent += this.OnToggleActionToggled;
                this.ActivePrimaryToggleAction = null;
            }

            this.ActivePrimaryToggleAction = action;
            this.ActivePrimaryToggleAction.ToggledEvent -= this.OnToggleActionToggled;
            this.ActivePrimaryToggleAction.Toggled = true;
            this.ActivePrimaryToggleAction.ToggledEvent += this.OnToggleActionToggled;
            this.ActivePrimaryToggleAction.UpdateUIElements();
        }
        #endregion // Private Methods

        /// The owner view of the Action Manager should connect its event handlers
        /// to the following methods that it supports.  When these events happen 
        /// they will consequently be filtered down to the active primary toggle
        /// action and then any active secondary toggle actions.
        #region View Event Handlers
        public void OnKeyDown(Object sender, KeyEventArgs e)
        {
            // Need to determine here any key events that are activating
            // any of our toggle or command actions.
            foreach (uiToggleAction action in this.PrimaryToggleActions)
            {
                if ((action != this.ActivePrimaryToggleAction) && 
                    (action.ShortcutKey == e.KeyData))
                {
                    this.ActivePrimaryToggleAction = action;

                    RSG.Base.Logging.Log.Log__Debug(
                        String.Format("Activating Primary Toggle Action: {0}.", action.Text));
                }
            }

            if (null != this.ActivePrimaryToggleAction)
                this.ActivePrimaryToggleAction.KeyDown(this, e);

            foreach (uiToggleAction secondary in this.SecondaryToggleActions)
                secondary.KeyDown(this, e);
        }

        public void OnKeyUp(Object sender, KeyEventArgs e)
        {
            // Need to determine here any key events that are deactivating
            // any of our toggle or command actions.
            if ((null != this.ActivePrimaryToggleAction) && 
                (this.ActivePrimaryToggleAction.ShortcutKey == e.KeyData))
            {
                RSG.Base.Logging.Log.Log__Debug(String.Format("Deactivating Primary Toggle Action: {0}.",
                    this.ActivePrimaryToggleAction.Text));

                this.ActivePrimaryToggleAction = null;
            }

            if (null != this.ActivePrimaryToggleAction)
                this.ActivePrimaryToggleAction.KeyUp(this, e);

            foreach (uiToggleAction secondary in this.SecondaryToggleActions)
                secondary.KeyUp(this, e);
        }

        public void OnMouseDown(Object sender, MouseEventArgs e)
        {
            // Need to determine here any mouse events that are activating
            // any of our toggle or command actions.
            foreach (uiToggleAction primary in this.PrimaryToggleActions)
            {
                primary.MouseDown(sender, e);
                bool active = primary.Toggled;
                if (active)
                {
                    this.ActivePrimaryToggleAction = primary;
                    break;
                }
            }

            foreach (uiToggleAction secondary in this.SecondaryToggleActions)
                secondary.MouseDown(this, e);
        }

        public void OnMouseUp(Object sender, MouseEventArgs e)
        {
            // Need to determine here any mouse events that are deactivating
            // any of our toggle or command actions.
            if (null != this.ActivePrimaryToggleAction)
                this.ActivePrimaryToggleAction.MouseUp(this, e);

            if (null != this.ActivePrimaryToggleAction)
            {
                // Deactivate
                if (!this.ActivePrimaryToggleAction.Toggled)
                    this.ActivePrimaryToggleAction = null;
            }

            foreach (uiToggleAction secondary in this.SecondaryToggleActions)
                secondary.MouseUp(this, e);
        }

        public void OnMouseMove(Object sender, MouseEventArgs e)
        {
            if (null != this.ActivePrimaryToggleAction)
                this.ActivePrimaryToggleAction.MouseMove(this, e);

            foreach (uiToggleAction secondary in this.SecondaryToggleActions)
                secondary.MouseMove(this, e);
        }

        public void OnMouseWheel(Object sender, MouseEventArgs e)
        {
            // Need to determine here any mouse event that are activating/deactivating
            // any of our toggle or command actions.
            foreach (uiToggleAction primary in this.PrimaryToggleActions)
                primary.MouseWheel(this, e);

            if (null != this.ActivePrimaryToggleAction)
                this.ActivePrimaryToggleAction.MouseWheel(this, e);

            foreach (uiToggleAction secondary in this.SecondaryToggleActions)
                secondary.MouseWheel(this, e);
        }
        #endregion // View Event Handlers

        #region Action Event Handlers
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnCommand(Object sender, cCommandEventArgs e)
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnCommandActionExecuted(Object sender, EventArgs e)
        {
            uiCommandAction action = (sender as uiCommandAction);
            Diag.Debug.Assert(null != action);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnToggleActionToggled(Object sender, EventArgs e)
        {
            uiToggleAction action = (sender as uiToggleAction);
            Diag.Debug.Assert(null != action, "Invalid Toggle Action.");

            if (action.Toggled)
            {
                // Action has been toggled On
                if (this.PrimaryToggleActions.Contains(action))
                    ActivatePrimaryToggleAction(action);
            }
            else
            {
                // Action has been toggled Off
                if ((null != this.DefaultPrimaryToggleAction) &&
                    (action != this.DefaultPrimaryToggleAction))
                    ActivatePrimaryToggleAction(this.DefaultPrimaryToggleAction);
            }
        }
        #endregion // Action Event Handlers
    }

} // End of RSG.Base.Actions namespace

// End of file
