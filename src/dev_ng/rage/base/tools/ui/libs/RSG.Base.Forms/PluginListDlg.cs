//
// File: PluginListDlg.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of PluginListDlg.cs class
//

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using RSG.Base.Plugins;

namespace RSG.Base.Forms
{
    
    /// <summary>
    /// 
    /// </summary>
    public partial class PluginListDlg : Form
    {
        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public PluginListDlg()
        {
            InitializeComponent();
        }
        #endregion // Constructor

        #region Controller Methods
        /// <summary>
        /// OK Button Click Event Handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnOK_Click(object sender, EventArgs e)
        {
            Close();
        }

        /// <summary>
        /// Set list of plugins
        /// </summary>
        /// <param name="plugins"></param>
        public void SetPlugins(List<IPlugin> plugins)
        {
            uIPluginListView1.Plugins = plugins;
        }
        #endregion // Controller Methods

    }

} // End of RSG.Base.Forms namespace

// End of file
