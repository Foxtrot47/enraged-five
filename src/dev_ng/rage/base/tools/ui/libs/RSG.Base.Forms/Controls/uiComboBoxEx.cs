//
// File: uiComboBoxEx.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of uiComboBoxEx.cs class
//
// Reference: http://www.csharphelp.com/archives/archive280.html
//

using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace RSG.Base.Controls
{
    
    /// <summary>
    /// Enhanced ComboBox Control with Image Support
    /// </summary>
    public class uiComboBoxEx : ComboBox
    {
        #region Properties and Associated Member Data
        /// <summary>
        /// ImageList to store images for combobox items
        /// </summary>
        public ImageList ImageList
        {
            get { return m_ImageList; }
            set { m_ImageList = value; }
        }
        private ImageList m_ImageList;
        #endregion // Properties and Associated Member Data

        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public uiComboBoxEx()
        {
            this.DrawMode = DrawMode.OwnerDrawFixed;
        }

        /// <summary>
        /// Constructor, specifying ImageList
        /// </summary>
        /// <param name="imageList">ImageList</param>
        public uiComboBoxEx(ImageList imageList)
        {
            ImageList = imageList;
            this.DrawMode = DrawMode.OwnerDrawFixed;
        }
        #endregion // Constructor

        #region Overridden Methods
        /// <summary>
        /// Draw ComboBox Item
        /// </summary>
        /// <param name="e"></param>
        protected override void OnDrawItem(DrawItemEventArgs ea)
        {
            ea.DrawBackground();
            ea.DrawFocusRectangle();

            uiComboBoxExItem item;
            Size imageSize = ImageList.ImageSize;
            Rectangle bounds = ea.Bounds;

            try
            {
                item = (uiComboBoxExItem)Items[ea.Index];

                if (item.ImageIndex != -1)
                {
                    ImageList.Draw(ea.Graphics, bounds.Left, bounds.Top, item.ImageIndex);
                    ea.Graphics.DrawString(item.Text, ea.Font, new SolidBrush(ea.ForeColor), bounds.Left + imageSize.Width, bounds.Top);
                }
                else
                {
                    ea.Graphics.DrawString(item.Text, ea.Font, new SolidBrush(ea.ForeColor), bounds.Left, bounds.Top);
                }
            }
            catch
            {
                if (ea.Index != -1)
                {
                    ea.Graphics.DrawString(Items[ea.Index].ToString(), ea.Font, new SolidBrush(ea.ForeColor), bounds.Left, bounds.Top);
                }
                else
                {
                    ea.Graphics.DrawString(Text, ea.Font, new SolidBrush(ea.ForeColor), bounds.Left, bounds.Top);
                }
            }

            base.OnDrawItem(ea);
        }
        #endregion // Overridden Methods
    }

    /// <summary>
    /// 
    /// </summary>
    class uiComboBoxExItem
    {
        #region Properties and Associated Member Data
        public string Text
        {
            get { return _text; }
            set { _text = value; }
        }
        private string _text;

        public int ImageIndex
        {
            get { return _imageIndex; }
            set { _imageIndex = value; }
        }
        private int _imageIndex;
        #endregion // Properties and Associated Member Data

        #region Constructors
        public uiComboBoxExItem()
            : this("")
        {
        }

        public uiComboBoxExItem(string text)
            : this(text, -1)
        {
        }

        public uiComboBoxExItem(string text, int imageIndex)
        {
            _text = text;
            _imageIndex = imageIndex;
        }
        #endregion // Constructors

        #region Controller Methods
        public override string ToString()
        {
            return _text;
        }
        #endregion // Controller Methods
    }

} // End of RSG.Base.Controls namespace

// End of file
