using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Media;
using System.Text;
using System.Windows.Forms;

using RSG.Base.Command;
using RSG.Base.IO;
using System.DirectoryServices;
using System.Reflection;
using System.Runtime.InteropServices;
using RSG.Base.Logging;
using RSG.Interop.Microsoft.Office.Outlook;
using RSG.Interop.Microsoft.Office;
using System.Globalization;

namespace RSG.Base.Forms
{
    [Obsolete("DEAD: unhandled exception dialog.", false)]
    public partial class UnhandledExceptionDialog : Form
    {
        private readonly ExceptionFormatter _formatter;

        protected UnhandledExceptionDialog()
        {
            InitializeComponent();
        }

        public UnhandledExceptionDialog( string applicationTitle, List<string> attachments, 
            string toEmailAddress, Exception e )
            : this()
        {
            Console.WriteLine( "UnhandledExceptionDialog" );

            m_applicationTitle = applicationTitle;
            m_attachments = attachments;
            m_toEmailAddress = toEmailAddress;
            m_exception = e;

            StringBuilder s = new StringBuilder();
            s.Append("An unhandled exception occurred in the program ");
            s.Append(Path.GetFileNameWithoutExtension(Application.ExecutablePath));
            s.Append(".\n   Path = ");
            s.Append(Application.ExecutablePath);
            s.Append("\n   Version = ");
            s.Append(System.Reflection.Assembly.GetEntryAssembly().GetName().Version.ToString());
            s.Append("\n   Build Date = ");
            s.Append(GetBuildDateTime().ToString());
            s.Append("\n   CommandLine Arguments =");

            string[] args = Environment.GetCommandLineArgs();
            for ( int i = 1; i < args.Length; ++i )
            {
                s.Append( " " );
                s.Append( args[i] );
            }
            s.Append("\n\n");

            _formatter = new ExceptionFormatter(e);
            foreach (String line in _formatter.Format())
            {
                s.AppendLine(line);
            }
            m_message = s.ToString();

            Console.WriteLine( "UnhandledExceptionDialog" );
            Console.WriteLine( m_message );
        }

        #region Variables 
        private readonly string m_applicationTitle;
        private List<string> m_attachments;
        private readonly Exception m_exception;

        private readonly string m_toEmailAddress;
        private readonly string m_message;
        #endregion

        #region Properties
        public string ApplicationTitle
        {
            get
            {
                return m_applicationTitle;
            }
        }
        #endregion

        #region Public Functions
        /// <summary>
        /// Sends the error report to the email recipient(s).  If anything goes wrong, another exception will be thrown.
        /// </summary>
        public void SendErrorReport()
        {
            // Set up the email body.
            StringBuilder applicationBody = new StringBuilder();
            applicationBody.Append(Environment.UserName);
            applicationBody.Append(" on ");
            applicationBody.Append(Environment.MachineName);
            applicationBody.Append("\r\n\r\n");
            applicationBody.Append(m_message);

            // Set up the attachments next.
            if ((m_attachments != null) && (m_attachments.Count > 0))
            {
                // try to zip it up
                string zipExecutable = Path.GetFullPath(Path.Combine(Path.GetDirectoryName(Application.ExecutablePath), @"..\zip.exe"));
                try
                {
                    if (!File.Exists(zipExecutable))
                    {
                        zipExecutable = Path.GetFullPath(Path.Combine(Path.GetDirectoryName(Application.ExecutablePath), @"zip.exe"));
                        if (!File.Exists(zipExecutable))
                        {
                            zipExecutable = null;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                    zipExecutable = null;
                }

                bool zipped = false;
                if (zipExecutable != null)
                {
                    string zipFilename = Path.Combine(Path.GetTempPath(), rageFileUtilities.GenerateTimeBasedFilename());
                    zipFilename += ".zip";
                    zipFilename = Path.GetFullPath(zipFilename);

                    rageExecuteCommand command = new rageExecuteCommand();

                    StringBuilder args = new StringBuilder();
                    args.Append('\"');
                    args.Append(zipFilename);
                    args.Append('\"');

                    foreach (string attachment in m_attachments)
                    {
                        args.Append(" \"");
                        args.Append(attachment);
                        args.Append('\"');
                    }

                    command.Arguments = args.ToString();

                    command.Command = zipExecutable;
                    command.EchoToConsole = true;
                    command.LogToHtmlFile = false;
                    command.TimeOutInSeconds = 60.0f;
                    command.TimeOutWithoutOutputInSeconds = 60.0f;
                    command.UseBusySpinner = false;

                    Console.WriteLine(command.GetDosCommand());

                    rageStatus status;
                    command.Execute(out status);
                    if (status.Success())
                    {
                        m_attachments.Clear();
                        m_attachments.Add(zipFilename);

                        zipped = true;
                    }
                }

                if (!zipped)
                {
                    // make a copy of the attachments, if any.  we'll get IO errors in other places if we don't.
                    for (int i = 0; i < m_attachments.Count; ++i)
                    {
                        string tempFilename = Path.Combine(Path.GetTempPath(), Path.GetFileName(m_attachments[i]));
                        tempFilename = Path.GetFullPath(tempFilename);

                        File.Copy(m_attachments[i], tempFilename, true);

                        m_attachments[i] = tempFilename;
                    }
                }
            }

            using (IApplication app = OutlookApplication.Create())
            {
                object obj = app.CreateItem(OlItemType.olMailItem);
                using (IMailItem mail = (IMailItem)COMWrapper.Wrap(obj, typeof(IMailItem)))
                {
                    mail.Recipients.Add(m_toEmailAddress);
                    mail.Recipients.ResolveAll();
                    mail.Subject = this._formatter.FormatHeading();
                    mail.Body = applicationBody.ToString();

                    foreach (String attachment in m_attachments)
                    {
                        mail.Attachments.Add(attachment, (int)OlAttachmentType.olByValue, mail.Body.Length + 1, Path.GetFileName(attachment));
                    }

                    mail.Save();
                    mail.Display(false);
                }
            }
        }
        #endregion

        #region Private Functions

        // Seems to be the only way to get the build date from the assembly.. very nice! ;)
        // http://stackoverflow.com/questions/1600962/c-displaying-the-build-date
        // http://stackoverflow.com/questions/3820985/suppressing-is-never-used-and-is-never-assigned-to-warnings-in-c

        #pragma warning disable 0649
        struct _IMAGE_FILE_HEADER
        {
            public ushort Machine;
            public ushort NumberOfSections;
            public uint TimeDateStamp;
            public uint PointerToSymbolTable;
            public uint NumberOfSymbols;
            public ushort SizeOfOptionalHeader;
            public ushort Characteristics;
        };
        #pragma warning restore 0649


        static DateTime GetBuildDateTime()
        {
            try
            {
                Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();
                if ( File.Exists( assembly.Location ) )
                {
                    var buffer = new byte[System.Math.Max( Marshal.SizeOf( typeof( _IMAGE_FILE_HEADER ) ), 4 )];
                    using ( var fileStream = new FileStream( assembly.Location, FileMode.Open, FileAccess.Read ) )
                    {
                        fileStream.Position = 0x3C;
                        fileStream.Read( buffer, 0, 4 );
                        fileStream.Position = BitConverter.ToUInt32( buffer, 0 ); // COFF header offset
                        fileStream.Read( buffer, 0, 4 ); // "PE\0\0"
                        fileStream.Read( buffer, 0, buffer.Length );
                    }
                    var pinnedBuffer = GCHandle.Alloc( buffer, GCHandleType.Pinned );
                    try
                    {
                        _IMAGE_FILE_HEADER coffHeader = (_IMAGE_FILE_HEADER)Marshal.PtrToStructure( pinnedBuffer.AddrOfPinnedObject(), typeof( _IMAGE_FILE_HEADER ) );

                        return TimeZone.CurrentTimeZone.ToLocalTime( new DateTime( 1970, 1, 1 ) + new TimeSpan( coffHeader.TimeDateStamp * TimeSpan.TicksPerSecond ) );
                    }
                    finally
                    {
                        pinnedBuffer.Free();
                    }
                }
            }
            catch ( Exception e)
            {
                Console.WriteLine( "Failed getting build date" );
                Console.WriteLine( e.ToString() );
            }

            return new DateTime();
        }


        #endregion

        #region Event Handlers
        private void UnhandledExceptionForm_Load( object sender, EventArgs e )
        {
            this.Text = String.Format( "Unhandled Exception in {0}!", this.ApplicationTitle );
            this.errorRichTextBox.Text = m_message;

            SystemSounds.Hand.Play();
        }

        private void sendButton_Click( object sender, EventArgs e )
        {
            try
            {
                SendErrorReport();
                this.DialogResult = DialogResult.OK;
            }
            catch ( Exception ex )
            {
                rageMessageBox.ShowError( this, ex.ToString(), "SendEmail Failed" );
            }
        }

        /// <summary>
        /// Handles the Show event for the form.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Event arguments.</param>
        private void UnhandledExceptionDialog_Shown(object sender, EventArgs e)
        {
            // Forces the dialog to be the top-most window.
            TopMost = true;
        }
        #endregion
    }
}
