namespace RSG.Base.Threading
{
    partial class uiThreadPoolView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lstThreads = new System.Windows.Forms.ListView();
            this.hdrID = new System.Windows.Forms.ColumnHeader();
            this.hdrName = new System.Windows.Forms.ColumnHeader();
            this.hdrBackground = new System.Windows.Forms.ColumnHeader();
            this.hdrAlive = new System.Windows.Forms.ColumnHeader();
            this.hdrPriority = new System.Windows.Forms.ColumnHeader();
            this.SuspendLayout();
            // 
            // lstThreads
            // 
            this.lstThreads.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.hdrID,
            this.hdrName,
            this.hdrBackground,
            this.hdrAlive,
            this.hdrPriority});
            this.lstThreads.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstThreads.FullRowSelect = true;
            this.lstThreads.Location = new System.Drawing.Point(0, 0);
            this.lstThreads.Name = "lstThreads";
            this.lstThreads.Size = new System.Drawing.Size(509, 150);
            this.lstThreads.TabIndex = 1;
            this.lstThreads.UseCompatibleStateImageBehavior = false;
            this.lstThreads.View = System.Windows.Forms.View.Details;
            // 
            // hdrID
            // 
            this.hdrID.Text = "ID";
            // 
            // hdrName
            // 
            this.hdrName.Text = "Name";
            this.hdrName.Width = 160;
            // 
            // hdrBackground
            // 
            this.hdrBackground.Text = "Background";
            this.hdrBackground.Width = 81;
            // 
            // hdrAlive
            // 
            this.hdrAlive.Text = "Alive";
            // 
            // hdrPriority
            // 
            this.hdrPriority.Text = "Priority";
            this.hdrPriority.Width = 100;
            // 
            // uiThreadPoolView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lstThreads);
            this.Name = "uiThreadPoolView";
            this.Size = new System.Drawing.Size(509, 150);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView lstThreads;
        private System.Windows.Forms.ColumnHeader hdrID;
        private System.Windows.Forms.ColumnHeader hdrName;
        private System.Windows.Forms.ColumnHeader hdrBackground;
        private System.Windows.Forms.ColumnHeader hdrPriority;
        private System.Windows.Forms.ColumnHeader hdrAlive;
    }
}
