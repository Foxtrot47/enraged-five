﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Reflection;
using System.Threading;
using System.Windows.Forms;

namespace RSG.Base.Forms
{

    /// <summary>
    /// 
    /// </summary>
    public partial class Splashscreen : Form
    {
        #region Constants
        private double m_dblOpacityIncrement = 0.05;
        private double m_dblOpacityDecrement = 0.08;
        private const int TIMER_INTERVAL = 50; 
        #endregion // Constants

        #region Properties and Associated Member Data
        public static Splashscreen SplashForm
        {
            get { return sm_Splashscreen; }
        }
        private static Splashscreen sm_Splashscreen = null;

        /// <summary>
        /// 
        /// </summary>
        public static Image SplashImage
        {
            get { return sm_SplashImage; }
            set { sm_SplashImage = value; }
        }
        private static Image sm_SplashImage;

        /// <summary>
        /// Current status message display.
        /// </summary>
        public static String CurrentMessage
        {
            get { return sm_sCurrentMessage; }
            set { sm_sCurrentMessage = value; }
        }
        private static String sm_sCurrentMessage;
        #endregion // Properties and Associated Member Data

        #region Static Member Data
        private static Thread sm_SplashThread = null;
        #endregion // Static Member Data

        #region Constructor(s)
        /// <summary>
        /// Default constructor
        /// </summary>
        public Splashscreen()
        {
            InitializeComponent();
            this.Opacity = 0.0;
            this.timer.Interval = TIMER_INTERVAL;
            this.timer.Start();
        }
        #endregion // Constructor(s)

        #region Static Controller Methods
        /// <summary>
        /// 
        /// </summary>
        public static void ShowSplash()
        {
            if (sm_Splashscreen != null)
                return;

            sm_SplashThread = new Thread(new ThreadStart(Splashscreen.ShowForm));
            sm_SplashThread.IsBackground = true;
            sm_SplashThread.SetApartmentState(ApartmentState.STA);
            sm_SplashThread.Name = "Splash thread";
            sm_SplashThread.Start();
        }

        /// <summary>
        /// 
        /// </summary>
        public static void CloseSplash()
        {
            if (sm_Splashscreen != null && sm_Splashscreen.IsDisposed == false)
            {
                // Make it start going away.
                sm_Splashscreen.m_dblOpacityIncrement =- sm_Splashscreen.m_dblOpacityDecrement;
            }
            sm_SplashThread = null;
            sm_Splashscreen = null;
        }

        private static void ShowForm()
        {
            sm_Splashscreen = new Splashscreen();
            sm_Splashscreen.pictureBox1.Image = SplashImage;
            Application.Run(sm_Splashscreen);
        }
        #endregion // Static Controller Methods

        #region Private Methods
        #region Event Handlers
        #region Form Event Handlers
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Splashscreen_Load(Object sender, EventArgs e)
        {
            Assembly entryAssembly = Assembly.GetEntryAssembly();

            // Product Name
            Object[] attributesProduct = entryAssembly.GetCustomAttributes(typeof(AssemblyProductAttribute), false);
            if (1 == attributesProduct.Length)
                lblAppName.Text = ((AssemblyProductAttribute)attributesProduct[0]).Product;
            else
                lblAppName.Text = "";

            // Company Name
            Object[] attributesCompany = entryAssembly.GetCustomAttributes(typeof(AssemblyCompanyAttribute), false);
            if (1 == attributesCompany.Length)
                lblCompany.Text = ((AssemblyCompanyAttribute)attributesCompany[0]).Company;
            else
                lblCompany.Text = "";

            // Set version string.
            lblVersion.Text = entryAssembly.GetName().Version.ToString();
        }
        #endregion // Form Event Handlers

        #region Timer Event Handlers
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timer_Tick(Object sender, EventArgs e)
        {
            lblStatusMessage.Text = CurrentMessage;

            if (m_dblOpacityIncrement > 0)
            {
                if (this.Opacity < 1)
                    this.Opacity += m_dblOpacityIncrement;
            }
            else
            {
                if (this.Opacity > 0)
                    this.Opacity += m_dblOpacityIncrement;
                else
                {
                    this.Close();
                }
            }
        }
        #endregion // Timer Event Handlers
        #endregion // Event Handlers
        #endregion // Private Methods
    }

} // RSG.Base.Forms namespace
