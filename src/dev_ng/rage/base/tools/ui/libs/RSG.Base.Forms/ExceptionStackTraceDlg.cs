///
/// File:        cExceptionStackTraceDialog.cs
/// Description: Implementation of a RSG.Base.cExceptionStackTraceDialog class
/// Author:      David Muir <David.Muir@rockstarnorth.com>
/// 

using System;
using System.Diagnostics;
using System.IO;
using System.Net.Mail;
using System.Windows.Forms;

namespace RSG.Base.Forms
{

    /// <summary>
    /// Dialog which auto-populates textbox with complete Stack Trace information
    /// </summary>
    /// This dialog is designed to be shown on unhandled exceptions, typically
    /// with an outer try..catch block around the program's entry point.
    /// 
    /// This should be able to report some stack information in both Debug and
    /// Release builds although you will likely not get extended file location
    /// and filename information in Release builds.
    public partial class ExceptionStackTraceDlg : Form
    {
        #region Constants
        private readonly String EMAIL_SERVER = "rsgediexg1.rockstar.t2.corp";
        #endregion // Constants

        #region Properties and Associated Member Data
        /// <summary>
        /// Exception thrown that the dialog is displaying
        /// </summary>
        public Exception ThrownException
        {
            get { return m_ThrownException; }
            private set { m_ThrownException = value; }
        }
        private Exception m_ThrownException;

        /// <summary>
        /// 
        /// </summary>
        public String CustomMessage
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public String CustomStackTrace
        {
            get;
            private set;
        }

        /// <summary>
        /// Application Name
        /// </summary>
        public String ApplicationName
        {
            get { return m_sApplicationName; }
            private set { m_sApplicationName = value; }
        }
        private String m_sApplicationName;

        /// <summary>
        /// Application Version
        /// </summary>
        public String ApplicationVersion
        {
            get { return m_sApplicationVersion; }
            private set { m_sApplicationVersion = value; }
        }
        private String m_sApplicationVersion;

        /// <summary>
        /// Application Author
        /// </summary>
        public String ApplicationAuthor
        {
            get { return m_sApplicationAuthor; }
            private set { m_sApplicationAuthor = value; }
        }
        private String m_sApplicationAuthor;

        /// <summary>
        /// Application Email
        /// </summary>
        public String ApplicationEmail
        {
            get { return m_sApplicationEmail; }
            private set { m_sApplicationEmail = value; }
        }
        private String m_sApplicationEmail;
        #endregion // Properties and Associated Member Data

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        public ExceptionStackTraceDlg(Exception ex)
        {
            InitializeComponent();
            Initialise(ex, String.Empty, String.Empty);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="author"></param>
        public ExceptionStackTraceDlg(Exception ex, String author)
        {
            InitializeComponent();
            Initialise(ex, author, String.Empty);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="author"></param>
        /// <param name="email"></param>
        public ExceptionStackTraceDlg(Exception ex, String author, String email)
        {
            InitializeComponent();
            Initialise(ex, author, email);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        /// <param name="stack"></param>
        /// <param name="author"></param>
        /// <param name="email"></param>
        public ExceptionStackTraceDlg(String message, String stack, String script, String version, String author, String email)
        {
            InitializeComponent();
            Initialise(null, author, email);
            this.ApplicationName = script;
            this.ApplicationVersion = version;
            this.CustomMessage = message;
            this.CustomStackTrace = stack;
        }
        #endregion // Constructors

        #region Private Methods
        /// <summary>
        /// Common initialisation method (invoked from constructors)
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="author"></param>
        /// <param name="email"></param>
        private void Initialise(Exception ex, String author, String email)
        {
            this.ThrownException = ex;
            this.ApplicationName = Application.ProductName;
            this.ApplicationVersion = Application.ProductVersion;
            this.ApplicationAuthor = author;
            this.ApplicationEmail = email;
        }
        #endregion // Private Methods

        #region UI Event Handlers
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dlgExceptionStackTrace_Load(Object sender, EventArgs e)
        {
            Text = String.Format("Unhandled Exception in {0}", this.ApplicationName);

            if (this.ApplicationEmail.Length > 0)
                btnEmail.Enabled = true;
            else
                btnEmail.Enabled = false;

            // Initialise Application Info Labels
            if (this.ApplicationName.Length > 0)
                lblApplicationName.Text = this.ApplicationName;
            if (this.ApplicationVersion.Length > 0)
                lblApplicationVersion.Text = this.ApplicationVersion;
            if (this.ApplicationAuthor.Length > 0)
                lblApplicationAuthor.Text = this.ApplicationAuthor;

            // Initialise Stack Trace TextBox
            txtStackTrace.ScrollBars = ScrollBars.Both;
            txtStackTrace.Focus();

            if (null != m_ThrownException)
            {
                txtStackTrace.AppendText("Uncaught exception in " + Application.ProductName + " (version: " + Application.ProductVersion + ")");
                txtStackTrace.AppendText(Environment.NewLine);
                txtStackTrace.AppendText(DateTime.Now.ToString());
                txtStackTrace.AppendText(Environment.NewLine + Environment.NewLine);
                txtStackTrace.AppendText("Exception Message: " + Environment.NewLine + m_ThrownException.Message);
                txtStackTrace.AppendText(Environment.NewLine + Environment.NewLine);
                txtStackTrace.AppendText("Exception Helplink: " + Environment.NewLine + m_ThrownException.HelpLink);
                txtStackTrace.AppendText(Environment.NewLine + Environment.NewLine);
                txtStackTrace.AppendText("Exception Stacktrace: " + Environment.NewLine);
                txtStackTrace.AppendText(m_ThrownException.StackTrace);
                txtStackTrace.AppendText(Environment.NewLine + Environment.NewLine);
                txtStackTrace.AppendText("Environment Stacktrace: " + Environment.NewLine);
                txtStackTrace.AppendText(Environment.StackTrace);
                
                if (null != m_ThrownException.InnerException)
                {
                    txtStackTrace.AppendText(Environment.NewLine + Environment.NewLine);
                    txtStackTrace.AppendText(Environment.NewLine + Environment.NewLine);
                    txtStackTrace.AppendText("Inner Exception Message: " + Environment.NewLine + m_ThrownException.InnerException.Message);
                    txtStackTrace.AppendText(Environment.NewLine + Environment.NewLine);
                    txtStackTrace.AppendText("Inner Exception Helplink: " + Environment.NewLine + m_ThrownException.InnerException.HelpLink);
                    txtStackTrace.AppendText(Environment.NewLine + Environment.NewLine);
                }
            }
            else
            {
                txtStackTrace.AppendText("Uncaught exception in " + this.ApplicationName);
                txtStackTrace.AppendText(Environment.NewLine);
                txtStackTrace.AppendText(DateTime.Now.ToString());
                txtStackTrace.AppendText(Environment.NewLine + Environment.NewLine);
                txtStackTrace.AppendText("Exception Message: " + Environment.NewLine + this.CustomMessage);
                txtStackTrace.AppendText(Environment.NewLine + Environment.NewLine);
                txtStackTrace.AppendText(this.CustomStackTrace.Replace("|", Environment.NewLine));
            }
        }

        /// <summary>
        /// Save the stack trace information to a text file
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSave_Click(object sender, EventArgs e)
        {
            if (DialogResult.OK != dlgFileSave.ShowDialog())
                return;

            try
            {
                using (StreamWriter writer = new StreamWriter(dlgFileSave.FileName))
                {
                    foreach (string line in txtStackTrace.Lines)
                    {
                        writer.WriteLine(line);
                    }
                }
            }
            catch
            {
                MessageBox.Show("Error writing unhandled exception log.", "File Write Error");
            }         
        }

        /// <summary>
        /// Close Button Click Event Handler (likely to exit application)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        /// <summary>
        /// Email Button Click Event Handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnEmail_Click(object sender, EventArgs e)
        {
            try
            {
                MailMessage mail = new MailMessage();
                mail.To.Add(new MailAddress(this.ApplicationEmail));
                mail.From = new MailAddress(this.ApplicationEmail);
                mail.Subject = String.Format("Unhandled Exception in {0} (User: {1}, Machine: {2})", 
                    this.ApplicationName, Environment.UserName, Environment.MachineName);
                mail.Body = this.txtStackTrace.Text;

                SmtpClient smtpClient = new SmtpClient(EMAIL_SERVER);
                smtpClient.Send(mail);

                // Disable so user doesn't go nuts...
                btnEmail.Enabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("Email send error: {0}.", ex.Message));
            }
        }
        #endregion // UI Event Handlers
    }

} // End of RsBaseForms namespace

// End of file