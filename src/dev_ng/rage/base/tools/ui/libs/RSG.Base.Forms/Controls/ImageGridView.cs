﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Text;
using System.Windows.Forms;

namespace RSG.Base.Forms.Controls
{

    /// <summary>
    /// 
    /// </summary>
    public class ImageGridView : Panel
    {

        #region Events
        /// <summary>
        /// Event raised when the user selects an image in the grid view.
        /// </summary>
        public event EventHandler<ImageGridViewItemEventArgs> ImageSelected;
        #endregion // Events

        #region Properties and Associated Member Data
        /// <summary>
        /// ImageList storage cache for loaded prop images.
        /// </summary>
        [Browsable(false)]
        public List<ImageGridViewItem> Items
        {
            get { return m_Items; }
            protected set { m_Items = value; }
        }
        private List<ImageGridViewItem> m_Items;

        /// <summary>
        /// 
        /// </summary>
        [Browsable(true)]
        public int Border
        {
            get { return m_nBorder; }
            set { m_nBorder = value; }
        }
        private int m_nBorder;

        /// <summary>
        /// 
        /// </summary>
        [Browsable(true)]
        public bool ShowCaptions
        {
            get { return m_bShowCaptions; }
            set { m_bShowCaptions = value; }
        }
        private bool m_bShowCaptions;

        [Browsable(true)]
        public bool ShowToolTips
        {
            get { return m_bShowToolTips; }
            set { m_bShowToolTips = value; }
        }
        private bool m_bShowToolTips;

        /// <summary>
        /// Width in pixels of images.  Set before populating the ImageList.
        /// </summary>
        [Browsable(true)]
        public int ImageWidth
        {
            get { return m_nImageWidth; }
            set { m_nImageWidth = value; }
        }
        private int m_nImageWidth;

        /// <summary>
        /// Height in pixels of images.  Set before populating the ImageList.
        /// </summary>
        [Browsable(true)]
        public int ImageHeight
        {
            get { return m_nImageHeight; }
            set { m_nImageHeight = value; }
        }
        private int m_nImageHeight;

        /// <summary>
        /// User-selected ImageGridViewItem (or null).
        /// </summary>
        [Browsable(false)]
        public ImageGridViewItem SelectedItem
        {
            get { return (m_SelectedItem); }
            private set 
            { 
                m_SelectedItem = value;
                if (null != ImageSelected)
                    ImageSelected(this, new ImageGridViewItemEventArgs(m_SelectedItem));
            }
        }
        private ImageGridViewItem m_SelectedItem;
        #endregion // Properties and Associated Member Data

        #region Member Data
        /// <summary>
        /// Tooltip UI object.
        /// </summary>
        private ToolTip m_ToolTip;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public ImageGridView()
        {
            this.Items = new List<ImageGridViewItem>();
            this.ShowToolTips = true;
            this.ShowCaptions = false;
            this.AutoScroll = true;
            this.ImageWidth = 100;
            this.ImageHeight = 100;
            this.Border = 16;
            this.m_ToolTip = new ToolTip();
            this.m_ToolTip.ShowAlways = true;
            SetStyle(ControlStyles.OptimizedDoubleBuffer | 
                ControlStyles.UserPaint | ControlStyles.AllPaintingInWmPaint,
                true);
        }
        #endregion // Constructor(s)

        #region Control Overridden Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPaint(PaintEventArgs e)
        {
            this.SuspendLayout();
            if (this.Items.Count > 0)
                Render(e.Graphics);
            else
                base.OnPaint(e);
            this.ResumeLayout();
        }
        #endregion // Control Overridden Methods

        #region Protected Methods
        /// <summary>
        /// Render all ImageGridViewItems.
        /// </summary>
        /// <param name="g">Graphics object</param>
        protected virtual void Render(Graphics g)
        {
            Pen selectedPen = new Pen(Color.Yellow, 2.0f);
            Pen unselectedPen = new Pen(Color.Black);
            int column = 0;
            int row = 0;

            //Adjust the scroll position
            int maxRows = MaxNumberOfRows();
            this.AutoScrollMinSize = new Size(this.MinimumSize.Width, maxRows * (ImageHeight + Border)); // autoscroll

            Matrix m = new Matrix();
            m.Translate(this.AutoScrollPosition.X,this.AutoScrollPosition.Y, MatrixOrder.Append);
            g.Transform = m;

            foreach (ImageGridViewItem item in this.Items)
            {
                int x = (column * (ImageWidth + Border));
                int y = (row * (ImageHeight + Border));

                if (item == this.SelectedItem)
                    RenderItem(g, item, selectedPen, x, y, this.ImageWidth, this.ImageHeight);
                else
                    RenderItem(g, item, unselectedPen, x, y, this.ImageWidth, this.ImageHeight);
               
                if (((column + 2) * (ImageWidth + Border)) > this.ClientSize.Width)
                {
                    column = 0;
                    ++row;
                }
                else
                    ++column;
            }
        }

        /// <summary>
        /// Render a single ImageGridViewItem object.
        /// </summary>
        /// <param name="g"></param>
        /// <param name="item"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="w"></param>
        /// <param name="h"></param>
        protected virtual void RenderItem(Graphics g, ImageGridViewItem item, Pen p, int x, int y, int w, int h)
        {
            g.DrawImage(item.Image, x, y, ImageWidth, ImageHeight);
            g.DrawRectangle(p, x, y, m_nImageWidth, m_nImageHeight);

            if (this.ShowCaptions)
            {
                SizeF capSize = g.MeasureString(item.Caption, this.Font);
                g.DrawString(item.Caption, this.Font, Brushes.Black,
                    (x + (w / 2.0f)) - capSize.Width / 2.0f, 
                    y + h);               
            }
        }
        #endregion // Protected Methods

        #region Private Methods
        /// <summary>
        /// Return the number of rows required to render all images.
        /// </summary>
        /// <returns></returns>
        private int MaxNumberOfRows()
        {
            int row = 0;
            int column = 0;
            foreach (ImageGridViewItem item in this.Items)
            {
                int x = (column * (ImageWidth + Border));
                int y = (row * (ImageHeight + Border));

                if (((column + 2) * (ImageWidth + Border)) > this.ClientSize.Width)
                {
                    column = 0;
                    ++row;
                }
                else
                    ++column;
            }

            return (++row);
        }

        #region Event-Handlers
        /// <summary>
        /// Internally handle mouse clicks for selection and set SelectedItem appropriately.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnMouseClick(MouseEventArgs e)
        {
            // Only use left/right-click for selections.
            if ((MouseButtons.Left != e.Button) && (MouseButtons.Right != e.Button))
            {
                base.OnMouseClick(e);
                return;
            }

            // Determine if user-click was with the boundary of an Item.
            Point mousePos = new Point(e.X-this.AutoScrollPosition.X, e.Y-this.AutoScrollPosition.Y);

            int column = 0;
            int row = 0;
            foreach (ImageGridViewItem item in this.Items)
            {
                int x = (column * (ImageWidth + Border));
                int y = (row * (ImageHeight + Border));

                if ((mousePos.X >= x) && (mousePos.X <= x + ImageWidth))
                    if ((mousePos.Y >= y) && (mousePos.Y <= y + ImageHeight))
                        this.SelectedItem = item;

                if (((column + 2) * (ImageWidth + Border)) > this.ClientSize.Width)
                {
                    column = 0;
                    ++row;
                }
                else
                    ++column;
            }
            base.OnMouseClick(e);
            Invalidate();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected override void OnMouseMove(MouseEventArgs e)
        {            
            // Determine if user-click was with the boundary of an Item.
            Point mousePos = new Point(e.X - this.AutoScrollPosition.X, e.Y - this.AutoScrollPosition.Y);
            ImageGridViewItem hoverItem = null;

            int column = 0;
            int row = 0;
            foreach (ImageGridViewItem item in this.Items)
            {
                int x = (column * (ImageWidth + Border));
                int y = (row * (ImageHeight + Border));

                if ((mousePos.X >= x) && (mousePos.X <= x + ImageWidth))
                    if ((mousePos.Y >= y) && (mousePos.Y <= y + ImageHeight))
                        hoverItem = item;

                if (((column + 2) * (ImageWidth + Border)) > this.ClientSize.Width)
                {
                    column = 0;
                    ++row;
                }
                else
                    ++column;
            }
            if ((null != hoverItem) && 
                (hoverItem.Tag != String.Empty))
            {
                if (hoverItem.Tag.ToString() != this.m_ToolTip.GetToolTip(this))
                    this.m_ToolTip.SetToolTip(this, hoverItem.Tag);
            }
            else
            {
                this.m_ToolTip.SetToolTip(this, String.Empty);
            }

            base.OnMouseMove(e);
        }
        #endregion // Event-Handlers
        #endregion // Private Methods

    }

    /// <summary>
    /// ImageGridView visible item.
    /// </summary>
    /// A ImageGridViewItem has an Image and an optional caption String.
    /// The ImageGridView's ShowCaption property must be true to enable
    /// rendering of the item's captions.
    /// 
    public class ImageGridViewItem : Control
    {

        #region Properties and Associated Member Data

        /// <summary>
        /// 
        /// </summary>
        public Image Image
        {
            get { return m_Image; }
            set { m_Image = value; }
        }
        private Image m_Image;

        /// <summary>
        /// Caption string.
        /// </summary>
        public String Caption
        {
            get { return m_sCaption; }
            set { m_sCaption = value; }
        }
        private String m_sCaption;

        /// <summary>
        /// Tag String, used for item tooltip.
        /// </summary>
        public new String Tag
        {
            get { return m_sTag; }
            set { m_sTag = value; }
        }
        private String m_sTag;

        #endregion // Properties and Associated Member Data

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        public ImageGridViewItem(Image image)
        {
            this.Image = image;
            this.Caption = String.Empty;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="caption"></param>
        public ImageGridViewItem(Image image, String caption)
        {
            this.Image = image;
            this.Caption = caption;
        }
        #endregion // Constructor(s)

    }

    /// <summary>
    /// 
    /// </summary>
    public class ImageGridViewItemEventArgs : EventArgs
    {

        #region Properties and Associated Member Data
        /// <summary>
        /// Associated item.
        /// </summary>
        public ImageGridViewItem Item
        {
            get { return m_Item; }
            private set { m_Item = value; }
        }
        private ImageGridViewItem m_Item;
        #endregion // Properties and Associated Member Data

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="item"></param>
        public ImageGridViewItemEventArgs(ImageGridViewItem item)
        {
            this.Item = item;
        }
        #endregion // Constructor(s)

    }

} // RSG.Base.Forms.Controls namespace
