using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;

using RSG.Base.Command;
using RSG.Base.IO;

namespace RSG.Base.Forms
{
    public partial class FeedbackForm : Form
    {
        protected FeedbackForm()
        {
            InitializeComponent();

            this.CanZipAttachments = true;
        }

        public FeedbackForm( string applicationTitle, string fromEmailAddress, string toEmailAddress )
        {
            InitializeComponent();

            this.CanZipAttachments = true;
            this.ApplicationTitle = applicationTitle;
            this.FromEmailAddress = fromEmailAddress;
            this.ToEmailAddress = toEmailAddress;
            this.ShowIcon = false;
        }

        public FeedbackForm( string applicationTitle, string fromEmailAddress, string toEmailAddress, System.Drawing.Icon icon )
            : this( applicationTitle, fromEmailAddress, toEmailAddress )
        {
            this.Icon = icon.Clone() as System.Drawing.Icon;
            this.ShowIcon = true;
        }

        #region Variables
        private string m_applicationTitle;
        private bool m_canZipAttachments;
        #endregion

        #region Delegates
        public delegate void FromEmailAddressChangedEventHandler( object sender, FromEmailAddressChangedEventArgs e );
        #endregion

        #region Events
        public event FromEmailAddressChangedEventHandler FromEmailAddressChanged;
        #endregion

        #region Properties
        public bool CanEditToEmailAddress
        {
            get
            {
                return !this.toTextBox.ReadOnly;
            }
            set
            {
                this.toTextBox.ReadOnly = !value;
            }
        }

        public bool CanEditSubject
        {
            get
            {
                return !this.subjectTextBox.ReadOnly;
            }
            set
            {
                this.subjectTextBox.ReadOnly = !value;
            }
        }

        public bool CanEditCategory
        {
            get
            {
                return this.categoryComboBox.Enabled;
            }
            set
            {
                this.categoryComboBox.Enabled = value;
            }
        }

        public bool CanEditAttachments
        {
            get
            {
                return !this.attachmentsTextBox.ReadOnly;
            }
            set
            {
                this.attachmentsTextBox.ReadOnly = !value;
                this.browseAttachmentsButton.Enabled = value;
            }
        }

        public bool CanZipAttachments
        {
            get
            {
                return m_canZipAttachments;
            }
            set
            {
                m_canZipAttachments = value;
                this.zipAttachmentsCheckBox.Enabled = value && !String.IsNullOrEmpty( this.ZipExecutable );
            }
        }

        public string ApplicationTitle
        {
            get
            {
                return m_applicationTitle;
            }
            set
            {
                m_applicationTitle = value;

                if ( !String.IsNullOrEmpty( m_applicationTitle ) )
                {
                    this.Text = String.Format( "Submit {0} Feedback", m_applicationTitle );
                }
            }
        }

        public string FromEmailAddress
        {
            get
            {
                return this.yourEmailAddressTextBox.Text;
            }
            set
            {
                if ( value != null )
                {
                    this.yourEmailAddressTextBox.Text = value;
                }
                else
                {
                    this.yourEmailAddressTextBox.Text = string.Empty;
                }
            }
        }

        public string ToEmailAddress
        {
            get
            {
                return this.toTextBox.Text;
            }
            set
            {
                if ( value != null )
                {
                    this.toTextBox.Text = value;
                }
                else
                {
                    this.toTextBox.Text = string.Empty;
                }
            }
        }

        public string Subject
        {
            get
            {
                return this.subjectTextBox.Text;
            }
            set
            {
                if ( value != null )
                {
                    this.subjectTextBox.Text = value;
                }
                else
                {
                    this.subjectTextBox.Text = string.Empty;
                }
            }
        }

        public string Category
        {
            get
            {
                string category = this.categoryComboBox.SelectedItem as string;
                if ( category != null )
                {
                    return category;
                }
                else
                {
                    return string.Empty;
                }
            }
            set
            {
                int indexOf = this.categoryComboBox.Items.IndexOf( value );
                this.categoryComboBox.SelectedIndex = indexOf;
            }
        }

        public List<string> Attachments
        {
            get
            {
                List<string> attachments = new List<string>();

                string[] attachmentsSplit = this.attachmentsTextBox.Text.Split( 
                    new char[] { System.IO.Path.PathSeparator }, StringSplitOptions.RemoveEmptyEntries );
                if ( attachmentsSplit != null )
                {
                    attachments.AddRange( attachmentsSplit );
                }

                return attachments;
            }
            set
            {
                if ( value != null )
                {
                    string pathSeparator = new string( System.IO.Path.PathSeparator, 1 );
                    string attachments = String.Join( pathSeparator, value.ToArray() );
                    this.attachmentsTextBox.Text = attachments;
                }
                else
                {
                    this.attachmentsTextBox.Text = string.Empty;
                }
            }
        }

        public bool ZipAttachments
        {
            get
            {
                return this.zipAttachmentsCheckBox.Checked;
            }
            set
            {
                this.zipAttachmentsCheckBox.Checked = value;
            }
        }

        public string ZipExecutable
        {
            get
            {
                string zipExecutable = Path.GetFullPath( Path.Combine( Path.GetDirectoryName( Application.ExecutablePath ), @"..\zip.exe" ) );
                try
                {
                    if ( !File.Exists( zipExecutable ) )
                    {
                        zipExecutable = Path.GetFullPath( Path.Combine( Path.GetDirectoryName( Application.ExecutablePath ), @"zip.exe" ) );
                        if ( !File.Exists( zipExecutable ) )
                        {
                            zipExecutable = null;
                        }
                    }
                }
                catch ( Exception ex )
                {
                    Console.WriteLine( ex.ToString() );
                    zipExecutable = null;
                }

                return zipExecutable;
            }
        }

        public string Message
        {
            get
            {
                return this.messageTextBox.Text;
            }
            set
            {
                if ( value != null )
                {
                    this.messageTextBox.Text = value;
                }
                else
                {
                    this.messageTextBox.Text = string.Empty;
                }
            }
        }
        #endregion

        #region Event Handlers
        private void FeedbackDialog_Load( object sender, EventArgs e )
        {
            if ( this.CanEditToEmailAddress )
            {
                this.toTextBox.Focus();
            }
            else
            {
                this.yourEmailAddressTextBox.Focus();
            }
        }

        private void FeedbackDialog_FormClosing( object sender, FormClosingEventArgs e )
        {
            OnFromEmailAddressChanged();

            if ( !this.Modal )
            {
                e.Cancel = true;
                this.Hide();
            }
        }

        private void EnableSendButtonCheck_EventHandler( object sender, EventArgs e )
        {
            this.sendButton.Enabled = (this.ToEmailAddress.Trim() != string.Empty)
                && (this.FromEmailAddress.Trim() != string.Empty)
                && (this.Message.Trim() != string.Empty)
                && (!this.CanEditCategory || (this.categoryComboBox.SelectedIndex != -1));
        }

        private void browseAttachmentsButton_Click( object sender, EventArgs e )
        {
            DialogResult result = this.openFileDialog.ShowDialog( this );
            if ( result == DialogResult.OK )
            {
                List<string> attachments = this.Attachments;

                foreach ( string filename in this.openFileDialog.FileNames )
                {
                    string uppercaseFilename = filename.ToUpper();

                    bool found = false;
                    foreach ( string attachment in attachments )
                    {
                        if ( uppercaseFilename == attachment.ToUpper() )
                        {
                            found = true;
                            break;
                        }
                    }

                    if ( !found )
                    {
                        attachments.Add( filename );
                    }
                }

                this.Attachments = attachments;
            }
        }

        private void sendButton_Click( object sender, EventArgs e )
        {
            string from = this.FromEmailAddress.Trim();
            string to = this.ToEmailAddress.Trim();

            string applicationTo = to;
            string applicationFrom = from;

            string userTo = from;
            string userFrom = null;
            
            string[] usersFrom = to.Split();
            foreach ( string user in usersFrom )
            {
                string[] subUsers = user.Split( new char[] { ';' } );
                foreach ( string subUser in subUsers )
                {
                    if ( subUser != string.Empty )
                    {
                        if ( userFrom == null )
                        {
                            userFrom = subUser;
                        }
                        else
                        {
                            break;
                        }
                    }
                }
            }

            StringBuilder applicationSubject = new StringBuilder();
            applicationSubject.Append( this.ApplicationTitle );
            applicationSubject.Append( ' ' );
            applicationSubject.Append( this.Category );
            string subj = this.Subject.Trim();
            if ( !String.IsNullOrEmpty( subj ) )
            {
                applicationSubject.Append( ": " );
                applicationSubject.Append( subj );
            }

            StringBuilder userSubject = new StringBuilder();
            userSubject.Append( "AUTOMATED REPLY: " );
            userSubject.Append( applicationSubject.ToString() );

            StringBuilder applicationBody = new StringBuilder();
            applicationBody.Append( Application.ExecutablePath );
            applicationBody.Append( Environment.NewLine );
            applicationBody.Append( Environment.UserName );
            applicationBody.Append( " on " );
            applicationBody.Append( Environment.MachineName );
            applicationBody.Append( Environment.NewLine );
            applicationBody.Append( this.ApplicationTitle );
            applicationBody.Append( " version " );
            applicationBody.Append( System.Reflection.Assembly.GetEntryAssembly().GetName().Version.ToString() );
            applicationBody.Append( Environment.NewLine );
            applicationBody.Append( Environment.NewLine );
            applicationBody.Append( this.Message );
            applicationBody.Append( Environment.NewLine );
            applicationBody.Append( Environment.NewLine );

            StringBuilder userBody = new StringBuilder();
            userBody.Append( "The following is a copy of the email that was sent to " );
            userBody.Append( userFrom );
            userBody.Append( ':' );
            userBody.Append( Environment.NewLine );
            userBody.Append( Environment.NewLine );
            userBody.Append( applicationBody.ToString() );

            List<string> attachments = this.Attachments;

            // remove duplicates, if any
            for ( int i = 0; i < attachments.Count; ++i )
            {
                string uppercaseAttachment = attachments[i].ToUpper();

                for ( int j = i + 1; j < attachments.Count; ++j )
                {
                    if ( attachments[j].ToUpper() == uppercaseAttachment )
                    {
                        attachments.RemoveAt( j );
                        break;
                    }
                }
            }

            if ( this.CanZipAttachments && this.ZipAttachments && (attachments.Count > 0) )
            {
                string zipExecutable = this.ZipExecutable;
                if ( zipExecutable != null )
                {
                    this.CanZipAttachments = true;  // Will re-enable our option if was not found previously

                    string zipFilename = Path.Combine( Path.GetTempPath(), rageFileUtilities.GenerateTimeBasedFilename() );
                    zipFilename += ".zip";
                    zipFilename = Path.GetFullPath( zipFilename );

                    rageExecuteCommand command = new rageExecuteCommand();
                    
                    StringBuilder args = new StringBuilder();
                    args.Append( '\"' );
                    args.Append( zipFilename );
                    args.Append( '\"' );

                    foreach ( string attachment in attachments )
                    {
                        args.Append( " \"" );
                        args.Append( attachment );
                        args.Append( '\"' );
                    }
                    
                    command.Arguments = args.ToString();

                    command.Command = zipExecutable;
                    command.EchoToConsole = true;
                    command.LogToHtmlFile = false;
                    command.TimeOutInSeconds = 60.0f;
                    command.TimeOutWithoutOutputInSeconds = 60.0f;
                    command.UseBusySpinner = false;

                    Console.WriteLine( command.GetDosCommand() );

                    rageStatus status;
                    command.Execute( out status );
                    if ( status.Success() )
                    {
                        attachments.Clear();
                        attachments.Add( zipFilename );
                    }
                }
            }
// rageEmailUtilities has been deprecated. Commenting out offending code in case this is still used anywhere.
#if false
            try
            {
                // send mail to the application developers
                rageEmailUtilities.SendEmail( applicationTo, applicationFrom, applicationTo, applicationSubject.ToString(),
                applicationBody.ToString(), attachments.ToArray(), rageEmailUtilities.MailFormat.Text );

                // send the auto-reply to the application user
                rageEmailUtilities.SendEmail( userTo, userFrom, userTo, userSubject.ToString(),
                    userBody.ToString(), attachments.ToArray(), rageEmailUtilities.MailFormat.Text );
            }
            catch ( Exception ex )
            {
                rageMessageBox.ShowError( this, ex.Message, "SendEmail Failed" );
                return; // so we don't close
            }
#endif

            this.Subject = string.Empty;
            this.Attachments = null;
            this.messageTextBox.Clear();

            if ( this.Modal )
            {
                this.DialogResult = DialogResult.OK;
            }
            else
            {
                this.Close();
            }
        }
        
        private void cancelButton_Click( object sender, EventArgs e )
        {
            if ( this.Modal )
            {
                this.DialogResult = DialogResult.Cancel;
            }
            else
            {
                this.Close();
            }
        }
        #endregion

        #region Event Dispatchers
        public void OnFromEmailAddressChanged()
        {
            if ( this.FromEmailAddressChanged != null )
            {
                this.FromEmailAddressChanged( this, new FromEmailAddressChangedEventArgs( this.FromEmailAddress ) );
            }
        }
        #endregion
    }
    
    public class FromEmailAddressChangedEventArgs : EventArgs
    {
        public FromEmailAddressChangedEventArgs( string fromEmailAddress )
        {
            m_fromEmailAddress = fromEmailAddress;
        }

        #region Variables
        private string m_fromEmailAddress;
        #endregion

        #region Properties
        public string FromEmailAddress
        {
            get
            {
                return m_fromEmailAddress;
            }
        }
        #endregion
    }
}