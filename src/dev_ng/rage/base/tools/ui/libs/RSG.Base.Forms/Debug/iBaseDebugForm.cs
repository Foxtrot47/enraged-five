//
// File: iBaseDebugForm.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of iBaseDebugForm.cs class
//

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace RSG.Base.Debug
{

    /// <summary>
    /// Base abstract debug form class
    /// </summary>
    /// Applications can reuse this to provide a consistent Debug Window,
    /// that automatically refreshes itself (using a timer).  It also
    /// contains a manual Refresh and Close button.
    public partial class iBaseDebugForm : Form
    {
        #region Constants
        readonly int sC_s_ms_DefaultTimerInterval = 1000;
        #endregion 

        #region Protected Properties and Associated Member Data
        /// <summary>
        /// Read-access to the UI/display Refresh timer object
        /// </summary>
        protected Timer RefreshTimer
        {
            get { return m_Timer; }
        }
        private Timer m_Timer;
        #endregion

        #region Constructors
        public iBaseDebugForm()
        {
            InitializeComponent();
            InitialiseTimer(sC_s_ms_DefaultTimerInterval);
        }

        public iBaseDebugForm(int s_ms_TimerInterval)
        {
            InitializeComponent();
            InitialiseTimer(s_ms_TimerInterval);
        }
        #endregion

        #region Public Abstract Methods
        /// <summary>
        /// Virtual method to refresh the sub-classed Debug Window
        /// </summary>
        public override void Refresh()
        {
        }
        #endregion

        #region UI Event Handlers
        /// <summary>
        /// Thread-safe method to refresh the UI
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Update(object sender, EventArgs e)
        {
            if (InvokeRequired)
            {
                BeginInvoke(new MethodInvoker(delegate() { Refresh(); }));
            }
            else
            {
                Refresh();
            }
        }

        /// <summary>
        /// Refresh button click event handler (user-specified refresh)
        /// </summary>
        private void btnRefresh_Click(object sender, EventArgs e)
        {
            Refresh();
        }

        /// <summary>
        /// Close button click event handler
        /// </summary>
        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }
        #endregion

        #region Initialisation
        /// <summary>
        /// Initialise the Refresh Timer
        /// </summary>
        /// <param name="s_ms_TimerInterval">Timer interval (milliseconds)</param>
        private void InitialiseTimer(int s_ms_TimerInterval)
        {
            m_Timer = new Timer();
            m_Timer.Interval = sC_s_ms_DefaultTimerInterval;
            m_Timer.Tick += new EventHandler(Update);
            m_Timer.Enabled = true;
        }
        #endregion
    }

} // End of RSG.Base.Debug namespace

// End of file
