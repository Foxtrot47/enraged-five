//
// File: AboutBox.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of AboutBox.cs class
//

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using System.Reflection;

namespace RSG.Base.Forms
{

    /// <summary>
    /// Application About Box
    /// </summary>
    partial class AboutBox : Form
    {
        #region Properties
        /// <summary>
        /// Associated assembly (fetches about information from this)
        /// </summary>
        public Assembly Assembly
        {
            get { return m_Assembly;  }
        }
        private Assembly m_Assembly;

        public String AssemblyTitle
        {
            get
            {
                // Get all Title attributes on this assembly
                object[] attributes = this.Assembly.GetCustomAttributes(typeof(AssemblyTitleAttribute), false);
                // If there is at least one Title attribute
                if (attributes.Length > 0)
                {
                    // Select the first one
                    AssemblyTitleAttribute titleAttribute = (AssemblyTitleAttribute)attributes[0];
                    // If it is not an empty string, return it
                    if (titleAttribute.Title != "")
                        return titleAttribute.Title;
                }
                // If there was no Title attribute, or if the Title attribute was the empty string, return the .exe name
                return System.IO.Path.GetFileNameWithoutExtension(Assembly.GetExecutingAssembly().CodeBase);
            }
        }

        public String AssemblyVersion
        {
            get
            {
                return this.Assembly.GetName().Version.ToString();
            }
        }

        public String AssemblyDescription
        {
            get
            {
                // Get all Description attributes on this assembly
                object[] attributes = this.Assembly.GetCustomAttributes(typeof(AssemblyDescriptionAttribute), false);
                // If there aren't any Description attributes, return an empty string
                if (attributes.Length == 0)
                    return "";
                // If there is a Description attribute, return its value
                return ((AssemblyDescriptionAttribute)attributes[0]).Description;
            }
        }

        public String AssemblyProduct
        {
            get
            {
                // Get all Product attributes on this assembly
                object[] attributes = this.Assembly.GetCustomAttributes(typeof(AssemblyProductAttribute), false);
                // If there aren't any Product attributes, return an empty string
                if (attributes.Length == 0)
                    return "";
                // If there is a Product attribute, return its value
                return ((AssemblyProductAttribute)attributes[0]).Product;
            }
        }

        public String AssemblyCopyright
        {
            get
            {
                // Get all Copyright attributes on this assembly
                object[] attributes = this.Assembly.GetCustomAttributes(typeof(AssemblyCopyrightAttribute), false);
                // If there aren't any Copyright attributes, return an empty string
                if (attributes.Length == 0)
                    return "";
                // If there is a Copyright attribute, return its value
                return ((AssemblyCopyrightAttribute)attributes[0]).Copyright;
            }
        }

        public String AssemblyCompany
        {
            get
            {
                // Get all Company attributes on this assembly
                object[] attributes = this.Assembly.GetCustomAttributes(typeof(AssemblyCompanyAttribute), false);
                // If there aren't any Company attributes, return an empty string
                if (attributes.Length == 0)
                    return "";
                // If there is a Company attribute, return its value
                return ((AssemblyCompanyAttribute)attributes[0]).Company;
            }
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor, specifying an assembly.
        /// </summary>
        /// <param name="assembly"></param>
        public AboutBox(Assembly assembly)
        {
            InitializeComponent();
            this.m_Assembly = assembly;
            Init();
        }

        /// <summary>
        /// Constructor, specifying an assembly and image bitmap.
        /// </summary>
        /// <param name="assembly"></param>
        /// <param name="image">(best 120 pixels wide, upto around 262 pixels height)</param>
        public AboutBox(Assembly assembly, Bitmap image, PictureBoxSizeMode mode)
        {
            InitializeComponent();
            this.m_Assembly = assembly;
            this.logoPictureBox.Image = image;
            this.logoPictureBox.SizeMode = mode;
            Init();
        }
        #endregion // Constructor(s)

        #region Private Methods
        /// <summary>
        /// Common initialisation method (called from constructors)
        /// </summary>
        private void Init()
        {
            this.Text = String.Format("About {0}", AssemblyTitle);
            this.labelProductName.Text = AssemblyProduct;
            this.labelVersion.Text = String.Format("Version {0}", AssemblyVersion);
            this.labelCopyright.Text = AssemblyCopyright;
            this.labelCompanyName.Text = AssemblyCompany;
            this.textBoxDescription.Text = AssemblyDescription;
        }
        #endregion // Private Methods
    }

} // End of RSG.Base.Forms namespace

// End of file
