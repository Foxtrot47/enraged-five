using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Text;

using RSG.Base.Command;
using RSG.Base.IO;

namespace RSG.Base.Forms.SCM
{

	public class ragePerforce
	{
        private static string m_PerforcePathSuffix = "/...";

        public class ragePerforceView
        {
            public ragePerforceView()
            {

            }

            public ragePerforceView( string perforcePath, string localPath )
            {
                m_perforcePath = perforcePath;
                m_localPath = localPath;
            }

            #region Variables
            private string m_perforcePath = string.Empty;
            private string m_localPath = string.Empty;
            #endregion

            #region Properties
            public string PerforcePath
            {
                get
                {
                    return m_perforcePath;
                }
                set
                {
                    m_perforcePath = value;
                }
            }

            public string LocalPath
            {
                get
                {
                    return m_localPath;
                }
                set
                {
                    m_localPath = value;
                }
            }
            #endregion
        }

        public class ragePerforceClientspec
        {
            public ragePerforceClientspec()
            {

            }

            #region Enums
            public enum LineEndType
            {
                Local,
                Unix,
                Mac,
                Win,
                Share
            }
            #endregion

            #region Variables
            private string m_port = string.Empty;
            private string m_client = string.Empty;
            private string m_host = string.Empty;
            private string m_owner = string.Empty;
            private DateTime m_update;
            private DateTime m_access;
            private string m_description = string.Empty;
            private string m_root = string.Empty;
            private List<string> m_altRoots = new List<string>();

            private bool m_allWriteOption = false;
            private bool m_lockedOption = false;
            private bool m_clobberOption = false;
            private bool m_modTimeOption = false;
            private bool m_compressOption = false;
            private bool m_rmDirOption = false;

            private LineEndType m_lineEnd = LineEndType.Local;

            private List<ragePerforceView> m_views = new List<ragePerforceView>();
            #endregion

            #region Properties
            public string Port
            {
                get
                {
                    return m_port;
                }
                set
                {
                    m_port = value;
                }
            }

            public string Client
            {
                get
                {
                    return m_client;
                }
                set
                {
                    m_client = value;
                }
            }

            public string Host
            {
                get
                {
                    return m_host;
                }
                set
                {
                    m_host = value;
                }
            }

            public string Owner
            {
                get
                {
                    return m_owner;
                }
                set
                {
                    m_owner = value;
                }
            }

            public DateTime Update
            {
                get
                {
                    return m_update;
                }
                set
                {
                    m_update = value;
                }
            }

            public DateTime Access
            {
                get
                {
                    return m_access;
                }
                set
                {
                    m_access = value;
                }
            }

            public string Description
            {
                get
                {
                    return m_description;
                }
                set
                {
                    m_description = value;
                }
            }

            public string Root
            {
                get
                {
                    return m_root;
                }
                set
                {
                    m_root = value;
                }
            }

            public List<string> AltRoots
            {
                get
                {
                    return m_altRoots;
                }
                set
                {
                    m_altRoots = value;
                }
            }

            public bool AllWriteOption
            {
                get
                {
                    return m_allWriteOption;
                }
                set
                {
                    m_allWriteOption = value;
                }
            }

            public bool LockedOption
            {
                get
                {
                    return m_lockedOption;
                }
                set
                {
                    m_lockedOption = value;
                }
            }

            public bool ClobberOption
            {
                get
                {
                    return m_clobberOption;
                }
                set
                {
                    m_clobberOption = value;
                }
            }

            public bool ModTimeOption
            {
                get
                {
                    return m_modTimeOption;
                }
                set
                {
                    m_modTimeOption = value;
                }
            }

            public bool CompressOption
            {
                get
                {
                    return m_compressOption;
                }
                set
                {
                    m_compressOption = value;
                }
            }

            public bool RmDirOption
            {
                get
                {
                    return m_rmDirOption;
                }
                set
                {
                    m_rmDirOption = value;
                }
            }

            public LineEndType LineEnd
            {
                get
                {
                    return m_lineEnd;
                }
                set
                {
                    m_lineEnd = value;
                }
            }

            public List<ragePerforceView> Views
            {
                get
                {
                    return m_views;
                }
                set
                {
                    m_views = value;
                }
            }
            #endregion

            #region Overrides
            public override string ToString()
            {
                StringBuilder str = new StringBuilder();

                if ( !String.IsNullOrEmpty( this.Port ) )
                {
                    str.Append( this.Port );
                }

                if ( !String.IsNullOrEmpty( this.Client ) )
                {
                    if ( str.Length > 0 )
                    {
                        str.Append( ' ' );
                    }

                    str.Append( this.Client );
                }

                if ( !String.IsNullOrEmpty( this.Owner ) )
                {
                    if ( str.Length > 0 )
                    {
                        str.Append( ' ' );
                    }

                    str.Append( this.Owner );
                }

                return str.ToString();
            }
            #endregion

            #region Public Functions

            public string ConvertToPerforcePath(string localPath)
            {
                localPath = localPath.Replace("/", "\\");
                foreach(ragePerforceView view in m_views)
                {
                    if(localPath.StartsWith(view.LocalPath, true, CultureInfo.CurrentCulture) == true)
                    {
                        string perforcePath = localPath.ToLower();
                        perforcePath = perforcePath.Replace(view.LocalPath, view.PerforcePath);
                        perforcePath = perforcePath.Replace("\\", "/");
                        return perforcePath;
                    }
                }   

                return null;
            }

            public string ConvertToLocalPath(string perforcePath)
            {
                perforcePath = perforcePath.Replace("\\", "/");
                foreach (ragePerforceView view in m_views)
                {
                    if (perforcePath.StartsWith(view.PerforcePath, true, CultureInfo.CurrentCulture) == true)
                    {
                        string localPath = perforcePath.ToLower();
                        localPath.Replace(view.PerforcePath, view.LocalPath);
                        localPath.Replace("/", "\\");
                        return localPath;
                    }
                }

                return null;
            }

            #endregion
        }

        public class ragePerforceUser
        {
            public ragePerforceUser()
            {

            }

            public ragePerforceUser( string username, string emailAddress )
            {
                m_username = username;
                m_emailAddress = emailAddress;
            }

            #region Variables
            private string m_username;
            private string m_emailAddress;
            #endregion

            #region Properties
            public string Username
            {
                get
                {
                    return m_username;
                }
                set
                {
                    m_username = value;
                }
            }
            
            public string EmailAddress
            {
                get
                {
                    return m_emailAddress;
                }
                set
                {
                    m_emailAddress = value;
                }
            }
            #endregion
        }

		private class ragePerforceFile
		{
			private string m_strDepotFile;
			private string m_strClientFile;
			private string m_strIsMapped;
			private string m_strHeadAction;
			private string m_strHeadType;
			private DateTime m_obHeadTime;
			private string m_strHeadRev;
			private string m_strHeadChange;
			private DateTime m_obHeadModTime;
			private string m_strFileSize;
			private string m_strDigest;

			public string DepotFile
			{
				get
				{
					return m_strDepotFile;
				}
				set
				{
					m_strDepotFile = value.Replace("\\", "/");
				}
			}
			public string ClientFile
			{
				get
				{
					return m_strClientFile;
				}
				set
				{
					m_strClientFile = value.Replace("\\", "/");
				}
			}

			public string IsMapped
			{
				set
				{
					m_strIsMapped = value;
				}
			}

			public string HeadAction
			{
				set
				{
					m_strHeadAction = value;
				}
			}

			public string HeadType
			{
				set
				{
					m_strHeadType = value;
				}
			}

			public string HeadTimeFromstring
			{
				set
				{
					m_obHeadTime = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc) + TimeSpan.FromSeconds(Int64.Parse(value));
				}
			}

			public DateTime HeadTime
			{
				get
				{
					return m_obHeadTime;
				}
			}

			public string HeadRev
			{
				set
				{
					m_strHeadRev = value;
				}
			}

			public string HeadChange
			{
				set
				{
					m_strHeadChange = value;
				}
			}

			public string HeadModTimeFromstring
			{
				set
				{
					m_obHeadModTime = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc) + TimeSpan.FromSeconds(Int64.Parse(value));
				}
			}

			public DateTime HeadModTime
			{
				get
				{
					return m_obHeadModTime;
				}
			}

			public string FileSize
			{
				set
				{
					m_strFileSize = value;
				}
			}

			public string Digest
			{
				set
				{
					m_strDigest = value;
				}
			}
		}

        public class Changelist
        {
            public Changelist(int number)
            {
                m_number = number.ToString();
                m_files = new List<string>();
            }

            public void AddFile(string file) 
            { 
                m_files.Add(file); 
            }

            public string Number
            {
                get { return m_number; }
            }

            public List<string> Files
            {
                get { return m_files; }
            }

            public string m_defaultDescription;
            private string m_number;
            private List<string> m_files;
        }

        static Changelist m_currentChangelist;
        static private string m_perforceCommand = "p4";

		static public bool IsPerforceInstalled()
		{
			// Execute the command
			rageStatus obStatus;
			rageExecuteCommand obCommand = new rageExecuteCommand();
			obCommand.Command = m_perforceCommand;
			obCommand.Arguments = CommonPerforceOptions;
			obCommand.UpdateLogAfterEverySoManyLines = 500;
			obCommand.LogToHtmlFile = false;
			obCommand.RemoveLog = true;
			obCommand.UseBusySpinner = false;
			obCommand.EchoToConsole = EchoToConsole;
			if (EchoToConsole) Console.WriteLine(obCommand.GetDosCommand()); 
			obCommand.Execute(out obStatus);
			return (obStatus.Success());
		}

        static public bool Login(string password)
        {
            Process loginProcess = new Process();
            loginProcess.StartInfo.FileName = m_perforceCommand;
            loginProcess.StartInfo.Arguments = CommonPerforceOptions + " login";
            loginProcess.StartInfo.RedirectStandardInput = true;
            loginProcess.StartInfo.UseShellExecute = false;

            loginProcess.Start();
            loginProcess.StandardInput.WriteLine(password);
            loginProcess.WaitForExit();

            if (loginProcess.ExitCode != 0)
                return false;

            return true;
        }

        static public int GetLastSubmittedChangelist()
        {
            return GetLastSubmittedChangelist(DateTime.Now);
        }

        static public int GetLastSubmittedChangelist(DateTime time)
        {
            rageStatus obStatus = new rageStatus();
            rageExecuteCommand obCommand = new rageExecuteCommand();
            obCommand.Command = m_perforceCommand;
            obCommand.Arguments = CommonPerforceOptions + "changes -m1 submitted";

            string formatDate = time.ToString("yyyy/MM/dd");
            string formatTime = time.ToString("HH:mm:ss");
            obCommand.Arguments += " @" + formatDate + ":" + formatTime;
            
            obCommand.UpdateLogAfterEverySoManyLines = 500;
            obCommand.LogToHtmlFile = false;
            obCommand.RemoveLog = true;
            obCommand.UseBusySpinner = false;
            obCommand.EchoToConsole = EchoToConsole;
            if (EchoToConsole) Console.WriteLine(obCommand.GetDosCommand());
            obCommand.Execute(out obStatus);
            if (!obStatus.Success())
            {
                // Badness happened
                return -1;
            }

            int changelistNumber = -1;
            List<string> commandOutput = obCommand.GetLogAsArray();
            foreach (string line in commandOutput)
            {
                if (line.StartsWith("Change") == true)
                {
                    string[] splitLine = line.Split(" ".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                    if (splitLine.Length > 2)
                    {
                        Int32.TryParse(splitLine[1], out changelistNumber);
                        break;
                    }
                }
            }

            return changelistNumber;
        }

		static public string[] GetCodeLines(string strDepotRoot, out rageStatus obStatus)
		{
			// Console.WriteLine("GetCodeLines(\"" + strDepotRoot +"\", out rageStatus obStatus)");
			// Execute the command
			rageExecuteCommand obCommand = new rageExecuteCommand();
			obCommand.Command = m_perforceCommand;
			obCommand.Arguments = CommonPerforceOptions +"dirs " + strDepotRoot + "*";
			obCommand.UpdateLogAfterEverySoManyLines = 500;
			obCommand.LogToHtmlFile = false;
			obCommand.RemoveLog = true;
			obCommand.UseBusySpinner = false;
			obCommand.EchoToConsole = EchoToConsole;
			if (EchoToConsole) Console.WriteLine(obCommand.GetDosCommand());
			obCommand.Execute(out obStatus);
			if (!obStatus.Success())
			{
				// Badness happened
				return null;
			}

			// Parse the results
			List<string> obAStrCommandOutput = obCommand.GetLogAsArray();
			string[] astrCodeLines = new string[obAStrCommandOutput.Count];
			int iCodeLine = 0;
			foreach (string strCommandOutput in obAStrCommandOutput)
			{
				astrCodeLines[iCodeLine] = strCommandOutput.Substring(strDepotRoot.Length);
				iCodeLine++;
			}
			//for (int i = 0; i < astrCodeLines.Length; i++)
			//{
			//    Console.WriteLine(i + " : " + astrCodeLines[i]);
			//}
			return astrCodeLines;
		}

		static public void GetLatestPerforceFolder(string strDepotRoot, string strCodeLine, string strFolder, bool bForceSync, out rageStatus obStatus)
		{
			GetLabelOfPerforceFolder(strDepotRoot + strCodeLine + "/" + strFolder, "Latest", bForceSync, out obStatus);
		}

		static public void GetLatestPerforceFolder(string strPerforcePath, bool bForceSync, out rageStatus obStatus)
		{
			GetLabelOfPerforceFolder(strPerforcePath, "Latest", bForceSync, out obStatus);
		}

		static public void GetLabelOfPerforceFolder(string strDepotRoot, string strCodeLine, string strFolder, string strLabel, bool bForceSync, out rageStatus obStatus)
		{
			GetLabelOfPerforceFolder(strDepotRoot + strCodeLine + "/" + strFolder, strLabel, bForceSync, out obStatus);
		}

		static public void GetLabelOfPerforceFolder(string strPerforcePath, string strLabel, bool bForceSync, out rageStatus obStatus)
		{
			GetLabelOfPerforceFolder(strPerforcePath, strLabel, bForceSync, true, out obStatus);
		}

		static public void GetLabelOfPerforceFolder(string strPerforcePath, string strLabel, bool bForceSync, bool bAppendSlashAsterixToPerforcePath, out rageStatus obStatus)
		{
			string strCompletePerforcePath = strPerforcePath;
			if(bAppendSlashAsterixToPerforcePath)
			{
				strCompletePerforcePath += "/*";
			}

			// Execute the command
			rageExecuteCommand obCommand = new rageExecuteCommand();
			obCommand.Command = m_perforceCommand;
			obCommand.Arguments = CommonPerforceOptions + "sync" + (bForceSync ? " -f " : " ") +"\""+ strCompletePerforcePath +"\"";
			if (strLabel != "Latest")
			{
				obCommand.Arguments += "@" + strLabel;
			}
			else
			{
				obCommand.Arguments += "#head";
			}
			obCommand.UpdateLogAfterEverySoManyLines = 500;
			obCommand.LogToHtmlFile = false;
			obCommand.RemoveLog = true;
			obCommand.EchoToConsole = EchoToConsole;
			obCommand.UseBusySpinner = false;
			if (EchoToConsole) Console.WriteLine(obCommand.GetDosCommand());
			obCommand.Execute(out obStatus);
			if (!obStatus.Success())
			{
				// Badness happened
				return;
			}
		}

        static public int GetHaveChangesetOfPerforceFolder( string strDepotRoot, string strCodeLine, string strFolder, out rageStatus obStatus )
        {
            return GetHaveChangesetOfPerforceFolder( strDepotRoot + strCodeLine + "/" + strFolder, out obStatus );
        }

        static public int GetHaveChangesetOfPerforceFolder( string strPerforcePath, out rageStatus obStatus )
        {
            return GetHaveChangesetOfPerforceFolder( strPerforcePath, true, out obStatus );
        }

        static public int GetHaveChangesetOfPerforceFolder( string strPerforcePath, bool bAppendSlashAsterixToPerforcePath, out rageStatus obStatus )
        {
            string strCompletePerforcePath = strPerforcePath;
            if ( bAppendSlashAsterixToPerforcePath )
            {
                strCompletePerforcePath += "/*";
            }

            // Execute the command
            rageExecuteCommand obCommand = new rageExecuteCommand();
            obCommand.Command = m_perforceCommand;
            obCommand.Arguments = CommonPerforceOptions + "changes -m1 \"#have\" \"" + strCompletePerforcePath + "\"";
            obCommand.UpdateLogAfterEverySoManyLines = 500;
            obCommand.LogToHtmlFile = false;
            obCommand.RemoveLog = true;
            obCommand.EchoToConsole = EchoToConsole;
            obCommand.UseBusySpinner = false;
            if ( EchoToConsole ) Console.WriteLine( obCommand.GetDosCommand() );
            obCommand.Execute( out obStatus );
            if ( !obStatus.Success() )
            {
                // Badness happened
                return -1;
            }

            List<string> results = obCommand.GetLogAsArray();
            if ( results.Count == 0 )
            {
                obStatus = new rageStatus( "No results found." );
                return -1;
            }

            string[] split = results[0].Split();
            if ( (split.Length >= 2) && (split[0] == "Change") )
            {
                return int.Parse( split[1] );
            }
            else
            {
                obStatus = new rageStatus( "Could not parse result \"" + results[0] + "\"." );
                return -1;
            }
        }

		static public string[] GetPerforceLabelsForFile(string strDepotRoot, string strCodeLine, string strPathAndFilename, out rageStatus obStatus)
		{
			return GetPerforceLabelsForFile(strDepotRoot + strCodeLine + "/" + strPathAndFilename, out obStatus);
		}

		static public string[] GetPerforceLabelsForFile(string strPerforcePath, out rageStatus obStatus)
		{
            strPerforcePath = CleanFilenameOfWildcards( strPerforcePath );

			// Setup command
			rageExecuteCommand obP4Command = new rageExecuteCommand();
			obP4Command.Command = m_perforceCommand;
            obP4Command.Arguments = CommonPerforceOptions + "labels " + "\"" + strPerforcePath + "\"";
			obP4Command.UseBusySpinner = false;
			obP4Command.UpdateLogFileInRealTime = false;
			obP4Command.LogToHtmlFile = false;
			obP4Command.RemoveLog = true;
			obP4Command.EchoToConsole = EchoToConsole;
			if (EchoToConsole) Console.WriteLine(obP4Command.GetDosCommand());
			obP4Command.Execute(out obStatus);

			// Get log
			List<string> astrLog = obP4Command.GetLogAsArray();
			List<string> astrLabels = new List<string>();

			// put most recent label first to match CVS's order
			for (int i = astrLog.Count - 1; i >= 0; --i)
			{
				string strLogLine = astrLog[i] as string;
				string[] split = strLogLine.Split(new char[] { ' ' });
				if ((split.Length >= 2) && (split[0] == "Label"))
				{
					astrLabels.Add(split[1]);
				}
			}
			return astrLabels.ToArray();
		}

		static public string GetLocalPathFromPerforcePath(string strDepotRoot, string strCodeLine, string strPathAndFilename, out rageStatus obStatus)
		{
			return GetLocalPathFromPerforcePath(strDepotRoot + strCodeLine + "/" + strPathAndFilename, out obStatus);
		}

		static public string GetLocalPathFromPerforcePath(string strPerforcePath, out rageStatus obStatus)
		{
			// strPerforcePath should NOT end in a slash
			if(strPerforcePath.EndsWith("/"))
			{
				return GetLocalPathFromPerforcePath(strPerforcePath.Substring(0, strPerforcePath.Length - 1), out obStatus);
			}

            strPerforcePath = CleanFilenameOfWildcards( strPerforcePath );

			// Do it
			rageExecuteCommand obP4Command = new rageExecuteCommand();
			obP4Command.Command = m_perforceCommand;
            obP4Command.Arguments = CommonPerforceOptions + "where " + "\"" + strPerforcePath + "\"";
			obP4Command.WorkingDirectory = string.Empty;
			obP4Command.UseBusySpinner = false;
			obP4Command.UpdateLogFileInRealTime = false;
			obP4Command.LogToHtmlFile = false;
			obP4Command.RemoveLog = true;
			obP4Command.EchoToConsole = EchoToConsole;
			if (EchoToConsole) Console.WriteLine(obP4Command.GetDosCommand());
			obP4Command.Execute(out obStatus);
			if (!obStatus.Success())
			{
				// Badness
				return null;
			}

			List<string> logList = obP4Command.GetLogAsArray();
			if (logList.Count > 0)
			{
				// The first line of the log will look something like:
				//
				// //rage/dev/rage/qa //krose/dev/rage/qa C:/soft/dev\rage\qa
				//
				// So split it up on spaces
				string logLine = logList[0] as string;
				string[] astrLogLineParts = logLine.Split(" ".ToCharArray());

				// There should only be three parts, so check for that
				if (astrLogLineParts.Length > 3)
				{
					// Badness
					obStatus = new rageStatus("An error occurred converting Perforce path " + strPerforcePath + " to local path.  I executed \"p4 where " + strPerforcePath + "\" but the result contained more than 2 spaces");
					return null;
				}
				return astrLogLineParts[2].Replace("\\", "/");
			}

			// Badness
			obStatus = new rageStatus("An error occurred converting Perforce path " + strPerforcePath + " to local path.");
			return null;
		}

		static private List<ragePerforceFile> GetFileList(string strPerforceFolder)
		{
			// Console.WriteLine("GetFileList(\""+ strPerforceFolder +"\")");
			List<ragePerforceFile> obReturnMe = new List<ragePerforceFile>();

			// Get the list of files
			rageStatus obStatus = new rageStatus();
			rageExecuteCommand obP4Command = new rageExecuteCommand();
			obP4Command.Command = m_perforceCommand;
            obP4Command.Arguments = CommonPerforceOptions + "fstat -Ol " + "\"" + strPerforceFolder + "\"" + "*";
			obP4Command.WorkingDirectory = string.Empty;
			obP4Command.UseBusySpinner = false;
			obP4Command.UpdateLogFileInRealTime = false;
			obP4Command.LogToHtmlFile = false;
			obP4Command.RemoveLog = true;
			obP4Command.EchoToConsole = EchoToConsole;
			if (EchoToConsole) Console.WriteLine(obP4Command.GetDosCommand());
			obP4Command.Execute(out obStatus);
			if (!obStatus.Success())
			{
				// Badness
				return null;
			}

			List<string> logList = obP4Command.GetLogAsArray();
			if (logList.Count > 0)
			{
				// The array will look something like this....
				//... depotFile //rage/dev/rage/tools/base/moduleSettings.xml
				//... clientFile C:/soft/dev\rage\tools\base\moduleSettings.xml
				//... isMapped
				//... headAction add
				//... headType text
				//... headTime 1158616406
				//... headRev 1
				//... headChange 65122
				//... headModTime 1158615153
				//... fileSize 1787
				//... digest F1A9BCE42BB2486FE93427E956407BC0
				//
				//... depotFile //rage/dev/rage/tools/base/rageCommandCenterMenu.xml
				//... clientFile C:/soft/dev\rage\tools\base\rageCommandCenterMenu.xml
				//... isMapped
				//... headAction add
				//... headType text
				//... headTime 1158616406
				//... headRev 1
				//... headChange 65122
				//... headModTime 1156207132
				//... fileSize 778
				//... digest 692B518279F600B1ED427F6F4E9184F7
				//
				//
				// So create ragePerforceFile from it
				ragePerforceFile obCurrentFile = new ragePerforceFile();
				foreach (string strLogLine in logList)
				{
					if (strLogLine == "")
					{
						// Finished a file, so save it
						obReturnMe.Add(obCurrentFile);
						//Console.WriteLine("Added " + obCurrentFile.DepotFile);
						//Console.WriteLine(obCurrentFile.HeadTime.ToLongTimestring() + " " + obCurrentFile.HeadTime.ToLongDatestring());
						//Console.WriteLine(obCurrentFile.HeadModTime.ToLongTimestring() + " " + obCurrentFile.HeadModTime.ToLongDatestring());
						obCurrentFile = new ragePerforceFile();
					}
					else
					{
						// Add info
						if (strLogLine.StartsWith("... depotFile ")) obCurrentFile.DepotFile = strLogLine.Substring(14);
						if (strLogLine.StartsWith("... clientFile ")) obCurrentFile.ClientFile = strLogLine.Substring(15);
						if (strLogLine.StartsWith("... isMapped ")) obCurrentFile.IsMapped = strLogLine.Substring(13);
						if (strLogLine.StartsWith("... headAction ")) obCurrentFile.HeadAction = strLogLine.Substring(15);
						if (strLogLine.StartsWith("... headType ")) obCurrentFile.HeadType = strLogLine.Substring(13);
						if (strLogLine.StartsWith("... headRev ")) obCurrentFile.HeadRev = strLogLine.Substring(12);
						if (strLogLine.StartsWith("... headChange ")) obCurrentFile.HeadChange = strLogLine.Substring(15);
						if (strLogLine.StartsWith("... headModTime ")) obCurrentFile.HeadModTimeFromstring = strLogLine.Substring(16);
						if (strLogLine.StartsWith("... fileSize ")) obCurrentFile.FileSize = strLogLine.Substring(13);
						if (strLogLine.StartsWith("... digest ")) obCurrentFile.Digest = strLogLine.Substring(11);
					}
				}
			}

			// Recurse
			// Get the list of files
			obP4Command.Command = m_perforceCommand;
            obP4Command.Arguments = CommonPerforceOptions + "dirs " + "\"" + strPerforceFolder + "\"" + "*";
			obP4Command.WorkingDirectory = string.Empty;
			obP4Command.UseBusySpinner = false;
			obP4Command.UpdateLogFileInRealTime = false;
			obP4Command.LogToHtmlFile = false;
			obP4Command.RemoveLog = true;
			obP4Command.EchoToConsole = EchoToConsole;
			if (EchoToConsole) Console.WriteLine(obP4Command.GetDosCommand());
			obP4Command.Execute(out obStatus);
			if (!obStatus.Success())
			{
				// Badness
				return null;
			}

			logList = obP4Command.GetLogAsArray();
			if (logList.Count > 0)
			{
				// The array will look something like this....
				//	//rage/dev/rage/tools/base
				//	//rage/dev/rage/tools/maya
				//
				//
				// So use it to recurse
				foreach (string strLogLine in logList)
				{
					if ((strLogLine != "") && (!strLogLine.Contains("no such file(s).")))
					{
						// Recurse on it
						obReturnMe.AddRange(GetFileList(strLogLine + "/"));
					}
				}
			}

			return obReturnMe;
		}

		static public void CompareFolders(string strPerforceFolder, out List<string> obAStrFilesOnlyExistInLocalFolder, out List<string> obAStrFilesOnlyExistInPerforceFolder, out List<string> obAStrFilesExistInBothButNewerInLocalFolder, out List<string> obAStrFilesExistInBothButNewerInPerforceFolder, out List<string> obAStrFilesSameInBoth)
		{
			rageStatus obStatus;
			string strLocalFolder = GetLocalPathFromPerforcePath(strPerforceFolder, out obStatus);
			if (!obStatus.Success())
			{
				// Badness
				obAStrFilesOnlyExistInLocalFolder = new List<string>();
				obAStrFilesOnlyExistInPerforceFolder = new List<string>();
				obAStrFilesExistInBothButNewerInLocalFolder = new List<string>();
				obAStrFilesExistInBothButNewerInPerforceFolder = new List<string>();
				obAStrFilesSameInBoth = new List<string>();
				return;
			}
			CompareFolders(strLocalFolder, strPerforceFolder, out obAStrFilesOnlyExistInLocalFolder, out obAStrFilesOnlyExistInPerforceFolder, out obAStrFilesExistInBothButNewerInLocalFolder, out obAStrFilesExistInBothButNewerInPerforceFolder, out obAStrFilesSameInBoth);
		}

		static private void CompareFolders(string strLocalFolder, string strPerforceFolder, out List<string> obAStrFilesOnlyExistInLocalFolder, out List<string> obAStrFilesOnlyExistInPerforceFolder, out List<string> obAStrFilesExistInBothButNewerInLocalFolder, out List<string> obAStrFilesExistInBothButNewerInPerforceFolder, out List<string> obAStrFilesSameInBoth)
		{
			// Console.WriteLine("CompareFolders(\"" + strLocalFolder + "\", \"" + strPerforceFolder + "\")");
			// Paths should end with a "/", check for that
			if (!strLocalFolder.EndsWith("/"))
			{
				CompareFolders(strLocalFolder + "/", strPerforceFolder, out obAStrFilesOnlyExistInLocalFolder, out obAStrFilesOnlyExistInPerforceFolder, out obAStrFilesExistInBothButNewerInLocalFolder, out obAStrFilesExistInBothButNewerInPerforceFolder, out obAStrFilesSameInBoth);
				return;
			}
			if (!strPerforceFolder.EndsWith("/"))
			{
				CompareFolders(strLocalFolder, strPerforceFolder + "/", out obAStrFilesOnlyExistInLocalFolder, out obAStrFilesOnlyExistInPerforceFolder, out obAStrFilesExistInBothButNewerInLocalFolder, out obAStrFilesExistInBothButNewerInPerforceFolder, out obAStrFilesSameInBoth);
				return;
			}

			// First off, get what time based information I can
			CompareFolders_TimeBasedOnly(strLocalFolder, strPerforceFolder, out obAStrFilesOnlyExistInLocalFolder, out obAStrFilesOnlyExistInPerforceFolder, out obAStrFilesExistInBothButNewerInLocalFolder, out obAStrFilesExistInBothButNewerInPerforceFolder, out obAStrFilesSameInBoth);

			// The problem with just a time based compare is that some files may have different times, but their contents are actually the same, 
			// so I need to do additional contents comparisons as well

			// Get what info I can from perforce, it is quicker this way than actually just querying the files I care about
			// Get a list of the currently opened files and what needs to be done to them
			List<string> obAStrFilesInPerforce = new List<string>();
			List<string> obAStrStatusOfFilesInPerforce = new List<string>();

			// Get the list of files
			rageStatus obStatus;
			rageExecuteCommand obP4Command = new rageExecuteCommand();
			obP4Command.Command = m_perforceCommand;
            obP4Command.Arguments = CommonPerforceOptions + "diff -f -t -sl " + "\"" + strPerforceFolder + "\"" + "...";
			obP4Command.WorkingDirectory = strLocalFolder;
			obP4Command.UseBusySpinner = false;
			obP4Command.UpdateLogFileInRealTime = false;
			obP4Command.LogToHtmlFile = false;
			obP4Command.RemoveLog = true;
			obP4Command.EchoToConsole = EchoToConsole;
			if (EchoToConsole) Console.WriteLine(obP4Command.GetDosCommand());
			obP4Command.Execute(out obStatus);
			if (!obStatus.Success())
			{
				// Badness
				return;
			}

			List<string> logList = obP4Command.GetLogAsArray();
			if (logList.Count > 0)
			{
				// The array will look something like this....
				//same C:/soft/dev\rage\tools\base\exes\msvcr80.dll
				//same C:/soft/dev\rage\tools\base\exes\msvcr80d.dll
				//missing C:/soft/dev\rage\tools\base\exes\MSVCRTD.DLL
				//diff C:/soft/dev\rage\tools\base\exes\rag.bat
				//same C:/soft/dev\rage\tools\base\exes\rag\ColorPicker.dll
				//same C:/soft/dev\rage\tools\base\exes\rag\Interop.XDevkit.dll
				//
				//
				// So extract the file names and the file status from it
				foreach (string strLogLine in logList)
				{
					if (strLogLine != "")
					{
						// Split it up
						int iPosOfStartOfFilename = strLogLine.IndexOf(' ');
						string strFilename = strLogLine.Substring(iPosOfStartOfFilename + 1);
						string strStatus = strLogLine.Substring(0, iPosOfStartOfFilename);
						obAStrFilesInPerforce.Add(strFilename.Substring(strLocalFolder.Length).Replace('\\', '/'));
						obAStrStatusOfFilesInPerforce.Add(strStatus);
					}
				}
			}

			//for (int i = 0; i < obAStrFilesInPerforce.Count; i++)
			//{
			//    Console.WriteLine("File : \"" + obAStrFilesInPerforce[i] + "\"");
			//    Console.WriteLine("Status : \"" + obAStrStatusOfFilesInPerforce[i] + "\"");
			//}

			// Ok, I know what perforce thinks about the differences and I know what the timestamps are telling me
			// so combine both sources of information to get a true idea of what has changed
			for(int f1=0; f1<obAStrFilesExistInBothButNewerInLocalFolder.Count; f1++)
			{
				// Find the file in obAStrFilesInPerforce
				for (int f2 = 0; f2 < obAStrFilesInPerforce.Count; f2++)
				{
					// Console.WriteLine("Comparing " + obAStrFilesExistInBothButNewerInLocalFolder[f1] + " and " + obAStrFilesInPerforce[f2]);
					if (obAStrFilesExistInBothButNewerInLocalFolder[f1].ToLower() == obAStrFilesInPerforce[f2].ToLower())
					{
						// Bingo!
						// Console.WriteLine("Bingo!");
						// They have different times, but are the contents of the files really the same?
						if(obAStrStatusOfFilesInPerforce[f2] == "same")
						{
							// The time stamp may differ, but they are still the same file, so move from obAStrFilesExistInBothButNewerInLocalFolder 
							// and into obAStrFilesSameInBoth
							Console.Write(obAStrFilesExistInBothButNewerInLocalFolder[f1] + " is more recent locally, but the file contents locally are identical to the version in Perforce, so ignoring");
							obAStrFilesSameInBoth.Add(obAStrFilesExistInBothButNewerInLocalFolder[f1]);
							obAStrFilesExistInBothButNewerInLocalFolder.RemoveAt(f1);
							f1--;
						}
						break;
					}
				}
			}
			for (int f1 = 0; f1 < obAStrFilesExistInBothButNewerInPerforceFolder.Count; f1++)
			{
				// Find the file in obAStrFilesInPerforce
				for (int f2 = 0; f2 < obAStrFilesInPerforce.Count; f2++)
				{
					// Console.WriteLine("Comparing " + obAStrFilesExistInBothButNewerInPerforceFolder[f1] + " and " + obAStrFilesInPerforce[f2]);
					if (obAStrFilesExistInBothButNewerInPerforceFolder[f1].ToLower() == obAStrFilesInPerforce[f2].ToLower())
					{
						// Bingo!
						// Console.WriteLine("Bingo!");
						// They have different times, but are the contents of the files really the same?
						if (obAStrStatusOfFilesInPerforce[f2] == "same")
						{
							// The time stamp may differ, but they are still the same file, so move from obAStrFilesExistInBothButNewerInLocalFolder 
							// and into obAStrFilesSameInBoth
							Console.Write(obAStrFilesExistInBothButNewerInLocalFolder[f1] + " is more recent in Perforce, but the file contents locally are identical to the version in Perforce, so ignoring");
							obAStrFilesSameInBoth.Add(obAStrFilesExistInBothButNewerInPerforceFolder[f1]);
							obAStrFilesExistInBothButNewerInPerforceFolder.RemoveAt(f1);
							f1--;
						}
						break;
					}
				}
			}

			// Sort it all
			obAStrFilesOnlyExistInLocalFolder.Sort();
			obAStrFilesOnlyExistInPerforceFolder.Sort();
			obAStrFilesExistInBothButNewerInLocalFolder.Sort();
			obAStrFilesExistInBothButNewerInPerforceFolder.Sort();
			obAStrFilesSameInBoth.Sort();
		}

		static public void CompareFolders_TimeBasedOnly(string strLocalFolder, string strPerforceFolder, out List<string> obAStrFilesOnlyExistInLocalFolder, out List<string> obAStrFilesOnlyExistInPerforceFolder, out List<string> obAStrFilesExistInBothButNewerInLocalFolder, out List<string> obAStrFilesExistInBothButNewerInPerforceFolder, out List<string> obAStrFilesSameInBoth)
		{
			Console.WriteLine("CompareFolders_TimeBasedOnly(\""+ strLocalFolder +"\", \""+ strPerforceFolder +"\")");
			// Paths should end with a "/", check for that
			if(!strLocalFolder.EndsWith("/"))
			{
				CompareFolders_TimeBasedOnly(strLocalFolder + "/", strPerforceFolder, out obAStrFilesOnlyExistInLocalFolder, out obAStrFilesOnlyExistInPerforceFolder, out obAStrFilesExistInBothButNewerInLocalFolder, out obAStrFilesExistInBothButNewerInPerforceFolder, out obAStrFilesSameInBoth);
				return;
			}
			if (!strPerforceFolder.EndsWith("/"))
			{
				CompareFolders_TimeBasedOnly(strLocalFolder, strPerforceFolder + "/", out obAStrFilesOnlyExistInLocalFolder, out obAStrFilesOnlyExistInPerforceFolder, out obAStrFilesExistInBothButNewerInLocalFolder, out obAStrFilesExistInBothButNewerInPerforceFolder, out obAStrFilesSameInBoth);
				return;
			}

			// Compare a local folder to a perforce one
			List<string> obAStrLocalFolderFiles = rageFileUtilities.GetFilesAsStringList(strLocalFolder, "*");
			List<ragePerforceFile> obAPerforceFolderFiles = ragePerforce.GetFileList(strPerforceFolder);

			obAStrFilesOnlyExistInLocalFolder = new List<string>();
			obAStrFilesOnlyExistInPerforceFolder = new List<string>();
			obAStrFilesExistInBothButNewerInLocalFolder = new List<string>();
			obAStrFilesExistInBothButNewerInPerforceFolder = new List<string>();
			obAStrFilesSameInBoth = new List<string>();

			// Standardize my slashes
			for (int i = 0; i < obAStrLocalFolderFiles.Count; i++)
			{
				obAStrLocalFolderFiles[i] = obAStrLocalFolderFiles[i].Replace("\\", "/");
			}

			// First compare obAStrLocalFolderFiles to obAStrPerforceFolderFiles
			foreach (string strLocalFolderFile in obAStrLocalFolderFiles)
			{
				// Does the LocalFolder file exist in PerforceFolder files?
				string strLocalFolderRelativeFileNotLowerCase = strLocalFolderFile.Substring(strLocalFolder.Length, (strLocalFolderFile.Length - strLocalFolder.Length));
				string strLocalFolderRelativeFile = strLocalFolderRelativeFileNotLowerCase.ToLower();

				// Compare to PerforceFolder files
				bool bFileExists = false;
				ragePerforceFile obMatchingPerforceFolderFile = null;
				foreach (ragePerforceFile obPerforceFolderFile in obAPerforceFolderFiles)
				{
					// Get relative file
					string strPerforceFolderFile = obPerforceFolderFile.DepotFile;
					string strPerforceFolderRelativeFile = strPerforceFolderFile.Substring(strPerforceFolder.Length, (strPerforceFolderFile.Length - strPerforceFolder.Length)).ToLower();

					if (strLocalFolderRelativeFile == strPerforceFolderRelativeFile)
					{
						// Found it!
						obMatchingPerforceFolderFile = obPerforceFolderFile;
						bFileExists = true;
						break;
					}
				}

				if (bFileExists)
				{
					// File exists in both locations
					// So compare...
					DateTime obLocalFolderFileFileTime = File.GetLastWriteTime(strLocalFolderFile);
					DateTime obPerforceFolderFileFileTime = obMatchingPerforceFolderFile.HeadModTime;

					// Convert to utc time
					DateTime obLocalFolderFileUTCFileTime = obLocalFolderFileFileTime.ToUniversalTime();

					// Compare
					TimeSpan obTimeDifference = obLocalFolderFileUTCFileTime - obPerforceFolderFileFileTime;

					// If it is within one second, then close enough
					if ((obTimeDifference.Seconds < 1) && (obTimeDifference.Seconds > -1))
					{
						// Identical
						obAStrFilesSameInBoth.Add(strLocalFolderRelativeFileNotLowerCase);
					}
					else if (obTimeDifference.Seconds > 0)
					{
						// Newer in LocalFolder
						obAStrFilesExistInBothButNewerInLocalFolder.Add(strLocalFolderRelativeFileNotLowerCase);
					}
					else if (obTimeDifference.Seconds < 0)
					{
						// Newer in PerforceFolder
						obAStrFilesExistInBothButNewerInPerforceFolder.Add(strLocalFolderRelativeFileNotLowerCase);
					}
				}
				else
				{
					// File only exists in LocalFolder
					obAStrFilesOnlyExistInLocalFolder.Add(strLocalFolderRelativeFileNotLowerCase);
				}
			}

			// Now compare the other way around, obAStrPerforceFolderFiles to obAStrLocalFolderFiles
			foreach (ragePerforceFile obPerforceFolderFile in obAPerforceFolderFiles)
			{
				// Does the PerforceFolder file exist in LocalFolder files?
				string strPerforceFolderFile = obPerforceFolderFile.DepotFile;
				string strPerforceFolderRelativeFileNotLowerCase = strPerforceFolderFile.Substring(strPerforceFolder.Length, (strPerforceFolderFile.Length - strPerforceFolder.Length));
				string strPerforceFolderRelativeFile = strPerforceFolderRelativeFileNotLowerCase.ToLower();

				// Compare to LocalFolder files
				bool bFileExists = false;
				foreach (string strLocalFolderFile in obAStrLocalFolderFiles)
				{
					// Get relative file
					string strLocalFolderRelativeFile = strLocalFolderFile.Substring(strLocalFolder.Length, (strLocalFolderFile.Length - strLocalFolder.Length)).ToLower();

					if (strPerforceFolderRelativeFile == strLocalFolderRelativeFile)
					{
						// Found it!
						bFileExists = true;
						break;
					}
				}

				if (!bFileExists)
				{
					// File only exists in PerforceFolder
					obAStrFilesOnlyExistInPerforceFolder.Add(strPerforceFolderRelativeFileNotLowerCase);
				}
			}
		}

        static public void CheckOutFile(string filePath, out rageStatus obStatus)
        {
            if (String.IsNullOrEmpty(filePath) == true)
            {
                obStatus = new rageStatus("File path is not specified!");
                return;
            }

            // Execute the command
            rageExecuteCommand obCommand = new rageExecuteCommand();
            obCommand.Command = m_perforceCommand;
            obCommand.Arguments = CommonPerforceOptions + "edit " + " \"" + filePath + "\"";
            obCommand.LogToHtmlFile = false;
            obCommand.RemoveLog = true;
            obCommand.UseBusySpinner = false;
            obCommand.EchoToConsole = EchoToConsole;
            if (EchoToConsole) Console.WriteLine(obCommand.GetDosCommand());
            obCommand.Execute(out obStatus);
            if (obStatus.Success() == false)
            {
                return;
            }
        }

		static public void CheckOutFile(string filePath, string description, out rageStatus obStatus)
		{
            if(String.IsNullOrEmpty(filePath) == true)
            {
                obStatus = new rageStatus("File path is not specified!");
                return;
            }

            if(m_currentChangelist == null && description == null)
            {
                //Use a default changelist description if none has been specified.
                description = "new unnamed changelist";
            }

            //To maintain backward compatibility with the existing processes, if there is no changelist
            //currently created and no description been offered, then check out into the default changelist.
            //
            if(m_currentChangelist == null)
            {
                //Creating the changelist will automatically check out the file specified.
                CreateChangelist(null, description, out obStatus);
                if(obStatus.Success() == false)
                {
                    return;
                }

                m_currentChangelist.AddFile(filePath);

                CheckOutFile(filePath, description, out obStatus);
            }
            else
            {
                // Execute the command
                rageExecuteCommand obCommand = new rageExecuteCommand();
                obCommand.Command = m_perforceCommand;
                obCommand.Arguments = CommonPerforceOptions + "edit -c " + m_currentChangelist.Number + " \"" + filePath + "\"";
                obCommand.LogToHtmlFile = false;
                obCommand.RemoveLog = true;
                obCommand.UseBusySpinner = false;
                obCommand.EchoToConsole = EchoToConsole;
                if (EchoToConsole) Console.WriteLine(obCommand.GetDosCommand());
                obCommand.Execute(out obStatus);
                if (obStatus.Success() == false)
                {
                    return;
                }

                m_currentChangelist.AddFile(filePath);
            }

		}

        static public int CreateChangelist(string description)
        {
            // Next fake up a submit description
            string strSubmitDescriptionFilename = (Path.GetTempPath() + "/CompleteChangelistescription_" + rageFileUtilities.GenerateTimeBasedFilename() + ".txt");
            StreamWriter obSubmitDescriptionWriter = File.CreateText(strSubmitDescriptionFilename);

            // Write it out
            obSubmitDescriptionWriter.WriteLine("# A Perforce Change Specification.");
            obSubmitDescriptionWriter.WriteLine("#");
            obSubmitDescriptionWriter.WriteLine("#  Change:      The change number. 'new' on a new changelist.  Read-only.");
            obSubmitDescriptionWriter.WriteLine("#  Date:        The date this specification was last modified.  Read-only.");
            obSubmitDescriptionWriter.WriteLine("#  Client:      The client on which the changelist was created.  Read-only.");
            obSubmitDescriptionWriter.WriteLine("#  User:        The user who created the changelist. Read-only.");
            obSubmitDescriptionWriter.WriteLine("#  Status:      Either 'pending' or 'submitted'. Read-only.");
            obSubmitDescriptionWriter.WriteLine("#  Description: Comments about the changelist.  Required.");
            obSubmitDescriptionWriter.WriteLine("#  Jobs:        What opened jobs are to be closed by this changelist.");
            obSubmitDescriptionWriter.WriteLine("#               You may delete jobs from this list.  (New changelists only.)");
            obSubmitDescriptionWriter.WriteLine("#  Files:       What opened files from the default changelist are to be added");
            obSubmitDescriptionWriter.WriteLine("#               to this changelist.  You may delete files from this list.");
            obSubmitDescriptionWriter.WriteLine("#               (New changelists only.)");
            obSubmitDescriptionWriter.WriteLine("");
            obSubmitDescriptionWriter.WriteLine("Change:	new");
            obSubmitDescriptionWriter.WriteLine("");
            obSubmitDescriptionWriter.WriteLine("Client:	" + (String.IsNullOrEmpty(ragePerforce.WorkspaceName) ? Environment.MachineName : ragePerforce.WorkspaceName));
            obSubmitDescriptionWriter.WriteLine("");
            obSubmitDescriptionWriter.WriteLine("User:	" + (String.IsNullOrEmpty(ragePerforce.UserName) ? Environment.UserName : ragePerforce.UserName));
            obSubmitDescriptionWriter.WriteLine("");
            obSubmitDescriptionWriter.WriteLine("Status:	new");
            obSubmitDescriptionWriter.WriteLine("");
            obSubmitDescriptionWriter.WriteLine("Description:");
            obSubmitDescriptionWriter.WriteLine("\t" + description);
            obSubmitDescriptionWriter.WriteLine("");
            obSubmitDescriptionWriter.WriteLine("Files:");
            obSubmitDescriptionWriter.WriteLine("");

            // Close file
            obSubmitDescriptionWriter.Close();

            // Feed the complete client/Submit description back into perforce
            string strSubmitCreatingBatFilename = (Path.GetTempPath() + "/CompleteSubmitCreatingBatFile_" + rageFileUtilities.GenerateTimeBasedFilename() + ".bat");
            StreamWriter strSubmitCreatingBatWriter = File.CreateText(strSubmitCreatingBatFilename);
            strSubmitCreatingBatWriter.WriteLine("type \"" + strSubmitDescriptionFilename.Replace("/", "\\") + "\" | p4 " + CommonPerforceOptions + " change -i");
            strSubmitCreatingBatWriter.Close();

            // Execute the command
            rageExecuteCommand obCommand = new rageExecuteCommand();
            obCommand.Command = strSubmitCreatingBatFilename;
            obCommand.UpdateLogAfterEverySoManyLines = 500;
            obCommand.LogToHtmlFile = false;
            obCommand.RemoveLog = true;
            obCommand.UseBusySpinner = false;
            obCommand.EchoToConsole = EchoToConsole;
            if (EchoToConsole) Console.WriteLine(obCommand.GetDosCommand());
            rageStatus obStatus = new rageStatus();
            obCommand.Execute(out obStatus);
            if (!obStatus.Success())
            {
                // Badness happened
                return -1;
            }

            File.Delete(strSubmitDescriptionFilename);
            File.Delete(strSubmitCreatingBatFilename);

            List<string> logList = obCommand.GetLogAsArray();
            if (logList.Count > 1)
            {
                // This command's output will look similar to:
                // Change 247135 created.
                // 
                // The output should only extend one line (via the "-m 1" option).
                // Extract the last changelist created.
                //
                foreach (string strLogLine in logList)
                {
                    if (String.IsNullOrEmpty(strLogLine) == false && strLogLine.StartsWith("Change") == true)
                    {
                        string[] splitString = strLogLine.Split(" ".ToCharArray());

                        //We rely on the second token being the last changelist number  (e.g. Changelist 247135).
                        if (splitString.Length >= 2)
                        {
                            string changelistNumber = splitString[1];

                            //Ensure that we can parse this supposed integer properly.
                            int changelistInt;
                            if (Int32.TryParse(changelistNumber, out changelistInt) == true)
                            {
                                return changelistInt;
                            }
                        }
                    }
                }
            }

            return -1;
        }

        static public bool CheckOut(string filePath, int changelistNumber)
        {
            // Execute the command
            rageStatus obStatus = new rageStatus();
            rageExecuteCommand obCommand = new rageExecuteCommand();
            obCommand.Command = m_perforceCommand;
            obCommand.Arguments = CommonPerforceOptions + "edit -c " + changelistNumber + " \"" + filePath + "\"";
            obCommand.LogToHtmlFile = false;
            obCommand.RemoveLog = true;
            obCommand.UseBusySpinner = false;
            obCommand.EchoToConsole = EchoToConsole;
            if (EchoToConsole) Console.WriteLine(obCommand.GetDosCommand());
            obCommand.Execute(out obStatus);
            if (obStatus.Success() == false)
            {
                return false;
            }

            return true;
        }

        static public void CreateChangelist(string filePath, string description, out rageStatus obStatus)
        {
            // Next fake up a submit description
            string strSubmitDescriptionFilename = (Path.GetTempPath() + "/CompleteChangelistescription_" + rageFileUtilities.GenerateTimeBasedFilename() + ".txt");
            StreamWriter obSubmitDescriptionWriter = File.CreateText(strSubmitDescriptionFilename);

            // Write it out
            obSubmitDescriptionWriter.WriteLine("# A Perforce Change Specification.");
            obSubmitDescriptionWriter.WriteLine("#");
            obSubmitDescriptionWriter.WriteLine("#  Change:      The change number. 'new' on a new changelist.  Read-only.");
            obSubmitDescriptionWriter.WriteLine("#  Date:        The date this specification was last modified.  Read-only.");
            obSubmitDescriptionWriter.WriteLine("#  Client:      The client on which the changelist was created.  Read-only.");
            obSubmitDescriptionWriter.WriteLine("#  User:        The user who created the changelist. Read-only.");
            obSubmitDescriptionWriter.WriteLine("#  Status:      Either 'pending' or 'submitted'. Read-only.");
            obSubmitDescriptionWriter.WriteLine("#  Description: Comments about the changelist.  Required.");
            obSubmitDescriptionWriter.WriteLine("#  Jobs:        What opened jobs are to be closed by this changelist.");
            obSubmitDescriptionWriter.WriteLine("#               You may delete jobs from this list.  (New changelists only.)");
            obSubmitDescriptionWriter.WriteLine("#  Files:       What opened files from the default changelist are to be added");
            obSubmitDescriptionWriter.WriteLine("#               to this changelist.  You may delete files from this list.");
            obSubmitDescriptionWriter.WriteLine("#               (New changelists only.)");
            obSubmitDescriptionWriter.WriteLine("");
            obSubmitDescriptionWriter.WriteLine("Change:	new");
            obSubmitDescriptionWriter.WriteLine("");
            obSubmitDescriptionWriter.WriteLine("Client:	" + (String.IsNullOrEmpty(ragePerforce.WorkspaceName) ? Environment.MachineName : ragePerforce.WorkspaceName));
            obSubmitDescriptionWriter.WriteLine("");
            obSubmitDescriptionWriter.WriteLine("User:	" + (String.IsNullOrEmpty(ragePerforce.UserName) ? Environment.UserName : ragePerforce.UserName));
            obSubmitDescriptionWriter.WriteLine("");
            obSubmitDescriptionWriter.WriteLine("Status:	new");
            obSubmitDescriptionWriter.WriteLine("");
            obSubmitDescriptionWriter.WriteLine("Description:");
            obSubmitDescriptionWriter.WriteLine("\t" + description);
            obSubmitDescriptionWriter.WriteLine("");
            obSubmitDescriptionWriter.WriteLine("Files:");
            obSubmitDescriptionWriter.WriteLine("\t" + filePath + "\t#\tedit");

            // Close file
            obSubmitDescriptionWriter.Close();

            // Feed the complete client/Submit description back into perforce
            string strSubmitCreatingBatFilename = (Path.GetTempPath() + "/CompleteSubmitCreatingBatFile_" + rageFileUtilities.GenerateTimeBasedFilename() + ".bat");
            StreamWriter strSubmitCreatingBatWriter = File.CreateText(strSubmitCreatingBatFilename);
            strSubmitCreatingBatWriter.WriteLine("type \"" + strSubmitDescriptionFilename.Replace("/", "\\") + "\" | p4 " + CommonPerforceOptions + " change -i");
            strSubmitCreatingBatWriter.Close();

            // Execute the command
            rageExecuteCommand obCommand = new rageExecuteCommand();
            obCommand.Command = strSubmitCreatingBatFilename;
            obCommand.UpdateLogAfterEverySoManyLines = 500;
            obCommand.LogToHtmlFile = false;
            obCommand.RemoveLog = true;
            obCommand.UseBusySpinner = false;
            obCommand.EchoToConsole = EchoToConsole;
            if (EchoToConsole) Console.WriteLine(obCommand.GetDosCommand());
            obCommand.Execute(out obStatus);
            if (!obStatus.Success())
            {
                // Badness happened
                return;
            }

            File.Delete(strSubmitDescriptionFilename);
            File.Delete(strSubmitCreatingBatFilename);

            List<string> logList = obCommand.GetLogAsArray();
            if (logList.Count > 1)
            {
                // This command's output will look similar to:
                // Change 247135 created.
                // 
                // The output should only extend one line (via the "-m 1" option).
                // Extract the last changelist created.
                //
                foreach (string strLogLine in logList)
                {
                    if (String.IsNullOrEmpty(strLogLine) == false && strLogLine.StartsWith("Change") == true)
                    {
                        string[] splitString = strLogLine.Split(" ".ToCharArray());

                        //We rely on the second token being the last changelist number  (e.g. Changelist 247135).
                        if(splitString.Length >= 2)
                        {
                            string changelistNumber = splitString[1];

                            //Ensure that we can parse this supposed integer properly.
                            int changelistInt;
                            if (Int32.TryParse(changelistNumber, out changelistInt) == true)
                            {
                                m_currentChangelist = new Changelist(changelistInt);
                            }
                            else
                            {   
                                //We were unable to find the changelist number created.
                                obStatus = new rageStatus("Unable to properly parse the p4 changes command!");
                            }
                        }
                    }
                }
            }
        }

        static public void SyncFile( string strLocalFilePathAndFileName, out rageStatus obStatus )
        {
            strLocalFilePathAndFileName = CleanFilenameOfWildcards( strLocalFilePathAndFileName );

            // Execute the command
            rageExecuteCommand obCommand = new rageExecuteCommand();
            obCommand.WorkingDirectory = rageFileUtilities.GetLocationFromPath( strLocalFilePathAndFileName );
            obCommand.Command = m_perforceCommand;
            obCommand.Arguments = CommonPerforceOptions + "sync " + "\"" + rageFileUtilities.GetFilenameFromFilePath( strLocalFilePathAndFileName ) + "\"";
            obCommand.UpdateLogAfterEverySoManyLines = 500;
            obCommand.LogToHtmlFile = false;
            obCommand.RemoveLog = true;
            obCommand.UseBusySpinner = false;
            obCommand.EchoToConsole = EchoToConsole;
            if ( EchoToConsole ) Console.WriteLine( obCommand.GetDosCommand() );
            obCommand.Execute( out obStatus );
            if ( !obStatus.Success() )
            {
                // Badness happened
                return;
            }
        }

        static public bool IsLocalFileLatestVersion( string strLocalFilePathAndFileName, out rageStatus obStatus )
        {
            strLocalFilePathAndFileName = CleanFilenameOfWildcards( strLocalFilePathAndFileName );

            // Execute the command
            rageExecuteCommand obCommand = new rageExecuteCommand();
            obCommand.WorkingDirectory = rageFileUtilities.GetLocationFromPath( strLocalFilePathAndFileName );
            obCommand.Command = m_perforceCommand;
            obCommand.Arguments = CommonPerforceOptions + "sync -n " + "\"" + rageFileUtilities.GetFilenameFromFilePath( strLocalFilePathAndFileName ) + "\"";
            obCommand.UpdateLogAfterEverySoManyLines = 500;
            obCommand.LogToHtmlFile = false;
            obCommand.RemoveLog = true;
            obCommand.UseBusySpinner = false;
            obCommand.EchoToConsole = EchoToConsole;
            if ( EchoToConsole ) Console.WriteLine( obCommand.GetDosCommand() );
            obCommand.Execute( out obStatus );
            if ( !obStatus.Success() )
            {
                // Badness happened
                return false;
            }

            List<string> astrLogArray = obCommand.GetLogAsArray();
            foreach ( string strLogLine in astrLogArray )
            {
                if ( strLogLine.Contains( " updating " ) )
                {
                    return false;
                }
            }

            return true;
        }

        static public void CheckOutFolder( string strPerforceFolderPath, out rageStatus obStatus )
		{
			if(!strPerforceFolderPath.EndsWith("/..."))
			{
				if(strPerforceFolderPath.EndsWith("/"))
				{
					CheckOutFolder(strPerforceFolderPath +"...", out obStatus);
					return;
				}
				else
				{
					CheckOutFolder(strPerforceFolderPath +"/", out obStatus);
					return;
				}
			}
			// Execute the command
			rageExecuteCommand obCommand = new rageExecuteCommand();
			obCommand.Command = m_perforceCommand;
            obCommand.Arguments = CommonPerforceOptions + "edit " + "\"" + strPerforceFolderPath + "\"";
			obCommand.UpdateLogAfterEverySoManyLines = 500;
			obCommand.LogToHtmlFile = false;
			obCommand.RemoveLog = true;
			obCommand.UseBusySpinner = false;
			obCommand.EchoToConsole = EchoToConsole;
			if (EchoToConsole) Console.WriteLine(obCommand.GetDosCommand());
			obCommand.Execute(out obStatus);
			if (!obStatus.Success())
			{
				// Badness happened
				return;
			}
		}

        static public bool ShelveChangelist(int changelist, string path)
        {
            //NOTE:  The path must be a properly formatted Perforce path.
            rageExecuteCommand obCommand = new rageExecuteCommand();
            obCommand.Command = m_perforceCommand;
            obCommand.Arguments = CommonPerforceOptions + "shelve -c " + changelist + " -f " + path;
            obCommand.UpdateLogAfterEverySoManyLines = 500;
            obCommand.LogToHtmlFile = false;
            obCommand.RemoveLog = true;
            obCommand.UseBusySpinner = false;
            obCommand.EchoToConsole = EchoToConsole;
            if (EchoToConsole) Console.WriteLine(obCommand.GetDosCommand());
            
            rageStatus obStatus;
            obCommand.Execute(out obStatus);
            if (!obStatus.Success())
            {
                return false;
            }

            return true;
        }

        static public bool DeleteShelvedFiles(int changelist, string path)
        {
            //NOTE:  The path must be a properly formatted Perforce path.
            rageExecuteCommand obCommand = new rageExecuteCommand();
            obCommand.Command = m_perforceCommand;
            obCommand.Arguments = CommonPerforceOptions + "shelve -d -c " + changelist + " " + path;
            obCommand.UpdateLogAfterEverySoManyLines = 500;
            obCommand.LogToHtmlFile = false;
            obCommand.RemoveLog = true;
            obCommand.UseBusySpinner = false;
            obCommand.EchoToConsole = EchoToConsole;
            if (EchoToConsole) Console.WriteLine(obCommand.GetDosCommand());

            rageStatus obStatus;
            obCommand.Execute(out obStatus);
            if (!obStatus.Success())
            {
                return false;
            }

            return true;
        }

        static public bool UnshelveChangelist(int shelvedChangelist, string path, int userChangelist)
        {
            //NOTE:  The path must be a properly formatted Perforce path.

            rageExecuteCommand obCommand = new rageExecuteCommand();
            obCommand.Command = m_perforceCommand;
            obCommand.Arguments = CommonPerforceOptions + "unshelve -s " + shelvedChangelist + " -f -c " + userChangelist + " " + path;
            obCommand.UpdateLogAfterEverySoManyLines = 500;
            obCommand.LogToHtmlFile = false;
            obCommand.RemoveLog = true;
            obCommand.UseBusySpinner = false;
            obCommand.EchoToConsole = EchoToConsole;
            if (EchoToConsole) Console.WriteLine(obCommand.GetDosCommand());

            rageStatus obStatus;
            obCommand.Execute(out obStatus);
            if (!obStatus.Success())
            {
                return false;
            }

            return true;
        }

        static public void SyncFolder( string strPerforceFolderPath, int changelistNumber, out rageStatus obStatus )
        {
            if ( !strPerforceFolderPath.EndsWith( m_PerforcePathSuffix ) )
            {
                strPerforceFolderPath += m_PerforcePathSuffix;
            }

            if (changelistNumber != -1)
            {
                strPerforceFolderPath += "@" + changelistNumber;
            }

            // Execute the command
            rageExecuteCommand obCommand = new rageExecuteCommand();
            obCommand.Command = m_perforceCommand;
            obCommand.Arguments = CommonPerforceOptions + "sync " + "\"" + strPerforceFolderPath + "\"";
            obCommand.UpdateLogAfterEverySoManyLines = 500;
            obCommand.LogToHtmlFile = false;
            obCommand.RemoveLog = true;
            obCommand.UseBusySpinner = false;
            obCommand.EchoToConsole = EchoToConsole;
            if ( EchoToConsole ) Console.WriteLine( obCommand.GetDosCommand() );
            obCommand.Execute( out obStatus );
            if ( !obStatus.Success() )
            {
                // Badness happened
                return;
            }
        }

        static public bool RevertChangelist(int changelistNumber)
        {
            rageStatus obStatus = new rageStatus();

            // Execute the reverting of all files in the given changelist.
            rageExecuteCommand obCommand = new rageExecuteCommand();
            obCommand.Command = m_perforceCommand;
            obCommand.Arguments = CommonPerforceOptions + "revert -c " + changelistNumber.ToString() + " //...";
            obCommand.UpdateLogAfterEverySoManyLines = 500;
            obCommand.LogToHtmlFile = false;
            obCommand.RemoveLog = true;
            obCommand.UseBusySpinner = false;
            obCommand.EchoToConsole = EchoToConsole;
            if (EchoToConsole)
                Console.WriteLine(obCommand.GetDosCommand());
            obCommand.Execute(out obStatus);

            //Delete the changelist.
            obCommand = new rageExecuteCommand();
            obCommand.Command = m_perforceCommand;
            obCommand.Arguments = CommonPerforceOptions + "change -d " + changelistNumber.ToString();
            obCommand.UpdateLogAfterEverySoManyLines = 500;
            obCommand.LogToHtmlFile = false;
            obCommand.RemoveLog = true;
            obCommand.UseBusySpinner = false;
            obCommand.EchoToConsole = EchoToConsole;
            if (EchoToConsole)
                Console.WriteLine(obCommand.GetDosCommand());
            obCommand.Execute(out obStatus);

            return obStatus.Success();
        }

        static public void RevertChangelist(out rageStatus obStatus)
        {
            if (m_currentChangelist == null)
            {
                obStatus = new rageStatus();
                return;
            }
            
            // Execute the reverting of all files in the given changelist.
            rageExecuteCommand obCommand = new rageExecuteCommand();
            obCommand.Command = m_perforceCommand;
            obCommand.Arguments = CommonPerforceOptions + "revert -c " + m_currentChangelist.Number.ToString() + " //...";
            obCommand.UpdateLogAfterEverySoManyLines = 500;
            obCommand.LogToHtmlFile = false;
            obCommand.RemoveLog = true;
            obCommand.UseBusySpinner = false;
            obCommand.EchoToConsole = EchoToConsole;
            if (EchoToConsole) 
                Console.WriteLine(obCommand.GetDosCommand());
            obCommand.Execute(out obStatus);

            //Delete the changelist.
            obCommand = new rageExecuteCommand();
            obCommand.Command = m_perforceCommand;
            obCommand.Arguments = CommonPerforceOptions + "change -d " + m_currentChangelist.Number.ToString();
            obCommand.UpdateLogAfterEverySoManyLines = 500;
            obCommand.LogToHtmlFile = false;
            obCommand.RemoveLog = true;
            obCommand.UseBusySpinner = false;
            obCommand.EchoToConsole = EchoToConsole;
            if (EchoToConsole)
                Console.WriteLine(obCommand.GetDosCommand());
            obCommand.Execute(out obStatus);

            m_currentChangelist.Files.Clear();
            m_currentChangelist = null;
        }

		static public void RevertUnchangedFolder(string strPerforceFolderPath, out rageStatus obStatus)
		{
			if (!strPerforceFolderPath.EndsWith("/..."))
			{
				if (strPerforceFolderPath.EndsWith("/"))
				{
					RevertUnchangedFolder(strPerforceFolderPath + "...", out obStatus);
					return;
				}
				else
				{
					RevertUnchangedFolder(strPerforceFolderPath + "/", out obStatus);
					return;
				}
			}
			// Execute the command
			rageExecuteCommand obCommand = new rageExecuteCommand();
			obCommand.Command = m_perforceCommand;
            obCommand.Arguments = CommonPerforceOptions + "revert -a " + "\"" + strPerforceFolderPath + "\"";
			obCommand.UpdateLogAfterEverySoManyLines = 500;
			obCommand.LogToHtmlFile = false;
			obCommand.RemoveLog = true;
			obCommand.UseBusySpinner = false;
			obCommand.EchoToConsole = EchoToConsole;
			if (EchoToConsole) Console.WriteLine(obCommand.GetDosCommand());
			obCommand.Execute(out obStatus);
			if (!obStatus.Success())
			{
				// Badness happened
				return;
			}
		}

		static public void RevertFolder(string strPerforceFolderPath, out rageStatus obStatus)
		{
			if (!strPerforceFolderPath.EndsWith("/..."))
			{
				if (strPerforceFolderPath.EndsWith("/"))
				{
					RevertFolder(strPerforceFolderPath + "...", out obStatus);
					return;
				}
				else
				{
					RevertFolder(strPerforceFolderPath + "/", out obStatus);
					return;
				}
			}

			// Execute the command
			rageExecuteCommand obCommand = new rageExecuteCommand();
			obCommand.Command = m_perforceCommand;
			obCommand.Arguments = CommonPerforceOptions + "revert \"" + strPerforceFolderPath + "\"";
			obCommand.UpdateLogAfterEverySoManyLines = 500;
			obCommand.LogToHtmlFile = false;
			obCommand.RemoveLog = true;
			obCommand.UseBusySpinner = false;
			obCommand.EchoToConsole = EchoToConsole;
			if (EchoToConsole) Console.WriteLine(obCommand.GetDosCommand());
			obCommand.Execute(out obStatus);
			if (!obStatus.Success())
			{
				// Badness happened
				return;
			}
		}

        static public void RevertFile( string stcPerforceFileName, bool onlyIfUnchanged, out rageStatus obStatus )
        {
            stcPerforceFileName = CleanFilenameOfWildcards( stcPerforceFileName );

            // Execute the command
            rageExecuteCommand obCommand = new rageExecuteCommand();
            obCommand.Command = m_perforceCommand;
            
            obCommand.Arguments = CommonPerforceOptions + "revert ";
            if ( onlyIfUnchanged )
            {
                obCommand.Arguments += " -a ";
            }
            obCommand.Arguments += "\"" + stcPerforceFileName + "\"";

            obCommand.UpdateLogAfterEverySoManyLines = 500;
            obCommand.LogToHtmlFile = false;
            obCommand.RemoveLog = true;
            obCommand.UseBusySpinner = false;
            obCommand.EchoToConsole = EchoToConsole;
            if ( EchoToConsole ) Console.WriteLine( obCommand.GetDosCommand() );
            obCommand.Execute( out obStatus );
            if ( !obStatus.Success() )
            {
                // Badness happened
                return;
            }
        }

		static public void AddFile(string strLocalFilePathAndFileName, bool bStoreLatestOnly, out rageStatus obStatus)
		{
			// Do not add if one of the "special" types
			if (strLocalFilePathAndFileName.EndsWith(".exp") || strLocalFilePathAndFileName.EndsWith(".ilk") || strLocalFilePathAndFileName.EndsWith(".lib"))
			{
				// Special file type, so don't add it
				obStatus = new rageStatus();
			}
			else
			{
                bool bContainsWildcards = strLocalFilePathAndFileName.IndexOfAny( new char[] { '@', '#', '%', '*' } ) != -1;

				// Execute the command
				rageExecuteCommand obCommand = new rageExecuteCommand();
				obCommand.Command = m_perforceCommand;
                obCommand.Arguments = CommonPerforceOptions + "add " + (bContainsWildcards ? "-f " : "") + (bStoreLatestOnly ? "-t \"+S\" " : "") + "\"" + rageFileUtilities.GetFilenameFromFilePath( strLocalFilePathAndFileName ) + "\"";
				obCommand.WorkingDirectory = rageFileUtilities.GetLocationFromPath(strLocalFilePathAndFileName);
				obCommand.UpdateLogAfterEverySoManyLines = 500;
				obCommand.LogToHtmlFile = false;
				obCommand.RemoveLog = true;
				obCommand.UseBusySpinner = false;
				obCommand.EchoToConsole = EchoToConsole;
				if (EchoToConsole) Console.WriteLine(obCommand.GetDosCommand());
				obCommand.Execute(out obStatus);
				if (!obStatus.Success())
				{
					// Badness happened
					return;
				}
			}
		}

		static public void AddAllFilesInFolder(string strLocalFolderPath, bool bStoreLatestOnly, out rageStatus obStatus)
		{
			obStatus = new rageStatus();
			if (strLocalFolderPath.EndsWith("/"))
			{
				// No need for last slash
				AddAllFilesInFolder(strLocalFolderPath.Substring(0, strLocalFolderPath.Length - 1), bStoreLatestOnly, out obStatus);
				return;
			}

			// Ok, this is stupid, but you cannot actually add a folder, you can only add files
			// And it is quicker add an existing file and has it error, than it is to see if a file exists
			// So just add EVERYTHING and let Perforce workout what it already has
			if (Directory.Exists(strLocalFolderPath))
			{
				// Add my files
				foreach (string strFile in Directory.GetFiles(strLocalFolderPath))
				{
					// It has files, so add them
					AddFile(strFile, bStoreLatestOnly, out obStatus);
					if(!obStatus.Success())
					{
						return;
					}
				}

				// Recurse
				foreach (string strSubDir in Directory.GetDirectories(strLocalFolderPath))
				{
					// It has folders, so recurse
					AddAllFilesInFolder(strSubDir, bStoreLatestOnly, out obStatus);
					if (!obStatus.Success())
					{
						return;
					}
				}
			}
		}

        static public void DeleteFile( string strLocalFilePathAndFileName, out rageStatus obStatus )
        {
            strLocalFilePathAndFileName = CleanFilenameOfWildcards( strLocalFilePathAndFileName );

            // Execute the command
            rageExecuteCommand obCommand = new rageExecuteCommand();
            obCommand.Command = m_perforceCommand;
            obCommand.Arguments = CommonPerforceOptions + "delete \"" + rageFileUtilities.GetFilenameFromFilePath( strLocalFilePathAndFileName ) + "\"";
            obCommand.WorkingDirectory = rageFileUtilities.GetLocationFromPath( strLocalFilePathAndFileName );
            obCommand.UpdateLogAfterEverySoManyLines = 500;
            obCommand.LogToHtmlFile = false;
            obCommand.RemoveLog = true;
            obCommand.UseBusySpinner = false;
            obCommand.EchoToConsole = EchoToConsole;
            if ( EchoToConsole ) Console.WriteLine( obCommand.GetDosCommand() );
            obCommand.Execute( out obStatus );
            if ( !obStatus.Success() )
            {
                // Badness happened
                return;
            }
        }

		static public int SubmitChangelist(bool revertUnchangedFiles, string description, out rageStatus obStatus)
		{
            int changelistNumber = -1;

            if (m_currentChangelist == null || m_currentChangelist.Files.Count == 0)
            {
                obStatus = new rageStatus();
                return changelistNumber;
            }

            foreach(string file in m_currentChangelist.Files)
            {
                // Execute the resolve commands for each file.
			    rageExecuteCommand resolveCommand = new rageExecuteCommand();
                resolveCommand.Command = m_perforceCommand;
                resolveCommand.Arguments = CommonPerforceOptions + " resolve -ay \"" + file + "\"";
                resolveCommand.UpdateLogAfterEverySoManyLines = 500;
                resolveCommand.LogToHtmlFile = false;
                resolveCommand.RemoveLog = true;
                resolveCommand.UseBusySpinner = false;
                resolveCommand.EchoToConsole = EchoToConsole;
                if (EchoToConsole) Console.WriteLine(resolveCommand.GetDosCommand());
                resolveCommand.Execute(out obStatus);
			    if (!obStatus.Success())
			    {
				    // Badness happened
                    m_currentChangelist = null;
                    return changelistNumber;
			    }
            }

            if(revertUnchangedFiles == true)
            {
                // Revert any unchanged files from this changelist.
                rageExecuteCommand revertCommand = new rageExecuteCommand();
                revertCommand.Command = m_perforceCommand;
                revertCommand.Arguments = CommonPerforceOptions + " revert -a -c " + m_currentChangelist.Number;
                revertCommand.UpdateLogAfterEverySoManyLines = 500;
                revertCommand.LogToHtmlFile = false;
                revertCommand.RemoveLog = true;
                revertCommand.UseBusySpinner = false;
                revertCommand.EchoToConsole = EchoToConsole;
                if (EchoToConsole) Console.WriteLine(revertCommand.GetDosCommand());
                revertCommand.Execute(out obStatus);
                if (!obStatus.Success())
                {
                    m_currentChangelist = null;
                    return changelistNumber;
                }
            }

			// Execute the actual submit command.
            rageExecuteCommand obCommand = new rageExecuteCommand();
			obCommand.Command = m_perforceCommand;
            obCommand.Arguments = CommonPerforceOptions + " submit -c " + m_currentChangelist.Number;

            if (String.IsNullOrEmpty(description) == false)
                obCommand.Arguments += " -d " + description;

			obCommand.UpdateLogAfterEverySoManyLines = 500;
			obCommand.LogToHtmlFile = false;
			obCommand.RemoveLog = true;
			obCommand.UseBusySpinner = false;
			obCommand.EchoToConsole = EchoToConsole;
			
            if (EchoToConsole) 
                Console.WriteLine(obCommand.GetDosCommand());
			
            obCommand.Execute(out obStatus);
			
            if (!obStatus.Success())
			{
                DeleteChangelist(obStatus);
                return changelistNumber;
			}

            //Look for output that says "Changelist ###### submitted." and parse
            //the number of the changelist.
            List<string> output = obCommand.StdOutLog;
            const string changeToken = "Change ";
            const string submittedToken = " submitted.";
            const string renamedToken = " renamed change ";
            const string renamedSubmitToken = " and submitted.";
            foreach (string outputLine in output)
            {
                if (outputLine.StartsWith(changeToken) && outputLine.EndsWith(submittedToken))
                {
                    //There are two lines that have to be parsed:
                    //Change 288045 submitted.
                    //Change 288045 renamed change 288048 and submitted.

                    int startIndex = changeToken.Length;
                    int endIndex = submittedToken.Length;

                    if (outputLine.Contains(renamedToken) == true)
                    {
                        startIndex = outputLine.IndexOf(renamedToken) + renamedToken.Length;
                        endIndex = renamedSubmitToken.Length;
                    }

                    string substring = outputLine.Substring(startIndex, outputLine.Length - startIndex - endIndex);

                    if (Int32.TryParse(substring, out changelistNumber) == false)
                    {
                        changelistNumber = -1;
                    }

                    break;
                }
            }

            //We've submitted the changelist; create a new one if the API calls for it.
            m_currentChangelist = null;
            return changelistNumber;
		}

        static public void DeleteChangelist(rageStatus obStatus)
        {
            if (m_currentChangelist == null)
            {
                obStatus = new rageStatus();
                return;
            }


            // Execute the resolve commands for each file.
            rageExecuteCommand deleteCommand = new rageExecuteCommand();
            deleteCommand.Command = m_perforceCommand;
            deleteCommand.Arguments = CommonPerforceOptions + " change -d " + m_currentChangelist.Number;
            deleteCommand.UpdateLogAfterEverySoManyLines = 500;
            deleteCommand.LogToHtmlFile = false;
            deleteCommand.RemoveLog = true;
            deleteCommand.UseBusySpinner = false;
            deleteCommand.EchoToConsole = EchoToConsole;
            if (EchoToConsole) Console.WriteLine(deleteCommand.GetDosCommand());
            deleteCommand.Execute(out obStatus);
            if (!obStatus.Success())
            {
                // Badness happened
                m_currentChangelist = null;
                return;
            }

            m_currentChangelist = null;
        }

		static public string[] GetMyPendingChangeLists(out rageStatus obStatus)
		{
			return GetPendingChangeLists(Environment.MachineName.ToLower(), out obStatus);
		}

		static public string[] GetPendingChangeLists(string strClient, out rageStatus obStatus)
		{
			List<string> astrReturnMe = new List<string>();
			// Execute the command
			rageExecuteCommand obCommand = new rageExecuteCommand();
			obCommand.Command = m_perforceCommand;
			obCommand.Arguments = CommonPerforceOptions + " changes -s pending -c  " + strClient;
			obCommand.UpdateLogAfterEverySoManyLines = 500;
			obCommand.LogToHtmlFile = false;
			obCommand.RemoveLog = true;
			obCommand.UseBusySpinner = false;
			obCommand.EchoToConsole = EchoToConsole;
			if (EchoToConsole) Console.WriteLine(obCommand.GetDosCommand());
			obCommand.Execute(out obStatus);
			if (!obStatus.Success())
			{
				// Badness happened
				return astrReturnMe.ToArray();
			}

			// Parse output
			List<string> logList = obCommand.GetLogAsArray();
			if (logList.Count > 1)
			{
				// The array will look something like this....
				//Change 72594 on 2006/10/18 by ragemonkey@SANW-FRG01 *pending* 'Tools automagically added by th'
				//Change 72572 on 2006/10/18 by ragemonkey@SANW-FRG01 *pending* 'Tools automagically added by th'
				//Change 72556 on 2006/10/18 by ragemonkey@SANW-FRG01 *pending* 'Tools automagically added by th'	
				//
				//
				// So extract the changelist numbers from it
				foreach (string strLogLine in logList)
				{
					if (strLogLine != "")
					{
						// Split it up
						String[] astrLogLineParts = strLogLine.Split(" ".ToCharArray());
						if(astrLogLineParts.Length > 1)
						{
							astrReturnMe.Add(astrLogLineParts[1]);
						}
					}
				}
			}


			// Return changelists
			return astrReturnMe.ToArray();
		}

		static public void DeleteEmptyPendingChangeLists(out rageStatus obStatus)
		{
			// Get my pending changelists
			String[] astrMyPendingChangeLists = GetMyPendingChangeLists(out obStatus);
			if(!obStatus.Success())
			{
				// Badness happened
			}

			// Delete change lists
			foreach(String strChangeList in astrMyPendingChangeLists)
			{
				// Execute the command
				rageExecuteCommand obCommand = new rageExecuteCommand();
				obCommand.Command = m_perforceCommand;
				obCommand.Arguments = CommonPerforceOptions + " change -d " + strChangeList;
				obCommand.UpdateLogAfterEverySoManyLines = 500;
				obCommand.LogToHtmlFile = false;
				obCommand.RemoveLog = true;
				obCommand.UseBusySpinner = false;
				obCommand.EchoToConsole = EchoToConsole;
				if (EchoToConsole) Console.WriteLine(obCommand.GetDosCommand());
				obCommand.Execute(out obStatus);
				if (!obStatus.Success())
				{
					// Badness happened
					return;
				}
			}
		}

		static public string[] GetWorkspacesForCurrentUserOnCurrentHost()
		{
            ragePerforceClientspec[] clientspecs = GetClientspecsForCurrentUserOnCurrentHost();

            List<string> workspaces = new List<string>();
            foreach ( ragePerforceClientspec clientspec in clientspecs )
            {
                workspaces.Add( clientspec.Client );
            }

            return workspaces.ToArray();
		}

		static public string[] GetWorkspacesForAUsersOnAHost(string strUserName, string strHost)
		{
            ragePerforceClientspec[] clientspecs = GetClientspecsForAUsersOnAHost( strUserName, strHost );

            List<string> workspaces = new List<string>();
            foreach ( ragePerforceClientspec clientspec in clientspecs )
            {
                workspaces.Add( clientspec.Client );
            }

            return workspaces.ToArray();
        }

        static public string[] GetWorkspacesForAUsersOnAHostAndPort( string strUserName, string strHost, string strPort )
        {
            ragePerforceClientspec[] clientspecs = GetClientspecsForAUsersOnAHostAndPort( strUserName, strHost, strPort );

            List<string> workspaces = new List<string>();
            foreach ( ragePerforceClientspec clientspec in clientspecs )
            {
                workspaces.Add( clientspec.Client );
            }

            return workspaces.ToArray();
        }

        static public ragePerforceClientspec[] GetClientspecsForCurrentUserOnCurrentHost()
        {
            return GetClientspecsForAUsersOnAHost( Environment.UserName, Environment.MachineName );
        }

        static public ragePerforceClientspec[] GetClientspecsForAUsersOnAHost( string strUserName, string strHost )
        {
            return GetClientspecsForAUsersOnAHostAndPort( strUserName, strHost, string.Empty );
        }

        static public ragePerforceClientspec[] GetClientspecsForAUsersOnAHostAndPort( string strUserName, string strHost, string strPort )
        {
            strUserName = strUserName.ToLower();
            strHost = strHost.ToLower();
            List<ragePerforceClientspec> obAClientspecReturnMe = new List<ragePerforceClientspec>();

            // Get the list of files
            rageStatus obStatus;
            rageExecuteCommand obP4Command = new rageExecuteCommand();
            obP4Command.Command = m_perforceCommand;

            if ( !String.IsNullOrEmpty( strPort ) )
            {
                obP4Command.Arguments = "-p " + strPort + " workspaces";
            }
            else
            {
                obP4Command.Arguments = "workspaces";
            }

            obP4Command.UseBusySpinner = false;
            obP4Command.UpdateLogFileInRealTime = false;
            obP4Command.LogToHtmlFile = false;
            obP4Command.RemoveLog = true;
            obP4Command.EchoToConsole = EchoToConsole;
            if ( EchoToConsole ) Console.WriteLine( obP4Command.GetDosCommand() );
            obP4Command.Execute( out obStatus );
            if ( !obStatus.Success() )
            {
                // Badness
                return obAClientspecReturnMe.ToArray();
            }

            List<string> logList = obP4Command.GetLogAsArray();
            if ( logList.Count > 0 )
            {
                // The array will look something like this....
                //Client JWOOD 2006/09/15 root null 'Created by woodward. '
                //Client KBACA 2006/09/12 root C:\Soft 'Created by kbaca. '
                //Client kbaca_rage_north 2006/09/22 root X:\ 'Created by kbaca. '
                //Client klaas-temp-workspace 2006/08/31 root x:\rage 'Created by adam. '
                //Client KMURF-soft 2006/09/01 root c:\soft 'created by MakeClient for kmurfitt '
                //Client KROBE-soft 2006/09/04 root c:\soft 'created by MakeClient for kroberts '
                //Client KROSE 2006/09/19 root C:/soft/ 'Created by CreateCompleteWorkSpace. '
                //Client krose_ragedev 2006/09/06 root null 'Created by krose. '
                //Client KTAKE-soft 2006/09/04 root c:\soft 'created by MakeClient for ktakeuchi '
                //
                //
                // So extract the workspace names
                foreach ( string strLogLine in logList )
                {
                    if ( strLogLine.ToLower().Contains( strUserName ) || strLogLine.ToLower().Contains( strHost ) )
                    {
                        string[] astrWorkspaceParts = strLogLine.Split( ' ' );
                        string strWorkspaceName = astrWorkspaceParts[1];

                        // and get the full workspace info
                        ragePerforceClientspec clientSpec = GetClientspec( strWorkspaceName, strPort );
                        if ( (clientSpec != null) && (clientSpec.Owner.ToLower() == strUserName) && (clientSpec.Host.ToLower() == strHost) )
                        {
                            obAClientspecReturnMe.Add( clientSpec );
                        }
                    }
                }
            }
            return obAClientspecReturnMe.ToArray();
        }

        static public ragePerforceClientspec GetClientspec( string strWorkspaceName, string strPort )
        {
            rageExecuteCommand obP4Command = new rageExecuteCommand();

            // Got the workspace's name, next get the workspace's information
            obP4Command.Command = m_perforceCommand;

            if ( !String.IsNullOrEmpty( strPort ) )
            {
                obP4Command.Arguments = "-p " + strPort + " workspace -o " + strWorkspaceName;
            }
            else
            {
                obP4Command.Arguments = "workspace -o " + strWorkspaceName;
            }

            obP4Command.UseBusySpinner = false;
            obP4Command.UpdateLogFileInRealTime = false;
            obP4Command.LogToHtmlFile = false;
            obP4Command.RemoveLog = true;
            obP4Command.EchoToConsole = EchoToConsole;
            if ( EchoToConsole )
            {
                Console.WriteLine( obP4Command.GetDosCommand() );
            }

            rageStatus obStatus;
            obP4Command.Execute( out obStatus );
            if ( !obStatus.Success() )
            {
                // Badness
                return null;
            }
            List<string> obAStrWorkspaceInfo = obP4Command.GetLogAsArray();
            if ( obAStrWorkspaceInfo.Count > 0 )
            {
                ragePerforceClientspec clientspec = ParseClientspec( obAStrWorkspaceInfo );
                clientspec.Port = strPort;

                return clientspec;
            }

            return null;
        }

        static private ragePerforceClientspec ParseClientspec( List<string> lines )
        {
            // The array will look something like this....
            //# A Perforce Client Specification.
            //#
            //#  Client:      The client name.
            //#  Update:      The date this specification was last modified.
            //#  Access:      The date this client was last used in any way.
            //#  Owner:       The user who created this client.
            //#  Host:        If set, restricts access to the named host.
            //#  Description: A short description of the client (optional).
            //#  Root:        The base directory of the client workspace.
            //#  AltRoots:    Up to two alternate client workspace roots.
            //#  Options:     Client options:
            //#                      [no]allwrite [no]clobber [no]compress
            //#                      [un]locked [no]modtime [no]rmdir
            //#  LineEnd:     Text file line endings on client: local/unix/mac/win/share.
            //#  View:        Lines to map depot files into the client workspace.
            //#
            //# Use 'p4 help client' to see more about client views and options.

            //Client: KROSE
            //Update: 2006/09/19 10:58:01
            //Access: 2006/09/22 10:34:56
            //Owner:  krose
            //Host:   KROSE
            //Description:
            //        Created by CreateCompleteWorkSpace.
            //Root:   C:/soft/
            //Options:        noallwrite noclobber nocompress unlocked nomodtime normdir
            //LineEnd:        local
            //View:
            //        //rage/dev/... //KROSE/dev/...							//
            //
            // So extract the info I need
            ragePerforceClientspec clientspec = new ragePerforceClientspec();

            bool inDescription = false;
            bool inAltRoots = false;
            bool inView = false;

            foreach ( string line in lines )
            {
                if ( line.StartsWith( "#" ) )
                {
                    continue;
                }

                if ( line == string.Empty )
                {
                    if ( inDescription )
                    {
                        inDescription = false;
                    }
                    else if ( inAltRoots )
                    {
                        inAltRoots = false;
                    }
                    else if ( inView )
                    {
                        inView = false;
                    }

                    continue;
                }

                if ( line.StartsWith( "Client" ) )
                {
                    clientspec.Client = line.Substring( line.LastIndexOf( '\t' ) + 1 );
                }
                else if ( line.StartsWith( "Update" ) )
                {
                    clientspec.Update = DateTime.Parse( line.Substring( line.LastIndexOf( '\t' ) + 1 ) );
                }
                else if ( line.StartsWith( "Access" ) )
                {
                    clientspec.Access = DateTime.Parse( line.Substring( line.LastIndexOf( '\t' ) + 1 ) );
                }
                else if ( line.StartsWith( "Owner" ) )
                {
                    clientspec.Owner = line.Substring( line.LastIndexOf( '\t' ) + 1 );
                }
                else if ( line.StartsWith( "Host" ) )
                {
                    clientspec.Host = line.Substring( line.LastIndexOf( '\t' ) + 1 );
                }
                else if ( line.StartsWith( "Description" ) )
                {
                    inDescription = true;
                }
                else if ( line.StartsWith( "Root" ) )
                {
                    clientspec.Root = line.Substring( line.LastIndexOf( '\t' ) + 1 );

                    //Currently in RAGE we set the Perforce Clientspec root to 'null' since
                    //we have a nasty habit to specify different drives in a client spec.
                    //
                    if(String.Compare(clientspec.Root, "null", true) == 0)
                        clientspec.Root = null;
                }
                else if ( line.StartsWith( "AltRoots" ) )
                {
                    inAltRoots = true;
                }
                else if ( line.StartsWith( "Options" ) )
                {
                    string str = line.Substring( line.LastIndexOf( '\t' ) + 1 ).Trim();

                    string[] split = str.Split();
                    foreach ( string s in split )
                    {                       
                        switch ( s )
                        {
                            case "allwrite":
                                clientspec.AllWriteOption = true;
                                break;
                            case "clobber":
                                clientspec.ClobberOption = true;
                                break;
                            case "compress":
                                clientspec.CompressOption = true;
                                break;
                            case "locked":
                                clientspec.LockedOption = true;
                                break;
                            case "modtime":
                                clientspec.ModTimeOption = true;
                                break;
                            case "rmdir":
                                clientspec.RmDirOption = true;
                                break;
                        }
                    }
                }
                else if ( line.StartsWith( "LineEnd" ) )
                {
                    string str = line.Substring( line.LastIndexOf( '\t' ) + 1 ).Trim();

                    clientspec.LineEnd = (ragePerforceClientspec.LineEndType)Enum.Parse( typeof( ragePerforceClientspec.LineEndType ), 
                        str, true );
                }
                else if ( line.StartsWith( "View" ) )
                {
                    inView = true;
                }
                else if ( line.StartsWith( "\t" ) )
                {
                    string str = line.Trim();
                    if ( inDescription )
                    {
                        if ( String.IsNullOrEmpty( clientspec.Description ) )
                        {
                            clientspec.Description = str;
                        }
                        else
                        {
                            clientspec.Description += Environment.NewLine + str;
                        }
                    }
                    else if ( inAltRoots )
                    {
                        clientspec.AltRoots.Add( str );
                    }
                    else if ( inView )
                    {
                        string[] split = str.Split( new char[] { '.' }, StringSplitOptions.RemoveEmptyEntries );
                        if ( split.Length == 2 )
                        {
                            ragePerforceView view = new ragePerforceView();
                            view.PerforcePath = split[0].ToLower().Trim();
                            view.LocalPath = split[1].ToLower().Trim();

                            //Client Specification workspaces use "//<workspace name/".
                            string clientSpecPrefix = "//" + clientspec.Client.ToLower() + "/";
                            if(clientspec.Root == null)
                            {
                                view.LocalPath = view.LocalPath.Replace(clientSpecPrefix, "");    
                            }
                            else
                            {
                                string root = clientspec.Root.ToLower();
                                if (root.EndsWith("/") == false && root.EndsWith("\\") == false)
                                {
                                    root += "/";
                                }

                                view.LocalPath = view.LocalPath.Replace(clientSpecPrefix, root);
                            }

                            view.LocalPath = view.LocalPath.Replace("/", "\\");
                            view.PerforcePath = view.PerforcePath.Replace("\\", "/");

                            clientspec.Views.Add( view );
                        }
                    }
                }
            }

            return clientspec;
        }

        static public ragePerforceUser[] GetUsers()
        {
            rageExecuteCommand obP4Command = new rageExecuteCommand();

            // Got the workspace's name, next get the workspace's information
            obP4Command.Command = m_perforceCommand;
            obP4Command.Arguments = String.Format( "{0}users", (Port != "") ? String.Format( "-p {0} ", Port ) : "" );
            obP4Command.UseBusySpinner = false;
            obP4Command.UpdateLogFileInRealTime = false;
            obP4Command.LogToHtmlFile = false;
            obP4Command.RemoveLog = true;
            obP4Command.EchoToConsole = EchoToConsole;
            if ( EchoToConsole )
            {
                Console.WriteLine( obP4Command.GetDosCommand() );
            }

            rageStatus obStatus;
            obP4Command.Execute( out obStatus );
            if ( !obStatus.Success() )
            {
                // Badness
                return null;
            }

            List<ragePerforceUser> obAUsers = new List<ragePerforceUser>();

            List<string> obAStrUserInfos = obP4Command.GetLogAsArray();
            foreach ( string obAStrUserInfo in obAStrUserInfos )
            {
                ragePerforceUser obUser = ParseUser( obAStrUserInfo );
                if ( obUser != null )
                {
                    obAUsers.Add( obUser );
                }
            }

            return obAUsers.ToArray();
        }

        /// <summary>
        /// Determines if the given filename (either local or server path) exists in the current Perforce project.
        /// </summary>
        /// <param name="strFilename">the filename to check</param>
        /// <param name="obStatus">The status of the operation.</param>
        /// <returns><c>true</c> if it exists, otherwise <c>false</c>.</returns>
        public static bool FileExists( string strFilename, out rageStatus obStatus )
        {
            strFilename = CleanFilenameOfWildcards( strFilename );

            rageExecuteCommand obP4Command = new rageExecuteCommand();          

            // Got the workspace's name, next get the workspace's information
            obP4Command.Command = m_perforceCommand;
            obP4Command.Arguments = String.Format( "{0}have \"{1}\"", (Port != "") ? String.Format( "-p {0} ", Port ) : "", strFilename );
            obP4Command.UseBusySpinner = false;
            obP4Command.UpdateLogFileInRealTime = false;
            obP4Command.LogToHtmlFile = false;
            obP4Command.RemoveLog = true;
            obP4Command.EchoToConsole = EchoToConsole;
            obP4Command.TimeOutInSeconds = 30;
            if ( EchoToConsole )
            {
                Console.WriteLine( obP4Command.GetDosCommand() );
            }

            obP4Command.Execute( out obStatus );

            foreach ( string strLine in obP4Command.GetLogAsArray() )
            {
                if ( strLine.Contains( " file(s) not on client." ) || strLine.Contains(" is not under client's root"))
                {
                    // Not really badness. Just an untracked file.
                    obStatus = new rageStatus();
                    return false;
                }
            }

            if ( !obStatus.Success() )
            {
                // Badness (pass errored status out)
                return false;
            }

            obStatus = new rageStatus();
            return true;
        }

        private static ragePerforceUser ParseUser( string strUserInfo )
        {
            string[] strSplit = strUserInfo.Split( new char[] { '(', ')', '<', '>' }, StringSplitOptions.RemoveEmptyEntries );
            if ( strSplit.Length >= 2 )
            {
                return new ragePerforceUser( strSplit[0].Trim(), strSplit[1].Trim() );
            }

            return null;
        }

        private static string CleanFilenameOfWildcards( string filename )
        {
            string formattedFilename = filename.Replace( "%", "%25" );
            formattedFilename = formattedFilename.Replace( "@", "%40" );
            formattedFilename = formattedFilename.Replace( "#", "%23" );
            formattedFilename = formattedFilename.Replace( "*", "%2A" );
            return formattedFilename;
        }

        static private bool m_bEchoToConsole = false;
		static public bool EchoToConsole
		{
			get { return m_bEchoToConsole; }
			set
			{
				m_bEchoToConsole = value;
			}
		}

		static private string m_strWorkspaceName = "";
		static public string WorkspaceName
		{
			get { return m_strWorkspaceName; }
			set
			{
				m_strWorkspaceName = value;
			}
		}


		static private string m_strUserName = "";
		static public string UserName
		{
			get { return m_strUserName; }
			set
			{
				m_strUserName = value;
			}
		}

		static private string m_strPort = "";
		static public string Port
		{
			get { return m_strPort; }
			set
			{
				m_strPort = value;
			}
		}

		static public string CommonPerforceOptions
		{
			get
			{
				return "" + ((WorkspaceName != "") ? (" -c " + WorkspaceName) : " ") + " " + ((Port != "") ? (" -p " + Port) : " ") + " " + ((UserName != "") ? (" -u " + UserName) : " ") + " ";
			}
		}
	}
}
