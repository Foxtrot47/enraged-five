﻿
using System;
using System.Windows.Forms;
using RSG.Base.Command;


namespace RSG.Base.Forms.Controls
{

    /// <summary>
    /// 
    /// </summary>
    public class PropertyGrid : System.Windows.Forms.PropertyGrid
    {
        #region Published Events

        #endregion // Published Events

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public PropertyGrid()
            : base()
        {
        }
        #endregion // Constructor(s)

        #region Controller Methods

        #endregion // Controller Methods

        #region Overridden Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPropertyValueChanged(PropertyValueChangedEventArgs e)
        {
            base.OnPropertyValueChanged(e);
        }
        #endregion // Overridden Methods
    }

} // RSG.Base.Forms.Controls namespace
