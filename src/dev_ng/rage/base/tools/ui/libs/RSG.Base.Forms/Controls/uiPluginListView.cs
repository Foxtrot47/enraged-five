//
// File: uIPluginListView.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of uIPluginListView.cs class
//

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;

using RSG.Base.Plugins;

namespace RSG.Base.Controls
{

    #region Generic Plugin ListView Control
    /// <summary>
    /// Generic Plugin ListView Control
    /// </summary>
    /// Generic type parameter must be a reference type and support a 
    /// parameterless constructor method.
    /// 
    public class uIPluginListView : ListView
    {
        #region Properties and Associated Member Data
        /// <summary>
        /// Plugins List
        /// </summary>
        [Browsable(true),
        Description("List of plugins shown in view.")]
        public List<IPlugin> Plugins
        {
            get { return m_Plugins; }
            set { m_Plugins = value; Repopulate(); }
        }
        private List<IPlugin> m_Plugins;
        #endregion // Properties and Associated Member Data

        #region Constructors
        /// <summary>
        /// Default constructor
        /// </summary>
        public uIPluginListView()
            : base()
        {
            this.Plugins = null;
            Initialise();
        }
        #endregion // Constructors

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        private void Initialise()
        {
            this.HeaderStyle = ColumnHeaderStyle.Nonclickable;
            this.View = View.Details;

            // Create column headers
            this.Columns.Add("Plugin", 80);
            this.Columns.Add("Version", 80);
            this.Columns.Add("Description", 180);

            // Populate plugin items
            Repopulate();
        }

        /// <summary>
        /// Repopulate ListView
        /// </summary>
        private void Repopulate()
        {
            if (!this.DesignMode && null != this.Plugins)
            {
                foreach (IPlugin plugin in this.Plugins)
                {
                    this.Items.Add(new uIPluginListViewItem(plugin));
                }
            }
        }
        #endregion // Private Methods
    }
    #endregion // Plugin ListView Control

    #region Plugin ListViewItem Class
    /// <summary>
    /// Plugin ListViewItem Class
    /// </summary>
    class uIPluginListViewItem : ListViewItem
    {
        #region Properties and Associated Member Data
        /// <summary>
        /// Associated Plugin
        /// </summary>
        public IPlugin Plugin
        {
            get { return m_Plugin; }
            private set { m_Plugin = value; }
        }
        private IPlugin m_Plugin;
        #endregion // Properties and Associated Member Data

        #region Constructors
        /// <summary>
        /// Constructor
        /// </summary>
        public uIPluginListViewItem(IPlugin plugin)
            : base()
        {
            this.Plugin = plugin;
            this.Text = this.Plugin.Name;
            this.SubItems.Add(new ListViewSubItem(this, this.Plugin.Version.ToString()));
            this.SubItems.Add(new ListViewSubItem(this, this.Plugin.Description));
        }

        /// <summary>
        /// Constructor
        /// </summary>
        public uIPluginListViewItem(String text, IPlugin plugin)
            : base(text)
        {
            this.Plugin = plugin;
        }
        #endregion // Constructors
    }
    #endregion // Plugin ListViewItem Class

} // End of RSG.Base.Controls namespace

// End of file
