using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using RSG.Base.Forms.Controls;

namespace RSG.Base.Forms
{

    /// <summary>
    /// 
    /// </summary>
    public partial class MessageListBoxTimeout : Form
    {

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public MessageListBoxTimeout()
        {
            InitializeComponent();
        }
        #endregion // Constructor(s)
    }

} // RSG.Base.Forms namespace