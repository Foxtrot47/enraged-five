using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace RSG.Base.Forms
{
    #region Enums
    public enum rageSearchReplaceType
    {
        Find,
        Replace,
        FindAll,
        ReplaceAll,
    }

    public enum rageSearchReplaceResult
    {
        Found,
        Error,
        PastEndOfFile,
        PastStartOfSearch,
        NotFound,
        Cancelled
    };
    #endregion

    /// <summary>
    /// Holds the parameters for a RichTextBox Search/Replace.
    /// </summary>
    public class rageRichTextBoxSearchReplaceOptions
    {
        public rageRichTextBoxSearchReplaceOptions()
        {

        }

        #region Variables
        private string m_findString = string.Empty;
        private string m_replaceString = string.Empty;
        private RichTextBoxFinds m_finds = RichTextBoxFinds.None;
        #endregion

        #region Properties
        /// <summary>
        /// The text to find.
        /// </summary>
        public string FindText
        {
            get
            {
                return m_findString;
            }
            set
            {
                m_findString = value;
            }
        }

        /// <summary>
        /// The text to replace with on a Search and Replace operation.
        /// </summary>
        public string ReplaceText
        {
            get
            {
                return m_replaceString;
            }
            set
            {
                m_replaceString = value;
            }
        }

        /// <summary>
        /// Match the case of the FindText.
        /// </summary>
        public bool MatchCase
        {
            get
            {
                return (m_finds & RichTextBoxFinds.MatchCase) != 0;
            }
            set
            {
                if ( value )
                {
                    m_finds |= RichTextBoxFinds.MatchCase;
                }
                else
                {
                    m_finds &= ~RichTextBoxFinds.MatchCase;
                }
            }
        }

        /// <summary>
        /// Whether or not to highlight the FindText if it is found.
        /// </summary>
        public bool NoHighlight
        {
            get
            {
                return (m_finds & RichTextBoxFinds.NoHighlight) != 0;
            }
            set
            {
                if ( value )
                {
                    m_finds |= RichTextBoxFinds.NoHighlight;
                }
                else
                {
                    m_finds &= ~RichTextBoxFinds.NoHighlight;
                }
            }
        }

        /// <summary>
        /// The search direction.
        /// </summary>
        public bool Reverse
        {
            get
            {
                return (m_finds & RichTextBoxFinds.Reverse) != 0;
            }
            set
            {
                if ( value )
                {
                    m_finds |= RichTextBoxFinds.Reverse;
                }
                else
                {
                    m_finds &= ~RichTextBoxFinds.Reverse;
                }
            }
        }

        /// <summary>
        /// Match FindText as a whole word.
        /// </summary>
        public bool WholeWord
        {
            get
            {
                return (m_finds & RichTextBoxFinds.WholeWord) != 0;
            }
            set
            {
                if ( value )
                {
                    m_finds |= RichTextBoxFinds.WholeWord;
                }
                else
                {
                    m_finds &= ~RichTextBoxFinds.WholeWord;
                }
            }
        }

        /// <summary>
        /// Retrieves the bitmask of RichTextBoxFinds used by the RichTextBox Find function.
        /// </summary>
        public RichTextBoxFinds Finds
        {
            get
            {
                return m_finds;
            }
        }
        #endregion
    }

    /// <summary>
    /// Holds the rageRichTextBoxSearchReplaceOptions and Search/Replace history for use by a user settings system.
    /// </summary>
    public class rageRichTextBoxSearchReplaceSettings
    {
        public rageRichTextBoxSearchReplaceSettings()
        {
            Reset();
        }

        #region Variables
        private rageRichTextBoxSearchReplaceOptions m_searchReplaceOptions = new rageRichTextBoxSearchReplaceOptions();
        private List<string> m_findHistory = new List<string>();
        private List<string> m_replaceHistory = new List<string>();
        #endregion

        #region Properties
        public rageRichTextBoxSearchReplaceOptions FindReplaceOptions
        {
            get
            {
                return m_searchReplaceOptions;
            }
            set
            {
                if ( value != null )
                {
                    m_searchReplaceOptions = value;
                }
            }
        }

        [XmlIgnore]
        public List<string> FindHistory
        {
            get
            {
                return m_findHistory;
            }
            set
            {
                m_findHistory = value;
            }
        }

        [XmlArray( "FindHistory" )]
        public string[] FindHistoryXml
        {
            get
            {
                return m_findHistory.ToArray();
            }
            set
            {
                m_findHistory.Clear();
                if ( value != null )
                {
                    m_findHistory.AddRange( value );
                }
            }
        }

        [XmlIgnore]
        public List<string> ReplaceHistory
        {
            get
            {
                return m_replaceHistory;
            }
            set
            {
                m_replaceHistory = value;
            }
        }

        [XmlArray( "ReplaceHistory" )]
        public string[] ReplaceHistoryXml
        {
            get
            {
                return m_replaceHistory.ToArray();
            }
            set
            {
                m_replaceHistory.Clear();
                if ( value != null )
                {
                    m_replaceHistory.AddRange( value );
                }
            }
        }
        #endregion

        #region Public Functions
        public void Reset()
        {
            this.FindReplaceOptions.FindText = string.Empty;
            this.FindReplaceOptions.MatchCase = false;
            this.FindReplaceOptions.NoHighlight = false;
            this.FindReplaceOptions.ReplaceText = string.Empty;
            this.FindReplaceOptions.Reverse = false;
            this.FindReplaceOptions.WholeWord = false;

            m_findHistory.Clear();
            m_replaceHistory.Clear();
        }

        public void CopyFrom( rageRichTextBoxSearchReplaceSettings f )
        {
            if ( f == null )
            {
                return;
            }

            this.FindReplaceOptions.FindText = f.FindReplaceOptions.FindText;
            this.FindReplaceOptions.MatchCase = f.FindReplaceOptions.MatchCase;
            this.FindReplaceOptions.NoHighlight = f.FindReplaceOptions.NoHighlight;
            this.FindReplaceOptions.ReplaceText = f.FindReplaceOptions.ReplaceText;
            this.FindReplaceOptions.Reverse = f.FindReplaceOptions.Reverse;
            this.FindReplaceOptions.WholeWord = f.FindReplaceOptions.WholeWord;

            this.FindHistory.Clear();
            this.FindHistory.AddRange( f.FindHistory );

            this.ReplaceHistory.Clear();
            this.ReplaceHistory.AddRange( f.ReplaceHistory );
        }

        public string Serialize()
        {
            try
            {
                XmlSerializer serializer = new XmlSerializer( this.GetType() );
                using (StringWriter writer = new StringWriter())
                {
                    serializer.Serialize( writer, this );
                    return writer.ToString();
                }
            }
            catch ( Exception e )
            {
                Console.WriteLine( "rageRichTextBoxSearchReplaceSettings Error serializing: {0}", e.Message );
                return null;
            }
        }

        public bool Deserialize( string serializedText )
        {
            try
            {
                XmlSerializer serializer = new XmlSerializer(this.GetType());
                using (TextReader reader = new StringReader(serializedText))
                {
                    rageRichTextBoxSearchReplaceSettings settings = serializer.Deserialize(reader) as rageRichTextBoxSearchReplaceSettings;

                    if ( settings == null )
                    {
                        Console.WriteLine( "rageRichTextBoxSearchReplaceSettings unable to deserialize string");
                        return false;
                    }

                    this.CopyFrom( settings );
                    return true;
                }
            }
            catch ( Exception e )
            {
                Console.WriteLine( "rageRichTextBoxSearchReplaceSettings Error deserializing string: {0}", e.Message );
                return false;
            }
        }
        #endregion
    }

    /// <summary>
    /// Holds the data associated with a single result in a RichTextBox Find operation.
    /// </summary>
    internal class rageSearchReplaceResultData
    {
        public rageSearchReplaceResultData( int offset, bool found, bool pastStartOfSearch, bool pastEndOfFile, int endOffset )
        {
            m_offset = offset;
            m_found = found;
            m_pastStartOfSearch = pastStartOfSearch;
            m_pastEndOfFile = pastEndOfFile;
            m_endOffset = endOffset;
        }

        #region Variables
        private int m_offset;
        private bool m_found = false;
        private bool m_pastStartOfSearch = false;
        private bool m_pastEndOfFile = false;
        private int m_endOffset = 0;
        #endregion

        #region Properties
        public int Offset
        {
            get
            {
                return m_offset;
            }
        }

        public bool Found
        {
            get
            {
                return m_found;
            }
        }

        public bool PastStartOfSearch
        {
            get
            {
                return m_pastStartOfSearch;
            }
        }

        public bool PastEndOfFile
        {
            get
            {
                return m_pastEndOfFile;
            }
        }

        public int EndOffset
        {
            get
            {
                return m_endOffset;
            }
        }
        #endregion
    }

    internal class SearchRichTextBox
    {
        public SearchRichTextBox( RichTextBox rtb )
        {
            m_richTextBox = rtb;
        }

        #region Variables
        private RichTextBox m_richTextBox;
        #endregion

        #region Properties
        public RichTextBox RichTextBox
        {
            get
            {
                return m_richTextBox;
            }
        }
        #endregion

        #region Public Functions
        public string GetSelectedText( int offset )
        {
            string selectedText = m_richTextBox.SelectedText;
            if ( !String.IsNullOrEmpty( selectedText ) )
            {
                if ( selectedText.Contains( "\n" ) )
                {
                    return null;
                }

                return selectedText;
            }

            int start = offset;
            int end = start;

            if ( start > m_richTextBox.TextLength )
            {
                return null;
            }
            else if ( (start == m_richTextBox.TextLength) || Char.IsWhiteSpace( m_richTextBox.Text[start] ) )
            {
                // allow us to look behind
                --start;
                --end;
            }

            if ( (m_richTextBox.TextLength > 0) && (start >= 0)
                && (Char.IsLetterOrDigit( m_richTextBox.Text[start] ) || Char.IsSymbol( m_richTextBox.Text[start] ) || (m_richTextBox.Text[start] == '_')) )
            {
                while ( (start >= 0)
                    && (Char.IsLetterOrDigit( m_richTextBox.Text[start] ) || Char.IsSymbol( m_richTextBox.Text[start] ) || (m_richTextBox.Text[start] == '_')) )
                {
                    --start;
                }

                while ( (end < m_richTextBox.TextLength)
                    && (Char.IsLetterOrDigit( m_richTextBox.Text[end] ) || Char.IsSymbol( m_richTextBox.Text[end] ) || (m_richTextBox.Text[end] == '_')) )
                {
                    ++end;
                }

                return m_richTextBox.Text.Substring( start + 1, end - start - 1 );
            }

            return null;
        }

        public rageSearchReplaceResultData FindNext( rageRichTextBoxSearchReplaceOptions options )
        {
            // if text is currently selected, start after it (or before it on Reverse)
            int startOffset = m_richTextBox.SelectionStart;
            if ( !options.Reverse && (m_richTextBox.SelectionLength > 0) )
            {
                startOffset += m_richTextBox.SelectionLength;
            }

            return FindNextInternal( options, startOffset );
        }

        public rageSearchReplaceResultData ReplaceNext( rageRichTextBoxSearchReplaceOptions options )
        {
            // if text is currently selected, start at the beginning (or at the end on Reverse)
            int startOffset = m_richTextBox.SelectionStart;
            if ( options.Reverse && (m_richTextBox.SelectionLength > 0) )
            {
                startOffset += m_richTextBox.SelectionLength;
            }

            rageSearchReplaceResultData resultData = FindNextInternal( options, startOffset );
            if ( resultData.Found )
            {
                // make the replacement
                m_richTextBox.SelectedText = options.ReplaceText;

                int offset = resultData.EndOffset;
                int endOffset = resultData.EndOffset;

                // find the next one
                resultData = FindNextInternal( options, options.Reverse ? endOffset - options.ReplaceText.Length : endOffset );

                return new rageSearchReplaceResultData( offset, true,
                    resultData.PastStartOfSearch, resultData.PastEndOfFile, resultData.EndOffset );
            }

            return resultData;
        }

        public List<rageSearchReplaceResultData> FindAll( rageRichTextBoxSearchReplaceOptions options )
        {
            List<rageSearchReplaceResultData> results = new List<rageSearchReplaceResultData>();

            bool reverse = options.Reverse;
            options.Reverse = false;

            bool noHighlight = options.NoHighlight;
            options.NoHighlight = true;

            int offset = 0;
            int indexOf = -1;
            do
            {
                indexOf = m_richTextBox.Find( options.FindText, offset, options.Finds );
                if ( indexOf != -1 )
                {
                    offset = indexOf + options.FindText.Length;
                    results.Add( new rageSearchReplaceResultData( indexOf, true, false, false, offset ) );
                }
            } while ( indexOf != -1 );

            // fix the last one
            if ( results.Count > 0 )
            {
                rageSearchReplaceResultData lastResultData = results[results.Count - 1];
                rageSearchReplaceResultData resultData = new rageSearchReplaceResultData( lastResultData.Offset,
                    lastResultData.Found, false, true, lastResultData.EndOffset );

                results[results.Count - 1] = resultData;
            }

            options.Reverse = reverse;
            options.NoHighlight = noHighlight;

            return results;
        }

        public List<rageSearchReplaceResultData> ReplaceAll( rageRichTextBoxSearchReplaceOptions options )
        {
            List<rageSearchReplaceResultData> results = new List<rageSearchReplaceResultData>();

            bool reverse = options.Reverse;
            options.Reverse = false;

            bool noHighlight = options.NoHighlight;
            options.NoHighlight = true;

            int offset = 0;
            int indexOf = -1;
            do
            {
                indexOf = m_richTextBox.Find( options.FindText, offset, options.Finds );
                if ( indexOf != -1 )
                {
                    m_richTextBox.SelectionStart = indexOf;
                    m_richTextBox.SelectionLength = options.FindText.Length;
                    m_richTextBox.SelectedText = options.ReplaceText;

                    offset = indexOf + options.ReplaceText.Length;
                    results.Add( new rageSearchReplaceResultData( indexOf, true, false, false, offset ) );
                }
            } while ( indexOf != -1 );

            // fix the last one
            if ( results.Count > 0 )
            {
                rageSearchReplaceResultData lastResultData = results[results.Count - 1];
                rageSearchReplaceResultData resultData = new rageSearchReplaceResultData( lastResultData.Offset,
                    lastResultData.Found, false, true, lastResultData.EndOffset );

                results[results.Count - 1] = resultData;
            }

            options.Reverse = reverse;
            options.NoHighlight = noHighlight;

            return results;
        }
        #endregion

        #region Private Functions
        public rageSearchReplaceResultData FindNextInternal( rageRichTextBoxSearchReplaceOptions options, int startOffset )
        {
            bool noHighlight = options.NoHighlight;
            options.NoHighlight = false;

            int indexOf = -1;
            if ( options.Reverse )
            {
                indexOf = m_richTextBox.Find( options.FindText, 0, startOffset, options.Finds );
            }
            else
            {
                indexOf = m_richTextBox.Find( options.FindText, startOffset, options.Finds );
            }

            if ( indexOf != -1 )
            {
                options.NoHighlight = noHighlight;
                return new rageSearchReplaceResultData( indexOf, true, false, false, indexOf + options.FindText.Length );
            }

            if ( options.Reverse )
            {
                if ( (startOffset == -1) || (startOffset >= m_richTextBox.TextLength) )
                {
                    options.NoHighlight = noHighlight;
                    return new rageSearchReplaceResultData( -1, false, false, false, 0 );
                }
                else
                {
                    if ( startOffset != 0 )
                    {
                        indexOf = m_richTextBox.Find( options.FindText, startOffset, -1, options.Finds );
                        if ( indexOf != -1 )
                        {
                            options.NoHighlight = noHighlight;
                            return new rageSearchReplaceResultData( indexOf, true, false, false, indexOf );
                        }
                    }

                    options.NoHighlight = noHighlight;
                    return new rageSearchReplaceResultData( -1, false, true, false, startOffset );
                }
            }
            else
            {
                if ( startOffset == 0 )
                {
                    options.NoHighlight = noHighlight;
                    return new rageSearchReplaceResultData( -1, false, false, true, m_richTextBox.TextLength );
                }
                else
                {
                    int newStartOffset = 0;
                    if ( newStartOffset != startOffset )
                    {
                        indexOf = m_richTextBox.Find( options.FindText, newStartOffset, options.Finds );
                        if ( indexOf != -1 )
                        {
                            options.NoHighlight = noHighlight;
                            return new rageSearchReplaceResultData( indexOf, true, false, false, indexOf + options.FindText.Length );
                        }
                    }

                    options.NoHighlight = noHighlight;
                    return new rageSearchReplaceResultData( -1, false, true, false, startOffset );
                }
            }
        }
        #endregion
    }

    #region EventHandlers and EventArgs
    public class rageSearchReplaceRichTextBoxEventArgs : EventArgs
    {
        public rageSearchReplaceRichTextBoxEventArgs()
        {

        }

        #region Variables
        private RichTextBox m_richTextBox;
        #endregion

        #region Properties
        public RichTextBox RichTextBox
        {
            get
            {
                return m_richTextBox;
            }
            set
            {
                m_richTextBox = value;
            }
        }
        #endregion
    }

    public delegate void rageSearchReplaceRichTextBoxEventHandler( object sender, rageSearchReplaceRichTextBoxEventArgs e );

    public class rageSearchReplaceEventArgs : EventArgs
    {
        public rageSearchReplaceEventArgs( rageSearchReplaceType searchType )
        {
            m_searchType = searchType;
        }

        #region Variables
        private rageSearchReplaceType m_searchType;
        #endregion

        #region Properties
        public rageSearchReplaceType Search
        {
            get
            {
                return m_searchType;
            }
        }
        #endregion
    }

    public delegate void rageSearchBeginEventHandler( object sender, rageSearchReplaceEventArgs e );

    public class rageSearchCompleteEventArgs : rageSearchReplaceEventArgs
    {
        public rageSearchCompleteEventArgs( int numFound, rageSearchReplaceResult result,
            string resultMessage, string resultTitle, rageSearchReplaceType searchType )
            : base( searchType )
        {
            m_numFound = numFound;
            m_result = result;
            m_resultMessage = resultMessage;
            m_resultTitle = resultTitle;
        }

        #region Variables
        private int m_numFound;
        private rageSearchReplaceResult m_result;
        private string m_resultMessage;
        private string m_resultTitle;
        #endregion

        #region Properties
        public int NumFound
        {
            get
            {
                return m_numFound;
            }
        }

        public rageSearchReplaceResult Result
        {
            get
            {
                return m_result;
            }
        }

        public string ResultMessage
        {
            get
            {
                return m_resultMessage;
            }
        }

        public string ResultTitle
        {
            get
            {
                return m_resultTitle;
            }
        }
        #endregion
    }

    public delegate void rageSearchCompleteEventHandler( object sender, rageSearchCompleteEventArgs e );

    public class rageSearchReplaceFoundStringEventArgs : rageSearchReplaceEventArgs
    {
        public rageSearchReplaceFoundStringEventArgs( int offset, string text, rageSearchReplaceType searchType )
            : base( searchType )
        {
            m_offset = offset;
            m_text = text;
        }

        #region Variables
        private int m_offset;
        private string m_text;
        #endregion

        #region Properties
        public int Offset
        {
            get
            {
                return m_offset;
            }
        }

        public string Text
        {
            get
            {
                return m_text;
            }
        }
        #endregion
    }

    public delegate void rageFoundStringEventHandler( object sender, rageSearchReplaceFoundStringEventArgs e );
    #endregion
}
