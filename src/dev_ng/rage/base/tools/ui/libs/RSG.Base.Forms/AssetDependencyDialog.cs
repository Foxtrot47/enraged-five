﻿using System;
using System.IO;
using System.Windows.Forms;

namespace RSG.Base.Forms
{

    /// <summary>
    /// 
    /// </summary>
    public partial class AssetDependencyDialog : Form
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="optional"></param>
        /// <param name="required"></param>
        public AssetDependencyDialog(String inputFile)
        {
            InitializeComponent();
            ParseFile(inputFile);
        }
        #endregion // Constructor(s)

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="inputFile"></param>
        private void ParseFile(String inputFile)
        {
            try
            {
                lvAssets.BeginUpdate();
                using (FileStream stream = new FileStream(inputFile, FileMode.Open))
                {
                    StreamReader reader = new StreamReader(stream);
                    while (!reader.EndOfStream)
                    {
                        String line = reader.ReadLine();
                        ListViewItem item = new ListViewItem();
                        item.Text = line.Trim();
                        item.SubItems.Add(line.Trim());
                        lvAssets.Items.Add(item);
                    }
                    reader.Close();
                }
            }
            finally
            {
                lvAssets.EndUpdate();
            }
        }
        #endregion // Private Methods
    }
    
} // RSG.Base.Forms namespace
