//
// File: Selection.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: 
//

using System;
using System.Collections;
using System.Collections.Generic;
using Diag = System.Diagnostics;

namespace RSG.Base.Forms.Selection
{
    
    /// <summary>
    /// Selection of objects (of type T)
    /// </summary>
    public class Selection<T> : CollectionBase
    {
        #region Events
        /// <summary>
        /// Selection change event raised when the selection is changed
        /// </summary>
        public event EventHandler<SelectionEventArgs> SelectionChangedEvent;
        #endregion // Events

        #region Properties and Associated Member Data
        #region ICollection Properties

        /// <summary>
        /// Readonly selection
        /// </summary>
        public bool IsReadOnly
        {
            get { return this.List.IsReadOnly; }
        }
        #endregion // ICollection Properties

        #region IList Properties
        /// <summary>
        /// 
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public T this[int index] 
        {
            get { return (T)(this.List[index]); }
            set 
            {
                Object[] prev = this.InnerList.ToArray();
                this.List[index] = value;
                RaiseSelectionChangedEvent(prev);
            }
        }
        #endregion // IList Properties
        #endregion // Properties and Associated Member Data

        #region Constructors
        /// <summary>
        /// Default constructor
        /// </summary>
        public Selection()
            : base()
        {
        }

        /// <summary>
        /// Constructor, specifying initial capacity
        /// </summary>
        /// <param name="capacity"></param>
        public Selection(int capacity)
            : base(capacity)
        {
        }
        #endregion // Constructors

        #region Controller Methods
        #region ICollection Methods
        /// <summary>
        /// Add an item to our selection
        /// </summary>
        /// <param name="obj">Object to add to the selection</param>
        public void Add(T obj)
        {
            Object[] prev = this.InnerList.ToArray();
            if (null != obj)
                this.List.Add(obj);

            RaiseSelectionChangedEvent(prev);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public bool Contains(T item)
        {
            return (this.List.Contains(item));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="array"></param>
        /// <param name="arrayIndex"></param>
        public void CopyTo(T[] array, int arrayIndex)
        {
            this.List.CopyTo(array, arrayIndex);
        }

        /// <summary>
        /// Remove an object from our selection
        /// </summary>
        /// <param name="obj">Object to remove from the selection</param>
        public void Remove(T obj)
        {
            Object[] prev = this.InnerList.ToArray();
            this.List.Remove(obj);
            RaiseSelectionChangedEvent(prev);
        }
        #endregion // ICollection Methods

        #region IList Methods
        /// <summary>
        /// Retrieve index of item in selection
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public int IndexOf(T item)
        {
            return (this.List.IndexOf(item));
        }

        /// <summary>
        /// Insert item into selection
        /// </summary>
        /// <param name="index"></param>
        /// <param name="item"></param>
        public void Insert(int index, T item)
        {
            Object[] prev = this.InnerList.ToArray();
            this.List.Insert(index, item);

            RaiseSelectionChangedEvent(prev);
        }
        #endregion // IList Methods

        #region Additional Methods
        /// <summary>
        /// Add a list of objects to our selection
        /// </summary>
        /// <param name="objs">List of objects to add to the selection</param>
        public void Add(List<T> objs)
        {
            foreach (T obj in objs)
                this.List.Add(obj);

            RaiseSelectionChangedEvent(null);
        }

        /// <summary>
        /// Replace list of objects with our list of objects
        /// </summary>
        /// <param name="objs">New list of objects to set selection to</param>
        public void Replace(List<T> objs)
        {
            Object[] prev = this.InnerList.ToArray();

            this.List.Clear();
            foreach (T o in objs)
                this.List.Add(o);

            RaiseSelectionChangedEvent(prev);
        }
        #endregion // Additional Methods
        #endregion // Controller Methods

        #region Protected Methods
        #region CollectionBase Overridden Methods
        protected override void OnClear()
        {
            Object[] prev = this.InnerList.ToArray();

            base.OnClear();
            RaiseSelectionChangedEvent(prev);
        }
        #endregion // CollectionBase Overridden Methods
        #endregion // Protected Methods

        #region Private Methods
        /// <summary>
        /// Raise SelectionChangedEvent (to any listeners)
        /// </summary>
        private void RaiseSelectionChangedEvent(Object[] previousSelection)
        {
            if (null != this.SelectionChangedEvent)
                this.SelectionChangedEvent(this, new SelectionEventArgs(this.InnerList.ToArray(), previousSelection));
        }
        #endregion // Private Methods
    }

    /// <summary>
    /// 
    /// </summary>
    public class SelectionEventArgs : EventArgs
    {
        #region Properties and Associated Member Data
        /// <summary>
        /// Updated selection
        /// </summary>
        public Object[] Selection
        {
            get { return m_Selection; }
        }
        private Object[] m_Selection;

        /// <summary>
        /// Previous selection
        /// </summary>
        public Object[] PreviousSelection
        {
            get { return m_PrevSelection; }
        }
        private Object[] m_PrevSelection;
        #endregion // Properties and Associated Member Data

        #region Constructor(s)
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="selection"></param>
        /// <param name="previous"></param>
        public SelectionEventArgs(Object[] selection, Object[] previous)
        {
            this.m_Selection = selection;
            this.m_PrevSelection = previous;
        }
        #endregion // Constructors(s)
    }

} // End of RSG.Base.Selection namespace

// End of file
