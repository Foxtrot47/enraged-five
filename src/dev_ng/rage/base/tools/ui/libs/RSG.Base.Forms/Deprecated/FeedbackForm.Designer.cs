namespace RSG.Base.Forms
{
    partial class FeedbackForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && (components != null) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.toLabel = new System.Windows.Forms.Label();
            this.toTextBox = new System.Windows.Forms.TextBox();
            this.yourEmailAddressLabel = new System.Windows.Forms.Label();
            this.yourEmailAddressTextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.subjectTextBox = new System.Windows.Forms.TextBox();
            this.categoryLabel = new System.Windows.Forms.Label();
            this.categoryComboBox = new System.Windows.Forms.ComboBox();
            this.attachmentsLabel = new System.Windows.Forms.Label();
            this.attachmentsTextBox = new System.Windows.Forms.TextBox();
            this.browseAttachmentsButton = new System.Windows.Forms.Button();
            this.messageTextBox = new System.Windows.Forms.TextBox();
            this.sendButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.messageLabel = new System.Windows.Forms.Label();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.zipAttachmentsCheckBox = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // toLabel
            // 
            this.toLabel.AutoSize = true;
            this.toLabel.Location = new System.Drawing.Point( 12, 9 );
            this.toLabel.Name = "toLabel";
            this.toLabel.Size = new System.Drawing.Size( 20, 13 );
            this.toLabel.TabIndex = 0;
            this.toLabel.Text = "To";
            // 
            // toTextBox
            // 
            this.toTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.toTextBox.Location = new System.Drawing.Point( 119, 6 );
            this.toTextBox.Name = "toTextBox";
            this.toTextBox.ReadOnly = true;
            this.toTextBox.Size = new System.Drawing.Size( 293, 20 );
            this.toTextBox.TabIndex = 1;
            // 
            // yourEmailAddressLabel
            // 
            this.yourEmailAddressLabel.AutoSize = true;
            this.yourEmailAddressLabel.Location = new System.Drawing.Point( 12, 35 );
            this.yourEmailAddressLabel.Name = "yourEmailAddressLabel";
            this.yourEmailAddressLabel.Size = new System.Drawing.Size( 101, 13 );
            this.yourEmailAddressLabel.TabIndex = 2;
            this.yourEmailAddressLabel.Text = "Your E-mail Address";
            // 
            // yourEmailAddressTextBox
            // 
            this.yourEmailAddressTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.yourEmailAddressTextBox.Location = new System.Drawing.Point( 119, 32 );
            this.yourEmailAddressTextBox.Name = "yourEmailAddressTextBox";
            this.yourEmailAddressTextBox.Size = new System.Drawing.Size( 293, 20 );
            this.yourEmailAddressTextBox.TabIndex = 3;
            this.yourEmailAddressTextBox.TextChanged += new System.EventHandler( this.EnableSendButtonCheck_EventHandler );
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point( 12, 61 );
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size( 43, 13 );
            this.label4.TabIndex = 4;
            this.label4.Text = "Subject";
            // 
            // subjectTextBox
            // 
            this.subjectTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.subjectTextBox.Location = new System.Drawing.Point( 119, 58 );
            this.subjectTextBox.Name = "subjectTextBox";
            this.subjectTextBox.Size = new System.Drawing.Size( 293, 20 );
            this.subjectTextBox.TabIndex = 5;
            // 
            // categoryLabel
            // 
            this.categoryLabel.AutoSize = true;
            this.categoryLabel.Location = new System.Drawing.Point( 12, 87 );
            this.categoryLabel.Name = "categoryLabel";
            this.categoryLabel.Size = new System.Drawing.Size( 49, 13 );
            this.categoryLabel.TabIndex = 6;
            this.categoryLabel.Text = "Category";
            // 
            // categoryComboBox
            // 
            this.categoryComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.categoryComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.categoryComboBox.FormattingEnabled = true;
            this.categoryComboBox.Items.AddRange( new object[] {
            "Comment",
            "Question",
            "Suggestion",
            "Bug",
            "Other Feedback"} );
            this.categoryComboBox.Location = new System.Drawing.Point( 119, 84 );
            this.categoryComboBox.Name = "categoryComboBox";
            this.categoryComboBox.Size = new System.Drawing.Size( 293, 21 );
            this.categoryComboBox.TabIndex = 7;
            this.categoryComboBox.SelectedIndexChanged += new System.EventHandler( this.EnableSendButtonCheck_EventHandler );
            // 
            // attachmentsLabel
            // 
            this.attachmentsLabel.AutoSize = true;
            this.attachmentsLabel.Location = new System.Drawing.Point( 12, 114 );
            this.attachmentsLabel.Name = "attachmentsLabel";
            this.attachmentsLabel.Size = new System.Drawing.Size( 66, 13 );
            this.attachmentsLabel.TabIndex = 8;
            this.attachmentsLabel.Text = "Attachments";
            // 
            // attachmentsTextBox
            // 
            this.attachmentsTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.attachmentsTextBox.Location = new System.Drawing.Point( 119, 111 );
            this.attachmentsTextBox.Name = "attachmentsTextBox";
            this.attachmentsTextBox.Size = new System.Drawing.Size( 262, 20 );
            this.attachmentsTextBox.TabIndex = 9;
            // 
            // browseAttachmentsButton
            // 
            this.browseAttachmentsButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.browseAttachmentsButton.Location = new System.Drawing.Point( 387, 109 );
            this.browseAttachmentsButton.Name = "browseAttachmentsButton";
            this.browseAttachmentsButton.Size = new System.Drawing.Size( 25, 23 );
            this.browseAttachmentsButton.TabIndex = 10;
            this.browseAttachmentsButton.Text = "...";
            this.browseAttachmentsButton.UseVisualStyleBackColor = true;
            this.browseAttachmentsButton.Click += new System.EventHandler( this.browseAttachmentsButton_Click );
            // 
            // messageTextBox
            // 
            this.messageTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.messageTextBox.Location = new System.Drawing.Point( 12, 174 );
            this.messageTextBox.Multiline = true;
            this.messageTextBox.Name = "messageTextBox";
            this.messageTextBox.Size = new System.Drawing.Size( 400, 212 );
            this.messageTextBox.TabIndex = 12;
            this.messageTextBox.TextChanged += new System.EventHandler( this.EnableSendButtonCheck_EventHandler );
            // 
            // sendButton
            // 
            this.sendButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.sendButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.sendButton.Enabled = false;
            this.sendButton.Location = new System.Drawing.Point( 256, 392 );
            this.sendButton.Name = "sendButton";
            this.sendButton.Size = new System.Drawing.Size( 75, 23 );
            this.sendButton.TabIndex = 13;
            this.sendButton.Text = "Send";
            this.sendButton.UseVisualStyleBackColor = true;
            this.sendButton.Click += new System.EventHandler( this.sendButton_Click );
            // 
            // cancelButton
            // 
            this.cancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.Location = new System.Drawing.Point( 337, 392 );
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size( 75, 23 );
            this.cancelButton.TabIndex = 14;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler( this.cancelButton_Click );
            // 
            // messageLabel
            // 
            this.messageLabel.AutoSize = true;
            this.messageLabel.Location = new System.Drawing.Point( 12, 158 );
            this.messageLabel.Name = "messageLabel";
            this.messageLabel.Size = new System.Drawing.Size( 53, 13 );
            this.messageLabel.TabIndex = 11;
            this.messageLabel.Text = "Message:";
            // 
            // openFileDialog
            // 
            this.openFileDialog.Filter = "All Files (*.*)|*.*";
            this.openFileDialog.Multiselect = true;
            this.openFileDialog.Title = "Browse for File Attachment";
            // 
            // zipAttachmentsCheckBox
            // 
            this.zipAttachmentsCheckBox.AutoSize = true;
            this.zipAttachmentsCheckBox.Checked = true;
            this.zipAttachmentsCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.zipAttachmentsCheckBox.Location = new System.Drawing.Point( 119, 138 );
            this.zipAttachmentsCheckBox.Name = "zipAttachmentsCheckBox";
            this.zipAttachmentsCheckBox.Size = new System.Drawing.Size( 175, 17 );
            this.zipAttachmentsCheckBox.TabIndex = 15;
            this.zipAttachmentsCheckBox.Text = "Zip attachments before sending";
            this.zipAttachmentsCheckBox.UseVisualStyleBackColor = true;
            // 
            // FeedbackForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cancelButton;
            this.ClientSize = new System.Drawing.Size( 424, 427 );
            this.Controls.Add( this.zipAttachmentsCheckBox );
            this.Controls.Add( this.messageLabel );
            this.Controls.Add( this.cancelButton );
            this.Controls.Add( this.sendButton );
            this.Controls.Add( this.messageTextBox );
            this.Controls.Add( this.browseAttachmentsButton );
            this.Controls.Add( this.attachmentsTextBox );
            this.Controls.Add( this.attachmentsLabel );
            this.Controls.Add( this.categoryComboBox );
            this.Controls.Add( this.categoryLabel );
            this.Controls.Add( this.subjectTextBox );
            this.Controls.Add( this.label4 );
            this.Controls.Add( this.yourEmailAddressTextBox );
            this.Controls.Add( this.yourEmailAddressLabel );
            this.Controls.Add( this.toTextBox );
            this.Controls.Add( this.toLabel );
            this.Name = "FeedbackForm";
            this.Text = "Submit Feedback";
            this.Load += new System.EventHandler( this.FeedbackDialog_Load );
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler( this.FeedbackDialog_FormClosing );
            this.ResumeLayout( false );
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label toLabel;
        private System.Windows.Forms.TextBox toTextBox;
        private System.Windows.Forms.Label yourEmailAddressLabel;
        private System.Windows.Forms.TextBox yourEmailAddressTextBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox subjectTextBox;
        private System.Windows.Forms.Label categoryLabel;
        private System.Windows.Forms.ComboBox categoryComboBox;
        private System.Windows.Forms.Label attachmentsLabel;
        private System.Windows.Forms.TextBox attachmentsTextBox;
        private System.Windows.Forms.Button browseAttachmentsButton;
        private System.Windows.Forms.TextBox messageTextBox;
        private System.Windows.Forms.Button sendButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Label messageLabel;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.CheckBox zipAttachmentsCheckBox;
    }
}