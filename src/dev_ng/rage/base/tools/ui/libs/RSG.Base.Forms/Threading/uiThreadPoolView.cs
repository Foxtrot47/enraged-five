//
// File: uiThreadPoolView.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of uiThreadPoolView.cs class
//

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading;
using System.Windows.Forms;

namespace RSG.Base.Threading
{

    /// <summary>
    /// ThreadPool View control
    /// </summary>
    public partial class uiThreadPoolView : UserControl
    {
        #region Constants
        /// <summary>
        /// Default update interval
        /// </summary>
        private const int INTERVAL = 2000;
        #endregion // Constants

        #region Delegates
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private delegate void UpdateViewDelegate();
        #endregion // Delegates

        #region Properties and Associated Member Data
        /// <summary>
        /// Associated Managed ThreadPool Object
        /// </summary>
        public RSG.Base.Threading.cThreadPool ThreadPool
        {
            get { return m_ThreadPool; }
            private set { m_ThreadPool = value; }
        }
        private RSG.Base.Threading.cThreadPool m_ThreadPool;

        /// <summary>
        /// Thread monitoring interval (milliseconds)
        /// </summary>
        public bool Interval
        {
            get { return m_nInterval; }
            set { m_nInterval = value; }
        }
        private bool m_nInterval;

        /// <summary>
        /// Thread monitoring flag
        /// </summary>
        public bool Monitoring
        {
            get { return m_bMonitoring; }
            set { m_bMonitoring = value; }
        }
        private bool m_bMonitoring;
        #endregion // Properties and Associated Member Data

        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public uiThreadPoolView()
        {
            InitializeComponent();
        }
        #endregion // Constructor

        #region Controller Methods
        /// <summary>
        /// Set associated ThreadPool, and start monitoring it.
        /// </summary>
        public void SetThreadPool(RSG.Base.Threading.cThreadPool pool)
        {
            ThreadPool = pool;
            Monitoring = true;
            ThreadPool.QueueWorkItem(new RSG.Base.Threading.WorkItemCallback(Run),
                                     "RsBaseForms_uiThreadPoolView");
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Thread Run Method
        /// </summary>
        private Object Run(Object stateInfo)
        {
            Thread.CurrentThread.Name = (String)(stateInfo);

            while (Monitoring)
            {
                UpdateView();

                Thread.Sleep(INTERVAL);
            }
            return (true);
        }

        /// <summary>
        /// Update View (in thread-safe manner)
        /// </summary>
        private void UpdateView()
        {
            if (lstThreads.InvokeRequired)
            {
                lstThreads.BeginInvoke(new UpdateViewDelegate(UpdateView));
            }
            else
            {
                lstThreads.Items.Clear();
                lstThreads.BeginUpdate();
                foreach (Thread thread in ThreadPool.Threads.Keys)
                {
                    lstThreads.Items.Add(new uiThreadListViewItem(thread));
                }
                lstThreads.EndUpdate();
            }
        }
        #endregion // Private Methods
    }

    #region uiThreadListViewItem Class
    /// <summary>
    /// 
    /// </summary>
    class uiThreadListViewItem : ListViewItem
    {
        #region Properties
        /// <summary>
        /// Associated thread
        /// </summary>
        public Thread Thread
        {
            get { return m_pThread; }
        }
        private Thread m_pThread;
        #endregion // Properties

        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public uiThreadListViewItem(Thread thread)
            : base()
        {
            Initialise(thread);
            UpdateView();
        }

        /// <summary>
        /// Constructor
        /// </summary>
        public uiThreadListViewItem(Thread thread, String text)
            : base(text)
        {
            Initialise(thread);
            UpdateView();
        }
        #endregion // Constructor

        #region Private Methods
        /// <summary>
        /// Initialisation
        /// </summary>
        private void Initialise(Thread thread)
        {
            m_pThread = thread;
            this.SubItems.Add(new ListViewItem.ListViewSubItem());
            this.SubItems.Add(new ListViewItem.ListViewSubItem());
            this.SubItems.Add(new ListViewItem.ListViewSubItem());
            this.SubItems.Add(new ListViewItem.ListViewSubItem());
            this.SubItems.Add(new ListViewItem.ListViewSubItem());
        }
        #endregion

        #region UpdateView
        /// <summary>
        /// 
        /// </summary>
        public void UpdateView()
        {
            this.SubItems[0].Text = Thread.ManagedThreadId.ToString();
            this.SubItems[1].Text = Thread.Name;
            this.SubItems[2].Text = Thread.IsBackground.ToString();
            this.SubItems[3].Text = Thread.IsAlive.ToString();
            this.SubItems[4].Text = Thread.ThreadState.ToString();
        }
        #endregion // UpdateView
    };
    #endregion

} // End of RSG.Base.Threading namespace

// End of file
