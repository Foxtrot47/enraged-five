﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Reflection;
using System.Threading;
using System.Windows.Forms;

namespace RSG.Base.Forms
{
    public partial class ProgressBarSplashScreen : Form
    {
        #region Constants
        private double m_dblOpacityIncrement = 0.05;
        private double m_dblOpacityDecrement = 0.08;
        private const int TIMER_INTERVAL = 1;
        #endregion // Constants

        #region Properties and Associated Member Data

        /// <summary>
        /// 
        /// </summary>
        public static ProgressBarSplashScreen SplashForm
        {
            get { return sm_Splashscreen; }
        }
        private static ProgressBarSplashScreen sm_Splashscreen = null;

        /// <summary>
        /// 
        /// </summary>
        public static Image SplashImage
        {
            get { return sm_SplashImage; }
            set { sm_SplashImage = value; }
        }
        private static Image sm_SplashImage;

        /// <summary>
        /// Current status message display.
        /// </summary>
        public static String CurrentMessage
        {
            get { return sm_sCurrentMessage; }
            set
            {
                sm_sCurrentMessage = value;
                if (sm_Splashscreen != null)
                {
                    UpdateCurrentMessageDelegate del = new UpdateCurrentMessageDelegate(sm_Splashscreen.UpdateCurrentMessage);
                    sm_Splashscreen.Invoke(del);
                }
            }
        }
        private static String sm_sCurrentMessage;

        private delegate void UpdateCurrentMessageDelegate();
        public void UpdateCurrentMessage()
        {
            sm_Splashscreen.lblStatusMessage.Text = sm_sCurrentMessage;
            sm_Splashscreen.Refresh();
        }

        /// <summary>
        /// Current progress percentage
        /// </summary>
        public static float CurrentProgressPercentage
        {
            get { return sm_sCurrentProgress; }
            set
            {
                sm_sCurrentProgress = value;
                if (sm_Splashscreen != null)
                {
                    UpdateCurrentProgressDelegate del = new UpdateCurrentProgressDelegate(sm_Splashscreen.UpdateCurrentProgress);
                    sm_Splashscreen.Invoke(del);
                }
            }
        }
        private static float sm_sCurrentProgress;

        private delegate void UpdateCurrentProgressDelegate();
        public void UpdateCurrentProgress()
        {
            sm_Splashscreen.progressBar.Value = (int)sm_sCurrentProgress;
            sm_Splashscreen.Refresh();
            sm_Splashscreen.Invalidate();
            sm_Splashscreen.Update();
        }

        #endregion // Properties and Associated Member Data

        #region Static Member Data

        private static Thread sm_SplashThread = null;

        #endregion // Static Member Data

        #region Constructor(s)
        /// <summary>
        /// Default constructor
        /// </summary>
        public ProgressBarSplashScreen()
        {
            InitializeComponent();
            this.Opacity = 0.0;
            this.timer.Interval = TIMER_INTERVAL;
            this.timer.Start();
        }
        #endregion // Constructor(s)

        #region Static Controller Methods

        /// <summary>
        /// 
        /// </summary>
        public static void ShowSplash()
        {
            if (sm_Splashscreen != null)
                return;

            sm_SplashThread = new Thread(new ThreadStart(ProgressBarSplashScreen.ShowForm));
            sm_SplashThread.IsBackground = true;
            sm_SplashThread.SetApartmentState(ApartmentState.STA);
            sm_SplashThread.Name = "Splash thread";
            sm_SplashThread.Start();
        }

        /// <summary>
        /// 
        /// </summary>
        public static void CloseSplash()
        {
            if (sm_Splashscreen != null && sm_Splashscreen.IsDisposed == false)
            {
                // Make it start going away.
                sm_Splashscreen.m_dblOpacityIncrement =- sm_Splashscreen.m_dblOpacityDecrement;
            }
            sm_SplashThread = null;
            sm_Splashscreen = null;
        }

        public static DialogResult ShowMessageBox(String message, String caption, MessageBoxButtons buttons, MessageBoxIcon icon)
        {
            if (sm_Splashscreen != null)
            {
                ShowMessageBoxDelegate del = new ShowMessageBoxDelegate(sm_Splashscreen.ShowMessageBoxFunction);
                Object result = sm_Splashscreen.Invoke(del, message, caption, buttons, icon);

                if (result is DialogResult)
                    return ((DialogResult)result);
            }

            return DialogResult.Abort;
        }

        private delegate DialogResult ShowMessageBoxDelegate(String message, String caption, MessageBoxButtons buttons, MessageBoxIcon icon);
        public DialogResult ShowMessageBoxFunction(String message, String caption, MessageBoxButtons buttons, MessageBoxIcon icon)
        {
            return MessageBox.Show(sm_Splashscreen, message, caption, buttons, icon);
        }

        /// <summary>
        /// Hides the window by calling the invoke method
        /// </summary>
        public static void HideWindow()
        {
            if (sm_Splashscreen != null)
            {
                HideWindowDelegate del = new HideWindowDelegate(sm_Splashscreen.HideSplashWindow);
                sm_Splashscreen.Invoke(del);
            }
        }

        /// <summary>
        /// Delegate functions for the hide window function
        /// </summary>
        private delegate void HideWindowDelegate();
        public void HideSplashWindow()
        {
            sm_Splashscreen.Hide();
        }

        /// <summary>
        /// Shows the window by calling the invoke method
        /// </summary>
        public static void ShowWindow()
        {
            if (sm_Splashscreen != null)
            {
                ShowWindowDelegate del = new ShowWindowDelegate(sm_Splashscreen.ShowSplashWindow);
                sm_Splashscreen.Invoke(del);
            }
        }

        /// <summary>
        /// Delegate functions for the show window function
        /// </summary>
        private delegate void ShowWindowDelegate();
        public void ShowSplashWindow()
        {
            sm_Splashscreen.Show();
        }

        /// <summary>
        /// Creates the splash screen and runs it
        /// </summary>
        private static void ShowForm()
        {
            sm_Splashscreen = new ProgressBarSplashScreen();
            sm_Splashscreen.pictureBox1.Image = SplashImage;
            Application.Run(sm_Splashscreen);
        }

        #endregion // Static Controller Methods

        #region Private Methods

        #region Event Handlers

        #region Form Event Handlers

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Splashscreen_Load(Object sender, EventArgs e)
        {
            Assembly entryAssembly = Assembly.GetEntryAssembly();

            // Product Name
            Object[] attributesProduct = entryAssembly.GetCustomAttributes(typeof(AssemblyProductAttribute), false);
            if (1 == attributesProduct.Length)
                lblAppName.Text = ((AssemblyProductAttribute)attributesProduct[0]).Product;
            else
                lblAppName.Text = "";

            // Company Name
            Object[] attributesCompany = entryAssembly.GetCustomAttributes(typeof(AssemblyCompanyAttribute), false);
            if (1 == attributesCompany.Length)
                lblCompany.Text = ((AssemblyCompanyAttribute)attributesCompany[0]).Company;
            else
                lblCompany.Text = "";

            // Set version string.
            lblVersion.Text = entryAssembly.GetName().Version.ToString();
        }

        #endregion // Form Event Handlers

        #region Timer Event Handlers

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timer_Tick(Object sender, EventArgs e)
        {

            if (m_dblOpacityIncrement > 0)
            {
                if (this.Opacity < 1)
                    this.Opacity += m_dblOpacityIncrement;
            }
            else
            {
                if (this.Opacity > 0)
                    this.Opacity += m_dblOpacityIncrement;
                else
                {
                    this.Close();
                }
            }
        }

        #endregion // Timer Event Handlers

        #endregion // Event Handlers

        #endregion // Private Methods
    }
}
