using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace RSG.Base.Forms
{
    public partial class RichTextBoxSearchReplaceDialog : Form
    {
        public RichTextBoxSearchReplaceDialog()
        {
            InitializeComponent();
        }

        #region Variables
        private bool m_locationSet = false;
        private rageRichTextBoxSearchReplaceOptions m_searchReplaceOptions = new rageRichTextBoxSearchReplaceOptions();
        private RichTextBox m_currentRichTextBox = null;

        private bool m_canFindAll = true;
        private bool m_canReplace = true;
        private bool m_canReplaceAll = true;        
        #endregion

        #region Events
        /// <summary>
        /// Invoked to retrieve the RichTextBox that the operation should be carried out on.
        /// </summary>
        public event rageSearchReplaceRichTextBoxEventHandler FindCurrentRichTextBox;

        /// <summary>
        /// Dispatched at the start of the search/replace, indicating what the operation is.
        /// </summary>
        public event rageSearchBeginEventHandler SearchBegin
        {
            add
            {
                this.richTextBoxSearchComponent.SearchBegin += value;
            }
            remove
            {
                this.richTextBoxSearchComponent.SearchBegin -= value;
            }
        }

        /// <summary>
        /// Dispatched at the end of the search, providing the final results of the operation.
        /// </summary>
        public event rageSearchCompleteEventHandler SearchComplete
        {
            add
            {
                this.richTextBoxSearchComponent.SearchComplete += value;
            }
            remove
            {
                this.richTextBoxSearchComponent.SearchComplete -= value;
            }
        }

        /// <summary>
        /// Dispatched when a match is found during the search/replace operation.
        /// </summary>
        public event rageFoundStringEventHandler FoundString
        {
            add
            {
                this.richTextBoxSearchComponent.FoundString += value;
            }
            remove
            {
                this.richTextBoxSearchComponent.FoundString -= value;
            }
        }
        #endregion

        #region Event Dispatchers
        protected RichTextBox OnFindCurrentRichTextBox()
        {
            if ( this.FindCurrentRichTextBox != null )
            {
                rageSearchReplaceRichTextBoxEventArgs e = new rageSearchReplaceRichTextBoxEventArgs();

                Control ctrl = this.FindForm();
                if ( (ctrl != null) && ctrl.InvokeRequired )
                {
                    ctrl.Invoke( this.FindCurrentRichTextBox, new object[] { this, e } );
                }
                else
                {
                    this.FindCurrentRichTextBox( this, e );
                }

                return e.RichTextBox;
            }

            return null;
        }
        #endregion

        #region Properties
        /// <summary>
        /// Enables/disables the Find All button.  
        /// </summary>
        public bool CanFindAll
        {
            get
            {
                return m_canFindAll;
            }
            set
            {
                m_canFindAll = value;
            }
        }

        /// <summary>
        /// Enables/disables the Replace button.  
        /// </summary>
        public bool CanReplace
        {
            get
            {
                return m_canReplace;
            }
            set
            {
                m_canReplace = value;
            }
        }

        /// <summary>
        /// Enables/disables the Replace All button.  
        /// </summary>
        public bool CanReplaceAll
        {
            get
            {
                return m_canReplaceAll;
            }
            set
            {
                m_canReplaceAll = value;
            }
        }

        /// <summary>
        /// Typically, the first time this dialog is shown you will want to center it in the parent Form.  After that
        /// the user can move it where they wish and it should appear in the last location.  This property
        /// can be be set after you've centered the Form so that you do not do so again.
        /// </summary>
        public bool LocationSet
        {
            get
            {
                return m_locationSet;
            }
            set
            {
                m_locationSet = value;
            }
        }

        public string FindWhat
        {
            get
            {
                return this.findWhatComboBox.Text;
            }
            set
            {
                if ( !String.IsNullOrEmpty( value ) )
                {
                    int indexOf = this.findWhatComboBox.Items.IndexOf( value );
                    if ( indexOf != -1 )
                    {
                        this.findWhatComboBox.SelectedIndex = indexOf;
                    }
                    else
                    {
                        this.findWhatComboBox.SelectedIndex = -1;
                        this.findWhatComboBox.Text = value;
                    }
                }
                else
                {
                    this.findWhatComboBox.SelectedIndex = -1;
                    this.findWhatComboBox.Text = string.Empty;
                }
            }
        }

        public List<string> FindHistory
        {
            get
            {
                List<string> findHistory = new List<string>();
                foreach ( object item in this.findWhatComboBox.Items )
                {
                    findHistory.Add( item.ToString() );
                }

                return findHistory;
            }
            set
            {
                this.findWhatComboBox.Items.Clear();
                if ( value != null )
                {
                    foreach ( string item in value )
                    {
                        this.findWhatComboBox.Items.Add( item );
                    }
                }
            }
        }

        public string ReplaceWith
        {
            get
            {
                return this.replaceWithComboBox.Text;
            }
            set
            {
                if ( !String.IsNullOrEmpty( value ) )
                {
                    int indexOf = this.replaceWithComboBox.Items.IndexOf( value );
                    if ( indexOf != -1 )
                    {
                        this.replaceWithComboBox.SelectedIndex = indexOf;
                    }
                    else
                    {
                        this.replaceWithComboBox.SelectedIndex = -1;
                        this.replaceWithComboBox.Text = value;
                    }
                }
                else
                {
                    this.replaceWithComboBox.SelectedIndex = -1;
                    this.replaceWithComboBox.Text = string.Empty;
                }
            }
        }

        public List<string> ReplaceHistory
        {
            get
            {
                List<string> replaceHistory = new List<string>();
                foreach ( object item in this.replaceWithComboBox.Items )
                {
                    replaceHistory.Add( item.ToString() );
                }

                return replaceHistory;
            }
            set
            {
                this.replaceWithComboBox.Items.Clear();
                if ( value != null )
                {
                    foreach ( string item in value )
                    {
                        this.replaceWithComboBox.Items.Add( item );
                    }
                }
            }
        }

        public bool MatchCase
        {
            get
            {
                return this.matchCaseCheckBox.Checked;
            }
            set
            {
                this.matchCaseCheckBox.Checked = value;
            }
        }

        public bool MatchWholeWord
        {
            get
            {
                return this.matchWholeWordCheckBox.Checked;
            }
            set
            {
                this.matchWholeWordCheckBox.Checked = value;
            }
        }

        public bool SearchUp
        {
            get
            {
                return this.searchUpCheckBox.Checked;
            }
            set
            {
                this.searchUpCheckBox.Checked = value;
            }
        }
        #endregion

        #region Event Handlers
        private void RichTextBoxSearchReplaceDialog_Activated( object sender, EventArgs e )
        {
            SelectNextControl( this.findNextButton, false, true, true, true );
        }

        private void RichTextBoxSearchReplaceDialog_FormClosing( object sender, FormClosingEventArgs e )
        {
            if ( e.CloseReason == CloseReason.UserClosing )
            {
                e.Cancel = true;
                this.Hide();

                RichTextBox rtb = OnFindCurrentRichTextBox();
                if ( rtb != null )
                {
                    rtb.Focus();
                }
            }
        }

        private void richTextBoxSearchComponent_SearchComplete( object sender, rageSearchCompleteEventArgs e )
        {
            switch ( e.Search )
            {
                case rageSearchReplaceType.Find:
                case rageSearchReplaceType.Replace:
                    {
                        switch ( e.Result )
                        {
                            case rageSearchReplaceResult.Error:
                                {
                                    MessageBox.Show( this, e.ResultMessage, e.ResultTitle, MessageBoxButtons.OK, MessageBoxIcon.Error );
                                }
                                break;
                            case rageSearchReplaceResult.NotFound:
                            case rageSearchReplaceResult.PastStartOfSearch:
                            case rageSearchReplaceResult.PastEndOfFile:
                                {
                                    MessageBox.Show( this, e.ResultMessage, e.ResultTitle, MessageBoxButtons.OK, MessageBoxIcon.Information );
                                }
                                break;
                            default:
                                break;
                        }

                        if ( e.Search == rageSearchReplaceType.Replace )
                        {
                            StoreReplaceText();
                        }
                    }
                    break;
                case rageSearchReplaceType.ReplaceAll:
                    {
                        switch ( e.Result )
                        {
                            case rageSearchReplaceResult.Found:
                                {
                                    MessageBox.Show( this, String.Format( "Total replaced: {0}", e.NumFound ), e.ResultTitle, 
                                        MessageBoxButtons.OK, MessageBoxIcon.Information );
                                }
                                break;
                            case rageSearchReplaceResult.Error:
                                {
                                    MessageBox.Show( this, e.ResultMessage, e.ResultTitle, MessageBoxButtons.OK, MessageBoxIcon.Error );
                                }
                                break;
                            default:
                                {
                                    MessageBox.Show( this, e.ResultMessage, e.ResultTitle, MessageBoxButtons.OK, MessageBoxIcon.Information );
                                }
                                break;
                        }

                        if ( e.Search == rageSearchReplaceType.ReplaceAll )
                        {
                            StoreReplaceText();
                        }
                    }
                    break;
                case rageSearchReplaceType.FindAll:
                    {
                        // not supported on this dialog
                    }
                    break;
            }

            StoreFindText();

            EnableButtons();

            SelectNextControl( this.findNextButton, false, true, true, true );

            if ( this.Visible )
            {
                this.Focus();
            }
        }

        private void findWhatComboBox_TextUpdate( object sender, EventArgs e )
        {
            EnableButtons();
        }

        private void findNextButton_Click( object sender, EventArgs e )
        {
            RichTextBox rtb;
            if ( !PrepareSearch( out rtb ) )
            {
                return;
            }

            DisableButtons();

            this.richTextBoxSearchComponent.FindNext( rtb, m_searchReplaceOptions );
        }

        private void replaceButton_Click( object sender, EventArgs e )
        {
            RichTextBox rtb;
            if ( !PrepareSearch( out rtb ) )
            {
                return;
            }

            DisableButtons();

            this.richTextBoxSearchComponent.ReplaceNext( rtb, m_searchReplaceOptions );
        }

        private void findAllButton_Click( object sender, EventArgs e )
        {
            RichTextBox rtb;
            if ( !PrepareSearch( out rtb ) )
            {
                return;
            }

            DisableButtons();

            this.richTextBoxSearchComponent.FindAll( rtb, m_searchReplaceOptions );
        }

        private void replaceAllButton_Click( object sender, EventArgs e )
        {
            RichTextBox rtb;
            if ( !PrepareSearch( out rtb ) )
            {
                return;
            }

            DisableButtons();

            this.richTextBoxSearchComponent.ReplaceAll( rtb, m_searchReplaceOptions );
        }

        private void closeButton_Click( object sender, EventArgs e )
        {
            this.Close();
        }
        #endregion

        #region Public Functions
        /// <summary>
        /// Initializes the dialog with the last set of user settings.
        /// </summary>
        /// <param name="settings"></param>
        public void SetUIFromSettings( rageRichTextBoxSearchReplaceSettings settings )
        {
            this.MatchCase = settings.FindReplaceOptions.MatchCase;
            this.MatchWholeWord = settings.FindReplaceOptions.WholeWord;
            this.SearchUp = settings.FindReplaceOptions.Reverse;
            this.FindHistory = settings.FindHistory;
            this.ReplaceHistory = settings.ReplaceHistory;
        }

        /// <summary>
        /// Retrieves the current dialog settings into the user settings structure.
        /// </summary>
        /// <param name="settings"></param>
        public void SetSettingsFromUI( rageRichTextBoxSearchReplaceSettings settings )
        {
            settings.FindReplaceOptions.MatchCase = this.MatchCase;
            settings.FindReplaceOptions.WholeWord = this.MatchWholeWord;
            settings.FindReplaceOptions.Reverse = this.SearchUp;
            settings.FindHistory = this.FindHistory;
            settings.ReplaceHistory = this.ReplaceHistory;
        }

        /// <summary>
        /// Initializes the Find What property of this dialog from the given RichTextBox.
        /// </summary>
        /// <param name="rtb"></param>
        /// <returns></returns>
        public bool InitSearchText( RichTextBox rtb )
        {
            if ( rtb == null )
            {
                // we need a RichTextBox to set the search text
                return false;
            }

            string searchText = this.richTextBoxSearchComponent.DetermineSearchText( rtb, rtb.SelectionStart );
            if ( String.IsNullOrEmpty( searchText ) )
            {
                return false;
            }

            int indexOf = this.findWhatComboBox.Items.IndexOf( searchText );
            if ( indexOf != -1 )
            {
                this.findWhatComboBox.SelectedIndex = indexOf;
            }
            else
            {
                this.findWhatComboBox.SelectedIndex = -1;
                this.findWhatComboBox.Text = searchText;
            }

            this.findWhatComboBox.SelectAll();

            m_searchReplaceOptions.FindText = searchText;

            m_currentRichTextBox = rtb;

            // make sure our buttons get enabled appropriately
            EnableButtons();

            return true;
        }

        /// <summary>
        /// Executes the last search again on the given RichTextBox, searching down.
        /// </summary>
        /// <param name="rtb"></param>
        public void SearchAgainDown( RichTextBox rtb )
        {
            if ( String.IsNullOrEmpty( m_searchReplaceOptions.FindText ) )
            {
                return;
            }

            bool oldSearchUp = m_searchReplaceOptions.Reverse;
            this.searchUpCheckBox.Checked = false;
            this.findNextButton_Click( rtb, new System.EventArgs() );
            this.searchUpCheckBox.Checked = oldSearchUp;

            // make sure this form doesn't steal focus
            rtb.Focus();
        }

        /// <summary>
        /// Executes the last search again on the given RichTextBox, searching up.
        /// </summary>
        /// <param name="rtb"></param>
        public void SearchAgainUp( RichTextBox rtb )
        {
            if ( String.IsNullOrEmpty( m_searchReplaceOptions.FindText ) )
            {
                return;
            }

            bool oldSearchUp = m_searchReplaceOptions.Reverse;
            this.searchUpCheckBox.Checked = true;
            this.findNextButton_Click( rtb, new System.EventArgs() );
            this.searchUpCheckBox.Checked = oldSearchUp;

            // make sure this form doesn't steal focus
            rtb.Focus();
        }

        /// <summary>
        /// Re-initializes the Find What field of this dialog, and executes the 
        /// last search again on the given RichTextBox, searching down.
        /// </summary>
        /// <param name="rtb"></param>
        public void SearchHighlightedDown( RichTextBox rtb )
        {
            if ( InitSearchText( rtb ) )
            {
                SearchAgainDown( rtb );
            }
        }

        /// <summary>
        /// Re-initializes the Find What field of this dialog, and executes the 
        /// last search again on the given RichTextBox, searching up.
        /// </summary>
        /// <param name="rtb"></param>
        public void SearchHighlightedUp( RichTextBox rtb )
        {
            if ( InitSearchText( rtb ) )
            {
                SearchAgainUp( rtb );
            }
        }
        #endregion

        #region Private Functions
        private void StoreFindText()
        {
            if ( this.findWhatComboBox.Items.Count == 20 )
            {
                this.findWhatComboBox.Items.RemoveAt( 19 );
            }

            string text = this.findWhatComboBox.Text;
            int indexOf = this.findWhatComboBox.Items.IndexOf( this.findWhatComboBox.Text );
            if ( indexOf != -1 )
            {
                this.findWhatComboBox.Items.RemoveAt( indexOf );
            }

            this.findWhatComboBox.Items.Insert( 0, text );
            this.findWhatComboBox.SelectedIndex = 0;
        }

        private void StoreReplaceText()
        {
            if ( this.replaceWithComboBox.Items.Count == 20 )
            {
                this.replaceWithComboBox.Items.RemoveAt( 19 );
            }

            string text = this.replaceWithComboBox.Text;
            int indexOf = this.replaceWithComboBox.Items.IndexOf( this.replaceWithComboBox.Text );
            if ( indexOf != -1 )
            {
                this.replaceWithComboBox.Items.RemoveAt( indexOf );
            }

            this.replaceWithComboBox.Items.Insert( 0, text );
            this.replaceWithComboBox.SelectedIndex = 0;
        }

        private void SetUIFromFindReplaceOptions()
        {
            this.FindWhat = m_searchReplaceOptions.FindText;
            this.MatchCase = m_searchReplaceOptions.MatchCase;
            this.MatchWholeWord = m_searchReplaceOptions.WholeWord;
            this.ReplaceWith = m_searchReplaceOptions.ReplaceText;
            this.SearchUp = m_searchReplaceOptions.Reverse;
        }

        private void SetFindReplaceOptionsFromUI()
        {
            m_searchReplaceOptions.NoHighlight = false;
            m_searchReplaceOptions.FindText = this.FindWhat;
            m_searchReplaceOptions.MatchCase = this.MatchCase;
            m_searchReplaceOptions.WholeWord = this.MatchWholeWord;
            m_searchReplaceOptions.ReplaceText = this.ReplaceWith;
            m_searchReplaceOptions.Reverse = this.SearchUp;
        }

        private bool PrepareSearch( out RichTextBox rtb )
        {
            SetFindReplaceOptionsFromUI();

            DisableButtons();

            rtb = OnFindCurrentRichTextBox();
            if ( rtb == null )
            {
                rtb = m_currentRichTextBox;
            }

            return rtb != null;
        }

        private void DisableButtons()
        {
            this.findNextButton.Enabled = false;
            this.replaceButton.Enabled = false;
            this.replaceAllButton.Enabled = false;
        }

        private void EnableButtons()
        {
            this.findNextButton.Enabled = !String.IsNullOrEmpty( this.FindWhat.Trim() );
            this.replaceButton.Enabled = this.findNextButton.Enabled && this.CanReplace;
            this.findAllButton.Enabled = this.findNextButton.Enabled && this.CanFindAll;
            this.replaceAllButton.Enabled = this.findNextButton.Enabled && this.CanReplaceAll;
        }
        #endregion
    }
}