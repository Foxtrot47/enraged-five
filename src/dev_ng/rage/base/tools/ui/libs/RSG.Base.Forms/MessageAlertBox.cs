﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace RSG.Base.Forms
{
    /// <summary>
    /// Alert box replacement for the system's MessageBox.
    /// </summary>
    public partial class MessageAlertBox : Form
    {
        #region Private constants

        /// <summary>
        /// Disables the close button if the user wants to display Yes/No or Abort/Ignore/Retry buttons. This is 
        /// because clicking the Close button generates a "Cancel" DialogResult.
        /// </summary>
        private const int CP_NOCLOSE_BUTTON = 0x200;

        #endregion

        #region Private member fields

        private List<Button> activeButtons = new List<Button>();

        #endregion

        #region Public properties

        /// <summary>
        /// The create params property has been overridden to allow us to disable the close button when the user
        /// chooses to display a Yes/No or Abort/Ignore/Retry dialog.
        /// </summary>
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams myCp = base.CreateParams;
                if (Buttons == MessageBoxButtons.AbortRetryIgnore || Buttons == MessageBoxButtons.YesNo)
                {
                    myCp.ClassStyle = myCp.ClassStyle | CP_NOCLOSE_BUTTON;
                }

                return myCp;
            }
        }

        /// <summary>
        /// The icon that will be displayed to the user.
        /// </summary>
        public MessageBoxIcon MessageBoxIcon { get; private set; }

        /// <summary>
        /// The buttons.
        /// </summary>
        public MessageBoxButtons Buttons { get; private set; }

        /// <summary>
        /// The default button.
        /// </summary>
        public MessageBoxDefaultButton DefaultButton { get; private set; }

        /// <summary>
        /// Message box options.
        /// </summary>
        public MessageBoxOptions Options { get; private set; }

        /// <summary>
        /// The text displayed in the title bar of the window.
        /// </summary>
        public string Caption { get { return Text; } }

        /// <summary>
        /// The text displayed in the body of the window.
        /// </summary>
        public string Message { get; private set; }

        #endregion

        #region Static methods to show the dialog. This is in keeping with the pattern used with the MessageBox system class

        public static DialogResult Show(string message)
        {
            return Show(null, message, "", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly);
        }

        public static DialogResult Show(string message, string caption)
        {
            return Show(null, message, caption, MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly);
        }

        public static DialogResult Show(string message, string caption, MessageBoxButtons buttons)
        {
            return Show(null, message, caption, buttons, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly);
        }

        public static DialogResult Show(IWin32Window owner, string message, string caption, MessageBoxButtons buttons)
        {
            return Show(owner, message, caption, buttons, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly);
        }

        public static DialogResult Show(IWin32Window owner, string message, string caption, MessageBoxButtons buttons, MessageBoxIcon icon)
        {
            return Show(owner, message, caption, buttons, icon, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly);
        }

        public static DialogResult Show(string message, string caption, MessageBoxButtons buttons, MessageBoxIcon icon)
        {
            return Show(null, message, caption, buttons, icon, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly);
        }

        public static DialogResult Show(IWin32Window owner, string message, string caption, MessageBoxButtons buttons, MessageBoxIcon icon, MessageBoxDefaultButton defaultButton)
        {
            return Show(owner, message, caption, buttons, icon, defaultButton, MessageBoxOptions.DefaultDesktopOnly);
        }

        public static DialogResult Show(string message, string caption, MessageBoxButtons buttons, MessageBoxIcon icon, MessageBoxDefaultButton defaultButton)
        {
            return Show(null, message, caption, buttons, icon, defaultButton, MessageBoxOptions.DefaultDesktopOnly);
        }

        public static DialogResult Show(string message, string caption, MessageBoxButtons buttons, MessageBoxIcon icon, MessageBoxDefaultButton defaultButton, MessageBoxOptions options)
        {
            return Show(null, message, caption, buttons, icon, defaultButton, options);
        }

        public static DialogResult Show(IWin32Window owner, string message, string caption, MessageBoxButtons buttons, MessageBoxIcon icon, MessageBoxDefaultButton defaultButton, MessageBoxOptions options)
        {
            DialogResult result = DialogResult.Cancel;
            using (MessageAlertBox mab = new MessageAlertBox(owner, message, caption, buttons, icon, defaultButton, options))
            {
                result = mab.ShowDialog(owner);
            }

            return result;
        }

        #endregion
      
        #region Constructors

        /// <summary>
        /// Constructor. Needed for Visual Studio design-time.
        /// </summary>
        public MessageAlertBox()
            : this("", "")
        {

        }

        private MessageAlertBox(string message)
            : this(message, "")
        {

        }

        private MessageAlertBox(string message, string caption)
            : this(null, message, caption, MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
        {

        }

        private MessageAlertBox(string message, string caption, MessageBoxButtons buttons)
            : this(null, message, caption, buttons, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
        {

        }

        private MessageAlertBox(string message, string caption, MessageBoxButtons buttons, MessageBoxIcon icon)
            : this(null, message, caption, buttons, icon, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
        {

        }

        private MessageAlertBox(string message, string caption, MessageBoxButtons buttons, MessageBoxIcon icon, MessageBoxDefaultButton defaultButton)
            : this(null, message, caption, buttons, icon, defaultButton, MessageBoxOptions.DefaultDesktopOnly)
        {

        }

        private MessageAlertBox(IWin32Window owner, string message, string caption, MessageBoxButtons buttons, MessageBoxIcon icon, MessageBoxDefaultButton defaultButton)
            : this(owner, message, caption, buttons, icon, defaultButton, MessageBoxOptions.DefaultDesktopOnly)
        {

        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="message"></param>
        /// <param name="caption"></param>
        /// <param name="buttons"></param>
        /// <param name="icon"></param>
        /// <param name="defaultButton"></param>
        /// <param name="options"></param>
        private MessageAlertBox(IWin32Window owner, string message, string caption, MessageBoxButtons buttons, MessageBoxIcon icon, MessageBoxDefaultButton defaultButton, MessageBoxOptions options)
        {
            InitializeComponent();

            Text = caption;
            TheMessage.Text = message;
            Buttons = buttons;
            MessageBoxIcon = icon;
            DefaultButton = defaultButton;
            Options = options;
            Message = message;

            SetupButtons();

            if (owner != null)
            {
                StartPosition = FormStartPosition.CenterParent;
            }
            else
            {
                StartPosition = FormStartPosition.CenterScreen;
            }

            Icon iconImg = GetIcon();
            if (iconImg != null)
            {
                IconPicture.Image = iconImg.ToBitmap();
            }
        }

        #endregion

        #region Private helper methods

        /// <summary>
        /// Set up the button.
        /// </summary>
        /// <param name="b">Button.</param>
        /// <param name="text">Button text.</param>
        /// <param name="result">Dialog result.</param>
        private void Set(Button b, string text, DialogResult result)
        {
            b.Text = text;
            b.DialogResult = result;
            activeButtons.Add(b);
        }

        /// <summary>
        /// Get the icon to be displayed on the alert form.
        /// </summary>
        /// <returns></returns>
        private Icon GetIcon()
        {
            switch (MessageBoxIcon)
            {
                case MessageBoxIcon.Asterisk:
                    return System.Drawing.SystemIcons.Asterisk;
                case MessageBoxIcon.Exclamation:
                    return System.Drawing.SystemIcons.Exclamation;
                case MessageBoxIcon.Question:
                    return System.Drawing.SystemIcons.Question;
                case MessageBoxIcon.Error:
                    return System.Drawing.SystemIcons.Error;
                default:
                    return null;
            }
        }

        /// <summary>
        /// Set up the buttons for the form. Because the buttons are contained within a flow layout,
        /// if they are not visible the other buttons 'float' to the right. This means that we don't
        /// have blank spaces, or complicated if-then statements for each button combination.
        /// </summary>
        private void SetupButtons()
        {
            switch(Buttons)
            {
                case MessageBoxButtons.AbortRetryIgnore:
                    Set(btn1, "&Abort", DialogResult.Abort);
                    Set(btn2, "&Retry", DialogResult.Retry);
                    Set(btn3, "&Ignore", DialogResult.Ignore);
                    DialogResult = DialogResult.Ignore;
                    CancelButton = btn3;
                    break;
                case MessageBoxButtons.OK:
                    Set(btn1, "&OK", DialogResult.OK);
                    CancelButton = btn1;
                    break;
                case MessageBoxButtons.OKCancel:
                    Set(btn1, "&OK", DialogResult.OK);
                    Set(btn2, "&Cancel", DialogResult.Cancel);
                    CancelButton = btn2;
                    break;
                case MessageBoxButtons.RetryCancel:
                    Set(btn1, "&Retry", DialogResult.Retry);
                    Set(btn2, "&Cancel", DialogResult.Cancel);
                    CancelButton = btn2;
                    break;
                case MessageBoxButtons.YesNo:
                    Set(btn1, "&Yes", DialogResult.Yes);
                    Set(btn2, "&No", DialogResult.No);
                    CancelButton = btn2;
                    break;
                case MessageBoxButtons.YesNoCancel:
                    Set(btn1, "&Yes", DialogResult.Yes);
                    Set(btn2, "&No", DialogResult.No);
                    Set(btn3, "&Cancel", DialogResult.Cancel);
                    CancelButton = btn3;
                    break;
            }

            HideIfNotUsed(btn1);
            HideIfNotUsed(btn2);
            HideIfNotUsed(btn3);
        }

        /// <summary>
        /// Hides the button if it is not used. We keep a list of active buttons and use that to check the 
        /// button against.
        /// </summary>
        /// <param name="b">Button</param>
        private void HideIfNotUsed(Button b)
        {
            // This is usually NOT good practice, but in this case we only have three buttons
            // and so checking references will work just fine. 
            Button button = activeButtons.Find((Button btn) => { return btn == b; });
            b.Visible = (button != null);
        }

        #endregion

        /// <summary>
        /// When the form is shown, it is made the top-most and activated. It's not possible
        /// to set the focus for a button when the form is loading. Works just fine when the
        /// form is being shown though.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Event arguments.</param>
        private void MessageAlertBox_Shown(object sender, EventArgs e)
        {
            TopMost = true;
            Activate();

            switch (DefaultButton)
            {
                case MessageBoxDefaultButton.Button1:
                    btn1.Focus();
                    AcceptButton = btn1;
                    break;
                case MessageBoxDefaultButton.Button2:
                    AcceptButton = btn2;
                    btn2.Focus();
                    break;
                case MessageBoxDefaultButton.Button3:
                    AcceptButton = btn3;
                    btn3.Focus();
                    break;
            }
        }

        /// <summary>
        /// Handle the copy to clipboard functionality of the system MessageBox.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Event arguments.</param>
        private void MessageAlertBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Control && e.KeyCode == Keys.C)
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendLine("---------------------------");
                sb.AppendLine(Text);
                sb.AppendLine("---------------------------");
                sb.AppendLine(Message);
                sb.AppendLine("---------------------------");

                switch (Buttons)
                {
                    case MessageBoxButtons.AbortRetryIgnore:
                        sb.AppendLine("Abort\tRetry\tIgnore");
                        break;
                    case MessageBoxButtons.OK:
                        sb.AppendLine("OK");
                        break;
                    case MessageBoxButtons.OKCancel:
                        sb.AppendLine("OK\tCancel");
                        break;
                    case MessageBoxButtons.RetryCancel:
                        sb.AppendLine("Retry\tCancel");
                        break;
                    case MessageBoxButtons.YesNo:
                        sb.AppendLine("Yes\tNo");
                        break;
                    case MessageBoxButtons.YesNoCancel:
                        sb.AppendLine("Yes\tNo\tCancel");
                        break;
                }

                sb.AppendLine("---------------------------");

                Clipboard.SetText(sb.ToString());
            }
        }
    }
}

