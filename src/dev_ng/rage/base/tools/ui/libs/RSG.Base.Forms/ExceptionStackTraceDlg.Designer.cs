namespace RSG.Base.Forms
{
    partial class ExceptionStackTraceDlg
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tblLayoutMain = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.tblLayoutTop = new System.Windows.Forms.TableLayoutPanel();
            this.txtStackTrace = new System.Windows.Forms.TextBox();
            this.tblLayoutAppInfo = new System.Windows.Forms.TableLayoutPanel();
            this.lblText1 = new System.Windows.Forms.Label();
            this.lblText2 = new System.Windows.Forms.Label();
            this.lblText3 = new System.Windows.Forms.Label();
            this.lblApplicationName = new System.Windows.Forms.Label();
            this.lblApplicationVersion = new System.Windows.Forms.Label();
            this.lblApplicationAuthor = new System.Windows.Forms.Label();
            this.dlgFileSave = new System.Windows.Forms.SaveFileDialog();
            this.btnEmail = new System.Windows.Forms.Button();
            this.lblText4 = new System.Windows.Forms.Label();
            this.tblLayoutMain.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tblLayoutTop.SuspendLayout();
            this.tblLayoutAppInfo.SuspendLayout();
            this.SuspendLayout();
            // 
            // tblLayoutMain
            // 
            this.tblLayoutMain.ColumnCount = 1;
            this.tblLayoutMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tblLayoutMain.Controls.Add(this.tableLayoutPanel2, 0, 1);
            this.tblLayoutMain.Controls.Add(this.tblLayoutTop, 0, 0);
            this.tblLayoutMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tblLayoutMain.GrowStyle = System.Windows.Forms.TableLayoutPanelGrowStyle.FixedSize;
            this.tblLayoutMain.Location = new System.Drawing.Point(0, 0);
            this.tblLayoutMain.Name = "tblLayoutMain";
            this.tblLayoutMain.RowCount = 2;
            this.tblLayoutMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tblLayoutMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tblLayoutMain.Size = new System.Drawing.Size(678, 537);
            this.tblLayoutMain.TabIndex = 0;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 4;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.Controls.Add(this.btnSave, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.btnClose, 3, 0);
            this.tableLayoutPanel2.Controls.Add(this.btnEmail, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.lblText4, 1, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 505);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.Size = new System.Drawing.Size(672, 29);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(513, 3);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 2;
            this.btnSave.Text = "&Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(594, 3);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 1;
            this.btnClose.Text = "&Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // tblLayoutTop
            // 
            this.tblLayoutTop.ColumnCount = 1;
            this.tblLayoutTop.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tblLayoutTop.Controls.Add(this.txtStackTrace, 0, 1);
            this.tblLayoutTop.Controls.Add(this.tblLayoutAppInfo, 0, 0);
            this.tblLayoutTop.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tblLayoutTop.Location = new System.Drawing.Point(3, 3);
            this.tblLayoutTop.Name = "tblLayoutTop";
            this.tblLayoutTop.RowCount = 2;
            this.tblLayoutTop.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 78F));
            this.tblLayoutTop.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tblLayoutTop.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tblLayoutTop.Size = new System.Drawing.Size(672, 496);
            this.tblLayoutTop.TabIndex = 1;
            // 
            // txtStackTrace
            // 
            this.txtStackTrace.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtStackTrace.Location = new System.Drawing.Point(3, 81);
            this.txtStackTrace.Multiline = true;
            this.txtStackTrace.Name = "txtStackTrace";
            this.txtStackTrace.Size = new System.Drawing.Size(666, 412);
            this.txtStackTrace.TabIndex = 2;
            this.txtStackTrace.WordWrap = false;
            // 
            // tblLayoutAppInfo
            // 
            this.tblLayoutAppInfo.ColumnCount = 2;
            this.tblLayoutAppInfo.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 19.54545F));
            this.tblLayoutAppInfo.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 80.45455F));
            this.tblLayoutAppInfo.Controls.Add(this.lblText1, 0, 0);
            this.tblLayoutAppInfo.Controls.Add(this.lblText2, 0, 2);
            this.tblLayoutAppInfo.Controls.Add(this.lblText3, 0, 1);
            this.tblLayoutAppInfo.Controls.Add(this.lblApplicationName, 1, 0);
            this.tblLayoutAppInfo.Controls.Add(this.lblApplicationVersion, 1, 1);
            this.tblLayoutAppInfo.Controls.Add(this.lblApplicationAuthor, 1, 2);
            this.tblLayoutAppInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tblLayoutAppInfo.Location = new System.Drawing.Point(3, 3);
            this.tblLayoutAppInfo.Name = "tblLayoutAppInfo";
            this.tblLayoutAppInfo.RowCount = 3;
            this.tblLayoutAppInfo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tblLayoutAppInfo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tblLayoutAppInfo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tblLayoutAppInfo.Size = new System.Drawing.Size(666, 72);
            this.tblLayoutAppInfo.TabIndex = 3;
            // 
            // lblText1
            // 
            this.lblText1.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblText1.AutoSize = true;
            this.lblText1.Location = new System.Drawing.Point(34, 5);
            this.lblText1.Name = "lblText1";
            this.lblText1.Size = new System.Drawing.Size(93, 13);
            this.lblText1.TabIndex = 0;
            this.lblText1.Text = "Application Name:";
            // 
            // lblText2
            // 
            this.lblText2.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblText2.AutoSize = true;
            this.lblText2.Location = new System.Drawing.Point(86, 53);
            this.lblText2.Name = "lblText2";
            this.lblText2.Size = new System.Drawing.Size(41, 13);
            this.lblText2.TabIndex = 1;
            this.lblText2.Text = "Author:";
            // 
            // lblText3
            // 
            this.lblText3.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblText3.AutoSize = true;
            this.lblText3.Location = new System.Drawing.Point(82, 29);
            this.lblText3.Name = "lblText3";
            this.lblText3.Size = new System.Drawing.Size(45, 13);
            this.lblText3.TabIndex = 1;
            this.lblText3.Text = "Version:";
            // 
            // lblApplicationName
            // 
            this.lblApplicationName.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblApplicationName.AutoSize = true;
            this.lblApplicationName.Location = new System.Drawing.Point(133, 5);
            this.lblApplicationName.Name = "lblApplicationName";
            this.lblApplicationName.Size = new System.Drawing.Size(56, 13);
            this.lblApplicationName.TabIndex = 2;
            this.lblApplicationName.Text = "Undefined";
            // 
            // lblApplicationVersion
            // 
            this.lblApplicationVersion.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblApplicationVersion.AutoSize = true;
            this.lblApplicationVersion.Location = new System.Drawing.Point(133, 29);
            this.lblApplicationVersion.Name = "lblApplicationVersion";
            this.lblApplicationVersion.Size = new System.Drawing.Size(56, 13);
            this.lblApplicationVersion.TabIndex = 3;
            this.lblApplicationVersion.Text = "Undefined";
            // 
            // lblApplicationAuthor
            // 
            this.lblApplicationAuthor.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblApplicationAuthor.AutoSize = true;
            this.lblApplicationAuthor.Location = new System.Drawing.Point(133, 53);
            this.lblApplicationAuthor.Name = "lblApplicationAuthor";
            this.lblApplicationAuthor.Size = new System.Drawing.Size(56, 13);
            this.lblApplicationAuthor.TabIndex = 4;
            this.lblApplicationAuthor.Text = "Undefined";
            // 
            // dlgFileSave
            // 
            this.dlgFileSave.Filter = "Text File (.txt)|.txt";
            // 
            // btnEmail
            // 
            this.btnEmail.Location = new System.Drawing.Point(3, 3);
            this.btnEmail.Name = "btnEmail";
            this.btnEmail.Size = new System.Drawing.Size(75, 23);
            this.btnEmail.TabIndex = 3;
            this.btnEmail.Text = "&Email";
            this.btnEmail.UseVisualStyleBackColor = true;
            this.btnEmail.Click += new System.EventHandler(this.btnEmail_Click);
            // 
            // lblText4
            // 
            this.lblText4.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblText4.AutoSize = true;
            this.lblText4.Location = new System.Drawing.Point(84, 8);
            this.lblText4.Name = "lblText4";
            this.lblText4.Size = new System.Drawing.Size(283, 13);
            this.lblText4.TabIndex = 4;
            this.lblText4.Text = "Your username and machine name are send with this email";
            // 
            // ExceptionStackTraceDlg
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(678, 537);
            this.Controls.Add(this.tblLayoutMain);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ExceptionStackTraceDlg";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Unhandled Exception";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.dlgExceptionStackTrace_Load);
            this.tblLayoutMain.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tblLayoutTop.ResumeLayout(false);
            this.tblLayoutTop.PerformLayout();
            this.tblLayoutAppInfo.ResumeLayout(false);
            this.tblLayoutAppInfo.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tblLayoutMain;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.SaveFileDialog dlgFileSave;
        private System.Windows.Forms.TableLayoutPanel tblLayoutTop;
        private System.Windows.Forms.TextBox txtStackTrace;
        private System.Windows.Forms.TableLayoutPanel tblLayoutAppInfo;
        private System.Windows.Forms.Label lblText1;
        private System.Windows.Forms.Label lblText2;
        private System.Windows.Forms.Label lblText3;
        private System.Windows.Forms.Label lblApplicationName;
        private System.Windows.Forms.Label lblApplicationVersion;
        private System.Windows.Forms.Label lblApplicationAuthor;
        private System.Windows.Forms.Button btnEmail;
        private System.Windows.Forms.Label lblText4;
    }
}