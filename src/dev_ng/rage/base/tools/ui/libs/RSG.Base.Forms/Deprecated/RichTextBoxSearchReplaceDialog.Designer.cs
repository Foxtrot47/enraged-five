namespace RSG.Base.Forms
{
    partial class RichTextBoxSearchReplaceDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && (components != null) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.richTextBoxSearchComponent = new RSG.Base.Forms.RichTextBoxSearchComponent(this.components);
            this.closeButton = new System.Windows.Forms.Button();
            this.replaceAllButton = new System.Windows.Forms.Button();
            this.replaceWithComboBox = new System.Windows.Forms.ComboBox();
            this.findWhatComboBox = new System.Windows.Forms.ComboBox();
            this.searchUpCheckBox = new System.Windows.Forms.CheckBox();
            this.matchWholeWordCheckBox = new System.Windows.Forms.CheckBox();
            this.matchCaseCheckBox = new System.Windows.Forms.CheckBox();
            this.replaceButton = new System.Windows.Forms.Button();
            this.replaceWithLabel = new System.Windows.Forms.Label();
            this.findNextButton = new System.Windows.Forms.Button();
            this.findWhatLabel = new System.Windows.Forms.Label();
            this.findAllButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // richTextBoxSearchComponent
            // 
            this.richTextBoxSearchComponent.SearchComplete += new RSG.Base.Forms.rageSearchCompleteEventHandler(this.richTextBoxSearchComponent_SearchComplete);
            // 
            // closeButton
            // 
            this.closeButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.closeButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.closeButton.Location = new System.Drawing.Point( 413, 143 );
            this.closeButton.Name = "closeButton";
            this.closeButton.Size = new System.Drawing.Size( 75, 23 );
            this.closeButton.TabIndex = 11;
            this.closeButton.Text = "Close";
            this.closeButton.UseVisualStyleBackColor = true;
            this.closeButton.Click += new System.EventHandler( this.closeButton_Click );
            // 
            // replaceAllButton
            // 
            this.replaceAllButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.replaceAllButton.Location = new System.Drawing.Point( 413, 96 );
            this.replaceAllButton.Name = "replaceAllButton";
            this.replaceAllButton.Size = new System.Drawing.Size( 75, 23 );
            this.replaceAllButton.TabIndex = 7;
            this.replaceAllButton.Text = "Replace A&ll";
            this.replaceAllButton.UseVisualStyleBackColor = true;
            this.replaceAllButton.Click += new System.EventHandler( this.replaceAllButton_Click );
            // 
            // replaceWithComboBox
            // 
            this.replaceWithComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.replaceWithComboBox.FormattingEnabled = true;
            this.replaceWithComboBox.Location = new System.Drawing.Point( 80, 39 );
            this.replaceWithComboBox.Name = "replaceWithComboBox";
            this.replaceWithComboBox.Size = new System.Drawing.Size( 327, 21 );
            this.replaceWithComboBox.TabIndex = 4;
            // 
            // findWhatComboBox
            // 
            this.findWhatComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.findWhatComboBox.FormattingEnabled = true;
            this.findWhatComboBox.Location = new System.Drawing.Point( 80, 12 );
            this.findWhatComboBox.Name = "findWhatComboBox";
            this.findWhatComboBox.Size = new System.Drawing.Size( 327, 21 );
            this.findWhatComboBox.TabIndex = 1;
            this.findWhatComboBox.TextUpdate += new System.EventHandler( this.findWhatComboBox_TextUpdate );
            // 
            // searchUpCheckBox
            // 
            this.searchUpCheckBox.AutoSize = true;
            this.searchUpCheckBox.Location = new System.Drawing.Point( 5, 112 );
            this.searchUpCheckBox.Name = "searchUpCheckBox";
            this.searchUpCheckBox.Size = new System.Drawing.Size( 77, 17 );
            this.searchUpCheckBox.TabIndex = 10;
            this.searchUpCheckBox.Text = "S&earch Up";
            this.searchUpCheckBox.UseVisualStyleBackColor = true;
            // 
            // matchWholeWordCheckBox
            // 
            this.matchWholeWordCheckBox.AutoSize = true;
            this.matchWholeWordCheckBox.Location = new System.Drawing.Point( 5, 89 );
            this.matchWholeWordCheckBox.Name = "matchWholeWordCheckBox";
            this.matchWholeWordCheckBox.Size = new System.Drawing.Size( 113, 17 );
            this.matchWholeWordCheckBox.TabIndex = 9;
            this.matchWholeWordCheckBox.Text = "Match &whole word";
            this.matchWholeWordCheckBox.UseVisualStyleBackColor = true;
            // 
            // matchCaseCheckBox
            // 
            this.matchCaseCheckBox.AutoSize = true;
            this.matchCaseCheckBox.Location = new System.Drawing.Point( 5, 66 );
            this.matchCaseCheckBox.Name = "matchCaseCheckBox";
            this.matchCaseCheckBox.Size = new System.Drawing.Size( 82, 17 );
            this.matchCaseCheckBox.TabIndex = 8;
            this.matchCaseCheckBox.Text = "Match &case";
            this.matchCaseCheckBox.UseVisualStyleBackColor = true;
            // 
            // replaceButton
            // 
            this.replaceButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.replaceButton.Location = new System.Drawing.Point( 413, 37 );
            this.replaceButton.Name = "replaceButton";
            this.replaceButton.Size = new System.Drawing.Size( 75, 23 );
            this.replaceButton.TabIndex = 5;
            this.replaceButton.Text = "&Replace";
            this.replaceButton.UseVisualStyleBackColor = true;
            this.replaceButton.Click += new System.EventHandler( this.replaceButton_Click );
            // 
            // replaceWithLabel
            // 
            this.replaceWithLabel.AutoSize = true;
            this.replaceWithLabel.Location = new System.Drawing.Point( 2, 42 );
            this.replaceWithLabel.Name = "replaceWithLabel";
            this.replaceWithLabel.Size = new System.Drawing.Size( 72, 13 );
            this.replaceWithLabel.TabIndex = 3;
            this.replaceWithLabel.Text = "Re&place with:";
            // 
            // findNextButton
            // 
            this.findNextButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.findNextButton.Location = new System.Drawing.Point( 413, 10 );
            this.findNextButton.Name = "findNextButton";
            this.findNextButton.Size = new System.Drawing.Size( 75, 23 );
            this.findNextButton.TabIndex = 2;
            this.findNextButton.Text = "&Find Next";
            this.findNextButton.UseVisualStyleBackColor = true;
            this.findNextButton.Click += new System.EventHandler( this.findNextButton_Click );
            // 
            // findWhatLabel
            // 
            this.findWhatLabel.AutoSize = true;
            this.findWhatLabel.Location = new System.Drawing.Point( 18, 15 );
            this.findWhatLabel.Name = "findWhatLabel";
            this.findWhatLabel.Size = new System.Drawing.Size( 56, 13 );
            this.findWhatLabel.TabIndex = 0;
            this.findWhatLabel.Text = "Fi&nd what:";
            // 
            // findAllButton
            // 
            this.findAllButton.Location = new System.Drawing.Point( 413, 67 );
            this.findAllButton.Name = "findAllButton";
            this.findAllButton.Size = new System.Drawing.Size( 75, 23 );
            this.findAllButton.TabIndex = 6;
            this.findAllButton.Text = "Find &All";
            this.findAllButton.UseVisualStyleBackColor = true;
            this.findAllButton.Click += new System.EventHandler( this.findAllButton_Click );
            // 
            // RichTextBoxSearchReplaceDialog
            // 
            this.AcceptButton = this.findNextButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.closeButton;
            this.ClientSize = new System.Drawing.Size( 500, 178 );
            this.Controls.Add( this.findAllButton );
            this.Controls.Add( this.closeButton );
            this.Controls.Add( this.replaceAllButton );
            this.Controls.Add( this.replaceWithComboBox );
            this.Controls.Add( this.findWhatComboBox );
            this.Controls.Add( this.searchUpCheckBox );
            this.Controls.Add( this.matchWholeWordCheckBox );
            this.Controls.Add( this.matchCaseCheckBox );
            this.Controls.Add( this.replaceButton );
            this.Controls.Add( this.replaceWithLabel );
            this.Controls.Add( this.findNextButton );
            this.Controls.Add( this.findWhatLabel );
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "RichTextBoxSearchReplaceDialog";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Find/Replace";
            this.Activated += new System.EventHandler( this.RichTextBoxSearchReplaceDialog_Activated );
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler( this.RichTextBoxSearchReplaceDialog_FormClosing );
            this.ResumeLayout( false );
            this.PerformLayout();

        }

        #endregion

        private RichTextBoxSearchComponent richTextBoxSearchComponent;
        private System.Windows.Forms.Button closeButton;
        private System.Windows.Forms.Button replaceAllButton;
        private System.Windows.Forms.ComboBox replaceWithComboBox;
        private System.Windows.Forms.ComboBox findWhatComboBox;
        private System.Windows.Forms.CheckBox searchUpCheckBox;
        private System.Windows.Forms.CheckBox matchWholeWordCheckBox;
        private System.Windows.Forms.CheckBox matchCaseCheckBox;
        private System.Windows.Forms.Button replaceButton;
        private System.Windows.Forms.Label replaceWithLabel;
        private System.Windows.Forms.Button findNextButton;
        private System.Windows.Forms.Label findWhatLabel;
        private System.Windows.Forms.Button findAllButton;
    }
}