//
// File: SettingsForm.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of SettingsForm.cs class
//

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Windows.Forms;

namespace RSG.Base.Forms
{

    /// <summary>
    /// SelectedObject Forms (tabbed for multiple settings pages).
    /// </summary>
    /// If you add objects that are derived from ApplicationSettingsBase then 
    /// the dialog supports save, reload and reset to defaults.
    /// 
    public partial class SettingsForm : Form
    {
        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public SettingsForm()
        {
            InitializeComponent();
        }
        #endregion // Constructor

        #region Controller Methods
        /// <summary>
        /// Add new settings page
        /// </summary>
        /// <param name="text"></param>
        /// <param name="settings">SelectedObject object to display in a new tabpage</param>
        /// 
        ///
        public void AddSettingsPage(String text, Object settings)
        {
            this.tabControl.TabPages.Add(new SettingsTabPage(text, settings));
        }
        #endregion // Controller Methods

        #region Private Methods
        #region Event Handlers
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnOK_Click(Object sender, EventArgs e)
        {
            foreach (SettingsTabPage page in this.tabControl.TabPages)
            {
                if (page.SelectedObject is ApplicationSettingsBase)
                    (page.SelectedObject as ApplicationSettingsBase).Save();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCancel_Click(object sender, EventArgs e)
        {
            foreach (SettingsTabPage page in this.tabControl.TabPages)
            {
                if (page.SelectedObject is ApplicationSettingsBase)
                    (page.SelectedObject as ApplicationSettingsBase).Reload();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDefault_Click(object sender, EventArgs e)
        {
            foreach (SettingsTabPage page in this.tabControl.TabPages)
            {
                if (page.SelectedObject is ApplicationSettingsBase)
                    (page.SelectedObject as ApplicationSettingsBase).Reset();
            }
        }
        #endregion // Event Handlers
        #endregion // Private Methods
    }

    /// <summary>
    /// 
    /// </summary>
    class SettingsTabPage : TabPage
    {
        #region Properties and Associated Member Data
        /// <summary>
        /// SelectedObject being displayed
        /// </summary>
        public Object SelectedObject
        {
            get { return m_SelectedObject; }
            private set { m_SelectedObject = value; }
        }
        private Object m_SelectedObject;
        #endregion // Properties and Associated Member Data

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="settings"></param>
        public SettingsTabPage(Object o)
        {
            Initialise(o);
        }
        
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="settings"></param>
        public SettingsTabPage(String text, Object o)
            : base(text)
        {
            Initialise(o);
        }
        #endregion // Constructor

        #region Private Methods
        /// <summary>
        /// Common initialisation method (invoked from constructors)
        /// </summary>
        /// <param name="settings"></param>
        private void Initialise(Object o)
        {
            this.SelectedObject = o;

            PropertyGrid pgSettings = new PropertyGrid();
            this.Controls.Add(pgSettings);
            pgSettings.Dock = DockStyle.Fill;
            pgSettings.SelectedObject = this.SelectedObject;
        }
        #endregion // Private Methods
    }

} // End of RSG.Base.Forms namespace

// End of file
