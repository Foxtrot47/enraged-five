﻿using PresentationControls;
using System.Data;
using System.Collections.Generic;
using System.Windows.Forms;

namespace RSG.Base.Forms.Controls
{
    partial class UniversalLogControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            PresentationControls.CheckBoxProperties checkBoxProperties1 = new PresentationControls.CheckBoxProperties();
            this.filterControl = new PresentationControls.CheckBoxComboBox();
            this.adressHistory = new ComboBox();
            this.m_tabContext = new System.Windows.Forms.TabControl();
            this.targetDataMap = new Dictionary<string, DataTable>();
            this.emailButton = new Button();
            this.m_tabContext.SuspendLayout();
            this.SuspendLayout();
            // 
            // filterControl
            // 
            this.filterControl.CheckBoxProperties = checkBoxProperties1;
            //            this.filterControl.DisplayMemberSingleItem = "";
            this.filterControl.Location = new System.Drawing.Point(0, 0);
            this.filterControl.Name = "filterControl";
            this.filterControl.Size = new System.Drawing.Size(150, 21);
            this.filterControl.TabIndex = 1;
            this.filterControl.Anchor = (AnchorStyles.Top | AnchorStyles.Left);
            this.filterControl.CheckBoxCheckedChanged += new System.EventHandler(ChangedFilterHandler);
            // Filters
            foreach (string l in m_comboLabels)
            {
                this.filterControl.Items.Add(l);
            }
            //foreach (CheckBoxComboBoxItem c in this.filterControl.CheckBoxItems)
            //    c.Checked = true;
            this.filterControl.CheckBoxItems["Errors"].Checked = true;
            this.filterControl.CheckBoxItems["Warnings"].Checked = true;
            // 
            // adressHistory
            // 
            this.adressHistory.Location = new System.Drawing.Point(155, 0);
            this.adressHistory.Name = "adressHistory";
            this.adressHistory.Size = new System.Drawing.Size(250, 50);
            this.adressHistory.TabIndex = 2;
            this.adressHistory.Anchor = (AnchorStyles.Top | AnchorStyles.Left);
            this.adressHistory.SelectedIndexChanged += new System.EventHandler(ChangedHistoryHandler);
            this.adressHistory.KeyDown += new System.Windows.Forms.KeyEventHandler(ChangedHistoryKeyHandler);
            // 
            // m_tabContext
            // 
            this.m_tabContext.Location = new System.Drawing.Point(0, 27);
            this.m_tabContext.Name = "m_tabContext";
            this.m_tabContext.SelectedIndex = 0;
            this.m_tabContext.Size = new System.Drawing.Size(500, 250);
            this.m_tabContext.TabIndex = 3;
            this.m_tabContext.Anchor = (AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left);
            this.m_tabContext.Selected += new System.Windows.Forms.TabControlEventHandler(SelectTabHandler);
            //
            // this.emailButton
            //
            this.emailButton.Location = new System.Drawing.Point(0, 280);
            this.emailButton.Name = "emailButton";
            this.emailButton.Text = "Send email";
            this.emailButton.Size = new System.Drawing.Size(500, 20);
            this.emailButton.TabIndex = 4;
            this.emailButton.Anchor = (AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right);
            this.emailButton.Click += new System.EventHandler(this.SendMailHandler);
            // 
            // XmlListDialog
            // 
            this.Controls.Add(this.m_tabContext);
            this.Controls.Add(this.filterControl);
            this.Controls.Add(this.adressHistory);
            this.Controls.Add(this.emailButton);
            this.Name = "XmlListDialog";
            this.Size = new System.Drawing.Size(500, 300);
            this.m_tabContext.ResumeLayout(false);
            this.ResumeLayout(false);


            this.Resize += new System.EventHandler(this.ResizeHandler);
            UniversalLogControl.MessageSelected += new System.EventHandler<ContextArgs>(MessageSelectedHandler);
        }

        #endregion

        private CheckBoxComboBox filterControl;
        private ComboBox adressHistory;
        private Button emailButton;
        private Dictionary<string, DataTable> targetDataMap;
        private System.Windows.Forms.TabControl m_tabContext;
    }
}
