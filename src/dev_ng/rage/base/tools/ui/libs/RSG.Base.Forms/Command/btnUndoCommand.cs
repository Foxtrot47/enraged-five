//
// File: btnUndoCommand.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: 
//

using System;
using System.Drawing;
using System.Windows.Forms;

using RSG.Base.Command;

namespace RSG.Base.Command
{
    
    /// <summary>
    /// Undo Command Button
    /// </summary>
    public sealed class btnUndoCommand : ToolStripSplitButton
    {
        #region Constants
        private readonly int DEFAULT_SHOWITEMS = 8;
        #endregion // Constants

        #region Properties and Associated Member Data
        /// <summary>
        /// 
        /// </summary>
        public cCommandManager CommandManager
        {
            get { return this.m_CommandManager; }
        }
        private cCommandManager m_CommandManager;

        /// <summary>
        /// Maximum number of items to show in drop down
        /// </summary>
        public int ShowItems
        {
            get { return m_nShowItems; }
            set { m_nShowItems = value; }
        }
        private int m_nShowItems;
        #endregion // Properties and Associated Member Data

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        public btnUndoCommand(cCommandManager manager)
            : base()
        {
            this.Text = "Undo";
            this.Image = RSG.Base.Forms.Resources.UNDO;
            this.ImageTransparentColor = Color.FromArgb(192, 192, 192);
            this.DisplayStyle = ToolStripItemDisplayStyle.ImageAndText;

            Initialise(manager);
        }

        public btnUndoCommand(cCommandManager manager, String text)
            : base()
        {
            this.Image = RSG.Base.Forms.Resources.UNDO;
            Initialise(manager);
        }

        public btnUndoCommand(cCommandManager manager, Image image)
            : base(image)
        {
            this.Text = "Undo";
            Initialise(manager);
        }

        public btnUndoCommand(cCommandManager manager, String text, Image image)
            : base(text, image)
        {
            Initialise(manager);
        }
        #endregion // Constructor

        #region ToolStripSplitButton Overrides
        // None
        #endregion // ToolStripSplitButton Overrides

        #region Private Methods
        /// <summary>
        /// Common initialisation method
        /// </summary>
        private void Initialise(cCommandManager manager)
        {
            this.ShowItems = DEFAULT_SHOWITEMS;
            this.m_CommandManager = manager;
            this.Enabled = (this.CommandManager.DoneStack.Count > 0);

            // Hook Command Manager events
            this.CommandManager.CommandDoneEvent += OnCommandProcessed;
            this.CommandManager.CommandUndoneEvent += OnCommandProcessed;

            // Hook UI Events
            this.ButtonClick += OnButtonClick;
            this.DropDownOpening += OnDropDownOpening;
        }
        #endregion // Private Methods 

        #region Command Manager Event Handlers
        /// <summary>
        /// Command Manager Command Done/Undone Event Handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnCommandProcessed(Object sender, cCommandEventArgs e)
        {
            this.Enabled = this.CommandManager.CanUndo();

            // Update tooltip
            if (this.CommandManager.CanUndo())
            {
                iCommand command = this.CommandManager.DoneStack.Peek();
                this.ToolTipText = String.Format("Undo {0}", command.Name);
            }
            else
            {
                // Nothing to undo, reset to default
                this.ToolTipText = "Undo (nothing to undo)";
            }
        }
        #endregion // Command Manager Event Handlers

        #region UI Event Handlers
        /// <summary>
        /// Button Click Event Handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnButtonClick(Object sender, EventArgs e)
        {
            if (!this.CommandManager.CanUndo())
                throw new NotSupportedException("Internal error: cannot undo as done stack is empty.");

            this.CommandManager.Undo();
        }

        /// <summary>
        /// Drop Down Opening Event Handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnDropDownOpening(Object sender, EventArgs e)
        {
            // Clear out drop down items as we reconstruct the list
            // each time we drop down.
            this.DropDownItems.Clear();

            // Loop through undo items in command manager adding upto
            // ShowItems count to our drop down.
            System.Collections.IEnumerator stackEnumerator = 
                this.CommandManager.DoneStack.GetEnumerator();

            int nItem = 0;
            while (stackEnumerator.MoveNext())
            {
                if (nItem >= this.ShowItems)
                    break;

                iCommand command = (stackEnumerator.Current as iCommand);
                this.DropDownItems.Add(command.Name);
                ++nItem;
            }
        }
        #endregion // UI Event Handlers
    }

} // End of RSG.Base.Command namespace

// End of file
