//
// File: iAction.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of iAction.cs class
//

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

using RSG.Base.Collections;
using RSG.Base.Command;

namespace RSG.Base.Forms.Actions
{
    
    /// <summary>
    /// Abstract base class for higher-level user-interface action classes
    /// </summary>
    public abstract class uiAction
    {
        #region Abstract Methods
        /// <summary>
        /// Add the Action to a Windows.Forms.Control (e.g. Menu, Toolbar)
        /// </summary>
        /// <param name="ctrl">Parent control</param>
        public abstract void AddTo(ToolStrip ctrl);
        public abstract void AddTo(ToolStripMenuItem ctrl);
        public abstract void AddTo(ContextMenuStrip ctrl);

        /// <summary>
        /// Remove the Action from a Windows.Forms.Control (e.g. Menu, Toolbar)
        /// </summary>
        /// <param name="ctrl">Parent control</param>
        public abstract void RemoveFrom(ToolStrip ctrl);
        public abstract void RemoveFrom(ToolStripMenuItem ctrl);
        public abstract void RemoveFrom(ContextMenuStrip ctrl);

        /// <summary>
        /// Update the Action state
        /// </summary>
        public abstract void Update(Object sender, cActionUpdateEventArgs e);
        #endregion // Abstract Methods

        #region Events
        /// <summary>
        /// Event raised (internally) to execute commands
        /// </summary>
        internal event EventHandler<cCommandEventArgs> CommandEvent;

        /// <summary>
        /// Event raised when Enabled status is changed
        /// </summary>
        public event EventHandler<EventArgs> EnabledEvent;
        #endregion // Events

        #region Properties and Associated Member Data
        /// <summary>
        /// Text string to display on Action UI
        /// </summary>
        public String Text
        {
            get { return m_sText; }
            set { m_sText = value; }
        }
        private String m_sText;

        /// <summary>
        /// Image to display on Action UI
        /// </summary>
        public Image Image
        {
            get { return m_Image; }
            set { m_Image = value; }
        }
        private Image m_Image;

        /// <summary>
        /// Transparent colour for Action UI Image
        /// </summary>
        public Color TransparentColour
        {
            get { return m_TransColour; }
            set { m_TransColour = value; }
        }
        private Color m_TransColour;

        /// <summary>
        /// Tooltip to display on Action UI
        /// </summary>
        public String ToolTip
        {
            get { return m_sToolTip; }
            set { m_sToolTip = value; }
        }
        private String m_sToolTip;

        /// <summary>
        /// Shortcut key for Action
        /// </summary>
        public Keys ShortcutKey
        {
            get { return m_Keys; }
            set { m_Keys = value; }
        }
        private Keys m_Keys;

        /// <summary>
        /// Action enabled state (updates any UI elements Enabled property)
        /// </summary>
        public bool Enabled
        {
            get { return m_bEnabled; }
            set
            {
                // Update state
                m_bEnabled = value;
                UpdateUIElements();

                RaiseEnabledEvent();
            }
        }
        private bool m_bEnabled;

        /// <summary>
        /// Action visible state (updates any UI elements Visible property)
        /// </summary>
        public bool Visible
        {
            get { return m_bVisible; }
            set
            {
                // Update state
                m_bVisible = value;
                UpdateUIElements();
            }
        }
        private bool m_bVisible;

        /// <summary>
        /// Read-access to a list of ToolStripMenuItem controls this Action has
        /// been added to.  List of pairs of Parent and action menu items.
        /// </summary>
        public List<Pair<ToolStripMenuItem, ToolStripMenuItem>> MenuItems
        {
            get { return m_MenuItems; }
            private set { m_MenuItems = value; }
        }
        private List<Pair<ToolStripMenuItem, ToolStripMenuItem>> m_MenuItems;

        /// <summary>
        /// 
        /// </summary>
        public List<Pair<ContextMenuStrip, ToolStripMenuItem>> ContextMenus
        {
            get { return m_ContextMenus; }
            private set { m_ContextMenus = value; }
        }
        private List<Pair<ContextMenuStrip, ToolStripMenuItem>> m_ContextMenus;

        /// <summary>
        /// Read-access to a list of ToolStrip controls this Action has been
        /// added to.  List of pairs of Parent and action toolstrip buttons.
        /// </summary>
        public List<Pair<ToolStrip, ToolStripButton>> ToolStripButtons
        {
            get { return m_ToolStripButtons; }
            private set { m_ToolStripButtons = value; }
        }
        private List<Pair<ToolStrip, ToolStripButton>> m_ToolStripButtons;
        #endregion // Properties and Associated Member Data

        #region Constructors
        /// <summary>
        /// Constructors
        /// </summary>
        /// <param name="sText"></param>
        /// <param name="pImage"></param>
        /// <param name="transparentColour"></param>
        /// <param name="sToolTip"></param>
        /// <param name="Shortcut"></param>
        public uiAction(String sText, Image pImage, Color transparentColour)
        {
            Initialise(sText, pImage, transparentColour, String.Empty, Keys.None);
        }

        public uiAction(String sText, Image pImage, Color transparentColour, String sToolTip)
        {
            Initialise(sText, pImage, transparentColour, sToolTip, Keys.None);
        }

        public uiAction(String sText, Image pImage, Color transparentColour, String sToolTip, Keys Shortcut)
        {
            Initialise(sText, pImage, transparentColour, sToolTip, Shortcut);
        }
        #endregion // Constructors

        #region Protected Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="command"></param>
        /// This method should be used by application-level Command and Toggle
        /// Action's to submit commands automatically to the correct Command
        /// Manager.
        /// 
        /// The Command Manager that is responsible for executing the commands
        /// depends on whether they are model or view commands.
        ///
        protected void RaiseCommandEvent(iCommand command)
        {
            if (null != this.CommandEvent)
                this.CommandEvent(this, new cCommandEventArgs(command));
        }

        /// <summary>
        /// Raise EnabledEvent (to any listeners)
        /// </summary>
        protected void RaiseEnabledEvent()
        {
            if (null != this.EnabledEvent)
                this.EnabledEvent(this, new EventArgs());
        }

        /// <summary>
        /// Update Action's UI Elements
        /// </summary>
        internal void UpdateUIElements()
        {
            foreach (Pair<ToolStripMenuItem, ToolStripMenuItem> miItem in MenuItems)
            {
                miItem.Second.Visible = m_bVisible;
                miItem.Second.Enabled = m_bEnabled;
            }

            foreach (Pair<ToolStrip, ToolStripButton> tbButton in ToolStripButtons)
            {
                tbButton.Second.Visible = m_bVisible;
                tbButton.Second.Enabled = m_bEnabled;
            }

            foreach (Pair<ContextMenuStrip, ToolStripMenuItem> miItem in ContextMenus)
            {
                miItem.Second.Visible = m_bVisible;
                miItem.Second.Enabled = m_bEnabled;
            }
        }
        #endregion // Protected Methods

        #region Private Methods
        /// <summary>
        /// Initalisation method
        /// </summary>
        /// <param name="sText"></param>
        /// <param name="pImage"></param>
        /// <param name="transparentColour"></param>
        /// <param name="sToolTip"></param>
        /// <param name="Shortcut"></param>
        private void Initialise(String sText, Image pImage, Color transparentColour, String sToolTip, Keys Shortcut)
        {
            this.MenuItems = new List<Pair<ToolStripMenuItem, ToolStripMenuItem>>();
            this.ContextMenus = new List<Pair<ContextMenuStrip, ToolStripMenuItem>>();
            this.ToolStripButtons = new List<Pair<ToolStrip, ToolStripButton>>();
            this.Text = sText;
            this.Image = pImage;
            this.TransparentColour = transparentColour;
            this.ToolTip = sToolTip;
            this.ShortcutKey = Shortcut;
            this.m_bEnabled = true;
            this.m_bVisible = true;
        }
        #endregion // Private Methods
    }

} // End of RsBaseForms namespace

// End of file
