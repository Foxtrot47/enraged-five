﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace RSG.Base.Forms.Controls
{
    public class HtmlDataGridCell : DataGridViewTextBoxCell
    {

        public HtmlDataGridCell()
            : base()
        {
            // Use the short date format.
            this.Style.Format = "d";
        }

        public override void InitializeEditingControl(int rowIndex, object
            initialFormattedValue, DataGridViewCellStyle dataGridViewCellStyle)
        {
            // Set the value of the editing control to the current cell value.
            base.InitializeEditingControl(rowIndex, initialFormattedValue,
                dataGridViewCellStyle);
            RichTextBox ctl =
                DataGridView.EditingControl as RichTextBox;
            // Use the default row value when Value property is null.
            if (this.Value == null)
            {
                ctl.Text = "";
            }
            else
            {
                ctl.Text = this.Value as string;
            }
        }

        public override Type EditType
        {
            get
            {
                // Return the type of the editing control that HtmlDataGridCell uses.
                return typeof(RichTextBox);
            }
        }

        public override Type ValueType
        {
            get
            {
                // Return the type of the value that HtmlDataGridCell contains.

                return typeof(string);
            }
        }

        public override object DefaultNewRowValue
        {
            get
            {
                // Use the current date and time as the default value.
                return "";
            }
        }
    }
}