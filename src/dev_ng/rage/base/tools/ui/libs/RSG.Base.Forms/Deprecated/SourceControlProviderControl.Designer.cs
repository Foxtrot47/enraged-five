namespace RSG.Base.Forms
{
    partial class SourceControlProviderControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && (components != null) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.providerLabel = new System.Windows.Forms.Label();
            this.serverPortLabel = new System.Windows.Forms.Label();
            this.projectWorkspaceLabel = new System.Windows.Forms.Label();
            this.loginUsernameLabel = new System.Windows.Forms.Label();
            this.providerComboBox = new System.Windows.Forms.ComboBox();
            this.serverPortTextBox = new System.Windows.Forms.TextBox();
            this.projectWorkspaceTextBox = new System.Windows.Forms.TextBox();
            this.loginUsernameTextBox = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // providerLabel
            // 
            this.providerLabel.AutoSize = true;
            this.providerLabel.Location = new System.Drawing.Point( 57, 6 );
            this.providerLabel.Name = "providerLabel";
            this.providerLabel.Size = new System.Drawing.Size( 49, 13 );
            this.providerLabel.TabIndex = 0;
            this.providerLabel.Text = "Provider:";
            // 
            // serverPortLabel
            // 
            this.serverPortLabel.AutoSize = true;
            this.serverPortLabel.Location = new System.Drawing.Point( 41, 33 );
            this.serverPortLabel.Name = "serverPortLabel";
            this.serverPortLabel.Size = new System.Drawing.Size( 65, 13 );
            this.serverPortLabel.TabIndex = 2;
            this.serverPortLabel.Text = "Server/Port:";
            // 
            // projectWorkspaceLabel
            // 
            this.projectWorkspaceLabel.AutoSize = true;
            this.projectWorkspaceLabel.Location = new System.Drawing.Point( 3, 59 );
            this.projectWorkspaceLabel.Name = "projectWorkspaceLabel";
            this.projectWorkspaceLabel.Size = new System.Drawing.Size( 103, 13 );
            this.projectWorkspaceLabel.TabIndex = 4;
            this.projectWorkspaceLabel.Text = "Project/Workspace:";
            // 
            // loginUsernameLabel
            // 
            this.loginUsernameLabel.AutoSize = true;
            this.loginUsernameLabel.Location = new System.Drawing.Point( 17, 85 );
            this.loginUsernameLabel.Name = "loginUsernameLabel";
            this.loginUsernameLabel.Size = new System.Drawing.Size( 89, 13 );
            this.loginUsernameLabel.TabIndex = 6;
            this.loginUsernameLabel.Text = "Login/Username:";
            // 
            // providerComboBox
            // 
            this.providerComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.providerComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.providerComboBox.FormattingEnabled = true;
            this.providerComboBox.Location = new System.Drawing.Point( 112, 3 );
            this.providerComboBox.Name = "providerComboBox";
            this.providerComboBox.Size = new System.Drawing.Size( 374, 21 );
            this.providerComboBox.TabIndex = 1;
            this.providerComboBox.SelectedIndexChanged += new System.EventHandler( this.providerComboBox_SelectedIndexChanged );
            // 
            // serverPortTextBox
            // 
            this.serverPortTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.serverPortTextBox.Location = new System.Drawing.Point( 112, 30 );
            this.serverPortTextBox.Name = "serverPortTextBox";
            this.serverPortTextBox.Size = new System.Drawing.Size( 374, 20 );
            this.serverPortTextBox.TabIndex = 3;
            this.serverPortTextBox.TextChanged += new System.EventHandler( this.TextBox_TextChanged );
            // 
            // projectWorkspaceTextBox
            // 
            this.projectWorkspaceTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.projectWorkspaceTextBox.Location = new System.Drawing.Point( 112, 56 );
            this.projectWorkspaceTextBox.Name = "projectWorkspaceTextBox";
            this.projectWorkspaceTextBox.Size = new System.Drawing.Size( 374, 20 );
            this.projectWorkspaceTextBox.TabIndex = 5;
            this.projectWorkspaceTextBox.TextChanged += new System.EventHandler( this.TextBox_TextChanged );
            // 
            // loginUsernameTextBox
            // 
            this.loginUsernameTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.loginUsernameTextBox.Location = new System.Drawing.Point( 112, 82 );
            this.loginUsernameTextBox.Name = "loginUsernameTextBox";
            this.loginUsernameTextBox.Size = new System.Drawing.Size( 374, 20 );
            this.loginUsernameTextBox.TabIndex = 7;
            this.loginUsernameTextBox.TextChanged += new System.EventHandler( this.TextBox_TextChanged );
            // 
            // SourceControlProviderControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add( this.loginUsernameTextBox );
            this.Controls.Add( this.projectWorkspaceTextBox );
            this.Controls.Add( this.serverPortTextBox );
            this.Controls.Add( this.providerComboBox );
            this.Controls.Add( this.loginUsernameLabel );
            this.Controls.Add( this.projectWorkspaceLabel );
            this.Controls.Add( this.serverPortLabel );
            this.Controls.Add( this.providerLabel );
            this.Name = "SourceControlProviderControl";
            this.Size = new System.Drawing.Size( 489, 107 );
            this.ResumeLayout( false );
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label providerLabel;
        private System.Windows.Forms.Label serverPortLabel;
        private System.Windows.Forms.Label projectWorkspaceLabel;
        private System.Windows.Forms.Label loginUsernameLabel;
        private System.Windows.Forms.ComboBox providerComboBox;
        private System.Windows.Forms.TextBox serverPortTextBox;
        private System.Windows.Forms.TextBox projectWorkspaceTextBox;
        private System.Windows.Forms.TextBox loginUsernameTextBox;
    }
}
