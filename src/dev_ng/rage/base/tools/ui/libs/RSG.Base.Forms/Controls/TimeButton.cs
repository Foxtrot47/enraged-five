//
// File: TimeButton.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of TimeButton class
//

using System;
using System.Threading;
using System.Windows.Forms;

namespace RSG.Base.Forms.Controls
{

    /// <summary>
    /// 
    /// </summary>
    public class TimeButton : Button
    {
        #region Constants
        /// <summary>
        /// Default timeout (seconds).
        /// </summary>
        private static int DEFAULT_TIMEOUT = 60;

        /// <summary>
        /// Default tick time (milliseconds).
        /// </summary>
        private static int DEFAULT_TICK = 1000;
        #endregion // Constants

        #region Delegates
        /// <summary>
        /// Delegate used to update button text during timeout period.
        /// </summary>
        private delegate void ButtonTextDelegate(String text);

        /// <summary>
        /// Delegate used to click button after timeout period.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private delegate void ButtonClickDelegate(Control sender, EventArgs e);
        #endregion // Delegates

        #region Properties and Associated Member Data
        /// <summary>
        /// Timeout (in seconds) before button has simulated click.
        /// </summary>
        public int Timeout
        {
            get { return m_nTimeout; }
            set { m_nTimeout = value; }
        }
        private int m_nTimeout;

        /// <summary>
        /// Suffix the button text with current time (in seconds).
        /// </summary>
        public bool ShowTimeout
        {
            get { return m_bShowTimeout; }
            set { m_bShowTimeout = value; }
        }
        private bool m_bShowTimeout;

        /// <summary>
        /// 
        /// </summary>
        public bool Frozen
        {
            get { return m_bFrozen; }
            private set { m_bFrozen = value; }
        }
        private bool m_bFrozen;
        #endregion // Properties and Associated Member Data

        #region Member Data
        /// <summary>
        /// Current timeout clock (in milliseconds).
        /// </summary>
        private int m_nCurrentTime;

        /// <summary>
        /// Original button text (for suffix).
        /// </summary>
        private String m_sOriginalText;

        /// <summary>
        /// 
        /// </summary>
        private Thread m_thrTimeoutThread;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public TimeButton()
            : base()
        {
            this.m_bFrozen = true;
            this.Timeout = DEFAULT_TIMEOUT;
            this.ShowTimeout = true;

            this.Click += OnClickHandler;
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Start the timeout clock.
        /// </summary>
        /// This is typically invoked after dialog construction and loading by
        /// the user application.
        public void Start()
        {
            this.m_sOriginalText = this.Text;
            this.m_nCurrentTime = this.Timeout * 1000; // Seconds to milliseconds.

            // Setup thread and start.
            this.m_thrTimeoutThread = new Thread(new ThreadStart(ThreadTimerProc));
            this.m_thrTimeoutThread.Name = "TimeButton timeout thread";
            this.m_thrTimeoutThread.Start();
            this.m_bFrozen = false;
        }

        /// <summary>
        /// Freeze the timeout clock (prevents click happening).
        /// </summary>
        public void Freeze()
        {
            this.m_bFrozen = true;
        }

        /// <summary>
        /// Unfreeze the timeout clock (prevents click happening).
        /// </summary>
        public void UnFreeze()
        {
            this.m_bFrozen = false;
        }
        #endregion // Controller Methods

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        private void ThreadTimerProc()
        {
            // Sit in sleepy loop while we havne't timed out or if we are frozen.
            while ((this.m_nCurrentTime > 0) && !this.Disposing)
            {
                Thread.Sleep(DEFAULT_TICK);
                this.m_nCurrentTime -= DEFAULT_TICK;
                
                // Update button text suffix with time (if set)
                if (this.ShowTimeout && !this.m_bFrozen)
                {
                    String newText = String.Format("{0} ({1})", this.m_sOriginalText, this.m_nCurrentTime / 1000);
                    if (this.IsHandleCreated)
                        this.BeginInvoke(new ButtonTextDelegate(ButtonTextUpdate), newText);
                }
            }

            if (!this.Disposing && !this.IsDisposed && this.IsHandleCreated)
            {
                // Reset text
                this.BeginInvoke(new ButtonTextDelegate(ButtonTextUpdate), this.m_sOriginalText);

                // Invoke the button click.
                this.BeginInvoke(new ButtonClickDelegate(ButtonClickUpdate), new Object[] { this, EventArgs.Empty }); 
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="newtext"></param>
        private void ButtonTextUpdate(String newtext)
        {
            this.Text = newtext;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonClickUpdate(Control sender, EventArgs e)
        {
            InvokeOnClick(sender, e);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnClickHandler(Object sender, EventArgs e)
        {
            this.m_nCurrentTime = 0;
        }
        #endregion // Private Methods
    }

} // RSG.Base.Forms.Controls namespace

// TimeButton.cs
