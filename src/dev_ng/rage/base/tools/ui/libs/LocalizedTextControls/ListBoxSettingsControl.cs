using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

using RSG.Base;
using RSG.Base.Forms;

namespace LocalizedTextControls
{
    /// <summary>
    /// This Control is used on the LocalizedTextSettingsDialog.
    /// </summary>
    public partial class ListBoxSettingsControl : UserControl
    {
        public ListBoxSettingsControl()
        {
            InitializeComponent();
        }

        #region Variables
        private Type m_editType = null;
        private string m_typeName = string.Empty;

        private bool m_dispatchEvents = true;
        private ListBoxSettingsEditItemDialog m_editItemDialog = new ListBoxSettingsEditItemDialog();
        #endregion

        #region Properties
        public HelpProvider HelpProvider
        {
            set
            {
                if ( value != null )
                {
                    m_editItemDialog.HelpNamespace = value.HelpNamespace;
                }
            }
        }

        /// <summary>
        /// The type of objects to edit.  This must be set in order to add and edit items.
        /// </summary>
        [Category( "Data" ), DefaultValue( null ), Description( "The type of objects to edit.  This must be set in order to add and edit items." )]
        public Type EditType
        {
            get
            {
                return m_editType;
            }
            set
            {
                m_editType = value;
            }
        }

        /// <summary>
        /// The name of the type to display to the user.
        /// </summary>
        [Category( "Appearance" ), DefaultValue( "" ), Description( "The name of the type to display to the user." )]
        public string TypeName
        {
            get
            {
                return m_typeName;
            }
            set
            {
                if ( value != null )
                {
                    m_typeName = value;
                }
                else
                {
                    m_typeName = string.Empty;
                }
            }
        }

        /// <summary>
        /// The list of objects to edit and were created/edited.
        /// </summary>
        [Category( "Data" ), Description( "The list of objects to edit and were created/edited." ), DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public List<ICloneable> Items
        {
            get
            {
                List<ICloneable> items = new List<ICloneable>();
                foreach ( ICloneable item in this.listBox.Items )
                {
                    items.Add( item );
                }

                return items;
            }
            set
            {
                m_dispatchEvents = false;

                this.listBox.BeginUpdate();

                this.listBox.Items.Clear();
                this.defaultComboBox.Items.Clear();
                if ( value != null )
                {
                    foreach ( ICloneable item in value )
                    {
                        this.listBox.Items.Add( item );
                        this.defaultComboBox.Items.Add( item );
                    }
                }

                this.listBox.EndUpdate();

                m_dispatchEvents = true;
            }
        }

        /// <summary>
        /// Whether or not the current list of items has a default.  Will enable/disable the Default Combo Box.
        /// </summary>
        [Category( "Behavior" ), DefaultValue( true ), Description( "Whether or not the current list of items has a default.  Will enable/disable the Default Combo Box." )]
        public bool DefaultEnabled
        {
            get
            {
                return this.defaultComboBox.Enabled;
            }
            set
            {
                this.defaultComboBox.Enabled = value;
            }
        }

        /// <summary>
        /// Gets or sets the default item.
        /// </summary>
        [Category( "Data" ), DefaultValue( null ), Description( "Gets or sets the default item." )]
        public ICloneable DefaultItem
        {
            get
            {
                return this.defaultComboBox.SelectedItem as ICloneable;
            }
            set
            {
                m_dispatchEvents = false;

                if ( value != null )
                {
                    int indexOf = this.defaultComboBox.Items.IndexOf( value );
                    this.defaultComboBox.SelectedIndex = indexOf;
                }
                else
                {
                    this.defaultComboBox.SelectedIndex = -1;
                }

                m_dispatchEvents = true;
            }
        }

        /// <summary>
        /// Gets or set the default item index.
        /// </summary>
        [Category( "Data" ), DefaultValue( -1 ), Description( "Gets or set the default item index." )]
        public int DefaultItemIndex
        {
            get
            {
                return this.defaultComboBox.SelectedIndex;
            }
            set
            {
                m_dispatchEvents = false;

                if ( (value >= 0) && (value < this.defaultComboBox.Items.Count) )
                {
                    this.defaultComboBox.SelectedIndex = value;
                }

                m_dispatchEvents = true;
            }
        }
        #endregion

        #region Events
        /// <summary>
        /// Occurs when an edit is made.
        /// </summary>
        [Category( "Property Changed" ), Description( "Occurs when an edit is made." )]
        public event EventHandler Changed;
        #endregion
        
        #region Event Dispatchers
        protected void OnChanged()
        {
            if ( m_dispatchEvents && (this.Changed != null) )
            {
                this.Changed( this, EventArgs.Empty );
            }
        }
        #endregion

        #region Event Handlers
        private void listBox_SelectedIndexChanged( object sender, EventArgs e )
        {            
            this.removeButton.Enabled = this.listBox.SelectedItems.Count > 0;

            bool enable = this.listBox.SelectedItems.Count == 1;
            this.editButton.Enabled = enable;
            this.moveUpButton.Enabled = enable;
            this.moveDownButton.Enabled = enable;
        }

        private void addButton_Click( object sender, EventArgs e )
        {
            if ( this.EditType == null )
            {
                return;
            }

            m_editItemDialog.Text = String.Format( "Add {0}", this.TypeName );
            m_editItemDialog.Item = this.EditType.GetConstructor( System.Type.EmptyTypes ).Invoke( null ) as ICloneable;

            while ( true )
            {
                DialogResult result = m_editItemDialog.ShowDialog( this );
                if ( result == DialogResult.OK )
                {
                    object item = m_editItemDialog.Item;
                    if ( IsItemUnqiue( item ) )
                    {
                        this.listBox.Items.Add( item );
                        this.defaultComboBox.Items.Add( item );
                        break;
                    }
                    else
                    {
                        rageMessageBox.ShowExclamation( this, String.Format( "The {0} entered is not unique.  Please try again", this.TypeName ),
                            "Item Collision" );
                    }
                }
                else
                {
                    break;
                }
            }
        }

        private void editButton_Click( object sender, EventArgs e )
        {
            if ( this.listBox.SelectedItems.Count != 1 )
            {
                return;
            }

            m_editItemDialog.Text = String.Format( "Edit {0}", this.TypeName );
            m_editItemDialog.Item = this.listBox.SelectedItems[0] as ICloneable;

            while ( true )
            {
                DialogResult result = m_editItemDialog.ShowDialog( this );
                if ( result == DialogResult.OK )
                {
                    if ( m_editItemDialog.Item == this.listBox.SelectedItems[0] )
                    {
                        // nothing was really changed
                        break;
                    }

                    object item = m_editItemDialog.Item;
                    if ( IsItemUnqiue( item ) )
                    {
                        this.listBox.BeginUpdate();

                        int index = this.listBox.SelectedIndices[0];
                        this.listBox.Items.RemoveAt( index );
                        this.listBox.Items.Insert( index, item );

                        this.listBox.EndUpdate();

                        this.listBox.SetSelected( index, true );

                        int selectedIndex = this.defaultComboBox.SelectedIndex;
                        this.defaultComboBox.Items.RemoveAt( index );
                        this.defaultComboBox.Items.Insert( index, item );
                        this.defaultComboBox.SelectedIndex = selectedIndex;

                        this.listBox.Focus();

                        break;
                    }
                    else
                    {
                        rageMessageBox.ShowExclamation( this, String.Format( "The {0} entered is not unique.  Please try again", this.TypeName ),
                            "Item Collision" );
                    }
                }
                else
                {
                    break;
                }
            }
        }

        private void removeButton_Click( object sender, EventArgs e )
        {
            if ( this.listBox.SelectedItems.Count > 0 )
            {
                object defaultItem = this.defaultComboBox.SelectedItem;

                this.listBox.BeginUpdate();

                List<object> selectedItems = new List<object>();
                foreach ( object obj in this.listBox.SelectedItems )
                {
                    selectedItems.Add( obj );
                }

                foreach ( object obj in selectedItems )
                {
                    this.listBox.Items.Remove( obj );
                    this.defaultComboBox.Items.Remove( obj );
                }

                this.listBox.EndUpdate();

                if ( defaultItem != null )
                {
                    int indexOf = this.defaultComboBox.Items.IndexOf( defaultItem );
                    if ( indexOf != -1 )
                    {
                        this.defaultComboBox.SelectedIndex = indexOf;
                    }
                }

                this.listBox.Focus();

                OnChanged();
            }
        }

        private void moveUpButton_Click( object sender, EventArgs e )
        {
            if ( this.listBox.SelectedItems.Count != 1 )
            {
                return;
            }
            
            int index = this.listBox.SelectedIndices[0];

            int newIndex = index - 1;
            if ( newIndex < 0 )
            {
                newIndex = 0;
            }

            if ( newIndex != index )
            {
                object obj = this.listBox.SelectedItems[0];

                this.listBox.Items.RemoveAt( index );
                this.listBox.Items.Insert( newIndex, obj );

                this.listBox.SetSelected( newIndex, true );

                object selectedItem = this.defaultComboBox.SelectedItem;
                this.defaultComboBox.Items.RemoveAt( index );
                this.defaultComboBox.Items.Insert( newIndex, obj );

                if ( selectedItem != null )
                {
                    int indexOf = this.defaultComboBox.Items.IndexOf( selectedItem );
                    if ( indexOf != -1 )
                    {
                        this.defaultComboBox.SelectedIndex = indexOf;
                    }
                }

                this.listBox.Focus();

                OnChanged();
            }
        }

        private void moveDownButton_Click( object sender, EventArgs e )
        {
            if ( this.listBox.SelectedItems.Count != 1 )
            {
                return;
            }

            int index = this.listBox.SelectedIndices[0];

            int newIndex = index + 1;
            if ( newIndex > this.listBox.Items.Count )
            {
                newIndex = this.listBox.Items.Count;
            }

            if ( newIndex != index )
            {
                object obj = this.listBox.SelectedItems[0];

                this.listBox.Items.RemoveAt( index );
                this.listBox.Items.Insert( newIndex, obj );

                this.listBox.SetSelected( newIndex, true );

                object selectedItem = this.defaultComboBox.SelectedItem;
                this.defaultComboBox.Items.RemoveAt( index );
                this.defaultComboBox.Items.Insert( newIndex, obj );

                if ( selectedItem != null )
                {
                    int indexOf = this.defaultComboBox.Items.IndexOf( selectedItem );
                    if ( indexOf != -1 )
                    {
                        this.defaultComboBox.SelectedIndex = indexOf;
                    }
                }

                this.listBox.Focus();

                OnChanged();
            }
        }

        private void defaultComboBox_SelectedIndexChanged( object sender, EventArgs e )
        {
            OnChanged();
        }
        #endregion

        #region Private Functions
        private bool IsItemUnqiue( object item )
        {
            foreach ( object listItem in this.listBox.Items )
            {
                if ( listItem.Equals( item ) )
                {
                    return false;
                }
            }

            return true;
        }
        #endregion
    }
}
