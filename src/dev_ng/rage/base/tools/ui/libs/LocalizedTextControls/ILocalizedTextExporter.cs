using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

using RSG.Base.Command;

namespace LocalizedTextControls
{
    /// <summary>
    /// An interface for exporting a collection of Localized Text Data to a file.
    /// </summary>
    public interface ILocalizedTextExporter
    {
        /// <summary>
        /// Exports the collection to a file in a game-specific format.
        /// </summary>
        /// <param name="filename">The full path of the file to save.</param>
        /// <param name="collection">The list of LocalizedTextData items.</param>
        /// <param name="options">The LocalizedTextOptions that were used when creating the collection.  Can be used to turn LocalizedTextData.FontName into an index, for example.</param>
        /// <param name="status">Out parameter for success or failure</param>
        /// <returns>The number of errors (0 on success, -1 on undetermined)</returns>
        int Export( string filename, LocalizedTextDataCollection collection, LocalizedTextOptions options, out rageStatus status );

        /// <summary>
        /// Invokers of Export will add a handler to this event.  Implementers should invoke ProgressChanged in their Export function appropriately.
        /// </summary>
        event ProgressChangedEventHandler ProgressChanged;
    }
}
