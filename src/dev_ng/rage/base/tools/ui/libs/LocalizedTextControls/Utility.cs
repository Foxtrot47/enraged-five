using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;

namespace LocalizedTextControls
{
    public class ExportMessageEventArgs : EventArgs
    {
        public ExportMessageEventArgs( string message )
        {
            m_message = message;
        }

        #region Variables
        private string m_message = string.Empty;
        #endregion

        #region Properties
        public string Message
        {
            get
            {
                return m_message;
            }
        }
        #endregion
    }

    public delegate void ExportMessageEventHandler( object sender, ExportMessageEventArgs e );

    public class SourceControlEventArgs : EventArgs
    {
        public SourceControlEventArgs( string filename )
        {
            m_filename = filename;
        }

        #region Variables
        private string m_filename = string.Empty;
        private DialogResult m_result = DialogResult.OK;
        #endregion

        #region Properties
        public string Filename
        {
            get
            {
                return m_filename;
            }
        }

        public DialogResult Result
        {
            get
            {
                return m_result;
            }
            set
            {
                m_result = value;
            }
        }
        #endregion
    }
 
    public delegate void SourceControlEventHandler( object sender, SourceControlEventArgs e );

    public class RtfColorData
    {
        public RtfColorData()
        {
            this.Color = Color.White;
            this.Index = 0;
        }

        public RtfColorData( Color c, int colorIndex )
        {
            this.Color = c;
            this.Index = colorIndex;
        }

        /// <summary>
        /// The Color
        /// </summary>
        public Color Color;

        /// <summary>
        /// The index of this ColorData instance in RtfData.ColorData.
        /// </summary>
        public int Index;

        public override bool Equals( object obj )
        {
            if ( base.Equals( obj ) )
            {
                return true;
            }

            RtfColorData other = obj as RtfColorData;
            if ( obj == null )
            {
                return false;
            }

            return this.Color.Equals( other ) 
                && (this.Index == other.Index);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

    public class RtfSpanData
    {
        public RtfSpanData()
        {
            this.Text = new StringBuilder();
            this.Bold = false;
            this.Italic = false;
            this.Underline = false;
            this.ColorData = new RtfColorData();
        }

        public RtfSpanData( string text )
        {
            this.Text = new StringBuilder();
            this.Text.Append( text );
            this.Bold = false;
            this.Italic = false;
            this.Underline = false;
            this.ColorData = new RtfColorData();
        }

        public RtfSpanData( string text, bool bold, bool italic, bool underline, RtfColorData colorData )
        {
            this.Text = new StringBuilder();
            this.Text.Append( text );
            this.Bold = bold;
            this.Italic = italic;
            this.Underline = underline;
            this.ColorData = colorData;
        }

        /// <summary>
        /// The text of the span
        /// </summary>
        public StringBuilder Text;

        /// <summary>
        /// Bold formatting for the span
        /// </summary>
        public bool Bold;

        /// <summary>
        /// Italic formatting for the span
        /// </summary>
        public bool Italic;

        /// <summary>
        /// Bold formatting for the span
        /// </Underline>
        public bool Underline;

        /// <summary>
        /// Color formatting for the span
        /// </summary>
        public RtfColorData ColorData;

        public override bool Equals( object obj )
        {
            if ( base.Equals( obj ) )
            {
                return true;
            }

            RtfSpanData other = obj as RtfSpanData;
            if ( other == null )
            {
                return false;
            }

            return this.ColorData.Equals( other.ColorData )
                && (this.Bold == other.Bold) 
                && (this.Italic == other.Italic) 
                && (this.Underline == other.Underline) 
                && (this.Text.ToString() == other.Text.ToString());
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

    public class RtfLineData
    {
        public RtfLineData()
        {
            this.SpanData = new List<RtfSpanData>();
        }

        /// <summary>
        /// The list of SpanData.
        /// </summary>
        public List<RtfSpanData> SpanData;
    }

    public class RtfData
    {
        public RtfData()
        {
            this.ColorData = new List<RtfColorData>();
            this.LineData = new List<RtfLineData>();

            this.ColorData.Add( new RtfColorData( Color.White, 0 ) );
        }

        /// <summary>
        /// The list of Colors used by the RTF.  This will start with the default White color at index 0
        /// </summary>
        public List<RtfColorData> ColorData;

        /// <summary>
        /// The list of LineData.
        /// </summary>
        public List<RtfLineData> LineData;
    }

    public class Utility
    {
        #region Public Static Functions
        public static void CenterLocation( Control parent, Form form )
        {
            Rectangle rect = parent.Bounds;
            form.StartPosition = FormStartPosition.Manual;

            int x = (rect.Width / 2) + rect.Left - (form.Width / 2);
            int y = (rect.Height / 2) + rect.Top - (form.Height / 2);

            form.Location = new Point( x, y );
        }

        /// <summary>
        /// Parses the RTF into the RtfData structure for ease of use
        /// </summary>
        /// <param name="rtf">The RTF string to parse</param>
        /// <returns>null on error</returns>
        public static RtfData ParseRtf( string rtf )
        {
            RtfData rtfData = new RtfData();

            // check if the string is actually RTF-encoded
            if ( !rtf.StartsWith( "{\\rtf" ) )
            {
                string tempRtf = rtf.Replace( "\r", "" );
                string[] split = tempRtf.Split( new char[] { '\n' } );
                foreach ( string line in split )
                {
                    RtfLineData ld = new RtfLineData();
                    ld.SpanData.Add( new RtfSpanData( line ) );
                    rtfData.LineData.Add( ld );
                }

                return rtfData;
            }

            // find the clrs
            string[] openBraceSplit = rtf.Split( new char[] { '{' } );
            foreach ( string openBrace in openBraceSplit )
            {
                if ( openBrace.StartsWith( "\\colortbl" ) )
                {
                    string[] colorSplit = openBrace.Split( new char[] { ';' } );
                    for ( int i = 1; i < colorSplit.Length - 1; ++i )
                    {
                        if ( colorSplit[i].StartsWith( "\\" ) )
                        {
                            int r = 0;
                            int g = 0;
                            int b = 0;

                            string[] rgbSplit = colorSplit[i].Split( new char[] { '\\' }, StringSplitOptions.RemoveEmptyEntries );
                            foreach ( string rgb in rgbSplit )
                            {
                                string digits = null;
                                for ( int j = rgb.Length - 1; j >= 0; --j )
                                {
                                    if ( !Char.IsDigit( rgb[j] ) )
                                    {
                                        digits = rgb.Substring( j + 1 );
                                        break;
                                    }
                                }

                                if ( !String.IsNullOrEmpty( digits ) )
                                {
                                    if ( rgb.StartsWith( "red" ) )
                                    {
                                        r = int.Parse( digits );
                                    }
                                    else if ( rgb.StartsWith( "green" ) )
                                    {
                                        g = int.Parse( digits );
                                    }
                                    else if ( rgb.StartsWith( "blue" ) )
                                    {
                                        b = int.Parse( digits );
                                    }
                                }
                            }

                            RtfColorData colorData = new RtfColorData( Color.FromArgb( r, g, b ), rtfData.ColorData.Count );
                            rtfData.ColorData.Add( colorData );
                        }
                    }
                    break;
                }
            }

            int lastIndexOfCloseBrace = rtf.LastIndexOf( '}' );

            string subRtf = rtf.Substring( 0, lastIndexOfCloseBrace - 1 );
            int secondToLastIndexOfCloseBrace = subRtf.LastIndexOf( '}' );

            string temp = subRtf;
            while ( (secondToLastIndexOfCloseBrace > 0) && (temp[secondToLastIndexOfCloseBrace - 1] == '\\') )
            {
                temp = temp.Substring( 0, secondToLastIndexOfCloseBrace - 1 );
                secondToLastIndexOfCloseBrace = temp.LastIndexOf( '}' );
            }

            subRtf = rtf.Substring( secondToLastIndexOfCloseBrace + 1, lastIndexOfCloseBrace - secondToLastIndexOfCloseBrace - 1 );
            subRtf = subRtf.Replace( "\r\n", "" );
            subRtf = subRtf.Replace( "\n", "" );

            RtfLineData lineData = null;
            RtfSpanData spanData = null;

            for ( int i = 0; i < subRtf.Length; ++i )
            {
                if ( subRtf[i] == '\\' )
                {
                    int indexOfBackslash = subRtf.IndexOf( '\\', i + 1 );
                    int indexOfSpace = subRtf.IndexOf( ' ', i + 1 );

                    string controlWord = null;
                    if ( (indexOfBackslash != -1) && ((indexOfBackslash < indexOfSpace) || (indexOfSpace == -1)) )
                    {
                        if ( indexOfBackslash == i + 1 )
                        {
                            controlWord = subRtf.Substring( i, 2 );
                            ++i;
                        }
                        else
                        {
                            controlWord = subRtf.Substring( i, indexOfBackslash - i );
                            i = indexOfBackslash - 1;
                        }
                    }
                    else if ( (indexOfSpace != -1) && ((indexOfSpace < indexOfBackslash) || (indexOfBackslash == -1)) )
                    {
                        controlWord = subRtf.Substring( i, indexOfSpace - i );
                        i = indexOfSpace;
                    }
                    else
                    {
                        controlWord = subRtf.Substring( i );
                        i += controlWord.Length - 1;
                    }

                    if ( String.IsNullOrEmpty( controlWord ) )
                    {
                        continue;
                    }

                    if ( (controlWord == "\\b") || (controlWord == "\\b0")
                        || (controlWord == "\\i") || (controlWord == "\\i0")
                        || (controlWord == "\\ul") || (controlWord == "\\ul0") || (controlWord == "\\ulnone") )
                    {
                        if ( spanData == null )
                        {
                            spanData = new RtfSpanData();
                        }
                        else
                        {
                            // we're changing the format
                            if ( spanData.Text.Length > 0 )
                            {
                                if ( lineData == null )
                                {
                                    lineData = new RtfLineData();
                                }

                                lineData.SpanData.Add( spanData );

                                spanData = new RtfSpanData();
                            }
                        }
                    }

                    switch ( controlWord )
                    {
                        case "\\b":
                            {
                                spanData.Bold = true;
                            }
                            break;
                        case "\\b0":
                            {
                                spanData.Bold = false;
                            }
                            break;
                        case "\\i":
                            {
                                spanData.Italic = true;
                            }
                            break;
                        case "\\i0":
                            {
                                spanData.Italic = false;
                            }
                            break;
                        case "\\ul":
                            {
                                spanData.Underline = true;
                            }
                            break;
                        case "\\ul0":
                        case "\\ulnone":
                            {
                                spanData.Underline = false;
                            }
                            break;
                        case "\\par":
                            {
                                // skip the last \par, if any
                                if ( i >= subRtf.Length - 1 )
                                {
                                    break;
                                }

                                // check if we need to create an empty line
                                if ( spanData == null )
                                {
                                    spanData = new RtfSpanData();
                                }

                                if ( lineData == null )
                                {
                                    lineData = new RtfLineData();
                                }

                                lineData.SpanData.Add( spanData );
                                rtfData.LineData.Add( lineData );
                                lineData = null;
                                spanData = null;
                            }
                            break;
                        case "\\\\":
                            {
                                if ( spanData == null )
                                {
                                    spanData = new RtfSpanData();
                                }

                                // a backslash in text
                                spanData.Text.Append( '\\' );
                            }
                            break;
                        default:
                            {
                                if ( controlWord.StartsWith( "\\par" ) && (controlWord != "\\pard") )
                                {
                                    // check if we need to create an empty span
                                    if ( spanData == null )
                                    {
                                        spanData = new RtfSpanData();
                                    }

                                    if ( lineData == null )
                                    {
                                        lineData = new RtfLineData();
                                    }

                                    lineData.SpanData.Add( spanData );
                                    rtfData.LineData.Add( lineData );
                                    lineData = null;
                                    spanData = null;

                                    // Set i back to the end of this control word since there was no space in between
                                    i -= controlWord.Length - 3;
                                }
                                else if ( controlWord.StartsWith( "\\cf" ) )
                                {
                                    string digits = controlWord.Substring( 3 );
                                    if ( !String.IsNullOrEmpty( digits ) )
                                    {                                        
                                        if ( spanData == null )
                                        {
                                            spanData = new RtfSpanData();
                                        }
                                        else
                                        {
                                            // we're changing the format
                                            if ( spanData.Text.Length > 0 )
                                            {
                                                if ( lineData == null )
                                                {
                                                    lineData = new RtfLineData();
                                                }

                                                lineData.SpanData.Add( spanData );

                                                spanData = new RtfSpanData();
                                            }
                                        }

                                        int colorIndex = int.Parse( digits );
                                        if ( (colorIndex >= 0) && (colorIndex < rtfData.ColorData.Count) )
                                        {
                                            spanData.ColorData = rtfData.ColorData[colorIndex];
                                        }
                                    }
                                }
                                else if ( controlWord.StartsWith( "\\{" ) )
                                {
                                    if ( spanData == null )
                                    {
                                        spanData = new RtfSpanData();
                                    }

                                    // open brace in text
                                    spanData.Text.Append( '{' );
                                    spanData.Text.Append( controlWord.Substring( 2 ) );
                                }
                                else if ( controlWord.StartsWith( "\\}" ) )
                                {
                                    if ( spanData == null )
                                    {
                                        spanData = new RtfSpanData();
                                    }

                                    // close brace in text
                                    spanData.Text.Append( '}' );
                                    spanData.Text.Append( controlWord.Substring( 2 ) );
                                }
                                else if ( controlWord.StartsWith( "\\'" ) )
                                {
                                    if ( spanData == null )
                                    {
                                        spanData = new RtfSpanData();
                                    }

                                    string hexString = controlWord.Substring( 2, 2 );
                                    int intValue = int.Parse( hexString, System.Globalization.NumberStyles.HexNumber );

                                    string postControlWord = controlWord.Substring( 4 );

                                    // a special character
                                    spanData.Text.Append( String.Format( "{0}{1}", Convert.ToChar( intValue ), postControlWord ) );

                                    if ( (postControlWord.Length > 0) && (i < subRtf.Length) && (subRtf[i] == ' ') )
                                    {
                                        // Back up 1 cuz we need the space
                                        --i;
                                    }
                                }
                                else if ( controlWord.StartsWith( "\\u" ) && (controlWord.Length > 2) && Char.IsDigit( controlWord[2] ) )
                                {
                                    int indexOfQuestionMark = controlWord.IndexOf( '?' );
                                    if ( indexOfQuestionMark != -1 )
                                    {
                                        if ( spanData == null )
                                        {
                                            spanData = new RtfSpanData();
                                        }

                                        string intString = controlWord.Substring( 2, indexOfQuestionMark - 2 );
                                        int intValue = int.Parse( intString );

                                        // a unicode character
                                        spanData.Text.Append( String.Format( "{0}{1}", Convert.ToChar( intValue ),
                                            controlWord.Substring( indexOfQuestionMark + 1 ) ) );
                                    }
                                }
                            }
                            break;
                    }
                }
                else if ( subRtf[i] == '\t' )
                {
                    if ( spanData == null )
                    {
                        spanData = new RtfSpanData();
                    }
                
                    // replace tab with spaces
                    spanData.Text.Append( "    " );
                }
                else if ( (subRtf[i] == '\r') || (subRtf[i] == '\n') )
                {
                    // don't write linebreaks or newlines
                    continue;
                }
                else
                {
                    if ( spanData == null )
                    {
                        spanData = new RtfSpanData();
                    }

                    // raw text
                    spanData.Text.Append( subRtf[i] );
                }
            }

            // add the final spanData if it hasn't been added already
            if ( spanData != null )
            {
                if ( lineData == null )
                {
                    lineData = new RtfLineData();
                }

                lineData.SpanData.Add( spanData );
            }

            // add the final lineData if it hasn't been added already
            if ( lineData != null )
            {
                rtfData.LineData.Add( lineData );
            }

            return rtfData;
        }
        #endregion
    }
}
