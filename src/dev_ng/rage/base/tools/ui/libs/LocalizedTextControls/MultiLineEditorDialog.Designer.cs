namespace LocalizedTextControls
{
    partial class MultiLineEditorDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && (components != null) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.undoToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.redoToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.cutToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.copyToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.pasteToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.deleteToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.boldToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.italicToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.underlineToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.colorToolStripDropDownButton = new System.Windows.Forms.ToolStripDropDownButton();
            this.autoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.chooseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.blackToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.whiteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.grayToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.silverToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.maroonToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.redToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.oliveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.yellowToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.greenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.limeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tealToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aquaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.navyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.blueToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.purpleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fuchsiaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.insertVariableToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.localizedRichTextBox = new LocalizedTextControls.LocalizedRichTextBox();
            this.okButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.helpProvider = new System.Windows.Forms.HelpProvider();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.Items.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.undoToolStripButton,
            this.redoToolStripButton,
            this.toolStripSeparator,
            this.cutToolStripButton,
            this.copyToolStripButton,
            this.pasteToolStripButton,
            this.deleteToolStripButton,
            this.toolStripSeparator1,
            this.boldToolStripButton,
            this.italicToolStripButton,
            this.underlineToolStripButton,
            this.colorToolStripDropDownButton,
            this.toolStripSeparator2,
            this.insertVariableToolStripButton} );
            this.toolStrip1.Location = new System.Drawing.Point( 0, 0 );
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size( 437, 25 );
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // undoToolStripButton
            // 
            this.undoToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.undoToolStripButton.Image = global::LocalizedTextControls.Properties.Resources.Edit_UndoHS;
            this.undoToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.undoToolStripButton.Name = "undoToolStripButton";
            this.undoToolStripButton.Size = new System.Drawing.Size( 23, 22 );
            this.undoToolStripButton.Text = "Undo";
            this.undoToolStripButton.Click += new System.EventHandler( this.undoToolStripButton_Click );
            // 
            // redoToolStripButton
            // 
            this.redoToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.redoToolStripButton.Image = global::LocalizedTextControls.Properties.Resources.Edit_RedoHS;
            this.redoToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.redoToolStripButton.Name = "redoToolStripButton";
            this.redoToolStripButton.Size = new System.Drawing.Size( 23, 22 );
            this.redoToolStripButton.Text = "Redo";
            this.redoToolStripButton.Click += new System.EventHandler( this.redoToolStripButton_Click );
            // 
            // toolStripSeparator
            // 
            this.toolStripSeparator.Name = "toolStripSeparator";
            this.toolStripSeparator.Size = new System.Drawing.Size( 6, 25 );
            // 
            // cutToolStripButton
            // 
            this.cutToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.cutToolStripButton.Image = global::LocalizedTextControls.Properties.Resources.CutHS;
            this.cutToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.cutToolStripButton.Name = "cutToolStripButton";
            this.cutToolStripButton.Size = new System.Drawing.Size( 23, 22 );
            this.cutToolStripButton.Text = "Cut";
            this.cutToolStripButton.Click += new System.EventHandler( this.cutToolStripButton_Click );
            // 
            // copyToolStripButton
            // 
            this.copyToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.copyToolStripButton.Image = global::LocalizedTextControls.Properties.Resources.CopyHS;
            this.copyToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.copyToolStripButton.Name = "copyToolStripButton";
            this.copyToolStripButton.Size = new System.Drawing.Size( 23, 22 );
            this.copyToolStripButton.Text = "Copy";
            this.copyToolStripButton.Click += new System.EventHandler( this.copyToolStripButton_Click );
            // 
            // pasteToolStripButton
            // 
            this.pasteToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.pasteToolStripButton.Image = global::LocalizedTextControls.Properties.Resources.PasteHS;
            this.pasteToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.pasteToolStripButton.Name = "pasteToolStripButton";
            this.pasteToolStripButton.Size = new System.Drawing.Size( 23, 22 );
            this.pasteToolStripButton.Text = "Paste";
            this.pasteToolStripButton.Click += new System.EventHandler( this.pasteToolStripButton_Click );
            // 
            // deleteToolStripButton
            // 
            this.deleteToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.deleteToolStripButton.Image = global::LocalizedTextControls.Properties.Resources.DeleteHS;
            this.deleteToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.deleteToolStripButton.Name = "deleteToolStripButton";
            this.deleteToolStripButton.Size = new System.Drawing.Size( 23, 22 );
            this.deleteToolStripButton.Text = "Delete";
            this.deleteToolStripButton.Click += new System.EventHandler( this.deleteToolStripButton_Click );
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size( 6, 25 );
            // 
            // boldToolStripButton
            // 
            this.boldToolStripButton.CheckOnClick = true;
            this.boldToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.boldToolStripButton.Image = global::LocalizedTextControls.Properties.Resources.boldhs;
            this.boldToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.boldToolStripButton.Name = "boldToolStripButton";
            this.boldToolStripButton.Size = new System.Drawing.Size( 23, 22 );
            this.boldToolStripButton.Text = "Bold";
            this.boldToolStripButton.Click += new System.EventHandler( this.boldToolStripButton_Click );
            // 
            // italicToolStripButton
            // 
            this.italicToolStripButton.CheckOnClick = true;
            this.italicToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.italicToolStripButton.Image = global::LocalizedTextControls.Properties.Resources.ItalicHS;
            this.italicToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.italicToolStripButton.Name = "italicToolStripButton";
            this.italicToolStripButton.Size = new System.Drawing.Size( 23, 22 );
            this.italicToolStripButton.Text = "Italic";
            this.italicToolStripButton.Click += new System.EventHandler( this.italicToolStripButton_Click );
            // 
            // underlineToolStripButton
            // 
            this.underlineToolStripButton.CheckOnClick = true;
            this.underlineToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.underlineToolStripButton.Image = global::LocalizedTextControls.Properties.Resources.UnderlineHS;
            this.underlineToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.underlineToolStripButton.Name = "underlineToolStripButton";
            this.underlineToolStripButton.Size = new System.Drawing.Size( 23, 22 );
            this.underlineToolStripButton.Text = "Underline";
            this.underlineToolStripButton.Click += new System.EventHandler( this.underlineToolStripButton_Click );
            // 
            // colorToolStripDropDownButton
            // 
            this.colorToolStripDropDownButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.colorToolStripDropDownButton.DropDownItems.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.autoToolStripMenuItem,
            this.chooseToolStripMenuItem,
            this.toolStripSeparator3,
            this.blackToolStripMenuItem,
            this.whiteToolStripMenuItem,
            this.grayToolStripMenuItem,
            this.silverToolStripMenuItem,
            this.maroonToolStripMenuItem,
            this.redToolStripMenuItem,
            this.oliveToolStripMenuItem,
            this.yellowToolStripMenuItem,
            this.greenToolStripMenuItem,
            this.limeToolStripMenuItem,
            this.tealToolStripMenuItem,
            this.aquaToolStripMenuItem,
            this.navyToolStripMenuItem,
            this.blueToolStripMenuItem,
            this.purpleToolStripMenuItem,
            this.fuchsiaToolStripMenuItem} );
            this.colorToolStripDropDownButton.Image = global::LocalizedTextControls.Properties.Resources.Color_fontHS;
            this.colorToolStripDropDownButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.colorToolStripDropDownButton.Name = "colorToolStripDropDownButton";
            this.colorToolStripDropDownButton.ShowDropDownArrow = false;
            this.colorToolStripDropDownButton.Size = new System.Drawing.Size( 20, 22 );
            this.colorToolStripDropDownButton.Text = "toolStripDropDownButton1";
            // 
            // autoToolStripMenuItem
            // 
            this.autoToolStripMenuItem.Name = "autoToolStripMenuItem";
            this.autoToolStripMenuItem.Size = new System.Drawing.Size( 133, 22 );
            this.autoToolStripMenuItem.Text = "&Auto";
            this.autoToolStripMenuItem.Click += new System.EventHandler( this.autoToolStripMenuItem_Click );
            // 
            // chooseToolStripMenuItem
            // 
            this.chooseToolStripMenuItem.Name = "chooseToolStripMenuItem";
            this.chooseToolStripMenuItem.Size = new System.Drawing.Size( 133, 22 );
            this.chooseToolStripMenuItem.Text = "&Choose...";
            this.chooseToolStripMenuItem.Click += new System.EventHandler( this.chooseToolStripMenuItem_Click );
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size( 130, 6 );
            // 
            // blackToolStripMenuItem
            // 
            this.blackToolStripMenuItem.Image = global::LocalizedTextControls.Properties.Resources.black;
            this.blackToolStripMenuItem.Name = "blackToolStripMenuItem";
            this.blackToolStripMenuItem.Size = new System.Drawing.Size( 133, 22 );
            this.blackToolStripMenuItem.Text = "&Black";
            this.blackToolStripMenuItem.Click += new System.EventHandler( this.blackToolStripMenuItem_Click );
            // 
            // whiteToolStripMenuItem
            // 
            this.whiteToolStripMenuItem.Image = global::LocalizedTextControls.Properties.Resources.white;
            this.whiteToolStripMenuItem.Name = "whiteToolStripMenuItem";
            this.whiteToolStripMenuItem.Size = new System.Drawing.Size( 133, 22 );
            this.whiteToolStripMenuItem.Text = "&White";
            this.whiteToolStripMenuItem.Click += new System.EventHandler( this.whiteToolStripMenuItem_Click );
            // 
            // grayToolStripMenuItem
            // 
            this.grayToolStripMenuItem.Image = global::LocalizedTextControls.Properties.Resources.gray;
            this.grayToolStripMenuItem.Name = "grayToolStripMenuItem";
            this.grayToolStripMenuItem.Size = new System.Drawing.Size( 133, 22 );
            this.grayToolStripMenuItem.Text = "&Gray";
            this.grayToolStripMenuItem.Click += new System.EventHandler( this.grayToolStripMenuItem_Click );
            // 
            // silverToolStripMenuItem
            // 
            this.silverToolStripMenuItem.Image = global::LocalizedTextControls.Properties.Resources.silver;
            this.silverToolStripMenuItem.Name = "silverToolStripMenuItem";
            this.silverToolStripMenuItem.Size = new System.Drawing.Size( 133, 22 );
            this.silverToolStripMenuItem.Text = "&Silver";
            this.silverToolStripMenuItem.Click += new System.EventHandler( this.silverToolStripMenuItem_Click );
            // 
            // maroonToolStripMenuItem
            // 
            this.maroonToolStripMenuItem.Image = global::LocalizedTextControls.Properties.Resources.maroon;
            this.maroonToolStripMenuItem.Name = "maroonToolStripMenuItem";
            this.maroonToolStripMenuItem.Size = new System.Drawing.Size( 133, 22 );
            this.maroonToolStripMenuItem.Text = "&Maroon";
            this.maroonToolStripMenuItem.Click += new System.EventHandler( this.maroonToolStripMenuItem_Click );
            // 
            // redToolStripMenuItem
            // 
            this.redToolStripMenuItem.Image = global::LocalizedTextControls.Properties.Resources.red;
            this.redToolStripMenuItem.Name = "redToolStripMenuItem";
            this.redToolStripMenuItem.Size = new System.Drawing.Size( 133, 22 );
            this.redToolStripMenuItem.Text = "&Red";
            this.redToolStripMenuItem.Click += new System.EventHandler( this.redToolStripMenuItem_Click );
            // 
            // oliveToolStripMenuItem
            // 
            this.oliveToolStripMenuItem.Image = global::LocalizedTextControls.Properties.Resources.olive;
            this.oliveToolStripMenuItem.Name = "oliveToolStripMenuItem";
            this.oliveToolStripMenuItem.Size = new System.Drawing.Size( 133, 22 );
            this.oliveToolStripMenuItem.Text = "&Olive";
            this.oliveToolStripMenuItem.Click += new System.EventHandler( this.oliveToolStripMenuItem_Click );
            // 
            // yellowToolStripMenuItem
            // 
            this.yellowToolStripMenuItem.Image = global::LocalizedTextControls.Properties.Resources.yellow;
            this.yellowToolStripMenuItem.Name = "yellowToolStripMenuItem";
            this.yellowToolStripMenuItem.Size = new System.Drawing.Size( 133, 22 );
            this.yellowToolStripMenuItem.Text = "&Yellow";
            this.yellowToolStripMenuItem.Click += new System.EventHandler( this.yellowToolStripMenuItem_Click );
            // 
            // greenToolStripMenuItem
            // 
            this.greenToolStripMenuItem.Image = global::LocalizedTextControls.Properties.Resources.green;
            this.greenToolStripMenuItem.Name = "greenToolStripMenuItem";
            this.greenToolStripMenuItem.Size = new System.Drawing.Size( 133, 22 );
            this.greenToolStripMenuItem.Text = "Gr&een";
            this.greenToolStripMenuItem.Click += new System.EventHandler( this.greenToolStripMenuItem_Click );
            // 
            // limeToolStripMenuItem
            // 
            this.limeToolStripMenuItem.Image = global::LocalizedTextControls.Properties.Resources.lime;
            this.limeToolStripMenuItem.Name = "limeToolStripMenuItem";
            this.limeToolStripMenuItem.Size = new System.Drawing.Size( 133, 22 );
            this.limeToolStripMenuItem.Text = "&Lime";
            this.limeToolStripMenuItem.Click += new System.EventHandler( this.limeToolStripMenuItem_Click );
            // 
            // tealToolStripMenuItem
            // 
            this.tealToolStripMenuItem.Image = global::LocalizedTextControls.Properties.Resources.teal;
            this.tealToolStripMenuItem.Name = "tealToolStripMenuItem";
            this.tealToolStripMenuItem.Size = new System.Drawing.Size( 133, 22 );
            this.tealToolStripMenuItem.Text = "&Teal";
            this.tealToolStripMenuItem.Click += new System.EventHandler( this.tealToolStripMenuItem_Click );
            // 
            // aquaToolStripMenuItem
            // 
            this.aquaToolStripMenuItem.Image = global::LocalizedTextControls.Properties.Resources.aqua;
            this.aquaToolStripMenuItem.Name = "aquaToolStripMenuItem";
            this.aquaToolStripMenuItem.Size = new System.Drawing.Size( 133, 22 );
            this.aquaToolStripMenuItem.Text = "A&qua";
            this.aquaToolStripMenuItem.Click += new System.EventHandler( this.aquaToolStripMenuItem_Click );
            // 
            // navyToolStripMenuItem
            // 
            this.navyToolStripMenuItem.Image = global::LocalizedTextControls.Properties.Resources.navy;
            this.navyToolStripMenuItem.Name = "navyToolStripMenuItem";
            this.navyToolStripMenuItem.Size = new System.Drawing.Size( 133, 22 );
            this.navyToolStripMenuItem.Text = "&Navy";
            this.navyToolStripMenuItem.Click += new System.EventHandler( this.navyToolStripMenuItem_Click );
            // 
            // blueToolStripMenuItem
            // 
            this.blueToolStripMenuItem.Image = global::LocalizedTextControls.Properties.Resources.blue;
            this.blueToolStripMenuItem.Name = "blueToolStripMenuItem";
            this.blueToolStripMenuItem.Size = new System.Drawing.Size( 133, 22 );
            this.blueToolStripMenuItem.Text = "B&lue";
            this.blueToolStripMenuItem.Click += new System.EventHandler( this.blueToolStripMenuItem_Click );
            // 
            // purpleToolStripMenuItem
            // 
            this.purpleToolStripMenuItem.Image = global::LocalizedTextControls.Properties.Resources.purple;
            this.purpleToolStripMenuItem.Name = "purpleToolStripMenuItem";
            this.purpleToolStripMenuItem.Size = new System.Drawing.Size( 133, 22 );
            this.purpleToolStripMenuItem.Text = "&Purple";
            this.purpleToolStripMenuItem.Click += new System.EventHandler( this.purpleToolStripMenuItem_Click );
            // 
            // fuchsiaToolStripMenuItem
            // 
            this.fuchsiaToolStripMenuItem.Image = global::LocalizedTextControls.Properties.Resources.fuchsia;
            this.fuchsiaToolStripMenuItem.Name = "fuchsiaToolStripMenuItem";
            this.fuchsiaToolStripMenuItem.Size = new System.Drawing.Size( 133, 22 );
            this.fuchsiaToolStripMenuItem.Text = "&Fuchsia";
            this.fuchsiaToolStripMenuItem.Click += new System.EventHandler( this.fuchsiaToolStripMenuItem_Click );
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size( 6, 25 );
            // 
            // insertVariableToolStripButton
            // 
            this.insertVariableToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.insertVariableToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.insertVariableToolStripButton.Name = "insertVariableToolStripButton";
            this.insertVariableToolStripButton.Size = new System.Drawing.Size( 93, 22 );
            this.insertVariableToolStripButton.Text = "Insert Variable...";
            this.insertVariableToolStripButton.Click += new System.EventHandler( this.insertVariableToolStripButton_Click );
            // 
            // localizedRichTextBox
            // 
            this.localizedRichTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.localizedRichTextBox.DetectUrls = false;
            this.localizedRichTextBox.Location = new System.Drawing.Point( 0, 28 );
            this.localizedRichTextBox.Name = "localizedRichTextBox";
            this.localizedRichTextBox.SelectionBold = false;
            this.localizedRichTextBox.SelectionItalic = false;
            this.localizedRichTextBox.SelectionUnderline = false;
            this.localizedRichTextBox.Size = new System.Drawing.Size( 437, 185 );
            this.localizedRichTextBox.TabIndex = 1;
            this.localizedRichTextBox.Text = "";
            this.localizedRichTextBox.CanCopyChanged += new System.EventHandler( this.localizedRichTextBox_CanCopyChanged );
            this.localizedRichTextBox.SelectionBoldChanged += new System.EventHandler( this.localizedRichTextBox_SelectionBoldChanged );
            this.localizedRichTextBox.CanRedoChanged += new System.EventHandler( this.localizedRichTextBox_CanRedoChanged );
            this.localizedRichTextBox.CanDeleteChanged += new System.EventHandler( this.localizedRichTextBox_CanDeleteChanged );
            this.localizedRichTextBox.SelectionItalicChanged += new System.EventHandler( this.localizedRichTextBox_SelectionItalicChanged );
            this.localizedRichTextBox.CanUndoChanged += new System.EventHandler( this.localizedRichTextBox_CanUndoChanged );
            this.localizedRichTextBox.CanPasteTextChanged += new System.EventHandler( this.localizedRichTextBox_CanPasteTextChanged );
            this.localizedRichTextBox.CustomColorChanged += new System.EventHandler( this.localizedRichTextBox_CustomColorChanged );
            this.localizedRichTextBox.CanCutChanged += new System.EventHandler( this.localizedRichTextBox_CanCutChanged );
            this.localizedRichTextBox.SelectionUnderlineChanged += new System.EventHandler( this.localizedRichTextBox_SelectionUnderlineChanged );
            // 
            // okButton
            // 
            this.okButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.okButton.Location = new System.Drawing.Point( 269, 219 );
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size( 75, 23 );
            this.okButton.TabIndex = 2;
            this.okButton.Text = "OK";
            this.okButton.UseVisualStyleBackColor = true;
            // 
            // cancelButton
            // 
            this.cancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.Location = new System.Drawing.Point( 350, 219 );
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size( 75, 23 );
            this.cancelButton.TabIndex = 3;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            // 
            // MultiLineEditorDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cancelButton;
            this.ClientSize = new System.Drawing.Size( 437, 254 );
            this.Controls.Add( this.cancelButton );
            this.Controls.Add( this.okButton );
            this.Controls.Add( this.localizedRichTextBox );
            this.Controls.Add( this.toolStrip1 );
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.helpProvider.SetHelpKeyword( this, "LocalizedTextMultiLineEditorDialog.html" );
            this.helpProvider.SetHelpNavigator( this, System.Windows.Forms.HelpNavigator.Topic );
            this.MinimumSize = new System.Drawing.Size( 445, 278 );
            this.Name = "MultiLineEditorDialog";
            this.helpProvider.SetShowHelp( this, true );
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Multi Line Editor";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler( this.MultiLineEditorDialog_FormClosing );
            this.toolStrip1.ResumeLayout( false );
            this.toolStrip1.PerformLayout();
            this.ResumeLayout( false );
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton undoToolStripButton;
        private System.Windows.Forms.ToolStripButton redoToolStripButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator;
        private System.Windows.Forms.ToolStripButton cutToolStripButton;
        private System.Windows.Forms.ToolStripButton copyToolStripButton;
        private System.Windows.Forms.ToolStripButton pasteToolStripButton;
        private System.Windows.Forms.ToolStripButton deleteToolStripButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton boldToolStripButton;
        private System.Windows.Forms.ToolStripButton italicToolStripButton;
        private System.Windows.Forms.ToolStripButton underlineToolStripButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton insertVariableToolStripButton;
        private LocalizedRichTextBox localizedRichTextBox;
        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.ToolStripDropDownButton colorToolStripDropDownButton;
        private System.Windows.Forms.ToolStripMenuItem autoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem chooseToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem blackToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem whiteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem grayToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem silverToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem maroonToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem redToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem oliveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem yellowToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem greenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem limeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tealToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aquaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem navyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem blueToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem purpleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fuchsiaToolStripMenuItem;
        private System.Windows.Forms.HelpProvider helpProvider;
    }
}