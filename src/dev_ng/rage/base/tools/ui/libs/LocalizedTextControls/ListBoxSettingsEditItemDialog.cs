using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

using RSG.Base;
using RSG.Base.Forms;

namespace LocalizedTextControls
{
    /// <summary>
    /// This dialog is used on the ListBoxSettingsControl.
    /// </summary>
    public partial class ListBoxSettingsEditItemDialog : Form
    {
        public ListBoxSettingsEditItemDialog()
        {
            InitializeComponent();
        }

        #region Variables
        private ICloneable m_originalObject = null;
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the HelpNamespace for the dialog.
        /// </summary>
        [DefaultValue( null ), Description( "Gets or sets the HelpNamespace for the dialog." )]
        public string HelpNamespace
        {
            get
            {
                return this.helpProvider.HelpNamespace;
            }
            set
            {
                this.helpProvider.HelpNamespace = value;
            }
        }

        public ICloneable Item
        {
            get
            {
                return this.propertyGrid.SelectedObject as ICloneable;
            }
            set
            {
                if ( value != null )
                {
                    this.propertyGrid.SelectedObject = value.Clone();
                }
                else
                {
                    this.propertyGrid.SelectedObject = null;
                }

                m_originalObject = value;
            }
        }
        #endregion

        #region Event Handlers
        private void okButton_Click( object sender, EventArgs e )
        {
            ICloneable item = this.Item;
            if ( item.Equals( m_originalObject ) )
            {
                this.DialogResult = DialogResult.Cancel;
            }
            else
            {
                // Make sure we've filled out our required properties                
                PropertyInfo[] pInfoArray = item.GetType().GetProperties();
                foreach ( PropertyInfo pInfo in pInfoArray )
                {
                    object[] customAtts = pInfo.GetCustomAttributes( true );
                    if ( customAtts != null )
                    {
                        foreach ( object customAtt in customAtts )
                        {
                            if ( customAtt is System.ComponentModel.CategoryAttribute )
                            {
                                System.ComponentModel.CategoryAttribute categoryAtt = customAtt as System.ComponentModel.CategoryAttribute;
                                if ( categoryAtt.Category == "Required" )
                                {
                                    object val = pInfo.GetValue( item, null );
                                    if ( (val == null) || String.IsNullOrEmpty( val.ToString() ) )
                                    {
                                        rageMessageBox.ShowExclamation( this,
                                            String.Format( "The Required property '{0}' is empty.  Please provide a value and try again.", pInfo.Name ),
                                            this.Text );
                                        return;
                                    }
                                    break;
                                }
                            }
                        }
                    }
                }

                this.DialogResult = DialogResult.OK;
            }
        }
        #endregion
    }
}