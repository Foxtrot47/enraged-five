using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

using GlacialComponents.Controls.GlacialTreeList;

namespace LocalizedTextControls
{
    public abstract class EditCommand
    {
        public EditCommand()
        {

        }

        #region Properties
        public abstract string Name { get; }
        #endregion

        #region Public Functions
        /// <summary>
        /// Executes the edit command.
        /// </summary>
        /// <param name="localizedTextEditorControl"></param>
        public abstract bool Do( LocalizedTextEditorControl localizedTextEditorControl );

        /// <summary>
        /// Reverses the edit command.
        /// </summary>
        /// <param name="localizedTextEditorControl"></param>
        public abstract bool Undo( LocalizedTextEditorControl localizedTextEditorControl );
        #endregion
    }

    public abstract class RowEditCommand : EditCommand
    {
        public RowEditCommand( int row, LocalizedTextData data )
        {
            m_row = row;
            m_data = data;
        }

        #region Variables
        private int m_row = -1;
        private LocalizedTextData m_data;
        #endregion

        #region Properties
        public int Row
        {
            get
            {
                return m_row;
            }
        }

        public LocalizedTextData Data
        {
            get
            {
                return m_data;
            }
        }
        #endregion
    }

    public abstract class RowsEditCommand : EditCommand
    {
        public RowsEditCommand( List<int> rowList, LocalizedTextDataCollection collection )
        {
            m_rowList.AddRange( rowList );
            m_textDataCollection.AddRange( collection );
        }

        #region Variables
        private List<int> m_rowList = new List<int>();
        private LocalizedTextDataCollection m_textDataCollection = new LocalizedTextDataCollection();
        #endregion

        #region Properties
        public List<int> RowList
        {
            get
            {
                return m_rowList;
            }
        }

        public LocalizedTextDataCollection TextDataCollection
        {
            get
            {
                return m_textDataCollection;
            }
        }
        #endregion
    }

    public class ChangeValueEditCommand : RowEditCommand
    {
        public ChangeValueEditCommand( int row, LocalizedTextData data, LocalizedTextData.DataMember member, string value )
            : base( row, data )
        {
            m_member = member;
            m_value = value;
        }

        #region Variables
        protected LocalizedTextData.DataMember m_member;
        protected string m_value;
        protected string m_previousValue = string.Empty;
        #endregion

        #region Properties
        public override string Name
        {
            get 
            {
                return "Change Value";
            }
        }

        public LocalizedTextData.DataMember Member
        {
            get
            {
                return m_member;
            }
        }

        public string Value
        {
            get
            {
                return m_value;
            }
        }

        public string PreviousValue
        {
            get
            {
                return m_previousValue;
            }
        }
        #endregion

        #region Public Functions
        /// <summary>
        /// Executes the edit command.
        /// </summary>
        /// <param name="localizedTextEditorControl"></param>
        public override bool Do( LocalizedTextEditorControl localizedTextEditorControl )
        {
            if ( localizedTextEditorControl.GetValue( this.Row, m_member, ref m_previousValue ) )
            {
                return localizedTextEditorControl.SetValue( this.Row, m_member, m_value );
            }

            return false;
        }

        /// <summary>
        /// Reverses the edit command.
        /// </summary>
        /// <param name="localizedTextEditorControl"></param>
        public override bool Undo( LocalizedTextEditorControl localizedTextEditorControl )
        {
            return localizedTextEditorControl.SetValue( this.Row, m_member, m_previousValue );
        }
        #endregion
    }

    
    public class AddRowEditCommand : RowEditCommand
    {
        public AddRowEditCommand( int row, LocalizedTextData data  )
            : base( row, data )
        {
            
        }

        #region Properties
        public override string Name
        {
            get
            {
                return "Add Row";
            }
        }
        #endregion

        #region Public Functions
        /// <summary>
        /// Executes the edit command.
        /// </summary>
        /// <param name="localizedTextEditorControl"></param>
        public override bool Do( LocalizedTextEditorControl localizedTextEditorControl )
        {
            return localizedTextEditorControl.Insert( this.Row, this.Data );
        }

        /// <summary>
        /// Reverses the edit command.
        /// </summary>
        /// <param name="localizedTextEditorControl"></param>
        public override bool Undo( LocalizedTextEditorControl localizedTextEditorControl )
        {
            return localizedTextEditorControl.RemoveAt( this.Row );
        }
        #endregion
    }

    public class RemoveRowEditCommand : AddRowEditCommand
    {
        public RemoveRowEditCommand( int row, LocalizedTextData data )
            : base( row, data )
        {
            
        }

        #region Properties
        public override string Name
        {
            get
            {
                return "Remove Row";
            }
        }
        #endregion

        #region Public Functions
        /// <summary>
        /// Executes the edit command.
        /// </summary>
        /// <param name="localizedTextEditorControl"></param>
        public override bool Do( LocalizedTextEditorControl localizedTextEditorControl )
        {
            return localizedTextEditorControl.RemoveAt( this.Row );
        }

        /// <summary>
        /// Reverses the edit command.
        /// </summary>
        /// <param name="localizedTextEditorControl"></param>
        public override bool Undo( LocalizedTextEditorControl localizedTextEditorControl )
        {
            return localizedTextEditorControl.Insert( this.Row, this.Data );
        }
        #endregion
    }

    public class MoveRowEditCommand : RowEditCommand
    {
        public MoveRowEditCommand( int row, LocalizedTextData data, int destination )
            : base( row, data )
        {
            m_destination = destination;
        }

        #region Variables
        protected int m_destination;
        #endregion

        #region Properties
        public override string Name
        {
            get
            {
                return "Move Row";
            }
        }

        public int Destination
        {
            get
            {
                return m_destination;
            }
        }
        #endregion

        #region Public Functions
        /// <summary>
        /// Executes the edit command.
        /// </summary>
        /// <param name="localizedTextEditorControl"></param>
        public override bool Do( LocalizedTextEditorControl localizedTextEditorControl )
        {
            return localizedTextEditorControl.MoveRow( this.Row, m_destination );
        }

        /// <summary>
        /// Reverses the edit command.
        /// </summary>
        /// <param name="localizedTextEditorControl"></param>
        public override bool Undo( LocalizedTextEditorControl localizedTextEditorControl )
        {
            return localizedTextEditorControl.MoveRow( m_destination, this.Row );
        }
        #endregion
    }

    public class AddRowsEditCommand : RowsEditCommand
    {
        public AddRowsEditCommand( List<int> rowList, LocalizedTextDataCollection collection )
            : base( rowList, collection )
        {

        }

        #region Properties
        public override string Name
        {
            get
            {
                return "Add Rows";
            }
        }
        #endregion

        #region Public Functions
        /// <summary>
        /// Executes the edit command.
        /// </summary>
        /// <param name="localizedTextEditorControl"></param>
        public override bool Do( LocalizedTextEditorControl localizedTextEditorControl )
        {
            return localizedTextEditorControl.Insert( this.RowList, this.TextDataCollection );
        }

        /// <summary>
        /// Reverses the edit command.
        /// </summary>
        /// <param name="localizedTextEditorControl"></param>
        public override bool Undo( LocalizedTextEditorControl localizedTextEditorControl )
        {
            return localizedTextEditorControl.Remove( this.TextDataCollection );
        }
        #endregion
    }

    public class RemoveRowsEditCommand : RowsEditCommand
    {
        public RemoveRowsEditCommand( List<int> rowList, LocalizedTextDataCollection collection )
            : base( rowList, collection )
        {

        }

        #region Properties
        public override string Name
        {
            get
            {
                return "Remove Rows";
            }
        }
        #endregion

        #region Public Functions
        /// <summary>
        /// Executes the edit command.
        /// </summary>
        /// <param name="localizedTextEditorControl"></param>
        public override bool Do( LocalizedTextEditorControl localizedTextEditorControl )
        {
            return localizedTextEditorControl.Remove( this.TextDataCollection );
        }

        /// <summary>
        /// Reverses the edit command.
        /// </summary>
        /// <param name="localizedTextEditorControl"></param>
        public override bool Undo( LocalizedTextEditorControl localizedTextEditorControl )
        {
            return localizedTextEditorControl.Insert( this.RowList, this.TextDataCollection );
        }
        #endregion
    }
}
