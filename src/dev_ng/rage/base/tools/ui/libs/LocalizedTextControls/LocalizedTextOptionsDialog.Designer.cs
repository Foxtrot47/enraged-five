namespace LocalizedTextControls
{
    partial class LocalizedTextOptionsDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && (components != null) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl = new System.Windows.Forms.TabControl();
            this.generalTabPage = new System.Windows.Forms.TabPage();
            this.generalBrowseTextExporterPluginButton = new System.Windows.Forms.Button();
            this.generalTextExporterPluginTextBox = new System.Windows.Forms.TextBox();
            this.generalTextExporterPluginLabel = new System.Windows.Forms.Label();
            this.generalImportButton = new System.Windows.Forms.Button();
            this.generalBrowseTextRendererPluginButton = new System.Windows.Forms.Button();
            this.generalTextRendererPluginTextBox = new System.Windows.Forms.TextBox();
            this.generalTextRendererPluginLabel = new System.Windows.Forms.Label();
            this.generalScreenHeightNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.generalScreenWidthNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.generalScreenResolutionLabel = new System.Windows.Forms.Label();
            this.languagesTabPage = new System.Windows.Forms.TabPage();
            this.languageListBoxSettingsControl = new LocalizedTextControls.ListBoxSettingsControl();
            this.regionsTabPage = new System.Windows.Forms.TabPage();
            this.regionListBoxSettingsControl = new LocalizedTextControls.ListBoxSettingsControl();
            this.platformsTabPage = new System.Windows.Forms.TabPage();
            this.platformListBoxSettingsControl = new LocalizedTextControls.ListBoxSettingsControl();
            this.fontsTabPage = new System.Windows.Forms.TabPage();
            this.fontListBoxSettingsControl = new LocalizedTextControls.ListBoxSettingsControl();
            this.flagsTabPage = new System.Windows.Forms.TabPage();
            this.flagListBoxSettingsControl = new LocalizedTextControls.ListBoxSettingsControl();
            this.variablesTabPage = new System.Windows.Forms.TabPage();
            this.variablesListBoxSettingsControl = new LocalizedTextControls.ListBoxSettingsControl();
            this.okButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.helpProvider = new System.Windows.Forms.HelpProvider();
            this.tabControl.SuspendLayout();
            this.generalTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.generalScreenHeightNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.generalScreenWidthNumericUpDown)).BeginInit();
            this.languagesTabPage.SuspendLayout();
            this.regionsTabPage.SuspendLayout();
            this.platformsTabPage.SuspendLayout();
            this.fontsTabPage.SuspendLayout();
            this.flagsTabPage.SuspendLayout();
            this.variablesTabPage.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl
            // 
            this.tabControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl.Controls.Add( this.generalTabPage );
            this.tabControl.Controls.Add( this.languagesTabPage );
            this.tabControl.Controls.Add( this.regionsTabPage );
            this.tabControl.Controls.Add( this.platformsTabPage );
            this.tabControl.Controls.Add( this.fontsTabPage );
            this.tabControl.Controls.Add( this.flagsTabPage );
            this.tabControl.Controls.Add( this.variablesTabPage );
            this.tabControl.Location = new System.Drawing.Point( 12, 12 );
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size( 506, 344 );
            this.tabControl.TabIndex = 0;
            // 
            // generalTabPage
            // 
            this.generalTabPage.Controls.Add( this.generalBrowseTextExporterPluginButton );
            this.generalTabPage.Controls.Add( this.generalTextExporterPluginTextBox );
            this.generalTabPage.Controls.Add( this.generalTextExporterPluginLabel );
            this.generalTabPage.Controls.Add( this.generalImportButton );
            this.generalTabPage.Controls.Add( this.generalBrowseTextRendererPluginButton );
            this.generalTabPage.Controls.Add( this.generalTextRendererPluginTextBox );
            this.generalTabPage.Controls.Add( this.generalTextRendererPluginLabel );
            this.generalTabPage.Controls.Add( this.generalScreenHeightNumericUpDown );
            this.generalTabPage.Controls.Add( this.generalScreenWidthNumericUpDown );
            this.generalTabPage.Controls.Add( this.generalScreenResolutionLabel );
            this.generalTabPage.Location = new System.Drawing.Point( 4, 22 );
            this.generalTabPage.Name = "generalTabPage";
            this.generalTabPage.Padding = new System.Windows.Forms.Padding( 3 );
            this.generalTabPage.Size = new System.Drawing.Size( 498, 318 );
            this.generalTabPage.TabIndex = 0;
            this.generalTabPage.Text = "General";
            this.generalTabPage.UseVisualStyleBackColor = true;
            // 
            // generalBrowseTextExporterPluginButton
            // 
            this.generalBrowseTextExporterPluginButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.helpProvider.SetHelpString( this.generalBrowseTextExporterPluginButton, "Browse for a Localized Text Data exporter plugin." );
            this.generalBrowseTextExporterPluginButton.Location = new System.Drawing.Point( 468, 30 );
            this.generalBrowseTextExporterPluginButton.Name = "generalBrowseTextExporterPluginButton";
            this.helpProvider.SetShowHelp( this.generalBrowseTextExporterPluginButton, true );
            this.generalBrowseTextExporterPluginButton.Size = new System.Drawing.Size( 24, 23 );
            this.generalBrowseTextExporterPluginButton.TabIndex = 5;
            this.generalBrowseTextExporterPluginButton.Text = "...";
            this.generalBrowseTextExporterPluginButton.UseVisualStyleBackColor = true;
            this.generalBrowseTextExporterPluginButton.Click += new System.EventHandler( this.generalBrowseTextExporterPluginButton_Click );
            // 
            // generalTextExporterPluginTextBox
            // 
            this.generalTextExporterPluginTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.helpProvider.SetHelpString( this.generalTextExporterPluginTextBox, "The path and filename of the .dll that exports the Localized Text Data.  Can be r" +
                    "elative to the project." );
            this.generalTextExporterPluginTextBox.Location = new System.Drawing.Point( 119, 32 );
            this.generalTextExporterPluginTextBox.Name = "generalTextExporterPluginTextBox";
            this.helpProvider.SetShowHelp( this.generalTextExporterPluginTextBox, true );
            this.generalTextExporterPluginTextBox.Size = new System.Drawing.Size( 343, 20 );
            this.generalTextExporterPluginTextBox.TabIndex = 4;
            this.generalTextExporterPluginTextBox.TextChanged += new System.EventHandler( this.generalTextExporterPluginTextBox_TextChanged );
            // 
            // generalTextExporterPluginLabel
            // 
            this.generalTextExporterPluginLabel.AutoSize = true;
            this.helpProvider.SetHelpString( this.generalTextExporterPluginLabel, "The path and filename of the .dll that exports the Localized Text Data.  Can be r" +
                    "elative to the project." );
            this.generalTextExporterPluginLabel.Location = new System.Drawing.Point( 11, 35 );
            this.generalTextExporterPluginLabel.Name = "generalTextExporterPluginLabel";
            this.helpProvider.SetShowHelp( this.generalTextExporterPluginLabel, true );
            this.generalTextExporterPluginLabel.Size = new System.Drawing.Size( 102, 13 );
            this.generalTextExporterPluginLabel.TabIndex = 3;
            this.generalTextExporterPluginLabel.Text = "Text Exporter Plugin";
            // 
            // generalImportButton
            // 
            this.generalImportButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.helpProvider.SetHelpString( this.generalImportButton, "Import a .txtproj.options file." );
            this.generalImportButton.Location = new System.Drawing.Point( 6, 85 );
            this.generalImportButton.Name = "generalImportButton";
            this.helpProvider.SetShowHelp( this.generalImportButton, true );
            this.generalImportButton.Size = new System.Drawing.Size( 486, 23 );
            this.generalImportButton.TabIndex = 9;
            this.generalImportButton.Text = "Import";
            this.generalImportButton.UseVisualStyleBackColor = true;
            this.generalImportButton.Click += new System.EventHandler( this.generalImportButton_Click );
            // 
            // generalBrowseTextRendererPluginButton
            // 
            this.generalBrowseTextRendererPluginButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.helpProvider.SetHelpString( this.generalBrowseTextRendererPluginButton, "Browse for a Localized Text Data renderer plugin." );
            this.generalBrowseTextRendererPluginButton.Location = new System.Drawing.Point( 468, 56 );
            this.generalBrowseTextRendererPluginButton.Name = "generalBrowseTextRendererPluginButton";
            this.helpProvider.SetShowHelp( this.generalBrowseTextRendererPluginButton, true );
            this.generalBrowseTextRendererPluginButton.Size = new System.Drawing.Size( 24, 23 );
            this.generalBrowseTextRendererPluginButton.TabIndex = 8;
            this.generalBrowseTextRendererPluginButton.Text = "...";
            this.generalBrowseTextRendererPluginButton.UseVisualStyleBackColor = true;
            this.generalBrowseTextRendererPluginButton.Click += new System.EventHandler( this.generalBrowseTextRendererPluginButton_Click );
            // 
            // generalTextRendererPluginTextBox
            // 
            this.generalTextRendererPluginTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.helpProvider.SetHelpString( this.generalTextRendererPluginTextBox, "The path and filename of the .dll that renders the Localized Text Data.  Can be r" +
                    "elative to the project." );
            this.generalTextRendererPluginTextBox.Location = new System.Drawing.Point( 119, 58 );
            this.generalTextRendererPluginTextBox.Name = "generalTextRendererPluginTextBox";
            this.helpProvider.SetShowHelp( this.generalTextRendererPluginTextBox, true );
            this.generalTextRendererPluginTextBox.Size = new System.Drawing.Size( 343, 20 );
            this.generalTextRendererPluginTextBox.TabIndex = 7;
            this.generalTextRendererPluginTextBox.TextChanged += new System.EventHandler( this.generalTextRendererPluginTextBox_TextChanged );
            // 
            // generalTextRendererPluginLabel
            // 
            this.generalTextRendererPluginLabel.AutoSize = true;
            this.helpProvider.SetHelpString( this.generalTextRendererPluginLabel, "The path and filename of the .dll that renders the Localized Text Data.  Can be r" +
                    "elative to the project." );
            this.generalTextRendererPluginLabel.Location = new System.Drawing.Point( 6, 61 );
            this.generalTextRendererPluginLabel.Name = "generalTextRendererPluginLabel";
            this.helpProvider.SetShowHelp( this.generalTextRendererPluginLabel, true );
            this.generalTextRendererPluginLabel.Size = new System.Drawing.Size( 107, 13 );
            this.generalTextRendererPluginLabel.TabIndex = 6;
            this.generalTextRendererPluginLabel.Text = "Text Renderer Plugin";
            // 
            // generalScreenHeightNumericUpDown
            // 
            this.helpProvider.SetHelpString( this.generalScreenHeightNumericUpDown, "The screen height." );
            this.generalScreenHeightNumericUpDown.Increment = new decimal( new int[] {
            10,
            0,
            0,
            0} );
            this.generalScreenHeightNumericUpDown.Location = new System.Drawing.Point( 186, 6 );
            this.generalScreenHeightNumericUpDown.Maximum = new decimal( new int[] {
            3200,
            0,
            0,
            0} );
            this.generalScreenHeightNumericUpDown.Minimum = new decimal( new int[] {
            1,
            0,
            0,
            0} );
            this.generalScreenHeightNumericUpDown.Name = "generalScreenHeightNumericUpDown";
            this.helpProvider.SetShowHelp( this.generalScreenHeightNumericUpDown, true );
            this.generalScreenHeightNumericUpDown.Size = new System.Drawing.Size( 60, 20 );
            this.generalScreenHeightNumericUpDown.TabIndex = 2;
            this.generalScreenHeightNumericUpDown.Value = new decimal( new int[] {
            720,
            0,
            0,
            0} );
            this.generalScreenHeightNumericUpDown.ValueChanged += new System.EventHandler( this.generalScreenHeightNumericUpDown_ValueChanged );
            // 
            // generalScreenWidthNumericUpDown
            // 
            this.helpProvider.SetHelpString( this.generalScreenWidthNumericUpDown, "The screen width." );
            this.generalScreenWidthNumericUpDown.Increment = new decimal( new int[] {
            10,
            0,
            0,
            0} );
            this.generalScreenWidthNumericUpDown.Location = new System.Drawing.Point( 120, 6 );
            this.generalScreenWidthNumericUpDown.Maximum = new decimal( new int[] {
            3200,
            0,
            0,
            0} );
            this.generalScreenWidthNumericUpDown.Minimum = new decimal( new int[] {
            1,
            0,
            0,
            0} );
            this.generalScreenWidthNumericUpDown.Name = "generalScreenWidthNumericUpDown";
            this.helpProvider.SetShowHelp( this.generalScreenWidthNumericUpDown, true );
            this.generalScreenWidthNumericUpDown.Size = new System.Drawing.Size( 60, 20 );
            this.generalScreenWidthNumericUpDown.TabIndex = 1;
            this.generalScreenWidthNumericUpDown.Value = new decimal( new int[] {
            1280,
            0,
            0,
            0} );
            this.generalScreenWidthNumericUpDown.ValueChanged += new System.EventHandler( this.generalScreenWidthNumericUpDown_ValueChanged );
            // 
            // generalScreenResolutionLabel
            // 
            this.generalScreenResolutionLabel.AutoSize = true;
            this.helpProvider.SetHelpString( this.generalScreenResolutionLabel, "The dimensions that the Localized Text Data should be rendered at.  Typically, th" +
                    "is should match the game\'s native resolution." );
            this.generalScreenResolutionLabel.Location = new System.Drawing.Point( 16, 8 );
            this.generalScreenResolutionLabel.Name = "generalScreenResolutionLabel";
            this.helpProvider.SetShowHelp( this.generalScreenResolutionLabel, true );
            this.generalScreenResolutionLabel.Size = new System.Drawing.Size( 94, 13 );
            this.generalScreenResolutionLabel.TabIndex = 0;
            this.generalScreenResolutionLabel.Text = "Screen Resolution";
            // 
            // languagesTabPage
            // 
            this.languagesTabPage.Controls.Add( this.languageListBoxSettingsControl );
            this.languagesTabPage.Location = new System.Drawing.Point( 4, 22 );
            this.languagesTabPage.Name = "languagesTabPage";
            this.languagesTabPage.Padding = new System.Windows.Forms.Padding( 3 );
            this.languagesTabPage.Size = new System.Drawing.Size( 498, 318 );
            this.languagesTabPage.TabIndex = 2;
            this.languagesTabPage.Text = "Languages";
            this.languagesTabPage.UseVisualStyleBackColor = true;
            // 
            // languageListBoxSettingsControl
            // 
            this.languageListBoxSettingsControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.languageListBoxSettingsControl.Location = new System.Drawing.Point( 3, 3 );
            this.languageListBoxSettingsControl.Name = "languageListBoxSettingsControl";
            this.languageListBoxSettingsControl.Size = new System.Drawing.Size( 492, 312 );
            this.languageListBoxSettingsControl.TabIndex = 0;
            this.languageListBoxSettingsControl.TypeName = "Language";
            this.languageListBoxSettingsControl.Changed += new System.EventHandler( this.listBoxSettingsControl_Changed );
            // 
            // regionsTabPage
            // 
            this.regionsTabPage.Controls.Add( this.regionListBoxSettingsControl );
            this.regionsTabPage.Location = new System.Drawing.Point( 4, 22 );
            this.regionsTabPage.Name = "regionsTabPage";
            this.regionsTabPage.Padding = new System.Windows.Forms.Padding( 3 );
            this.regionsTabPage.Size = new System.Drawing.Size( 498, 318 );
            this.regionsTabPage.TabIndex = 5;
            this.regionsTabPage.Text = "Regions";
            this.regionsTabPage.UseVisualStyleBackColor = true;
            // 
            // regionListBoxSettingsControl
            // 
            this.regionListBoxSettingsControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.regionListBoxSettingsControl.Location = new System.Drawing.Point( 3, 3 );
            this.regionListBoxSettingsControl.Name = "regionListBoxSettingsControl";
            this.regionListBoxSettingsControl.Size = new System.Drawing.Size( 492, 312 );
            this.regionListBoxSettingsControl.TabIndex = 0;
            this.regionListBoxSettingsControl.Changed += new System.EventHandler( this.listBoxSettingsControl_Changed );
            // 
            // platformsTabPage
            // 
            this.platformsTabPage.Controls.Add( this.platformListBoxSettingsControl );
            this.platformsTabPage.Location = new System.Drawing.Point( 4, 22 );
            this.platformsTabPage.Name = "platformsTabPage";
            this.platformsTabPage.Padding = new System.Windows.Forms.Padding( 3 );
            this.platformsTabPage.Size = new System.Drawing.Size( 498, 318 );
            this.platformsTabPage.TabIndex = 6;
            this.platformsTabPage.Text = "Platforms";
            this.platformsTabPage.UseVisualStyleBackColor = true;
            // 
            // platformListBoxSettingsControl
            // 
            this.platformListBoxSettingsControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.platformListBoxSettingsControl.Location = new System.Drawing.Point( 3, 3 );
            this.platformListBoxSettingsControl.Name = "platformListBoxSettingsControl";
            this.platformListBoxSettingsControl.Size = new System.Drawing.Size( 492, 312 );
            this.platformListBoxSettingsControl.TabIndex = 0;
            this.platformListBoxSettingsControl.Changed += new System.EventHandler( this.listBoxSettingsControl_Changed );
            // 
            // fontsTabPage
            // 
            this.fontsTabPage.Controls.Add( this.fontListBoxSettingsControl );
            this.fontsTabPage.Location = new System.Drawing.Point( 4, 22 );
            this.fontsTabPage.Name = "fontsTabPage";
            this.fontsTabPage.Padding = new System.Windows.Forms.Padding( 3 );
            this.fontsTabPage.Size = new System.Drawing.Size( 498, 318 );
            this.fontsTabPage.TabIndex = 1;
            this.fontsTabPage.Text = "Fonts";
            this.fontsTabPage.UseVisualStyleBackColor = true;
            // 
            // fontListBoxSettingsControl
            // 
            this.fontListBoxSettingsControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fontListBoxSettingsControl.Location = new System.Drawing.Point( 3, 3 );
            this.fontListBoxSettingsControl.Name = "fontListBoxSettingsControl";
            this.fontListBoxSettingsControl.Size = new System.Drawing.Size( 492, 312 );
            this.fontListBoxSettingsControl.TabIndex = 0;
            this.fontListBoxSettingsControl.TypeName = "Font";
            this.fontListBoxSettingsControl.Changed += new System.EventHandler( this.listBoxSettingsControl_Changed );
            // 
            // flagsTabPage
            // 
            this.flagsTabPage.Controls.Add( this.flagListBoxSettingsControl );
            this.flagsTabPage.Location = new System.Drawing.Point( 4, 22 );
            this.flagsTabPage.Name = "flagsTabPage";
            this.flagsTabPage.Padding = new System.Windows.Forms.Padding( 3 );
            this.flagsTabPage.Size = new System.Drawing.Size( 498, 318 );
            this.flagsTabPage.TabIndex = 3;
            this.flagsTabPage.Text = "Flags";
            this.flagsTabPage.UseVisualStyleBackColor = true;
            // 
            // flagListBoxSettingsControl
            // 
            this.flagListBoxSettingsControl.DefaultEnabled = false;
            this.flagListBoxSettingsControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flagListBoxSettingsControl.Location = new System.Drawing.Point( 3, 3 );
            this.flagListBoxSettingsControl.Name = "flagListBoxSettingsControl";
            this.flagListBoxSettingsControl.Size = new System.Drawing.Size( 492, 312 );
            this.flagListBoxSettingsControl.TabIndex = 0;
            this.flagListBoxSettingsControl.TypeName = "Flag";
            this.flagListBoxSettingsControl.Changed += new System.EventHandler( this.listBoxSettingsControl_Changed );
            // 
            // variablesTabPage
            // 
            this.variablesTabPage.Controls.Add( this.variablesListBoxSettingsControl );
            this.variablesTabPage.Location = new System.Drawing.Point( 4, 22 );
            this.variablesTabPage.Name = "variablesTabPage";
            this.variablesTabPage.Padding = new System.Windows.Forms.Padding( 3 );
            this.variablesTabPage.Size = new System.Drawing.Size( 498, 318 );
            this.variablesTabPage.TabIndex = 4;
            this.variablesTabPage.Text = "Variables";
            this.variablesTabPage.UseVisualStyleBackColor = true;
            // 
            // variablesListBoxSettingsControl
            // 
            this.variablesListBoxSettingsControl.DefaultEnabled = false;
            this.variablesListBoxSettingsControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.variablesListBoxSettingsControl.Location = new System.Drawing.Point( 3, 3 );
            this.variablesListBoxSettingsControl.Name = "variablesListBoxSettingsControl";
            this.variablesListBoxSettingsControl.Size = new System.Drawing.Size( 492, 312 );
            this.variablesListBoxSettingsControl.TabIndex = 0;
            this.variablesListBoxSettingsControl.TypeName = "Variable";
            this.variablesListBoxSettingsControl.Changed += new System.EventHandler( this.listBoxSettingsControl_Changed );
            // 
            // okButton
            // 
            this.okButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.okButton.Location = new System.Drawing.Point( 362, 362 );
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size( 75, 23 );
            this.okButton.TabIndex = 1;
            this.okButton.Text = "OK";
            this.okButton.UseVisualStyleBackColor = true;
            this.okButton.Click += new System.EventHandler( this.okButton_Click );
            // 
            // cancelButton
            // 
            this.cancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.Location = new System.Drawing.Point( 443, 362 );
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size( 75, 23 );
            this.cancelButton.TabIndex = 2;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            // 
            // openFileDialog
            // 
            this.openFileDialog.RestoreDirectory = true;
            // 
            // saveFileDialog
            // 
            this.saveFileDialog.Filter = "Localized Text Options Files (*.txtproj.options)|*.txtproj.options";
            this.saveFileDialog.Title = "Save Localized Text Options File";
            // 
            // LocalizedTextOptionsDialog
            // 
            this.AcceptButton = this.okButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cancelButton;
            this.ClientSize = new System.Drawing.Size( 530, 397 );
            this.Controls.Add( this.cancelButton );
            this.Controls.Add( this.okButton );
            this.Controls.Add( this.tabControl );
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.HelpButton = true;
            this.helpProvider.SetHelpKeyword( this, "LocalizedTextOptionsDialog.html" );
            this.helpProvider.SetHelpNavigator( this, System.Windows.Forms.HelpNavigator.Topic );
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "LocalizedTextOptionsDialog";
            this.helpProvider.SetShowHelp( this, true );
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Localized Text Options";
            this.tabControl.ResumeLayout( false );
            this.generalTabPage.ResumeLayout( false );
            this.generalTabPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.generalScreenHeightNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.generalScreenWidthNumericUpDown)).EndInit();
            this.languagesTabPage.ResumeLayout( false );
            this.regionsTabPage.ResumeLayout( false );
            this.platformsTabPage.ResumeLayout( false );
            this.fontsTabPage.ResumeLayout( false );
            this.flagsTabPage.ResumeLayout( false );
            this.variablesTabPage.ResumeLayout( false );
            this.ResumeLayout( false );

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage generalTabPage;
        private System.Windows.Forms.TabPage fontsTabPage;
        private System.Windows.Forms.TabPage languagesTabPage;
        private System.Windows.Forms.TabPage flagsTabPage;
        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Button generalBrowseTextRendererPluginButton;
        private System.Windows.Forms.TextBox generalTextRendererPluginTextBox;
        private System.Windows.Forms.Label generalTextRendererPluginLabel;
        private System.Windows.Forms.NumericUpDown generalScreenHeightNumericUpDown;
        private System.Windows.Forms.NumericUpDown generalScreenWidthNumericUpDown;
        private System.Windows.Forms.Label generalScreenResolutionLabel;
        private ListBoxSettingsControl fontListBoxSettingsControl;
        private ListBoxSettingsControl languageListBoxSettingsControl;
        private ListBoxSettingsControl flagListBoxSettingsControl;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.Button generalImportButton;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
        private System.Windows.Forms.TabPage variablesTabPage;
        private ListBoxSettingsControl variablesListBoxSettingsControl;
        private System.Windows.Forms.TabPage regionsTabPage;
        private System.Windows.Forms.TabPage platformsTabPage;
        private ListBoxSettingsControl regionListBoxSettingsControl;
        private ListBoxSettingsControl platformListBoxSettingsControl;
        private System.Windows.Forms.Button generalBrowseTextExporterPluginButton;
        private System.Windows.Forms.TextBox generalTextExporterPluginTextBox;
        private System.Windows.Forms.Label generalTextExporterPluginLabel;
        private System.Windows.Forms.HelpProvider helpProvider;
    }
}