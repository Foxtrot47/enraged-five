using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

using RSG.Base.Serialization;

namespace LocalizedTextControls
{
    /// <summary>
    /// A structure that holds all of the data associated with a variable that can be added to a piece of localized text.
    /// </summary>
    [Serializable]
    public class LocalizedTextPlatformOption : IRageClonableObject
    {
        public LocalizedTextPlatformOption()
        {

        }

        public LocalizedTextPlatformOption( string name )
        {
            m_name = name;
        }

        #region Variables
        private string m_name = string.Empty;
        #endregion

        #region Properties
        /// <summary>
        /// The name of the platform.
        /// </summary>
        [Description( "The name of the platform." ), Category( "Required" ), DefaultValue( "" )]
        public string Name
        {
            get
            {
                return m_name;
            }
            set
            {
                if ( m_name != null )
                {
                    m_name = value;
                }
                else
                {
                    m_name = string.Empty;
                }
            }
        }
        #endregion

        #region Overrides
        public override bool Equals( object obj )
        {
            if ( obj == null )
            {
                return false;
            }

            if ( base.Equals( obj ) )
            {
                return true;
            }

            if ( obj is LocalizedTextPlatformOption )
            {
                return Equals( obj as LocalizedTextPlatformOption );
            }

            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString()
        {
            return this.Name;
        }
        #endregion

        #region IRageClonableObject
        public object Clone()
        {
            LocalizedTextPlatformOption option = new LocalizedTextPlatformOption();
            option.CopyFrom( this );
            return option;
        }

        public void CopyFrom( IRageClonableObject other )
        {
            if ( other == null )
            {
                return;
            }

            if ( base.Equals( (object)other ) )
            {
                return;
            }

            if ( other is LocalizedTextPlatformOption )
            {
                CopyFrom( other as LocalizedTextPlatformOption );
            }
        }

        public void Reset()
        {
            this.Name = string.Empty;
        }
        #endregion

        #region Public Functions
        public void CopyFrom( LocalizedTextPlatformOption other )
        {
            if ( other == null )
            {
                return;
            }

            this.Name = other.Name;
        }

        public bool Equals( LocalizedTextPlatformOption other )
        {
            if ( other == null )
            {
                return false;
            }

            if ( base.Equals( (object)other ) )
            {
                return true;
            }

            return (this.Name == other.Name);
        }
        #endregion
    }

    /// <summary>
    /// A structure that contains a list of LocalizedTextPlatformOption items and can serialize the collection.
    /// </summary>
    [Serializable]
    public class LocalizedTextPlatformOptionCollection : rageSerializableList<LocalizedTextPlatformOption>
    {
        #region IRageClonableObject
        public override object Clone()
        {
            LocalizedTextPlatformOptionCollection collection = new LocalizedTextPlatformOptionCollection();
            collection.CopyFrom( collection );
            return collection;
        }
        #endregion
    }
}
