using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace LocalizedTextControls
{
    public partial class PreviewDialog : Form
    {
        public PreviewDialog()
        {
            InitializeComponent();

            UpdatePictureBoxSizeAndLocation();
        }

        #region Variables
        private Size c_defaultResolution = new Size( 1280, 720 );

        private Size m_resolution = Size.Empty;
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the HelpNamespace for the dialog.
        /// </summary>
        [DefaultValue( null ), Description( "Gets or sets the HelpNamespace for the dialog." )]
        public string HelpNamespace
        {
            get
            {
                return this.helpProvider.HelpNamespace;
            }
            set
            {
                this.helpProvider.HelpNamespace = value;
            }
        }

        public Bitmap Bitmap
        {
            set
            {
                this.pictureBox.Image = value;

                if ( value != null )
                {
                    if ( m_resolution != value.Size )
                    {
                        m_resolution = value.Size;

                        this.Size = m_resolution;
                        
                        UpdatePictureBoxSizeAndLocation();
                    }
                }
            }
        }
        #endregion

        #region Event Handlers
        private void panel_ClientSizeChanged( object sender, EventArgs e )
        {
            UpdatePictureBoxSizeAndLocation();
        }
        #endregion

        #region Private Functions
        private void UpdatePictureBoxSizeAndLocation()
        {
            // set the preview panel's size and location
            try
            {
                Size resolution = m_resolution;
                if ( resolution == Size.Empty )
                {
                    resolution = c_defaultResolution;
                }

                Size parentSize = this.panel.ClientSize;

                // Based on resolution enum, determine which aspect ratio to use
                float aspectRatio = (float)resolution.Width / (float)resolution.Height;

                // Maximize width and height while maintaining its aspect ratio
                int width = Convert.ToInt32( parentSize.Height * aspectRatio );
                int height = Convert.ToInt32( parentSize.Width / aspectRatio );
                if ( (parentSize.Width > parentSize.Height) && (width <= parentSize.Width) )
                {
                    height = parentSize.Height;
                }
                else
                {
                    width = parentSize.Width;
                }

                this.pictureBox.Size = new Size( width, height );

                // Position the window in the center of the dock window
                int x = (parentSize.Width / 2) - (this.pictureBox.Width / 2);
                int y = (parentSize.Height / 2) - (this.pictureBox.Height / 2);
                this.pictureBox.Location = new Point( x, y );
            }
            catch
            {

            }
        }
        #endregion
    }
}