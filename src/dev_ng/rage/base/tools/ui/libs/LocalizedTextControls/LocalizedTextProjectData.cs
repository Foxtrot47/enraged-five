using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Text;
using System.Windows.Forms;

using RSG.Base.Serialization;

namespace LocalizedTextControls
{
    public class LocalizedTextProjectData : rageSerializableObject
    {
        public LocalizedTextProjectData()
        {
            Reset();
        }

        #region Variables
        private string m_localizedTextOptionsFilename = string.Empty;
        private LocalizedTextDataCollection m_textDataCollection = new LocalizedTextDataCollection();
        private LocalizedTextExporterSettings m_exporterSettings = new LocalizedTextExporterSettings();
        #endregion

        #region Properties
        /// <summary>
        /// The name of the game-specific LocalizedTextOptions file to use for this Localized Text Project.
        /// </summary>
        [DefaultValue( "" ), Description( "The name of the game-specific LocalizedTextOptions file to use for this Subtitle Project." )]
        public string LocalizedTextOptionsFilename
        {
            get
            {
                return m_localizedTextOptionsFilename;
            }
            set
            {
                if ( value != null )
                {
                    m_localizedTextOptionsFilename = value;
                }
                else
                {
                    m_localizedTextOptionsFilename = string.Empty;
                }
            }
        }

        /// <summary>
        /// The collection of LocalizedTextData items.
        /// </summary>
        [Description( "The collection of LocalizedTextData items." )]
        public LocalizedTextDataCollection TextDataCollection
        {
            get
            {
                return m_textDataCollection;
            }
            set
            {
                if ( value != null )
                {
                    m_textDataCollection = value;
                }
            }
        }

        /// <summary>
        /// The collection of settings that specify how to export the localized text.
        /// </summary>
        [Description( "The collection of settings that specify how to export the localized text." )]
        public LocalizedTextExporterSettings ExporterSettings
        {
            get
            {
                return m_exporterSettings;
            }
            set
            {
                if ( value != null )
                {
                    m_exporterSettings = value;
                }
                else
                {
                    m_exporterSettings.Reset();
                }
            }
        }
        #endregion

        #region Overrides
        public override bool Equals( object obj )
        {
            if ( obj == null )
            {
                return false;
            }

            if ( base.Equals( obj ) )
            {
                return true;
            }

            if ( obj is LocalizedTextProjectData )
            {
                return Equals( obj as LocalizedTextProjectData );
            }

            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override object Clone()
        {
            LocalizedTextProjectData data = new LocalizedTextProjectData();
            data.CopyFrom( this );
            return data;
        }

        public override void CopyFrom( IRageClonableObject other )
        {
            if ( other == null )
            {
                return;
            }

            if ( base.Equals( (object)other ) )
            {
                return;
            }

            if ( other is LocalizedTextProjectData )
            {
                CopyFrom( other as LocalizedTextProjectData );
            }
        }

        public override void Reset()
        {
            base.Reset();

            this.ExporterSettings.Reset();
            this.LocalizedTextOptionsFilename = string.Empty;            
            this.TextDataCollection.Reset();            
        }
        #endregion

        #region Public Functions
        public void CopyFrom( LocalizedTextProjectData other )
        {
            if ( other == null )
            {
                return;
            }

            if ( base.Equals( (object)other ) )
            {
                return;
            }

            base.CopyFrom( other as rageSerializableObject );

            this.ExporterSettings.CopyFrom( other.ExporterSettings );
            this.LocalizedTextOptionsFilename = other.LocalizedTextOptionsFilename;
            this.TextDataCollection.CopyFrom( other.TextDataCollection );

            string filename = Path.Combine( Path.GetDirectoryName( Application.ExecutablePath ), "LocalizedTextOptions.txtproj.options" );
            if ( File.Exists( filename ) )
            {
                this.LocalizedTextOptionsFilename = filename;
            }
        }

        public bool Equals( LocalizedTextProjectData other )
        {
            if ( other == null )
            {
                return false;
            }

            if ( base.Equals( other as rageSerializableObject ) )
            {
                return true;
            }

            return (this.LocalizedTextOptionsFilename == other.LocalizedTextOptionsFilename)
                && this.ExporterSettings.Equals( other.ExporterSettings )
                && this.TextDataCollection.Equals( other.TextDataCollection );
        }
        #endregion
    }
}
