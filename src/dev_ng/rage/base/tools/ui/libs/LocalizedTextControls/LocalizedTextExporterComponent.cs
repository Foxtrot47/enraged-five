using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Threading;
using System.Windows.Forms;

using RSG.Base.Command;

namespace LocalizedTextControls
{
    public partial class LocalizedTextExporterComponent : Component
    {
        public LocalizedTextExporterComponent()
        {
            InitializeComponent();
        }

        public LocalizedTextExporterComponent( IContainer container )
        {
            container.Add( this );

            InitializeComponent();
        }

        #region Variables
        private LocalizedTextProjectData m_projectData = new LocalizedTextProjectData();
        private LocalizedTextOptions m_options = new LocalizedTextOptions();

        private int m_numProgressSteps = 0;
        private int m_currentProgressStep = 0;
        #endregion

        #region Properties
        [Category( "Data" ), DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public LocalizedTextProjectData LocalizedTextProjectData
        {
            get
            {
                return m_projectData;
            }
            set
            {
                if ( value != null )
                {
                    m_projectData = value;
                }
                else
                {
                    m_projectData.Reset();
                }
            }
        }

        [Category( "Data" ), DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public LocalizedTextOptions LocalizedTextOptions
        {
            get
            {
                return m_options;
            }
            set
            {
                if ( value != null )
                {
                    m_options = value;
                }
                else
                {
                    m_options.Reset();
                }
            }
        }

        [Browsable( false ), DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public bool IsBusy
        {
            get
            {
                return this.backgroundWorker.IsBusy;
            }
        }
        #endregion

        #region Events
        /// <summary>
        /// Occurs when the export begins.
        /// </summary>
        [Description( "Occurs when the export begins." ), Category( "Action" )]
        public event EventHandler ExportBegin;

        /// <summary>
        /// Occurs when the progress of the export is updated.
        /// </summary>
        [Description( "Occurs when the progress of the export is updated." ), Category( "Action" )]
        public event ProgressChangedEventHandler ProgressChanged;

        /// <summary>
        /// Occurs when the export wants to display a message.
        /// </summary>
        [Description( "Occurs when the export wants to display a message." ), Category( "Action" )]
        public event ExportMessageEventHandler ExportMessage;

        /// <summary>
        /// Occurs when the export finishes.
        /// </summary>
        [Description( "Occurs when the export finishes." ), Category( "Action" )]
        public event RunWorkerCompletedEventHandler ExportComplete;

        /// <summary>
        /// Dispatched when a file should be checked out from source control.
        /// </summary>
        [Description( "Dispatched when a file should be checked out from source control." ), Category( "Action" )]
        public event SourceControlEventHandler CheckOutFile;
        #endregion

        #region Event Dispatchers
        protected void OnExportBegin()
        {
            if ( this.ExportBegin != null )
            {
                this.ExportBegin( this, EventArgs.Empty );
            }
        }

        protected void OnProgressChanged( ProgressChangedEventArgs e )
        {
            if ( this.ProgressChanged != null )
            {
                this.ProgressChanged( this, e );
            }
        }

        protected void OnExportMessage( ExportMessageEventArgs e )
        {
            if ( this.ExportMessage != null )
            {
                this.ExportMessage( this, e );
            }
        }

        protected void OnExportComplete( RunWorkerCompletedEventArgs e )
        {
            if ( this.ExportComplete != null )
            {
                this.ExportComplete( this, e );
            }
        }

        protected void OnCheckOutFile( SourceControlEventArgs e )
        {
            if ( this.CheckOutFile != null )
            {
                this.CheckOutFile( this, e );
            }
        }
        #endregion

        #region Event Handlers
        private void backgroundWorker_DoWork( object sender, DoWorkEventArgs e )
        {
            OnExportBegin();

            string projectName = Path.GetFileNameWithoutExtension( this.LocalizedTextProjectData.CurrentFilename );
            string message = "Exporting...";
            if ( !String.IsNullOrEmpty( projectName ) )
            {
                message = String.Format( "Exporting {0}...", projectName );                
            }

            ReportProgress( 0, message );
            ReportMessage( message );

            Thread.Sleep( 10 );

            if ( e.Cancel )
            {
                e.Result = null;
                return;
            }

            string stringTableTextFilename = Path.Combine( this.LocalizedTextProjectData.ExporterSettings.StringTableTextOutputPath, projectName );
            stringTableTextFilename += ".txt";
            stringTableTextFilename = Path.Combine( Path.GetDirectoryName( this.LocalizedTextProjectData.CurrentFilename ), stringTableTextFilename );
            stringTableTextFilename = this.LocalizedTextProjectData.GetRelativePath( stringTableTextFilename );
            stringTableTextFilename = this.LocalizedTextProjectData.GetAbsolutePath( stringTableTextFilename );

            int errors = ExportStringTableText( stringTableTextFilename );
            if ( errors != 0 )
            {
                e.Result = errors;
                return;
            }

            if ( this.LocalizedTextProjectData.ExporterSettings.MakeStringTable )
            {
                if ( e.Cancel )
                {
                    e.Result = -1;
                    return;
                }

                string makeStringTableExecutableFilename = Path.GetFullPath( Path.Combine( Path.GetDirectoryName( Application.ExecutablePath ),
                    this.LocalizedTextProjectData.ExporterSettings.MakeStringTableExecutableFilename.Replace( '/', '\\' ) ) );

                string stringTableFilename = Path.Combine( this.LocalizedTextProjectData.ExporterSettings.StringTableOutputPath, projectName );
                stringTableFilename += ".strtbl";
                stringTableFilename = Path.Combine( Path.GetDirectoryName( this.LocalizedTextProjectData.CurrentFilename ), stringTableFilename );
                stringTableFilename = this.LocalizedTextProjectData.GetRelativePath( stringTableFilename );
                stringTableFilename = this.LocalizedTextProjectData.GetAbsolutePath( stringTableFilename );

                errors = ExportStringTable( makeStringTableExecutableFilename, stringTableTextFilename, stringTableFilename );
            }

            e.Result = errors;
        }

        private void backgroundWorker_ProgressChanged( object sender, ProgressChangedEventArgs e )
        {
            OnProgressChanged( e );
        }

        private void backgroundWorker_RunWorkerCompleted( object sender, RunWorkerCompletedEventArgs e )
        {
            OnExportComplete( e );
        }

        private void exporter_ProgressChanged( object sender, ProgressChangedEventArgs e )
        {
            ReportProgress( e.ProgressPercentage, null );

            if ( e.UserState != null )
            {
                ReportMessage( e.UserState.ToString() );
            }
        }
        #endregion

        #region Public Functions
        /// <summary>
        /// Begins exporting the given project asynchronously.
        /// </summary>
        public void Export()
        {
            if ( this.backgroundWorker.IsBusy )
            {
                return;
            }

            m_numProgressSteps = 1;
            if ( this.LocalizedTextProjectData.ExporterSettings.MakeStringTable )
            {
                ++m_numProgressSteps;
            }

            m_currentProgressStep = 0;

            this.backgroundWorker.RunWorkerAsync();
        }

        public void Cancel()
        {
            if ( this.backgroundWorker.IsBusy )
            {
                this.backgroundWorker.CancelAsync();
            }
        }
        #endregion

        #region Private Functions
        private void ReportProgress( int percent, string message )
        {
            float range = 100.0f / (float)m_numProgressSteps;
            float batchProgress = ((float)percent / 100.0f) * range;
            if ( m_currentProgressStep > 0 )
            {
                batchProgress += (m_currentProgressStep * range);
            }

            if ( batchProgress < 0.0f )
            {
                batchProgress = 0.0f;
            }
            else if ( batchProgress > 100.0f )
            {
                batchProgress = 100.0f;
            }

            this.backgroundWorker.ReportProgress( (int)batchProgress, message );
        }

        private void ReportMessage( string message )
        {
            OnExportMessage( new ExportMessageEventArgs( message ) );
        }

        private int ExportStringTableText( string stringTableTextFilename )
        {
            string message = String.Format( "Exporting {0}...", stringTableTextFilename );
            ReportProgress( 0, message );
            ReportMessage( message );

            string directory = Path.GetDirectoryName( stringTableTextFilename );
            if ( !Directory.Exists( directory ) )
            {
                Directory.CreateDirectory( directory );
            }

            ILocalizedTextExporter exporter = this.LocalizedTextOptions.CreateTextExporter();
            if ( exporter == null )
            {
                ReportMessage( "Unable to create the Localized Text Exporter object." );
                return 1;
            }

            exporter.ProgressChanged += new ProgressChangedEventHandler( exporter_ProgressChanged );

            if ( File.Exists( stringTableTextFilename ) )
            {
                SourceControlEventArgs e = new SourceControlEventArgs( stringTableTextFilename );
                OnCheckOutFile( e );
                if ( e.Result == DialogResult.Abort )
                {
                    return 1;
                }
            }

            rageStatus status;

            // validation
            int errors = 0;
            foreach ( LocalizedTextData data in this.LocalizedTextProjectData.TextDataCollection )
            {
                this.LocalizedTextProjectData.TextDataCollection.ValidateVariableUsage( data, out status );
                if ( !status.Success() )
                {
                    ReportMessage( status.ErrorString );
                    ++errors;
                }
            }

            if ( errors > 0 )
            {
                ReportMessage( "One or more circular dependencies were found.  Please resolve them and export again." );
                return errors;
            }

            errors = exporter.Export( stringTableTextFilename, this.LocalizedTextProjectData.TextDataCollection, 
                this.LocalizedTextOptions, out status );
            if ( !status.Success() )
            {
                ReportMessage( status.ErrorString );
                return errors;
            }

            message = String.Format( "Saved {0}.", stringTableTextFilename );
            ReportProgress( 100, message );
            ++m_currentProgressStep;
            
            ReportMessage( message );

            return 0;
        }

        private int ExportStringTable( string makeStringTableExecutableFilename, string stringTableTextFilename, string stringTableFilename )
        {
            string message = String.Format( "Exporting {0}...", stringTableFilename );
            ReportProgress( 0, message );
            ReportMessage( message );

            string directory = Path.GetDirectoryName( stringTableFilename );
            if ( !Directory.Exists( directory ) )
            {
                Directory.CreateDirectory( directory );
            }

            if ( File.Exists( stringTableFilename ) )
            {
                SourceControlEventArgs e = new SourceControlEventArgs( stringTableFilename );
                OnCheckOutFile( e );
                if ( e.Result == DialogResult.Abort )
                {
                    return 1;
                }
            }

            rageExecuteCommand command = new rageExecuteCommand();
            command.Command = Path.GetFileName( makeStringTableExecutableFilename );
            command.Arguments = String.Format( "-in \"{0}\" -out \"{1}\" -nopause -nopopups", stringTableTextFilename, stringTableFilename );
            command.EchoToConsole = false;
            command.LogToHtmlFile = false;
            command.TimeOutInSeconds = 60.0f;
            command.TimeOutWithoutOutputInSeconds = 60.0f;
            command.UseBusySpinner = false;
            command.WorkingDirectory = Path.GetDirectoryName( makeStringTableExecutableFilename );

            ReportMessage( command.GetDosCommand() );

            rageStatus status;
            command.Execute( out status );

            int errors = 0;
            List<string> log = command.GetLogAsArray();
            foreach ( string line in log )
            {
                ReportMessage( line );

                string uppercaseLine = line.ToUpper();
                if ( uppercaseLine.Contains( "ERROR" ) )
                {
                    ++errors;
                }
            }

            if ( !status.Success() )
            {
                ReportMessage( status.ErrorString );
                return errors;
            }

            message = String.Format( "Exported {0}.", stringTableFilename );
            ReportProgress( 100, message );
            ++m_currentProgressStep;

            ReportMessage( message );

            return 0;
        }
        #endregion
    }
}
