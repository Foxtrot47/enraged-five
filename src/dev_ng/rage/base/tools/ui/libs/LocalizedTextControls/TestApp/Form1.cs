using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;

using rageUsefulCSharpToolClasses;
using LocalizedTextControls;

namespace TestApp
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            InitializeAdditionalComponents();
        }

        private void InitializeAdditionalComponents()
        {
            this.localizedTextEditorControl1.LocalizedTextOptions.FlagOptionCollection.Add( new LocalizedTextFlagOption( "One" ) );
            this.localizedTextEditorControl1.LocalizedTextOptions.FlagOptionCollection.Add( new LocalizedTextFlagOption( "Two" ) );
            this.localizedTextEditorControl1.LocalizedTextOptions.FlagOptionCollection.Add( new LocalizedTextFlagOption( "Three" ) );
            this.localizedTextEditorControl1.LocalizedTextOptions.FlagOptionCollection.Add( new LocalizedTextFlagOption( "Four" ) );

            this.localizedTextEditorControl1.LocalizedTextOptions.FontOptionCollection.Add( new LocalizedTextFontOption( "Arial" ) );
            this.localizedTextEditorControl1.LocalizedTextOptions.FontOptionCollection.Add( new LocalizedTextFontOption( "Times New Roman" ) );
            this.localizedTextEditorControl1.LocalizedTextOptions.FontOptionCollection.Add( new LocalizedTextFontOption( "Courier New" ) );
            this.localizedTextEditorControl1.LocalizedTextOptions.DefaultFontOptionIndex = 0;

            this.localizedTextEditorControl1.LocalizedTextOptions.LanguageOptionCollection.Add( new LocalizedTextLanguageOption( "English", "En" ) );
            this.localizedTextEditorControl1.LocalizedTextOptions.LanguageOptionCollection.Add( new LocalizedTextLanguageOption( "French", "Fr" ) );
            this.localizedTextEditorControl1.LocalizedTextOptions.LanguageOptionCollection.Add( new LocalizedTextLanguageOption( "German", "Gr" ) );
            this.localizedTextEditorControl1.LocalizedTextOptions.DefaultLanguageOptionIndex = 0;

            this.localizedTextEditorControl1.LocalizedTextOptions.VariableOptionCollection.Add( 
                new LocalizedTextVariableOption( "time", "Game Variable", "The current game time." ) );
            this.localizedTextEditorControl1.LocalizedTextOptions.VariableOptionCollection.Add(
                new LocalizedTextVariableOption( "score", "Game Variable", "The current score." ) );
            this.localizedTextEditorControl1.LocalizedTextOptions.VariableOptionCollection.Add(
                new LocalizedTextVariableOption( "name", "Game Variable", "The player's name." ) );

            this.localizedTextExporterComponent1.LocalizedTextProjectData.CurrentFilename = "c:\\test.xml";
            this.localizedTextExporterComponent1.LocalizedTextProjectData.ExporterSettings.MakeStringTable = true;
            this.localizedTextExporterComponent1.LocalizedTextOptions = this.localizedTextEditorControl1.LocalizedTextOptions;
        }

        #region Variables
        private LocalizedTextDataCollection m_dataCollection = new LocalizedTextDataCollection();

        private string m_lastProgressMessage = string.Empty;
        #endregion

        private void addButton_Click( object sender, EventArgs e )
        {
            this.localizedTextEditorControl1.Add();
        }

        private void deleteButton_Click( object sender, EventArgs e )
        {
            this.localizedTextEditorControl1.RemoveSelected();
        }

        private void localizedTextEditorControl1_TextDataAdded( object sender, LocalizedTextDataIndexEventArgs e )
        {
            if ( e.Index == m_dataCollection.Count )
            {
                m_dataCollection.Add( e.LocalizedTextData );
            }
            else
            {
                m_dataCollection.Insert( e.Index, e.LocalizedTextData );
            }

            Console.WriteLine( "Added {0} at position {1}", e.LocalizedTextData.ToString(), 
                m_dataCollection.IndexOf( e.LocalizedTextData ) );
        }

        private void localizedTextEditorControl1_TextDataChanged( object sender, LocalizedTextControls.LocalizedTextDataChangedEventArgs e )
        {
            Console.WriteLine( "{0} changed {1}", e.LocalizedTextData.ToString(), e.Member.ToString() );
        }

        private void localizedTextEditorControl1_TextDataMoved( object sender, LocalizedTextDataMovedEventArgs e )
        {
            int indexOf = m_dataCollection.IndexOf( e.LocalizedTextData );
            Debug.Assert( indexOf == e.OldIndex );

            m_dataCollection.RemoveAt( e.OldIndex );
            m_dataCollection.Insert( e.Index, e.LocalizedTextData );

            Console.WriteLine( "{0} moved from {1} to {2}", e.LocalizedTextData.ToString(), e.OldIndex, 
                m_dataCollection.IndexOf( e.LocalizedTextData ) );
        }

        private void localizedTextEditorControl1_TextDataRemoved( object sender, LocalizedTextDataIndexEventArgs e )
        {
            int indexOf = m_dataCollection.IndexOf( e.LocalizedTextData );
            Debug.Assert( indexOf == e.Index );

            m_dataCollection.RemoveAt( e.Index );

            Console.WriteLine( "Removed {0}", e.LocalizedTextData.ToString() );
        }

        private void localizedTextEditorControl1_ModifiedChanged( object sender, EventArgs e )
        {
            Console.WriteLine( "IsModified = {0}", this.localizedTextEditorControl1.IsModified );
        }

        private void exportButton_Click( object sender, EventArgs e )
        {
            this.localizedTextExporterComponent1.LocalizedTextProjectData.TextDataCollection = this.localizedTextEditorControl1.TextDataCollection;
            this.localizedTextExporterComponent1.Export();
        }

        private void localizedTextExporterComponent1_CheckOutFile( object sender, SourceControlEventArgs e )
        {
            Console.WriteLine( "[EXPORT] Check Out {0}", e.Filename );
            e.Result = DialogResult.OK;
        }

        private void localizedTextExporterComponent1_ExportBegin( object sender, EventArgs e )
        {
            m_lastProgressMessage = string.Empty;

            Console.WriteLine( "[EXPORT] Export Begin" );
        }

        private void localizedTextExporterComponent1_ExportComplete( object sender, RunWorkerCompletedEventArgs e )
        {
            if ( e.Cancelled )
            {
                Console.WriteLine( "[EXPORT] Cancelled" );
            }
            else if ( (int)e.Result == 0 )
            {
                Console.WriteLine( "[EXPORT] Success!" );
            }
            else if ( e.Error != null )
            {
                Console.WriteLine( "[EXPORT] Error:  {0}", e.Error.ToString() );
            }
            else
            {
                Console.WriteLine( "[EXPORT] Error" );
            }
        }

        private void localizedTextExporterComponent1_ExportMessage( object sender, ExportMessageEventArgs e )
        {
            Console.WriteLine( "[EXPORT] {0}", e.Message );
        }

        private void localizedTextExporterComponent1_ProgressChanged( object sender, ProgressChangedEventArgs e )
        {
            if ( e.UserState != null )
            {
                m_lastProgressMessage = e.UserState.ToString();
            }

            Console.WriteLine( "[EXPORT] {0}% - {1}", e.ProgressPercentage, m_lastProgressMessage );
        }
    }
}