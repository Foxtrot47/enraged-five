using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Text;

using RSG.Base.Command;
using RSG.Base.Serialization;

namespace LocalizedTextControls
{
    /// <summary>
    /// A structure that holds all of the data associated with a piece of localized text.
    /// </summary>
    [Serializable]
    public class LocalizedTextData : IRageClonableObject
    {
        public LocalizedTextData()
        {

        }

        #region Enums
        public enum DataMember
        {
            Identifier,
            Language,
            Region,
            Platform,
            Speaker,
            String,
            Uppercase,
            Font,
            ScaleX,
            ScaleY,
            OffsetX,
            OffsetY,
            Flags,
            Comment
        }
        #endregion

        #region Variables
        private string m_id = string.Empty;
        private string m_fontName = string.Empty;
        private string m_language = string.Empty;
        private string m_platform = string.Empty;
        private string m_region = string.Empty;
        private string m_speaker = string.Empty;
        private string m_text = string.Empty;
        private string m_rtf = string.Empty;
        private bool m_uppercase = false;
        private float m_scaleX = 1.0f;
        private float m_scaleY = 1.0f;
        private int m_offsetX = 0;
        private int m_offsetY = 0;
        private uint m_flags = 1;
        private string m_comment = string.Empty;
        #endregion

        #region Properties
        /// <summary>
        /// The unique identifier.
        /// </summary>
        [Description( "The unique identifier." )]
        public string ID
        {
            get
            {
                return m_id;
            }
            set
            {
                m_id = value;
            }
        }

        /// <summary>
        /// The name of the Font to use for displaying the text.
        /// </summary>
        [Description( "The name of the Font to use for displaying the text." )]
        public string FontName
        {
            get
            {
                return m_fontName;
            }
            set
            {
                if ( value != null )
                {
                    m_fontName = value;
                }
                else
                {
                    m_fontName = string.Empty;
                }
            }
        }

        /// <summary>
        /// The Language that this text data belongs to.
        /// </summary>
        [Description( "The Language that this text data belongs to." )]
        public string Language
        {
            get
            {
                return m_language;
            }
            set
            {
                if ( value != null )
                {
                    m_language = value;
                }
                else
                {
                    m_language = string.Empty;
                }
            }
        }

        /// <summary>
        /// The Platform that this text data belongs to.
        /// </summary>
        [Description( "The Platform that this text data belongs to." )]
        public string Platform
        {
            get
            {
                return m_platform;
            }
            set
            {
                if ( value != null )
                {
                    m_platform = value;
                }
                else
                {
                    m_platform = string.Empty;
                }
            }
        }

        /// <summary>
        /// The Region that this text data belongs to.
        /// </summary>
        [Description( "The Region that this text data belongs to." )]
        public string Region
        {
            get
            {
                return m_region;
            }
            set
            {
                if ( value != null )
                {
                    m_region = value;
                }
                else
                {
                    m_region = string.Empty;
                }
            }
        }

        /// <summary>
        /// The character who is speaking, if any.
        /// </summary>
        [Description( "The character who is speaking, if any." )]
        public string Speaker
        {
            get
            {
                return m_speaker;
            }
            set
            {
                if ( value != null )
                {
                    m_speaker = value;
                }
                else
                {
                    m_speaker = string.Empty;
                }
            }
        }

        /// <summary>
        /// The plain text.
        /// </summary>
        [Description( "The plain text." )]
        public string Text
        {
            get
            {
                return m_text;
            }
            set
            {
                if ( value != null )
                {
                    m_text = value;
                }
                else
                {
                    m_text = string.Empty;
                }
            }
        }

        /// <summary>
        /// The rich text.
        /// </summary>
        [Description( "The rich text." )]
        public string Rtf
        {
            get
            {
                return m_rtf;
            }
            set
            {
                if ( value != null )
                {
                    m_rtf = value;
                }
                else
                {
                    m_rtf = string.Empty;
                }
            }
        }

        /// <summary>
        /// Whether or not to force the text to be uppercase.
        /// </summary>
        [Description( "Whether or not to force the text to be uppercase." )]
        public bool Uppercase
        {
            get
            {
                return m_uppercase;
            }
            set
            {
                m_uppercase = value;
            }
        }

        /// <summary>
        /// The horizontal scale to apply to the text when drawn on screen.
        /// </summary>
        [Description( "The horizontal scale to apply to the text when drawn on screen." )]
        public float ScaleX
        {
            get
            {
                return m_scaleX;
            }
            set
            {
                m_scaleX = value;
            }
        }

        /// <summary>
        /// The vertical scale to apply to the text when drawn on screen.
        /// </summary>
        [Description( "The vertical scale to apply to the text when drawn on screen." )]
        public float ScaleY
        {
            get
            {
                return m_scaleY;
            }
            set
            {
                m_scaleY = value;
            }
        }

        /// <summary>
        /// The horizontal offset to apply to the text when drawn on screen.
        /// </summary>
        [Description( "The horizontal offset to apply to the text when drawn on screen." )]
        public int OffsetX
        {
            get
            {
                return m_offsetX;
            }
            set
            {
                m_offsetX = value;
            }
        }

        /// <summary>
        /// The vertical offset to apply to the text when drawn on screen.
        /// </summary>
        [Description( "The vertical offset to apply to the text when drawn on screen." )]
        public int OffsetY
        {
            get
            {
                return m_offsetY;
            }
            set
            {
                m_offsetY = value;
            }
        }

        /// <summary>
        /// A bitmask of game-specific flags.
        /// </summary>
        [Description( "A bitmask of game-specific flags." )]
        public uint Flags
        {
            get
            {
                return m_flags;
            }
            set
            {
                m_flags = value;
            }
        }

        /// <summary>
        /// Information about this text data.
        /// </summary>
        [Description( "Information about this text data." )]
        public string Comment
        {
            get
            {
                return m_comment;
            }
            set
            {
                if ( value != null )
                {
                    m_comment = value;
                }
                else
                {
                    m_comment = string.Empty;
                }
            }
        }
        #endregion

        #region Overrides
        public override bool Equals( object obj )
        {
            if ( obj == null )
            {
                return false;
            }

            if ( base.Equals( obj ) )
            {
                return true;
            }

            if ( obj is LocalizedTextData )
            {
                return Equals( obj as LocalizedTextData );
            }

            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString()
        {
            return String.Format( "{0}: {1}: {2}", this.ID, this.Language, this.Text );
        }
        #endregion

        #region IRageClonableObject
        public object Clone()
        {
            LocalizedTextData data = new LocalizedTextData();
            data.CopyFrom( this );
            return data;
        }

        public void CopyFrom( IRageClonableObject other )
        {
            if ( other == null )
            {
                return;
            }

            if ( base.Equals( (object)other ) )
            {
                return;
            }

            if ( other is LocalizedTextData )
            {
                CopyFrom( other as LocalizedTextData );
            }
        }

        public void Reset()
        {
            this.Comment = string.Empty;
            this.Flags = 1;
            this.FontName = string.Empty;
            this.ID = string.Empty;
            this.Language = string.Empty;
            this.Platform = string.Empty;
            this.OffsetX = 0;
            this.OffsetY = 0;
            this.Region = string.Empty;
            this.Rtf = string.Empty;
            this.ScaleX = 1.0f;
            this.ScaleY = 1.0f;
            this.Speaker = string.Empty;
            this.Text = string.Empty;
            this.Uppercase = false;
        }
        #endregion

        #region Public Functions
        public void CopyFrom( LocalizedTextData other )
        {
            if ( other == null )
            {
                return;
            }

            this.Comment = other.Comment;
            this.Flags = other.Flags;
            this.FontName = other.FontName;
            this.ID = other.ID;
            this.Language = other.Language;
            this.Platform = other.Platform;
            this.OffsetX = other.OffsetX;
            this.OffsetY = other.OffsetY;
            this.Region = other.Region;
            this.Rtf = other.Rtf;
            this.ScaleX = other.ScaleX;
            this.ScaleY = other.ScaleY;
            this.Speaker = other.Speaker;
            this.Text = other.Text;
            this.Uppercase = other.Uppercase;
        }

        public bool Equals( LocalizedTextData other )
        {
            if ( other == null )
            {
                return false;
            }

            if ( base.Equals( (object)other ) )
            {
                return true;
            }

            return (this.Comment == other.Comment)
                && (this.Flags == other.Flags)
                && (this.FontName == other.FontName)
                && (this.ID == other.ID)
                && (this.Language == other.Language)
                && (this.Platform == other.Platform)
                && (this.OffsetX == other.OffsetX)
                && (this.OffsetY == other.OffsetY)
                && (this.Region == other.Region)
                && (this.Rtf == other.Rtf)
                && (this.ScaleX == other.ScaleX)
                && (this.ScaleY == other.ScaleY)
                && (this.Speaker == other.Speaker)
                && (this.Text == other.Text)
                && (this.Uppercase == other.Uppercase);
        }

        /// <summary>
        /// Retrieves the string representation of the given DataMember.
        /// </summary>
        /// <param name="member"></param>
        /// <returns></returns>
        public string GetToString( DataMember member )
        {
            string text = string.Empty;
            switch ( member )
            {
                case DataMember.Comment:
                    {
                        text = this.Comment;
                    }
                    break;
                case DataMember.Flags:
                    {
                        LocalizedTextData.ToHexString( this.Flags, ref text );
                    }
                    break;
                case DataMember.Font:
                    {
                        text = this.FontName;
                    }
                    break;
                case DataMember.Identifier:
                    {
                        text = this.ID;
                    }
                    break;
                case DataMember.Language:
                    {
                        text = this.Language;
                    }
                    break;
                case DataMember.Platform:
                    {
                        text = this.Platform;
                    }
                    break;
                case DataMember.OffsetX:
                    {
                        LocalizedTextData.ToIntString( this.OffsetX, ref text );
                    }
                    break;
                case DataMember.OffsetY:
                    {
                        LocalizedTextData.ToIntString( this.OffsetY, ref text );
                    }
                    break;
                case DataMember.Region:
                    {
                        text = this.Region;
                    }
                    break;
                case DataMember.String:
                    {
                        text = this.Rtf;
                    }
                    break;
                case DataMember.ScaleX:
                    {
                        LocalizedTextData.ToFloatString( this.ScaleX, ref text );
                    }
                    break;
                case DataMember.ScaleY:
                    {
                        LocalizedTextData.ToFloatString( this.ScaleY, ref text );
                    }
                    break;
                case DataMember.Speaker:
                    {
                        text = this.Speaker;
                    }
                    break;
                case DataMember.Uppercase:
                    {
                        LocalizedTextData.ToBoolString( this.Uppercase, ref text );
                    }
                    break;
            }

            return text;
        }

        /// <summary>
        /// Sets the given DataMember from its string representation.
        /// </summary>
        /// <param name="member"></param>
        /// <param name="text"></param>
        public void SetFromString( DataMember member, string text )
        {
            switch ( member )
            {
                case DataMember.Comment:
                    {
                        this.Comment = text;
                    }
                    break;
                case DataMember.Flags:
                    {
                        LocalizedTextData.FromHexString( text, ref m_flags );
                    }
                    break;
                case DataMember.Font:
                    {
                        this.FontName = text;
                    }
                    break;
                case DataMember.Identifier:
                    {
                        this.ID = text;
                    }
                    break;
                case DataMember.Language:
                    {
                        this.Language = text;
                    }
                    break;
                case DataMember.Platform:
                    {
                        this.Platform = text;
                    }
                    break;
                case DataMember.OffsetX:
                    {
                        int offsetX = this.OffsetX;
                        LocalizedTextData.FromIntString( text, ref offsetX );
                        this.OffsetX = offsetX;
                    }
                    break;
                case DataMember.OffsetY:
                    {
                        int offsetY = this.OffsetY;
                        LocalizedTextData.FromIntString( text, ref offsetY );
                        this.OffsetY = offsetY;
                    }
                    break;
                case DataMember.Region:
                    {
                        this.Region = text;
                    }
                    break;
                case DataMember.String:
                    {
                        this.Rtf = text;
                    }
                    break;
                case DataMember.ScaleX:
                    {
                        float scaleX = this.ScaleX;
                        LocalizedTextData.FromFloatString( text, ref scaleX );
                        this.ScaleX = scaleX;
                    }
                    break;
                case DataMember.ScaleY:
                    {
                        float scaleY = this.ScaleY;
                        LocalizedTextData.FromFloatString( text, ref scaleY );
                        this.ScaleY = scaleY;
                    }
                    break;
                case DataMember.Speaker:
                    {
                        this.Speaker = text;
                    }
                    break;
                case DataMember.Uppercase:
                    {
                        bool uppercase = this.Uppercase;
                        LocalizedTextData.FromBoolString( text, ref uppercase );
                        this.Uppercase = uppercase;
                    }
                    break;
            }
        }
        #endregion

        #region Public Static Functions
        /// <summary>
        /// Transforms the given boolean value into a formatted string.
        /// </summary>
        /// <param name="val"></param>
        /// <param name="text">The resulting string is passed back by reference.</param>
        /// <returns>true if the conversion was successful, otherwise false.</returns>
        public static bool ToBoolString( bool val, ref string text )
        {
            text = val.ToString();
            return true;
        }

        /// <summary>
        /// Sets the boolean value from the string representation.
        /// </summary>
        /// <param name="text">The input string.</param>
        /// <param name="val">The resulting value is passed back by reference.</param>
        /// <returns>true if the conversion was successful, otherwise false.</returns>
        public static bool FromBoolString( string text, ref bool val )
        {
            string uppercaseString = text.ToUpper();
            if ( (text == bool.FalseString) || (uppercaseString == "F") || (uppercaseString == "FALSE") || (uppercaseString == "0") )
            {
                val = false;
            }
            else
            {
                val = true;
            }

            return true;
        }

        /// <summary>
        /// Transforms the given float value into a formatted string.
        /// </summary>
        /// <param name="val"></param>
        /// <param name="text">The resulting string is passed back by reference.</param>
        /// <returns>true if the conversion was successful, otherwise false.</returns>
        public static bool ToFloatString( float val, ref string text )
        {
            try
            {
                text = String.Format( "{0:0.000}", val );
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Sets the float value from the string representation.
        /// </summary>
        /// <param name="text">The input string.</param>
        /// <param name="val">The resulting value is passed back by reference.</param>
        /// <returns>true if the conversion was successful, otherwise false.</returns>
        public static bool FromFloatString( string text, ref float val )
        {
            return float.TryParse( text, out val );
        }

        /// <summary>
        /// Transforms the given integer value into a formatted string.
        /// </summary>
        /// <param name="val">The input value.</param>
        /// <param name="text">The resulting string is passed back by reference.</param>
        /// <returns>true if the conversion was successful, otherwise false.</returns>
        public static bool ToIntString( int val, ref string text )
        {
            try
            {
                text = String.Format( "{0}", val );
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Sets the integer value from the string representation.
        /// </summary>
        /// <param name="text">The input string.</param>
        /// <param name="val">The resulting value is passed back by reference.</param>
        /// <returns>true if the conversion was successful, otherwise false.</returns>
        public static bool FromIntString( string text, ref int val )
        {
            return int.TryParse( text, out val );
        }

        /// <summary>
        /// Transforms the given unsigned integer value into a formatted string.
        /// </summary>
        /// <param name="val">The input value.</param>
        /// <param name="text">The resulting string is passed back by reference.</param>
        /// <returns>true if the conversion was successful, otherwise false.</returns>
        public static bool ToHexString( uint val, ref string text )
        {
            try
            {
                text = String.Format( "0x{0:X8}", val );
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Sets the unsigned integer value from the string representation.
        /// </summary>
        /// <param name="text">The input string.</param>
        /// <param name="val">The resulting value is passed back by reference.</param>
        /// <returns>true if the conversion was successful, otherwise false.</returns>
        public static bool FromHexString( string text, ref uint val )
        {
            try
            {
                string temp = text.ToUpper();
                if ( temp.StartsWith( "0X" ) )
                {
                    temp = temp.Remove( 0, 2 );

                    val = UInt32.Parse( temp, System.Globalization.NumberStyles.AllowHexSpecifier );
                    return true;
                }
                else
                {
                    val = Convert.ToUInt32( text );
                    return true;
                }
            }
            catch
            {
                return false;
            }
        }
        #endregion
    }

    /// <summary>
    /// A structure that contains a list of LocalizedTextData items and can serialize the collection.
    /// </summary>
    [Serializable]
    public class LocalizedTextDataCollection : rageSerializableList<LocalizedTextData>
    {
        #region IRageClonableObject
        public override object Clone()
        {
            LocalizedTextDataCollection collection = new LocalizedTextDataCollection();
            collection.CopyFrom( collection );
            return collection;
        }
        #endregion

        #region Public Functions
        /// <summary>
        /// Locates the LocalizedTextData object with the given language and ID.
        /// </summary>
        /// <param name="language">The language to look for.</param>
        /// <param name="id">The ID to look for.</param>
        /// <returns>null if not found.</returns>
        public LocalizedTextData Find( string language, string id )
        {
            foreach ( LocalizedTextData data in this )
            {
                if ( (data.Language == language) && (data.ID == id) )
                {
                    return data;
                }
            }

            return null;
        }

        /// <summary>
        /// Detects circular dependencies between LocalizedTextData items.
        /// </summary>
        /// <param name="data">The LocalizedTextData to check.</param>
        /// <param name="status">The result of the validation.</param>
        public void ValidateVariableUsage( LocalizedTextData data, out rageStatus status )
        {
            if ( data.Text.Contains( String.Format( "$({0})", data.ID ) ) )
            {
                status = new rageStatus( String.Format( "Localized Text '{0}' contains its own ID as a variable.", data.ID ) );
                return;
            }

            foreach ( LocalizedTextData checkData in this )
            {
                if ( checkData == data )
                {
                    continue;
                }

                Dictionary<LocalizedTextData, bool> checkedItems1 = new Dictionary<LocalizedTextData, bool>();
                Dictionary<LocalizedTextData, bool> checkedItems2 = new Dictionary<LocalizedTextData, bool>();
                if ( IncludesThisVariable( checkData, data, checkedItems1 ) && IncludesThisVariable( data, checkData, checkedItems2 ) )
                {
                    status = new rageStatus( String.Format( "Circular dependency between '{0}' and '{1}'.", data.ID, checkData.ID ) );
                    return;
                }
            }

            status = new rageStatus();
        }

        /// <summary>
        /// Is includeData a dependency of data? 
        /// </summary>
        /// <param name="data">The data to check the dependencies of.</param>
        /// <param name="includeData">The data to see if it's been included.</param>
        /// <param name="checkedItems">The dictionary accumulating our results (protects against circular dependencies).</param>
        /// <returns>true if data depends on includeData.</returns>
        public bool IncludesThisVariable( LocalizedTextData data, LocalizedTextData includeData, Dictionary<LocalizedTextData, bool> checkedItems )
        {
            if ( data == includeData )
            {
                checkedItems[data] = false;
                return false;
            }

            bool includesIt = false;
            if ( checkedItems.TryGetValue( data, out includesIt ) )
            {
                return includesIt;
            }

            if ( (!String.IsNullOrEmpty( data.Language ) && (data.Language != includeData.Language)) || String.IsNullOrEmpty( includeData.ID ) )
            {
                checkedItems[data] = false;
                return false;
            }

            string var = String.Format( "$({0})", includeData.ID );
            if ( data.Text.Contains( var ) )
            {
                checkedItems[data] = true;
                return true;
            }

            checkedItems[data] = false;

            int startIndex = data.Text.IndexOf( "$(" );
            while ( startIndex != -1 )
            {
                int endIndex = data.Text.IndexOf( ")", startIndex );
                if ( endIndex != -1 )
                {
                    string varName = data.Text.Substring( startIndex + 2, endIndex - startIndex - 2 );
                    LocalizedTextData subData = Find( data.Language, varName );
                    if ( (subData != null) && (subData != data) && !checkedItems.TryGetValue( subData, out includesIt ) )
                    {
                        if ( IncludesThisVariable( subData, includeData, checkedItems ) )
                        {
                            checkedItems[data] = true;
                            return true;
                        }
                    }
                    else if ( includesIt )
                    {
                        checkedItems[data] = true;
                        return true;
                    }
                }

                startIndex = data.Text.IndexOf( "$(", startIndex + 1 );
            }

            return false;
        }

        /// <summary>
        /// Resolve the variables in the LocalizedTextData's text.
        /// </summary>
        /// <param name="text">The text that may contain variables.</param>
        /// <param name="data">The LocalizedTextData that owns the text.</param>
        /// <param name="resolvedItems">Contains items that have aready been resolved.</param>
        /// <returns>The resolved string.</returns>
        public string ResolveVariables( string text, LocalizedTextData data, Dictionary<LocalizedTextData, string> resolvedItems )
        {
            string varValue = null;
            if ( resolvedItems.TryGetValue( data, out varValue ) )
            {
                return varValue;
            }

            // resolve variables, if any
            int startIndex = text.IndexOf( "$(" );
            while ( startIndex != -1 )
            {
                int endIndex = text.IndexOf( ")", startIndex );
                if ( endIndex != -1 )
                {
                    string var = text.Substring( startIndex, endIndex - startIndex + 1 );
                    if ( var.Length > 3 )
                    {
                        string varName = var.Substring( 2, var.Length - 3 );

                        LocalizedTextData varData = Find( data.Language, varName );
                        if ( (varData != null) && !resolvedItems.TryGetValue( varData, out varValue ) )
                        {
                            Dictionary<LocalizedTextData, bool> checkedItems = new Dictionary<LocalizedTextData, bool>();
                            if ( (varName != data.ID) && !IncludesThisVariable( varData, data, checkedItems ) ) // guard against circular dependencies
                            {
                                varValue = ResolveVariables( varData.Text, varData, resolvedItems );

                                text = text.Replace( var, varValue );
                                startIndex += varData.Text.Length - 1;
                            }
                        }
                        else if ( varValue != null )
                        {
                            text = text.Replace( var, varValue );
                            startIndex += varData.Text.Length - 1;
                        }
                    }
                }

                startIndex = text.IndexOf( "$(", startIndex + 1 );
            }

            resolvedItems[data] = text;
            return text;
        }
        #endregion
    }
}
