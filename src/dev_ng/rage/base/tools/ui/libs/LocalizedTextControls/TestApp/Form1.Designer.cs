namespace TestApp
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && (components != null) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            LocalizedTextControls.DefaultLocalizedTextRenderer defaultLocalizedTextRenderer8 = new LocalizedTextControls.DefaultLocalizedTextRenderer();
            this.localizedTextEditorControl1 = new LocalizedTextControls.LocalizedTextEditorControl();
            this.localizedTextEditorDialogComponent1 = new LocalizedTextControls.LocalizedTextEditorDialogComponent( this.components );
            this.addButton = new System.Windows.Forms.Button();
            this.removeButton = new System.Windows.Forms.Button();
            this.localizedTextExporterComponent1 = new LocalizedTextControls.LocalizedTextExporterComponent( this.components );
            this.exportButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // localizedTextEditorControl1
            // 
            this.localizedTextEditorControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.localizedTextEditorControl1.Location = new System.Drawing.Point( 12, 42 );
            this.localizedTextEditorControl1.Name = "localizedTextEditorControl1";
            this.localizedTextEditorControl1.Size = new System.Drawing.Size( 1214, 427 );
            this.localizedTextEditorControl1.TabIndex = 0;
            this.localizedTextEditorControl1.TextRenderer = defaultLocalizedTextRenderer8;
            this.localizedTextEditorControl1.TextDataChanged += new LocalizedTextControls.LocalizedTextDataChangedEventHandler( this.localizedTextEditorControl1_TextDataChanged );
            this.localizedTextEditorControl1.ModifiedChanged += new System.EventHandler( this.localizedTextEditorControl1_ModifiedChanged );
            this.localizedTextEditorControl1.TextDataAdded += new LocalizedTextControls.LocalizedTextDataIndexEventHandler( this.localizedTextEditorControl1_TextDataAdded );
            this.localizedTextEditorControl1.TextDataRemoved += new LocalizedTextControls.LocalizedTextDataIndexEventHandler( this.localizedTextEditorControl1_TextDataRemoved );
            this.localizedTextEditorControl1.TextDataMoved += new LocalizedTextControls.LocalizedTextDataMovedEventHandler( this.localizedTextEditorControl1_TextDataMoved );
            // 
            // localizedTextEditorDialogComponent1
            // 
            this.localizedTextEditorDialogComponent1.LocalizedTextEditorControl = this.localizedTextEditorControl1;
            // 
            // addButton
            // 
            this.addButton.Location = new System.Drawing.Point( 13, 13 );
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size( 75, 23 );
            this.addButton.TabIndex = 1;
            this.addButton.Text = "Add";
            this.addButton.UseVisualStyleBackColor = true;
            this.addButton.Click += new System.EventHandler( this.addButton_Click );
            // 
            // removeButton
            // 
            this.removeButton.Location = new System.Drawing.Point( 95, 13 );
            this.removeButton.Name = "removeButton";
            this.removeButton.Size = new System.Drawing.Size( 75, 23 );
            this.removeButton.TabIndex = 2;
            this.removeButton.Text = "Remove";
            this.removeButton.UseVisualStyleBackColor = true;
            this.removeButton.Click += new System.EventHandler( this.deleteButton_Click );
            // 
            // localizedTextExporterComponent1
            // 
            this.localizedTextExporterComponent1.ExportBegin += new System.EventHandler( this.localizedTextExporterComponent1_ExportBegin );
            this.localizedTextExporterComponent1.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler( this.localizedTextExporterComponent1_ProgressChanged );
            this.localizedTextExporterComponent1.ExportComplete += new System.ComponentModel.RunWorkerCompletedEventHandler( this.localizedTextExporterComponent1_ExportComplete );
            this.localizedTextExporterComponent1.ExportMessage += new LocalizedTextControls.ExportMessageEventHandler( this.localizedTextExporterComponent1_ExportMessage );
            this.localizedTextExporterComponent1.CheckOutFile += new LocalizedTextControls.SourceControlEventHandler( this.localizedTextExporterComponent1_CheckOutFile );
            // 
            // exportButton
            // 
            this.exportButton.Location = new System.Drawing.Point( 340, 13 );
            this.exportButton.Name = "exportButton";
            this.exportButton.Size = new System.Drawing.Size( 75, 23 );
            this.exportButton.TabIndex = 3;
            this.exportButton.Text = "Export";
            this.exportButton.UseVisualStyleBackColor = true;
            this.exportButton.Click += new System.EventHandler( this.exportButton_Click );
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size( 1238, 481 );
            this.Controls.Add( this.exportButton );
            this.Controls.Add( this.removeButton );
            this.Controls.Add( this.addButton );
            this.Controls.Add( this.localizedTextEditorControl1 );
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout( false );

        }

        #endregion

        private LocalizedTextControls.LocalizedTextEditorControl localizedTextEditorControl1;
        private LocalizedTextControls.LocalizedTextEditorDialogComponent localizedTextEditorDialogComponent1;
        private System.Windows.Forms.Button addButton;
        private System.Windows.Forms.Button removeButton;
        private LocalizedTextControls.LocalizedTextExporterComponent localizedTextExporterComponent1;
        private System.Windows.Forms.Button exportButton;
    }
}