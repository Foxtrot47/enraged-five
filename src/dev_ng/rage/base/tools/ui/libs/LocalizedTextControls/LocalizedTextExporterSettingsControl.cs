using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.IO;
using System.Text;
using System.Windows.Forms;

using RSG.Base.Command;

namespace LocalizedTextControls
{
    /// <summary>
    /// A Control for editing the LocalizedTextExporterSettings (part of a LocalizedTextProject).
    /// </summary>
    public partial class LocalizedTextExporterSettingsControl : UserControl
    {
        public LocalizedTextExporterSettingsControl()
        {
            InitializeComponent();
        }

        #region Variables
        private bool m_dispatchEvents = true;
        #endregion

        #region Properties
        /// <summary>
        /// The Application's HelpProvider.  When set, this control's items are added to it.
        /// </summary>
        [DefaultValue( null ), Description( "The Application's HelpProvider.  When set, this control's items are added to it." )]
        public HelpProvider HelpProvider
        {
            set
            {
                if ( value != null )
                {
                    string stringTableTextHelp = "The directory where the .txt file will be saved.  Can be relative to the project file.";
                    value.SetShowHelp( this.stringTableTextOutputFilenameLabel, true );
                    value.SetShowHelp( this.stringTableTextOutputPathTextBox, true );
                    value.SetHelpString( this.stringTableTextOutputFilenameLabel, stringTableTextHelp );
                    value.SetHelpString( this.stringTableTextOutputPathTextBox, stringTableTextHelp );

                    value.SetShowHelp( this.browseStringTableTextFilenameButton, true );
                    value.SetHelpString( this.browseStringTableTextFilenameButton, "Browse for the directory where the .txt file will be saved." );

                    string executableHelp = "The path and filename of the 'mkstrtbl.exe' utility.  Can be relative to the Editor.";
                    value.SetShowHelp( this.executableFilenameLabel, true );
                    value.SetShowHelp( this.executableFilenameTextBox, true );
                    value.SetHelpString( this.executableFilenameLabel, executableHelp );
                    value.SetHelpString( this.executableFilenameTextBox, executableHelp );

                    value.SetShowHelp( this.browseExecutableFilenameButton, true );
                    value.SetHelpString( this.browseExecutableFilenameButton, "Browse for the 'mkstrtbl.exe' utility." );

                    string stringTableHelp = "The directory where the .strtbl file will be saved.  Can be relative to the project file.";
                    value.SetShowHelp( this.stringTableOutputPathLabel, true );
                    value.SetShowHelp( this.stringTableOutputPathTextBox, true );
                    value.SetHelpString( this.stringTableOutputPathLabel, stringTableHelp );
                    value.SetHelpString( this.stringTableOutputPathTextBox, stringTableHelp );

                    value.SetShowHelp( this.browseStringTableOutputPathButton, true );
                    value.SetHelpString( this.browseStringTableOutputPathButton, "Browse for the directory where the .strtbl file will be saved." );
                }
            }
        }

        /// <summary>
        /// The settings to edit and were created/edited.
        /// </summary>
        [Category( "Data" ), Description( "The settings to edit and were created/edited." ), DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public LocalizedTextExporterSettings LocalizedTextExporterSettings
        {
            get
            {
                LocalizedTextExporterSettings settings = new LocalizedTextExporterSettings();
                
                settings.MakeStringTable = this.generateStringTableCheckBox.Checked;

                string executableFilename = this.executableFilenameTextBox.Text.Trim();
                if ( !String.IsNullOrEmpty( executableFilename ) )
                {
                    executableFilename = Path.GetFullPath( Path.Combine( Path.GetDirectoryName( Application.ExecutablePath ), executableFilename ) );
                    executableFilename = LocalizedTextProjectData.GetRelativePath( Path.GetDirectoryName( Application.ExecutablePath ), 
                        executableFilename );
                }
                settings.MakeStringTableExecutableFilename = executableFilename;
                
                settings.StringTableOutputPath = this.stringTableOutputPathTextBox.Text.Trim();
                settings.StringTableTextOutputPath = this.stringTableTextOutputPathTextBox.Text.Trim();

                return settings;
            }
            set
            {
                if ( value != null )
                {
                    m_dispatchEvents = false;

                    this.generateStringTableCheckBox.Checked = value.MakeStringTable;

                    string executableFilename = value.MakeStringTableExecutableFilename;
                    if ( !String.IsNullOrEmpty( executableFilename ) )
                    {
                        executableFilename = Path.GetFullPath( Path.Combine( Path.GetDirectoryName( Application.ExecutablePath ), executableFilename ) );
                    }
                    this.executableFilenameTextBox.Text = executableFilename;

                    this.stringTableOutputPathTextBox.Text = value.StringTableOutputPath;
                    this.stringTableTextOutputPathTextBox.Text = value.StringTableTextOutputPath;

                    m_dispatchEvents = true;
                }
            }
        }
        #endregion

        #region Events
        /// <summary>
        /// Occurs when an edit is made.
        /// </summary>
        [Category( "Property Changed" ), Description( "Occurs when an edit is made." )]
        public event EventHandler Changed;
        #endregion

        #region Event Dispatchers
        protected void OnChanged()
        {
            if ( m_dispatchEvents && (this.Changed != null) )
            {
                this.Changed( this, EventArgs.Empty );
            }
        }
        #endregion

        #region Event Handlers
        private void browseStringTableTextOutputPathButton_Click( object sender, EventArgs e )
        {
            this.folderBrowserDialog.Description = "Select the folder where 'stringtable.txt' will be saved.";

            string stringTableTextOutputPath = this.stringTableTextOutputPathTextBox.Text.Trim();
            if ( Directory.Exists( stringTableTextOutputPath ) )
            {
                this.folderBrowserDialog.SelectedPath = stringTableTextOutputPath;
            }

            DialogResult result = this.folderBrowserDialog.ShowDialog( this );
            if ( result == DialogResult.OK )
            {
                this.stringTableTextOutputPathTextBox.Text = this.folderBrowserDialog.SelectedPath;
                
                OnChanged();
            }
        }

        private void generateStringTableCheckBox_CheckedChanged( object sender, EventArgs e )
        {
            this.generateStringTableGroupBox.Enabled = this.generateStringTableCheckBox.Checked;

            OnChanged();
        }

        private void browseExecutableFilenameButton_Click( object sender, EventArgs e )
        {
            string executableFilename = this.executableFilenameTextBox.Text.Trim();
            if ( !String.IsNullOrEmpty( executableFilename ) )
            {
                string directory = Path.GetDirectoryName( executableFilename );
                if ( Directory.Exists( directory ) )
                {
                    this.openFileDialog.InitialDirectory = directory;
                }

                this.openFileDialog.FileName = Path.GetFileName( executableFilename );
            }

            DialogResult result = this.openFileDialog.ShowDialog( this );
            if ( result == DialogResult.OK )
            {
                this.executableFilenameTextBox.Text = this.openFileDialog.FileName;

                OnChanged();
            }
        }

        private void browseStringTableOutputPathButton_Click( object sender, EventArgs e )
        {
            this.folderBrowserDialog.Description = "Select the folder where 'stringtable.strtbl' will be saved.";

            string stringTableOutputPath = this.stringTableOutputPathTextBox.Text.Trim();
            if ( Directory.Exists( stringTableOutputPath ) )
            {
                this.folderBrowserDialog.SelectedPath = stringTableOutputPath;
            }

            DialogResult result = this.folderBrowserDialog.ShowDialog( this );
            if ( result == DialogResult.OK )
            {
                this.stringTableOutputPathTextBox.Text = this.folderBrowserDialog.SelectedPath;

                OnChanged();
            }
        }

        private void TextBox_TextChanged( object sender, EventArgs e )
        {
            OnChanged();
        }
        #endregion

        #region Public Functions
        public void Validate( out rageStatus status )
        {
            LocalizedTextExporterSettings settings = this.LocalizedTextExporterSettings;

            if ( settings.MakeStringTable && !String.IsNullOrEmpty( settings.MakeStringTableExecutableFilename ) )
            {
                string makeStringTableExecutableFilename = Path.GetFullPath( Path.Combine( Path.GetDirectoryName( Application.ExecutablePath ),
                    settings.MakeStringTableExecutableFilename ) );

                if ( !File.Exists( makeStringTableExecutableFilename ) )
                {
                    status = new rageStatus(
                        String.Format( "The Executable Filename '{0}' does not exist.", makeStringTableExecutableFilename ) );
                    return;
                }
            }

            status = new rageStatus();
        }
        #endregion
    }
}
