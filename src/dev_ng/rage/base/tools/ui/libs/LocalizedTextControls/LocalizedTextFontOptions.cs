using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Text;
using System.Xml.Serialization;

using RSG.Base.Serialization;

namespace LocalizedTextControls
{
    /// <summary>
    /// A structure that holds all of the data associated with a variable that can be added to a piece of localized text.
    /// </summary>
    [Serializable]
    public class LocalizedTextFontOption : IRageClonableObject
    {
        public LocalizedTextFontOption()
        {

        }

        public LocalizedTextFontOption( string name )
        {
            m_name = name;
        }

        #region Variables
        private string m_name = string.Empty;
        private Font m_font = new Font( "Arial", 12 );
        #endregion

        #region Properties
        /// <summary>
        /// The name of the font in the game.
        /// </summary>
        [Description( "The name of the font in the game." ), Category( "Required" ), DefaultValue( "" )]
        public string Name
        {
            get
            {
                return m_name;
            }
            set
            {
                if ( m_name != null )
                {
                    m_name = value;
                }
                else
                {
                    m_name = string.Empty;
                }
            }
        }

        /// <summary>
        /// The Font to use for rendering in the Editor
        /// </summary>
        [XmlIgnore, Description( "The Font to use for rendering in the Editor." ), Category( "Required" )]
        public Font Font
        {
            get
            {
                return m_font;
            }
            set
            {
                if ( m_font != null )
                {
                    m_font.Dispose();
                }

                if ( value != null )
                {
                    m_font = value.Clone() as Font;
                }
                else
                {
                    m_font = new Font( "Arial", 12 );
                }
            }
        }

        [DefaultValue("Arial, 12pt"), Browsable( false ), DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public string XmlFont
        {
            get
            {
                try
                {
                    TypeConverter converter = TypeDescriptor.GetConverter( typeof( Font ) );
                    return converter.ConvertToString( this.Font );
                }
                catch ( Exception e )
                {
                    Console.WriteLine( e.ToString() );

                    return this.Font.ToString();
                }
            }
            set
            {
                if ( !String.IsNullOrEmpty( value ) )
                {
                    try
                    {
                        TypeConverter converter = TypeDescriptor.GetConverter( typeof( Font ) );
                        this.Font = converter.ConvertFromString( value ) as Font;
                    }
                    catch ( Exception e )
                    {
                        Console.WriteLine( e.ToString() );

                        this.Font = null;   // will reset to default Font
                    }
                }
                else
                {
                    this.Font = null;   // will reset to default Font
                }
            }
        }
        #endregion

        #region Overrides
        public override bool Equals( object obj )
        {
            if ( obj == null )
            {
                return false;
            }

            if ( base.Equals( obj ) )
            {
                return true;
            }

            if ( obj is LocalizedTextFontOption )
            {
                return Equals( obj as LocalizedTextFontOption );
            }

            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString()
        {
            return this.Name;
        }
        #endregion

        #region IRageClonableObject
        public object Clone()
        {
            LocalizedTextFontOption option = new LocalizedTextFontOption();
            option.CopyFrom( this );
            return option;
        }

        public void CopyFrom( IRageClonableObject other )
        {
            if ( other == null )
            {
                return;
            }

            if ( base.Equals( (object)other ) )
            {
                return;
            }

            if ( other is LocalizedTextFontOption )
            {
                CopyFrom( other as LocalizedTextFontOption );
            }
        }

        public void Reset()
        {
            this.Name = string.Empty;
            this.Font = new Font( "Arial", 12 );
        }
        #endregion

        #region Public Functions
        public void CopyFrom( LocalizedTextFontOption other )
        {
            if ( other == null )
            {
                return;
            }

            this.Name = other.Name;
            this.Font = other.Font;
        }

        public bool Equals( LocalizedTextFontOption other )
        {
            if ( other == null )
            {
                return false;
            }

            if ( base.Equals( (object)other ) )
            {
                return true;
            }

            int hc1 = this.Font.GetHashCode();
            int hc2 = other.Font.GetHashCode();

            return (this.Name == other.Name)
                && (this.XmlFont == other.XmlFont);
        }
        #endregion
    }

    /// <summary>
    /// A structure that contains a list of LocalizedTextFontOption items and can serialize the collection.
    /// </summary>
    [Serializable]
    public class LocalizedTextFontOptionCollection : rageSerializableList<LocalizedTextFontOption>
    {
        #region IRageClonableObject
        public override object Clone()
        {
            LocalizedTextFontOptionCollection collection = new LocalizedTextFontOptionCollection();
            collection.CopyFrom( collection );
            return collection;
        }
        #endregion

        #region Public Functions
        public LocalizedTextFontOption Find( string fontName )
        {
            string uppercaseFontName = fontName.ToUpper();
            foreach ( LocalizedTextFontOption option in this )
            {
                if ( option.Name.ToUpper() == uppercaseFontName )
                {
                    return option;
                }
            }

            return null;
        }
        #endregion
    }
}
