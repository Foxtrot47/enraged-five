using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

using RSG.Base.Serialization;
using RSG.Base.SCM;

namespace LocalizedTextControls
{
    public class LocalizedTextProjectSettings : rageSerializableObject
    {
        public LocalizedTextProjectSettings()
        {

        }

        #region Variables
        private rageSourceControlProviderSettings m_sourceControlProviderSettings = new rageSourceControlProviderSettings();
        #endregion

        #region Properties
        /// <summary>
        /// The source control settings for the Subtitle Project.
        /// </summary>
        [Description( "The source control settings for the Subtitle Project." )]
        public rageSourceControlProviderSettings SourceControlProviderSettings
        {
            get
            {
                return m_sourceControlProviderSettings;
            }
            set
            {
                if ( value != null )
                {
                    m_sourceControlProviderSettings = value;
                }
                else
                {
                    m_sourceControlProviderSettings.Reset();
                }
            }
        }
        #endregion

        #region Overrides
        public override bool Equals( object obj )
        {
            if ( obj == null )
            {
                return false;
            }

            if ( base.Equals( obj ) )
            {
                return true;
            }

            if ( obj is LocalizedTextProjectSettings )
            {
                return Equals( obj as LocalizedTextProjectSettings );
            }

            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override object Clone()
        {
            LocalizedTextProjectSettings settings = new LocalizedTextProjectSettings();
            settings.CopyFrom( this );
            return settings;
        }

        public override void CopyFrom( IRageClonableObject other )
        {
            if ( other == null )
            {
                return;
            }

            if ( base.Equals( (object)other ) )
            {
                return;
            }

            if ( other is LocalizedTextProjectSettings )
            {
                CopyFrom( other as LocalizedTextProjectSettings );
            }
        }

        public override void Reset()
        {
            base.Reset();

            this.SourceControlProviderSettings.Reset();
        }
        #endregion

        #region Public Functions
        public void CopyFrom( LocalizedTextProjectSettings other )
        {
            if ( other == null )
            {
                return;
            }

            if ( base.Equals( (object)other ) )
            {
                return;
            }

            base.CopyFrom( other as rageSerializableObject );

            this.SourceControlProviderSettings.CopyFrom( other.SourceControlProviderSettings );
        }

        public bool Equals( LocalizedTextProjectSettings other )
        {
            if ( other == null )
            {
                return false;
            }

            if ( base.Equals( other as rageSerializableObject ) )
            {
                return true;
            }

            return this.SourceControlProviderSettings.Equals( other.SourceControlProviderSettings );
        }
        #endregion
    }
}
