using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Text;

using RSG.Base.Serialization;

namespace LocalizedTextControls
{
    /// <summary>
    /// A structure that holds all of the other associated with a variable that can be added to a piece of localized text.
    /// </summary>
    [Serializable]
    public class LocalizedTextFlagOption : IRageClonableObject
    {
        public LocalizedTextFlagOption()
        {

        }

        public LocalizedTextFlagOption( string name )
        {
            m_name = name;
        }

        #region Variables
        private string m_name = string.Empty;
        #endregion

        #region Properties
        /// <summary>
        /// The name of the flag.
        /// </summary>
        [Description( "The name of the flag." ), Category( "Required" ), DefaultValue( "" )]
        public string Name
        {
            get
            {
                return m_name;
            }
            set
            {
                if ( m_name != null )
                {
                    m_name = value;
                }
                else
                {
                    m_name = string.Empty;
                }
            }
        }
        #endregion

        #region Overrides
        public override bool Equals( object obj )
        {
            if ( obj == null )
            {
                return false;
            }

            if ( base.Equals( obj ) )
            {
                return true;
            }

            if ( obj is LocalizedTextFlagOption )
            {
                return Equals( obj as LocalizedTextFlagOption );
            }

            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString()
        {
            return this.Name;
        }
        #endregion

        #region IRageClonableObject
        public object Clone()
        {
            LocalizedTextFlagOption option = new LocalizedTextFlagOption();
            option.CopyFrom( this );
            return option;
        }

        public void CopyFrom( IRageClonableObject other )
        {
            if ( other == null )
            {
                return;
            }

            if ( base.Equals( (object)other ) )
            {
                return;
            }

            if ( other is LocalizedTextFlagOption )
            {
                CopyFrom( other as LocalizedTextFlagOption );
            }
        }

        public void Reset()
        {
            this.Name = string.Empty;
        }
        #endregion

        #region Public Functions
        public void CopyFrom( LocalizedTextFlagOption other )
        {
            if ( other == null )
            {
                return;
            }

            this.Name = other.Name;
        }

        public bool Equals( LocalizedTextFlagOption other )
        {
            if ( other == null )
            {
                return false;
            }

            if ( base.Equals( (object)other ) )
            {
                return true;
            }

            return (this.Name == other.Name);
        }
        #endregion
    }

    /// <summary>
    /// A structure that contains a list of LocalizedTextFlagOption items and can serialize the collection.
    /// </summary>
    [Serializable]
    public class LocalizedTextFlagOptionCollection : rageSerializableList<LocalizedTextFlagOption>
    {
        #region IRageClonableObject
        public override object Clone()
        {
            LocalizedTextFlagOptionCollection collection = new LocalizedTextFlagOptionCollection();
            collection.CopyFrom( collection );
            return collection;
        }
        #endregion
    }
}
