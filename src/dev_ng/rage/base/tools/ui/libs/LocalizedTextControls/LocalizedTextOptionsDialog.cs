using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;

using RSG.Base.Command;
using RSG.Base.Forms;

namespace LocalizedTextControls
{
    /// <summary>
    /// This is a dialog for editing the LocalizedTextOptions data.  It is not connected to anything,
    /// so it is up to the application author to show the dialog and read its data where appropriate.
    /// </summary>
    public partial class LocalizedTextOptionsDialog : Form
    {
        public LocalizedTextOptionsDialog()
        {
            InitializeComponent();

            this.flagListBoxSettingsControl.EditType = typeof( LocalizedTextFlagOption );
            this.fontListBoxSettingsControl.EditType = typeof( LocalizedTextFontOption );
            this.languageListBoxSettingsControl.EditType = typeof( LocalizedTextLanguageOption );
            this.platformListBoxSettingsControl.EditType = typeof( LocalizedTextPlatformOption );
            this.regionListBoxSettingsControl.EditType = typeof( LocalizedTextRegionOption );
            this.variablesListBoxSettingsControl.EditType = typeof( LocalizedTextVariableOption );
        }

        #region Variables
        private bool m_dispatchEvents = true;
        private LocalizedTextOptions m_originalLocalizedTextOptions = null;
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the HelpNamespace for the dialog.
        /// </summary>
        [DefaultValue( null ), Description( "Gets or sets the HelpNamespace for the dialog." )]
        public string HelpNamespace
        {
            get
            {
                return this.helpProvider.HelpNamespace;
            }
            set
            {
                this.helpProvider.HelpNamespace = value;

                this.flagListBoxSettingsControl.HelpProvider = this.helpProvider;
                this.fontListBoxSettingsControl.HelpProvider = this.helpProvider;
                this.languageListBoxSettingsControl.HelpProvider = this.helpProvider;
                this.platformListBoxSettingsControl.HelpProvider = this.helpProvider;
                this.regionListBoxSettingsControl.HelpProvider = this.helpProvider;
                this.variablesListBoxSettingsControl.HelpProvider = this.helpProvider;
            }
        }

        public LocalizedTextOptions LocalizedTextOptions
        {
            get
            {
                LocalizedTextOptions options = new LocalizedTextOptions();

                // get the general stuff
                options.ScreenHeight = (int)this.generalScreenHeightNumericUpDown.Value;
                options.ScreenWidth = (int)this.generalScreenWidthNumericUpDown.Value;
                options.TextExporterPluginFilename = this.generalTextExporterPluginTextBox.Text.Trim();
                options.TextRendererPluginFilename = this.generalTextRendererPluginTextBox.Text.Trim();

                // get the flags
                options.FlagOptionCollection.Clear();   // remove the default option(s), if any
                foreach ( LocalizedTextFlagOption option in this.flagListBoxSettingsControl.Items )
                {
                    options.FlagOptionCollection.Add( option );
                }

                // get the fonts
                options.FontOptionCollection.Clear();   // remove the default option(s), if any
                foreach ( LocalizedTextFontOption option in this.fontListBoxSettingsControl.Items )
                {
                    options.FontOptionCollection.Add( option );
                }

                options.DefaultFontOptionIndex = this.fontListBoxSettingsControl.DefaultItemIndex;

                // get the languages
                options.LanguageOptionCollection.Clear();   // remove the default option(s), if any
                foreach ( LocalizedTextLanguageOption option in this.languageListBoxSettingsControl.Items )
                {
                    options.LanguageOptionCollection.Add( option );
                }

                options.DefaultLanguageOptionIndex = this.languageListBoxSettingsControl.DefaultItemIndex;

                // get the platforms
                options.PlatformOptionCollection.Clear();   // remove the default option(s), if any
                foreach ( LocalizedTextPlatformOption option in this.platformListBoxSettingsControl.Items )
                {
                    options.PlatformOptionCollection.Add( option );
                }

                options.DefaultPlatformOptionIndex = this.platformListBoxSettingsControl.DefaultItemIndex;

                // get the regions
                options.RegionOptionCollection.Clear();   // remove the default option(s), if any
                foreach ( LocalizedTextRegionOption option in this.regionListBoxSettingsControl.Items )
                {
                    options.RegionOptionCollection.Add( option );
                }

                options.DefaultRegionOptionIndex = this.regionListBoxSettingsControl.DefaultItemIndex;

                // get the variables
                options.VariableOptionCollection.Clear();   // remove the default option(s), if any
                foreach ( LocalizedTextVariableOption option in this.variablesListBoxSettingsControl.Items )
                {
                    options.VariableOptionCollection.Add( option );
                }

                // preserve the file locations from the original options
                options.CurrentFilename = m_originalLocalizedTextOptions.CurrentFilename;
                options.OriginalFileLocation = m_originalLocalizedTextOptions.OriginalFileLocation;

                return options;
            }
            set
            {
                if ( value != null )
                {
                    m_dispatchEvents = false;

                    // load the general stuff
                    this.generalScreenHeightNumericUpDown.Value = value.ScreenHeight;
                    this.generalScreenWidthNumericUpDown.Value = value.ScreenWidth;
                    this.generalTextExporterPluginTextBox.Text = value.TextExporterPluginFilename;
                    this.generalTextRendererPluginTextBox.Text = value.TextRendererPluginFilename;

                    // load the flags
                    List<ICloneable> options = new List<ICloneable>();
                    foreach ( LocalizedTextFlagOption option in value.FlagOptionCollection )
                    {
                        options.Add( option );
                    }
                    this.flagListBoxSettingsControl.Items = options;

                    // load the fonts
                    options = new List<ICloneable>();
                    foreach ( LocalizedTextFontOption option in value.FontOptionCollection )
                    {
                        options.Add( option );
                    }
                    this.fontListBoxSettingsControl.Items = options;

                    this.fontListBoxSettingsControl.DefaultItemIndex = value.DefaultFontOptionIndex;

                    // load the languages
                    options = new List<ICloneable>();
                    foreach ( LocalizedTextLanguageOption option in value.LanguageOptionCollection )
                    {
                        options.Add( option );
                    }
                    this.languageListBoxSettingsControl.Items = options;

                    this.languageListBoxSettingsControl.DefaultItemIndex = value.DefaultLanguageOptionIndex;

                    // load the platforms
                    options = new List<ICloneable>();
                    foreach ( LocalizedTextPlatformOption option in value.PlatformOptionCollection )
                    {
                        options.Add( option );
                    }
                    this.platformListBoxSettingsControl.Items = options;

                    this.platformListBoxSettingsControl.DefaultItemIndex = value.DefaultPlatformOptionIndex;

                    // load the regions
                    options = new List<ICloneable>();
                    foreach ( LocalizedTextRegionOption option in value.RegionOptionCollection )
                    {
                        options.Add( option );
                    }
                    this.regionListBoxSettingsControl.Items = options;

                    this.regionListBoxSettingsControl.DefaultItemIndex = value.DefaultRegionOptionIndex;

                    // load the variables
                    options = new List<ICloneable>();
                    foreach ( LocalizedTextVariableOption option in value.VariableOptionCollection )
                    {
                        options.Add( option );
                    }
                    this.variablesListBoxSettingsControl.Items = options;

                    m_dispatchEvents = true;
                }

                m_originalLocalizedTextOptions = value;
            }
        }
        #endregion

        #region Events
        public event SourceControlEventHandler CheckOutFile;
        public event EventHandler Changed;
        #endregion

        #region Event Dispatchers
        protected void OnChanged()
        {
            if ( m_dispatchEvents && (this.Changed != null) )
            {
                this.Changed( this, EventArgs.Empty );
            }
        }

        protected void OnCheckOutFile( SourceControlEventArgs e )
        {
            if ( m_dispatchEvents && (this.CheckOutFile != null) )
            {
                this.CheckOutFile( this, e );
            }
        }
        #endregion

        #region Event Handlers
        private void generalScreenWidthNumericUpDown_ValueChanged( object sender, EventArgs e )
        {
            OnChanged();
        }

        private void generalScreenHeightNumericUpDown_ValueChanged( object sender, EventArgs e )
        {
            OnChanged();
        }

        private void generalTextExporterPluginTextBox_TextChanged( object sender, EventArgs e )
        {
            OnChanged();
        }

        private void generalBrowseTextExporterPluginButton_Click( object sender, EventArgs e )
        {
            this.openFileDialog.Filter = "Dynamic Link Libraries (*.dll)|*.dll";
            this.openFileDialog.Title = "Select Text Exporter Plugin";

            string filename = this.generalTextExporterPluginTextBox.Text.Trim();
            if ( String.IsNullOrEmpty( filename ) )
            {
                this.openFileDialog.InitialDirectory = Path.GetDirectoryName( Application.ExecutablePath );
                this.openFileDialog.FileName = Path.GetFileName( "LocalizedTextRenderer.dll" );
            }
            else
            {
                string directory = Path.GetDirectoryName( filename );
                if ( Directory.Exists( directory ) )
                {
                    this.openFileDialog.InitialDirectory = directory;
                }

                this.openFileDialog.FileName = Path.GetFileName( filename );
            }

            DialogResult result = this.openFileDialog.ShowDialog( this );
            if ( result == DialogResult.OK )
            {
                this.generalTextExporterPluginTextBox.Text = this.openFileDialog.FileName;
                OnChanged();
            }
        }

        private void generalTextRendererPluginTextBox_TextChanged( object sender, EventArgs e )
        {
            OnChanged();
        }

        private void generalBrowseTextRendererPluginButton_Click( object sender, EventArgs e )
        {
            this.openFileDialog.Filter = "Dynamic Link Libraries (*.dll)|*.dll";
            this.openFileDialog.Title = "Select Text Renderer Plugin";

            string filename = this.generalTextRendererPluginTextBox.Text.Trim();
            if ( String.IsNullOrEmpty( filename ) )
            {
                this.openFileDialog.InitialDirectory = Path.GetDirectoryName( Application.ExecutablePath );
                this.openFileDialog.FileName = Path.GetFileName( "LocalizedTextRenderer.dll" );
            }
            else
            {
                string directory = Path.GetDirectoryName( filename );
                if ( Directory.Exists( directory ) )
                {
                    this.openFileDialog.InitialDirectory = directory;
                }

                this.openFileDialog.FileName = Path.GetFileName( filename );
            }

            DialogResult result = this.openFileDialog.ShowDialog( this );
            if ( result == DialogResult.OK )
            {
                this.generalTextRendererPluginTextBox.Text = this.openFileDialog.FileName;
                OnChanged();
            }
        }

        private void generalImportButton_Click( object sender, EventArgs e )
        {
            DialogResult result = rageMessageBox.ShowWarning( this, 
                "Importing a Localized Text Options file will overwrite your current options,\n" +
                "but you can still revert to your original options by clicking the Cancel Button.\n\nDo you wish to proceed?", 
                "Import Localized Text Options File", MessageBoxButtons.YesNo );
            if ( result == DialogResult.No )
            {
                return;
            }

            LoadLocalizedTextOptionsFile();
        }

        private void listBoxSettingsControl_Changed( object sender, EventArgs e )
        {
            OnChanged();
        }

        private void okButton_Click( object sender, EventArgs e )
        {
            LocalizedTextOptions options = this.LocalizedTextOptions;
            if ( options.Equals( m_originalLocalizedTextOptions ) )
            {
                this.DialogResult = DialogResult.OK;
                return;
            }

            string msg = String.IsNullOrEmpty( options.CurrentFilename ) ? "You've never saved a Localized Text Options file.  Would you like to do that now?" : "The Localized Text Options has changed.  Would you like to save the file?";

            // optional save
            DialogResult result = rageMessageBox.ShowQuestion( this, msg,
                "Localized Text Options", MessageBoxButtons.YesNoCancel );
            if ( result == DialogResult.No )
            {
                this.DialogResult = DialogResult.OK;
                return;
            }
            else if ( result == DialogResult.Cancel )
            {
                return;
            }

            if ( SaveLocalizedTextOptionsFile() )
            {
                this.DialogResult = DialogResult.OK;
            }
        }
        #endregion

        #region Private Functions
        private bool SaveLocalizedTextOptionsFile()
        {
            this.saveFileDialog.Filter = "Localized Text Options Files (*.txtproj.options)|*.txtproj.options";
            this.saveFileDialog.Title = "Save Localized Text Options File As";

            if ( !String.IsNullOrEmpty( m_originalLocalizedTextOptions.CurrentFilename ) )
            {
                string directory = Path.GetDirectoryName( m_originalLocalizedTextOptions.CurrentFilename );
                if ( Directory.Exists( directory ) )
                {
                    this.saveFileDialog.InitialDirectory = directory;
                }

                this.saveFileDialog.FileName = Path.GetFileName( m_originalLocalizedTextOptions.CurrentFilename );
            }
            else
            {
                this.saveFileDialog.InitialDirectory = Path.GetDirectoryName( Application.ExecutablePath );
                this.saveFileDialog.FileName = "LocalizedTextOptions.txtproj.options";
            }

            DialogResult result = this.saveFileDialog.ShowDialog( this );
            if ( result == DialogResult.OK )
            {
                SourceControlEventArgs e = new SourceControlEventArgs( this.saveFileDialog.FileName );
                OnCheckOutFile( e );
                if ( e.Result == DialogResult.Abort )
                {
                    return false;
                }

                LocalizedTextOptions options = this.LocalizedTextOptions;
                while ( true )
                {
                    rageStatus status;
                    options.SaveFile( this.saveFileDialog.FileName, out status );
                    if ( status.Success() )
                    {
                        m_originalLocalizedTextOptions = options; // this is now our original options
                        break;
                    }
                    else
                    {
                        result = rageMessageBox.ShowExclamation( this, status.ErrorString, "Export Localized Text Options Error",
                            MessageBoxButtons.RetryCancel );
                        if ( result == DialogResult.Cancel )
                        {
                            return false;
                        }
                    }
                }
            }

            return true;
        }

        private void LoadLocalizedTextOptionsFile()
        {
            this.openFileDialog.Filter = "Localized Text Options Files (*.txtproj.options)|*.txtproj.options";
            this.openFileDialog.Title = "Open Localized Text Options File";

            if ( !String.IsNullOrEmpty( m_originalLocalizedTextOptions.CurrentFilename ) )
            {
                string directory = Path.GetDirectoryName( m_originalLocalizedTextOptions.CurrentFilename );
                if ( Directory.Exists( directory ) )
                {
                    this.openFileDialog.InitialDirectory = directory;
                }

                this.openFileDialog.FileName = Path.GetFileName( m_originalLocalizedTextOptions.CurrentFilename );
            }
            else
            {
                this.openFileDialog.InitialDirectory = Path.GetDirectoryName( Application.ExecutablePath );
                this.openFileDialog.FileName = "LocalizedTextOptions.txtproj.options";
            }

            DialogResult result = this.openFileDialog.ShowDialog( this );
            if ( result != DialogResult.OK )
            {
                return;
            }

            LocalizedTextOptions options = new LocalizedTextOptions();
            while ( true )
            {
                rageStatus status;
                options.LoadFile( this.openFileDialog.FileName, out status );
                if ( status.Success() )
                {
                    // this is now our current and our original options
                    this.LocalizedTextOptions = options;
                    OnChanged();
                    break;
                }
                else
                {
                    result = rageMessageBox.ShowExclamation( this, status.ErrorString, "Import Localized Text Options Error", 
                        MessageBoxButtons.RetryCancel );
                    if ( result == DialogResult.Cancel )
                    {
                        break;
                    }
                }
            }
        }
        #endregion
    }
}