using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace LocalizedTextControls
{
    /// <summary>
    /// An interface for rendering LocalizedTextData to a Bitmap.
    /// </summary>
    public interface ILocalizedTextRenderer
    {
        /// <summary>
        /// Renders the bitmap for the given localized text, setting LocalizedTextData.Bitmap.  The Bitmap should be the size
        /// of the screen resolution in the LocalizedTextOptions.  The font used should be the size specified by the
        /// LocalizedTextFontOption in the LocalizedTextOptions.
        /// </summary>
        /// <param name="data">The Localized Text Data to render.</param>
        /// <param name="collection">The collection to use when resolving variables.</param>
        /// <param name="options">The options used when the Localized Text was created.</param>
        /// <param name="bmp">Where to save the bitmap.  Only create a new one if bmp is null, or set it to null on error.</param>
        void Render( LocalizedTextData data, LocalizedTextDataCollection collection, LocalizedTextOptions options, ref Bitmap bmp );
    }
}
