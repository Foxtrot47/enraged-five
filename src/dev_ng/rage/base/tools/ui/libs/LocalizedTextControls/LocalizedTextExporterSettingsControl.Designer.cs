namespace LocalizedTextControls
{
    partial class LocalizedTextExporterSettingsControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && (components != null) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.stringTableTextOutputFilenameLabel = new System.Windows.Forms.Label();
            this.stringTableTextOutputPathTextBox = new System.Windows.Forms.TextBox();
            this.browseStringTableTextFilenameButton = new System.Windows.Forms.Button();
            this.generateStringTableCheckBox = new System.Windows.Forms.CheckBox();
            this.generateStringTableGroupBox = new System.Windows.Forms.GroupBox();
            this.browseStringTableOutputPathButton = new System.Windows.Forms.Button();
            this.stringTableOutputPathTextBox = new System.Windows.Forms.TextBox();
            this.stringTableOutputPathLabel = new System.Windows.Forms.Label();
            this.browseExecutableFilenameButton = new System.Windows.Forms.Button();
            this.executableFilenameTextBox = new System.Windows.Forms.TextBox();
            this.executableFilenameLabel = new System.Windows.Forms.Label();
            this.folderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.generateStringTableGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // stringTableTextOutputFilenameLabel
            // 
            this.stringTableTextOutputFilenameLabel.AutoSize = true;
            this.stringTableTextOutputFilenameLabel.Location = new System.Drawing.Point( 3, 6 );
            this.stringTableTextOutputFilenameLabel.Name = "stringTableTextOutputFilenameLabel";
            this.stringTableTextOutputFilenameLabel.Size = new System.Drawing.Size( 64, 13 );
            this.stringTableTextOutputFilenameLabel.TabIndex = 0;
            this.stringTableTextOutputFilenameLabel.Text = "Output Path";
            // 
            // stringTableTextOutputPathTextBox
            // 
            this.stringTableTextOutputPathTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.stringTableTextOutputPathTextBox.Location = new System.Drawing.Point( 73, 3 );
            this.stringTableTextOutputPathTextBox.Name = "stringTableTextOutputPathTextBox";
            this.stringTableTextOutputPathTextBox.Size = new System.Drawing.Size( 404, 20 );
            this.stringTableTextOutputPathTextBox.TabIndex = 1;
            this.stringTableTextOutputPathTextBox.TextChanged += new System.EventHandler( this.TextBox_TextChanged );
            // 
            // browseStringTableTextFilenameButton
            // 
            this.browseStringTableTextFilenameButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.browseStringTableTextFilenameButton.Location = new System.Drawing.Point( 483, 1 );
            this.browseStringTableTextFilenameButton.Name = "browseStringTableTextFilenameButton";
            this.browseStringTableTextFilenameButton.Size = new System.Drawing.Size( 24, 23 );
            this.browseStringTableTextFilenameButton.TabIndex = 2;
            this.browseStringTableTextFilenameButton.Text = "...";
            this.browseStringTableTextFilenameButton.UseVisualStyleBackColor = true;
            this.browseStringTableTextFilenameButton.Click += new System.EventHandler( this.browseStringTableTextOutputPathButton_Click );
            // 
            // generateStringTableCheckBox
            // 
            this.generateStringTableCheckBox.AutoSize = true;
            this.generateStringTableCheckBox.Location = new System.Drawing.Point( 6, 29 );
            this.generateStringTableCheckBox.Name = "generateStringTableCheckBox";
            this.generateStringTableCheckBox.Size = new System.Drawing.Size( 130, 17 );
            this.generateStringTableCheckBox.TabIndex = 3;
            this.generateStringTableCheckBox.Text = "Generate String Table";
            this.generateStringTableCheckBox.UseVisualStyleBackColor = true;
            this.generateStringTableCheckBox.CheckedChanged += new System.EventHandler( this.generateStringTableCheckBox_CheckedChanged );
            // 
            // generateStringTableGroupBox
            // 
            this.generateStringTableGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.generateStringTableGroupBox.Controls.Add( this.browseStringTableOutputPathButton );
            this.generateStringTableGroupBox.Controls.Add( this.stringTableOutputPathTextBox );
            this.generateStringTableGroupBox.Controls.Add( this.stringTableOutputPathLabel );
            this.generateStringTableGroupBox.Controls.Add( this.browseExecutableFilenameButton );
            this.generateStringTableGroupBox.Controls.Add( this.executableFilenameTextBox );
            this.generateStringTableGroupBox.Controls.Add( this.executableFilenameLabel );
            this.generateStringTableGroupBox.Enabled = false;
            this.generateStringTableGroupBox.Location = new System.Drawing.Point( 6, 52 );
            this.generateStringTableGroupBox.Name = "generateStringTableGroupBox";
            this.generateStringTableGroupBox.Size = new System.Drawing.Size( 501, 66 );
            this.generateStringTableGroupBox.TabIndex = 4;
            this.generateStringTableGroupBox.TabStop = false;
            // 
            // browseStringTableOutputPathButton
            // 
            this.browseStringTableOutputPathButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.browseStringTableOutputPathButton.Location = new System.Drawing.Point( 470, 37 );
            this.browseStringTableOutputPathButton.Name = "browseStringTableOutputPathButton";
            this.browseStringTableOutputPathButton.Size = new System.Drawing.Size( 25, 23 );
            this.browseStringTableOutputPathButton.TabIndex = 5;
            this.browseStringTableOutputPathButton.Text = "...";
            this.browseStringTableOutputPathButton.UseVisualStyleBackColor = true;
            this.browseStringTableOutputPathButton.Click += new System.EventHandler( this.browseStringTableOutputPathButton_Click );
            // 
            // stringTableOutputPathTextBox
            // 
            this.stringTableOutputPathTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.stringTableOutputPathTextBox.Location = new System.Drawing.Point( 117, 39 );
            this.stringTableOutputPathTextBox.Name = "stringTableOutputPathTextBox";
            this.stringTableOutputPathTextBox.Size = new System.Drawing.Size( 347, 20 );
            this.stringTableOutputPathTextBox.TabIndex = 4;
            this.stringTableOutputPathTextBox.TextChanged += new System.EventHandler( this.TextBox_TextChanged );
            // 
            // stringTableOutputPathLabel
            // 
            this.stringTableOutputPathLabel.AutoSize = true;
            this.stringTableOutputPathLabel.Location = new System.Drawing.Point( 47, 42 );
            this.stringTableOutputPathLabel.Name = "stringTableOutputPathLabel";
            this.stringTableOutputPathLabel.Size = new System.Drawing.Size( 64, 13 );
            this.stringTableOutputPathLabel.TabIndex = 3;
            this.stringTableOutputPathLabel.Text = "Output Path";
            // 
            // browseExecutableFilenameButton
            // 
            this.browseExecutableFilenameButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.browseExecutableFilenameButton.Location = new System.Drawing.Point( 470, 11 );
            this.browseExecutableFilenameButton.Name = "browseExecutableFilenameButton";
            this.browseExecutableFilenameButton.Size = new System.Drawing.Size( 25, 23 );
            this.browseExecutableFilenameButton.TabIndex = 2;
            this.browseExecutableFilenameButton.Text = "...";
            this.browseExecutableFilenameButton.UseVisualStyleBackColor = true;
            this.browseExecutableFilenameButton.Click += new System.EventHandler( this.browseExecutableFilenameButton_Click );
            // 
            // executableFilenameTextBox
            // 
            this.executableFilenameTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.executableFilenameTextBox.Location = new System.Drawing.Point( 117, 13 );
            this.executableFilenameTextBox.Name = "executableFilenameTextBox";
            this.executableFilenameTextBox.Size = new System.Drawing.Size( 347, 20 );
            this.executableFilenameTextBox.TabIndex = 1;
            this.executableFilenameTextBox.TextChanged += new System.EventHandler( this.TextBox_TextChanged );
            // 
            // executableFilenameLabel
            // 
            this.executableFilenameLabel.AutoSize = true;
            this.executableFilenameLabel.Location = new System.Drawing.Point( 6, 16 );
            this.executableFilenameLabel.Name = "executableFilenameLabel";
            this.executableFilenameLabel.Size = new System.Drawing.Size( 105, 13 );
            this.executableFilenameLabel.TabIndex = 0;
            this.executableFilenameLabel.Text = "Executable Filename";
            // 
            // folderBrowserDialog
            // 
            this.folderBrowserDialog.RootFolder = System.Environment.SpecialFolder.MyComputer;
            // 
            // openFileDialog
            // 
            this.openFileDialog.Filter = "Executables (*.exe)|*.exe";
            this.openFileDialog.RestoreDirectory = true;
            this.openFileDialog.Title = "Select the String Table Generator";
            // 
            // LocalizedTextExporterSettingsControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add( this.generateStringTableGroupBox );
            this.Controls.Add( this.generateStringTableCheckBox );
            this.Controls.Add( this.browseStringTableTextFilenameButton );
            this.Controls.Add( this.stringTableTextOutputPathTextBox );
            this.Controls.Add( this.stringTableTextOutputFilenameLabel );
            this.Name = "LocalizedTextExporterSettingsControl";
            this.Size = new System.Drawing.Size( 510, 121 );
            this.generateStringTableGroupBox.ResumeLayout( false );
            this.generateStringTableGroupBox.PerformLayout();
            this.ResumeLayout( false );
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label stringTableTextOutputFilenameLabel;
        private System.Windows.Forms.TextBox stringTableTextOutputPathTextBox;
        private System.Windows.Forms.Button browseStringTableTextFilenameButton;
        private System.Windows.Forms.CheckBox generateStringTableCheckBox;
        private System.Windows.Forms.GroupBox generateStringTableGroupBox;
        private System.Windows.Forms.Button browseStringTableOutputPathButton;
        private System.Windows.Forms.TextBox stringTableOutputPathTextBox;
        private System.Windows.Forms.Label stringTableOutputPathLabel;
        private System.Windows.Forms.Button browseExecutableFilenameButton;
        private System.Windows.Forms.TextBox executableFilenameTextBox;
        private System.Windows.Forms.Label executableFilenameLabel;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
    }
}
