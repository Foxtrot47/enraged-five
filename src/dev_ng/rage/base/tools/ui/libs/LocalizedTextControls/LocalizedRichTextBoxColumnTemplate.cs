using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using GlacialComponents.Controls.GTLCommon;

namespace LocalizedTextControls
{
    /// <summary>
    /// An EmbeddedControl used by the LocalizedTextEditorControl's GlacialTreeList for editing the text portion
    /// of a localized text item in place.
    /// </summary>
    public class LocalizedRichTextBoxColumnTemplate : LocalizedRichTextBox, IGEmbeddedControl
    {
        public LocalizedRichTextBoxColumnTemplate()
            : base()
        {
            
        }

        #region Variables
        private object m_Owner;
        private object m_UserData;
        #endregion

        #region Properties
        public object Owner
        {
            get
            {
                return m_Owner;
            }
        }

        public string RawText
        {
            get
            {
                return base.Text;
            }
        }

        public new string Text
        {
            get
            {
                return base.Rtf;
            }
            set
            {
                string rtf = base.Rtf;
                try
                {
                    base.Rtf = value;
                }
                catch
                {
                    base.Rtf = rtf;
                    base.Text = value;
                }
            }
        }

        public object UserData
        {
            get
            {
                return m_UserData;
            }
        }
        #endregion

        #region Public Functions
        public bool Load( string text, object userData, object owner )
        {
            this.Text = text;

            m_UserData = userData;
            m_Owner = owner;

            return true;
        }
        
        public string Unload()
        {
            return this.Text;
        }
        #endregion
    }
}
