namespace LocalizedTextControls
{
    partial class LocalizedRichTextBox
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && (components != null) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager( typeof( LocalizedRichTextBox ) );
            this.contextMenuStrip = new System.Windows.Forms.ContextMenuStrip( this.components );
            this.undoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.redoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.cutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.copyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pasteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.selectAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.boldToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.italicToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.underlineToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.colorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.autoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.chooseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.blackToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.whiteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.grayToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.silverToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.maroonToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.redToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.oliveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.yellowToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.greenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.limeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tealToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aquaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.navyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.blueToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.purpleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fuchsiaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.insertVariableToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.multiLineEditorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.timer = new System.Windows.Forms.Timer( this.components );
            this.contextMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // contextMenuStrip
            // 
            this.contextMenuStrip.Items.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.undoToolStripMenuItem,
            this.redoToolStripMenuItem,
            this.toolStripSeparator4,
            this.cutToolStripMenuItem,
            this.copyToolStripMenuItem,
            this.pasteToolStripMenuItem,
            this.deleteToolStripMenuItem,
            this.toolStripSeparator2,
            this.selectAllToolStripMenuItem,
            this.toolStripSeparator3,
            this.boldToolStripMenuItem,
            this.italicToolStripMenuItem,
            this.underlineToolStripMenuItem,
            this.colorToolStripMenuItem,
            this.toolStripSeparator5,
            this.insertVariableToolStripMenuItem,
            this.multiLineEditorToolStripMenuItem} );
            this.contextMenuStrip.Name = "contextMenuStrip";
            this.contextMenuStrip.Size = new System.Drawing.Size( 179, 314 );
            this.contextMenuStrip.Opening += new System.ComponentModel.CancelEventHandler( this.contextMenuStrip_Opening );
            // 
            // undoToolStripMenuItem
            // 
            this.undoToolStripMenuItem.Image = global::LocalizedTextControls.Properties.Resources.Edit_UndoHS;
            this.undoToolStripMenuItem.Name = "undoToolStripMenuItem";
            this.undoToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Z)));
            this.undoToolStripMenuItem.Size = new System.Drawing.Size( 178, 22 );
            this.undoToolStripMenuItem.Text = "&Undo";
            this.undoToolStripMenuItem.Click += new System.EventHandler( this.undoToolStripMenuItem_Click );
            // 
            // redoToolStripMenuItem
            // 
            this.redoToolStripMenuItem.Image = global::LocalizedTextControls.Properties.Resources.Edit_RedoHS;
            this.redoToolStripMenuItem.Name = "redoToolStripMenuItem";
            this.redoToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift)
                        | System.Windows.Forms.Keys.Z)));
            this.redoToolStripMenuItem.Size = new System.Drawing.Size( 178, 22 );
            this.redoToolStripMenuItem.Text = "&Redo";
            this.redoToolStripMenuItem.Click += new System.EventHandler( this.redoToolStripMenuItem_Click );
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size( 175, 6 );
            // 
            // cutToolStripMenuItem
            // 
            this.cutToolStripMenuItem.Image = global::LocalizedTextControls.Properties.Resources.CutHS;
            this.cutToolStripMenuItem.Name = "cutToolStripMenuItem";
            this.cutToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+X";
            this.cutToolStripMenuItem.Size = new System.Drawing.Size( 178, 22 );
            this.cutToolStripMenuItem.Text = "Cu&t";
            this.cutToolStripMenuItem.Click += new System.EventHandler( this.cutToolStripMenuItem_Click );
            // 
            // copyToolStripMenuItem
            // 
            this.copyToolStripMenuItem.Image = global::LocalizedTextControls.Properties.Resources.CopyHS;
            this.copyToolStripMenuItem.Name = "copyToolStripMenuItem";
            this.copyToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+C";
            this.copyToolStripMenuItem.Size = new System.Drawing.Size( 178, 22 );
            this.copyToolStripMenuItem.Text = "&Copy";
            this.copyToolStripMenuItem.Click += new System.EventHandler( this.copyToolStripMenuItem_Click );
            // 
            // pasteToolStripMenuItem
            // 
            this.pasteToolStripMenuItem.Image = global::LocalizedTextControls.Properties.Resources.PasteHS;
            this.pasteToolStripMenuItem.Name = "pasteToolStripMenuItem";
            this.pasteToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+P";
            this.pasteToolStripMenuItem.Size = new System.Drawing.Size( 178, 22 );
            this.pasteToolStripMenuItem.Text = "&Paste";
            this.pasteToolStripMenuItem.Click += new System.EventHandler( this.pasteToolStripMenuItem_Click );
            // 
            // deleteToolStripMenuItem
            // 
            this.deleteToolStripMenuItem.Image = global::LocalizedTextControls.Properties.Resources.DeleteHS;
            this.deleteToolStripMenuItem.Name = "deleteToolStripMenuItem";
            this.deleteToolStripMenuItem.ShortcutKeyDisplayString = "Del";
            this.deleteToolStripMenuItem.Size = new System.Drawing.Size( 178, 22 );
            this.deleteToolStripMenuItem.Text = "&Delete";
            this.deleteToolStripMenuItem.Click += new System.EventHandler( this.deleteToolStripMenuItem_Click );
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size( 175, 6 );
            // 
            // selectAllToolStripMenuItem
            // 
            this.selectAllToolStripMenuItem.Name = "selectAllToolStripMenuItem";
            this.selectAllToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+A";
            this.selectAllToolStripMenuItem.Size = new System.Drawing.Size( 178, 22 );
            this.selectAllToolStripMenuItem.Text = "Select &All";
            this.selectAllToolStripMenuItem.Click += new System.EventHandler( this.selectAllToolStripMenuItem_Click );
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size( 175, 6 );
            // 
            // boldToolStripMenuItem
            // 
            this.boldToolStripMenuItem.CheckOnClick = true;
            this.boldToolStripMenuItem.Image = global::LocalizedTextControls.Properties.Resources.boldhs;
            this.boldToolStripMenuItem.Name = "boldToolStripMenuItem";
            this.boldToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.B)));
            this.boldToolStripMenuItem.Size = new System.Drawing.Size( 178, 22 );
            this.boldToolStripMenuItem.Text = "&Bold";
            this.boldToolStripMenuItem.Click += new System.EventHandler( this.boldToolStripMenuItem_Click );
            // 
            // italicToolStripMenuItem
            // 
            this.italicToolStripMenuItem.CheckOnClick = true;
            this.italicToolStripMenuItem.Image = global::LocalizedTextControls.Properties.Resources.ItalicHS;
            this.italicToolStripMenuItem.Name = "italicToolStripMenuItem";
            this.italicToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.I)));
            this.italicToolStripMenuItem.Size = new System.Drawing.Size( 178, 22 );
            this.italicToolStripMenuItem.Text = "&Italic";
            this.italicToolStripMenuItem.Click += new System.EventHandler( this.italicToolStripMenuItem_Click );
            // 
            // underlineToolStripMenuItem
            // 
            this.underlineToolStripMenuItem.CheckOnClick = true;
            this.underlineToolStripMenuItem.Image = global::LocalizedTextControls.Properties.Resources.UnderlineHS;
            this.underlineToolStripMenuItem.Name = "underlineToolStripMenuItem";
            this.underlineToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.U)));
            this.underlineToolStripMenuItem.Size = new System.Drawing.Size( 178, 22 );
            this.underlineToolStripMenuItem.Text = "&Underline";
            this.underlineToolStripMenuItem.Click += new System.EventHandler( this.underlineToolStripMenuItem_Click );
            // 
            // colorToolStripMenuItem
            // 
            this.colorToolStripMenuItem.DropDownItems.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.autoToolStripMenuItem,
            this.chooseToolStripMenuItem,
            this.toolStripSeparator1,
            this.blackToolStripMenuItem,
            this.whiteToolStripMenuItem,
            this.grayToolStripMenuItem,
            this.silverToolStripMenuItem,
            this.maroonToolStripMenuItem,
            this.redToolStripMenuItem,
            this.oliveToolStripMenuItem,
            this.yellowToolStripMenuItem,
            this.greenToolStripMenuItem,
            this.limeToolStripMenuItem,
            this.tealToolStripMenuItem,
            this.aquaToolStripMenuItem,
            this.navyToolStripMenuItem,
            this.blueToolStripMenuItem,
            this.purpleToolStripMenuItem,
            this.fuchsiaToolStripMenuItem} );
            this.colorToolStripMenuItem.Image = global::LocalizedTextControls.Properties.Resources.Color_fontHS;
            this.colorToolStripMenuItem.Name = "colorToolStripMenuItem";
            this.colorToolStripMenuItem.Size = new System.Drawing.Size( 178, 22 );
            this.colorToolStripMenuItem.Text = "C&olor";
            // 
            // autoToolStripMenuItem
            // 
            this.autoToolStripMenuItem.Name = "autoToolStripMenuItem";
            this.autoToolStripMenuItem.Size = new System.Drawing.Size( 133, 22 );
            this.autoToolStripMenuItem.Text = "&Auto";
            this.autoToolStripMenuItem.Click += new System.EventHandler( this.autoToolStripMenuItem_Click );
            // 
            // chooseToolStripMenuItem
            // 
            this.chooseToolStripMenuItem.Name = "chooseToolStripMenuItem";
            this.chooseToolStripMenuItem.Size = new System.Drawing.Size( 133, 22 );
            this.chooseToolStripMenuItem.Text = "&Choose...";
            this.chooseToolStripMenuItem.Click += new System.EventHandler( this.chooseToolStripMenuItem_Click );
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size( 130, 6 );
            // 
            // blackToolStripMenuItem
            // 
            this.blackToolStripMenuItem.Image = global::LocalizedTextControls.Properties.Resources.black;
            this.blackToolStripMenuItem.Name = "blackToolStripMenuItem";
            this.blackToolStripMenuItem.Size = new System.Drawing.Size( 133, 22 );
            this.blackToolStripMenuItem.Text = "&Black";
            this.blackToolStripMenuItem.Click += new System.EventHandler( this.blackToolStripMenuItem_Click );
            // 
            // whiteToolStripMenuItem
            // 
            this.whiteToolStripMenuItem.Image = global::LocalizedTextControls.Properties.Resources.white;
            this.whiteToolStripMenuItem.Name = "whiteToolStripMenuItem";
            this.whiteToolStripMenuItem.Size = new System.Drawing.Size( 133, 22 );
            this.whiteToolStripMenuItem.Text = "&White";
            this.whiteToolStripMenuItem.Click += new System.EventHandler( this.whiteToolStripMenuItem_Click );
            // 
            // grayToolStripMenuItem
            // 
            this.grayToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject( "grayToolStripMenuItem.Image" )));
            this.grayToolStripMenuItem.Name = "grayToolStripMenuItem";
            this.grayToolStripMenuItem.Size = new System.Drawing.Size( 133, 22 );
            this.grayToolStripMenuItem.Text = "&Gray";
            this.grayToolStripMenuItem.Click += new System.EventHandler( this.grayToolStripMenuItem_Click );
            // 
            // silverToolStripMenuItem
            // 
            this.silverToolStripMenuItem.Image = global::LocalizedTextControls.Properties.Resources.silver;
            this.silverToolStripMenuItem.Name = "silverToolStripMenuItem";
            this.silverToolStripMenuItem.Size = new System.Drawing.Size( 133, 22 );
            this.silverToolStripMenuItem.Text = "&Silver";
            this.silverToolStripMenuItem.Click += new System.EventHandler( this.silverToolStripMenuItem_Click );
            // 
            // maroonToolStripMenuItem
            // 
            this.maroonToolStripMenuItem.Image = global::LocalizedTextControls.Properties.Resources.maroon;
            this.maroonToolStripMenuItem.Name = "maroonToolStripMenuItem";
            this.maroonToolStripMenuItem.Size = new System.Drawing.Size( 133, 22 );
            this.maroonToolStripMenuItem.Text = "&Maroon";
            this.maroonToolStripMenuItem.Click += new System.EventHandler( this.maroonToolStripMenuItem_Click );
            // 
            // redToolStripMenuItem
            // 
            this.redToolStripMenuItem.Image = global::LocalizedTextControls.Properties.Resources.red;
            this.redToolStripMenuItem.Name = "redToolStripMenuItem";
            this.redToolStripMenuItem.Size = new System.Drawing.Size( 133, 22 );
            this.redToolStripMenuItem.Text = "&Red";
            this.redToolStripMenuItem.Click += new System.EventHandler( this.redToolStripMenuItem_Click );
            // 
            // oliveToolStripMenuItem
            // 
            this.oliveToolStripMenuItem.Image = global::LocalizedTextControls.Properties.Resources.olive;
            this.oliveToolStripMenuItem.Name = "oliveToolStripMenuItem";
            this.oliveToolStripMenuItem.Size = new System.Drawing.Size( 133, 22 );
            this.oliveToolStripMenuItem.Text = "&Olive";
            this.oliveToolStripMenuItem.Click += new System.EventHandler( this.oliveToolStripMenuItem_Click );
            // 
            // yellowToolStripMenuItem
            // 
            this.yellowToolStripMenuItem.Image = global::LocalizedTextControls.Properties.Resources.yellow;
            this.yellowToolStripMenuItem.Name = "yellowToolStripMenuItem";
            this.yellowToolStripMenuItem.Size = new System.Drawing.Size( 133, 22 );
            this.yellowToolStripMenuItem.Text = "&Yellow";
            this.yellowToolStripMenuItem.Click += new System.EventHandler( this.yellowToolStripMenuItem_Click );
            // 
            // greenToolStripMenuItem
            // 
            this.greenToolStripMenuItem.Image = global::LocalizedTextControls.Properties.Resources.green;
            this.greenToolStripMenuItem.Name = "greenToolStripMenuItem";
            this.greenToolStripMenuItem.Size = new System.Drawing.Size( 133, 22 );
            this.greenToolStripMenuItem.Text = "Gr&een";
            this.greenToolStripMenuItem.Click += new System.EventHandler( this.greenToolStripMenuItem_Click );
            // 
            // limeToolStripMenuItem
            // 
            this.limeToolStripMenuItem.Image = global::LocalizedTextControls.Properties.Resources.lime;
            this.limeToolStripMenuItem.Name = "limeToolStripMenuItem";
            this.limeToolStripMenuItem.Size = new System.Drawing.Size( 133, 22 );
            this.limeToolStripMenuItem.Text = "&Lime";
            this.limeToolStripMenuItem.Click += new System.EventHandler( this.limeToolStripMenuItem_Click );
            // 
            // tealToolStripMenuItem
            // 
            this.tealToolStripMenuItem.Image = global::LocalizedTextControls.Properties.Resources.teal;
            this.tealToolStripMenuItem.Name = "tealToolStripMenuItem";
            this.tealToolStripMenuItem.Size = new System.Drawing.Size( 133, 22 );
            this.tealToolStripMenuItem.Text = "&Teal";
            this.tealToolStripMenuItem.Click += new System.EventHandler( this.tealToolStripMenuItem_Click );
            // 
            // aquaToolStripMenuItem
            // 
            this.aquaToolStripMenuItem.Image = global::LocalizedTextControls.Properties.Resources.aqua;
            this.aquaToolStripMenuItem.Name = "aquaToolStripMenuItem";
            this.aquaToolStripMenuItem.Size = new System.Drawing.Size( 133, 22 );
            this.aquaToolStripMenuItem.Text = "A&qua";
            this.aquaToolStripMenuItem.Click += new System.EventHandler( this.aquaToolStripMenuItem_Click );
            // 
            // navyToolStripMenuItem
            // 
            this.navyToolStripMenuItem.Image = global::LocalizedTextControls.Properties.Resources.navy;
            this.navyToolStripMenuItem.Name = "navyToolStripMenuItem";
            this.navyToolStripMenuItem.Size = new System.Drawing.Size( 133, 22 );
            this.navyToolStripMenuItem.Text = "&Navy";
            this.navyToolStripMenuItem.Click += new System.EventHandler( this.navyToolStripMenuItem_Click );
            // 
            // blueToolStripMenuItem
            // 
            this.blueToolStripMenuItem.Image = global::LocalizedTextControls.Properties.Resources.blue;
            this.blueToolStripMenuItem.Name = "blueToolStripMenuItem";
            this.blueToolStripMenuItem.Size = new System.Drawing.Size( 133, 22 );
            this.blueToolStripMenuItem.Text = "Bl&ue";
            this.blueToolStripMenuItem.Click += new System.EventHandler( this.blueToolStripMenuItem_Click );
            // 
            // purpleToolStripMenuItem
            // 
            this.purpleToolStripMenuItem.Image = global::LocalizedTextControls.Properties.Resources.purple;
            this.purpleToolStripMenuItem.Name = "purpleToolStripMenuItem";
            this.purpleToolStripMenuItem.Size = new System.Drawing.Size( 133, 22 );
            this.purpleToolStripMenuItem.Text = "&Purple";
            this.purpleToolStripMenuItem.Click += new System.EventHandler( this.purpleToolStripMenuItem_Click );
            // 
            // fuchsiaToolStripMenuItem
            // 
            this.fuchsiaToolStripMenuItem.Image = global::LocalizedTextControls.Properties.Resources.fuchsia;
            this.fuchsiaToolStripMenuItem.Name = "fuchsiaToolStripMenuItem";
            this.fuchsiaToolStripMenuItem.Size = new System.Drawing.Size( 133, 22 );
            this.fuchsiaToolStripMenuItem.Text = "&Fuchsia";
            this.fuchsiaToolStripMenuItem.Click += new System.EventHandler( this.fuchsiaToolStripMenuItem_Click );
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size( 175, 6 );
            // 
            // insertVariableToolStripMenuItem
            // 
            this.insertVariableToolStripMenuItem.Name = "insertVariableToolStripMenuItem";
            this.insertVariableToolStripMenuItem.Size = new System.Drawing.Size( 178, 22 );
            this.insertVariableToolStripMenuItem.Text = "Insert &Variable...";
            this.insertVariableToolStripMenuItem.Click += new System.EventHandler( this.insertVariableToolStripMenuItem_Click );
            // 
            // multiLineEditorToolStripMenuItem
            // 
            this.multiLineEditorToolStripMenuItem.Name = "multiLineEditorToolStripMenuItem";
            this.multiLineEditorToolStripMenuItem.Size = new System.Drawing.Size( 178, 22 );
            this.multiLineEditorToolStripMenuItem.Text = "&Multi-line Editor...";
            this.multiLineEditorToolStripMenuItem.Click += new System.EventHandler( this.multiLineEditorToolStripMenuItem_Click );
            // 
            // timer
            // 
            this.timer.Enabled = true;
            this.timer.Tick += new System.EventHandler( this.timer_Tick );
            // 
            // LocalizedRichTextBox
            // 
            this.ContextMenuStrip = this.contextMenuStrip;
            this.DetectUrls = false;
            this.KeyDown += new System.Windows.Forms.KeyEventHandler( this.LocalizedRichTextBox_KeyDown );
            this.SelectionChanged += new System.EventHandler( this.LocalizedRichTextBox_SelectionChanged );
            this.contextMenuStrip.ResumeLayout( false );
            this.ResumeLayout( false );

        }

        #endregion

        private System.Windows.Forms.ContextMenuStrip contextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem colorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem boldToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem italicToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem underlineToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem insertVariableToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem autoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem chooseToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem blackToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem whiteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem grayToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem silverToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem maroonToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem redToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem oliveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem yellowToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem greenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem limeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tealToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aquaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem navyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem blueToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem purpleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fuchsiaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem copyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pasteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem selectAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem undoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem redoToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripMenuItem deleteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem multiLineEditorToolStripMenuItem;
        private System.Windows.Forms.Timer timer;
    }
}
