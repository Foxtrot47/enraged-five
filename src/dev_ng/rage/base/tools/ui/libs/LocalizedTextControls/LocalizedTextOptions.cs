using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Text;
using System.Xml.Serialization;
using System.Windows.Forms;

using RSG.Base.Serialization;

namespace LocalizedTextControls
{
    /// <summary>
    /// Holds the options that determine the available Fonts, Languages, and Flags on a LocalizedTextEditorControl.
    /// </summary>
    [Serializable]
    public class LocalizedTextOptions : rageSerializableObject
    {
        public LocalizedTextOptions()
        {
            Reset();
        }

        #region Variables
        private int m_screenWidth = 1280;
        private int m_screenHeight = 720;

        private LocalizedTextFontOptionCollection m_fontOptionCollection = new LocalizedTextFontOptionCollection();
        private LocalizedTextLanguageOptionCollection m_languageOptionCollection = new LocalizedTextLanguageOptionCollection();
        private LocalizedTextFlagOptionCollection m_flagOptionCollection = new LocalizedTextFlagOptionCollection();
        private LocalizedTextVariableOptionCollection m_variableOptionCollection = new LocalizedTextVariableOptionCollection();
        private LocalizedTextRegionOptionCollection m_regionOptionCollection = new LocalizedTextRegionOptionCollection();
        private LocalizedTextPlatformOptionCollection m_platformOptionCollection = new LocalizedTextPlatformOptionCollection();
        
        private int m_defaultFontOptionIndex = -1;
        private int m_defaultLanguageOptionIndex = -1;
        private int m_defaultRegionOptionIndex = -1;
        private int m_defaultPlatformOptionIndex = -1;

        private string m_textRendererPluginFilename = string.Empty;
        private Assembly m_textRendererAssembly = null;

        private string m_textExporterPluginFilename = string.Empty;
        private Assembly m_textExporterAssembly = null;
        #endregion

        #region Properties
        /// <summary>
        /// The game's screen width.
        /// </summary>
        [Description( "The game's screen width." )]
        public int ScreenWidth
        {
            get
            {
                return m_screenWidth;
            }
            set
            {
                m_screenWidth = value;
            }
        }

        /// <summary>
        /// The game's screen height.
        /// </summary>
        [Description( "The game's screen height." )]
        public int ScreenHeight
        {
            get
            {
                return m_screenHeight;
            }
            set
            {
                m_screenHeight = value;
            }
        }

        [XmlIgnore, DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public Size ScreenResolution
        {
            get
            {
                return new Size( this.ScreenWidth, this.ScreenHeight );
            }
        }

        /// <summary>
        /// The collection of LocalizedTextFontOptions.
        /// </summary>
        [Description( "The collection of LocalizedTextFontOptions." )]
        public LocalizedTextFontOptionCollection FontOptionCollection
        {
            get
            {
                return m_fontOptionCollection;
            }
            set
            {
                if ( value != null )
                {
                    m_fontOptionCollection = value;
                }
                else
                {
                    m_fontOptionCollection.Clear();
                }
            }
        }

        /// <summary>
        /// The index of the default LocalizedTextFontOption in FontOptionCollection.
        /// </summary>
        [DefaultValue( -1 ), Description( "The index of the default LocalizedTextFontOption in FontOptionCollection." )]
        public int DefaultFontOptionIndex
        {
            get
            {
                return m_defaultFontOptionIndex;
            }
            set
            {
                m_defaultFontOptionIndex = value;
            }
        }

        [XmlIgnore, ReadOnly( true ), DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public LocalizedTextFontOption DefaultFontOption
        {
            get
            {
                if ( (this.DefaultFontOptionIndex >= 0) && (this.DefaultFontOptionIndex < this.FontOptionCollection.Count) )
                {
                    return this.FontOptionCollection[this.DefaultFontOptionIndex];
                }
                else
                {
                    return new LocalizedTextFontOption();
                }
            }
            set
            {
                if ( value != null )
                {
                    this.DefaultFontOptionIndex = this.FontOptionCollection.IndexOf( value );
                }
                else
                {
                    this.DefaultFontOptionIndex = -1;
                }
            }
        }

        /// <summary>
        /// The collection of LocalizedTextLanguageOptions.
        /// </summary>
        [Description( "The collection of LocalizedTextLanguageOptions." )]
        public LocalizedTextLanguageOptionCollection LanguageOptionCollection
        {
            get
            {
                return m_languageOptionCollection;
            }
            set
            {
                if ( value != null )
                {
                    m_languageOptionCollection = value;
                }
                else
                {
                    m_languageOptionCollection.Clear();
                }
            }
        }

        /// <summary>
        /// The index of the default LocalizedTextLanguageOption in LanguageOptionCollection.
        /// </summary>
        [DefaultValue( -1 ), Description( "The index of the default LocalizedTextLanguageOption in LanguageOptionCollection." )]
        public int DefaultLanguageOptionIndex
        {
            get
            {
                return m_defaultLanguageOptionIndex;
            }
            set
            {
                m_defaultLanguageOptionIndex = value;
            }
        }

        [XmlIgnore, ReadOnly( true ), DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public LocalizedTextLanguageOption DefaultLanguageOption
        {
            get
            {
                if ( (this.DefaultLanguageOptionIndex >= 0) && (this.DefaultLanguageOptionIndex < this.LanguageOptionCollection.Count) )
                {
                    return this.LanguageOptionCollection[this.DefaultLanguageOptionIndex];
                }
                else
                {
                    return new LocalizedTextLanguageOption();
                }
            }
            set
            {
                if ( value != null )
                {
                    this.DefaultLanguageOptionIndex = this.LanguageOptionCollection.IndexOf( value );
                }
                else
                {
                    this.DefaultLanguageOptionIndex = -1;
                }
            }
        }

        /// <summary>
        /// The collection of LocalizedTextFlagOptions.
        /// </summary>
        [Description( "The collection of LocalizedTextFlagOptions." )]
        public LocalizedTextFlagOptionCollection FlagOptionCollection
        {
            get
            {
                return m_flagOptionCollection;
            }
            set
            {
                if ( value != null )
                {
                    m_flagOptionCollection = value;
                }
                else
                {
                    m_flagOptionCollection.Clear();
                }
            }
        }

        /// <summary>
        /// The collection of LocalizedTextVariableOptions.
        /// </summary>
        [Description( "The collection of LocalizedTextVariableOptions." )]
        public LocalizedTextVariableOptionCollection VariableOptionCollection
        {
            get
            {
                return m_variableOptionCollection;
            }
            set
            {
                if ( value != null )
                {
                    m_variableOptionCollection = value;
                }
                else
                {
                    m_variableOptionCollection.Clear();
                }
            }
        }

        /// <summary>
        /// The index of the default LocalizedTextRegionOption in RegionOptionCollection.
        /// </summary>
        [DefaultValue( -1 ), Description( "The index of the default LocalizedTextRegionOption in RegionOptionCollection." )]
        public int DefaultRegionOptionIndex
        {
            get
            {
                return m_defaultRegionOptionIndex;
            }
            set
            {
                m_defaultRegionOptionIndex = value;
            }
        }

        [XmlIgnore, ReadOnly( true ), DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public LocalizedTextRegionOption DefaultRegionOption
        {
            get
            {
                if ( (this.DefaultRegionOptionIndex >= 0) && (this.DefaultRegionOptionIndex < this.RegionOptionCollection.Count) )
                {
                    return this.RegionOptionCollection[this.DefaultRegionOptionIndex];
                }
                else
                {
                    return new LocalizedTextRegionOption();
                }
            }
            set
            {
                if ( value != null )
                {
                    this.DefaultRegionOptionIndex = this.RegionOptionCollection.IndexOf( value );
                }
                else
                {
                    this.DefaultRegionOptionIndex = -1;
                }
            }
        }

        /// <summary>
        /// The collection of LocalizedTextRegionOptions.
        /// </summary>
        [Description( "The collection of LocalizedTextRegionOptions." )]
        public LocalizedTextRegionOptionCollection RegionOptionCollection
        {
            get
            {
                return m_regionOptionCollection;
            }
            set
            {
                if ( value != null )
                {
                    m_regionOptionCollection = value;
                }
                else
                {
                    m_regionOptionCollection.Clear();
                }
            }
        }

        /// <summary>
        /// The index of the default LocalizedTextPlatformOption in PlatformOptionCollection.
        /// </summary>
        [DefaultValue( -1 ), Description( "The index of the default LocalizedTextPlatformOption in PlatformOptionCollection." )]
        public int DefaultPlatformOptionIndex
        {
            get
            {
                return m_defaultPlatformOptionIndex;
            }
            set
            {
                m_defaultPlatformOptionIndex = value;
            }
        }

        [XmlIgnore, ReadOnly( true ), DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public LocalizedTextPlatformOption DefaultPlatformOption
        {
            get
            {
                if ( (this.DefaultPlatformOptionIndex >= 0) && (this.DefaultPlatformOptionIndex < this.PlatformOptionCollection.Count) )
                {
                    return this.PlatformOptionCollection[this.DefaultPlatformOptionIndex];
                }
                else
                {
                    return new LocalizedTextPlatformOption();
                }
            }
            set
            {
                if ( value != null )
                {
                    this.DefaultPlatformOptionIndex = this.PlatformOptionCollection.IndexOf( value );
                }
                else
                {
                    this.DefaultPlatformOptionIndex = -1;
                }
            }
        }

        /// <summary>
        /// The collection of LocalizedTextPlatformOptions.
        /// </summary>
        [Description( "The collection of LocalizedTextPlatformOptions." )]
        public LocalizedTextPlatformOptionCollection PlatformOptionCollection
        {
            get
            {
                return m_platformOptionCollection;
            }
            set
            {
                if ( value != null )
                {
                    m_platformOptionCollection = value;
                }
                else
                {
                    m_platformOptionCollection.Clear();
                }
            }
        }


        /// <summary>
        /// The filename of the game-specific Text Renderer Plugin to use for displaying the localized text.
        /// </summary>
        [DefaultValue( "" ), Description( "The filename of the game-specific Text Renderer Plugin to use for displaying the localized text." )]
        public string TextRendererPluginFilename
        {
            get
            {
                return m_textRendererPluginFilename;
            }
            set
            {
                if ( value != null )
                {
                    m_textRendererPluginFilename = value;
                }
                else
                {
                    m_textRendererPluginFilename = string.Empty;
                }
            }
        }

        /// <summary>
        /// The filename of the game-specific Text Exporter Plugin to use for exporting the localized text.
        /// </summary>
        [DefaultValue( "" ), Description( "The filename of the game-specific Text Exporter Plugin to use for exporting the localized text." )]
        public string TextExporterPluginFilename
        {
            get
            {
                return m_textExporterPluginFilename;
            }
            set
            {
                if ( value != null )
                {
                    m_textExporterPluginFilename = value;
                }
                else
                {
                    m_textExporterPluginFilename = string.Empty;
                }
            }
        }
        #endregion

        #region Overrides
        public override bool Equals( object obj )
        {
            if ( obj == null )
            {
                return false;
            }

            if ( base.Equals( obj ) )
            {
                return true;
            }

            if ( obj is LocalizedTextOptions )
            {
                return Equals( obj as LocalizedTextOptions );
            }

            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override object Clone()
        {
            LocalizedTextOptions options = new LocalizedTextOptions();
            options.CopyFrom( this );
            return options;
        }

        public override void CopyFrom( IRageClonableObject other )
        {
            if ( other == null )
            {
                return;
            }

            if ( base.Equals( (object)other ) )
            {
                return;
            }

            if ( other is LocalizedTextOptions )
            {
                CopyFrom( other as LocalizedTextOptions );
            }
        }

        public override void Reset()
        {
            base.Reset();

            this.DefaultFontOptionIndex = -1;
            this.DefaultLanguageOptionIndex = -1;
            this.DefaultPlatformOptionIndex = -1;
            this.DefaultRegionOptionIndex = -1;
            this.FlagOptionCollection.Reset();
            this.FontOptionCollection.Reset();
            this.LanguageOptionCollection.Reset();
            this.PlatformOptionCollection.Reset();
            this.RegionOptionCollection.Reset();
            this.ScreenHeight = 720;
            this.ScreenWidth = 1280;
            this.TextExporterPluginFilename = string.Empty;
            this.TextRendererPluginFilename = string.Empty;            
            this.VariableOptionCollection.Reset();

            this.FlagOptionCollection.Add( new LocalizedTextFlagOption( "Enabled" ) ); // Always have the Enabled flag

            UnloadTextExporterPlugin();
            UnloadTextRendererPlugin();
        }
        #endregion

        #region Public Functions
        public void CopyFrom( LocalizedTextOptions other )
        {
            if ( other == null )
            {
                return;
            }

            if ( base.Equals( (object)other ) )
            {
                return;
            }

            base.CopyFrom( other as rageSerializableObject );
            
            this.DefaultFontOptionIndex = other.DefaultFontOptionIndex;
            this.DefaultLanguageOptionIndex = other.DefaultLanguageOptionIndex;
            this.DefaultPlatformOptionIndex = other.DefaultPlatformOptionIndex;
            this.DefaultRegionOptionIndex = other.DefaultRegionOptionIndex;
            this.FlagOptionCollection.CopyFrom( other.FlagOptionCollection );
            this.FontOptionCollection.CopyFrom( other.FontOptionCollection );
            this.LanguageOptionCollection.CopyFrom( other.LanguageOptionCollection );
            this.PlatformOptionCollection.CopyFrom( other.PlatformOptionCollection);
            this.RegionOptionCollection.CopyFrom( other.RegionOptionCollection);
            this.ScreenHeight = other.ScreenHeight;
            this.ScreenWidth = other.ScreenWidth;
            this.TextExporterPluginFilename = other.TextExporterPluginFilename;
            this.TextRendererPluginFilename = other.TextRendererPluginFilename;
            this.VariableOptionCollection.CopyFrom( other.VariableOptionCollection );

            // Make sure we don't have multiple Flag Options named "Enabled"
            bool foundEnabled = false;
            for ( int i = 0; i < this.FlagOptionCollection.Count; ++i )
            {
                if ( this.FlagOptionCollection[i].Name == "Enabled" )
                {
                    if ( foundEnabled )
                    {
                        this.FlagOptionCollection.RemoveAt( i );
                        --i;
                    }
                    else
                    {
                        foundEnabled = true;
                    }
                }
            }
            
            m_textRendererAssembly = other.m_textRendererAssembly;
        }

        public bool Equals( LocalizedTextOptions other )
        {
            if ( other == null )
            {
                return false;
            }

            if ( base.Equals( other as rageSerializableObject ) )
            {
                return true;
            }

            return (this.DefaultFontOptionIndex == other.DefaultFontOptionIndex)
                && (this.DefaultLanguageOptionIndex == other.DefaultLanguageOptionIndex)
                && (this.DefaultPlatformOptionIndex == other.DefaultPlatformOptionIndex)
                && (this.DefaultRegionOptionIndex == other.DefaultRegionOptionIndex)
                && this.FlagOptionCollection.Equals( other.FlagOptionCollection )
                && this.FontOptionCollection.Equals( other.FontOptionCollection )
                && this.LanguageOptionCollection.Equals( other.LanguageOptionCollection )
                && this.PlatformOptionCollection.Equals( other.PlatformOptionCollection )
                && this.RegionOptionCollection.Equals( other.RegionOptionCollection )
                && (this.ScreenHeight == other.ScreenHeight)
                && (this.ScreenWidth == other.ScreenWidth)
                && (this.TextExporterPluginFilename == other.TextExporterPluginFilename)
                && (this.TextRendererPluginFilename == other.TextRendererPluginFilename)
                && this.VariableOptionCollection.Equals( other.VariableOptionCollection );
        }

        public void LoadTextRendererPlugin()
        {
            if ( String.IsNullOrEmpty( this.TextRendererPluginFilename ) )
            {
                return;
            }

            if ( m_textRendererAssembly == null )
            {
                string relativeFilename = GetRelativePath( this.TextRendererPluginFilename );
                string absoluteFilename = GetAbsolutePath( relativeFilename );

                if ( File.Exists( absoluteFilename ) )
                {
                    m_textRendererAssembly = Assembly.LoadFrom( absoluteFilename );
                }
            }
        }

        public void UnloadTextRendererPlugin()
        {
            // FIXME: is there a way to unload a plugin?
            m_textRendererAssembly = null;
        }

        public ILocalizedTextRenderer CreateTextRenderer()
        {
            if ( m_textRendererAssembly != null )
            {
                // Check for special types in the DLL.
                foreach ( Type t in m_textRendererAssembly.GetTypes() )
                {
                    if ( t.IsAbstract )
                    {
                        continue; // skip abstract classes
                    }

                    if ( t.GetInterface( "LocalizedTextControls.ILocalizedTextRenderer" ) != null )
                    {
                        // construct
                        return t.GetConstructor( System.Type.EmptyTypes ).Invoke( null ) as ILocalizedTextRenderer;
                    }
                }
            }

            return new DefaultLocalizedTextRenderer();
        }

        public void LoadTextExporterPlugin()
        {
            if ( String.IsNullOrEmpty( this.TextExporterPluginFilename ) )
            {
                return;
            }

            if ( m_textExporterAssembly == null )
            {
                string relativeFilename = GetRelativePath( this.TextExporterPluginFilename );
                string absoluteFilename = GetAbsolutePath( relativeFilename );

                if ( File.Exists( absoluteFilename ) )
                {
                    m_textExporterAssembly = Assembly.LoadFrom( absoluteFilename );
                }
            }
        }

        public void UnloadTextExporterPlugin()
        {
            // FIXME: is there a way to unload a plugin?
            m_textExporterAssembly = null;
        }

        public ILocalizedTextExporter CreateTextExporter()
        {
            if ( m_textExporterAssembly != null )
            {
                // Check for special types in the DLL.
                foreach ( Type t in m_textExporterAssembly.GetTypes() )
                {
                    if ( t.IsAbstract )
                    {
                        continue; // skip abstract classes
                    }

                    if ( t.GetInterface( "LocalizedTextControls.ILocalizedTextExporter" ) != null )
                    {
                        // construct
                        return t.GetConstructor( System.Type.EmptyTypes ).Invoke( null ) as ILocalizedTextExporter;
                    }
                }
            }

            return new DefaultLocalizedTextExporter();
        }
        #endregion
    }
}
