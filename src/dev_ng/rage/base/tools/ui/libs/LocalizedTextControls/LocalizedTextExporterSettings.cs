using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Text;

using RSG.Base.Serialization;

namespace LocalizedTextControls
{
    /// <summary>
    /// A class that contains the information for exporting a LocalizedTextDataCollection.
    /// </summary>
    public class LocalizedTextExporterSettings : IRageClonableObject
    {
        public LocalizedTextExporterSettings()
        {
            Reset();
        }

        #region Variables
        private string m_stringTableTextExportFilename = string.Empty;
        private bool m_makeStringTable = true;
        private string m_makeStringTableExecutableFilename = string.Empty;
        private string m_stringTableExportFilename = string.Empty;
        #endregion

        #region Properties
        /// <summary>
        /// The path where the stringtable.txt file should be saved.  It will have the same name as the SubtitleProjectData's filename.
        /// </summary>
        [Description( "The path where the stringtable.txt file should be saved.  It will have the same name as the SubtitleProjectData's filename." )]
        public string StringTableTextOutputPath
        {
            get
            {
                return m_stringTableTextExportFilename;
            }
            set
            {
                if ( value != null )
                {
                    m_stringTableTextExportFilename = value;
                }
                else
                {
                    m_stringTableTextExportFilename = string.Empty;
                }
            }
        }

        /// <summary>
        /// Whether or not convert the stringtable.txt to stringtable.strtbl.
        /// </summary>
        [Description( "Whether or not convert the stringtable.txt to stringtable.strtbl." )]
        public bool MakeStringTable
        {
            get
            {
                return m_makeStringTable;
            }
            set
            {
                m_makeStringTable = value;
            }
        }

        /// <summary>
        /// The full path and filename of the mkstrtbl executable that converts the stringtable.txt to stringtable.strtbl.
        /// </summary>
        [Description( "The full path and filename of the mkstrtbl executable that converts the stringtable.txt to stringtable.strtbl." )]
        public string MakeStringTableExecutableFilename
        {
            get
            {
                return m_makeStringTableExecutableFilename;
            }
            set
            {
                if ( value != null )
                {
                    m_makeStringTableExecutableFilename = value;
                }
                else
                {
                    m_makeStringTableExecutableFilename = string.Empty;
                }
            }
        }

        /// <summary>
        /// The path where the converted file should be saved.  It will have the same name as the SubtitleProjectData's filename.
        /// </summary>
        [Description( "The path where the converted file should be saved.  It will have the same name as the SubtitleProjectData's filename." )]
        public string StringTableOutputPath
        {
            get
            {
                return m_stringTableExportFilename;
            }
            set
            {
                if ( value != null )
                {
                    m_stringTableExportFilename = value;
                }
                else
                {
                    m_stringTableExportFilename = string.Empty;
                }
            }
        }
        #endregion

        #region Overrides
        public override bool Equals( object obj )
        {
            if ( obj == null )
            {
                return false;
            }

            if ( base.Equals( obj ) )
            {
                return true;
            }

            if ( obj is LocalizedTextExporterSettings )
            {
                return Equals( obj as LocalizedTextExporterSettings );
            }

            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        #endregion

        #region IRageClonableObject
        public object Clone()
        {
            LocalizedTextExporterSettings settings = new LocalizedTextExporterSettings();
            settings.CopyFrom( this );
            return settings;
        }

        public void CopyFrom( IRageClonableObject other )
        {
            if ( other == null )
            {
                return;
            }

            if ( base.Equals( (object)other ) )
            {
                return;
            }

            if ( other is LocalizedTextExporterSettings )
            {
                CopyFrom( other as LocalizedTextExporterSettings );
            }
        }

        public void Reset()
        {
            this.MakeStringTable = false;
            this.MakeStringTableExecutableFilename = "..\\mkstrtbl.exe";
            this.StringTableOutputPath = string.Empty;
            this.StringTableTextOutputPath = string.Empty;
        }
        #endregion

        #region Public Functions
        public void CopyFrom( LocalizedTextExporterSettings other )
        {
            if ( other == null )
            {
                return;
            }

            if ( base.Equals( (object)other ) )
            {
                return;
            }

            this.MakeStringTable = other.MakeStringTable;
            this.MakeStringTableExecutableFilename = other.MakeStringTableExecutableFilename;
            this.StringTableOutputPath = other.StringTableOutputPath;
            this.StringTableTextOutputPath = other.StringTableTextOutputPath;
        }

        public bool Equals( LocalizedTextExporterSettings other )
        {
            if ( other == null )
            {
                return false;
            }

            if ( base.Equals( (object)other ) )
            {
                return true;
            }

            return (this.MakeStringTable == other.MakeStringTable)
                && (this.MakeStringTableExecutableFilename == other.MakeStringTableExecutableFilename)
                && (this.StringTableOutputPath == other.StringTableOutputPath)
                && (this.StringTableTextOutputPath == other.StringTableTextOutputPath);
        }
        #endregion
    }
}
