using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Drawing.Text;

namespace LocalizedTextControls
{
    public class DefaultLocalizedTextRenderer : ILocalizedTextRenderer
    {
        public DefaultLocalizedTextRenderer()
        {

        }

        #region ILocalizedTextRenderer Interface
        internal class DrawText
        {
            public DrawText()
            {

            }

            public string Text;
            public Font Font;
            public Brush Brush;
        }

        public void Render( LocalizedTextData data, LocalizedTextDataCollection collection, LocalizedTextOptions options, ref Bitmap bmp )
        {
            if ( bmp == null )
            {
                bmp = new Bitmap( options.ScreenWidth, options.ScreenHeight, PixelFormat.Format32bppArgb );
            }

            Graphics g = Graphics.FromImage( bmp );

            // Do anti-aliased drawings
            g.SmoothingMode = SmoothingMode.AntiAlias;
            g.TextRenderingHint = TextRenderingHint.AntiAlias;

            // Clear the bitmap with complete transparency
            g.Clear( Color.Transparent );            

            Dictionary<LocalizedTextData, string> resolvedItems = new Dictionary<LocalizedTextData, string>();
            string rtf = collection.ResolveVariables( data.Rtf, data, resolvedItems );

            // Parse the rtf into our data structure
            RtfData rtfData = Utility.ParseRtf( rtf );
            if ( rtfData == null )
            {
                g.Dispose();
                return;
            }

            // Create a brush for each of our colors
            List<Brush> brushes = new List<Brush>();
            foreach ( RtfColorData colorData in rtfData.ColorData )
            {
                brushes.Add( new SolidBrush( colorData.Color ) );
            }

            // Create our base font
            string fontName = !String.IsNullOrEmpty( data.FontName ) ? data.FontName : "arial12";

            Font f = null;
            bool canDiposeFont = false;

            LocalizedTextFontOption fontOption = options.FontOptionCollection.Find( fontName );
            if ( (fontOption != null) && (fontOption.Font != null) )
            {
                f = fontOption.Font;
            }
            else
            {
                f = new Font( "Arial", 12 );
                canDiposeFont = true;
            }

            // Go through the RtfData and fill our DrawText lists
            List<List<DrawText>> drawTextList = new List<List<DrawText>>();
            foreach ( RtfLineData lineData in rtfData.LineData )
            {
                List<DrawText> dTextLine = new List<DrawText>();
                foreach ( RtfSpanData spanData in lineData.SpanData )
                {
                    DrawText dText = new DrawText();

                    FontStyle fontStyle = FontStyle.Regular;
                    if ( spanData.Bold )
                    {
                        fontStyle |= FontStyle.Bold;
                    }

                    if ( spanData.Italic )
                    {
                        fontStyle |= FontStyle.Italic;
                    }

                    if ( spanData.Underline )
                    {
                        fontStyle |= FontStyle.Underline;
                    }

                    dText.Text = spanData.Text.ToString();
                    dText.Brush = brushes[spanData.ColorData.Index];
                    dText.Font = new Font( f, fontStyle );

                    if ( data.Uppercase )
                    {
                        dText.Text = dText.Text.ToUpper();
                    }

                    dTextLine.Add( dText );
                }

                drawTextList.Add( dTextLine );
            }
           
            // find our extents and starting positions
            int height = drawTextList.Count * f.Height;
            int midX = (options.ScreenWidth / 2) + data.OffsetX;
            int safeZoneHeight = options.ScreenHeight * 4 / 5;
            int safeZoneBottomY = ((options.ScreenHeight - safeZoneHeight) / 2) + safeZoneHeight;
            int topY = safeZoneBottomY - (drawTextList.Count * f.Height) + data.OffsetY;

            // Go through the DrawText lists and render them into the bitmap
            foreach ( List<DrawText> dTextList in drawTextList )
            {
                // find the lengths so we can compute the middle and draw each part of the line in the correct place.
                List<float> strLengths = new List<float>();
                float totalLength = 0.0f;
                foreach ( DrawText dText in dTextList )
                {
                    SizeF strSize = g.MeasureString( dText.Text, dText.Font );
                    totalLength += strSize.Width;

                    strLengths.Add( strSize.Width );
                }

                int leftX = midX - (int)(totalLength / 2.0f);

                for ( int i = 0; i < dTextList.Count; ++i )
                {
                    g.DrawString( dTextList[i].Text, dTextList[i].Font, dTextList[i].Brush, leftX, topY );

                    leftX += (int)strLengths[i];
                }

                topY += f.Height;
            }

            // Release GDI+ objects
            foreach ( List<DrawText> dTextList in drawTextList )
            {
                foreach ( DrawText dText in dTextList )
                {
                    dText.Font.Dispose();
                }
            }

            foreach ( Brush brush in brushes )
            {
                brush.Dispose();
            }

            if ( canDiposeFont )
            {
                f.Dispose();
            }

            g.Dispose();
        }
        #endregion
    }
}
