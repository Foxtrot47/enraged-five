using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Windows.Forms;

using GlacialComponents.Controls.GlacialTreeList;

namespace LocalizedTextControls
{
    /// <summary>
    /// This Component is a helper class for the LocalizedTextEditorControl.  It manages the various Dialogs
    /// that the LocalizedTextEditorControl can open so that you can have one only one instance of each dialog
    /// in your application.  You can set the LocalizedTextEditorDialogComponent.LocalizedTextEditorControl
    /// property at any time to change the component's focus between instances of LocalizedTextEditorControl.
    /// </summary>
    public partial class LocalizedTextEditorDialogComponent : Component
    {
        public LocalizedTextEditorDialogComponent()
        {
            InitializeComponent();

            InitializeAdditionalComponents();
        }

        public LocalizedTextEditorDialogComponent( IContainer container )
        {
            container.Add( this );

            InitializeComponent();

            InitializeAdditionalComponents();
        }

        private void InitializeAdditionalComponents()
        {
            m_multiLineEditorDialog.ShowColorDialog += new ColorPickerEventHandler( localizedTextEditorControl_ShowColorDialog );
            m_multiLineEditorDialog.ShowInsertVariableDialog += new InsertVariableEventHandler( localizedTextEditorControl_ShowInsertVariableDialog );
        }

        #region Variables
        private LocalizedTextEditorControl m_localizedTextEditorControl = null;

        private ColorDialog m_colorDialog = new ColorDialog();
        private InsertVariableDialog m_insertVariableDialog = new InsertVariableDialog();
        private MultiLineEditorDialog m_multiLineEditorDialog = new MultiLineEditorDialog();
        private PreviewDialog m_previewDialog = new PreviewDialog();

        private bool m_dialogOpen = false;
        #endregion

        #region Properties
        /// <summary>
        /// The Application's HelpProvider.  When set, this control's items are added to it.
        /// </summary>
        [DefaultValue( null ), Description( "The Application's HelpProvider.  When set, this control's items are added to it." )]
        public HelpProvider HelpProvider
        {
            set
            {
                if ( value != null )
                {
                    m_insertVariableDialog.HelpNamespace = value.HelpNamespace;
                    m_multiLineEditorDialog.HelpNamespace = value.HelpNamespace;
                    m_previewDialog.HelpNamespace = value.HelpNamespace;
                }
            }
        }

        /// <summary>
        /// Returns true when a dialog managed by this component is open.  You may only switch 
        /// LocalizedTextEditorControl contexts when this is false.
        /// </summary>
        [Description( "Returns true when a dialog managed by this component is open.  You may only switch LocalizedTextEditorControl contexts when this is false." )]
        public bool DialogOpen
        {
            get
            {
                return m_dialogOpen;
            }
        }

        /// <summary>
        /// The current context for this component.
        /// </summary>
        [Category( "Data" ), Description( "The current context for this component." )]
        public LocalizedTextEditorControl LocalizedTextEditorControl
        {
            get
            {
                return m_localizedTextEditorControl;
            }
            set
            {
                if ( m_dialogOpen )
                {
                    throw (new Exception( "Please wait for open dialogs to close before attempting to set the LocalizedTextEditorControl property." ));
                }

                if ( m_localizedTextEditorControl != null )
                {
                    m_localizedTextEditorControl.ShowColorDialog -= new ColorPickerEventHandler( localizedTextEditorControl_ShowColorDialog );
                    m_localizedTextEditorControl.ShowInsertVariableDialog -= new InsertVariableEventHandler( localizedTextEditorControl_ShowInsertVariableDialog );
                    m_localizedTextEditorControl.ShowMultiLineEditorDialog -= new MultiLineEditorEventHandler( localizedTextEditorControl_ShowMultiLineEditorDialog );
                }

                m_localizedTextEditorControl = value;

                if ( m_localizedTextEditorControl != null )
                {
                    m_localizedTextEditorControl.ShowColorDialog += new ColorPickerEventHandler( localizedTextEditorControl_ShowColorDialog );
                    m_localizedTextEditorControl.ShowInsertVariableDialog += new InsertVariableEventHandler( localizedTextEditorControl_ShowInsertVariableDialog );
                    m_localizedTextEditorControl.ShowMultiLineEditorDialog += new MultiLineEditorEventHandler( localizedTextEditorControl_ShowMultiLineEditorDialog );
                }
            }
        }
        #endregion

        #region Event Handlers
        private void localizedTextEditorControl_ShowColorDialog( object sender, ColorPickerEventArgs e )
        {
            m_dialogOpen = true;

            m_colorDialog.Color = e.Color;

            DialogResult result = m_colorDialog.ShowDialog( this.LocalizedTextEditorControl.FindForm() );
            if ( result == DialogResult.OK )
            {
                e.Color = m_colorDialog.Color;
                e.Changed = true;
            }

            m_dialogOpen = false;
        }

        private void localizedTextEditorControl_ShowInsertVariableDialog( object sender, InsertVariableEventArgs e )
        {
            m_dialogOpen = true;

            LocalizedTextData currentData = null;

            // Try to determine the current selection.  We don't want that in our list
            Control ctrl = sender as Control;
            if ( (ctrl.Tag != null) && (ctrl.Tag is GTLSubItem) )
            {
                GTLSubItem subItem = ctrl.Tag as GTLSubItem;
                currentData = subItem.ParentNode.Tag as LocalizedTextData;
                if ( String.IsNullOrEmpty( currentData.ID ) )
                {
                    currentData = null;
                }
            }

            if ( currentData == null )
            {
                m_dialogOpen = false;
                return;
            }

            LocalizedTextVariableOptionCollection variableCollection = new LocalizedTextVariableOptionCollection();            

            // add the available, valid Localized Text items to the list
            LocalizedTextDataCollection textCollection = this.LocalizedTextEditorControl.TextDataCollection;
            foreach ( LocalizedTextData data in textCollection )
            {
                if ( data == currentData )
                {
                    continue;
                }

                Dictionary<LocalizedTextData, bool> checkedItems = new Dictionary<LocalizedTextData,bool>();
                if ( !textCollection.IncludesThisVariable( data, currentData, checkedItems ) )
                {
                    variableCollection.Add( new LocalizedTextVariableOption( data.ID, "Localized Text", data.Text ) );
                }
            }

            // Add the extra variables, if any
            variableCollection.AddRange( this.LocalizedTextEditorControl.LocalizedTextOptions.VariableOptionCollection );

            if ( variableCollection.Count == 0 )
            {
                m_dialogOpen = false;
                return;
            }

            string variable;
            DialogResult result = m_insertVariableDialog.ShowDialog( this.LocalizedTextEditorControl.FindForm(), variableCollection, out variable );
            if ( result == DialogResult.OK )
            {
                e.Variable = variable;
                e.Changed = true;
            }

            m_dialogOpen = false;
        }

        private void localizedTextEditorControl_ShowMultiLineEditorDialog( object sender, MultiLineEditorEventArgs e )
        {
            m_dialogOpen = true;

            Control ctrl = sender as Control;
            if ( (ctrl.Tag != null) && (ctrl.Tag is GTLSubItem) )
            {
                m_multiLineEditorDialog.Tag = ctrl.Tag;
            }

            string rtf = e.Rtf;
            DialogResult result = m_multiLineEditorDialog.ShowDialog( this.LocalizedTextEditorControl.FindForm(), ref rtf );
            if ( result == DialogResult.OK )
            {
                e.Rtf = rtf;
                e.Changed = true;
            }

            m_dialogOpen = false;
        }
        #endregion
    }
}
