using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Text;

using RSG.Base.Serialization;

namespace LocalizedTextControls
{
    /// <summary>
    /// A structure that holds all of the data associated with a variable that can be added to a piece of localized text.
    /// </summary>
    [Serializable]
    public class LocalizedTextVariableOption : IRageClonableObject
    {
        public LocalizedTextVariableOption()
        {

        }

        public LocalizedTextVariableOption( string name, string variableType, string description )
        {
            m_name = name;
            m_variableType = variableType;
            m_description = description;
        }

        #region Variables
        private string m_name = string.Empty;
        private string m_variableType = string.Empty;
        private string m_description = string.Empty;
        #endregion

        #region Properties
        /// <summary>
        /// The name of the variable.
        /// </summary>
        [Description( "The name of the variable." ), Category( "Required" ), DefaultValue( "" )]
        public string Name
        {
            get
            {
                return m_name;
            }
            set
            {
                if ( m_name != null )
                {
                    m_name = value;
                }
                else
                {
                    m_name = string.Empty;
                }
            }
        }

        /// <summary>
        /// The type of variable this is.  For example, the LocalizedTextEditorControl will automatically
        /// construct a LocalizedTextVariableOption instance for each piece of LocalizedTextData, and their
        /// variable types will be set to "LocalizedText".
        /// </summary>
        [Description( "The type of variable this is.  For example, the LocalizedTextEditorControl will automatically construct a LocalizedTextVariableOption instance for each piece of LocalizedTextData, and their variable types will be set to \"LocalizedText\"." ), Category( "Optional" ), DefaultValue( "" )]
        public string VariableType
        {
            get
            {
                return m_variableType;
            }
            set
            {
                if ( m_variableType != null )
                {
                    m_variableType = value;
                }
                else
                {
                    m_variableType = string.Empty;
                }
            }
        }

        /// <summary>
        /// The description of this variable.
        /// </summary>
        [Description( "The description of the variable." ), Category( "Optional" ), DefaultValue( "" )]
        public string Description
        {
            get
            {
                return m_description;
            }
            set
            {
                if ( m_description != null )
                {
                    m_description = value;
                }
                else
                {
                    m_description = string.Empty;
                }
            }
        }
        #endregion

        #region Overrides
        public override bool Equals( object obj )
        {
            if ( obj == null )
            {
                return false;
            }

            if ( base.Equals( obj ) )
            {
                return true;
            }

            if ( obj is LocalizedTextVariableOption )
            {
                return Equals( obj as LocalizedTextVariableOption );
            }

            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString()
        {
            return this.Name;
        }
        #endregion

        #region IRageClonableObject
        public object Clone()
        {
            LocalizedTextVariableOption option = new LocalizedTextVariableOption();
            option.CopyFrom( this );
            return option;
        }

        public void CopyFrom( IRageClonableObject other )
        {
            if ( other == null )
            {
                return;
            }

            if ( base.Equals( (object)other ) )
            {
                return;
            }

            if ( other is LocalizedTextVariableOption )
            {
                CopyFrom( other as LocalizedTextVariableOption );
            }
        }

        public void Reset()
        {
            this.Name = string.Empty;
            this.VariableType = string.Empty;
            this.Description = string.Empty;
        }
        #endregion

        #region Public Functions
        public void CopyFrom( LocalizedTextVariableOption other )
        {
            if ( other == null )
            {
                return;
            }

            this.Name = other.Name;
            this.VariableType = other.VariableType;
            this.Description = other.Description;
        }

        public bool Equals( LocalizedTextVariableOption other )
        {
            if ( other == null )
            {
                return false;
            }

            if ( base.Equals( (object)other ) )
            {
                return true;
            }

            return (this.Name == other.Name)
                && (this.VariableType == other.VariableType)
                && (this.Description == other.Description);
        }
        #endregion
    }

    /// <summary>
    /// A structure that contains a list of LocalizedTextVariableOption items and can serialize the collection.
    /// </summary>
    [Serializable]
    public class LocalizedTextVariableOptionCollection : rageSerializableList<LocalizedTextVariableOption>
    {
        #region IRageClonableObject
        public override object Clone()
        {
            LocalizedTextVariableOptionCollection collection = new LocalizedTextVariableOptionCollection();
            collection.CopyFrom( collection );
            return collection;
        }
        #endregion
    }
}
