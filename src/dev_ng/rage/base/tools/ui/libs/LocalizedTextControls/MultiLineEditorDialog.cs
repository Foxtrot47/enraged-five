using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace LocalizedTextControls
{
    /// <summary>
    /// A dialog for editing a piece of localized text in a multiline format.
    /// </summary>
    public partial class MultiLineEditorDialog : Form
    {
        public MultiLineEditorDialog()
        {
            InitializeComponent();

            this.chooseToolStripMenuItem.Image = LocalizedRichTextBox.CustomColorBitmap;
        }

        #region Variables
        private DialogResult m_actualDialogResult = DialogResult.Cancel;
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the HelpNamespace for the dialog.
        /// </summary>
        [DefaultValue( null ), Description( "Gets or sets the HelpNamespace for the dialog." )]
        public string HelpNamespace
        {
            get
            {
                return this.helpProvider.HelpNamespace;
            }
            set
            {
                this.helpProvider.HelpNamespace = value;
            }
        }

        public new object Tag
        {
            get
            {
                return base.Tag;
            }
            set
            {
                base.Tag = value;
                this.localizedRichTextBox.Tag = value;
            }
        }
        #endregion

        #region Events
        public event ColorPickerEventHandler ShowColorDialog
        {
            add
            {
                this.localizedRichTextBox.ShowColorDialog += value;
            }
            remove
            {
                this.localizedRichTextBox.ShowColorDialog -= value;
            }
        }

        public event InsertVariableEventHandler ShowInsertVariableDialog
        {
            add
            {
                this.localizedRichTextBox.ShowInsertVariableDialog += value;
            }
            remove
            {
                this.localizedRichTextBox.ShowInsertVariableDialog -= value;
            }
        }
        #endregion

        #region Event Handlers
        private void MultiLineEditorDialog_FormClosing( object sender, FormClosingEventArgs e )
        {
            if ( (e.CloseReason == CloseReason.UserClosing) || (e.CloseReason == CloseReason.None) )
            {
                m_actualDialogResult = this.DialogResult;

                e.Cancel = true;

                this.Hide();
            }
        }

        #region LocalizedRichTextBox
        private void localizedRichTextBox_CanCopyChanged( object sender, EventArgs e )
        {
            this.copyToolStripButton.Enabled = this.localizedRichTextBox.CanCopy;
        }

        private void localizedRichTextBox_CanCutChanged( object sender, EventArgs e )
        {
            this.cutToolStripButton.Enabled = this.localizedRichTextBox.CanCut;
        }

        private void localizedRichTextBox_CanDeleteChanged( object sender, EventArgs e )
        {
            this.deleteToolStripButton.Enabled = this.localizedRichTextBox.CanDelete;
        }

        private void localizedRichTextBox_CanPasteTextChanged( object sender, EventArgs e )
        {
            this.pasteToolStripButton.Enabled = this.localizedRichTextBox.CanPasteText;
        }

        private void localizedRichTextBox_CustomColorChanged( object sender, EventArgs e )
        {
            this.chooseToolStripMenuItem.Image = LocalizedRichTextBox.CustomColorBitmap;
        }

        private void localizedRichTextBox_SelectionBoldChanged( object sender, EventArgs e )
        {
            this.boldToolStripButton.Checked = this.localizedRichTextBox.SelectionBold;
        }

        private void localizedRichTextBox_SelectionItalicChanged( object sender, EventArgs e )
        {
            this.italicToolStripButton.Checked = this.localizedRichTextBox.SelectionItalic;
        }

        private void localizedRichTextBox_SelectionUnderlineChanged( object sender, EventArgs e )
        {
            this.underlineToolStripButton.Checked = this.localizedRichTextBox.SelectionUnderline;
        }
        
        private void localizedRichTextBox_CanRedoChanged( object sender, EventArgs e )
        {
            this.redoToolStripButton.Enabled = this.localizedRichTextBox.CanRedo;
        }

        private void localizedRichTextBox_CanUndoChanged( object sender, EventArgs e )
        {
            this.undoToolStripButton.Enabled = this.localizedRichTextBox.CanUndo;
        }
        #endregion

        #region ToolStrip
        private void undoToolStripButton_Click( object sender, EventArgs e )
        {
            this.localizedRichTextBox.Undo();
        }

        private void redoToolStripButton_Click( object sender, EventArgs e )
        {
            this.localizedRichTextBox.Redo();
        }

        private void cutToolStripButton_Click( object sender, EventArgs e )
        {
            this.localizedRichTextBox.Cut();
        }

        private void copyToolStripButton_Click( object sender, EventArgs e )
        {
            this.localizedRichTextBox.Copy();
        }

        private void pasteToolStripButton_Click( object sender, EventArgs e )
        {
            this.localizedRichTextBox.Paste();
        }

        private void deleteToolStripButton_Click( object sender, EventArgs e )
        {
            this.localizedRichTextBox.Delete();
        }

        private void boldToolStripButton_Click( object sender, EventArgs e )
        {
            this.localizedRichTextBox.SelectionBold = this.boldToolStripButton.Checked;
        }

        private void italicToolStripButton_Click( object sender, EventArgs e )
        {
            this.localizedRichTextBox.SelectionItalic = this.italicToolStripButton.Checked;
        }

        private void underlineToolStripButton_Click( object sender, EventArgs e )
        {
            this.localizedRichTextBox.SelectionUnderline = this.underlineToolStripButton.Checked;
        }

        private void insertVariableToolStripButton_Click( object sender, EventArgs e )
        {
            this.localizedRichTextBox.InsertVariable();
        }

        #region Color
        private void autoToolStripMenuItem_Click( object sender, EventArgs e )
        {
            this.localizedRichTextBox.SelectionColor = this.localizedRichTextBox.ForeColor;
        }

        private void chooseToolStripMenuItem_Click( object sender, EventArgs e )
        {
            this.localizedRichTextBox.ChooseColor();
        }

        private void blackToolStripMenuItem_Click( object sender, EventArgs e )
        {
            this.localizedRichTextBox.SelectionColor = Color.Black;
        }

        private void whiteToolStripMenuItem_Click( object sender, EventArgs e )
        {
            this.localizedRichTextBox.SelectionColor = Color.White;
        }

        private void grayToolStripMenuItem_Click( object sender, EventArgs e )
        {
            this.localizedRichTextBox.SelectionColor = Color.Gray;
        }

        private void silverToolStripMenuItem_Click( object sender, EventArgs e )
        {
            this.localizedRichTextBox.SelectionColor = Color.Silver;
        }

        private void maroonToolStripMenuItem_Click( object sender, EventArgs e )
        {
            this.localizedRichTextBox.SelectionColor = Color.Maroon;
        }

        private void redToolStripMenuItem_Click( object sender, EventArgs e )
        {
            this.localizedRichTextBox.SelectionColor = Color.Red;
        }

        private void oliveToolStripMenuItem_Click( object sender, EventArgs e )
        {
            this.localizedRichTextBox.SelectionColor = Color.Olive;
        }

        private void yellowToolStripMenuItem_Click( object sender, EventArgs e )
        {
            this.localizedRichTextBox.SelectionColor = Color.Yellow;
        }

        private void greenToolStripMenuItem_Click( object sender, EventArgs e )
        {
            this.localizedRichTextBox.SelectionColor = Color.Green;
        }

        private void limeToolStripMenuItem_Click( object sender, EventArgs e )
        {
            this.localizedRichTextBox.SelectionColor = Color.Lime;
        }

        private void tealToolStripMenuItem_Click( object sender, EventArgs e )
        {
            this.localizedRichTextBox.SelectionColor = Color.Teal;
        }

        private void aquaToolStripMenuItem_Click( object sender, EventArgs e )
        {
            this.localizedRichTextBox.SelectionColor = Color.Aqua;
        }

        private void navyToolStripMenuItem_Click( object sender, EventArgs e )
        {
            this.localizedRichTextBox.SelectionColor = Color.Navy;
        }

        private void blueToolStripMenuItem_Click( object sender, EventArgs e )
        {
            this.localizedRichTextBox.SelectionColor = Color.Blue;
        }

        private void purpleToolStripMenuItem_Click( object sender, EventArgs e )
        {
            this.localizedRichTextBox.SelectionColor = Color.Purple;
        }

        private void fuchsiaToolStripMenuItem_Click( object sender, EventArgs e )
        {
            this.localizedRichTextBox.SelectionColor = Color.Fuchsia;
        }
        #endregion

        #endregion
        #endregion

        #region Public Functions
        /// <summary>
        /// Initializes the UI with the RTF text, and displays the dialog.
        /// </summary>
        /// <param name="rtf"></param>
        /// <returns>DialogResult.OK if a variable was chosen, otherwise DialogResult.Cancel.</returns>
        public DialogResult ShowDialog( ref string rtf )
        {
            InitDialog( rtf );

            ShowDialog();
            if ( m_actualDialogResult == DialogResult.OK )
            {
                rtf = this.localizedRichTextBox.Rtf;
            }

            return m_actualDialogResult;
        }

        /// <summary>
        /// Initializes the UI with the RTF text, and displays the dialog.
        /// </summary>
        /// <param name="owner">The owner of the dialog.</param>
        /// <param name="rtf"></param>
        /// <returns>DialogResult.OK if a variable was chosen, otherwise DialogResult.Cancel.</returns>
        public DialogResult ShowDialog( IWin32Window owner, ref string rtf )
        {
            InitDialog( rtf );

            ShowDialog( owner );
            if ( m_actualDialogResult == DialogResult.OK )
            {
                rtf = this.localizedRichTextBox.Rtf;
            }

            return m_actualDialogResult;
        }
        #endregion

        #region Private Functions
        private void InitDialog( string rtf )
        {
            m_actualDialogResult = DialogResult.Cancel;

            this.localizedRichTextBox.Rtf = rtf;

            this.localizedRichTextBox.Focus();
            this.localizedRichTextBox.SelectionStart = this.localizedRichTextBox.TextLength;
            this.localizedRichTextBox.SelectionLength = 0;

            this.undoToolStripButton.Enabled = this.localizedRichTextBox.CanUndo;
            this.redoToolStripButton.Enabled = this.localizedRichTextBox.CanRedo;
            this.cutToolStripButton.Enabled = this.localizedRichTextBox.CanCut;
            this.copyToolStripButton.Enabled = this.localizedRichTextBox.CanCopy;
            this.pasteToolStripButton.Enabled = this.localizedRichTextBox.CanPasteText;
            this.deleteToolStripButton.Enabled = this.localizedRichTextBox.CanDelete;
            this.boldToolStripButton.Checked = this.localizedRichTextBox.SelectionBold;
            this.italicToolStripButton.Checked = this.localizedRichTextBox.SelectionItalic;
            this.underlineToolStripButton.Checked = this.localizedRichTextBox.SelectionUnderline;
        }
        #endregion
    }
}