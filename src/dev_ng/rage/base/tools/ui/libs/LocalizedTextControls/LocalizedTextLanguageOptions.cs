using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Text;

using RSG.Base.Serialization;

namespace LocalizedTextControls
{
    /// <summary>
    /// A structure that holds all of the data associated with a variable that can be added to a piece of localized text.
    /// </summary>
    [Serializable]
    public class LocalizedTextLanguageOption : IRageClonableObject
    {
        public LocalizedTextLanguageOption()
        {

        }

        public LocalizedTextLanguageOption( string language, string abbreviation )
        {
            m_language = language;
            m_abbreviation = abbreviation;
        }

        #region Variables
        private string m_language = string.Empty;
        private string m_abbreviation = string.Empty;
        #endregion

        #region Properties
        /// <summary>
        /// The name of the language.
        /// </summary>
        [Description( "The name of the language." ), Category( "Required" ), DefaultValue( "" )]
        public string Language
        {
            get
            {
                return m_language;
            }
            set
            {
                if ( value != null )
                {
                    m_language = value;
                }
                else
                {
                    m_language = string.Empty;
                }
            }
        }

        /// <summary>
        /// The language's abbreviation.
        /// </summary>
        [Description( "The language's abbreviation." ), Category( "Required" ), DefaultValue( "" )]
        public string Abbreviation
        {
            get
            {
                return m_abbreviation;
            }
            set
            {
                if ( value != null )
                {
                    m_abbreviation = value;
                }
                else
                {
                    m_abbreviation = string.Empty;
                }
            }
        }
        #endregion

        #region Overrides
        public override bool Equals( object obj )
        {
            if ( obj == null )
            {
                return false;
            }

            if ( base.Equals( obj ) )
            {
                return true;
            }

            if ( obj is LocalizedTextLanguageOption )
            {
                return Equals( obj as LocalizedTextLanguageOption );
            }

            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString()
        {
            return this.Language;
        }
        #endregion

        #region IRageClonableObject
        public object Clone()
        {
            LocalizedTextLanguageOption option = new LocalizedTextLanguageOption();
            option.CopyFrom( this );
            return option;
        }

        public void CopyFrom( IRageClonableObject other )
        {
            if ( other == null )
            {
                return;
            }

            if ( base.Equals( (object)other ) )
            {
                return;
            }

            if ( other is LocalizedTextLanguageOption )
            {
                CopyFrom( other as LocalizedTextLanguageOption );
            }
        }

        public void Reset()
        {
            this.Language = string.Empty;
            this.Abbreviation = string.Empty;
        }
        #endregion

        #region Public Functions
        public void CopyFrom( LocalizedTextLanguageOption other )
        {
            if ( other == null )
            {
                return;
            }

            this.Language = other.Language;
            this.Abbreviation = other.Abbreviation;
        }

        public bool Equals( LocalizedTextLanguageOption other )
        {
            if ( other == null )
            {
                return false;
            }

            if ( base.Equals( (object)other ) )
            {
                return true;
            }

            return (this.Language == other.Language)
                && (this.Abbreviation == other.Abbreviation);
        }
        #endregion
    }

    /// <summary>
    /// A structure that contains a list of LocalizedTextLanguageOption items and can serialize the collection.
    /// </summary>
    [Serializable]
    public class LocalizedTextLanguageOptionCollection : rageSerializableList<LocalizedTextLanguageOption>
    {
        #region IRageClonableObject
        public override object Clone()
        {
            LocalizedTextLanguageOptionCollection collection = new LocalizedTextLanguageOptionCollection();
            collection.CopyFrom( collection );
            return collection;
        }
        #endregion

        #region Public Functions
        public LocalizedTextLanguageOption Find( string language )
        {
            string uppercaseLanguage = language.ToUpper();
            foreach ( LocalizedTextLanguageOption option in this )
            {
                if ( option.Language.ToUpper() == uppercaseLanguage )
                {
                    return option;
                }
            }

            return null;
        }
        #endregion
    }
}