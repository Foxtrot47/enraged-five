using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using GlacialComponents.Controls.GTLCommon;

namespace LocalizedTextControls
{
    /// <summary>
    /// An EmbeddedControl used by the LocalizedTextEditorControl's GlacialTreeList for editing 
    /// numeric values.
    /// </summary>
    public class NumericUpDownColumnTemplate : NumericUpDown, IGEmbeddedControl
	{
		public NumericUpDownColumnTemplate()
		{
			//
			// TODO: Add constructor logic here
			//
        }

        #region Variables
        private object m_Owner;
		private object m_UserData;
        #endregion

        #region Properties
        public object Owner
		{ 
			get
			{
				return m_Owner;
			}
		}

		public object UserData
		{ 
			get
			{
				return m_UserData;
			}
		}

		public override string Text
		{ 
			get
			{
				return base.Text;
			}
			set
			{
				if ( this.DecimalPlaces == 0 )
				{
					base.Text = value;
                    base.Value = Convert.ToDecimal( value );
				}
				else
				{
					decimal val = Convert.ToDecimal( value );
					val = Decimal.Round( val, this.DecimalPlaces );
					base.Text = val.ToString();
                    base.Value = val;
				}
			}
        }
        #endregion

        #region Public Functions
        public bool Load( string text, object userData, object owner )
		{
			this.Text = text;
			m_UserData = userData;
			m_Owner = owner;

			return true;
		}
		
		public string Unload()
		{
			return this.Text;
        }
        #endregion
    }
}
