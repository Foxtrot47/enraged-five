using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace LocalizedTextControls
{
    /// <summary>
    /// A dialog for selecting a variable from a list to insert into a piece of text.
    /// </summary>
    public partial class InsertVariableDialog : Form
    {
        public InsertVariableDialog()
        {
            InitializeComponent();
        }

        #region Variables
        private DialogResult m_actualDialogResult = DialogResult.Cancel;
        #endregion

        #region Event Handlers
        private void InsertVariableDialog_FormClosing( object sender, FormClosingEventArgs e )
        {
            if ( (e.CloseReason == CloseReason.UserClosing) || (e.CloseReason == CloseReason.None) )
            {
                m_actualDialogResult = this.DialogResult;

                e.Cancel = true;

                this.Hide();
            }
        }

        private void listView_MouseDoubleClick( object sender, MouseEventArgs e )
        {
            if ( (e.Button == MouseButtons.Left) && this.okButton.Enabled )
            {
                this.DialogResult = DialogResult.OK;
            }
        }

        private void listView_SelectedIndexChanged( object sender, EventArgs e )
        {
            this.okButton.Enabled = this.listView.SelectedItems.Count > 0;
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the HelpNamespace for the dialog.
        /// </summary>
        [DefaultValue( null ), Description( "Gets or sets the HelpNamespace for the dialog." )]
        public string HelpNamespace
        {
            get
            {
                return this.helpProvider.HelpNamespace;
            }
            set
            {
                this.helpProvider.HelpNamespace = value;
            }
        }
        #endregion

        #region Public Functions
        /// <summary>
        /// Populates the UI with the variables in the list, and displays the dialog.
        /// </summary>
        /// <param name="collection">The list of variables to choose from.</param>
        /// <param name="variable">The selected variable, if any</param>
        /// <returns>DialogResult.OK if a variable was chosen, otherwise DialogResult.Cancel.</returns>
        public DialogResult ShowDialog( LocalizedTextVariableOptionCollection collection, out string variable )
        {
            variable = string.Empty;

            InitDialog( collection );

            ShowDialog();
            if ( m_actualDialogResult == DialogResult.OK )
            {
                if ( this.listView.SelectedItems.Count > 0 )
                {
                    variable = String.Format( "$({0})", this.listView.SelectedItems[0].Text );
                }
            }

            return m_actualDialogResult;
        }

        /// <summary>
        /// Populates the UI with the variables in the list, and displays the dialog.
        /// </summary>
        /// <param name="owner">The owner of the dialog.</param>
        /// <param name="collection">The list of variables to choose from.</param>
        /// <param name="variable">The selected variable, if any</param>
        /// <returns>DialogResult.OK if a variable was chosen, otherwise DialogResult.Cancel.</returns>
        public DialogResult ShowDialog( IWin32Window owner, LocalizedTextVariableOptionCollection collection, out string variable )
        {
            variable = string.Empty;

            InitDialog( collection );

            ShowDialog( owner );
            if ( m_actualDialogResult == DialogResult.OK )
            {
                if ( this.listView.SelectedItems.Count > 0 )
                {
                    variable = String.Format( "$({0})", this.listView.SelectedItems[0].Text );
                }
            }

            return m_actualDialogResult;
        }
        #endregion

        #region Private Functions
        private void InitDialog( LocalizedTextVariableOptionCollection collection )
        {
            m_actualDialogResult = DialogResult.Cancel;

            this.listView.Items.Clear();
            foreach ( LocalizedTextVariableOption option in collection )
            {
                this.listView.Items.Add( new ListViewItem( new string[] { option.Name, option.VariableType, option.Description } ) );
            }
        }
        #endregion
    }
}