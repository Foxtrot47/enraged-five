using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Text;

using RSG.Base.Command;

namespace LocalizedTextControls
{
    public class DefaultLocalizedTextExporter : ILocalizedTextExporter
    {
        public DefaultLocalizedTextExporter()
        {

        }

        #region ILocalizedTextExporter
        public int Export( string filename, LocalizedTextDataCollection collection, LocalizedTextOptions options, out rageStatus status )
        {
            int errors = 0;

            TextWriter writer = null;
            try
            {
                writer = new StreamWriter( filename, false, Encoding.Unicode );

                // write the column headers
                for ( int i = 0; i <= (int)LocalizedTextData.DataMember.Comment; ++i )
                {
                    if ( i > 0 )
                    {
                        writer.Write( "\t" );
                    }

                    LocalizedTextData.DataMember member = (LocalizedTextData.DataMember)i;
                    string columnName = member.ToString();
                    for ( int j = columnName.Length - 1; j > 0; --j )
                    {
                        if ( Char.ToUpper( columnName[j] ) == columnName[j] )
                        {
                            string part1 = columnName.Substring( 0, j );
                            string part2 = columnName.Substring( j );
                            columnName = String.Format( "{0} {1}", part1, part2 );
                            break;
                        }
                    }

                    writer.Write( columnName );
                }

                writer.WriteLine( String.Empty );

                // go through each text data in the language and write out its data
                for ( int i = 0; i < collection.Count; ++i )
                {
                    for ( int j = 0; j <= (int)LocalizedTextData.DataMember.Comment; ++j )
                    {
                        if ( j > 0 )
                        {
                            writer.Write( "\t" );
                        }

                        LocalizedTextData.DataMember member = (LocalizedTextData.DataMember)j;
                        switch ( member )
                        {
                            case LocalizedTextData.DataMember.Flags:
                                {
                                    writer.Write( collection[i].Flags );
                                }
                                break;
                            case LocalizedTextData.DataMember.Language:
                                {
                                    LocalizedTextLanguageOption languageOption = options.LanguageOptionCollection.Find( collection[i].Language );
                                    if ( languageOption != null )
                                    {
                                        writer.Write( languageOption.Abbreviation );
                                    }
                                }
                                break;
                            case LocalizedTextData.DataMember.String:
                                {
                                    WriteRtf( collection[i].Rtf, writer );
                                }
                                break;
                            default:
                                {
                                    string text = collection[i].GetToString( member );
                                    if ( !String.IsNullOrEmpty( text ) )
                                    {
                                        writer.Write( text );
                                    }
                                }
                                break;
                        }
                    }

                    writer.WriteLine( string.Empty );

                    OnProgressChanged( new ProgressChangedEventArgs( (int)(((float)i / (float)collection.Count) * 100.0f), collection[i].ToString() ) );
                }

                status = new rageStatus();
            }
            catch ( Exception e )
            {
                status = new rageStatus( e.ToString() );
                errors = 1;
            }
            finally
            {
                if ( writer != null )
                {
                    writer.Close();
                }
            }

            return errors;
        }

        public event ProgressChangedEventHandler ProgressChanged;
        #endregion

        #region Protected
        protected void OnProgressChanged( ProgressChangedEventArgs e )
        {
            if ( this.ProgressChanged != null )
            {
                this.ProgressChanged( this, e );
            }
        }

        protected void WriteRtf( string rtf, TextWriter writer )
        {
            RtfData rtfData = Utility.ParseRtf( rtf );
            if ( (rtfData == null) || (rtfData.LineData.Count == 0) )
            {
                return;
            }

            writer.Write( "<?xml version=\"1.0\" encoding=\"utf-16\" ?><rtf>" );
            {
                // Only write if we have more than the default
                if ( rtfData.ColorData.Count > 1 )
                {
                    writer.Write( "<clrs>" );
                    {
                        for ( int i = 1; i < rtfData.ColorData.Count; ++i )
                        {
                            writer.Write( String.Format( "<c r=\"{0}\" g=\"{1}\" b=\"{2}\" />",
                                rtfData.ColorData[i].Color.R, rtfData.ColorData[i].Color.G, rtfData.ColorData[i].Color.B ) );
                        }
                    }
                    writer.Write( "</clrs>" );
                }

                writer.Write( "<txt>" );
                {
                    for ( int i = 0; i < rtfData.LineData.Count; ++i )
                    {
                        if ( i > 0 )
                        {
                            writer.Write( "</br>" );
                        }

                        foreach ( RtfSpanData spanData in rtfData.LineData[i].SpanData )
                        {
                            StringBuilder spanStyle = new StringBuilder( "<sp" );

                            if ( !spanData.Bold && !spanData.Italic && !spanData.Underline && (spanData.ColorData.Index == 0) )
                            {
                                spanStyle.Append( '>' );
                            }
                            else
                            {
                                spanStyle.Append( " st=\"" );

                                bool addedStyle = false;
                                if ( spanData.Bold )
                                {
                                    spanStyle.Append( "b" );
                                    addedStyle = true;
                                }

                                if ( spanData.Italic )
                                {
                                    if ( addedStyle )
                                    {
                                        spanStyle.Append( ';' );
                                    }

                                    spanStyle.Append( 'i' );
                                    addedStyle = true;
                                }

                                if ( spanData.Underline )
                                {
                                    if ( addedStyle )
                                    {
                                        spanStyle.Append( ';' );
                                    }

                                    spanStyle.Append( 'u' );
                                    addedStyle = true;
                                }

                                if ( spanData.ColorData.Index > 0 )
                                {
                                    if ( addedStyle )
                                    {
                                        spanStyle.Append( ';' );
                                    }

                                    spanStyle.Append( String.Format( "c:{0}", spanData.ColorData.Index ) );
                                    addedStyle = true;
                                }

                                spanStyle.Append( "\">" );
                            }

                            writer.Write( spanStyle.ToString() );
                            {
                                writer.Write( spanData.Text.ToString() );
                            }
                            writer.Write( "</sp>" );
                        }
                    }
                }
                writer.Write( "</txt>" );
            }
            writer.Write( "</rtf>" );
        }
        #endregion
    }
}
