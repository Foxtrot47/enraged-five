using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

using GlacialComponents.Controls.GTLCommon;

namespace LocalizedTextControls
{
    /// <summary>
    /// An EmbeddedControl used by the LocalizedTextEditorControl's GlacialTreeList for editing 
    /// a ComboBox-style item where multiple items can be selected simultaneously.
    /// </summary>
    public partial class MultiSelectComboBoxColumnTemplate : UserControl, IGEmbeddedControl
    {
        public MultiSelectComboBoxColumnTemplate()
        {
            InitializeComponent();
        }

        #region Variables
        private object m_Owner;
        private object m_UserData;
        private bool m_textIsFlags = true;
        private string m_originalText = String.Empty;
        #endregion

        #region Properties
        public object Owner
        {
            get
            {
                return m_Owner;
            }
        }

        public object UserData
        {
            get
            {
                return m_UserData;
            }
        }

        public bool TextIsFlags
        {
            get
            {
                return m_textIsFlags;
            }
            set
            {
                if ( value != m_textIsFlags )
                {
                    m_textIsFlags = value;

                    if ( this.TextIsFlags )
                    {
                        m_originalText = this.Text;
                        this.Text = this.SelectedFlags.ToString();
                    }
                    else
                    {
                        this.Text = m_originalText;
                    }
                }
            }
        }

        public uint SelectedFlags
        {
            get
            {
                uint flags = 0;
                for ( int i = 0; i < this.contextMenuStrip.Items.Count; ++i )
                {
                    ToolStripMenuItem item = this.contextMenuStrip.Items[i] as ToolStripMenuItem;
                    if ( item.Checked )
                    {
                        flags |= (uint)(1 << i);
                    }
                }

                return flags;
            }
            set
            {
                uint val = value;
                int index = 0;
                while ( val != 0 )
                {
                    if ( index < this.contextMenuStrip.Items.Count )
                    {
                        ToolStripMenuItem item = this.contextMenuStrip.Items[index] as ToolStripMenuItem;
                        item.Checked = (val & 0x1) != 0;
                    }

                    ++index;
                    val = val >> 1;
                }

                if ( this.TextIsFlags )
                {
                    string text = this.Text;
                    LocalizedTextData.ToHexString( value, ref text );
                    this.label.Text = text;
                }
            }
        }

        public override string Text
        {
            get
            {
                return this.label.Text;
            }
            set
            {
                if ( this.TextIsFlags )
                {
                    uint flags = 0;
                    LocalizedTextData.FromHexString( value, ref flags );
                    this.SelectedFlags = flags;
                }
                else
                {
                    this.label.Text = value;
                }
            }
        }
        #endregion

        #region Events
        public event EventHandler Changed;
        #endregion

        #region Event Dispatchers
        protected void OnChanged()
        {
            if ( this.Changed != null )
            {
                this.Changed( this, EventArgs.Empty );
            }
        }
        #endregion

        #region Event Handlers
        private void button_Click( object sender, EventArgs e )
        {
            this.contextMenuStrip.Show( this.label, new Point( 0, this.label.Height ) );
        }

        private void Item_Click( object sender, EventArgs e )
        {
            // update this.Text
            if ( this.TextIsFlags )
            {
                string text = this.Text;
                LocalizedTextData.ToHexString( this.SelectedFlags, ref text );
                this.label.Text = text;
            }

            OnChanged();
        }
        #endregion

        #region Public Functions
        public void AddMenuItems( List<string> menuItemNames )
        {
            this.contextMenuStrip.Items.Clear();
            foreach ( string menuItemName in menuItemNames )
            {
                ToolStripMenuItem item = new ToolStripMenuItem( menuItemName );
                item.CheckOnClick = true;
                item.Click += new EventHandler( Item_Click );

                this.contextMenuStrip.Items.Add( item );
            }
        }

        public new bool Load( string text, object userData, object owner )
        {
            this.Text = text;
            m_UserData = userData;
            m_Owner = owner;

            return true;
        }

        public string Unload()
        {
            return this.Text;
        }
        #endregion
    }
}
