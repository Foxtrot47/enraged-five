using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace LocalizedTextControls
{
    /// <summary>
    /// An advanced RichTextBox with events, right-click context menus, and public functions
    /// to make editing easier.  Specialized for localized text.
    /// </summary>
    public partial class LocalizedRichTextBox : RichTextBox
    {
        public LocalizedRichTextBox()
        {
            InitializeComponent();

            if ( sm_customBitmap == null )
            {
                sm_customBitmap = new Bitmap( 16, 16, System.Drawing.Imaging.PixelFormat.Format32bppArgb );

                GenerateCustomBitmap( this.ForeColor );
            }

            this.chooseToolStripMenuItem.Image = sm_customBitmap;
        }

        #region Variables
        private static Color sm_customColor = Color.Empty;
        private static Bitmap sm_customBitmap = null;
        private static Regex sm_fontRegex = new Regex( "(?<fontID>\\x5cf[0-9]+)", RegexOptions.Compiled );
        private static Regex sm_fontSizeRegex = new Regex( "(?<fontSize>\\x5cfs[0-9]+)", RegexOptions.Compiled );

        private string m_defaultFontTable = null;
        private bool m_lastCanUndo = false;
        private bool m_lastCanRedo = false;
        private bool m_lastCanCut = false;
        private bool m_lastCanCopy = false;
        private bool m_lastCanPasteText = false;
        private bool m_lastCanDelete = false;
        #endregion

        #region Properties
        public bool CanCut
        {
            get
            {
                return !String.IsNullOrEmpty( this.SelectedText );
            }
        }

        public bool CanCopy
        {
            get
            {
                return this.SelectionLength > 0;
            }
        }

        public bool CanPasteText
        {
            get
            {
                DataFormats.Format rtfFormat = DataFormats.GetFormat( DataFormats.Rtf );
                if ( this.CanPaste( rtfFormat ) )
                {
                    return true;
                }
                else
                {
                    DataFormats.Format txtFormat = DataFormats.GetFormat( DataFormats.Text );
                    if ( this.CanPaste( txtFormat ) )
                    {
                        return true;
                    }
                }

                return false;
            }
        }

        public bool CanDelete
        {
            get
            {
                return this.SelectionLength > 0;
            }
        }

        public bool SelectionBold
        {
            get
            {
                if ( this.SelectionFont != null )
                {
                    return this.SelectionFont.Bold;
                }

                return false;
            }
            set
            {
                FontStyle style = FontStyle.Regular;
                if ( value )
                {
                    style |= FontStyle.Bold;
                }

                if ( this.SelectionItalic )
                {
                    style |= FontStyle.Italic;
                }

                if ( this.SelectionUnderline )
                {
                    style |= FontStyle.Underline;
                }

                if ( (this.SelectionFont == null) || (this.SelectionFont.Style != style) )
                {
                    if ( this.SelectionFont == null )
                    {
                        this.SelectionFont = new Font( this.Font, style );
                    }
                    else
                    {
                        this.SelectionFont = new Font( this.SelectionFont, style );
                    }
                }
            }
        }

        public bool SelectionItalic
        {
            get
            {
                if ( this.SelectionFont != null )
                {
                    return this.SelectionFont.Italic;
                }

                return false;
            }
            set
            {
                FontStyle style = FontStyle.Regular;
                if ( this.SelectionBold )
                {
                    style |= FontStyle.Bold;
                }

                if ( value )
                {
                    style |= FontStyle.Italic;
                }

                if ( this.SelectionUnderline )
                {
                    style |= FontStyle.Underline;
                }

                if ( (this.SelectionFont == null) || (this.SelectionFont.Style != style) )
                {
                    if ( this.SelectionFont == null )
                    {
                        this.SelectionFont = new Font( this.Font, style );
                    }
                    else
                    {
                        this.SelectionFont = new Font( this.SelectionFont, style );
                    }
                }
            }
        }

        public bool SelectionUnderline
        {
            get
            {
                if ( this.SelectionFont != null )
                {
                    return this.SelectionFont.Underline;
                }

                return false;
            }
            set
            {
                FontStyle style = FontStyle.Regular;
                if ( this.SelectionBold )
                {
                    style |= FontStyle.Bold;
                }

                if ( this.SelectionItalic )
                {
                    style |= FontStyle.Italic;
                }

                if ( value )
                {
                    style |= FontStyle.Underline;
                }

                if ( (this.SelectionFont == null) || (this.SelectionFont.Style != style) )
                {
                    if ( this.SelectionFont == null )
                    {
                        this.SelectionFont = new Font( this.Font, style );
                    }
                    else
                    {
                        this.SelectionFont = new Font( this.SelectionFont, style );
                    }
                }
            }
        }

        [Browsable( false ), DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public static Color CustomColor
        {
            get
            {
                return sm_customColor;
            }
            set
            {
                sm_customColor = value;
            }
        }

        public static Bitmap CustomColorBitmap
        {
            get
            {
                if ( sm_customBitmap == null )
                {
                    sm_customBitmap = new Bitmap( 16, 16, System.Drawing.Imaging.PixelFormat.Format32bppArgb );

                    GenerateCustomBitmap( sm_customColor );
                }

                return sm_customBitmap;
            }
        }
        #endregion

        #region Events
        public event EventHandler CanUndoChanged;
        public event EventHandler CanRedoChanged;
        public event EventHandler CanCutChanged;
        public event EventHandler CanCopyChanged;
        public event EventHandler CanPasteTextChanged;
        public event EventHandler CanDeleteChanged;
        public event EventHandler SelectionBoldChanged;
        public event EventHandler SelectionItalicChanged;
        public event EventHandler SelectionUnderlineChanged;
        public event EventHandler CustomColorChanged;

        public event ColorPickerEventHandler ShowColorDialog;
        public event InsertVariableEventHandler ShowInsertVariableDialog;
        public event MultiLineEditorEventHandler ShowMultiLineEditorDialog;
        #endregion

        #region Event Dispatchers
        protected void OnCanUndoChanged()
        {
            if ( this.CanUndoChanged != null )
            {
                this.CanUndoChanged( this, EventArgs.Empty );
            }
        }

        protected void OnCanRedoChanged()
        {
            if ( this.CanRedoChanged != null )
            {
                this.CanRedoChanged( this, EventArgs.Empty );
            }
        }

        protected void OnCanCutChanged()
        {
            if ( this.CanCutChanged != null )
            {
                this.CanCutChanged( this, EventArgs.Empty );
            }
        }

        protected void OnCanCopyChanged()
        {
            if ( this.CanCopyChanged != null )
            {
                this.CanCopyChanged( this, EventArgs.Empty );
            }
        }

        protected void OnCanPasteTextChanged()
        {
            if ( this.CanPasteTextChanged != null )
            {
                this.CanPasteTextChanged( this, EventArgs.Empty );
            }
        }

        protected void OnCanDeleteChanged()
        {
            if ( this.CanDeleteChanged != null )
            {
                this.CanDeleteChanged( this, EventArgs.Empty );
            }
        }

        protected void OnSelectionBoldChanged()
        {
            if ( this.SelectionBoldChanged != null )
            {
                this.SelectionBoldChanged( this, EventArgs.Empty );
            }
        }

        protected void OnSelectionItalicChanged()
        {
            if ( this.SelectionItalicChanged != null )
            {
                this.SelectionItalicChanged( this, EventArgs.Empty );
            }
        }

        protected void OnSelectionUnderlineChanged()
        {
            if ( this.SelectionUnderlineChanged != null )
            {
                this.SelectionUnderlineChanged( this, EventArgs.Empty );
            }
        }

        protected void OnCustomColorChanged()
        {
            if ( this.CustomColorChanged != null )
            {
                this.CustomColorChanged( this, EventArgs.Empty );
            }
        }

        protected void OnShowColorDialog( ColorPickerEventArgs e )
        {
            if ( this.ShowColorDialog != null )
            {
                this.ShowColorDialog( this, e );
            }
        }

        protected void OnShowInsertVariableDialog( InsertVariableEventArgs e )
        {
            if ( this.ShowInsertVariableDialog != null )
            {
                this.ShowInsertVariableDialog( this, e );
            }
        }

        protected void OnShowMultiLineEditorDialog( MultiLineEditorEventArgs e )
        {
            if ( this.ShowMultiLineEditorDialog != null )
            {
                this.ShowMultiLineEditorDialog( this, e );
            }
        }
        #endregion

        #region Overrides
        protected override void OnTextChanged( EventArgs e )
        {
            // store off the default string table
            if ( String.IsNullOrEmpty( m_defaultFontTable ) )
            {
                m_defaultFontTable = FindFontTable( this.Rtf );
            }

            base.OnTextChanged( e );
        }
        #endregion

        #region Event Handlers
        private void LocalizedRichTextBox_KeyDown( object sender, KeyEventArgs e )
        {
            // call our version of Paste
            if ( e.Control && (e.KeyCode == Keys.V) && !String.IsNullOrEmpty( m_defaultFontTable ) )
            {
                e.Handled = true;
                e.SuppressKeyPress = true;

                this.Paste();
            }
        }

        private void LocalizedRichTextBox_SelectionChanged( object sender, EventArgs e )
        {
            bool canCut = this.CanCut;
            bool canCopy = this.CanCopy;
            bool canDelete = this.CanDelete;

            if ( canCut != m_lastCanCut )
            {
                m_lastCanCut = canCut;

                OnCanCutChanged();
            }

            if ( canCopy != m_lastCanCopy )
            {
                m_lastCanCopy = canCopy;

                OnCanCopyChanged();
            }

            if ( canDelete != m_lastCanDelete )
            {
                m_lastCanDelete = canDelete;

                OnCanDeleteChanged();
            }

            this.boldToolStripMenuItem.Checked = this.SelectionBold;
            OnSelectionBoldChanged();

            this.italicToolStripMenuItem.Checked = this.SelectionItalic;
            OnSelectionItalicChanged();

            this.underlineToolStripMenuItem.Checked = this.SelectionUnderline;
            OnSelectionUnderlineChanged();
        }

        private void timer_Tick( object sender, EventArgs e )
        {
            bool canUndo = this.CanUndo;
            bool canRedo = this.CanRedo;
            bool canPasteText = this.CanPasteText;

            if ( canUndo != m_lastCanUndo )
            {
                m_lastCanUndo = canUndo;

                OnCanUndoChanged();
            }

            if ( canRedo != m_lastCanRedo )
            {
                m_lastCanRedo = canRedo;

                OnCanRedoChanged();
            }

            if ( canPasteText != m_lastCanPasteText )
            {
                m_lastCanPasteText = canPasteText;

                OnCanPasteTextChanged();
            }
        }

        #region ContextMenuStrip
        private void contextMenuStrip_Opening( object sender, CancelEventArgs e )
        {
            this.undoToolStripMenuItem.Enabled = this.CanUndo;
            this.redoToolStripMenuItem.Enabled = this.CanRedo;

            this.cutToolStripMenuItem.Enabled = this.CanCut;
            this.copyToolStripMenuItem.Enabled = this.CanCopy;
            this.pasteToolStripMenuItem.Enabled = this.CanPasteText;
            this.deleteToolStripMenuItem.Enabled = this.CanDelete;

            this.boldToolStripMenuItem.Checked = this.SelectionBold;
            this.italicToolStripMenuItem.Checked = this.SelectionItalic;
            this.underlineToolStripMenuItem.Checked = this.SelectionUnderline;
        }

        private void undoToolStripMenuItem_Click( object sender, EventArgs e )
        {
            Undo();
        }

        private void redoToolStripMenuItem_Click( object sender, EventArgs e )
        {
            Redo();
        }

        private void cutToolStripMenuItem_Click( object sender, EventArgs e )
        {
            Cut();
        }

        private void copyToolStripMenuItem_Click( object sender, EventArgs e )
        {
            Copy();
        }

        private void pasteToolStripMenuItem_Click( object sender, EventArgs e )
        {
            Paste();
        }

        private void deleteToolStripMenuItem_Click( object sender, EventArgs e )
        {
            Delete();
        }

        private void selectAllToolStripMenuItem_Click( object sender, EventArgs e )
        {
            this.SelectAll();
        }

        private void boldToolStripMenuItem_Click( object sender, EventArgs e )
        {
            this.SelectionBold = this.boldToolStripMenuItem.Checked;

            OnSelectionBoldChanged();
        }

        private void italicToolStripMenuItem_Click( object sender, EventArgs e )
        {
            this.SelectionItalic = this.italicToolStripMenuItem.Checked;

            OnSelectionItalicChanged();
        }

        private void underlineToolStripMenuItem_Click( object sender, EventArgs e )
        {
            this.SelectionUnderline = this.underlineToolStripMenuItem.Checked;

            OnSelectionUnderlineChanged();
        }

        private void insertVariableToolStripMenuItem_Click( object sender, EventArgs e )
        {
            InsertVariable();
        }

        private void multiLineEditorToolStripMenuItem_Click( object sender, EventArgs e )
        {
            OpenMultiLineEditorDialog();
        }

        #region Color Menu
        private void autoToolStripMenuItem_Click( object sender, EventArgs e )
        {
            this.SelectionColor = Color.Empty;
        }

        private void chooseToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( ChooseColor() )
            {
                OnCustomColorChanged();
            }
        }

        private void blackToolStripMenuItem_Click( object sender, EventArgs e )
        {
            this.SelectionColor = Color.Black;
        }

        private void whiteToolStripMenuItem_Click( object sender, EventArgs e )
        {
            this.SelectionColor = Color.White;
        }

        private void grayToolStripMenuItem_Click( object sender, EventArgs e )
        {
            this.SelectionColor = Color.Gray;
        }

        private void silverToolStripMenuItem_Click( object sender, EventArgs e )
        {
            this.SelectionColor = Color.Silver;
        }

        private void maroonToolStripMenuItem_Click( object sender, EventArgs e )
        {
            this.SelectionColor = Color.Maroon;
        }

        private void redToolStripMenuItem_Click( object sender, EventArgs e )
        {
            this.SelectionColor = Color.Red;
        }

        private void oliveToolStripMenuItem_Click( object sender, EventArgs e )
        {
            this.SelectionColor = Color.Olive;
        }

        private void yellowToolStripMenuItem_Click( object sender, EventArgs e )
        {
            this.SelectionColor = Color.Yellow;
        }

        private void limeToolStripMenuItem_Click( object sender, EventArgs e )
        {
            this.SelectionColor = Color.Lime;
        }

        private void greenToolStripMenuItem_Click( object sender, EventArgs e )
        {
            this.SelectionColor = Color.Green;
        }

        private void aquaToolStripMenuItem_Click( object sender, EventArgs e )
        {
            this.SelectionColor = Color.Aqua;
        }

        private void tealToolStripMenuItem_Click( object sender, EventArgs e )
        {
            this.SelectionColor = Color.Teal;
        }

        private void navyToolStripMenuItem_Click( object sender, EventArgs e )
        {
            this.SelectionColor = Color.Navy;
        }

        private void blueToolStripMenuItem_Click( object sender, EventArgs e )
        {
            this.SelectionColor = Color.Blue;
        }

        private void purpleToolStripMenuItem_Click( object sender, EventArgs e )
        {
            this.SelectionColor = Color.Purple;
        }

        private void fuchsiaToolStripMenuItem_Click( object sender, EventArgs e )
        {
            this.SelectionColor = Color.Fuchsia;
        }
        #endregion
        #endregion
        #endregion

        #region Public Functions
        /// <summary>
        /// Pastes text from the clipboard, if available and of the type Rtf or Text.
        /// </summary>
        public new void Paste()
        {
            DataFormats.Format rtfFormat = DataFormats.GetFormat( DataFormats.Rtf );
            if ( this.CanPaste( rtfFormat ) )
            {
                base.Paste( rtfFormat );

                /* Hmm, I want to remove the extra fonts and font sizes, but this will also clear the UndoRedo stack
                string rtf = this.Rtf;
                string fontTable = FindFontTable( rtf );
                if ( !String.IsNullOrEmpty( fontTable ) && (m_defaultFontTable != fontTable) )
                {
                    int selectionStart = this.SelectionStart;

                    // replace the font table
                    rtf = rtf.Replace( fontTable, m_defaultFontTable );

                    // find all font usage
                    int startIndex = rtf.IndexOf( m_defaultFontTable ) + m_defaultFontTable.Length;
                    Match m = sm_fontRegex.Match( rtf, startIndex );

                    List<string> fontIDs = new List<string>();
                    while ( m.Success )
                    {
                        fontIDs.Add( m.Result( "${fontID}" ) );

                        m = m.NextMatch();
                    }

                    // set all fonts to "\\f0"
                    foreach ( string fontID in fontIDs )
                    {
                        rtf = rtf.Replace( fontID, "\\f0" );
                    }

                    // find all font size usage
                    m = sm_fontSizeRegex.Match( rtf, startIndex );

                    List<string> fontSizes = new List<string>();
                    while ( m.Success )
                    {
                        fontSizes.Add( m.Result( "${fontSize}" ) );

                        m = m.NextMatch();
                    }

                    // set all fonts to "\\fs20"
                    string defaultFontSize = String.Format( "\\fs{0}", (int)Math.Ceiling( this.Font.SizeInPoints * 2 ) );
                    foreach ( string fontSize in fontSizes )
                    {
                        rtf = rtf.Replace( fontSize, defaultFontSize );
                    }

                    this.Rtf = rtf;
                    this.SelectionStart = selectionStart;
                }
                */
            }
            else
            {
                DataFormats.Format txtFormat = DataFormats.GetFormat( DataFormats.Text );
                if ( this.CanPaste( txtFormat ) )
                {
                    base.Paste( txtFormat );
                }
            }
        }

        /// <summary>
        /// Deletes the selected text, if any.
        /// </summary>
        public void Delete()
        {
            int len = this.SelectionLength;
            if ( len > 0 )
            {
                int start = this.SelectionStart;
                this.Text = this.Text.Remove( start, len );

                this.SelectionStart = start;
            }
        }

        /// <summary>
        /// Disptatches the ShowColorDialog event and handles the results.
        /// </summary>
        /// <returns></returns>
        public bool ChooseColor()
        {
            ColorPickerEventArgs e = new ColorPickerEventArgs( sm_customColor );
            OnShowColorDialog( e );
            if ( e.Changed )
            {
                this.SelectionColor = e.Color;

                GenerateCustomBitmap( e.Color );

                return true;
            }

            return false;
        }

        /// <summary>
        /// Disptatches the ShowInsertVariableDialog event and handles the results.
        /// </summary>
        /// <returns></returns>
        public bool InsertVariable()
        {
            InsertVariableEventArgs e = new InsertVariableEventArgs();
            OnShowInsertVariableDialog( e );
            if ( e.Changed )
            {
                this.SelectedText = e.Variable;

                return true;
            }

            return false;
        }

        /// <summary>
        /// Disptatches the ShowMultiLineEditorDialog event and handles the results.
        /// </summary>
        /// <returns></returns>
        public bool OpenMultiLineEditorDialog()
        {
            MultiLineEditorEventArgs e = new MultiLineEditorEventArgs( this.Rtf );
            OnShowMultiLineEditorDialog( e );
            if ( e.Changed )
            {
                this.Rtf = e.Rtf;

                // put the caret at the end
                this.SelectionStart = this.TextLength;
                this.SelectionLength = 0;

                return true;
            }

            return false;
        }
        #endregion

        #region Private Functions
        private static void GenerateCustomBitmap( Color c )
        {
            sm_customColor = c;

            for ( int i = 0; i < 16; ++i )
            {
                for ( int j = 0; j < 16; ++j )
                {
                    sm_customBitmap.SetPixel( i, j, sm_customColor );
                }
            }
        }

        private string FindFontTable( string rtf )
        {
            int indexOf = rtf.IndexOf( "{\\fonttbl" );
            if ( indexOf != -1 )
            {
                int braceCount = 1;
                for ( int i = indexOf + 1; i < rtf.Length; ++i )
                {
                    if ( rtf[i] == '{' )
                    {
                        ++braceCount;
                    }
                    else if ( rtf[i] == '}' )
                    {
                        --braceCount;
                    }

                    if ( braceCount == 0 )
                    {
                        return rtf.Substring( indexOf, i - indexOf + 1 );
                    }
                }
            }

            return null;
        }
        #endregion
    }

    #region EventArgs and EventHandlers
    public class ColorPickerEventArgs : EventArgs
    {
        public ColorPickerEventArgs( Color color )
        {
            m_color = color;
        }

        #region Variables
        private Color m_color;
        private bool m_changed = false;
        #endregion

        #region Properties
        public Color Color
        {
            get
            {
                return m_color;
            }
            set
            {
                m_color = value;
            }
        }

        public bool Changed
        {
            get
            {
                return m_changed;
            }
            set
            {
                m_changed = value;
            }
        }
        #endregion
    }

    public delegate void ColorPickerEventHandler( object sender, ColorPickerEventArgs e );

    public class InsertVariableEventArgs : EventArgs
    {
        public InsertVariableEventArgs()
        {
            
        }

        #region Variables
        private string m_variable;
        private bool m_changed = false;
        #endregion

        #region Properties
        public string Variable
        {
            get
            {
                return m_variable;
            }
            set
            {
                m_variable = value;
            }
        }

        public bool Changed
        {
            get
            {
                return m_changed;
            }
            set
            {
                m_changed = value;
            }
        }
        #endregion
    }

    public delegate void InsertVariableEventHandler( object sender, InsertVariableEventArgs e );

    public class MultiLineEditorEventArgs : EventArgs
    {
        public MultiLineEditorEventArgs( string rtf )
        {
            m_rtf = rtf;
        }

        #region Variables
        private string m_rtf;
        private bool m_changed = false;
        #endregion

        #region Properties
        public string Rtf
        {
            get
            {
                return m_rtf;
            }
            set
            {
                m_rtf = value;
            }
        }

        public bool Changed
        {
            get
            {
                return m_changed;
            }
            set
            {
                m_changed = value;
            }
        }
        #endregion
    }

    public delegate void MultiLineEditorEventHandler( object sender, MultiLineEditorEventArgs e );
    #endregion
}
