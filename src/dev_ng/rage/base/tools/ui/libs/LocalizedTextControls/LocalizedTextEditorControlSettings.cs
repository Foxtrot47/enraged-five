using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Text;
using System.Xml.Serialization;

using RSG.Base.Serialization;

namespace LocalizedTextControls
{
    /// <summary>
    /// Holds the per-user LocalizedTextEditorControl settings.
    /// </summary>
    public class LocalizedTextEditorControlSettings : rageSerializableObject
    {
        public LocalizedTextEditorControlSettings()
        {
            Reset();
        }

        #region Variables
        private rageSerializableIntegerCollection m_columnWidthCollection = new rageSerializableIntegerCollection();
        #endregion

        #region Properties
        /// <summary>
        /// The widths of the columns in the LocalizedTextEditorControl.
        /// </summary>
        [Description( "The widths of the columns in the LocalizedTextEditorControl." ), DesignerSerializationVisibility( DesignerSerializationVisibility.Content )]
        public rageSerializableIntegerCollection ColumnWidthCollection
        {
            get
            {
                return m_columnWidthCollection;
            }
            set
            {
                if ( value != null )
                {
                    m_columnWidthCollection = value;
                }
                else
                {
                    m_columnWidthCollection.Clear();
                }
            }
        }
        #endregion

        #region Overrides
        public override bool Equals( object obj )
        {
            if ( obj == null )
            {
                return false;
            }

            if ( base.Equals( obj ) )
            {
                return true;
            }

            if ( obj is LocalizedTextEditorControlSettings )
            {
                return Equals( obj as LocalizedTextEditorControlSettings );
            }

            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override object Clone()
        {
            LocalizedTextEditorControlSettings settings = new LocalizedTextEditorControlSettings();
            settings.CopyFrom( this );
            return settings;
        }

        public override void CopyFrom( IRageClonableObject other )
        {
            if ( other == null )
            {
                return;
            }

            if ( base.Equals( (object)other ) )
            {
                return;
            }

            if ( other is LocalizedTextEditorControlSettings )
            {
                CopyFrom( other as LocalizedTextEditorControlSettings );
            }
        }

        public override void Reset()
        {
            base.Reset();

            this.ColumnWidthCollection.Reset();
        }
        #endregion

        #region Public Functions
        public void CopyFrom( LocalizedTextEditorControlSettings other )
        {
            if ( other == null )
            {
                return;
            }

            if ( base.Equals( (object)other ) )
            {
                return;
            }

            base.CopyFrom( other as rageSerializableObject );

            this.ColumnWidthCollection.CopyFrom( other.ColumnWidthCollection );
        }

        public bool Equals( LocalizedTextEditorControlSettings other )
        {
            if ( other == null )
            {
                return false;
            }

            if ( base.Equals( other as rageSerializableObject ) )
            {
                return true;
            }

            return this.ColumnWidthCollection.Equals( other.ColumnWidthCollection );
        }
        #endregion
    }
}
