namespace LocalizedTextControls
{
    partial class LocalizedTextEditorControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && (components != null) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            GlacialComponents.Controls.GlacialTreeList.GTLColumn gtlColumn1 = new GlacialComponents.Controls.GlacialTreeList.GTLColumn();
            GlacialComponents.Controls.GlacialTreeList.GTLColumn gtlColumn2 = new GlacialComponents.Controls.GlacialTreeList.GTLColumn();
            GlacialComponents.Controls.GlacialTreeList.GTLColumn gtlColumn3 = new GlacialComponents.Controls.GlacialTreeList.GTLColumn();
            GlacialComponents.Controls.GlacialTreeList.GTLColumn gtlColumn4 = new GlacialComponents.Controls.GlacialTreeList.GTLColumn();
            GlacialComponents.Controls.GlacialTreeList.GTLColumn gtlColumn5 = new GlacialComponents.Controls.GlacialTreeList.GTLColumn();
            GlacialComponents.Controls.GlacialTreeList.GTLColumn gtlColumn6 = new GlacialComponents.Controls.GlacialTreeList.GTLColumn();
            GlacialComponents.Controls.GlacialTreeList.GTLColumn gtlColumn7 = new GlacialComponents.Controls.GlacialTreeList.GTLColumn();
            GlacialComponents.Controls.GlacialTreeList.GTLColumn gtlColumn8 = new GlacialComponents.Controls.GlacialTreeList.GTLColumn();
            GlacialComponents.Controls.GlacialTreeList.GTLColumn gtlColumn9 = new GlacialComponents.Controls.GlacialTreeList.GTLColumn();
            GlacialComponents.Controls.GlacialTreeList.GTLColumn gtlColumn10 = new GlacialComponents.Controls.GlacialTreeList.GTLColumn();
            GlacialComponents.Controls.GlacialTreeList.GTLColumn gtlColumn11 = new GlacialComponents.Controls.GlacialTreeList.GTLColumn();
            GlacialComponents.Controls.GlacialTreeList.GTLColumn gtlColumn12 = new GlacialComponents.Controls.GlacialTreeList.GTLColumn();
            GlacialComponents.Controls.GlacialTreeList.GTLColumn gtlColumn13 = new GlacialComponents.Controls.GlacialTreeList.GTLColumn();
            GlacialComponents.Controls.GlacialTreeList.GTLColumn gtlColumn14 = new GlacialComponents.Controls.GlacialTreeList.GTLColumn();
            this.glacialTreeList = new GlacialComponents.Controls.GlacialTreeList.GlacialTreeList();
            this.contextMenuStrip = new System.Windows.Forms.ContextMenuStrip( this.components );
            this.undoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.redoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.addToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.cutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.copyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pasteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.selectAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.showPreviewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // glacialTreeList
            // 
            this.glacialTreeList.AllowDrop = true;
            this.glacialTreeList.BackColor = System.Drawing.SystemColors.Window;
            gtlColumn1.EmbeddedControlType = GlacialComponents.Controls.GTLCommon.GEmbeddedControlTypes.TextBox;
            gtlColumn1.EmbeddedDisplayConditions = GlacialComponents.Controls.GTLCommon.GEmbeddedDisplayConditions.DoubleClick;
            gtlColumn1.ImageIndex = -1;
            gtlColumn1.Name = "idColumn";
            gtlColumn1.Text = "Identifier";
            gtlColumn2.EmbeddedControlType = GlacialComponents.Controls.GTLCommon.GEmbeddedControlTypes.ComboBox;
            gtlColumn2.EmbeddedDisplayConditions = GlacialComponents.Controls.GTLCommon.GEmbeddedDisplayConditions.DoubleClick;
            gtlColumn2.ImageIndex = -1;
            gtlColumn2.Name = "languageColumn";
            gtlColumn2.Text = "Language";
            gtlColumn3.EmbeddedControlType = GlacialComponents.Controls.GTLCommon.GEmbeddedControlTypes.ComboBox;
            gtlColumn3.EmbeddedDisplayConditions = GlacialComponents.Controls.GTLCommon.GEmbeddedDisplayConditions.DoubleClick;
            gtlColumn3.ImageIndex = -1;
            gtlColumn3.Name = "regionColumn";
            gtlColumn3.Text = "Region";
            gtlColumn4.EmbeddedControlType = GlacialComponents.Controls.GTLCommon.GEmbeddedControlTypes.ComboBox;
            gtlColumn4.EmbeddedDisplayConditions = GlacialComponents.Controls.GTLCommon.GEmbeddedDisplayConditions.DoubleClick;
            gtlColumn4.ImageIndex = -1;
            gtlColumn4.Name = "platformColumn";
            gtlColumn4.Text = "Platform";
            gtlColumn5.EmbeddedControlType = GlacialComponents.Controls.GTLCommon.GEmbeddedControlTypes.ComboBox;
            gtlColumn5.EmbeddedDisplayConditions = GlacialComponents.Controls.GTLCommon.GEmbeddedDisplayConditions.DoubleClick;
            gtlColumn5.ImageIndex = -1;
            gtlColumn5.Name = "speakerColumn";
            gtlColumn5.Text = "Speaker";
            gtlColumn6.EmbeddedControlType = GlacialComponents.Controls.GTLCommon.GEmbeddedControlTypes.ColumnTemplate;
            gtlColumn6.EmbeddedDisplayConditions = GlacialComponents.Controls.GTLCommon.GEmbeddedDisplayConditions.AlwaysVisible;
            gtlColumn6.ImageIndex = -1;
            gtlColumn6.Name = "stringColumn";
            gtlColumn6.SortType = GlacialComponents.Controls.GTLCommon.SortTypes.None;
            gtlColumn6.Text = "String";
            gtlColumn7.CheckBoxAlign = System.Windows.Forms.HorizontalAlignment.Center;
            gtlColumn7.CheckBoxes = true;
            gtlColumn7.ImageIndex = -1;
            gtlColumn7.Name = "uppercaseColumn";
            gtlColumn7.Text = "Uppercase";
            gtlColumn8.EmbeddedControlType = GlacialComponents.Controls.GTLCommon.GEmbeddedControlTypes.ComboBox;
            gtlColumn8.EmbeddedDisplayConditions = GlacialComponents.Controls.GTLCommon.GEmbeddedDisplayConditions.DoubleClick;
            gtlColumn8.ImageIndex = -1;
            gtlColumn8.Name = "fontColumn";
            gtlColumn8.Text = "Font";
            gtlColumn9.EmbeddedControlType = GlacialComponents.Controls.GTLCommon.GEmbeddedControlTypes.ColumnTemplate;
            gtlColumn9.EmbeddedDisplayConditions = GlacialComponents.Controls.GTLCommon.GEmbeddedDisplayConditions.DoubleClick;
            gtlColumn9.ImageIndex = -1;
            gtlColumn9.ItemComparerType = GlacialComponents.Controls.GTLCommon.ItemComparerTypes.Numeric;
            gtlColumn9.Name = "scaleXColumn";
            gtlColumn9.SortType = GlacialComponents.Controls.GTLCommon.SortTypes.None;
            gtlColumn9.Text = "Scale X";
            gtlColumn10.EmbeddedControlType = GlacialComponents.Controls.GTLCommon.GEmbeddedControlTypes.ColumnTemplate;
            gtlColumn10.EmbeddedDisplayConditions = GlacialComponents.Controls.GTLCommon.GEmbeddedDisplayConditions.DoubleClick;
            gtlColumn10.ImageIndex = -1;
            gtlColumn10.ItemComparerType = GlacialComponents.Controls.GTLCommon.ItemComparerTypes.Numeric;
            gtlColumn10.Name = "scaleYColumn";
            gtlColumn10.SortType = GlacialComponents.Controls.GTLCommon.SortTypes.None;
            gtlColumn10.Text = "Scale Y";
            gtlColumn11.EmbeddedControlType = GlacialComponents.Controls.GTLCommon.GEmbeddedControlTypes.ColumnTemplate;
            gtlColumn11.EmbeddedDisplayConditions = GlacialComponents.Controls.GTLCommon.GEmbeddedDisplayConditions.DoubleClick;
            gtlColumn11.ImageIndex = -1;
            gtlColumn11.ItemComparerType = GlacialComponents.Controls.GTLCommon.ItemComparerTypes.Numeric;
            gtlColumn11.Name = "offsetXColumn";
            gtlColumn11.SortType = GlacialComponents.Controls.GTLCommon.SortTypes.None;
            gtlColumn11.Text = "Offset X";
            gtlColumn12.EmbeddedControlType = GlacialComponents.Controls.GTLCommon.GEmbeddedControlTypes.ColumnTemplate;
            gtlColumn12.EmbeddedDisplayConditions = GlacialComponents.Controls.GTLCommon.GEmbeddedDisplayConditions.DoubleClick;
            gtlColumn12.ImageIndex = -1;
            gtlColumn12.ItemComparerType = GlacialComponents.Controls.GTLCommon.ItemComparerTypes.Numeric;
            gtlColumn12.Name = "offsetYColumn";
            gtlColumn12.SortType = GlacialComponents.Controls.GTLCommon.SortTypes.None;
            gtlColumn12.Text = "Offset Y";
            gtlColumn13.EmbeddedControlType = GlacialComponents.Controls.GTLCommon.GEmbeddedControlTypes.ColumnTemplate;
            gtlColumn13.EmbeddedDisplayConditions = GlacialComponents.Controls.GTLCommon.GEmbeddedDisplayConditions.DoubleClick;
            gtlColumn13.ImageIndex = -1;
            gtlColumn13.Name = "flagsColumn";
            gtlColumn13.SortType = GlacialComponents.Controls.GTLCommon.SortTypes.None;
            gtlColumn13.Text = "Flags";
            gtlColumn14.EmbeddedControlType = GlacialComponents.Controls.GTLCommon.GEmbeddedControlTypes.TextBox;
            gtlColumn14.EmbeddedDisplayConditions = GlacialComponents.Controls.GTLCommon.GEmbeddedDisplayConditions.DoubleClick;
            gtlColumn14.ImageIndex = -1;
            gtlColumn14.Name = "commentColumn";
            gtlColumn14.SortType = GlacialComponents.Controls.GTLCommon.SortTypes.None;
            gtlColumn14.Text = "Comment";
            gtlColumn14.Width = 167;
            this.glacialTreeList.Columns.AddRange( new GlacialComponents.Controls.GlacialTreeList.GTLColumn[] {
            gtlColumn1,
            gtlColumn2,
            gtlColumn3,
            gtlColumn4,
            gtlColumn5,
            gtlColumn6,
            gtlColumn7,
            gtlColumn8,
            gtlColumn9,
            gtlColumn10,
            gtlColumn11,
            gtlColumn12,
            gtlColumn13,
            gtlColumn14} );
            this.glacialTreeList.ContextMenuStrip = this.contextMenuStrip;
            this.glacialTreeList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.glacialTreeList.ForeColor = System.Drawing.SystemColors.WindowText;
            this.glacialTreeList.GridColor = System.Drawing.SystemColors.Control;
            this.glacialTreeList.HideSelection = true;
            this.glacialTreeList.HotTrackingColor = System.Drawing.SystemColors.HotTrack;
            this.glacialTreeList.ItemHeight = 20;
            this.glacialTreeList.Location = new System.Drawing.Point( 0, 0 );
            this.glacialTreeList.MultiSelect = true;
            this.glacialTreeList.Name = "glacialTreeList";
            this.glacialTreeList.SelectedTextColor = System.Drawing.SystemColors.HighlightText;
            this.glacialTreeList.SelectionColor = System.Drawing.SystemColors.Highlight;
            this.glacialTreeList.ShadedColumnMode = GlacialComponents.Controls.GlacialTreeList.GTLShadedColumnModes.None;
            this.glacialTreeList.ShowPlusMinus = false;
            this.glacialTreeList.ShowRootLines = false;
            this.glacialTreeList.Size = new System.Drawing.Size( 1182, 150 );
            this.glacialTreeList.SnapLastColumn = true;
            this.glacialTreeList.TabIndex = 0;
            this.glacialTreeList.Text = "glacialTreeList1";
            this.glacialTreeList.UnfocusedSelectionColor = System.Drawing.SystemColors.Control;
            this.glacialTreeList.UnfocusedSelectionTextColor = System.Drawing.SystemColors.ControlText;
            this.glacialTreeList.NodeDrag += new GlacialComponents.Controls.GlacialTreeList.GTLEventHandler( this.glacialTreeList_NodeDrag );
            this.glacialTreeList.BeforeCellEdit += new GlacialComponents.Controls.GlacialTreeList.GTLBeforeEditEventHandlerDelegate( this.glacialTreeList_BeforeCellEdit );
            this.glacialTreeList.DragEnter += new System.Windows.Forms.DragEventHandler( this.glacialTreeList_DragEnter );
            this.glacialTreeList.AfterCheck += new GlacialComponents.Controls.GlacialTreeList.GTLCheckChangeEventHandler( this.glacialTreeList_AfterCheck );
            this.glacialTreeList.DragDrop += new System.Windows.Forms.DragEventHandler( this.glacialTreeList_DragDrop );
            this.glacialTreeList.AfterCellEdit += new GlacialComponents.Controls.GlacialTreeList.GTLEmbeddedControlEventHandler( this.glacialTreeList_AfterCellEdit );
            this.glacialTreeList.AfterSort += new GlacialComponents.Controls.GlacialTreeList.GTLEventHandler( this.glacialTreeList_AfterSort );
            this.glacialTreeList.EmbeddedControlHide += new GlacialComponents.Controls.GlacialTreeList.GTLEmbeddedControlEventHandler( this.glacialTreeList_EmbeddedControlHide );
            this.glacialTreeList.DragOver += new System.Windows.Forms.DragEventHandler( this.glacialTreeList_DragOver );
            this.glacialTreeList.EmbeddedControlShow += new GlacialComponents.Controls.GlacialTreeList.GTLBeforeEditEventHandlerDelegate( this.glacialTreeList_EmbeddedControlShow );
            // 
            // contextMenuStrip
            // 
            this.contextMenuStrip.Items.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.undoToolStripMenuItem,
            this.redoToolStripMenuItem,
            this.toolStripSeparator1,
            this.addToolStripMenuItem,
            this.toolStripSeparator2,
            this.cutToolStripMenuItem,
            this.copyToolStripMenuItem,
            this.pasteToolStripMenuItem,
            this.deleteToolStripMenuItem,
            this.toolStripSeparator3,
            this.selectAllToolStripMenuItem,
            this.toolStripSeparator4,
            this.showPreviewToolStripMenuItem} );
            this.contextMenuStrip.Name = "contextMenuStrip";
            this.contextMenuStrip.Size = new System.Drawing.Size( 179, 226 );
            // 
            // undoToolStripMenuItem
            // 
            this.undoToolStripMenuItem.Enabled = false;
            this.undoToolStripMenuItem.Image = global::LocalizedTextControls.Properties.Resources.Edit_UndoHS;
            this.undoToolStripMenuItem.Name = "undoToolStripMenuItem";
            this.undoToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Z)));
            this.undoToolStripMenuItem.Size = new System.Drawing.Size( 178, 22 );
            this.undoToolStripMenuItem.Text = "&Undo";
            this.undoToolStripMenuItem.Click += new System.EventHandler( this.undoToolStripMenuItem_Click );
            // 
            // redoToolStripMenuItem
            // 
            this.redoToolStripMenuItem.Enabled = false;
            this.redoToolStripMenuItem.Image = global::LocalizedTextControls.Properties.Resources.Edit_RedoHS;
            this.redoToolStripMenuItem.Name = "redoToolStripMenuItem";
            this.redoToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift)
                        | System.Windows.Forms.Keys.Z)));
            this.redoToolStripMenuItem.Size = new System.Drawing.Size( 178, 22 );
            this.redoToolStripMenuItem.Text = "&Redo";
            this.redoToolStripMenuItem.Click += new System.EventHandler( this.redoToolStripMenuItem_Click );
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size( 175, 6 );
            // 
            // addToolStripMenuItem
            // 
            this.addToolStripMenuItem.Image = global::LocalizedTextControls.Properties.Resources.NewCardHS;
            this.addToolStripMenuItem.Name = "addToolStripMenuItem";
            this.addToolStripMenuItem.ShortcutKeyDisplayString = "";
            this.addToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.Insert;
            this.addToolStripMenuItem.Size = new System.Drawing.Size( 178, 22 );
            this.addToolStripMenuItem.Text = "&Add";
            this.addToolStripMenuItem.Click += new System.EventHandler( this.addToolStripMenuItem_Click );
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size( 175, 6 );
            // 
            // cutToolStripMenuItem
            // 
            this.cutToolStripMenuItem.Image = global::LocalizedTextControls.Properties.Resources.CutHS;
            this.cutToolStripMenuItem.Name = "cutToolStripMenuItem";
            this.cutToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.X)));
            this.cutToolStripMenuItem.Size = new System.Drawing.Size( 178, 22 );
            this.cutToolStripMenuItem.Text = "Cu&t";
            this.cutToolStripMenuItem.Click += new System.EventHandler( this.cutToolStripMenuItem_Click );
            // 
            // copyToolStripMenuItem
            // 
            this.copyToolStripMenuItem.Image = global::LocalizedTextControls.Properties.Resources.CopyHS;
            this.copyToolStripMenuItem.Name = "copyToolStripMenuItem";
            this.copyToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.C)));
            this.copyToolStripMenuItem.Size = new System.Drawing.Size( 178, 22 );
            this.copyToolStripMenuItem.Text = "&Copy";
            this.copyToolStripMenuItem.Click += new System.EventHandler( this.copyToolStripMenuItem_Click );
            // 
            // pasteToolStripMenuItem
            // 
            this.pasteToolStripMenuItem.Image = global::LocalizedTextControls.Properties.Resources.PasteHS;
            this.pasteToolStripMenuItem.Name = "pasteToolStripMenuItem";
            this.pasteToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.V)));
            this.pasteToolStripMenuItem.Size = new System.Drawing.Size( 178, 22 );
            this.pasteToolStripMenuItem.Text = "&Paste";
            this.pasteToolStripMenuItem.Click += new System.EventHandler( this.pasteToolStripMenuItem_Click );
            // 
            // deleteToolStripMenuItem
            // 
            this.deleteToolStripMenuItem.Image = global::LocalizedTextControls.Properties.Resources.DeleteHS;
            this.deleteToolStripMenuItem.Name = "deleteToolStripMenuItem";
            this.deleteToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.Delete;
            this.deleteToolStripMenuItem.Size = new System.Drawing.Size( 178, 22 );
            this.deleteToolStripMenuItem.Text = "&Delete";
            this.deleteToolStripMenuItem.Click += new System.EventHandler( this.deleteToolStripMenuItem_Click );
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size( 175, 6 );
            // 
            // selectAllToolStripMenuItem
            // 
            this.selectAllToolStripMenuItem.Name = "selectAllToolStripMenuItem";
            this.selectAllToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.A)));
            this.selectAllToolStripMenuItem.Size = new System.Drawing.Size( 178, 22 );
            this.selectAllToolStripMenuItem.Text = "Select &All";
            this.selectAllToolStripMenuItem.Click += new System.EventHandler( this.selectAllToolStripMenuItem_Click );
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size( 175, 6 );
            // 
            // showPreviewToolStripMenuItem
            // 
            this.showPreviewToolStripMenuItem.Name = "showPreviewToolStripMenuItem";
            this.showPreviewToolStripMenuItem.Size = new System.Drawing.Size( 178, 22 );
            this.showPreviewToolStripMenuItem.Text = "&Show Preview";
            this.showPreviewToolStripMenuItem.Click += new System.EventHandler( this.showPreviewToolStripMenuItem_Click );
            // 
            // LocalizedTextEditorControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add( this.glacialTreeList );
            this.Name = "LocalizedTextEditorControl";
            this.Size = new System.Drawing.Size( 1182, 150 );
            this.contextMenuStrip.ResumeLayout( false );
            this.ResumeLayout( false );

        }

        #endregion

        private GlacialComponents.Controls.GlacialTreeList.GlacialTreeList glacialTreeList;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem addToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem copyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pasteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem undoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem redoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cutToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem selectAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem showPreviewToolStripMenuItem;
    }
}
