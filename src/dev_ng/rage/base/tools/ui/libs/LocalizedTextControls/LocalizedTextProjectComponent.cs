using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;

using RSG.Base.Command;

namespace LocalizedTextControls
{
    public partial class LocalizedTextProjectComponent : Component
    {
        public LocalizedTextProjectComponent()
        {
            InitializeComponent();
        }

        public LocalizedTextProjectComponent( IContainer container )
        {
            container.Add( this );

            InitializeComponent();
        }

        #region Variables
        private string m_filename = string.Empty;
        private bool m_modified = false;

        private bool m_fileWatcherEnabled = true;

        private LocalizedTextProjectData m_projectData = new LocalizedTextProjectData();
        private LocalizedTextProjectSettings m_projectSettings = new LocalizedTextProjectSettings();
        #endregion

        #region Properties
        /// <summary>
        /// The name of the currently loaded Localized Text Project.
        /// </summary>
        [DefaultValue( "" ), Description( "The name of the currently loaded Localized Text Project." )]
        public string Filename
        {
            get
            {
                return this.ProjectData.CurrentFilename;
            }
            set
            {
                this.ProjectData.CurrentFilename = value;
            }
        }

        /// <summary>
        /// Gets or set the IsModified flag.
        /// </summary>
        [DefaultValue( false ), Description( "Gets or set the IsModified flag." )]
        public bool IsModified
        {
            get
            {
                return m_modified;
            }
            set
            {
                if ( m_modified != value )
                {
                    m_modified = value;

                    rageStatus status;
                    if ( m_modified && !String.IsNullOrEmpty( this.Filename ) && File.Exists( this.Filename )
                        && m_projectSettings.SourceControlProviderSettings.IsSourceControlled( this.Filename, out status ) )
                    {
                        m_projectSettings.SourceControlProviderSettings.CheckOut( this.Filename, out status );
                    }

                    OnModifiedChanged();
                }
            }
        }

        /// <summary>
        /// Enables or disables the FileWatcher.
        /// </summary>
        [Category( "Behavior" ), DefaultValue( true ), Description( "Enables or disables the FileWatcher." )]
        public bool FileWatcherEnabled
        {
            get
            {
                return m_fileWatcherEnabled;
            }
            set
            {
                if ( m_fileWatcherEnabled != value )
                {
                    m_fileWatcherEnabled = value;

                    if ( value )
                    {
                        AddFileWatcher();
                    }
                    else
                    {
                        RemoveFileWatcher();
                    }
                }
            }
        }

        /// <summary>
        /// Retrieves the data for the current Localized Text Project.
        /// </summary>
        [Category( "Data" ), Description( "Retrieves the data for the current Localized Text Project." )]
        public LocalizedTextProjectData ProjectData
        {
            get
            {
                return m_projectData;
            }
        }

        /// <summary>
        /// Retrieves the user-specific data associated with the current Localized Text Project.
        /// </summary>
        [Category( "Data" ), Description( "Retrieves the user-specific data associated with the current Localized Text Project." )]
        public LocalizedTextProjectSettings ProjectSettings
        {
            get
            {
                return m_projectSettings;
            }
        }
        #endregion

        #region Events
        /// <summary>
        /// Occurs when the currently loaded Localized Text Project file is modified outside of the application.
        /// </summary>
        [Category( "Action" ), Description( "Occurs when the currently loaded Localized Text Project file is modified outside of the application." )]
        public event EventHandler FileExternallyModified;

        /// <summary>
        /// Occurs when a modification is made or the IsModified property is changed.
        /// </summary>
        [Category( "Property Changed" ), Description( "Occurs when a modification is made or the IsModified property is changed." )]
        public event EventHandler ModifiedChanged;
        #endregion

        #region Event Dispatchers
        protected void OnFileExternallyModified()
        {
            if ( this.FileExternallyModified != null )
            {
                this.FileExternallyModified( this, EventArgs.Empty );
            }
        }

        protected void OnModifiedChanged()
        {
            if ( this.ModifiedChanged != null )
            {
                this.ModifiedChanged( this, EventArgs.Empty );
            }
        }
        #endregion

        #region Public Functions
        public void LoadProject( string filename )
        {
            Clear();

            // load the project file
            rageStatus status;
            m_projectData.LoadFile( filename, out status );
            if ( !status.Success() )
            {
                throw (new Exception( status.ErrorString ));
            }

            this.IsModified = false;

            AddFileWatcher();

            // load the project settings file
            LoadProjectSettings();
        }

        public void SaveProject( string filename )
        {
            rageStatus status;

            // make sure the project file is checked out
            if ( File.Exists( filename ) && m_projectSettings.SourceControlProviderSettings.IsSourceControlled( filename, out status ) )
            {
                m_projectSettings.SourceControlProviderSettings.CheckOut( filename, out status );
                if ( !status.Success() )
                {
                    throw (new Exception( status.ErrorString ));
                }
            }

            this.fileSystemWatcher.EnableRaisingEvents = false;

            // save the project file
            m_projectData.SaveFile( filename, out status );
            if ( !status.Success() )
            {
                throw (new Exception( status.ErrorString ));
            }

            this.fileSystemWatcher.EnableRaisingEvents = this.FileWatcherEnabled;

            this.IsModified = false;

            SaveProjectSettings();
        }

        public void LoadProjectSettings()
        {
            if ( String.IsNullOrEmpty( this.Filename ) )
            {
                return;
            }

            string settingsFilename = this.Filename + ".settings";

            rageStatus status;
            m_projectSettings.LoadFile( settingsFilename, out status );
            if ( !status.Success() )
            {
                Console.WriteLine( "Error trying to load '{0}': {1}", settingsFilename, status.ErrorString );
            }
        }

        public void SaveProjectSettings()
        {
            if ( String.IsNullOrEmpty( this.Filename ) )
            {
                return;
            }

            string settingsFilename = this.Filename + ".settings";

            // make sure the project settings file is writable (it should not be source controlled)
            if ( File.Exists( settingsFilename ) )
            {
                try
                {
                    FileAttributes attributes = File.GetAttributes( settingsFilename );
                    if ( (attributes & FileAttributes.ReadOnly) == FileAttributes.ReadOnly )
                    {
                        File.SetAttributes( settingsFilename, File.GetAttributes( settingsFilename ) & (~FileAttributes.ReadOnly) );
                    }
                }
                catch ( Exception e )
                {
                    Console.WriteLine( "Error trying to make '{0}' writable: {1}", settingsFilename, e.Message );
                    return;
                }
            }

            // save the project settings file
            rageStatus status;
            m_projectSettings.SaveFile( settingsFilename, out status );
            if ( !status.Success() )
            {
                Console.WriteLine( "Error trying to save '{0}': {1}", settingsFilename, status.ErrorString );
            }
        }

        public void Clear()
        {
            RemoveFileWatcher();

            this.IsModified = false;

            m_projectData.Reset();
            m_projectSettings.Reset();
        }
        #endregion

        #region Private Functions
        private void AddFileWatcher()
        {
            if ( this.FileWatcherEnabled && !String.IsNullOrEmpty( this.Filename ) )
            {
                try
                {
                    if ( !File.Exists( this.Filename ) )
                    {
                        return;
                    }
                }
                catch
                {
                    return;
                }

                this.fileSystemWatcher.Path = Path.GetDirectoryName( this.Filename );
                this.fileSystemWatcher.Filter = Path.GetFileName( this.Filename );
                this.fileSystemWatcher.EnableRaisingEvents = true;
            }
        }

        private void RemoveFileWatcher()
        {
            if ( this.fileSystemWatcher.EnableRaisingEvents )
            {
                this.fileSystemWatcher.EnableRaisingEvents = false;
                this.fileSystemWatcher.Filter = string.Empty;
                this.fileSystemWatcher.Path = string.Empty;
            }
        }

        private void PromptToReload()
        {
            this.fileSystemWatcher.EnableRaisingEvents = false;

            OnFileExternallyModified();

            this.fileSystemWatcher.EnableRaisingEvents = this.FileWatcherEnabled;
        }

        private void fileSystemWatcher_Changed( object sender, FileSystemEventArgs e )
        {
            PromptToReload();
        }

        private void fileSystemWatcher_Renamed( object source, RenamedEventArgs e )
        {
            if ( e.FullPath.ToLower() == this.Filename.ToLower() )
            {
                PromptToReload();
            }
        }
        #endregion
    }
}
