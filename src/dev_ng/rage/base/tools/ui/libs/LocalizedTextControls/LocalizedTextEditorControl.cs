using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Data;
using System.IO;
using System.Text;
using System.Windows.Forms;

using GlacialComponents.Controls.GTLCommon;
using GlacialComponents.Controls.GlacialTreeList;
using RSG.Base.Command;
using RSG.Base.Forms;

namespace LocalizedTextControls
{
    /// <summary>
    /// The main control for editing a collection of localized text items.
    /// </summary>
    public partial class LocalizedTextEditorControl : UserControl
    {
        public LocalizedTextEditorControl()
        {
            InitializeComponent();

            this.glacialTreeList.Columns[(int)LocalizedTextData.DataMember.String].EmbeddedControlTemplateType 
                = typeof( LocalizedRichTextBoxColumnTemplate );
            this.glacialTreeList.Columns[(int)LocalizedTextData.DataMember.ScaleX].EmbeddedControlTemplateType 
                = typeof( NumericUpDownColumnTemplate );
            this.glacialTreeList.Columns[(int)LocalizedTextData.DataMember.ScaleY].EmbeddedControlTemplateType 
                = typeof( NumericUpDownColumnTemplate );
            this.glacialTreeList.Columns[(int)LocalizedTextData.DataMember.OffsetX].EmbeddedControlTemplateType 
                = typeof( NumericUpDownColumnTemplate );
            this.glacialTreeList.Columns[(int)LocalizedTextData.DataMember.OffsetY].EmbeddedControlTemplateType 
                = typeof( NumericUpDownColumnTemplate );
            this.glacialTreeList.Columns[(int)LocalizedTextData.DataMember.Flags].EmbeddedControlTemplateType 
                = typeof( MultiSelectComboBoxColumnTemplate );
        }

        #region Constants
        private const string c_clipboardDataObjectName = "LocalizedTextEditorControl GTLTreeNode List";

        private const int c_undoRedoStackCapacity = 200;
        #endregion

        #region Variables
        private int m_idNumber = 0;

        private LocalizedTextOptions m_options = new LocalizedTextOptions();
        private LocalizedTextDataCollection m_textDataCollection = new LocalizedTextDataCollection();

        private Dictionary<int, Control> m_alwaysVisibleEditControls = new Dictionary<int, Control>();
        private Dictionary<int, GTLTreeNode> m_treeNodesByKey = new Dictionary<int, GTLTreeNode>();
        
        private SortedDictionary<string, int> m_uniqueIDDictionary = new SortedDictionary<string, int>();
        private SortedDictionary<string, int> m_uniqueSpeakerDictionary = new SortedDictionary<string, int>();

        private bool m_canMove = true;

        private List<EditCommand> m_undoStack = new List<EditCommand>( c_undoRedoStackCapacity );
        private List<EditCommand> m_redoStack = new List<EditCommand>( c_undoRedoStackCapacity );

        private bool m_executingCommand = false;
        private bool m_modified = false;
        private int m_savePosition = 0;
        private string m_filename;

        private ILocalizedTextRenderer m_textRenderer = new DefaultLocalizedTextRenderer();
        private PreviewDialog m_previewDialog = new PreviewDialog();
        #endregion

        #region Properties
        /// <summary>
        /// The Application's HelpProvider.  When set, this control's items are added to it.
        /// </summary>
        [DefaultValue( null ), Description( "The Application's HelpProvider.  When set, this control's items are added to it." )]
        public HelpProvider HelpProvider
        {
            set
            {
                if ( value != null )
                {
                    m_previewDialog.HelpNamespace = value.HelpNamespace;
                }
            }
        }

        [Category( "Data" ), DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public LocalizedTextOptions LocalizedTextOptions
        {
            get
            {
                return m_options;
            }
            set
            {
                if ( value != null )
                {
                    m_options = value;
                }
                else
                {
                    m_options.Reset();
                }
            }
        }

        /// <summary>
        /// The current Text Renderer to use for displaying localized text.
        /// </summary>
        [Category( "Data" ), Description( "The current Text Renderer to use for displaying localized text." )]
        public ILocalizedTextRenderer TextRenderer
        {
            get
            {
                return m_textRenderer;
            }
            set
            {
                m_textRenderer = value;
            }
        }

        /// <summary>
        /// The collection of LocalizedTextData items.
        /// </summary>
        [Category( "Data" ), Description( "The collection of LocalizedTextData items." )]
        public LocalizedTextDataCollection TextDataCollection
        {
            get
            {
                return m_textDataCollection;
            }
        }

        /// <summary>
        /// The collection of currently selected LocalizedTextData items.
        /// </summary>
        [Category( "Data" ), Description( "The collection of currently selected LocalizedTextData items." )]
        public LocalizedTextDataCollection SelectedTextDataCollection
        {
            get
            {
                LocalizedTextDataCollection collection = new LocalizedTextDataCollection();

                foreach ( GTLTreeNode node in this.glacialTreeList.TreeList.SelectedNodes )
                {
                    collection.Add( node.Tag as LocalizedTextData );
                }

                return collection;
            }
        }

        /// <summary>
        /// Enables Add and Paste functionality.
        /// </summary>
        [Category( "Behavior" ), DefaultValue( true ), Description( "Enables Add and Paste functionality." )]
        public bool CanAdd
        {
            get
            {
                return this.addToolStripMenuItem.Enabled;
            }
            set
            {
                this.addToolStripMenuItem.Enabled = value;
                this.pasteToolStripMenuItem.Enabled = value;
            }
        }

        /// <summary>
        /// Enables Delete functionality.
        /// </summary>
        [Category( "Behavior" ), DefaultValue( true ), Description( "Enables Delete functionality." )]
        public bool CanRemove
        {
            get
            {
                return this.deleteToolStripMenuItem.Enabled;
            }
            set
            {
                this.deleteToolStripMenuItem.Enabled = value;
                this.cutToolStripMenuItem.Enabled = value;
            }
        }

        /// <summary>
        /// Enables Edit functionality.
        /// </summary>
        [Category( "Behavior" ), DefaultValue( true ), Description( "Enables Edit functionality." )]
        public bool CanChange
        {
            get
            {
                return this.glacialTreeList.TreeList.Columns[0].EmbeddedDisplayConditions != GEmbeddedDisplayConditions.None;
            }
            set
            {
                foreach ( GTLColumn column in this.glacialTreeList.TreeList.Columns )
                {
                    if ( column.EmbeddedDisplayConditions != GEmbeddedDisplayConditions.AlwaysVisible )
                    {
                        column.EmbeddedDisplayConditions = value ? GEmbeddedDisplayConditions.DoubleClick : GEmbeddedDisplayConditions.None;
                    }
                }

                foreach ( Control ctrl in m_alwaysVisibleEditControls.Values )
                {
                    if ( ctrl is TextBoxBase )
                    {
                        TextBoxBase textBoxBase = ctrl as TextBoxBase;
                        textBoxBase.ReadOnly = !value;
                    }
                    else
                    {
                        ctrl.Enabled = value;
                    }
                }
            }
        }

        /// <summary>
        /// Enables Drag 'n' Drop functionality to reorder items.
        /// </summary>
        [Category( "Behavior" ), DefaultValue( true ), Description( "Enables Drag 'n' Drop functionality to reorder items." )]
        public bool CanMove
        {
            get
            {
                return m_canMove;
            }
            set
            {
                m_canMove = value;
            }
        }

        /// <summary>
        /// Whether or not there are any edit commands that can be undone.
        /// </summary>
        [Category( "Behavior" ), Description( "Whether or not there are any edit commands that can be undone." )]
        public bool CanUndo
        {
            get
            {
                return m_undoStack.Count > 0;
            }
        }

        /// <summary>
        /// Whether or not there are any edit commands that can be redone.
        /// </summary>
        [Category( "Behavior" ), Description( "Whether or not there are any edit commands that can be redone." )]
        public bool CanRedo
        {
            get
            {
                return m_redoStack.Count > 0;
            }
        }

        [Browsable( false ), DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public bool IsModified
        {
            get
            {
                return m_modified;
            }
            set
            {
                m_modified = value;
            }
        }

        [Browsable( false ), DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public string Filename
        {
            get
            {
                return m_filename;
            }
            set
            {
                m_filename = value;
            }
        }

        [Category( "Data" ), DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public LocalizedTextEditorControlSettings LocalizedTextEditorControlSettings
        {
            get
            {
                LocalizedTextEditorControlSettings settings = new LocalizedTextEditorControlSettings();
                
                settings.ColumnWidthCollection.Clear();
                foreach ( GTLColumn column in this.glacialTreeList.TreeList.Columns )
                {
                    settings.ColumnWidthCollection.Add( column.Width );
                }

                return settings;
            }
            set
            {
                if ( value != null )
                {
                    for ( int i = 0; (i < this.glacialTreeList.TreeList.Columns.Count) && (i < value.ColumnWidthCollection.Count); ++i )
                    {
                        try
                        {
                            this.glacialTreeList.TreeList.Columns[i].Width = value.ColumnWidthCollection[i];
                        }
                        catch
                        {

                        }
                    }
                }
            }
        }

        /// <summary>
        /// Retrieves the the Glacial Tree List control
        /// </summary>
        public GlacialTreeList TreeList
        {
            get
            {
                return this.glacialTreeList;
            }
        }
        #endregion

        #region Events
        /// <summary>
        /// Occurs before LocalizedTextData is added, allowing the operation to be cancelled.
        /// </summary>
        [Category( "Action" ), Description( "Occurs before LocalizedTextData is added, allowing the operation to be cancelled." )]
        public event LocalizedTextDataCancelEventHandler TextDataAdd;

        /// <summary>
        /// Occurs when LocalizedTextData is added.
        /// </summary>
        [Category( "Action" ), Description( "Occurs when LocalizedTextData is added." )]
        public event LocalizedTextDataIndexEventHandler TextDataAdded;

        /// <summary>
        /// Occurs before LocalizedTextData is removed, allowing the operation to be cancelled.
        /// </summary>
        [Category( "Action" ), Description( "Occurs before LocalizedTextData is removed, allowing the operation to be cancelled." )]
        public event LocalizedTextDataCancelEventHandler TextDataRemove;

        /// <summary>
        /// Occurs when LocalizedTextData is removed.
        /// </summary>
        [Category( "Action" ), Description( "Occurs when LocalizedTextData is removed." )]
        public event LocalizedTextDataIndexEventHandler TextDataRemoved;

        /// <summary>
        /// Occurs before LocalizedTextData is changed, allowing the operation to be cancelled.
        /// </summary>
        [Category( "Action" ), Description( "Occurs before LocalizedTextData is changed, allowing the operation to be cancelled." )]
        public event LocalizedTextDataCancelEditEventHandler TextDataChange;

		/// <summary>
		/// Occurs when LocalizedTextData is edited but before the value is written to the field, allowing the operation to be cancelled.
		/// </summary>
		[Category("Action"), Description("Occurs when LocalizedTextData is edited but before the value is written to the field, allowing the operation to be cancelled.")]
		public event LocalizedTextDataPreChangedEventHandler TextDataPreChanged;

		/// <summary>
        /// Occurs when LocalizedTextData is edited.
        /// </summary>
        [Category( "Action" ), Description( "Occurs when LocalizedTextData is edited." )]
        public event LocalizedTextDataChangedEventHandler TextDataChanged;

        /// <summary>
        /// Occurs before LocalizedTextData is moved, allowing the operation to be cancelled.
        /// </summary>
        [Category( "Action" ), Description( "Occurs before LocalizedTextData is moved, allowing the operation to be cancelled." )]
        public event LocalizedTextDataCancelEventHandler TextDataMove;

        /// <summary>
        /// Occurs when LocalizedTextData is moved.
        /// </summary>
        [Category( "Action" ), Description( "Occurs when LocalizedTextData is moved." )]
        public event LocalizedTextDataMovedEventHandler TextDataMoved;

        /// <summary>
        /// Event triggered when the ColorPickerDialog should be shown.  Should only be set when not using a LocalizedTextEditorDialogComponent.
        /// </summary>
        [Description( "Event triggered when the ColorPickerDialog should be shown.  Should only be set when not using a LocalizedTextEditorDialogComponent." )]
        public event ColorPickerEventHandler ShowColorDialog;

        /// <summary>
        /// Event triggered when the InsertVariableDialog should be shown.  Should only be set when not using a LocalizedTextEditorDialogComponent.
        /// </summary>
        [Description( "Event triggered when the InsertVariableDialog should be shown.  Should only be set when not using a LocalizedTextEditorDialogComponent." )]
        public event InsertVariableEventHandler ShowInsertVariableDialog;

        /// <summary>
        /// Event triggered when the MultiLineEditorDialog should be shown.  Should only be set when not using a LocalizedTextEditorDialogComponent.
        /// </summary>
        [Description( "Event triggered when the MultiLineEditorDialog should be shown.  Should only be set when not using a LocalizedTextEditorDialogComponent." )]
        public event MultiLineEditorEventHandler ShowMultiLineEditorDialog;

        /// <summary>
        /// Occurs when a modification is made.
        /// </summary>
        [Category( "Property Changed" ), Description( "Occurs when a modification is made." )]
        public event EventHandler ModifiedChanged;
        #endregion

        #region Event Dispatchers
        protected void OnLocalizedTextDataAdd( LocalizedTextDataCancelEventArgs e )
        {
            if ( this.TextDataAdd != null )
            {
                this.TextDataAdd( this, e );
            }
        }

        protected void OnLocalizedTextDataAdded( LocalizedTextDataIndexEventArgs e )
        {
            if ( this.TextDataAdded != null )
            {
                this.TextDataAdded( this, e );
            }
        }

        protected void OnLocalizedTextDataRemove( LocalizedTextDataCancelEventArgs e )
        {
            if ( this.TextDataRemove != null )
            {
                this.TextDataRemove( this, e );
            }
        }

        protected void OnLocalizedTextDataRemoved( LocalizedTextDataIndexEventArgs e )
        {
            if ( this.TextDataRemoved != null )
            {
                this.TextDataRemoved( this, e );
            }
        }

        protected void OnLocalizedTextDataChange( LocalizedTextDataCancelEditEventArgs e )
        {
            if ( this.TextDataChange != null )
            {
                this.TextDataChange( this, e );
            }
        }

		protected void OnLocalizedTextDataPreChanged(LocalizedTextDataPreChangedEventArgs e)
		{
			if (this.TextDataPreChanged != null)
			{
				this.TextDataPreChanged(this, e);
			}
		}

		protected void OnLocalizedTextDataChanged(LocalizedTextDataChangedEventArgs e)
        {
            if ( this.TextDataChanged != null )
            {
                this.TextDataChanged( this, e );
            }
        }

        protected void OnLocalizedTextDataMove( LocalizedTextDataCancelEventArgs e )
        {
            if ( this.TextDataMove != null )
            {
                this.TextDataMove( this, e );
            }
        }

        protected void OnLocalizedTextDataMoved( LocalizedTextDataMovedEventArgs e )
        {
            if ( this.TextDataMoved != null )
            {
                this.TextDataMoved( this, e );
            }
        }

        protected void OnShowColorDialog( object sender, ColorPickerEventArgs e )
        {
            if ( this.ShowColorDialog != null )
            {
                this.ShowColorDialog( sender, e );
            }
        }

        protected void OnShowInsertVariableDialog( object sender, InsertVariableEventArgs e )
        {
            if ( this.ShowInsertVariableDialog != null )
            {
                this.ShowInsertVariableDialog( sender, e );
            }
        }

        protected void OnShowMultiLineEditorDialog( object sender, MultiLineEditorEventArgs e )
        {
            if ( this.ShowMultiLineEditorDialog != null )
            {
                this.ShowMultiLineEditorDialog( sender, e );
            }
        }

        protected void OnModifiedChanged()
        {
            if ( this.ModifiedChanged != null )
            {
                this.ModifiedChanged( this, EventArgs.Empty );
            }
        }
        #endregion

        #region Event Handlers
        #region GlacialTreeList
        private void glacialTreeList_AfterSort( object source, GTLEventArgs args )
        {
            // rebuild the list in the current order
            m_textDataCollection.Clear();

            foreach ( GTLTreeNode node in this.glacialTreeList.TreeList.Nodes )
            {
                m_textDataCollection.Add( node.Tag as LocalizedTextData );
            }
        }

        private void glacialTreeList_BeforeCellEdit( object source, GTLBeforeEditEventArgs args )
        {
            if ( !SetupEditControl( args.TreeNode, args.Column, args.Text, args.Control ) )
            {
                args.Cancel = true;
            }
            else
            {
                this.contextMenuStrip.Enabled = false;
            }
        }

        private void glacialTreeList_AfterCellEdit( object source, GTLEmbeddedControlEventArgs args )
        {
            if ( !args.Cancel )
            {
                ReadEditControl( args.TreeNode, args.Column, args.Text, args.Control );

                // cancel because we handled the transfer ourselves
                args.Cancel = true;
            }

            this.contextMenuStrip.Enabled = true;
        }

        private void glacialTreeList_AfterCheck( object source, GTLCheckChangeEventArgs args )
        {
            if ( args.Column == (int)LocalizedTextData.DataMember.Uppercase )
            {
                if ( this.CanChange )
                {
                    SetValue( args.TreeNode.Index, LocalizedTextData.DataMember.Uppercase, 
                        args.TreeNode.SubItems[args.Column].Checked.ToString() );
                }
            }
        }

        private void glacialTreeList_EmbeddedControlShow( object source, GTLBeforeEditEventArgs args )
        {
            // set up the text
            if ( !SetupEditControl( args.TreeNode, args.Column, args.Text, args.Control ) )
            {
                args.Cancel = true;
                return;
            }

            // set up the control's event handler(s)
            if ( (args.Control is GTextBox) || (args.Control is LocalizedRichTextBoxColumnTemplate) )
            {
                args.Control.KeyDown += new KeyEventHandler( EditControl_KeyDown );
                args.Control.Leave += new EventHandler( EditControl_Leave );
            }
            else if ( args.Control is GComboBox )
            {
                GlacialComponents.Controls.GTLCommon.GComboBox gComboBox = args.Control as GComboBox;
                gComboBox.SelectedIndexChanged += new EventHandler( EditControl_SelectedIndexChanged );
            }
            else if ( args.Control is NumericUpDownColumnTemplate )
            {
                NumericUpDownColumnTemplate numeric = args.Control as NumericUpDownColumnTemplate;
                numeric.ValueChanged += new EventHandler( EditControl_ValueChanged );
                numeric.Leave += new EventHandler( EditControl_Leave );
            }
            else if ( args.Control is MultiSelectComboBoxColumnTemplate )
            {
                MultiSelectComboBoxColumnTemplate msComboBox = args.Control as MultiSelectComboBoxColumnTemplate;
                msComboBox.Changed += new EventHandler( EditControl_ValueChanged );
            }

            if ( args.Control is LocalizedRichTextBoxColumnTemplate )
            {
                LocalizedRichTextBoxColumnTemplate localizedRichTextBoxColumnTemplate = args.Control as LocalizedRichTextBoxColumnTemplate;
                localizedRichTextBoxColumnTemplate.ShowColorDialog += new ColorPickerEventHandler( EditControl_ShowColorDialog );
                localizedRichTextBoxColumnTemplate.ShowInsertVariableDialog += new InsertVariableEventHandler( EditControl_ShowInsertVariableDialog );
                localizedRichTextBoxColumnTemplate.ShowMultiLineEditorDialog += new MultiLineEditorEventHandler( EditControl_ShowMultiLineEditorDialog );
                localizedRichTextBoxColumnTemplate.GotFocus += new EventHandler( EditControl_GotFocus );
                localizedRichTextBoxColumnTemplate.LostFocus += new EventHandler( EditControl_LostFocus );
            }
            else
            {
                this.contextMenuStrip.Enabled = false;
            }

            // remember what controls are always visible
            GTLSubItem gtlSubItem = args.TreeNode.SubItems[args.Column];
            if ( !m_alwaysVisibleEditControls.ContainsKey( gtlSubItem.GetHashCode() ) )
            {
                m_alwaysVisibleEditControls.Add( gtlSubItem.GetHashCode(), args.Control );
            }
        }

        private void glacialTreeList_EmbeddedControlHide( object source, GTLEmbeddedControlEventArgs args )
        {
            if ( !args.Cancel )
            {
                string prevValue = args.TreeNode.SubItems[args.Column].Text;

                // read back the text
                ReadEditControl( args.TreeNode, args.Column, args.Text, args.Control );

                // cancel because we handled the transfer ourselves
                args.Cancel = true;

                GTLSubItem gtlSubItem = args.TreeNode.SubItems[args.Column];
                m_alwaysVisibleEditControls.Remove( gtlSubItem.GetHashCode() );
            }

            this.contextMenuStrip.Enabled = true;
        }

        private void glacialTreeList_NodeDrag( object source, GlacialComponents.Controls.GlacialTreeList.GTLEventArgs args )
        {
            if ( this.CanMove )
            {
                GlacialTreeList treelist = source as GlacialTreeList;
                if ( treelist != null )
                {
                    treelist.DoDragDrop( args, DragDropEffects.Move );
                }
            }
        }

        private void glacialTreeList_DragDrop( object sender, System.Windows.Forms.DragEventArgs e )
        {
            if ( e.Data.GetDataPresent( "GlacialComponents.Controls.GlacialTreeList.GTLEventArgs", true ) )
            {
                GlacialTreeList treelist = sender as GlacialTreeList;
                if ( treelist != null )
                {
                    GTLEventArgs args = (GTLEventArgs)e.Data.GetData( "GlacialComponents.Controls.GlacialTreeList.GTLEventArgs" );
                    System.Drawing.Point pt = treelist.PointToClient( new System.Drawing.Point( e.X, e.Y ) );
                    GTLTreeNode destinationNode = treelist.GetNodeAt( pt.X, pt.Y );

                    LocalizedTextData data = args.TreeNode.Tag as LocalizedTextData;
                    
                    LocalizedTextDataCancelEventArgs cancelEventArgs = new LocalizedTextDataCancelEventArgs( data );
                    OnLocalizedTextDataMove( cancelEventArgs );
                    if ( cancelEventArgs.Cancel )
                    {
                        e.Effect = DragDropEffects.None;
                        return;
                    }
                    
                    // if we're coming from a different TreeList, clone it
                    if ( treelist != args.TreeNode.TreeList )
                    {
                        int index = treelist.Nodes.Count;
                        if ( destinationNode != null )
                        {
                            index = destinationNode.Index;
                        }

                        if ( this.CanAdd )
                        {
                            DoCommand( new AddRowEditCommand( index, data.Clone() as LocalizedTextData ) );
                        }
                    }
                    else if ( args.TreeNode != destinationNode )
                    {                        
                        int newIndex = -1;
                        if ( destinationNode != null )
                        {
                            newIndex = destinationNode.Index;
                        }
                        else if ( args.TreeNode.Index < treelist.Nodes.Count - 1 )
                        {
                            // we must be off the end, and we aren't the last node
                            newIndex = treelist.Nodes.Count;
                        }

                        if ( (newIndex != -1) && this.CanMove )
                        {
                            DoCommand( new MoveRowEditCommand( args.TreeNode.Index, data, newIndex ) );
                        }
                    }
                }
            }

            e.Effect = DragDropEffects.None;
        }

        private void glacialTreeList_DragEnter( object sender, System.Windows.Forms.DragEventArgs e )
        {
            if ( this.CanMove )
            {
                e.Effect = DragDropEffects.Move;
            }
        }

        private void glacialTreeList_DragOver( object sender, System.Windows.Forms.DragEventArgs e )
        {
            if ( this.CanMove && e.Data.GetDataPresent( "GlacialComponents.Controls.GlacialTreeList.GTLEventArgs", true ) )
            {
                e.Effect = DragDropEffects.Move;
            }
            else
            {
                e.Effect = DragDropEffects.None;
            }
        }
        #endregion

        #region EditControl
        private void EditControl_KeyDown( object sender, KeyEventArgs e )
        {
            if ( e.KeyCode == Keys.Enter )
            {
                UpdateAlwaysVisibleItem( sender as Control );
            }
        }

        private void EditControl_Leave( object sender, EventArgs e )
        {
            UpdateAlwaysVisibleItem( sender as Control );
        }

        private void EditControl_SelectedIndexChanged( object sender, EventArgs e )
        {
            UpdateAlwaysVisibleItem( sender as Control );
        }

        private void EditControl_ValueChanged( object sender, EventArgs e )
        {
            if ( sender is NumericUpDownColumnTemplate )
            {
                NumericUpDownColumnTemplate numeric = sender as NumericUpDownColumnTemplate;
                numeric.Text = numeric.Value.ToString();
            }

            UpdateAlwaysVisibleItem( sender as Control );
        }

        private void EditControl_ShowColorDialog( object sender, ColorPickerEventArgs e )
        {
            OnShowColorDialog( sender, e );
        }

        private void EditControl_ShowInsertVariableDialog( object sender, InsertVariableEventArgs e )
        {
            OnShowInsertVariableDialog( sender, e );
        }

        private void EditControl_ShowMultiLineEditorDialog( object sender, MultiLineEditorEventArgs e )
        {
            OnShowMultiLineEditorDialog( sender, e );
        }
        
        private void EditControl_GotFocus( object sender, EventArgs e )
        {
            this.contextMenuStrip.Enabled = false;
        }

        private void EditControl_LostFocus( object sender, EventArgs e )
        {
            this.contextMenuStrip.Enabled = true;
        }
        #endregion

        #region ContxtMenuStrip
        private void undoToolStripMenuItem_Click( object sender, EventArgs e )
        {
            Undo();
        }

        private void redoToolStripMenuItem_Click( object sender, EventArgs e )
        {
            Redo();
        }

        private void addToolStripMenuItem_Click( object sender, EventArgs e )
        {
            LocalizedTextData data = new LocalizedTextData();
            data.ID = NextID();
            data.FontName = this.LocalizedTextOptions.DefaultFontOption.Name;
            data.Language = this.LocalizedTextOptions.DefaultLanguageOption.Language;
            data.Platform = this.LocalizedTextOptions.DefaultPlatformOption.Name;
            data.Region = this.LocalizedTextOptions.DefaultRegionOption.Name;

            LocalizedTextDataCancelEventArgs cancelEventArgs = new LocalizedTextDataCancelEventArgs( data );
            OnLocalizedTextDataAdd( cancelEventArgs );
            if ( !cancelEventArgs.Cancel )
            {
                Add( data );
            }
        }

        private void cutToolStripMenuItem_Click( object sender, EventArgs e )
        {
            LocalizedTextDataCollection collection = new LocalizedTextDataCollection();
            foreach ( GTLTreeNode node in this.glacialTreeList.TreeList.SelectedNodes )
            {
                collection.Add( node.Tag as LocalizedTextData );
            }

            LocalizedTextDataCancelEventArgs cancelEventArgs = new LocalizedTextDataCancelEventArgs( collection );
            OnLocalizedTextDataRemove( cancelEventArgs );
            if ( !cancelEventArgs.Cancel )
            {
                this.glacialTreeList.SelectedNodes.Clear();
                foreach ( LocalizedTextData data in cancelEventArgs.TextDataCollection )
                {
                    int hash = data.GetHashCode();
                    GTLTreeNode node;
                    if ( m_treeNodesByKey.TryGetValue( hash, out node ) )
                    {
                        node.Selected = true;
                    }
                }

                CutSelectedToClipboard();
            }
        }

        private void copyToolStripMenuItem_Click( object sender, EventArgs e )
        {
            CopySelectedToClipboard();
        }

        private void pasteToolStripMenuItem_Click( object sender, EventArgs e )
        {
            IDataObject dataObj = Clipboard.GetDataObject();
            if ( dataObj != null )
            {
                object obj = dataObj.GetData( c_clipboardDataObjectName );
                if ( obj != null )
                {
                    LocalizedTextDataCollection collection = obj as LocalizedTextDataCollection;
                    
                    LocalizedTextDataCancelEventArgs cancelEventArgs = new LocalizedTextDataCancelEventArgs( collection );
                    OnLocalizedTextDataAdd( cancelEventArgs );
                    if ( !cancelEventArgs.Cancel )
                    {
                        collection.Clear();
                        collection.AddRange( cancelEventArgs.TextDataCollection );

                        PasteFromClipboard();
                    }
                }
            }
        }

        private void deleteToolStripMenuItem_Click( object sender, EventArgs e )
        {
            LocalizedTextDataCollection collection = new LocalizedTextDataCollection();
            foreach ( GTLTreeNode node in this.glacialTreeList.TreeList.SelectedNodes )
            {
                collection.Add( node.Tag as LocalizedTextData );
            }

            LocalizedTextDataCancelEventArgs cancelEventArgs = new LocalizedTextDataCancelEventArgs( collection );
            OnLocalizedTextDataRemove( cancelEventArgs );
            if ( !cancelEventArgs.Cancel )
            {
                this.glacialTreeList.SelectedNodes.Clear();
                foreach ( LocalizedTextData data in cancelEventArgs.TextDataCollection )
                {
                    int hash = data.GetHashCode();
                    GTLTreeNode node;
                    if ( m_treeNodesByKey.TryGetValue( hash, out node ) )
                    {
                        node.Selected = true;
                    }
                }

                RemoveSelected();
            }
        }

        private void selectAllToolStripMenuItem_Click( object sender, EventArgs e )
        {
            this.glacialTreeList.TreeList.SetAllSelect( true );
        }

        private void showPreviewToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( (this.TextRenderer == null) || (this.glacialTreeList.TreeList.SelectedNodes.Count == 0) )
            {
                return;
            }

            int index = this.glacialTreeList.TreeList.SelectedNodes[0].Index;
            if ( index < m_textDataCollection.Count )
            {
                LocalizedTextData data = m_textDataCollection[index];
                Bitmap bmp = null;
                this.TextRenderer.Render( data, m_textDataCollection, this.LocalizedTextOptions, ref bmp );
                if ( bmp != null )
                {
                    m_previewDialog.Bitmap = bmp;
                    m_previewDialog.ShowDialog( this.FindForm() );
                    
                    bmp.Dispose();
                }
            }
        }
        #endregion
        #endregion

        #region Public Functions
        /// <summary>
        /// Loads the data from the file.  Throws exceptions on error.
        /// </summary>
        /// <param name="filename"></param>
        public void LoadFile( string filename )
        {
            Clear();

            LocalizedTextProjectData project = new LocalizedTextProjectData();

            rageStatus status;
            project.LoadFile( filename, out status );
            if ( !status.Success() )
            {
                throw (new Exception( status.ErrorString ));
            }

            if ( !String.IsNullOrEmpty( project.LocalizedTextOptionsFilename ) && File.Exists( project.LocalizedTextOptionsFilename ) )
            {
                this.LocalizedTextOptions.LoadFile( project.LocalizedTextOptionsFilename, out status );
                if ( !status.Success() )
                {
                    throw (new Exception( status.ErrorString ));
                }
            }

            this.Filename = Path.GetFullPath( filename );

            Add( project.TextDataCollection );

            ClearUndoRedoStack();

            this.IsModified = false;
            OnModifiedChanged();
        }

        /// <summary>
        /// Saves the data to the file.  Throws exceptions on error.
        /// </summary>
        /// <param name="filename"></param>
        public void SaveFile( string filename )
        {
            this.Filename = Path.GetFullPath( filename );

            ValidateIdentifiers();

            LocalizedTextProjectData project = new LocalizedTextProjectData();
            project.LocalizedTextOptionsFilename = this.LocalizedTextOptions.CurrentFilename;
            project.TextDataCollection = m_textDataCollection;

            rageStatus status;
            project.SaveFile( filename, out status );
            if ( !status.Success() )
            {
                throw (new Exception( status.ErrorString ));
            }

            if ( this.IsModified )
            {
                this.IsModified = false;
                OnModifiedChanged();
            }

            m_savePosition = m_undoStack.Count;
        }

        /// <summary>
        /// Clears the TreeList and all internals.
        /// </summary>
        public void Clear()
        {
            m_idNumber = 0;

            m_textDataCollection.Clear();

            this.glacialTreeList.TreeList.BeginNodeListChange();
            this.glacialTreeList.TreeList.Nodes.Clear();
            this.glacialTreeList.TreeList.EndNodeListChange();

            m_alwaysVisibleEditControls.Clear();
            m_treeNodesByKey.Clear();
            m_uniqueIDDictionary.Clear();
            m_uniqueSpeakerDictionary.Clear();
                        
            m_executingCommand = false;
            m_modified = false;

            ClearUndoRedoStack();

            OnModifiedChanged();
        }

        /// <summary>
        /// Clears the UndoRedo stack
        /// </summary>
        public void ClearUndoRedoStack()
        {
            m_undoStack.Clear();
            m_redoStack.Clear();

            m_savePosition = 0;
        }

        /// <summary>
        /// Returns the next automatic id.
        /// </summary>
        /// <returns></returns>
        public string NextID()
        {
			StringBuilder idPreFix = new StringBuilder();

			if (!String.IsNullOrEmpty(this.Filename))
			{
				string name = Path.GetFileNameWithoutExtension(this.Filename);
				idPreFix.Append(name);
			}

			while (true)
			{
				StringBuilder id = new StringBuilder(idPreFix.ToString());

				id.Append(m_idNumber);
				++m_idNumber;

				// Does this id already exist?
				bool bAlreadyExists = false;
				foreach (LocalizedTextData obData in TextDataCollection)
				{
					if (obData.ID == id.ToString())
					{
						bAlreadyExists = true;
					}
				}

				if (!bAlreadyExists)
				{
					// Unique, so return it
					return id.ToString();
				}
			}
        }

        /// <summary>
        /// Adds a new LocalizedTextData to the TreeList, initializing it with the default values from LocalizedTextDataSettings.
        /// </summary>
        /// <returns>The LocalizedTextData that was created and added.  null if there was a problem.</returns>
        public LocalizedTextData Add()
        {
            LocalizedTextData data = new LocalizedTextData();
            data.ID = NextID();
            data.FontName = this.LocalizedTextOptions.DefaultFontOption.Name;
            data.Language = this.LocalizedTextOptions.DefaultLanguageOption.Language;
            data.Platform = this.LocalizedTextOptions.DefaultPlatformOption.Name;
            data.Region = this.LocalizedTextOptions.DefaultRegionOption.Name;

            if ( Add( data ) )
            {
                return data;
            }

            return null;
        }

        /// <summary>
        /// Creates and add the specified number of LocalizedTextData items, all initialized with the default values from LocalizedTextDataSettings.
        /// </summary>
        /// <param name="count"></param>
        /// <returns>The list of created LocalizedTextData items, if created successfully.</returns>
        public LocalizedTextDataCollection Add( int count )
        {
            LocalizedTextDataCollection collection = new LocalizedTextDataCollection();

            for ( int i = 0; i < count; ++i )
            {
                LocalizedTextData data = new LocalizedTextData();
                data.ID = NextID();
                data.FontName = this.LocalizedTextOptions.DefaultFontOption.Name;
                data.Language = this.LocalizedTextOptions.DefaultLanguageOption.Language;
                data.Platform = this.LocalizedTextOptions.DefaultPlatformOption.Name;
                data.Region = this.LocalizedTextOptions.DefaultRegionOption.Name;

                collection.Add( data );
            }

            if ( Add( collection ) )
            {
                return collection;
            }

            return null;
        }

        /// <summary>
        /// Adds the specified LocalizedTextData to the TreeList.
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public bool Add( LocalizedTextData data )
        {
            if ( !m_executingCommand )
            {
                return DoCommand( new AddRowEditCommand( this.glacialTreeList.TreeList.Nodes.Count, data ) );
            }

            GTLTreeNode node = null;
            try
            {
                node = CreateTreeNode( data );
            }
            catch ( Exception e )
            {
                rageMessageBox.ShowExclamation( FindForm(), e.Message, "Add Line Problem" );
                return false;
            }

            if ( node != null )
            {
                this.glacialTreeList.BeginNodeListChange();

                this.glacialTreeList.TreeList.Nodes.Add( node );
                m_textDataCollection.Add( data );

                this.glacialTreeList.EndNodeListChange();

                m_treeNodesByKey[data.GetHashCode()] = node;

                OnLocalizedTextDataAdded( new LocalizedTextDataIndexEventArgs( data, this.glacialTreeList.TreeList.Nodes.Count - 1 ) );

                return true;
            }

            return false;
        }

        /// <summary>
        /// Adds the list of LocalizedTextData items to the end.
        /// </summary>
        /// <param name="collection"></param>
        /// <returns></returns>
        public bool Add( LocalizedTextDataCollection collection )
        {
            if ( !m_executingCommand )
            {
                List<int> rowList = new List<int>();
                for ( int i = 0; i < collection.Count; ++i )
                {
                    rowList.Add( this.glacialTreeList.TreeList.Nodes.Count + i );
                }

                return DoCommand( new AddRowsEditCommand( rowList, collection ) );
            }


            // see if we can create the nodes first
            List<GTLTreeNode> nodes = new List<GTLTreeNode>();
            foreach ( LocalizedTextData data in collection )
            {
                try
                {
                    GTLTreeNode node = CreateTreeNode( data );
                    if ( node == null )
                    {
                        return false;                        
                    }

                    nodes.Add( node );
                }
                catch ( Exception e )
                {
                    rageMessageBox.ShowExclamation( FindForm(), e.Message, "Add Line Problem" );
                    return false;
                }
            }

            this.glacialTreeList.BeginNodeListChange();
            
            // now add them to the TreeList
            for ( int i = 0; i < nodes.Count; ++i )
            {
                this.glacialTreeList.TreeList.Nodes.Add( nodes[i] );
                m_textDataCollection.Add( collection[i] );

                m_treeNodesByKey[collection[i].GetHashCode()] = nodes[i];

                OnLocalizedTextDataAdded( new LocalizedTextDataIndexEventArgs( collection[i], this.glacialTreeList.TreeList.Nodes.Count - 1 ) );
            }

            this.glacialTreeList.EndNodeListChange();

            return true;
        }

        /// <summary>
        /// Adds a new LocalizedTextData to the TreeList, initializing it with the default values from LocalizedTextDataSettings, at the 
        /// specified index.
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public LocalizedTextData Insert( int index )
        {
            LocalizedTextData data = new LocalizedTextData();
            data.ID = NextID();
            data.FontName = this.LocalizedTextOptions.DefaultFontOption.Name;
            data.Language = this.LocalizedTextOptions.DefaultLanguageOption.Language;
            data.Platform = this.LocalizedTextOptions.DefaultPlatformOption.Name;
            data.Region = this.LocalizedTextOptions.DefaultRegionOption.Name;

            if ( Insert( index, data ) )
            {
                return data;
            }

            return null;
        }

        /// <summary>
        /// Inserts the specified number of LocalizedTextData items into the list at the specified index.  The LocalizedTextData items
        /// are initialized with the default values from LocalizedTextDataSettings.
        /// </summary>
        /// <param name="index"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public LocalizedTextDataCollection Insert( int index, int count )
        {
            List<int> rowList = new List<int>();
            LocalizedTextDataCollection collection = new LocalizedTextDataCollection();

            for ( int i = 0; i < count; ++i )
            {
                rowList.Add( index + i );

                LocalizedTextData data = new LocalizedTextData();
                data.ID = NextID();
                data.FontName = this.LocalizedTextOptions.DefaultFontOption.Name;
                data.Language = this.LocalizedTextOptions.DefaultLanguageOption.Language;
                data.Platform = this.LocalizedTextOptions.DefaultPlatformOption.Name;
                data.Region = this.LocalizedTextOptions.DefaultRegionOption.Name;
                
                collection.Add( data );
            }

            if ( Insert( rowList, collection ) )
            {
                return collection;
            }

            return null;
        }

        /// <summary>
        /// Adds the specified LocalizedTextData to the TreeList at the given index.
        /// </summary>
        /// <param name="index"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public bool Insert( int index, LocalizedTextData data )
        {
            if ( !m_executingCommand )
            {
                return DoCommand( new AddRowEditCommand( index, data ) );
            }

            GTLTreeNode node = null;
            try
            {
                node = CreateTreeNode( data );
            }
            catch ( Exception e )
            {
                rageMessageBox.ShowExclamation( FindForm(), e.Message, "Add Line Problem" );
                return false;
            }

            if ( node != null )
            {
                this.glacialTreeList.BeginNodeListChange();

                if ( (index >= 0) && (index < this.glacialTreeList.TreeList.Nodes.Count) )
                {
                    this.glacialTreeList.TreeList.Nodes.Insert( index, node );
                    m_textDataCollection.Insert( index, data );
                }
                else
                {
                    this.glacialTreeList.TreeList.Nodes.Add( node );
                    m_textDataCollection.Add( data );

                    index = this.glacialTreeList.TreeList.Nodes.Count - 1;
                }

                this.glacialTreeList.EndNodeListChange();

                m_treeNodesByKey[data.GetHashCode()] = node;

                OnLocalizedTextDataAdded( new LocalizedTextDataIndexEventArgs( data, index ) );

                return true;
            }

            return false;
        }

        public bool Insert( List<int> rowList, LocalizedTextDataCollection collection )
        {
            if ( !m_executingCommand )
            {
                return DoCommand( new AddRowsEditCommand( rowList, collection ) );
            }

            // see if we can create the nodes first
            List<GTLTreeNode> nodes = new List<GTLTreeNode>();
            for ( int i = 0; i < collection.Count; ++i )
            {
                try
                {
                    GTLTreeNode node = CreateTreeNode( collection[i] );
                    if ( node == null )
                    {
                        return false;
                    }

                    nodes.Add( node );
                }
                catch ( Exception e )
                {
                    rageMessageBox.ShowExclamation( FindForm(), e.Message, "Add Line Problem" );
                    return false;
                }
            }

            this.glacialTreeList.BeginNodeListChange();

            // now add them to the TreeList
            for ( int i = 0; i < nodes.Count; ++i )
            {
                int addAt = this.glacialTreeList.TreeList.Nodes.Count;
                if ( i < rowList.Count )
                {
                    addAt = rowList[i];
                }

                if ( (addAt >= 0) && (addAt < this.glacialTreeList.TreeList.Nodes.Count) )
                {
                    this.glacialTreeList.TreeList.Nodes.Insert( addAt, nodes[i] );
                    m_textDataCollection.Insert( addAt, collection[i] );
                }
                else
                {
                    this.glacialTreeList.TreeList.Nodes.Add( nodes[i] );
                    m_textDataCollection.Add( collection[i] );

                    addAt = this.glacialTreeList.TreeList.Nodes.Count - 1;
                }

                m_treeNodesByKey[collection[i].GetHashCode()] = nodes[i];

                OnLocalizedTextDataAdded( new LocalizedTextDataIndexEventArgs( collection[i], addAt ) );
            }

            this.glacialTreeList.EndNodeListChange();
            return true;
        }

        /// <summary>
        /// Removes the specified LocalizedTextData from the TreeList.
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public bool Remove( LocalizedTextData data )
        {
            GTLTreeNode node;

            if ( !m_executingCommand )
            {
                if ( m_treeNodesByKey.TryGetValue( data.GetHashCode(), out node ) )
                {
                    return DoCommand( new RemoveRowEditCommand( node.Index, data ) );
                }
            }

            if ( !m_treeNodesByKey.TryGetValue( data.GetHashCode(), out node ) )
            {
                rageMessageBox.ShowExclamation( FindForm(),
                    String.Format( "Unable to remove the line with ID '{0}' for the {1} language because it does not exist.", data.ID, data.Language ),
                    "Remove Problem" );
                return false;
            }
            
            m_treeNodesByKey.Remove( data.GetHashCode() );

            foreach ( GTLSubItem gtlSubItem in node.SubItems )
            {
                m_alwaysVisibleEditControls.Remove( gtlSubItem.GetHashCode() );
            }

            this.glacialTreeList.BeginNodeListChange();

            int index = node.Index;
            this.glacialTreeList.TreeList.Nodes.RemoveAt( index );
            m_textDataCollection.RemoveAt( index );
            
            this.glacialTreeList.EndNodeListChange();

            RemoveUniqueID( data.ID );
            RemoveUniqueSpeaker( data.Speaker );

            OnLocalizedTextDataRemoved( new LocalizedTextDataIndexEventArgs( data, index ) );

            return true;
        }

        /// <summary>
        /// Removes the specified LocalizedTextData items from the TreeList.
        /// </summary>
        /// <param name="collection"></param>
        /// <returns></returns>
        public bool Remove( LocalizedTextDataCollection collection )
        {           
            if ( !m_executingCommand )
            {
                List<int> rowList = new List<int>();
                foreach ( LocalizedTextData data in collection )
                {
                    GTLTreeNode node;
                    if ( m_treeNodesByKey.TryGetValue( data.GetHashCode(), out node ) )
                    {
                        rowList.Add( node.Index );
                    }
                    else
                    {
                        rowList.Add( -1 );
                    }
                }

                return DoCommand( new RemoveRowsEditCommand( rowList, collection ) );
            }

            this.glacialTreeList.BeginNodeListChange();

            foreach ( LocalizedTextData data in collection )
            {
                GTLTreeNode node;
                if ( !m_treeNodesByKey.TryGetValue( data.GetHashCode(), out node ) )
                {
                    rageMessageBox.ShowExclamation( FindForm(),
                        String.Format( "Unable to remove the line with ID '{0}' for the {1} language because it does not exist.", data.ID, data.Language ),
                        "Remove Problem" );
                    return false;
                }

                m_treeNodesByKey.Remove( data.GetHashCode() );

                foreach ( GTLSubItem gtlSubItem in node.SubItems )
                {
                    m_alwaysVisibleEditControls.Remove( gtlSubItem.GetHashCode() );
                }

                int index = node.Index;
                this.glacialTreeList.TreeList.Nodes.RemoveAt( index );
                m_textDataCollection.RemoveAt( index );

                RemoveUniqueID( data.ID );
                RemoveUniqueSpeaker( data.Speaker );

                OnLocalizedTextDataRemoved( new LocalizedTextDataIndexEventArgs( data, index ) );
            }
            
            this.glacialTreeList.EndNodeListChange();
            return true;
        }

        /// <summary>
        /// Removes the specified LocalizedTextData from the TreeList at the given index.
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public bool RemoveAt( int index )
        {
            if ( (index >= 0) && (index < this.glacialTreeList.TreeList.Nodes.Count) )
            {
                GTLTreeNode node = this.glacialTreeList.TreeList.Nodes[index];
                LocalizedTextData data = node.Tag as LocalizedTextData;
                return Remove( data );
            }

            return false;
        }

        /// <summary>
        /// Removes the specified LocalizedTextData items from the TreeList at the given indexes.
        /// </summary>
        /// <param name="indexList"></param>
        /// <returns></returns>
        public bool RemoveAt( List<int> indexList )
        {
            LocalizedTextDataCollection collection = new LocalizedTextDataCollection();
            foreach ( int index in indexList )
            {
                if ( (index >= 0) && (index < this.glacialTreeList.TreeList.Nodes.Count) )
                {
                    collection.Add( this.glacialTreeList.TreeList.Nodes[index].Tag as LocalizedTextData );
                }
            }

            return Remove( collection );
        }

        /// <summary>
        /// Moves the given row to another place.
        /// </summary>
        /// <param name="row"></param>
        /// <param name="destination"></param>
        /// <returns></returns>
        public bool MoveRow( int row, int destination )
        {
            if ( (row >= 0) && (row < this.glacialTreeList.TreeList.Nodes.Count) )
            {
                GTLTreeNode node = this.glacialTreeList.TreeList.Nodes[row];
                LocalizedTextData data = node.Tag as LocalizedTextData;
                
                if ( !m_executingCommand )
                {
                    return DoCommand( new MoveRowEditCommand( row, data, destination ) );
                }

                this.glacialTreeList.TreeList.BeginNodeListChange();

                this.glacialTreeList.TreeList.Nodes.RemoveAt( row );
                m_textDataCollection.RemoveAt( row );

                if ( destination < this.glacialTreeList.TreeList.Nodes.Count )
                {
                    this.glacialTreeList.TreeList.Nodes.Insert( destination, node );
                    m_textDataCollection.Insert( destination, node.Tag as LocalizedTextData );
                }
                else
                {
                    this.glacialTreeList.TreeList.Nodes.Add( node );
                    m_textDataCollection.Add( node.Tag as LocalizedTextData );

                    destination = this.glacialTreeList.TreeList.Nodes.Count - 1;
                }

                this.glacialTreeList.TreeList.EndNodeListChange();

                OnLocalizedTextDataMoved( new LocalizedTextDataMovedEventArgs( data, destination, row ) );

                return true;
            }

            return false;
        }

        /// <summary>
        /// Sets the value of the given row's DataMember
        /// </summary>
        /// <param name="row"></param>
        /// <param name="member"></param>
        /// <param name="value"></param>
        /// <returns>true if the value was set, otherwise false.</returns>
        public bool SetValue( int row, LocalizedTextData.DataMember member, string value )
        {
            if ( (row >= 0) && (row < this.glacialTreeList.TreeList.Nodes.Count) )
            {
                GTLTreeNode node = this.glacialTreeList.TreeList.Nodes[row];
                LocalizedTextData data = node.Tag as LocalizedTextData;

                if ( !m_executingCommand )
                {
                    return DoCommand( new ChangeValueEditCommand( row, data, member, value ) );
                }
               
                string oldValue = data.GetToString( member );
                
                // set the LocalizedTextData
                data.SetFromString( member, value );

                // pull it out from the LocalizedTextData again to make sure it's in the correct format.
                value = data.GetToString( member );
                if ( (int)member == 0 )
                {
                    node.Text = value;
                }
                else if ( member == LocalizedTextData.DataMember.Uppercase )
                {
                    node.SubItems[(int)member].Checked = data.Uppercase;
                }
                else
                {
                    node.SubItems[(int)member].Text = value;
                }

                if ( member == LocalizedTextData.DataMember.Identifier )
                {
                    // update our dictionary of unique IDs
                    ChangeUniqueID( value, oldValue );
                }
                else if ( member == LocalizedTextData.DataMember.Speaker )
                {
                    // update our dictionary of unique Speakers
                    ChangeUniqueSpeaker( value, oldValue );
                }
                else if ( member == LocalizedTextData.DataMember.String )
                {
                    // validation check.
                    rageStatus status;
                    this.TextDataCollection.ValidateVariableUsage( data, out status );
                    if ( !status.Success() )
                    {
                        rageMessageBox.ShowExclamation( FindForm(),
                            String.Format( "{0}\n\nPlease resolve this before exporting to the game.", status.ErrorString ),
                            "Localized Text Error" );
                    }
                }

                OnLocalizedTextDataChanged( new LocalizedTextDataChangedEventArgs( data, member, oldValue ) );

                return true;
            }

            return false;
        }

        /// <summary>
        /// Gets the value at the given row's DataMember.
        /// </summary>
        /// <param name="row"></param>
        /// <param name="member"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public bool GetValue( int row, LocalizedTextData.DataMember member, ref string value )
        {
            if ( (row >= 0) && (row < this.glacialTreeList.TreeList.Nodes.Count) )
            {
                GTLTreeNode node = this.glacialTreeList.TreeList.Nodes[row];
                LocalizedTextData data = node.Tag as LocalizedTextData;

                value = data.GetToString( member );
                return true;
            }

            return false;
        }

        /// <summary>
        /// Sets the value of the given LocalizedTextData's DataMember, including its cell in the spreadsheet.
        /// </summary>
        /// <param name="row"></param>
        /// <param name="member"></param>
        /// <param name="value"></param>
        /// <returns>true if the value was set, otherwise false.</returns>
        public bool SetValue( LocalizedTextData data, LocalizedTextData.DataMember member, string value )
        {
            GTLTreeNode node;
            if ( m_treeNodesByKey.TryGetValue( data.GetHashCode(), out node ) )
            {
                return SetValue( node.Index, member, value );
            }

            return false;
        }

        /// <summary>
        /// Gets the value of the given LocalizedTextData's DataMember.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="member"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public bool GetValue( LocalizedTextData data, LocalizedTextData.DataMember member, ref string value )
        {
            GTLTreeNode node;
            if ( m_treeNodesByKey.TryGetValue( data.GetHashCode(), out node ) )
            {
                return GetValue( node.Index, member, ref value );
            }

            return false;
        }

        /// <summary>
        /// Removes the selected LocalizedTextData structures from the TreeList and copies them to the clipboard.
        /// </summary>
        public void CutSelectedToClipboard()
        {
            CopySelectedToClipboard();

            RemoveSelected();           
        }

        /// <summary>
        /// Copies a list of all selected LocalizedTextData structures to the clipboard.
        /// </summary>
        public void CopySelectedToClipboard()
        {
            LocalizedTextDataCollection collection = new LocalizedTextDataCollection();
            foreach ( GTLTreeNode node in this.glacialTreeList.TreeList.SelectedNodes )
            {
                LocalizedTextData data = (node.Tag as LocalizedTextData).Clone() as LocalizedTextData;
                collection.Add( data );
            }

            while ( true )
            {
                try
                {
                    Clipboard.SetDataObject( new DataObject( c_clipboardDataObjectName, collection ) );

                    IDataObject dataObj = Clipboard.GetDataObject();
                    Debug.Assert( dataObj != null );
                    Debug.Assert( dataObj.GetDataPresent( c_clipboardDataObjectName ) );
                    break;
                }
                catch
                {
                    DialogResult result = rageMessageBox.ShowExclamation( FindForm(), "Unable to copy the items to the clipboard.", "Clipboard Error",
                        MessageBoxButtons.RetryCancel );
                    if ( result == DialogResult.Cancel )
                    {
                        return;
                    }
                }
            }
        }

        /// <summary>
        /// Adds to the TreeList the list of LocalizedTextData structures from the clipboard, if any.
        /// </summary>
        public void PasteFromClipboard()
        {
            // Retrieves the data from the clipboard.
            IDataObject dataObj = Clipboard.GetDataObject();
            if ( dataObj != null )
            {
                object obj = dataObj.GetData( c_clipboardDataObjectName );
                if ( obj != null )
                {
                    LocalizedTextDataCollection collection = obj as LocalizedTextDataCollection;
                    if ( collection.Count > 1 )
                    {
                        Add( collection );
                    }
                    else
                    {
                        Add( collection[0] );
                    }
                }
            }
        }

        /// <summary>
        /// Removes all of the selected LocalizedTextData structures from the TreeList.
        /// </summary>
        public void RemoveSelected()
        {
            LocalizedTextDataCollection collection = new LocalizedTextDataCollection();
            foreach ( GTLTreeNode node in this.glacialTreeList.TreeList.SelectedNodes )
            {
                collection.Add( node.Tag as LocalizedTextData );
            }

            if ( collection.Count == 1 )
            {
                Remove( collection[0] );
            }
            else if ( collection.Count > 1 )
            {
                Remove( collection );
            }
        }

        public void Undo()
        {
            if ( this.CanUndo )
            {
                EditCommand command = m_undoStack[m_undoStack.Count - 1];
                m_undoStack.RemoveAt( m_undoStack.Count - 1 );
                
                UndoCommand( command );
            }
        }

        public void Redo()
        {
            if ( this.CanRedo )
            {
                EditCommand command = m_redoStack[m_redoStack.Count - 1];
                m_redoStack.RemoveAt( m_redoStack.Count - 1 );

                RedoCommand( command );
            }
        }

        public void ValidateIdentifiers()
        {
            if ( String.IsNullOrEmpty( this.Filename ) )
            {
                return;
            }

            string name = Path.GetFileNameWithoutExtension( this.Filename );

            foreach ( GTLTreeNode node in this.glacialTreeList.Nodes )
            {
                bool bIsAllDigits = true;
                foreach ( char c in node.Text )
                {
                    if ( !Char.IsDigit( c ) )
                    {
                        bIsAllDigits = false;
                        break;
                    }
                }

                if ( !bIsAllDigits )
                {
                    continue;
                }

                // If we're all digits, prepend the filename
                LocalizedTextData data = node.Tag as LocalizedTextData;

                LocalizedTextDataCancelEditEventArgs changeEventArgs = new LocalizedTextDataCancelEditEventArgs( data,
                    LocalizedTextData.DataMember.Identifier );
                OnLocalizedTextDataChange( changeEventArgs );
                if ( changeEventArgs.Cancel )
                {
                    continue;
                }

                string oldValue = node.Text;
                string newValue = String.Format( "{0}{1}", name, node.Text );

                // Make sure we have a unique name
                bool bAlreadyExists = false;
                foreach ( LocalizedTextData obData in this.TextDataCollection )
                {
                    if ( obData == data )
                    {
                        continue;
                    }

                    if ( obData.ID == newValue )
                    {
                        bAlreadyExists = true;
                    }
                }

                if ( bAlreadyExists )
                {
                    newValue = NextID();
                }

                LocalizedTextDataPreChangedEventArgs preChangedEventArgs = new LocalizedTextDataPreChangedEventArgs( data,
                    LocalizedTextData.DataMember.Identifier, oldValue, newValue );
                OnLocalizedTextDataPreChanged( preChangedEventArgs );
                if ( preChangedEventArgs.Cancelled )
                {
                    continue;
                }

                data.ID = newValue;
                node.Text = newValue;

                ChangeUniqueID( newValue, node.Text );

                OnLocalizedTextDataChanged( new LocalizedTextDataChangedEventArgs( data,
                    LocalizedTextData.DataMember.Identifier, oldValue ) );
            }
        }
        #endregion

        #region Private Functions
        private void AddUnqiueID( string id )
        {
            if ( !String.IsNullOrEmpty( id ) )
            {
                int count;
                if ( m_uniqueIDDictionary.TryGetValue( id, out count ) )
                {
                    ++count;
                    m_uniqueIDDictionary[id] = count;
                }
                else
                {
                    m_uniqueIDDictionary.Add( id, 1 );
                }
            }
        }

        private void RemoveUniqueID( string id )
        {
            if ( !String.IsNullOrEmpty( id ) )
            {
                int count;
                if ( m_uniqueIDDictionary.TryGetValue( id, out count ) )
                {
                    --count;
                    if ( count == 0 )
                    {
                        m_uniqueIDDictionary.Remove( id );
                    }
                    else
                    {
                        m_uniqueIDDictionary[id] = count;
                    }
                }
            }
        }

        private void ChangeUniqueID( string id, string oldId )
        {
            if ( id != oldId )
            {
                RemoveUniqueID( oldId );
                AddUnqiueID( id );

                rageRichTextBoxSearchReplaceOptions options = new rageRichTextBoxSearchReplaceOptions();

                options.FindText = String.Format( "$({0})", oldId );
                options.MatchCase = false;
                options.NoHighlight = true;
                options.ReplaceText = String.Format( "$({0})", id );
                options.Reverse = false;
                options.WholeWord = false;

                // Update variables
                foreach ( GTLTreeNode node in this.glacialTreeList.TreeList.Nodes )
                {
                    if ( node.Text == oldId )
                    {
                        continue;
                    }

                    LocalizedTextData data = node.Tag as LocalizedTextData;

                    LocalizedTextDataCancelEditEventArgs changeEventArgs = new LocalizedTextDataCancelEditEventArgs( data,
                        LocalizedTextData.DataMember.String );
                    OnLocalizedTextDataChange( changeEventArgs );
                    if ( changeEventArgs.Cancel )
                    {
                        continue;
                    }

                    string oldValue = data.Rtf;
                    string value = data.Rtf.Replace( options.FindText, options.ReplaceText );

                    if ( value == oldValue )
                    {
                        continue;
                    }

                    LocalizedTextDataPreChangedEventArgs preChangedEventArgs = new LocalizedTextDataPreChangedEventArgs( data,
                        LocalizedTextData.DataMember.String, oldValue, value );
                    OnLocalizedTextDataPreChanged( preChangedEventArgs );
                    if ( preChangedEventArgs.Cancelled )
                    {
                        continue;
                    }

                    data.Rtf = value;
                    node.SubItems[(int)LocalizedTextData.DataMember.String].Text = value;

                    if ( node.SubItems[(int)LocalizedTextData.DataMember.String].Control is LocalizedRichTextBoxColumnTemplate )
                    {
                        LocalizedRichTextBoxColumnTemplate localizedRichTextBoxColumnTemplate 
                            = node.SubItems[(int)LocalizedTextData.DataMember.String].Control as LocalizedRichTextBoxColumnTemplate;

                        int offset = 0;
                        int indexOf = -1;
                        do
                        {
                            indexOf = localizedRichTextBoxColumnTemplate.Find( options.FindText, offset, options.Finds );
                            if ( indexOf != -1 )
                            {
                                localizedRichTextBoxColumnTemplate.SelectionStart = indexOf;
                                localizedRichTextBoxColumnTemplate.SelectionLength = options.FindText.Length;
                                localizedRichTextBoxColumnTemplate.SelectedText = options.ReplaceText;

                                offset = indexOf + options.ReplaceText.Length;
                            }
                        } while ( indexOf != -1 );

                        data.Text = localizedRichTextBoxColumnTemplate.RawText;
                    }

                    OnLocalizedTextDataChanged( new LocalizedTextDataChangedEventArgs( data, 
                        LocalizedTextData.DataMember.String, oldValue ) );
                }
            }
        }

        private void AddUnqiueSpeaker( string speaker )
        {
            if ( !String.IsNullOrEmpty( speaker ) )
            {
                int count;
                if ( m_uniqueSpeakerDictionary.TryGetValue( speaker, out count ) )
                {
                    ++count;
                    m_uniqueSpeakerDictionary[speaker] = count;
                }
                else
                {
                    m_uniqueSpeakerDictionary.Add( speaker, 1 );
                }
            }
        }

        private void RemoveUniqueSpeaker( string speaker )
        {
            if ( !String.IsNullOrEmpty( speaker ) )
            {
                int count;
                if ( m_uniqueSpeakerDictionary.TryGetValue( speaker, out count ) )
                {
                    --count;
                    if ( count == 0 )
                    {
                        m_uniqueSpeakerDictionary.Remove( speaker );
                    }
                    else
                    {
                        m_uniqueSpeakerDictionary[speaker] = count;
                    }
                }
            }
        }

        private void ChangeUniqueSpeaker( string speaker, string oldSpeaker )
        {
            if ( speaker != oldSpeaker )
            {
                RemoveUniqueSpeaker( oldSpeaker );
                AddUnqiueSpeaker( speaker );
            }
        }

        private GTLTreeNode CreateTreeNode( LocalizedTextData data )
        {
            // see if we've already added the node
            GTLTreeNode node;
            if ( m_treeNodesByKey.TryGetValue( data.GetHashCode(), out node ) )
            {
                throw (new Exception( 
                    String.Format( "Unable to add the new line.  The line with ID '{0}' for the {1} language has already been added.",
                        data.ID, data.Language ) ));
            }

            // see if we already have a node with the same ID and language
            if ( data.ID != string.Empty )
            {
                foreach ( GTLTreeNode n in this.glacialTreeList.TreeList.Nodes )
                {
                    LocalizedTextData nData = n.Tag as LocalizedTextData;
                    if ( (data.ID == nData.ID) && (data.Language == nData.Language) )
                    {
                        throw (new Exception(
                            String.Format( "Unable to add the new line.  The ID '{0}' for the {1} language already exists.", data.ID, 
                            String.IsNullOrEmpty( data.Language) ? "undetermined" : data.Language ) ));
                    }
                }
            }

            node = new GTLTreeNode( data.ID );
            node.Tag = data;

            for ( int i = 1; i <= (int)LocalizedTextData.DataMember.Comment; ++i )
            {
                GTLSubItem gtlSubItem;
                if ( i == (int)LocalizedTextData.DataMember.Uppercase )
                {
                    gtlSubItem = new GTLSubItem();
                    gtlSubItem.Checked = data.Uppercase;
                }
                else
                {
                    gtlSubItem = new GTLSubItem( data.GetToString( (LocalizedTextData.DataMember)i ) );
                }

                node.SubItems.Add( gtlSubItem );
            }

            AddUnqiueID( data.ID );
            AddUnqiueSpeaker( data.Speaker );

            return node;
        }

        /// <summary>
        /// Fills the text of the edit control appropriately
        /// </summary>
        /// <param name="node"></param>
        /// <param name="column"></param>
        /// <param name="text"></param>
        /// <param name="theControl"></param>
        /// <returns>false if we should cancel the edit, true if we handled the transfer</returns>
        private bool SetupEditControl( GTLTreeNode node, int column, string text, Control theControl )
        {
            if ( column >= node.SubItems.Count )
            {
                return false;
            }

            LocalizedTextDataCancelEditEventArgs cancelEventArgs 
                = new LocalizedTextDataCancelEditEventArgs( m_textDataCollection[node.Index], (LocalizedTextData.DataMember)column );
            OnLocalizedTextDataChange( cancelEventArgs );
            if ( cancelEventArgs.Cancel )
            {
                return false;
            }

            // the control should always know what subItem it is editing
            theControl.Tag = node.SubItems[column];

            LocalizedTextData.DataMember member = (LocalizedTextData.DataMember)column;

            // setup specific edit controls
            if ( theControl is GComboBox )
            {
                GComboBox comboBox = theControl as GComboBox;
                comboBox.DropDownStyle = (member == LocalizedTextData.DataMember.Speaker) ? ComboBoxStyle.DropDown : ComboBoxStyle.DropDownList;

                comboBox.Items.Clear();

                switch ( member )
                {
                    case LocalizedTextData.DataMember.Font:
                        {
                            foreach ( LocalizedTextFontOption option in this.LocalizedTextOptions.FontOptionCollection )
                            {
                                comboBox.Items.Add( option.Name );
                            }    
                        }
                        break;
                    case LocalizedTextData.DataMember.Language:
                        {
                            foreach ( LocalizedTextLanguageOption option in this.LocalizedTextOptions.LanguageOptionCollection )
                            {
                                comboBox.Items.Add( option.Language );
                            }
                        }
                        break;
                    case LocalizedTextData.DataMember.Platform:
                        {
                            foreach ( LocalizedTextPlatformOption option in this.LocalizedTextOptions.PlatformOptionCollection )
                            {
                                comboBox.Items.Add( option.Name );
                            }
                        }
                        break;
                    case LocalizedTextData.DataMember.Region:
                        {
                            foreach ( LocalizedTextRegionOption option in this.LocalizedTextOptions.RegionOptionCollection )
                            {
                                comboBox.Items.Add( option.Name );
                            }
                        }
                        break;
                    case LocalizedTextData.DataMember.Speaker:
                        {                           
                            foreach ( KeyValuePair<string, int> pair in m_uniqueSpeakerDictionary )
                            {
                                comboBox.Items.Add( pair.Key );
                            }
                        }
                        break;
                    default:
                        return false;   // something is wrong here                        
                }
                
                comboBox.SelectedIndex = comboBox.Items.IndexOf( text );
            }
            else if ( theControl is NumericUpDownColumnTemplate )
            {
                NumericUpDownColumnTemplate numericControl = theControl as NumericUpDownColumnTemplate;
                if ( (column == (int)LocalizedTextData.DataMember.OffsetX) || (column == (int)LocalizedTextData.DataMember.OffsetY) )
                {
                    numericControl.DecimalPlaces = 0;

                    if ( column == (int)LocalizedTextData.DataMember.OffsetX )
                    {
                        numericControl.Minimum = -this.LocalizedTextOptions.ScreenWidth;
                        numericControl.Maximum = this.LocalizedTextOptions.ScreenWidth;
                        numericControl.Increment = 1;
                    }
                    else
                    {
                        numericControl.Minimum = -this.LocalizedTextOptions.ScreenHeight;
                        numericControl.Maximum = this.LocalizedTextOptions.ScreenHeight;
                        numericControl.Increment = 1;
                    }
                }
                else
                {
                    numericControl.DecimalPlaces = 3;
                    
                    numericControl.Minimum = 0;
                    numericControl.Maximum = 1;
                    numericControl.Increment = 0.1M;
                }

                numericControl.Text = text;
            }
            else if ( theControl is MultiSelectComboBoxColumnTemplate )
            {
                List<string> options = new List<string>();
                foreach ( LocalizedTextFlagOption option in this.LocalizedTextOptions.FlagOptionCollection )
                {
                    options.Add( option.Name );
                }

                MultiSelectComboBoxColumnTemplate msComboBox = theControl as MultiSelectComboBoxColumnTemplate;
                msComboBox.TextIsFlags = true;
                msComboBox.AddMenuItems( options );
                msComboBox.Text = text;
            }
            else if ( column == (int)LocalizedTextData.DataMember.Uppercase )
            {
                bool val = node.SubItems[column].Checked;
                if ( LocalizedTextData.FromBoolString( text, ref val ) )
                {
                    node.SubItems[column].Checked = val;
                }
            }
            else
            {
                theControl.Text = text;
            }            

            return true;
        }

        /// <summary>
        /// Fills the GLTSubItem after an edit has been made.  Also, messages the game when a value changes.
        /// </summary>
        /// <param name="node"></param>
        /// <param name="column"></param>
        /// <param name="theControl"></param>
        /// <param name="transferHandled"></param>
        /// <returns>true if we should change the display value</returns>
        private bool ReadEditControl( GTLTreeNode node, int column, string text, Control theControl )
        {
            if ( column >= node.SubItems.Count )
            {
                return false;
            }

            GTLSubItem gtlSubItem = node.SubItems[column];
            LocalizedTextData data = node.Tag as LocalizedTextData;

            bool success = false;

            string originalText = gtlSubItem.Text;
            bool modify = false;

            LocalizedTextData.DataMember member = (LocalizedTextData.DataMember)column;

			LocalizedTextDataPreChangedEventArgs preEvent = new LocalizedTextDataPreChangedEventArgs(data, member, originalText, text);
			OnLocalizedTextDataPreChanged(preEvent);
			if (preEvent.Cancelled)
			{
				return false;
			}
			
			
			switch ( member )
            {
                case LocalizedTextData.DataMember.Identifier:
                    {
                        success = true;
                        modify = text != originalText;

                        if ( success && modify )
                        {
                            // verify that we don't already have this id for the given language
                            foreach ( GTLTreeNode treeNode in this.glacialTreeList.TreeList.Nodes )
                            {
                                if ( treeNode == node )
                                {
                                    continue;
                                }

                                LocalizedTextData nodeData = treeNode.Tag as LocalizedTextData;
                                if ( (nodeData.ID == text) && (data.Language == nodeData.Language) )
                                {
                                    rageMessageBox.ShowExclamation( FindForm(),
                                        String.Format( "Duplicate ID for the {0} language.  Line {1} vs line {2}.",
                                        String.IsNullOrEmpty( data.Language ) ? "undetermined" : data.Language, treeNode.Index, node.Index ),
                                        "Duplicate ID" );

                                    success = false;
                                    modify = false;
                                    break;
                                }
                            }
                        }
                    }
                    break;
                case LocalizedTextData.DataMember.Language:
                    {
                        GComboBox comboBox = theControl as GComboBox;

                        int indexOf = comboBox.Items.IndexOf( text );
                        success = indexOf != -1;

                        modify = text != originalText;

                        if ( success && modify )
                        {
                            // verify that we don't already have this id for the given language
                            foreach ( GTLTreeNode treeNode in this.glacialTreeList.TreeList.Nodes )
                            {
                                if ( treeNode == node )
                                {
                                    continue;
                                }

                                LocalizedTextData nodeData = treeNode.Tag as LocalizedTextData;
                                if ( (nodeData.Language == text) && (data.ID == nodeData.ID) )
                                {
                                    rageMessageBox.ShowExclamation( FindForm(),
                                        String.Format( "That language already has ID '{0}'.  Line {1} vs line {2}.",
                                        data.ID, treeNode.Index, node.Index ),
                                        "Duplicate Language" );

                                    success = false;
                                    modify = false;
                                    break;
                                }
                            }
                        }
                    }
                    break;
                case LocalizedTextData.DataMember.Region:
                case LocalizedTextData.DataMember.Platform:
                case LocalizedTextData.DataMember.Font:
                    {
                        GComboBox comboBox = theControl as GComboBox;

                        int indexOf = comboBox.Items.IndexOf( text );
                        success = indexOf != -1;

                        modify = text != originalText;
                    }
                    break;
                case LocalizedTextData.DataMember.Speaker:
                case LocalizedTextData.DataMember.String:
                case LocalizedTextData.DataMember.Comment:
                    {
                        success = true;
                        modify = text != originalText;
                    }
                    break;
                case LocalizedTextData.DataMember.ScaleX:
                case LocalizedTextData.DataMember.ScaleY:
                    {
                        // see if we have a float.  make sure it has a decimal point.  remove trailing 'f' as in '5.46f'
                        success = GetFloatString( ref text );
                        if ( success && GetFloatString( ref originalText ) )
                        {
                            modify = text != originalText;
                        }
                    }
                    break;
                case LocalizedTextData.DataMember.OffsetX:
                case LocalizedTextData.DataMember.OffsetY:
                    {
                        // see if we really have an integer
                        success = GetIntString( ref text );
                        if ( success && GetIntString( ref originalText ) )
                        {
                            modify = text != originalText;
                        }
                    }
                    break;
                case LocalizedTextData.DataMember.Flags:
                    {
                        // see if we really have a hex number
                        success = GetHexString( ref text );
                        if ( success && GetHexString( ref originalText ) )
                        {
                            modify = text != originalText;
                        }
                    }
                    break;
                case LocalizedTextData.DataMember.Uppercase:
                    {
                        text = node.SubItems[column].Checked.ToString();

                        success = GetBoolString( ref text );
                        if ( success && GetBoolString( ref originalText ) )
                        {
                            modify = text != originalText;
                        }
                    }
                    break;
                default:
                    break;
            }

            if ( modify && success )
            {
                if ( member == LocalizedTextData.DataMember.String )
                {
                    // update the LocalizedTextData item's text
                    LocalizedRichTextBoxColumnTemplate localizedRichTextBoxColumnTemplate = theControl as LocalizedRichTextBoxColumnTemplate;
                    data.Text = localizedRichTextBoxColumnTemplate.RawText;
                }

                SetValue( data, member, text );
            }

            return success;
        }

        private bool DoCommand( EditCommand command )
        {
            m_executingCommand = true;
            bool result = command.Do( this );
            m_executingCommand = false;

            if ( m_undoStack.Count == c_undoRedoStackCapacity )
            {
                m_undoStack.RemoveAt( 0 );
            }

            if ( result )
            {
                m_undoStack.Add( command );
                m_redoStack.Clear();

                if ( !this.IsModified )
                {
                    this.IsModified = true;
                    OnModifiedChanged();
                }
            }

            this.undoToolStripMenuItem.Enabled = this.CanUndo;
            this.redoToolStripMenuItem.Enabled = this.CanRedo;

            return result;
        }

        private bool UndoCommand( EditCommand command )
        {
            m_executingCommand = true;
            bool result = command.Undo( this );
            m_executingCommand = false;

            if ( m_redoStack.Count == c_undoRedoStackCapacity )
            {
                m_redoStack.RemoveAt( 0 );
            }

            if ( result )
            {
                m_redoStack.Add( command );
                
                if ( m_undoStack.Count == m_savePosition )
                {
                    this.IsModified = false;
                    OnModifiedChanged();
                }
            }

            this.undoToolStripMenuItem.Enabled = this.CanUndo;
            this.redoToolStripMenuItem.Enabled = this.CanRedo;

            return result;
        }

        private bool RedoCommand( EditCommand command )
        {
            m_executingCommand = true;
            bool result = command.Do( this );
            m_executingCommand = false;

            if ( m_undoStack.Count == c_undoRedoStackCapacity )
            {
                m_undoStack.RemoveAt( 0 );
            }

            m_undoStack.Add( command );

            if ( result )
            {
                if ( !this.IsModified )
                {
                    this.IsModified = true;
                    OnModifiedChanged();
                }
            }

            this.undoToolStripMenuItem.Enabled = this.CanUndo;
            this.redoToolStripMenuItem.Enabled = this.CanRedo;

            return result;
        }

        private bool GetBoolString( ref string t )
        {
            bool val = true;
            return LocalizedTextData.FromBoolString( t, ref val );
        }
        
        private bool GetFloatString( ref string t )
        {
            string temp = t.ToUpper();
            int indexOf = temp.IndexOf( "F" );
            if ( indexOf != -1 )
            {
                temp = temp.Remove( indexOf );
            }

            try
            {
                float val = (float)Convert.ToDouble( temp );
                if ( LocalizedTextData.ToFloatString( val, ref temp ) )
                {
                    t = temp;
                    return true;
                }
            }
            catch
            {
            }

            return false;
        }

        private bool GetIntString( ref string t )
        {
            try
            {
                int val = Convert.ToInt32( t );
                return LocalizedTextData.ToIntString( val, ref t );
            }
            catch
            {
                return false;
            }
        }

        private bool GetHexString( ref string t )
        {
            string temp = t.ToUpper();
            uint val = 0;
            if ( temp.StartsWith( "0X" ) )
            {
                temp = temp.Remove( 0, 2 );
                
                try
                {
                    val = UInt32.Parse( temp, System.Globalization.NumberStyles.AllowHexSpecifier );                    
                }
                catch
                {
                    return false;
                }
            }
            else
            {
                try
                {
                    val = Convert.ToUInt32( temp );
                }
                catch
                {
                    return false;
                }
            }

            if ( LocalizedTextData.ToHexString( val, ref temp ) )
            {
                t = temp;
                return true;
            }

            return false;
        }

        private void UpdateAlwaysVisibleItem( Control theControl )
        {
            if ( theControl == null )
            {
                return;
            }

            GTLSubItem gtlSubItem = theControl.Tag as GTLSubItem;
            if ( gtlSubItem == null )
            {
                return;
            }

            string text = theControl.Text;
            if ( theControl is LocalizedRichTextBoxColumnTemplate )
            {
                text = (theControl as LocalizedRichTextBoxColumnTemplate).Rtf;
            }

            if ( !ReadEditControl( gtlSubItem.ParentNode, gtlSubItem.ParentNode.SubItems.IndexOf( gtlSubItem ), text, theControl ) )
            {
                // reset the control's text because it's not valid
                if ( theControl is GComboBox )
                {
                    GComboBox comboBox = theControl as GComboBox;
                    comboBox.SelectedIndex = comboBox.Items.IndexOf( gtlSubItem.Text );
                }
                else
                {
                    theControl.Text = gtlSubItem.Text;
                }
            }
        }
        #endregion        
    }

    #region EventArgs and EventHandlers
    public class LocalizedTextDataEventArgs : EventArgs
    {
        public LocalizedTextDataEventArgs( LocalizedTextData data )
        {
            m_localizedTextData = data;
        }

        #region Variables
        private LocalizedTextData m_localizedTextData;
        #endregion

        #region Properties
        public LocalizedTextData LocalizedTextData
        {
            get
            {
                return m_localizedTextData;
            }
        }
        #endregion
    }

    public delegate void LocalizedTextDataEventHandler( object sender, LocalizedTextDataEventArgs e );

    public class LocalizedTextDataCancelEventArgs : EventArgs
    {
        public LocalizedTextDataCancelEventArgs( LocalizedTextData data )
        {
            m_collection.Add( data );
        }

        public LocalizedTextDataCancelEventArgs( LocalizedTextDataCollection collection )
        {
            m_collection.AddRange( collection );
        }

        #region Variables
        private LocalizedTextDataCollection m_collection = new LocalizedTextDataCollection();
        private bool m_cancel = false;
        #endregion

        #region Properties
        public LocalizedTextData LocalizedTextData
        {
            get
            {
                return m_collection[0];
            }
        }

        public LocalizedTextDataCollection TextDataCollection
        {
            get
            {
                return m_collection;
            }
        }

        public bool Cancel
        {
            get
            {
                return m_cancel;
            }
            set
            {
                m_cancel = value;
            }
        }
        #endregion
    }

    public delegate void LocalizedTextDataCancelEventHandler( object sender, LocalizedTextDataCancelEventArgs e );

    public class LocalizedTextDataCancelEditEventArgs : LocalizedTextDataCancelEventArgs
    {
        public LocalizedTextDataCancelEditEventArgs( LocalizedTextData data, LocalizedTextData.DataMember member )
            : base( data )
        {
            m_member = member;
        }

        #region Variables
        private LocalizedTextData.DataMember m_member;
        #endregion

        #region Properties
        public LocalizedTextData.DataMember Member
        {
            get
            {
                return m_member;
            }
        }
        #endregion
    }

    public delegate void LocalizedTextDataCancelEditEventHandler( object sender, LocalizedTextDataCancelEditEventArgs e );


	public class LocalizedTextDataPreChangedEventArgs : LocalizedTextDataChangedEventArgs
	{
		public LocalizedTextDataPreChangedEventArgs(LocalizedTextData data, LocalizedTextData.DataMember member, string previousStringValue, string newStringValue)
			: base(data, member, previousStringValue)
		{
			m_newStringValue = newStringValue;
		}

		#region Variables
		private string m_newStringValue;
		private bool m_bCancelled = false;
		#endregion

		#region Properties
		/// <summary>
		/// Set to true if action needs to be cancelled.
		/// </summary>
		public bool Cancelled
		{
			get
			{
				return m_bCancelled;
			}
			set
			{
				m_bCancelled = value;
			}
		}

		/// <summary>
		/// The new string value of the member that was changed.
		/// </summary>
		public string NewStringValue
		{
			get
			{
				return m_newStringValue;
			}
		}

		#endregion
	}

	public delegate void LocalizedTextDataPreChangedEventHandler(object sender, LocalizedTextDataPreChangedEventArgs e);


    public class LocalizedTextDataChangedEventArgs : LocalizedTextDataEventArgs
    {
        public LocalizedTextDataChangedEventArgs( LocalizedTextData data, LocalizedTextData.DataMember member, string previousStringValue )
            : base( data )
        {
            m_member = member;
            m_previousStringValue = previousStringValue;
        }
        
        #region Variables
        private LocalizedTextData.DataMember m_member;
        private string m_previousStringValue;
        #endregion

        #region Properties
        /// <summary>
        /// The member that was changed.
        /// </summary>
        public LocalizedTextData.DataMember Member
        {
            get
            {
                return m_member;
            }
        }

        /// <summary>
        /// The previous string value of the member that was changed.
        /// </summary>
        public string PreviousStringValue
        {
            get
            {
                return m_previousStringValue;
            }
        }
        #endregion
    }

    public delegate void LocalizedTextDataChangedEventHandler( object sender, LocalizedTextDataChangedEventArgs e );

    public class LocalizedTextDataIndexEventArgs : LocalizedTextDataEventArgs
    {
        public LocalizedTextDataIndexEventArgs( LocalizedTextData data, int index )
            : base( data )
        {
            m_index = index;
        }
        
        #region Variables
        private int m_index = -1;
        #endregion

        #region Properties
        public int Index
        {
            get
            {
                return m_index;
            }
        }
        #endregion
    }

    public delegate void LocalizedTextDataIndexEventHandler( object sender, LocalizedTextDataIndexEventArgs e );
    
    public class LocalizedTextDataMovedEventArgs : LocalizedTextDataIndexEventArgs
    {
        public LocalizedTextDataMovedEventArgs( LocalizedTextData data, int index, int oldIndex )
            : base( data, index )
        {
            m_oldIndex = oldIndex;
        }
        
        #region Variables
        private int m_oldIndex = -1;
        #endregion

        #region Properties
        public int OldIndex
        {
            get
            {
                return m_oldIndex;
            }
        }
        #endregion
    }

    public delegate void LocalizedTextDataMovedEventHandler( object sender, LocalizedTextDataMovedEventArgs e );
    #endregion
}
