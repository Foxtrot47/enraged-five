using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Reflection;

namespace Rockstar.UnicodeTool
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class Form1 : System.Windows.Forms.Form
	{
		private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.RichTextBox richTextBox2;
		private System.Windows.Forms.RichTextBox richTextBox3;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Button buttonUniqueGen;
		private System.Windows.Forms.RichTextBox richTextBoxOriginalText;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Splitter splitter1;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.MainMenu mainMenu1;
		private System.Windows.Forms.MenuItem menuItem1;
		private System.Windows.Forms.MenuItem menuItemExit;
		private System.Windows.Forms.MenuItem menuItem2;
        private System.Windows.Forms.MenuItem menuItemAbout;
        private Panel panel3;
        private LinkLabel linkLabelClear;
        private Panel panel4;
        private Panel panel7;
        private Panel panel6;
        private LinkLabel linkLabel4;
        private LinkLabel linkLabel9;
        private LinkLabel linkLabel5;
        private LinkLabel linkLabel6;
        private TextBox textBoxEnd;
        private TextBox textBoxStart;
        private LinkLabel linkLabelKanji;
        private Label label5;
        private Button button1;
        private Panel panel5;
        private LinkLabel linkLabel1;
        private Label label6;
        private StatusStrip statusStrip1;
        private ToolStripStatusLabel toolStripStatusLabel1;
        private ToolStripStatusLabel toolStripStatusLabel2;
        private IContainer components;

		public Form1()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.richTextBox2 = new System.Windows.Forms.RichTextBox();
            this.richTextBox3 = new System.Windows.Forms.RichTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.buttonUniqueGen = new System.Windows.Forms.Button();
            this.richTextBoxOriginalText = new System.Windows.Forms.RichTextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.panel2 = new System.Windows.Forms.Panel();
            this.mainMenu1 = new System.Windows.Forms.MainMenu(this.components);
            this.menuItem1 = new System.Windows.Forms.MenuItem();
            this.menuItemExit = new System.Windows.Forms.MenuItem();
            this.menuItem2 = new System.Windows.Forms.MenuItem();
            this.menuItemAbout = new System.Windows.Forms.MenuItem();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.textBoxEnd = new System.Windows.Forms.TextBox();
            this.textBoxStart = new System.Windows.Forms.TextBox();
            this.panel6 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.linkLabelKanji = new System.Windows.Forms.LinkLabel();
            this.linkLabel4 = new System.Windows.Forms.LinkLabel();
            this.linkLabel9 = new System.Windows.Forms.LinkLabel();
            this.linkLabel5 = new System.Windows.Forms.LinkLabel();
            this.linkLabel6 = new System.Windows.Forms.LinkLabel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.linkLabelClear = new System.Windows.Forms.LinkLabel();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel4.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // richTextBox1
            // 
            this.richTextBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.richTextBox1.Location = new System.Drawing.Point(104, 32);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.ReadOnly = true;
            this.richTextBox1.Size = new System.Drawing.Size(328, 246);
            this.richTextBox1.TabIndex = 12;
            this.richTextBox1.Text = "";
            // 
            // richTextBox2
            // 
            this.richTextBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.richTextBox2.Location = new System.Drawing.Point(104, 310);
            this.richTextBox2.Name = "richTextBox2";
            this.richTextBox2.ReadOnly = true;
            this.richTextBox2.Size = new System.Drawing.Size(328, 136);
            this.richTextBox2.TabIndex = 13;
            this.richTextBox2.Text = "";
            // 
            // richTextBox3
            // 
            this.richTextBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.richTextBox3.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox3.Location = new System.Drawing.Point(104, 478);
            this.richTextBox3.Name = "richTextBox3";
            this.richTextBox3.ReadOnly = true;
            this.richTextBox3.Size = new System.Drawing.Size(328, 168);
            this.richTextBox3.TabIndex = 14;
            this.richTextBox3.Text = "";
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label1.Location = new System.Drawing.Point(104, 454);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(232, 23);
            this.label1.TabIndex = 5;
            this.label1.Text = "Unused Characters:";
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(104, 8);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(192, 23);
            this.label2.TabIndex = 6;
            this.label2.Text = "Used Characters (for Flash):";
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label3.Location = new System.Drawing.Point(104, 286);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(232, 23);
            this.label3.TabIndex = 7;
            this.label3.Text = "Used Characters (for FontLab .ENC file):";
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(8, 8);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(208, 23);
            this.label4.TabIndex = 10;
            this.label4.Text = "Enter Text:";
            // 
            // buttonUniqueGen
            // 
            this.buttonUniqueGen.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.buttonUniqueGen.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buttonUniqueGen.Location = new System.Drawing.Point(16, 319);
            this.buttonUniqueGen.Name = "buttonUniqueGen";
            this.buttonUniqueGen.Size = new System.Drawing.Size(72, 23);
            this.buttonUniqueGen.TabIndex = 11;
            this.buttonUniqueGen.Text = ">>";
            this.buttonUniqueGen.Click += new System.EventHandler(this.buttonUniqueGen_Click);
            // 
            // richTextBoxOriginalText
            // 
            this.richTextBoxOriginalText.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.richTextBoxOriginalText.Location = new System.Drawing.Point(8, 32);
            this.richTextBoxOriginalText.Name = "richTextBoxOriginalText";
            this.richTextBoxOriginalText.Size = new System.Drawing.Size(163, 614);
            this.richTextBoxOriginalText.TabIndex = 10;
            this.richTextBoxOriginalText.Text = "";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.richTextBoxOriginalText);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(219, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(171, 654);
            this.panel1.TabIndex = 11;
            // 
            // splitter1
            // 
            this.splitter1.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitter1.Location = new System.Drawing.Point(387, 0);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(3, 654);
            this.splitter1.TabIndex = 12;
            this.splitter1.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.buttonUniqueGen);
            this.panel2.Controls.Add(this.richTextBox1);
            this.panel2.Controls.Add(this.richTextBox2);
            this.panel2.Controls.Add(this.richTextBox3);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel2.Location = new System.Drawing.Point(390, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(440, 654);
            this.panel2.TabIndex = 13;
            // 
            // mainMenu1
            // 
            this.mainMenu1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuItem1,
            this.menuItem2});
            // 
            // menuItem1
            // 
            this.menuItem1.Index = 0;
            this.menuItem1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuItemExit});
            this.menuItem1.Text = "&File";
            // 
            // menuItemExit
            // 
            this.menuItemExit.Index = 0;
            this.menuItemExit.Text = "E&xit";
            this.menuItemExit.Click += new System.EventHandler(this.menuItemExit_Click);
            // 
            // menuItem2
            // 
            this.menuItem2.Index = 1;
            this.menuItem2.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuItemAbout});
            this.menuItem2.Text = "&Help";
            // 
            // menuItemAbout
            // 
            this.menuItemAbout.Index = 0;
            this.menuItemAbout.Text = "&About R* Unicode Tool...";
            this.menuItemAbout.Click += new System.EventHandler(this.menuItemAbout_Click);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(119)))), ((int)(((byte)(151)))), ((int)(((byte)(224)))));
            this.panel3.Controls.Add(this.panel5);
            this.panel3.Controls.Add(this.panel7);
            this.panel3.Controls.Add(this.panel6);
            this.panel3.Controls.Add(this.panel4);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(219, 654);
            this.panel3.TabIndex = 8;
            // 
            // panel5
            // 
            this.panel5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(214)))), ((int)(((byte)(223)))), ((int)(((byte)(247)))));
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel5.Controls.Add(this.linkLabel1);
            this.panel5.Location = new System.Drawing.Point(12, 374);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(197, 50);
            this.panel5.TabIndex = 5;
            // 
            // linkLabel1
            // 
            this.linkLabel1.ActiveLinkColor = System.Drawing.Color.Black;
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.LinkColor = System.Drawing.Color.Black;
            this.linkLabel1.Location = new System.Drawing.Point(16, 16);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(145, 13);
            this.linkLabel1.TabIndex = 9;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "Count Number of Characters";
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked_1);
            // 
            // panel7
            // 
            this.panel7.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panel7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(214)))), ((int)(((byte)(223)))), ((int)(((byte)(247)))));
            this.panel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel7.Controls.Add(this.label6);
            this.panel7.Controls.Add(this.button1);
            this.panel7.Controls.Add(this.textBoxEnd);
            this.panel7.Controls.Add(this.textBoxStart);
            this.panel7.Location = new System.Drawing.Point(12, 261);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(197, 96);
            this.panel7.TabIndex = 4;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(90, 18);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(17, 13);
            this.label6.TabIndex = 6;
            this.label6.Text = "to";
            this.label6.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(102, 56);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 8;
            this.button1.Text = "Generate";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // textBoxEnd
            // 
            this.textBoxEnd.Location = new System.Drawing.Point(119, 15);
            this.textBoxEnd.Name = "textBoxEnd";
            this.textBoxEnd.Size = new System.Drawing.Size(58, 21);
            this.textBoxEnd.TabIndex = 7;
            // 
            // textBoxStart
            // 
            this.textBoxStart.Location = new System.Drawing.Point(19, 15);
            this.textBoxStart.Name = "textBoxStart";
            this.textBoxStart.Size = new System.Drawing.Size(58, 21);
            this.textBoxStart.TabIndex = 6;
            // 
            // panel6
            // 
            this.panel6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(214)))), ((int)(((byte)(223)))), ((int)(((byte)(247)))));
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel6.Controls.Add(this.label5);
            this.panel6.Controls.Add(this.linkLabelKanji);
            this.panel6.Controls.Add(this.linkLabel4);
            this.panel6.Controls.Add(this.linkLabel9);
            this.panel6.Controls.Add(this.linkLabel5);
            this.panel6.Controls.Add(this.linkLabel6);
            this.panel6.Location = new System.Drawing.Point(12, 79);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(197, 165);
            this.panel6.TabIndex = 3;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(16, 16);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(61, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Japanese";
            // 
            // linkLabelKanji
            // 
            this.linkLabelKanji.ActiveLinkColor = System.Drawing.Color.Black;
            this.linkLabelKanji.AutoSize = true;
            this.linkLabelKanji.LinkColor = System.Drawing.Color.Black;
            this.linkLabelKanji.Location = new System.Drawing.Point(16, 132);
            this.linkLabelKanji.Name = "linkLabelKanji";
            this.linkLabelKanji.Size = new System.Drawing.Size(85, 13);
            this.linkLabelKanji.TabIndex = 5;
            this.linkLabelKanji.TabStop = true;
            this.linkLabelKanji.Text = "Use Kanji Range";
            this.linkLabelKanji.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabelKanji_LinkClicked);
            // 
            // linkLabel4
            // 
            this.linkLabel4.ActiveLinkColor = System.Drawing.Color.Black;
            this.linkLabel4.AutoSize = true;
            this.linkLabel4.LinkColor = System.Drawing.Color.Black;
            this.linkLabel4.Location = new System.Drawing.Point(16, 110);
            this.linkLabel4.Name = "linkLabel4";
            this.linkLabel4.Size = new System.Drawing.Size(161, 13);
            this.linkLabel4.TabIndex = 4;
            this.linkLabel4.TabStop = true;
            this.linkLabel4.Text = "Use Half-Width Katakana Range";
            this.linkLabel4.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel3_LinkClicked);
            // 
            // linkLabel9
            // 
            this.linkLabel9.ActiveLinkColor = System.Drawing.Color.Black;
            this.linkLabel9.AutoSize = true;
            this.linkLabel9.LinkColor = System.Drawing.Color.Black;
            this.linkLabel9.Location = new System.Drawing.Point(17, 44);
            this.linkLabel9.Name = "linkLabel9";
            this.linkLabel9.Size = new System.Drawing.Size(105, 13);
            this.linkLabel9.TabIndex = 1;
            this.linkLabel9.TabStop = true;
            this.linkLabel9.Text = "Use Hiragana Range";
            this.linkLabel9.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabelHiragana_LinkClicked);
            // 
            // linkLabel5
            // 
            this.linkLabel5.ActiveLinkColor = System.Drawing.Color.Black;
            this.linkLabel5.AutoSize = true;
            this.linkLabel5.LinkColor = System.Drawing.Color.Black;
            this.linkLabel5.Location = new System.Drawing.Point(17, 88);
            this.linkLabel5.Name = "linkLabel5";
            this.linkLabel5.Size = new System.Drawing.Size(126, 13);
            this.linkLabel5.TabIndex = 3;
            this.linkLabel5.TabStop = true;
            this.linkLabel5.Text = "Use Alphanumeric Range";
            this.linkLabel5.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel2_LinkClicked);
            // 
            // linkLabel6
            // 
            this.linkLabel6.ActiveLinkColor = System.Drawing.Color.Black;
            this.linkLabel6.AutoSize = true;
            this.linkLabel6.LinkColor = System.Drawing.Color.Black;
            this.linkLabel6.Location = new System.Drawing.Point(17, 66);
            this.linkLabel6.Name = "linkLabel6";
            this.linkLabel6.Size = new System.Drawing.Size(107, 13);
            this.linkLabel6.TabIndex = 2;
            this.linkLabel6.TabStop = true;
            this.linkLabel6.Text = "Use Katakana Range";
            this.linkLabel6.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // panel4
            // 
            this.panel4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(214)))), ((int)(((byte)(223)))), ((int)(((byte)(247)))));
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.linkLabelClear);
            this.panel4.Location = new System.Drawing.Point(12, 12);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(197, 50);
            this.panel4.TabIndex = 2;
            // 
            // linkLabelClear
            // 
            this.linkLabelClear.ActiveLinkColor = System.Drawing.Color.Black;
            this.linkLabelClear.AutoSize = true;
            this.linkLabelClear.LinkColor = System.Drawing.Color.Black;
            this.linkLabelClear.Location = new System.Drawing.Point(16, 16);
            this.linkLabelClear.Name = "linkLabelClear";
            this.linkLabelClear.Size = new System.Drawing.Size(32, 13);
            this.linkLabelClear.TabIndex = 0;
            this.linkLabelClear.TabStop = true;
            this.linkLabelClear.Text = "Clear";
            this.linkLabelClear.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabelClear_LinkClicked);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.toolStripStatusLabel2});
            this.statusStrip1.Location = new System.Drawing.Point(0, 654);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(830, 22);
            this.statusStrip1.TabIndex = 6;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(811, 17);
            this.toolStripStatusLabel1.Spring = true;
            this.toolStripStatusLabel1.Text = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Left;
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(4, 17);
            // 
            // Form1
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 14);
            this.ClientSize = new System.Drawing.Size(830, 676);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.statusStrip1);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Menu = this.mainMenu1;
            this.Name = "Form1";
            this.Text = "R* Unicode Tool";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.EnableVisualStyles();
			Application.DoEvents();
			Application.Run(new Form1());
		}

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            
			AssemblyCopyrightAttribute[] aca = (AssemblyCopyrightAttribute[])(Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyCopyrightAttribute),true));
			AssemblyDescriptionAttribute[] ada = (AssemblyDescriptionAttribute[])(Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyDescriptionAttribute),true));
			string cpyrt = aca[0].Copyright;
            this.toolStripStatusLabel1.Text = cpyrt;

            this.CountUsedChars();
        }

		/// <summary>
		/// Genereates list of used unicode numbers for FontLab ENC file use.
		/// </summary>
		public void GenerateUsedUnusedCharacters()
		{
			this.richTextBox2.Clear();
			this.richTextBox3.Clear();

			bool[] bogus = new bool[65535];
			for(int i = 0; i < 65000; i++)
			{
				bogus[i] = false;
			}

			ArrayList list = new ArrayList();

			foreach(char c in this.richTextBox1.Text)
			{
				int i = Convert.ToInt32(c);

				list.Add(i);
			}

			list.Sort();

			foreach(int i in list)
			{
				bogus[i] = true;
			}

			System.Text.StringBuilder sb2 = new System.Text.StringBuilder();

			//detect edges:
			int iCurrentCount = 1;
			bool currentState = bogus[0];
			int iRangeStart = 0;
			for(int i = 0; i < bogus.Length; i++)
			{
				if((currentState == bogus[i]) && (i < bogus.Length - 1))
				{
					iCurrentCount++;
				}
				else
				{
					if(!currentState)
					{
						sb2.AppendFormat("{0:X4}-{1:X4}:\t{3}\n",iRangeStart,i - 1,currentState,iCurrentCount);
					}

					iCurrentCount = 1;
					iRangeStart = i;

					currentState = bogus[i];
				}
			}
			this.richTextBox3.Text = sb2.ToString();

			//spit out raw used or unused:
			System.Text.StringBuilder sb = new System.Text.StringBuilder(65535 * 8,65535 * 32);

			int j = 255;
			for(int i = 0; i < bogus.Length; i++)
			{
				if(!bogus[i])
				{
					//sb.Append("\tNNNNNN");
				}
				else
				{
					sb.AppendFormat("uni{0:X4} {1}",i,j + 1);
					sb.Append("\n");
					//sb.Append("\tYYYYYY");
					j++;
				}
			}

			this.richTextBox2.Text = sb.ToString();
		}

		public void GenerateUniqueCharacters()
		{
			//figure out which chars are used:
			bool[] usedChars = new bool[65535];
			foreach(char c in this.richTextBoxOriginalText.Text)
			{
				int i = Convert.ToInt32(c);
				usedChars[i] = true;
			}

			//print it out:
			System.Text.StringBuilder sb = new System.Text.StringBuilder(65535 * 8,65535 * 32);
			int i2 = 0;
			foreach(bool b in usedChars)
			{
				if(b)
				{
					sb.AppendFormat("{0}",(char)i2);
				}
				i2++;
			}

			this.richTextBox1.Text = sb.ToString();
		}

        private void CountUsedChars()
        {
            this.toolStripStatusLabel2.Text = string.Format("{0} used characters", this.richTextBox1.Text.Length);
        }

		private void buttonUniqueGen_Click(object sender, System.EventArgs e)
		{
			Cursor.Current = Cursors.WaitCursor;
			this.GenerateUniqueCharacters();
			this.GenerateUsedUnusedCharacters();

            this.CountUsedChars();
		}

		private void menuItemExit_Click(object sender, System.EventArgs e)
		{
			Application.Exit();
		}

		private void menuItemAbout_Click(object sender, System.EventArgs e)
		{
			AssemblyCopyrightAttribute[] aca = (AssemblyCopyrightAttribute[])(Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyCopyrightAttribute),true));
			AssemblyDescriptionAttribute[] ada = (AssemblyDescriptionAttribute[])(Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyDescriptionAttribute),true));
			string cpyrt = aca[0].Copyright;
			string desc = ada[0].Description;
			MessageBox.Show(this,string.Format("Version {1}\n\n{2}\n\n{3}",Application.ProductName,Application.ProductVersion,cpyrt,desc),string.Format("About {0}...",Application.ProductName));
		}

        private void linkLabelHiragana_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            this.textBoxStart.Text = "3040";
            this.textBoxEnd.Text = "309F";
        }

        private void linkLabelClear_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            this.richTextBoxOriginalText.Text = "";
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            this.textBoxStart.Text = "30A0";
            this.textBoxEnd.Text = "30FF";
        }

        private void linkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            this.textBoxStart.Text = "FF01";
            this.textBoxEnd.Text = "FF5E";
        }

        private void linkLabel3_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            this.textBoxStart.Text = "FF61";
            this.textBoxEnd.Text = "FF9F";
        }

        private void linkLabelKanji_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            this.textBoxStart.Text = "4E00";
            this.textBoxEnd.Text = "9FBF";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                char blah = Convert.ToChar(0x3042);

                System.Text.StringBuilder sb = new System.Text.StringBuilder(65535 * 8, 65535 * 32);

                int iStart = int.Parse(this.textBoxStart.Text, System.Globalization.NumberStyles.HexNumber);
                int iEnd = int.Parse(this.textBoxEnd.Text, System.Globalization.NumberStyles.HexNumber);

                for (int i = iStart; i < iEnd; i++)
                {
                    sb.AppendFormat("{0}", Convert.ToChar(i));
                }

                this.richTextBoxOriginalText.Text += sb.ToString();
            }
            catch (System.FormatException x)
            {
                MessageBox.Show(x.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void linkLabel1_LinkClicked_1(object sender, LinkLabelLinkClickedEventArgs e)
        {
            MessageBox.Show(this.richTextBoxOriginalText.Text.Length.ToString());
        }
	}
}
