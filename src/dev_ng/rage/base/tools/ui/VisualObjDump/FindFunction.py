import sys
import os
import os.path

def FindFunction(addr):
    ps3root = os.getenv("SCE_PS3_ROOT");
    objpath = os.path.join(ps3root, "host-win32/ppu/bin/ppu-lv2-objdump.exe")
    elfname = sys.argv[1]
    commandLine = objpath + " -d --demangle --start-address=%d --stop-address=%d --section=.text -w %s" % (addr+12, addr+16, elfname)
    os.system(commandLine)

while 1:
    i = raw_input("Address: 0x")
    FindFunction(int(i, 16))

