import re
import sys

stdin = file(sys.argv[1], "r")
for line in stdin:
    l = line.strip()
    m = re.search(r"((lqd)|(stqd)).*,-?([0-9]+)\(", l)
    if m:
        num = int(m.group(4))
        if num % 4 != 0:
            print num, "!!!"
        
