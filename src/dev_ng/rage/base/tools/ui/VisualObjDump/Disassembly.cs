using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Text;
using System.Windows.Forms;

namespace VisualObjDump
{
    public partial class Disassembly : Form
    {
        public Disassembly()
        {
            InitializeComponent();
        }

        public string Code 
        {
            get {
                return richTextBox1.Text;
            }
            set {
                richTextBox1.Text = value;
            }
        }

        public void ConvertGlueBranches()
        {
            string origText = richTextBox1.Text;

            /*
            for each line
            Regex.Match(".*\s+bl\s+([0-9a-f]+)\s+<(.*)+0x([0-9a-f]+)>")
            addr = String.Parse(match[0], hex);
            offset = String.Parse(match[1], hex);
            if (offset > 16) {
              string commandLine = String.Format("objdump.exe -d --demangle --start-address={0} --end-address={1} {2}", addr + 12, addr+16, filename);
              output = Exec(commandLine)
              get the last (or 2nd to last) output line
              make sure it's a 'b' instruction (uncon. branch)
              replace <.*> in orig line with "# glue for {0}"
            }
            
            Other way (less text processing afterwards) is dump and record all the unconditional branches in a table
            Would take a while since we'd be disassembling the whole elf though
            
            Once we know the start addr. for glue code we could dump everything from the start addr to what we're
            branching to this time. If we have to branch later we dump everything from prev. high water mark to here.
            Need to handle multiple glue sections if we do this though.
            */
        }
    }
}