using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.Reflection;
using Dia2Lib;

namespace VisualObjDump
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            AppDomain.CurrentDomain.AssemblyResolve += new ResolveEventHandler(CurrentDomain_AssemblyResolve);
            InitializeComponent();

            m_Ps3Root = System.Environment.GetEnvironmentVariable("SCE_PS3_ROOT");
            m_XenonRoot = System.Environment.GetEnvironmentVariable("XEDK");

            m_PpuObjDump =  m_Ps3Root + "\\host-win32\\ppu\\bin\\ppu-lv2-objdump.exe";
            m_SpuObjDump =  m_Ps3Root + "\\host-win32\\spu\\bin\\spu-lv2-objdump.exe";
            m_Unfself =     m_Ps3Root + "\\host-win32\\bin\\unfself.exe";
            m_DumpBin =     m_XenonRoot + "\\bin\\win32\\dumpbin.exe";

            openRecentFile1.Text = VisualObjDump.Properties.Settings.Default.RecentFile1;
            openRecentFile2.Text = VisualObjDump.Properties.Settings.Default.RecentFile2;
            openRecentFile3.Text = VisualObjDump.Properties.Settings.Default.RecentFile3;
            openRecentFile4.Text = VisualObjDump.Properties.Settings.Default.RecentFile4;
        }

        Assembly CurrentDomain_AssemblyResolve(object sender, ResolveEventArgs args)
        {
            return Assembly.Load(m_XenonRoot + "\\bin\\win32\\" + args.Name);
        }

        protected string m_ElfName;
        protected bool m_IsSpuElf;
        protected string m_PdbName;
        protected string m_ExeName;
        
        private Dictionary<UInt32, string> m_SectionNumberToNameMap = new Dictionary<UInt32,string>();
        private UInt32 m_XenonCodeOffset = 0x82000000;
        
        private string m_XenonRoot;
        private string m_Ps3Root;

        // These are the tools we'll be running. 
        private string m_PpuObjDump;
        private string m_SpuObjDump;
        private string m_Unfself;
        private string m_DumpBin;

        class SectionSpec
        {
            public uint m_StartAddr;
            public uint m_StopAddr;
            public string m_Name;
        };

        class SectionGroup
        {
            public List<SectionSpec> m_Sections = new List<SectionSpec>();

            public string FindSection(uint addr)
            {
                foreach(SectionSpec s in m_Sections)
                {
                    if (s.m_StartAddr <= addr && s.m_StopAddr > addr)
                    {
                        return s.m_Name;
                    }
                }
                return null;
            }
        };

        class SectionMap
        {
            public Dictionary<uint, SectionGroup> m_SectionGroups = new Dictionary<uint, SectionGroup>();

            public string FindSection(uint group, uint addr)
            {
                return m_SectionGroups[group].FindSection(addr);
            }
        };

        

        private void RunCommand(string commandName, string args)
        {
            Process p = new Process();
            p.StartInfo.CreateNoWindow = true;
            p.StartInfo.RedirectStandardOutput = false;
            p.StartInfo.UseShellExecute = false;

            p.StartInfo.FileName = commandName;
            p.StartInfo.Arguments = args;

            p.Start();
            p.WaitForExit();
        }

        private StreamReader RunCommandWithOutput(string commandName, string args)
        {
            Process p = new Process();
            p.StartInfo.CreateNoWindow = true;
            p.StartInfo.RedirectStandardOutput = true;
            p.StartInfo.UseShellExecute = false;

            p.StartInfo.FileName = commandName;
            p.StartInfo.Arguments = args;

            p.Start();
            return p.StandardOutput;
        }

        private void ParseXenonMap(string filename)
        {
            objectDump1.SymbolInfo.BeginLoadData();

            StreamReader s;

            Cursor.Current = Cursors.WaitCursor;

            s = new StreamReader(filename);

            filterInfo.Text = String.Format("Loading {0}...", filename);
            filterInfo.Invalidate();
            progressBar.Value = 0;
            progressBar.Minimum = 0;
            progressBar.Maximum = 100;

            bool absProgressBar = true;
            long origLength = 0;
            try
            {
                origLength = s.BaseStream.Length;
                progressBar.Style = ProgressBarStyle.Continuous;
            }
            catch (NotSupportedException)
            {
                progressBar.Style = ProgressBarStyle.Marquee;
                absProgressBar = false;
            }

            // Skip ahead to " Start"
            while (!s.EndOfStream)
            {
                string line = s.ReadLine();
                if (line.StartsWith(" Start"))
                {
                    break;
                }
            }

            // read the address -> section map
            SectionMap sm = new SectionMap();

            // Not sure why this isn't in the table
            SectionSpec defSpec = new SectionSpec();
            defSpec.m_StartAddr = 0;
            defSpec.m_StopAddr = 0xFFFFFFFF;
            defSpec.m_Name = "<unknown>";
            sm.m_SectionGroups[0] = new SectionGroup();
            sm.m_SectionGroups[0].m_Sections.Add(defSpec);

            while(!s.EndOfStream)
            {
                string line = s.ReadLine();
                line = line.Trim();
                if (line.StartsWith("Address"))
                {
                    break;
                }
                Match m = Regex.Match(line, @"^\s*([0-9a-f]+):([0-9a-f]+)\s+([0-9a-f]+)H\s+(\S+)");
                if (m.Success)
                {
                    uint section = UInt32.Parse(m.Groups[1].Value, System.Globalization.NumberStyles.HexNumber);
                    uint startAddr = UInt32.Parse(m.Groups[2].Value, System.Globalization.NumberStyles.HexNumber);
                    uint size = UInt32.Parse(m.Groups[3].Value, System.Globalization.NumberStyles.HexNumber);
                    string name = m.Groups[4].Value;

                    SectionGroup sg = null;
                    if (!sm.m_SectionGroups.TryGetValue(section, out sg))
                    {
                        sg = new SectionGroup();
                        sm.m_SectionGroups[section] = sg;
                    }

                    SectionSpec spec = new SectionSpec();
                    spec.m_Name = name;
                    spec.m_StartAddr = startAddr;
                    spec.m_StopAddr = startAddr + size;
                    sg.m_Sections.Add(spec);
                }
            }

            // Keep track of old row info, to compute sizes
            DataRow prevRow = null;
            uint prevAddr = 0;
            uint prevSection = 0;

            // Now read all the symbols
            while(!s.EndOfStream)
            {
                string line = s.ReadLine();
                line = line.Trim();
                if (line.Length == 0)
                {
                    continue;
                }


                if (absProgressBar)
                {
                    long curPos = s.BaseStream.Position;
                    float progress = (float)curPos / (float)origLength;
                    int percent = (int)(progress * 100.0f);
                    progressBar.Value = percent;
                }
                else
                {
                }
    
                Match m = Regex.Match(line, @"^\s*([0-9a-f]+):([0-9a-f]+)\s+(\S+)");
                if (m.Success)
                {
                    uint section = UInt32.Parse(m.Groups[1].Value, System.Globalization.NumberStyles.HexNumber);
                    uint startAddr = UInt32.Parse(m.Groups[2].Value, System.Globalization.NumberStyles.HexNumber);
                    string name = m.Groups[3].Value;

                    if (prevRow != null)
                    {
                        if (prevSection == section)
                        {
                            prevRow[2] = startAddr - prevAddr;
                        }
                    }

                    DataRow row = objectDump1.SymbolInfo.NewRow();

                    row[0] = name;
                    row[1] = sm.FindSection(section, startAddr);
                    row[2] = 0xFFFFFFFF; // don't know this yet.
                    row[3] = startAddr;

                    objectDump1.SymbolInfo.Rows.Add(row);

                    prevRow = row;
                    prevSection = section;
                    prevAddr = startAddr;
                }

            }

            filterInfo.Text = "";
            progressBar.Value = 0;
            progressBar.Style = ProgressBarStyle.Continuous;

            Cursor.Current = Cursors.Default;

            objectDump1.SymbolInfo.EndLoadData();
        }


        private void GetCoffHeaderInfo()
        {
            // If there is an .exe in this directory, parse the DUMPBIN output to get 
            // section names. Otherwise make our best guess.
            if (m_ExeName != null)
            {

                StreamReader s = RunCommandWithOutput(m_DumpBin, "/HEADERS " + m_ExeName);

                UInt32 currSection = 0;

                while(!s.EndOfStream)
                {
                    // scan forward until we get to the first "SECTION HEADER". See if we 
                    // can match "image base" along the way

                    string line = s.ReadLine();
                    if (line.StartsWith("SECTION HEADER"))
                    {
                        // Look for SECTION HEADER #8
                        // (number is in hex)
                        Match m = Regex.Match(line, @"^SECTION HEADER #([0-9a-fA-F]+)");
                        if (m.Success)
                        {
                            currSection = UInt32.Parse(m.Groups[1].Value, System.Globalization.NumberStyles.HexNumber);
                        }
                        break;
                    }
                    else if (line.Contains("image base")) // Look for 82000000 image base (82000000 to 8207DBFF)
                    {
                        string val = line.Split(new char[]{' ','\t'}, 3, StringSplitOptions.RemoveEmptyEntries)[0];
                        m_XenonCodeOffset = UInt32.Parse(val, System.Globalization.NumberStyles.HexNumber);
                    }
                }

                // OK, saw the first SECTION HEADER, now look for names.
                while(!s.EndOfStream)
                {
                    string line = s.ReadLine();
                    string[] toks = line.Split(new char[]{' ','\t'}, 3, StringSplitOptions.RemoveEmptyEntries);

                    // Look for:
                    //  .text name
                    //  .rdata name
                    if (toks.Length == 2 && toks[1] == "name")
                    {
                        m_SectionNumberToNameMap[currSection] = toks[0];
                    }

                    // Look for next section header
                    Match m = Regex.Match(line, @"^SECTION HEADER #([0-9a-fA-F]+)");
                    if (m.Success)
                    {
                        currSection = UInt32.Parse(m.Groups[1].Value, System.Globalization.NumberStyles.HexNumber);
                    }
                }

            }
            else
            {
                m_SectionNumberToNameMap[1] = ".rdata?";
                m_SectionNumberToNameMap[2] = ".pdata?";
                m_SectionNumberToNameMap[3] = ".text?";
                m_SectionNumberToNameMap[4] = ".data?";
                m_SectionNumberToNameMap[5] = ".tls?";
                m_SectionNumberToNameMap[6] = ".idata?";
                m_SectionNumberToNameMap[7] = ".XBLD?";
                m_SectionNumberToNameMap[8] = ".reloc?";
            }
        }

        private void ParsePdb(string filename)
        {
            try
            {
                m_PdbName = filename;


                m_ExeName = m_PdbName.Replace(".pdb", ".exe");
                if (!File.Exists(m_ExeName))
                {
                    m_ExeName = null;
                }
                GetCoffHeaderInfo();


                objectDump1.SymbolInfo.BeginLoadData();
                Cursor.Current = Cursors.WaitCursor;


                filterInfo.Text = String.Format("Loading {0}...", filename);
                filterInfo.Invalidate();
                progressBar.Value = 0;
                progressBar.Minimum = 0;
                progressBar.Maximum = 100;
                progressBar.Style = ProgressBarStyle.Marquee;
                progressBar.MarqueeAnimationSpeed = 30;

                Dia2Lib.DiaSourceClass diaSource = new DiaSourceClass();
                diaSource.loadDataFromPdb(filename);

                Dia2Lib.IDiaSession diaSession;
                diaSource.openSession(out diaSession);

                Dia2Lib.IDiaEnumTables diaTables;
                diaSession.getEnumTables(out diaTables);

                int tableCount = diaTables.count;
                for (int i = 0; i < tableCount; i++)
                {
                    Dia2Lib.IDiaTable table = diaTables.Item(i);
                    // find the section -> name mapping here?
                }

                Dia2Lib.IDiaEnumSymbolsByAddr diaSymbolIter;
                diaSession.getSymbolsByAddr(out diaSymbolIter);

                // gets the first sym in section 1
                Dia2Lib.IDiaSymbol firstSym = diaSymbolIter.symbolByAddr(1, 0);

                // gets the symbol with the lowest RVA
                firstSym = diaSymbolIter.symbolByRVA(firstSym.relativeVirtualAddress);

                Dia2Lib.IDiaSymbol symbol = firstSym;

                uint count = 1;
                do
                {
                    DataRow row = objectDump1.SymbolInfo.NewRow();
                    row[3] = symbol.virtualAddress;
                    row[2] = symbol.length;

                    string section = "";
                    if (!m_SectionNumberToNameMap.TryGetValue(symbol.addressSection, out section))
                    {
                        section = String.Format("{0}", symbol.addressSection);
                    }
                    row[1] = section;
                    row[0] = symbol.name;
                    objectDump1.SymbolInfo.Rows.Add(row);

                    diaSymbolIter.Next(1, out symbol, out count);
                }
                while (symbol != null && count == 1);

                filterInfo.Text = "";
                progressBar.Value = 0;
                progressBar.Style = ProgressBarStyle.Continuous;

                objectDump1.SymbolInfo.EndLoadData();
                Cursor.Current = Cursors.Default;
            }
            catch (System.Runtime.InteropServices.COMException ex)
            {
                if ((UInt32)ex.ErrorCode == 0x80040154)
                {
                    MessageBox.Show("DIA2Lib isn't registered with the system.\nRun the following command from the command line:\nC:\\WINDOWS\\system32\\regsvr32.exe \"C:\\Program Files\\Microsoft Visual Studio 8\\DIA SDK\\bin\\msdia80.dll\"", "msdia80.dll not registered", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void ParseFile(string filename)
        {
            objectDump1.SymbolInfo.BeginLoadData();

            StreamReader s;

            Cursor.Current = Cursors.WaitCursor;

            if (filename.EndsWith(".self"))
            {
                string tempDir = System.Environment.GetEnvironmentVariable("TEMP");
                string newFileName = tempDir + "\\vod.elf";

                RunCommand(m_Unfself, filename + " " + newFileName);

                filename = newFileName;
            }

            if (filename.EndsWith(".elf") || 
                filename.EndsWith(".obj") ||
                filename.EndsWith(".lib") ||
                filename.EndsWith(".self"))
            {
                m_ElfName = filename;

                string args = " -t --demangle ";
                args += filename;

                if (filename.EndsWith(".job.elf") || filename.EndsWith(".task.elf") || filename.EndsWith(".frag.elf"))
                {
                    m_IsSpuElf = true;
                }
                else
                {
                    m_IsSpuElf = false;
                }

                s = RunCommandWithOutput(m_IsSpuElf ? m_SpuObjDump : m_PpuObjDump, args);
            }
            else
            {
                s = new StreamReader(filename);
                m_ElfName = null;
            }

            filterInfo.Text = String.Format("Loading {0}...", filename);
            filterInfo.Invalidate();
            progressBar.Value = 0;
            progressBar.Minimum = 0;
            progressBar.Maximum = 100;

            bool absProgressBar = true;
            long origLength = 0;
            try
            {
                origLength = s.BaseStream.Length;
                progressBar.Style = ProgressBarStyle.Continuous;
            }
            catch (NotSupportedException)
            {
                progressBar.Style = ProgressBarStyle.Marquee;
                absProgressBar = false;
            }
            

            while(!s.EndOfStream)
            {
                string line = s.ReadLine();
                if (line == "SYMBOL TABLE:")
                {
                    break;
                }
            }

            while(!s.EndOfStream)
            {
                string line = s.ReadLine();
                line = line.Trim();
                if (line.Length == 0)
                {
                    continue;
                }

                if (absProgressBar)
                {
                    long curPos = s.BaseStream.Position;
                    float progress = (float)curPos / (float)origLength;
                    int percent = (int)(progress * 100.0f);
                    progressBar.Value = percent;
                }
                else
                {
                }

                int addrSize = m_IsSpuElf ? 8 : 16;

                string addrString = line.Substring(0, addrSize);
                string flags = line.Substring(addrSize, 9); // don't know what to do with these yet
                string[] sectionSizeName = line.Substring(addrSize + 9).Split(new char[]{' ','\t'}, 3, StringSplitOptions.RemoveEmptyEntries);

                DataRow row = objectDump1.SymbolInfo.NewRow();
                row[3] = System.UInt32.Parse(addrString, System.Globalization.NumberStyles.HexNumber);
                row[1] = sectionSizeName[0];
                row[2] = System.UInt32.Parse(sectionSizeName[1], System.Globalization.NumberStyles.HexNumber);
                row[0] = sectionSizeName[2];
                objectDump1.SymbolInfo.Rows.Add(row);
            }

            filterInfo.Text = "";
            progressBar.Value = 0;
            progressBar.Style = ProgressBarStyle.Continuous;

            objectDump1.SymbolInfo.EndLoadData();

            Cursor.Current = Cursors.Default;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                if (filterString.Text.Contains(" "))
                {
                    symbolInfoBindingSource.Filter = filterString.Text;
                }
                else
                {
                    symbolInfoBindingSource.Filter = "Name like '*" + filterString.Text + "*'";
                }
                Cursor.Current = Cursors.Default;
                filterString.ForeColor = Color.DarkGreen;
                filterInfo.Text = String.Format("Found {0} items", symbolInfoBindingSource.Count);
            }
            catch (Exception ex)
            {
                filterString.ForeColor = Color.Red;
                filterInfo.Text = ex.Message;

                MessageBox.Show("Incorrect filter. Please use a kinda SQL-style query to search.\n\nFor a simple substring search, try \"Name Like '*substring*'\".", "Bad query");
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            filterString.ForeColor = Color.Black;
            filterInfo.Text = "";
        }

        private void statusStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            uint totalSize = 0;
            foreach (DataGridViewRow dr in dataGridView1.SelectedRows)
            {
                DataGridViewCell cell = dr.Cells[2];
                totalSize += (uint)cell.Value;
            }

            selectionInfo.Text = String.Format("Selection size: {0}", totalSize);
        }

        private void LoadElf(string filename)
        {
            // reset the whole dataset
            objectDump1.Clear();

            ParseFile(filename);
        }

        private void LoadXenonMap(string filename)
        {
            objectDump1.Clear();

            ParsePdb(filename);
        }

        private void openRecentFile_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem item = sender as ToolStripMenuItem;
            string filename = item.Text;
            if (filename.EndsWith(".pdb"))
            {
                LoadXenonMap(filename);
            }
            else
            {
                LoadElf(filename);
            }
        }

        private void toolStripSplitButton1_ButtonClick(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.DefaultExt = ".elf";
            ofd.Filter = "All Readable (*.self;*.elf;*.pdb;*.lib;*.obj;*.txt)|*.self;*.elf;*.pdb;*.lib;*.obj;*.txt|PS3 (*.self;*.elf;*.lib;*.obj;*.txt)|*.self;*.elf;*.lib;*.obj;*.txt|Xenon (*.pdb)|*.pdb|All Files (*.*)|*.*";
            string filename = null;
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                filename = ofd.FileName;
                if (filename.EndsWith(".pdb"))
                {
                    LoadXenonMap(filename);
                }
                else
                {
                    LoadElf(filename);
                }
                if (filename != openRecentFile1.Text &&
                    filename != openRecentFile2.Text &&
                    filename != openRecentFile3.Text &&
                    filename != openRecentFile4.Text)
                {
                    // Everyone moves down one.
                    openRecentFile4.Text = openRecentFile3.Text;
                    openRecentFile3.Text = openRecentFile2.Text;
                    openRecentFile2.Text = openRecentFile1.Text;
                    openRecentFile1.Text = filename;

                    VisualObjDump.Properties.Settings.Default.RecentFile1 = openRecentFile1.Text;
                    VisualObjDump.Properties.Settings.Default.RecentFile2 = openRecentFile2.Text;
                    VisualObjDump.Properties.Settings.Default.RecentFile3 = openRecentFile3.Text;
                    VisualObjDump.Properties.Settings.Default.RecentFile4 = openRecentFile4.Text;

                    VisualObjDump.Properties.Settings.Default.Save();
                }
            }
            Text = "VOD - " + System.IO.Path.GetFileName(filename);
        }

        private void dataGridView1_DoubleClick(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count != 1)
            {
                return;
            }
            string section = dataGridView1.SelectedRows[0].Cells[0].Value as string;
            uint startAddr = (uint)dataGridView1.SelectedRows[0].Cells[1].Value;
            uint size = (uint)dataGridView1.SelectedRows[0].Cells[2].Value;
            string name = dataGridView1.SelectedRows[0].Cells[3].Value as string;
            uint endAddr = startAddr + size;

            if (m_ElfName != null)
            {
                if (section.StartsWith(".text") || section.StartsWith(".before_text"))
                {
                    string args = String.Format(" --disassemble --demangle --start-address={0} --stop-address={1} --section={2} -w ", startAddr, endAddr, section);
                    args += m_ElfName;

                    string output = RunCommandWithOutput(m_IsSpuElf ? m_SpuObjDump : m_PpuObjDump, args).ReadToEnd();

                    Disassembly d = new Disassembly();
                    d.Text = String.Format("Disassembly: {0}", name);
                    d.Code = output;
                    d.Show();
                }
            }
            else if (m_ExeName != null)
            {
                startAddr += m_XenonCodeOffset;
                endAddr += m_XenonCodeOffset - 4; // 4 because the xenon addr range is inclusive
                string args = String.Format("/DISASM /RANGE:{1},{2} ", section, startAddr, endAddr);
                args += m_ExeName;

                string output = RunCommandWithOutput(m_DumpBin, args).ReadToEnd();

                Disassembly d = new Disassembly();
                d.Text = String.Format("Disassembly: {0}", name);
                d.Code = output;
                d.Show();
            }
        }

        private void filterString_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                button2_Click(sender, e);
            }
        }
    }
}