//-----------------------------------------------------------------------------
// DebugManager.cs
//
// This file is the main source file for the C# version of xbWatson.
//
// Xbox Advanced Technology Group.
// Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------

using System;
using XDevkit;

namespace Xbox.ATG
{
    //-------------------------------------------------------------------------
    // Name: DebugManager
    // Desc: The DebugManager class is the main application class for
    //       C# xbWatson. It connects to the default Xbox, waits
    //       for notifications, and then handles those notifications.
    //-------------------------------------------------------------------------
    public class DebugManager
    {
        // Class for the xbWatson UI
        private XboxWatson xboxWatson;

        // xManager gives you access to one or more devkits, including letting
        // you add to the list of available consoles.
        private XDevkit.XboxManagerClass xManager;
        private XDevkit.XboxConsole xboxConsole;
        private XDevkit.IXboxDebugTarget xDebugTarget;

        // Are we connected to an Xbox devkit?
        bool fConnected;

		// Prevent masses of spam when rebooting to get a coredump.
		private bool loggingDisableOnReboot = false;

		// Previously detected running process.  Used for checking when
		// coredump write is complete.
		private string runningProcess;


        //---------------------------------------------------------------------
        // Name: Constructor
        //---------------------------------------------------------------------
        public DebugManager( XboxWatson xboxWatson )
        {
            this.xboxWatson = xboxWatson;
            fConnected = false;
            xManager = new XboxManagerClass();
            // Open the default Xbox. This will fail if there is no default
            // Xbox or if it is unreachable for any reason.
            xboxConsole = xManager.OpenConsole( xManager.DefaultConsole );
        }


        //---------------------------------------------------------------------
        // Name: InitDM
        // Desc: Request notifications from the Xbox devkit
        //---------------------------------------------------------------------
        public void InitDM()
        {
            // Get the Xbox we are talking to
            try 
            {
                // Hook up Notification Channel - tell the Xbox devkit to send
                // notifications to us. The set of notifications is documented under
                // DmNotify in the help file.
                xboxConsole.OnStdNotify  += new XboxEvents_OnStdNotifyEventHandler ( xboxConsole_OnStdNotify  );
				xboxConsole.OnTextNotify += new XboxEvents_OnTextNotifyEventHandler( xboxConsole_OnTextNotify );
                xDebugTarget = xboxConsole.DebugTarget;
            }
            catch(Exception e)
            {
                // Display an error message if connecting fails. Translating
                // error codes to text is not currently supported.
				xboxConsole.OnStdNotify  -= new XboxEvents_OnStdNotifyEventHandler ( xboxConsole_OnStdNotify  );
				xboxConsole.OnTextNotify -= new XboxEvents_OnTextNotifyEventHandler( xboxConsole_OnTextNotify );
				xboxWatson.ForceLog("Could not connect to Xbox\n " + e);
                
            }
        }


		// Recursively move all dump files off the Xbox 360 console to %RAGE_CRASHDUMP_DIR%.
		// Directory structure is flattened in this process.
		private bool RecurseMoveDumps(string xboxBaseDir, string hostDir, bool removeDirectory = false)
		{
			bool ret = false;
			foreach (IXboxFile xboxPath in xboxConsole.DirectoryFiles(xboxBaseDir))
			{
				string xboxPathStr = xboxPath.Name;
				if (xboxPath.IsDirectory)
				{
					ret |= RecurseMoveDumps(xboxPathStr, hostDir, true);
					if (removeDirectory)
						xboxConsole.RemoveDirectory(xboxPathStr);
				}
				else
				{
					string sloshFilename = xboxPathStr.Substring(xboxPathStr.LastIndexOf("\\"));
					System.IO.FileInfo hostFile = new System.IO.FileInfo(hostDir + sloshFilename);
					xboxWatson.ForceLog("\nMoving dump from \"x" + xboxPathStr + "\" to \"" + hostFile.FullName + "\"...\n");
					if (hostFile.Exists)
						hostFile.Delete();
					xboxConsole.ReceiveFile(hostFile.FullName, xboxPathStr);
					xboxConsole.DeleteFile(xboxPathStr);
					ret = true;
				}
			}
			return ret;
		}


		// Obtain running process information from the Xbox 360 console.
		private void UpdateRunningProcess()
		{
			string name = string.Empty;

			for (; ; )
			{
				try
				{
					name = xboxConsole.RunningProcessInfo.ProgramName;
					if (!string.IsNullOrEmpty(name))
						break;
				}
				catch (NullReferenceException) { break; }
			}

			// Have we just finished running the built in executable that processes the dump files after a console reboot?
			if (runningProcess == "\\Device\\Flash\\ProcessDump.Xex" && name != runningProcess)
			{
				String crashdumpenv = System.Environment.GetEnvironmentVariable(XboxWatson.DUMP_ENV_VAR);
				if (crashdumpenv != null)
				{
					// Move the dump files off the console to the host PC.
					if (RecurseMoveDumps("E:\\dumps", crashdumpenv))
						xboxWatson.ForceLog("\nMoving dumps complete\n");
				}
			}

			runningProcess = name;
		}


		//---------------------------------------------------------------------
        // Name: xboxConsole_OnStdNotify
        // Desc: Callback for DmNotify style notifications
        //---------------------------------------------------------------------
        private void xboxConsole_OnStdNotify( XboxDebugEventType eventCode, IXboxEventInfo eventInformation )
        {
            bool fStopped = eventInformation.Info.IsThreadStopped == 0 ? false : true;
            bool NotStopped, Exception = true;
            switch( eventCode )
            {
                    // Handler for DM_EXEC changes, mainly for detecting reboots
                case XDevkit.XboxDebugEventType.ExecStateChange:
                    if ( eventInformation.Info.ExecState != XDevkit.XboxExecutionState.Rebooting )
                        xDebugTarget.StopOn( XboxStopOnFlags.OnStackTrace, true );
                    if( !fConnected )
                    {
                        fConnected = true;
                        xboxWatson.ForceLog ("\nxbWatson: Connection to Xbox "
                                             + xDebugTarget.Name
                                             + " successful\n" );
                    }
					switch (eventInformation.Info.ExecState)
					{
						case XDevkit.XboxExecutionState.Rebooting:
							xboxWatson.ForceLog("\nxbWatson; Xbox is rebooting\n");
							if (loggingDisableOnReboot)
							{
								loggingDisableOnReboot = false;
								xboxWatson.LoggingEnabled = false;
							}
							break;
						case XDevkit.XboxExecutionState.Running:
							UpdateRunningProcess();
							break;
					}
                    break;

                    // Handler for DM_DEBUGSTR notifications, to display debug
                    // print text.
                case XDevkit.XboxDebugEventType.DebugString:
                    xboxWatson.Log(eventInformation.Info.Message);
                    if( fStopped )
                    {
                        eventInformation.Info.Thread.Continue( Exception );
                        xDebugTarget.Go( out NotStopped );
                    }
                    break;

                    // Handle all those notification types that don't take special handling.
				case XDevkit.XboxDebugEventType.AssertionFailed:
				case XDevkit.XboxDebugEventType.RIP:
				case XDevkit.XboxDebugEventType.ExecutionBreak:
				case XDevkit.XboxDebugEventType.DataBreak:
				case XDevkit.XboxDebugEventType.Exception:
				default:
                    xboxWatson.Log(eventInformation.Info.Message);
                    break;
                        
            }
        }


		//---------------------------------------------------------------------
		// Name: xboxConsole_OnTextNotify
		// Desc: Callback for DmSendNotificationString style notifications
		//---------------------------------------------------------------------
		void xboxConsole_OnTextNotify(string source, string notification)
		{
			if (source == "R*")
			{
				if (notification == "LOG_START")
				{
					xboxWatson.LoggingEnabled = true;
				}
				else if (notification == "LOG_STOP_ON_REBOOT")
				{
					loggingDisableOnReboot = true;
				}
			}
		}

    }
}
