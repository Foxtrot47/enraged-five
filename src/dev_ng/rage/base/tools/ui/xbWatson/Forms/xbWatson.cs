using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.IO;
using XDevkit;

namespace Xbox.ATG
{
    /// <summary>
    /// Summary description for Form1.
    /// </summary>
    public class XboxWatson : System.Windows.Forms.Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        /// 
        private const uint MAXLINES = 100000;
        private const uint LINESTOCUT = 2000;
        private System.ComponentModel.Container components = null;
        private System.Windows.Forms.RichTextBox log;

        public  const string DUMP_ENV_VAR   = "RAGE_CRASHDUMP_DIR";
        private const string VERSION        = "0.92";

        private string m_DumpDir;
        private string m_LogFn;
        private StreamWriter m_LogSw;
        
        //Declare menu items
        //Main menu
        MainMenu xbWatsonMenu;
        private System.Windows.Forms.SaveFileDialog SaveLogFile;
        private bool fLimitBufferLength;
        private bool fTimestamp;
        private System.Threading.Mutex logLock = new System.Threading.Mutex();
        private ArrayList logStringList = new ArrayList();
		private bool loggingEnabled = true;

        delegate void DeferDelegate();

        private static XboxWatson s_xboxWatson;
 
        //Event Handler
        public event EventHandler OnClickMenuItem;

        //DebugManager
        DebugManager xboxDebugManager;
        public XboxWatson()
        {
            s_xboxWatson = this;
            //
            // Required for Windows Form Designer support
            //
            this.fLimitBufferLength = false;
            this.fTimestamp = false;
            InitializeComponent();
            this.OnClickMenuItem +=new EventHandler(xbWatson_onClick);
            CreatexbWatsonMenu();
			this.Visible = true;

            this.ConnectToXbox();

            //
            // TODO: Add any constructor code after InitializeComponent call
            //

            //Test purpose;
            /*XDevkit.XboxManagerClass xMan = new XboxManagerClass();
            XDevkit.XboxConsole xCon = xMan.OpenConsole(xMan.DefaultConsole);
            XDevkit.IXboxThread Thread = xCon.DebugTarget.Threads[1];
            AssertManager assertManager = new AssertManager(xCon);
            string message = "Assert Failed\r\nFile No:";
            assertManager.HandleAssert(message,Thread);*/
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose( bool disposing )
        {
            if( disposing )
            {
                if (components != null) 
                {
                    components.Dispose();
                }
            }
            base.Dispose( disposing );
        }

        private void CreatexbWatsonMenu()
        {
            //First level menu items
            MenuItem MIFile;
            MenuItem MIEdit;
            MenuItem MIHelp;

            //Second Level menu items
            MenuItem MIFileSave = new MenuItem("Save", OnClickMenuItem);
            MenuItem MIFileConnect = new MenuItem("Connect to Xbox", OnClickMenuItem);
            MenuItem MIFileReboot = new MenuItem("Reboot Xbox", OnClickMenuItem);
            MenuItem MIFileExitReboot = new MenuItem("Exit/Reboot Xbox", OnClickMenuItem);
            MenuItem MIFileExit = new MenuItem("Exit", OnClickMenuItem);
            MenuItem MIEditCopy = new MenuItem("Copy", OnClickMenuItem);
            MenuItem MIEditClearWindow = new MenuItem("Clear Window", OnClickMenuItem);
            MenuItem MIEditSelectAll = new MenuItem("Select All", OnClickMenuItem);
            MenuItem MIEditLimitBufferLengths = new MenuItem("Limit Buffer Lengths", OnClickMenuItem);
            MenuItem MIEditTimestamps = new MenuItem("Time Stamps", OnClickMenuItem);
            MenuItem MIHelpAbout = new MenuItem("About", OnClickMenuItem);
            
            MenuItem[] MIFileArray = new MenuItem[5];
            MIFileArray[0] = MIFileSave;
            MIFileArray[1] = MIFileConnect;
            MIFileArray[2] = MIFileReboot;
            MIFileArray[3] = MIFileExitReboot;
            MIFileArray[4] = MIFileExit;

            MenuItem[] MIEditArray = new MenuItem[5];
            MIEditArray[0] = MIEditCopy;
            MIEditArray[1] = MIEditClearWindow;
            MIEditArray[2] = MIEditSelectAll;
            MIEditArray[3] = MIEditLimitBufferLengths;
            MIEditArray[4] = MIEditTimestamps;

            MenuItem[] MIHelpArray = new MenuItem[1];
            MIHelpArray[0] = MIHelpAbout;



            MIFile = new MenuItem("File", MIFileArray);
            MIEdit = new MenuItem("Edit", MIEditArray);
            MIHelp = new MenuItem("Help", MIHelpArray);
            

            xbWatsonMenu = new MainMenu();
            xbWatsonMenu.MenuItems.Add(0, MIFile);
            xbWatsonMenu.MenuItems.Add(1, MIEdit);
            xbWatsonMenu.MenuItems.Add(2, MIHelp);

            Menu = xbWatsonMenu;
        }

		public bool LoggingEnabled
		{
			get { return loggingEnabled; }
			set { loggingEnabled = value; }
		}

		public void Log(string logText)
        {
            if(this.InvokeRequired)
            {
                // Make sure we have exclusive access to the list of strings.
                logLock.WaitOne();

                // Add some text to the list of text to be displayed.
                logStringList.Add(logText);

                // Release our lock
                logLock.ReleaseMutex();

                DeferDelegate process = new DeferDelegate(this.ProcessLogBuffer);
                this.Invoke(process);
            }
            else
            {
                ProcessLogBuffer();
                LogInternal(logText);
            }
        }

		public void ForceLog(string logText)
		{
			bool oldLoggingEnabled = loggingEnabled;
			loggingEnabled = true;
			Log(logText);
			loggingEnabled = oldLoggingEnabled;
		}

        public void SaveLog()
        {
            if(this.InvokeRequired)
            {
                DeferDelegate saver = new DeferDelegate(this.SaveLog);
                this.Invoke(saver);
            }
            else
            {
                ProcessLogBuffer();

                /*if (m_LogFn != null)
                {
                    log.SaveFile(m_LogFn, RichTextBoxStreamType.TextTextOleObjs);
                }*/
            }
        }

        private string DumpPostfix()
        {
            DateTime timenow = DateTime.Now;
            int year = timenow.Year, month = timenow.Month, day = timenow.Day;
            String datestamp = year + month.ToString("D2") + day.ToString("D2") + "_" + timenow.Hour.ToString("D2") + "_" + timenow.Minute.ToString("D2") + "_" + timenow.Second.ToString("D2");
            String username = System.Environment.UserName.ToString();
            String postfix = username + "_" + datestamp;

            return postfix.ToString();
        }

        private void ProcessLogBuffer()
        {
            // Make sure we have exclusive access to the list of strings.
            logLock.WaitOne();

            // Process all of the queued log strings and then clear the list.
            foreach (string logText in logStringList)
                LogInternal(logText);
            logStringList.Clear();

            // Release our lock
            logLock.ReleaseMutex();
        }

        private void LogInternal(string logText)
        {
			if(!loggingEnabled)
				return;
            if(logText == null)
                return;
            if(fLimitBufferLength)
            {
                if(log.Lines.Length > MAXLINES)
                {
                    string[] buf = new string[log.Lines.Length - LINESTOCUT];                                
                    for(uint i = LINESTOCUT, j = 0; i < log.Lines.Length; i++,j++)
                        buf[j] = log.Lines[i];
                    log.Lines = buf;

                    
                    /*log.Select(1, log.Lines[LINESTOCUT]);
                    log.Cut();*/

                }
            }
            if (fTimestamp)
            {
                DateTime dt = DateTime.Now;
                log.AppendText(dt.ToLongDateString() + "\t" + dt.ToLongTimeString() + "\t");
            }

            if(null != m_LogSw)
            {
                //Use CR LF newlines.
                m_LogSw.Write(logText.Replace("\n", "\r\n"));
            }

            log.AppendText(logText);
        }

        #region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(XboxWatson));
            this.log = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // log
            // 
            this.log.Location = new System.Drawing.Point(0,0);
            this.log.Name = "log";
            this.log.ReadOnly = true;
            this.log.Size = new System.Drawing.Size(100,96);
            this.log.TabIndex = 0;
            this.log.Text = "";
            // 
            // XboxWatson
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5,13);
            this.ClientSize = new System.Drawing.Size(1112,590);
            this.Controls.Add(this.log);
            this.HelpButton = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "xbWatson";
            this.Text = "xbWatson " + VERSION + " (RAGE)";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.xbWatson_FormClosed);
            this.SizeChanged += new System.EventHandler(this.xbWatson_SizeChanged);
            this.Load += new System.EventHandler(this.xbWatson_Load);
            this.ResumeLayout(false);

		}
		#endregion

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main() 
        {
            Application.Run(new XboxWatson());
        }

        private void xbWatson_Load(object sender, System.EventArgs e)
        {
        
        }

        private void xbWatson_SizeChanged(object sender, EventArgs e)
        {
			System.Drawing.Size logSize = new Size();
			logSize.Height = this.log.Parent.Size.Height - 55;
			logSize.Width = this.log.Parent.Size.Width - 10;
            this.log.Size = logSize;
        }

        private void xbWatson_onClick(object sender, EventArgs e)
        {
            MenuItem mi = (MenuItem)sender;
            switch(mi.Text)
            {
                case "Save":
                    this.SaveLogFile = new System.Windows.Forms.SaveFileDialog();
                    this.SaveLogFile.DefaultExt = "txt";
                    this.SaveLogFile.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
                    this.SaveLogFile.FilterIndex = 1;
                    this.SaveLogFile.OverwritePrompt = true;
                    if (this.SaveLogFile.ShowDialog(this) == DialogResult.OK && this.SaveLogFile.FileName.Length > 0)
                    {
                        this.log.SaveFile(this.SaveLogFile.FileName, RichTextBoxStreamType.PlainText );
                    }
                    else
                    {

                    }

                    
                    break;
                case "Connect to Xbox":
                    this.ConnectToXbox();
                    break;
                case "Reboot Xbox":
                    this.RebootXbox();
                    break;
                case "Exit/Reboot Xbox":
                    this.RebootXbox();
                    this.Close();
                    this.Dispose();
                    break;
                case "Exit":
                    this.Close();
                    this.Dispose();
                    
                    break;
                case "Copy":
                    this.log.Copy();
                    break;
                case "Clear Window":
                    this.log.Clear();
                    break;
                case "Select All":
                    this.log.SelectAll();
                    break;
                case "Limit Buffer Lengths":
                    this.fLimitBufferLength = !this.fLimitBufferLength;
                    mi.Checked = this.fLimitBufferLength? true: false;
                    break;
                case "Time Stamps":
                    this.fTimestamp = !this.fTimestamp;
                    mi.Checked = this.fTimestamp? true: false;
                    break;
                case "About":
                    new AboutDialog(this);
                    break;
            }
        }

        private void ConnectToXbox()
        {
            this.Log("\nxbWatson: " + VERSION + " (RAGE)\n\n");

            m_DumpDir = System.Environment.GetEnvironmentVariable(DUMP_ENV_VAR);

            if (m_DumpDir == null)
            {
                this.Log("\nno value for " + DUMP_ENV_VAR + "\n");
            }
            else
            {
                m_LogFn = m_DumpDir + "\\console_" + this.DumpPostfix() + ".log";
            }

            try 
            {
                xboxDebugManager = new DebugManager(this);
                xboxDebugManager.InitDM();
            }
            catch(Exception exp)
            {
                this.Log("\nCould not connect to Xbox\n " + exp);
            }

            if(null != m_LogFn)
            {
                try
                {
                    //Write current contents of text box to the log file.
                    log.SaveFile(m_LogFn, RichTextBoxStreamType.TextTextOleObjs);

                    //Open the log file
                    m_LogSw = File.AppendText(m_LogFn);
                    m_LogSw.AutoFlush = true;
                }
                catch(Exception exp)
                {
                    this.Log("\nCould not open log file " + m_LogFn + "\n " + exp);
                }
            }
        }

        private void RebootXbox()
        {
            XDevkit.XboxManagerClass xManager = new XboxManagerClass();
            XDevkit.XboxConsole xboxConsole = xManager.OpenConsole( xManager.DefaultConsole );
            xboxConsole.Reboot(null, null, null, XboxRebootFlags.Cold);
        }

        private void xbWatson_FormClosed(object sender,FormClosedEventArgs e)
        {
            try
            {
                this.SaveLog();

                if(null != m_LogSw)
                {
                    m_LogSw.Close();
                    m_LogSw = null;
                }
            }
            catch(Exception)
            {
            }
        }
    }
}
