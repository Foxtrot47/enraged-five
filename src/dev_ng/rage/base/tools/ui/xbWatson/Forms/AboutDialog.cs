using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace Xbox.ATG
{
    /// <summary>
    /// Summary description for AboutDialog.
    /// </summary>
    public class AboutDialog : System.Windows.Forms.Form
    {
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button OK;
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.Container components = null;
        private string CopyrightWarningText = "Warning: This computer program is protected by copyright law and\ninternational treaties. Unauthorized reproduction or distribution of\nthis program, or any portion of it, may result in severe civil and\ncriminal penalties, and will be prosecuted to the maximum extent\npossible under the law.";

        public AboutDialog(XboxWatson xboxWatson)
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
            this.ShowDialog(xboxWatson);

            //
            // TODO: Add any constructor code after InitializeComponent call
            //
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose( bool disposing )
        {
            if( disposing )
            {
                if(components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(AboutDialog));
			this.label1 = new System.Windows.Forms.Label();
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			this.label2 = new System.Windows.Forms.Label();
			this.OK = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(48, 8);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(200, 48);
			this.label1.TabIndex = 0;
			this.label1.Text = "Microsoft (R) xbWatson\nJuly 2004 Release\nCopyright (C) Microsoft Corp.";
			// 
			// pictureBox1
			// 
			this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
			this.pictureBox1.Location = new System.Drawing.Point(8, 8);
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new System.Drawing.Size(40, 32);
			this.pictureBox1.TabIndex = 1;
			this.pictureBox1.TabStop = false;
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(16, 80);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(344, 104);
			this.label2.TabIndex = 2;
			this.label2.Text = CopyrightWarningText;
			// 
			// OK
			// 
			this.OK.Location = new System.Drawing.Point(136, 208);
			this.OK.Name = "OK";
			this.OK.TabIndex = 3;
			this.OK.Text = "OK";
			this.OK.Click += new System.EventHandler(this.OK_Click);
			// 
			// AboutDialog
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(362, 240);
			this.Controls.Add(this.OK);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.pictureBox1);
			this.Controls.Add(this.label1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "AboutDialog";
			this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
			this.Text = "About xbWatson";
			this.TopMost = true;
			this.ResumeLayout(false);

		}
		#endregion

        private void OK_Click(object sender, System.EventArgs e)
        {
            this.Close();
        }
    }
}
