using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Threading;
//using System.Windows.Forms;

namespace MemTrackerControls
{
    #region Enums
    public enum DuplicateAllocationAction
    {
        Leave,
        Overwrite,
        OverwriteAll
    }
    #endregion

    public class Tracker
    {
        #region Delegates
        public delegate void DuplicateAllocationEventHandler( object sender, DuplicateAllocationEventArgs e );
        #endregion

        #region Variables
        private string m_platform = "Unknown";

        private Stack<string> m_stackNameStack = new Stack<string>();
        private SortedDictionary<string, byte> m_stackNamesDictionary = new SortedDictionary<string, byte>();
        private SortedDictionary<string, byte> m_datatypesDictionary = new SortedDictionary<string, byte>();

        private List<ReportItem> m_reports = new List<ReportItem>();
        private List<TrackerAction> m_trackerActions = new List<TrackerAction>();
        private uint[] m_availableMemory = new uint[(int)Allocation.MemoryTypeIndex.NumIndexes];

        private Dictionary<uint, Allocation> m_allocationDictionary = new Dictionary<uint, Allocation>();

        private Mutex m_reportsMutex = new Mutex();

        private bool m_overwriteAllAllocations = false;
        private bool m_rememberDeallocations = false;

        private bool m_initialRememberDeallocationsSet = false;
        private bool m_initialRememberDeallocations = false;
        #endregion

        #region Properties
        public event DuplicateAllocationEventHandler DuplicateAllocation;

        public string Platform
        {
            get
            {
                return m_platform;
            }
            set
            {
                m_platform = value;
            }
        }

        public bool TrackDeallocations
        {
            get
            {
                return m_rememberDeallocations;
            }
            set
            {
                m_rememberDeallocations = value;

                if ( !m_initialRememberDeallocationsSet )
                {
                    m_initialRememberDeallocations = m_rememberDeallocations;
                    m_initialRememberDeallocationsSet = true;
                }
            }
        }

        public bool OverwriteDuplicateAllocations
        {
            get
            {
                return m_overwriteAllAllocations;
            }
            set
            {
                m_overwriteAllAllocations = value;
            }
        }

        public uint[] AvailableMemory
        {
            get
            {
                return m_availableMemory;
            }
        }

        public ReportItem[] Reports
        {
            get
            {
                return m_reports.ToArray();
            }
        }

        public List<TrackerAction> Actions
        {
            get
            {
                return m_trackerActions;
            }
        }

        public string[] StackNames
        {
            get
            {
                string[] array = new string[m_stackNamesDictionary.Keys.Count];
                m_stackNamesDictionary.Keys.CopyTo( array, 0 );
                return array;
            }
        }

        public string[] DataTypes
        {
            get
            {
                string[] array = new string[m_datatypesDictionary.Keys.Count];
                m_datatypesDictionary.Keys.CopyTo( array, 0 );
                return array;
            }
        }
        #endregion

        #region Public Functions
        public void PushStackAction( uint threadId, string stackName )
        {
            // make sure we don't have backslashes because that will later become our delimiter for the full stack path
            stackName = stackName.Replace( '\\', '/' );

            m_stackNameStack.Push( stackName );

            m_stackNamesDictionary[stackName] = 1;

            m_trackerActions.Add( new PushStackAction( threadId, stackName ) );
        }

        public void PopStackAction( uint threadId )
        {
            m_stackNameStack.Pop();

            m_trackerActions.Add( new PopStackAction( threadId ) );
        }

        public void TallyAllocationAction( uint threadId, uint decimalAddress, int type, uint size )
        {
            Allocation theAllocation;
            if ( !m_allocationDictionary.TryGetValue( decimalAddress, out theAllocation ) )
            {
                theAllocation = new Allocation( type, decimalAddress, size );

                m_allocationDictionary.Add( decimalAddress, theAllocation );
            }
            else
            {
                System.Text.StringBuilder s = new System.Text.StringBuilder();
                s.Append( "Ta:" );
                s.Append( decimalAddress.ToString( "X" ) );
                s.Append( ";" );
                s.Append( theAllocation.Size );
                s.Append( ";" );
                s.Append( theAllocation.MemoryTypeName );
                s.Append( ";" );
                s.Append( theAllocation.FullStackPath );
                s.Append( ";" );
                s.Append( theAllocation.TypeName );
                s.Append( ";" );
                s.Append( theAllocation.IsArray );
                s.Append( ";" );
                s.Append( theAllocation.FileName );
                s.Append( ";" );
                s.Append( theAllocation.LineNumber );

                if ( !m_overwriteAllAllocations )
                {
                    DuplicateAllocationAction actionToTake = OnDuplicateAllocation( decimalAddress.ToString( "X" ) );
                    switch ( actionToTake )
                    {
                        case DuplicateAllocationAction.Leave:
                            s.Append( ";L" );   // L for leave
                            return;
                        case DuplicateAllocationAction.Overwrite:
                            s.Append( ";O" );   // O for Overwrite this
                            break;
                        case DuplicateAllocationAction.OverwriteAll:
                            m_overwriteAllAllocations = true;
                            s.Append( ";A" ); // A for overwrite All
                            break;
                    }
                }
                else
                {
                    s.Append( ";A" );   // A for overwrite All
                }
                
                s.Append( " Allocation was replaced." );

                // remember theAllocation so if we choose to overwrite, we'll have the new allocation info
                ErrorAction a = new ErrorAction( threadId, theAllocation, s.ToString() );
                m_trackerActions.Add( a );

                theAllocation.FileName = string.Empty;
                theAllocation.TypeName = Allocation.Unknown;
                theAllocation.LineNumber = 0;

                // remove tracker actions
                for ( int i = 0; i < m_trackerActions.Count; )
                {
                    if ( m_trackerActions[i] is TallyAllocationAction )
                    {
                        TallyAllocationAction tallyAction = m_trackerActions[i] as TallyAllocationAction;
                        if ( tallyAction.TheAllocation == theAllocation )
                        {
                            m_trackerActions.RemoveAt( i );
                        }
                        else
                        {
                            ++i;
                        }
                    }
                    else
                    {
                        ++i;
                    }
                }
            }

            string[] stacks = new string[m_stackNameStack.Count];
            m_stackNameStack.CopyTo( stacks, 0 );
            theAllocation.Stack = stacks;

            m_trackerActions.Add( new TallyAllocationAction( threadId, theAllocation ) );
        }

        public void AddInfoAction( uint threadId, uint decimalAddress, uint size, uint dataTypeHash, uint fileNameHash, int line )
        {
            Allocation theAllocation;
            if ( !m_allocationDictionary.TryGetValue( decimalAddress, out theAllocation ) || (theAllocation.FileName != string.Empty) )
            {
                // treat this as an array element, so find it's "parent" tally action
                Allocation arrayAllocation = null;
                int i = m_trackerActions.Count - 1;
                while ( i >= 0 )
                {
                    if ( m_trackerActions[i].TheAction == TrackerAction.Action.TallyAllocation )
                    {
                        Allocation a = ((TallyAllocationAction)m_trackerActions[i]).TheAllocation;
                        if ( (decimalAddress >= a.DecimalAddress) && (decimalAddress < a.DecimalAddress + a.Size) )
                        {
                            arrayAllocation = a;
                            break;
                        }
                    }

                    --i;
                }

                string typeName;

                if ( arrayAllocation == null )
                {
                    //
                    // We believe this happens when you do something like this:
                    // new myClass;
                    //
                    TallyAllocationAction( threadId, decimalAddress, -1, size );
                    m_allocationDictionary.TryGetValue( decimalAddress, out theAllocation );

                    theAllocation.TypeNameHashCode = dataTypeHash;
                    theAllocation.FileNameHashCode = fileNameHash;
                    theAllocation.LineNumber = line;

                    if ( Allocation.DataTypeDictionary.TryGetValue( dataTypeHash, out typeName ) )
                    {
                        AddDataType( typeName );
                    }

                    return;
                }

                theAllocation = new Allocation( arrayAllocation.MemoryType, decimalAddress, size );
                theAllocation.TypeNameHashCode = dataTypeHash;
                theAllocation.FileNameHashCode = fileNameHash;
                theAllocation.LineNumber = line;

                if ( Allocation.DataTypeDictionary.TryGetValue( dataTypeHash, out typeName ) )
                {
                    AddDataType( typeName );
                }

                typeName += "[]";
                arrayAllocation.TypeName = typeName;
                //arrayAllocation.FileName = fileName;
                //arrayAllocation.LineNumber = line;

                if ( arrayAllocation.Items == null )
                {
                    arrayAllocation.Items = new List<Allocation>();
                }

                arrayAllocation.Items.Add( theAllocation );

                AddDataType( typeName );
            }
            else
            {
                theAllocation.TypeNameHashCode = dataTypeHash;
                theAllocation.FileNameHashCode = fileNameHash;
                theAllocation.LineNumber = line;

                string typeName;
                if ( Allocation.DataTypeDictionary.TryGetValue( dataTypeHash, out typeName ) )
                {
                    AddDataType( typeName );
                }
            }
        }

        public void UntallyAllocationAction( uint threadId, uint decimalAddress, uint size )
        {
            Allocation theAllocation;
            if ( m_allocationDictionary.TryGetValue( decimalAddress, out theAllocation ) )
            {
                int index = -1;
                bool spansReports = true;

                if ( !m_rememberDeallocations )
                {
                    if ( m_reports.Count > 0 )
                    {
                        // try to find our corresponding tally before the last report
                        for ( int i = m_trackerActions.Count - 1; i >= m_reports[m_reports.Count - 1].Index; --i )
                        {
                            if ( (m_trackerActions[i].TheAction == TrackerAction.Action.TallyAllocation)
                                && (((TallyAllocationAction)m_trackerActions[i]).TheAllocation == theAllocation) )
                            {
                                index = i;
                                spansReports = false;
                                break;
                            }
                        }
                    }
                    else
                    {
                        // try to find our corresponding tally
                        for ( int i = 0; i < m_trackerActions.Count; ++i )
                        {
                            if ( (m_trackerActions[i].TheAction == TrackerAction.Action.TallyAllocation)
                                && (((TallyAllocationAction)m_trackerActions[i]).TheAllocation == theAllocation) )
                            {
                                index = i;
                                spansReports = false;
                                break;
                            }
                        }
                    }
                }

                m_reportsMutex.WaitOne();

                // we straddle a report, so remember the untally
                if ( spansReports )
                {
                    m_trackerActions.Add( new UntallyAllocationAction( threadId, theAllocation ) );
                }
                // otherwise, let's forget about the allocation altogether
                else
                {
                    Debug.Assert( index != -1, "Could not locate Tally action for an Untally." );

                    // fixup report indexes
                    foreach ( ReportItem report in m_reports )
                    {
                        if ( report.Index > index )
                        {
                            --report.Index;
                        }
                    }

                    m_trackerActions.RemoveAt( index );
                }

                m_reportsMutex.ReleaseMutex();

                m_allocationDictionary.Remove( decimalAddress );
            }
            else
            {
                m_trackerActions.Add( new ErrorAction( threadId, null, "Un:" + decimalAddress.ToString( "X" ) + ";" + size ) );
            }
        }

        public void AddReportAction( uint threadId, string reportName, uint[] available, bool trackingDeallocations )
        {
            m_reportsMutex.WaitOne();

            StringBuilder name = new StringBuilder();
            name.Append( m_reports.Count );
            name.Append( ". " );
            name.Append( reportName );
            name.Append( " (" );
            name.Append( System.DateTime.Now.ToString( "G" ) );
            name.Append( ")" );

            if ( trackingDeallocations )
            {
                name.Append( "*" );
            }

            ReportItem report = new ReportItem( name.ToString(), m_trackerActions.Count, available, trackingDeallocations );
            m_trackerActions.Add( new ReportAction( threadId, report ) );
            m_reports.Add( report );

            m_reportsMutex.ReleaseMutex();
        }

        public void SaveReport( string filename, ReportItem report )
        {
            int reportIndex = report != null ? report.Index : m_trackerActions.Count - 1;

            using ( StreamWriter writer = new StreamWriter( filename ) )
            {
                writer.Write( "0;Pl;" );
                writer.Write( m_platform );
                writer.Write( ";" );
                writer.Write( m_initialRememberDeallocations.ToString() );
                writer.Write( "\r\n" );

                for ( int i = 0; i <= reportIndex; ++i )
                {
                    TrackerAction action = m_trackerActions[i];

                    // IMPORTANT: what we do here needs to match the original message that was sent to us from rage/base/src/diag/tracker_file.cpp

                    System.Text.StringBuilder s = new System.Text.StringBuilder();
                    switch ( action.TheAction )
                    {
                        case TrackerAction.Action.PushStack:
                            {
                                PushStackAction stackAction = action as PushStackAction;
                                s.Append( stackAction.ThreadID );
                                s.Append( ";Pu;" );
                                s.Append( stackAction.Name );
                                s.Append( "\r\n" );
                            }
                            break;
                        case TrackerAction.Action.PopStack:
                            {
                                PopStackAction stackAction = action as PopStackAction;
                                s.Append( stackAction.ThreadID );
                                s.Append( ";Po" );
                                s.Append( "\r\n" );
                            }
                            break;
                        case TrackerAction.Action.TallyAllocation:
                            {
                                TallyAllocationAction tallyAction = action as TallyAllocationAction;
                                s.Append( tallyAction.ThreadID );
                                s.Append( ";Ta;" );
                                s.Append( tallyAction.TheAllocation.Address );
                                s.Append( ";" );
                                s.Append( tallyAction.TheAllocation.Size );
                                s.Append( ";" );
                                s.Append( (int)(tallyAction.TheAllocation.MemoryType) - 1 ); // subtract 1 because that's what tracker.exe expects
                                s.Append( "\r\n" );

                                if ( tallyAction.TheAllocation.FileName != string.Empty )
                                {
                                    s.Append( tallyAction.ThreadID );
                                    s.Append( ";In;" );
                                    s.Append( tallyAction.TheAllocation.Address );
                                    s.Append( ";" );
                                    s.Append( tallyAction.TheAllocation.Size );
                                    s.Append( ";" );
                                    s.Append( tallyAction.TheAllocation.TypeName );
                                    s.Append( ";" );
                                    s.Append( tallyAction.TheAllocation.FileName );
                                    s.Append( ";" );
                                    s.Append( tallyAction.TheAllocation.LineNumber );
                                    s.Append( "\r\n" );
                                }

                                if ( tallyAction.TheAllocation.IsArray )
                                {
                                    foreach ( Allocation a in tallyAction.TheAllocation.Items )
                                    {
                                        s.Append( tallyAction.ThreadID );
                                        s.Append( ";In;" );
                                        s.Append( a.Address );
                                        s.Append( ";" );
                                        s.Append( a.Size );
                                        s.Append( ";" );
                                        s.Append( a.TypeName );
                                        s.Append( ";" );
                                        s.Append( a.FileName );
                                        s.Append( ";" );
                                        s.Append( a.LineNumber );
                                        s.Append( "\r\n" );
                                    }
                                }
                            }
                            break;
                        case TrackerAction.Action.UntallyAllocation:
                            {
                                UntallyAllocationAction untallyAction = action as UntallyAllocationAction;
                                s.Append( untallyAction.ThreadID );
                                s.Append( ";Un;" );
                                s.Append( untallyAction.TheAllocation.Address );
                                s.Append( ";" );
                                s.Append( untallyAction.TheAllocation.Size );
                                s.Append( "\r\n" );
                            }
                            break;
                        case TrackerAction.Action.Report:
                            {
                                ReportAction reportAction = action as ReportAction;

                                s.Append( reportAction.ThreadID );
                                s.Append( ";Re;" );

                                string text = reportAction.Report.Name;
                                if ( text.EndsWith( "*" ) )
                                {
                                    text = text.Substring( 0, text.Length - 1 );
                                }
                                s.Append( text );

                                foreach ( uint a in reportAction.Report.Available )
                                {
                                    s.Append( ";" );
                                    s.Append( a );
                                }

                                s.Append( ";" );
                                s.Append( reportAction.Report.TrackedDeallocations.ToString() );

                                s.Append( "\r\n" );
                            }
                            break;
                        case TrackerAction.Action.Error:
                            {
                                ErrorAction errorAction = action as ErrorAction;
                                s.Append( errorAction.ThreadID );
                                s.Append( ";Er;" );
                                s.Append( errorAction.Description );
                                s.Append( "\r\n" );
                            }
                            break;
                    }

                    writer.Write( s.ToString() );
                }
            }
        }
        #endregion

        #region Private Functions
        private void AddDataType( string typeName )
        {
            if ( (typeName == Allocation.Unknown) || m_datatypesDictionary.ContainsKey( typeName ) )
            {
                return;
            }

            m_datatypesDictionary.Add( typeName, 1 );
        }

        private DuplicateAllocationAction OnDuplicateAllocation( string address )
        {
            if ( this.DuplicateAllocation != null )
            {
                DuplicateAllocationEventArgs e = new DuplicateAllocationEventArgs( address );
                this.DuplicateAllocation( this, e );

                return e.ActionToTake;
            }

            return DuplicateAllocationAction.Overwrite;
        }
        #endregion
    }
}
