namespace MemTrackerControls
{
    partial class OverviewListControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( m_trackerComponent != null )
            {
                m_trackerComponent.ReportAdded -= m_reportAddedEventHandler;
                m_trackerComponent.ReportRemoved -= m_reportRemovedEventHandler;
                m_trackerComponent.StackAdded -= m_stackAddedEventHandler;
                m_trackerComponent.DatatypeAdded -= m_datatypeAddedEventHandler;
            }

            if ( disposing && (components != null) )
            {
                components.Dispose();
            }

            base.Dispose( disposing );
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager( typeof( OverviewListControl ) );
            this.listView1 = new System.Windows.Forms.ListView();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip( this.components );
            this.copyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.selectAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.generateReportToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.leftReportToolStripLabel = new System.Windows.Forms.ToolStripLabel();
            this.leftReportToolStripComboBox = new System.Windows.Forms.ToolStripComboBox();
            this.rightReportToolStripLabel = new System.Windows.Forms.ToolStripLabel();
            this.rightReportToolStripComboBox = new System.Windows.Forms.ToolStripComboBox();
            this.optionsToolStripDropDownButton = new System.Windows.Forms.ToolStripDropDownButton();
            this.showByToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showByStackNameToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showByDatatypeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showByMemoryTypeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.showByAllocationsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showByDeallocationsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stacksToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stacksAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stacksNoneToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.stacksUnassignedToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.stacksSelectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.datatypesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.datatypesAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.datatypesNoneToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.datatypesNativeTypesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.datatypesClassesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.datatypesStructuresToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.datatypesUnknownTypesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.datatypesSelectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showEmptyItemsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.refreshToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.showByToolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.selectedStacksToolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.selectedDatatypesToolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.selectedSizeToolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.backgroundWorker = new System.ComponentModel.BackgroundWorker();
            this.contextMenuStrip1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // listView1
            // 
            this.listView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.listView1.ContextMenuStrip = this.contextMenuStrip1;
            this.listView1.FullRowSelect = true;
            this.listView1.GridLines = true;
            this.listView1.HideSelection = false;
            this.listView1.Location = new System.Drawing.Point( 3, 28 );
            this.listView1.Name = "listView1";
            this.listView1.ShowGroups = false;
            this.listView1.Size = new System.Drawing.Size( 854, 278 );
            this.listView1.TabIndex = 1;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            this.listView1.SelectedIndexChanged += new System.EventHandler( this.listView1_SelectedIndexChanged );
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.copyToolStripMenuItem,
            this.selectAllToolStripMenuItem} );
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.ShowImageMargin = false;
            this.contextMenuStrip1.Size = new System.Drawing.Size( 143, 48 );
            // 
            // copyToolStripMenuItem
            // 
            this.copyToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.copyToolStripMenuItem.Name = "copyToolStripMenuItem";
            this.copyToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.C)));
            this.copyToolStripMenuItem.Size = new System.Drawing.Size( 142, 22 );
            this.copyToolStripMenuItem.Text = "&Copy";
            this.copyToolStripMenuItem.Click += new System.EventHandler( this.copyToolStripMenuItem_Click );
            // 
            // selectAllToolStripMenuItem
            // 
            this.selectAllToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.selectAllToolStripMenuItem.Name = "selectAllToolStripMenuItem";
            this.selectAllToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.A)));
            this.selectAllToolStripMenuItem.Size = new System.Drawing.Size( 142, 22 );
            this.selectAllToolStripMenuItem.Text = "Select &All";
            this.selectAllToolStripMenuItem.Click += new System.EventHandler( this.selectAllToolStripMenuItem_Click );
            // 
            // toolStrip1
            // 
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.Items.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.generateReportToolStripButton,
            this.toolStripSeparator1,
            this.leftReportToolStripLabel,
            this.leftReportToolStripComboBox,
            this.rightReportToolStripLabel,
            this.rightReportToolStripComboBox,
            this.optionsToolStripDropDownButton,
            this.toolStripSeparator8,
            this.refreshToolStripButton} );
            this.toolStrip1.Location = new System.Drawing.Point( 0, 0 );
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.ShowItemToolTips = false;
            this.toolStrip1.Size = new System.Drawing.Size( 860, 25 );
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // generateReportToolStripButton
            // 
            this.generateReportToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.generateReportToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject( "generateReportToolStripButton.Image" )));
            this.generateReportToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.generateReportToolStripButton.Name = "generateReportToolStripButton";
            this.generateReportToolStripButton.Size = new System.Drawing.Size( 92, 22 );
            this.generateReportToolStripButton.Text = "Generate Report";
            this.generateReportToolStripButton.Click += new System.EventHandler( this.generateReportToolStripButton_Click );
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size( 6, 25 );
            // 
            // leftReportToolStripLabel
            // 
            this.leftReportToolStripLabel.AutoSize = false;
            this.leftReportToolStripLabel.Name = "leftReportToolStripLabel";
            this.leftReportToolStripLabel.Size = new System.Drawing.Size( 80, 22 );
            this.leftReportToolStripLabel.Text = "Left Report:";
            this.leftReportToolStripLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // leftReportToolStripComboBox
            // 
            this.leftReportToolStripComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.leftReportToolStripComboBox.Name = "leftReportToolStripComboBox";
            this.leftReportToolStripComboBox.Size = new System.Drawing.Size( 220, 25 );
            this.leftReportToolStripComboBox.SelectedIndexChanged += new System.EventHandler( this.leftReportToolStripComboBox_SelectedIndexChanged );
            // 
            // rightReportToolStripLabel
            // 
            this.rightReportToolStripLabel.AutoSize = false;
            this.rightReportToolStripLabel.Name = "rightReportToolStripLabel";
            this.rightReportToolStripLabel.Size = new System.Drawing.Size( 80, 22 );
            this.rightReportToolStripLabel.Text = "Right Report:";
            this.rightReportToolStripLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // rightReportToolStripComboBox
            // 
            this.rightReportToolStripComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.rightReportToolStripComboBox.Name = "rightReportToolStripComboBox";
            this.rightReportToolStripComboBox.Size = new System.Drawing.Size( 220, 25 );
            this.rightReportToolStripComboBox.SelectedIndexChanged += new System.EventHandler( this.rightReportToolStripComboBox_SelectedIndexChanged );
            // 
            // optionsToolStripDropDownButton
            // 
            this.optionsToolStripDropDownButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.optionsToolStripDropDownButton.DropDownItems.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.showByToolStripMenuItem,
            this.stacksToolStripMenuItem,
            this.datatypesToolStripMenuItem,
            this.showEmptyItemsToolStripMenuItem} );
            this.optionsToolStripDropDownButton.Image = ((System.Drawing.Image)(resources.GetObject( "optionsToolStripDropDownButton.Image" )));
            this.optionsToolStripDropDownButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.optionsToolStripDropDownButton.Name = "optionsToolStripDropDownButton";
            this.optionsToolStripDropDownButton.Size = new System.Drawing.Size( 57, 22 );
            this.optionsToolStripDropDownButton.Text = "Options";
            this.optionsToolStripDropDownButton.DropDownOpening += new System.EventHandler( this.optionsToolStripDropDownButton_DropDownOpening );
            // 
            // showByToolStripMenuItem
            // 
            this.showByToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.showByToolStripMenuItem.DropDownItems.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.showByStackNameToolStripMenuItem,
            this.showByDatatypeToolStripMenuItem,
            this.showByMemoryTypeToolStripMenuItem,
            this.toolStripSeparator3,
            this.showByAllocationsToolStripMenuItem,
            this.showByDeallocationsToolStripMenuItem} );
            this.showByToolStripMenuItem.Name = "showByToolStripMenuItem";
            this.showByToolStripMenuItem.Size = new System.Drawing.Size( 174, 22 );
            this.showByToolStripMenuItem.Text = "Show &By";
            this.showByToolStripMenuItem.DropDownOpening += new System.EventHandler( this.showByToolStripMenuItem_DropDownOpening );
            // 
            // showByStackNameToolStripMenuItem
            // 
            this.showByStackNameToolStripMenuItem.CheckOnClick = true;
            this.showByStackNameToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.showByStackNameToolStripMenuItem.Name = "showByStackNameToolStripMenuItem";
            this.showByStackNameToolStripMenuItem.Size = new System.Drawing.Size( 150, 22 );
            this.showByStackNameToolStripMenuItem.Text = "&Stack Name";
            this.showByStackNameToolStripMenuItem.Click += new System.EventHandler( this.showByStackNameToolStripMenuItem_Click );
            // 
            // showByDatatypeToolStripMenuItem
            // 
            this.showByDatatypeToolStripMenuItem.CheckOnClick = true;
            this.showByDatatypeToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.showByDatatypeToolStripMenuItem.Name = "showByDatatypeToolStripMenuItem";
            this.showByDatatypeToolStripMenuItem.Size = new System.Drawing.Size( 150, 22 );
            this.showByDatatypeToolStripMenuItem.Text = "&Datatype";
            this.showByDatatypeToolStripMenuItem.Click += new System.EventHandler( this.showByDatatypeToolStripMenuItem_Click );
            // 
            // showByMemoryTypeToolStripMenuItem
            // 
            this.showByMemoryTypeToolStripMenuItem.CheckOnClick = true;
            this.showByMemoryTypeToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.showByMemoryTypeToolStripMenuItem.Name = "showByMemoryTypeToolStripMenuItem";
            this.showByMemoryTypeToolStripMenuItem.Size = new System.Drawing.Size( 150, 22 );
            this.showByMemoryTypeToolStripMenuItem.Text = "&Memory Type";
            this.showByMemoryTypeToolStripMenuItem.Click += new System.EventHandler( this.showByMemoryTypeToolStripMenuItem_Click );
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size( 147, 6 );
            // 
            // showByAllocationsToolStripMenuItem
            // 
            this.showByAllocationsToolStripMenuItem.CheckOnClick = true;
            this.showByAllocationsToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.showByAllocationsToolStripMenuItem.Name = "showByAllocationsToolStripMenuItem";
            this.showByAllocationsToolStripMenuItem.Size = new System.Drawing.Size( 150, 22 );
            this.showByAllocationsToolStripMenuItem.Text = "&Allocations";
            this.showByAllocationsToolStripMenuItem.Click += new System.EventHandler( this.showByAllocationsToolStripMenuItem_Click );
            // 
            // showByDeallocationsToolStripMenuItem
            // 
            this.showByDeallocationsToolStripMenuItem.CheckOnClick = true;
            this.showByDeallocationsToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.showByDeallocationsToolStripMenuItem.Name = "showByDeallocationsToolStripMenuItem";
            this.showByDeallocationsToolStripMenuItem.Size = new System.Drawing.Size( 150, 22 );
            this.showByDeallocationsToolStripMenuItem.Text = "D&eallocations";
            this.showByDeallocationsToolStripMenuItem.Click += new System.EventHandler( this.showByDeallocationsToolStripMenuItem_Click );
            // 
            // stacksToolStripMenuItem
            // 
            this.stacksToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.stacksToolStripMenuItem.DropDownItems.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.stacksAllToolStripMenuItem,
            this.stacksNoneToolStripMenuItem,
            this.toolStripSeparator4,
            this.stacksUnassignedToolStripMenuItem,
            this.toolStripSeparator5,
            this.stacksSelectToolStripMenuItem} );
            this.stacksToolStripMenuItem.Name = "stacksToolStripMenuItem";
            this.stacksToolStripMenuItem.Size = new System.Drawing.Size( 174, 22 );
            this.stacksToolStripMenuItem.Text = "&Stacks";
            this.stacksToolStripMenuItem.DropDownOpening += new System.EventHandler( this.stacksToolStripMenuItem_DropDownOpening );
            // 
            // stacksAllToolStripMenuItem
            // 
            this.stacksAllToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.stacksAllToolStripMenuItem.Name = "stacksAllToolStripMenuItem";
            this.stacksAllToolStripMenuItem.Size = new System.Drawing.Size( 140, 22 );
            this.stacksAllToolStripMenuItem.Text = "&All";
            this.stacksAllToolStripMenuItem.Click += new System.EventHandler( this.stacksAllToolStripMenuItem_Click );
            // 
            // stacksNoneToolStripMenuItem
            // 
            this.stacksNoneToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.stacksNoneToolStripMenuItem.Name = "stacksNoneToolStripMenuItem";
            this.stacksNoneToolStripMenuItem.Size = new System.Drawing.Size( 140, 22 );
            this.stacksNoneToolStripMenuItem.Text = "&None";
            this.stacksNoneToolStripMenuItem.Click += new System.EventHandler( this.stacksNoneToolStripMenuItem_Click );
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size( 137, 6 );
            // 
            // stacksUnassignedToolStripMenuItem
            // 
            this.stacksUnassignedToolStripMenuItem.CheckOnClick = true;
            this.stacksUnassignedToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.stacksUnassignedToolStripMenuItem.Name = "stacksUnassignedToolStripMenuItem";
            this.stacksUnassignedToolStripMenuItem.Size = new System.Drawing.Size( 140, 22 );
            this.stacksUnassignedToolStripMenuItem.Text = "&Unassigned";
            this.stacksUnassignedToolStripMenuItem.Click += new System.EventHandler( this.stacksUnassignedToolStripMenuItem_Click );
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size( 137, 6 );
            // 
            // stacksSelectToolStripMenuItem
            // 
            this.stacksSelectToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.stacksSelectToolStripMenuItem.Name = "stacksSelectToolStripMenuItem";
            this.stacksSelectToolStripMenuItem.Size = new System.Drawing.Size( 140, 22 );
            this.stacksSelectToolStripMenuItem.Text = "&Select...";
            this.stacksSelectToolStripMenuItem.Click += new System.EventHandler( this.stacksSelectToolStripMenuItem_Click );
            // 
            // datatypesToolStripMenuItem
            // 
            this.datatypesToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.datatypesToolStripMenuItem.DropDownItems.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.datatypesAllToolStripMenuItem,
            this.datatypesNoneToolStripMenuItem,
            this.toolStripSeparator6,
            this.datatypesNativeTypesToolStripMenuItem,
            this.datatypesClassesToolStripMenuItem,
            this.datatypesStructuresToolStripMenuItem,
            this.datatypesUnknownTypesToolStripMenuItem,
            this.toolStripSeparator7,
            this.datatypesSelectToolStripMenuItem} );
            this.datatypesToolStripMenuItem.Name = "datatypesToolStripMenuItem";
            this.datatypesToolStripMenuItem.Size = new System.Drawing.Size( 174, 22 );
            this.datatypesToolStripMenuItem.Text = "&Datatypes";
            this.datatypesToolStripMenuItem.DropDownOpening += new System.EventHandler( this.datatypesToolStripMenuItem_DropDownOpening );
            // 
            // datatypesAllToolStripMenuItem
            // 
            this.datatypesAllToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.datatypesAllToolStripMenuItem.Name = "datatypesAllToolStripMenuItem";
            this.datatypesAllToolStripMenuItem.Size = new System.Drawing.Size( 159, 22 );
            this.datatypesAllToolStripMenuItem.Text = "&All";
            this.datatypesAllToolStripMenuItem.Click += new System.EventHandler( this.datatypesAllToolStripMenuItem_Click );
            // 
            // datatypesNoneToolStripMenuItem
            // 
            this.datatypesNoneToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.datatypesNoneToolStripMenuItem.Name = "datatypesNoneToolStripMenuItem";
            this.datatypesNoneToolStripMenuItem.Size = new System.Drawing.Size( 159, 22 );
            this.datatypesNoneToolStripMenuItem.Text = "&None";
            this.datatypesNoneToolStripMenuItem.Click += new System.EventHandler( this.datatypesNoneToolStripMenuItem_Click );
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size( 156, 6 );
            // 
            // datatypesNativeTypesToolStripMenuItem
            // 
            this.datatypesNativeTypesToolStripMenuItem.CheckOnClick = true;
            this.datatypesNativeTypesToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.datatypesNativeTypesToolStripMenuItem.Name = "datatypesNativeTypesToolStripMenuItem";
            this.datatypesNativeTypesToolStripMenuItem.Size = new System.Drawing.Size( 159, 22 );
            this.datatypesNativeTypesToolStripMenuItem.Text = "&Native Types";
            this.datatypesNativeTypesToolStripMenuItem.Click += new System.EventHandler( this.datatypesNativeTypesToolStripMenuItem_Click );
            // 
            // datatypesClassesToolStripMenuItem
            // 
            this.datatypesClassesToolStripMenuItem.CheckOnClick = true;
            this.datatypesClassesToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.datatypesClassesToolStripMenuItem.Name = "datatypesClassesToolStripMenuItem";
            this.datatypesClassesToolStripMenuItem.Size = new System.Drawing.Size( 159, 22 );
            this.datatypesClassesToolStripMenuItem.Text = "&Classes";
            this.datatypesClassesToolStripMenuItem.Click += new System.EventHandler( this.datatypesClassesToolStripMenuItem_Click );
            // 
            // datatypesStructuresToolStripMenuItem
            // 
            this.datatypesStructuresToolStripMenuItem.CheckOnClick = true;
            this.datatypesStructuresToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.datatypesStructuresToolStripMenuItem.Name = "datatypesStructuresToolStripMenuItem";
            this.datatypesStructuresToolStripMenuItem.Size = new System.Drawing.Size( 159, 22 );
            this.datatypesStructuresToolStripMenuItem.Text = "S&tructures";
            this.datatypesStructuresToolStripMenuItem.Click += new System.EventHandler( this.datatypesStructuresToolStripMenuItem_Click );
            // 
            // datatypesUnknownTypesToolStripMenuItem
            // 
            this.datatypesUnknownTypesToolStripMenuItem.CheckOnClick = true;
            this.datatypesUnknownTypesToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.datatypesUnknownTypesToolStripMenuItem.Name = "datatypesUnknownTypesToolStripMenuItem";
            this.datatypesUnknownTypesToolStripMenuItem.Size = new System.Drawing.Size( 159, 22 );
            this.datatypesUnknownTypesToolStripMenuItem.Text = "&Unknown types";
            this.datatypesUnknownTypesToolStripMenuItem.Click += new System.EventHandler( this.datatypesUnknownTypesToolStripMenuItem_Click );
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size( 156, 6 );
            // 
            // datatypesSelectToolStripMenuItem
            // 
            this.datatypesSelectToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.datatypesSelectToolStripMenuItem.Name = "datatypesSelectToolStripMenuItem";
            this.datatypesSelectToolStripMenuItem.Size = new System.Drawing.Size( 159, 22 );
            this.datatypesSelectToolStripMenuItem.Text = "&Select...";
            this.datatypesSelectToolStripMenuItem.Click += new System.EventHandler( this.datatypesSelectToolStripMenuItem_Click );
            // 
            // showEmptyItemsToolStripMenuItem
            // 
            this.showEmptyItemsToolStripMenuItem.CheckOnClick = true;
            this.showEmptyItemsToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.showEmptyItemsToolStripMenuItem.Name = "showEmptyItemsToolStripMenuItem";
            this.showEmptyItemsToolStripMenuItem.Size = new System.Drawing.Size( 174, 22 );
            this.showEmptyItemsToolStripMenuItem.Text = "Show &Empty Items";
            this.showEmptyItemsToolStripMenuItem.Click += new System.EventHandler( this.showEmptyItemsToolStripMenuItem_Click );
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size( 6, 25 );
            // 
            // refreshToolStripButton
            // 
            this.refreshToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.refreshToolStripButton.Enabled = false;
            this.refreshToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject( "refreshToolStripButton.Image" )));
            this.refreshToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.refreshToolStripButton.Name = "refreshToolStripButton";
            this.refreshToolStripButton.Size = new System.Drawing.Size( 49, 22 );
            this.refreshToolStripButton.Text = "Refresh";
            this.refreshToolStripButton.Click += new System.EventHandler( this.refreshToolStripButton_Click );
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.showByToolStripStatusLabel,
            this.selectedStacksToolStripStatusLabel,
            this.selectedDatatypesToolStripStatusLabel,
            this.selectedSizeToolStripStatusLabel} );
            this.statusStrip1.Location = new System.Drawing.Point( 0, 309 );
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size( 860, 22 );
            this.statusStrip1.TabIndex = 2;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // showByToolStripStatusLabel
            // 
            this.showByToolStripStatusLabel.AutoSize = false;
            this.showByToolStripStatusLabel.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top)
                        | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right)
                        | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.showByToolStripStatusLabel.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter;
            this.showByToolStripStatusLabel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.showByToolStripStatusLabel.Name = "showByToolStripStatusLabel";
            this.showByToolStripStatusLabel.Size = new System.Drawing.Size( 215, 17 );
            this.showByToolStripStatusLabel.Text = "Show By:";
            this.showByToolStripStatusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // selectedStacksToolStripStatusLabel
            // 
            this.selectedStacksToolStripStatusLabel.AutoSize = false;
            this.selectedStacksToolStripStatusLabel.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top)
                        | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right)
                        | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.selectedStacksToolStripStatusLabel.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter;
            this.selectedStacksToolStripStatusLabel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.selectedStacksToolStripStatusLabel.Name = "selectedStacksToolStripStatusLabel";
            this.selectedStacksToolStripStatusLabel.Size = new System.Drawing.Size( 93, 17 );
            this.selectedStacksToolStripStatusLabel.Text = "Stacks:";
            this.selectedStacksToolStripStatusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // selectedDatatypesToolStripStatusLabel
            // 
            this.selectedDatatypesToolStripStatusLabel.AutoSize = false;
            this.selectedDatatypesToolStripStatusLabel.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top)
                        | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right)
                        | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.selectedDatatypesToolStripStatusLabel.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter;
            this.selectedDatatypesToolStripStatusLabel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.selectedDatatypesToolStripStatusLabel.Name = "selectedDatatypesToolStripStatusLabel";
            this.selectedDatatypesToolStripStatusLabel.Size = new System.Drawing.Size( 112, 17 );
            this.selectedDatatypesToolStripStatusLabel.Text = "Datatypes:";
            this.selectedDatatypesToolStripStatusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // selectedSizeToolStripStatusLabel
            // 
            this.selectedSizeToolStripStatusLabel.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top)
                        | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right)
                        | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.selectedSizeToolStripStatusLabel.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter;
            this.selectedSizeToolStripStatusLabel.Name = "selectedSizeToolStripStatusLabel";
            this.selectedSizeToolStripStatusLabel.Size = new System.Drawing.Size( 365, 17 );
            this.selectedSizeToolStripStatusLabel.Spring = true;
            this.selectedSizeToolStripStatusLabel.Text = "Selected Size:";
            this.selectedSizeToolStripStatusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // backgroundWorker
            // 
            this.backgroundWorker.WorkerReportsProgress = true;
            this.backgroundWorker.DoWork += new System.ComponentModel.DoWorkEventHandler( this.backgroundWorker_DoWork );
            this.backgroundWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler( this.backgroundWorker_RunWorkerCompleted );
            this.backgroundWorker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler( this.backgroundWorker_ProgressChanged );
            // 
            // OverviewListControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add( this.statusStrip1 );
            this.Controls.Add( this.toolStrip1 );
            this.Controls.Add( this.listView1 );
            this.Name = "OverviewListControl";
            this.Size = new System.Drawing.Size( 860, 331 );
            this.contextMenuStrip1.ResumeLayout( false );
            this.toolStrip1.ResumeLayout( false );
            this.toolStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout( false );
            this.statusStrip1.PerformLayout();
            this.ResumeLayout( false );
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem copyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem selectAllToolStripMenuItem;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton generateReportToolStripButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripLabel leftReportToolStripLabel;
        private System.Windows.Forms.ToolStripComboBox leftReportToolStripComboBox;
        private System.Windows.Forms.ToolStripLabel rightReportToolStripLabel;
        private System.Windows.Forms.ToolStripComboBox rightReportToolStripComboBox;
        private System.Windows.Forms.ToolStripDropDownButton optionsToolStripDropDownButton;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel showByToolStripStatusLabel;
        private System.Windows.Forms.ToolStripStatusLabel selectedStacksToolStripStatusLabel;
        private System.Windows.Forms.ToolStripStatusLabel selectedDatatypesToolStripStatusLabel;
        private System.Windows.Forms.ToolStripStatusLabel selectedSizeToolStripStatusLabel;
        private System.Windows.Forms.ToolStripMenuItem showByToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem showByStackNameToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem showByDatatypeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem showByMemoryTypeToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem showByAllocationsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem showByDeallocationsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem stacksToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem datatypesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem showEmptyItemsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem stacksAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem stacksNoneToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem stacksUnassignedToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripMenuItem stacksSelectToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem datatypesAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem datatypesNoneToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripMenuItem datatypesNativeTypesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem datatypesClassesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem datatypesStructuresToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem datatypesUnknownTypesToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripMenuItem datatypesSelectToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripButton refreshToolStripButton;
        private System.ComponentModel.BackgroundWorker backgroundWorker;
    }
}
