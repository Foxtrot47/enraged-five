using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace MemTrackerControls
{
    public partial class TrackerComponent : Component
    {
        public TrackerComponent()
        {
            InitializeComponent();

            if ( !this.DesignMode )
            {
                Reset();
            }
        }

        public TrackerComponent( IContainer container )
        {
            container.Add( this );

            InitializeComponent();

            if ( !this.DesignMode )
            {
                Reset();
            }
        }

        #region Properties
        /// <summary>
        /// The parent <see cref="Control"/> that is on the main thread of the application, typically the main <see cref="Form"/>.
        /// This will be used to invoke the events, avoiding illegal cross-threaded operations.
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        Description( "The parent Control that is on the main thread of the application." )]
        public Control ParentControl
        {
            get
            {
                return m_parentControl;
            }
            set
            {
                m_parentControl = value;
            }
        }

        /// <summary>
        /// Gets or sets the platform for which memory is being tracked.
        /// </summary>
        [Browsable( false )]
        public string Platform
        {
            get
            {
                return m_platform;
            }
            set
            {
                m_platform = value;
            }
        }

        /// <summary>
        /// Gets or sets whether information for deallocations is remembered.  Disable this to save on system memory.
        /// </summary>
        [Browsable( true ),
        Category( "Behavior" ),
        Description( "Specifies whether deallocation data is kept in memory or discarded." )]
        public bool TrackDeallocations
        {
            get
            {
                return m_rememberDeallocations;
            }
            set
            {
                m_rememberDeallocations = value;

                if ( !m_initialRememberDeallocationsSet )
                {
                    m_initialRememberDeallocations = m_rememberDeallocations;
                    m_initialRememberDeallocationsSet = true;
                }
            }
        }

        /// <summary>
        /// Determines how duplicate allocations are handled.  When false, the DuplicateAllocation Event will be raised.
        /// </summary>
        [Browsable( true ),
        Category( "Behavior" ),
        Description( "Determines how duplicate allocations are handled.  When false, the DuplicateAllocation Event will be raised." )]
        public bool OverwriteDuplicateAllocations
        {
            get
            {
                return m_overwriteAllAllocations;
            }
            set
            {
                m_overwriteAllAllocations = value;
            }
        }

        /// <summary>
        /// Gets (a copy) of the current amount of memory that is available for each of the Memory Types.
        /// </summary>
        [Browsable( false )]
        public uint[] AvailableMemory
        {
            get
            {
                uint[] array = new uint[m_availableMemory.Length];
                m_availableMemory.CopyTo( array, 0 );
                return array;
            }
        }

        /// <summary>
        /// Gets a list of <see cref="ReportItem"/>s that have been generated.
        /// </summary>
        [Browsable( false )]
        public ReportItem[] Reports
        {
            get
            {
                m_reportsMutex.WaitOne();
                
                ReportItem[] reports = m_reports.ToArray();
                
                m_reportsMutex.ReleaseMutex();
                
                return reports;
            }
        }

        /// <summary>
        /// Gets a list of <see cref="TrackerAction"/>s that have been accumulated.  Don't modify this list.
        /// </summary>
        [Browsable( false )]
        public List<TrackerAction> Actions
        {
            get
            {
                return m_trackerActions;
            }
        }

        /// <summary>
        /// Locks or unlocks the data tracking to perform an update one of the Controls.
        /// </summary>
        [Browsable( false )]
        public bool LockForUpdate
        {
            set
            {
                if ( value == true )
                {
                    m_actionMutex.WaitOne();
                }
                else
                {
                    m_actionMutex.ReleaseMutex();
                }
            }
        }
        #endregion

        #region Events
        /// <summary>
        /// Occurs when a new <see cref="ReportItem"/> is generated.
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        Description( "Occurs when a new report is generated." )]
        public event ReportChangedEventHandler ReportAdded;

        /// <summary>
        /// Occurs when a <see cref="ReportItem"/> is removed by calling <see cref="RemoveLastAction"/>.
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        Description( "Occurs when a report is removed." )]
        public event ReportChangedEventHandler ReportRemoved;

        /// <summary>
        /// Occurs when a new stack name is added.
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        Description( "Occurs when a new stack name is added." )]
        public event StackChangedEventHandler StackAdded;

        /// <summary>
        /// Occurs when a new datatype name is added.
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        Description( "Occurs when a new datatype name is added." )]
        public event DatatypeAddedEventHandler DatatypeAdded;

        /// <summary>
        /// Occurs when a new filename name is added.
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        Description( "Occurs when a new filename is added." )]
        public event FilenameAddedEventHandler FilenameAdded;

        /// <summary>
        /// Occurs when <see cref="TallyAllocationAction"/> is called for an address that already exists as an <see cref="Allocation"/>.
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        Description( "Occurs when a duplicate allocation is found." )]
        public event DuplicateAllocationEventHandler DuplicateAllocation;
        #endregion

        #region Variables
        private Control m_parentControl = null;

        private string m_platform = "Unknown";

        private Stack<string> m_stackNameStack = new Stack<string>();

        private List<ReportItem> m_reports = new List<ReportItem>();
        private List<TrackerAction> m_trackerActions = new List<TrackerAction>();

        private bool m_initialAvailableMemorySet = false;
        private uint[] m_initialAvailableMemory = new uint[(int)Allocation.MemoryTypeIndex.NumIndexes];
        private uint[] m_availableMemory = new uint[(int)Allocation.MemoryTypeIndex.NumIndexes];

        private Dictionary<uint, Allocation> m_allocationDictionary = new Dictionary<uint, Allocation>();

        private Mutex m_actionMutex = new Mutex();
        private Mutex m_reportsMutex = new Mutex();
        private Mutex m_stacksMutex = new Mutex();
        private Mutex m_datatypesMutex = new Mutex();
        private Mutex m_filenameMutex = new Mutex();

        private bool m_overwriteAllAllocations = false;
        private bool m_rememberDeallocations = false;

        private bool m_initialRememberDeallocationsSet = false;
        private bool m_initialRememberDeallocations = false;
        #endregion

        #region Public Functions
        /// <summary>
        /// Pushes a stack name.
        /// </summary>
        /// <param name="threadId">The thread on which the stack name was created.</param>
        /// <param name="stackName">The name of the stack.</param>
        public void PushStackAction( uint threadId, string stackName )
        {
            m_actionMutex.WaitOne();

            // make sure we don't have backslashes because that will later become our delimiter for the full stack path
            stackName = stackName.Replace( '\\', '/' );

            m_stackNameStack.Push( stackName );

            uint stackKey = (uint)stackName.GetHashCode();

            AddStackName( stackName, stackKey );

            m_trackerActions.Add( new PushStackAction( threadId, stackKey ) );

            m_actionMutex.ReleaseMutex();
        }

        /// <summary>
        /// Pops the stack.
        /// </summary>
        /// <param name="threadId">The thread on which the stack was popped.</param>
        public void PopStackAction( uint threadId )
        {
            m_actionMutex.WaitOne();

            string stackName = m_stackNameStack.Pop();

            m_trackerActions.Add( new PopStackAction( threadId, (uint)stackName.GetHashCode() ) );

            m_actionMutex.ReleaseMutex();
        }

        /// <summary>
        /// Records a new memory allocation.
        /// </summary>
        /// <param name="threadId">The thread on which the allocation was made.</param>
        /// <param name="decimalAddress">The address of the allocation.</param>
        /// <param name="type">The Memory Type used to allocate the memory.</param>
        /// <param name="size">The size of the allocation.</param>
        public void TallyAllocationAction( uint threadId, uint decimalAddress, int type, uint size )
        {
            m_actionMutex.WaitOne();

            Allocation theAllocation;
            if ( !m_allocationDictionary.TryGetValue( decimalAddress, out theAllocation ) )
            {
                theAllocation = new Allocation( type, decimalAddress, size );

                m_allocationDictionary.Add( decimalAddress, theAllocation );
            }
            else
            {
                Allocation existingAllocation = theAllocation;

                DuplicateAllocationAction actionToTake = DuplicateAllocationAction.OverwriteAll;
                if ( !m_overwriteAllAllocations )
                {
                    actionToTake = OnDuplicateAllocation( existingAllocation );
                    if ( actionToTake == DuplicateAllocationAction.OverwriteAll )
                    {
                        m_overwriteAllAllocations = true;
                    }
                }

                // create a new object
                theAllocation = new Allocation( type, decimalAddress, size );

                // remember allocation error
                m_trackerActions.Add( new TallyErrorAction( threadId, existingAllocation, theAllocation, actionToTake ) );

                // forget the new allocation
                if ( actionToTake == DuplicateAllocationAction.Leave )
                {
                    m_actionMutex.ReleaseMutex();
                    return;
                }

                // remove the last TallyAllocationAction for the existing Allocation
                for ( int i = m_trackerActions.Count - 1; i >= 0; --i )
                {
                    if ( m_trackerActions[i].TheAction == TrackerAction.Action.TallyAllocation )
                    {
                        TallyAllocationAction tallyAction = m_trackerActions[i] as TallyAllocationAction;
                        if ( tallyAction.TheAllocation == existingAllocation )
                        {
                            AddToAvailableMemory( existingAllocation );

                            // fixup report indexes
                            foreach ( ReportItem report in m_reports )
                            {
                                if ( report.Index > i )
                                {
                                    --report.Index;
                                }
                            }

                            m_trackerActions.RemoveAt( i );
                            break;
                        }
                    }
                }

                m_allocationDictionary[decimalAddress] = theAllocation;
            }

            RemoveFromAvailableMemory( theAllocation );

            string[] stacks = new string[m_stackNameStack.Count];
            m_stackNameStack.CopyTo( stacks, 0 );
            theAllocation.Stack = stacks;

            m_trackerActions.Add( new TallyAllocationAction( threadId, theAllocation ) );

            m_actionMutex.ReleaseMutex();
        }

        /// <summary>
        /// Adds additional information for an <see cref="Allocation"/>.
        /// </summary>
        /// <param name="threadId">The thread on which the info is being added from.</param>
        /// <param name="decimalAddress">The address of the allocation.</param>
        /// <param name="size">The size of the allocation.</param>
        /// <param name="dataTypeHash">The hash key indicating the datatype of the allocation.</param>
        /// <param name="fileNameHash">The hash key indicating the filename for where the allocation was made.</param>
        /// <param name="line">The line number in the file on which the allocation was made.</param>
        public void AddInfoAction( uint threadId, uint decimalAddress, uint size, uint dataTypeHash, uint fileNameHash, int line )
        {
            m_actionMutex.WaitOne();

            Allocation theAllocation;
            if ( !m_allocationDictionary.TryGetValue( decimalAddress, out theAllocation ) || (theAllocation.FileName != string.Empty) )
            {
                // treat this as an array element, so find it's "parent" tally action
                Allocation arrayAllocation = null;
                int i = m_trackerActions.Count - 1;
                while ( i >= 0 )
                {
                    if ( m_trackerActions[i].TheAction == TrackerAction.Action.TallyAllocation )
                    {
                        Allocation a = ((TallyAllocationAction)m_trackerActions[i]).TheAllocation;
                        if ( (decimalAddress >= a.DecimalAddress) && (decimalAddress < a.DecimalAddress + a.Size) )
                        {
                            arrayAllocation = a;
                            break;
                        }
                    }

                    --i;
                }

                if ( arrayAllocation == null )
                {
                    //
                    // We believe this happens when you do something like this:
                    // new myClass;
                    //
                    TallyAllocationAction( threadId, decimalAddress, -1, size );
                    m_allocationDictionary.TryGetValue( decimalAddress, out theAllocation );

                    theAllocation.TypeNameHashCode = dataTypeHash;
                    theAllocation.FileNameHashCode = fileNameHash;
                    theAllocation.LineNumber = line;

                    m_actionMutex.ReleaseMutex();
                    return;
                }

                theAllocation = new Allocation( arrayAllocation.MemoryType, decimalAddress, size );
                theAllocation.TypeNameHashCode = dataTypeHash;
                theAllocation.FileNameHashCode = fileNameHash;
                theAllocation.LineNumber = line;

                string typeName = null;
                if ( Allocation.DataTypeDictionary.TryGetValue( dataTypeHash, out typeName ) )
                {
                    typeName += "[]";
                    AddDataType( typeName, (uint)typeName.GetHashCode() );
                }

                arrayAllocation.TypeName = typeName;
                //arrayAllocation.FileName = fileName;
                //arrayAllocation.LineNumber = line;

                if ( arrayAllocation.Items == null )
                {
                    arrayAllocation.Items = new List<Allocation>();
                }

                arrayAllocation.Items.Add( theAllocation );
            }
            else
            {
                theAllocation.TypeNameHashCode = dataTypeHash;
                theAllocation.FileNameHashCode = fileNameHash;
                theAllocation.LineNumber = line;
            }

            m_actionMutex.ReleaseMutex();
        }

        /// <summary>
        /// Reports that an allocation was free'd.
        /// </summary>
        /// <param name="threadId">The thread on which the allocation was free'd.</param>
        /// <param name="decimalAddress">The address of the allocation.</param>
        /// <param name="size">The number of bytes that were free'd.</param>
        public void UntallyAllocationAction( uint threadId, uint decimalAddress, uint size )
        {
            m_actionMutex.WaitOne();

            Allocation theAllocation;
            if ( m_allocationDictionary.TryGetValue( decimalAddress, out theAllocation ) )
            {
                int index = -1;
                bool spansReports = true;

                m_reportsMutex.WaitOne();

                if ( !m_rememberDeallocations )
                {
                    if ( m_reports.Count > 0 )
                    {
                        // try to find our corresponding tally before the last report
                        for ( int i = m_trackerActions.Count - 1; i >= m_reports[m_reports.Count - 1].Index; --i )
                        {
                            if ( (m_trackerActions[i].TheAction == TrackerAction.Action.TallyAllocation)
                                && (((TallyAllocationAction)m_trackerActions[i]).TheAllocation == theAllocation) )
                            {
                                index = i;
                                spansReports = false;
                                break;
                            }
                        }
                    }
                    else
                    {
                        // try to find our corresponding tally
                        for ( int i = 0; i < m_trackerActions.Count; ++i )
                        {
                            if ( (m_trackerActions[i].TheAction == TrackerAction.Action.TallyAllocation)
                                && (((TallyAllocationAction)m_trackerActions[i]).TheAllocation == theAllocation) )
                            {
                                index = i;
                                spansReports = false;
                                break;
                            }
                        }
                    }
                }

                // we straddle a report, so remember the untally
                if ( spansReports )
                {
                    m_trackerActions.Add( new UntallyAllocationAction( threadId, theAllocation ) );
                }
                // otherwise, let's forget about the allocation altogether
                else
                {
                    Debug.Assert( index != -1, "Could not locate Tally action for an Untally." );

                    // fixup report indexes
                    foreach ( ReportItem report in m_reports )
                    {
                        if ( report.Index > index )
                        {
                            --report.Index;
                        }
                    }

                    m_trackerActions.RemoveAt( index );
                }

                AddToAvailableMemory( theAllocation );

                m_reportsMutex.ReleaseMutex();

                m_allocationDictionary.Remove( decimalAddress );
            }
            else
            {
                m_trackerActions.Add( new UntallyErrorAction( threadId, decimalAddress, size ) );
            }

            m_actionMutex.ReleaseMutex();
        }

        /// <summary>
        /// Adds a new <see cref="ReportItem"/>.
        /// </summary>
        /// <param name="threadId">The thread on which the report is being created.</param>
        /// <param name="reportName">The name of the report.</param>
        /// <param name="available">The available memory at the time of the report.</param>
        /// <param name="trackingDeallocations">Indicates whether deallocations were being remembered or forgotten.</param>
        /// <returns>The <see cref="ReportItem"/> that was created.</returns>
        public ReportItem AddReportAction( uint threadId, string reportName, uint[] available, bool trackingDeallocations )
        {
            SetAvailableMemory( threadId, available, true );

            m_actionMutex.WaitOne();
            m_reportsMutex.WaitOne();

            StringBuilder name = new StringBuilder();
            name.Append( m_reports.Count );
            name.Append( ". " );
            name.Append( reportName );
            name.Append( " (" );
            name.Append( System.DateTime.Now.ToString( "G" ) );
            name.Append( ")" );

            if ( trackingDeallocations )
            {
                name.Append( "*" );
            }

            ReportItem report = new ReportItem( name.ToString(), m_trackerActions.Count, available, trackingDeallocations );
            m_trackerActions.Add( new ReportAction( threadId, report ) );
            m_reports.Add( report );

            m_reportsMutex.ReleaseMutex();
            m_actionMutex.ReleaseMutex();            

            OnReportAdded( report );

            return report;
        }

        /// <summary>
        /// Adds a new datatype
        /// </summary>
        /// <param name="name">The name of the datatype.</param>
        /// <param name="hashKey">It's hash key.</param>
        public void AddDataType( string name, uint hashKey )
        {
            m_datatypesMutex.WaitOne();

            if ( (name == Allocation.Unknown) || Allocation.DataTypeDictionary.ContainsKey( hashKey ) )
            {
                m_datatypesMutex.ReleaseMutex();
                return;
            }

            Allocation.DataTypeDictionary.Add( hashKey, name );

            m_datatypesMutex.ReleaseMutex();

            OnDatatypeAdded( name, hashKey );
        }

        /// <summary>
        /// Adds a new stack name.
        /// </summary>
        /// <param name="name">The name of the stack.</param>
        /// <param name="hashKey">It's hash key.</param>
        public void AddStackName( string name, uint hashKey )
        {
            m_stacksMutex.WaitOne();

            if ( (name == Allocation.Unassigned) || Allocation.StackNameDictionary.ContainsKey( hashKey ) )
            {
                m_stacksMutex.ReleaseMutex();
                return;
            }

            Allocation.StackNameDictionary.Add( hashKey, name );

            m_stacksMutex.ReleaseMutex();

            OnStackAdded( name, hashKey );
        }

        /// <summary>
        /// Adds a new file name.
        /// </summary>
        /// <param name="filename">The name of the file.</param>
        /// <param name="hashKey">It's hash key.</param>
        public void AddFilename( string filename, uint hashKey )
        {
            m_filenameMutex.WaitOne();

            if ( Allocation.FileNameDictionary.ContainsKey( hashKey ) || (filename == string.Empty) )
            {
                m_filenameMutex.ReleaseMutex();
                return;
            }

            Allocation.FileNameDictionary.Add( hashKey, filename );

            m_filenameMutex.ReleaseMutex();
        }

        /// <summary>
        /// Set the current available memory
        /// </summary>
        /// <param name="availableMemory">The array of available memory for each memory type.</param>
        /// <param name="validate"><c>true</c> if we should verify that the available memory matches up with our running totals.</param>
        public void SetAvailableMemory( uint threadID, uint[] availableMemory, bool validate )
        {
            m_actionMutex.WaitOne();

            if ( !m_initialAvailableMemorySet )
            {
                m_initialAvailableMemory = new uint[availableMemory.Length];
                availableMemory.CopyTo( m_initialAvailableMemory, 0 );

                m_initialAvailableMemorySet = true;
            }
            else if ( validate )
            {
                // gather the memory indexes to check
                List<int> indexes = new List<int>();
                switch ( this.Platform )                
                {
                    case "PS3":
                        indexes.Add( (int)Allocation.MemoryTypeIndex.GameVirtual );
                        indexes.Add( (int)Allocation.MemoryTypeIndex.ResourceVirtual );
                        indexes.Add( (int)Allocation.MemoryTypeIndex.GamePhysical );
                        indexes.Add( (int)Allocation.MemoryTypeIndex.ResourcePhysical );
                        break;
                    case "Xenon":
                        indexes.Add( (int)Allocation.MemoryTypeIndex.GameVirtual );
                        indexes.Add( (int)Allocation.MemoryTypeIndex.GamePhysical );
                        indexes.Add( (int)Allocation.MemoryTypeIndex.ConsoleSpecific );
                        break;
                    default:
                        indexes.Add( (int)Allocation.MemoryTypeIndex.GameVirtual );
                        indexes.Add( (int)Allocation.MemoryTypeIndex.GamePhysical );
                        break;
                }

                // check each available memory type size against our running total
                foreach ( int index in indexes )
                {
                    if ( availableMemory[index] != m_availableMemory[index] )
                    {
                        // uh-oh
                        MemoryErrorAction errorAction = new MemoryErrorAction( threadID, (Allocation.MemoryTypeIndex)index,
                            m_availableMemory[index], availableMemory[index] );
                        m_trackerActions.Add( errorAction );
                    }
                }
            }

            m_availableMemory = new uint[availableMemory.Length];
            availableMemory.CopyTo( m_availableMemory, 0 );

            m_actionMutex.ReleaseMutex();
        }

        /// <summary>
        /// Removes the last <see cref="TrackerAction"/> and any associated data.
        /// </summary>
        public void RemoveLastAction()
        {
            m_actionMutex.WaitOne();
            m_reportsMutex.WaitOne();
            m_stacksMutex.WaitOne();

            ReportItem reportRemoved = null;

            if ( m_trackerActions.Count > 0 )
            {
                TrackerAction action = m_trackerActions[m_trackerActions.Count - 1];

                m_trackerActions.RemoveAt( m_trackerActions.Count - 1 );

                switch ( action.TheAction )
                {
                    case TrackerAction.Action.Report:
                        {
                            ReportAction reportAction = action as ReportAction;
                            m_reports.Remove( reportAction.Report );

                            reportRemoved = reportAction.Report;
                        }
                        break;
                    case TrackerAction.Action.PushStack:
                        {
                            PushStackAction stackAction = action as PushStackAction;
                            string topStackName = m_stackNameStack.Peek();
                            if ( topStackName == stackAction.Name )
                            {
                                m_stackNameStack.Pop();
                            }
                            else
                            {
                                Console.WriteLine( "Error:  Stack names do not match" );
                            }
                        }
                        break;
                    case TrackerAction.Action.PopStack:
                        {
                            PopStackAction stackAction = action as PopStackAction;
                            m_stackNameStack.Push( stackAction.Name );
                        }
                        break;
                    case TrackerAction.Action.TallyAllocation:
                        {
                            TallyAllocationAction tallyAction = action as TallyAllocationAction;
                            m_allocationDictionary.Remove( tallyAction.TheAllocation.DecimalAddress );
                        }
                        break;
                    case TrackerAction.Action.UntallyAllocation:
                        {
                            UntallyAllocationAction untallyAction = action as UntallyAllocationAction;
                            m_allocationDictionary.Add( untallyAction.TheAllocation.DecimalAddress, untallyAction.TheAllocation );
                        }
                        break;
                }
            }

            m_stacksMutex.ReleaseMutex();
            m_reportsMutex.ReleaseMutex();
            m_actionMutex.ReleaseMutex();

            if ( reportRemoved != null )
            {
                OnReportRemoved( reportRemoved );
            }
        }

        /// <summary>
        /// Removes any allocation information for the specified address
        /// </summary>
        public void RemoveInfoAction( uint decimalAddress )
        {
            m_actionMutex.WaitOne();

            Allocation theAllocation;
            if ( !m_allocationDictionary.TryGetValue( decimalAddress, out theAllocation ) )
            {
                // treat this as an array element, so find it's "parent" tally action
                Allocation arrayAllocation = null;
                int i = m_trackerActions.Count - 1;
                while ( i >= 0 )
                {
                    if ( m_trackerActions[i].TheAction == TrackerAction.Action.TallyAllocation )
                    {
                        Allocation a = ((TallyAllocationAction)m_trackerActions[i]).TheAllocation;
                        if ( (decimalAddress >= a.DecimalAddress) && (decimalAddress < a.DecimalAddress + a.Size) )
                        {
                            arrayAllocation = a;
                            break;
                        }
                    }

                    --i;
                }

                if ( arrayAllocation == null )
                {
                    //
                    // We believe this happens when you do something like this:
                    // new myClass;
                    //

                    m_actionMutex.ReleaseMutex();
                    return;
                }

                if ( arrayAllocation.Items != null )
                {
                    for ( i = 0; i < arrayAllocation.Items.Count; ++i )
                    {
                        if ( arrayAllocation.Items[i].DecimalAddress == decimalAddress )
                        {
                            arrayAllocation.Items.RemoveAt( i );
                            break;
                        }
                    }

                    if ( arrayAllocation.Items.Count == 0 )
                    {
                        arrayAllocation.TypeName = Allocation.Unknown;
                        arrayAllocation.FileName = string.Empty;
                        arrayAllocation.LineNumber = 0;
                        arrayAllocation.Items = null;
                    }
                }
            }
            else
            {
                theAllocation.TypeName = Allocation.Unknown;
                theAllocation.FileName = string.Empty;
                theAllocation.LineNumber = 0;
            }

            m_actionMutex.ReleaseMutex();
        }

        /// <summary>
        /// Saves the report to the specified file.  If the <see cref="ReportItem"/> is <c>null</c>, the entire data set is saved.
        /// </summary>
        /// <param name="filename">The name of the file to save.</param>
        /// <param name="report">The <see cref="ReportItem"/> indicating what data to save.</param>
        public void SaveReport( string filename, ReportItem report )
        {
            m_actionMutex.WaitOne();
            m_reportsMutex.WaitOne();

            int reportIndex = report != null ? report.Index : m_trackerActions.Count - 1;

            using ( StreamWriter writer = new StreamWriter( filename ) )
            {
                writer.Write( "0;Pl;" );
                writer.Write( m_platform );
                writer.Write( ";" );
                foreach ( uint availableMemory in m_availableMemory )
                {
                    writer.Write( ";" );
                    writer.Write( availableMemory );
                }
                writer.Write( ";" );
                writer.Write( m_initialRememberDeallocations.ToString() );
                writer.Write( "\r\n" );

                for ( int i = 0; i <= reportIndex; ++i )
                {
                    TrackerAction action = m_trackerActions[i];

                    // IMPORTANT: what we do here needs to match the original message that was sent to us from rage/base/src/diag/tracker_file.cpp

                    System.Text.StringBuilder s = new System.Text.StringBuilder();

                    s.Append( action.ToString() );
                    s.Append( "\r\n" );

                    if ( action.TheAction == TrackerAction.Action.TallyAllocation )
                    {
                        TallyAllocationAction tallyAction = action as TallyAllocationAction;
                        
                        string info = tallyAction.InfoString;
                        if ( info != string.Empty )
                        {
                            s.Append( info );
                            s.Append( "\r\n" );
                        }

                        if ( tallyAction.TheAllocation.IsArray )
                        {
                            foreach ( Allocation a in tallyAction.TheAllocation.Items )
                            {
                                info = tallyAction.GetInfoString( a );
                                if ( info != string.Empty )
                                {
                                    s.Append( info );
                                    s.Append( "\r\n" );
                                }
                            }
                        }
                    }

                    writer.Write( s.ToString() );
                }
            }

            m_reportsMutex.ReleaseMutex();
            m_actionMutex.ReleaseMutex();
        }

        public void Reset()
        {
            m_actionMutex.WaitOne();
            m_reportsMutex.WaitOne();
            m_stacksMutex.WaitOne();
            m_datatypesMutex.WaitOne();
            m_filenameMutex.WaitOne();

            m_platform = "Unknown";
            m_stackNameStack.Clear();

            m_reports.Clear();
            m_trackerActions.Clear();

            m_initialAvailableMemorySet = false;

            for ( int i = 0; i < m_initialAvailableMemory.Length; ++i )
            {
                m_initialAvailableMemory[i] = 0;
            }

            for ( int i = 0; i < m_availableMemory.Length; ++i )
            {
                m_availableMemory[0] = 0;
            }

            m_allocationDictionary.Clear();

            m_overwriteAllAllocations = false;
            m_rememberDeallocations = false;

            m_initialRememberDeallocationsSet = false;
            m_initialRememberDeallocations = false;

            m_filenameMutex.ReleaseMutex();
            m_datatypesMutex.ReleaseMutex();
            m_stacksMutex.ReleaseMutex();
            m_reportsMutex.ReleaseMutex();
            m_actionMutex.ReleaseMutex();
        }
        #endregion

        #region Protected Functions
        protected virtual void OnReportAdded( ReportItem report )
        {
            if ( this.ReportAdded != null )
            {
                ReportChangedEventArgs e = new ReportChangedEventArgs( report );

                if ( m_parentControl != null )
                {
                    object[] args = new object[] { this, e };
                    m_parentControl.Invoke( this.ReportAdded, args );
                }
                else
                {
                    this.ReportAdded( this, e );
                }
            }
        }

        protected virtual void OnReportRemoved( ReportItem report )
        {
            if ( this.ReportRemoved != null )
            {
                ReportChangedEventArgs e = new ReportChangedEventArgs( report );

                if ( m_parentControl != null )
                {
                    object[] args = new object[] { this, e };
                    m_parentControl.Invoke( this.ReportRemoved, args );
                }
                else
                {
                    this.ReportRemoved( this, e );
                }
            }
        }

        protected virtual void OnStackAdded( string name, uint hashKey )
        {
            if ( this.StackAdded != null )
            {
                StackChangedEventArgs e = new StackChangedEventArgs( name, hashKey );

                if ( m_parentControl != null )
                {
                    object[] args = new object[] { this, e };
                    m_parentControl.Invoke( this.StackAdded, args );
                }
                else
                {
                    this.StackAdded( this, e );
                }
            }
        }

        protected virtual void OnDatatypeAdded( string name, uint hashKey )
        {
            if ( this.DatatypeAdded != null )
            {
                DatatypeAddedEventArgs e = new DatatypeAddedEventArgs( name, hashKey );

                if ( m_parentControl != null )
                {
                    object[] args = new object[] { this, e };
                    m_parentControl.Invoke( this.DatatypeAdded, args );
                }
                else
                {
                    this.DatatypeAdded( this, e );
                }
            }
        }

        protected virtual void OnFilenameAdded( string name, uint hashKey )
        {
            if ( this.FilenameAdded != null )
            {
                FilenameAddedEventArgs e = new FilenameAddedEventArgs( name, hashKey );

                if ( m_parentControl != null )
                {
                    object[] args = new object[] { this, e };
                    m_parentControl.Invoke( this.FilenameAdded, args );
                }
                else
                {
                    this.FilenameAdded( this, e );
                }
            }
        }

        protected virtual DuplicateAllocationAction OnDuplicateAllocation( Allocation theAllocation )
        {
            if ( this.DuplicateAllocation != null )
            {
                DuplicateAllocationEventArgs e = new DuplicateAllocationEventArgs( theAllocation );

                if ( m_parentControl != null )
                {
                    object[] args = new object[] { this, e };
                    m_parentControl.Invoke( this.DuplicateAllocation, args );
                }
                else
                {
                    this.DuplicateAllocation( this, e );
                }

                return e.ActionToTake;
            }

            return DuplicateAllocationAction.Overwrite;
        }
        #endregion

        #region Private Functions
        private void AddToAvailableMemory( Allocation theAllocation )
        {
            // ResourceVirtual and GamePhysical and ResourcePhysical use the same memory allocation, so only total them once
            switch ( theAllocation.MemoryType )
            {
                case Allocation.MemoryTypeIndex.GamePhysical:
                case Allocation.MemoryTypeIndex.ResourcePhysical:
                case Allocation.MemoryTypeIndex.ResourceVirtual:
                    {
                        if ( this.Platform == "PS3" )
                        {
                            m_availableMemory[(int)theAllocation.MemoryType] -= theAllocation.Size;
                        }
                        else
                        {
                            m_availableMemory[(int)Allocation.MemoryTypeIndex.GamePhysical] -= theAllocation.Size;
                            m_availableMemory[(int)Allocation.MemoryTypeIndex.ResourcePhysical] -= theAllocation.Size;
                            m_availableMemory[(int)Allocation.MemoryTypeIndex.ResourceVirtual] -= theAllocation.Size;
                        }
                    }
                    break;
                case Allocation.MemoryTypeIndex.GameVirtual:
                case Allocation.MemoryTypeIndex.Undetermined:
                case Allocation.MemoryTypeIndex.ConsoleSpecific:
                    {
                        m_availableMemory[(int)theAllocation.MemoryType] -= theAllocation.Size;
                    }
                    break;
            }
        }

        private void RemoveFromAvailableMemory( Allocation theAllocation )
        {
            // ResourceVirtual and GamePhysical and ResourcePhysical use the same memory allocation, so only total them once
            switch ( theAllocation.MemoryType )
            {
                case Allocation.MemoryTypeIndex.GamePhysical:
                case Allocation.MemoryTypeIndex.ResourcePhysical:
                case Allocation.MemoryTypeIndex.ResourceVirtual:
                    {
                        if ( this.Platform == "PS3" )
                        {
                            m_availableMemory[(int)theAllocation.MemoryType] += theAllocation.Size;
                        }
                        else
                        {
                            m_availableMemory[(int)Allocation.MemoryTypeIndex.GamePhysical] += theAllocation.Size;
                            m_availableMemory[(int)Allocation.MemoryTypeIndex.ResourcePhysical] += theAllocation.Size;
                            m_availableMemory[(int)Allocation.MemoryTypeIndex.ResourceVirtual] += theAllocation.Size;
                        }
                    }
                    break;
                case Allocation.MemoryTypeIndex.GameVirtual:
                case Allocation.MemoryTypeIndex.Undetermined:
                case Allocation.MemoryTypeIndex.ConsoleSpecific:
                    {
                        m_availableMemory[(int)theAllocation.MemoryType] += theAllocation.Size;
                    }
                    break;
            }
        }
        #endregion
    }
}
