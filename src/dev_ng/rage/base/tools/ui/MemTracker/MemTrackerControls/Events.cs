using System;
using System.Collections.Generic;
using System.Text;

namespace MemTrackerControls
{
    #region Enums
    public enum DuplicateAllocationAction
    {
        Leave,
        Overwrite,
        OverwriteAll
    }
    #endregion

    #region Delegates
    public delegate void ReportChangedEventHandler( object sender, ReportChangedEventArgs e );
    public delegate void StackChangedEventHandler( object sender, StackChangedEventArgs e );
    public delegate void DatatypeAddedEventHandler( object sender, DatatypeAddedEventArgs e );
    public delegate void FilenameAddedEventHandler( object sender, FilenameAddedEventArgs e );
    
    public delegate void DuplicateAllocationEventHandler( object sender, DuplicateAllocationEventArgs e );
    public delegate void ErrorEventHandler( object sender, ErrorEventArgs e );
    
    public delegate void UpdateBeginEventHandler( object sender, UpdateBeginEventArgs e );
    public delegate void UpdateStepEventHandler( object sender, UpdateStepEventArgs e );
    public delegate void UpdateCompleteEventHandler( object sender, UpdateCompleteEventArgs e );
    #endregion

    #region EventArgs
    public class ReportChangedEventArgs : EventArgs
    {
        public ReportChangedEventArgs( ReportItem report )
        {
            m_report = report;            
        }

        #region Properties
        /// <summary>
        /// Gets the <see cref="ReportItem"/> that was generated.
        /// </summary>
        public ReportItem Report
        {
            get
            {
                return m_report;
            }
        }
        #endregion

        #region Variables
        private ReportItem m_report;
        #endregion
    }

    public class StackChangedEventArgs : EventArgs
    {
        public StackChangedEventArgs( string name, uint hashKey )
        {
            m_name = name;
            m_hashKey = hashKey;
        }

        #region Properties
        /// <summary>
        /// Gets the name of the stack that was added.
        /// </summary>
        public string StackName
        {
            get
            {
                return m_name;
            }
        }

        /// <summary>
        /// Gets the hash key for the stack that was added.
        /// </summary>
        public uint HashKey
        {
            get
            {
                return m_hashKey;
            }
        }
        #endregion

        #region Variables
        private string m_name;
        private uint m_hashKey;
        #endregion
    }

    public class DatatypeAddedEventArgs : EventArgs
    {
        public DatatypeAddedEventArgs( string name, uint hashKey )
        {
            m_name = name;
            m_hashKey = hashKey;
        }

        #region Properties
        /// <summary>
        /// Gets the name of the datatype that was added.
        /// </summary>
        public string DatatypeName
        {
            get
            {
                return m_name;
            }
        }

        /// <summary>
        /// Gets the hash key for the datatype that was added.
        /// </summary>
        public uint HashKey
        {
            get
            {
                return m_hashKey;
            }
        }
        #endregion

        #region Variables
        private string m_name;
        private uint m_hashKey;
        #endregion
    }
    
    public class FilenameAddedEventArgs : EventArgs
    {
        public FilenameAddedEventArgs( string filename, uint hashKey )
        {
            m_filename = filename;
            m_hashKey = hashKey;
        }

        #region Properties
        /// <summary>
        /// Gets the name of the datatype that was added.
        /// </summary>
        public string FileName
        {
            get
            {
                return m_filename;
            }
        }

        /// <summary>
        /// Gets the hash key for the datatype that was added.
        /// </summary>
        public uint HashKey
        {
            get
            {
                return m_hashKey;
            }
        }
        #endregion

        #region Variables
        private string m_filename;
        private uint m_hashKey;
        #endregion
    }

    public class DuplicateAllocationEventArgs : EventArgs
    {
        public DuplicateAllocationEventArgs( Allocation theAllocation )
        {
            m_allocation = theAllocation;
            m_actionToTake = DuplicateAllocationAction.Overwrite;
        }

        #region Properties
        /// <summary>
        /// Gets the <see cref="Allocation"/> with the address that we are getting the duplicate <see cref="TallyAllocationAction"/> for.
        /// </summary>
        public Allocation TheAllocation
        {
            get
            {
                return m_allocation;
            }
        }

        /// <summary>
        /// Gets or sets what to do with the duplicate allocation.
        /// </summary>
        public DuplicateAllocationAction ActionToTake
        {
            get
            {
                return m_actionToTake;
            }
            set
            {
                m_actionToTake = value;
            }
        }
        #endregion

        #region Variables
        private Allocation m_allocation;
        private DuplicateAllocationAction m_actionToTake;
        #endregion
    }

    public class ErrorEventArgs : EventArgs
    {
        public ErrorEventArgs( string message, Exception e )
        {
            m_message = message;
            m_exception = e;
        }

        #region Properties
        /// <summary>
        /// Gets the error message.
        /// </summary>
        public string Message
        {
            get
            {
                return m_message;
            }
        }

        /// <summary>
        /// Gets the <see cref="Exception"/>, if any.
        /// </summary>
        public Exception Exception
        {
            get
            {
                return m_exception;
            }
        }
        #endregion

        #region Variables
        private string m_message;
        private Exception m_exception;
        #endregion
    }

    public class UpdateBeginEventArgs : EventArgs
    {
        public UpdateBeginEventArgs( int numSteps, bool generatingReport )
        {
            m_numSteps = numSteps;
            m_generatingReport = generatingReport;
        }

        #region Properties
        /// <summary>
        /// Gets the total number of steps that the update will take.
        /// </summary>
        public int NumSteps
        {
            get
            {
                return m_numSteps;
            }
        }

        /// <summary>
        /// Gets if this update is part of generating a new report.  Returns <c>true</c> if generating, <c>false</c> on a basic refresh.
        /// </summary>
        public bool GeneratingReport
        {
            get
            {
                return m_generatingReport;
            }
        }
        #endregion

        #region Variables
        private int m_numSteps;
        private bool m_generatingReport;
        #endregion
    }

    public class UpdateStepEventArgs : EventArgs
    {
        public UpdateStepEventArgs( int stepNumber, bool generatingReport )
        {
            m_stepNumber = stepNumber;
            m_generatingReport = generatingReport;
        }

        #region Properties
        /// <summary>
        /// Gets the current step that the update is on.
        /// </summary>
        public int StepNumber
        {
            get
            {
                return m_stepNumber;
            }
        }

        /// <summary>
        /// Gets whether this update is part of generating a new report.  Returns <c>true</c> if generating, <c>false</c> on a basic refresh.
        /// </summary>
        public bool GeneratingReport
        {
            get
            {
                return m_generatingReport;
            }
        }
        #endregion

        #region Variables
        private int m_stepNumber;
        private bool m_generatingReport;        
        #endregion
    }    

    public class UpdateCompleteEventArgs : EventArgs
    {
        public UpdateCompleteEventArgs( ReportItem reportGenerated, bool success )
        {
            m_reportGenerated = reportGenerated;
            m_success = success;
        }

        #region Properties
        /// <summary>
        /// Gets the <see cref="ReportItem"/> that was generated, if any.
        /// </summary>
        public ReportItem ReportGenerated
        {
            get
            {
                return m_reportGenerated;
            }
        }
        
        /// <summary>
        /// Gets whether this update was a success or not.
        /// </summary>
        public bool Success
        {
            get
            {
                return m_success;
            }
        }
        #endregion

        #region Variables
        private ReportItem m_reportGenerated;
        private bool m_success;
        #endregion
    }
    #endregion
}
