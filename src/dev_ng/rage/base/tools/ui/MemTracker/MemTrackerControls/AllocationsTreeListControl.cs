using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Threading;
using System.Windows.Forms;

using GlacialComponents.Controls.GlacialTreeList;

namespace MemTrackerControls
{
    /// <summary>
    /// A re-usable tree list for displaying a view of each allocation of memory based on several settings.  
    /// Has 2 display modes:  Stack Name, and Allocation ORder.
    /// </summary>
    public partial class AllocationsTreeListControl : UserControl
    {
        public AllocationsTreeListControl()
        {
            InitializeComponent();
            InitializeAdditionalComponents();
        }

        #region Enums
        /// <summary>
        /// The type of report to display.
        /// </summary>
        public enum DisplayType
        {
            Order,
            StackName
        }

        /// <summary>
        /// The range of stacks that are selected for display
        /// </summary>
        public enum SelectedStacks
        {
            All,
            None,
            Selected
        }

        /// <summary>
        /// The range of datatypes that are selected for display
        /// </summary>
        public enum SelectedDatatypes
        {
            All,
            None,
            Selected
        }

        /// <summary>
        /// The range of memory types that are selected for display
        /// </summary>
        public enum SelectedMemoryTypes
        {
            All,
            None,
            Selected
        }

        /// <summary>
        /// An enumeration of columns
        /// </summary>
        protected enum AllocationColumns
        {
            Name,
            Index,
            Type,
            Address,
            Size,
            File,
            LineNumber
        }
        #endregion

        #region Properties
        /// <summary>
        /// The underlying <see cref="GlacialTreeList"/> control.
        /// </summary>
        [Browsable( true ),
        DesignOnly( true ),
        Category( "Misc" )]
        public GlacialTreeList TreeList
        {
            get
            {
                return this.glacialTreeList1;
            }
        }

        /// <summary>
        /// The <see cref="TrackerComponent"/> that manages all of the memory allocation data.
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        Description( "The object that manages the allocation, stack, and datatype information." )]
        public TrackerComponent Tracker
        {
            get
            {
                return m_trackerComponent;
            }
            set
            {
                m_trackerComponent = value;

                if ( !this.DesignMode && (m_trackerComponent != null) )
                {
                    m_trackerComponent.LockForUpdate = true;

                    ReportItem[] reports = m_trackerComponent.Reports;
                    foreach ( ReportItem report in reports )
                    {
                        trackerComponent_ReportAdded( m_trackerComponent, new ReportChangedEventArgs( report ) );
                    }

                    m_reportAddedEventHandler = new ReportChangedEventHandler( trackerComponent_ReportAdded );
                    m_trackerComponent.ReportAdded += m_reportAddedEventHandler;

                    m_reportRemovedEventHandler = new ReportChangedEventHandler( trackerComponent_ReportRemoved );
                    m_trackerComponent.ReportRemoved += m_reportRemovedEventHandler;

                    foreach ( KeyValuePair<uint, string> pair in Allocation.StackNameDictionary )
                    {
                        trackerComponent_StackAdded( m_trackerComponent, new StackChangedEventArgs( pair.Value, pair.Key ) );
                    }

                    m_stackAddedEventHandler = new StackChangedEventHandler( trackerComponent_StackAdded );
                    m_trackerComponent.StackAdded += m_stackAddedEventHandler;

                    foreach ( KeyValuePair<uint, string> pair in Allocation.DataTypeDictionary )
                    {
                        trackerComponent_DatatypeAdded( m_trackerComponent, new DatatypeAddedEventArgs( pair.Value, pair.Key ) );
                    }

                    m_datatypeAddedEventHandler = new DatatypeAddedEventHandler( trackerComponent_DatatypeAdded );
                    m_trackerComponent.DatatypeAdded += m_datatypeAddedEventHandler;

                    m_trackerComponent.LockForUpdate = false;
                }
            }
        }

        /// <summary>
        /// A <see cref="Control"/>, typically the application's main <see cref="Form"/> that can be used to launch
        /// dialog boxes and Invoke cross-threaded methods.
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        Description( "Sets the parent Control so that dialog boxes can appear in the correct location." )]
        public Control ParentControl
        {
            get
            {
                return m_parentControl;
            }
            set
            {
                m_parentControl = value;
            }
        }

        /// <summary>
        /// Show or hide the Status Bar's sizing grip at the lower right corner of the control.
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        Description( "Show or hide the Status Bar's sizing grip at the lower right corner of the control." )]
        public bool ShowSizingGrip
        {
            get
            {
                return this.statusStrip1.SizingGrip;
            }
            set
            {
                this.statusStrip1.SizingGrip = value;
            }
        }

        /// <summary>
        /// Gets or sets the minimum allocation size to display.
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        Description( "The minimum size of the allocation to be displayed." ),
        DefaultValue( 0 )]
        public int MinSize
        {
            get
            {
                return m_minSize;
            }
            set
            {
                if ( value < 0 )
                {
                    value = 0;
                }
                else if ( value > m_maxSize )
                {
                    value = m_maxSize;
                }

                if ( m_minSize != value )
                {
                    m_minSize = value;

                    if ( m_minSize.ToString() != this.minSizeToolStripTextBox.Text )
                    {
                        this.minSizeToolStripTextBox.Text = m_minSize.ToString();
                    }

                    if ( !this.DesignMode )
                    {
                        this.RefreshNeeded = true;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the maximum allocation size to display.
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        Description( "The maximum size of the allocation to be displayed." ),
        DefaultValue( 999999999 )]
        public int MaxSize
        {
            get
            {
                return m_maxSize;
            }
            set
            {
                if ( value < m_minSize )
                {
                    value = m_minSize;
                }
                else if ( value < 0 )
                {
                    value = 0;
                }
                else if ( value > 999999999 )
                {
                    value = 999999999;
                }

                if ( m_maxSize != value )
                {
                    m_maxSize = value;

                    if ( m_maxSize.ToString() != this.maxSizeToolStripTextBox.Text )
                    {
                        this.maxSizeToolStripTextBox.Text = m_minSize.ToString();
                    }

                    if ( !this.DesignMode )
                    {
                        this.RefreshNeeded = true;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the <see cref="DisplayType"/>.
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        Description( "How to arrange the allocation nodes." ),
        DefaultValue( "DisplayType.MemoryType" )]
        public DisplayType GroupBy
        {
            get
            {
                return m_groupBy;
            }
            set
            {
                if ( m_groupBy != value )
                {
                    m_groupBy = value;

                    if ( !this.DesignMode )
                    {
                        this.RefreshNeeded = true;                        
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets whether to display the allocations under the Unassigned stack name.
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        Description( "Whether to show the Unassigned Stack." ),
        DefaultValue( true )]
        public bool ShowUnassignedStack
        {
            get
            {
                return m_showUnassignedStack;
            }
            set
            {
                if ( m_showUnassignedStack != value )
                {
                    m_showUnassignedStack = value;

                    if ( !this.DesignMode )
                    {
                        m_stackMutex.WaitOne();

                        DetermineVisibleStacks();

                        m_stackMutex.ReleaseMutex();

                        this.RefreshNeeded = true;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets whether to display allocations with Native datatypes.
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        Description( "Whether to show Native datatypes." ),
        DefaultValue( true )]
        public bool ShowNativeDatatypes
        {
            get
            {
                return m_showNativeDatatypes;
            }
            set
            {
                if ( m_showNativeDatatypes != value )
                {
                    m_showNativeDatatypes = value;

                    if ( !this.DesignMode )
                    {
                        m_datatypeMutex.WaitOne();

                        // collect a list of type names to change the visibility of
                        List<uint> nativeTypes = new List<uint>();
                        foreach ( KeyValuePair<uint, bool> pair in m_datatypeVisibility )
                        {
                            string name;
                            if ( (pair.Value != m_showNativeDatatypes) && Allocation.DataTypeDictionary.TryGetValue( pair.Key, out name ) )
                            {
                                if ( !name.StartsWith( "struct" ) && !name.StartsWith( "class" ) )
                                {
                                    nativeTypes.Add( pair.Key );
                                }
                            }
                        }

                        if ( nativeTypes.Count > 0 )
                        {
                            foreach ( uint nativeType in nativeTypes )
                            {
                                m_datatypeVisibility[nativeType] = m_showNativeDatatypes;
                            }
                        }

                        this.RefreshNeeded = true;

                        DetermineVisibleDatatypes();

                        m_datatypeMutex.ReleaseMutex();
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets whether to display allocations that are Classes.
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        Description( "Whether to show Classes." ),
        DefaultValue( true )]
        public bool ShowClassDatatypes
        {
            get
            {
                return m_showClassDatatypes;
            }
            set
            {
                if ( m_showClassDatatypes != value )
                {
                    m_showClassDatatypes = value;

                    if ( !this.DesignMode )
                    {
                        m_datatypeMutex.WaitOne();

                        // collect a list of type names to change the visibility of
                        List<uint> classTypes = new List<uint>();
                        foreach ( KeyValuePair<uint, bool> pair in m_datatypeVisibility )
                        {
                            string name;
                            if ( (pair.Value != m_showClassDatatypes) && Allocation.DataTypeDictionary.TryGetValue( pair.Key, out name ) )
                            {
                                if ( name.StartsWith( "class" ) )
                                {
                                    classTypes.Add( pair.Key );
                                }
                            }
                        }

                        if ( classTypes.Count > 0 )
                        {
                            foreach ( uint classType in classTypes )
                            {
                                m_datatypeVisibility[classType] = m_showClassDatatypes;
                            }
                        }

                        this.RefreshNeeded = true;

                        DetermineVisibleDatatypes();

                        m_datatypeMutex.ReleaseMutex();
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets whether to display allocations that are Structures.
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        Description( "Whether to show Structures." ),
        DefaultValue( true )]
        public bool ShowStructureDatatypes
        {
            get
            {
                return m_showStructDatatypes;
            }
            set
            {
                if ( m_showStructDatatypes != value )
                {
                    m_showStructDatatypes = value;

                    if ( !this.DesignMode )
                    {
                        m_datatypeMutex.WaitOne();

                        // collect a list of type names to change the visibility of
                        List<uint> structTypes = new List<uint>();
                        foreach ( KeyValuePair<uint, bool> pair in m_datatypeVisibility )
                        {
                            string name;
                            if ( (pair.Value != m_showStructDatatypes) && Allocation.DataTypeDictionary.TryGetValue( pair.Key, out name ) )
                            {
                                if ( name.StartsWith( "struct" ) )
                                {
                                    structTypes.Add( pair.Key );
                                }
                            }
                        }

                        if ( structTypes.Count > 0 )
                        {
                            foreach ( uint structType in structTypes )
                            {
                                m_datatypeVisibility[structType] = m_showStructDatatypes;
                            }
                        }

                        this.RefreshNeeded = true;

                        DetermineVisibleDatatypes();

                        m_datatypeMutex.ReleaseMutex();
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets whether to display allocations of Unknown datatypes.
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        Description( "Whether to show Unknown datatypes." ),
        DefaultValue( true )]
        public bool ShowUnknownDatatypes
        {
            get
            {
                return m_showUnknownDatatypes;
            }
            set
            {
                if ( m_showUnknownDatatypes != value )
                {
                    m_showUnknownDatatypes = value;

                    if ( !this.DesignMode )
                    {
                        m_datatypeMutex.WaitOne();

                        DetermineVisibleDatatypes();

                        m_datatypeMutex.ReleaseMutex();

                        this.RefreshNeeded = true;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets whether to display allocations made with the Game Virtual memory type.
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        Description( "Whether to show Game Virtual memory." ),
        DefaultValue( true )]
        public bool ShowGameVirtualMemory
        {
            get
            {
                return m_memoryTypeVisibility[(int)Allocation.MemoryTypeIndex.GameVirtual];
            }
            set
            {
                if ( m_memoryTypeVisibility[(int)Allocation.MemoryTypeIndex.GameVirtual] != value )
                {
                    m_memoryTypeVisibility[(int)Allocation.MemoryTypeIndex.GameVirtual] = value;

                    if ( !this.DesignMode )
                    {
                        this.RefreshNeeded = true;                        
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets whether to display allocations made with the Game Resource Physical memory type.
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        Description( "Whether to show Game Physical memory." ),
        DefaultValue( true )]
        public bool ShowGamePhysicalMemory
        {
            get
            {
                return m_memoryTypeVisibility[(int)Allocation.MemoryTypeIndex.GamePhysical];
            }
            set
            {
                if ( m_memoryTypeVisibility[(int)Allocation.MemoryTypeIndex.GamePhysical] != value )
                {
                    m_memoryTypeVisibility[(int)Allocation.MemoryTypeIndex.GamePhysical] = value;

                    if ( !this.DesignMode )
                    {
                        this.RefreshNeeded = true;                        
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets whether to display allocations made with the Game Resource Physical memory type.
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        Description( "Whether to show Resource Physical memory." ),
        DefaultValue( true )]
        public bool ShowResourcePhysicalMemory
        {
            get
            {
                return m_memoryTypeVisibility[(int)Allocation.MemoryTypeIndex.ResourcePhysical];
            }
            set
            {
                if ( m_memoryTypeVisibility[(int)Allocation.MemoryTypeIndex.ResourcePhysical] != value )
                {
                    m_memoryTypeVisibility[(int)Allocation.MemoryTypeIndex.ResourcePhysical] = value;

                    if ( !this.DesignMode )
                    {
                        this.RefreshNeeded = true;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets whether to display allocations made with the Resource Virtual memory type.
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        Description( "Whether to show Resource Virtual memory." ),
        DefaultValue( true )]
        public bool ShowResourceVirtualMemory
        {
            get
            {
                return m_memoryTypeVisibility[(int)Allocation.MemoryTypeIndex.ResourceVirtual];
            }
            set
            {
                if ( m_memoryTypeVisibility[(int)Allocation.MemoryTypeIndex.ResourceVirtual] != value )
                {
                    m_memoryTypeVisibility[(int)Allocation.MemoryTypeIndex.ResourceVirtual] = value;

                    if ( !this.DesignMode )
                    {
                        this.RefreshNeeded = true;                        
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets whether to display allocations of Undetermined memory type.
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        Description( "Whether to show Undetermined memory." ),
        DefaultValue( true )]
        public bool ShowUndeterminedMemory
        {
            get
            {
                return m_memoryTypeVisibility[(int)Allocation.MemoryTypeIndex.Undetermined];
            }
            set
            {
                if ( m_memoryTypeVisibility[(int)Allocation.MemoryTypeIndex.Undetermined] != value )
                {
                    m_memoryTypeVisibility[(int)Allocation.MemoryTypeIndex.Undetermined] = value;

                    if ( !this.DesignMode )
                    {
                        this.RefreshNeeded = true;                        
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets whether to display allocations made with the Console Specific memory type, typically Xenon External.
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        Description( "Whether to show Console Specific memory." ),
        DefaultValue( true )]
        public bool ShowConsoleSpecificMemory
        {
            get
            {
                return m_memoryTypeVisibility[(int)Allocation.MemoryTypeIndex.ConsoleSpecific];
            }
            set
            {
                if ( m_memoryTypeVisibility[(int)Allocation.MemoryTypeIndex.ConsoleSpecific] != value )
                {
                    m_memoryTypeVisibility[(int)Allocation.MemoryTypeIndex.ConsoleSpecific] = value;

                    if ( !this.DesignMode )
                    {
                        this.RefreshNeeded = true;                        
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets whether to display allocations or deallocations.
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        Description( "Whether to show Allocations or Deallocations." ),
        DefaultValue( true )]
        public bool ShowAllocations
        {
            get
            {
                return m_showAllocations;
            }
            set
            {
                if ( m_showAllocations != value )
                {
                    m_showAllocations = value;

                    if ( !this.DesignMode )
                    {
                        this.RefreshNeeded = true;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the <see cref="ReportItem"/>.
        /// </summary>
        [Browsable( false )]
        public ReportItem Report
        {
            get
            {
                return m_report;
            }
            set
            {
                if ( m_report != value )
                {
                    m_report = value;
                    this.reportToolStripComboBox.SelectedItem = value;

                    this.RefreshNeeded = true;
                }
            }
        }

        /// <summary>
        /// Gets or sets the visibility of Stack names using the <see cref="SelectedStacks"/> enumeration.
        /// </summary>
        [Browsable( false )]
        public SelectedStacks VisibleStacks
        {
            get
            {
                return m_selectedStacks;
            }
            set
            {
                bool stacksChanged;
                bool unassignedStackChanged;
                SetSelectedStacks( value, out stacksChanged, out unassignedStackChanged );
            }
        }

        /// <summary>
        /// Gets or sets which Stack types to be displayed.  Everything else is hidden.
        /// </summary>
        [Browsable( false )]
        public List<string> VisibleStackTypeNames
        {
            get
            {
                List<string> stackTypes = new List<string>();
                if ( this.ShowUnassignedStack )
                {
                    stackTypes.Add( "unassigned" );
                }

                return stackTypes;
            }
            set
            {
                this.ShowUnassignedStack = value.Contains( "unassigned" );
            }
        }

        /// <summary>
        /// Gets or sets which individual Stack hash keys to be displayed.  Everything else is hidden.
        /// </summary>
        [Browsable( false )]
        public List<uint> VisibleStackHashKeys
        {
            get
            {
                m_stackMutex.WaitOne();

                List<uint> stackKeys = new List<uint>();
                foreach ( KeyValuePair<uint, bool> pair in m_stackVisibility )
                {
                    if ( pair.Value )
                    {
                        stackKeys.Add( pair.Key );
                    }
                }

                m_stackMutex.ReleaseMutex();

                return stackKeys;
            }
            set
            {
                m_stackMutex.WaitOne();

                // first, set all invisible.
                this.VisibleStacks = SelectedStacks.None;

                foreach ( uint stackKey in value )
                {
                    m_stackVisibility[stackKey] = true;
                }

                DetermineVisibleStacks();

                m_stackMutex.ReleaseMutex();
            }
        }

        /// <summary>
        /// Gets or sets the visibility of Datatypes using the <see cref="SelectedDatatypes"/> enumeration.
        /// </summary>
        [Browsable( false )]
        public SelectedDatatypes VisibleDatatypes
        {
            get
            {
                return m_selectedDatatypes;
            }
            set
            {
                bool selectedDatatypesChanged;
                bool nativeDatatypesChanged;
                bool classesChanged;
                bool structuresChanged;
                bool unknownDatatypesChanged;
                SetSelectedDatatypes( value, out selectedDatatypesChanged,
                    out nativeDatatypesChanged, out classesChanged, out structuresChanged, out unknownDatatypesChanged );
            }
        }

        /// <summary>
        /// Gets or sets which Datatype types to be displayed.  Everything else is hidden.
        /// </summary>
        [Browsable( false )]
        public List<string> VisibleDatatypesTypeNames
        {
            get
            {
                List<string> datatypeTypes = new List<string>();

                if ( this.ShowNativeDatatypes )
                {
                    datatypeTypes.Add( "native" );
                }

                if ( this.ShowClassDatatypes )
                {
                    datatypeTypes.Add( "class" );
                }

                if ( this.ShowStructureDatatypes )
                {
                    datatypeTypes.Add( "struct" );
                }

                if ( this.ShowUnknownDatatypes )
                {
                    datatypeTypes.Add( "unknown" );
                }

                return datatypeTypes;
            }
            set
            {
                this.ShowNativeDatatypes = value.Contains( "native" );
                this.ShowClassDatatypes = value.Contains( "class" );
                this.ShowStructureDatatypes = value.Contains( "struct" );
                this.ShowUnknownDatatypes = value.Contains( "unknown" );
            }
        }

        /// <summary>
        /// Gets or sets which individual Datatype hash keys to be displayed.  Everything else is hidden.
        /// </summary>
        [Browsable( false )]
        public List<uint> VisibleDatatypeHashKeys
        {
            get
            {
                m_datatypeMutex.WaitOne();

                List<uint> datatypeKeys = new List<uint>();
                foreach ( KeyValuePair<uint, bool> pair in m_datatypeVisibility )
                {
                    if ( pair.Value )
                    {
                        datatypeKeys.Add( pair.Key );
                    }
                }

                m_datatypeMutex.ReleaseMutex();

                return datatypeKeys;
            }
            set
            {
                m_datatypeMutex.WaitOne();

                // first, set all invisible.
                this.VisibleDatatypes = SelectedDatatypes.None;

                foreach ( uint datatypeKey in value )
                {
                    m_datatypeVisibility[datatypeKey] = true;
                }

                m_datatypeMutex.ReleaseMutex();
            }
        }

        /// <summary>
        /// Gets or sets the visibility of Memory Types using the <see cref="SelectedMemoryTypes"/> enumeration.
        /// </summary>
        [Browsable( false )]
        public SelectedMemoryTypes VisibleMemoryTypes
        {
            get
            {
                return m_selectedMemoryTypes;
            }
            set
            {
                bool gameVirtualChanged;
                bool gamePhysicalChanged;
                bool resourceVirtualChanged;
                bool resourcePhysicalChanged;
                bool undeterminedChanged;
                bool consoleSpecificChanged;
                SetSelectedMemoryTypes( value, out gameVirtualChanged, out gamePhysicalChanged,
                    out resourceVirtualChanged, out resourcePhysicalChanged,
                    out undeterminedChanged, out consoleSpecificChanged );
            }
        }

        /// <summary>
        /// Gets or sets which Memory Types to be displayed.  Everything else is hidden.
        /// </summary>
        [Browsable( false )]
        public List<string> VisibleMemoryTypeNames
        {
            get
            {
                List<string> memoryTypes = new List<string>();
                
                if ( this.ShowGameVirtualMemory )
                {
                    memoryTypes.Add( "game_virtual" );
                }

                if ( this.ShowGamePhysicalMemory )
                {
                    memoryTypes.Add( "game_physical" );
                }

                if ( this.ShowResourceVirtualMemory )
                {
                    memoryTypes.Add( "resource_virtual" );
                }

                if ( this.ShowResourcePhysicalMemory )
                {
                    memoryTypes.Add( "resource_physical" );
                }

                if ( this.ShowUndeterminedMemory )
                {
                    memoryTypes.Add( "undetermined" );
                }

                if ( this.ShowConsoleSpecificMemory )
                {
                    memoryTypes.Add( "console_specific" );
                }

                return memoryTypes;
            }
            set
            {
                this.ShowGameVirtualMemory = value.Contains( "game_virtual" );
                this.ShowGamePhysicalMemory = value.Contains( "game_physical" );
                this.ShowResourceVirtualMemory = value.Contains( "resource_virtual" );
                this.ShowResourcePhysicalMemory = value.Contains( "resource_physical" );
                this.ShowUndeterminedMemory = value.Contains( "undetermined" );
                this.ShowConsoleSpecificMemory = value.Contains( "console_specific" );
            }
        }

        /// <summary>
        /// Gets or sets whether refreshing a report is threaded.
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        Description( "Enables or disables threading during the update." ),
        DefaultValue( false )]
        public bool ThreadingEnabled
        {
            get
            {
                return m_threadingEnabled;
            }
            set
            {
                m_threadingEnabled = value;
            }
        }

        /// <summary>
        /// Gets or sets whether multiple rows can be selected, typically for copying to the <see cref="Clipboard"/>.
        /// </summary>
        /// <remarks>
        /// Enabling this operation impacts performance negatively.  It is recommended that it be turned off immediately
        /// after completing whatever operation for which it was enabled.
        /// </remarks>
        [Browsable( true ),
        Category( "Misc" ),
        Description( "Allow multiple row selection." )]
        public bool MultiSelect
        {
            get
            {
                return this.glacialTreeList1.MultiSelect;
            }
            set
            {
                this.glacialTreeList1.MultiSelect = value;
            }
        }

        /// <summary>
        /// Gets or sets the Enabled property of the Generate Report button.
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        Description( "Whether to enable the Generate Report button." ),
        DefaultValue( true )]
        public bool GenerateReportButtonEnabled
        {
            get
            {
                return this.generateReportToolStripButton.Enabled;
            }
            set
            {
                this.generateReportToolStripButton.Enabled = value;
            }
        }

        /// <summary>
        /// Gets or sets the Enabled property of the Refresh button.  
        /// Will only be enabled if we have a report selected and/or have changed an option since the last Refresh.
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        Description( "Whether to enable the Refresh button." ),
        DefaultValue( true )]
        public bool RefeshButtonEnabled
        {
            get
            {
                return this.refreshToolStripButton.Enabled;
            }
            set
            {
                m_allowRefresh = value;

                if ( this.DesignMode )
                {
                    this.refreshToolStripButton.Enabled = value;
                }
                else
                {
                    this.refreshToolStripButton.Enabled = m_allowRefresh && m_refreshNeeded;
                }
            }
        }

        /// <summary>
        /// Private property to aid in setting the Enabled property of the Refresh button properly.
        /// </summary>
        [Browsable( false )]
        private bool RefreshNeeded
        {
            set
            {
                m_refreshNeeded = value && (m_report != null);

                this.refreshToolStripButton.Enabled = m_allowRefresh && m_refreshNeeded;
            }
        }
        #endregion

        #region Events
        /// <summary>
        /// Signals that a report is about to begin it's refresh.
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        Description( "When populating the list, informs the UI how many steps it will take." )]
        public event UpdateBeginEventHandler UpdateBegin;

        /// <summary>
        /// Signals that one step of the refresh is complete.
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        Description( "When populating the list, informs the UI when a portion of the work has completed." )]
        public event UpdateStepEventHandler UpdateStep;

        /// <summary>
        /// Signals that the entire refresh is complete
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        Description( "When populating the list, informs the UI when all the work is completed." )]
        public event UpdateCompleteEventHandler UpdateComplete;

        /// <summary>
        /// Occurs when <see cref="MinSize"/> is changed.
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        Description( "Occurs when the Min Size is changed." )]
        public event EventHandler MinSizeChanged;

        /// <summary>
        /// Occurs when <see cref="MaxSize"/> is changed.
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        Description( "Occurs when the Max Size is changed." )]
        public event EventHandler MaxSizeChanged;

        /// <summary>
        /// Occurs when <see cref="GroupBy"/> is changed.
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        Description( "Occurs when the DisplayType is changed." )]
        public event EventHandler GroupByChanged;

        /// <summary>
        /// Occurs when <see cref="ShowUnassignedStack"/> is changed.
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        Description( "Occurs when the visibility of the Unassigned Stack is changed." )]
        public event EventHandler ShowUnassignedStackChanged;

        /// <summary>
        /// Occurs when any of the individual Stack names' visiblity are changed.
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        Description( "Occurs when the visibility of any of the Stacks is changed." )]
        public event EventHandler SelectedStacksChanged;

        /// <summary>
        /// Occurs when <see cref="ShowNativeDatatypes"/> is changed.
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        Description( "Occurs when the visibility Native datatypes is changed." )]
        public event EventHandler ShowNativeDatatypesChanged;

        /// <summary>
        /// Occurs when <see cref="ShowClassDatatypes"/> is changed.
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        Description( "Occurs when the visibility of Class datatypes is changed." )]
        public event EventHandler ShowClassDatatypesChanged;

        /// <summary>
        /// Occurs when <see cref="ShowStructureDatatypes"/> is changed.
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        Description( "Occurs when the visibility of Structure datatypes is changed." )]
        public event EventHandler ShowStructureDatatypesChanged;

        /// <summary>
        /// Occurs when <see cref="ShowUnknownDatatypes"/> is changed.
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        Description( "Occurs when the visibility of the Unknown Datatype is changed." )]
        public event EventHandler ShowUnknownDatatypesChanged;

        /// <summary>
        /// Occurs when any of the individual Datatypes' visiblity are changed.
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        Description( "Occurs when the visibility of any of the Datatypes is changed." )]
        public event EventHandler SelectedDatatypesChanged;

        /// <summary>
        /// Occurs when <see cref="ShowGameVirtualMemory"/> is changed.
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        Description( "Occurs when the visibility of Game Virtual memory is changed." )]
        public event EventHandler ShowGameVirtualMemoryChanged;

        /// <summary>
        /// Occurs when <see cref="ShowGamePhysicalMemory"/> is changed.
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        Description( "Occurs when the visibility of Game Physical memory is changed." )]
        public event EventHandler ShowGamePhysicalMemoryChanged;

        /// <summary>
        /// Occurs when <see cref="ShowResourcePhysicalMemory"/> is changed.
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        Description( "Occurs when the visibility of Resource Physical memory is changed." )]
        public event EventHandler ShowResourcePhysicalMemoryChanged;
        
        /// <summary>
        /// Occurs when <see cref="ShowResourceVirtualMemory"/> is changed.
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        Description( "Occurs when the visibility of Resource Virtual memory is changed." )]
        public event EventHandler ShowResourceVirtualMemoryChanged;

        /// <summary>
        /// Occurs when <see cref="ShowUndeterminedMemory"/> is changed.
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        Description( "Occurs when the visibility of Undetermined memory is changed." )]
        public event EventHandler ShowUndeterminedMemoryChanged;

        /// <summary>
        /// Occurs when <see cref="ShowConsoleSpecificMemory"/> is changed.
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        Description( "Occurs when the visibility of Console Specific memory is changed." )]
        public event EventHandler ShowConsoleSpecificMemoryChanged;

        /// <summary>
        /// Occurs when <see cref="ShowAllocations"/> is changed.
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        Description( "Occurs when the displaying Allocations/Deallocations is changed." )]
        public event EventHandler ShowAllocationsChanged;

        /// <summary>
        /// Occurs during a refresh or copy operation and the system runs out of memory.
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        Description( "Occurs on an OutOfMemoryException error." )]
        public event ErrorEventHandler OutOfMemoryError;
        #endregion

        #region Variables
        private Control m_parentControl = null;

        private TrackerComponent m_trackerComponent;
        private ReportChangedEventHandler m_reportAddedEventHandler;
        private ReportChangedEventHandler m_reportRemovedEventHandler;
        private StackChangedEventHandler m_stackAddedEventHandler;
        private DatatypeAddedEventHandler m_datatypeAddedEventHandler;

        private bool m_threadingEnabled = false;
        private bool m_refreshNeeded = false;
        private bool m_allowRefresh = true;

        private Exception m_refreshException = null;

        private DisplayType m_groupBy = DisplayType.Order;
        private bool m_showAllocations = true;
        private bool m_showUnassignedStack = true;
        private bool m_showNativeDatatypes = true;
        private bool m_showClassDatatypes = true;
        private bool m_showStructDatatypes = true;
        private bool m_showUnknownDatatypes = true;        

        private ReportItem m_report = null;
        private int m_minSize = 0;
        private int m_maxSize = 999999999;

        private bool m_reportGenerated = false;

        private SelectedStacks m_selectedStacks = SelectedStacks.All;
        private SelectedDatatypes m_selectedDatatypes = SelectedDatatypes.All;
        private SelectedMemoryTypes m_selectedMemoryTypes = SelectedMemoryTypes.All;

        private Dictionary<uint, bool> m_stackVisibility = new Dictionary<uint, bool>();
        private Dictionary<uint, bool> m_datatypeVisibility = new Dictionary<uint, bool>();
        private bool[] m_memoryTypeVisibility = new bool[(int)Allocation.MemoryTypeIndex.NumIndexes];

        private Mutex m_stackMutex = new Mutex();
        private Mutex m_datatypeMutex = new Mutex();

        private GTLTreeNode m_currentTreeNode = null;
        private Dictionary<string, GTLTreeNode> m_stackNodeDictionary = new Dictionary<string, GTLTreeNode>();
        private Dictionary<uint, GTLTreeNode> m_allocationNodeDictionary = new Dictionary<uint, GTLTreeNode>();

        private List<GTLTreeNode> m_nodes = new List<GTLTreeNode>();
        #endregion

        #region Public Functions
        /// <summary>
        /// Refreshes the <see cref="GlacialTreeList"/> based on the current settings.
        /// </summary>
        public virtual void FillData()
        {
            this.toolStrip1.Enabled = false;            

            this.glacialTreeList1.Nodes.Clear();

            if ( !m_threadingEnabled )
            {
                OnUpdateBegin( 100, m_reportGenerated );
                backgroundWorker_DoWork( this, new DoWorkEventArgs( null ) );
                backgroundWorker_RunWorkerCompleted( this, 
                    new RunWorkerCompletedEventArgs( (m_report == null) ? 0 : 1, null, m_refreshException != null ) );
            }
            else
            {
                backgroundWorker.RunWorkerAsync();
                OnUpdateBegin( 100, m_reportGenerated );
            }
        }

        /// <summary>
        /// Refreshes the <see cref="GlacialTreeList"/> based on the current settings, but using the given <see cref="ReportItem"/>.  Subsequent
        /// Update messages will indicate that we are generating a report.
        /// </summary>
        /// <param name="report">The <see cref="ReportItem"/> to use.</param>
        public virtual void FillData( ReportItem report )
        {
            this.Report = report;

            m_reportGenerated = true;

            FillData();
        }
        #endregion

        #region Protected Functions
        protected virtual int FileOrderedData()
        {
            if ( m_report == null )
            {
                return 0;
            }

            m_trackerComponent.LockForUpdate = true;

            m_stackNodeDictionary.Clear();
            m_allocationNodeDictionary.Clear();

            m_currentTreeNode = null;
            m_stackNodeDictionary.Add( Allocation.Unassigned, null );

            bool showAllocations = m_report.TrackedDeallocations ? m_showAllocations : true;

            try
            {
                float multiplier = 100.0f / (float)(m_report.Index + 1);

                List<TrackerAction> trackerActions = m_trackerComponent.Actions;
                for ( int i = 0; i <= m_report.Index; ++i )
                {
                    TrackerAction action = trackerActions[i];

                    switch ( action.TheAction )
                    {
                        case TrackerAction.Action.PushStack:
                            {
                                PushStackAction stackAction = action as PushStackAction;
                                PushStackNode( stackAction.Name, i );

                                m_stackNodeDictionary.Add( stackAction.Name + i.ToString(), m_currentTreeNode );
                            }
                            break;
                        case TrackerAction.Action.PopStack:
                            {
                                PopStackNode();
                            }
                            break;
                        case TrackerAction.Action.TallyAllocation:
                            {
                                if ( showAllocations )
                                {
                                    TallyAllocationAction tallyAction = action as TallyAllocationAction;
                                    TallyAllocation( tallyAction.TheAllocation, i );
                                }
                            }
                            break;
                        case TrackerAction.Action.UntallyAllocation:
                            {
                                UntallyAllocationAction untallyAction = action as UntallyAllocationAction;
                                if ( showAllocations )
                                {
                                    UntallyAllocation( untallyAction.TheAllocation );
                                }
                                else
                                {
                                    TallyAllocation( untallyAction.TheAllocation, i );
                                }
                            }
                            break;
                        default:
                            break;
                    }

                    int percentComplete = (int)((float)i * multiplier);
                    if ( m_threadingEnabled )
                    {
                        backgroundWorker.ReportProgress( percentComplete );
                    }
                    else
                    {
                        OnUpdateStep( percentComplete, m_reportGenerated );
                    }
                }
            }
            catch ( OutOfMemoryException e )
            {
                m_currentTreeNode = null;
                m_stackNodeDictionary.Clear();

                m_refreshException = e;
                if ( m_threadingEnabled )
                {
                    backgroundWorker.CancelAsync();
                }
            }
            finally
            {
                m_trackerComponent.LockForUpdate = false;
            }

            return 1;
        }

        protected virtual int FillStackData()
        {
            if ( m_report == null )
            {
                return 0;
            }

            m_trackerComponent.LockForUpdate = true;

            m_stackNodeDictionary.Clear();
            m_allocationNodeDictionary.Clear();

            m_currentTreeNode = null;
            m_stackNodeDictionary.Add( Allocation.Unassigned, null );

            bool showAllocations = m_report.TrackedDeallocations ? m_showAllocations : true;

            try
            {
                float multiplier = 100.0f / (float)(m_report.Index + 1);

                List<TrackerAction> trackerActions = m_trackerComponent.Actions;
                for ( int i = 0; i <= m_report.Index; ++i )
                {
                    TrackerAction action = trackerActions[i];

                    switch ( action.TheAction )
                    {
                        case TrackerAction.Action.TallyAllocation:
                            {
                                if ( showAllocations )
                                {
                                    TallyAllocationAction tallyAction = action as TallyAllocationAction;
                                    TallyAllocation( tallyAction.TheAllocation, i );
                                }
                            }
                            break;
                        case TrackerAction.Action.UntallyAllocation:
                            {
                                UntallyAllocationAction untallyAction = action as UntallyAllocationAction;
                                if ( showAllocations )
                                {
                                    UntallyAllocation( untallyAction.TheAllocation );
                                }
                                else
                                {
                                    TallyAllocation( untallyAction.TheAllocation, i );
                                }
                            }
                            break;
                        default:
                            break;
                    }

                    int percentComplete = (int)((float)i * multiplier);
                    if ( m_threadingEnabled )
                    {
                        backgroundWorker.ReportProgress( percentComplete );
                    }
                    else
                    {
                        OnUpdateStep( percentComplete, m_reportGenerated );
                    }
                }
            }
            catch ( OutOfMemoryException e )
            {
                m_currentTreeNode = null;
                m_stackNodeDictionary.Clear();

                m_refreshException = e;

                if ( m_threadingEnabled )
                {
                    backgroundWorker.CancelAsync();
                }
            }
            finally
            {
                m_trackerComponent.LockForUpdate = false;
            }

            return 1;
        }

        protected virtual void OnUpdateBegin( int numSteps, bool generatingReport )
        {
            if ( this.UpdateBegin != null )
            {
                UpdateBeginEventArgs e = new UpdateBeginEventArgs( numSteps, generatingReport );
                this.UpdateBegin( this, e );
            }
        }

        protected virtual void OnUpdateStep( int stepNumber, bool generatingReport )
        {
            if ( this.UpdateStep != null )
            {
                UpdateStepEventArgs e = new UpdateStepEventArgs( stepNumber, generatingReport );
                this.UpdateStep( this, e );
            }
        }

        protected virtual void OnUpdateComplete( ReportItem reportGenerated, bool succes )
        {
            if ( this.UpdateComplete != null )
            {
                UpdateCompleteEventArgs e = new UpdateCompleteEventArgs( reportGenerated, succes );
                this.UpdateComplete( this, e );
            }
        }

        protected virtual void OnMinSizeChanged()
        {
            if ( this.MinSizeChanged != null )
            {
                this.MinSizeChanged( this, new EventArgs() );
            }
        }

        protected virtual void OnMaxSizeChanged()
        {
            if ( this.MaxSizeChanged != null )
            {
                this.MaxSizeChanged( this, new EventArgs() );
            }
        }

        protected virtual void OnGroupByChanged()
        {
            if ( this.GroupByChanged != null )
            {
                this.GroupByChanged( this, new EventArgs() );
            }
        }

        protected virtual void OnShowUnassignedStackChanged()
        {
            if ( this.ShowUnassignedStackChanged != null )
            {
                this.ShowUnassignedStackChanged( this, new EventArgs() );
            }
        }

        protected virtual void OnSelectedStacksChanged()
        {
            if ( this.SelectedStacksChanged != null )
            {
                this.SelectedStacksChanged( this, new EventArgs() );
            }
        }

        protected virtual void OnShowNativeDatatypesChanged()
        {
            if ( this.ShowNativeDatatypesChanged != null )
            {
                this.ShowNativeDatatypesChanged( this, new EventArgs() );
            }
        }

        protected virtual void OnShowClassDatatypesChanged()
        {
            if ( this.ShowClassDatatypesChanged != null )
            {
                this.ShowClassDatatypesChanged( this, new EventArgs() );
            }
        }

        protected virtual void OnShowStructureDatatypesChanged()
        {
            if ( this.ShowStructureDatatypesChanged != null )
            {
                this.ShowStructureDatatypesChanged( this, new EventArgs() );
            }
        }

        protected virtual void OnShowUnknownDatatypesChanged()
        {
            if ( this.ShowUnknownDatatypesChanged != null )
            {
                this.ShowUnknownDatatypesChanged( this, new EventArgs() );
            }
        }

        protected virtual void OnSelectedDatatypesChanged()
        {
            if ( this.SelectedDatatypesChanged != null )
            {
                this.SelectedDatatypesChanged( this, new EventArgs() );
            }
        }

        protected virtual void OnShowGameVirtualMemoryChanged()
        {
            if ( this.ShowGameVirtualMemoryChanged != null )
            {
                this.ShowGameVirtualMemoryChanged( this, new EventArgs() );
            }
        }

        protected virtual void OnShowGamePhysicalMemoryChanged()
        {
            if ( this.ShowGamePhysicalMemoryChanged != null )
            {
                this.ShowGamePhysicalMemoryChanged( this, new EventArgs() );
            }
        }

        protected virtual void OnShowResourcePhysicalMemoryChanged()
        {
            if ( this.ShowResourcePhysicalMemoryChanged != null )
            {
                this.ShowResourcePhysicalMemoryChanged( this, new EventArgs() );
            }
        }

        protected virtual void OnShowResourceVirtualMemoryChanged()
        {
            if ( this.ShowResourceVirtualMemoryChanged != null )
            {
                this.ShowResourceVirtualMemoryChanged( this, new EventArgs() );
            }
        }

        protected virtual void OnShowUndeterminedMemoryChanged()
        {
            if ( this.ShowUndeterminedMemoryChanged != null )
            {
                this.ShowUndeterminedMemoryChanged( this, new EventArgs() );
            }
        }

        protected virtual void OnShowConsoleSpecificMemoryChanged()
        {
            if ( this.ShowConsoleSpecificMemoryChanged != null )
            {
                this.ShowConsoleSpecificMemoryChanged( this, new EventArgs() );
            }
        }

        protected virtual void OnShowAllocationsChanged()
        {
            if ( this.ShowAllocationsChanged != null )
            {
                this.ShowAllocationsChanged( this, new EventArgs() );
            }
        }

        protected virtual void OnOutOfMemoryError( string message, Exception e )
        {
            if ( this.OutOfMemoryError != null )
            {
                ErrorEventArgs args = new ErrorEventArgs( message, e );
                this.OutOfMemoryError( this, args );
            }
        }
        #endregion

        #region Private Functions
        private void InitializeAdditionalComponents()
        {
            for ( int i = 0; i < m_memoryTypeVisibility.Length; ++i )
            {
                m_memoryTypeVisibility[i] = true;
            }

            this.RefreshNeeded = false;
        }

        private void TallyAllocation( Allocation theAllocation, int index )
        {
            bool showStack = false;
            string[] stacks = theAllocation.Stack;
            if ( stacks[0] == Allocation.Unassigned )
            {
                showStack = this.ShowUnassignedStack;
            }
            else
            {
                foreach ( string stack in stacks )
                {
                    bool vis;
                    if ( m_stackVisibility.TryGetValue( (uint)stack.GetHashCode(), out vis ) && vis )
                    {
                        showStack = true;
                        break;
                    }
                }
            }

            bool showDataType = this.ShowUnknownDatatypes;
            if ( theAllocation.TypeName != Allocation.Unknown )
            {
                bool vis;                
                if ( m_datatypeVisibility.TryGetValue( theAllocation.TypeNameHashCode, out vis ) )
                {
                    showDataType = vis;
                }
            }

            bool showMemoryType = m_memoryTypeVisibility[(int)theAllocation.MemoryType];

            bool showSize = (theAllocation.Size >= m_minSize) && (theAllocation.Size <= m_maxSize);

            if ( showStack && showDataType && showMemoryType && showSize )
            {
                if ( m_groupBy == DisplayType.Order )
                {
                    TallyAllocationNode( theAllocation, index );
                }
                else
                {
                    AddAllocationNode( theAllocation, index );
                }
            }
        }

        private void UntallyAllocation( Allocation theAllocation )
        {
            bool showStack = false;
            string[] stacks = theAllocation.Stack;
            if ( stacks[0] == Allocation.Unassigned )
            {
                showStack = this.ShowUnassignedStack;
            }
            else
            {
                foreach ( string stack in stacks )
                {
                    bool vis;
                    if ( m_stackVisibility.TryGetValue( (uint)stack.GetHashCode(), out vis ) && vis )
                    {
                        showStack = true;
                        break;
                    }
                }
            }

            bool showDataType = this.ShowUnknownDatatypes;
            if ( theAllocation.TypeName != Allocation.Unknown )
            {
                bool vis;
                if ( m_datatypeVisibility.TryGetValue( theAllocation.TypeNameHashCode, out vis ) )
                {
                    showDataType = vis;
                }
            }

            bool showMemoryType = m_memoryTypeVisibility[(int)theAllocation.MemoryType];

            bool showSize = (theAllocation.Size >= m_minSize) && (theAllocation.Size <= m_maxSize);

            if ( showStack && showDataType && showMemoryType && showSize )
            {
                if ( m_groupBy == DisplayType.Order )
                {
                    UntallyAllocationNode( theAllocation );
                }
                else
                {
                    RemoveAllocationNode( theAllocation );
                }
            }
        }

        private void PushStackNode( string name, int index )
        {
            GTLTreeNode treeNode = new GTLTreeNode( name );
            treeNode.SubItems.Add( (index > -1) ? index.ToString() : string.Empty );
            treeNode.SubItems.Add( string.Empty );
            treeNode.SubItems.Add( string.Empty );
            treeNode.SubItems.Add( "0" );

            if ( m_currentTreeNode == null )
            {
                m_nodes.Add( treeNode );
            }
            else
            {
                m_currentTreeNode.Nodes.Add( treeNode );
            }

            m_currentTreeNode = treeNode;
        }

        private void PopStackNode()
        {
            if ( m_currentTreeNode != null )
            {
                GTLTreeNode oldNode = m_currentTreeNode;
                m_currentTreeNode = m_currentTreeNode.ParentNode;

                // remove empty nodes
                if ( oldNode.Nodes.Count == 0 )
                {
                    if ( m_currentTreeNode != null )
                    {
                        m_currentTreeNode.Nodes.Remove( oldNode );
                    }
                    else
                    {
                        m_nodes.Remove( oldNode );
                    }
                }
            }
        }

        private void TallyAllocationNode( Allocation theAllocation, int index )
        {
            GTLTreeNode treeNode = null;
            if ( !m_allocationNodeDictionary.TryGetValue( theAllocation.DecimalAddress, out treeNode ) )
            {
                treeNode = new GTLTreeNode( theAllocation.StackName == Allocation.Unassigned ? Allocation.Unassigned : string.Empty );
                treeNode.SubItems.Add( index.ToString() );
                treeNode.SubItems.Add( theAllocation.TypeName );
                treeNode.SubItems.Add( theAllocation.Address );
                treeNode.SubItems.Add( theAllocation.Size.ToString( "n" ).Replace( ".00", "" ) );
                treeNode.SubItems.Add( theAllocation.FileName );

                m_allocationNodeDictionary.Add( theAllocation.DecimalAddress, treeNode );

                if ( theAllocation.IsArray )
                {
                    foreach ( Allocation a in theAllocation.Items )
                    {
                        GTLTreeNode subTreeNode = new GTLTreeNode( string.Empty );
                        subTreeNode.SubItems.Add( string.Empty );
                        subTreeNode.SubItems.Add( a.TypeName );
                        subTreeNode.SubItems.Add( a.Address );
                        subTreeNode.SubItems.Add( a.Size.ToString( "n" ).Replace( ".00", "" ) );
                        subTreeNode.SubItems.Add( a.FileName );

                        if ( a.FileName != string.Empty )
                        {
                            subTreeNode.SubItems.Add( a.LineNumber.ToString() );
                        }
                        else
                        {
                            subTreeNode.SubItems.Add( string.Empty );
                        }

                        treeNode.Nodes.Add( subTreeNode );
                    }
                }

                if ( theAllocation.FileName != string.Empty )
                {
                    treeNode.SubItems.Add( theAllocation.LineNumber.ToString() );
                }
                else
                {
                    treeNode.SubItems.Add( string.Empty );
                }

                treeNode.SubItems.Add( theAllocation.MemoryTypeName );

                if ( m_currentTreeNode == null )
                {
                    m_nodes.Add( treeNode );
                }
                else
                {
                    m_currentTreeNode.Nodes.Add( treeNode );

                    ComputeTotalSizes( m_currentTreeNode, theAllocation.Size );
                }
            }
            else
            {
                treeNode.SubItems[(int)AllocationColumns.Size].Text = theAllocation.Size.ToString( "n" ).Replace( ".00", "" );
            }
        }

        private void UntallyAllocationNode( Allocation theAllocation )
        {
            GTLTreeNode treeNode;
            if ( m_allocationNodeDictionary.TryGetValue( theAllocation.DecimalAddress, out treeNode ) )
            {
                try
                {
                    if ( treeNode.ParentNode == null )
                    {
                        m_nodes.Remove( treeNode );
                    }
                    else
                    {
                        treeNode.ParentNode.Nodes.Remove( treeNode );

                        ComputeTotalSizes( treeNode.ParentNode, -theAllocation.Size );
                    }
                }
                catch
                {
                    // in case our remove operation fails for some reason
                }

                m_allocationNodeDictionary.Remove( theAllocation.DecimalAddress );
            }
        }

        private void AddAllocationNode( Allocation theAllocation, int index )
        {
            m_currentTreeNode = null;

            GTLTreeNode stackNode;
            if ( m_stackNodeDictionary.TryGetValue( theAllocation.FullStackPath, out stackNode ) )
            {
                m_currentTreeNode = stackNode;
            }
            else
            {
                System.Text.StringBuilder fullStackPath = new System.Text.StringBuilder();
                string[] stacks = theAllocation.Stack;
                if ( stacks[0] != Allocation.Unassigned )
                {
                    for ( int i = stacks.Length - 1; i >= 0; --i )
                    {
                        string stackName = stacks[i];

                        fullStackPath.Append( stackName );
                        fullStackPath.Append( '\\' );

                        if ( m_stackNodeDictionary.TryGetValue( fullStackPath.ToString(), out stackNode ) )
                        {
                            m_currentTreeNode = stackNode;
                        }
                        else
                        {
                            PushStackNode( stackName, -1 );
                            m_stackNodeDictionary.Add( fullStackPath.ToString(), m_currentTreeNode );
                        }
                    }
                }
            }

            TallyAllocationNode( theAllocation, index );
        }

        private void RemoveAllocationNode( Allocation theAllocation )
        {
            UntallyAllocationNode( theAllocation );
        }

        private void ComputeTotalSizes( GTLTreeNode node, Int64 size )
        {
            string strTotal = node.SubItems[(int)AllocationColumns.Size].Text;
            Int64 total = Int64.Parse( strTotal.Replace( ",", "" ) );

            total += size;
            node.SubItems[(int)AllocationColumns.Size].Text = total.ToString( "n" ).Replace( ".00", "" );

            if ( node.ParentNode != null )
            {
                ComputeTotalSizes( node.ParentNode, size );
            }
        }

        private void GetTreeNodeRowText( GTLTreeNode treeNode, StringBuilder text, int depth )
        {
            if ( depth > 0 )
            {
                for ( int i = 0; i < depth; ++i )
                {
                    text.Append( '|' );
                }

                text.Append( "|- " );
            }


            for ( int i = 0; i < treeNode.SubItems.Count; ++i )
            {
                if ( i > 0 )
                {
                    text.Append( '\t' );
                }

                if ( treeNode.SubItems[i].Text == string.Empty )
                {
                    text.Append( '_' );
                }
                else
                {
                    text.Append( treeNode.SubItems[i].Text );
                }
            }
            text.Append( '\n' );

            foreach ( GTLTreeNode subNode in treeNode.Nodes )
            {
                GetTreeNodeRowText( subNode, text, depth + 1 );
            }
        }

        private void DetermineVisibleStacks()
        {
            // determine what stacks we have selected
            int visibleCount = 0;
            foreach ( bool vis in m_stackVisibility.Values )
            {
                if ( vis )
                {
                    ++visibleCount;
                }
            }

            // set selected stacks
            if ( (visibleCount == 0) && !this.ShowUnassignedStack )
            {
                m_selectedStacks = SelectedStacks.None;
            }
            else if ( (visibleCount == m_stackVisibility.Count) && this.ShowUnassignedStack )
            {
                m_selectedStacks = SelectedStacks.All;
            }
            else
            {
                m_selectedStacks = SelectedStacks.Selected;
            }
        }

        private void DetermineVisibleDatatypes()
        {
            // determine what datatypes we have selected
            int visibleCount = 0;
            foreach ( bool vis in m_datatypeVisibility.Values )
            {
                if ( vis )
                {
                    ++visibleCount;
                }
            }

            // set selected datatypes
            if ( (visibleCount == 0) && !this.ShowUnknownDatatypes )
            {
                m_selectedDatatypes = SelectedDatatypes.None;
            }
            else if ( (visibleCount == m_datatypeVisibility.Count) && this.ShowUnknownDatatypes )
            {
                m_selectedDatatypes = SelectedDatatypes.All;
            }
            else
            {
                m_selectedDatatypes = SelectedDatatypes.Selected;
            }
        }

        private void DetermineVisibleMemoryTypes()
        {
            int visibleCount = 0;

            if ( this.ShowGameVirtualMemory )
            {
                ++visibleCount;
            }

            if ( this.ShowGamePhysicalMemory )
            {
                ++visibleCount;
            }

            if ( this.ShowResourcePhysicalMemory )
            {
                ++visibleCount;
            }

            if ( this.ShowResourceVirtualMemory )
            {
                ++visibleCount;
            }

            if ( this.ShowUndeterminedMemory )
            {
                ++visibleCount;
            }

            if ( this.ShowConsoleSpecificMemory )
            {
                ++visibleCount;
            }

            if ( visibleCount == 0 )
            {
                m_selectedMemoryTypes = SelectedMemoryTypes.None;
            }
            else if ( visibleCount == (int)Allocation.MemoryTypeIndex.NumIndexes )
            {
                m_selectedMemoryTypes = SelectedMemoryTypes.All;
            }
            else
            {
                m_selectedMemoryTypes = SelectedMemoryTypes.Selected;
            }
        }

        /// <summary>
        /// Internal function to set the visible stacks.
        /// </summary>
        /// <param name="selectedStacks">The <see cref="SelectedStacks"/> enumeration to set the visibility to.</param>
        /// <param name="stacksChanged">Return if any of the individual stacks' visibility changed.</param>
        /// <param name="unassignedStackChanged">Return if the unassigned stack visibility changed.</param>
        /// <returns><c>true</c> if m_selectedStacks changed, otherwise, <c>false</c>.</returns>
        private bool SetSelectedStacks( SelectedStacks selectedStacks, out bool stacksChanged, out bool unassignedStackChanged )
        {
            stacksChanged = false;
            unassignedStackChanged = false;

            if ( m_selectedStacks != selectedStacks )
            {
                m_selectedStacks = selectedStacks;

                if ( !this.DesignMode )
                {
                    this.RefreshNeeded = true;

                    if ( m_selectedStacks != SelectedStacks.Selected )
                    {
                        bool vis = m_selectedStacks == SelectedStacks.All;

                        m_stackMutex.WaitOne();

                        List<uint> changedStacks = new List<uint>();
                        foreach ( KeyValuePair<uint, bool> pair in m_stackVisibility )
                        {
                            if ( pair.Value != vis )
                            {
                                changedStacks.Add( pair.Key );
                            }
                        }

                        stacksChanged = changedStacks.Count > 0;
                        if ( stacksChanged )
                        {
                            foreach ( uint changedStack in changedStacks )
                            {
                                m_stackVisibility[changedStack] = vis;
                            }
                        }

                        m_stackMutex.ReleaseMutex();

                        unassignedStackChanged = m_showUnassignedStack != vis;
                        if ( unassignedStackChanged )
                        {
                            m_showUnassignedStack = vis;
                        }
                    }
                }

                return true;
            }

            return false;
        }

        /// <summary>
        /// Internal function to set the visible datatypes.
        /// </summary>
        /// <param name="selectedDatatypes">The <see cref="SelectedDatatypes"/> enumeration to set the visibility to.</param>
        /// <param name="selectedDatatypesChanged">Return if any of the individual datatypes' visibility changed.</param>
        /// <param name="nativeDatatypesChanged">Return if the native datatypes visibility changed.</param>
        /// <param name="classesChanged">Return if the classes visibility changed.</param>
        /// <param name="structuresChanged">Return if the structures visibility changed.</param>
        /// <param name="unknownDatatypesChanged">Return if the unknown datatypes visibility changed.</param>
        /// <returns><c>true</c> if m_selectedDatatypes changed, otherwise, <c>false</c>.</returns>
        private bool SetSelectedDatatypes( SelectedDatatypes selectedDatatypes, out bool selectedDatatypesChanged,
            out bool nativeDatatypesChanged, out bool classesChanged, out bool structuresChanged, out bool unknownDatatypesChanged )
        {
            selectedDatatypesChanged = false;
            nativeDatatypesChanged = false;
            classesChanged = false;
            structuresChanged = false;
            unknownDatatypesChanged = false;

            if ( m_selectedDatatypes != selectedDatatypes )
            {
                m_selectedDatatypes = selectedDatatypes;

                if ( !this.DesignMode )
                {
                    this.RefreshNeeded = true;

                    if ( m_selectedDatatypes != SelectedDatatypes.Selected )
                    {
                        bool vis = m_selectedDatatypes == SelectedDatatypes.All;

                        m_datatypeMutex.WaitOne();

                        List<uint> changedDatatypes = new List<uint>();
                        foreach ( KeyValuePair<uint, bool> pair in m_datatypeVisibility )
                        {
                            if ( pair.Value != vis )
                            {
                                changedDatatypes.Add( pair.Key );
                            }
                        }

                        selectedDatatypesChanged = changedDatatypes.Count > 0;
                        if ( selectedDatatypesChanged )
                        {
                            foreach ( uint changedDatatype in changedDatatypes )
                            {
                                m_datatypeVisibility[changedDatatype] = vis;
                            }
                        }

                        m_datatypeMutex.ReleaseMutex();

                        nativeDatatypesChanged = m_showNativeDatatypes != vis;
                        if ( nativeDatatypesChanged )
                        {
                            m_showNativeDatatypes = vis;
                        }

                        classesChanged = m_showClassDatatypes != vis;
                        if ( classesChanged )
                        {
                            m_showClassDatatypes = vis;
                        }

                        structuresChanged = m_showStructDatatypes != vis;
                        if ( structuresChanged )
                        {
                            m_showStructDatatypes = vis;
                        }

                        unknownDatatypesChanged = m_showUnknownDatatypes != vis;
                        if ( unknownDatatypesChanged )
                        {
                            m_showUnknownDatatypes = vis;
                        }
                    }
                }

                return true;
            }

            return false;
        }

        /// <summary>
        /// Internal function for setting the visible memory types.
        /// </summary>
        /// <param name="selectedMemoryTypes">The <see cref="SelectedMemoryTypes"/> enumeration to set the visibility to.</param>
        /// <param name="gameVirtualChanged">Returns if game virtual changed.</param>
        /// <param name="gamePhysicalChanged">Returns if game physical changed.</param>
        /// <param name="resourceVirtualChanged">Returns if resource virtual changed.</param>
        /// <param name="resourcePhysicalChanged">Returns if resource physical changed. </param>
        /// <param name="undeterminedChanged">Returns if undetermined memory changed.</param>
        /// <param name="consoleSpecificChanged">Returns if console specific changed.</param>
        /// <returns><c>true</c> if m_selectedMemoryTypes changed, otherwise, <c>false</c>.</returns>
        private bool SetSelectedMemoryTypes( SelectedMemoryTypes selectedMemoryTypes, 
            out bool gameVirtualChanged, out bool gamePhysicalChanged, 
            out bool resourceVirtualChanged, out bool resourcePhysicalChanged,
            out bool undeterminedChanged, out bool consoleSpecificChanged )
        {
            gameVirtualChanged = false;
            gamePhysicalChanged = false;
            resourceVirtualChanged = false;
            resourcePhysicalChanged = false;
            undeterminedChanged = false;
            consoleSpecificChanged = false;

            if ( m_selectedMemoryTypes != selectedMemoryTypes )
            {
                m_selectedMemoryTypes = selectedMemoryTypes;

                if ( !this.DesignMode )
                {
                    this.RefreshNeeded = true;

                    if ( m_selectedMemoryTypes != SelectedMemoryTypes.Selected )
                    {
                        bool vis = m_selectedMemoryTypes == SelectedMemoryTypes.All;

                        gameVirtualChanged = m_memoryTypeVisibility[(int)Allocation.MemoryTypeIndex.GameVirtual] != vis;
                        if ( gameVirtualChanged )
                        {
                            m_memoryTypeVisibility[(int)Allocation.MemoryTypeIndex.GameVirtual] = vis;
                        }

                        gamePhysicalChanged = m_memoryTypeVisibility[(int)Allocation.MemoryTypeIndex.GamePhysical] != vis;
                        if ( gamePhysicalChanged )
                        {
                            m_memoryTypeVisibility[(int)Allocation.MemoryTypeIndex.GamePhysical] = vis;
                        }

                        resourceVirtualChanged = m_memoryTypeVisibility[(int)Allocation.MemoryTypeIndex.ResourceVirtual] != vis;
                        if ( resourceVirtualChanged )
                        {
                            m_memoryTypeVisibility[(int)Allocation.MemoryTypeIndex.ResourceVirtual] = vis;
                        }

                        resourcePhysicalChanged = m_memoryTypeVisibility[(int)Allocation.MemoryTypeIndex.ResourcePhysical] != vis;
                        if ( resourcePhysicalChanged )
                        {
                            m_memoryTypeVisibility[(int)Allocation.MemoryTypeIndex.ResourcePhysical] = vis;
                        }

                        undeterminedChanged = m_memoryTypeVisibility[(int)Allocation.MemoryTypeIndex.Undetermined] != vis;
                        if ( undeterminedChanged )
                        {
                            m_memoryTypeVisibility[(int)Allocation.MemoryTypeIndex.Undetermined] = vis;
                        }

                        consoleSpecificChanged = m_memoryTypeVisibility[(int)Allocation.MemoryTypeIndex.ConsoleSpecific] != vis;
                        if ( consoleSpecificChanged )
                        {
                            m_memoryTypeVisibility[(int)Allocation.MemoryTypeIndex.ConsoleSpecific] = vis;
                        }
                    }
                }

                return true;
            }

            return false;
        }
        #endregion

        #region TreeList Event Handlers
        private void glacialTreeList1_SelectedIndexChanged( object source, GTSelectionChangedEventArgs e )
        {
            uint selectedSize = 0;
            foreach ( GTLTreeNode node in this.glacialTreeList1.SelectedNodes )
            {
                bool addSize = true;

                // don't include a child node's size if one of it's ancestor nodes is selected.
                GTLTreeNode parentNode = node.ParentNode;
                while ( parentNode != null )
                {
                    if ( this.glacialTreeList1.SelectedNodes.Contains( parentNode ) )
                    {
                        addSize = false;
                        break;
                    }

                    parentNode = parentNode.ParentNode;
                }

                if ( addSize )
                {
                    string sizeString = node.SubItems[(int)AllocationColumns.Size].Text.Replace( ",", "" ).Trim();
                    if ( sizeString != string.Empty )
                    {
                        selectedSize += uint.Parse( sizeString );
                    }
                }
            }

            StringBuilder selectedSizeText = new StringBuilder( "Selected Size:  " );
            selectedSizeText.Append( selectedSize.ToString( "n" ).Replace( ".00", "" ) );
            selectedSizeText.Append( " bytes" );
            this.selectedSizeToolStripStatusLabel.Text = selectedSizeText.ToString();
        }
        #endregion

        #region ContextMenu Event Handlers
        private void copyToolStripMenuItem_Click( object sender, EventArgs e )
        {
            try
            {
                if ( this.glacialTreeList1.SelectedNodes.Count > 0 )
                {
                    StringBuilder text = new StringBuilder();

                    int length = 0;
                    for ( int i = 0; i < this.glacialTreeList1.Columns.Count; ++i )
                    {
                        if ( i > 0 )
                        {
                            text.Append( '\t' );
                            length += 4;
                        }

                        text.Append( this.glacialTreeList1.Columns[i].Text.ToUpper() );
                        length += this.glacialTreeList1.Columns[i].Text.Length;
                    }
                    text.Append( '\n' );

                    for ( int i = 0; i < length; ++i )
                    {
                        text.Append( '-' );
                    }
                    text.Append( '\n' );

                    foreach ( GTLTreeNode treeNode in this.glacialTreeList1.SelectedNodes )
                    {
                        GetTreeNodeRowText( treeNode, text, 0 );
                    }

                    Clipboard.SetText( text.ToString() );
                }
            }
            catch ( OutOfMemoryException ex )
            {
                OnOutOfMemoryError( "Select fewer items and try again.", ex );
            }
        }

        private void collapseAllStacksToolStripMenuItem_Click( object sender, EventArgs e )
        {
            foreach ( KeyValuePair<string, GTLTreeNode> pair in m_stackNodeDictionary )
            {
                if ( pair.Value != null )
                {
                    pair.Value.Collapse();
                }
            }
        }

        private void expandAllStacksToolStripMenuItem_Click( object sender, EventArgs e )
        {
            foreach ( KeyValuePair<string, GTLTreeNode> pair in m_stackNodeDictionary )
            {
                if ( pair.Value != null )
                {
                    pair.Value.Expand();
                }
            }
        }
        #endregion

        #region ToolStrip Event Handlers
        private void generateReportToolStripButton_Click( object sender, EventArgs e )
        {
            ReportItem report = m_trackerComponent.AddReportAction( 0, string.Empty,
                m_trackerComponent.AvailableMemory, m_trackerComponent.TrackDeallocations );

            FillData( report );
        }

        private void reportToolStripComboBox_SelectedIndexChanged( object sender, EventArgs e )
        {
            this.Report = this.reportToolStripComboBox.SelectedItem as ReportItem;
        }

        private void minSizeToolStripTextBox_TextChanged( object sender, EventArgs e )
        {            
            string text = this.minSizeToolStripTextBox.Text.Trim();
            int size = 0;
            
            if ( text != string.Empty )
            {
                try
                {
                    size = int.Parse( text );
                }
                catch
                {
                    size = this.MinSize;
                }
            }

            if ( size != this.MinSize )
            {
                this.MinSize = size;

                OnMinSizeChanged();
            }
        }

        private void maxSizeToolStripTextBox_TextChanged( object sender, EventArgs e )
        {
            string text = this.maxSizeToolStripTextBox.Text.Trim();
            int size = 0;

            if ( text != string.Empty )
            {
                try
                {
                    size = int.Parse( text );
                }
                catch
                {
                    size = this.MaxSize;
                }
            }

            if ( size != this.MaxSize )
            {
                this.MaxSize = size;

                OnMaxSizeChanged();
            }
        }

        #region Options Drop Down Menu
        private void optionsToolStripDropDownButton_DropDownOpening( object sender, EventArgs e )
        {
            this.multiSelectToolStripMenuItem.Checked = this.MultiSelect;
        }

        private void groupByToolStripMenuItem_DropDownOpening( object sender, EventArgs e )
        {
            this.groupByAllocationOrderToolStripMenuItem.Checked = m_groupBy == DisplayType.Order;
            this.groupByStackNameToolStripMenuItem.Checked = m_groupBy == DisplayType.StackName;

            bool enable = m_trackerComponent.TrackDeallocations;
            if ( m_report != null )
            {
                enable = m_report.TrackedDeallocations;
            }

            this.groupByAllocationsToolStripMenuItem.Enabled = enable;
            this.groupByDeallocationsToolStripMenuItem.Enabled = enable;

            this.groupByAllocationsToolStripMenuItem.Checked = m_showAllocations == true;
            this.groupByDeallocationsToolStripMenuItem.Checked = m_showAllocations == false;
        }

        #region GroupBy Drop Down Menu
        private void groupByAllocationOrderToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.GroupBy != DisplayType.Order )
            {
                this.GroupBy = DisplayType.Order;
                OnGroupByChanged();
            }
        }

        private void groupByStackNameToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.GroupBy != DisplayType.StackName )
            {
                this.GroupBy = DisplayType.StackName;
                OnGroupByChanged();
            }
        }

        private void groupByAllocationsToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.ShowAllocations != true )
            {
                this.ShowAllocations = true;
                OnShowAllocationsChanged();
            }
        }

        private void groupByDeallocationsToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.ShowAllocations != false )
            {
                this.ShowAllocations = false;
                OnShowAllocationsChanged();
            }
        }
        #endregion

        private void stacksToolStripMenuItem_DropDownOpening( object sender, EventArgs e )
        {
            this.stacksUnassignedToolStripMenuItem.Checked = this.ShowUnassignedStack;
        }

        #region Stacks Drop Down Menu
        private void stacksAllToolStripMenuItem_Click( object sender, EventArgs e )
        {
            bool stacksChanged;
            bool unassignedStackChanged;
            if ( SetSelectedStacks( SelectedStacks.All, out stacksChanged, out unassignedStackChanged ) )
            {
                if ( stacksChanged )
                {
                    OnSelectedStacksChanged();
                }

                if ( unassignedStackChanged )
                {
                    OnShowUnassignedStackChanged();
                }
            }
        }

        private void stacksNoneToolStripMenuItem_Click( object sender, EventArgs e )
        {
            bool stacksChanged;
            bool unassignedStackChanged;
            if ( SetSelectedStacks( SelectedStacks.None, out stacksChanged, out unassignedStackChanged ) )
            {
                if ( stacksChanged )
                {
                    OnSelectedStacksChanged();
                }

                if ( unassignedStackChanged )
                {
                    OnShowUnassignedStackChanged();
                }
            }
        }

        private void stacksUnassignedToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.ShowUnassignedStack != this.stacksUnassignedToolStripMenuItem.Checked )
            {
                this.ShowUnassignedStack = this.stacksUnassignedToolStripMenuItem.Checked;
                OnShowUnassignedStackChanged();
            }
        }

        private void stacksSelectToolStripMenuItem_Click( object sender, EventArgs e )
        {
            Dictionary<uint, bool> stackVisibility = m_stackVisibility;
            bool showUnassignedStack = m_showUnassignedStack;
            
            if ( ItemVisibilityForm.ShowIt( m_parentControl, "Select Stacks",
                ref stackVisibility, Allocation.StackNameDictionary, ref showUnassignedStack, Allocation.Unassigned ) )
            {
                m_stackMutex.WaitOne();

                bool selectedStacksChanged = false;
                foreach ( KeyValuePair<uint, bool> pair in stackVisibility )
                {
                    if ( pair.Value != m_stackVisibility[pair.Key] )
                    {
                        selectedStacksChanged = true;
                        break;
                    }
                }

                if ( selectedStacksChanged )
                {
                    m_stackVisibility = stackVisibility;
                }

                DetermineVisibleStacks();

                m_stackMutex.ReleaseMutex();

                this.RefreshNeeded = true;

                if ( selectedStacksChanged )
                {
                    OnSelectedStacksChanged();
                }

                if ( m_showUnassignedStack != showUnassignedStack )
                {
                    m_showUnassignedStack = showUnassignedStack;
                    OnShowUnassignedStackChanged();
                }
            }
        }
        #endregion

        private void datatypesToolStripMenuItem_DropDownOpening( object sender, EventArgs e )
        {
            this.datatypesNativeTypesToolStripMenuItem.Checked = this.ShowNativeDatatypes;
            this.datatypesClassesToolStripMenuItem.Checked = this.ShowClassDatatypes;
            this.datatypesStructuresToolStripMenuItem.Checked = this.ShowStructureDatatypes;
            this.datatypesUnknownTypesToolStripMenuItem.Checked = this.ShowUnknownDatatypes;
        }

        #region Datatypes Drop Down Menu
        private void datatypesAllToolStripMenuItem_Click( object sender, EventArgs e )
        {
            bool selectedDatatypesChanged;
            bool nativeDatatypesChanged;
            bool classesChanged;
            bool structuresChanged;
            bool unknownDatatypesChanged;
            if ( SetSelectedDatatypes( SelectedDatatypes.All, out selectedDatatypesChanged,
                out nativeDatatypesChanged, out classesChanged, out structuresChanged, out unknownDatatypesChanged ) )
            {
                if ( selectedDatatypesChanged )
                {
                    OnSelectedDatatypesChanged();
                }

                if ( nativeDatatypesChanged )
                {
                    OnShowNativeDatatypesChanged();
                }

                if ( classesChanged )
                {
                    OnShowClassDatatypesChanged();
                }

                if ( structuresChanged )
                {
                    OnShowStructureDatatypesChanged();
                }

                if ( unknownDatatypesChanged )
                {
                    OnShowUnknownDatatypesChanged();
                }
            }
        }

        private void datatypesNoneToolStripMenuItem_Click( object sender, EventArgs e )
        {
            bool selectedDatatypesChanged;
            bool nativeDatatypesChanged;
            bool classesChanged;
            bool structuresChanged;
            bool unknownDatatypesChanged;
            if ( SetSelectedDatatypes( SelectedDatatypes.None, out selectedDatatypesChanged,
                out nativeDatatypesChanged, out classesChanged, out structuresChanged, out unknownDatatypesChanged ) )
            {
                if ( selectedDatatypesChanged )
                {
                    OnSelectedDatatypesChanged();
                }

                if ( nativeDatatypesChanged )
                {
                    OnShowNativeDatatypesChanged();
                }

                if ( classesChanged )
                {
                    OnShowClassDatatypesChanged();
                }

                if ( structuresChanged )
                {
                    OnShowStructureDatatypesChanged();
                }

                if ( unknownDatatypesChanged )
                {
                    OnShowUnknownDatatypesChanged();
                }
            }
        }

        private void datatypesNativeTypesToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.ShowNativeDatatypes != this.datatypesNativeTypesToolStripMenuItem.Checked )
            {
                this.ShowNativeDatatypes = this.datatypesNativeTypesToolStripMenuItem.Checked;
                OnShowNativeDatatypesChanged();
            }
        }

        private void datatypesClassesToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.ShowClassDatatypes != this.datatypesClassesToolStripMenuItem.Checked )
            {
                this.ShowClassDatatypes = this.datatypesClassesToolStripMenuItem.Checked;
                OnShowClassDatatypesChanged();
            }
        }

        private void datatypesStructuresToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.ShowStructureDatatypes != this.datatypesClassesToolStripMenuItem.Checked )
            {
                this.ShowStructureDatatypes = this.datatypesClassesToolStripMenuItem.Checked;
                OnShowStructureDatatypesChanged();
            }
        }

        private void datatypesUnknownTypesToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.ShowUnknownDatatypes != this.datatypesUnknownTypesToolStripMenuItem.Checked )
            {
                this.ShowUnknownDatatypes = this.datatypesUnknownTypesToolStripMenuItem.Checked;
                OnShowUnknownDatatypesChanged();
            }
        }

        private void datatypesSelectToolStripMenuItem_Click( object sender, EventArgs e )
        {
            Dictionary<uint, bool> datatypeVisibility = m_datatypeVisibility;
            bool showUnknownDatatypes = m_showUnknownDatatypes;

            if ( ItemVisibilityForm.ShowIt( m_parentControl, "Select Data Types",
                ref datatypeVisibility, Allocation.DataTypeDictionary, ref showUnknownDatatypes, Allocation.Unknown ) )
            {
                m_datatypeMutex.WaitOne();

                bool selectedDatatypesChanged = false;
                foreach ( KeyValuePair<uint, bool> pair in datatypeVisibility )
                {
                    if ( pair.Value != m_datatypeVisibility[pair.Key] )
                    {
                        selectedDatatypesChanged = true;
                        break;
                    }
                }

                if ( selectedDatatypesChanged )
                {
                    m_datatypeVisibility = datatypeVisibility;
                }

                DetermineVisibleDatatypes();

                // set our datatype Types
                bool selectNativeTypes = true;
                bool selectClassTypes = true;
                bool selectStructTypes = true;
                if ( m_selectedDatatypes == SelectedDatatypes.Selected )
                {
                    foreach ( KeyValuePair<uint, bool> pair in m_datatypeVisibility )
                    {
                        if ( !pair.Value )
                        {
                            string datatypeName;
                            if ( Allocation.DataTypeDictionary.TryGetValue( pair.Key, out datatypeName ) )
                            {
                                if ( selectClassTypes )
                                {
                                    if ( datatypeName.StartsWith( "class" ) )
                                    {
                                        selectClassTypes = false;
                                    }
                                }
                                else if ( selectStructTypes )
                                {
                                    if ( datatypeName.StartsWith( "struct" ) )
                                    {
                                        selectStructTypes = false;
                                    }
                                }
                                else if ( selectNativeTypes )
                                {
                                    selectNativeTypes = false;
                                }
                                else
                                {
                                    break;
                                }
                            }
                        }
                    }
                }
                else
                {
                    bool vis = m_selectedDatatypes == SelectedDatatypes.All;
                    selectNativeTypes = vis;
                    selectClassTypes = vis;
                    selectStructTypes = vis;
                }

                m_datatypeMutex.ReleaseMutex();

                this.RefreshNeeded = true;

                if ( selectedDatatypesChanged )
                {
                    OnSelectedDatatypesChanged();
                }

                if ( m_showNativeDatatypes != selectNativeTypes )
                {
                    m_showNativeDatatypes = selectNativeTypes;
                    OnShowNativeDatatypesChanged();
                }

                if ( m_showClassDatatypes != selectClassTypes )
                {
                    m_showClassDatatypes = selectClassTypes;
                    OnShowClassDatatypesChanged();
                }

                if ( m_showStructDatatypes != selectStructTypes )
                {
                    m_showStructDatatypes = selectStructTypes;
                    OnShowStructureDatatypesChanged();
                }

                if ( m_showUnknownDatatypes != showUnknownDatatypes )
                {
                    m_showUnknownDatatypes = showUnknownDatatypes;
                    OnShowUnknownDatatypesChanged();
                }
            }
        }
        #endregion

        private void memoryTypesToolStripMenuItem_DropDownOpening( object sender, EventArgs e )
        {
            this.memoryTypesGameVirtualToolStripMenuItem.Checked = this.ShowGameVirtualMemory;
            this.memoryTypesGamePhysicalToolStripMenuItem.Checked = this.ShowGamePhysicalMemory;
            this.memoryTypesResourceVirtualToolStripMenuItem.Checked = this.ShowResourceVirtualMemory;
            this.memoryTypesResourcePhysicalToolStripMenuItem.Checked = this.ShowResourcePhysicalMemory;
            this.memoryTypesUndeterminedToolStripMenuItem.Checked = this.ShowUndeterminedMemory;
            this.memoryTypesConsoleSpecificlToolStripMenuItem.Checked = this.ShowConsoleSpecificMemory;
        }

        #region Memory Types Drop Down Menu
        private void memoryTypesAllToolStripMenuItem_Click( object sender, EventArgs e )
        {
            bool gameVirtualChanged;
            bool gamePhysicalChanged;
            bool resourceVirtualChanged;
            bool resourcePhysicalChanged;
            bool undeterminedChanged;
            bool consoleSpecificChanged;
            if ( SetSelectedMemoryTypes( SelectedMemoryTypes.All, out gameVirtualChanged, out gamePhysicalChanged,
                out resourceVirtualChanged, out resourcePhysicalChanged,
                out undeterminedChanged, out consoleSpecificChanged ) )
            {
                if ( gameVirtualChanged )
                {
                    OnShowGameVirtualMemoryChanged();
                }

                if ( gamePhysicalChanged )
                {
                    OnShowGamePhysicalMemoryChanged();
                }

                if ( resourceVirtualChanged )
                {
                    OnShowResourceVirtualMemoryChanged();
                }

                if ( resourcePhysicalChanged )
                {
                    OnShowResourcePhysicalMemoryChanged();
                }

                if ( undeterminedChanged )
                {
                    OnShowUndeterminedMemoryChanged();
                }

                if ( consoleSpecificChanged )
                {
                    OnShowConsoleSpecificMemoryChanged();
                }
            }
        }

        private void memoryTypesNoneToolStripMenuItem_Click( object sender, EventArgs e )
        {
            bool gameVirtualChanged;
            bool gamePhysicalChanged;
            bool resourceVirtualChanged;
            bool resourcePhysicalChanged;
            bool undeterminedChanged;
            bool consoleSpecificChanged;
            if ( SetSelectedMemoryTypes( SelectedMemoryTypes.None, out gameVirtualChanged, out gamePhysicalChanged,
                out resourceVirtualChanged, out resourcePhysicalChanged,
                out undeterminedChanged, out consoleSpecificChanged ) )
            {
                if ( gameVirtualChanged )
                {
                    OnShowGameVirtualMemoryChanged();
                }

                if ( gamePhysicalChanged )
                {
                    OnShowGamePhysicalMemoryChanged();
                }

                if ( resourceVirtualChanged )
                {
                    OnShowResourceVirtualMemoryChanged();
                }

                if ( resourcePhysicalChanged )
                {
                    OnShowResourcePhysicalMemoryChanged();
                }

                if ( undeterminedChanged )
                {
                    OnShowUndeterminedMemoryChanged();
                }

                if ( consoleSpecificChanged )
                {
                    OnShowConsoleSpecificMemoryChanged();
                }
            }
        }

        private void memoryTypesGameVirtualToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.ShowGameVirtualMemory != this.memoryTypesGameVirtualToolStripMenuItem.Checked )
            {
                this.ShowGameVirtualMemory = this.memoryTypesGameVirtualToolStripMenuItem.Checked;
                OnShowGameVirtualMemoryChanged();
            }
        }

        private void memoryTypesResourceVirtualToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.ShowResourceVirtualMemory != this.memoryTypesResourceVirtualToolStripMenuItem.Checked )
            {
                this.ShowResourceVirtualMemory = this.memoryTypesResourceVirtualToolStripMenuItem.Checked;
                OnShowResourceVirtualMemoryChanged();
            }
        }

        private void memoryTypesGamePhysicalToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.ShowGamePhysicalMemory != this.memoryTypesGamePhysicalToolStripMenuItem.Checked )
            {
                this.ShowGamePhysicalMemory = this.memoryTypesGamePhysicalToolStripMenuItem.Checked;
                OnShowGamePhysicalMemoryChanged();
            }
        }

        private void memoryTypesResourcePhysicalToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.ShowResourcePhysicalMemory != this.memoryTypesResourcePhysicalToolStripMenuItem.Checked )
            {
                this.ShowResourcePhysicalMemory = this.memoryTypesResourcePhysicalToolStripMenuItem.Checked;
                OnShowResourcePhysicalMemoryChanged();
            }
        }

        private void memoryTypesConsoleSpecificToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.ShowConsoleSpecificMemory != this.memoryTypesConsoleSpecificlToolStripMenuItem.Checked )
            {
                this.ShowConsoleSpecificMemory = this.memoryTypesConsoleSpecificlToolStripMenuItem.Checked;
                OnShowConsoleSpecificMemoryChanged();
            }
        }

        private void memoryTypesUndeterminedToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.ShowUndeterminedMemory != this.memoryTypesUndeterminedToolStripMenuItem.Checked )
            {
                this.ShowUndeterminedMemory = this.memoryTypesUndeterminedToolStripMenuItem.Checked;
                OnShowUndeterminedMemoryChanged();
            }
        }
        #endregion

        private void multiSelectToolStripMenuItem_Click( object sender, EventArgs e )
        {
            this.MultiSelect = this.multiSelectToolStripMenuItem.Checked;
        }
        #endregion

        private void refreshToolStripButton_Click( object sender, EventArgs e )
        {
            FillData();
        }
        #endregion

        #region TrackerComponent Event Handlers
        private void trackerComponent_ReportAdded( object sender, ReportChangedEventArgs e )
        {
            this.reportToolStripComboBox.Items.Add( e.Report );
        }

        private void trackerComponent_ReportRemoved( object sender, ReportChangedEventArgs e )
        {
            this.reportToolStripComboBox.Items.Remove( e.Report );
        }

        private void trackerComponent_StackAdded( object sender, StackChangedEventArgs e )
        {
            m_stackMutex.WaitOne();

            if ( !m_stackVisibility.ContainsKey( e.HashKey ) )
            {
                m_stackVisibility.Add( e.HashKey, m_selectedStacks == SelectedStacks.All );
            }

            m_stackMutex.ReleaseMutex();
        }

        private void trackerComponent_DatatypeAdded( object sender, DatatypeAddedEventArgs e )
        {
            m_datatypeMutex.WaitOne();

            if ( !m_datatypeVisibility.ContainsKey( e.HashKey ) )
            {
                m_datatypeVisibility.Add( e.HashKey, m_selectedDatatypes == SelectedDatatypes.All );
            }

            m_datatypeMutex.ReleaseMutex();
        }
        #endregion

        #region BackgroundWorker Event Handlers
        private void backgroundWorker_DoWork( object sender, DoWorkEventArgs e )
        {
            switch ( m_groupBy )
            {
                case DisplayType.Order:
                    e.Result = FileOrderedData();
                    break;
                case DisplayType.StackName:
                    e.Result = FillStackData();
                    break;
            }
        }

        private void backgroundWorker_ProgressChanged( object sender, ProgressChangedEventArgs e )
        {
            OnUpdateStep( e.ProgressPercentage, m_reportGenerated );
        }

        private void backgroundWorker_RunWorkerCompleted( object sender, RunWorkerCompletedEventArgs e )
        {
            // enable the toolstrip
            this.toolStrip1.Enabled = true;

            int result = (int)e.Result;

            if ( e.Cancelled )
            {
                OnOutOfMemoryError( "De-select some Stack Names, Datatypes, and/or Memory Types and try again.", m_refreshException );
                m_refreshException = null;
            }
            else if ( result == 1 )
            {
                // fill the tree
                bool largeNumberOfNodes = m_nodes.Count >= 500;

                this.glacialTreeList1.TreeList.BeginUpdate();

                if ( largeNumberOfNodes )
                {
                    this.glacialTreeList1.TreeList.BeginNodeListChange();
                }

                foreach ( GTLTreeNode node in m_nodes )
                {
                    this.glacialTreeList1.TreeList.Nodes.Add( node );
                }

                if ( largeNumberOfNodes )
                {
                    this.glacialTreeList1.TreeList.EndNodeListChange();
                }

                this.glacialTreeList1.TreeList.EndUpdate();

                // set Show By:
                StringBuilder showByText = new StringBuilder( "Group By:  " );
                switch ( m_groupBy )
                {
                    case DisplayType.Order:
                        showByText.Append( "Allocation Order " );
                        break;
                    case DisplayType.StackName:
                        showByText.Append( "Stack Name " );
                        break;
                    default:
                        break;
                }

                bool showAllocations = m_report.TrackedDeallocations ? m_showAllocations : true;
                if ( showAllocations )
                {
                    showByText.Append( "(allocations)" );
                }
                else
                {
                    showByText.Append( "(deallocations)" );
                }

                this.groupByToolStripStatusLabel.Text = showByText.ToString();

                // set Selected Stacks:
                StringBuilder selectedStacksText = new StringBuilder( "Stacks:  " );
                selectedStacksText.Append( m_selectedStacks.ToString() );
                this.selectedStacksToolStripStatusLabel.Text = selectedStacksText.ToString();

                // set Selected Datatypes:
                StringBuilder selectedDataTypesText = new StringBuilder( "Datatypes:  " );
                selectedDataTypesText.Append( m_selectedDatatypes.ToString() );
                this.selectedDatatypesToolStripStatusLabel.Text = selectedDataTypesText.ToString();

                // set Selected Memory Types:
                StringBuilder selectedMemoryTypesText = new StringBuilder( "Memory Types:  " );
                selectedMemoryTypesText.Append( m_selectedMemoryTypes.ToString() );
                this.selectedMemoryTypesToolStripStatusLabel.Text = selectedMemoryTypesText.ToString();

                // set Size Range:
                StringBuilder sizeRangeText = new StringBuilder( "Size Range:  " );
                sizeRangeText.Append( this.MinSize.ToString( "n" ).Replace( ".00", "" ) );
                sizeRangeText.Append( " to " );
                sizeRangeText.Append( this.MaxSize.ToString( "n" ).Replace( ".00", "" ) );
                sizeRangeText.Append( " bytes" );
                this.sizeRangeToolStripStatusLabel.Text = sizeRangeText.ToString();

                // set Selected Size:
                this.selectedSizeToolStripStatusLabel.Text = "Selected Size:  0 bytes";

                // disable the Refresh Button
                this.RefreshNeeded = false;
            }

            m_nodes.Clear();

            OnUpdateComplete( m_reportGenerated ? m_report : null, result == 1 );

            m_reportGenerated = false;
        }
        #endregion
    }
}
