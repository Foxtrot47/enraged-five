using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace MemTrackerControls
{
    public class ReportItem
    {
        public ReportItem() : base()
        {
        }

        public ReportItem( string name, int index, uint[] available, bool trackingDeallocations ) : base()
        {
            m_name = name;
            m_index = index;

            Debug.Assert( available.Length == (int)Allocation.MemoryTypeIndex.NumIndexes );
            available.CopyTo( m_availableMemory, 0 );

            m_trackedDeallocations = trackingDeallocations;
        }

        #region Properties
        /// <summary>
        /// Gets the name of this report.
        /// </summary>
        public string Name
        {
            get
            {
                return m_name;
            }
        }

        /// <summary>
        /// Gets or sets the index of this report in the <see cref="TrackerComponent"/>'s <see cref="TrackerAction"/> list.
        /// </summary>
        public int Index
        {
            get
            {
                return m_index;
            }
            set
            {
                m_index = value;
            }
        }

        /// <summary>
        /// Gets the amount of memory available when this report was generated.
        /// </summary>
        public uint[] Available
        {
            get
            {
                return m_availableMemory;
            }
        }

        /// <summary>
        /// Gets whether this report contains deallocation info.
        /// </summary>
        public bool TrackedDeallocations
        {
            get
            {
                return m_trackedDeallocations;
            }
        }
        #endregion

        #region Variables
        private string m_name = string.Empty;
        private int m_index = -1;
        private uint[] m_availableMemory = new uint[(int)Allocation.MemoryTypeIndex.NumIndexes];
        private bool m_trackedDeallocations = false;
        #endregion

        #region Overrides
        public override string ToString()
        {
            return m_name;
        }
        #endregion
    }
}
