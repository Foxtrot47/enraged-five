namespace MemTrackerControls
{
    partial class MemoryLayoutControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( m_trackerComponent != null )
            {
                m_trackerComponent.ReportAdded -= m_reportAddedEventHandler;
                m_trackerComponent.ReportRemoved -= m_reportRemovedEventHandler;
            }

            if ( disposing && (components != null) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager( typeof( MemoryLayoutControl ) );
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.memoryGroupBox = new System.Windows.Forms.GroupBox();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip( this.components );
            this.recenterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.memoryPanel = new System.Windows.Forms.Panel();
            this.colorKeyGroupBox = new System.Windows.Forms.GroupBox();
            this.colorKeyListView = new System.Windows.Forms.ListView();
            this.colorKeyImageList = new System.Windows.Forms.ImageList( this.components );
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.generateReportToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.reportToolStripLabel = new System.Windows.Forms.ToolStripLabel();
            this.reportToolStripComboBox = new System.Windows.Forms.ToolStripComboBox();
            this.optionsToolStripDropDownButton = new System.Windows.Forms.ToolStripDropDownButton();
            this.colorizeByToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.colorizeByStackNameToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.colorizeByFullStackPathToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.colorizeByAllocationSizeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.colorizeByDatatypeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bytesPerPixelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bytesPerPixel4ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bytesPerPixel8ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bytesPerPixel16ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bytesPerPixel32ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bytesPerPixel64ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bytesPerPixel128ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bytesPerPixel256ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bytesPerPixel512ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bytesPerPixel1024ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pixelWidthToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pixelWidth32ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pixelWidth64ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pixelWidth128ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pixelWidth256ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pixelWidth512ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pixelWidth1024ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sizeScaleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sizeScaleCoarseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sizeScaleMediumToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sizeScaleFineToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sizeScaleVeryFineToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.refreshToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.addressToolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.allocationToolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.backgroundWorker = new System.ComponentModel.BackgroundWorker();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.memoryGroupBox.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.colorKeyGroupBox.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.Location = new System.Drawing.Point( 3, 28 );
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add( this.memoryGroupBox );
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add( this.colorKeyGroupBox );
            this.splitContainer1.Size = new System.Drawing.Size( 593, 307 );
            this.splitContainer1.SplitterDistance = 345;
            this.splitContainer1.TabIndex = 0;
            // 
            // memoryGroupBox
            // 
            this.memoryGroupBox.BackColor = System.Drawing.SystemColors.Control;
            this.memoryGroupBox.ContextMenuStrip = this.contextMenuStrip1;
            this.memoryGroupBox.Controls.Add( this.memoryPanel );
            this.memoryGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.memoryGroupBox.Location = new System.Drawing.Point( 0, 0 );
            this.memoryGroupBox.Name = "memoryGroupBox";
            this.memoryGroupBox.Size = new System.Drawing.Size( 345, 307 );
            this.memoryGroupBox.TabIndex = 0;
            this.memoryGroupBox.TabStop = false;
            this.memoryGroupBox.Text = "Memory";
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.recenterToolStripMenuItem} );
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.ShowImageMargin = false;
            this.contextMenuStrip1.Size = new System.Drawing.Size( 109, 26 );
            // 
            // recenterToolStripMenuItem
            // 
            this.recenterToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.recenterToolStripMenuItem.Name = "recenterToolStripMenuItem";
            this.recenterToolStripMenuItem.Size = new System.Drawing.Size( 108, 22 );
            this.recenterToolStripMenuItem.Text = "&Re-center";
            this.recenterToolStripMenuItem.Click += new System.EventHandler( this.recenterToolStripMenuItem_Click );
            // 
            // memoryPanel
            // 
            this.memoryPanel.BackColor = System.Drawing.Color.White;
            this.memoryPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.memoryPanel.Location = new System.Drawing.Point( 3, 16 );
            this.memoryPanel.Name = "memoryPanel";
            this.memoryPanel.Size = new System.Drawing.Size( 339, 288 );
            this.memoryPanel.TabIndex = 0;
            this.memoryPanel.MouseLeave += new System.EventHandler( this.memoryPanel_MouseLeave );
            this.memoryPanel.MouseDown += new System.Windows.Forms.MouseEventHandler( this.memoryPanel_MouseDown );
            this.memoryPanel.MouseMove += new System.Windows.Forms.MouseEventHandler( this.memoryPanel_MouseMove );
            this.memoryPanel.MouseEnter += new System.EventHandler( this.memoryPanel_MouseEnter );
            this.memoryPanel.Paint += new System.Windows.Forms.PaintEventHandler( this.memoryPanel_Paint );
            this.memoryPanel.MouseHover += new System.EventHandler( this.memoryPanel_MouseHover );
            this.memoryPanel.MouseUp += new System.Windows.Forms.MouseEventHandler( this.memoryPanel_MouseUp );
            // 
            // colorKeyGroupBox
            // 
            this.colorKeyGroupBox.Controls.Add( this.colorKeyListView );
            this.colorKeyGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.colorKeyGroupBox.Location = new System.Drawing.Point( 0, 0 );
            this.colorKeyGroupBox.Name = "colorKeyGroupBox";
            this.colorKeyGroupBox.Size = new System.Drawing.Size( 244, 307 );
            this.colorKeyGroupBox.TabIndex = 0;
            this.colorKeyGroupBox.TabStop = false;
            this.colorKeyGroupBox.Text = "Color Key";
            // 
            // colorKeyListView
            // 
            this.colorKeyListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.colorKeyListView.Location = new System.Drawing.Point( 3, 16 );
            this.colorKeyListView.MultiSelect = false;
            this.colorKeyListView.Name = "colorKeyListView";
            this.colorKeyListView.ShowItemToolTips = true;
            this.colorKeyListView.Size = new System.Drawing.Size( 238, 288 );
            this.colorKeyListView.SmallImageList = this.colorKeyImageList;
            this.colorKeyListView.TabIndex = 0;
            this.colorKeyListView.UseCompatibleStateImageBehavior = false;
            this.colorKeyListView.View = System.Windows.Forms.View.List;
            // 
            // colorKeyImageList
            // 
            this.colorKeyImageList.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.colorKeyImageList.ImageSize = new System.Drawing.Size( 16, 16 );
            this.colorKeyImageList.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // toolStrip1
            // 
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.Items.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.generateReportToolStripButton,
            this.toolStripSeparator1,
            this.reportToolStripLabel,
            this.reportToolStripComboBox,
            this.optionsToolStripDropDownButton,
            this.toolStripSeparator2,
            this.refreshToolStripButton} );
            this.toolStrip1.Location = new System.Drawing.Point( 0, 0 );
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.ShowItemToolTips = false;
            this.toolStrip1.Size = new System.Drawing.Size( 599, 25 );
            this.toolStrip1.TabIndex = 1;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // generateReportToolStripButton
            // 
            this.generateReportToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.generateReportToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject( "generateReportToolStripButton.Image" )));
            this.generateReportToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.generateReportToolStripButton.Name = "generateReportToolStripButton";
            this.generateReportToolStripButton.Size = new System.Drawing.Size( 92, 22 );
            this.generateReportToolStripButton.Text = "Generate Report";
            this.generateReportToolStripButton.Click += new System.EventHandler( this.generateReportToolStripButton_Click );
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size( 6, 25 );
            // 
            // reportToolStripLabel
            // 
            this.reportToolStripLabel.AutoSize = false;
            this.reportToolStripLabel.Name = "reportToolStripLabel";
            this.reportToolStripLabel.Size = new System.Drawing.Size( 50, 22 );
            this.reportToolStripLabel.Text = "Report:";
            this.reportToolStripLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // reportToolStripComboBox
            // 
            this.reportToolStripComboBox.AutoSize = false;
            this.reportToolStripComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.reportToolStripComboBox.Name = "reportToolStripComboBox";
            this.reportToolStripComboBox.Size = new System.Drawing.Size( 220, 21 );
            this.reportToolStripComboBox.SelectedIndexChanged += new System.EventHandler( this.reportToolStripComboBox_SelectedIndexChanged );
            // 
            // optionsToolStripDropDownButton
            // 
            this.optionsToolStripDropDownButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.optionsToolStripDropDownButton.DropDownItems.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.colorizeByToolStripMenuItem,
            this.bytesPerPixelToolStripMenuItem,
            this.pixelWidthToolStripMenuItem,
            this.sizeScaleToolStripMenuItem} );
            this.optionsToolStripDropDownButton.Image = ((System.Drawing.Image)(resources.GetObject( "optionsToolStripDropDownButton.Image" )));
            this.optionsToolStripDropDownButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.optionsToolStripDropDownButton.Name = "optionsToolStripDropDownButton";
            this.optionsToolStripDropDownButton.Size = new System.Drawing.Size( 57, 22 );
            this.optionsToolStripDropDownButton.Text = "Options";
            this.optionsToolStripDropDownButton.DropDownOpening += new System.EventHandler( this.optionsToolStripDropDownButton_DropDownOpening );
            // 
            // colorizeByToolStripMenuItem
            // 
            this.colorizeByToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.colorizeByToolStripMenuItem.DropDownItems.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.colorizeByStackNameToolStripMenuItem,
            this.colorizeByFullStackPathToolStripMenuItem,
            this.colorizeByAllocationSizeToolStripMenuItem,
            this.colorizeByDatatypeToolStripMenuItem} );
            this.colorizeByToolStripMenuItem.Name = "colorizeByToolStripMenuItem";
            this.colorizeByToolStripMenuItem.Size = new System.Drawing.Size( 156, 22 );
            this.colorizeByToolStripMenuItem.Text = "&Colorize By";
            this.colorizeByToolStripMenuItem.DropDownOpening += new System.EventHandler( this.colorizeByToolStripMenuItem_DropDownOpening );
            // 
            // colorizeByStackNameToolStripMenuItem
            // 
            this.colorizeByStackNameToolStripMenuItem.CheckOnClick = true;
            this.colorizeByStackNameToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.colorizeByStackNameToolStripMenuItem.Name = "colorizeByStackNameToolStripMenuItem";
            this.colorizeByStackNameToolStripMenuItem.Size = new System.Drawing.Size( 155, 22 );
            this.colorizeByStackNameToolStripMenuItem.Text = "&Stack Name";
            this.colorizeByStackNameToolStripMenuItem.Click += new System.EventHandler( this.colorizeByStackNameToolStripMenuItem_Click );
            // 
            // colorizeByFullStackPathToolStripMenuItem
            // 
            this.colorizeByFullStackPathToolStripMenuItem.CheckOnClick = true;
            this.colorizeByFullStackPathToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.colorizeByFullStackPathToolStripMenuItem.Name = "colorizeByFullStackPathToolStripMenuItem";
            this.colorizeByFullStackPathToolStripMenuItem.Size = new System.Drawing.Size( 155, 22 );
            this.colorizeByFullStackPathToolStripMenuItem.Text = "&Full Stack Path";
            this.colorizeByFullStackPathToolStripMenuItem.Click += new System.EventHandler( this.colorizeByFullStackPathToolStripMenuItem_Click );
            // 
            // colorizeByAllocationSizeToolStripMenuItem
            // 
            this.colorizeByAllocationSizeToolStripMenuItem.CheckOnClick = true;
            this.colorizeByAllocationSizeToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.colorizeByAllocationSizeToolStripMenuItem.Name = "colorizeByAllocationSizeToolStripMenuItem";
            this.colorizeByAllocationSizeToolStripMenuItem.Size = new System.Drawing.Size( 155, 22 );
            this.colorizeByAllocationSizeToolStripMenuItem.Text = "&Allocation Size";
            this.colorizeByAllocationSizeToolStripMenuItem.Click += new System.EventHandler( this.colorizeByAllocationSizeToolStripMenuItem_Click );
            // 
            // colorizeByDatatypeToolStripMenuItem
            // 
            this.colorizeByDatatypeToolStripMenuItem.CheckOnClick = true;
            this.colorizeByDatatypeToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.colorizeByDatatypeToolStripMenuItem.Name = "colorizeByDatatypeToolStripMenuItem";
            this.colorizeByDatatypeToolStripMenuItem.Size = new System.Drawing.Size( 155, 22 );
            this.colorizeByDatatypeToolStripMenuItem.Text = "&Datatype";
            this.colorizeByDatatypeToolStripMenuItem.Click += new System.EventHandler( this.colorizeByDatatypeToolStripMenuItem_Click );
            // 
            // bytesPerPixelToolStripMenuItem
            // 
            this.bytesPerPixelToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bytesPerPixelToolStripMenuItem.DropDownItems.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.bytesPerPixel4ToolStripMenuItem,
            this.bytesPerPixel8ToolStripMenuItem,
            this.bytesPerPixel16ToolStripMenuItem,
            this.bytesPerPixel32ToolStripMenuItem,
            this.bytesPerPixel64ToolStripMenuItem,
            this.bytesPerPixel128ToolStripMenuItem,
            this.bytesPerPixel256ToolStripMenuItem,
            this.bytesPerPixel512ToolStripMenuItem,
            this.bytesPerPixel1024ToolStripMenuItem} );
            this.bytesPerPixelToolStripMenuItem.Name = "bytesPerPixelToolStripMenuItem";
            this.bytesPerPixelToolStripMenuItem.Size = new System.Drawing.Size( 156, 22 );
            this.bytesPerPixelToolStripMenuItem.Text = "&Bytes Per Pixel";
            this.bytesPerPixelToolStripMenuItem.DropDownOpening += new System.EventHandler( this.bytesPerPixelToolStripMenuItem_DropDownOpening );
            // 
            // bytesPerPixel4ToolStripMenuItem
            // 
            this.bytesPerPixel4ToolStripMenuItem.CheckOnClick = true;
            this.bytesPerPixel4ToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bytesPerPixel4ToolStripMenuItem.Name = "bytesPerPixel4ToolStripMenuItem";
            this.bytesPerPixel4ToolStripMenuItem.Size = new System.Drawing.Size( 109, 22 );
            this.bytesPerPixel4ToolStripMenuItem.Text = "4";
            this.bytesPerPixel4ToolStripMenuItem.Click += new System.EventHandler( this.bytesPerPixel4ToolStripMenuItem_Click );
            // 
            // bytesPerPixel8ToolStripMenuItem
            // 
            this.bytesPerPixel8ToolStripMenuItem.CheckOnClick = true;
            this.bytesPerPixel8ToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bytesPerPixel8ToolStripMenuItem.Name = "bytesPerPixel8ToolStripMenuItem";
            this.bytesPerPixel8ToolStripMenuItem.Size = new System.Drawing.Size( 109, 22 );
            this.bytesPerPixel8ToolStripMenuItem.Text = "8";
            this.bytesPerPixel8ToolStripMenuItem.Click += new System.EventHandler( this.bytesPerPixel8ToolStripMenuItem_Click );
            // 
            // bytesPerPixel16ToolStripMenuItem
            // 
            this.bytesPerPixel16ToolStripMenuItem.CheckOnClick = true;
            this.bytesPerPixel16ToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bytesPerPixel16ToolStripMenuItem.Name = "bytesPerPixel16ToolStripMenuItem";
            this.bytesPerPixel16ToolStripMenuItem.Size = new System.Drawing.Size( 109, 22 );
            this.bytesPerPixel16ToolStripMenuItem.Text = "16";
            this.bytesPerPixel16ToolStripMenuItem.Click += new System.EventHandler( this.bytesPerPixel16ToolStripMenuItem_Click );
            // 
            // bytesPerPixel32ToolStripMenuItem
            // 
            this.bytesPerPixel32ToolStripMenuItem.CheckOnClick = true;
            this.bytesPerPixel32ToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bytesPerPixel32ToolStripMenuItem.Name = "bytesPerPixel32ToolStripMenuItem";
            this.bytesPerPixel32ToolStripMenuItem.Size = new System.Drawing.Size( 109, 22 );
            this.bytesPerPixel32ToolStripMenuItem.Text = "32";
            this.bytesPerPixel32ToolStripMenuItem.Click += new System.EventHandler( this.bytesPerPixel32ToolStripMenuItem_Click );
            // 
            // bytesPerPixel64ToolStripMenuItem
            // 
            this.bytesPerPixel64ToolStripMenuItem.CheckOnClick = true;
            this.bytesPerPixel64ToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bytesPerPixel64ToolStripMenuItem.Name = "bytesPerPixel64ToolStripMenuItem";
            this.bytesPerPixel64ToolStripMenuItem.Size = new System.Drawing.Size( 109, 22 );
            this.bytesPerPixel64ToolStripMenuItem.Text = "64";
            this.bytesPerPixel64ToolStripMenuItem.Click += new System.EventHandler( this.bytesPerPixel64ToolStripMenuItem_Click );
            // 
            // bytesPerPixel128ToolStripMenuItem
            // 
            this.bytesPerPixel128ToolStripMenuItem.CheckOnClick = true;
            this.bytesPerPixel128ToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bytesPerPixel128ToolStripMenuItem.Name = "bytesPerPixel128ToolStripMenuItem";
            this.bytesPerPixel128ToolStripMenuItem.Size = new System.Drawing.Size( 109, 22 );
            this.bytesPerPixel128ToolStripMenuItem.Text = "128";
            this.bytesPerPixel128ToolStripMenuItem.Click += new System.EventHandler( this.bytesPerPixel128ToolStripMenuItem_Click );
            // 
            // bytesPerPixel256ToolStripMenuItem
            // 
            this.bytesPerPixel256ToolStripMenuItem.CheckOnClick = true;
            this.bytesPerPixel256ToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bytesPerPixel256ToolStripMenuItem.Name = "bytesPerPixel256ToolStripMenuItem";
            this.bytesPerPixel256ToolStripMenuItem.Size = new System.Drawing.Size( 109, 22 );
            this.bytesPerPixel256ToolStripMenuItem.Text = "256";
            this.bytesPerPixel256ToolStripMenuItem.Click += new System.EventHandler( this.bytesPerPixel256ToolStripMenuItem_Click );
            // 
            // bytesPerPixel512ToolStripMenuItem
            // 
            this.bytesPerPixel512ToolStripMenuItem.CheckOnClick = true;
            this.bytesPerPixel512ToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bytesPerPixel512ToolStripMenuItem.Name = "bytesPerPixel512ToolStripMenuItem";
            this.bytesPerPixel512ToolStripMenuItem.Size = new System.Drawing.Size( 109, 22 );
            this.bytesPerPixel512ToolStripMenuItem.Text = "512";
            this.bytesPerPixel512ToolStripMenuItem.Click += new System.EventHandler( this.bytesPerPixel512ToolStripMenuItem_Click );
            // 
            // bytesPerPixel1024ToolStripMenuItem
            // 
            this.bytesPerPixel1024ToolStripMenuItem.CheckOnClick = true;
            this.bytesPerPixel1024ToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bytesPerPixel1024ToolStripMenuItem.Name = "bytesPerPixel1024ToolStripMenuItem";
            this.bytesPerPixel1024ToolStripMenuItem.Size = new System.Drawing.Size( 109, 22 );
            this.bytesPerPixel1024ToolStripMenuItem.Text = "1024";
            this.bytesPerPixel1024ToolStripMenuItem.Click += new System.EventHandler( this.bytesPerPixel1024ToolStripMenuItem_Click );
            // 
            // pixelWidthToolStripMenuItem
            // 
            this.pixelWidthToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.pixelWidthToolStripMenuItem.DropDownItems.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.pixelWidth32ToolStripMenuItem,
            this.pixelWidth64ToolStripMenuItem,
            this.pixelWidth128ToolStripMenuItem,
            this.pixelWidth256ToolStripMenuItem,
            this.pixelWidth512ToolStripMenuItem,
            this.pixelWidth1024ToolStripMenuItem} );
            this.pixelWidthToolStripMenuItem.Name = "pixelWidthToolStripMenuItem";
            this.pixelWidthToolStripMenuItem.Size = new System.Drawing.Size( 156, 22 );
            this.pixelWidthToolStripMenuItem.Text = "&Pixel Width";
            this.pixelWidthToolStripMenuItem.DropDownOpening += new System.EventHandler( this.pixelWidthToolStripMenuItem_DropDownOpening );
            // 
            // pixelWidth32ToolStripMenuItem
            // 
            this.pixelWidth32ToolStripMenuItem.CheckOnClick = true;
            this.pixelWidth32ToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.pixelWidth32ToolStripMenuItem.Name = "pixelWidth32ToolStripMenuItem";
            this.pixelWidth32ToolStripMenuItem.Size = new System.Drawing.Size( 109, 22 );
            this.pixelWidth32ToolStripMenuItem.Text = "32";
            this.pixelWidth32ToolStripMenuItem.Click += new System.EventHandler( this.pixelWidth32ToolStripMenuItem_Click );
            // 
            // pixelWidth64ToolStripMenuItem
            // 
            this.pixelWidth64ToolStripMenuItem.CheckOnClick = true;
            this.pixelWidth64ToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.pixelWidth64ToolStripMenuItem.Name = "pixelWidth64ToolStripMenuItem";
            this.pixelWidth64ToolStripMenuItem.Size = new System.Drawing.Size( 109, 22 );
            this.pixelWidth64ToolStripMenuItem.Text = "64";
            this.pixelWidth64ToolStripMenuItem.Click += new System.EventHandler( this.pixelWidth64ToolStripMenuItem_Click );
            // 
            // pixelWidth128ToolStripMenuItem
            // 
            this.pixelWidth128ToolStripMenuItem.CheckOnClick = true;
            this.pixelWidth128ToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.pixelWidth128ToolStripMenuItem.Name = "pixelWidth128ToolStripMenuItem";
            this.pixelWidth128ToolStripMenuItem.Size = new System.Drawing.Size( 109, 22 );
            this.pixelWidth128ToolStripMenuItem.Text = "128";
            this.pixelWidth128ToolStripMenuItem.Click += new System.EventHandler( this.pixelWidth128ToolStripMenuItem_Click );
            // 
            // pixelWidth256ToolStripMenuItem
            // 
            this.pixelWidth256ToolStripMenuItem.CheckOnClick = true;
            this.pixelWidth256ToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.pixelWidth256ToolStripMenuItem.Name = "pixelWidth256ToolStripMenuItem";
            this.pixelWidth256ToolStripMenuItem.Size = new System.Drawing.Size( 109, 22 );
            this.pixelWidth256ToolStripMenuItem.Text = "256";
            this.pixelWidth256ToolStripMenuItem.Click += new System.EventHandler( this.pixelWidth256ToolStripMenuItem_Click );
            // 
            // pixelWidth512ToolStripMenuItem
            // 
            this.pixelWidth512ToolStripMenuItem.CheckOnClick = true;
            this.pixelWidth512ToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.pixelWidth512ToolStripMenuItem.Name = "pixelWidth512ToolStripMenuItem";
            this.pixelWidth512ToolStripMenuItem.Size = new System.Drawing.Size( 109, 22 );
            this.pixelWidth512ToolStripMenuItem.Text = "512";
            this.pixelWidth512ToolStripMenuItem.Click += new System.EventHandler( this.pixelWidth512ToolStripMenuItem_Click );
            // 
            // pixelWidth1024ToolStripMenuItem
            // 
            this.pixelWidth1024ToolStripMenuItem.CheckOnClick = true;
            this.pixelWidth1024ToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.pixelWidth1024ToolStripMenuItem.Name = "pixelWidth1024ToolStripMenuItem";
            this.pixelWidth1024ToolStripMenuItem.Size = new System.Drawing.Size( 109, 22 );
            this.pixelWidth1024ToolStripMenuItem.Text = "1024";
            this.pixelWidth1024ToolStripMenuItem.Click += new System.EventHandler( this.pixelWidth1024ToolStripMenuItem_Click );
            // 
            // sizeScaleToolStripMenuItem
            // 
            this.sizeScaleToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.sizeScaleToolStripMenuItem.DropDownItems.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.sizeScaleCoarseToolStripMenuItem,
            this.sizeScaleMediumToolStripMenuItem,
            this.sizeScaleFineToolStripMenuItem,
            this.sizeScaleVeryFineToolStripMenuItem} );
            this.sizeScaleToolStripMenuItem.Name = "sizeScaleToolStripMenuItem";
            this.sizeScaleToolStripMenuItem.Size = new System.Drawing.Size( 156, 22 );
            this.sizeScaleToolStripMenuItem.Text = "&Size Scale";
            this.sizeScaleToolStripMenuItem.DropDownOpening += new System.EventHandler( this.sizeScaleToolStripMenuItem_DropDownOpening );
            // 
            // sizeScaleCoarseToolStripMenuItem
            // 
            this.sizeScaleCoarseToolStripMenuItem.CheckOnClick = true;
            this.sizeScaleCoarseToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.sizeScaleCoarseToolStripMenuItem.Name = "sizeScaleCoarseToolStripMenuItem";
            this.sizeScaleCoarseToolStripMenuItem.Size = new System.Drawing.Size( 130, 22 );
            this.sizeScaleCoarseToolStripMenuItem.Text = "&Coarse";
            this.sizeScaleCoarseToolStripMenuItem.Click += new System.EventHandler( this.sizeScaleCoarseToolStripMenuItem_Click );
            // 
            // sizeScaleMediumToolStripMenuItem
            // 
            this.sizeScaleMediumToolStripMenuItem.CheckOnClick = true;
            this.sizeScaleMediumToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.sizeScaleMediumToolStripMenuItem.Name = "sizeScaleMediumToolStripMenuItem";
            this.sizeScaleMediumToolStripMenuItem.Size = new System.Drawing.Size( 130, 22 );
            this.sizeScaleMediumToolStripMenuItem.Text = "&Medium";
            this.sizeScaleMediumToolStripMenuItem.Click += new System.EventHandler( this.sizeScaleMediumToolStripMenuItem_Click );
            // 
            // sizeScaleFineToolStripMenuItem
            // 
            this.sizeScaleFineToolStripMenuItem.CheckOnClick = true;
            this.sizeScaleFineToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.sizeScaleFineToolStripMenuItem.Name = "sizeScaleFineToolStripMenuItem";
            this.sizeScaleFineToolStripMenuItem.Size = new System.Drawing.Size( 130, 22 );
            this.sizeScaleFineToolStripMenuItem.Text = "&Fine";
            this.sizeScaleFineToolStripMenuItem.Click += new System.EventHandler( this.sizeScaleFineToolStripMenuItem_Click );
            // 
            // sizeScaleVeryFineToolStripMenuItem
            // 
            this.sizeScaleVeryFineToolStripMenuItem.CheckOnClick = true;
            this.sizeScaleVeryFineToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.sizeScaleVeryFineToolStripMenuItem.Name = "sizeScaleVeryFineToolStripMenuItem";
            this.sizeScaleVeryFineToolStripMenuItem.Size = new System.Drawing.Size( 130, 22 );
            this.sizeScaleVeryFineToolStripMenuItem.Text = "&Very Fine";
            this.sizeScaleVeryFineToolStripMenuItem.Click += new System.EventHandler( this.sizeScaleVeryFineToolStripMenuItem_Click );
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size( 6, 25 );
            // 
            // refreshToolStripButton
            // 
            this.refreshToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.refreshToolStripButton.Enabled = false;
            this.refreshToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject( "refreshToolStripButton.Image" )));
            this.refreshToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.refreshToolStripButton.Name = "refreshToolStripButton";
            this.refreshToolStripButton.Size = new System.Drawing.Size( 49, 22 );
            this.refreshToolStripButton.Text = "Refresh";
            this.refreshToolStripButton.Click += new System.EventHandler( this.refreshToolStripButton_Click );
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.addressToolStripStatusLabel,
            this.allocationToolStripStatusLabel} );
            this.statusStrip1.Location = new System.Drawing.Point( 0, 338 );
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size( 599, 22 );
            this.statusStrip1.TabIndex = 2;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // addressToolStripStatusLabel
            // 
            this.addressToolStripStatusLabel.AutoSize = false;
            this.addressToolStripStatusLabel.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top)
                        | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right)
                        | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.addressToolStripStatusLabel.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter;
            this.addressToolStripStatusLabel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.addressToolStripStatusLabel.Name = "addressToolStripStatusLabel";
            this.addressToolStripStatusLabel.Size = new System.Drawing.Size( 120, 17 );
            this.addressToolStripStatusLabel.Text = "Address:";
            this.addressToolStripStatusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // allocationToolStripStatusLabel
            // 
            this.allocationToolStripStatusLabel.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top)
                        | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right)
                        | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.allocationToolStripStatusLabel.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter;
            this.allocationToolStripStatusLabel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.allocationToolStripStatusLabel.Name = "allocationToolStripStatusLabel";
            this.allocationToolStripStatusLabel.Size = new System.Drawing.Size( 464, 17 );
            this.allocationToolStripStatusLabel.Spring = true;
            this.allocationToolStripStatusLabel.Text = "Allocation:";
            this.allocationToolStripStatusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // backgroundWorker
            // 
            this.backgroundWorker.WorkerReportsProgress = true;
            this.backgroundWorker.DoWork += new System.ComponentModel.DoWorkEventHandler( this.backgroundWorker_DoWork );
            this.backgroundWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler( this.backgroundWorker_RunWorkerCompleted );
            this.backgroundWorker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler( this.backgroundWorker_ProgressChanged );
            // 
            // MemoryLayoutControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add( this.statusStrip1 );
            this.Controls.Add( this.toolStrip1 );
            this.Controls.Add( this.splitContainer1 );
            this.Name = "MemoryLayoutControl";
            this.Size = new System.Drawing.Size( 599, 360 );
            this.splitContainer1.Panel1.ResumeLayout( false );
            this.splitContainer1.Panel2.ResumeLayout( false );
            this.splitContainer1.ResumeLayout( false );
            this.memoryGroupBox.ResumeLayout( false );
            this.contextMenuStrip1.ResumeLayout( false );
            this.colorKeyGroupBox.ResumeLayout( false );
            this.toolStrip1.ResumeLayout( false );
            this.toolStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout( false );
            this.statusStrip1.PerformLayout();
            this.ResumeLayout( false );
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem recenterToolStripMenuItem;
        private System.Windows.Forms.GroupBox memoryGroupBox;
        private System.Windows.Forms.GroupBox colorKeyGroupBox;
        private System.Windows.Forms.ListView colorKeyListView;
        private System.Windows.Forms.ImageList colorKeyImageList;
        private System.Windows.Forms.Panel memoryPanel;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripButton generateReportToolStripButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripLabel reportToolStripLabel;
        private System.Windows.Forms.ToolStripComboBox reportToolStripComboBox;
        private System.Windows.Forms.ToolStripDropDownButton optionsToolStripDropDownButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton refreshToolStripButton;
        private System.Windows.Forms.ToolStripMenuItem bytesPerPixelToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bytesPerPixel4ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bytesPerPixel8ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pixelWidthToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem colorizeByToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sizeScaleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bytesPerPixel16ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bytesPerPixel32ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bytesPerPixel64ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bytesPerPixel128ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bytesPerPixel256ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bytesPerPixel512ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bytesPerPixel1024ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pixelWidth32ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pixelWidth64ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pixelWidth128ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pixelWidth256ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pixelWidth512ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pixelWidth1024ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem colorizeByStackNameToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem colorizeByFullStackPathToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem colorizeByDatatypeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sizeScaleCoarseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sizeScaleMediumToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sizeScaleFineToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sizeScaleVeryFineToolStripMenuItem;
        private System.Windows.Forms.ToolStripStatusLabel addressToolStripStatusLabel;
        private System.Windows.Forms.ToolStripStatusLabel allocationToolStripStatusLabel;
        private System.Windows.Forms.ToolStripMenuItem colorizeByAllocationSizeToolStripMenuItem;
        private System.ComponentModel.BackgroundWorker backgroundWorker;
    }
}
