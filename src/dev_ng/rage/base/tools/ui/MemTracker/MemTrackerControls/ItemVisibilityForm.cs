using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace MemTrackerControls
{
    public partial class ItemVisibilityForm : Form
    {
        protected ItemVisibilityForm()
        {
            InitializeComponent();
        }

        protected ItemVisibilityForm( string title, Dictionary<uint, bool> itemVisibility, Dictionary<uint, string> nameDictionary,
            bool mysteryItemVisible, string mysteryItemName )
        {
            InitializeComponent();

            m_title = title;
            m_itemVisibility = new Dictionary<uint, bool>( itemVisibility );  // make copy
            m_mysteryItemName = mysteryItemName;
            m_mysteryItemVisible = mysteryItemVisible;

            // build a sorted list of LstViewItems
            SortedDictionary<string, ListViewItem> sortedVisibility = new SortedDictionary<string, ListViewItem>();
            foreach ( KeyValuePair<uint, bool> pair in itemVisibility )
            {
                string name = null;
                if ( nameDictionary.TryGetValue( pair.Key, out name ) )
                {
                    ListViewItem item = new ListViewItem( name );
                    item.Checked = pair.Value;
                    item.Tag = pair.Key;

                    sortedVisibility.Add( name, item );
                }
            }

            // crate the mystery name ListViewItem and add it to the ListView first
            if ( m_mysteryItemName != string.Empty )
            {
                ListViewItem mysteryItem = new ListViewItem( m_mysteryItemName );
                mysteryItem.Checked = mysteryItemVisible;
                this.listView1.Items.Add( mysteryItem );
            }

            // now add the other ListViewItems to the ListView
            foreach ( ListViewItem item in sortedVisibility.Values )
            {
                this.listView1.Items.Add( item );
            }
        }

        /// <summary>
        /// Creates a new form, populates the a ListView starting with the mysteryItemName, and followed by each name in alphabetical order
        /// from the nameDictionary.  Items are checked if their entry in the itemVisibility dictionary are true.
        /// </summary>
        /// <param name="parentControl">The form's parent control, usually the main form of the application.</param>
        /// <param name="title">The title to give this instance of <see cref="ItemVisibilityForm"/>.</param>
        /// <param name="itemVisibility">The dictionary of hash keys to visibility.</param>
        /// <param name="nameDictionary">The dictionary of hash keys to string name.</param>
        /// <param name="mysteryItemVisible">The visibility of the mystery item.</param>
        /// <param name="mysteryItemName">The name of the mystery item.</param>
        /// <returns><c>true</c> if the user accepted the changes, otherwise, <c>false</c>.</returns>
        /// <remarks>The visibilities are returned by reference.  In the case of itemVisibility, this is a copy of the
        /// original dictionary, but updated with new visibilities.</remarks>
        public static bool ShowIt( Control parentControl, string title, 
            ref Dictionary<uint, bool> itemVisibility, Dictionary<uint, string> nameDictionary,
            ref bool mysteryItemVisible, string mysteryItemName  )
        {
            ItemVisibilityForm form = new ItemVisibilityForm( title, itemVisibility, nameDictionary, mysteryItemVisible, mysteryItemName );
            DialogResult result = form.ShowDialog( parentControl );
            if ( result == DialogResult.OK )
            {
                itemVisibility = form.m_itemVisibility;
                mysteryItemVisible = form.m_mysteryItemVisible;
                return true;
            }

            return false;
        }

        #region Variables
        private string m_title;
        private Dictionary<uint, bool> m_itemVisibility;
        private string m_mysteryItemName;
        private bool m_mysteryItemVisible;
        #endregion

        #region Event Handlers
        private void ItemVisibilityForm_Load( object sender, EventArgs e )
        {
            this.Text = m_title;
        }

        private void filterTextBox_TextChanged( object sender, EventArgs e )
        {
            if ( this.filterTextBox.Text.Length == 0 )
            {
                return;
            }
        }

        private void allButton_Click( object sender, EventArgs e )
        {
            this.filterTextBox.Text = string.Empty;

            foreach ( ListViewItem item in this.listView1.Items )
            {
                item.Checked = true;
            }
        }

        private void noneButton_Click( object sender, EventArgs e )
        {
            this.filterTextBox.Text = string.Empty;

            foreach ( ListViewItem item in this.listView1.Items )
            {
                item.Checked = false;
            }
        }

        private void listView1_ItemChecked( object sender, ItemCheckedEventArgs e )
        {
            string name = e.Item.Text;
            bool check = e.Item.Checked;

            if ( name == m_mysteryItemName )
            {
                m_mysteryItemVisible = check;
            }
            else
            {
                uint hashKey = (uint)e.Item.Tag;
                m_itemVisibility[hashKey] = check;
            }
        }
        #endregion
    }
}