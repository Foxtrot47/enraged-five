using System;
using System.Collections.Generic;
using System.Text;

namespace MemTrackerControls
{
    public class AllocationStats
    {
        public AllocationStats( string name, uint nameHashKey )
        {
            m_name = name;
            m_hashKey = nameHashKey;
        }

        #region Properties
        /// <summary>
        /// Gets the name of this stat group
        /// </summary>
        public string Name
        {
            get
            {
                return m_name;
            }
        }

        /// <summary>
        /// Gets the hash key for the name.
        /// </summary>
        public uint HashKey
        {
            get
            {
                return m_hashKey;
            }
        }
        
        /// <summary>
        /// Gets or sets the number of Exclusive bytes in this stat group.
        /// </summary>
        public uint Exclusive;

        /// <summary>
        /// Gets or sets the number of Inclusive bytes in this stat group.
        /// </summary>
        public uint Inclusive;
        #endregion

        #region Variables
        private string m_name;
        private uint m_hashKey;
        #endregion

        #region Overrides
        public override string ToString()
        {
            return Name;
        }

        public override int GetHashCode()
        {
            return ToString().GetHashCode();
        }
        #endregion
    }
}
