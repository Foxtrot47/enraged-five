namespace MemTrackerControls
{
    partial class DuplicateAllocationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && (components != null) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.messageLabel = new System.Windows.Forms.Label();
            this.overwriteButton = new System.Windows.Forms.Button();
            this.leaveButton = new System.Windows.Forms.Button();
            this.overwriteAllButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // messageLabel
            // 
            this.messageLabel.AutoEllipsis = true;
            this.messageLabel.Dock = System.Windows.Forms.DockStyle.Top;
            this.messageLabel.Location = new System.Drawing.Point( 0, 0 );
            this.messageLabel.Name = "messageLabel";
            this.messageLabel.Size = new System.Drawing.Size( 526, 124 );
            this.messageLabel.TabIndex = 0;
            this.messageLabel.Text = "\r\nAn attempt was made to tally an allocation that already exists:\r\n\r\n    Address:" +
                " 0x00000000, Datatype: XXXXXXXX, Full Stack Path: \\blah\\blah\\blah, Size: 433\r\n\r\n" +
                "What would you like to do?";
            // 
            // overwriteButton
            // 
            this.overwriteButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.overwriteButton.DialogResult = System.Windows.Forms.DialogResult.Abort;
            this.overwriteButton.Location = new System.Drawing.Point( 277, 127 );
            this.overwriteButton.Name = "overwriteButton";
            this.overwriteButton.Size = new System.Drawing.Size( 75, 23 );
            this.overwriteButton.TabIndex = 1;
            this.overwriteButton.Text = "Overwrite";
            this.overwriteButton.UseVisualStyleBackColor = true;
            // 
            // leaveButton
            // 
            this.leaveButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.leaveButton.DialogResult = System.Windows.Forms.DialogResult.Retry;
            this.leaveButton.Location = new System.Drawing.Point( 358, 127 );
            this.leaveButton.Name = "leaveButton";
            this.leaveButton.Size = new System.Drawing.Size( 75, 23 );
            this.leaveButton.TabIndex = 2;
            this.leaveButton.Text = "Leave";
            this.leaveButton.UseVisualStyleBackColor = true;
            // 
            // overwriteAllButton
            // 
            this.overwriteAllButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.overwriteAllButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.overwriteAllButton.Location = new System.Drawing.Point( 439, 127 );
            this.overwriteAllButton.Name = "overwriteAllButton";
            this.overwriteAllButton.Size = new System.Drawing.Size( 75, 23 );
            this.overwriteAllButton.TabIndex = 3;
            this.overwriteAllButton.Text = "Overwrite All";
            this.overwriteAllButton.UseVisualStyleBackColor = true;
            // 
            // DuplicateAllocationForm
            // 
            this.AcceptButton = this.overwriteButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size( 526, 162 );
            this.ControlBox = false;
            this.Controls.Add( this.overwriteAllButton );
            this.Controls.Add( this.leaveButton );
            this.Controls.Add( this.overwriteButton );
            this.Controls.Add( this.messageLabel );
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DuplicateAllocationForm";
            this.Text = "Duplicate Allocation Found";
            this.ResumeLayout( false );

        }

        #endregion

        private System.Windows.Forms.Label messageLabel;
        private System.Windows.Forms.Button overwriteButton;
        private System.Windows.Forms.Button leaveButton;
        private System.Windows.Forms.Button overwriteAllButton;
    }
}