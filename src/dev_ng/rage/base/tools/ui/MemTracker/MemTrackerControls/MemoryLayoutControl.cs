using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace MemTrackerControls
{
    public partial class MemoryLayoutControl : UserControl
    {
        public MemoryLayoutControl()
        {
            InitializeComponent();

            this.RefreshNeeded = false;
        }

        #region Enums
        /// <summary>
        /// The type of Report to display
        /// </summary>
        public enum DisplayType
        {
            StackNames,
            FullStackPath,
            AllocationSize,
            DataType
        }

        /// <summary>
        /// An enumeration of the available BytesPerPixel settings
        /// </summary>
        public enum Bytes
        {
            Four = 4,
            Eight = 8,
            Sixteen = 16,
            ThirtyTwo = 32,
            SixtyFour = 64,
            OneHundredTwentyEight = 128,
            TwoHundredFiftySix = 256,
            FiveHundredTwelve = 512,
            OneThousandTwentyFour = 1024
        }

        /// <summary>
        /// An enumeration of the available PixelWidth settings
        /// </summary>
        public enum Widths
        {
            ThirtyTwo = 32,
            SixtyFour = 64,
            OneHundredTwentyEight = 128,
            TwoHundredFiftySix = 256,
            FiveHundredTwelve = 512,
            OneThousandTwentyFour = 1024
        }

        /// <summary>
        /// An enumeration of the available SizeScale settings
        /// </summary>
        public enum Sizes
        {
            Coarse,
            Medium,
            Fine,
            VeryFine
        }
        #endregion

        #region Constants
        const int c_memoryGraphTextLeft = 2;
        const int c_memoryGraphTop = 8;
        const int c_memoryGraphLeft = 70;

        const int c_colorKeyTop = 16;
        const int c_colorKeyLeft = 8;
        const int c_colorKeyTextLeft = 24;
        const int c_colorKeyColorBoxLength = 12;
        const int c_colorKeyLineSpacing = 20;
        const int c_colorKeyColumnSpacing = 0;
        const int c_colorKeyMaxLengthMultiplier = 6;

        private List<Color> c_colors = GenerateColors();

        private static List<Color> GenerateColors()
        {
            List<List<Color>> lists = new List<List<Color>>();

            lists.Add( GenerateColors( 255, 0, 0 ) );
            lists.Add( GenerateColors( 255, 255, 0 ) );
            lists.Add( GenerateColors( 0, 255, 0 ) );
            lists.Add( GenerateColors( 0, 255, 255 ) );
            lists.Add( GenerateColors( 0, 0, 255 ) );
            lists.Add( GenerateColors( 255, 0, 255 ) );

            List<Color> finalList = new List<Color>();
            finalList.Add( Color.Gray );

            // put them in "rainbow" order
            int count = lists[0].Count;
            for ( int i = 0; i < count; ++i )
            {
                for ( int j = 0; j < lists.Count; ++j )
                {
                    finalList.Add( lists[j][i] );
                }
            }

            finalList.Add( Color.DarkGray );

            return finalList;
        }

        private static List<Color> GenerateColors( int origRed, int origGreen, int origBlue )
        {
            List<Color> colors = new List<Color>();

            int addRed = origRed == 0 ? 1 : 0;
            int addGreen = origGreen == 0 ? 1 : 0;
            int addBlue = origBlue == 0 ? 1 : 0;

            int subtractRed = origRed == 0 ? 0 : 1;
            int subtractGreen = origGreen == 0 ? 0 : 1;
            int subtractBlue = origBlue == 0 ? 0 : 1;

            int[] subtractValues = new int[] { 0, 100, 50, 150, 25, 75, 125, 175 };
            int[] addValues = new int[] { 0, 100, 50, 125, 25, 75 };

            for ( int i = 0; i < 6; ++i )
            {
                int red, green, blue;
                red = origRed + (addRed * addValues[i]);
                green = origGreen + (addGreen * addValues[i]);
                blue = origBlue + (addBlue * addValues[i]);

                for ( int j = 0; j < 8; ++j )
                {
                    int r, g, b;
                    r = red - (subtractRed * subtractValues[j]);
                    g = green - (subtractGreen * subtractValues[j]);
                    b = blue - (subtractBlue * subtractValues[j]);

                    if ( subtractRed == 1 )
                    {
                        if ( addGreen == 1 )
                        {
                            if ( g > r - 30 )
                            {
                                break;
                            }
                        }
                        else if ( addBlue == 1 )
                        {
                            if ( b > r - 30 )
                            {
                                break;
                            }
                        }
                    }
                    else if ( subtractGreen == 1 )
                    {
                        if ( r > g - 30 )
                        {
                            break;
                        }
                    }
                    else if ( subtractBlue == 1 )
                    {
                        if ( r > b - 30 )
                        {
                            break;
                        }
                    }

                    colors.Add( Color.FromArgb( r, g, b ) );
                }
            }

            return colors;
        }
        #endregion

        #region Properties
        /// <summary>
        /// The underlying <see cref="ListView"/> control used for the color key.
        /// </summary>
        [Browsable( true ),
        DesignOnly( true ),
        Category( "Misc" )]
        public ListView ListView
        {
            get
            {
                return this.colorKeyListView;
            }
        }

        /// <summary>
        /// The <see cref="TrackerComponent"/> that manages all of the memory allocation data.
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        Description( "The object that manages the allocation, stack, and datatype information." )]
        public TrackerComponent Tracker
        {
            get
            {
                return m_trackerComponent;
            }
            set
            {
                m_trackerComponent = value;

                if ( !this.DesignMode && (m_trackerComponent != null) )
                {
                    ReportItem[] reports = m_trackerComponent.Reports;
                    foreach ( ReportItem report in reports )
                    {
                        trackerComponent_ReportAdded( m_trackerComponent, new ReportChangedEventArgs( report ) );
                    }

                    m_reportAddedEventHandler = new ReportChangedEventHandler( trackerComponent_ReportAdded );
                    m_trackerComponent.ReportAdded += m_reportAddedEventHandler;

                    m_reportRemovedEventHandler = new ReportChangedEventHandler( trackerComponent_ReportRemoved );
                    m_trackerComponent.ReportRemoved += m_reportRemovedEventHandler;
                }
            }
        }

        /// <summary>
        /// A <see cref="Control"/>, typically the application's main <see cref="Form"/> that can be used to launch
        /// dialog boxes and Invoke cross-threaded methods.
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        Description( "Sets the parent Control so that dialog boxes can appear in the correct location." )]
        public Control ParentControl
        {
            get
            {
                return m_parentControl;
            }
            set
            {
                m_parentControl = value;
            }
        }

        /// <summary>
        /// Show or hide the Status Bar's sizing grip at the lower right corner of the control.
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        Description( "Show or hide the Status Bar's sizing grip at the lower right corner of the control." )]
        public bool ShowSizingGrip
        {
            get
            {
                return this.statusStrip1.SizingGrip;
            }
            set
            {
                this.statusStrip1.SizingGrip = value;
            }
        }

        /// <summary>
        /// Gets or sets the coloring scheme of the report.
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        DefaultValue( "DisplayType.StackNames" ),
        Description( "The coloring scheme of the report." )]
        public DisplayType ColorizeBy
        {
            get
            {
                return m_colorizeBy;
            }
            set
            {
                if ( m_colorizeBy != value )
                {
                    m_colorizeBy = value;

                    if ( !this.DesignMode )
                    {
                        this.memoryPanel.Invalidate();

                        FillColorKeyListView();                        
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the number of bytes that each pixel represents.
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        DefaultValue( "Bytes.OneHundredTwentyEight" ),
        Description( "The number of bytes that each pixel represents." )]
        public Bytes BytesPerPixel
        {
            get
            {
                return m_bytesPerPixel;
            }
            set
            {
                if ( m_bytesPerPixel != value )
                {
                    m_bytesPerPixel = value;

                    if ( !this.DesignMode )
                    {
                        this.memoryPanel.Invalidate();                        
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the number of pixels across to display.
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        DefaultValue( "Widths.TwoHundredFiftySix" ),
        Description( "The number of pixels across to display." )]
        public Widths PixelWidth
        {
            get
            {
                return m_pixelWidth;
            }
            set
            {
                if ( m_pixelWidth != value )
                {
                    m_pixelWidth = value;

                    if ( !this.DesignMode )
                    {
                        this.memoryPanel.Invalidate();                        
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the level of detail for the colors when <see cref="ColorizeBy"/> is <see cref="DisplayType"/>.AllocationSize.
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        DefaultValue( "Sizes.Medium" ),
        Description( "The level of detail for the colors when ColorizeBy is DisplayType.AllocationSize." )]
        public Sizes SizeScale
        {
            get
            {
                return m_sizeScale;
            }
            set
            {
                if ( m_sizeScale != value )
                {
                    m_sizeScale = value;

                    if ( !this.DesignMode )
                    {
                        m_memoryLayoutDivisionSizes = GetDivisionSizes();

                        this.memoryPanel.Invalidate();

                        FillColorKeyListView();                        
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the <see cref="ReportItem"/>.
        /// </summary>
        [Browsable( false )]
        public ReportItem Report
        {
            get
            {
                return m_report;
            }
            set
            {
                if ( m_report != value )
                {
                    m_report = value;
                    this.reportToolStripComboBox.SelectedItem = value;
                    this.RefreshNeeded = true;
                }
            }
        }

        /// <summary>
        /// Gets or sets whether refreshing a report is threaded.
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        Description( "Enables or disables threading during the update." ),
        DefaultValue( false )]
        public bool ThreadingEnabled
        {
            get
            {
                return m_threadingEnabled;
            }
            set
            {
                m_threadingEnabled = value;
            }
        }

        /// <summary>
        /// Gets or sets the Enabled property of the Generate Report button.
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        Description( "Whether to enable the Generate Report button." ),
        DefaultValue( true )]
        public bool GenerateReportButtonEnabled
        {
            get
            {
                return this.generateReportToolStripButton.Enabled;
            }
            set
            {
                this.generateReportToolStripButton.Enabled = value;
            }
        }

        /// <summary>
        /// Gets or sets the Enabled property of the Refresh button.  
        /// Will only be enabled if we have a report selected and/or have changed an option since the last Refresh.
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        Description( "Whether to enable the Refresh button." ),
        DefaultValue( true )]
        public bool RefeshButtonEnabled
        {
            get
            {
                return this.refreshToolStripButton.Enabled;
            }
            set
            {
                m_allowRefresh = value;

                if ( this.DesignMode )
                {
                    this.refreshToolStripButton.Enabled = value;
                }
                else
                {
                    this.refreshToolStripButton.Enabled = m_allowRefresh && m_refreshNeeded;
                }
            }
        }

        /// <summary>
        /// Private property to aid in setting the Enabled property of the Refresh button properly.
        /// </summary>
        [Browsable( false )]
        private bool RefreshNeeded
        {
            set
            {
                m_refreshNeeded = value && (m_report != null);

                this.refreshToolStripButton.Enabled = m_allowRefresh && m_refreshNeeded;
            }
        }
        #endregion

        #region Events
        /// <summary>
        /// Signals that a report is about to begin it's refresh.
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        Description( "When calculating the layout, informs the UI how many steps it will take." )]
        public event UpdateBeginEventHandler UpdateBegin;

        /// <summary>
        /// Signals that one step of the refresh is complete.
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        Description( "When calculating the layout, informs the UI when a portion of the work has completed." )]
        public event UpdateStepEventHandler UpdateStep;

        /// <summary>
        /// Signals that the entire refresh is complete
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        Description( "When calculating the layout, informs the UI when all the work is completed." )]
        public event UpdateCompleteEventHandler UpdateComplete;

        /// <summary>
        /// Occurs when <see cref="ColorizeBy"/> is changed.
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        Description( "Occurs when the DisplayType is changed." )]
        public event EventHandler ColorizeByChanged;

        /// <summary>
        /// Occurs when <see cref="BytesPerPixel"/> is changed.
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        Description( "Occurs when BytesPerPixel is changed." )]
        public event EventHandler BytesPerPixelChanged;

        /// <summary>
        /// Occurs when <see cref="PixelWidth"/> is changed.
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        Description( "Occurs when PixelWidth is changed." )]
        public event EventHandler PixelWidthChanged;

        /// <summary>
        /// Occurs when <see cref="SizeScale"/> is changed.
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        Description( "Occurs when SizeScale is changed." )]
        public event EventHandler SizeScaleChanged;
        #endregion

        #region Variables
        private Control m_parentControl = null;

        private TrackerComponent m_trackerComponent;
        private ReportChangedEventHandler m_reportAddedEventHandler;
        private ReportChangedEventHandler m_reportRemovedEventHandler;

        private bool m_threadingEnabled = false;
        private bool m_refreshNeeded = false;
        private bool m_allowRefresh = true;

        private DisplayType m_colorizeBy = DisplayType.StackNames;
        private Bytes m_bytesPerPixel = Bytes.OneHundredTwentyEight;
        private Widths m_pixelWidth = Widths.TwoHundredFiftySix;
        private Sizes m_sizeScale = Sizes.Medium;

        private ReportItem m_report = null;

        private bool m_reportGenerated = false;

        private Mutex m_stackMutex = new Mutex();
        private Mutex m_datatypeMutex = new Mutex();

        private List<string> m_sortedDataTypes = new List<string>();
        private List<string> m_truncatedDataTypes = new List<string>();
        private List<string> m_memoryLayoutDivisionSizes = new List<string>();

        private List<Allocation> m_memoryLayoutAllocations = new List<Allocation>();
        private List<string> m_memoryLayoutStackNames = new List<string>();
        private List<string> m_memoryLayoutFullStackPaths = new List<string>();

        private bool m_memoryLayoutPanelDragging = false;
        private Point m_memoryLayoutPanelDraggingStartPoint;
        private Point m_memoryLayoutPanelDrawOffset = new Point();

        private Allocation m_lowestAddressAllocation;
        private Allocation m_highestAddressAllocation;
        private Allocation m_smallestAllocation;
        private Allocation m_largestAllocation;
        #endregion

        #region Public Functions
        /// <summary>
        /// Refreshes the layout based on the current settings.
        /// </summary>
        public virtual void FillData()
        {
            this.toolStrip1.Enabled = false;            

            if ( !m_threadingEnabled )
            {
                OnUpdateBegin( 100, m_reportGenerated );
                backgroundWorker_DoWork( this, new DoWorkEventArgs( null ) );
                backgroundWorker_RunWorkerCompleted( this, new RunWorkerCompletedEventArgs( (m_report == null) ? 0 : 1, null, false ) );
            }
            else
            {
                backgroundWorker.RunWorkerAsync();
                OnUpdateBegin( 100, m_reportGenerated );
            }            
        }

        /// <summary>
        /// Refreshes the <see cref="GlacialTreeList"/> based on the current settings, but using the given <see cref="ReportItem"/>.  Subsequent
        /// Update messages will indicate that we are generating a report.
        /// </summary>
        /// <param name="report">The <see cref="ReportItem"/> to use.</param>
        public virtual void FillData( ReportItem report )
        {
            this.Report = report;
            
            m_reportGenerated = true;
            
            FillData();
        }
        #endregion

        #region Protected Functions
        protected virtual int GatherMemoryData()
        {
            if ( m_report == null )
            {
                return 0;
            }

            m_trackerComponent.LockForUpdate = true;

            Stack<string> theStack = new Stack<string>();

            m_memoryLayoutAllocations.Clear();
            m_memoryLayoutStackNames.Clear();
            m_memoryLayoutFullStackPaths.Clear();

            float multiplier = 100.0f / (float)(m_report.Index + 1);

            List<TrackerAction> trackerActions = m_trackerComponent.Actions;
            for ( int i = 0; i <= m_report.Index; ++i )
            {
                TrackerAction action = trackerActions[i];
                switch ( action.TheAction )
                {
                    case TrackerAction.Action.PushStack:
                        {
                            PushStackAction stackAction = action as PushStackAction;
                            theStack.Push( stackAction.Name );
                            AddStackColor( theStack );
                        }
                        break;
                    case TrackerAction.Action.PopStack:
                        {
                            theStack.Pop();
                        }
                        break;
                    case TrackerAction.Action.TallyAllocation:
                        {
                            TallyAllocationAction tallyAction = action as TallyAllocationAction;
                            m_memoryLayoutAllocations.Add( tallyAction.TheAllocation );
                        }
                        break;
                    case TrackerAction.Action.UntallyAllocation:
                        {
                            UntallyAllocationAction untallyAction = action as UntallyAllocationAction;
                            m_memoryLayoutAllocations.Remove( untallyAction.TheAllocation );
                        }
                        break;
                }

                int percentComplete = (int)((float)i * multiplier);
                if ( m_threadingEnabled )
                {
                    backgroundWorker.ReportProgress( percentComplete );
                }
                else
                {
                    OnUpdateStep( percentComplete, m_reportGenerated );
                }
            }

            m_memoryLayoutStackNames.Sort();
            m_memoryLayoutStackNames.Insert( 0, Allocation.Unassigned );

            m_memoryLayoutFullStackPaths.Sort();
            m_memoryLayoutFullStackPaths.Insert( 0, Allocation.Unassigned );

            if ( m_memoryLayoutAllocations.Count > 0 )
            {
                m_memoryLayoutAllocations.Sort( SortAllocationByAddress );
                m_lowestAddressAllocation = m_memoryLayoutAllocations[0];
                m_highestAddressAllocation = m_memoryLayoutAllocations[m_memoryLayoutAllocations.Count - 1];

                m_memoryLayoutAllocations.Sort( SortAllocationBySize );
                m_smallestAllocation = m_memoryLayoutAllocations[0];
                m_largestAllocation = m_memoryLayoutAllocations[m_memoryLayoutAllocations.Count - 1];
            }

            m_sortedDataTypes.Clear();           
            m_truncatedDataTypes.Clear();

            foreach ( KeyValuePair<uint, string> pair in Allocation.DataTypeDictionary )
            {
                m_sortedDataTypes.Add( pair.Value );

                if ( !pair.Value.StartsWith( "class" ) && !pair.Value.StartsWith( "struct" ) )
                {
                    m_truncatedDataTypes.Add( pair.Value );
                }
            }

            m_sortedDataTypes.Sort();
            m_sortedDataTypes.Insert( 0, Allocation.Unknown );

            m_truncatedDataTypes.Sort();
            m_truncatedDataTypes.Insert( 0, Allocation.Unknown );
            m_truncatedDataTypes.Insert( 1, "Classes" );
            m_truncatedDataTypes.Insert( 2, "Structures" );

            m_memoryLayoutDivisionSizes = GetDivisionSizes();

            m_trackerComponent.LockForUpdate = false;

            return 1;
        }

        protected virtual void OnUpdateBegin( int numSteps, bool generatingReport )
        {
            if ( this.UpdateBegin != null )
            {
                UpdateBeginEventArgs e = new UpdateBeginEventArgs( numSteps, generatingReport );
                this.UpdateBegin( this, e );
            }
        }

        protected virtual void OnUpdateStep( int stepNumber, bool generatingReport )
        {
            if ( this.UpdateStep != null )
            {
                UpdateStepEventArgs e = new UpdateStepEventArgs( stepNumber, generatingReport );
                this.UpdateStep( this, e );
            }
        }

        protected virtual void OnUpdateComplete( ReportItem reportGenerated, bool success )
        {
            if ( this.UpdateComplete != null )
            {
                UpdateCompleteEventArgs e = new UpdateCompleteEventArgs( reportGenerated, success );
                this.UpdateComplete( this, e );
            }
        }

        protected virtual void OnColorizeByChanged()
        {
            if ( this.ColorizeByChanged != null )
            {
                this.ColorizeByChanged( this, new EventArgs() );
            }
        }

        protected virtual void OnBytesPerPixelChanged()
        {
            if ( this.BytesPerPixelChanged != null )
            {
                this.BytesPerPixelChanged( this, new EventArgs() );
            }
        }

        protected virtual void OnPixelWidthChanged()
        {
            if ( this.PixelWidthChanged != null )
            {
                this.PixelWidthChanged( this, new EventArgs() );
            }
        }

        protected virtual void OnSizeScaleChanged()
        {
            if ( this.SizeScaleChanged != null )
            {
                this.SizeScaleChanged( this, new EventArgs() );
            }
        }
        #endregion

        #region Private Functions
        private int SortAllocationBySize( Allocation a, Allocation b )
        {
            if ( a == null )
            {
                if ( b == null )
                {
                    return 0;
                }
                else
                {
                    return -1;
                }
            }
            else
            {
                if ( b == null )
                {
                    return 1;
                }
                else
                {
                    return a.Size.CompareTo( b.Size );
                }
            }
        }

        private int SortAllocationByAddress( Allocation a, Allocation b )
        {
            if ( a == null )
            {
                if ( b == null )
                {
                    return 0;
                }
                else
                {
                    return -1;
                }
            }
            else
            {
                if ( b == null )
                {
                    return 1;
                }
                else
                {
                    return a.DecimalAddress.CompareTo( b.DecimalAddress );
                }
            }
        }

        private void AddStackColor( Stack<string> theStack )
        {
            string stackName = theStack.Peek() as string;
            if ( !m_memoryLayoutStackNames.Contains( stackName ) )
            {
                m_memoryLayoutStackNames.Add( stackName );
            }

            string[] fullStack = theStack.ToArray() as string[];
            System.Text.StringBuilder pathBuilder = new System.Text.StringBuilder();
            for ( int i = fullStack.Length - 1; i >= 0; --i )
            {
                pathBuilder.Append( fullStack[i] );
                pathBuilder.Append( '\\' );
            }
            string path = pathBuilder.ToString();
            if ( !m_memoryLayoutFullStackPaths.Contains( path ) )
            {
                m_memoryLayoutFullStackPaths.Add( path );
            }
        }

        private void FillColorKeyListView()
        {
            this.colorKeyListView.Items.Clear();

            List<string> list = null;
            switch ( m_colorizeBy )
            {
                case DisplayType.StackNames:
                    list = m_memoryLayoutStackNames;
                    break;
                case DisplayType.FullStackPath:
                    list = m_memoryLayoutFullStackPaths;
                    break;
                case DisplayType.AllocationSize:
                    list = m_memoryLayoutDivisionSizes;
                    break;
                case DisplayType.DataType:
                    if ( m_sortedDataTypes.Count < c_colors.Count )
                    {
                        list = m_sortedDataTypes;
                    }
                    else
                    {
                        list = m_truncatedDataTypes;
                    }
                    break;
            }

            int i = 0;
            for ( i = 0; (i < list.Count) && (i < c_colors.Count - 1); ++i )
            {
                string text;
                if ( m_colorizeBy == DisplayType.AllocationSize )
                {
                    uint size = uint.Parse( list[i] );
                    text = "< " + size.ToString( "n" ).Replace( ".00", "" ) + " bytes";
                }
                else
                {
                    text = list[i];
                }

                this.colorKeyListView.Items.Add( new ListViewItem( text, i ) );
            }

            if ( i < list.Count )
            {
                System.Text.StringBuilder textBuilder = new System.Text.StringBuilder();
                if ( m_colorizeBy == DisplayType.AllocationSize )
                {
                    uint size = uint.Parse( list[list.Count - 1] );
                    textBuilder.Append( "< " );
                    textBuilder.Append( size.ToString( "n" ).Replace( ".00", "" ) );
                    textBuilder.Append( " bytes" );
                }
                else
                {
                    textBuilder.Append( list[i] );
                    for ( i = i + 1; i < list.Count; ++i )
                    {
                        textBuilder.Append( ", " );
                        textBuilder.Append( list[i] );
                    }
                }

                this.colorKeyListView.Items.Add( new ListViewItem( textBuilder.ToString(), c_colors.Count - 1 ) );
            }
        }

        private Point AddressToPoint( uint address )
        {
            int bytesAcross = (int)m_pixelWidth * (int)m_bytesPerPixel;

            uint row = address / (uint)bytesAcross;
            uint col = (address - (row * (uint)bytesAcross)) / (uint)m_bytesPerPixel;

            return new Point( (int)col, (int)row );
        }

        private uint PointToAddress( Point p )
        {
            int bytesAcross = (int)m_pixelWidth * (int)m_bytesPerPixel;

            uint address = ((uint)p.Y * (uint)bytesAcross) + ((uint)p.X * (uint)m_bytesPerPixel);

            return address;
        }

        private Allocation PointToAllocation( Point p )
        {
            uint address = PointToAddress( p );

            for ( int i = m_memoryLayoutAllocations.Count - 1; i >= 0; --i )
            {
                Allocation theAllocation = m_memoryLayoutAllocations[i];
                if ( (address >= theAllocation.DecimalAddress) && (address < theAllocation.DecimalAddress + theAllocation.Size) )
                {
                    return theAllocation;
                }
            }

            return null;
        }

        private Color AllocationToColor( Allocation theAllocation )
        {
            if ( m_colorizeBy == DisplayType.AllocationSize )
            {
                if ( m_memoryLayoutDivisionSizes.Count > 0 )
                {
                    int i = 0;
                    while ( (i < m_memoryLayoutDivisionSizes.Count) && (theAllocation.Size > Convert.ToUInt32( m_memoryLayoutDivisionSizes[i] )) )
                    {
                        ++i;
                    }

                    if ( (i < m_memoryLayoutDivisionSizes.Count) && (i < c_colors.Count - 1) )
                    {
                        return c_colors[i];
                    }
                }
            }
            else
            {
                List<string> list = null;
                string name = null;

                switch ( m_colorizeBy )
                {
                    case DisplayType.StackNames:
                        list = m_memoryLayoutStackNames;
                        name = theAllocation.StackName;
                        break;
                    case DisplayType.FullStackPath:
                        list = m_memoryLayoutFullStackPaths;
                        name = theAllocation.FullStackPath;
                        break;
                    case DisplayType.DataType:
                        if ( m_sortedDataTypes.Count < c_colors.Count )
                        {
                            list = m_sortedDataTypes;
                            name = theAllocation.TypeName;
                        }
                        else
                        {
                            list = m_truncatedDataTypes;

                            name = theAllocation.TypeName;
                            if ( name.StartsWith( "class" ) )
                            {
                                name = "Classes";
                            }
                            else if ( name.StartsWith( "struct" ) )
                            {
                                name = "Structures";
                            }
                        }
                        break;
                }

                int i = list.IndexOf( name );
                if ( (i > -1) && (i < c_colors.Count - 1) )
                {
                    return c_colors[i];
                }
            }

            return Color.DarkGray;
        }

        private List<string> GetDivisionSizes()
        {
            uint minSize = 4;
            if ( m_smallestAllocation != null )
            {
                minSize = m_smallestAllocation.Size;
            }

            uint maxSize = 1024 * 1024;
            if ( m_largestAllocation != null )
            {
                maxSize = m_largestAllocation.Size;
            }

            // start with 16, 32, 64, etc.
            List<string> sizes = new List<string>();
            uint s = 8;
            do
            {
                s *= 2;
                sizes.Add( s.ToString() );
            }
            while ( s <= maxSize );

            // then subdivide for each sizeScale above 0
            int scale = (int)m_sizeScale;
            while ( scale > 0 )
            {
                for ( int i = 1; i < sizes.Count; i += 2 )
                {
                    uint s0 = Convert.ToUInt32( sizes[i - 1] );
                    uint s1 = Convert.ToUInt32( sizes[i] );
                    s = ((s1 - s0) / 2) + s0;

                    sizes.Insert( i, s.ToString() );
                }

                --scale;
            }

            // trim allocations smaller than the minimum
            s = Convert.ToUInt32( sizes[0] );
            while ( s <= minSize )
            {
                sizes.RemoveAt( 0 );
                s = Convert.ToUInt32( sizes[0] );
            }

            // trim allocations larger than the maximum
            if ( sizes.Count > 0 )
            {
                uint oldS = 0;
                s = Convert.ToUInt32( sizes[sizes.Count - 1] );
                while ( s > maxSize )
                {
                    sizes.RemoveAt( sizes.Count - 1 );
                    oldS = s;

                    if ( sizes.Count > 0 )
                    {
                        s = Convert.ToUInt32( sizes[sizes.Count - 1] );
                    }
                    else
                    {
                        break;
                    }
                }
                sizes.Add( oldS.ToString() ); // add the last one back on
            }

            return sizes;
        }
        #endregion

        #region ContextMenu Event Handlers
        private void recenterToolStripMenuItem_Click( object sender, EventArgs e )
        {
            m_memoryLayoutPanelDrawOffset.X = 0;
            m_memoryLayoutPanelDrawOffset.Y = 0;

            this.memoryPanel.Invalidate();
        }
        #endregion

        #region MemoryPanel Event Handlers
        private void memoryPanel_MouseEnter( object sender, EventArgs e )
        {
            this.addressToolStripStatusLabel.Text = "Address:";
            this.allocationToolStripStatusLabel.Text = "Allocation:";
        }

        private void memoryPanel_MouseLeave( object sender, EventArgs e )
        {
            memoryPanel_MouseEnter( sender, e );

            m_memoryLayoutPanelDragging = false;
        }

        private void memoryPanel_MouseMove( object sender, MouseEventArgs e )
        {
            memoryPanel_MouseHover( sender, e );

            if ( m_memoryLayoutPanelDragging )
            {
                Point loc = this.memoryPanel.PointToClient( e.Location );
                int x = loc.X - m_memoryLayoutPanelDraggingStartPoint.X;
                int y = loc.Y - m_memoryLayoutPanelDraggingStartPoint.Y;

                m_memoryLayoutPanelDraggingStartPoint.X = loc.X;
                m_memoryLayoutPanelDraggingStartPoint.Y = loc.Y;

                m_memoryLayoutPanelDrawOffset.X += x;
                m_memoryLayoutPanelDrawOffset.Y += y;

                this.memoryPanel.Invalidate();
            }
        }

        private void memoryPanel_MouseHover( object sender, EventArgs e )
        {
            int offsetY = 0;
            if ( m_lowestAddressAllocation != null )
            {
                Point lowestStart = AddressToPoint( m_lowestAddressAllocation.DecimalAddress );
                offsetY = lowestStart.Y;
            }

            int lastY = 0;
            if ( m_highestAddressAllocation != null )
            {
                Point highestEnd = AddressToPoint( m_highestAddressAllocation.DecimalAddress + m_highestAddressAllocation.Size - 1 );
                lastY = highestEnd.Y;
            }

            Point p = this.memoryPanel.PointToClient( Control.MousePosition );
            int left = c_memoryGraphLeft + m_memoryLayoutPanelDrawOffset.X;
            int top = c_memoryGraphTop + m_memoryLayoutPanelDrawOffset.Y;
            p.X -= left;
            p.Y -= top - offsetY;

            if ( (p.X < 0) || (p.Y < 0) || (p.X > (int)m_pixelWidth) || (p.Y > lastY) )
            {
                memoryPanel_MouseEnter( sender, e );
            }
            else
            {
                uint address = PointToAddress( p );
                string hex = Convert.ToString( address, 16 );

                while ( hex.Length < 8 )
                {
                    hex = hex.Insert( 0, "0" );
                }

                hex = hex.Insert( 0, "0x" );

                string addressText = "Address:  " + hex;

                string allocationText = "Allocation:";
                Allocation theAllocation = PointToAllocation( p );
                if ( theAllocation != null )
                {
                    hex = theAllocation.Address;

                    while ( hex.Length < 8 )
                    {
                        hex = hex.Insert( 0, "0" );
                    }

                    hex = hex.Insert( 0, "0x" );

                    System.Text.StringBuilder text = new System.Text.StringBuilder();
                    text.Append( "Allocation: " );
                    text.Append( theAllocation.TypeName );
                    text.Append( " @ " );
                    text.Append( hex );
                    text.Append( "    " );
                    text.Append( theAllocation.Size.ToString( "n" ).Replace( ".00", "" ) );
                    text.Append( " bytes" );
                    text.Append( "    " );
                    text.Append( theAllocation.FullStackPath );

                    if ( theAllocation.FileName != string.Empty )
                    {
                        text.Append( " @ " );
                        text.Append( theAllocation.FileName );
                        text.Append( " line " );
                        text.Append( theAllocation.LineNumber.ToString() );
                    }

                    allocationText = text.ToString();
                }

                this.addressToolStripStatusLabel.Text = addressText;
                this.allocationToolStripStatusLabel.Text = allocationText;
            }
        }

        private void memoryPanel_MouseDown( object sender, MouseEventArgs e )
        {
            if ( e.Button == MouseButtons.Left )
            {
                m_memoryLayoutPanelDragging = true;
                m_memoryLayoutPanelDraggingStartPoint = this.memoryPanel.PointToClient( e.Location );

                this.memoryPanel.Cursor = Cursors.Hand;
            }
        }

        private void memoryPanel_MouseUp( object sender, MouseEventArgs e )
        {
            if ( e.Button == MouseButtons.Left )
            {
                m_memoryLayoutPanelDragging = false;

                this.memoryPanel.Cursor = Cursors.Default;
            }
        }

        private void memoryPanel_Paint( object sender, PaintEventArgs e )
        {
            try
            {
                Pen pen = new Pen( Color.Blue );
                pen.Width = 1.0f;

                int offsetY = 0;
                uint baseAddress = 0;
                if ( m_lowestAddressAllocation != null )
                {
                    baseAddress = m_lowestAddressAllocation.DecimalAddress;
                    Point smallestStart = AddressToPoint( m_lowestAddressAllocation.DecimalAddress );
                    offsetY = smallestStart.Y;
                }

                int left = c_memoryGraphLeft + m_memoryLayoutPanelDrawOffset.X;
                int top = c_memoryGraphTop + m_memoryLayoutPanelDrawOffset.Y;
                int textLeft = c_memoryGraphTextLeft + m_memoryLayoutPanelDrawOffset.X;

                // draw memory 
                foreach ( Allocation theAllocation in m_memoryLayoutAllocations )
                {
                    Point start = AddressToPoint( theAllocation.DecimalAddress );
                    pen.Color = AllocationToColor( theAllocation );

                    int pixels = (int)(theAllocation.Size / (uint)m_bytesPerPixel);
                    while ( pixels > 0 )
                    {
                        int width = (int)m_pixelWidth - start.X;
                        width = (pixels < width) ? pixels : width;
                        Point p1 = new Point( start.X + left, start.Y + top - offsetY );
                        Point p2 = new Point( start.X + width + left, start.Y + top - offsetY );

                        if ( (p1.X >= e.ClipRectangle.Left) && (p1.X <= e.ClipRectangle.Right)
                            && (p1.Y >= e.ClipRectangle.Top) && (p1.Y <= e.ClipRectangle.Bottom) )
                        {
                            e.Graphics.DrawLine( pen, p1, p2 );
                        }

                        pixels -= width;
                        start.X = 0;
                        start.Y++;
                    }
                }

                // draw addresses
                int lastY = 0;
                if ( m_highestAddressAllocation != null )
                {
                    Point largestEnd = AddressToPoint( m_highestAddressAllocation.DecimalAddress + m_highestAddressAllocation.Size - 1 );
                    lastY = largestEnd.Y;
                }

                Font font = new Font( FontFamily.GenericSansSerif, 8.0f );
                pen.Color = Color.Black;

                int y = offsetY;
                while ( y < lastY )
                {
                    uint address = PointToAddress( new Point( 0, y ) ); // don't subtract offsetY so we get the true address as if we weren't cropping the top of memory
                    string hex = Convert.ToString( address, 16 );

                    while ( hex.Length < 8 )
                    {
                        hex = hex.Insert( 0, "0" );
                    }

                    hex = hex.Insert( 0, "0x" );

                    e.Graphics.DrawString( hex, font, Brushes.Black, textLeft, y + top - offsetY - 6 );
                    e.Graphics.DrawLine( pen, left - 5, y + top - offsetY,
                        left - 2, y + top - offsetY );

                    y += 64;
                }
                font.Dispose();
            }
            catch
            {
            }
        }
        #endregion

        #region ToolStrip Event Handlers
        private void generateReportToolStripButton_Click( object sender, EventArgs e )
        {
            ReportItem report = m_trackerComponent.AddReportAction( 0, string.Empty,
                m_trackerComponent.AvailableMemory, m_trackerComponent.TrackDeallocations );

            FillData( report );
        }

        private void reportToolStripComboBox_SelectedIndexChanged( object sender, EventArgs e )
        {
            this.Report = this.reportToolStripComboBox.SelectedItem as ReportItem;
        }

        #region Options Drop Down Menu
        private void optionsToolStripDropDownButton_DropDownOpening( object sender, EventArgs e )
        {
            this.sizeScaleToolStripMenuItem.Enabled = this.ColorizeBy == DisplayType.AllocationSize;

            foreach ( ToolStripMenuItem item in this.sizeScaleToolStripMenuItem.DropDownItems )
            {
                item.Enabled = this.sizeScaleToolStripMenuItem.Enabled;
            }
        }

        private void colorizeByToolStripMenuItem_DropDownOpening( object sender, EventArgs e )
        {
            this.colorizeByStackNameToolStripMenuItem.Checked = this.ColorizeBy == DisplayType.StackNames;
            this.colorizeByFullStackPathToolStripMenuItem.Checked = this.ColorizeBy == DisplayType.FullStackPath;
            this.colorizeByAllocationSizeToolStripMenuItem.Checked = this.ColorizeBy == DisplayType.AllocationSize;
            this.colorizeByDatatypeToolStripMenuItem.Checked = this.ColorizeBy == DisplayType.DataType;
        }

        #region ColorizeBy Drop Down Menu
        private void colorizeByStackNameToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.ColorizeBy != DisplayType.StackNames )
            {
                this.ColorizeBy = DisplayType.StackNames;
                OnColorizeByChanged();
            }
        }

        private void colorizeByFullStackPathToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.ColorizeBy != DisplayType.FullStackPath )
            {
                this.ColorizeBy = DisplayType.FullStackPath;
                OnColorizeByChanged();
            }
        }

        private void colorizeByAllocationSizeToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.ColorizeBy != DisplayType.AllocationSize )
            {
                this.ColorizeBy = DisplayType.AllocationSize;
                OnColorizeByChanged();
            }
        }

        private void colorizeByDatatypeToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.ColorizeBy != DisplayType.DataType )
            {
                this.ColorizeBy = DisplayType.DataType;
                OnColorizeByChanged();
            }
        }
        #endregion

        private void bytesPerPixelToolStripMenuItem_DropDownOpening( object sender, EventArgs e )
        {
            this.bytesPerPixel4ToolStripMenuItem.Checked = this.BytesPerPixel == Bytes.Four;
            this.bytesPerPixel8ToolStripMenuItem.Checked = this.BytesPerPixel == Bytes.Eight;
            this.bytesPerPixel16ToolStripMenuItem.Checked = this.BytesPerPixel == Bytes.Sixteen;
            this.bytesPerPixel32ToolStripMenuItem.Checked = this.BytesPerPixel == Bytes.ThirtyTwo;
            this.bytesPerPixel64ToolStripMenuItem.Checked = this.BytesPerPixel == Bytes.SixtyFour;
            this.bytesPerPixel128ToolStripMenuItem.Checked = this.BytesPerPixel == Bytes.OneHundredTwentyEight;
            this.bytesPerPixel256ToolStripMenuItem.Checked = this.BytesPerPixel == Bytes.TwoHundredFiftySix;
            this.bytesPerPixel512ToolStripMenuItem.Checked = this.BytesPerPixel == Bytes.FiveHundredTwelve;
            this.bytesPerPixel1024ToolStripMenuItem.Checked = this.BytesPerPixel == Bytes.OneThousandTwentyFour;
        }

        #region BytesPerPixel Drop Down Menu
        private void bytesPerPixel4ToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.BytesPerPixel != Bytes.Four )
            {
                this.BytesPerPixel = Bytes.Four;
                OnBytesPerPixelChanged();
            }
        }

        private void bytesPerPixel8ToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.BytesPerPixel != Bytes.Eight )
            {
                this.BytesPerPixel = Bytes.Eight;
                OnBytesPerPixelChanged();
            }
        }

        private void bytesPerPixel16ToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.BytesPerPixel != Bytes.Sixteen )
            {
                this.BytesPerPixel = Bytes.Sixteen;
                OnBytesPerPixelChanged();
            }
        }

        private void bytesPerPixel32ToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.BytesPerPixel != Bytes.ThirtyTwo )
            {
                this.BytesPerPixel = Bytes.ThirtyTwo;
                OnBytesPerPixelChanged();
            }
        }

        private void bytesPerPixel64ToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.BytesPerPixel != Bytes.SixtyFour )
            {
                this.BytesPerPixel = Bytes.SixtyFour;
                OnBytesPerPixelChanged();
            }
        }

        private void bytesPerPixel128ToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.BytesPerPixel != Bytes.OneHundredTwentyEight )
            {
                this.BytesPerPixel = Bytes.OneHundredTwentyEight;
                OnBytesPerPixelChanged();
            }
        }

        private void bytesPerPixel256ToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.BytesPerPixel != Bytes.TwoHundredFiftySix )
            {
                this.BytesPerPixel = Bytes.TwoHundredFiftySix;
                OnBytesPerPixelChanged();
            }
        }

        private void bytesPerPixel512ToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.BytesPerPixel != Bytes.FiveHundredTwelve )
            {
                this.BytesPerPixel = Bytes.FiveHundredTwelve;
                OnBytesPerPixelChanged();
            }
        }

        private void bytesPerPixel1024ToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.BytesPerPixel != Bytes.OneThousandTwentyFour )
            {
                this.BytesPerPixel = Bytes.OneThousandTwentyFour;
                OnBytesPerPixelChanged();
            }
        }
        #endregion

        private void pixelWidthToolStripMenuItem_DropDownOpening( object sender, EventArgs e )
        {
            this.pixelWidth32ToolStripMenuItem.Checked = this.PixelWidth == Widths.ThirtyTwo;
            this.pixelWidth64ToolStripMenuItem.Checked = this.PixelWidth == Widths.SixtyFour;
            this.pixelWidth128ToolStripMenuItem.Checked = this.PixelWidth == Widths.OneHundredTwentyEight;
            this.pixelWidth256ToolStripMenuItem.Checked = this.PixelWidth == Widths.TwoHundredFiftySix;
            this.pixelWidth512ToolStripMenuItem.Checked = this.PixelWidth == Widths.FiveHundredTwelve;
            this.pixelWidth1024ToolStripMenuItem.Checked = this.PixelWidth == Widths.OneThousandTwentyFour;
        }

        #region PixelWidth Drop Down Menu
        private void pixelWidth32ToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.PixelWidth != Widths.ThirtyTwo )
            {
                this.PixelWidth = Widths.ThirtyTwo;
                OnPixelWidthChanged();
            }
        }

        private void pixelWidth64ToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.PixelWidth != Widths.SixtyFour )
            {
                this.PixelWidth = Widths.SixtyFour;
                OnPixelWidthChanged();
            }
        }

        private void pixelWidth128ToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.PixelWidth != Widths.OneHundredTwentyEight )
            {
                this.PixelWidth = Widths.OneHundredTwentyEight;
                OnPixelWidthChanged();
            }
        }

        private void pixelWidth256ToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.PixelWidth != Widths.TwoHundredFiftySix )
            {
                this.PixelWidth = Widths.TwoHundredFiftySix;
                OnPixelWidthChanged();
            }
        }

        private void pixelWidth512ToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.PixelWidth != Widths.FiveHundredTwelve )
            {
                this.PixelWidth = Widths.FiveHundredTwelve;
                OnPixelWidthChanged();
            }
        }

        private void pixelWidth1024ToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.PixelWidth != Widths.OneThousandTwentyFour )
            {
                this.PixelWidth = Widths.OneThousandTwentyFour;
                OnPixelWidthChanged();
            }
        }
        #endregion

        private void sizeScaleToolStripMenuItem_DropDownOpening( object sender, EventArgs e )
        {
            this.sizeScaleCoarseToolStripMenuItem.Checked = this.SizeScale == Sizes.Coarse;
            this.sizeScaleMediumToolStripMenuItem.Checked = this.SizeScale == Sizes.Medium;
            this.sizeScaleFineToolStripMenuItem.Checked = this.SizeScale == Sizes.Fine;
            this.sizeScaleVeryFineToolStripMenuItem.Checked = this.SizeScale == Sizes.VeryFine;
        }

        #region SizeScale Drop Down Menu
        private void sizeScaleCoarseToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.SizeScale != Sizes.Coarse )
            {
                this.SizeScale = Sizes.Coarse;
                OnSizeScaleChanged();
            }
        }

        private void sizeScaleMediumToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.SizeScale != Sizes.Medium )
            {
                this.SizeScale = Sizes.Medium;
                OnSizeScaleChanged();
            }
        }

        private void sizeScaleFineToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.SizeScale != Sizes.Fine )
            {
                this.SizeScale = Sizes.Fine;
                OnSizeScaleChanged();
            }
        }

        private void sizeScaleVeryFineToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.SizeScale != Sizes.VeryFine )
            {
                this.SizeScale = Sizes.VeryFine;
                OnSizeScaleChanged();
            }
        }
        #endregion

        #endregion

        private void refreshToolStripButton_Click( object sender, EventArgs e )
        {
            FillData();
        }
        #endregion

        #region TrackerComponent Event Handlers
        private void trackerComponent_ReportAdded( object sender, ReportChangedEventArgs e )
        {
            this.reportToolStripComboBox.Items.Add( e.Report );
        }

        private void trackerComponent_ReportRemoved( object sender, ReportChangedEventArgs e )
        {
            this.reportToolStripComboBox.Items.Remove( e.Report );
        }
        #endregion

        #region BackgroundWorker Event Handlers
        private void backgroundWorker_DoWork( object sender, DoWorkEventArgs e )
        {
            e.Result = GatherMemoryData();
        }

        private void backgroundWorker_ProgressChanged( object sender, ProgressChangedEventArgs e )
        {
            OnUpdateStep( e.ProgressPercentage, m_reportGenerated );
        }

        private void backgroundWorker_RunWorkerCompleted( object sender, RunWorkerCompletedEventArgs e )
        {
            // enable the toolstrip
            this.toolStrip1.Enabled = true;

            int result = (int)e.Result;
            if ( result == 1 )
            {
                // build the image list of colors
                if ( this.colorKeyImageList.Images.Count == 0 )
                {
                    int i = m_report.Index + 1;
                    foreach ( Color c in c_colors )
                    {
                        Bitmap img = new Bitmap( 16, 16 );
                        for ( int x = 0; x < 16; ++x )
                        {
                            for ( int y = 0; y < 16; ++y )
                            {
                                img.SetPixel( x, y, c );
                            }
                        }
                        this.colorKeyImageList.Images.Add( img );
                    }
                }

                this.memoryPanel.Invalidate();

                FillColorKeyListView();

                // disable the Refresh Button
                this.RefreshNeeded = false;
            }

            OnUpdateComplete( m_reportGenerated ? m_report : null, result == 1 );

            m_reportGenerated = false;
        }
        #endregion
    }
}
