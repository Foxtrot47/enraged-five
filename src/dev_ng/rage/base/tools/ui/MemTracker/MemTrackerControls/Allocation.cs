using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Text;

using GlacialComponents.Controls.GlacialTreeList;

namespace MemTrackerControls
{
    public class Allocation
    {
        public Allocation( int type, string address, uint allocSize )
        {
            m_memoryType = (MemoryTypeIndex)(type + 1); // add one because to the game, -1 is our undetermined memory type
            m_Address = address;
            m_DecimalAddress = uint.Parse( address, NumberStyles.HexNumber );
            Size = allocSize;
        }

        public Allocation( int type, uint decimalAddress, uint allocSize )
        {
            m_memoryType = (MemoryTypeIndex)(type + 1); // add one because to the game, -1 is our undetermined memory type
            m_Address = decimalAddress.ToString( "X" );
            m_DecimalAddress = decimalAddress;
            Size = allocSize;
        }

        public Allocation( MemoryTypeIndex typeIndex, string address, uint allocSize )
        {
            m_memoryType = typeIndex;
            m_Address = address;
            m_DecimalAddress = uint.Parse( address, NumberStyles.HexNumber );
            Size = allocSize;
        }

        public Allocation( MemoryTypeIndex typeIndex, uint decimalAddress, uint allocSize )
        {
            m_memoryType = typeIndex;
            m_Address = decimalAddress.ToString( "X" );
            m_DecimalAddress = decimalAddress;
            Size = allocSize;
        }

        #region Enums
        /// <summary>
        /// An enumeration for our different Memory Types
        /// </summary>
        public enum MemoryTypeIndex
        {
            Undetermined,
            GameVirtual,
            ResourceVirtual,
            GamePhysical,
            ResourcePhysical,
            ConsoleSpecific,
            NumIndexes
        }
        #endregion

        #region Static and Constant Properties
        /// <summary>
        /// The string for the Unknown datatype name.
        /// </summary>
        public const string Unknown = "(Unknown)";

        /// <summary>
        /// The string for the Unassigned stack name.
        /// </summary>
        public const string Unassigned = "(Unassigned)";

        /// <summary>
        /// The string names of each Memory Type
        /// </summary>
        public static string[] MemoryTypeNames = new string[]
        {
            "Undetermined",
            "Game Virtual",
	        "Resource Virtual",
	        "Game Physical",
            "Resource Physical",
	        "Xenon External",
        };

        /// <summary>
        /// Gets the dictionary of hash keys to Memory Type names.
        /// </summary>
        public static Dictionary<uint, string> MemoryTypeDictionary
        {
            get
            {
                return sm_memoryTypeDictionary;
            }
        }

        /// <summary>
        /// Gets the dictionary of hash keys to Datatype names.
        /// </summary>
        public static Dictionary<uint, string> DataTypeDictionary
        {
            get
            {
                return sm_dataTypeDictionary;
            }
        }

        /// <summary>
        /// Gets the dictionary of hash keys to Stack names.
        /// </summary>
        public static Dictionary<uint, string> StackNameDictionary
        {
            get
            {
                return sm_stackNameDictionary;
            }
        }

        /// <summary>
        /// Gets the dictionary of hash keys to file names.
        /// </summary>
        public static Dictionary<uint, string> FileNameDictionary
        {
            get
            {
                return sm_fileNameDictionary;
            }
        }
        #endregion

        #region Static Variables
        private static Dictionary<uint, string> sm_dataTypeDictionary = new Dictionary<uint, string>();
        private static Dictionary<uint, string> sm_fileNameDictionary = new Dictionary<uint, string>();
        private static Dictionary<uint, string> sm_stackNameDictionary = new Dictionary<uint, string>();
        private static Dictionary<uint, string> sm_fullStackPathDictionary = new Dictionary<uint, string>();
        private static Dictionary<uint, string> sm_memoryTypeDictionary = BuildMemoryTypeDictionary();

        private static Dictionary<uint, string> BuildMemoryTypeDictionary()
        {
            Dictionary<uint, string> dictionary = new Dictionary<uint, string>();

            foreach ( string memoryTypeName in Allocation.MemoryTypeNames )
            {
                dictionary.Add( (uint)memoryTypeName.GetHashCode(), memoryTypeName );
            }

            return dictionary;
        }
        #endregion

        #region Static Functions
        public static void Reset()
        {
            sm_dataTypeDictionary.Clear();
            sm_fileNameDictionary.Clear();
            sm_stackNameDictionary.Clear();
            sm_fullStackPathDictionary.Clear();
            sm_memoryTypeDictionary = BuildMemoryTypeDictionary();
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets the Memory Type name which this was allocated with.
        /// </summary>
        public string MemoryTypeName
        {
            get
            {
                return MemoryTypeNames[(int)m_memoryType];
            }
        }

        /// <summary>
        /// Gets the <see cref="MemoryTypeIndex"/> which this was allocated with.
        /// </summary>
        public MemoryTypeIndex MemoryType
        {
            get
            {
                return m_memoryType;
            }
        }

        /// <summary>
        /// Gets the hexidecimal string address.
        /// </summary>
        public string Address
        {
            get
            {
                return m_Address;
            }
        }

        /// <summary>
        /// Gets the decimal interger address.
        /// </summary>
        public uint DecimalAddress
        {
            get
            {
                return m_DecimalAddress;
            }
        }

        /// <summary>
        /// Gets or sets the size of the allocation.
        /// </summary>
        public uint Size;
        
        /// <summary>
        /// Gets or sets a list of the Stack names that contain the allocation.
        /// </summary>
        public string[] Stack
        {
            get
            {
                string fullPath;
                if ( sm_fullStackPathDictionary.TryGetValue( m_fullStackPathHashCode, out fullPath ) )
                {
                    string[] fullPathSplit = fullPath.Split( new char[] { '\\' }, StringSplitOptions.RemoveEmptyEntries );
                    string[] stack = new string[fullPathSplit.Length];
                    for ( int i = 0; i < stack.Length; i++ )
                    {
                        stack[i] = fullPathSplit[stack.Length - 1 - i];
                    }

                    return stack;
                }
                else
                {
                    return new string[] { Allocation.Unassigned };
                }
            }
            set
            {
                Debug.Assert( value != null );

                string stackName = Allocation.Unassigned;
                string fullPath = Allocation.Unassigned;
                if ( value.Length > 0 )
                {
                    stackName = value[value.Length - 1];

                    System.Text.StringBuilder stackPath = new System.Text.StringBuilder();
                    for ( int i = value.Length - 1; i >= 0; --i )
                    {
                        stackPath.Append( value[i] );
                        stackPath.Append( '\\' );
                    }

                    fullPath = stackPath.ToString();                    
                }

                m_stackNameHashCode = (uint)stackName.GetHashCode();
                sm_stackNameDictionary[m_stackNameHashCode] = stackName;

                m_fullStackPathHashCode = (uint)fullPath.GetHashCode();
                sm_fullStackPathDictionary[m_fullStackPathHashCode] = fullPath;
            }
        }
        
        /// <summary>
        /// Gets or sets the Datatype name.
        /// </summary>
        public string TypeName
        {
            get
            {
                string name;
                if ( sm_dataTypeDictionary.TryGetValue( m_dataTypeHashCode, out name ) )
                {
                    return name;
                }
                else
                {
                    return Allocation.Unknown;
                }
            }
            set
            {
                m_dataTypeHashCode = (uint)value.GetHashCode();

                if ( value != string.Empty )
                {
                    sm_dataTypeDictionary[m_dataTypeHashCode] = value;
                }
            }
        }
        
        /// <summary>
        /// Gets or sets the File name where the allocation was made.
        /// </summary>
        public string FileName
        {
            get
            {
                string name;
                if ( sm_fileNameDictionary.TryGetValue( m_fileNameHashCode, out name ) )
                {
                    return name;
                }
                else
                {
                    return string.Empty;
                }
            }
            set
            {
                m_fileNameHashCode = (uint)value.GetHashCode();

                if ( value != string.Empty )
                {
                    sm_fileNameDictionary[m_fileNameHashCode] = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets the hash key for the Datatype name.
        /// </summary>
        public uint TypeNameHashCode
        {
            get
            {
                return m_dataTypeHashCode;
            }
            set
            {
                m_dataTypeHashCode = value;
            }
        }

        /// <summary>
        /// Gets or sets the hash key for the File name.
        /// </summary>
        public uint FileNameHashCode
        {
            get
            {
                return m_fileNameHashCode;
            }
            set
            {
                m_fileNameHashCode = value;
            }
        }

        /// <summary>
        /// gets or sets the line number in the file where the allocation was made.
        /// </summary>
        public int LineNumber = 0;
        
        /// <summary>
        /// Gets or sets any sub-allocations, typically for an array.
        /// </summary>
        public List<Allocation> Items = null;

        /// <summary>
        /// Gets if this allocation is an array.
        /// </summary>
        public bool IsArray
        {
            get
            {
                return Items != null;
            }
        }

        /// <summary>
        /// Gets the name of the containing stack.
        /// </summary>
        public string StackName
        {
            get
            {
                string name;
                if ( sm_stackNameDictionary.TryGetValue( m_stackNameHashCode, out name ) )
                {
                    return name;
                }
                else
                {
                    return Allocation.Unassigned;
                }
            }
        }

        /// <summary>
        /// Gets the full stack path.
        /// </summary>
        public string FullStackPath
        {
            get
            {
                string name;
                if ( sm_fullStackPathDictionary.TryGetValue( m_fullStackPathHashCode, out name ) )
                {
                    return name;
                }
                else
                {
                    return Allocation.Unassigned;
                }
            }
        }

        /// <summary>
        /// Gets a string representing the extra file information.        
        /// </summary>
        /// <remarks>This string can be saved to a tracker file after pre-pending the thread ID and "In;" (separated by semi-colons).</remarks>
        public string InfoString
        {
            get
            {
                StringBuilder text = new StringBuilder();

                string filename = this.FileName;
                if ( filename != string.Empty )
                {
                    text.Append( this.Address );
                    text.Append( ";" );
                    text.Append( this.Size );
                    text.Append( ";" );
                    text.Append( this.TypeName );
                    text.Append( ";" );
                    text.Append( filename );
                    text.Append( ";" );
                    text.Append( this.LineNumber );
                }

                return text.ToString();
            }
        }
        #endregion

        #region Variables
        private MemoryTypeIndex m_memoryType;
        private string m_Address;
        private uint m_DecimalAddress;
        private uint m_dataTypeHashCode = (uint)string.Empty.GetHashCode();
        private uint m_fileNameHashCode = (uint)string.Empty.GetHashCode();
        private uint m_stackNameHashCode = (uint)Allocation.Unassigned.GetHashCode();
        private uint m_fullStackPathHashCode = (uint)Allocation.Unassigned.GetHashCode();
        #endregion

        #region Add/Remove Memory
        /// <summary>
        /// Adds this allocation's memory to the appropriate stack names in the dictionary.
        /// </summary>
        /// <param name="memory">The <see cref="SortedDictionary"/> of stack names to <see cref="AllocationStats"/> objects.</param>
        public void AddStackMemory( SortedDictionary<string, AllocationStats> memory )
        {
            string[] stacks = this.Stack;
            if ( stacks[0] == Allocation.Unassigned )
            {
                AllocationStats stats = memory[Allocation.Unassigned];
                stats.Inclusive += Size;
                stats.Exclusive += Size;
            }
            else
            {
                for ( int i = 0; i < stacks.Length; i++ )
                {
                    AllocationStats stats = memory[stacks[i]];
                    stats.Inclusive += Size;
                    if ( i == stacks.Length - 1 )
                    {
                        stats.Exclusive += Size;
                    }
                }
            }
        }

        /// <summary>
        /// Removes this allocation's memory to the appropriate stack names in the dictionary.
        /// </summary>
        /// <param name="memory">The <see cref="SortedDictionary"/> of stack names to <see cref="AllocationStats"/> objects.</param>
        public void RemoveStackMemory( SortedDictionary<string, AllocationStats> memory )
        {
            string[] stacks = this.Stack;
            if ( stacks[0] == Allocation.Unassigned )
            {
                AllocationStats stats = memory[Allocation.Unassigned];
                stats.Inclusive -= Size;
                stats.Exclusive -= Size;
            }
            else
            {
                for ( int i = 0; i < stacks.Length; i++ )
                {
                    AllocationStats stats = memory[stacks[i]];
                    stats.Inclusive -= Size;
                    if ( i == stacks.Length - 1 )
                    {
                        stats.Exclusive -= Size;
                    }
                }
            }
        }

        /// <summary>
        /// Adds this allocation's memory to the appropriate datatype names in the dictionary.
        /// </summary>
        /// <param name="memory">The <see cref="SortedDictionary"/> of datatype names to <see cref="AllocationStats"/> objects.</param>
        public void AddDataTypeMemory( SortedDictionary<string, AllocationStats> memory )
        {
            AllocationStats stats = memory[TypeName];
            stats.Inclusive += Size;
        }

        /// <summary>
        /// Removes this allocation's memory to the appropriate datatype names in the dictionary.
        /// </summary>
        /// <param name="memory">The <see cref="SortedDictionary"/> of datatype names to <see cref="AllocationStats"/> objects.</param>
        public void RemoveDataTypeMemory( SortedDictionary<string, AllocationStats> memory )
        {
            AllocationStats stats = memory[TypeName];
            stats.Inclusive -= Size;
        }

        /// <summary>
        /// Adds this allocation's memory to the appropriate memory type names in the dictionary.
        /// </summary>
        /// <param name="memory">The <see cref="SortedDictionary"/> of memory type names to <see cref="AllocationStats"/> objects.</param>
        public void AddMemoryTypeMemory( SortedDictionary<string, AllocationStats> memory )
        {
            AllocationStats stats = memory[MemoryTypeName];
            stats.Inclusive += Size;
        }

        /// <summary>
        /// Removes this allocation's memory to the appropriate memory type names in the dictionary.
        /// </summary>
        /// <param name="memory">The <see cref="SortedDictionary"/> of memory type names to <see cref="AllocationStats"/> objects.</param>
        public void RemoveMemoryTypeMemory( SortedDictionary<string, AllocationStats> memory )
        {
            AllocationStats stats = memory[MemoryTypeName];
            stats.Inclusive -= Size;
        }
        #endregion

        #region Overrides
        public override string ToString()
        {
            StringBuilder text = new StringBuilder();

            text.Append( this.Address );
            text.Append( ";" );
            text.Append( this.Size );
            text.Append( ";" );
            text.Append( (int)(this.MemoryType) - 1 ); // subtract 1 because that's what tracker.exe expects
            
            return text.ToString();
        }        
        #endregion
    }
}
