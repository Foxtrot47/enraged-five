namespace MemTrackerControls
{
    partial class AllocationsTreeListControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( m_trackerComponent != null )
            {
                m_trackerComponent.ReportAdded -= m_reportAddedEventHandler;
                m_trackerComponent.ReportRemoved -= m_reportRemovedEventHandler;
                m_trackerComponent.StackAdded -= m_stackAddedEventHandler;
                m_trackerComponent.DatatypeAdded -= m_datatypeAddedEventHandler;
            }

            if ( disposing && (components != null) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager( typeof( AllocationsTreeListControl ) );
            GlacialComponents.Controls.GlacialTreeList.GTLColumn gtlColumn1 = new GlacialComponents.Controls.GlacialTreeList.GTLColumn();
            GlacialComponents.Controls.GlacialTreeList.GTLColumn gtlColumn2 = new GlacialComponents.Controls.GlacialTreeList.GTLColumn();
            GlacialComponents.Controls.GlacialTreeList.GTLColumn gtlColumn3 = new GlacialComponents.Controls.GlacialTreeList.GTLColumn();
            GlacialComponents.Controls.GlacialTreeList.GTLColumn gtlColumn4 = new GlacialComponents.Controls.GlacialTreeList.GTLColumn();
            GlacialComponents.Controls.GlacialTreeList.GTLColumn gtlColumn5 = new GlacialComponents.Controls.GlacialTreeList.GTLColumn();
            GlacialComponents.Controls.GlacialTreeList.GTLColumn gtlColumn6 = new GlacialComponents.Controls.GlacialTreeList.GTLColumn();
            GlacialComponents.Controls.GlacialTreeList.GTLColumn gtlColumn7 = new GlacialComponents.Controls.GlacialTreeList.GTLColumn();
            GlacialComponents.Controls.GlacialTreeList.GTLColumn gtlColumn8 = new GlacialComponents.Controls.GlacialTreeList.GTLColumn();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip( this.components );
            this.copyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.collapseAllStacksToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.expandAllStacksToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.generateReportToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.reportToolStripLabel = new System.Windows.Forms.ToolStripLabel();
            this.reportToolStripComboBox = new System.Windows.Forms.ToolStripComboBox();
            this.minSizeToolStripLabel = new System.Windows.Forms.ToolStripLabel();
            this.minSizeToolStripTextBox = new System.Windows.Forms.ToolStripTextBox();
            this.maxSizeToolStripLabel = new System.Windows.Forms.ToolStripLabel();
            this.maxSizeToolStripTextBox = new System.Windows.Forms.ToolStripTextBox();
            this.optionsToolStripDropDownButton = new System.Windows.Forms.ToolStripDropDownButton();
            this.groupByToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupByAllocationOrderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupByStackNameToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.groupByAllocationsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupByDeallocationsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stacksToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stacksAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stacksNoneToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.stacksUnassignedToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.stacksSelectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.datatypesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.datatypesAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.datatypesNoneToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.datatypesNativeTypesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.datatypesClassesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.datatypesStructuresToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.datatypesUnknownTypesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator9 = new System.Windows.Forms.ToolStripSeparator();
            this.datatypesSelectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.memoryTypesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.memoryTypesAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.memoryTypesNoneToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator10 = new System.Windows.Forms.ToolStripSeparator();
            this.memoryTypesGameVirtualToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.memoryTypesResourceVirtualToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.memoryTypesGamePhysicalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.memoryTypesResourcePhysicalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.memoryTypesConsoleSpecificlToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.memoryTypesUndeterminedToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.multiSelectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.refreshToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.groupByToolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.selectedStacksToolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.selectedDatatypesToolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.selectedMemoryTypesToolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.sizeRangeToolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.selectedSizeToolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.glacialTreeList1 = new GlacialComponents.Controls.GlacialTreeList.GlacialTreeList();
            this.backgroundWorker = new System.ComponentModel.BackgroundWorker();
            this.contextMenuStrip1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.copyToolStripMenuItem,
            this.toolStripSeparator1,
            this.collapseAllStacksToolStripMenuItem,
            this.expandAllStacksToolStripMenuItem} );
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.ShowImageMargin = false;
            this.contextMenuStrip1.Size = new System.Drawing.Size( 149, 76 );
            // 
            // copyToolStripMenuItem
            // 
            this.copyToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.copyToolStripMenuItem.Name = "copyToolStripMenuItem";
            this.copyToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.C)));
            this.copyToolStripMenuItem.Size = new System.Drawing.Size( 148, 22 );
            this.copyToolStripMenuItem.Text = "&Copy";
            this.copyToolStripMenuItem.Click += new System.EventHandler( this.copyToolStripMenuItem_Click );
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size( 145, 6 );
            // 
            // collapseAllStacksToolStripMenuItem
            // 
            this.collapseAllStacksToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.collapseAllStacksToolStripMenuItem.Name = "collapseAllStacksToolStripMenuItem";
            this.collapseAllStacksToolStripMenuItem.Size = new System.Drawing.Size( 148, 22 );
            this.collapseAllStacksToolStripMenuItem.Text = "Collapse All &Stacks";
            this.collapseAllStacksToolStripMenuItem.Click += new System.EventHandler( this.collapseAllStacksToolStripMenuItem_Click );
            // 
            // expandAllStacksToolStripMenuItem
            // 
            this.expandAllStacksToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.expandAllStacksToolStripMenuItem.Name = "expandAllStacksToolStripMenuItem";
            this.expandAllStacksToolStripMenuItem.Size = new System.Drawing.Size( 148, 22 );
            this.expandAllStacksToolStripMenuItem.Text = "&Expand All Stacks";
            this.expandAllStacksToolStripMenuItem.Click += new System.EventHandler( this.expandAllStacksToolStripMenuItem_Click );
            // 
            // toolStrip1
            // 
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.Items.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.generateReportToolStripButton,
            this.toolStripSeparator2,
            this.reportToolStripLabel,
            this.reportToolStripComboBox,
            this.minSizeToolStripLabel,
            this.minSizeToolStripTextBox,
            this.maxSizeToolStripLabel,
            this.maxSizeToolStripTextBox,
            this.optionsToolStripDropDownButton,
            this.toolStripSeparator4,
            this.refreshToolStripButton} );
            this.toolStrip1.Location = new System.Drawing.Point( 0, 0 );
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.ShowItemToolTips = false;
            this.toolStrip1.Size = new System.Drawing.Size( 1116, 25 );
            this.toolStrip1.TabIndex = 1;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // generateReportToolStripButton
            // 
            this.generateReportToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.generateReportToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject( "generateReportToolStripButton.Image" )));
            this.generateReportToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.generateReportToolStripButton.Name = "generateReportToolStripButton";
            this.generateReportToolStripButton.Size = new System.Drawing.Size( 92, 22 );
            this.generateReportToolStripButton.Text = "Generate Report";
            this.generateReportToolStripButton.Click += new System.EventHandler( this.generateReportToolStripButton_Click );
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size( 6, 25 );
            // 
            // reportToolStripLabel
            // 
            this.reportToolStripLabel.AutoSize = false;
            this.reportToolStripLabel.Name = "reportToolStripLabel";
            this.reportToolStripLabel.Size = new System.Drawing.Size( 50, 22 );
            this.reportToolStripLabel.Text = "Report:";
            this.reportToolStripLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // reportToolStripComboBox
            // 
            this.reportToolStripComboBox.Name = "reportToolStripComboBox";
            this.reportToolStripComboBox.Size = new System.Drawing.Size( 220, 25 );
            this.reportToolStripComboBox.SelectedIndexChanged += new System.EventHandler( this.reportToolStripComboBox_SelectedIndexChanged );
            // 
            // minSizeToolStripLabel
            // 
            this.minSizeToolStripLabel.AutoSize = false;
            this.minSizeToolStripLabel.Name = "minSizeToolStripLabel";
            this.minSizeToolStripLabel.Size = new System.Drawing.Size( 60, 22 );
            this.minSizeToolStripLabel.Text = "Min Size:";
            this.minSizeToolStripLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // minSizeToolStripTextBox
            // 
            this.minSizeToolStripTextBox.AutoSize = false;
            this.minSizeToolStripTextBox.MaxLength = 9;
            this.minSizeToolStripTextBox.Name = "minSizeToolStripTextBox";
            this.minSizeToolStripTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.minSizeToolStripTextBox.Size = new System.Drawing.Size( 70, 25 );
            this.minSizeToolStripTextBox.Text = "0";
            this.minSizeToolStripTextBox.TextBoxTextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.minSizeToolStripTextBox.TextChanged += new System.EventHandler( this.minSizeToolStripTextBox_TextChanged );
            // 
            // maxSizeToolStripLabel
            // 
            this.maxSizeToolStripLabel.AutoSize = false;
            this.maxSizeToolStripLabel.Name = "maxSizeToolStripLabel";
            this.maxSizeToolStripLabel.Size = new System.Drawing.Size( 60, 22 );
            this.maxSizeToolStripLabel.Text = "Max Size:";
            this.maxSizeToolStripLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // maxSizeToolStripTextBox
            // 
            this.maxSizeToolStripTextBox.AutoSize = false;
            this.maxSizeToolStripTextBox.MaxLength = 9;
            this.maxSizeToolStripTextBox.Name = "maxSizeToolStripTextBox";
            this.maxSizeToolStripTextBox.Size = new System.Drawing.Size( 70, 25 );
            this.maxSizeToolStripTextBox.Text = "999999999";
            this.maxSizeToolStripTextBox.TextBoxTextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.maxSizeToolStripTextBox.TextChanged += new System.EventHandler( this.maxSizeToolStripTextBox_TextChanged );
            // 
            // optionsToolStripDropDownButton
            // 
            this.optionsToolStripDropDownButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.optionsToolStripDropDownButton.DropDownItems.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.groupByToolStripMenuItem,
            this.stacksToolStripMenuItem,
            this.datatypesToolStripMenuItem,
            this.memoryTypesToolStripMenuItem,
            this.multiSelectToolStripMenuItem} );
            this.optionsToolStripDropDownButton.Image = ((System.Drawing.Image)(resources.GetObject( "optionsToolStripDropDownButton.Image" )));
            this.optionsToolStripDropDownButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.optionsToolStripDropDownButton.Name = "optionsToolStripDropDownButton";
            this.optionsToolStripDropDownButton.Size = new System.Drawing.Size( 57, 22 );
            this.optionsToolStripDropDownButton.Text = "Options";
            this.optionsToolStripDropDownButton.DropDownOpening += new System.EventHandler( this.optionsToolStripDropDownButton_DropDownOpening );
            // 
            // groupByToolStripMenuItem
            // 
            this.groupByToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.groupByToolStripMenuItem.DropDownItems.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.groupByAllocationOrderToolStripMenuItem,
            this.groupByStackNameToolStripMenuItem,
            this.toolStripSeparator5,
            this.groupByAllocationsToolStripMenuItem,
            this.groupByDeallocationsToolStripMenuItem} );
            this.groupByToolStripMenuItem.Name = "groupByToolStripMenuItem";
            this.groupByToolStripMenuItem.Size = new System.Drawing.Size( 155, 22 );
            this.groupByToolStripMenuItem.Text = "Group &By";
            this.groupByToolStripMenuItem.DropDownOpening += new System.EventHandler( this.groupByToolStripMenuItem_DropDownOpening );
            // 
            // groupByAllocationOrderToolStripMenuItem
            // 
            this.groupByAllocationOrderToolStripMenuItem.CheckOnClick = true;
            this.groupByAllocationOrderToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.groupByAllocationOrderToolStripMenuItem.Name = "groupByAllocationOrderToolStripMenuItem";
            this.groupByAllocationOrderToolStripMenuItem.Size = new System.Drawing.Size( 162, 22 );
            this.groupByAllocationOrderToolStripMenuItem.Text = "Allocation &Order";
            this.groupByAllocationOrderToolStripMenuItem.Click += new System.EventHandler( this.groupByAllocationOrderToolStripMenuItem_Click );
            // 
            // groupByStackNameToolStripMenuItem
            // 
            this.groupByStackNameToolStripMenuItem.CheckOnClick = true;
            this.groupByStackNameToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.groupByStackNameToolStripMenuItem.Name = "groupByStackNameToolStripMenuItem";
            this.groupByStackNameToolStripMenuItem.Size = new System.Drawing.Size( 162, 22 );
            this.groupByStackNameToolStripMenuItem.Text = "Stack &Name";
            this.groupByStackNameToolStripMenuItem.Click += new System.EventHandler( this.groupByStackNameToolStripMenuItem_Click );
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size( 159, 6 );
            // 
            // groupByAllocationsToolStripMenuItem
            // 
            this.groupByAllocationsToolStripMenuItem.CheckOnClick = true;
            this.groupByAllocationsToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.groupByAllocationsToolStripMenuItem.Name = "groupByAllocationsToolStripMenuItem";
            this.groupByAllocationsToolStripMenuItem.Size = new System.Drawing.Size( 162, 22 );
            this.groupByAllocationsToolStripMenuItem.Text = "&Allocations";
            this.groupByAllocationsToolStripMenuItem.Click += new System.EventHandler( this.groupByAllocationsToolStripMenuItem_Click );
            // 
            // groupByDeallocationsToolStripMenuItem
            // 
            this.groupByDeallocationsToolStripMenuItem.CheckOnClick = true;
            this.groupByDeallocationsToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.groupByDeallocationsToolStripMenuItem.Name = "groupByDeallocationsToolStripMenuItem";
            this.groupByDeallocationsToolStripMenuItem.Size = new System.Drawing.Size( 162, 22 );
            this.groupByDeallocationsToolStripMenuItem.Text = "&Deallocations";
            this.groupByDeallocationsToolStripMenuItem.Click += new System.EventHandler( this.groupByDeallocationsToolStripMenuItem_Click );
            // 
            // stacksToolStripMenuItem
            // 
            this.stacksToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.stacksToolStripMenuItem.DropDownItems.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.stacksAllToolStripMenuItem,
            this.stacksNoneToolStripMenuItem,
            this.toolStripSeparator6,
            this.stacksUnassignedToolStripMenuItem,
            this.toolStripSeparator7,
            this.stacksSelectToolStripMenuItem} );
            this.stacksToolStripMenuItem.Name = "stacksToolStripMenuItem";
            this.stacksToolStripMenuItem.Size = new System.Drawing.Size( 155, 22 );
            this.stacksToolStripMenuItem.Text = "&Stacks";
            this.stacksToolStripMenuItem.DropDownOpening += new System.EventHandler( this.stacksToolStripMenuItem_DropDownOpening );
            // 
            // stacksAllToolStripMenuItem
            // 
            this.stacksAllToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.stacksAllToolStripMenuItem.Name = "stacksAllToolStripMenuItem";
            this.stacksAllToolStripMenuItem.Size = new System.Drawing.Size( 140, 22 );
            this.stacksAllToolStripMenuItem.Text = "&All";
            this.stacksAllToolStripMenuItem.Click += new System.EventHandler( this.stacksAllToolStripMenuItem_Click );
            // 
            // stacksNoneToolStripMenuItem
            // 
            this.stacksNoneToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.stacksNoneToolStripMenuItem.Name = "stacksNoneToolStripMenuItem";
            this.stacksNoneToolStripMenuItem.Size = new System.Drawing.Size( 140, 22 );
            this.stacksNoneToolStripMenuItem.Text = "&None";
            this.stacksNoneToolStripMenuItem.Click += new System.EventHandler( this.stacksNoneToolStripMenuItem_Click );
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size( 137, 6 );
            // 
            // stacksUnassignedToolStripMenuItem
            // 
            this.stacksUnassignedToolStripMenuItem.CheckOnClick = true;
            this.stacksUnassignedToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.stacksUnassignedToolStripMenuItem.Name = "stacksUnassignedToolStripMenuItem";
            this.stacksUnassignedToolStripMenuItem.Size = new System.Drawing.Size( 140, 22 );
            this.stacksUnassignedToolStripMenuItem.Text = "&Unassigned";
            this.stacksUnassignedToolStripMenuItem.Click += new System.EventHandler( this.stacksUnassignedToolStripMenuItem_Click );
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size( 137, 6 );
            // 
            // stacksSelectToolStripMenuItem
            // 
            this.stacksSelectToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.stacksSelectToolStripMenuItem.Name = "stacksSelectToolStripMenuItem";
            this.stacksSelectToolStripMenuItem.Size = new System.Drawing.Size( 140, 22 );
            this.stacksSelectToolStripMenuItem.Text = "&Select...";
            this.stacksSelectToolStripMenuItem.Click += new System.EventHandler( this.stacksSelectToolStripMenuItem_Click );
            // 
            // datatypesToolStripMenuItem
            // 
            this.datatypesToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.datatypesToolStripMenuItem.DropDownItems.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.datatypesAllToolStripMenuItem,
            this.datatypesNoneToolStripMenuItem,
            this.toolStripSeparator8,
            this.datatypesNativeTypesToolStripMenuItem,
            this.datatypesClassesToolStripMenuItem,
            this.datatypesStructuresToolStripMenuItem,
            this.datatypesUnknownTypesToolStripMenuItem,
            this.toolStripSeparator9,
            this.datatypesSelectToolStripMenuItem} );
            this.datatypesToolStripMenuItem.Name = "datatypesToolStripMenuItem";
            this.datatypesToolStripMenuItem.Size = new System.Drawing.Size( 155, 22 );
            this.datatypesToolStripMenuItem.Text = "&Datatypes";
            this.datatypesToolStripMenuItem.DropDownOpening += new System.EventHandler( this.datatypesToolStripMenuItem_DropDownOpening );
            // 
            // datatypesAllToolStripMenuItem
            // 
            this.datatypesAllToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.datatypesAllToolStripMenuItem.Name = "datatypesAllToolStripMenuItem";
            this.datatypesAllToolStripMenuItem.Size = new System.Drawing.Size( 161, 22 );
            this.datatypesAllToolStripMenuItem.Text = "&All";
            this.datatypesAllToolStripMenuItem.Click += new System.EventHandler( this.datatypesAllToolStripMenuItem_Click );
            // 
            // datatypesNoneToolStripMenuItem
            // 
            this.datatypesNoneToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.datatypesNoneToolStripMenuItem.Name = "datatypesNoneToolStripMenuItem";
            this.datatypesNoneToolStripMenuItem.Size = new System.Drawing.Size( 161, 22 );
            this.datatypesNoneToolStripMenuItem.Text = "&None";
            this.datatypesNoneToolStripMenuItem.Click += new System.EventHandler( this.datatypesNoneToolStripMenuItem_Click );
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size( 158, 6 );
            // 
            // datatypesNativeTypesToolStripMenuItem
            // 
            this.datatypesNativeTypesToolStripMenuItem.CheckOnClick = true;
            this.datatypesNativeTypesToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.datatypesNativeTypesToolStripMenuItem.Name = "datatypesNativeTypesToolStripMenuItem";
            this.datatypesNativeTypesToolStripMenuItem.Size = new System.Drawing.Size( 161, 22 );
            this.datatypesNativeTypesToolStripMenuItem.Text = "&Native Types";
            this.datatypesNativeTypesToolStripMenuItem.Click += new System.EventHandler( this.datatypesNativeTypesToolStripMenuItem_Click );
            // 
            // datatypesClassesToolStripMenuItem
            // 
            this.datatypesClassesToolStripMenuItem.CheckOnClick = true;
            this.datatypesClassesToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.datatypesClassesToolStripMenuItem.Name = "datatypesClassesToolStripMenuItem";
            this.datatypesClassesToolStripMenuItem.Size = new System.Drawing.Size( 161, 22 );
            this.datatypesClassesToolStripMenuItem.Text = "&Classes";
            this.datatypesClassesToolStripMenuItem.Click += new System.EventHandler( this.datatypesClassesToolStripMenuItem_Click );
            // 
            // datatypesStructuresToolStripMenuItem
            // 
            this.datatypesStructuresToolStripMenuItem.CheckOnClick = true;
            this.datatypesStructuresToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.datatypesStructuresToolStripMenuItem.Name = "datatypesStructuresToolStripMenuItem";
            this.datatypesStructuresToolStripMenuItem.Size = new System.Drawing.Size( 161, 22 );
            this.datatypesStructuresToolStripMenuItem.Text = "S&tructures";
            this.datatypesStructuresToolStripMenuItem.Click += new System.EventHandler( this.datatypesStructuresToolStripMenuItem_Click );
            // 
            // datatypesUnknownTypesToolStripMenuItem
            // 
            this.datatypesUnknownTypesToolStripMenuItem.CheckOnClick = true;
            this.datatypesUnknownTypesToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.datatypesUnknownTypesToolStripMenuItem.Name = "datatypesUnknownTypesToolStripMenuItem";
            this.datatypesUnknownTypesToolStripMenuItem.Size = new System.Drawing.Size( 161, 22 );
            this.datatypesUnknownTypesToolStripMenuItem.Text = "&Unknown Types";
            this.datatypesUnknownTypesToolStripMenuItem.Click += new System.EventHandler( this.datatypesUnknownTypesToolStripMenuItem_Click );
            // 
            // toolStripSeparator9
            // 
            this.toolStripSeparator9.Name = "toolStripSeparator9";
            this.toolStripSeparator9.Size = new System.Drawing.Size( 158, 6 );
            // 
            // datatypesSelectToolStripMenuItem
            // 
            this.datatypesSelectToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.datatypesSelectToolStripMenuItem.Name = "datatypesSelectToolStripMenuItem";
            this.datatypesSelectToolStripMenuItem.Size = new System.Drawing.Size( 161, 22 );
            this.datatypesSelectToolStripMenuItem.Text = "&Select...";
            this.datatypesSelectToolStripMenuItem.Click += new System.EventHandler( this.datatypesSelectToolStripMenuItem_Click );
            // 
            // memoryTypesToolStripMenuItem
            // 
            this.memoryTypesToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.memoryTypesToolStripMenuItem.DropDownItems.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.memoryTypesAllToolStripMenuItem,
            this.memoryTypesNoneToolStripMenuItem,
            this.toolStripSeparator10,
            this.memoryTypesGameVirtualToolStripMenuItem,
            this.memoryTypesResourceVirtualToolStripMenuItem,
            this.memoryTypesGamePhysicalToolStripMenuItem,
            this.memoryTypesResourcePhysicalToolStripMenuItem,
            this.memoryTypesConsoleSpecificlToolStripMenuItem,
            this.memoryTypesUndeterminedToolStripMenuItem} );
            this.memoryTypesToolStripMenuItem.Name = "memoryTypesToolStripMenuItem";
            this.memoryTypesToolStripMenuItem.Size = new System.Drawing.Size( 155, 22 );
            this.memoryTypesToolStripMenuItem.Text = "&Memory Types";
            this.memoryTypesToolStripMenuItem.DropDownOpening += new System.EventHandler( this.memoryTypesToolStripMenuItem_DropDownOpening );
            // 
            // memoryTypesAllToolStripMenuItem
            // 
            this.memoryTypesAllToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.memoryTypesAllToolStripMenuItem.Name = "memoryTypesAllToolStripMenuItem";
            this.memoryTypesAllToolStripMenuItem.Size = new System.Drawing.Size( 171, 22 );
            this.memoryTypesAllToolStripMenuItem.Text = "&All";
            this.memoryTypesAllToolStripMenuItem.Click += new System.EventHandler( this.memoryTypesAllToolStripMenuItem_Click );
            // 
            // memoryTypesNoneToolStripMenuItem
            // 
            this.memoryTypesNoneToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.memoryTypesNoneToolStripMenuItem.Name = "memoryTypesNoneToolStripMenuItem";
            this.memoryTypesNoneToolStripMenuItem.Size = new System.Drawing.Size( 171, 22 );
            this.memoryTypesNoneToolStripMenuItem.Text = "&None";
            this.memoryTypesNoneToolStripMenuItem.Click += new System.EventHandler( this.memoryTypesNoneToolStripMenuItem_Click );
            // 
            // toolStripSeparator10
            // 
            this.toolStripSeparator10.Name = "toolStripSeparator10";
            this.toolStripSeparator10.Size = new System.Drawing.Size( 168, 6 );
            // 
            // memoryTypesGameVirtualToolStripMenuItem
            // 
            this.memoryTypesGameVirtualToolStripMenuItem.CheckOnClick = true;
            this.memoryTypesGameVirtualToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.memoryTypesGameVirtualToolStripMenuItem.Name = "memoryTypesGameVirtualToolStripMenuItem";
            this.memoryTypesGameVirtualToolStripMenuItem.Size = new System.Drawing.Size( 171, 22 );
            this.memoryTypesGameVirtualToolStripMenuItem.Text = "&Game Virtual";
            this.memoryTypesGameVirtualToolStripMenuItem.Click += new System.EventHandler( this.memoryTypesGameVirtualToolStripMenuItem_Click );
            // 
            // memoryTypesResourceVirtualToolStripMenuItem
            // 
            this.memoryTypesResourceVirtualToolStripMenuItem.CheckOnClick = true;
            this.memoryTypesResourceVirtualToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.memoryTypesResourceVirtualToolStripMenuItem.Name = "memoryTypesResourceVirtualToolStripMenuItem";
            this.memoryTypesResourceVirtualToolStripMenuItem.Size = new System.Drawing.Size( 171, 22 );
            this.memoryTypesResourceVirtualToolStripMenuItem.Text = "&Resource Virtual";
            this.memoryTypesResourceVirtualToolStripMenuItem.Click += new System.EventHandler( this.memoryTypesResourceVirtualToolStripMenuItem_Click );
            // 
            // memoryTypesGamePhysicalToolStripMenuItem
            // 
            this.memoryTypesGamePhysicalToolStripMenuItem.CheckOnClick = true;
            this.memoryTypesGamePhysicalToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.memoryTypesGamePhysicalToolStripMenuItem.Name = "memoryTypesGamePhysicalToolStripMenuItem";
            this.memoryTypesGamePhysicalToolStripMenuItem.Size = new System.Drawing.Size( 171, 22 );
            this.memoryTypesGamePhysicalToolStripMenuItem.Text = "G&ame Physical";
            this.memoryTypesGamePhysicalToolStripMenuItem.Click += new System.EventHandler( this.memoryTypesGamePhysicalToolStripMenuItem_Click );
            // 
            // memoryTypesResourcePhysicalToolStripMenuItem
            // 
            this.memoryTypesResourcePhysicalToolStripMenuItem.CheckOnClick = true;
            this.memoryTypesResourcePhysicalToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.memoryTypesResourcePhysicalToolStripMenuItem.Name = "memoryTypesResourcePhysicalToolStripMenuItem";
            this.memoryTypesResourcePhysicalToolStripMenuItem.Size = new System.Drawing.Size( 171, 22 );
            this.memoryTypesResourcePhysicalToolStripMenuItem.Text = "R&esource Physical";
            this.memoryTypesResourcePhysicalToolStripMenuItem.Click += new System.EventHandler( this.memoryTypesResourcePhysicalToolStripMenuItem_Click );
            // 
            // memoryTypesConsoleSpecificlToolStripMenuItem
            // 
            this.memoryTypesConsoleSpecificlToolStripMenuItem.CheckOnClick = true;
            this.memoryTypesConsoleSpecificlToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.memoryTypesConsoleSpecificlToolStripMenuItem.Name = "memoryTypesConsoleSpecificlToolStripMenuItem";
            this.memoryTypesConsoleSpecificlToolStripMenuItem.Size = new System.Drawing.Size( 171, 22 );
            this.memoryTypesConsoleSpecificlToolStripMenuItem.Text = "&Xenon External";
            this.memoryTypesConsoleSpecificlToolStripMenuItem.Click += new System.EventHandler( this.memoryTypesConsoleSpecificToolStripMenuItem_Click );
            // 
            // memoryTypesUndeterminedToolStripMenuItem
            // 
            this.memoryTypesUndeterminedToolStripMenuItem.CheckOnClick = true;
            this.memoryTypesUndeterminedToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.memoryTypesUndeterminedToolStripMenuItem.Name = "memoryTypesUndeterminedToolStripMenuItem";
            this.memoryTypesUndeterminedToolStripMenuItem.Size = new System.Drawing.Size( 171, 22 );
            this.memoryTypesUndeterminedToolStripMenuItem.Text = "&Undetermined";
            this.memoryTypesUndeterminedToolStripMenuItem.Click += new System.EventHandler( this.memoryTypesUndeterminedToolStripMenuItem_Click );
            // 
            // multiSelectToolStripMenuItem
            // 
            this.multiSelectToolStripMenuItem.CheckOnClick = true;
            this.multiSelectToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.multiSelectToolStripMenuItem.Name = "multiSelectToolStripMenuItem";
            this.multiSelectToolStripMenuItem.Size = new System.Drawing.Size( 155, 22 );
            this.multiSelectToolStripMenuItem.Text = "M&ulti-Select";
            this.multiSelectToolStripMenuItem.Click += new System.EventHandler( this.multiSelectToolStripMenuItem_Click );
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size( 6, 25 );
            // 
            // refreshToolStripButton
            // 
            this.refreshToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.refreshToolStripButton.Enabled = false;
            this.refreshToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject( "refreshToolStripButton.Image" )));
            this.refreshToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.refreshToolStripButton.Name = "refreshToolStripButton";
            this.refreshToolStripButton.Size = new System.Drawing.Size( 49, 22 );
            this.refreshToolStripButton.Text = "Refresh";
            this.refreshToolStripButton.Click += new System.EventHandler( this.refreshToolStripButton_Click );
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.groupByToolStripStatusLabel,
            this.selectedStacksToolStripStatusLabel,
            this.selectedDatatypesToolStripStatusLabel,
            this.selectedMemoryTypesToolStripStatusLabel,
            this.sizeRangeToolStripStatusLabel,
            this.selectedSizeToolStripStatusLabel} );
            this.statusStrip1.Location = new System.Drawing.Point( 0, 331 );
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size( 1116, 22 );
            this.statusStrip1.TabIndex = 3;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // groupByToolStripStatusLabel
            // 
            this.groupByToolStripStatusLabel.AutoSize = false;
            this.groupByToolStripStatusLabel.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top)
                        | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right)
                        | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.groupByToolStripStatusLabel.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter;
            this.groupByToolStripStatusLabel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.groupByToolStripStatusLabel.Name = "groupByToolStripStatusLabel";
            this.groupByToolStripStatusLabel.Size = new System.Drawing.Size( 215, 17 );
            this.groupByToolStripStatusLabel.Text = "Group By:";
            this.groupByToolStripStatusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // selectedStacksToolStripStatusLabel
            // 
            this.selectedStacksToolStripStatusLabel.AutoSize = false;
            this.selectedStacksToolStripStatusLabel.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top)
                        | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right)
                        | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.selectedStacksToolStripStatusLabel.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter;
            this.selectedStacksToolStripStatusLabel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.selectedStacksToolStripStatusLabel.Name = "selectedStacksToolStripStatusLabel";
            this.selectedStacksToolStripStatusLabel.Size = new System.Drawing.Size( 93, 17 );
            this.selectedStacksToolStripStatusLabel.Text = "Stacks:";
            this.selectedStacksToolStripStatusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // selectedDatatypesToolStripStatusLabel
            // 
            this.selectedDatatypesToolStripStatusLabel.AutoSize = false;
            this.selectedDatatypesToolStripStatusLabel.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top)
                        | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right)
                        | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.selectedDatatypesToolStripStatusLabel.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter;
            this.selectedDatatypesToolStripStatusLabel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.selectedDatatypesToolStripStatusLabel.Name = "selectedDatatypesToolStripStatusLabel";
            this.selectedDatatypesToolStripStatusLabel.Size = new System.Drawing.Size( 112, 17 );
            this.selectedDatatypesToolStripStatusLabel.Text = "Datatypes:";
            this.selectedDatatypesToolStripStatusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // selectedMemoryTypesToolStripStatusLabel
            // 
            this.selectedMemoryTypesToolStripStatusLabel.AutoSize = false;
            this.selectedMemoryTypesToolStripStatusLabel.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top)
                        | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right)
                        | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.selectedMemoryTypesToolStripStatusLabel.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter;
            this.selectedMemoryTypesToolStripStatusLabel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.selectedMemoryTypesToolStripStatusLabel.Name = "selectedMemoryTypesToolStripStatusLabel";
            this.selectedMemoryTypesToolStripStatusLabel.Size = new System.Drawing.Size( 132, 17 );
            this.selectedMemoryTypesToolStripStatusLabel.Text = "Memory Types:";
            this.selectedMemoryTypesToolStripStatusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // sizeRangeToolStripStatusLabel
            // 
            this.sizeRangeToolStripStatusLabel.AutoSize = false;
            this.sizeRangeToolStripStatusLabel.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top)
                        | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right)
                        | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.sizeRangeToolStripStatusLabel.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter;
            this.sizeRangeToolStripStatusLabel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.sizeRangeToolStripStatusLabel.Name = "sizeRangeToolStripStatusLabel";
            this.sizeRangeToolStripStatusLabel.Size = new System.Drawing.Size( 244, 17 );
            this.sizeRangeToolStripStatusLabel.Text = "Size Range:";
            this.sizeRangeToolStripStatusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // selectedSizeToolStripStatusLabel
            // 
            this.selectedSizeToolStripStatusLabel.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top)
                        | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right)
                        | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.selectedSizeToolStripStatusLabel.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter;
            this.selectedSizeToolStripStatusLabel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.selectedSizeToolStripStatusLabel.Name = "selectedSizeToolStripStatusLabel";
            this.selectedSizeToolStripStatusLabel.Size = new System.Drawing.Size( 305, 17 );
            this.selectedSizeToolStripStatusLabel.Spring = true;
            this.selectedSizeToolStripStatusLabel.Text = "Selected Size:";
            this.selectedSizeToolStripStatusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // glacialTreeList1
            // 
            this.glacialTreeList1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            gtlColumn1.ImageIndex = -1;
            gtlColumn1.Name = "nameColumn";
            gtlColumn1.Text = "Name";
            gtlColumn1.Width = 85;
            gtlColumn2.ImageIndex = -1;
            gtlColumn2.ItemComparerType = GlacialComponents.Controls.GTLCommon.ItemComparerTypes.Numeric;
            gtlColumn2.Name = "numberColumn";
            gtlColumn2.Text = "#";
            gtlColumn2.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gtlColumn2.Width = 45;
            gtlColumn3.ImageIndex = -1;
            gtlColumn3.Name = "typeColumn";
            gtlColumn3.Text = "Datatype";
            gtlColumn3.Width = 85;
            gtlColumn4.ImageIndex = -1;
            gtlColumn4.Name = "addressColumn";
            gtlColumn4.Text = "Address";
            gtlColumn4.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gtlColumn4.Width = 85;
            gtlColumn5.ImageIndex = -1;
            gtlColumn5.ItemComparerType = GlacialComponents.Controls.GTLCommon.ItemComparerTypes.Numeric;
            gtlColumn5.Name = "sizeColumn";
            gtlColumn5.Text = "Size";
            gtlColumn5.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gtlColumn5.Width = 85;
            gtlColumn6.ImageIndex = -1;
            gtlColumn6.Name = "fileColumn";
            gtlColumn6.Text = "File";
            gtlColumn6.Width = 115;
            gtlColumn7.ImageIndex = -1;
            gtlColumn7.ItemComparerType = GlacialComponents.Controls.GTLCommon.ItemComparerTypes.Numeric;
            gtlColumn7.Name = "lineColumn";
            gtlColumn7.Text = "Line";
            gtlColumn7.Width = 45;
            gtlColumn8.ImageIndex = -1;
            gtlColumn8.Name = "memoryTypeColumn";
            gtlColumn8.Text = "Memory Type";
            gtlColumn8.Width = 85;
            this.glacialTreeList1.Columns.AddRange( new GlacialComponents.Controls.GlacialTreeList.GTLColumn[] {
            gtlColumn1,
            gtlColumn2,
            gtlColumn3,
            gtlColumn4,
            gtlColumn5,
            gtlColumn6,
            gtlColumn7,
            gtlColumn8} );
            this.glacialTreeList1.ContextMenuStrip = this.contextMenuStrip1;
            this.glacialTreeList1.HideSelection = true;
            this.glacialTreeList1.HotTrackingColor = System.Drawing.SystemColors.HotTrack;
            this.glacialTreeList1.Location = new System.Drawing.Point( 3, 28 );
            this.glacialTreeList1.Name = "glacialTreeList1";
            this.glacialTreeList1.OptimizeForHighNodeCount = true;
            this.glacialTreeList1.SelectedTextColor = System.Drawing.SystemColors.HighlightText;
            this.glacialTreeList1.SelectionColor = System.Drawing.SystemColors.Highlight;
            this.glacialTreeList1.ShadedColumnMode = GlacialComponents.Controls.GlacialTreeList.GTLShadedColumnModes.None;
            this.glacialTreeList1.ShowRootLines = false;
            this.glacialTreeList1.Size = new System.Drawing.Size( 1110, 300 );
            this.glacialTreeList1.TabIndex = 2;
            this.glacialTreeList1.Text = "glacialTreeList1";
            this.glacialTreeList1.UnfocusedSelectionColor = System.Drawing.SystemColors.Control;
            this.glacialTreeList1.UnfocusedSelectionTextColor = System.Drawing.SystemColors.ControlText;
            this.glacialTreeList1.SelectedIndexChanged += new GlacialComponents.Controls.GlacialTreeList.GTSelectionChangedEventHandler( this.glacialTreeList1_SelectedIndexChanged );
            // 
            // backgroundWorker
            // 
            this.backgroundWorker.WorkerReportsProgress = true;
            this.backgroundWorker.WorkerSupportsCancellation = true;
            this.backgroundWorker.DoWork += new System.ComponentModel.DoWorkEventHandler( this.backgroundWorker_DoWork );
            this.backgroundWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler( this.backgroundWorker_RunWorkerCompleted );
            this.backgroundWorker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler( this.backgroundWorker_ProgressChanged );
            // 
            // AllocationsTreeListControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add( this.glacialTreeList1 );
            this.Controls.Add( this.statusStrip1 );
            this.Controls.Add( this.toolStrip1 );
            this.Name = "AllocationsTreeListControl";
            this.Size = new System.Drawing.Size( 1116, 353 );
            this.contextMenuStrip1.ResumeLayout( false );
            this.toolStrip1.ResumeLayout( false );
            this.toolStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout( false );
            this.statusStrip1.PerformLayout();
            this.ResumeLayout( false );
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem copyToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem collapseAllStacksToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem expandAllStacksToolStripMenuItem;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private GlacialComponents.Controls.GlacialTreeList.GlacialTreeList glacialTreeList1;
        private System.Windows.Forms.ToolStripButton generateReportToolStripButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripLabel reportToolStripLabel;
        private System.Windows.Forms.ToolStripComboBox reportToolStripComboBox;
        private System.Windows.Forms.ToolStripDropDownButton optionsToolStripDropDownButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripButton refreshToolStripButton;
        private System.Windows.Forms.ToolStripStatusLabel groupByToolStripStatusLabel;
        private System.Windows.Forms.ToolStripStatusLabel selectedStacksToolStripStatusLabel;
        private System.Windows.Forms.ToolStripStatusLabel selectedDatatypesToolStripStatusLabel;
        private System.Windows.Forms.ToolStripStatusLabel selectedMemoryTypesToolStripStatusLabel;
        private System.Windows.Forms.ToolStripStatusLabel selectedSizeToolStripStatusLabel;
        private System.Windows.Forms.ToolStripMenuItem groupByToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem stacksToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem datatypesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem memoryTypesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem multiSelectToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem groupByAllocationOrderToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem groupByStackNameToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripMenuItem groupByAllocationsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem groupByDeallocationsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem stacksAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem stacksNoneToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripMenuItem stacksUnassignedToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripMenuItem stacksSelectToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem datatypesAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem datatypesNoneToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripMenuItem datatypesNativeTypesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem datatypesClassesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem datatypesStructuresToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem datatypesUnknownTypesToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator9;
        private System.Windows.Forms.ToolStripMenuItem datatypesSelectToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem memoryTypesAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem memoryTypesNoneToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator10;
        private System.Windows.Forms.ToolStripMenuItem memoryTypesGameVirtualToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem memoryTypesGamePhysicalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem memoryTypesResourceVirtualToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem memoryTypesUndeterminedToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem memoryTypesConsoleSpecificlToolStripMenuItem;
        private System.Windows.Forms.ToolStripLabel minSizeToolStripLabel;
        private System.Windows.Forms.ToolStripLabel maxSizeToolStripLabel;
        private System.Windows.Forms.ToolStripTextBox maxSizeToolStripTextBox;
        private System.Windows.Forms.ToolStripTextBox minSizeToolStripTextBox;
        private System.ComponentModel.BackgroundWorker backgroundWorker;
        private System.Windows.Forms.ToolStripStatusLabel sizeRangeToolStripStatusLabel;
        private System.Windows.Forms.ToolStripMenuItem memoryTypesResourcePhysicalToolStripMenuItem;
    }
}
