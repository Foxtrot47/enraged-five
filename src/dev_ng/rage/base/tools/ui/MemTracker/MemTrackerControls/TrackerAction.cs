using System;
using System.Collections.Generic;
using System.Text;

namespace MemTrackerControls
{
    public abstract class TrackerAction
    {
        public TrackerAction( Action a, uint threadID )
        {
            m_action = a;
            m_threadID = threadID;
        }

        #region Enums
        /// <summary>
        /// An enumeration of each derived type.
        /// </summary>
        public enum Action
        {
            PushStack,
            PopStack,
            TallyAllocation,
            UntallyAllocation,
            Report,
            CountError,
            TallyError,
            UntallyError,
            MemoryError,
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets the <see cref="Action"/> type.
        /// </summary>
        public Action TheAction
        {
            get
            {
                return m_action;
            }
        }

        /// <summary>
        /// Gets the thread for this action.
        /// </summary>
        public uint ThreadID
        {
            get
            {
                return m_threadID;
            }
        }
        #endregion

        #region Variables
        protected Action m_action;
        protected uint m_threadID;
        #endregion

        #region Overrides
        public override string ToString()
        {
            StringBuilder text = new StringBuilder();
            text.Append( m_threadID );
            text.Append( ';' );

            switch ( m_action )
            {
                case Action.CountError:
                    text.Append( "CE" );
                    break;
                case Action.PopStack:
                    text.Append( "Po" );
                    break;
                case Action.PushStack:
                    text.Append( "Pu" );
                    break;
                case Action.Report:
                    text.Append( "Re" );
                    break;
                case Action.TallyAllocation:
                    text.Append( "Ta" );
                    break;
                case Action.TallyError:
                    text.Append( "TE" );
                    break;
                case Action.UntallyAllocation:
                    text.Append( "Un" );
                    break;
                case Action.UntallyError:
                    text.Append( "UE" );
                    break;
                case Action.MemoryError:
                    text.Append( "ME" );
                    break;
            }

            return text.ToString();
        }
        #endregion
    }

    public class PushStackAction : TrackerAction
    {
        public PushStackAction( uint threadID, uint nameHashKey )
            : base( Action.PushStack, threadID )
        {
            m_nameHashKey = nameHashKey;
        }

        #region Properties
        /// <summary>
        /// Gets the name of the stack that was pushed
        /// </summary>
        public string Name
        {
            get
            {
                string name;
                if ( Allocation.StackNameDictionary.TryGetValue( m_nameHashKey, out name ) )
                {
                    return name;
                }

                return Allocation.Unassigned;
            }
        }

        /// <summary>
        /// Gets the hash key of the stack name.
        /// </summary>
        public uint NameHashKey
        {
            get
            {
                return m_nameHashKey;
            }
        }
        #endregion

        #region Variables
        private uint m_nameHashKey = (uint)string.Empty.GetHashCode();
        #endregion

        #region Overrides
        public override string ToString()
        {
            StringBuilder text = new StringBuilder( base.ToString() );

            text.Append( ';' );
            text.Append( this.Name );
            
            return text.ToString();
        }
        #endregion
    }

    public class PopStackAction : TrackerAction
    {
        public PopStackAction( uint threadID, uint nameHashKey )
            : base( Action.PopStack, threadID )
        {
            m_nameHashKey = nameHashKey;
        }

        #region Properties
        /// <summary>
        /// Gets the name of the stack that was popped.
        /// </summary>
        public string Name
        {
            get
            {
                string name;
                if ( Allocation.StackNameDictionary.TryGetValue( m_nameHashKey, out name ) )
                {
                    return name;
                }

                return Allocation.Unassigned;
            }
        }

        /// <summary>
        /// Gets the hash key of the stack name.
        /// </summary>
        public uint NameHashKey
        {
            get
            {
                return m_nameHashKey;
            }
        }
        #endregion

        #region Variables
        private uint m_nameHashKey = (uint)string.Empty.GetHashCode();
        #endregion

        #region Overrides
        public override string ToString()
        {
            return base.ToString();
        }
        #endregion
    }

    public class TallyAllocationAction : TrackerAction
    {
        public TallyAllocationAction( uint threadID, Allocation theAllocation )
            : base( Action.TallyAllocation, threadID )
        {
            m_allocation = theAllocation;
        }

        #region Properties
        /// <summary>
        /// Gets the <see cref="Allocation"/> object associated with this tally.
        /// </summary>
        public Allocation TheAllocation
        {
            get
            {
                return m_allocation;
            }
        }

        public string InfoString
        {
            get
            {
                return GetInfoString( m_allocation );
            }
        }
        #endregion

        #region Variables
        private Allocation m_allocation;
        #endregion

        #region Overrides
        public override string ToString()
        {
            StringBuilder text = new StringBuilder( base.ToString() );

            text.Append( ';' );
            text.Append( m_allocation.ToString() );
            
            return text.ToString();
        }
        #endregion

        #region Public Functions
        public string GetInfoString( Allocation theAllocation )
        {
            StringBuilder text = new StringBuilder();

            string info = theAllocation.InfoString;
            if ( info != string.Empty )
            {
                text.Append( m_threadID );
                text.Append( ";In;" );
                text.Append( info );
            }

            return text.ToString();
        }
        #endregion
    }

    public class UntallyAllocationAction : TrackerAction
    {
        public UntallyAllocationAction( uint threadID, Allocation theAllocation )
            : base( Action.UntallyAllocation, threadID )
        {
            m_allocation = theAllocation;
        }

        #region Properties
        /// <summary>
        /// Gets the <see cref="Allocation"/> object associated with this untally.
        /// </summary>
        public Allocation TheAllocation
        {
            get
            {
                return m_allocation;
            }
        }
        #endregion

        #region Variables
        private Allocation m_allocation;
        #endregion

        #region Overrides
        public override string ToString()
        {
            StringBuilder text = new StringBuilder( base.ToString() );
            
            text.Append( ';' );
            text.Append( m_allocation.Address );
            text.Append( ";" );
            text.Append( m_allocation.Size );

            return text.ToString();
        }
        #endregion
    }

    public class ReportAction : TrackerAction
    {
        public ReportAction( uint threadID, ReportItem report )
            : base( Action.Report, threadID )
        {
            m_report = report;
        }

        #region Properties
        /// <summary>
        /// Gets the <see cref="ReportItem"/> associated with this action.
        /// </summary>
        public ReportItem Report
        {
            get
            {
                return m_report;
            }
        }
        #endregion

        #region Variables
        private ReportItem m_report;
        #endregion

        #region Overrides
        public override string ToString()
        {
            StringBuilder text = new StringBuilder( base.ToString() );

            text.Append( ';' );
            string name = m_report.Name;
            if ( name.EndsWith( "*" ) )
            {
                name = name.Substring( 0, text.Length - 1 );
            }
            text.Append( name );

            foreach ( uint a in m_report.Available )
            {
                text.Append( ";" );
                text.Append( a );
            }

            text.Append( ";" );
            text.Append( m_report.TrackedDeallocations.ToString() );


            return text.ToString();
        }
        #endregion
    }

    public class CountErrorAction : TrackerAction
    {
        public CountErrorAction( uint threadID, uint countExpected, uint countReceived )
            : base( Action.CountError, threadID )
        {
            m_countExpected = countExpected;
            m_countReceived = countReceived;
        }

        #region Properties
        public uint CountExpected
        {
            get
            {
                return m_countExpected;
            }
        }

        public uint CountReceived
        {
            get
            {
                return m_countReceived;
            }
        }
        #endregion

        #region Variables
        private uint m_countExpected;
        private uint m_countReceived;
        #endregion

        #region Overrides
        public override string ToString()
        {
            StringBuilder text = new StringBuilder( base.ToString() );

            text.Append( ';' );
            text.Append( m_countExpected );

            text.Append( ';' );
            text.Append( m_countReceived );

            return text.ToString();
        }
        #endregion
    }

    public class TallyErrorAction : TrackerAction
    {
        public TallyErrorAction( uint threadID, Allocation existingAllocation, Allocation newAllocation, DuplicateAllocationAction actionTaken )
            : base( Action.TallyError, threadID )
        {
            m_existingAllocation = existingAllocation;
            m_newAllocation = newAllocation;
            m_actionTaken = actionTaken;
        }

        #region Properties
        public Allocation ExistingAllocation
        {
            get
            {
                return m_existingAllocation;
            }
        }

        public Allocation NewAllocation
        {
            get
            {
                return m_newAllocation;
            }
        }

        public DuplicateAllocationAction TheActionTaken
        {
            get
            {
                return m_actionTaken;
            }
        }
        #endregion

        #region Variables
        private Allocation m_existingAllocation;
        private Allocation m_newAllocation;
        private DuplicateAllocationAction m_actionTaken;
        #endregion

        #region Overrides
        public override string ToString()
        {
            StringBuilder text = new StringBuilder( base.ToString() );

            text.Append( ';' );
            text.Append( m_existingAllocation.ToString() );

            text.Append( ';' );
            text.Append( m_newAllocation.ToString() );

            text.Append( ';' );
            text.Append( m_actionTaken.ToString() );

            return text.ToString();
        }
        #endregion
    }

    public class UntallyErrorAction : TrackerAction
    {
        public UntallyErrorAction( uint threadID, uint address, uint size )
            : base( Action.UntallyError, threadID )
        {
            m_decimalAddress = address;
            m_size = size;
        }

        #region Properties
        public string Address
        {
            get
            {
                return m_decimalAddress.ToString( "X" );
            }
        }

        public uint DecimalAddress
        {
            get
            {
                return m_decimalAddress;
            }
        }

        public uint Size
        {
            get
            {
                return m_size;
            }
        }
        #endregion

        #region Variables
        private uint m_decimalAddress;
        private uint m_size;
        #endregion

        #region Overrides
        public override string ToString()
        {
            StringBuilder text = new StringBuilder( base.ToString() );

            text.Append( ';' );
            text.Append( m_decimalAddress );

            text.Append( ';' );
            text.Append( m_size );

            return text.ToString();
        }
        #endregion
    }

    public class MemoryErrorAction : TrackerAction
    {
        public MemoryErrorAction( uint threadID, Allocation.MemoryTypeIndex memoryType, uint expectedAvailable, uint available )
            : base( Action.MemoryError, threadID )
        {
            m_memoryType = memoryType;
            m_expectedAvailable = expectedAvailable;
            m_available = available;
        }

        #region Properties
        public Allocation.MemoryTypeIndex MemoryType
        {
            get
            {
                return m_memoryType;
            }
        }

        public uint ExpectedAvailable
        {
            get
            {
                return m_expectedAvailable;
            }
        }

        public uint Available
        {
            get
            {
                return m_available;
            }
        }
        #endregion

        #region Variables
        private Allocation.MemoryTypeIndex m_memoryType;
        private uint m_expectedAvailable;
        private uint m_available;
        #endregion

        #region Overrides
        public override string ToString()
        {
            StringBuilder text = new StringBuilder( base.ToString() );

            text.Append( ';' );
            text.Append( m_memoryType.ToString() );

            text.Append( ';' );
            text.Append( m_expectedAvailable );

            text.Append( ';' );
            text.Append( m_available );

            return text.ToString();
        }
        #endregion
    }
}
