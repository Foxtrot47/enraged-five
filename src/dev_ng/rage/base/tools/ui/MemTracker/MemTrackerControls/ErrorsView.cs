using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace MemTrackerControls
{
    public partial class ErrorsView : Form
    {
        protected ErrorsView()
        {
            InitializeComponent();
        }

        public static ErrorsView Create( TrackerComponent tracker )
        {
            ErrorsView eView = new ErrorsView();

            tracker.LockForUpdate = true;

            List<TrackerAction> errorActions = tracker.Actions;
            for ( int i = 0; i < errorActions.Count; ++i )
            {
                StringBuilder msg = new StringBuilder();
                StringBuilder details = new StringBuilder();

                switch ( errorActions[i].TheAction )
                {
                    case TrackerAction.Action.CountError:
                        {
                            CountErrorAction errorAction = errorActions[i] as CountErrorAction;

                            msg.Append( "Count of messages received does not match the internal count." );

                            details.Append( "Expected " );
                            details.Append( errorAction.CountExpected );
                            details.Append( "but received " );
                            details.Append( errorAction.CountReceived );
                        }
                        break;
                    case TrackerAction.Action.TallyError:
                        {
                            TallyErrorAction errorAction = errorActions[i] as TallyErrorAction;

                            msg.Append( "Allocation at " );
                            msg.Append( errorAction.ExistingAllocation.Address );
                            msg.Append( " already exists." );

                            details.Append( "User chose to " );
                            details.Append( errorAction.TheActionTaken.ToString() );                            
                            details.Append( "\n\n" );

                            details.Append( "EXISTING ALLOCATION:\n" );
                            details.Append( '\t' );
                            details.Append( errorAction.ExistingAllocation.ToString() );

                            string info = errorAction.ExistingAllocation.InfoString;
                            if ( info != string.Empty )
                            {
                                details.Append( "\n\t" );
                                details.Append( errorAction.ThreadID );
                                details.Append( ";In;" );
                                details.Append( info );
                            }
                            details.Append( "\n\n" );

                            details.Append( "NEW ALLOCATION:\n" );
                            details.Append( '\t' );
                            details.Append( errorAction.NewAllocation.ToString() );

                            info = errorAction.NewAllocation.InfoString;
                            if ( info != string.Empty )
                            {
                                details.Append( "\n\t" );
                                details.Append( errorAction.ThreadID );
                                details.Append( ";In;" );
                                details.Append( info );
                            }
                        }
                        break;
                    case TrackerAction.Action.UntallyError:
                        {
                            UntallyErrorAction errorAction = errorActions[i] as UntallyErrorAction;

                            msg.Append( "Received an Untally for address " );
                            msg.Append( errorAction.Address );
                            msg.Append( " but never had a corresponding Tally." );

                            details.Append( "Address: " );
                            details.Append( errorAction.Address );
                            details.Append( "\nSize: " );
                            details.Append( errorAction.Size.ToString( "n" ).Replace( ".00", "" ) );
                            details.Append( " bytes" );
                        }
                        break;
                    case TrackerAction.Action.MemoryError:
                        {
                            MemoryErrorAction errorAction = errorActions[i] as MemoryErrorAction;

                            msg.Append( "Incorrect size of Available " );
                            msg.Append( errorAction.MemoryType.ToString() );
                            msg.Append( " Memory." );

                            details.Append( "Memory Type: " );
                            details.Append( errorAction.MemoryType.ToString() );
                            details.Append( " (" );
                            details.Append( ((int)errorAction.MemoryType) - 1 );
                            details.Append( ")\n" );

                            details.Append( "Expected Size: " );
                            details.Append( errorAction.ExpectedAvailable.ToString( "n" ).Replace( ".00", "" ) );
                            details.Append( " bytes\nAvailable Size: " );
                            details.Append( errorAction.Available.ToString( "n" ).Replace( ".00", "" ) );
                            details.Append( " bytes" );
                        }
                        break;
                }

                if ( msg.Length > 0 )
                {
                    ListViewItem item = new ListViewItem( i.ToString() );
                    item.SubItems.Add( msg.ToString() );
                    item.Tag = details.ToString();

                    eView.listView1.Items.Add( item );
                }
            }

            tracker.LockForUpdate = false;

            eView.Text = "Errors View (" + System.DateTime.Now.ToString( "G" ) + ")";

            return eView;
        }

        private void listView1_SelectedIndexChanged( object sender, EventArgs e )
        {
            this.richTextBox1.Clear();

            if ( this.listView1.SelectedItems.Count > 0 )
            {
                ListViewItem item = this.listView1.SelectedItems[0];
                string details = item.Tag as string;

                this.richTextBox1.AppendText( details );
            }
        }
    }
}