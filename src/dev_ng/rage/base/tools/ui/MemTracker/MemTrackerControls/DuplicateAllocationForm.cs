using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace MemTrackerControls
{
    public partial class DuplicateAllocationForm : Form
    {
        public DuplicateAllocationForm()
        {
            InitializeComponent();
        }

        public static DuplicateAllocationForm Create( Allocation theAllocation )
        {
            DuplicateAllocationForm form = new DuplicateAllocationForm();

            StringBuilder msg = new StringBuilder();
            msg.Append( "An attempt was made to tally an allocation that already exists:\n\n" );
            msg.Append( "    Address: " );
            msg.Append( theAllocation.Address );
            msg.Append( ", Datatype: " );
            msg.Append( theAllocation.TypeName );
            msg.Append( ", Full Stack Path: " );
            msg.Append( theAllocation.FullStackPath );
            msg.Append( ", Size: " );
            msg.Append( theAllocation.Size.ToString( "n" ).Replace( ".00", "" ) );
            msg.Append( "\n\nWhat would you like to do?" );

            form.messageLabel.Text = msg.ToString();

            return form;
        }

        public MemTrackerControls.DuplicateAllocationAction ShowIt( Form mainForm )
        {
            DialogResult result = this.ShowDialog( mainForm );
            switch ( result )
            {
                case DialogResult.Retry:
                    return DuplicateAllocationAction.Leave;
                case DialogResult.Cancel:
                    return DuplicateAllocationAction.OverwriteAll;
                default:
                    return DuplicateAllocationAction.Overwrite;
            }
        }
    }
}