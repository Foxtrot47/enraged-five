using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace MemTrackerControls
{
    /// <summary>
    /// A re-usable list for displaying an Overview of memory based on several settings.  
    /// Has 3 display modes:  Memory Type, Stack Name, and Datatype
    /// </summary>
    public partial class OverviewListControl : UserControl
    {
        public OverviewListControl()
        {
            InitializeComponent();
            InitializeAdditionalComponents();
        }

        #region Enums
        /// <summary>
        /// The type of Report to display
        /// </summary>
        public enum DisplayType
        {
            StackNames,
            DataTypes,
            MemoryType
        }

        /// <summary>
        /// The range of stacks that are selected for display
        /// </summary>
        public enum SelectedStacks
        {
            All,
            None,
            Selected
        }

        /// <summary>
        /// The range of datatypes that are selected for display
        /// </summary>
        public enum SelectedDatatypes
        {
            All,
            None,
            Selected
        }
        #endregion

        #region Constants
        const int c_numStackNameColumnHeaders = 8;
        const int c_numDataTypeColumnHeaders = 5;
        const int c_numMemoryTypeColumnHeaders = 8;
        #endregion

        #region Properties
        /// <summary>
        /// The underlying <see cref="ListView"/> control.
        /// </summary>
        [Browsable( true ),
        DesignOnly( true ),
        Category( "Misc" )]
        public ListView ListView
        {
            get
            {
                return this.listView1;
            }
        }

        /// <summary>
        /// The <see cref="TrackerComponent"/> that manages all of the memory allocation data.
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        Description( "The object that manages the allocation, stack, and datatype information." )]
        public TrackerComponent Tracker
        {
            get
            {
                return m_trackerComponent;
            }
            set
            {
                m_trackerComponent = value;

                if ( !this.DesignMode && (m_trackerComponent != null) )
                {
                    m_trackerComponent.LockForUpdate = true;

                    ReportItem[] reports = m_trackerComponent.Reports;
                    foreach ( ReportItem report in reports )
                    {
                        trackerComponent_ReportAdded( m_trackerComponent, new ReportChangedEventArgs( report ) );
                    }

                    m_reportAddedEventHandler = new ReportChangedEventHandler( trackerComponent_ReportAdded );
                    m_trackerComponent.ReportAdded += m_reportAddedEventHandler;

                    m_reportRemovedEventHandler = new ReportChangedEventHandler( trackerComponent_ReportRemoved );
                    m_trackerComponent.ReportRemoved += m_reportRemovedEventHandler;

                    foreach ( KeyValuePair<uint, string> pair in Allocation.StackNameDictionary )
                    {
                        trackerComponent_StackAdded( m_trackerComponent, new StackChangedEventArgs( pair.Value, pair.Key ) );
                    }

                    m_stackAddedEventHandler = new StackChangedEventHandler( trackerComponent_StackAdded );
                    m_trackerComponent.StackAdded += m_stackAddedEventHandler;

                    foreach ( KeyValuePair<uint, string> pair in Allocation.DataTypeDictionary )
                    {
                        trackerComponent_DatatypeAdded( m_trackerComponent, new DatatypeAddedEventArgs( pair.Value, pair.Key) );
                    }

                    m_datatypeAddedEventHandler = new DatatypeAddedEventHandler( trackerComponent_DatatypeAdded );
                    m_trackerComponent.DatatypeAdded += m_datatypeAddedEventHandler;

                    m_trackerComponent.LockForUpdate = false;
                }
            }
        }

        /// <summary>
        /// A <see cref="Control"/>, typically the application's main <see cref="Form"/> that can be used to launch
        /// dialog boxes and Invoke cross-threaded methods.
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        Description( "Sets the parent Control so that dialog boxes can appear in the correct location." )]
        public Control ParentControl
        {
            get
            {
                return m_parentControl;
            }
            set
            {
                m_parentControl = value;
            }
        }

        /// <summary>
        /// Show or hide the Status Bar's sizing grip at the lower right corner of the control.
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        Description( "Show or hide the Status Bar's sizing grip at the lower right corner of the control." )]
        public bool ShowSizingGrip
        {
            get
            {
                return this.statusStrip1.SizingGrip;
            }
            set
            {
                this.statusStrip1.SizingGrip = value;
            }
        }

        /// <summary>
        /// Gets or sets the <see cref="DisplayType"/>.
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        Description( "The type of list to show." ),
        DefaultValue( "DisplayType.MemoryType" )]
        public DisplayType ShowBy
        {
            get
            {
                return m_showBy;
            }
            set
            {
                if ( m_showBy != value )
                {
                    m_showBy = value;

                    if ( this.DesignMode )
                    {
                        SetColumnHeaders( false, m_showAllocations );
                    }
                    else
                    {
                        this.RefreshNeeded = true;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets whether to display the Unassigned stack name when <see cref="ShowBy"/> is set to <see cref="DisplayType."/>StackNames.
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        Description( "Whether to show the Unassigned Stack." ),
        DefaultValue( true )]
        public bool ShowUnassignedStack
        {
            get
            {
                return m_showUnassignedStack;
            }
            set
            {
                if ( m_showUnassignedStack != value )
                {
                    m_showUnassignedStack = value;

                    if ( !this.DesignMode  )
                    {
                        m_stackMutex.WaitOne();

                        DetermineVisibleStacks();

                        m_stackMutex.ReleaseMutex();

                        this.RefreshNeeded = true;
                    }                
                }
            }
        }

        /// <summary>
        /// Gets or sets whether to display Native datatypes when <see cref="ShowBy"/> is set to <see cref="DisplayType."/>DataTypes.
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        Description( "Whether to show Native datatypes." ),
        DefaultValue( true )]
        public bool ShowNativeDatatypes
        {
            get
            {
                return m_showNativeDatatypes;
            }
            set
            {
                if ( m_showNativeDatatypes != value )
                {
                    m_showNativeDatatypes = value;
                    
                    if ( !this.DesignMode )
                    {
                        m_datatypeMutex.WaitOne();

                        // collect a list of type names to change the visibility of
                        List<uint> nativeTypes = new List<uint>();
                        foreach ( KeyValuePair<uint, bool> pair in m_datatypeVisibility )
                        {
                            string name;
                            if ( (pair.Value != m_showNativeDatatypes) && Allocation.DataTypeDictionary.TryGetValue( pair.Key, out name ) )
                            {
                                if ( !name.StartsWith( "struct" ) && !name.StartsWith( "class" ) )
                                {
                                    nativeTypes.Add( pair.Key );
                                }
                            }
                        }

                        if ( nativeTypes.Count > 0 )
                        {
                            foreach ( uint nativeType in nativeTypes )
                            {
                                m_datatypeVisibility[nativeType] = m_showNativeDatatypes;
                            }
                        }

                        this.RefreshNeeded = true;

                        DetermineVisibleDatatypes();

                        m_datatypeMutex.ReleaseMutex();                        
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets whether to display Classes when <see cref="ShowBy"/> is set to <see cref="DisplayType."/>DataTypes.
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        Description( "Whether to show Classes." ),
        DefaultValue( true )]
        public bool ShowClassDatatypes
        {
            get
            {
                return m_showClassDatatypes;
            }
            set
            {
                if ( m_showClassDatatypes != value )
                {
                    m_showClassDatatypes = value;
                    
                    if ( !this.DesignMode )
                    {
                        m_datatypeMutex.WaitOne();

                        // collect a list of type names to change the visibility of
                        List<uint> classTypes = new List<uint>();
                        foreach ( KeyValuePair<uint, bool> pair in m_datatypeVisibility )
                        {
                            string name;
                            if ( (pair.Value != m_showClassDatatypes) && Allocation.DataTypeDictionary.TryGetValue( pair.Key, out name ) )
                            {
                                if ( name.StartsWith( "class" ) )
                                {
                                    classTypes.Add( pair.Key );
                                }
                            }
                        }

                        if ( classTypes.Count > 0 )
                        {
                            foreach ( uint classType in classTypes )
                            {
                                m_datatypeVisibility[classType] = m_showClassDatatypes;
                            }
                        }

                        this.RefreshNeeded = true;

                        DetermineVisibleDatatypes();

                        m_datatypeMutex.ReleaseMutex();                        
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets whether to display Structures when <see cref="ShowBy"/> is set to <see cref="DisplayType."/>DataTypes.
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        Description( "Whether to show Structures." ),
        DefaultValue( true )]
        public bool ShowStructureDatatypes
        {
            get
            {
                return m_showStructDatatypes;
            }
            set
            {
                if ( m_showStructDatatypes != value )
                {
                    m_showStructDatatypes = value;

                    if ( !this.DesignMode )
                    {
                        m_datatypeMutex.WaitOne();

                        // collect a list of type names to change the visibility of
                        List<uint> structTypes = new List<uint>();
                        foreach ( KeyValuePair<uint, bool> pair in m_datatypeVisibility )
                        {
                            string name;
                            if ( (pair.Value != m_showStructDatatypes) && Allocation.DataTypeDictionary.TryGetValue( pair.Key, out name ) )
                            {
                                if ( name.StartsWith( "struct" ) )
                                {
                                    structTypes.Add( pair.Key );
                                }
                            }
                        }

                        if ( structTypes.Count > 0 )
                        {
                            foreach ( uint structType in structTypes )
                            {
                                m_datatypeVisibility[structType] = m_showStructDatatypes;
                            }
                        }

                        this.RefreshNeeded = true;

                        DetermineVisibleDatatypes();

                        m_datatypeMutex.ReleaseMutex();                        
                    }                    
                }
            }
        }

        /// <summary>
        /// Gets or sets whether to display Unknown datatypes when <see cref="ShowBy"/> is set to <see cref="DisplayType."/>DataTypes.
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        Description( "Whether to show Unknown datatypes." ),
        DefaultValue( true )]
        public bool ShowUnknownDatatypes
        {
            get
            {
                return m_showUnknownDatatypes;
            }
            set
            {
                if ( m_showUnknownDatatypes != value )
                {
                    m_showUnknownDatatypes = value;

                    if ( !this.DesignMode )
                    {
                        m_datatypeMutex.WaitOne();

                        DetermineVisibleDatatypes();

                        m_datatypeMutex.ReleaseMutex();

                        this.RefreshNeeded = true;                        
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets whether to display allocations or deallocations.
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        Description( "Whether to show Allocations or Deallocations." ),
        DefaultValue( true )]
        public bool ShowAllocations
        {
            get
            {
                return m_showAllocations;
            }
            set
            {
                if ( m_showAllocations != value )
                {
                    m_showAllocations = value;

                    if ( this.DesignMode )
                    {
                        SetColumnHeaders( false, m_showAllocations );
                    }
                    else
                    {
                        this.RefreshNeeded = true;                        
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets whether to display or hide items with a size of 0 bytes.
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        Description( "Whether to show items with a size of 0 bytes." ),
        DefaultValue( false )]
        public bool ShowEmptyItems
        {
            get
            {
                return m_showEmptyItems;
            }
            set
            {
                if ( m_showEmptyItems != value )
                {
                    m_showEmptyItems = value;

                    if ( !this.DesignMode )
                    {
                        this.RefreshNeeded = true;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the Left <see cref="ReportItem"/>.
        /// </summary>
        [Browsable( false )]
        public ReportItem LeftReport
        {
            get
            {
                return m_leftReport;
            }
            set
            {
                if ( m_leftReport != value )
                {
                    m_leftReport = value;
                    this.leftReportToolStripComboBox.SelectedItem = value;
                    this.RefreshNeeded = true;
                    rightReportToolStripComboBox_SelectedIndexChanged( this, new EventArgs() );
                }
            }
        }

        /// <summary>
        /// Gets or sets the Right <see cref="ReportItem"/>.
        /// </summary>
        [Browsable( false )]
        public ReportItem RightReport
        {
            get
            {
                return m_rightReport;
            }
            set
            {
                if ( m_rightReport != value )
                {
                    m_rightReport = value;
                    this.rightReportToolStripComboBox.SelectedItem = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets the visibility of Stack names using the <see cref="SelectedStacks"/> enumeration.
        /// </summary>
        [Browsable( false )]
        public SelectedStacks VisibleStacks
        {
            get
            {
                return m_selectedStacks;
            }
            set
            {
                bool stacksChanged;
                bool unassignedStackChanged;
                SetSelectedStacks( value, out stacksChanged, out unassignedStackChanged );
            }
        }

        /// <summary>
        /// Gets or sets which Stack types to be displayed.  Everything else is hidden.
        /// </summary>
        [Browsable( false )]
        public List<string> VisibleStackTypeNames
        {
            get
            {
                List<string> stackTypes = new List<string>();
                if ( this.ShowUnassignedStack )
                {
                    stackTypes.Add( "unassigned" );
                }

                return stackTypes;
            }
            set
            {
                this.ShowUnassignedStack = value.Contains( "unassigned" );
            }
        }

        /// <summary>
        /// Gets or sets which individual Stack hash keys to be displayed.  Everything else is hidden.
        /// </summary>
        [Browsable( false )]
        public List<uint> VisibleStackHashKeys
        {
            get
            {
                m_stackMutex.WaitOne();

                List<uint> stackKeys = new List<uint>();
                foreach ( KeyValuePair<uint, bool> pair in m_stackVisibility )
                {
                    if ( pair.Value )
                    {
                        stackKeys.Add( pair.Key );
                    }
                }

                m_stackMutex.ReleaseMutex();

                return stackKeys;
            }
            set
            {                
                m_stackMutex.WaitOne();

                // first, set all invisible.
                this.VisibleStacks = SelectedStacks.None;

                foreach ( uint stackKey in value )
                {
                    m_stackVisibility[stackKey] = true;
                }

                DetermineVisibleStacks();

                m_stackMutex.ReleaseMutex();
            }
        }

        /// <summary>
        /// Gets or sets the visibility of Datatypes using the <see cref="SelectedDatatypes"/> enumeration.
        /// </summary>
        [Browsable( false )]
        public SelectedDatatypes VisibleDatatypes
        {
            get
            {
                return m_selectedDatatypes;
            }
            set
            {
                bool selectedDatatypesChanged;
                bool nativeDatatypesChanged;
                bool classesChanged;
                bool structuresChanged;
                bool unknownDatatypesChanged;
                SetSelectedDatatypes( value, out selectedDatatypesChanged,
                    out nativeDatatypesChanged, out classesChanged, out structuresChanged, out unknownDatatypesChanged );
            }
        }

        /// <summary>
        /// Gets or sets which Datatype types to be displayed.  Everything else is hidden.
        /// </summary>
        [Browsable( false )]
        public List<string> VisibleDatatypesTypeNames
        {
            get
            {
                List<string> datatypeTypes = new List<string>();

                if ( this.ShowNativeDatatypes )
                {
                    datatypeTypes.Add( "native" );
                }

                if ( this.ShowClassDatatypes )
                {
                    datatypeTypes.Add( "class" );
                }

                if ( this.ShowStructureDatatypes )
                {
                    datatypeTypes.Add( "struct" );
                }

                if ( this.ShowUnknownDatatypes )
                {
                    datatypeTypes.Add( "unknown" );
                }

                return datatypeTypes;
            }
            set
            {
                this.ShowNativeDatatypes = value.Contains( "native" );
                this.ShowClassDatatypes = value.Contains( "class" );
                this.ShowStructureDatatypes = value.Contains( "struct" );
                this.ShowUnknownDatatypes = value.Contains( "unknown" );
            }
        }

        /// <summary>
        /// Gets or sets which individual Datatype hash keys to be displayed.  Everything else is hidden.
        /// </summary>
        [Browsable( false )]
        public List<uint> VisibleDatatypeHashKeys
        {
            get
            {
                m_datatypeMutex.WaitOne();

                List<uint> datatypeKeys = new List<uint>();
                foreach ( KeyValuePair<uint, bool> pair in m_datatypeVisibility )
                {
                    if ( pair.Value )
                    {
                        datatypeKeys.Add( pair.Key );
                    }
                }

                m_datatypeMutex.ReleaseMutex();

                return datatypeKeys;
            }
            set
            {
                m_datatypeMutex.WaitOne();

                // first, set all invisible.
                this.VisibleDatatypes = SelectedDatatypes.None;

                foreach ( uint datatypeKey in value )
                {
                    m_datatypeVisibility[datatypeKey] = true;
                }

                m_datatypeMutex.ReleaseMutex();
            }
        }

        /// <summary>
        /// Gets or sets whether refreshing a report is threaded.
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        Description( "Enables or disables threading during the update." ),
        DefaultValue( false )]
        public bool ThreadingEnabled
        {
            get
            {
                return m_threadingEnabled;
            }
            set
            {
                m_threadingEnabled = value;
            }
        }

        /// <summary>
        /// Gets or sets the Enabled property of the Generate Report button.
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        Description( "Whether to enable the Generate Report button." ),
        DefaultValue( true )]
        public bool GenerateReportButtonEnabled
        {
            get
            {
                return this.generateReportToolStripButton.Enabled;
            }
            set
            {
                this.generateReportToolStripButton.Enabled = value;
            }
        }

        /// <summary>
        /// Gets or sets the Enabled property of the Refresh button.  
        /// Will only be enabled if we have a report selected and/or have changed an option since the last Refresh.
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        Description( "Whether to enable the Refresh button." ),
        DefaultValue( true )]
        public bool RefeshButtonEnabled
        {
            get
            {
                return this.refreshToolStripButton.Enabled;
            }
            set
            {
                m_allowRefresh = value;

                if ( this.DesignMode )
                {
                    this.refreshToolStripButton.Enabled = value;
                }
                else
                {
                    this.refreshToolStripButton.Enabled = m_allowRefresh && m_refreshNeeded;
                }
            }
        }

        /// <summary>
        /// Private property to aid in setting the Enabled property of the Refresh button properly.
        /// </summary>
        [Browsable( false )]
        private bool RefreshNeeded
        {
            set
            {
                m_refreshNeeded = value && (m_leftReport != null);

                this.refreshToolStripButton.Enabled = m_allowRefresh && m_refreshNeeded;
            }
        }
        #endregion

        #region Events
        /// <summary>
        /// Signals that a report is about to begin it's refresh.
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        Description( "When populating the list, informs the UI how many steps it will take." )]
        public event UpdateBeginEventHandler UpdateBegin;

        /// <summary>
        /// Signals that one step of the refresh is complete.
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        Description( "When populating the list, informs the UI when a portion of the work has completed." )]
        public event UpdateStepEventHandler UpdateStep;

        /// <summary>
        /// Signals that the entire refresh is complete
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        Description( "When populating the list, informs the UI when all the work is completed." )]
        public event UpdateCompleteEventHandler UpdateComplete;

        /// <summary>
        /// Occurs when <see cref="ShowBy"/> is changed.
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        Description( "Occurs when the DisplayType is changed." )]
        public event EventHandler ShowByChanged;

        /// <summary>
        /// Occurs when <see cref="ShowUnassignedStack"/> is changed.
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        Description( "Occurs when the visibility of the Unassigned Stack is changed." )]
        public event EventHandler ShowUnassignedStackChanged;

        /// <summary>
        /// Occurs when any of the individual Stack names' visiblity are changed.
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        Description( "Occurs when the visibility of any of the Stacks is changed." )]
        public event EventHandler SelectedStacksChanged;

        /// <summary>
        /// Occurs when <see cref="ShowNativeDatatypes"/> is changed.
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        Description( "Occurs when the visibility Native datatypes is changed." )]
        public event EventHandler ShowNativeDatatypesChanged;

        /// <summary>
        /// Occurs when <see cref="ShowClassDatatypes"/> is changed.
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        Description( "Occurs when the visibility of Class datatypes is changed." )]
        public event EventHandler ShowClassDatatypesChanged;

        /// <summary>
        /// Occurs when <see cref="ShowStructureDatatypes"/> is changed.
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        Description( "Occurs when the visibility of Structure datatypes is changed." )]
        public event EventHandler ShowStructureDatatypesChanged;

        /// <summary>
        /// Occurs when <see cref="ShowUnknownDatatypes"/> is changed.
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        Description( "Occurs when the visibility of the Unknown Datatype is changed." )]
        public event EventHandler ShowUnknownDatatypesChanged;

        /// <summary>
        /// Occurs when any of the individual Datatypes' visiblity are changed.
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        Description( "Occurs when the visibility of any of the Datatypes is changed." )]
        public event EventHandler SelectedDatatypesChanged;

        /// <summary>
        /// Occurs when <see cref="ShowAllocations"/> is changed.
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        Description( "Occurs when the displaying Allocations/Deallocations is changed." )]
        public event EventHandler ShowAllocationsChanged;

        /// <summary>
        /// Occurs when <see cref="ShowEmptyItems"/> is changed.
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        Description( "Occurs when the Show Empty Items option is changed." )]
        public event EventHandler ShowEmptyItemsChanged;
        #endregion

        #region Variables
        private Control m_parentControl = null;

        private TrackerComponent m_trackerComponent;
        private ReportChangedEventHandler m_reportAddedEventHandler;
        private ReportChangedEventHandler m_reportRemovedEventHandler;
        private StackChangedEventHandler m_stackAddedEventHandler;
        private DatatypeAddedEventHandler m_datatypeAddedEventHandler;

        private bool m_threadingEnabled = false;
        private bool m_refreshNeeded = false;
        private bool m_allowRefresh = true;

        private DisplayType m_showBy = DisplayType.MemoryType;
        private bool m_showAllocations = true;
        private bool m_showEmptyItems = false;
        private bool m_showUnassignedStack = true;
        private bool m_showNativeDatatypes = true;
        private bool m_showClassDatatypes = true;
        private bool m_showStructDatatypes = true;
        private bool m_showUnknownDatatypes = true;

        private ReportItem m_leftReport = null;
        private ReportItem m_rightReport = null;

        private bool m_reportGenerated = false;

        private SelectedStacks m_selectedStacks = SelectedStacks.All;
        private SelectedDatatypes m_selectedDatatypes = SelectedDatatypes.All;

        private Dictionary<uint, bool> m_stackVisibility = new Dictionary<uint, bool>();
        private Dictionary<uint, bool> m_datatypeVisibility = new Dictionary<uint, bool>();

        private Mutex m_stackMutex = new Mutex();
        private Mutex m_datatypeMutex = new Mutex();

        private ColumnHeader[] m_stackNameColumnHeaders = new ColumnHeader[c_numStackNameColumnHeaders];
        private ColumnHeader[] m_dataTypeColumnHeaders = new ColumnHeader[c_numDataTypeColumnHeaders];
        private ColumnHeader[] m_memoryTypeColumnHeaders = new ColumnHeader[c_numMemoryTypeColumnHeaders];
        private ColumnHeader[] m_stackNameFreedColumnHeaders = new ColumnHeader[c_numStackNameColumnHeaders];
        private ColumnHeader[] m_dataTypeFreedColumnHeaders = new ColumnHeader[c_numDataTypeColumnHeaders];
        private ColumnHeader[] m_memoryTypeFreedColumnHeaders = new ColumnHeader[c_numMemoryTypeColumnHeaders];

        private ListViewItem[] m_rows = null;
        #endregion

        #region Public Functions
        /// <summary>
        /// Refreshes the <see cref="GlacialTreeList"/> based on the current settings.
        /// </summary>
        public virtual void FillData()
        {
            this.toolStrip1.Enabled = false;            

            if ( !m_threadingEnabled )
            {
                OnUpdateBegin( 100, m_reportGenerated );
                backgroundWorker_DoWork( this, new DoWorkEventArgs( null ) );
                backgroundWorker_RunWorkerCompleted( this, new RunWorkerCompletedEventArgs( (m_leftReport == null) ? 0 : 1, null, false ) );
            }
            else
            {
                backgroundWorker.RunWorkerAsync();
                OnUpdateBegin( 100, m_reportGenerated );
            }            
        }

        /// <summary>
        /// Refreshes the <see cref="GlacialTreeList"/> based on the current settings, but using the given <see cref="ReportItem"/>.  Subsequent
        /// Update messages will indicate that we are generating a report.
        /// </summary>
        /// <param name="report">The <see cref="ReportItem"/> to use.</param>
        public virtual void FillData( ReportItem report )
        {
            ReportItem oldReport = this.LeftReport;            
            this.LeftReport = report;
            
            if ( m_reportGenerated )
            {
                if ( oldReport != null )
                {
                    this.RightReport = oldReport;
                }
                else
                {
                    this.RightReport = this.LeftReport;
                }
            }

            m_reportGenerated = true;
            
            FillData();
        }
        #endregion

        #region Protected Functions
        protected virtual int GatherStackData()
        {            
            if ( m_leftReport == null )
            {
                return 0;
            }

            m_trackerComponent.LockForUpdate = true;

            bool showAllocations = m_leftReport.TrackedDeallocations ? m_showAllocations : true;

            // determine how many actions to look at
            int leftReportIndex = m_leftReport.Index;
            int rightReportIndex = -1;
            if ( m_rightReport != null )
            {
                rightReportIndex = m_rightReport.Index;
            }

            bool rightDone = (rightReportIndex > -1) && (rightReportIndex < leftReportIndex) ? false : true;

            SortedDictionary<string, AllocationStats> leftSortedDictionary = new SortedDictionary<string, AllocationStats>();
            SortedDictionary<string, AllocationStats> rightSortedDictionary = new SortedDictionary<string, AllocationStats>();

            leftSortedDictionary.Add( Allocation.Unassigned, new AllocationStats( Allocation.Unassigned, (uint)Allocation.Unassigned.GetHashCode() ) );

            if ( !rightDone )
            {
                rightSortedDictionary.Add( Allocation.Unassigned, new AllocationStats( Allocation.Unassigned, (uint)Allocation.Unassigned.GetHashCode() ) );
            }

            float multiplier = 100.0f / (float)(m_leftReport.Index + 1);

            List<TrackerAction> trackerActions = m_trackerComponent.Actions;
            for ( int i = 0; i <= leftReportIndex; ++i )
            {
                TrackerAction action = trackerActions[i];

                switch ( action.TheAction )
                {
                    case TrackerAction.Action.PushStack:
                        {
                            PushStackAction stackAction = action as PushStackAction;
                            if ( !leftSortedDictionary.ContainsKey( stackAction.Name ) )
                            {
                                leftSortedDictionary.Add( stackAction.Name, new AllocationStats( stackAction.Name, stackAction.NameHashKey ) );
                            }

                            if ( !rightDone && !rightSortedDictionary.ContainsKey( stackAction.Name ) )
                            {
                                rightSortedDictionary.Add( stackAction.Name, new AllocationStats( stackAction.Name, stackAction.NameHashKey ) );
                            }
                        }
                        break;
                    case TrackerAction.Action.TallyAllocation:
                        if ( showAllocations )
                        {
                            TallyAllocationAction tallyAction = action as TallyAllocationAction;
                            TallyOverviewAllocation( tallyAction.TheAllocation, DisplayType.StackNames, 
                                rightDone, leftSortedDictionary, rightSortedDictionary );
                        }
                        break;
                    case TrackerAction.Action.UntallyAllocation:
                        {
                            UntallyAllocationAction untallyAction = action as UntallyAllocationAction;
                            if ( showAllocations )
                            {
                                UntallyOverviewAllocation( untallyAction.TheAllocation, DisplayType.StackNames, 
                                    rightDone, leftSortedDictionary, rightSortedDictionary );
                            }
                            else
                            {
                                TallyOverviewAllocation( untallyAction.TheAllocation, DisplayType.StackNames, 
                                    rightDone, leftSortedDictionary, rightSortedDictionary );
                            }
                        }
                        break;
                    case TrackerAction.Action.Report:
                        if ( i == rightReportIndex )
                        {
                            rightDone = true;
                        }
                        break;
                }

                int percentComplete = (int)((float)i * multiplier);
                if ( m_threadingEnabled )
                {
                    backgroundWorker.ReportProgress( percentComplete );
                }
                else
                {
                    OnUpdateStep( percentComplete, m_reportGenerated );
                }
            }

            // remove entries that we don't want to show
            List<string> removedKeys = new List<string>();
            foreach ( KeyValuePair<string, AllocationStats> pair in leftSortedDictionary )
            {
                bool visible = true;
                if ( pair.Key == Allocation.Unassigned )
                {
                    visible = this.ShowUnassignedStack;
                }
                else
                {
                    m_stackMutex.WaitOne();

                    bool vis;
                    if ( m_stackVisibility.TryGetValue( pair.Value.HashKey, out vis ) )
                    {
                        visible = vis;
                    }

                    m_stackMutex.ReleaseMutex();
                }

                if ( !visible )
                {
                    removedKeys.Add( pair.Key );
                    continue;
                }

                AllocationStats leftStat = leftSortedDictionary[pair.Key];
                if ( !m_showEmptyItems && (leftStat.Inclusive == 0) && (leftStat.Exclusive == 0) )
                {
                    AllocationStats rightStat = null;
                    if ( rightSortedDictionary.TryGetValue( pair.Key, out rightStat ) )
                    {
                        if ( (rightStat.Inclusive == 0) && (rightStat.Exclusive == 0) )
                        {
                            removedKeys.Add( pair.Key );
                            continue;
                        }
                    }
                    else
                    {
                        removedKeys.Add( pair.Key );
                        continue;
                    }
                }
            }

            foreach ( string key in removedKeys )
            {
                leftSortedDictionary.Remove( key );
                rightSortedDictionary.Remove( key );
            }

            // create rows
            m_rows = new ListViewItem[leftSortedDictionary.Count];
            int rowNum = 0;
            foreach ( KeyValuePair<string, AllocationStats> pair in leftSortedDictionary )
            {
                AllocationStats leftStat = pair.Value;
                AllocationStats rightStat;
                bool haveRightStat = rightSortedDictionary.TryGetValue( pair.Key, out rightStat );

                ListViewItem row = new ListViewItem( rowNum.ToString(), 0 );
                row.SubItems.Add( leftStat.Name );

                long diff = leftStat.Inclusive;
                if ( haveRightStat )
                {
                    diff -= rightStat.Inclusive;
                }

                row.SubItems.Add( leftStat.Inclusive.ToString( "n" ).Replace( ".00", "" ) );
                row.SubItems.Add( haveRightStat ? rightStat.Inclusive.ToString( "n" ).Replace( ".00", "" ) : string.Empty );
                row.SubItems.Add( diff.ToString( "n" ).Replace( ".00", "" ) );

                diff = leftStat.Exclusive;
                if ( haveRightStat )
                {
                    diff -= rightStat.Exclusive;
                }

                row.SubItems.Add( leftStat.Exclusive.ToString( "n" ).Replace( ".00", "" ) );
                row.SubItems.Add( haveRightStat ? rightStat.Exclusive.ToString( "n" ).Replace( ".00", "" ) : string.Empty );
                row.SubItems.Add( diff.ToString( "n" ).Replace( ".00", "" ) );

                m_rows[rowNum] = row;                

                rowNum++;
            }

            m_trackerComponent.LockForUpdate = false;

            return 1;
        }

        protected virtual int GatherDatatypeData()
        {
            if ( m_leftReport == null )
            {
                return 0;
            }

            m_trackerComponent.LockForUpdate = true;

            bool showAllocations = m_leftReport.TrackedDeallocations ? m_showAllocations : true;

            // determine how many actions to look at
            int leftReportIndex = m_leftReport.Index;
            int rightReportIndex = -1;
            if ( m_rightReport != null )
            {
                rightReportIndex = m_rightReport.Index;
            }

            bool rightDone = (rightReportIndex > -1) && (rightReportIndex < leftReportIndex) ? false : true;

            SortedDictionary<string, AllocationStats> leftSortedDictionary = new SortedDictionary<string, AllocationStats>();
            SortedDictionary<string, AllocationStats> rightSortedDictionary = new SortedDictionary<string, AllocationStats>();

            float multiplier = 100.0f / (float)(m_leftReport.Index + 1);

            List<TrackerAction> trackerActions = m_trackerComponent.Actions;
            for ( int i = 0; i <= leftReportIndex; ++i )
            {
                TrackerAction action = trackerActions[i];

                switch ( action.TheAction )
                {
                    case TrackerAction.Action.TallyAllocation:
                        if ( showAllocations )
                        {
                            TallyAllocationAction tallyAction = action as TallyAllocationAction;
                            TallyOverviewAllocation( tallyAction.TheAllocation, DisplayType.DataTypes, 
                                rightDone, leftSortedDictionary, rightSortedDictionary );
                        }
                        break;
                    case TrackerAction.Action.UntallyAllocation:
                        {
                            UntallyAllocationAction untallyAction = action as UntallyAllocationAction;
                            if ( showAllocations )
                            {
                                UntallyOverviewAllocation( untallyAction.TheAllocation, DisplayType.DataTypes,
                                    rightDone, leftSortedDictionary, rightSortedDictionary );
                            }
                            else
                            {
                                TallyOverviewAllocation( untallyAction.TheAllocation, DisplayType.DataTypes, 
                                    rightDone, leftSortedDictionary, rightSortedDictionary );
                            }
                        }
                        break;
                    case TrackerAction.Action.Report:
                        if ( i == rightReportIndex )
                        {
                            rightDone = true;
                        }
                        break;
                }

                int percentComplete = (int)((float)i * multiplier);
                if ( m_threadingEnabled )
                {
                    backgroundWorker.ReportProgress( percentComplete );
                }
                else
                {
                    OnUpdateStep( percentComplete, m_reportGenerated );
                }
            }

            // remove entries that we don't want to show
            List<string> removedKeys = new List<string>();
            foreach ( KeyValuePair<string, AllocationStats> pair in leftSortedDictionary )
            {
                bool visible = true;
                if ( pair.Key == Allocation.Unknown )
                {
                    visible = this.ShowUnknownDatatypes;
                }
                else
                {
                    bool vis;
                    if ( m_datatypeVisibility.TryGetValue( pair.Value.HashKey, out vis ) )
                    {
                        visible = vis;
                    }
                }

                if ( !visible )
                {
                    removedKeys.Add( pair.Key );
                    continue;
                }

                AllocationStats leftStat = leftSortedDictionary[pair.Key];
                if ( !m_showEmptyItems && (leftStat.Inclusive == 0) && (leftStat.Exclusive == 0) )
                {
                    AllocationStats rightStat = null;
                    if ( rightSortedDictionary.TryGetValue( pair.Key, out rightStat ) )
                    {
                        if ( (rightStat.Inclusive == 0) && (rightStat.Exclusive == 0) )
                        {
                            removedKeys.Add( pair.Key );
                            continue;
                        }
                    }
                    else
                    {
                        removedKeys.Add( pair.Key );
                        continue;
                    }
                }
            }

            foreach ( string key in removedKeys )
            {
                leftSortedDictionary.Remove( key );
                rightSortedDictionary.Remove( key );
            }

            // create rows
            m_rows = new ListViewItem[leftSortedDictionary.Count];
            int rowNum = 0;
            foreach ( KeyValuePair<string, AllocationStats> pair in leftSortedDictionary )
            {
                AllocationStats leftStat = pair.Value;
                AllocationStats rightStat;
                bool haveRightStat = rightSortedDictionary.TryGetValue( pair.Key, out rightStat );

                ListViewItem row = new ListViewItem( rowNum.ToString(), 0 );
                row.SubItems.Add( leftStat.Name );

                long diff = leftStat.Inclusive;
                if ( haveRightStat )
                {
                    diff -= rightStat.Inclusive;
                }

                row.SubItems.Add( leftStat.Inclusive.ToString( "n" ).Replace( ".00", "" ) );
                row.SubItems.Add( haveRightStat ? rightStat.Inclusive.ToString( "n" ).Replace( ".00", "" ) : string.Empty );
                row.SubItems.Add( diff.ToString( "n" ).Replace( ".00", "" ) );

                m_rows[rowNum] = row;

                rowNum++;
            }

            m_trackerComponent.LockForUpdate = false;

            return 1;
        }

        protected virtual int GatherMemoryTypeData()
        {
            if ( m_leftReport == null )
            {
                return 0;
            }

            m_trackerComponent.LockForUpdate = true;

            bool showAllocations = m_leftReport.TrackedDeallocations ? m_showAllocations : true;

            // determine how many actions to look at
            int leftReportIndex = m_leftReport.Index;
            int rightReportIndex = -1;
            if ( m_rightReport != null )
            {
                rightReportIndex = m_rightReport.Index;
            }

            bool rightDone = (rightReportIndex > -1) && (rightReportIndex < leftReportIndex) ? false : true;

            SortedDictionary<string, AllocationStats> leftSortedDictionary = new SortedDictionary<string, AllocationStats>();
            SortedDictionary<string, AllocationStats> rightSortedDictionary = new SortedDictionary<string, AllocationStats>();

            List<TrackerAction> trackerActions = m_trackerComponent.Actions;
            for ( int i = 0; i < Allocation.MemoryTypeNames.Length; ++i )
            {
                if ( ((Allocation.MemoryTypeIndex)i == Allocation.MemoryTypeIndex.ConsoleSpecific)
                    && (m_trackerComponent.Platform != "Xenon") )
                {
                    continue;
                }

                leftSortedDictionary.Add( Allocation.MemoryTypeNames[i], 
                    new AllocationStats( Allocation.MemoryTypeNames[i], (uint)Allocation.MemoryTypeNames[i].GetHashCode() ) );

                if ( !rightDone )
                {
                    rightSortedDictionary.Add( Allocation.MemoryTypeNames[i], 
                        new AllocationStats( Allocation.MemoryTypeNames[i], (uint)Allocation.MemoryTypeNames[i].GetHashCode() ) );
                }
            }

            float multiplier = 100.0f / (float)(m_leftReport.Index + 1);

            for ( int i = 0; i <= leftReportIndex; ++i )
            {
                TrackerAction action = trackerActions[i];

                switch ( action.TheAction )
                {
                    case TrackerAction.Action.TallyAllocation:
                        if ( showAllocations )
                        {
                            TallyAllocationAction tallyAction = action as TallyAllocationAction;
                            TallyOverviewAllocation( tallyAction.TheAllocation, DisplayType.MemoryType, 
                                rightDone, leftSortedDictionary, rightSortedDictionary );
                        }
                        break;
                    case TrackerAction.Action.UntallyAllocation:
                        {
                            UntallyAllocationAction untallyAction = action as UntallyAllocationAction;
                            if ( showAllocations )
                            {
                                UntallyOverviewAllocation( untallyAction.TheAllocation, DisplayType.MemoryType, 
                                    rightDone, leftSortedDictionary, rightSortedDictionary );
                            }
                            else
                            {
                                TallyOverviewAllocation( untallyAction.TheAllocation, DisplayType.MemoryType, 
                                    rightDone, leftSortedDictionary, rightSortedDictionary );
                            }
                        }
                        break;
                    case TrackerAction.Action.Report:
                        if ( i == rightReportIndex )
                        {
                            rightDone = true;
                        }
                        break;
                }

                int percentComplete = (int)((float)i * multiplier);
                if ( m_threadingEnabled )
                {
                    backgroundWorker.ReportProgress( percentComplete );
                }
                else
                {
                    OnUpdateStep( percentComplete, m_reportGenerated );
                }
            }

            // create rows
            m_rows = new ListViewItem[leftSortedDictionary.Count + 1];
            int rowNum = 0;
            long[] totals = new long[6];
            foreach ( KeyValuePair<string, AllocationStats> pair in leftSortedDictionary )
            {
                AllocationStats leftStat = pair.Value;
                AllocationStats rightStat;
                bool haveRightStat = rightSortedDictionary.TryGetValue( pair.Key, out rightStat );

                ListViewItem row = new ListViewItem( rowNum.ToString(), 0 );
                row.SubItems.Add( leftStat.Name );

                long diff = leftStat.Inclusive;
                if ( haveRightStat )
                {
                    diff -= rightStat.Inclusive;
                }

                row.SubItems.Add( leftStat.Inclusive.ToString( "n" ).Replace( ".00", "" ) );
                row.SubItems.Add( haveRightStat ? rightStat.Inclusive.ToString( "n" ).Replace( ".00", "" ) : string.Empty );
                row.SubItems.Add( diff.ToString( "n" ).Replace( ".00", "" ) );

                totals[0] += leftStat.Inclusive;
                totals[1] += haveRightStat ? rightStat.Inclusive : 0;
                totals[2] += diff;

                int availableIndex = -1;
                for ( int i = 0; i < Allocation.MemoryTypeNames.Length; ++i )
                {
                    if ( Allocation.MemoryTypeNames[i] == pair.Key )
                    {
                        availableIndex = i;
                        break;
                    }
                }

                diff = m_leftReport.Available[availableIndex];
                if ( haveRightStat )
                {
                    diff -= m_rightReport.Available[availableIndex];
                }

                row.SubItems.Add( m_leftReport.Available[availableIndex].ToString( "n" ).Replace( ".00", "" ) );
                row.SubItems.Add( haveRightStat ? m_rightReport.Available[availableIndex].ToString( "n" ).Replace( ".00", "" ) : string.Empty );
                row.SubItems.Add( diff.ToString( "n" ).Replace( ".00", "" ) );

                // ResourceVirtual and GamePhysical and ResourcePhysical use the same memory allocation, so only total them once
                if ( (m_trackerComponent.Platform == "PS3")
                    || ((availableIndex != (int)Allocation.MemoryTypeIndex.ResourceVirtual)
                    && (availableIndex != (int)Allocation.MemoryTypeIndex.GamePhysical)) )
                {
                    totals[3] += m_leftReport.Available[availableIndex];
                    totals[4] += haveRightStat ? m_rightReport.Available[availableIndex] : 0;
                    totals[5] += diff;
                }

                m_rows[rowNum] = row;

                rowNum++;
            }

            // add TOTALS row
            ListViewItem totalsRow = new ListViewItem( rowNum.ToString(), 0 );
            totalsRow.SubItems.Add( "{{TOTALS}}" );
            foreach ( long i in totals )
            {
                totalsRow.SubItems.Add( i.ToString( "n" ).Replace( ".00", "" ) );
            }

            m_rows[rowNum] = totalsRow;
            
            rowNum++;

            m_trackerComponent.LockForUpdate = false;

            return 1;
        }

        protected virtual void OnUpdateBegin( int numSteps, bool generatingReport )
        {
            if ( this.UpdateBegin != null )
            {
                UpdateBeginEventArgs e = new UpdateBeginEventArgs( numSteps, generatingReport );
                this.UpdateBegin( this, e );
            }
        }

        protected virtual void OnUpdateStep( int stepNumber, bool generatingReport )
        {
            if ( this.UpdateStep != null )
            {
                UpdateStepEventArgs e = new UpdateStepEventArgs( stepNumber, generatingReport );
                this.UpdateStep( this, e );
            }
        }

        protected virtual void OnUpdateComplete( ReportItem reportGenerated, bool success )
        {
            if ( this.UpdateComplete != null )
            {
                UpdateCompleteEventArgs e = new UpdateCompleteEventArgs( reportGenerated, success );
                this.UpdateComplete( this, e );
            }
        }        

        protected virtual void OnShowByChanged()
        {
            if ( this.ShowByChanged != null )
            {
                this.ShowByChanged( this, new EventArgs() );
            }
        }

        protected virtual void OnShowUnassignedStackChanged()
        {
            if ( this.ShowUnassignedStackChanged != null )
            {
                this.ShowUnassignedStackChanged( this, new EventArgs() );
            }
        }

        protected virtual void OnSelectedStacksChanged()
        {
            if ( this.SelectedStacksChanged != null )
            {
                this.SelectedStacksChanged( this, new EventArgs() );
            }
        }

        protected virtual void OnShowNativeDatatypesChanged()
        {
            if ( this.ShowNativeDatatypesChanged != null )
            {
                this.ShowNativeDatatypesChanged( this, new EventArgs() );
            }
        }

        protected virtual void OnShowClassDatatypesChanged()
        {
            if ( this.ShowClassDatatypesChanged != null )
            {
                this.ShowClassDatatypesChanged( this, new EventArgs() );
            }
        }

        protected virtual void OnShowStructureDatatypesChanged()
        {
            if ( this.ShowStructureDatatypesChanged != null )
            {
                this.ShowStructureDatatypesChanged( this, new EventArgs() );
            }
        }

        protected virtual void OnShowUnknownDatatypesChanged()
        {
            if ( this.ShowUnknownDatatypesChanged != null )
            {
                this.ShowUnknownDatatypesChanged( this, new EventArgs() );
            }
        }

        protected virtual void OnSelectedDatatypesChanged()
        {
            if ( this.SelectedDatatypesChanged != null )
            {
                this.SelectedDatatypesChanged( this, new EventArgs() );
            }
        }

        protected virtual void OnShowAllocationsChanged()
        {
            if ( this.ShowAllocationsChanged != null )
            {
                this.ShowAllocationsChanged( this, new EventArgs() );
            }
        }

        protected virtual void OnShowEmptyItemsChanged()
        {
            if ( this.ShowEmptyItemsChanged != null )
            {
                this.ShowEmptyItemsChanged( this, new EventArgs() );
            }
        }
        #endregion

        #region Private Functions
        private void InitializeAdditionalComponents()
        {
            // setup overview column headers:
            string[] stackNameHeaderNames = new string[c_numStackNameColumnHeaders] { "#", "Name", 
                "Left Inclusive", "Right Inclusive", "Inclusive Difference",
                "Left Exclusive", "Right Exclusive", "Exclusive Difference" };
            int[] stackNameWidths = new int[c_numStackNameColumnHeaders] { 45, 95, 
                85, 85, 115,
                85, 85, 115 };
            for ( int i = 0; i < c_numStackNameColumnHeaders; i++ )
            {
                m_stackNameColumnHeaders[i] = new ColumnHeader();
                m_stackNameColumnHeaders[i].Text = stackNameHeaderNames[i];
                m_stackNameColumnHeaders[i].TextAlign = (i >= 2) ? HorizontalAlignment.Right : HorizontalAlignment.Left;
                m_stackNameColumnHeaders[i].Width = stackNameWidths[i];
            }

            string[] dataTypeHeaderNames = new String[c_numDataTypeColumnHeaders] { "#", "Data Type",
                "Left Size", "Right Size", "Difference" };
            int[] dataTypeWidths = new int[c_numDataTypeColumnHeaders] { 45, 95,
                85, 85, 115 };
            for ( int i = 0; i < c_numDataTypeColumnHeaders; i++ )
            {
                m_dataTypeColumnHeaders[i] = new ColumnHeader();
                m_dataTypeColumnHeaders[i].Text = dataTypeHeaderNames[i];
                m_dataTypeColumnHeaders[i].TextAlign = (i >= 2) ? HorizontalAlignment.Right : HorizontalAlignment.Left;
                m_dataTypeColumnHeaders[i].Width = dataTypeWidths[i];
            }

            string[] memoryTypeHeaderNames = new String[c_numMemoryTypeColumnHeaders] { "#", "Memory Type",
                "Left Size", "Right Size", "Size Difference", 
                "Left Available", "Right Available", "Available Difference" };
            int[] memoryTypeWidths = new int[c_numMemoryTypeColumnHeaders] { 45, 95, 
                85, 85, 115,
                85, 85, 115 };
            for ( int i = 0; i < c_numMemoryTypeColumnHeaders; i++ )
            {
                m_memoryTypeColumnHeaders[i] = new ColumnHeader();
                m_memoryTypeColumnHeaders[i].Text = memoryTypeHeaderNames[i];
                m_memoryTypeColumnHeaders[i].TextAlign = (i >= 2) ? HorizontalAlignment.Right : HorizontalAlignment.Left;
                m_memoryTypeColumnHeaders[i].Width = memoryTypeWidths[i];
            }

            stackNameHeaderNames = new string[c_numStackNameColumnHeaders] { "#", "Name", 
                "Left Inclusive Free'd", "Right Inclusive Free'd", "Inclusive Free'd Difference",
                "Left Exclusive Free'd", "Right Exclusive Free'd", "Exclusive Free'd Difference" };
            stackNameWidths = new int[c_numStackNameColumnHeaders] { 45, 95, 
                115, 115, 145,
                115, 115, 145 };
            for ( int i = 0; i < c_numStackNameColumnHeaders; i++ )
            {
                m_stackNameFreedColumnHeaders[i] = new ColumnHeader();
                m_stackNameFreedColumnHeaders[i].Text = stackNameHeaderNames[i];
                m_stackNameFreedColumnHeaders[i].TextAlign = (i >= 2) ? HorizontalAlignment.Right : HorizontalAlignment.Left;
                m_stackNameFreedColumnHeaders[i].Width = stackNameWidths[i];
            }

            dataTypeHeaderNames = new String[c_numDataTypeColumnHeaders] { "#", "Data Type",
                "Left Free'd", "Right Free'd", "Free'd Difference" };
            dataTypeWidths = new int[c_numDataTypeColumnHeaders] { 45, 95, 
                85, 85, 115 };
            for ( int i = 0; i < c_numDataTypeColumnHeaders; i++ )
            {
                m_dataTypeFreedColumnHeaders[i] = new ColumnHeader();
                m_dataTypeFreedColumnHeaders[i].Text = dataTypeHeaderNames[i];
                m_dataTypeFreedColumnHeaders[i].TextAlign = (i >= 2) ? HorizontalAlignment.Right : HorizontalAlignment.Left;
                m_dataTypeFreedColumnHeaders[i].Width = dataTypeWidths[i];
            }

            memoryTypeHeaderNames = new String[c_numMemoryTypeColumnHeaders] { "#", "Memory Type",
                "Left Free'd", "Right Free'd", "Free'd Difference", 
                "Left Available", "Right Available", "Available Difference" };
            memoryTypeWidths = new int[c_numMemoryTypeColumnHeaders] { 45, 95, 
                85, 85, 115,
                85, 85, 115 };
            for ( int i = 0; i < c_numMemoryTypeColumnHeaders; i++ )
            {
                m_memoryTypeFreedColumnHeaders[i] = new ColumnHeader();
                m_memoryTypeFreedColumnHeaders[i].Text = memoryTypeHeaderNames[i];
                m_memoryTypeFreedColumnHeaders[i].TextAlign = (i >= 2) ? HorizontalAlignment.Right : HorizontalAlignment.Left;
                m_memoryTypeFreedColumnHeaders[i].Width = memoryTypeWidths[i];
            }

            // set our initial column headers
            SetColumnHeaders( true, m_showAllocations );

            // add a blank item to the right report
            this.rightReportToolStripComboBox.Items.Add( string.Empty );
            this.rightReportToolStripComboBox.SelectedIndex = 0;

            this.RefreshNeeded = false;
        }

        private void SetColumnHeaders( bool init, bool showAllocations )
        {
            this.listView1.Items.Clear();
            this.listView1.Columns.Clear();

            ColumnHeader[] headers = null;
            switch ( m_showBy )
            {
                case DisplayType.DataTypes:
                    headers = showAllocations ? m_dataTypeColumnHeaders : m_dataTypeFreedColumnHeaders;
                    break;
                case DisplayType.MemoryType:
                    headers = showAllocations ? m_memoryTypeColumnHeaders : m_memoryTypeFreedColumnHeaders;
                    break;
                case DisplayType.StackNames:
                    headers = showAllocations ? m_stackNameColumnHeaders : m_stackNameFreedColumnHeaders;
                    break;
            }

            // add initial column headers
            foreach ( ColumnHeader header in headers )
            {
                this.listView1.Columns.Add( header );
            }

            if ( (this.DesignMode || init) && (m_showBy == DisplayType.MemoryType) )
            {
                // add totals row
                ListViewItem row = new ListViewItem( "0", 0 );
                row.SubItems.Add( "{{TOTALS}}" );
                for ( int i = 0; i < 6; ++i )
                {
                    row.SubItems.Add( "0" );
                }

                this.listView1.Items.Add( row );
            }
        }

        private void TallyOverviewAllocation( Allocation theAllocation, DisplayType showBy, bool rightDone,
            SortedDictionary<string, AllocationStats> leftSortedDictionary, SortedDictionary<string, AllocationStats> rightSortedDictionary )
        {
            if ( showBy == DisplayType.StackNames )
            {
                // ignore tallies that are array members
                theAllocation.AddStackMemory( leftSortedDictionary );

                if ( !rightDone )
                {
                    theAllocation.AddStackMemory( rightSortedDictionary );
                }
            }
            else if ( showBy == DisplayType.DataTypes )
            {
                if ( !leftSortedDictionary.ContainsKey( theAllocation.TypeName ) )
                {
                    leftSortedDictionary.Add( theAllocation.TypeName, 
                        new AllocationStats( theAllocation.TypeName, theAllocation.TypeNameHashCode ) );
                }

                theAllocation.AddDataTypeMemory( leftSortedDictionary );

                if ( !rightDone )
                {
                    if ( !rightSortedDictionary.ContainsKey( theAllocation.TypeName ) )
                    {
                        rightSortedDictionary.Add( theAllocation.TypeName, 
                            new AllocationStats( theAllocation.TypeName, theAllocation.TypeNameHashCode ) );
                    }

                    theAllocation.AddDataTypeMemory( rightSortedDictionary );
                }
            }
            else
            {
                theAllocation.AddMemoryTypeMemory( leftSortedDictionary );

                if ( !rightDone )
                {
                    theAllocation.AddMemoryTypeMemory( rightSortedDictionary );
                }
            }
        }

        private void UntallyOverviewAllocation( Allocation theAllocation, DisplayType showBy, bool rightDone,
            SortedDictionary<string, AllocationStats> leftSortedDictionary, SortedDictionary<string, AllocationStats> rightSortedDictionary )
        {
            if ( showBy == DisplayType.StackNames )
            {
                theAllocation.RemoveStackMemory( leftSortedDictionary );

                if ( !rightDone )
                {
                    theAllocation.RemoveStackMemory( rightSortedDictionary );
                }
            }
            else if ( showBy == DisplayType.DataTypes )
            {
                theAllocation.RemoveDataTypeMemory( leftSortedDictionary );

                if ( !rightDone )
                {
                    theAllocation.RemoveDataTypeMemory( rightSortedDictionary );
                }
            }
            else
            {
                theAllocation.RemoveMemoryTypeMemory( leftSortedDictionary );

                if ( !rightDone )
                {
                    theAllocation.RemoveMemoryTypeMemory( rightSortedDictionary );
                }
            }
        }

        private void DetermineVisibleStacks()
        {
            // determine what stacks we have selected
            int visibleCount = 0;
            foreach ( bool vis in m_stackVisibility.Values )
            {
                if ( vis )
                {
                    ++visibleCount;
                }
            }

            // set selected stacks
            if ( (visibleCount == 0) && !this.ShowUnassignedStack )
            {
                m_selectedStacks = SelectedStacks.None;
            }
            else if ( (visibleCount == m_stackVisibility.Count) && this.ShowUnassignedStack )
            {
                m_selectedStacks = SelectedStacks.All;
            }
            else
            {
                m_selectedStacks = SelectedStacks.Selected;
            }
        }

        private void DetermineVisibleDatatypes()
        {
            // determine what datatypes we have selected
            int visibleCount = 0;
            foreach ( bool vis in m_datatypeVisibility.Values )
            {
                if ( vis )
                {
                    ++visibleCount;
                }
            }

            // set selected datatypes
            if ( (visibleCount == 0) && !this.ShowUnknownDatatypes )
            {
                m_selectedDatatypes = SelectedDatatypes.None;
            }
            else if ( (visibleCount == m_datatypeVisibility.Count) && this.ShowUnknownDatatypes )
            {
                m_selectedDatatypes = SelectedDatatypes.All;
            }
            else
            {
                m_selectedDatatypes = SelectedDatatypes.Selected;
            }
        }

        /// <summary>
        /// Internal function to set the visible stacks.
        /// </summary>
        /// <param name="selectedStacks">The <see cref="SelectedStacks"/> enumeration to set the visibility to.</param>
        /// <param name="stacksChanged">Return if any of the individual stacks' visibility changed.</param>
        /// <param name="unassignedStackChanged">Return if the unassigned stack visibility changed.</param>
        /// <returns><c>true</c> if m_selectedStacks changed, otherwise, <c>false</c>.</returns>
        private bool SetSelectedStacks( SelectedStacks selectedStacks, out bool stacksChanged, out bool unassignedStackChanged )
        {
            stacksChanged = false;
            unassignedStackChanged = false;

            if ( m_selectedStacks != selectedStacks )
            {
                m_selectedStacks = selectedStacks;

                if ( !this.DesignMode )
                {
                    this.RefreshNeeded = true;

                    if ( m_selectedStacks != SelectedStacks.Selected )
                    {
                        bool vis = m_selectedStacks == SelectedStacks.All;

                        m_stackMutex.WaitOne();

                        List<uint> changedStacks = new List<uint>();
                        foreach ( KeyValuePair<uint, bool> pair in m_stackVisibility )
                        {
                            if ( pair.Value != vis )
                            {
                                changedStacks.Add( pair.Key );
                            }
                        }

                        stacksChanged = changedStacks.Count > 0;
                        if ( stacksChanged )
                        {
                            foreach ( uint changedStack in changedStacks )
                            {
                                m_stackVisibility[changedStack] = vis;
                            }
                        }

                        m_stackMutex.ReleaseMutex();

                        unassignedStackChanged = m_showUnassignedStack != vis;
                        if ( unassignedStackChanged )
                        {
                            m_showUnassignedStack = vis;
                        }
                    }
                }

                return true;
            }

            return false;
        }

        /// <summary>
        /// Internal function to set the visible datatypes.
        /// </summary>
        /// <param name="selectedDatatypes">The <see cref="SelectedDatatypes"/> enumeration to set the visibility to.</param>
        /// <param name="selectedDatatypesChanged">Return if any of the individual datatypes' visibility changed.</param>
        /// <param name="nativeDatatypesChanged">Return if the native datatypes visibility changed.</param>
        /// <param name="classesChanged">Return if the classes visibility changed.</param>
        /// <param name="structuresChanged">Return if the structures visibility changed.</param>
        /// <param name="unknownDatatypesChanged">Return if the unknown datatypes visibility changed.</param>
        /// <returns><c>true</c> if m_selectedDatatypes changed, otherwise, <c>false</c>.</returns>
        private bool SetSelectedDatatypes( SelectedDatatypes selectedDatatypes, out bool selectedDatatypesChanged,
            out bool nativeDatatypesChanged, out bool classesChanged, out bool structuresChanged, out bool unknownDatatypesChanged )
        {
            selectedDatatypesChanged = false;
            nativeDatatypesChanged = false;
            classesChanged = false;
            structuresChanged = false;
            unknownDatatypesChanged = false;

            if ( m_selectedDatatypes != selectedDatatypes )
            {
                m_selectedDatatypes = selectedDatatypes;

                if ( !this.DesignMode )
                {
                    this.RefreshNeeded = true;

                    if ( m_selectedDatatypes != SelectedDatatypes.Selected )
                    {
                        bool vis = m_selectedDatatypes == SelectedDatatypes.All;

                        m_datatypeMutex.WaitOne();

                        List<uint> changedDatatypes = new List<uint>();
                        foreach ( KeyValuePair<uint, bool> pair in m_datatypeVisibility )
                        {
                            if ( pair.Value != vis )
                            {
                                changedDatatypes.Add( pair.Key );
                            }
                        }

                        selectedDatatypesChanged = changedDatatypes.Count > 0;
                        if ( selectedDatatypesChanged )
                        {
                            foreach ( uint changedDatatype in changedDatatypes )
                            {
                                m_datatypeVisibility[changedDatatype] = vis;
                            }
                        }

                        m_datatypeMutex.ReleaseMutex();

                        nativeDatatypesChanged = m_showNativeDatatypes != vis;
                        if ( nativeDatatypesChanged )
                        {
                            m_showNativeDatatypes = vis;
                        }

                        classesChanged = m_showClassDatatypes != vis;
                        if ( classesChanged )
                        {
                            m_showClassDatatypes = vis;
                        }

                        structuresChanged = m_showStructDatatypes != vis;
                        if ( structuresChanged )
                        {
                            m_showStructDatatypes = vis;
                        }

                        unknownDatatypesChanged = m_showUnknownDatatypes != vis;
                        if ( unknownDatatypesChanged )
                        {
                            m_showUnknownDatatypes = vis;
                        }
                    }
                }

                return true;
            }

            return false;
        }
        #endregion

        #region ListView Event Handlers
        private void listView1_SelectedIndexChanged( object sender, EventArgs e )
        {
            int[] totals = new int[this.listView1.Columns.Count - 2];
            for ( int i = 0; i < totals.Length; ++i )
            {
                totals[i] = 0;
            }

            foreach ( ListViewItem item in this.listView1.SelectedItems )
            {
                if ( item.SubItems[1].Text != "{{TOTALS}}" )
                {
                    for ( int i = 0; i < totals.Length; ++i )
                    {
                        string sizeText = item.SubItems[i + 2].Text.Replace( ",", "" );
                        if ( sizeText != string.Empty )
                        {
                            totals[i] += int.Parse( sizeText );
                        }
                    }
                }
            }

            StringBuilder text = new StringBuilder( "Selected Size:  " );
            for ( int i = 0; i < this.listView1.Columns.Count - 2; ++i )
            {
                if ( i > 0 )
                {
                    text.Append( " / " );
                }

                text.Append( this.listView1.Columns[i + 2].Name );
                text.Append( ' ' );
                text.Append( totals[i].ToString( "n" ).Replace( ".00", "" ) );
            }
            text.Append( " bytes" );

            this.selectedSizeToolStripStatusLabel.Text = text.ToString();
        }
        #endregion

        #region ContextMenu Event Handlers
        private void copyToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.listView1.SelectedItems.Count > 0 )
            {
                StringBuilder text = new StringBuilder();

                int length = 0;
                for ( int i = 0; i < this.listView1.Columns.Count; ++i )
                {
                    if ( i > 0 )
                    {
                        text.Append( '\t' );
                        length += 4;
                    }

                    text.Append( this.listView1.Columns[i].Name.ToUpper() );
                    length += this.listView1.Columns[i].Name.Length;
                }
                text.Append( '\n' );

                for ( int i = 0; i < length; ++i )
                {
                    text.Append( '-' );
                }
                text.Append( '\n' );

                foreach ( ListViewItem item in this.listView1.SelectedItems )
                {
                    for ( int i = 0; i < item.SubItems.Count; ++i )
                    {
                        if ( i > 0 )
                        {
                            text.Append( "    " );
                        }

                        text.Append( item.SubItems[i].Text );
                    }
                    text.Append( '\n' );
                }

                Clipboard.SetText( text.ToString() );
            }
        }

        private void selectAllToolStripMenuItem_Click( object sender, EventArgs e )
        {
            this.listView1.SelectedIndices.Clear();

            for ( int i = 0; i < this.listView1.Items.Count; ++i )
            {
                this.listView1.SelectedIndices.Add( i );
            }
        }
        #endregion

        #region ToolStrip Event Handlers
        private void generateReportToolStripButton_Click( object sender, EventArgs e )
        {
            ReportItem report = m_trackerComponent.AddReportAction( 0, string.Empty, 
                m_trackerComponent.AvailableMemory, m_trackerComponent.TrackDeallocations );

            FillData( report );
        }

        private void leftReportToolStripComboBox_SelectedIndexChanged( object sender, EventArgs e )
        {
            this.LeftReport = this.leftReportToolStripComboBox.SelectedItem as ReportItem;
        }

        private void rightReportToolStripComboBox_SelectedIndexChanged( object sender, EventArgs e )
        {
            int lastIndex = 0;
            if ( m_rightReport != null )
            {
                lastIndex = this.rightReportToolStripComboBox.Items.IndexOf( m_rightReport );
            }

            int rightIndex = this.rightReportToolStripComboBox.SelectedIndex;
            if ( rightIndex > -1 )
            {
                if ( m_leftReport == null )
                {
                    rightIndex = 0;
                }
                else
                {
                    int leftIndex = this.leftReportToolStripComboBox.Items.IndexOf( m_leftReport );
                    if ( rightIndex > leftIndex )
                    {
                        rightIndex = leftIndex;
                    }
                }
            }

            if ( rightIndex > 0 )
            {
                m_rightReport = this.rightReportToolStripComboBox.Items[rightIndex] as ReportItem;
            }
            else
            {
                m_rightReport = null;
            }

            if ( rightIndex != this.rightReportToolStripComboBox.SelectedIndex )
            {
                this.rightReportToolStripComboBox.SelectedIndex = rightIndex;
            }

            if ( lastIndex != this.rightReportToolStripComboBox.SelectedIndex )
            {
                this.RefreshNeeded = true;
            }
        }

        #region Options Drop Down Menu
        private void optionsToolStripDropDownButton_DropDownOpening( object sender, EventArgs e )
        {
            this.stacksToolStripMenuItem.Enabled = m_showBy == DisplayType.StackNames;
            this.datatypesToolStripMenuItem.Enabled = m_showBy == DisplayType.DataTypes;
            this.showEmptyItemsToolStripMenuItem.Checked = m_showEmptyItems;

            foreach ( ToolStripItem item in this.stacksToolStripMenuItem.DropDownItems )
            {
                item.Enabled = this.stacksToolStripMenuItem.Enabled;
            }

            foreach ( ToolStripItem item in this.datatypesToolStripMenuItem.DropDownItems )
            {
                item.Enabled = this.datatypesToolStripMenuItem.Enabled;
            }
        }

        private void showByToolStripMenuItem_DropDownOpening( object sender, EventArgs e )
        {
            this.showByDatatypeToolStripMenuItem.Checked = m_showBy == DisplayType.DataTypes;
            this.showByMemoryTypeToolStripMenuItem.Checked = m_showBy == DisplayType.MemoryType;
            this.showByStackNameToolStripMenuItem.Checked = m_showBy == DisplayType.StackNames;

            bool enable = m_trackerComponent.TrackDeallocations;
            if ( m_leftReport != null )
            {
                enable = m_leftReport.TrackedDeallocations;
                if ( enable && (m_rightReport != null) && !m_rightReport.TrackedDeallocations )
                {
                    enable = false;
                }
            }

            this.showByAllocationsToolStripMenuItem.Enabled = enable;
            this.showByDeallocationsToolStripMenuItem.Enabled = enable;

            this.showByAllocationsToolStripMenuItem.Checked = m_showAllocations == true;
            this.showByDeallocationsToolStripMenuItem.Checked = m_showAllocations == false;
        }

        #region ShowBy Drop Down Menu
        private void showByStackNameToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.ShowBy != DisplayType.StackNames )
            {
                this.ShowBy = DisplayType.StackNames;
                OnShowByChanged();
            }
        }

        private void showByDatatypeToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.ShowBy != DisplayType.DataTypes )
            {
                this.ShowBy = DisplayType.DataTypes;
                OnShowByChanged();
            }
        }

        private void showByMemoryTypeToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.ShowBy != DisplayType.MemoryType )
            {
                this.ShowBy = DisplayType.MemoryType;
                OnShowByChanged();
            }
        }

        private void showByAllocationsToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.ShowAllocations != true )
            {
                this.ShowAllocations = true;
                OnShowAllocationsChanged();
            }
        }

        private void showByDeallocationsToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.ShowAllocations != false )
            {
                this.ShowAllocations = false;
                OnShowAllocationsChanged();
            }
        }
        #endregion

        private void stacksToolStripMenuItem_DropDownOpening( object sender, EventArgs e )
        {
            this.stacksUnassignedToolStripMenuItem.Checked = this.ShowUnassignedStack;
        }

        #region Stacks Drop Down Menu
        private void stacksAllToolStripMenuItem_Click( object sender, EventArgs e )
        {
            bool stacksChanged;
            bool unassignedStackChanged;
            if ( SetSelectedStacks( SelectedStacks.All, out stacksChanged, out unassignedStackChanged ) )
            {
                if ( stacksChanged )
                {
                    OnSelectedStacksChanged();
                }

                if ( unassignedStackChanged )
                {
                    OnShowUnassignedStackChanged();
                }
            }
        }

        private void stacksNoneToolStripMenuItem_Click( object sender, EventArgs e )
        {
            bool stacksChanged;
            bool unassignedStackChanged;
            if ( SetSelectedStacks( SelectedStacks.None, out stacksChanged, out unassignedStackChanged ) )
            {
                if ( stacksChanged )
                {
                    OnSelectedStacksChanged();
                }

                if ( unassignedStackChanged )
                {
                    OnShowUnassignedStackChanged();
                }
            }
        }

        private void stacksUnassignedToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.ShowUnassignedStack != this.stacksUnassignedToolStripMenuItem.Checked )
            {
                this.ShowUnassignedStack = this.stacksUnassignedToolStripMenuItem.Checked;
                OnShowUnassignedStackChanged();
            }
        }

        private void stacksSelectToolStripMenuItem_Click( object sender, EventArgs e )
        {
            Dictionary<uint, bool> stackVisibility = m_stackVisibility;
            bool showUnassignedStack = m_showUnassignedStack;
            
            if ( ItemVisibilityForm.ShowIt( m_parentControl, "Select Stacks",
                ref stackVisibility, Allocation.StackNameDictionary, ref showUnassignedStack, Allocation.Unassigned ) )
            {
                m_stackMutex.WaitOne();

                bool selectedStacksChanged = false;
                foreach ( KeyValuePair<uint, bool> pair in stackVisibility )
                {
                    if ( pair.Value != m_stackVisibility[pair.Key] )
                    {
                        selectedStacksChanged = true;
                        break;
                    }
                }

                if ( selectedStacksChanged )
                {
                    m_stackVisibility = stackVisibility;
                }

                DetermineVisibleStacks();

                m_stackMutex.ReleaseMutex();

                this.RefreshNeeded = true;

                if ( selectedStacksChanged )
                {
                    OnSelectedStacksChanged();
                }

                if ( m_showUnassignedStack != showUnassignedStack )
                {
                    m_showUnassignedStack = showUnassignedStack;
                    OnShowUnassignedStackChanged();
                }
            }
        }
        #endregion

        private void datatypesToolStripMenuItem_DropDownOpening( object sender, EventArgs e )
        {
            this.datatypesNativeTypesToolStripMenuItem.Checked = this.ShowNativeDatatypes;
            this.datatypesClassesToolStripMenuItem.Checked = this.ShowClassDatatypes;
            this.datatypesStructuresToolStripMenuItem.Checked = this.ShowStructureDatatypes;
            this.datatypesUnknownTypesToolStripMenuItem.Checked = this.ShowUnknownDatatypes;
        }

        #region Datatypes Drop Down Menu
        private void datatypesAllToolStripMenuItem_Click( object sender, EventArgs e )
        {
            bool selectedDatatypesChanged;
            bool nativeDatatypesChanged;
            bool classesChanged;
            bool structuresChanged;
            bool unknownDatatypesChanged;
            if ( SetSelectedDatatypes( SelectedDatatypes.All, out selectedDatatypesChanged,
                out nativeDatatypesChanged, out classesChanged, out structuresChanged, out unknownDatatypesChanged ) )
            {
                if ( selectedDatatypesChanged )
                {
                    OnSelectedDatatypesChanged();
                }

                if ( nativeDatatypesChanged )
                {
                    OnShowNativeDatatypesChanged();
                }

                if ( classesChanged )
                {
                    OnShowClassDatatypesChanged();
                }

                if ( structuresChanged )
                {
                    OnShowStructureDatatypesChanged();
                }

                if ( unknownDatatypesChanged )
                {
                    OnShowUnknownDatatypesChanged();
                }
            }
        }

        private void datatypesNoneToolStripMenuItem_Click( object sender, EventArgs e )
        {
            bool selectedDatatypesChanged;
            bool nativeDatatypesChanged;
            bool classesChanged;
            bool structuresChanged;
            bool unknownDatatypesChanged;
            if ( SetSelectedDatatypes( SelectedDatatypes.None, out selectedDatatypesChanged,
                out nativeDatatypesChanged, out classesChanged, out structuresChanged, out unknownDatatypesChanged ) )
            {
                if ( selectedDatatypesChanged )
                {
                    OnSelectedDatatypesChanged();
                }

                if ( nativeDatatypesChanged )
                {
                    OnShowNativeDatatypesChanged();
                }

                if ( classesChanged )
                {
                    OnShowClassDatatypesChanged();
                }

                if ( structuresChanged )
                {
                    OnShowStructureDatatypesChanged();
                }

                if ( unknownDatatypesChanged )
                {
                    OnShowUnknownDatatypesChanged();
                }
            }
        }

        private void datatypesNativeTypesToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.ShowNativeDatatypes != this.datatypesNativeTypesToolStripMenuItem.Checked )
            {
                this.ShowNativeDatatypes = this.datatypesNativeTypesToolStripMenuItem.Checked;
                OnShowNativeDatatypesChanged();
            }
        }

        private void datatypesClassesToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.ShowClassDatatypes != this.datatypesClassesToolStripMenuItem.Checked )
            {
                this.ShowClassDatatypes = this.datatypesClassesToolStripMenuItem.Checked;
                OnShowClassDatatypesChanged();
            }
        }

        private void datatypesStructuresToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.ShowStructureDatatypes != this.datatypesClassesToolStripMenuItem.Checked )
            {
                this.ShowStructureDatatypes = this.datatypesClassesToolStripMenuItem.Checked;
                OnShowStructureDatatypesChanged();
            }
        }

        private void datatypesUnknownTypesToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.ShowUnknownDatatypes != this.datatypesUnknownTypesToolStripMenuItem.Checked )
            {
                this.ShowUnknownDatatypes = this.datatypesUnknownTypesToolStripMenuItem.Checked;
                OnShowUnknownDatatypesChanged();
            }
        }

        private void datatypesSelectToolStripMenuItem_Click( object sender, EventArgs e )
        {
            Dictionary<uint, bool> datatypeVisibility = m_datatypeVisibility;
            bool showUnknownDatatypes = m_showUnknownDatatypes;

            if ( ItemVisibilityForm.ShowIt( m_parentControl, "Select Data Types",
                ref datatypeVisibility, Allocation.DataTypeDictionary, ref showUnknownDatatypes, Allocation.Unknown ) )
            {
                m_datatypeMutex.WaitOne();

                bool selectedDatatypesChanged = false;
                foreach ( KeyValuePair<uint, bool> pair in datatypeVisibility )
                {
                    if ( pair.Value != m_datatypeVisibility[pair.Key] )
                    {
                        selectedDatatypesChanged = true;
                        break;
                    }
                }

                if ( selectedDatatypesChanged )
                {
                    m_datatypeVisibility = datatypeVisibility;
                }

                DetermineVisibleDatatypes();

                // set our datatype Types
                bool selectNativeTypes = true;
                bool selectClassTypes = true;
                bool selectStructTypes = true;
                if ( m_selectedDatatypes == SelectedDatatypes.Selected )
                {
                    foreach ( KeyValuePair<uint, bool> pair in m_datatypeVisibility )
                    {
                        if ( !pair.Value )
                        {
                            string datatypeName;
                            if ( Allocation.DataTypeDictionary.TryGetValue( pair.Key, out datatypeName ) )
                            {
                                if ( selectClassTypes )
                                {
                                    if ( datatypeName.StartsWith( "class" ) )
                                    {
                                        selectClassTypes = false;
                                    }
                                }
                                else if ( selectStructTypes )
                                {
                                    if ( datatypeName.StartsWith( "struct" ) )
                                    {
                                        selectStructTypes = false;
                                    }
                                }
                                else if ( selectNativeTypes )
                                {
                                    selectNativeTypes = false;
                                }
                                else
                                {
                                    break;
                                }
                            }
                        }
                    }
                }
                else
                {
                    bool vis = m_selectedDatatypes == SelectedDatatypes.All;
                    selectNativeTypes = vis;
                    selectClassTypes = vis;
                    selectStructTypes = vis;
                }

                m_datatypeMutex.ReleaseMutex();

                this.RefreshNeeded = true;

                if ( selectedDatatypesChanged )
                {
                    OnSelectedDatatypesChanged();
                }

                if ( m_showNativeDatatypes != selectNativeTypes )
                {
                    m_showNativeDatatypes = selectNativeTypes;
                    OnShowNativeDatatypesChanged();
                }

                if ( m_showClassDatatypes != selectClassTypes )
                {
                    m_showClassDatatypes = selectClassTypes;
                    OnShowClassDatatypesChanged();
                }

                if ( m_showStructDatatypes != selectStructTypes )
                {
                    m_showStructDatatypes = selectStructTypes;
                    OnShowStructureDatatypesChanged();
                }

                if ( m_showUnknownDatatypes != showUnknownDatatypes )
                {
                    m_showUnknownDatatypes = showUnknownDatatypes;
                    OnShowUnknownDatatypesChanged();
                }
            }
        }
        #endregion

        private void showEmptyItemsToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.ShowEmptyItems != this.showEmptyItemsToolStripMenuItem.Checked )
            {
                this.ShowEmptyItems = this.showEmptyItemsToolStripMenuItem.Checked;
                OnShowEmptyItemsChanged();
            }
        }
        #endregion

        private void refreshToolStripButton_Click( object sender, EventArgs e )
        {
            FillData();
        }
        #endregion

        #region TrackerComponent Event Handlers
        private void trackerComponent_ReportAdded( object sender, ReportChangedEventArgs e )
        {
            this.leftReportToolStripComboBox.Items.Add( e.Report );
            this.rightReportToolStripComboBox.Items.Add( e.Report );
        }

        private void trackerComponent_ReportRemoved( object sender, ReportChangedEventArgs e )
        {
            this.leftReportToolStripComboBox.Items.Remove( e.Report );
            this.rightReportToolStripComboBox.Items.Remove( e.Report );
        }

        private void trackerComponent_StackAdded( object sender, StackChangedEventArgs e )
        {
            m_stackMutex.WaitOne();

            if ( !m_stackVisibility.ContainsKey( e.HashKey ) )
            {
                m_stackVisibility.Add( e.HashKey, m_selectedStacks == SelectedStacks.All );
            }

            m_stackMutex.ReleaseMutex();
        }

        private void trackerComponent_DatatypeAdded( object sender, DatatypeAddedEventArgs e )
        {
            m_datatypeMutex.WaitOne();

            if ( !m_datatypeVisibility.ContainsKey( e.HashKey ) )
            {
                m_datatypeVisibility.Add( e.HashKey, m_selectedDatatypes == SelectedDatatypes.All );
            }

            m_datatypeMutex.ReleaseMutex();
        }
        #endregion

        #region BackgroundWorker Event Handlers
        private void backgroundWorker_DoWork( object sender, DoWorkEventArgs e )
        {
            switch ( m_showBy )
            {
                case DisplayType.DataTypes:
                    e.Result = GatherDatatypeData();
                    break;
                case DisplayType.MemoryType:
                    e.Result = GatherMemoryTypeData();
                    break;
                case DisplayType.StackNames:
                    e.Result = GatherStackData();
                    break;
            }
        }

        private void backgroundWorker_ProgressChanged( object sender, ProgressChangedEventArgs e )
        {
            OnUpdateStep( e.ProgressPercentage, m_reportGenerated );
        }

        private void backgroundWorker_RunWorkerCompleted( object sender, RunWorkerCompletedEventArgs e )
        {
            // enable the toolstrip
            this.toolStrip1.Enabled = true;

            int result = (int)e.Result;
            if ( result == 1 )
            {
                bool showAllocations = m_leftReport.TrackedDeallocations ? m_showAllocations : true;

                // setup column headers
                SetColumnHeaders( false, showAllocations );

                // add rows
                foreach ( ListViewItem row in m_rows )
                {
                    this.listView1.Items.Add( row );
                }

                // set Show By:
                StringBuilder showByText = new StringBuilder( "Show By:  " );
                switch ( m_showBy )
                {
                    case DisplayType.DataTypes:
                        showByText.Append( "Datatype " );
                        break;
                    case DisplayType.MemoryType:
                        showByText.Append( "Memory Type " );
                        break;
                    case DisplayType.StackNames:
                        showByText.Append( "Stack Name " );
                        break;
                    default:
                        break;
                }
                
                if ( showAllocations )
                {
                    showByText.Append( "(allocations)" );
                }
                else
                {
                    showByText.Append( "(deallocations)" );
                }

                this.showByToolStripStatusLabel.Text = showByText.ToString();

                // set Selected Stacks:
                StringBuilder selectedStacksText = new StringBuilder( "Stacks:  " );
                selectedStacksText.Append( m_selectedStacks.ToString() );
                this.selectedStacksToolStripStatusLabel.Text = selectedStacksText.ToString();

                // set Selected Datatypes:
                StringBuilder selectedDataTypesText = new StringBuilder( "Datatypes:  " );
                selectedDataTypesText.Append( m_selectedDatatypes.ToString() );
                this.selectedDatatypesToolStripStatusLabel.Text = selectedDataTypesText.ToString();

                // set Selected Size:
                this.selectedSizeToolStripStatusLabel.Text = "Selected Size:  0 bytes";

                // disable the Refresh Button
                this.RefreshNeeded = false;
            }

            m_rows = null;

            OnUpdateComplete( m_reportGenerated ? m_leftReport : null, result == 1 );

            m_reportGenerated = false;
        }
        #endregion
    }    
}