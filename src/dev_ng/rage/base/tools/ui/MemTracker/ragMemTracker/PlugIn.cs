using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using ragWidgets;
using RSG.Base.Logging;

namespace ragMemTracker
{
	/// <summary>
	/// Summary description for Class1.
	/// </summary>
	public class PlugIn : ragWidgetPage.WidgetPagePlugIn
    {
        #region Overrides
        public override string Name 
		{
			get
			{
				return "Memory Tracker";
			}
		}

		public override void InitPlugIn(PlugInHostData hostData)
		{
            m_hostData = hostData;

			base.InitPlugIn(hostData);
			m_Instance = this;

            // find the top-level form
            Control parent = hostData.MenuToolStrip.Parent;
            while ( (parent != null) && !(parent is Form) )
            {
                parent = parent.Parent;
            }
            m_parentControl = parent;

            // register our create del with the map
            base.m_pageNameToCreateDelMap[MemTracker.MyName] = new ragWidgetPage.WidgetPage.CreatePageDel( MemTracker.CreatePage );

            MemTracker.Initialize();

            if ( !String.IsNullOrEmpty( hostData.MainRageApplication.Args ) )
            {
                string[] splitArgs = hostData.MainRageApplication.Args.Split( new char[] { '-' }, StringSplitOptions.RemoveEmptyEntries );
                foreach ( string arg in splitArgs )
                {
                    string lowerArg = arg.Trim().ToLower();
                    if ( !lowerArg.StartsWith( "!" ) && lowerArg.Contains( "trackerrag" ) )
                    {
                        bool connect = true;

                        string[] splitArg = lowerArg.Split( new char[] { ':' }, StringSplitOptions.RemoveEmptyEntries );
                        if ( (splitArg.Length > 1) && (splitArg[1] == "trackerrag") )
                        {
                            switch ( hostData.Platform )
                            {
                                case "Win32":
                                    connect = splitArg[0] == "pc";
                                    break;
                                case "Xbox360":
                                    connect = splitArg[0] == "xe";
                                    break;
                                case "PlayStation3":
                                    connect = splitArg[0] == "p3";
                                    break;
                                default:
                                    connect = false;
                                    break;
                            }
                        }
                        else if ( splitArg.Length > 0 )
                        {
                            connect = splitArg[0] == "trackerrag";
                        }
                        else
                        {
                            connect = false;
                        }

                        if ( connect )
                        {
                            MemTracker.Connect();
                            break;
                        }
                    }
                }
            }
		}

		public override void RemovePlugIn()
		{
            base.RemovePlugIn();
			MemTracker.Shutdown();
            m_Instance = null;
        }

        public override bool CheckPluginStatus()
        {
            return MemTracker.IsConnected;
        }

        public override void ActivatePlugInMenuItem( object sender, EventArgs args )
        {
            // Create the window if it doesn't exist:
            if ( MemTracker.Instance == null || MemTracker.Instance.DockControl == null)
            {
                base.CreateAndOpenView(base.m_pageNameToCreateDelMap[MemTracker.MyName], LogFactory.ApplicationLog);
            }
            // if it's not already open, open it in its last known position 
            else if ( !MemTracker.Instance.DockControl.IsOpen )
            {
                MemTracker.Instance.DockControl.Open( TD.SandDock.WindowOpenMethod.OnScreenActivate );
            }
        }
        #endregion

        #region Static Properties
        public static PlugIn Instance
		{
			get 
			{
                return m_Instance;
			}
        }

        #endregion

        #region Static Variables
        protected static PlugIn m_Instance;
        #endregion

        #region Variables
        private Control m_parentControl;
        private bool m_rememberDeallocations = false;
        private PlugInHostData m_hostData;
        #endregion

        #region Properties
        public Control ParentControl
        {
            get
            {
                return m_parentControl;
            }
        }

        public bool RememberDeallocations
        {
            get
            {
                return m_rememberDeallocations;
            }
        }

        public PlugInHostData HostData
        {
            get
            {
                return m_hostData;
            }
        }
        #endregion

        #region Public Static Functions
        public static void CenterLocation( Control parent, Form form )
        {
            Rectangle rect = parent.Bounds;
            form.StartPosition = FormStartPosition.Manual;

            int x = (rect.Width / 2) + rect.Left - (form.Width / 2);
            int y = (rect.Height / 2) + rect.Top - (form.Height / 2);

            form.Location = new Point( x, y );
        }
        #endregion
    }
}
