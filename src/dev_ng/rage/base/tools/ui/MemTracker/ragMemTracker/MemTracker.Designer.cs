namespace ragMemTracker
{
    partial class MemTracker
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && (components != null) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager( typeof( MemTracker ) );
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.OverviewTagPage = new System.Windows.Forms.TabPage();
            this.overviewListControl = new MemTrackerControls.OverviewListControl();
            this.AllocationsTabPage = new System.Windows.Forms.TabPage();
            this.allocationsTreeListControl = new MemTrackerControls.AllocationsTreeListControl();
            this.memoryTabPage = new System.Windows.Forms.TabPage();
            this.memoryLayoutControl = new MemTrackerControls.MemoryLayoutControl();
            this.BottomToolStripPanel = new System.Windows.Forms.ToolStripPanel();
            this.TopToolStripPanel = new System.Windows.Forms.ToolStripPanel();
            this.RightToolStripPanel = new System.Windows.Forms.ToolStripPanel();
            this.LeftToolStripPanel = new System.Windows.Forms.ToolStripPanel();
            this.ContentPanel = new System.Windows.Forms.ToolStripContentPanel();
            this.topToolStrip = new System.Windows.Forms.ToolStrip();
            this.connectionStatusToolStripLabel = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator18 = new System.Windows.Forms.ToolStripSeparator();
            this.refreshProgressToolStripLabel = new System.Windows.Forms.ToolStripLabel();
            this.refreshToolStripProgressBar = new System.Windows.Forms.ToolStripProgressBar();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.generateAllToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator17 = new System.Windows.Forms.ToolStripSeparator();
            this.optionsToolStripDropDownButton = new System.Windows.Forms.ToolStripDropDownButton();
            this.saveReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showErrorsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.trackDeallocationsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator15 = new System.Windows.Forms.ToolStripSeparator();
            this.refreshAllToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.tabControl1.SuspendLayout();
            this.OverviewTagPage.SuspendLayout();
            this.AllocationsTabPage.SuspendLayout();
            this.memoryTabPage.SuspendLayout();
            this.topToolStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add( this.OverviewTagPage );
            this.tabControl1.Controls.Add( this.AllocationsTabPage );
            this.tabControl1.Controls.Add( this.memoryTabPage );
            this.tabControl1.Location = new System.Drawing.Point( 0, 38 );
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size( 1014, 572 );
            this.tabControl1.TabIndex = 0;
            // 
            // OverviewTagPage
            // 
            this.OverviewTagPage.Controls.Add( this.overviewListControl );
            this.OverviewTagPage.Location = new System.Drawing.Point( 4, 22 );
            this.OverviewTagPage.Name = "OverviewTagPage";
            this.OverviewTagPage.Padding = new System.Windows.Forms.Padding( 3 );
            this.OverviewTagPage.Size = new System.Drawing.Size( 1006, 546 );
            this.OverviewTagPage.TabIndex = 0;
            this.OverviewTagPage.Text = "Overview";
            this.OverviewTagPage.UseVisualStyleBackColor = true;
            // 
            // overviewListControl
            // 
            this.overviewListControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.overviewListControl.LeftReport = null;
            this.overviewListControl.Location = new System.Drawing.Point( 3, 3 );
            this.overviewListControl.Name = "overviewListControl";
            this.overviewListControl.ParentControl = null;
            this.overviewListControl.RightReport = null;
            this.overviewListControl.ShowAllocations = true;
            this.overviewListControl.ShowBy = MemTrackerControls.OverviewListControl.DisplayType.MemoryType;
            this.overviewListControl.ShowClassDatatypes = true;
            this.overviewListControl.ShowEmptyItems = false;
            this.overviewListControl.ShowNativeDatatypes = true;
            this.overviewListControl.ShowSizingGrip = false;
            this.overviewListControl.ShowStructureDatatypes = true;
            this.overviewListControl.ShowUnassignedStack = true;
            this.overviewListControl.ShowUnknownDatatypes = true;
            this.overviewListControl.Size = new System.Drawing.Size( 1000, 540 );
            this.overviewListControl.TabIndex = 0;
            this.overviewListControl.ThreadingEnabled = false;
            this.overviewListControl.Tracker = null;
            this.overviewListControl.VisibleDatatypeHashKeys = ((System.Collections.Generic.List<uint>)(resources.GetObject( "overviewListControl.VisibleDatatypeHashKeys" )));
            this.overviewListControl.VisibleDatatypes = MemTrackerControls.OverviewListControl.SelectedDatatypes.All;
            this.overviewListControl.VisibleDatatypesTypeNames = ((System.Collections.Generic.List<string>)(resources.GetObject( "overviewListControl.VisibleDatatypesTypeNames" )));
            this.overviewListControl.VisibleStackHashKeys = ((System.Collections.Generic.List<uint>)(resources.GetObject( "overviewListControl.VisibleStackHashKeys" )));
            this.overviewListControl.VisibleStacks = MemTrackerControls.OverviewListControl.SelectedStacks.All;
            this.overviewListControl.VisibleStackTypeNames = ((System.Collections.Generic.List<string>)(resources.GetObject( "overviewListControl.VisibleStackTypeNames" )));
            this.overviewListControl.UpdateStep += new MemTrackerControls.UpdateStepEventHandler( this.MemTrackerControls_UpdateStep );
            this.overviewListControl.UpdateComplete += new MemTrackerControls.UpdateCompleteEventHandler( this.MemTrackerControls_UpdateComplete );
            this.overviewListControl.UpdateBegin += new MemTrackerControls.UpdateBeginEventHandler( this.MemTrackerControls_UpdateBegin );
            // 
            // AllocationsTabPage
            // 
            this.AllocationsTabPage.Controls.Add( this.allocationsTreeListControl );
            this.AllocationsTabPage.Location = new System.Drawing.Point( 4, 22 );
            this.AllocationsTabPage.Name = "AllocationsTabPage";
            this.AllocationsTabPage.Padding = new System.Windows.Forms.Padding( 3 );
            this.AllocationsTabPage.Size = new System.Drawing.Size( 1006, 546 );
            this.AllocationsTabPage.TabIndex = 1;
            this.AllocationsTabPage.Text = "Allocations";
            this.AllocationsTabPage.UseVisualStyleBackColor = true;
            // 
            // allocationsTreeListControl
            // 
            this.allocationsTreeListControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.allocationsTreeListControl.GroupBy = MemTrackerControls.AllocationsTreeListControl.DisplayType.Order;
            this.allocationsTreeListControl.Location = new System.Drawing.Point( 3, 3 );
            this.allocationsTreeListControl.MaxSize = 999999999;
            this.allocationsTreeListControl.MinSize = 0;
            this.allocationsTreeListControl.MultiSelect = false;
            this.allocationsTreeListControl.Name = "allocationsTreeListControl";
            this.allocationsTreeListControl.ParentControl = null;
            this.allocationsTreeListControl.Report = null;
            this.allocationsTreeListControl.ShowAllocations = true;
            this.allocationsTreeListControl.ShowClassDatatypes = true;
            this.allocationsTreeListControl.ShowConsoleSpecificMemory = true;
            this.allocationsTreeListControl.ShowGamePhysicalMemory = true;
            this.allocationsTreeListControl.ShowGameVirtualMemory = true;
            this.allocationsTreeListControl.ShowNativeDatatypes = true;
            this.allocationsTreeListControl.ShowResourcePhysicalMemory = true;
            this.allocationsTreeListControl.ShowResourceVirtualMemory = true;
            this.allocationsTreeListControl.ShowSizingGrip = true;
            this.allocationsTreeListControl.ShowStructureDatatypes = true;
            this.allocationsTreeListControl.ShowUnassignedStack = true;
            this.allocationsTreeListControl.ShowUndeterminedMemory = true;
            this.allocationsTreeListControl.ShowUnknownDatatypes = true;
            this.allocationsTreeListControl.Size = new System.Drawing.Size( 1000, 540 );
            this.allocationsTreeListControl.TabIndex = 0;
            this.allocationsTreeListControl.ThreadingEnabled = false;
            this.allocationsTreeListControl.Tracker = null;
            this.allocationsTreeListControl.VisibleDatatypeHashKeys = ((System.Collections.Generic.List<uint>)(resources.GetObject( "allocationsTreeListControl.VisibleDatatypeHashKeys" )));
            this.allocationsTreeListControl.VisibleDatatypes = MemTrackerControls.AllocationsTreeListControl.SelectedDatatypes.All;
            this.allocationsTreeListControl.VisibleDatatypesTypeNames = ((System.Collections.Generic.List<string>)(resources.GetObject( "allocationsTreeListControl.VisibleDatatypesTypeNames" )));
            this.allocationsTreeListControl.VisibleMemoryTypeNames = ((System.Collections.Generic.List<string>)(resources.GetObject( "allocationsTreeListControl.VisibleMemoryTypeNames" )));
            this.allocationsTreeListControl.VisibleMemoryTypes = MemTrackerControls.AllocationsTreeListControl.SelectedMemoryTypes.All;
            this.allocationsTreeListControl.VisibleStackHashKeys = ((System.Collections.Generic.List<uint>)(resources.GetObject( "allocationsTreeListControl.VisibleStackHashKeys" )));
            this.allocationsTreeListControl.VisibleStacks = MemTrackerControls.AllocationsTreeListControl.SelectedStacks.All;
            this.allocationsTreeListControl.VisibleStackTypeNames = ((System.Collections.Generic.List<string>)(resources.GetObject( "allocationsTreeListControl.VisibleStackTypeNames" )));
            this.allocationsTreeListControl.OutOfMemoryError += new MemTrackerControls.ErrorEventHandler( this.allocationsTreeListControl_OutOfMemoryError );
            this.allocationsTreeListControl.UpdateStep += new MemTrackerControls.UpdateStepEventHandler( this.MemTrackerControls_UpdateStep );
            this.allocationsTreeListControl.UpdateComplete += new MemTrackerControls.UpdateCompleteEventHandler( this.MemTrackerControls_UpdateComplete );
            this.allocationsTreeListControl.UpdateBegin += new MemTrackerControls.UpdateBeginEventHandler( this.MemTrackerControls_UpdateBegin );
            // 
            // memoryTabPage
            // 
            this.memoryTabPage.Controls.Add( this.memoryLayoutControl );
            this.memoryTabPage.Location = new System.Drawing.Point( 4, 22 );
            this.memoryTabPage.Name = "memoryTabPage";
            this.memoryTabPage.Padding = new System.Windows.Forms.Padding( 3 );
            this.memoryTabPage.Size = new System.Drawing.Size( 1006, 546 );
            this.memoryTabPage.TabIndex = 2;
            this.memoryTabPage.Text = "Memory Layout";
            this.memoryTabPage.UseVisualStyleBackColor = true;
            // 
            // memoryLayoutControl
            // 
            this.memoryLayoutControl.BytesPerPixel = MemTrackerControls.MemoryLayoutControl.Bytes.OneHundredTwentyEight;
            this.memoryLayoutControl.ColorizeBy = MemTrackerControls.MemoryLayoutControl.DisplayType.StackNames;
            this.memoryLayoutControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.memoryLayoutControl.Location = new System.Drawing.Point( 3, 3 );
            this.memoryLayoutControl.Name = "memoryLayoutControl";
            this.memoryLayoutControl.ParentControl = null;
            this.memoryLayoutControl.PixelWidth = MemTrackerControls.MemoryLayoutControl.Widths.TwoHundredFiftySix;
            this.memoryLayoutControl.Report = null;
            this.memoryLayoutControl.ShowSizingGrip = false;
            this.memoryLayoutControl.Size = new System.Drawing.Size( 1000, 540 );
            this.memoryLayoutControl.SizeScale = MemTrackerControls.MemoryLayoutControl.Sizes.Medium;
            this.memoryLayoutControl.TabIndex = 0;
            this.memoryLayoutControl.ThreadingEnabled = false;
            this.memoryLayoutControl.Tracker = null;
            this.memoryLayoutControl.UpdateStep += new MemTrackerControls.UpdateStepEventHandler( this.MemTrackerControls_UpdateStep );
            this.memoryLayoutControl.UpdateComplete += new MemTrackerControls.UpdateCompleteEventHandler( this.MemTrackerControls_UpdateComplete );
            this.memoryLayoutControl.UpdateBegin += new MemTrackerControls.UpdateBeginEventHandler( this.MemTrackerControls_UpdateBegin );
            // 
            // BottomToolStripPanel
            // 
            this.BottomToolStripPanel.Location = new System.Drawing.Point( 0, 0 );
            this.BottomToolStripPanel.Name = "BottomToolStripPanel";
            this.BottomToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.BottomToolStripPanel.RowMargin = new System.Windows.Forms.Padding( 3, 0, 0, 0 );
            this.BottomToolStripPanel.Size = new System.Drawing.Size( 0, 0 );
            // 
            // TopToolStripPanel
            // 
            this.TopToolStripPanel.Location = new System.Drawing.Point( 0, 0 );
            this.TopToolStripPanel.Name = "TopToolStripPanel";
            this.TopToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.TopToolStripPanel.RowMargin = new System.Windows.Forms.Padding( 3, 0, 0, 0 );
            this.TopToolStripPanel.Size = new System.Drawing.Size( 0, 0 );
            // 
            // RightToolStripPanel
            // 
            this.RightToolStripPanel.Location = new System.Drawing.Point( 0, 0 );
            this.RightToolStripPanel.Name = "RightToolStripPanel";
            this.RightToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.RightToolStripPanel.RowMargin = new System.Windows.Forms.Padding( 3, 0, 0, 0 );
            this.RightToolStripPanel.Size = new System.Drawing.Size( 0, 0 );
            // 
            // LeftToolStripPanel
            // 
            this.LeftToolStripPanel.Location = new System.Drawing.Point( 0, 0 );
            this.LeftToolStripPanel.Name = "LeftToolStripPanel";
            this.LeftToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.LeftToolStripPanel.RowMargin = new System.Windows.Forms.Padding( 3, 0, 0, 0 );
            this.LeftToolStripPanel.Size = new System.Drawing.Size( 0, 0 );
            // 
            // ContentPanel
            // 
            this.ContentPanel.Size = new System.Drawing.Size( 962, 410 );
            // 
            // topToolStrip
            // 
            this.topToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.topToolStrip.Items.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.connectionStatusToolStripLabel,
            this.toolStripSeparator18,
            this.refreshProgressToolStripLabel,
            this.refreshToolStripProgressBar,
            this.toolStripSeparator7,
            this.generateAllToolStripButton,
            this.toolStripSeparator17,
            this.optionsToolStripDropDownButton,
            this.toolStripSeparator15,
            this.refreshAllToolStripButton} );
            this.topToolStrip.Location = new System.Drawing.Point( 0, 0 );
            this.topToolStrip.Name = "topToolStrip";
            this.topToolStrip.Size = new System.Drawing.Size( 1017, 25 );
            this.topToolStrip.Stretch = true;
            this.topToolStrip.TabIndex = 2;
            this.topToolStrip.Text = "toolStrip1";
            // 
            // connectionStatusToolStripLabel
            // 
            this.connectionStatusToolStripLabel.Name = "connectionStatusToolStripLabel";
            this.connectionStatusToolStripLabel.Size = new System.Drawing.Size( 73, 22 );
            this.connectionStatusToolStripLabel.Text = "Connecting...";
            // 
            // toolStripSeparator18
            // 
            this.toolStripSeparator18.Name = "toolStripSeparator18";
            this.toolStripSeparator18.Size = new System.Drawing.Size( 6, 25 );
            // 
            // refreshProgressToolStripLabel
            // 
            this.refreshProgressToolStripLabel.Name = "refreshProgressToolStripLabel";
            this.refreshProgressToolStripLabel.Size = new System.Drawing.Size( 53, 22 );
            this.refreshProgressToolStripLabel.Text = "Progress:";
            this.refreshProgressToolStripLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // refreshToolStripProgressBar
            // 
            this.refreshToolStripProgressBar.Name = "refreshToolStripProgressBar";
            this.refreshToolStripProgressBar.Size = new System.Drawing.Size( 100, 22 );
            this.refreshToolStripProgressBar.Step = 1;
            this.refreshToolStripProgressBar.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size( 6, 25 );
            // 
            // generateAllToolStripButton
            // 
            this.generateAllToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.generateAllToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject( "generateAllToolStripButton.Image" )));
            this.generateAllToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.generateAllToolStripButton.Name = "generateAllToolStripButton";
            this.generateAllToolStripButton.Size = new System.Drawing.Size( 114, 22 );
            this.generateAllToolStripButton.Text = "Generate Report (All)";
            this.generateAllToolStripButton.ToolTipText = "Generate a new report and refresh all tabs";
            this.generateAllToolStripButton.Click += new System.EventHandler( this.generateAllToolStripButton_Click );
            // 
            // toolStripSeparator17
            // 
            this.toolStripSeparator17.Name = "toolStripSeparator17";
            this.toolStripSeparator17.Size = new System.Drawing.Size( 6, 25 );
            // 
            // optionsToolStripDropDownButton
            // 
            this.optionsToolStripDropDownButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.optionsToolStripDropDownButton.DropDownItems.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.saveReportToolStripMenuItem,
            this.showErrorsToolStripMenuItem,
            this.trackDeallocationsToolStripMenuItem} );
            this.optionsToolStripDropDownButton.Image = ((System.Drawing.Image)(resources.GetObject( "optionsToolStripDropDownButton.Image" )));
            this.optionsToolStripDropDownButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.optionsToolStripDropDownButton.Name = "optionsToolStripDropDownButton";
            this.optionsToolStripDropDownButton.Size = new System.Drawing.Size( 82, 22 );
            this.optionsToolStripDropDownButton.Text = "Main Options";
            this.optionsToolStripDropDownButton.DropDownOpening += new System.EventHandler( this.mainOptionsToolStripDropDownButton_DropDownOpening );
            // 
            // saveReportToolStripMenuItem
            // 
            this.saveReportToolStripMenuItem.Name = "saveReportToolStripMenuItem";
            this.saveReportToolStripMenuItem.Size = new System.Drawing.Size( 177, 22 );
            this.saveReportToolStripMenuItem.Text = "&Save Report";
            // 
            // showErrorsToolStripMenuItem
            // 
            this.showErrorsToolStripMenuItem.Name = "showErrorsToolStripMenuItem";
            this.showErrorsToolStripMenuItem.Size = new System.Drawing.Size( 177, 22 );
            this.showErrorsToolStripMenuItem.Text = "Show &Errors";
            this.showErrorsToolStripMenuItem.Click += new System.EventHandler( this.showErrorsToolStripMenuItem_Click );
            // 
            // trackDeallocationsToolStripMenuItem
            // 
            this.trackDeallocationsToolStripMenuItem.CheckOnClick = true;
            this.trackDeallocationsToolStripMenuItem.Name = "trackDeallocationsToolStripMenuItem";
            this.trackDeallocationsToolStripMenuItem.Size = new System.Drawing.Size( 177, 22 );
            this.trackDeallocationsToolStripMenuItem.Text = "&Track Deallocations";
            this.trackDeallocationsToolStripMenuItem.Click += new System.EventHandler( this.trackDeallocationsToolStripMenuItem_Click );
            // 
            // toolStripSeparator15
            // 
            this.toolStripSeparator15.Name = "toolStripSeparator15";
            this.toolStripSeparator15.Size = new System.Drawing.Size( 6, 25 );
            // 
            // refreshAllToolStripButton
            // 
            this.refreshAllToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.refreshAllToolStripButton.Enabled = false;
            this.refreshAllToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject( "refreshAllToolStripButton.Image" )));
            this.refreshAllToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.refreshAllToolStripButton.Name = "refreshAllToolStripButton";
            this.refreshAllToolStripButton.Size = new System.Drawing.Size( 63, 22 );
            this.refreshAllToolStripButton.Text = "Refresh All";
            this.refreshAllToolStripButton.ToolTipText = "Refresh all tabs with the current tab\'s selected report";
            this.refreshAllToolStripButton.Click += new System.EventHandler( this.refreshAllToolStripButton_Click );
            // 
            // saveFileDialog
            // 
            this.saveFileDialog.DefaultExt = "txt";
            this.saveFileDialog.Filter = "Text files (*.txt)|*.txt";
            this.saveFileDialog.Title = "Save Tracker File";
            // 
            // MemTracker
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add( this.topToolStrip );
            this.Controls.Add( this.tabControl1 );
            this.Name = "MemTracker";
            this.Size = new System.Drawing.Size( 1017, 610 );
            this.Load += new System.EventHandler( this.MemTracker_Load );
            this.tabControl1.ResumeLayout( false );
            this.OverviewTagPage.ResumeLayout( false );
            this.AllocationsTabPage.ResumeLayout( false );
            this.memoryTabPage.ResumeLayout( false );
            this.topToolStrip.ResumeLayout( false );
            this.topToolStrip.PerformLayout();
            this.ResumeLayout( false );
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage OverviewTagPage;
        private System.Windows.Forms.TabPage AllocationsTabPage;
        private System.Windows.Forms.TabPage memoryTabPage;
        private System.Windows.Forms.ToolStripPanel BottomToolStripPanel;
        private System.Windows.Forms.ToolStripPanel TopToolStripPanel;
        private System.Windows.Forms.ToolStripPanel RightToolStripPanel;
        private System.Windows.Forms.ToolStripPanel LeftToolStripPanel;
        private System.Windows.Forms.ToolStripContentPanel ContentPanel;
        private System.Windows.Forms.ToolStrip topToolStrip;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripButton refreshAllToolStripButton;
        private System.Windows.Forms.ToolStripLabel connectionStatusToolStripLabel;
        private System.Windows.Forms.ToolStripProgressBar refreshToolStripProgressBar;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator15;
        private System.Windows.Forms.ToolStripButton generateAllToolStripButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator17;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator18;
        private System.Windows.Forms.ToolStripLabel refreshProgressToolStripLabel;
        private System.Windows.Forms.ToolStripDropDownButton optionsToolStripDropDownButton;
        private System.Windows.Forms.ToolStripMenuItem saveReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem showErrorsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem trackDeallocationsToolStripMenuItem;
        private MemTrackerControls.OverviewListControl overviewListControl;
        private MemTrackerControls.AllocationsTreeListControl allocationsTreeListControl;
        private MemTrackerControls.MemoryLayoutControl memoryLayoutControl;
    }
}
