using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Data;
using System.Globalization;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Windows.Forms;

using MemTrackerControls;
using ragCompression;
using ragCore;
using ragUi;
using ragWidgetPage;
using ragWidgets;
using RSG.Base.Logging;

namespace ragMemTracker
{
    public partial class MemTracker : ragWidgetPage.WidgetPage
    {
        protected MemTracker()
        {
            InitializeComponent();
            InitializeAdditionalComponents();
        }

        public MemTracker(String appName, IBankManager bankMgr, TD.SandDock.DockControl dockControl, ILog log)
            : base( appName, bankMgr, null, dockControl, log )
        {
            InitializeComponent();
            InitializeAdditionalComponents();

            // does it kill us to not do this?
            //int handle = this.Handle.ToInt32();  // *sigh* this insures that the windows handle is actually created!
            //Debug.Assert( handle > 0 );
        }        

        #region Delegates
        private delegate void FlushStringDel( string theString );
        private delegate void RebuildDel( ControlList.ColumnData[] columnData, ControlList.RowData[] rowData, int width );
        #endregion

        #region Static Properties
        public static MemTracker Instance;
        public static bool IsConnected
        {
            get
            {
                return sm_IsConnected;
            }
        }
        public static string MyName = "Memory Tracker";
        #endregion

        #region Constants
        private const int c_numPipes = 20;
        const int c_TrackerPortOffset = (int)ragCore.PipeID.EnumSocketOffset.SOCKET_OFFSET_USER5 + 2;
        
        const int c_numStackNameColumnHeaders = 8;
        const int c_numDataTypeColumnHeaders = 5;
        const int c_numMemoryTypeColumnHeaders = 8;

        const int c_memoryGraphTextLeft = 2;
        const int c_memoryGraphTop = 8;
        const int c_memoryGraphLeft = 70;

        const int c_colorKeyTop = 16;
        const int c_colorKeyLeft = 8;
        const int c_colorKeyTextLeft = 24;
        const int c_colorKeyColorBoxLength = 12;
        const int c_colorKeyLineSpacing = 20;
        const int c_colorKeyColumnSpacing = 0;
        const int c_colorKeyMaxLengthMultiplier = 6;

        private List<Color> c_colors = GenerateColors();

        private static List<Color> GenerateColors()
        {
            List<List<Color>> lists = new List<List<Color>>();
            
            lists.Add( GenerateColors( 255, 0, 0 ) );
            lists.Add( GenerateColors( 255, 255, 0 ) );
            lists.Add( GenerateColors( 0, 255, 0 ) );
            lists.Add( GenerateColors( 0, 255, 255 ) );
            lists.Add( GenerateColors( 0, 0, 255 ) );
            lists.Add( GenerateColors( 255, 0, 255 ) );

            List<Color> finalList = new List<Color>();
            finalList.Add( Color.Gray );

            // put them in "rainbow" order
            int count = lists[0].Count;
            for ( int i = 0; i < count; ++i )
            {
                for ( int j = 0; j < lists.Count; ++j )
                {
                    finalList.Add( lists[j][i] );
                }
            }

            finalList.Add( Color.DarkGray );

            return finalList;
        }

        private static List<Color> GenerateColors( int origRed, int origGreen, int origBlue )
        {
            List<Color> colors = new List<Color>();

            int addRed = origRed == 0 ? 1 : 0;
            int addGreen = origGreen == 0 ? 1 : 0;
            int addBlue = origBlue == 0 ? 1 : 0;

            int subtractRed = origRed == 0 ? 0 : 1;
            int subtractGreen = origGreen == 0 ? 0 : 1;
            int subtractBlue = origBlue == 0 ? 0 : 1;

            int[] subtractValues = new int[]{ 0, 100, 50, 150, 25, 75, 125, 175 };
            int[] addValues = new int[] { 0, 100, 50, 125, 25, 75 }; 

            for ( int i = 0; i < 6; ++i )
            {
                int red, green, blue;
                red = origRed + (addRed * addValues[i]);
                green = origGreen + (addGreen * addValues[i]);
                blue = origBlue + (addBlue * addValues[i]);

                for ( int j = 0; j < 8; ++j )
                {
                    int r, g, b;
                    r = red - (subtractRed * subtractValues[j]);
                    g = green - (subtractGreen * subtractValues[j]);
                    b = blue - (subtractBlue * subtractValues[j]);

                    if ( subtractRed == 1 )
                    {
                        if ( addGreen == 1 )
                        {
                            if ( g > r - 30 )
                            {
                                break;
                            }
                        }
                        else if ( addBlue == 1 )
                        {
                            if ( b > r - 30 )
                            {
                                break;
                            }
                        }
                    }
                    else if ( subtractGreen == 1 )
                    {
                        if ( r > g - 30 )
                        {
                            break;
                        }
                    }
                    else if ( subtractBlue == 1 )
                    {
                        if ( r > b - 30 )
                        {
                            break;
                        }
                    }

                    colors.Add( Color.FromArgb( r, g, b ) );
                }
            }

            return colors;
        }
        #endregion

        #region Static Variables
        private static TrackerPipe[] sm_pipes = new TrackerPipe[c_numPipes];
        private static Thread sm_ConnectThread;
        private static Thread sm_ReadThread;
                
        private static bool sm_IsConnected = false;
        private static uint sm_messageCount = 0;

        private static bool sm_initialRememberDeallocationsSet = false;
        private static bool sm_initialRememberDeallocations = false;

        private static TrackerComponent sm_trackerComponent = new TrackerComponent();
        #endregion

        #region Variables
        private int m_generateReportSteps = -1;
        #endregion

        #region Init and Shutdown
        public static ragWidgetPage.WidgetPage CreatePage(String appName, IBankManager bankMgr, IKeyUpProcessor keyProcessor, TD.SandDock.DockControl dockControl, ILog log)
        {
            if ( Instance == null )
            {
                Instance = new MemTracker( appName, bankMgr, dockControl, log );

                return Instance;
            }
            else
            {
                if (Instance.DockControl == null)
                {
                    Instance.DockControl = dockControl;
                }

                return Instance;
            }
        }

        public static void Initialize()
        {
            sm_trackerComponent.ParentControl = PlugIn.Instance.ParentControl;

            for ( int i = 0; i < sm_pipes.Length; ++i )
            {
                sm_pipes[i] = new TrackerPipe( new TrackerPipe.HandlePacketDelegate( HandlePacket ) );
            }
        }

        public static void Shutdown()
        {
            if ( sm_ConnectThread != null )
            {
                sm_ConnectThread.Abort();
            }

            if ( sm_ReadThread != null )
            {
                sm_ReadThread.Abort();
            }

            foreach ( TrackerPipe pipe in sm_pipes )
            {
                pipe.Close();
            }

            if ( Instance != null )
            {
                Instance.ClearView();
            }
        }
        #endregion

        #region Overrides
        public override string GetMyName()
        {
            return MyName;
        }

        public override string GetTabText()
        {
            return MyName;
        }

        public override bool InitiallyFloating
        {
            get
            {
                return true;
            }
        }

        public override bool IsSingleInstance
        {
            get
            {
                return true;
            }
        }
        #endregion

        #region Event Handlers
        private void MemTracker_Load( object sender, EventArgs e )
        {
            this.connectionStatusToolStripLabel.Text = "Connected to application (port " + c_TrackerPortOffset + "-" + (c_TrackerPortOffset + c_numPipes - 1) + ")";

            this.saveReportToolStripMenuItem.DropDownItems.Clear();

            ReportItem[] reports = sm_trackerComponent.Reports;
            foreach ( ReportItem report in reports )
            {
                trackerComponent_ReportAdded( sm_trackerComponent, new ReportChangedEventArgs( report ) );
            }
        }

        #region ToolStrip Event Handlers
        private void generateAllToolStripButton_Click( object sender, EventArgs e )
        {
            ReportItem report = sm_trackerComponent.AddReportAction( 0, string.Empty, 
                sm_trackerComponent.AvailableMemory, sm_trackerComponent.TrackDeallocations );

            // kick off the update for the current tab.  passing in the report will subsequently cause the others to be updated as well.
            switch ( this.tabControl1.SelectedIndex )
            {
                case 0:
                    this.overviewListControl.FillData( report );
                    break;
                case 1:
                    this.allocationsTreeListControl.FillData( report );
                    break;
                case 2:
                    this.memoryLayoutControl.FillData( report );
                    break;
            }
        }

        private void mainOptionsToolStripDropDownButton_DropDownOpening( object sender, EventArgs e )
        {
            this.trackDeallocationsToolStripMenuItem.Checked = sm_trackerComponent.TrackDeallocations;
        }

        #region Main Options Drop Down Menu
        private void showErrorsToolStripMenuItem_Click( object sender, EventArgs e )
        {
            ErrorsView eView = ErrorsView.Create( sm_trackerComponent );
            PlugIn.CenterLocation( PlugIn.Instance.ParentControl, eView );
            eView.ShowInTaskbar = true;

            eView.Show( PlugIn.Instance.ParentControl );
        }

        private void trackDeallocationsToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( sm_trackerComponent.TrackDeallocations != this.trackDeallocationsToolStripMenuItem.Checked )
            {
                // Add a report item to maintain consistency
                sm_trackerComponent.AddReportAction( 0, string.Empty, sm_trackerComponent.AvailableMemory, sm_trackerComponent.TrackDeallocations );
                
                sm_trackerComponent.TrackDeallocations = this.trackDeallocationsToolStripMenuItem.Checked;
            }
        }
        #endregion

        private void refreshAllToolStripButton_Click( object sender, EventArgs e )
        {
            ReportItem report = null;
            switch ( this.tabControl1.SelectedIndex )
            {
                case 0:
                    report = this.overviewListControl.LeftReport;
                    break;
                case 1:
                    report = this.allocationsTreeListControl.Report;
                    break;
                case 2:
                    report = this.memoryLayoutControl.Report;
                    break;
            }

            ReportItem[] reports = sm_trackerComponent.Reports;
            if ( (report == null) && (reports.Length > 0) )
            {
                report = reports[reports.Length - 1];
            }

            if ( report != null )
            {
                switch ( this.tabControl1.SelectedIndex )
                {
                    case 0:
                        this.overviewListControl.FillData( report );
                        break;
                    case 1:
                        this.allocationsTreeListControl.FillData( report );
                        break;
                    case 2:
                        this.memoryLayoutControl.FillData( report );
                        break;
                }
            }
        }
        #endregion

        #endregion

        #region Connection and Network Message Processing
        public static void Connect()
        {
            sm_ConnectThread = new Thread( new ThreadStart( ConnectProc ) );
            sm_ConnectThread.Name = "Memory Tracker Connect Thread";
            sm_ConnectThread.Start();
        }

        private static void ConnectProc()
        {
            try
            {
                int connectCount = 0;
                for ( int i = 0; i < sm_pipes.Length; ++i )
                {
                    PipeID pipeID = PlugIn.Instance.HostData.MainRageApplication.GetRagPipeSocket( c_TrackerPortOffset + i );
                    if ( sm_pipes[i].Create( pipeID , true ) )
                    {
                        ++connectCount;
                    }
                }

                if ( connectCount == sm_pipes.Length )
                {
                    sm_IsConnected = true;
                    StartReadThread();
                }
            }
            catch ( ThreadAbortException )
            {
                return;
            }
        }        

        private static void StartReadThread()
        {
            sm_ReadThread = new Thread( new ThreadStart( ReadProc ) );
            sm_ReadThread.Name = "Memory Tracker Read Thread";
            sm_ReadThread.Priority = ThreadPriority.Highest;
            sm_ReadThread.Start();
        }

        private static void ReadProc()
        {
            Dictionary<uint, CompressionData> dataDictionary = new Dictionary<uint, CompressionData>();

            while ( true )
            {
                try
                {
                    bool error = false;

                    // put each CompressionData in a dictionary according to the Count packet that should be at the front of each data buffer
                    foreach ( TrackerPipe pipe in sm_pipes )
                    {
                        if ( pipe.ReadPipe( dataDictionary ) == -1 )
                        {
                            error = true;
                            break;
                        }
                    }

                    if ( !error && (dataDictionary.Count > 0) )
                    {
                        // find the CompressionData corresponding to the next message count we're looking for
                        CompressionData cData;
                        while ( dataDictionary.TryGetValue( sm_messageCount, out cData ) )
                        {
                            if ( sm_pipes[0].HandlePackets( cData ) == -1 )
                            {
                                error = true;
                                break;
                            }

                            dataDictionary.Remove( sm_messageCount );

                            ++sm_messageCount;
                        }
                    }
                }
                catch ( ThreadAbortException )
                {
                    break;
                }
            }
        }

        private static void HandlePacket( RemotePacket p )
        {
            try
            {
                switch ( (TrackerPipe.Commands)p.Command )
                {
                    case TrackerPipe.Commands.Tally:
                        {
                            p.Begin();
                            uint threadId = p.Read_u32();
                            uint decimalAddress = p.Read_u32();
                            uint size = p.Read_u32();
                            int memoryType = p.Read_s32();
                            p.End();

                            sm_trackerComponent.TallyAllocationAction( threadId, decimalAddress, memoryType, size );
                        }
                        break;
                    case TrackerPipe.Commands.Untally:
                        {
                            p.Begin();
                            uint threadId = p.Read_u32();
                            uint decimalAddress = p.Read_u32();
                            uint size = p.Read_u32();
                            p.End();

                            sm_trackerComponent.UntallyAllocationAction( threadId, decimalAddress, size );
                        }
                        break;
                    case TrackerPipe.Commands.Info:
                        {
                            p.Begin();
                            uint threadId = p.Read_u32();
                            uint decimalAddress = p.Read_u32();
                            uint size = p.Read_u32();
                            uint dataTypeHash = p.Read_u32();
                            uint fileNameHash = p.Read_u32();
                            int line = p.Read_s32();
                            p.End();

                            sm_trackerComponent.AddInfoAction( threadId, decimalAddress, size, dataTypeHash, fileNameHash, line );
                        }
                        break;
                    case TrackerPipe.Commands.Count:
                        {
                            p.Begin();
                            uint threadId = p.Read_u32();
                            uint count = p.Read_u32();
                            p.End();

                            // don't do anything
                        }
                        break;
                    case TrackerPipe.Commands.Push:
                        {
                            p.Begin();
                            uint threadId = p.Read_u32();
                            string stackName = p.Read_const_char();
                            p.End();

                            sm_trackerComponent.PushStackAction( threadId, stackName );
                        }
                        break;
                    case TrackerPipe.Commands.Pop:
                        {
                            p.Begin();
                            uint threadId = p.Read_u32();
                            p.End();

                            sm_trackerComponent.PopStackAction( threadId );
                        }
                        break;
                    case TrackerPipe.Commands.Report:
                        {
                            p.Begin();
                            uint threadId = p.Read_u32();
                            string reportName = p.Read_const_char();
                            uint[] available = new uint[(int)Allocation.MemoryTypeIndex.NumIndexes];
                            for ( int i = 0; i < available.Length; ++i )
                            {
                                available[i] = p.Read_u32();
                            }
                            p.End();

                            sm_trackerComponent.AddReportAction( threadId, reportName, available, sm_trackerComponent.TrackDeallocations );
                        }
                        break;
                    case TrackerPipe.Commands.Filename:
                        {
                            p.Begin();
                            uint threadId = p.Read_u32();
                            string fileName = p.Read_const_char();
                            uint fileNameHash = p.Read_u32();
                            p.End();

                            sm_trackerComponent.AddFilename( fileName, fileNameHash );
                        }
                        break;
                    case TrackerPipe.Commands.Typename:
                        {
                            p.Begin();
                            uint threadId = p.Read_u32();
                            byte parts = p.Read_u8();
                            string dataType = string.Empty;
                            for ( int i = 0; i < parts; ++i )
                            {
                                dataType += p.Read_const_char();
                            }
                            uint dataTypeHash = p.Read_u32();
                            p.End();

                            sm_trackerComponent.AddDataType( dataType, dataTypeHash );
                        }
                        break;
                    case TrackerPipe.Commands.Memory:
                        {
                            p.Begin();
                            uint threadId = p.Read_u32();

                            uint[] availableMemory = new uint[sm_trackerComponent.AvailableMemory.Length];
                            for ( int i = 0; i < sm_trackerComponent.AvailableMemory.Length; ++i )
                            {
                                availableMemory[i] = p.Read_u32();
                            }

                            sm_trackerComponent.SetAvailableMemory( threadId, availableMemory, false );

                            p.End();
                        }
                        break;
                    case TrackerPipe.Commands.TrackDeallocations:
                        {
                            p.Begin();
                            uint threadId = p.Read_u32();
                            sm_trackerComponent.TrackDeallocations = p.Read_bool();

                            if ( !sm_initialRememberDeallocationsSet )
                            {
                                // remember this for saving a report
                                sm_initialRememberDeallocations = sm_trackerComponent.TrackDeallocations;
                                sm_initialRememberDeallocationsSet = true;
                            }

                            p.End();
                        }
                        break;
                    case TrackerPipe.Commands.Platform:
                        {
                            p.Begin();
                            sm_trackerComponent.Platform = p.Read_const_char();

                            uint[] availableMemory = new uint[sm_trackerComponent.AvailableMemory.Length];
                            for ( int i = 0; i < sm_trackerComponent.AvailableMemory.Length; ++i )
                            {
                                availableMemory[i] = p.Read_u32();
                            }

                            sm_trackerComponent.SetAvailableMemory( 0, availableMemory, false );
                            
                            p.End();
                        }
                        break;
                }
            }
            catch
            {
            }
        }     
        #endregion

        #region Private Functions
        private void InitializeAdditionalComponents()
        {
            sm_trackerComponent.ReportAdded += new ReportChangedEventHandler( trackerComponent_ReportAdded );
            sm_trackerComponent.DuplicateAllocation += new DuplicateAllocationEventHandler( trackerComponent_DuplicateAllocation );

            this.overviewListControl.Tracker = sm_trackerComponent;
            this.overviewListControl.ParentControl = PlugIn.Instance.ParentControl;

            this.allocationsTreeListControl.Tracker = sm_trackerComponent;
            this.allocationsTreeListControl.ParentControl = PlugIn.Instance.ParentControl;

            this.memoryLayoutControl.Tracker = sm_trackerComponent;
            this.memoryLayoutControl.ParentControl = PlugIn.Instance.ParentControl;
        }

        private void SaveReport_Click( object sender, EventArgs e )
        {
            ToolStripMenuItem menuItem = sender as ToolStripMenuItem;
            if ( menuItem != null )
            {
                ReportItem report = menuItem.Tag as ReportItem;
                if ( report != null )
                {
                    string filename = report.Name;
                    char[] invalidChars = Path.GetInvalidFileNameChars();
                    foreach ( char c in invalidChars )
                    {
                        filename = filename.Replace( c, '_' );
                    }
                    this.saveFileDialog.FileName = filename;

                    DialogResult result = this.saveFileDialog.ShowDialog( PlugIn.Instance.ParentControl );
                    if ( result == DialogResult.OK )
                    {
                        sm_trackerComponent.SaveReport( this.saveFileDialog.FileName, report );
                    }
                }
            }
        }
        #endregion

        #region Save/Load Widget Page Data
        public override void SaveWidgetPageData(Dictionary<string,string> data)
        {
 	        base.SaveWidgetPageData(data);

            // overview tab
            data["OverviewShowBy"] = this.overviewListControl.ShowBy.ToString();
            data["OverviewShowEmptyItems"] = this.overviewListControl.ShowEmptyItems.ToString();
            data["OverviewShowStacks"] = SerializeVisibility( this.overviewListControl.VisibleStacks.ToString(), 
                this.overviewListControl.VisibleStackTypeNames, this.overviewListControl.VisibleStackHashKeys );
            data["OverviewShowDatatypes"] = SerializeVisibility( this.overviewListControl.VisibleDatatypes.ToString(),
                this.overviewListControl.VisibleDatatypesTypeNames, this.overviewListControl.VisibleDatatypeHashKeys );
            data["OverviewShowAllocations"] = this.overviewListControl.ShowAllocations.ToString();

            // allocations tab
            data["AllocationsGroupBy"] = this.allocationsTreeListControl.GroupBy.ToString();
            data["AllocationsMinSize"] = this.allocationsTreeListControl.MinSize.ToString();
            data["AllocationsMaxSize"] = this.allocationsTreeListControl.MaxSize.ToString();
            data["AllocationsShowStacks"] = SerializeVisibility( this.allocationsTreeListControl.VisibleStacks.ToString(),
                this.allocationsTreeListControl.VisibleStackTypeNames, this.allocationsTreeListControl.VisibleStackHashKeys );
            data["AllocationsShowDatatypes"] = SerializeVisibility( this.allocationsTreeListControl.VisibleDatatypes.ToString(),
                this.allocationsTreeListControl.VisibleDatatypesTypeNames, this.allocationsTreeListControl.VisibleDatatypeHashKeys );
            data["AllocationsShowMemoryTypes"] = SerializeVisibility( "Selected",
                this.allocationsTreeListControl.VisibleMemoryTypeNames, new List<uint>() );
            data["AllocationsMultiSelect"] = this.allocationsTreeListControl.MultiSelect.ToString();
            data["AllocationsShowAllocations"] = this.allocationsTreeListControl.ShowAllocations.ToString();

            // memory layout tab
            data["MemoryLayoutBytes"] = this.memoryLayoutControl.BytesPerPixel.ToString();
            data["MemoryLayoutWidth"] = this.memoryLayoutControl.PixelWidth.ToString();
            data["MemoryLayoutColorize"] = this.memoryLayoutControl.ColorizeBy.ToString();
            data["MemoryLayoutSizeScale"] = this.memoryLayoutControl.SizeScale.ToString();
        }

        public override void LoadWidgetPageData(Dictionary<string,string> data)
        {
 	        base.LoadWidgetPageData(data);

            string value;

            // overview tab
            if ( data.TryGetValue( "OverviewShowBy", out value ) )
            {
                this.overviewListControl.ShowBy = (OverviewListControl.DisplayType)Enum.Parse( typeof( OverviewListControl.DisplayType), value );
            }

            if ( data.TryGetValue( "OverviewShowEmptyItems", out value ) )
            {
                this.overviewListControl.ShowEmptyItems = bool.Parse( value );
            }

            if ( data.TryGetValue( "OverviewShowStacks", out value ) )
            {
                string prefix = null;
                List<string> stackTypes = new List<string>();
                List<uint> stackKeys = new List<uint>();
                if ( DeserializeVisibility( value, ref prefix, stackTypes, stackKeys ) )
                {
                    this.overviewListControl.VisibleStacks
                       = (OverviewListControl.SelectedStacks)Enum.Parse( typeof( OverviewListControl.SelectedStacks ), prefix );

                    if ( this.overviewListControl.VisibleStacks == OverviewListControl.SelectedStacks.Selected )
                    {
                        this.overviewListControl.VisibleStackHashKeys = stackKeys;
                        this.overviewListControl.VisibleStackTypeNames = stackTypes;
                    }
                }
            }

            if ( data.TryGetValue( "OverviewShowDatatypes", out value ) )
            {
                string prefix = null;
                List<string> datatypeTypes = new List<string>();
                List<uint> datatypeKeys = new List<uint>();
                if ( DeserializeVisibility( value, ref prefix, datatypeTypes, datatypeKeys ) )
                {
                    this.overviewListControl.VisibleDatatypes
                       = (OverviewListControl.SelectedDatatypes)Enum.Parse( typeof( OverviewListControl.SelectedDatatypes ), prefix );

                    if ( this.overviewListControl.VisibleDatatypes == OverviewListControl.SelectedDatatypes.Selected )
                    {
                        this.overviewListControl.VisibleDatatypeHashKeys = datatypeKeys;
                        this.overviewListControl.VisibleDatatypesTypeNames = datatypeTypes;
                    }
                }
            }

            if ( data.TryGetValue( "OverviewShowAllocations", out value ) )
            {
                this.overviewListControl.ShowAllocations = bool.Parse( value );
            }

            // allocations tab
            if ( data.TryGetValue( "AllocationsGroupBy", out value ) )
            {
                this.allocationsTreeListControl.GroupBy 
                    = (AllocationsTreeListControl.DisplayType)Enum.Parse( typeof( AllocationsTreeListControl.DisplayType ), value );
            }

            if ( data.TryGetValue( "AllocationsMinSize", out value ) )
            {
                this.allocationsTreeListControl.MinSize = int.Parse( value );
            }

            if ( data.TryGetValue( "AllocationsMaxSize", out value ) )
            {
                this.allocationsTreeListControl.MaxSize = int.Parse( value );
            }

            if ( data.TryGetValue( "AllocationsShowStacks", out value ) )
            {
                string prefix = null;
                List<string> stackTypes = new List<string>();
                List<uint> stackKeys = new List<uint>();
                if ( DeserializeVisibility( value, ref prefix, stackTypes, stackKeys ) )
                {
                    this.allocationsTreeListControl.VisibleStacks
                       = (AllocationsTreeListControl.SelectedStacks)Enum.Parse( typeof( AllocationsTreeListControl.SelectedStacks ), prefix );

                    if ( this.allocationsTreeListControl.VisibleStacks == AllocationsTreeListControl.SelectedStacks.Selected )
                    {
                        this.allocationsTreeListControl.VisibleStackHashKeys = stackKeys;
                        this.allocationsTreeListControl.VisibleStackTypeNames = stackTypes;
                    }
                }
            }

            if ( data.TryGetValue( "AllocationsShowDatatypes", out value ) )
            {
                string prefix = null;
                List<string> datatypeTypes = new List<string>();
                List<uint> datatypeKeys = new List<uint>();
                if ( DeserializeVisibility( value, ref prefix, datatypeTypes, datatypeKeys ) )
                {
                    this.allocationsTreeListControl.VisibleDatatypes
                       = (AllocationsTreeListControl.SelectedDatatypes)Enum.Parse( typeof( AllocationsTreeListControl.SelectedDatatypes ), prefix );

                    if ( this.allocationsTreeListControl.VisibleDatatypes == AllocationsTreeListControl.SelectedDatatypes.Selected )
                    {
                        this.allocationsTreeListControl.VisibleDatatypeHashKeys = datatypeKeys;
                        this.allocationsTreeListControl.VisibleDatatypesTypeNames = datatypeTypes;
                    }
                }
            }

            if ( data.TryGetValue( "AllocationsShowMemoryTypes", out value ) )
            {
                string prefix = null;
                List<string> memoryTypes = new List<string>();
                List<uint> memorytypeKeys = new List<uint>();
                if ( DeserializeVisibility( value, ref prefix, memoryTypes, memorytypeKeys ) )
                {
                    this.allocationsTreeListControl.VisibleMemoryTypes
                       = (AllocationsTreeListControl.SelectedMemoryTypes)Enum.Parse( typeof( AllocationsTreeListControl.SelectedMemoryTypes ), prefix );

                    if ( this.allocationsTreeListControl.VisibleMemoryTypes == AllocationsTreeListControl.SelectedMemoryTypes.Selected )
                    {
                        this.allocationsTreeListControl.VisibleDatatypesTypeNames = memoryTypes;
                    }
                }
            }

            if ( data.TryGetValue( "AllocationsMultiSelect", out value ) )
            {
                this.allocationsTreeListControl.MultiSelect = bool.Parse( value );
            }

            if ( data.TryGetValue( "AllocationsShowAllocations", out value ) )
            {
                this.allocationsTreeListControl.ShowAllocations = bool.Parse( value );
            }

            // memory layout tab
            if ( data.TryGetValue( "MemoryLayoutBytes", out value ) )
            {
                this.memoryLayoutControl.BytesPerPixel = (MemoryLayoutControl.Bytes)Enum.Parse( typeof( MemoryLayoutControl.Bytes ), value );
            }

            if ( data.TryGetValue( "MemoryLayoutWidth", out value ) )
            {
                this.memoryLayoutControl.PixelWidth = (MemoryLayoutControl.Widths)Enum.Parse( typeof( MemoryLayoutControl.Widths ), value );
            }

            if ( data.TryGetValue( "MemoryLayoutColorize", out value ) )
            {
                this.memoryLayoutControl.ColorizeBy = (MemoryLayoutControl.DisplayType)Enum.Parse( typeof( MemoryLayoutControl.DisplayType ), value );
            }

            if ( data.TryGetValue( "MemoryLayoutSizeScale", out value ) )
            {
                this.memoryLayoutControl.SizeScale = (MemoryLayoutControl.Sizes)Enum.Parse( typeof( MemoryLayoutControl.Sizes ), value );
            }
        }
        
        private string SerializeVisibility( string prefix, List<string> visibleTypeNames, List<uint> visibleKeys )
        {
            StringBuilder s = new StringBuilder( prefix );
            if ( prefix == "Selected" )
            {
                foreach ( string visibleTypeName in visibleTypeNames )
                {
                    s.Append( ',' );
                    s.Append( visibleTypeName );
                }

                s.Append( ';' );

                foreach ( uint visibleKey in visibleKeys )
                {
                    s.Append( ',' );
                    s.Append( visibleKey );
                }
            }
            else
            {
                s.Append( ';' );
            }

            return s.ToString();
        }

        private bool DeserializeVisibility( string s, ref string prefix, List<string> visibleTypeNames, List<uint> visibleKeys )
        {
            int indexOf = s.IndexOf( ';' );
            if ( indexOf == -1 )
            {
                return false;
            }

            string[] headers = (s.Substring( 0, indexOf )).Split( new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries );
            string[] items = (s.Substring( indexOf + 1 )).Split( new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries );

            if ( headers.Length == 0 )
            {
                return false;
            }

            prefix = headers[0];
            if ( prefix == "Selected" )
            {               
                for ( int i = 1; i < headers.Length; ++i )
                {
                    visibleTypeNames.Add( headers[i] );
                }

                foreach ( string item in items )
                {
                    visibleKeys.Add( uint.Parse( item ) );
                }
            }

            return true;
        }
        #endregion

        #region Tracker Component Event Handlers
        private void trackerComponent_ReportAdded( object sender, ReportChangedEventArgs e )
        {
            ToolStripMenuItem item = new ToolStripMenuItem( e.Report.Name );
            item.DisplayStyle = ToolStripItemDisplayStyle.Text;
            item.Tag = e.Report;
            item.Click += new EventHandler( SaveReport_Click );

            this.saveReportToolStripMenuItem.DropDownItems.Add( item );
            this.refreshAllToolStripButton.Enabled = true;
        }

        private void trackerComponent_DuplicateAllocation( object sender, DuplicateAllocationEventArgs e )
        {
            DuplicateAllocationForm dupForm = DuplicateAllocationForm.Create( e.TheAllocation );
            PlugIn.CenterLocation( this.ParentForm, dupForm );

            e.ActionToTake = dupForm.ShowIt( this.ParentForm );
        }
        #endregion

        #region MemTrackerControls Event Handlers
        private void MemTrackerControls_UpdateBegin( object sender, UpdateBeginEventArgs e )
        {
            if ( m_generateReportSteps == -1 )
            {
                m_generateReportSteps = 0;

                this.topToolStrip.Enabled = false;
                
                this.refreshToolStripProgressBar.Value = 0;
                this.refreshToolStripProgressBar.Maximum = e.GeneratingReport ? (e.NumSteps * 3) : e.NumSteps;
            }
            else if ( e.GeneratingReport )
            {
                ++m_generateReportSteps;
            }
        }

        private void MemTrackerControls_UpdateStep( object sender, UpdateStepEventArgs e )
        {
            int targetSteps = (m_generateReportSteps * 100) + e.StepNumber;
            this.refreshToolStripProgressBar.Value = targetSteps;
        }

        private void MemTrackerControls_UpdateComplete( object sender, UpdateCompleteEventArgs e )
        {
            if ( e.Success && (e.ReportGenerated != null) && (m_generateReportSteps < 2) )
            {
                int nextTabIndex = this.tabControl1.SelectedIndex + m_generateReportSteps + 1;
                if ( nextTabIndex >= 3 )
                {
                    nextTabIndex = nextTabIndex - 3;
                }

                // kick off the update for the next tab.  passing in the report will subsequently cause the remaining to be updated as well.
                switch ( nextTabIndex )
                {
                    case 0:
                        this.overviewListControl.FillData( e.ReportGenerated );
                        break;
                    case 1:
                        this.allocationsTreeListControl.FillData( e.ReportGenerated );
                        break;
                    case 2:
                        this.memoryLayoutControl.FillData( e.ReportGenerated );
                        break;
                }

                return;
            }
            
            m_generateReportSteps = -1;

            this.topToolStrip.Enabled = true;
        }
        #endregion

        #region AllocationTreeListControl Event Handlers
        private void allocationsTreeListControl_OutOfMemoryError( object sender, MemTrackerControls.ErrorEventArgs e )
        {
            MessageBox.Show( PlugIn.Instance.ParentControl, e.Message, "Out of Memory",
                MessageBoxButtons.OK, MessageBoxIcon.Stop );
        }
        #endregion
    }
}
