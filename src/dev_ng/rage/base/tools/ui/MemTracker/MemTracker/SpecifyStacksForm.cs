using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace MemTracker
{
    public partial class SpecifyStacksForm : Form
    {
        protected SpecifyStacksForm()
        {
            InitializeComponent();
        }

        protected SpecifyStacksForm( List<string> specifiedStacks )
            : this()
        {            
            foreach ( string stack in specifiedStacks )
            {
                this.stacksRichTextBox.Text += stack;
                this.stacksRichTextBox.Text += "\n";
            }
        }

        public static bool ShowIt( Control parent, ref List<string> specifiedStacks )
        {
            SpecifyStacksForm form = new SpecifyStacksForm( specifiedStacks );
            Form1.CenterLocation( parent, form );

            DialogResult result = form.ShowDialog( parent );
            if ( result == DialogResult.OK )
            {
                specifiedStacks = new List<string>( form.SpecifiedStacks );
                return true;
            }

            return false;
        }

        #region Properties
        private string[] SpecifiedStacks
        {
            get
            {
                return this.stacksRichTextBox.Text.Split( new char[] { '\n' }, StringSplitOptions.RemoveEmptyEntries );
            }
        }
        #endregion
    }
}