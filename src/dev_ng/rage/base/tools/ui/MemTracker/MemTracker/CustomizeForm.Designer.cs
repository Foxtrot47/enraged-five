namespace MemTracker
{
    partial class CustomizeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && (components != null) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.overviewTabPage = new System.Windows.Forms.TabPage();
            this.overviewPropertyGrid = new System.Windows.Forms.PropertyGrid();
            this.allocationsTabPage = new System.Windows.Forms.TabPage();
            this.allocationsPropertyGrid = new System.Windows.Forms.PropertyGrid();
            this.memoryLayoutTabPage = new System.Windows.Forms.TabPage();
            this.memoryLayoutPropertyGrid = new System.Windows.Forms.PropertyGrid();
            this.okButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.overviewTabPage.SuspendLayout();
            this.allocationsTabPage.SuspendLayout();
            this.memoryLayoutTabPage.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add( this.overviewTabPage );
            this.tabControl1.Controls.Add( this.allocationsTabPage );
            this.tabControl1.Controls.Add( this.memoryLayoutTabPage );
            this.tabControl1.Location = new System.Drawing.Point( 12, 12 );
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size( 457, 305 );
            this.tabControl1.TabIndex = 0;
            // 
            // overviewTabPage
            // 
            this.overviewTabPage.Controls.Add( this.overviewPropertyGrid );
            this.overviewTabPage.Location = new System.Drawing.Point( 4, 22 );
            this.overviewTabPage.Name = "overviewTabPage";
            this.overviewTabPage.Padding = new System.Windows.Forms.Padding( 3 );
            this.overviewTabPage.Size = new System.Drawing.Size( 449, 279 );
            this.overviewTabPage.TabIndex = 0;
            this.overviewTabPage.Text = "Default Overview Settings";
            this.overviewTabPage.UseVisualStyleBackColor = true;
            // 
            // overviewPropertyGrid
            // 
            this.overviewPropertyGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.overviewPropertyGrid.Location = new System.Drawing.Point( 3, 3 );
            this.overviewPropertyGrid.Name = "overviewPropertyGrid";
            this.overviewPropertyGrid.Size = new System.Drawing.Size( 443, 273 );
            this.overviewPropertyGrid.TabIndex = 0;
            // 
            // allocationsTabPage
            // 
            this.allocationsTabPage.Controls.Add( this.allocationsPropertyGrid );
            this.allocationsTabPage.Location = new System.Drawing.Point( 4, 22 );
            this.allocationsTabPage.Name = "allocationsTabPage";
            this.allocationsTabPage.Padding = new System.Windows.Forms.Padding( 3 );
            this.allocationsTabPage.Size = new System.Drawing.Size( 449, 198 );
            this.allocationsTabPage.TabIndex = 1;
            this.allocationsTabPage.Text = "Default Allocations Settings";
            this.allocationsTabPage.UseVisualStyleBackColor = true;
            // 
            // allocationsPropertyGrid
            // 
            this.allocationsPropertyGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.allocationsPropertyGrid.Location = new System.Drawing.Point( 3, 3 );
            this.allocationsPropertyGrid.Name = "allocationsPropertyGrid";
            this.allocationsPropertyGrid.Size = new System.Drawing.Size( 443, 192 );
            this.allocationsPropertyGrid.TabIndex = 0;
            // 
            // memoryLayoutTabPage
            // 
            this.memoryLayoutTabPage.Controls.Add( this.memoryLayoutPropertyGrid );
            this.memoryLayoutTabPage.Location = new System.Drawing.Point( 4, 22 );
            this.memoryLayoutTabPage.Name = "memoryLayoutTabPage";
            this.memoryLayoutTabPage.Padding = new System.Windows.Forms.Padding( 3 );
            this.memoryLayoutTabPage.Size = new System.Drawing.Size( 449, 198 );
            this.memoryLayoutTabPage.TabIndex = 2;
            this.memoryLayoutTabPage.Text = "Default Memory Layout Settings";
            this.memoryLayoutTabPage.UseVisualStyleBackColor = true;
            // 
            // memoryLayoutPropertyGrid
            // 
            this.memoryLayoutPropertyGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.memoryLayoutPropertyGrid.Location = new System.Drawing.Point( 3, 3 );
            this.memoryLayoutPropertyGrid.Name = "memoryLayoutPropertyGrid";
            this.memoryLayoutPropertyGrid.Size = new System.Drawing.Size( 443, 192 );
            this.memoryLayoutPropertyGrid.TabIndex = 0;
            // 
            // okButton
            // 
            this.okButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.okButton.Location = new System.Drawing.Point( 313, 323 );
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size( 75, 23 );
            this.okButton.TabIndex = 1;
            this.okButton.Text = "OK";
            this.okButton.UseVisualStyleBackColor = true;
            // 
            // cancelButton
            // 
            this.cancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.Location = new System.Drawing.Point( 394, 323 );
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size( 75, 23 );
            this.cancelButton.TabIndex = 2;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            // 
            // CustomizeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size( 481, 358 );
            this.Controls.Add( this.cancelButton );
            this.Controls.Add( this.okButton );
            this.Controls.Add( this.tabControl1 );
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CustomizeForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
            this.Text = "Customize";
            this.tabControl1.ResumeLayout( false );
            this.overviewTabPage.ResumeLayout( false );
            this.allocationsTabPage.ResumeLayout( false );
            this.memoryLayoutTabPage.ResumeLayout( false );
            this.ResumeLayout( false );

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage overviewTabPage;
        private System.Windows.Forms.TabPage allocationsTabPage;
        private System.Windows.Forms.TabPage memoryLayoutTabPage;
        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.PropertyGrid overviewPropertyGrid;
        private System.Windows.Forms.PropertyGrid allocationsPropertyGrid;
        private System.Windows.Forms.PropertyGrid memoryLayoutPropertyGrid;

    }
}