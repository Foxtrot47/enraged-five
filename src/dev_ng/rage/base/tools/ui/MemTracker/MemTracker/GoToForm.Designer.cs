namespace MemTracker
{
    partial class GoToForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && (components != null) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.messageNumberRadioButton = new System.Windows.Forms.RadioButton();
            this.messageNumberNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.reportRadioButton = new System.Windows.Forms.RadioButton();
            this.reportGroupBox = new System.Windows.Forms.GroupBox();
            this.selectReportComboBox = new System.Windows.Forms.ComboBox();
            this.selectReportRadioButton = new System.Windows.Forms.RadioButton();
            this.nextReportRadioButton = new System.Windows.Forms.RadioButton();
            this.previousReportRadioButton = new System.Windows.Forms.RadioButton();
            this.stackRadioButton = new System.Windows.Forms.RadioButton();
            this.stackGroupBox = new System.Windows.Forms.GroupBox();
            this.specifyStacksButton = new System.Windows.Forms.Button();
            this.specifiedStacksCheckBox = new System.Windows.Forms.CheckBox();
            this.selectStacksButton = new System.Windows.Forms.Button();
            this.selectedStackCheckBox = new System.Windows.Forms.CheckBox();
            this.nextStackPopRadioButton = new System.Windows.Forms.RadioButton();
            this.previousStackPopRadioButton = new System.Windows.Forms.RadioButton();
            this.nextStackPushRadioButton = new System.Windows.Forms.RadioButton();
            this.previousStackPushRadioButton = new System.Windows.Forms.RadioButton();
            this.allocationRadioButton = new System.Windows.Forms.RadioButton();
            this.allocationGroupBox = new System.Windows.Forms.GroupBox();
            this.hexadecimalLabel = new System.Windows.Forms.Label();
            this.nextAllocationRadioButton = new System.Windows.Forms.RadioButton();
            this.previousDeallocationRadioButton = new System.Windows.Forms.RadioButton();
            this.allocationSizeToLabel = new System.Windows.Forms.Label();
            this.allocationSizeToNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.allocationSizeToCheckBox = new System.Windows.Forms.CheckBox();
            this.allocationSizeLabel = new System.Windows.Forms.Label();
            this.allocationSizeNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.allocationSizeCheckBox = new System.Windows.Forms.CheckBox();
            this.allocationAddressToTextBox = new System.Windows.Forms.TextBox();
            this.allocationAddressToCheckBox = new System.Windows.Forms.CheckBox();
            this.allocationAddressTextBox = new System.Windows.Forms.TextBox();
            this.allocationAddressCheckBox = new System.Windows.Forms.CheckBox();
            this.nextDeallocationRadioButton = new System.Windows.Forms.RadioButton();
            this.previousAllocationRadioButton = new System.Windows.Forms.RadioButton();
            this.okButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.messageNumberNumericUpDown)).BeginInit();
            this.reportGroupBox.SuspendLayout();
            this.stackGroupBox.SuspendLayout();
            this.allocationGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.allocationSizeToNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.allocationSizeNumericUpDown)).BeginInit();
            this.SuspendLayout();
            // 
            // messageNumberRadioButton
            // 
            this.messageNumberRadioButton.AutoSize = true;
            this.messageNumberRadioButton.Location = new System.Drawing.Point( 12, 12 );
            this.messageNumberRadioButton.Name = "messageNumberRadioButton";
            this.messageNumberRadioButton.Size = new System.Drawing.Size( 78, 17 );
            this.messageNumberRadioButton.TabIndex = 0;
            this.messageNumberRadioButton.TabStop = true;
            this.messageNumberRadioButton.Text = "Message #";
            this.messageNumberRadioButton.UseVisualStyleBackColor = true;
            this.messageNumberRadioButton.CheckedChanged += new System.EventHandler( this.messageNumberRadioButton_CheckedChanged );
            // 
            // messageNumberNumericUpDown
            // 
            this.messageNumberNumericUpDown.Location = new System.Drawing.Point( 96, 12 );
            this.messageNumberNumericUpDown.Maximum = new decimal( new int[] {
            999999999,
            0,
            0,
            0} );
            this.messageNumberNumericUpDown.Name = "messageNumberNumericUpDown";
            this.messageNumberNumericUpDown.Size = new System.Drawing.Size( 120, 20 );
            this.messageNumberNumericUpDown.TabIndex = 1;
            // 
            // reportRadioButton
            // 
            this.reportRadioButton.AutoSize = true;
            this.reportRadioButton.Location = new System.Drawing.Point( 12, 38 );
            this.reportRadioButton.Name = "reportRadioButton";
            this.reportRadioButton.Size = new System.Drawing.Size( 57, 17 );
            this.reportRadioButton.TabIndex = 2;
            this.reportRadioButton.TabStop = true;
            this.reportRadioButton.Text = "Report";
            this.reportRadioButton.UseVisualStyleBackColor = true;
            this.reportRadioButton.CheckedChanged += new System.EventHandler( this.reportRadioButton_CheckedChanged );
            // 
            // reportGroupBox
            // 
            this.reportGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.reportGroupBox.Controls.Add( this.selectReportComboBox );
            this.reportGroupBox.Controls.Add( this.selectReportRadioButton );
            this.reportGroupBox.Controls.Add( this.nextReportRadioButton );
            this.reportGroupBox.Controls.Add( this.previousReportRadioButton );
            this.reportGroupBox.Location = new System.Drawing.Point( 96, 38 );
            this.reportGroupBox.Name = "reportGroupBox";
            this.reportGroupBox.Size = new System.Drawing.Size( 444, 49 );
            this.reportGroupBox.TabIndex = 3;
            this.reportGroupBox.TabStop = false;
            // 
            // selectReportComboBox
            // 
            this.selectReportComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.selectReportComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.selectReportComboBox.FormattingEnabled = true;
            this.selectReportComboBox.Location = new System.Drawing.Point( 192, 18 );
            this.selectReportComboBox.Name = "selectReportComboBox";
            this.selectReportComboBox.Size = new System.Drawing.Size( 246, 21 );
            this.selectReportComboBox.TabIndex = 3;
            // 
            // selectReportRadioButton
            // 
            this.selectReportRadioButton.AutoSize = true;
            this.selectReportRadioButton.Location = new System.Drawing.Point( 131, 19 );
            this.selectReportRadioButton.Name = "selectReportRadioButton";
            this.selectReportRadioButton.Size = new System.Drawing.Size( 55, 17 );
            this.selectReportRadioButton.TabIndex = 2;
            this.selectReportRadioButton.TabStop = true;
            this.selectReportRadioButton.Text = "Select";
            this.selectReportRadioButton.UseVisualStyleBackColor = true;
            this.selectReportRadioButton.CheckedChanged += new System.EventHandler( this.selectReportRadioButton_CheckedChanged );
            // 
            // nextReportRadioButton
            // 
            this.nextReportRadioButton.AutoSize = true;
            this.nextReportRadioButton.Location = new System.Drawing.Point( 78, 19 );
            this.nextReportRadioButton.Name = "nextReportRadioButton";
            this.nextReportRadioButton.Size = new System.Drawing.Size( 47, 17 );
            this.nextReportRadioButton.TabIndex = 1;
            this.nextReportRadioButton.TabStop = true;
            this.nextReportRadioButton.Text = "Next";
            this.nextReportRadioButton.UseVisualStyleBackColor = true;
            // 
            // previousReportRadioButton
            // 
            this.previousReportRadioButton.AutoSize = true;
            this.previousReportRadioButton.Location = new System.Drawing.Point( 6, 19 );
            this.previousReportRadioButton.Name = "previousReportRadioButton";
            this.previousReportRadioButton.Size = new System.Drawing.Size( 66, 17 );
            this.previousReportRadioButton.TabIndex = 0;
            this.previousReportRadioButton.TabStop = true;
            this.previousReportRadioButton.Text = "Previous";
            this.previousReportRadioButton.UseVisualStyleBackColor = true;
            // 
            // stackRadioButton
            // 
            this.stackRadioButton.AutoSize = true;
            this.stackRadioButton.Location = new System.Drawing.Point( 12, 93 );
            this.stackRadioButton.Name = "stackRadioButton";
            this.stackRadioButton.Size = new System.Drawing.Size( 53, 17 );
            this.stackRadioButton.TabIndex = 4;
            this.stackRadioButton.TabStop = true;
            this.stackRadioButton.Text = "Stack";
            this.stackRadioButton.UseVisualStyleBackColor = true;
            this.stackRadioButton.CheckedChanged += new System.EventHandler( this.stackRadioButton_CheckedChanged );
            // 
            // stackGroupBox
            // 
            this.stackGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.stackGroupBox.Controls.Add( this.specifyStacksButton );
            this.stackGroupBox.Controls.Add( this.specifiedStacksCheckBox );
            this.stackGroupBox.Controls.Add( this.selectStacksButton );
            this.stackGroupBox.Controls.Add( this.selectedStackCheckBox );
            this.stackGroupBox.Controls.Add( this.nextStackPopRadioButton );
            this.stackGroupBox.Controls.Add( this.previousStackPopRadioButton );
            this.stackGroupBox.Controls.Add( this.nextStackPushRadioButton );
            this.stackGroupBox.Controls.Add( this.previousStackPushRadioButton );
            this.stackGroupBox.Location = new System.Drawing.Point( 96, 93 );
            this.stackGroupBox.Name = "stackGroupBox";
            this.stackGroupBox.Size = new System.Drawing.Size( 444, 74 );
            this.stackGroupBox.TabIndex = 5;
            this.stackGroupBox.TabStop = false;
            // 
            // specifyStacksButton
            // 
            this.specifyStacksButton.Location = new System.Drawing.Point( 329, 42 );
            this.specifyStacksButton.Name = "specifyStacksButton";
            this.specifyStacksButton.Size = new System.Drawing.Size( 75, 23 );
            this.specifyStacksButton.TabIndex = 7;
            this.specifyStacksButton.Text = "Specify...";
            this.specifyStacksButton.UseVisualStyleBackColor = true;
            this.specifyStacksButton.Click += new System.EventHandler( this.specifyStacksButton_Click );
            // 
            // specifiedStacksCheckBox
            // 
            this.specifiedStacksCheckBox.AutoSize = true;
            this.specifiedStacksCheckBox.Location = new System.Drawing.Point( 217, 46 );
            this.specifiedStacksCheckBox.Name = "specifiedStacksCheckBox";
            this.specifiedStacksCheckBox.Size = new System.Drawing.Size( 106, 17 );
            this.specifiedStacksCheckBox.TabIndex = 6;
            this.specifiedStacksCheckBox.Text = "Specified Stacks";
            this.specifiedStacksCheckBox.UseVisualStyleBackColor = true;
            this.specifiedStacksCheckBox.CheckedChanged += new System.EventHandler( this.specifiedStacksCheckBox_CheckedChanged );
            // 
            // selectStacksButton
            // 
            this.selectStacksButton.Location = new System.Drawing.Point( 121, 42 );
            this.selectStacksButton.Name = "selectStacksButton";
            this.selectStacksButton.Size = new System.Drawing.Size( 75, 23 );
            this.selectStacksButton.TabIndex = 5;
            this.selectStacksButton.Text = "Select...";
            this.selectStacksButton.UseVisualStyleBackColor = true;
            this.selectStacksButton.Click += new System.EventHandler( this.selectStacksButton_Click );
            // 
            // selectedStackCheckBox
            // 
            this.selectedStackCheckBox.AutoSize = true;
            this.selectedStackCheckBox.Location = new System.Drawing.Point( 6, 46 );
            this.selectedStackCheckBox.Name = "selectedStackCheckBox";
            this.selectedStackCheckBox.Size = new System.Drawing.Size( 104, 17 );
            this.selectedStackCheckBox.TabIndex = 4;
            this.selectedStackCheckBox.Text = "Selected Stacks";
            this.selectedStackCheckBox.UseVisualStyleBackColor = true;
            this.selectedStackCheckBox.CheckedChanged += new System.EventHandler( this.selectedStackCheckBox_CheckedChanged );
            // 
            // nextStackPopRadioButton
            // 
            this.nextStackPopRadioButton.AutoSize = true;
            this.nextStackPopRadioButton.Location = new System.Drawing.Point( 279, 19 );
            this.nextStackPopRadioButton.Name = "nextStackPopRadioButton";
            this.nextStackPopRadioButton.Size = new System.Drawing.Size( 69, 17 );
            this.nextStackPopRadioButton.TabIndex = 3;
            this.nextStackPopRadioButton.TabStop = true;
            this.nextStackPopRadioButton.Text = "Next Pop";
            this.nextStackPopRadioButton.UseVisualStyleBackColor = true;
            // 
            // previousStackPopRadioButton
            // 
            this.previousStackPopRadioButton.AutoSize = true;
            this.previousStackPopRadioButton.Location = new System.Drawing.Point( 185, 19 );
            this.previousStackPopRadioButton.Name = "previousStackPopRadioButton";
            this.previousStackPopRadioButton.Size = new System.Drawing.Size( 88, 17 );
            this.previousStackPopRadioButton.TabIndex = 2;
            this.previousStackPopRadioButton.TabStop = true;
            this.previousStackPopRadioButton.Text = "Previous Pop";
            this.previousStackPopRadioButton.UseVisualStyleBackColor = true;
            // 
            // nextStackPushRadioButton
            // 
            this.nextStackPushRadioButton.AutoSize = true;
            this.nextStackPushRadioButton.Location = new System.Drawing.Point( 105, 19 );
            this.nextStackPushRadioButton.Name = "nextStackPushRadioButton";
            this.nextStackPushRadioButton.Size = new System.Drawing.Size( 74, 17 );
            this.nextStackPushRadioButton.TabIndex = 1;
            this.nextStackPushRadioButton.TabStop = true;
            this.nextStackPushRadioButton.Text = "Next Push";
            this.nextStackPushRadioButton.UseVisualStyleBackColor = true;
            // 
            // previousStackPushRadioButton
            // 
            this.previousStackPushRadioButton.AutoSize = true;
            this.previousStackPushRadioButton.Location = new System.Drawing.Point( 6, 19 );
            this.previousStackPushRadioButton.Name = "previousStackPushRadioButton";
            this.previousStackPushRadioButton.Size = new System.Drawing.Size( 93, 17 );
            this.previousStackPushRadioButton.TabIndex = 0;
            this.previousStackPushRadioButton.TabStop = true;
            this.previousStackPushRadioButton.Text = "Previous Push";
            this.previousStackPushRadioButton.UseVisualStyleBackColor = true;
            // 
            // allocationRadioButton
            // 
            this.allocationRadioButton.AutoSize = true;
            this.allocationRadioButton.Location = new System.Drawing.Point( 12, 173 );
            this.allocationRadioButton.Name = "allocationRadioButton";
            this.allocationRadioButton.Size = new System.Drawing.Size( 71, 17 );
            this.allocationRadioButton.TabIndex = 6;
            this.allocationRadioButton.TabStop = true;
            this.allocationRadioButton.Text = "Allocation";
            this.allocationRadioButton.UseVisualStyleBackColor = true;
            this.allocationRadioButton.CheckedChanged += new System.EventHandler( this.allocationRadioButton_CheckedChanged );
            // 
            // allocationGroupBox
            // 
            this.allocationGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.allocationGroupBox.Controls.Add( this.hexadecimalLabel );
            this.allocationGroupBox.Controls.Add( this.nextAllocationRadioButton );
            this.allocationGroupBox.Controls.Add( this.previousDeallocationRadioButton );
            this.allocationGroupBox.Controls.Add( this.allocationSizeToLabel );
            this.allocationGroupBox.Controls.Add( this.allocationSizeToNumericUpDown );
            this.allocationGroupBox.Controls.Add( this.allocationSizeToCheckBox );
            this.allocationGroupBox.Controls.Add( this.allocationSizeLabel );
            this.allocationGroupBox.Controls.Add( this.allocationSizeNumericUpDown );
            this.allocationGroupBox.Controls.Add( this.allocationSizeCheckBox );
            this.allocationGroupBox.Controls.Add( this.allocationAddressToTextBox );
            this.allocationGroupBox.Controls.Add( this.allocationAddressToCheckBox );
            this.allocationGroupBox.Controls.Add( this.allocationAddressTextBox );
            this.allocationGroupBox.Controls.Add( this.allocationAddressCheckBox );
            this.allocationGroupBox.Controls.Add( this.nextDeallocationRadioButton );
            this.allocationGroupBox.Controls.Add( this.previousAllocationRadioButton );
            this.allocationGroupBox.Location = new System.Drawing.Point( 96, 173 );
            this.allocationGroupBox.Name = "allocationGroupBox";
            this.allocationGroupBox.Size = new System.Drawing.Size( 444, 102 );
            this.allocationGroupBox.TabIndex = 7;
            this.allocationGroupBox.TabStop = false;
            // 
            // hexadecimalLabel
            // 
            this.hexadecimalLabel.AutoSize = true;
            this.hexadecimalLabel.Location = new System.Drawing.Point( 340, 45 );
            this.hexadecimalLabel.Name = "hexadecimalLabel";
            this.hexadecimalLabel.Size = new System.Drawing.Size( 66, 13 );
            this.hexadecimalLabel.TabIndex = 8;
            this.hexadecimalLabel.Text = "hexadecimal";
            // 
            // nextAllocationRadioButton
            // 
            this.nextAllocationRadioButton.AutoSize = true;
            this.nextAllocationRadioButton.Location = new System.Drawing.Point( 103, 19 );
            this.nextAllocationRadioButton.Name = "nextAllocationRadioButton";
            this.nextAllocationRadioButton.Size = new System.Drawing.Size( 72, 17 );
            this.nextAllocationRadioButton.TabIndex = 1;
            this.nextAllocationRadioButton.TabStop = true;
            this.nextAllocationRadioButton.Text = "Next Tally";
            this.nextAllocationRadioButton.UseVisualStyleBackColor = true;
            // 
            // previousDeallocationRadioButton
            // 
            this.previousDeallocationRadioButton.AutoSize = true;
            this.previousDeallocationRadioButton.Location = new System.Drawing.Point( 181, 19 );
            this.previousDeallocationRadioButton.Name = "previousDeallocationRadioButton";
            this.previousDeallocationRadioButton.Size = new System.Drawing.Size( 101, 17 );
            this.previousDeallocationRadioButton.TabIndex = 2;
            this.previousDeallocationRadioButton.TabStop = true;
            this.previousDeallocationRadioButton.Text = "Previous Untally";
            this.previousDeallocationRadioButton.UseVisualStyleBackColor = true;
            // 
            // allocationSizeToLabel
            // 
            this.allocationSizeToLabel.AutoSize = true;
            this.allocationSizeToLabel.Location = new System.Drawing.Point( 407, 72 );
            this.allocationSizeToLabel.Name = "allocationSizeToLabel";
            this.allocationSizeToLabel.Size = new System.Drawing.Size( 32, 13 );
            this.allocationSizeToLabel.TabIndex = 14;
            this.allocationSizeToLabel.Text = "bytes";
            // 
            // allocationSizeToNumericUpDown
            // 
            this.allocationSizeToNumericUpDown.Increment = new decimal( new int[] {
            4,
            0,
            0,
            0} );
            this.allocationSizeToNumericUpDown.Location = new System.Drawing.Point( 281, 67 );
            this.allocationSizeToNumericUpDown.Maximum = new decimal( new int[] {
            999999999,
            0,
            0,
            0} );
            this.allocationSizeToNumericUpDown.Name = "allocationSizeToNumericUpDown";
            this.allocationSizeToNumericUpDown.Size = new System.Drawing.Size( 120, 20 );
            this.allocationSizeToNumericUpDown.TabIndex = 13;
            this.allocationSizeToNumericUpDown.ValueChanged += new System.EventHandler( this.allocationSizeToNumericUpDown_ValueChanged );
            // 
            // allocationSizeToCheckBox
            // 
            this.allocationSizeToCheckBox.AutoSize = true;
            this.allocationSizeToCheckBox.Location = new System.Drawing.Point( 240, 71 );
            this.allocationSizeToCheckBox.Name = "allocationSizeToCheckBox";
            this.allocationSizeToCheckBox.Size = new System.Drawing.Size( 35, 17 );
            this.allocationSizeToCheckBox.TabIndex = 12;
            this.allocationSizeToCheckBox.Text = "to";
            this.allocationSizeToCheckBox.UseVisualStyleBackColor = true;
            this.allocationSizeToCheckBox.CheckedChanged += new System.EventHandler( this.allocationSizeToCheckBox_CheckedChanged );
            // 
            // allocationSizeLabel
            // 
            this.allocationSizeLabel.AutoSize = true;
            this.allocationSizeLabel.Location = new System.Drawing.Point( 202, 72 );
            this.allocationSizeLabel.Name = "allocationSizeLabel";
            this.allocationSizeLabel.Size = new System.Drawing.Size( 32, 13 );
            this.allocationSizeLabel.TabIndex = 11;
            this.allocationSizeLabel.Text = "bytes";
            // 
            // allocationSizeNumericUpDown
            // 
            this.allocationSizeNumericUpDown.Increment = new decimal( new int[] {
            4,
            0,
            0,
            0} );
            this.allocationSizeNumericUpDown.Location = new System.Drawing.Point( 76, 67 );
            this.allocationSizeNumericUpDown.Maximum = new decimal( new int[] {
            999999999,
            0,
            0,
            0} );
            this.allocationSizeNumericUpDown.Name = "allocationSizeNumericUpDown";
            this.allocationSizeNumericUpDown.Size = new System.Drawing.Size( 120, 20 );
            this.allocationSizeNumericUpDown.TabIndex = 10;
            this.allocationSizeNumericUpDown.ValueChanged += new System.EventHandler( this.allocationSizeNumericUpDown_ValueChanged );
            // 
            // allocationSizeCheckBox
            // 
            this.allocationSizeCheckBox.AutoSize = true;
            this.allocationSizeCheckBox.Location = new System.Drawing.Point( 6, 68 );
            this.allocationSizeCheckBox.Name = "allocationSizeCheckBox";
            this.allocationSizeCheckBox.Size = new System.Drawing.Size( 46, 17 );
            this.allocationSizeCheckBox.TabIndex = 9;
            this.allocationSizeCheckBox.Text = "Size";
            this.allocationSizeCheckBox.UseVisualStyleBackColor = true;
            this.allocationSizeCheckBox.CheckedChanged += new System.EventHandler( this.allocationSizeCheckBox_CheckedChanged );
            // 
            // allocationAddressToTextBox
            // 
            this.allocationAddressToTextBox.Location = new System.Drawing.Point( 234, 42 );
            this.allocationAddressToTextBox.MaxLength = 8;
            this.allocationAddressToTextBox.Name = "allocationAddressToTextBox";
            this.allocationAddressToTextBox.Size = new System.Drawing.Size( 100, 20 );
            this.allocationAddressToTextBox.TabIndex = 7;
            this.allocationAddressToTextBox.TextChanged += new System.EventHandler( this.allocationAddressToTextBox_TextChanged );
            // 
            // allocationAddressToCheckBox
            // 
            this.allocationAddressToCheckBox.AutoSize = true;
            this.allocationAddressToCheckBox.Location = new System.Drawing.Point( 193, 44 );
            this.allocationAddressToCheckBox.Name = "allocationAddressToCheckBox";
            this.allocationAddressToCheckBox.Size = new System.Drawing.Size( 35, 17 );
            this.allocationAddressToCheckBox.TabIndex = 6;
            this.allocationAddressToCheckBox.Text = "to";
            this.allocationAddressToCheckBox.UseVisualStyleBackColor = true;
            this.allocationAddressToCheckBox.CheckedChanged += new System.EventHandler( this.allocationAddressToCheckBox_CheckedChanged );
            // 
            // allocationAddressTextBox
            // 
            this.allocationAddressTextBox.Location = new System.Drawing.Point( 76, 42 );
            this.allocationAddressTextBox.MaxLength = 8;
            this.allocationAddressTextBox.Name = "allocationAddressTextBox";
            this.allocationAddressTextBox.Size = new System.Drawing.Size( 100, 20 );
            this.allocationAddressTextBox.TabIndex = 5;
            this.allocationAddressTextBox.TextChanged += new System.EventHandler( this.allocationAddressTextBox_TextChanged );
            // 
            // allocationAddressCheckBox
            // 
            this.allocationAddressCheckBox.AutoSize = true;
            this.allocationAddressCheckBox.Location = new System.Drawing.Point( 6, 44 );
            this.allocationAddressCheckBox.Name = "allocationAddressCheckBox";
            this.allocationAddressCheckBox.Size = new System.Drawing.Size( 64, 17 );
            this.allocationAddressCheckBox.TabIndex = 4;
            this.allocationAddressCheckBox.Text = "Address";
            this.allocationAddressCheckBox.UseVisualStyleBackColor = true;
            this.allocationAddressCheckBox.CheckedChanged += new System.EventHandler( this.allocationAddressCheckBox_CheckedChanged );
            // 
            // nextDeallocationRadioButton
            // 
            this.nextDeallocationRadioButton.AutoSize = true;
            this.nextDeallocationRadioButton.Location = new System.Drawing.Point( 288, 19 );
            this.nextDeallocationRadioButton.Name = "nextDeallocationRadioButton";
            this.nextDeallocationRadioButton.Size = new System.Drawing.Size( 82, 17 );
            this.nextDeallocationRadioButton.TabIndex = 3;
            this.nextDeallocationRadioButton.TabStop = true;
            this.nextDeallocationRadioButton.Text = "Next Untally";
            this.nextDeallocationRadioButton.UseVisualStyleBackColor = true;
            // 
            // previousAllocationRadioButton
            // 
            this.previousAllocationRadioButton.AutoSize = true;
            this.previousAllocationRadioButton.Location = new System.Drawing.Point( 6, 19 );
            this.previousAllocationRadioButton.Name = "previousAllocationRadioButton";
            this.previousAllocationRadioButton.Size = new System.Drawing.Size( 91, 17 );
            this.previousAllocationRadioButton.TabIndex = 0;
            this.previousAllocationRadioButton.TabStop = true;
            this.previousAllocationRadioButton.Text = "Previous Tally";
            this.previousAllocationRadioButton.UseVisualStyleBackColor = true;
            // 
            // okButton
            // 
            this.okButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.okButton.Location = new System.Drawing.Point( 384, 281 );
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size( 75, 23 );
            this.okButton.TabIndex = 8;
            this.okButton.Text = "OK";
            this.okButton.UseVisualStyleBackColor = true;
            this.okButton.Click += new System.EventHandler( this.okButton_Click );
            // 
            // cancelButton
            // 
            this.cancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.Location = new System.Drawing.Point( 465, 281 );
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size( 75, 23 );
            this.cancelButton.TabIndex = 9;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            // 
            // GoToForm
            // 
            this.AcceptButton = this.okButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cancelButton;
            this.ClientSize = new System.Drawing.Size( 552, 316 );
            this.Controls.Add( this.cancelButton );
            this.Controls.Add( this.okButton );
            this.Controls.Add( this.allocationGroupBox );
            this.Controls.Add( this.allocationRadioButton );
            this.Controls.Add( this.stackGroupBox );
            this.Controls.Add( this.stackRadioButton );
            this.Controls.Add( this.reportGroupBox );
            this.Controls.Add( this.reportRadioButton );
            this.Controls.Add( this.messageNumberNumericUpDown );
            this.Controls.Add( this.messageNumberRadioButton );
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "GoToForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "Go To";
            ((System.ComponentModel.ISupportInitialize)(this.messageNumberNumericUpDown)).EndInit();
            this.reportGroupBox.ResumeLayout( false );
            this.reportGroupBox.PerformLayout();
            this.stackGroupBox.ResumeLayout( false );
            this.stackGroupBox.PerformLayout();
            this.allocationGroupBox.ResumeLayout( false );
            this.allocationGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.allocationSizeToNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.allocationSizeNumericUpDown)).EndInit();
            this.ResumeLayout( false );
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton messageNumberRadioButton;
        private System.Windows.Forms.NumericUpDown messageNumberNumericUpDown;
        private System.Windows.Forms.RadioButton reportRadioButton;
        private System.Windows.Forms.GroupBox reportGroupBox;
        private System.Windows.Forms.RadioButton previousReportRadioButton;
        private System.Windows.Forms.RadioButton nextReportRadioButton;
        private System.Windows.Forms.RadioButton selectReportRadioButton;
        private System.Windows.Forms.ComboBox selectReportComboBox;
        private System.Windows.Forms.RadioButton stackRadioButton;
        private System.Windows.Forms.GroupBox stackGroupBox;
        private System.Windows.Forms.RadioButton nextStackPopRadioButton;
        private System.Windows.Forms.RadioButton previousStackPopRadioButton;
        private System.Windows.Forms.RadioButton nextStackPushRadioButton;
        private System.Windows.Forms.RadioButton previousStackPushRadioButton;
        private System.Windows.Forms.CheckBox selectedStackCheckBox;
        private System.Windows.Forms.Button selectStacksButton;
        private System.Windows.Forms.CheckBox specifiedStacksCheckBox;
        private System.Windows.Forms.Button specifyStacksButton;
        private System.Windows.Forms.RadioButton allocationRadioButton;
        private System.Windows.Forms.GroupBox allocationGroupBox;
        private System.Windows.Forms.RadioButton previousAllocationRadioButton;
        private System.Windows.Forms.RadioButton nextDeallocationRadioButton;
        private System.Windows.Forms.CheckBox allocationAddressCheckBox;
        private System.Windows.Forms.TextBox allocationAddressTextBox;
        private System.Windows.Forms.TextBox allocationAddressToTextBox;
        private System.Windows.Forms.CheckBox allocationAddressToCheckBox;
        private System.Windows.Forms.CheckBox allocationSizeCheckBox;
        private System.Windows.Forms.Label allocationSizeLabel;
        private System.Windows.Forms.NumericUpDown allocationSizeNumericUpDown;
        private System.Windows.Forms.NumericUpDown allocationSizeToNumericUpDown;
        private System.Windows.Forms.CheckBox allocationSizeToCheckBox;
        private System.Windows.Forms.Label allocationSizeToLabel;
        private System.Windows.Forms.RadioButton nextAllocationRadioButton;
        private System.Windows.Forms.RadioButton previousDeallocationRadioButton;
        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Label hexadecimalLabel;

    }
}