using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using MemTrackerControls;

namespace MemTracker
{
    public partial class GoToForm : Form
    {
        protected GoToForm()
        {
            InitializeComponent();
        }

        public GoToForm( GoToOptions options, bool allowBackwards )
            : this()
        {
            this.Options = options;

            this.previousAllocationRadioButton.Enabled = allowBackwards;
            this.previousDeallocationRadioButton.Enabled = allowBackwards;
            this.previousReportRadioButton.Enabled = allowBackwards;
            this.previousStackPopRadioButton.Enabled = allowBackwards;
            this.previousStackPushRadioButton.Enabled = allowBackwards;
        }

        #region Properties
        public GoToOptions Options
        {
            get
            {
                return m_goToOptions;
            }
            set
            {
                m_goToOptions = value;

                // Message Number
                this.messageNumberRadioButton.Checked = this.messageNumberNumericUpDown.Enabled 
                    = m_goToOptions.Search == GoToOptions.SearchType.MessageNumber;

                this.messageNumberNumericUpDown.Value = m_goToOptions.MessageNumber;

                // Report
                this.reportRadioButton.Checked = this.reportGroupBox.Enabled
                    = m_goToOptions.Search == GoToOptions.SearchType.Report;

                switch ( m_goToOptions.Report )
                {
                    case GoToOptions.ReportType.Next:
                        this.nextReportRadioButton.Checked = true;
                        break;
                    case GoToOptions.ReportType.Previous:
                        this.previousReportRadioButton.Checked = true;
                        break;
                    case GoToOptions.ReportType.Selected:
                        this.selectReportRadioButton.Checked = true;
                        break;
                }

                this.selectReportComboBox.Enabled = this.selectReportRadioButton.Checked;

                this.selectReportComboBox.Items.Clear();
                foreach ( ReportItem report in m_goToOptions.Reports )
                {
                    this.selectReportComboBox.Items.Add( report );
                }

                this.selectReportComboBox.SelectedItem = m_goToOptions.SelectedReport;

                // Stack
                this.stackRadioButton.Checked = this.stackGroupBox.Enabled
                    = m_goToOptions.Search == GoToOptions.SearchType.Stack;

                switch ( m_goToOptions.Stack )
                {
                    case GoToOptions.StackType.NextPop:
                        this.nextStackPopRadioButton.Checked = true;
                        break;
                    case GoToOptions.StackType.NextPush:
                        this.nextStackPushRadioButton.Checked = true;
                        break;
                    case GoToOptions.StackType.PreviousPop:
                        this.previousStackPopRadioButton.Checked = true;
                        break;
                    case GoToOptions.StackType.PreviousPush:
                        this.previousStackPushRadioButton.Checked = true;
                        break;
                }

                this.selectedStackCheckBox.Checked = this.selectStacksButton.Enabled = m_goToOptions.UseSelectedStacks;
                this.specifiedStacksCheckBox.Checked = this.specifyStacksButton.Enabled = m_goToOptions.UseSpecifiedStacks;

                m_selectedStacks = new Dictionary<uint, bool>( m_goToOptions.SelectedStacks );
                m_specifiedStacks = new List<string>( m_goToOptions.SpecifiedStacks );

                // Allocation
                this.allocationRadioButton.Checked = this.allocationGroupBox.Enabled
                    = m_goToOptions.Search == GoToOptions.SearchType.Allocation;

                switch ( m_goToOptions.Allocation )
                {
                    case GoToOptions.AllocationType.NextAllocation:
                        this.nextAllocationRadioButton.Checked = true;
                        break;
                    case GoToOptions.AllocationType.NextDeallocation:
                        this.nextDeallocationRadioButton.Checked = true;
                        break;
                    case GoToOptions.AllocationType.PreviousAllocation:
                        this.previousAllocationRadioButton.Checked = true;
                        break;
                    case GoToOptions.AllocationType.PreviousDeallocation:
                        this.previousAllocationRadioButton.Checked = true;
                        break;
                }

                m_startAddress = m_goToOptions.StartAddress;
                m_endAddress = m_goToOptions.EndAddress;
                m_startAddressString = Convert.ToString( m_goToOptions.StartAddress, 16 );
                m_endAddressString = Convert.ToString( m_goToOptions.EndAddress, 16 );

                this.allocationAddressCheckBox.Checked = this.allocationAddressTextBox.Enabled = m_goToOptions.UseStartAddress;
                this.allocationAddressTextBox.Text = m_startAddressString;
                this.allocationAddressToCheckBox.Checked = m_goToOptions.UseEndAddress;
                this.allocationAddressToCheckBox.Enabled = this.allocationAddressCheckBox.Checked;
                this.allocationAddressToTextBox.Enabled = this.allocationAddressToCheckBox.Checked;
                this.allocationAddressToTextBox.Text = m_endAddressString;

                this.allocationSizeCheckBox.Checked = this.allocationSizeNumericUpDown.Enabled = m_goToOptions.UseMinimumSize;
                this.allocationSizeNumericUpDown.Value = m_goToOptions.MinimumSize;
                this.allocationSizeToCheckBox.Checked = m_goToOptions.UseMaximumSize;
                this.allocationSizeToCheckBox.Enabled = this.allocationSizeCheckBox.Checked;
                this.allocationSizeToNumericUpDown.Enabled = this.allocationSizeToCheckBox.Checked;
                this.allocationSizeToNumericUpDown.Value = m_goToOptions.MaximumSize;
            }
        }
        #endregion

        #region Variables
        private GoToOptions m_goToOptions = new GoToOptions();

        private Dictionary<uint, bool> m_selectedStacks = new Dictionary<uint, bool>();
        private List<string> m_specifiedStacks = new List<string>();

        private uint m_startAddress = 0;
        private uint m_endAddress = 0;
        private string m_startAddressString = string.Empty;
        private string m_endAddressString = string.Empty;
        #endregion

        #region Event Handlers

        #region Message Number
        private void messageNumberRadioButton_CheckedChanged( object sender, EventArgs e )
        {
            this.messageNumberNumericUpDown.Enabled = this.messageNumberRadioButton.Checked;
        }
        #endregion

        #region Report
        private void reportRadioButton_CheckedChanged( object sender, EventArgs e )
        {
            this.reportGroupBox.Enabled = this.reportRadioButton.Checked;
        }

        private void selectReportRadioButton_CheckedChanged( object sender, EventArgs e )
        {
            this.selectReportComboBox.Enabled = this.selectReportRadioButton.Checked;
        }
        #endregion

        #region Stack
        private void stackRadioButton_CheckedChanged( object sender, EventArgs e )
        {
            this.stackGroupBox.Enabled = this.stackRadioButton.Checked;
        }

        private void selectedStackCheckBox_CheckedChanged( object sender, EventArgs e )
        {
            this.selectStacksButton.Enabled = this.selectedStackCheckBox.Checked;
        }

        private void selectStacksButton_Click( object sender, EventArgs e )
        {
            bool dummy = false;
            ItemVisibilityForm.ShowIt( this, "Select Stacks", ref m_selectedStacks, Allocation.StackNameDictionary,
                ref dummy, string.Empty );
        }

        private void specifiedStacksCheckBox_CheckedChanged( object sender, EventArgs e )
        {
            this.specifyStacksButton.Enabled = this.specifiedStacksCheckBox.Checked;
        }

        private void specifyStacksButton_Click( object sender, EventArgs e )
        {
            SpecifyStacksForm.ShowIt( this, ref m_specifiedStacks );
        }
        #endregion

        #region Allocation
        private void allocationRadioButton_CheckedChanged( object sender, EventArgs e )
        {
            this.allocationGroupBox.Enabled = this.allocationRadioButton.Checked;
        }

        private void allocationAddressCheckBox_CheckedChanged( object sender, EventArgs e )
        {
            this.allocationAddressTextBox.Enabled = this.allocationAddressCheckBox.Checked;
            this.allocationAddressToCheckBox.Enabled = this.allocationAddressTextBox.Enabled;
            this.allocationAddressToTextBox.Enabled = this.allocationAddressToCheckBox.Enabled && this.allocationAddressToCheckBox.Checked;
        }

        private void allocationAddressTextBox_TextChanged( object sender, EventArgs e )
        {
            string text = this.allocationAddressTextBox.Text;

            if ( (text.Trim() != string.Empty) && (text != m_startAddressString) )
            {
                try
                {
                    uint startAddress = uint.Parse( text, System.Globalization.NumberStyles.HexNumber );
                    m_startAddress = startAddress;
                    m_startAddressString = text;

                    if ( startAddress < 0 )
                    {
                        startAddress = 0;
                    }
                    else if ( this.allocationSizeToCheckBox.Checked && (m_endAddressString != string.Empty) )
                    {
                        if ( startAddress > m_endAddress )
                        {
                            startAddress = m_endAddress;
                        }
                    }

                    if ( m_startAddress != startAddress )
                    {
                        m_startAddress = startAddress;
                        m_startAddressString = Convert.ToString( startAddress, 16 );
                        this.allocationAddressTextBox.Text = m_startAddressString;
                    }
                }
                catch
                {
                    this.allocationAddressTextBox.Text = m_startAddressString;
                    return;
                }
            }
        }

        private void allocationAddressToCheckBox_CheckedChanged( object sender, EventArgs e )
        {
            this.allocationAddressToTextBox.Enabled = this.allocationAddressToCheckBox.Checked;

            if ( this.allocationAddressToCheckBox.Checked && (m_startAddressString != string.Empty) && (m_endAddressString != string.Empty) )
            {
                if ( m_endAddress < m_startAddress )
                {
                    m_endAddressString = m_startAddressString;
                    this.allocationAddressToTextBox.Text = m_endAddressString;
                }
            }
        }

        private void allocationAddressToTextBox_TextChanged( object sender, EventArgs e )
        {
            string text = this.allocationAddressToTextBox.Text;

            if ( (text.Trim() != string.Empty) && (text != m_endAddressString) )
            {
                try
                {
                    uint endAddress = uint.Parse( text, System.Globalization.NumberStyles.HexNumber );
                    m_endAddress = endAddress;
                    m_endAddressString = text;

                    if ( endAddress < m_startAddress )
                    {
                        endAddress = m_startAddress;
                    }
                    else if ( endAddress < 0 )
                    {
                        endAddress = 0;
                    }

                    if ( m_endAddress != endAddress )
                    {
                        m_endAddress = endAddress;
                        m_endAddressString = Convert.ToString( endAddress, 16 );
                        this.allocationAddressToTextBox.Text = m_endAddressString;
                    }
                }
                catch
                {
                    this.allocationAddressToTextBox.Text = m_endAddressString;
                    return;
                }
            }
        }

        private void allocationSizeCheckBox_CheckedChanged( object sender, EventArgs e )
        {
            this.allocationSizeNumericUpDown.Enabled = this.allocationSizeCheckBox.Checked;
            this.allocationSizeToCheckBox.Enabled = this.allocationSizeNumericUpDown.Enabled;
            this.allocationSizeToNumericUpDown.Enabled = this.allocationSizeToCheckBox.Enabled && this.allocationSizeToCheckBox.Checked;
        }

        private void allocationSizeNumericUpDown_ValueChanged( object sender, EventArgs e )
        {
            if ( this.allocationSizeNumericUpDown.Value < 0 )
            {
                this.allocationSizeNumericUpDown.Value = 0;
            }
            else if ( (this.allocationSizeNumericUpDown.Value > this.allocationSizeToNumericUpDown.Value)
                && this.allocationSizeToCheckBox.Checked )
            {
                this.allocationSizeNumericUpDown.Value = this.allocationSizeToNumericUpDown.Value;
            }
        }

        private void allocationSizeToCheckBox_CheckedChanged( object sender, EventArgs e )
        {
            this.allocationSizeToNumericUpDown.Enabled = this.allocationSizeToCheckBox.Checked;

            if ( (this.allocationSizeToNumericUpDown.Value < this.allocationSizeNumericUpDown.Value)
                && this.allocationSizeToCheckBox.Checked )
            {
                this.allocationSizeToNumericUpDown.Value = this.allocationSizeNumericUpDown.Value;
            }
        }

        private void allocationSizeToNumericUpDown_ValueChanged( object sender, EventArgs e )
        {
            if ( this.allocationSizeToNumericUpDown.Value < 0 )
            {
                this.allocationSizeToNumericUpDown.Value = 0;
            }
            else if ( this.allocationSizeToNumericUpDown.Value < this.allocationSizeNumericUpDown.Value )
            {
                this.allocationSizeToNumericUpDown.Value = this.allocationSizeNumericUpDown.Value;
            }
            else if ( this.allocationSizeToNumericUpDown.Value > 999999999 )
            {
                this.allocationSizeToNumericUpDown.Value = 999999999;
            }
        }
        #endregion

        private void okButton_Click( object sender, EventArgs e )
        {
            if ( this.messageNumberRadioButton.Checked )
            {
                m_goToOptions.Search = GoToOptions.SearchType.MessageNumber;
            }
            else if ( this.reportRadioButton.Checked )
            {
                m_goToOptions.Search = GoToOptions.SearchType.Report;
            }
            else if ( this.stackRadioButton.Checked )
            {
                m_goToOptions.Search = GoToOptions.SearchType.Stack;
            }
            else
            {
                m_goToOptions.Search = GoToOptions.SearchType.Allocation;
            }

            // Message Number
            m_goToOptions.MessageNumber = (uint)this.messageNumberNumericUpDown.Value;

            // Report
            if ( this.nextReportRadioButton.Checked )
            {
                m_goToOptions.Report = GoToOptions.ReportType.Next;
            }
            else if ( this.previousReportRadioButton.Checked )
            {
                m_goToOptions.Report = GoToOptions.ReportType.Previous;
            }
            else if ( this.selectReportRadioButton.Checked )
            {
                m_goToOptions.Report = GoToOptions.ReportType.Selected;
            }

            m_goToOptions.SelectedReport = this.selectReportComboBox.SelectedItem as ReportItem;

            // Stack
            if ( this.nextStackPopRadioButton.Checked )
            {
                m_goToOptions.Stack = GoToOptions.StackType.NextPop;
            }
            else if ( this.nextStackPushRadioButton.Checked )
            {
                m_goToOptions.Stack = GoToOptions.StackType.NextPush;
            }
            else if ( this.previousStackPopRadioButton.Checked )
            {
                m_goToOptions.Stack = GoToOptions.StackType.PreviousPop;
            }
            else
            {
                m_goToOptions.Stack = GoToOptions.StackType.PreviousPush;
            }

            m_goToOptions.UseSelectedStacks = this.selectedStackCheckBox.Checked;
            m_goToOptions.UseSpecifiedStacks = this.specifiedStacksCheckBox.Checked;

            m_goToOptions.SelectedStacks = m_selectedStacks;
            m_goToOptions.SpecifiedStacks = m_specifiedStacks;

            // Allocation
            if ( this.nextAllocationRadioButton.Checked )
            {
                m_goToOptions.Allocation = GoToOptions.AllocationType.NextAllocation;
            }
            else if ( this.nextDeallocationRadioButton.Checked )
            {
                m_goToOptions.Allocation = GoToOptions.AllocationType.NextDeallocation;
            }
            else if ( this.previousAllocationRadioButton.Checked )
            {
                m_goToOptions.Allocation = GoToOptions.AllocationType.PreviousAllocation;
            }
            else
            {
                m_goToOptions.Allocation = GoToOptions.AllocationType.PreviousDeallocation;
            }

            m_goToOptions.UseStartAddress = this.allocationAddressCheckBox.Checked;
            m_goToOptions.StartAddress = m_startAddress;
            m_goToOptions.UseEndAddress = this.allocationAddressToCheckBox.Checked;
            m_goToOptions.EndAddress = m_endAddress;

            m_goToOptions.UseMinimumSize = this.allocationSizeCheckBox.Checked;
            m_goToOptions.MinimumSize = (uint)this.allocationSizeNumericUpDown.Value;
            m_goToOptions.UseMaximumSize = this.allocationSizeToCheckBox.Checked;
            m_goToOptions.MaximumSize = (uint)this.allocationSizeToNumericUpDown.Value;
        }
        #endregion
    }

    public class GoToOptions
    {
        public GoToOptions()
        {

        }

        #region Enums
        public enum SearchType
        {
            MessageNumber,
            Report,
            Stack,
            Allocation
        }

        public enum ReportType
        {
            Previous,
            Next,
            Selected
        }

        public enum StackType
        {
            PreviousPush,
            NextPush,
            PreviousPop,
            NextPop
        }

        public enum AllocationType
        {
            PreviousAllocation,
            NextAllocation,
            PreviousDeallocation,
            NextDeallocation
        }
        #endregion

        #region Properties
        public SearchType Search = SearchType.MessageNumber;

        public uint MessageNumber = 0;

        public ReportType Report = ReportType.Next;
        public ReportItem SelectedReport = null;
        public List<ReportItem> Reports = new List<ReportItem>();

        public StackType Stack = StackType.NextPush;
        public bool UseSelectedStacks = false;
        public Dictionary<uint, bool> SelectedStacks = new Dictionary<uint, bool>();
        public bool UseSpecifiedStacks = false;
        public List<string> SpecifiedStacks = new List<string>();

        public AllocationType Allocation = AllocationType.NextAllocation;
        public bool UseStartAddress = false;
        public uint StartAddress = 0;
        public bool UseEndAddress = false;
        public uint EndAddress = 0;
        public bool UseMinimumSize = false;
        public uint MinimumSize = 0;
        public bool UseMaximumSize = false;
        public uint MaximumSize = 999999999;
        #endregion

        #region Public Functions
        public string Serialize()
        {
            StringBuilder text = new StringBuilder();

            text.Append( this.Search.ToString() );
            text.Append( ';' );

            // Message Number
            text.Append( this.MessageNumber );
            text.Append( ';' );

            // Report
            text.Append( this.Report.ToString() );
            text.Append( ';' );

            // Stack
            text.Append( this.Stack.ToString() );
            text.Append( ';' );

            text.Append( this.UseSelectedStacks );
            text.Append( ';' );

            foreach ( KeyValuePair<uint, bool> pair in this.SelectedStacks )
            {
                if ( pair.Value )
                {
                    text.Append( pair.Key );
                    text.Append( ',' );
                }
            }
            text.Append( ';' );

            text.Append( this.UseSpecifiedStacks );
            text.Append( ';' );

            foreach ( string stack in this.SpecifiedStacks )
            {
                text.Append( stack.Replace( ';', ' ' ) );
                text.Append( '\n' );
            }
            text.Append( ';' );

            // Allocation
            text.Append( this.Allocation.ToString() );
            text.Append( ';' );

            text.Append( this.UseStartAddress );
            text.Append( ';' );

            text.Append( this.StartAddress );
            text.Append( ';' );

            text.Append( this.UseEndAddress );
            text.Append( ';' );

            text.Append( this.EndAddress );
            text.Append( ';' );

            text.Append( this.UseMinimumSize );
            text.Append( ';' );

            text.Append( this.MinimumSize );
            text.Append( ';' );

            text.Append( this.UseMaximumSize );
            text.Append( ';' );

            text.Append( this.MaximumSize );
            text.Append( ';' );

            return text.ToString();
        }

        public bool Deserialize( string text )
        {
            string[] split = text.Split( new char[] { ';' } );
            if ( split.Length < 17 )
            {
                return false;
            }

            this.Search = (SearchType)Enum.Parse( typeof( SearchType ), split[0] );

            // Message Number
            this.MessageNumber = uint.Parse( split[1] );

            // Report
            this.Report = (ReportType)Enum.Parse( typeof( ReportType ), split[2] );

            // Stack
            this.Stack = (StackType)Enum.Parse( typeof( StackType ), split[3] );
            this.UseSelectedStacks = bool.Parse( split[4] );

            string[] stackSplit = split[5].Split( new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries );
            this.SelectedStacks.Clear();
            foreach ( string stack in stackSplit )
            {
                uint hashKey = uint.Parse( stack );
                this.SelectedStacks.Add( hashKey, true );
            }

            this.UseSpecifiedStacks = bool.Parse( split[6] );

            string[] specifiedSplit = split[7].Split( new char[] { '\n' }, StringSplitOptions.RemoveEmptyEntries );
            this.SpecifiedStacks.Clear();
            foreach ( string specified in specifiedSplit )
            {
                this.SpecifiedStacks.Add( specified );
            }

            this.Allocation = (AllocationType)Enum.Parse( typeof( AllocationType ), split[8] );
            this.UseStartAddress = bool.Parse( split[9] );
            this.StartAddress = uint.Parse( split[10] );
            this.UseEndAddress = bool.Parse( split[11] );
            this.EndAddress = uint.Parse( split[12] );
            this.UseMinimumSize = bool.Parse( split[13] );
            this.MinimumSize = uint.Parse( split[14] );
            this.UseMaximumSize = bool.Parse( split[15] );
            this.MaximumSize = uint.Parse( split[16] );

            return true;
        }
        #endregion
    }
}