using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using MemTrackerControls;

namespace MemTracker
{
    public partial class RefreshProgressForm : Form
    {
        protected RefreshProgressForm()
        {
            InitializeComponent();

            m_updateStepEventHandler = new UpdateStepEventHandler( MemTrackerControls_UpdateStep );
            m_updateCompleteEventHandler = new UpdateCompleteEventHandler( MemTrackerControls_UpdateComplete );
        }

        public RefreshProgressForm( OverviewListControl overview, int numSteps )
            : this()
        {
            InitializeComponent();

            this.progressBar1.Maximum = numSteps;

            m_overview = overview;
            m_overview.UpdateStep += m_updateStepEventHandler;
            m_overview.UpdateComplete += m_updateCompleteEventHandler;
        }

        public RefreshProgressForm( AllocationsTreeListControl allocations, int numSteps )
            : this()
        {
            InitializeComponent();

            this.progressBar1.Maximum = numSteps;

            m_allocations = allocations;
            m_allocations.UpdateStep += m_updateStepEventHandler;
            m_allocations.UpdateComplete += m_updateCompleteEventHandler;
        }

        public RefreshProgressForm( MemoryLayoutControl memoryLayout, int numSteps )
            : this()
        {
            InitializeComponent();

            this.progressBar1.Maximum = numSteps;

            m_memoryLayout = memoryLayout;
            m_memoryLayout.UpdateStep += m_updateStepEventHandler;
            m_memoryLayout.UpdateComplete += m_updateCompleteEventHandler;
        }

        #region Variables
        private OverviewListControl m_overview = null;
        private AllocationsTreeListControl m_allocations = null;
        private MemoryLayoutControl m_memoryLayout = null;

        private UpdateStepEventHandler m_updateStepEventHandler;
        private UpdateCompleteEventHandler m_updateCompleteEventHandler;
        #endregion

        #region MemTrackerControls Event Handlers
        private void MemTrackerControls_UpdateStep( object sender, UpdateStepEventArgs e )
        {
            this.progressBar1.Value = e.StepNumber;
        }

        private void MemTrackerControls_UpdateComplete( object sender, UpdateCompleteEventArgs e )
        {
            this.Close();
        }
        #endregion
    }
}