namespace MemTracker
{
    partial class RefreshProgressForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( m_overview != null )
            {
                m_overview.UpdateStep -= m_updateStepEventHandler;
                m_overview.UpdateComplete -= m_updateCompleteEventHandler;
            }

            if ( m_allocations != null )
            {
                m_allocations.UpdateStep -= m_updateStepEventHandler;
                m_allocations.UpdateComplete -= m_updateCompleteEventHandler;
            }

            if ( m_memoryLayout != null )
            {
                m_memoryLayout.UpdateStep -= m_updateStepEventHandler;
                m_memoryLayout.UpdateComplete -= m_updateCompleteEventHandler;
            }
            
            if ( disposing && (components != null) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.SuspendLayout();
            // 
            // progressBar1
            // 
            this.progressBar1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.progressBar1.Location = new System.Drawing.Point( 0, 0 );
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size( 402, 39 );
            this.progressBar1.Step = 1;
            this.progressBar1.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.progressBar1.TabIndex = 0;
            // 
            // RefreshProgressForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size( 402, 39 );
            this.ControlBox = false;
            this.Controls.Add( this.progressBar1 );
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "RefreshProgressForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "Refreshing...";
            this.ResumeLayout( false );

        }

        #endregion

        private System.Windows.Forms.ProgressBar progressBar1;
    }
}