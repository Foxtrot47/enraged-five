namespace MemTracker
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && (components != null) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            TD.SandDock.DockContainer dockContainer1;
            TD.SandDock.DockingRules dockingRules1 = new TD.SandDock.DockingRules();
            TD.SandDock.DockingRules dockingRules2 = new TD.SandDock.DockingRules();
            TD.SandDock.DockContainer dockContainer2;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager( typeof( Form1 ) );
            this.overviewDockableWindow1 = new TD.SandDock.DockableWindow();
            this.overviewListControl1 = new MemTrackerControls.OverviewListControl();
            this.trackerComponent1 = new MemTrackerControls.TrackerComponent( this.components );
            this.allocationsDockableWindow1 = new TD.SandDock.DockableWindow();
            this.allocationsTreeListControl1 = new MemTrackerControls.AllocationsTreeListControl();
            this.memoryLayoutDockableWindow1 = new TD.SandDock.DockableWindow();
            this.memoryLayoutControl1 = new MemTrackerControls.MemoryLayoutControl();
            this.sandDockManager1 = new TD.SandDock.SandDockManager();
            this.toolStripContainer1 = new System.Windows.Forms.ToolStripContainer();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.messageCountToolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.messagesDockableWindow = new TD.SandDock.DockableWindow();
            this.messagesListBox = new System.Windows.Forms.ListBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fileOpenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fileOpenSingleTrackerFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fileOpenMultiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fileCloseTrackerFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.fileSaveReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.fileRecentFilesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.fileExitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.playbackToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.playbackSkipBackwardToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.playbackStepBackwardToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.playbackPauseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.playbackPlayToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.playbackStepForwardToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.playbackSkipForwardToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.playbackGoToToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.playbackGoToAgainToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolsCustomizeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolsOptionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.windowToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.windowNewOverviewWindowToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.windowNewAllocationsWindowToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.windowNewMemoryLayoutWindowToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.viewErrorsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpContentsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.helpAboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newWindowsToolStrip = new System.Windows.Forms.ToolStrip();
            this.newOverviewWindowToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.newAllocationsWindowToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.newMemoryLayoutWindowToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.playbackToolStrip = new System.Windows.Forms.ToolStrip();
            this.skipBackwardToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.stepBackwardToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.pauseToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.playToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.stepForwardToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.skipForwardToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.timer1 = new System.Windows.Forms.Timer( this.components );
            dockContainer1 = new TD.SandDock.DockContainer();
            dockContainer2 = new TD.SandDock.DockContainer();
            dockContainer1.SuspendLayout();
            this.overviewDockableWindow1.SuspendLayout();
            this.allocationsDockableWindow1.SuspendLayout();
            this.memoryLayoutDockableWindow1.SuspendLayout();
            this.toolStripContainer1.BottomToolStripPanel.SuspendLayout();
            this.toolStripContainer1.ContentPanel.SuspendLayout();
            this.toolStripContainer1.TopToolStripPanel.SuspendLayout();
            this.toolStripContainer1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            dockContainer2.SuspendLayout();
            this.messagesDockableWindow.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.newWindowsToolStrip.SuspendLayout();
            this.playbackToolStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // dockContainer1
            // 
            dockContainer1.ContentSize = 843;
            dockContainer1.Controls.Add( this.overviewDockableWindow1 );
            dockContainer1.Controls.Add( this.allocationsDockableWindow1 );
            dockContainer1.Controls.Add( this.memoryLayoutDockableWindow1 );
            dockContainer1.Dock = System.Windows.Forms.DockStyle.Left;
            dockContainer1.LayoutSystem = new TD.SandDock.SplitLayoutSystem( new System.Drawing.SizeF( 843F, 386F ), System.Windows.Forms.Orientation.Horizontal, new TD.SandDock.LayoutSystemBase[] {
            ((TD.SandDock.LayoutSystemBase)(new TD.SandDock.ControlLayoutSystem(new System.Drawing.SizeF(843F, 386F), new TD.SandDock.DockControl[] {
                        ((TD.SandDock.DockControl)(this.overviewDockableWindow1)),
                        ((TD.SandDock.DockControl)(this.allocationsDockableWindow1)),
                        ((TD.SandDock.DockControl)(this.memoryLayoutDockableWindow1))}, this.overviewDockableWindow1)))} );
            dockContainer1.Location = new System.Drawing.Point( 0, 0 );
            dockContainer1.Manager = this.sandDockManager1;
            dockContainer1.Name = "dockContainer1";
            dockContainer1.Size = new System.Drawing.Size( 847, 386 );
            dockContainer1.TabIndex = 0;
            // 
            // overviewDockableWindow1
            // 
            this.overviewDockableWindow1.CloseAction = TD.SandDock.DockControlCloseAction.Dispose;
            this.overviewDockableWindow1.Controls.Add( this.overviewListControl1 );
            dockingRules1.AllowDockBottom = true;
            dockingRules1.AllowDockLeft = true;
            dockingRules1.AllowDockRight = true;
            dockingRules1.AllowDockTop = true;
            dockingRules1.AllowFloat = true;
            dockingRules1.AllowTab = true;
            this.overviewDockableWindow1.DockingRules = dockingRules1;
            this.overviewDockableWindow1.FloatingSize = new System.Drawing.Size( 1000, 600 );
            this.overviewDockableWindow1.Guid = new System.Guid( "5c664a66-8528-46a7-9dee-24c6ec367da7" );
            this.overviewDockableWindow1.Location = new System.Drawing.Point( 0, 16 );
            this.overviewDockableWindow1.Name = "overviewDockableWindow1";
            this.overviewDockableWindow1.ShowOptions = false;
            this.overviewDockableWindow1.Size = new System.Drawing.Size( 843, 346 );
            this.overviewDockableWindow1.TabImage = global::MemTracker.Properties.Resources.OverviewWindow;
            this.overviewDockableWindow1.TabIndex = 0;
            this.overviewDockableWindow1.Text = "Overview (1)";
            // 
            // overviewListControl1
            // 
            this.overviewListControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.overviewListControl1.LeftReport = null;
            this.overviewListControl1.Location = new System.Drawing.Point( 0, 0 );
            this.overviewListControl1.Name = "overviewListControl1";
            this.overviewListControl1.ParentControl = this;
            this.overviewListControl1.RefeshButtonEnabled = false;
            this.overviewListControl1.RightReport = null;
            this.overviewListControl1.ShowBy = MemTrackerControls.OverviewListControl.DisplayType.MemoryType;
            this.overviewListControl1.ShowSizingGrip = false;
            this.overviewListControl1.Size = new System.Drawing.Size( 843, 346 );
            this.overviewListControl1.TabIndex = 0;
            this.overviewListControl1.ThreadingEnabled = true;
            this.overviewListControl1.Tracker = this.trackerComponent1;
            this.overviewListControl1.VisibleDatatypeHashKeys = ((System.Collections.Generic.List<uint>)(resources.GetObject( "overviewListControl1.VisibleDatatypeHashKeys" )));
            this.overviewListControl1.VisibleDatatypes = MemTrackerControls.OverviewListControl.SelectedDatatypes.All;
            this.overviewListControl1.VisibleDatatypesTypeNames = ((System.Collections.Generic.List<string>)(resources.GetObject( "overviewListControl1.VisibleDatatypesTypeNames" )));
            this.overviewListControl1.VisibleStackHashKeys = ((System.Collections.Generic.List<uint>)(resources.GetObject( "overviewListControl1.VisibleStackHashKeys" )));
            this.overviewListControl1.VisibleStacks = MemTrackerControls.OverviewListControl.SelectedStacks.All;
            this.overviewListControl1.VisibleStackTypeNames = ((System.Collections.Generic.List<string>)(resources.GetObject( "overviewListControl1.VisibleStackTypeNames" )));
            this.overviewListControl1.UpdateComplete += new MemTrackerControls.UpdateCompleteEventHandler( this.MemTrackerControl_UpdateComplete );
            this.overviewListControl1.UpdateBegin += new MemTrackerControls.UpdateBeginEventHandler( this.overviewListControl1_UpdateBegin );
            // 
            // trackerComponent1
            // 
            this.trackerComponent1.OverwriteDuplicateAllocations = false;
            this.trackerComponent1.ParentControl = this;
            this.trackerComponent1.Platform = "Unknown";
            this.trackerComponent1.TrackDeallocations = true;
            this.trackerComponent1.StackAdded += new MemTrackerControls.StackChangedEventHandler( this.trackerComponent1_StackAdded );
            this.trackerComponent1.ReportRemoved += new MemTrackerControls.ReportChangedEventHandler( this.trackerComponent1_ReportRemoved );
            this.trackerComponent1.ReportAdded += new MemTrackerControls.ReportChangedEventHandler( this.trackerComponent1_ReportAdded );
            this.trackerComponent1.DuplicateAllocation += new MemTrackerControls.DuplicateAllocationEventHandler( this.trackerComponent1_DuplicateAllocation );
            // 
            // allocationsDockableWindow1
            // 
            this.allocationsDockableWindow1.CloseAction = TD.SandDock.DockControlCloseAction.Dispose;
            this.allocationsDockableWindow1.Controls.Add( this.allocationsTreeListControl1 );
            dockingRules2.AllowDockBottom = true;
            dockingRules2.AllowDockLeft = true;
            dockingRules2.AllowDockRight = true;
            dockingRules2.AllowDockTop = true;
            dockingRules2.AllowFloat = true;
            dockingRules2.AllowTab = true;
            this.allocationsDockableWindow1.DockingRules = dockingRules2;
            this.allocationsDockableWindow1.FloatingSize = new System.Drawing.Size( 1000, 600 );
            this.allocationsDockableWindow1.Guid = new System.Guid( "30f8b04d-203f-4dfb-8142-d0fa6af52ce1" );
            this.allocationsDockableWindow1.Location = new System.Drawing.Point( 0, 0 );
            this.allocationsDockableWindow1.Name = "allocationsDockableWindow1";
            this.allocationsDockableWindow1.ShowOptions = false;
            this.allocationsDockableWindow1.Size = new System.Drawing.Size( 843, 346 );
            this.allocationsDockableWindow1.TabImage = global::MemTracker.Properties.Resources.AllocationsWindow;
            this.allocationsDockableWindow1.TabIndex = 0;
            this.allocationsDockableWindow1.Text = "Allocations (1)";
            // 
            // allocationsTreeListControl1
            // 
            this.allocationsTreeListControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.allocationsTreeListControl1.GroupBy = MemTrackerControls.AllocationsTreeListControl.DisplayType.Order;
            this.allocationsTreeListControl1.Location = new System.Drawing.Point( 0, 0 );
            this.allocationsTreeListControl1.MultiSelect = false;
            this.allocationsTreeListControl1.Name = "allocationsTreeListControl1";
            this.allocationsTreeListControl1.ParentControl = this;
            this.allocationsTreeListControl1.RefeshButtonEnabled = false;
            this.allocationsTreeListControl1.Report = null;
            this.allocationsTreeListControl1.ShowSizingGrip = false;
            this.allocationsTreeListControl1.Size = new System.Drawing.Size( 843, 346 );
            this.allocationsTreeListControl1.TabIndex = 0;
            this.allocationsTreeListControl1.ThreadingEnabled = true;
            this.allocationsTreeListControl1.Tracker = this.trackerComponent1;
            this.allocationsTreeListControl1.VisibleDatatypeHashKeys = ((System.Collections.Generic.List<uint>)(resources.GetObject( "allocationsTreeListControl1.VisibleDatatypeHashKeys" )));
            this.allocationsTreeListControl1.VisibleDatatypes = MemTrackerControls.AllocationsTreeListControl.SelectedDatatypes.All;
            this.allocationsTreeListControl1.VisibleDatatypesTypeNames = ((System.Collections.Generic.List<string>)(resources.GetObject( "allocationsTreeListControl1.VisibleDatatypesTypeNames" )));
            this.allocationsTreeListControl1.VisibleMemoryTypeNames = ((System.Collections.Generic.List<string>)(resources.GetObject( "allocationsTreeListControl1.VisibleMemoryTypeNames" )));
            this.allocationsTreeListControl1.VisibleMemoryTypes = MemTrackerControls.AllocationsTreeListControl.SelectedMemoryTypes.All;
            this.allocationsTreeListControl1.VisibleStackHashKeys = ((System.Collections.Generic.List<uint>)(resources.GetObject( "allocationsTreeListControl1.VisibleStackHashKeys" )));
            this.allocationsTreeListControl1.VisibleStacks = MemTrackerControls.AllocationsTreeListControl.SelectedStacks.All;
            this.allocationsTreeListControl1.VisibleStackTypeNames = ((System.Collections.Generic.List<string>)(resources.GetObject( "allocationsTreeListControl1.VisibleStackTypeNames" )));
            this.allocationsTreeListControl1.OutOfMemoryError += new MemTrackerControls.ErrorEventHandler( this.allocationsTreeListControl1_OutOfMemoryError );
            this.allocationsTreeListControl1.UpdateComplete += new MemTrackerControls.UpdateCompleteEventHandler( this.MemTrackerControl_UpdateComplete );
            this.allocationsTreeListControl1.UpdateBegin += new MemTrackerControls.UpdateBeginEventHandler( this.allocationsTreeListControl1_UpdateBegin );
            // 
            // memoryLayoutDockableWindow1
            // 
            this.memoryLayoutDockableWindow1.CloseAction = TD.SandDock.DockControlCloseAction.Dispose;
            this.memoryLayoutDockableWindow1.Controls.Add( this.memoryLayoutControl1 );
            this.memoryLayoutDockableWindow1.FloatingSize = new System.Drawing.Size( 1000, 600 );
            this.memoryLayoutDockableWindow1.Guid = new System.Guid( "994924b4-25b2-40a4-8f18-a83b3d904d0f" );
            this.memoryLayoutDockableWindow1.Location = new System.Drawing.Point( 0, 0 );
            this.memoryLayoutDockableWindow1.Name = "memoryLayoutDockableWindow1";
            this.memoryLayoutDockableWindow1.ShowOptions = false;
            this.memoryLayoutDockableWindow1.Size = new System.Drawing.Size( 843, 346 );
            this.memoryLayoutDockableWindow1.TabImage = global::MemTracker.Properties.Resources.MemoryLayoutWindow;
            this.memoryLayoutDockableWindow1.TabIndex = 0;
            this.memoryLayoutDockableWindow1.Text = "Memory Layout (1)";
            // 
            // memoryLayoutControl1
            // 
            this.memoryLayoutControl1.BytesPerPixel = MemTrackerControls.MemoryLayoutControl.Bytes.OneHundredTwentyEight;
            this.memoryLayoutControl1.ColorizeBy = MemTrackerControls.MemoryLayoutControl.DisplayType.StackNames;
            this.memoryLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.memoryLayoutControl1.Location = new System.Drawing.Point( 0, 0 );
            this.memoryLayoutControl1.Name = "memoryLayoutControl1";
            this.memoryLayoutControl1.ParentControl = this;
            this.memoryLayoutControl1.PixelWidth = MemTrackerControls.MemoryLayoutControl.Widths.TwoHundredFiftySix;
            this.memoryLayoutControl1.RefeshButtonEnabled = false;
            this.memoryLayoutControl1.Report = null;
            this.memoryLayoutControl1.ShowSizingGrip = false;
            this.memoryLayoutControl1.Size = new System.Drawing.Size( 843, 346 );
            this.memoryLayoutControl1.SizeScale = MemTrackerControls.MemoryLayoutControl.Sizes.Medium;
            this.memoryLayoutControl1.TabIndex = 0;
            this.memoryLayoutControl1.ThreadingEnabled = true;
            this.memoryLayoutControl1.Tracker = this.trackerComponent1;
            this.memoryLayoutControl1.UpdateComplete += new MemTrackerControls.UpdateCompleteEventHandler( this.MemTrackerControl_UpdateComplete );
            this.memoryLayoutControl1.UpdateBegin += new MemTrackerControls.UpdateBeginEventHandler( this.memoryLayoutControl1_UpdateBegin );
            // 
            // sandDockManager1
            // 
            this.sandDockManager1.DockSystemContainer = this.toolStripContainer1.ContentPanel;
            this.sandDockManager1.MaximumDockContainerSize = 900;
            this.sandDockManager1.MinimumDockContainerSize = 100;
            this.sandDockManager1.OwnerForm = this;
            this.sandDockManager1.SerializeTabbedDocuments = true;
            this.sandDockManager1.ResolveDockControl += new TD.SandDock.ResolveDockControlEventHandler( this.sandDockManager1_ResolveDockControl );
            // 
            // toolStripContainer1
            // 
            // 
            // toolStripContainer1.BottomToolStripPanel
            // 
            this.toolStripContainer1.BottomToolStripPanel.Controls.Add( this.statusStrip1 );
            // 
            // toolStripContainer1.ContentPanel
            // 
            this.toolStripContainer1.ContentPanel.Controls.Add( dockContainer1 );
            this.toolStripContainer1.ContentPanel.Controls.Add( dockContainer2 );
            this.toolStripContainer1.ContentPanel.Size = new System.Drawing.Size( 1200, 529 );
            this.toolStripContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.toolStripContainer1.Location = new System.Drawing.Point( 0, 0 );
            this.toolStripContainer1.Name = "toolStripContainer1";
            this.toolStripContainer1.Size = new System.Drawing.Size( 1200, 600 );
            this.toolStripContainer1.TabIndex = 0;
            this.toolStripContainer1.Text = "toolStripContainer1";
            // 
            // toolStripContainer1.TopToolStripPanel
            // 
            this.toolStripContainer1.TopToolStripPanel.Controls.Add( this.menuStrip1 );
            this.toolStripContainer1.TopToolStripPanel.Controls.Add( this.newWindowsToolStrip );
            this.toolStripContainer1.TopToolStripPanel.Controls.Add( this.playbackToolStrip );
            // 
            // statusStrip1
            // 
            this.statusStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.statusStrip1.Items.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.messageCountToolStripStatusLabel} );
            this.statusStrip1.Location = new System.Drawing.Point( 0, 0 );
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size( 1200, 22 );
            this.statusStrip1.TabIndex = 0;
            // 
            // messageCountToolStripStatusLabel
            // 
            this.messageCountToolStripStatusLabel.AutoSize = false;
            this.messageCountToolStripStatusLabel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.messageCountToolStripStatusLabel.Name = "messageCountToolStripStatusLabel";
            this.messageCountToolStripStatusLabel.Size = new System.Drawing.Size( 145, 17 );
            this.messageCountToolStripStatusLabel.Text = "Message Count:";
            this.messageCountToolStripStatusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // dockContainer2
            // 
            dockContainer2.ContentSize = 139;
            dockContainer2.Controls.Add( this.messagesDockableWindow );
            dockContainer2.Dock = System.Windows.Forms.DockStyle.Bottom;
            dockContainer2.LayoutSystem = new TD.SandDock.SplitLayoutSystem( new System.Drawing.SizeF( 1200F, 139F ), System.Windows.Forms.Orientation.Vertical, new TD.SandDock.LayoutSystemBase[] {
            ((TD.SandDock.LayoutSystemBase)(new TD.SandDock.ControlLayoutSystem(new System.Drawing.SizeF(1200F, 139F), new TD.SandDock.DockControl[] {
                        ((TD.SandDock.DockControl)(this.messagesDockableWindow))}, this.messagesDockableWindow)))} );
            dockContainer2.Location = new System.Drawing.Point( 0, 386 );
            dockContainer2.Manager = this.sandDockManager1;
            dockContainer2.Name = "dockContainer2";
            dockContainer2.Size = new System.Drawing.Size( 1200, 143 );
            dockContainer2.TabIndex = 1;
            // 
            // messagesDockableWindow
            // 
            this.messagesDockableWindow.AllowClose = false;
            this.messagesDockableWindow.Controls.Add( this.messagesListBox );
            this.messagesDockableWindow.FloatingSize = new System.Drawing.Size( 1000, 100 );
            this.messagesDockableWindow.Guid = new System.Guid( "12e2170c-85f5-4478-9d92-1c4b2b31ed16" );
            this.messagesDockableWindow.Location = new System.Drawing.Point( 0, 20 );
            this.messagesDockableWindow.Name = "messagesDockableWindow";
            this.messagesDockableWindow.ShowOptions = false;
            this.messagesDockableWindow.Size = new System.Drawing.Size( 1200, 99 );
            this.messagesDockableWindow.TabImage = global::MemTracker.Properties.Resources.Messages;
            this.messagesDockableWindow.TabIndex = 0;
            this.messagesDockableWindow.Text = "Messages";
            // 
            // messagesListBox
            // 
            this.messagesListBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.messagesListBox.FormattingEnabled = true;
            this.messagesListBox.Location = new System.Drawing.Point( 0, 0 );
            this.messagesListBox.Name = "messagesListBox";
            this.messagesListBox.SelectionMode = System.Windows.Forms.SelectionMode.None;
            this.messagesListBox.Size = new System.Drawing.Size( 1200, 95 );
            this.messagesListBox.TabIndex = 0;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.menuStrip1.Items.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.playbackToolStripMenuItem,
            this.toolsToolStripMenuItem,
            this.windowToolStripMenuItem,
            this.helpToolStripMenuItem} );
            this.menuStrip1.Location = new System.Drawing.Point( 0, 0 );
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size( 1200, 24 );
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.fileOpenToolStripMenuItem,
            this.fileCloseTrackerFileToolStripMenuItem,
            this.toolStripSeparator,
            this.fileSaveReportToolStripMenuItem,
            this.toolStripSeparator1,
            this.fileRecentFilesToolStripMenuItem,
            this.toolStripSeparator2,
            this.fileExitToolStripMenuItem} );
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size( 35, 20 );
            this.fileToolStripMenuItem.Text = "&File";
            this.fileToolStripMenuItem.DropDownOpening += new System.EventHandler( this.fileToolStripMenuItem_DropDownOpening );
            // 
            // fileOpenToolStripMenuItem
            // 
            this.fileOpenToolStripMenuItem.DropDownItems.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.fileOpenSingleTrackerFileToolStripMenuItem,
            this.fileOpenMultiToolStripMenuItem} );
            this.fileOpenToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject( "fileOpenToolStripMenuItem.Image" )));
            this.fileOpenToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.fileOpenToolStripMenuItem.Name = "fileOpenToolStripMenuItem";
            this.fileOpenToolStripMenuItem.Size = new System.Drawing.Size( 169, 22 );
            this.fileOpenToolStripMenuItem.Text = "&Open";
            // 
            // fileOpenSingleTrackerFileToolStripMenuItem
            // 
            this.fileOpenSingleTrackerFileToolStripMenuItem.Name = "fileOpenSingleTrackerFileToolStripMenuItem";
            this.fileOpenSingleTrackerFileToolStripMenuItem.Size = new System.Drawing.Size( 172, 22 );
            this.fileOpenSingleTrackerFileToolStripMenuItem.Text = "&Single Tracker File";
            this.fileOpenSingleTrackerFileToolStripMenuItem.Click += new System.EventHandler( this.fileOpenSingleTrackerFileToolStripMenuItem_Click );
            // 
            // fileOpenMultiToolStripMenuItem
            // 
            this.fileOpenMultiToolStripMenuItem.Name = "fileOpenMultiToolStripMenuItem";
            this.fileOpenMultiToolStripMenuItem.Size = new System.Drawing.Size( 172, 22 );
            this.fileOpenMultiToolStripMenuItem.Text = "&Tracker File Series";
            this.fileOpenMultiToolStripMenuItem.Click += new System.EventHandler( this.fileOpenMultiToolStripMenuItem_Click );
            // 
            // fileCloseTrackerFileToolStripMenuItem
            // 
            this.fileCloseTrackerFileToolStripMenuItem.Name = "fileCloseTrackerFileToolStripMenuItem";
            this.fileCloseTrackerFileToolStripMenuItem.Size = new System.Drawing.Size( 169, 22 );
            this.fileCloseTrackerFileToolStripMenuItem.Text = "&Close Tracker File";
            this.fileCloseTrackerFileToolStripMenuItem.Click += new System.EventHandler( this.fileCloseTrackerFileToolStripMenuItem_Click );
            // 
            // toolStripSeparator
            // 
            this.toolStripSeparator.Name = "toolStripSeparator";
            this.toolStripSeparator.Size = new System.Drawing.Size( 166, 6 );
            // 
            // fileSaveReportToolStripMenuItem
            // 
            this.fileSaveReportToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject( "fileSaveReportToolStripMenuItem.Image" )));
            this.fileSaveReportToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.fileSaveReportToolStripMenuItem.Name = "fileSaveReportToolStripMenuItem";
            this.fileSaveReportToolStripMenuItem.Size = new System.Drawing.Size( 169, 22 );
            this.fileSaveReportToolStripMenuItem.Text = "&Save Report";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size( 166, 6 );
            // 
            // fileRecentFilesToolStripMenuItem
            // 
            this.fileRecentFilesToolStripMenuItem.Name = "fileRecentFilesToolStripMenuItem";
            this.fileRecentFilesToolStripMenuItem.Size = new System.Drawing.Size( 169, 22 );
            this.fileRecentFilesToolStripMenuItem.Text = "&Recent Files";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size( 166, 6 );
            // 
            // fileExitToolStripMenuItem
            // 
            this.fileExitToolStripMenuItem.Name = "fileExitToolStripMenuItem";
            this.fileExitToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F4)));
            this.fileExitToolStripMenuItem.Size = new System.Drawing.Size( 169, 22 );
            this.fileExitToolStripMenuItem.Text = "E&xit";
            this.fileExitToolStripMenuItem.Click += new System.EventHandler( this.fileExitToolStripMenuItem_Click );
            // 
            // playbackToolStripMenuItem
            // 
            this.playbackToolStripMenuItem.DropDownItems.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.playbackSkipBackwardToolStripMenuItem,
            this.playbackStepBackwardToolStripMenuItem,
            this.playbackPauseToolStripMenuItem,
            this.playbackPlayToolStripMenuItem,
            this.playbackStepForwardToolStripMenuItem,
            this.playbackSkipForwardToolStripMenuItem,
            this.toolStripSeparator3,
            this.playbackGoToToolStripMenuItem,
            this.playbackGoToAgainToolStripMenuItem} );
            this.playbackToolStripMenuItem.Name = "playbackToolStripMenuItem";
            this.playbackToolStripMenuItem.Size = new System.Drawing.Size( 61, 20 );
            this.playbackToolStripMenuItem.Text = "&Playback";
            this.playbackToolStripMenuItem.DropDownOpening += new System.EventHandler( this.playbackToolStripMenuItem_DropDownOpening );
            // 
            // playbackSkipBackwardToolStripMenuItem
            // 
            this.playbackSkipBackwardToolStripMenuItem.Image = global::MemTracker.Properties.Resources.SkipBackward;
            this.playbackSkipBackwardToolStripMenuItem.Name = "playbackSkipBackwardToolStripMenuItem";
            this.playbackSkipBackwardToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift)
                        | System.Windows.Forms.Keys.B)));
            this.playbackSkipBackwardToolStripMenuItem.Size = new System.Drawing.Size( 221, 22 );
            this.playbackSkipBackwardToolStripMenuItem.Tag = "SkipBackward";
            this.playbackSkipBackwardToolStripMenuItem.Text = "Skip Backward";
            this.playbackSkipBackwardToolStripMenuItem.Click += new System.EventHandler( this.appActionToolStripMenuItem_Click );
            // 
            // playbackStepBackwardToolStripMenuItem
            // 
            this.playbackStepBackwardToolStripMenuItem.Image = global::MemTracker.Properties.Resources.StepBackward;
            this.playbackStepBackwardToolStripMenuItem.Name = "playbackStepBackwardToolStripMenuItem";
            this.playbackStepBackwardToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.B)));
            this.playbackStepBackwardToolStripMenuItem.Size = new System.Drawing.Size( 221, 22 );
            this.playbackStepBackwardToolStripMenuItem.Tag = "StepBackward";
            this.playbackStepBackwardToolStripMenuItem.Text = "Step Backward";
            this.playbackStepBackwardToolStripMenuItem.Click += new System.EventHandler( this.appActionToolStripMenuItem_Click );
            // 
            // playbackPauseToolStripMenuItem
            // 
            this.playbackPauseToolStripMenuItem.Image = global::MemTracker.Properties.Resources.Pause;
            this.playbackPauseToolStripMenuItem.Name = "playbackPauseToolStripMenuItem";
            this.playbackPauseToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.P)));
            this.playbackPauseToolStripMenuItem.Size = new System.Drawing.Size( 221, 22 );
            this.playbackPauseToolStripMenuItem.Tag = "Pause";
            this.playbackPauseToolStripMenuItem.Text = "Pause";
            this.playbackPauseToolStripMenuItem.Click += new System.EventHandler( this.appActionToolStripMenuItem_Click );
            // 
            // playbackPlayToolStripMenuItem
            // 
            this.playbackPlayToolStripMenuItem.Image = global::MemTracker.Properties.Resources.Play;
            this.playbackPlayToolStripMenuItem.Name = "playbackPlayToolStripMenuItem";
            this.playbackPlayToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.P)));
            this.playbackPlayToolStripMenuItem.Size = new System.Drawing.Size( 221, 22 );
            this.playbackPlayToolStripMenuItem.Tag = "Play";
            this.playbackPlayToolStripMenuItem.Text = "Play";
            this.playbackPlayToolStripMenuItem.Click += new System.EventHandler( this.appActionToolStripMenuItem_Click );
            // 
            // playbackStepForwardToolStripMenuItem
            // 
            this.playbackStepForwardToolStripMenuItem.Image = global::MemTracker.Properties.Resources.StepForward;
            this.playbackStepForwardToolStripMenuItem.Name = "playbackStepForwardToolStripMenuItem";
            this.playbackStepForwardToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F)));
            this.playbackStepForwardToolStripMenuItem.Size = new System.Drawing.Size( 221, 22 );
            this.playbackStepForwardToolStripMenuItem.Tag = "StepForward";
            this.playbackStepForwardToolStripMenuItem.Text = "Step Forward";
            this.playbackStepForwardToolStripMenuItem.Click += new System.EventHandler( this.appActionToolStripMenuItem_Click );
            // 
            // playbackSkipForwardToolStripMenuItem
            // 
            this.playbackSkipForwardToolStripMenuItem.Image = global::MemTracker.Properties.Resources.SkipForward;
            this.playbackSkipForwardToolStripMenuItem.Name = "playbackSkipForwardToolStripMenuItem";
            this.playbackSkipForwardToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift)
                        | System.Windows.Forms.Keys.F)));
            this.playbackSkipForwardToolStripMenuItem.Size = new System.Drawing.Size( 221, 22 );
            this.playbackSkipForwardToolStripMenuItem.Tag = "SkipForward";
            this.playbackSkipForwardToolStripMenuItem.Text = "Skip Forward";
            this.playbackSkipForwardToolStripMenuItem.Click += new System.EventHandler( this.appActionToolStripMenuItem_Click );
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size( 218, 6 );
            // 
            // playbackGoToToolStripMenuItem
            // 
            this.playbackGoToToolStripMenuItem.Name = "playbackGoToToolStripMenuItem";
            this.playbackGoToToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.G)));
            this.playbackGoToToolStripMenuItem.Size = new System.Drawing.Size( 221, 22 );
            this.playbackGoToToolStripMenuItem.Text = "&Go To...";
            this.playbackGoToToolStripMenuItem.Click += new System.EventHandler( this.playbackGoToToolStripMenuItem_Click );
            // 
            // playbackGoToAgainToolStripMenuItem
            // 
            this.playbackGoToAgainToolStripMenuItem.Name = "playbackGoToAgainToolStripMenuItem";
            this.playbackGoToAgainToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F4;
            this.playbackGoToAgainToolStripMenuItem.Size = new System.Drawing.Size( 221, 22 );
            this.playbackGoToAgainToolStripMenuItem.Text = "Go To &Again";
            this.playbackGoToAgainToolStripMenuItem.Click += new System.EventHandler( this.playbackGoToAgainToolStripMenuItem_Click );
            // 
            // toolsToolStripMenuItem
            // 
            this.toolsToolStripMenuItem.DropDownItems.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.toolsCustomizeToolStripMenuItem,
            this.toolsOptionsToolStripMenuItem} );
            this.toolsToolStripMenuItem.Name = "toolsToolStripMenuItem";
            this.toolsToolStripMenuItem.Size = new System.Drawing.Size( 44, 20 );
            this.toolsToolStripMenuItem.Text = "&Tools";
            // 
            // toolsCustomizeToolStripMenuItem
            // 
            this.toolsCustomizeToolStripMenuItem.Name = "toolsCustomizeToolStripMenuItem";
            this.toolsCustomizeToolStripMenuItem.Size = new System.Drawing.Size( 134, 22 );
            this.toolsCustomizeToolStripMenuItem.Text = "&Customize";
            this.toolsCustomizeToolStripMenuItem.Click += new System.EventHandler( this.toolsCustomizeToolStripMenuItem_Click );
            // 
            // toolsOptionsToolStripMenuItem
            // 
            this.toolsOptionsToolStripMenuItem.Name = "toolsOptionsToolStripMenuItem";
            this.toolsOptionsToolStripMenuItem.Size = new System.Drawing.Size( 134, 22 );
            this.toolsOptionsToolStripMenuItem.Text = "&Options";
            this.toolsOptionsToolStripMenuItem.Click += new System.EventHandler( this.toolsOptionsToolStripMenuItem_Click );
            // 
            // windowToolStripMenuItem
            // 
            this.windowToolStripMenuItem.DropDownItems.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.windowNewOverviewWindowToolStripMenuItem,
            this.windowNewAllocationsWindowToolStripMenuItem,
            this.windowNewMemoryLayoutWindowToolStripMenuItem,
            this.toolStripSeparator4,
            this.viewErrorsToolStripMenuItem} );
            this.windowToolStripMenuItem.Name = "windowToolStripMenuItem";
            this.windowToolStripMenuItem.Size = new System.Drawing.Size( 57, 20 );
            this.windowToolStripMenuItem.Text = "&Window";
            // 
            // windowNewOverviewWindowToolStripMenuItem
            // 
            this.windowNewOverviewWindowToolStripMenuItem.Image = global::MemTracker.Properties.Resources.NewOverviewWindow;
            this.windowNewOverviewWindowToolStripMenuItem.Name = "windowNewOverviewWindowToolStripMenuItem";
            this.windowNewOverviewWindowToolStripMenuItem.Size = new System.Drawing.Size( 224, 22 );
            this.windowNewOverviewWindowToolStripMenuItem.Tag = "NewOverviewWindow";
            this.windowNewOverviewWindowToolStripMenuItem.Text = "New &Overview Window";
            this.windowNewOverviewWindowToolStripMenuItem.Click += new System.EventHandler( this.appActionToolStripMenuItem_Click );
            // 
            // windowNewAllocationsWindowToolStripMenuItem
            // 
            this.windowNewAllocationsWindowToolStripMenuItem.Image = global::MemTracker.Properties.Resources.NewAllocationsWindow;
            this.windowNewAllocationsWindowToolStripMenuItem.Name = "windowNewAllocationsWindowToolStripMenuItem";
            this.windowNewAllocationsWindowToolStripMenuItem.Size = new System.Drawing.Size( 224, 22 );
            this.windowNewAllocationsWindowToolStripMenuItem.Tag = "NewAllocationsWindow";
            this.windowNewAllocationsWindowToolStripMenuItem.Text = "New &Allocations Window";
            this.windowNewAllocationsWindowToolStripMenuItem.Click += new System.EventHandler( this.appActionToolStripMenuItem_Click );
            // 
            // windowNewMemoryLayoutWindowToolStripMenuItem
            // 
            this.windowNewMemoryLayoutWindowToolStripMenuItem.Image = global::MemTracker.Properties.Resources.NewMemoryLayoutWindow;
            this.windowNewMemoryLayoutWindowToolStripMenuItem.Name = "windowNewMemoryLayoutWindowToolStripMenuItem";
            this.windowNewMemoryLayoutWindowToolStripMenuItem.Size = new System.Drawing.Size( 224, 22 );
            this.windowNewMemoryLayoutWindowToolStripMenuItem.Tag = "NewMemoryLayoutWindow";
            this.windowNewMemoryLayoutWindowToolStripMenuItem.Text = "New &Memory Layout Window";
            this.windowNewMemoryLayoutWindowToolStripMenuItem.Click += new System.EventHandler( this.appActionToolStripMenuItem_Click );
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size( 221, 6 );
            // 
            // viewErrorsToolStripMenuItem
            // 
            this.viewErrorsToolStripMenuItem.Name = "viewErrorsToolStripMenuItem";
            this.viewErrorsToolStripMenuItem.Size = new System.Drawing.Size( 224, 22 );
            this.viewErrorsToolStripMenuItem.Text = "View &Errors";
            this.viewErrorsToolStripMenuItem.Click += new System.EventHandler( this.viewErrorsToolStripMenuItem_Click );
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.helpContentsToolStripMenuItem,
            this.toolStripSeparator5,
            this.helpAboutToolStripMenuItem} );
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size( 40, 20 );
            this.helpToolStripMenuItem.Text = "&Help";
            // 
            // helpContentsToolStripMenuItem
            // 
            this.helpContentsToolStripMenuItem.Name = "helpContentsToolStripMenuItem";
            this.helpContentsToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F1;
            this.helpContentsToolStripMenuItem.Size = new System.Drawing.Size( 148, 22 );
            this.helpContentsToolStripMenuItem.Text = "&Contents";
            this.helpContentsToolStripMenuItem.Click += new System.EventHandler( this.helpContentsToolStripMenuItem_Click );
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size( 145, 6 );
            // 
            // helpAboutToolStripMenuItem
            // 
            this.helpAboutToolStripMenuItem.Name = "helpAboutToolStripMenuItem";
            this.helpAboutToolStripMenuItem.Size = new System.Drawing.Size( 148, 22 );
            this.helpAboutToolStripMenuItem.Text = "&About...";
            this.helpAboutToolStripMenuItem.Click += new System.EventHandler( this.helpAboutToolStripMenuItem_Click );
            // 
            // newWindowsToolStrip
            // 
            this.newWindowsToolStrip.Dock = System.Windows.Forms.DockStyle.None;
            this.newWindowsToolStrip.Items.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.newOverviewWindowToolStripButton,
            this.newAllocationsWindowToolStripButton,
            this.newMemoryLayoutWindowToolStripButton} );
            this.newWindowsToolStrip.Location = new System.Drawing.Point( 3, 24 );
            this.newWindowsToolStrip.Name = "newWindowsToolStrip";
            this.newWindowsToolStrip.Size = new System.Drawing.Size( 79, 25 );
            this.newWindowsToolStrip.TabIndex = 1;
            // 
            // newOverviewWindowToolStripButton
            // 
            this.newOverviewWindowToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.newOverviewWindowToolStripButton.Image = global::MemTracker.Properties.Resources.NewOverviewWindow;
            this.newOverviewWindowToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.newOverviewWindowToolStripButton.Name = "newOverviewWindowToolStripButton";
            this.newOverviewWindowToolStripButton.Size = new System.Drawing.Size( 23, 22 );
            this.newOverviewWindowToolStripButton.Tag = "NewOverviewWindow";
            this.newOverviewWindowToolStripButton.Text = "New Overview Window";
            this.newOverviewWindowToolStripButton.ToolTipText = "New Overview Window";
            this.newOverviewWindowToolStripButton.Click += new System.EventHandler( this.appActionToolStripMenuItem_Click );
            // 
            // newAllocationsWindowToolStripButton
            // 
            this.newAllocationsWindowToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.newAllocationsWindowToolStripButton.Image = global::MemTracker.Properties.Resources.NewAllocationsWindow;
            this.newAllocationsWindowToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.newAllocationsWindowToolStripButton.Name = "newAllocationsWindowToolStripButton";
            this.newAllocationsWindowToolStripButton.Size = new System.Drawing.Size( 23, 22 );
            this.newAllocationsWindowToolStripButton.Tag = "NewAllocationsWindow";
            this.newAllocationsWindowToolStripButton.Text = "New Allocations Window";
            this.newAllocationsWindowToolStripButton.ToolTipText = "New Allocations Window";
            this.newAllocationsWindowToolStripButton.Click += new System.EventHandler( this.appActionToolStripMenuItem_Click );
            // 
            // newMemoryLayoutWindowToolStripButton
            // 
            this.newMemoryLayoutWindowToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.newMemoryLayoutWindowToolStripButton.Image = global::MemTracker.Properties.Resources.NewMemoryLayoutWindow;
            this.newMemoryLayoutWindowToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.newMemoryLayoutWindowToolStripButton.Name = "newMemoryLayoutWindowToolStripButton";
            this.newMemoryLayoutWindowToolStripButton.Size = new System.Drawing.Size( 23, 22 );
            this.newMemoryLayoutWindowToolStripButton.Tag = "NewMemoryLayoutWindow";
            this.newMemoryLayoutWindowToolStripButton.Text = "New Memory Layout Window";
            this.newMemoryLayoutWindowToolStripButton.ToolTipText = "New Memory Layout Window";
            this.newMemoryLayoutWindowToolStripButton.Click += new System.EventHandler( this.appActionToolStripMenuItem_Click );
            // 
            // playbackToolStrip
            // 
            this.playbackToolStrip.Dock = System.Windows.Forms.DockStyle.None;
            this.playbackToolStrip.Items.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.skipBackwardToolStripButton,
            this.stepBackwardToolStripButton,
            this.pauseToolStripButton,
            this.playToolStripButton,
            this.stepForwardToolStripButton,
            this.skipForwardToolStripButton} );
            this.playbackToolStrip.Location = new System.Drawing.Point( 82, 24 );
            this.playbackToolStrip.Name = "playbackToolStrip";
            this.playbackToolStrip.Size = new System.Drawing.Size( 148, 25 );
            this.playbackToolStrip.TabIndex = 2;
            // 
            // skipBackwardToolStripButton
            // 
            this.skipBackwardToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.skipBackwardToolStripButton.Image = global::MemTracker.Properties.Resources.SkipBackward;
            this.skipBackwardToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.skipBackwardToolStripButton.Name = "skipBackwardToolStripButton";
            this.skipBackwardToolStripButton.Size = new System.Drawing.Size( 23, 22 );
            this.skipBackwardToolStripButton.Tag = "SkipBackward";
            this.skipBackwardToolStripButton.Text = "Skip Backward";
            this.skipBackwardToolStripButton.Click += new System.EventHandler( this.appActionToolStripMenuItem_Click );
            // 
            // stepBackwardToolStripButton
            // 
            this.stepBackwardToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.stepBackwardToolStripButton.Image = global::MemTracker.Properties.Resources.StepBackward;
            this.stepBackwardToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.stepBackwardToolStripButton.Name = "stepBackwardToolStripButton";
            this.stepBackwardToolStripButton.Size = new System.Drawing.Size( 23, 22 );
            this.stepBackwardToolStripButton.Tag = "StepBackward";
            this.stepBackwardToolStripButton.Text = "Step Backward";
            this.stepBackwardToolStripButton.Click += new System.EventHandler( this.appActionToolStripMenuItem_Click );
            // 
            // pauseToolStripButton
            // 
            this.pauseToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.pauseToolStripButton.Image = global::MemTracker.Properties.Resources.Pause;
            this.pauseToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.pauseToolStripButton.Name = "pauseToolStripButton";
            this.pauseToolStripButton.Size = new System.Drawing.Size( 23, 22 );
            this.pauseToolStripButton.Tag = "Pause";
            this.pauseToolStripButton.Text = "Pause";
            this.pauseToolStripButton.Click += new System.EventHandler( this.appActionToolStripMenuItem_Click );
            // 
            // playToolStripButton
            // 
            this.playToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.playToolStripButton.Image = global::MemTracker.Properties.Resources.Play;
            this.playToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.playToolStripButton.Name = "playToolStripButton";
            this.playToolStripButton.Size = new System.Drawing.Size( 23, 22 );
            this.playToolStripButton.Tag = "Play";
            this.playToolStripButton.Text = "Play";
            this.playToolStripButton.Click += new System.EventHandler( this.appActionToolStripMenuItem_Click );
            // 
            // stepForwardToolStripButton
            // 
            this.stepForwardToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.stepForwardToolStripButton.Image = global::MemTracker.Properties.Resources.StepForward;
            this.stepForwardToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.stepForwardToolStripButton.Name = "stepForwardToolStripButton";
            this.stepForwardToolStripButton.Size = new System.Drawing.Size( 23, 22 );
            this.stepForwardToolStripButton.Tag = "StepForward";
            this.stepForwardToolStripButton.Text = "Step Forward";
            this.stepForwardToolStripButton.Click += new System.EventHandler( this.appActionToolStripMenuItem_Click );
            // 
            // skipForwardToolStripButton
            // 
            this.skipForwardToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.skipForwardToolStripButton.Image = global::MemTracker.Properties.Resources.SkipForward;
            this.skipForwardToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.skipForwardToolStripButton.Name = "skipForwardToolStripButton";
            this.skipForwardToolStripButton.Size = new System.Drawing.Size( 23, 22 );
            this.skipForwardToolStripButton.Tag = "SkipForward";
            this.skipForwardToolStripButton.Text = "Skip Forward";
            this.skipForwardToolStripButton.Click += new System.EventHandler( this.appActionToolStripMenuItem_Click );
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";
            this.openFileDialog1.Title = "Open Single Tracker File";
            // 
            // folderBrowserDialog1
            // 
            this.folderBrowserDialog1.Description = "Select the folder containing the tracker file series";
            this.folderBrowserDialog1.ShowNewFolderButton = false;
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";
            this.saveFileDialog1.Title = "Save Tracker File";
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler( this.timer1_Tick );
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size( 1200, 600 );
            this.Controls.Add( this.toolStripContainer1 );
            this.Icon = ((System.Drawing.Icon)(resources.GetObject( "$this.Icon" )));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "MemTracker";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler( this.Form1_FormClosing );
            this.Load += new System.EventHandler( this.Form1_Load );
            dockContainer1.ResumeLayout( false );
            this.overviewDockableWindow1.ResumeLayout( false );
            this.allocationsDockableWindow1.ResumeLayout( false );
            this.memoryLayoutDockableWindow1.ResumeLayout( false );
            this.toolStripContainer1.BottomToolStripPanel.ResumeLayout( false );
            this.toolStripContainer1.BottomToolStripPanel.PerformLayout();
            this.toolStripContainer1.ContentPanel.ResumeLayout( false );
            this.toolStripContainer1.TopToolStripPanel.ResumeLayout( false );
            this.toolStripContainer1.TopToolStripPanel.PerformLayout();
            this.toolStripContainer1.ResumeLayout( false );
            this.toolStripContainer1.PerformLayout();
            this.statusStrip1.ResumeLayout( false );
            this.statusStrip1.PerformLayout();
            dockContainer2.ResumeLayout( false );
            this.messagesDockableWindow.ResumeLayout( false );
            this.menuStrip1.ResumeLayout( false );
            this.menuStrip1.PerformLayout();
            this.newWindowsToolStrip.ResumeLayout( false );
            this.newWindowsToolStrip.PerformLayout();
            this.playbackToolStrip.ResumeLayout( false );
            this.playbackToolStrip.PerformLayout();
            this.ResumeLayout( false );

        }

        #endregion

        private System.Windows.Forms.ToolStripContainer toolStripContainer1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStrip newWindowsToolStrip;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fileOpenToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator;
        private System.Windows.Forms.ToolStripMenuItem fileExitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolsCustomizeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolsOptionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpContentsToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripMenuItem helpAboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fileSaveReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem fileOpenSingleTrackerFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fileOpenMultiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem windowToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem windowNewOverviewWindowToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem windowNewAllocationsWindowToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem windowNewMemoryLayoutWindowToolStripMenuItem;
        private MemTrackerControls.TrackerComponent trackerComponent1;
        private System.Windows.Forms.ToolStripButton newOverviewWindowToolStripButton;
        private System.Windows.Forms.ToolStripButton newAllocationsWindowToolStripButton;
        private System.Windows.Forms.ToolStripButton newMemoryLayoutWindowToolStripButton;
        private System.Windows.Forms.ToolStrip playbackToolStrip;
        private System.Windows.Forms.ToolStripButton skipBackwardToolStripButton;
        private System.Windows.Forms.ToolStripButton stepBackwardToolStripButton;
        private System.Windows.Forms.ToolStripButton pauseToolStripButton;
        private System.Windows.Forms.ToolStripButton playToolStripButton;
        private System.Windows.Forms.ToolStripButton stepForwardToolStripButton;
        private System.Windows.Forms.ToolStripButton skipForwardToolStripButton;
        private System.Windows.Forms.ToolStripMenuItem playbackToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem playbackSkipBackwardToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem playbackStepBackwardToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem playbackPauseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem playbackPlayToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem playbackStepForwardToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem playbackSkipForwardToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem playbackGoToToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fileCloseTrackerFileToolStripMenuItem;
        private TD.SandDock.DockableWindow overviewDockableWindow1;
        private TD.SandDock.SandDockManager sandDockManager1;
        private MemTrackerControls.OverviewListControl overviewListControl1;
        private TD.SandDock.DockableWindow allocationsDockableWindow1;
        private MemTrackerControls.AllocationsTreeListControl allocationsTreeListControl1;
        private TD.SandDock.DockableWindow memoryLayoutDockableWindow1;
        private MemTrackerControls.MemoryLayoutControl memoryLayoutControl1;
        private TD.SandDock.DockableWindow messagesDockableWindow;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.ListBox messagesListBox;
        private System.Windows.Forms.ToolStripMenuItem fileRecentFilesToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripStatusLabel messageCountToolStripStatusLabel;
        private System.Windows.Forms.ToolStripMenuItem playbackGoToAgainToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem viewErrorsToolStripMenuItem;
    }
}

