using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace MemTracker
{
    public partial class LoadMultiTrackerFileForm : Form
    {
        protected LoadMultiTrackerFileForm()
        {
            InitializeComponent();
        }

        public LoadMultiTrackerFileForm( string directory )
        {
            InitializeComponent();

            string[] filenames = Directory.GetFiles( directory );
            foreach ( string filename in filenames )
            {
                this.filenamesListBox.Items.Add( Path.GetFileName( filename ) );
            }
        }

        #region Properties
        public string Prefix
        {
            get
            {
                return m_prefix;
            }
        }

        public string Postfix
        {
            get
            {
                return m_postfix;
            }
        }
        #endregion

        #region Variables
        private string m_prefix = string.Empty;
        private string m_postfix = string.Empty;

        private Regex m_filenameRegex = new Regex( "(?<prefix>\\S+)([0-9]{4,4})(?<postfix>\\S+)", RegexOptions.Compiled );
        #endregion

        #region Event Handlers
        private void prefixTextBox_TextChanged( object sender, EventArgs e )
        {
            string text = this.prefixTextBox.Text.Trim();
            if ( text.IndexOfAny( Path.GetInvalidFileNameChars() ) > -1 )
            {
                this.prefixTextBox.Text = m_prefix;
            }
            else
            {
                m_prefix = text;
                ValidateTextBoxes();
            }
        }

        private void postfixTextBox_TextChanged( object sender, EventArgs e )
        {
            string text = this.postfixTextBox.Text.Trim();
            if ( text.IndexOfAny( Path.GetInvalidFileNameChars() ) > -1 )
            {
                this.postfixTextBox.Text = m_postfix;
            }
            else
            {
                m_postfix = text;
                ValidateTextBoxes();
            }
        }

        private void filenamesListBox_SelectedIndexChanged( object sender, EventArgs e )
        {
            object obj = this.filenamesListBox.SelectedItem;
            if ( obj != null )
            {
                string filename = obj as string;

                // look for a pattern of 4 integers
                Match m = m_filenameRegex.Match( filename );
                if ( m.Success )
                {
                    string prefix = m.Result( "${prefix}" );
                    string postfix = m.Result( "${postfix}" );

                    this.prefixTextBox.Text = prefix;
                    this.postfixTextBox.Text = postfix;
                }
            }
        }
        #endregion

        #region Private Functions
        private void ValidateTextBoxes()
        {
            string text = this.prefixTextBox.Text.Trim();
            if ( text == string.Empty )
            {
                this.okButton.Enabled = false;
            }
            else
            {
                text = this.postfixTextBox.Text.Trim();
                if ( text == string.Empty )
                {
                    this.okButton.Enabled = false;
                }
                else
                {
                    this.okButton.Enabled = true;
                }
            }

            this.exampleTextBox.Text = Form1.BuildTrackerFilename( this.Prefix, this.Postfix, 42 );
        }
        #endregion
    }
}