using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace MemTracker
{
    public partial class OptionsForm : Form
    {
        protected OptionsForm()
        {
            InitializeComponent();
        }

        public OptionsForm( bool loadLastFile, decimal playbackRate, int skipSize, EventHandler clearRecentFiles )
        {
            InitializeComponent();

            this.loadLastFileCheckBox.Checked = loadLastFile;

            if ( playbackRate > 0 )
            {
                m_playbackRate = 1000.0M / playbackRate;
            }
            else
            {
                m_playbackRate = this.playbackRateNumericUpDown.Minimum;
            }

            this.playbackRateNumericUpDown.Value = m_playbackRate;

            if ( this.playbackRateNumericUpDown.Value >= 1000 )
            {
                this.playbackRateNumericUpDown.Increment = 1000;
            }
            else
            {
                this.playbackRateNumericUpDown.Increment = 10;
            }

            if ( skipSize > 0 )
            {
                this.skipSizeNumericUpDown.Value = skipSize;
            }
            else
            {
                this.skipSizeNumericUpDown.Value = this.skipSizeNumericUpDown.Minimum;
            }

            if ( clearRecentFiles != null )
            {
                m_clearRecentFiles += clearRecentFiles;
            }
        }

        #region Properties
        public bool LoadLastFile
        {
            get
            {
                return this.loadLastFileCheckBox.Checked;
            }
        }

        public decimal PlaybackRate
        {
            get
            {
                return 1000.0M / m_playbackRate;
            }
        }

        public int SkipSize
        {
            get
            {
                return (int)this.skipSizeNumericUpDown.Value;
            }
        }
        #endregion

        #region Variables
        private decimal m_playbackRate = 1000;
        private EventHandler m_clearRecentFiles;
        #endregion

        #region Event Handlers
        private void clearRecentFilesButton_Click( object sender, EventArgs e )
        {
            if ( m_clearRecentFiles != null )
            {
                m_clearRecentFiles( this, new EventArgs() );
            }
        }
        #endregion

        private void playbackRateNumericUpDown_ValueChanged( object sender, EventArgs e )
        {
            if ( m_playbackRate != this.playbackRateNumericUpDown.Value )
            {
                bool incremented = this.playbackRateNumericUpDown.Value > m_playbackRate;
                if ( incremented )
                {
                    if ( this.playbackRateNumericUpDown.Value >= 1000 )
                    {
                        this.playbackRateNumericUpDown.Increment = 1000;
                    }
                }
                else
                {
                    if ( this.playbackRateNumericUpDown.Value < 1000 )
                    {
                        this.playbackRateNumericUpDown.Increment = 10;
                    }

                    if ( (m_playbackRate >= 1000) && (this.playbackRateNumericUpDown.Value < 1000) )
                    {
                        m_playbackRate = 990;
                        this.playbackRateNumericUpDown.Value = m_playbackRate;                        
                    }
                }

                m_playbackRate = this.playbackRateNumericUpDown.Value;
            }
        }
    }
}