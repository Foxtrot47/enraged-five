namespace MemTracker
{
    partial class LoadMultiTrackerFileForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && (components != null) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.okButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.filesInDirectoryGroupBox = new System.Windows.Forms.GroupBox();
            this.filenamesListBox = new System.Windows.Forms.ListBox();
            this.filenamePatternGroupBox = new System.Windows.Forms.GroupBox();
            this.exampleTextBox = new System.Windows.Forms.TextBox();
            this.exampleLabel = new System.Windows.Forms.Label();
            this.postfixTextBox = new System.Windows.Forms.TextBox();
            this.postfixLabel = new System.Windows.Forms.Label();
            this.prefixTextBox = new System.Windows.Forms.TextBox();
            this.prefixLabel = new System.Windows.Forms.Label();
            this.filesInDirectoryGroupBox.SuspendLayout();
            this.filenamePatternGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // okButton
            // 
            this.okButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.okButton.Enabled = false;
            this.okButton.Location = new System.Drawing.Point( 171, 261 );
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size( 75, 23 );
            this.okButton.TabIndex = 2;
            this.okButton.Text = "OK";
            this.okButton.UseVisualStyleBackColor = true;
            // 
            // cancelButton
            // 
            this.cancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.Location = new System.Drawing.Point( 252, 261 );
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size( 75, 23 );
            this.cancelButton.TabIndex = 3;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            // 
            // filesInDirectoryGroupBox
            // 
            this.filesInDirectoryGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.filesInDirectoryGroupBox.Controls.Add( this.filenamesListBox );
            this.filesInDirectoryGroupBox.Location = new System.Drawing.Point( 12, 12 );
            this.filesInDirectoryGroupBox.Name = "filesInDirectoryGroupBox";
            this.filesInDirectoryGroupBox.Size = new System.Drawing.Size( 315, 139 );
            this.filesInDirectoryGroupBox.TabIndex = 0;
            this.filesInDirectoryGroupBox.TabStop = false;
            this.filesInDirectoryGroupBox.Text = "Files in Directory";
            // 
            // filenamesListBox
            // 
            this.filenamesListBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.filenamesListBox.FormattingEnabled = true;
            this.filenamesListBox.Location = new System.Drawing.Point( 6, 23 );
            this.filenamesListBox.Name = "filenamesListBox";
            this.filenamesListBox.Size = new System.Drawing.Size( 303, 108 );
            this.filenamesListBox.Sorted = true;
            this.filenamesListBox.TabIndex = 0;
            this.filenamesListBox.SelectedIndexChanged += new System.EventHandler( this.filenamesListBox_SelectedIndexChanged );
            // 
            // filenamePatternGroupBox
            // 
            this.filenamePatternGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.filenamePatternGroupBox.Controls.Add( this.exampleTextBox );
            this.filenamePatternGroupBox.Controls.Add( this.exampleLabel );
            this.filenamePatternGroupBox.Controls.Add( this.postfixTextBox );
            this.filenamePatternGroupBox.Controls.Add( this.postfixLabel );
            this.filenamePatternGroupBox.Controls.Add( this.prefixTextBox );
            this.filenamePatternGroupBox.Controls.Add( this.prefixLabel );
            this.filenamePatternGroupBox.Location = new System.Drawing.Point( 12, 157 );
            this.filenamePatternGroupBox.Name = "filenamePatternGroupBox";
            this.filenamePatternGroupBox.Size = new System.Drawing.Size( 315, 98 );
            this.filenamePatternGroupBox.TabIndex = 1;
            this.filenamePatternGroupBox.TabStop = false;
            this.filenamePatternGroupBox.Text = "Specify the File Naming Convention";
            // 
            // exampleTextBox
            // 
            this.exampleTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.exampleTextBox.Location = new System.Drawing.Point( 59, 72 );
            this.exampleTextBox.Name = "exampleTextBox";
            this.exampleTextBox.ReadOnly = true;
            this.exampleTextBox.Size = new System.Drawing.Size( 250, 20 );
            this.exampleTextBox.TabIndex = 5;
            // 
            // exampleLabel
            // 
            this.exampleLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.exampleLabel.AutoSize = true;
            this.exampleLabel.Location = new System.Drawing.Point( 6, 75 );
            this.exampleLabel.Name = "exampleLabel";
            this.exampleLabel.Size = new System.Drawing.Size( 47, 13 );
            this.exampleLabel.TabIndex = 4;
            this.exampleLabel.Text = "Example";
            this.exampleLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // postfixTextBox
            // 
            this.postfixTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.postfixTextBox.Location = new System.Drawing.Point( 59, 46 );
            this.postfixTextBox.Name = "postfixTextBox";
            this.postfixTextBox.Size = new System.Drawing.Size( 250, 20 );
            this.postfixTextBox.TabIndex = 3;
            this.postfixTextBox.TextChanged += new System.EventHandler( this.postfixTextBox_TextChanged );
            // 
            // postfixLabel
            // 
            this.postfixLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.postfixLabel.AutoSize = true;
            this.postfixLabel.Location = new System.Drawing.Point( 15, 49 );
            this.postfixLabel.Name = "postfixLabel";
            this.postfixLabel.Size = new System.Drawing.Size( 38, 13 );
            this.postfixLabel.TabIndex = 2;
            this.postfixLabel.Text = "Postfix";
            this.postfixLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // prefixTextBox
            // 
            this.prefixTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.prefixTextBox.Location = new System.Drawing.Point( 59, 20 );
            this.prefixTextBox.Name = "prefixTextBox";
            this.prefixTextBox.Size = new System.Drawing.Size( 250, 20 );
            this.prefixTextBox.TabIndex = 1;
            this.prefixTextBox.TextChanged += new System.EventHandler( this.prefixTextBox_TextChanged );
            // 
            // prefixLabel
            // 
            this.prefixLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.prefixLabel.AutoSize = true;
            this.prefixLabel.Location = new System.Drawing.Point( 20, 23 );
            this.prefixLabel.Name = "prefixLabel";
            this.prefixLabel.Size = new System.Drawing.Size( 33, 13 );
            this.prefixLabel.TabIndex = 0;
            this.prefixLabel.Text = "Prefix";
            // 
            // LoadMultiTrackerFileForm
            // 
            this.AcceptButton = this.okButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cancelButton;
            this.ClientSize = new System.Drawing.Size( 339, 296 );
            this.Controls.Add( this.filenamePatternGroupBox );
            this.Controls.Add( this.filesInDirectoryGroupBox );
            this.Controls.Add( this.cancelButton );
            this.Controls.Add( this.okButton );
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "LoadMultiTrackerFileForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "Load Multi-tracker File";
            this.filesInDirectoryGroupBox.ResumeLayout( false );
            this.filenamePatternGroupBox.ResumeLayout( false );
            this.filenamePatternGroupBox.PerformLayout();
            this.ResumeLayout( false );

        }

        #endregion

        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.GroupBox filesInDirectoryGroupBox;
        private System.Windows.Forms.GroupBox filenamePatternGroupBox;
        private System.Windows.Forms.ListBox filenamesListBox;
        private System.Windows.Forms.TextBox exampleTextBox;
        private System.Windows.Forms.Label exampleLabel;
        private System.Windows.Forms.TextBox postfixTextBox;
        private System.Windows.Forms.Label postfixLabel;
        private System.Windows.Forms.TextBox prefixTextBox;
        private System.Windows.Forms.Label prefixLabel;

    }
}