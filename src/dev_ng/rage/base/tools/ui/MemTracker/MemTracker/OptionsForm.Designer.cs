namespace MemTracker
{
    partial class OptionsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && (components != null) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.fileGroupBox = new System.Windows.Forms.GroupBox();
            this.clearRecentFilesButton = new System.Windows.Forms.Button();
            this.loadLastFileCheckBox = new System.Windows.Forms.CheckBox();
            this.playbackGroupBox = new System.Windows.Forms.GroupBox();
            this.skipSizeUnitsLabel = new System.Windows.Forms.Label();
            this.skipSizeNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.skipSizeLabel = new System.Windows.Forms.Label();
            this.playbackRateUnitsLabel = new System.Windows.Forms.Label();
            this.playbackRateNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.playbackRateLabel = new System.Windows.Forms.Label();
            this.okButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.fileGroupBox.SuspendLayout();
            this.playbackGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.skipSizeNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.playbackRateNumericUpDown)).BeginInit();
            this.SuspendLayout();
            // 
            // fileGroupBox
            // 
            this.fileGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.fileGroupBox.Controls.Add( this.clearRecentFilesButton );
            this.fileGroupBox.Controls.Add( this.loadLastFileCheckBox );
            this.fileGroupBox.Location = new System.Drawing.Point( 12, 12 );
            this.fileGroupBox.Name = "fileGroupBox";
            this.fileGroupBox.Size = new System.Drawing.Size( 335, 71 );
            this.fileGroupBox.TabIndex = 0;
            this.fileGroupBox.TabStop = false;
            this.fileGroupBox.Text = "File";
            // 
            // clearRecentFilesButton
            // 
            this.clearRecentFilesButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.clearRecentFilesButton.AutoSize = true;
            this.clearRecentFilesButton.Location = new System.Drawing.Point( 6, 42 );
            this.clearRecentFilesButton.Name = "clearRecentFilesButton";
            this.clearRecentFilesButton.Size = new System.Drawing.Size( 118, 23 );
            this.clearRecentFilesButton.TabIndex = 1;
            this.clearRecentFilesButton.Text = "Clear Recent Files list";
            this.clearRecentFilesButton.UseVisualStyleBackColor = true;
            this.clearRecentFilesButton.Click += new System.EventHandler( this.clearRecentFilesButton_Click );
            // 
            // loadLastFileCheckBox
            // 
            this.loadLastFileCheckBox.AutoSize = true;
            this.loadLastFileCheckBox.Location = new System.Drawing.Point( 6, 19 );
            this.loadLastFileCheckBox.Name = "loadLastFileCheckBox";
            this.loadLastFileCheckBox.Size = new System.Drawing.Size( 132, 17 );
            this.loadLastFileCheckBox.TabIndex = 0;
            this.loadLastFileCheckBox.Text = "Load last file at startup";
            this.loadLastFileCheckBox.UseVisualStyleBackColor = true;
            // 
            // playbackGroupBox
            // 
            this.playbackGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.playbackGroupBox.Controls.Add( this.skipSizeUnitsLabel );
            this.playbackGroupBox.Controls.Add( this.skipSizeNumericUpDown );
            this.playbackGroupBox.Controls.Add( this.skipSizeLabel );
            this.playbackGroupBox.Controls.Add( this.playbackRateUnitsLabel );
            this.playbackGroupBox.Controls.Add( this.playbackRateNumericUpDown );
            this.playbackGroupBox.Controls.Add( this.playbackRateLabel );
            this.playbackGroupBox.Location = new System.Drawing.Point( 12, 89 );
            this.playbackGroupBox.Name = "playbackGroupBox";
            this.playbackGroupBox.Size = new System.Drawing.Size( 335, 65 );
            this.playbackGroupBox.TabIndex = 1;
            this.playbackGroupBox.TabStop = false;
            this.playbackGroupBox.Text = "Playback";
            // 
            // skipSizeUnitsLabel
            // 
            this.skipSizeUnitsLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.skipSizeUnitsLabel.AutoSize = true;
            this.skipSizeUnitsLabel.Location = new System.Drawing.Point( 218, 41 );
            this.skipSizeUnitsLabel.Name = "skipSizeUnitsLabel";
            this.skipSizeUnitsLabel.Size = new System.Drawing.Size( 54, 13 );
            this.skipSizeUnitsLabel.TabIndex = 5;
            this.skipSizeUnitsLabel.Text = "messages";
            this.skipSizeUnitsLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // skipSizeNumericUpDown
            // 
            this.skipSizeNumericUpDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.skipSizeNumericUpDown.Location = new System.Drawing.Point( 92, 39 );
            this.skipSizeNumericUpDown.Maximum = new decimal( new int[] {
            1000000,
            0,
            0,
            0} );
            this.skipSizeNumericUpDown.Minimum = new decimal( new int[] {
            1,
            0,
            0,
            0} );
            this.skipSizeNumericUpDown.Name = "skipSizeNumericUpDown";
            this.skipSizeNumericUpDown.Size = new System.Drawing.Size( 120, 20 );
            this.skipSizeNumericUpDown.TabIndex = 4;
            this.skipSizeNumericUpDown.Value = new decimal( new int[] {
            1,
            0,
            0,
            0} );
            // 
            // skipSizeLabel
            // 
            this.skipSizeLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.skipSizeLabel.AutoSize = true;
            this.skipSizeLabel.Location = new System.Drawing.Point( 32, 41 );
            this.skipSizeLabel.Name = "skipSizeLabel";
            this.skipSizeLabel.Size = new System.Drawing.Size( 54, 13 );
            this.skipSizeLabel.TabIndex = 3;
            this.skipSizeLabel.Text = "Skip Size:";
            this.skipSizeLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // playbackRateUnitsLabel
            // 
            this.playbackRateUnitsLabel.AutoSize = true;
            this.playbackRateUnitsLabel.Location = new System.Drawing.Point( 218, 16 );
            this.playbackRateUnitsLabel.Name = "playbackRateUnitsLabel";
            this.playbackRateUnitsLabel.Size = new System.Drawing.Size( 110, 13 );
            this.playbackRateUnitsLabel.TabIndex = 2;
            this.playbackRateUnitsLabel.Text = "messages per second";
            this.playbackRateUnitsLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // playbackRateNumericUpDown
            // 
            this.playbackRateNumericUpDown.Increment = new decimal( new int[] {
            1000,
            0,
            0,
            0} );
            this.playbackRateNumericUpDown.Location = new System.Drawing.Point( 92, 14 );
            this.playbackRateNumericUpDown.Maximum = new decimal( new int[] {
            1000000,
            0,
            0,
            0} );
            this.playbackRateNumericUpDown.Minimum = new decimal( new int[] {
            1,
            0,
            0,
            0} );
            this.playbackRateNumericUpDown.Name = "playbackRateNumericUpDown";
            this.playbackRateNumericUpDown.Size = new System.Drawing.Size( 120, 20 );
            this.playbackRateNumericUpDown.TabIndex = 1;
            this.playbackRateNumericUpDown.Value = new decimal( new int[] {
            1000,
            0,
            0,
            0} );
            this.playbackRateNumericUpDown.ValueChanged += new System.EventHandler( this.playbackRateNumericUpDown_ValueChanged );
            // 
            // playbackRateLabel
            // 
            this.playbackRateLabel.AutoSize = true;
            this.playbackRateLabel.Location = new System.Drawing.Point( 6, 16 );
            this.playbackRateLabel.Name = "playbackRateLabel";
            this.playbackRateLabel.Size = new System.Drawing.Size( 80, 13 );
            this.playbackRateLabel.TabIndex = 0;
            this.playbackRateLabel.Text = "Playback Rate:";
            this.playbackRateLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // okButton
            // 
            this.okButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.okButton.Location = new System.Drawing.Point( 191, 163 );
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size( 75, 23 );
            this.okButton.TabIndex = 2;
            this.okButton.Text = "OK";
            this.okButton.UseVisualStyleBackColor = true;
            // 
            // cancelButton
            // 
            this.cancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.Location = new System.Drawing.Point( 272, 163 );
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size( 75, 23 );
            this.cancelButton.TabIndex = 3;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            // 
            // OptionsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cancelButton;
            this.ClientSize = new System.Drawing.Size( 359, 198 );
            this.Controls.Add( this.cancelButton );
            this.Controls.Add( this.okButton );
            this.Controls.Add( this.playbackGroupBox );
            this.Controls.Add( this.fileGroupBox );
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "OptionsForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "Options";
            this.fileGroupBox.ResumeLayout( false );
            this.fileGroupBox.PerformLayout();
            this.playbackGroupBox.ResumeLayout( false );
            this.playbackGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.skipSizeNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.playbackRateNumericUpDown)).EndInit();
            this.ResumeLayout( false );

        }

        #endregion

        private System.Windows.Forms.GroupBox fileGroupBox;
        private System.Windows.Forms.GroupBox playbackGroupBox;
        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Button clearRecentFilesButton;
        private System.Windows.Forms.CheckBox loadLastFileCheckBox;
        private System.Windows.Forms.Label playbackRateUnitsLabel;
        private System.Windows.Forms.NumericUpDown playbackRateNumericUpDown;
        private System.Windows.Forms.Label playbackRateLabel;
        private System.Windows.Forms.Label skipSizeLabel;
        private System.Windows.Forms.Label skipSizeUnitsLabel;
        private System.Windows.Forms.NumericUpDown skipSizeNumericUpDown;
    }
}