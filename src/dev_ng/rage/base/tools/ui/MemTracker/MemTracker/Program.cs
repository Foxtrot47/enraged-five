using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;

using MemTracker.Properties;
using rageUsefulCSharpToolClasses;
using TD.SandDock;

namespace MemTracker
{
    static class Program
    {
        static Form1 m_mainForm = null;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main( string[] args )
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault( false );
            Application.ThreadException += new System.Threading.ThreadExceptionEventHandler( Application_ThreadException );

            SandDockManager.ActivateProduct( "1029|mNHCocHlEeYJt/q8SORWBb15rDw=" ); // 3.0.4.1

            m_mainForm = new Form1( args );
            Application.Run( m_mainForm );
        }

        static void Application_ThreadException( object sender, System.Threading.ThreadExceptionEventArgs e )
        {
            List<string> attachments = new List<string>();

            string userConfigFilename = rageUsefulCSharpToolClasses.rageFileUtilities.GetRageApplicationUserConfigFilename( "MemTracker.exe" );
            if ( !String.IsNullOrEmpty( userConfigFilename ) && File.Exists( userConfigFilename ) )
            {
                attachments.Add( userConfigFilename );
            }

            UnhandledExceptionDialog eForm = new UnhandledExceptionDialog( "MemTracker", attachments,
                Settings.Default.ExceptionToEmailAddress, Settings.Default.ExceptionFromEmailAddress, e.Exception );
            eForm.ShowDialog();

            Settings.Default.ExceptionFromEmailAddress = eForm.YourEmailAddress;

            m_mainForm.ExitingAfterException = true;

            try
            {
                Application.Exit();
            }
            catch ( Exception ex )
            {
                Console.WriteLine( "Application.Exit() failed: " + ex.Message );
                Application.Exit();
            }
        }
    }
}