#define ENABLE_BACKWARDS_PLAYBACK

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using System.Xml.Serialization;

using MemTracker.Properties;
using MemTrackerControls;
using rageUsefulCSharpToolClasses;
using TD.SandDock;

namespace MemTracker
{
    public partial class Form1 : Form
    {
        protected Form1()
        {
            InitializeComponent();
            InitializeAdditionalComponents();
        }

        public Form1( string[] args )
        {
            InitializeComponent();
            InitializeAdditionalComponents();

            if ( args.Length > 1 )
            {
                string filename = args[1].Trim();
                if ( filename != string.Empty )
                {
                    OpenTrackerFile( Path.GetFullPath( filename ) );
                }
            }
        }

        #region Enums
        private enum AppAction
        {
            NewOverviewWindow,
            NewAllocationsWindow,
            NewMemoryLayoutWindow,
            SkipBackward,
            StepBackward,
            Pause,
            Play,
            StepForward,
            SkipForward
        }

        private enum PlaybackState
        {
            Stopped,
            Paused,
            Play
        }
        #endregion

        #region Constants
        const int c_toolStripSpacing = 3;
        const string c_reportGeneratedString = "Report Generated:  ";
        const string c_helpFile = @"chm\diag.chm";
        #endregion

        #region Variables
        private string m_trackerFilePrefix = null;
        private string m_trackerFilePostfix = null;
        private int m_multiTrackerFileIndex = -1;
        private bool m_isMultiTrackerFile = false;

        private uint m_messageCountBeforeRead = 0;
        private uint m_messageCount = 0;

        private int m_numberReadsPerTimerUpdate = 1;

        private int m_overviewWindowCount = 1;
        private int m_allocationsWindowCount = 1;
        private int m_memoryLayoutWindowCount = 1;

        private PlaybackState m_playbackState = PlaybackState.Stopped;

        private FileStream m_previousTrackerFileStream;
        private FileStream m_currentTrackerFileStream;
        private FileStream m_nextTrackerFileStream;

        private TextReader m_previousTrackerFileTextReader;
        private TextReader m_currentTrackerFileTextReader;
        private TextReader m_nextTrackerFileTextReader;

        private MessageLine m_previousMessageLine = null;
        private MessageLine m_currentMessageLine = null;
        private MessageLine m_nextMessageLine = null;

        private Stack<MessageLine> m_nextMessageLinesStack = new Stack<MessageLine>();

        private bool m_exitingAfterException = false;

        private List<OverviewListControlSettings> m_overviewSettings = new List<OverviewListControlSettings>();
        private List<AllocationsTreeListControlSettings> m_allocationsSettings = new List<AllocationsTreeListControlSettings>();
        private List<MemoryLayoutControlSettings> m_memoryLayoutSettings = new List<MemoryLayoutControlSettings>();

        private OverviewListControlSettings m_defaultOverviewSettings = null;
        private AllocationsTreeListControlSettings m_defaultAllocationsSettings = null;
        private MemoryLayoutControlSettings m_defaultMemoryLayoutSettings = null;

        private GoToOptions m_goToOptions = new GoToOptions();
        #endregion

        #region Properties
        public bool ExitingAfterException
        {
            set
            {
                m_exitingAfterException = value;
            }
        }

        private PlaybackState State
        {
            get
            {
                return m_playbackState;
            }
            set
            {
                m_playbackState = value;

                bool paused = m_playbackState == PlaybackState.Paused;
                bool forward = m_nextMessageLine != null;

#if ENABLE_BACKWARDS_PLAYBACK
                bool backward = m_previousMessageLine != null;
#else
                bool backward = false;
#endif
                
                this.playbackSkipBackwardToolStripMenuItem.Enabled = paused && backward;
                this.playbackStepBackwardToolStripMenuItem.Enabled = paused && backward;
                this.playbackPauseToolStripMenuItem.Enabled = m_playbackState == PlaybackState.Play;
                this.playbackPlayToolStripMenuItem.Enabled = paused && forward;
                this.playbackStepForwardToolStripMenuItem.Enabled = paused && forward;
                this.playbackSkipForwardToolStripMenuItem.Enabled = paused && forward;
                this.playbackGoToToolStripMenuItem.Enabled = paused;
                this.playbackGoToAgainToolStripMenuItem.Enabled = paused;

                this.skipBackwardToolStripButton.Enabled = paused && backward;
                this.stepBackwardToolStripButton.Enabled = paused && backward;
                this.pauseToolStripButton.Enabled = m_playbackState == PlaybackState.Play;
                this.playToolStripButton.Enabled = paused && forward;
                this.stepForwardToolStripButton.Enabled = paused && forward;
                this.skipForwardToolStripButton.Enabled = paused && forward;

                switch ( m_playbackState )
                {
                    case PlaybackState.Paused:
                        this.timer1.Stop();
                        break;
                    case PlaybackState.Play:
                        this.timer1.Start();
                        break;
                    case PlaybackState.Stopped:
                        this.AllowRefresh = false;
                        break;
                }
            }
        }

        private decimal PlaybackRate
        {
            set
            {
                int rate = (int)value;
                if ( rate > 0 )
                {
                    this.timer1.Interval = rate;
                    m_numberReadsPerTimerUpdate = 1;
                }
                else
                {
                    this.timer1.Interval = 1;
                    m_numberReadsPerTimerUpdate = (int)((1000 / value) / 1000);
                }
            }
        }

        private string PreviousTrackerFilename
        {
            get
            {
                if ( !m_isMultiTrackerFile || (m_multiTrackerFileIndex > 0) )
                {
                    return Form1.BuildTrackerFilename( m_trackerFilePrefix, m_trackerFilePostfix, m_multiTrackerFileIndex - 1 );
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        private string CurrentTrackerFilename
        {
            get
            {
                return Form1.BuildTrackerFilename( m_trackerFilePrefix, m_trackerFilePostfix, m_multiTrackerFileIndex );
            }
        }

        private string NextTrackerFilename
        {
            get
            {
                return Form1.BuildTrackerFilename( m_trackerFilePrefix, m_trackerFilePostfix, m_multiTrackerFileIndex + 1 );
            }
        }

        private bool AllowRefresh
        {
            set
            {
                DockControl[] windows = this.sandDockManager1.GetDockControls();
                foreach ( DockControl window in windows )
                {
                    if ( window != this.messagesDockableWindow )
                    {
                        Control control = window.Controls[0];
                        if ( control is OverviewListControl )
                        {
                            ((OverviewListControl)control).GenerateReportButtonEnabled = value;
                            ((OverviewListControl)control).RefeshButtonEnabled = value;
                        }
                        else if ( control is AllocationsTreeListControl )
                        {
                            ((AllocationsTreeListControl)control).GenerateReportButtonEnabled = value;
                            ((AllocationsTreeListControl)control).RefeshButtonEnabled = value;
                        }
                        else if ( control is MemoryLayoutControl )
                        {
                            ((MemoryLayoutControl)control).GenerateReportButtonEnabled = value;
                            ((MemoryLayoutControl)control).RefeshButtonEnabled = value;
                        }
                    }
                }
            }
        }
        #endregion

        #region Public Static Functions
        /// <summary>
        /// Centers the <see cref="Form"/> over the <see cref="Control"/>.
        /// </summary>
        /// <param name="parent">The parent <see cref="Control"/>, usually the main <see cref="Form"/> of the application.</param>
        /// <param name="form">The <see cref="Form"/> to center.</param>
        public static void CenterLocation( Control parent, Form form )
        {
            Rectangle rect = parent.Bounds;
            form.StartPosition = FormStartPosition.Manual;

            int x = (rect.Width / 2) + rect.Left - (form.Width / 2);
            int y = (rect.Height / 2) + rect.Top - (form.Height / 2);

            form.Location = new Point( x, y );
        }

        /// <summary>
        /// Builds the file name from the 3 parts.  If index is -1, a '+' is put in it's place.
        /// </summary>
        /// <param name="prefix"></param>
        /// <param name="postfix"></param>
        /// <param name="index"></param>
        /// <returns></returns>
        public static string BuildTrackerFilename( string prefix, string postfix, int index )
        {
            if ( (prefix == null) || (prefix == string.Empty) )
            {
                return string.Empty;
            }

            StringBuilder filename = new StringBuilder( prefix );

            if ( (postfix != null) && (postfix != string.Empty) )
            {
                string text = "+";
                if ( index > -1 )
                {
                    StringBuilder count = new StringBuilder( index.ToString() );
                    for ( int i = count.Length; i < 4; ++i )
                    {
                        count.Insert( 0, '0' );
                    }
                    text = count.ToString();
                }

                filename.Append( text );
                filename.Append( postfix );
            }

            return filename.ToString();
        }
        #endregion

        #region Form Event Handlers
        private void Form1_Load( object sender, EventArgs e )
        {
            LoadLayout();
        }

        private void Form1_FormClosing( object sender, FormClosingEventArgs e )
        {
            if ( m_exitingAfterException )
            {
                return;
            }

            SaveLayout();
        }
        #endregion

        #region SandDockManager Event Handlers
        private void sandDockManager1_ResolveDockControl( object sender, ResolveDockControlEventArgs e )
        {
            DockableWindow window = null;
            
            string guid = e.Guid.ToString();

            for ( int i = 0; i < m_overviewSettings.Count; ++i )
            {
                if ( m_overviewSettings[i].Guid == guid )
                {
                    window = CreateOverviewWindow( m_overviewSettings[i] );
                    m_overviewSettings.RemoveAt( i );
                    break;
                }
            }

            if ( window == null )
            {
                for ( int i = 0; i < m_allocationsSettings.Count; ++i )
                {
                    if ( m_allocationsSettings[i].Guid == guid )
                    {
                        window = CreateAllocationsWindow( m_allocationsSettings[i] );
                        m_allocationsSettings.RemoveAt( i );
                        break;
                    }
                }
            }

            if ( window == null )
            {
                for ( int i = 0; i < m_memoryLayoutSettings.Count; ++i )
                {
                    if ( m_memoryLayoutSettings[i].Guid == guid )
                    {
                        window = CreateMemoryLayoutWindow( m_memoryLayoutSettings[i] );
                        m_memoryLayoutSettings.RemoveAt( i );
                        break;
                    }
                }
            }

            e.DockControl = window;
        }
        #endregion

        #region MenuStrip Event Handlers
        private void fileToolStripMenuItem_DropDownOpening( object sender, EventArgs e )
        {
            this.fileCloseTrackerFileToolStripMenuItem.Enabled = m_trackerFilePrefix != null;
        }

        #region File Drop Down Menu
        private void fileOpenSingleTrackerFileToolStripMenuItem_Click( object sender, EventArgs e )
        {
            DialogResult result = this.openFileDialog1.ShowDialog( this );
            if ( result == DialogResult.OK )
            {
                OpenTrackerFile( Path.GetFullPath( this.openFileDialog1.FileName ) );
            }
        }

        private void fileOpenMultiToolStripMenuItem_Click( object sender, EventArgs e )
        {
            DialogResult result = this.folderBrowserDialog1.ShowDialog( this );
            if ( result == DialogResult.OK )
            {
                LoadMultiTrackerFileForm loadForm = new LoadMultiTrackerFileForm( this.folderBrowserDialog1.SelectedPath );
                Form1.CenterLocation( this, loadForm );

                result = loadForm.ShowDialog( this );
                if ( result == DialogResult.OK )
                {
                    OpenTrackerFile( Form1.BuildTrackerFilename( 
                        Path.Combine( this.folderBrowserDialog1.SelectedPath, loadForm.Prefix ), loadForm.Postfix, -1 ) );
                }
            }
        }

        private void fileCloseTrackerFileToolStripMenuItem_Click( object sender, EventArgs e )
        {
            CloseTrackerFile();
        }

        private void fileSaveReportItemToolStripMenuItem_Click( object sender, EventArgs e )
        {
            ToolStripMenuItem menuItem = sender as ToolStripMenuItem;
            if ( menuItem != null )
            {
                ReportItem report = menuItem.Tag as ReportItem;
                if ( report != null )
                {
                    string filename = report.Name;
                    char[] invalidChars = Path.GetInvalidFileNameChars();
                    foreach ( char c in invalidChars )
                    {
                        filename = filename.Replace( c, '_' );
                    }
                    this.saveFileDialog1.FileName = filename;

                    DialogResult result = this.saveFileDialog1.ShowDialog( this );
                    if ( result == DialogResult.OK )
                    {
                        this.trackerComponent1.SaveReport( this.saveFileDialog1.FileName, report );
                    }
                }
            }
        }

        private void fileRecentFilesItemToolStripMenuItem_Click( object sender, EventArgs e )
        {
            ToolStripMenuItem item = sender as ToolStripMenuItem;
            OpenTrackerFile( item.Text );
        }

        private void fileExitToolStripMenuItem_Click( object sender, EventArgs e )
        {
            Application.Exit();
        }
        #endregion

        private void playbackToolStripMenuItem_DropDownOpening( object sender, EventArgs e )
        {

        }

        #region Playback Drop Down Menu
        private void playbackGoToToolStripMenuItem_Click( object sender, EventArgs e )
        {
#if ENABLE_BACKWARDS_PLAYBACK
            GoToForm form = new GoToForm( m_goToOptions, true );
#else
            GoToForm form = new GoToForm( m_goToOptions, false );
#endif
            CenterLocation( this, form );
            
            DialogResult result = form.ShowDialog( this );
            if ( result == DialogResult.OK )
            {
                playbackGoToAgainToolStripMenuItem_Click( sender, e );
            }
        }

        private void playbackGoToAgainToolStripMenuItem_Click( object sender, EventArgs e )
        {
            BeginReadMessage();

            switch ( m_goToOptions.Search )
            {
                case GoToOptions.SearchType.Allocation:
                    {
                        bool forward = true;
                        if ( (m_goToOptions.Allocation == GoToOptions.AllocationType.PreviousAllocation) 
                            || (m_goToOptions.Allocation == GoToOptions.AllocationType.PreviousDeallocation) )
                        {
                            forward = false;
                        }

                        bool alloc = true;
                        if ( (m_goToOptions.Allocation == GoToOptions.AllocationType.NextDeallocation) 
                            || (m_goToOptions.Allocation == GoToOptions.AllocationType.PreviousDeallocation) )
                        {
                            alloc = false;
                        }

                        int preCount = this.trackerComponent1.Actions.Count;

                        while ( true )
                        {
                            if ( forward )
                            {
                                if ( !ReadLineForward() )
                                {
                                    break;
                                }
                            }
                            else
                            {
                                if ( !ReadLineBackward() )
                                {
                                    break;
                                }
                            }

                            if ( (this.trackerComponent1.Actions.Count == 0) || (this.trackerComponent1.Actions.Count == preCount) )
                            {
                                continue;
                            }

                            TrackerAction action = this.trackerComponent1.Actions[this.trackerComponent1.Actions.Count - 1];
                            if ( (action.TheAction != TrackerAction.Action.TallyAllocation) && (action.TheAction != TrackerAction.Action.UntallyAllocation) )
                            {
                                continue;
                            }

                            uint address = 0;
                            uint size = 0;
                            if ( alloc && (action.TheAction == TrackerAction.Action.TallyAllocation) )
                            {
                                if ( !m_goToOptions.UseStartAddress && !m_goToOptions.UseMinimumSize )
                                {
                                    break;
                                }

                                TallyAllocationAction tallyAction = action as TallyAllocationAction;
                                address = tallyAction.TheAllocation.DecimalAddress;
                                size = tallyAction.TheAllocation.Size;
                            }
                            else if ( !alloc && (action.TheAction == TrackerAction.Action.UntallyAllocation) )
                            {
                                if ( !m_goToOptions.UseStartAddress && !m_goToOptions.UseMinimumSize )
                                {
                                    break;
                                }

                                UntallyAllocationAction tallyAction = action as UntallyAllocationAction;
                                address = tallyAction.TheAllocation.DecimalAddress;
                                size = tallyAction.TheAllocation.Size;
                            }

                            if ( m_goToOptions.UseStartAddress )
                            {
                                if ( m_goToOptions.UseEndAddress )
                                {
                                    if ( (address >= m_goToOptions.StartAddress) && (address <= m_goToOptions.EndAddress) )
                                    {
                                        break;
                                    }
                                }
                                else
                                {
                                    if ( address == m_goToOptions.StartAddress )
                                    {
                                        break;
                                    }
                                }
                            }

                            if ( m_goToOptions.UseMinimumSize )
                            {
                                if ( m_goToOptions.UseMaximumSize )
                                {
                                    if ( (size >= m_goToOptions.MinimumSize) && (size <= m_goToOptions.MaximumSize) )
                                    {
                                        break;
                                    }
                                }
                                else
                                {
                                    if ( size == m_goToOptions.MinimumSize )
                                    {
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    break;
                case GoToOptions.SearchType.MessageNumber:
                    {
                        int difference = (int)(m_messageCount - m_goToOptions.MessageNumber);
                        while ( m_messageCount - 1 != m_goToOptions.MessageNumber )
                        {
                            if ( difference > 0 )
                            {
#if ENABLE_BACKWARDS_PLAYBACK
                                if ( !ReadLineBackward() )
                                {
                                    break;
                                }
#endif
                            }
                            else
                            {
                                if ( !ReadLineForward() )
                                {
                                    break;
                                }
                            }
                        }
                    }
                    break;
                case GoToOptions.SearchType.Report:
                    {
                        switch ( m_goToOptions.Report )
                        {
                            case GoToOptions.ReportType.Next:
                                {
                                    int reportCount = this.trackerComponent1.Reports.Length;
                                    while ( reportCount == this.trackerComponent1.Reports.Length )
                                    {
                                        if ( !ReadLineForward() )
                                        {
                                            break;
                                        }
                                    }
                                }
                                break;
                            case GoToOptions.ReportType.Previous:
                                {
#if ENABLE_BACKWARDS_PLAYBACK
                                    int reportCount = this.trackerComponent1.Reports.Length;
                                    while ( reportCount == this.trackerComponent1.Reports.Length )
                                    {
                                        if ( !ReadLineBackward() )
                                        {
                                            break;
                                        }
                                    }
#endif
                                }
                                break;
                            case GoToOptions.ReportType.Selected:
                                {
                                    bool forward = true;
                                    if ( m_goToOptions.SelectedReport.Index < this.trackerComponent1.Actions.Count - 1 )
                                    {
                                        forward = false;
                                    }

                                    while ( this.trackerComponent1.Actions.Count != m_goToOptions.SelectedReport.Index + 1 )
                                    {
                                        if ( forward )
                                        {
                                            if ( !ReadLineForward() )
                                            {
                                                break;
                                            }
                                        }
                                        else
                                        {
#if ENABLE_BACKWARDS_PLAYBACK
                                            if ( !ReadLineBackward() )
                                            {
                                                break;
                                            }
#endif
                                        }
                                    }
                                }
                                break;
                        }
                    }
                    break;
                case GoToOptions.SearchType.Stack:
                    {
                        bool forward = true;
#if ENABLE_BACKWARDS_PLAYBACK
                        if ( (m_goToOptions.Stack == GoToOptions.StackType.PreviousPop)
                            || (m_goToOptions.Stack == GoToOptions.StackType.PreviousPush) )
                        {
                            forward = false;
                        }
#endif

                        bool push = true;
                        if ( (m_goToOptions.Stack == GoToOptions.StackType.NextPop) 
                            || (m_goToOptions.Stack == GoToOptions.StackType.PreviousPop) )
                        {
                            push = false;
                        }

                        int preCount = this.trackerComponent1.Actions.Count;

                        while ( true )
                        {
                            if ( forward )
                            {
                                if ( !ReadLineForward() )
                                {
                                    break;
                                }
                            }
                            else
                            {
                                if ( !ReadLineBackward() )
                                {
                                    break;
                                }
                            }

                            if ( (this.trackerComponent1.Actions.Count == 0) || (this.trackerComponent1.Actions.Count == preCount) )
                            {
                                continue;
                            }

                            TrackerAction action = this.trackerComponent1.Actions[this.trackerComponent1.Actions.Count - 1];
                            if ( (action.TheAction != TrackerAction.Action.PushStack) && (action.TheAction != TrackerAction.Action.PopStack) )
                            {
                                continue;
                            }

                            string stackName = string.Empty;
                            if ( push && (action.TheAction == TrackerAction.Action.PushStack) )
                            {
                                if ( !m_goToOptions.UseSelectedStacks && !m_goToOptions.UseSpecifiedStacks )
                                {
                                    break;
                                }

                                PushStackAction stackAction = action as PushStackAction;
                                stackName = stackAction.Name;
                            }
                            else if ( !push && (action.TheAction == TrackerAction.Action.PopStack) )
                            {
                                if ( !m_goToOptions.UseSelectedStacks && !m_goToOptions.UseSpecifiedStacks )
                                {
                                    break;
                                }

                                PopStackAction stackAction = action as PopStackAction;
                                stackName = stackAction.Name;
                            }

                            if ( m_goToOptions.UseSelectedStacks )
                            {
                                bool vis;
                                if ( m_goToOptions.SelectedStacks.TryGetValue( (uint)stackName.GetHashCode(), out vis ) )
                                {
                                    if ( vis )
                                    {
                                        break;
                                    }
                                }
                            }

                            if ( m_goToOptions.UseSpecifiedStacks )
                            {
                                if ( m_goToOptions.SpecifiedStacks.Contains( stackName ) )
                                {
                                    break;
                                }
                            }
                        }
                    }
                    break;
            }

            EndReadMessage();

            this.State = PlaybackState.Paused;
        }
        #endregion

        #region Tools Drop Down Menu
        private void toolsCustomizeToolStripMenuItem_Click( object sender, EventArgs e )
        {            
            CustomizeForm form = new CustomizeForm( m_defaultOverviewSettings, m_defaultAllocationsSettings, m_defaultMemoryLayoutSettings );
            Form1.CenterLocation( this, form );

            DialogResult result = form.ShowDialog( this );
            if ( result == DialogResult.OK )
            {
                m_defaultOverviewSettings = form.OverviewSettings;
                m_defaultAllocationsSettings = form.AllocationsSettings;
                m_defaultMemoryLayoutSettings = form.MemoryLayoutSettings;
            }
        }

        private void toolsOptionsToolStripMenuItem_Click( object sender, EventArgs e )
        {
            OptionsForm form = new OptionsForm( Settings.Default.LoadLastFile,
                Settings.Default.PlaybackRate, Settings.Default.SkipSize, new EventHandler( clearRecentFilesEventHandler ) );
            Form1.CenterLocation( this, form );

            DialogResult result = form.ShowDialog( this );
            if ( result == DialogResult.OK )
            {
                Settings.Default.LoadLastFile = form.LoadLastFile;
                Settings.Default.PlaybackRate = this.PlaybackRate = form.PlaybackRate;
                Settings.Default.SkipSize = form.SkipSize;
            }
        }

        private void clearRecentFilesEventHandler( object sender, EventArgs e )
        {
            this.fileRecentFilesToolStripMenuItem.DropDownItems.Clear();
        }
        #endregion

        #region Windows Drop Down Menu
        private void viewErrorsToolStripMenuItem_Click( object sender, EventArgs e )
        {
            ErrorsView eView = ErrorsView.Create( this.trackerComponent1 );
            Form1.CenterLocation( this, eView );

            eView.ShowDialog( this );
        }
        #endregion

        #region Help Drop Down Menu
        private void helpContentsToolStripMenuItem_Click( object sender, EventArgs e )
        {
            string helpfile = Path.Combine( Path.GetDirectoryName( Application.ExecutablePath ), c_helpFile );
            if ( File.Exists( helpfile ) )
            {
                try
                {
                    Process.Start( helpfile );
                }
                catch
                {
                }
            }
        }

        private void helpAboutToolStripMenuItem_Click( object sender, EventArgs e )
        {
            string versionStr = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
            MessageBox.Show( this, "MemTracker written by Adam Dickinson, Rockstar San Diego 2007\nVersion " + versionStr, "About..." );
        }
        #endregion

        #endregion

        #region AppAction
        private void appActionToolStripMenuItem_Click( object sender, EventArgs e )
        {
            ToolStripItem item = sender as ToolStripItem;
            string tag = item.Tag as string;
            AppAction action = (AppAction)Enum.Parse( typeof( AppAction ), tag );
            ExecuteAppAction( action );
        }

        private void ExecuteAppAction( AppAction action )
        {
            switch ( action )
            {
                case AppAction.NewOverviewWindow:
                    {
                        DockableWindow window = CreateOverviewWindow( m_defaultOverviewSettings );

                        CenterLocation( this, window );

                        window.OpenFloating( WindowOpenMethod.OnScreenActivate );
                    }
                    break;
                case AppAction.NewAllocationsWindow:
                    {
                        DockableWindow window = CreateAllocationsWindow( m_defaultAllocationsSettings );

                        CenterLocation( this, window );

                        window.OpenFloating( WindowOpenMethod.OnScreenActivate );
                    }
                    break;
                case AppAction.NewMemoryLayoutWindow:
                    {
                        DockableWindow window = CreateMemoryLayoutWindow( m_defaultMemoryLayoutSettings );

                        CenterLocation( this, window );

                        window.OpenFloating( WindowOpenMethod.OnScreenActivate );
                    }
                    break;
                case AppAction.SkipBackward:
                    SkipBackward();
                    break;
                case AppAction.StepBackward:
                    StepBackward();
                    break;
                case AppAction.Pause:
                    Pause();
                    break;
                case AppAction.Play:
                    Play();
                    break;
                case AppAction.StepForward:
                    StepForward();
                    break;
                case AppAction.SkipForward:
                    SkipForward();
                    break;
            }
        }

        private DockableWindow CreateOverviewWindow( OverviewListControlSettings overviewSettings )
        {
            Guid guid;
            int windowNumber;
            if ( (overviewSettings != null) && (overviewSettings.Index != -1) )
            {
                guid = new Guid( overviewSettings.Guid );

                windowNumber = overviewSettings.Index;
                if ( windowNumber > m_overviewWindowCount )
                {
                    m_overviewWindowCount = windowNumber;
                }
            }
            else
            {
                guid = Guid.NewGuid();

                ++m_overviewWindowCount;
                windowNumber = m_overviewWindowCount;
            }

            OverviewListControl overview = new OverviewListControl();
            overview.Dock = System.Windows.Forms.DockStyle.Fill;
            overview.Location = new System.Drawing.Point( 0, 0 );
            overview.Name = "overviewListControl" + windowNumber;
            overview.ParentControl = this;
            overview.TabIndex = 0;
            overview.ThreadingEnabled = true;
            overview.Tracker = this.trackerComponent1;
            overview.UpdateBegin += new UpdateBeginEventHandler( overviewListControl1_UpdateBegin );
            overview.UpdateComplete += new UpdateCompleteEventHandler( MemTrackerControl_UpdateComplete );

            if ( overviewSettings != null )
            {
                SetOverviewSettings( overviewSettings, overview );
            }

            overview.GenerateReportButtonEnabled = (m_playbackState != PlaybackState.Stopped) && (m_messageCount > 0);
            overview.RefeshButtonEnabled = overview.GenerateReportButtonEnabled;

            DockableWindow window = new DockableWindow( this.sandDockManager1, overview, String.Format( "Overview ({0})", windowNumber ) );
            window.CloseAction = TD.SandDock.DockControlCloseAction.Dispose;
            window.Controls.Add( overview );
            window.DockingRules = new DockingRules( true, true, true );
            window.FloatingSize = new System.Drawing.Size( 1000, 600 );
            window.Guid = guid;
            window.Location = new System.Drawing.Point( 0, 16 );
            window.Name = "overviewDockableWindow" + windowNumber;
            window.ShowOptions = false;
            window.Size = new System.Drawing.Size( 800, 400 );
            window.TabImage = global::MemTracker.Properties.Resources.OverviewWindow;

            return window;
        }

        private DockableWindow CreateAllocationsWindow( AllocationsTreeListControlSettings allocationsSettings )
        {
            Guid guid;
            int windowNumber;
            if ( (allocationsSettings != null) && (allocationsSettings.Index != -1) )
            {
                guid = new Guid( allocationsSettings.Guid );

                windowNumber = allocationsSettings.Index;
                if ( windowNumber > m_allocationsWindowCount )
                {
                    m_allocationsWindowCount = windowNumber;
                }
            }
            else
            {
                guid = Guid.NewGuid();

                ++m_allocationsWindowCount;
                windowNumber = m_allocationsWindowCount;
            }

            AllocationsTreeListControl allocations = new AllocationsTreeListControl();
            allocations.Dock = System.Windows.Forms.DockStyle.Fill;
            allocations.Location = new System.Drawing.Point( 0, 0 );
            allocations.Name = "allocationsTreeListControl" + windowNumber;
            allocations.ParentControl = this;
            allocations.ShowSizingGrip = false;
            allocations.Size = new System.Drawing.Size( 800, 400 );
            allocations.TabIndex = 0;
            allocations.ThreadingEnabled = true;
            allocations.Tracker = this.trackerComponent1;
            allocations.OutOfMemoryError += new MemTrackerControls.ErrorEventHandler( this.allocationsTreeListControl1_OutOfMemoryError );
            allocations.UpdateBegin += new UpdateBeginEventHandler( allocationsTreeListControl1_UpdateBegin );
            allocations.UpdateComplete += new UpdateCompleteEventHandler( MemTrackerControl_UpdateComplete );

            if ( allocationsSettings != null )
            {
                SetAllocationsSettings( allocationsSettings, allocations );
            }

            allocations.GenerateReportButtonEnabled = (m_playbackState != PlaybackState.Stopped) && (m_messageCount > 0);
            allocations.RefeshButtonEnabled = allocations.GenerateReportButtonEnabled;

            DockableWindow window = new DockableWindow( this.sandDockManager1, allocations, String.Format( "Allocations ({0})", windowNumber ) );
            window.CloseAction = TD.SandDock.DockControlCloseAction.Dispose;
            window.Controls.Add( allocations );
            window.DockingRules = new DockingRules( true, true, true );
            window.FloatingSize = new System.Drawing.Size( 1000, 600 );
            window.Guid = guid;
            window.Location = new System.Drawing.Point( 0, 16 );
            window.Name = "allocationsDockableWindow" + windowNumber;
            window.ShowOptions = false;
            window.Size = new System.Drawing.Size( 843, 349 );
            window.TabImage = global::MemTracker.Properties.Resources.AllocationsWindow;

            return window;
        }

        private DockableWindow CreateMemoryLayoutWindow( MemoryLayoutControlSettings memoryLayoutSettings )
        {
            Guid guid;
            int windowNumber;
            if ( (memoryLayoutSettings != null) && (memoryLayoutSettings.Index != -1) )
            {
                guid = new Guid( memoryLayoutSettings.Guid );

                windowNumber = memoryLayoutSettings.Index;
                if ( windowNumber > m_memoryLayoutWindowCount )
                {
                    m_memoryLayoutWindowCount = windowNumber;
                }
            }
            else
            {
                guid = Guid.NewGuid();

                ++m_memoryLayoutWindowCount;
                windowNumber = m_memoryLayoutWindowCount;
            }

            MemoryLayoutControl memoryLayout = new MemoryLayoutControl();
            memoryLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            memoryLayout.Location = new System.Drawing.Point( 0, 0 );
            memoryLayout.Name = "memoryLayoutControl" + windowNumber;
            memoryLayout.ParentControl = this;
            memoryLayout.ShowSizingGrip = false;
            memoryLayout.Size = new System.Drawing.Size( 843, 349 );
            memoryLayout.TabIndex = 0;
            memoryLayout.ThreadingEnabled = true;
            memoryLayout.Tracker = this.trackerComponent1;
            memoryLayout.UpdateBegin += new UpdateBeginEventHandler( memoryLayoutControl1_UpdateBegin );
            memoryLayout.UpdateComplete += new UpdateCompleteEventHandler( MemTrackerControl_UpdateComplete );

            if ( memoryLayoutSettings != null )
            {
                SetMemoryLayoutSettings( memoryLayoutSettings, memoryLayout );
            }

            memoryLayout.GenerateReportButtonEnabled = (m_playbackState != PlaybackState.Stopped) && (m_messageCount > 0);
            memoryLayout.RefeshButtonEnabled = memoryLayout.GenerateReportButtonEnabled;

            DockableWindow window = new DockableWindow( this.sandDockManager1, memoryLayout, String.Format( "Memory Layout ({0})", windowNumber ) );
            window.CloseAction = DockControlCloseAction.Dispose;
            window.Controls.Add( memoryLayout );
            window.FloatingSize = new System.Drawing.Size( 1000, 600 );
            window.Guid = guid;
            window.Location = new System.Drawing.Point( 0, 16 );
            window.Name = "memoryLayoutDockableWindow" + windowNumber;
            window.ShowOptions = false;
            window.Size = new System.Drawing.Size( 800, 400 );
            window.TabImage = global::MemTracker.Properties.Resources.MemoryLayoutWindow;
            window.TabIndex = 0;

            return window;
        }

        private void SkipBackward()
        {
            BeginReadMessage();

            for ( int i = 0; i < Settings.Default.SkipSize; ++i )
            {
                if ( !ReadLineBackward() )
                {
                    break;
                }
            }

            EndReadMessage();

            this.State = PlaybackState.Paused;
        }

        private void StepBackward()
        {
            BeginReadMessage();
            ReadLineBackward();
            EndReadMessage();

            this.State = PlaybackState.Paused;
        }

        private void Pause()
        {
            this.State = PlaybackState.Paused;
        }

        private void Play()
        {
            this.State = PlaybackState.Play;
        }

        private void StepForward()
        {
            BeginReadMessage();
            ReadLineForward();
            EndReadMessage();

            this.State = PlaybackState.Paused;
        }

        private void SkipForward()
        {
            BeginReadMessage();

            for ( int i = 0; i < Settings.Default.SkipSize; ++i )
            {
                if ( !ReadLineForward() )
                {
                    break;
                }
            }

            EndReadMessage();

            this.State = PlaybackState.Paused;
        }
        #endregion

        #region TrackerComponent Event Handlers
        private void trackerComponent1_DuplicateAllocation( object sender, DuplicateAllocationEventArgs e )
        {
            PlaybackState oldState = this.State;
            oldState = PlaybackState.Paused;

            DuplicateAllocationForm dupForm = DuplicateAllocationForm.Create( e.TheAllocation );
            Form1.CenterLocation( this, dupForm );

            e.ActionToTake = dupForm.ShowIt( this );

            this.State = oldState;
        }

        private void trackerComponent1_ReportAdded( object sender, ReportChangedEventArgs e )
        {
            m_goToOptions.Reports.Add( e.Report );

            ToolStripMenuItem item = new ToolStripMenuItem( e.Report.Name );
            item.DisplayStyle = ToolStripItemDisplayStyle.Text;
            item.Tag = e.Report;
            item.Click += new EventHandler( fileSaveReportItemToolStripMenuItem_Click );
            
            this.fileSaveReportToolStripMenuItem.DropDownItems.Add( item );
        }

        private void trackerComponent1_ReportRemoved( object sender, ReportChangedEventArgs e )
        {
            for ( int i = 0; i < this.fileSaveReportToolStripMenuItem.DropDownItems.Count; ++i )
            {
                if ( this.fileSaveReportToolStripMenuItem.DropDownItems[i].Text == e.Report.Name )
                {
                    this.fileSaveReportToolStripMenuItem.DropDownItems.RemoveAt( i );
                    break;
                }
            }
        }

        private void trackerComponent1_StackAdded( object sender, StackChangedEventArgs e )
        {
            if ( !m_goToOptions.SelectedStacks.ContainsKey( e.HashKey ) )
            {
                m_goToOptions.SelectedStacks.Add( e.HashKey, false );
            }
        }
        #endregion

        #region MemTrackerControl Event Handlers
        private void overviewListControl1_UpdateBegin( object sender, UpdateBeginEventArgs e )
        {
            RefreshProgressForm form = new RefreshProgressForm( sender as OverviewListControl, e.NumSteps );
            Form1.CenterLocation( this, form );

            form.ShowDialog( this );
        }

        private void allocationsTreeListControl1_UpdateBegin( object sender, UpdateBeginEventArgs e )
        {
            RefreshProgressForm form = new RefreshProgressForm( sender as AllocationsTreeListControl, e.NumSteps );
            Form1.CenterLocation( this, form );

            form.ShowDialog( this );
        }

        private void memoryLayoutControl1_UpdateBegin( object sender, UpdateBeginEventArgs e )
        {
            RefreshProgressForm form = new RefreshProgressForm( sender as MemoryLayoutControl, e.NumSteps );
            Form1.CenterLocation( this, form );

            form.ShowDialog( this );
        }

        private void allocationsTreeListControl1_OutOfMemoryError( object sender, MemTrackerControls.ErrorEventArgs e )
        {
            MessageBox.Show( this, e.Message, "Out of Memory",
                MessageBoxButtons.OK, MessageBoxIcon.Stop );
        }

        private void MemTrackerControl_UpdateComplete( object sender, UpdateCompleteEventArgs e )
        {
            if ( e.Success && (e.ReportGenerated != null) )
            {
                m_previousMessageLine = m_currentMessageLine;
                m_currentMessageLine = new MessageLine( m_messageCount, c_reportGeneratedString + e.ReportGenerated.Name, MessageLine.FileFlag.None );

                // add message
                this.messagesListBox.Items.Add( m_currentMessageLine );
                SendMessage( this.messagesListBox.Handle, WM_VSCROLL, (IntPtr)SB_BOTTOM, IntPtr.Zero );

                ++m_messageCount;
                this.messageCountToolStripStatusLabel.Text = "Message Count:  " + m_messageCount;

                // fixup indexes
                if ( m_nextMessageLine != null )
                {
                    ++m_nextMessageLine.Index;
                }

                MessageLine[] lines = m_nextMessageLinesStack.ToArray();
                m_nextMessageLinesStack.Clear();

                for ( int i = lines.Length - 1; i >= 0; --i )
                {
                    ++lines[i].Index;
                    m_nextMessageLinesStack.Push( lines[i] );
                }

                this.AllowRefresh = true;
            }
        }
        #endregion

        #region Timer Event Handlers
        private void timer1_Tick( object sender, EventArgs e )
        {
            BeginReadMessage();

            for ( int i = 0; i < m_numberReadsPerTimerUpdate; ++i )
            {
                if ( !ReadLineForward() )
                {
                    this.State = PlaybackState.Paused;
                    break;
                }
            }

            EndReadMessage();
        }
        #endregion

        #region Tracker File
        private bool OpenTrackerFile( string filename )
        {
            if ( this.CurrentTrackerFilename != string.Empty )
            {
                CloseTrackerFile();
            }

            bool success = true;

            int indexOf = filename.IndexOf( '+' );
            if ( indexOf == -1 )
            {
                success = OpenSingleTrackerFile( filename );
            }
            else
            {
                string prefix = filename.Substring( 0, indexOf );
                string postfix = filename.Substring( indexOf + 1 );
                success = OpenMultiTrackerFile( prefix, postfix );
            }

            if ( success )
            {
                // determine if it is a valid file.
                string[] nextLineSplit = m_nextMessageLine.Text.Split( new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries );
                if ( nextLineSplit.Length >= 2 )
                {
                    if ( (nextLineSplit[1] != "Pl") || (nextLineSplit.Length < 4 + this.trackerComponent1.AvailableMemory.Length) )
                    {
                        MessageBox.Show( this, "'" + filename + "' was generated from a version of Rage earlier than 1.9 or WR219\n\nUnable to load the file.",
                            "Old File Version Detected", MessageBoxButtons.OK, MessageBoxIcon.Exclamation );
                        return false;
                    }
                }
                else
                {
                    MessageBox.Show( this, "'" + filename + "' does not appear to be a tracker file.\n\nUnable to load the file.",
                        "Invalid Tracker File", MessageBoxButtons.OK, MessageBoxIcon.Exclamation );
                    return false;
                }

                DockControl[] windows = this.sandDockManager1.GetDockControls();
                if ( windows.Length == 1 )
                {                    
                    DockContainer container = this.sandDockManager1.FindDockContainer( ContainerDockLocation.Left );
                    if ( container == null )
                    {
                        container = this.sandDockManager1.CreateNewDockContainer( ContainerDockLocation.Left, ContainerDockEdge.Inside, 800 );
                        container.Size = new Size( 800, 400 );
                    }

                    DockableWindow overviewWindow = CreateOverviewWindow( m_defaultOverviewSettings );
                    CenterLocation( this, overviewWindow );
                    overviewWindow.OpenDocked( ContainerDockLocation.Left );

                    DockableWindow allocationsWindow = CreateAllocationsWindow( m_defaultAllocationsSettings );
                    CenterLocation( this, allocationsWindow );
                    allocationsWindow.OpenWith( overviewWindow );

                    DockableWindow memoryLayoutWindow = CreateMemoryLayoutWindow( m_defaultMemoryLayoutSettings );
                    CenterLocation( this, memoryLayoutWindow );
                    memoryLayoutWindow.OpenWith( allocationsWindow );
                }

                this.Text = "MemTracker " + this.NextTrackerFilename;
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool OpenSingleTrackerFile( string filename )
        {
            m_trackerFilePrefix = filename;
            m_trackerFilePostfix = null;
            m_multiTrackerFileIndex = -1;
            m_isMultiTrackerFile = false;

            if ( !File.Exists( filename ) )
            {
                MessageBox.Show( this, "Unable to open tracker file '" + filename + "'.",
                    "File Doesn't Exist", MessageBoxButtons.OK, MessageBoxIcon.Error );

                CloseTrackerFile();
                return false;
            }

            if ( OpenTrackerFileStream( filename, ref m_nextTrackerFileStream, ref m_nextTrackerFileTextReader ) )
            {
                if ( ReadNextMessageLine() )
                {
                    m_nextMessageLine.IsStartFile = true;
                }

                this.State = PlaybackState.Paused;
                return true;
            }
            else
            {
                RemoveRecentFile( filename );
                return false;
            }
        }

        private bool OpenMultiTrackerFile( string prefix, string postfix )
        {
            m_trackerFilePrefix = prefix;
            m_trackerFilePostfix = postfix;
            m_multiTrackerFileIndex = -1;
            m_isMultiTrackerFile = true;

            string filename = this.NextTrackerFilename;
            if ( !File.Exists( filename ) )
            {
                MessageBox.Show( this, "Unable to open tracker file '" + filename + "'.",
                    "File Doesn't Exist", MessageBoxButtons.OK, MessageBoxIcon.Error );

                CloseTrackerFile();
                return false;
            }

            if ( OpenTrackerFileStream( filename, ref m_nextTrackerFileStream, ref m_nextTrackerFileTextReader ) )
            {
                if ( ReadNextMessageLine() )
                {
                    m_nextMessageLine.IsStartFile = true;
                }

                this.State = PlaybackState.Paused;
                return true;
            }
            else
            {
                RemoveRecentFile( Form1.BuildTrackerFilename( prefix, postfix, -1 ) );
                return false;
            }
        }

        private bool OpenTrackerFileStream( string filename, ref FileStream stream, ref TextReader reader )
        {
            if ( reader != null )
            {
                reader.Close();
                reader = null;
            }

            try
            {
                stream = new FileStream( filename, FileMode.Open, FileAccess.Read, FileShare.ReadWrite );
                reader = new StreamReader( stream );
            }
            catch ( Exception e )
            {
                DialogResult result = MessageBox.Show( this, "Unable to open tracker file '" + filename + "'.\n\n" + e.Message, "Open File Failure",
                    MessageBoxButtons.RetryCancel, MessageBoxIcon.Error );
                if ( result == DialogResult.Retry )
                {
                    return OpenTrackerFileStream( filename, ref stream, ref reader );
                }
                else
                {
                    return false;
                }
            }            

            return true;
        }

        private void CloseTrackerFile()
        {
            this.State = PlaybackState.Stopped;

            this.fileSaveReportToolStripMenuItem.DropDownItems.Clear();

            DockControl[] windows = this.sandDockManager1.GetDockControls();
            foreach ( DockControl window in windows )
            {
                if ( window != this.messagesDockableWindow )
                {
                    window.Close();
                }
            }

            this.messagesListBox.Items.Clear();
            
            this.trackerComponent1.Reset();

            AddRecentFile( Form1.BuildTrackerFilename( m_trackerFilePrefix, m_trackerFilePostfix, -1 ) );

            m_trackerFilePrefix = null;
            m_trackerFilePostfix = null;
            m_multiTrackerFileIndex = -1;
            m_messageCount = 0;
            m_messageCountBeforeRead = 0;

            m_overviewWindowCount = 0;
            m_allocationsWindowCount = 0;
            m_memoryLayoutWindowCount = 0;

            m_previousMessageLine = null;
            m_currentMessageLine = null;
            m_nextMessageLine = null;
            m_nextMessageLinesStack.Clear();

            if ( m_previousTrackerFileTextReader != null )
            {
                m_previousTrackerFileTextReader.Close();
                m_previousTrackerFileTextReader = null;
                m_previousTrackerFileStream = null;
            }

            if ( m_currentTrackerFileTextReader != null )
            {
                m_currentTrackerFileTextReader.Close();
                m_currentTrackerFileTextReader = null;
                m_currentTrackerFileStream = null;
            }

            if ( m_nextTrackerFileTextReader != null )
            {
                m_nextTrackerFileTextReader.Close();
                m_nextTrackerFileTextReader = null;
                m_nextTrackerFileStream = null;
            }

            this.Text = "MemTracker";
            this.messageCountToolStripStatusLabel.Text = "Message Count:";
        }        

        /// <summary>
        /// Parses m_nextMessageLine, adds the appropriate action to the <see cref="TrackerComponent"/>, and adds a new MessageLine.
        /// </summary>
        /// <returns><c>true</c> if it could read the next message line, otherwise <c>false</c>.</returns>
        private bool ReadLineForward()
        {
            if ( (m_nextTrackerFileTextReader == null) || (m_nextMessageLine == null) )
            {
                this.State = PlaybackState.Paused;
                return false;
            }

            // parse the information.
            if ( m_nextMessageLine.Text.StartsWith( c_reportGeneratedString ) )
            {
                string reportName = m_nextMessageLine.Text.Substring( c_reportGeneratedString.Length );
                this.trackerComponent1.AddReportAction( 0, reportName, 
                    this.trackerComponent1.AvailableMemory, this.trackerComponent1.TrackDeallocations );
            }
            else
            {
                string[] sectionSplit = m_nextMessageLine.Text.Split( new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries );
                if ( sectionSplit.Length >= 2 )
                {
                    switch ( sectionSplit[1] )
                    {
                        case "Pl":
                            if ( sectionSplit.Length >= 4 + (int)Allocation.MemoryTypeIndex.NumIndexes )
                            {
                                uint threadID = uint.Parse( sectionSplit[0] );
                                string platform = sectionSplit[2];

                                uint[] initialAvailableMemory = new uint[(int)Allocation.MemoryTypeIndex.NumIndexes];
                                for ( int i = 0; i < this.trackerComponent1.AvailableMemory.Length; ++i )
                                {
                                    initialAvailableMemory[i] = uint.Parse( sectionSplit[i + 3] );
                                }

                                bool trackDeallocations = bool.Parse( sectionSplit[(int)Allocation.MemoryTypeIndex.NumIndexes + 3] );

                                this.trackerComponent1.SetAvailableMemory( threadID, initialAvailableMemory, false );

                                this.trackerComponent1.Platform = platform;
                                this.trackerComponent1.TrackDeallocations = trackDeallocations;
                            }
                            break;
                        case "Pu":
                            if ( sectionSplit.Length >= 3 )
                            {
                                uint threadID = uint.Parse( sectionSplit[0] );
                                string stackName = sectionSplit[2];

                                this.trackerComponent1.PushStackAction( threadID, stackName );
                            }
                            break;
                        case "Po":
                            if ( sectionSplit.Length >= 2 )
                            {
                                uint threadID = uint.Parse( sectionSplit[0] );

                                this.trackerComponent1.PopStackAction( threadID );
                            }
                            break;
                        case "Ta":
                            if ( sectionSplit.Length >= 5 )
                            {
                                uint threadID = uint.Parse( sectionSplit[0] );
                                uint decimalAddress = uint.Parse( sectionSplit[2], System.Globalization.NumberStyles.HexNumber );
                                uint size = uint.Parse( sectionSplit[3] );
                                int memoryType = int.Parse( sectionSplit[4] );

                                this.trackerComponent1.TallyAllocationAction( threadID, decimalAddress, memoryType, size );
                            }
                            break;
                        case "Un":
                            if ( sectionSplit.Length >= 4 )
                            {
                                uint threadID = uint.Parse( sectionSplit[0] );
                                uint decimalAddress = uint.Parse( sectionSplit[2], System.Globalization.NumberStyles.HexNumber );
                                uint size = uint.Parse( sectionSplit[3] );

                                this.trackerComponent1.UntallyAllocationAction( threadID, decimalAddress, size );
                            }
                            break;
                        case "In":
                            if ( sectionSplit.Length >= 7 )
                            {
                                uint threadID = uint.Parse( sectionSplit[0] );
                                uint decimalAddress = uint.Parse( sectionSplit[2], System.Globalization.NumberStyles.HexNumber );
                                uint size = uint.Parse( sectionSplit[3] );
                                string datatype = sectionSplit[4];
                                string filename = sectionSplit[5];
                                int lineNumber = int.Parse( sectionSplit[6] );

                                uint dataTypeHash = (uint)datatype.GetHashCode();
                                uint fileNameHash = (uint)filename.GetHashCode();

                                this.trackerComponent1.AddDataType( datatype, dataTypeHash );
                                this.trackerComponent1.AddFilename( filename, fileNameHash );
                                this.trackerComponent1.AddInfoAction( threadID, decimalAddress, size, dataTypeHash, fileNameHash, lineNumber );
                            }
                            break;
                        case "Re":
                            if ( sectionSplit.Length >= 9 )
                            {
                                uint threadID = uint.Parse( sectionSplit[0] );
                                string reportName = sectionSplit[2];

                                uint[] available = new uint[(int)Allocation.MemoryTypeIndex.NumIndexes];
                                for ( int i = 0; i < available.Length; ++i )
                                {
                                    available[i] = uint.Parse( sectionSplit[i + 3] );
                                }

                                bool trackedDeallocations = bool.Parse( sectionSplit[(int)Allocation.MemoryTypeIndex.NumIndexes + 3] );

                                this.trackerComponent1.AddReportAction( threadID, reportName, available, trackedDeallocations );
                            }
                            break;
                    }
                }
            }

            if ( (m_previousMessageLine != null) && m_previousMessageLine.IsEndFile )
            {
                m_previousTrackerFileTextReader.Close();
                m_previousTrackerFileTextReader = null;
                m_previousTrackerFileStream = null;
            }

            m_previousTrackerFileStream = m_currentTrackerFileStream;
            m_previousTrackerFileTextReader = m_currentTrackerFileTextReader;
            m_previousMessageLine = m_currentMessageLine;

            m_currentTrackerFileStream = m_nextTrackerFileStream;
            m_currentTrackerFileTextReader = m_nextTrackerFileTextReader;
            m_currentMessageLine = m_nextMessageLine;

            // add message
            this.messagesListBox.Items.Add( m_currentMessageLine );

            if ( m_currentMessageLine.IsStartFile )
            {
                ++m_multiTrackerFileIndex;
            }

            ++m_messageCount;

            this.AllowRefresh = true;

            // read next line
            return ReadNextMessageLine();
        }

        /// <summary>
        /// Parses m_previousMessageLine, removes the appropriate action from <see cref="TrackerComponent"/>, and removes the last MessageLine.
        /// </summary>
        /// <returns><c>true</c> if it could read the previous message line, otherwise <c>false</c>.</returns>
        private bool ReadLineBackward()
        {
            if ( (m_previousTrackerFileTextReader == null) || (m_previousMessageLine == null) )
            {
                this.State = PlaybackState.Paused;
                return false;
            }

            // parse the information.
            if ( m_currentMessageLine.Text.StartsWith( c_reportGeneratedString ) )
            {
                this.trackerComponent1.RemoveLastAction();
            }
            else
            {
                string[] sectionSplit = m_currentMessageLine.Text.Split( new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries );
                if ( sectionSplit.Length >= 2 )
                {
                    TrackerAction lastAction = this.trackerComponent1.Actions[this.trackerComponent1.Actions.Count - 1];
                    switch ( sectionSplit[1] )
                    {
                        case "Pl":
                            break;
                        case "Pu":
                            if ( lastAction.TheAction == TrackerAction.Action.PushStack )
                            {
                                this.trackerComponent1.RemoveLastAction();
                            }
                            break;
                        case "Po":
                            if ( lastAction.TheAction == TrackerAction.Action.PopStack )
                            {
                                this.trackerComponent1.RemoveLastAction();
                            }
                            break;
                        case "Ta":
                            if ( lastAction.TheAction == TrackerAction.Action.TallyAllocation )
                            {
                                this.trackerComponent1.RemoveLastAction();
                            }
                            break;
                        case "Un":
                            if ( lastAction.TheAction == TrackerAction.Action.UntallyAllocation )
                            {
                                this.trackerComponent1.RemoveLastAction();
                            }
                            break;
                        case "Re":
                            if ( lastAction.TheAction == TrackerAction.Action.Report )
                            {
                                this.trackerComponent1.RemoveLastAction();
                            }
                            break;
                        case "In":
                            if ( sectionSplit.Length >= 7 )
                            {
                                uint decimalAddress = uint.Parse( sectionSplit[2], System.Globalization.NumberStyles.HexNumber );

                                this.trackerComponent1.RemoveInfoAction( decimalAddress );
                            }
                            break;
                        case "CE":
                            if ( lastAction.TheAction == TrackerAction.Action.CountError )
                            {
                                this.trackerComponent1.RemoveLastAction();
                            }
                            break;
                        case "TE":
                            if ( lastAction.TheAction == TrackerAction.Action.TallyError )
                            {
                                this.trackerComponent1.RemoveLastAction();
                            }
                            break;
                        case "UE":
                            if ( lastAction.TheAction == TrackerAction.Action.UntallyError )
                            {
                                this.trackerComponent1.RemoveLastAction();
                            }
                            break;
                        case "ME":
                            if ( lastAction.TheAction == TrackerAction.Action.MemoryError )
                            {
                                this.trackerComponent1.RemoveLastAction();
                            }
                            break;
                    }
                }
            }

            if ( m_nextMessageLine != null )
            {
                if ( m_nextMessageLine.IsStartFile )
                {
                    m_nextTrackerFileTextReader.Close();
                    m_nextTrackerFileTextReader = null;
                    m_nextTrackerFileStream = null;

                    m_nextMessageLinesStack.Clear();
                }
                else
                {
                    m_nextMessageLinesStack.Push( m_nextMessageLine );
                }                
            }

            m_nextTrackerFileStream = m_currentTrackerFileStream;
            m_nextTrackerFileTextReader = m_currentTrackerFileTextReader;
            m_nextMessageLine = m_currentMessageLine;

            m_currentTrackerFileStream = m_previousTrackerFileStream;
            m_currentTrackerFileTextReader = m_previousTrackerFileTextReader;
            m_currentMessageLine = m_previousMessageLine;

            // remove message
            this.messagesListBox.Items.RemoveAt( this.messagesListBox.Items.Count - 1 );

            if ( m_currentMessageLine.IsEndFile )
            {
                --m_multiTrackerFileIndex;
            }

            --m_messageCount;

            this.AllowRefresh = m_messageCount > 0;

            return ReadPreviousMessageLine();
        }

        /// <summary>
        /// Reads the next message line from the file.  If we're in a mulit-file situation and a file is finished, opens the next file and reads the first line.
        /// </summary>
        /// <returns><c>true</c> if m_nextMessageLine is not null, otherwise <c>false</c> indicating the end of input.</returns>
        private bool ReadNextMessageLine()
        {
            m_nextMessageLine = null;

            if ( m_nextMessageLinesStack.Count > 0 )
            {
                m_nextMessageLine = m_nextMessageLinesStack.Pop();

                if ( m_isMultiTrackerFile && m_nextMessageLine.IsStartFile )
                {
                    string filename = this.NextTrackerFilename;
                    if ( File.Exists( filename ) )
                    {
                        m_nextTrackerFileTextReader = null;
                        m_nextTrackerFileStream = null;

                        if ( OpenTrackerFileStream( filename, ref m_nextTrackerFileStream, ref m_nextTrackerFileTextReader ) )
                        {
                            if ( ReadNextMessageLine() )
                            {
                                m_nextMessageLine.IsStartFile = true;
                            }
                        }
                        else
                        {
                            m_nextTrackerFileTextReader = null;
                            m_nextTrackerFileStream = null;

                            // reached end of input
                            this.State = PlaybackState.Paused;
                        }
                    }

                    m_nextMessageLinesStack.Clear();
                }
            }
            else
            {
                string nextLine = m_nextTrackerFileTextReader.ReadLine();
                if ( nextLine == null )
                {
                    if ( m_isMultiTrackerFile )
                    {
                        if ( m_currentMessageLine != null )
                        {
                            m_currentMessageLine.IsEndFile = true;
                        }

                        string filename = this.NextTrackerFilename;
                        if ( File.Exists( filename ) )
                        {
                            m_nextTrackerFileTextReader = null;
                            m_nextTrackerFileStream = null;

                            if ( OpenTrackerFileStream( filename, ref m_nextTrackerFileStream, ref m_nextTrackerFileTextReader ) 
                                && ReadNextMessageLine() )
                            {
                                m_nextMessageLine.IsStartFile = true;
                            }
                            else
                            {
                                m_nextTrackerFileTextReader = null;
                                m_nextTrackerFileStream = null;

                                // reached end of input
                                this.State = PlaybackState.Paused;
                            }
                        }
                    }
                    else
                    {
                        m_nextTrackerFileTextReader = null;
                        m_nextTrackerFileStream = null;

                        // reached end of input
                        this.State = PlaybackState.Paused;
                    }
                }
                else
                {
                    m_nextMessageLine = new MessageLine( m_messageCount, nextLine.Trim(), MessageLine.FileFlag.None );
                }
            }

            return m_nextMessageLine != null;
        }

        /// <summary>
        /// Reads the previous message line from the messagesListBox.  If that line marks the end of a file, the previous tracker file is opened.
        /// </summary>
        /// <returns><c>true</c> if m_previousMessageLine is not null, otherwise <c>false</c> indicating the beginning of input.</returns>
        private bool ReadPreviousMessageLine()
        {
            m_previousMessageLine = null;
            if ( this.messagesListBox.Items.Count > 1 )
            {
                m_previousMessageLine = this.messagesListBox.Items[this.messagesListBox.Items.Count - 2] as MessageLine;

                if ( m_isMultiTrackerFile && m_previousMessageLine.IsEndFile )
                {
                    string filename = this.PreviousTrackerFilename;
                    if ( (filename != string.Empty) && File.Exists( filename ) )
                    {
                        m_previousTrackerFileStream = null;
                        m_previousTrackerFileTextReader = null;

                        if ( OpenTrackerFileStream( filename, ref m_previousTrackerFileStream, ref m_previousTrackerFileTextReader ) )
                        {
                            // read to end so next call to ReadLine() will return null
                            m_previousTrackerFileTextReader.ReadToEnd();
                        }
                    }
                }
            }
            else
            {
                m_previousTrackerFileStream = null;
                m_previousTrackerFileTextReader = null;

                // reached start of input
                this.State = PlaybackState.Paused;
            }

            return m_previousMessageLine != null;
        }
        #endregion

        #region Save/Load
        private void SaveLayout()
        {
            // set this so we know when we can call Upgrade()
            Settings.Default.UpgradeNeeded = "no";

            // Screen resolution
            Settings.Default.ScreenSize = Screen.GetWorkingArea( this ).Size;

            // Form
            Settings.Default.FormLocation = this.Location;
            Settings.Default.FormSize = this.Size;
            Settings.Default.FormWindowState = this.WindowState;

            // Last File
            if ( Settings.Default.LoadLastFile )
            {
                StringBuilder filename = new StringBuilder();
                if ( (m_trackerFilePrefix != null) && (m_trackerFilePrefix != string.Empty) )
                {
                    filename.Append( m_trackerFilePrefix );

                    if ( (m_trackerFilePostfix != null) && (m_trackerFilePostfix != string.Empty) )
                    {
                        filename.Append( '+' );
                        filename.Append( m_trackerFilePostfix );
                    }
                }

                Settings.Default.LastFileLoaded = filename.ToString();
            }

            // Default MemTrackerControlSettings
            Settings.Default.DefaultOverviewSettings = (m_defaultOverviewSettings != null) ? m_defaultOverviewSettings.Serialize() : string.Empty;
            Settings.Default.DefaultAllocationsSettings = (m_defaultAllocationsSettings != null) ? m_defaultAllocationsSettings.Serialize() : string.Empty;
            Settings.Default.DefaultMemoryLayoutSettings = (m_defaultMemoryLayoutSettings != null) ? m_defaultMemoryLayoutSettings.Serialize() : string.Empty;

            // MemTrackerControlSettings
            System.Collections.Specialized.StringCollection overviewSettingsStrings = new System.Collections.Specialized.StringCollection();
            System.Collections.Specialized.StringCollection allocationsSettingsStrings = new System.Collections.Specialized.StringCollection();
            System.Collections.Specialized.StringCollection memoryLayoutSettingsStrings = new System.Collections.Specialized.StringCollection();

            DockControl[] windows = this.sandDockManager1.GetDockControls();
            foreach ( DockControl window in windows )
            {
                if ( window == this.messagesDockableWindow )
                {
                    continue;
                }

                int index = int.Parse( window.Name.Substring( window.Name.Length - 1 ) );

                Control control = window.Controls[0];
                if ( control is OverviewListControl )
                {
                    OverviewListControlSettings overviewSettings = new OverviewListControlSettings( window.Guid.ToString(),
                        index, control as OverviewListControl );
                    
                    overviewSettingsStrings.Add( overviewSettings.Serialize() );
                }
                else if ( control is AllocationsTreeListControl )
                {
                    AllocationsTreeListControlSettings allocationsSettings = new AllocationsTreeListControlSettings( window.Guid.ToString(),
                        index, control as AllocationsTreeListControl );

                    allocationsSettingsStrings.Add( allocationsSettings.Serialize() );
                }
                else if ( control is MemoryLayoutControl )
                {
                    MemoryLayoutControlSettings memoryLayoutSettings = new MemoryLayoutControlSettings( window.Guid.ToString(),
                        index, control as MemoryLayoutControl );

                    memoryLayoutSettingsStrings.Add( memoryLayoutSettings.Serialize() );
                }
            }

            Settings.Default.OverviewSettings = overviewSettingsStrings;
            Settings.Default.AllocationsSettings = allocationsSettingsStrings;
            Settings.Default.MemoryLayoutSettings = memoryLayoutSettingsStrings;

            // Layout
            Settings.Default.SandDockLayout = this.sandDockManager1.GetLayout();

            // Recent Files (collect in reverse order because of how AddRecentFile works)
            System.Collections.Specialized.StringCollection recentFiles = new System.Collections.Specialized.StringCollection();
            for ( int i = this.fileRecentFilesToolStripMenuItem.DropDownItems.Count - 1; i >= 0; --i )
            {
                recentFiles.Add( this.fileRecentFilesToolStripMenuItem.DropDownItems[i].Text );
            }
            Settings.Default.RecentFiles = recentFiles;
            
            // ToolStrips
            ToolStripLocation loc = CreateToolStripLocation( this.newWindowsToolStrip );
            Settings.Default.NewWindowToolStripLocation = loc.Location;
            Settings.Default.NewWindowToolStripDock = loc.Dock;

            loc = CreateToolStripLocation( this.playbackToolStrip );
            Settings.Default.PlaybackToolStripLocation = loc.Location;
            Settings.Default.PlaybackToolStripDock = loc.Dock;

            // GoToOptions
            Settings.Default.GoToOptions = m_goToOptions.Serialize();

            // now save it
            int retryCount = 10;
            do
            {
                try
                {
                    Settings.Default.Save();
                    break;
                }
                catch
                {
                    --retryCount;
                    System.Threading.Thread.Sleep( 100 );
                }
            }
            while ( retryCount > 0 );
        }

        private void LoadLayout()
        {
            // ensure the window handle has been created before setting the window position
            int handle = (int)this.Handle;

            // decide when to upgrade
            if ( Settings.Default.UpgradeNeeded == string.Empty )
            {
                Settings.Default.Upgrade();
            }

            // Form
            Point loc = Settings.Default.FormLocation;
            Size size = Settings.Default.FormSize;
            rageImage.AdjustForResolution( Settings.Default.ScreenSize, Screen.GetWorkingArea( this ), ref loc, ref size );

            this.Location = loc;
            this.Size = size;
            this.WindowState = Settings.Default.FormWindowState;

            // Playback
            this.PlaybackRate = Settings.Default.PlaybackRate;

            // Last File (if we didn't load one off the command line)
            if ( Settings.Default.LoadLastFile && (m_trackerFilePrefix == null) )
            {
                string filename = Settings.Default.LastFileLoaded;
                if ( filename != string.Empty )
                {
                    OpenTrackerFile( filename );
                }
            }

            // Default MemTrackerControlSettings (for new windows)
            string overviewSettingsString = Settings.Default.DefaultOverviewSettings;
            if ( overviewSettingsString != string.Empty )
            {
                OverviewListControlSettings overviewSettings = new OverviewListControlSettings();
                if ( overviewSettings.Deserialize( overviewSettingsString ) )
                {
                    m_defaultOverviewSettings = overviewSettings;
                    SetOverviewSettings( m_defaultOverviewSettings, this.overviewListControl1 );
                }
            }

            string allocationsSettingsString = Settings.Default.DefaultAllocationsSettings;
            if ( allocationsSettingsString != string.Empty )
            {
                AllocationsTreeListControlSettings allocationsSettings = new AllocationsTreeListControlSettings();
                if ( allocationsSettings.Deserialize( allocationsSettingsString ) )
                {
                    m_defaultAllocationsSettings = allocationsSettings;
                    SetAllocationsSettings( m_defaultAllocationsSettings, this.allocationsTreeListControl1 );                    
                }
            }

            string memoryLayoutSettingsString = Settings.Default.DefaultMemoryLayoutSettings;
            if ( memoryLayoutSettingsString != string.Empty )
            {
                MemoryLayoutControlSettings memoryLayoutSettings = new MemoryLayoutControlSettings();
                if ( memoryLayoutSettings.Deserialize( memoryLayoutSettingsString ) )
                {
                    m_defaultMemoryLayoutSettings = memoryLayoutSettings;
                    SetMemoryLayoutSettings( m_defaultMemoryLayoutSettings, this.memoryLayoutControl1 );
                }
            }

            // MemTrackerControlSettings
            m_overviewSettings.Clear();
            System.Collections.Specialized.StringCollection settingStrings = Settings.Default.OverviewSettings;
            if ( settingStrings != null )
            {
                foreach ( string settingString in settingStrings )
                {
                    OverviewListControlSettings overviewSettings = new OverviewListControlSettings();
                    if ( overviewSettings.Deserialize( settingString ) )
                    {
                        if ( overviewSettings.Guid == this.overviewDockableWindow1.Guid.ToString() )
                        {
                            SetOverviewSettings( overviewSettings, this.overviewListControl1 );
                        }
                        else
                        {
                            m_overviewSettings.Add( overviewSettings );
                        }
                    }
                }
            }

            m_allocationsSettings.Clear();
            settingStrings = Settings.Default.AllocationsSettings;
            if ( settingStrings != null )
            {
                foreach ( string settingString in settingStrings )
                {
                    AllocationsTreeListControlSettings allocationsSettings = new AllocationsTreeListControlSettings();
                    if ( allocationsSettings.Deserialize( settingString ) )
                    {
                        if ( allocationsSettings.Guid == this.allocationsDockableWindow1.Guid.ToString() )
                        {
                            SetAllocationsSettings( allocationsSettings, this.allocationsTreeListControl1 );
                        }
                        else
                        {
                            m_allocationsSettings.Add( allocationsSettings );
                        }
                    }
                }
            }

            m_memoryLayoutSettings.Clear();
            settingStrings = Settings.Default.MemoryLayoutSettings;
            if ( settingStrings != null )
            {
                foreach ( string settingString in settingStrings )
                {
                    MemoryLayoutControlSettings memoryLayoutSettings = new MemoryLayoutControlSettings();
                    if ( memoryLayoutSettings.Deserialize( settingString ) )
                    {
                        if ( memoryLayoutSettings.Guid == this.memoryLayoutDockableWindow1.Guid.ToString() )
                        {
                            SetMemoryLayoutSettings( memoryLayoutSettings, this.memoryLayoutControl1 );
                        }
                        else
                        {
                            m_memoryLayoutSettings.Add( memoryLayoutSettings );
                        }
                    }
                }
            }

            // Layout
            string layout = Settings.Default.SandDockLayout;
            if ( layout != string.Empty )
            {
                this.sandDockManager1.SetLayout( layout );

                // ensure all windows are at least partially visible on screen
                DockControl[] dockControls = this.sandDockManager1.GetDockControls();
                foreach ( DockControl dockControl in dockControls )
                {
                    Point windowLoc = dockControl.FloatingLocation;
                    Size windowSize = dockControl.FloatingSize;
                    rageImage.AdjustForResolution( Settings.Default.ScreenSize, Screen.GetWorkingArea( this ),
                        ref windowLoc, ref windowSize );

                    dockControl.FloatingLocation = windowLoc;
                    dockControl.FloatingSize = windowSize;
                }
            }

            // Recent Files
            System.Collections.Specialized.StringCollection recentFiles = Settings.Default.RecentFiles;
            if ( recentFiles != null )
            {
                foreach ( string recentFile in recentFiles )
                {
                    AddRecentFile( recentFile );
                }
            }

            // ToolStrips
            List<ToolStripLocation> locations = new List<ToolStripLocation>();
            locations.Add( new ToolStripLocation( this.newWindowsToolStrip, Settings.Default.NewWindowToolStripDock, Settings.Default.NewWindowToolStripLocation ) );
            locations.Add( new ToolStripLocation( this.playbackToolStrip, Settings.Default.PlaybackToolStripDock, Settings.Default.PlaybackToolStripLocation ) );
            SetToolStripLocations( locations );

            // GoToOptions
            string options = Settings.Default.GoToOptions;
            if ( options != null )
            {
                m_goToOptions.Deserialize( options );
            }
        }
        #endregion

        #region Private Functions
        private void InitializeAdditionalComponents()
        {
            this.State = PlaybackState.Stopped;

            m_defaultOverviewSettings = new OverviewListControlSettings();
            m_defaultAllocationsSettings = new AllocationsTreeListControlSettings();
            m_defaultMemoryLayoutSettings = new MemoryLayoutControlSettings();

            SetOverviewSettings( m_defaultOverviewSettings, this.overviewListControl1 );
            SetAllocationsSettings( m_defaultAllocationsSettings, this.allocationsTreeListControl1 );
            SetMemoryLayoutSettings( m_defaultMemoryLayoutSettings, this.memoryLayoutControl1 );
        }

        private void SetOverviewSettings( OverviewListControlSettings overviewSettings, OverviewListControl overview )
        {
            overview.ShowBy = overviewSettings.ShowBy;
            overview.ShowEmptyItems = overviewSettings.ShowEmptyItems;

            overview.VisibleStacks = overviewSettings.VisibleStacks;
            if ( overviewSettings.VisibleStacks == OverviewListControl.SelectedStacks.Selected )
            {
                overview.ShowUnassignedStack = overviewSettings.ShowUnassignedStack;
            }

            overview.VisibleDatatypes = overviewSettings.VisibleDatatypes;
            if ( overviewSettings.VisibleDatatypes == OverviewListControl.SelectedDatatypes.Selected )
            {
                overview.ShowNativeDatatypes = overviewSettings.ShowNativeDatatypes;
                overview.ShowClassDatatypes = overviewSettings.ShowClassDatatypes;
                overview.ShowStructureDatatypes = overviewSettings.ShowStructureDatatypes;
                overview.ShowUnknownDatatypes = overviewSettings.ShowUnknownDatatypes;
            }

            overview.ShowAllocations = overviewSettings.ShowAllocations;
        }

        private void SetAllocationsSettings( AllocationsTreeListControlSettings allocationsSettings, AllocationsTreeListControl allocations )
        {
            allocations.GroupBy = allocationsSettings.GroupBy;
            allocations.MinSize = allocationsSettings.MinSize;
            allocations.MaxSize = allocationsSettings.MaxSize;

            allocations.VisibleStacks = allocationsSettings.VisibleStacks;
            if ( allocationsSettings.VisibleStacks == AllocationsTreeListControl.SelectedStacks.Selected )
            {
                allocations.ShowUnassignedStack = allocationsSettings.ShowUnassignedStack;
            }

            allocations.VisibleDatatypes = allocationsSettings.VisibleDatatypes;
            if ( allocationsSettings.VisibleDatatypes == AllocationsTreeListControl.SelectedDatatypes.Selected )
            {
                allocations.ShowNativeDatatypes = allocationsSettings.ShowNativeDatatypes;
                allocations.ShowClassDatatypes = allocationsSettings.ShowClassDatatypes;
                allocations.ShowStructureDatatypes = allocationsSettings.ShowStructureDatatypes;
                allocations.ShowUnknownDatatypes = allocationsSettings.ShowUnknownDatatypes;
            }

            allocations.VisibleMemoryTypes = allocationsSettings.VisibleMemoryTypes;
            if ( allocationsSettings.VisibleMemoryTypes == AllocationsTreeListControl.SelectedMemoryTypes.Selected )
            {
                allocations.ShowGameVirtualMemory = allocationsSettings.ShowGameVirtualMemory;
                allocations.ShowResourceVirtualMemory = allocationsSettings.ShowResourceVirtualMemory;
                allocations.ShowGamePhysicalMemory = allocationsSettings.ShowGamePhysicalMemory;
                allocations.ShowResourcePhysicalMemory = allocationsSettings.ShowResourcePhysicalMemory;
                allocations.ShowConsoleSpecificMemory = allocationsSettings.ShowConsoleSpecificMemory;
                allocations.ShowUndeterminedMemory = allocationsSettings.ShowUndeterminedMemory;
            }

            allocations.MultiSelect = allocationsSettings.MultiSelect;
            allocations.ShowAllocations = allocationsSettings.ShowAllocations;
        }

        private void SetMemoryLayoutSettings( MemoryLayoutControlSettings memoryLayoutSettings, MemoryLayoutControl memoryLayout )
        {
            memoryLayout.ColorizeBy = memoryLayoutSettings.ColorizeBy;
            memoryLayout.BytesPerPixel = memoryLayoutSettings.BytesPerPixel;
            memoryLayout.PixelWidth = memoryLayoutSettings.PixelWidth;
            memoryLayout.SizeScale = memoryLayoutSettings.SizeScale;
        }

        private void SetToolStripLocations( List<ToolStripLocation> locations )
        {
            Dictionary<ToolStrip, Point> locationDictionary = new Dictionary<ToolStrip, Point>();
            SortedList<ToolStrip, ToolStrip> topPanelToolStrips = new SortedList<ToolStrip, ToolStrip>( new ToolStripHorizontalSortComparer() );
            SortedList<ToolStrip, ToolStrip> leftPanelToolStrips = new SortedList<ToolStrip, ToolStrip>( new ToolStripVerticalSortComparer() );
            SortedList<ToolStrip, ToolStrip> rightPanelToolStrips = new SortedList<ToolStrip, ToolStrip>( new ToolStripVerticalSortComparer() );
            SortedList<ToolStrip, ToolStrip> bottomPanelToolStrips = new SortedList<ToolStrip, ToolStrip>( new ToolStripHorizontalSortComparer() );

            // find the location and add to the appropriate panel list
            foreach ( ToolStripLocation loc in locations )
            {
                if ( loc.Location != Point.Empty )
                {
                    loc.Strip.Location = loc.Location;
                    locationDictionary.Add( loc.Strip, loc.Location );
                }

                switch ( loc.Dock )
                {
                    case DockStyle.Top:
                        topPanelToolStrips.Add( loc.Strip, loc.Strip );
                        break;
                    case DockStyle.Left:
                        leftPanelToolStrips.Add( loc.Strip, loc.Strip );
                        break;
                    case DockStyle.Right:
                        rightPanelToolStrips.Add( loc.Strip, loc.Strip );
                        break;
                    case DockStyle.Bottom:
                        bottomPanelToolStrips.Add( loc.Strip, loc.Strip );
                        break;
                    default:
                        topPanelToolStrips.Add( loc.Strip, loc.Strip );
                        break;
                }
            }

            DetermineToolStripBounds( this.toolStripContainer1.TopToolStripPanel, topPanelToolStrips, locationDictionary );
            DetermineToolStripBounds( this.toolStripContainer1.LeftToolStripPanel, leftPanelToolStrips, locationDictionary );
            DetermineToolStripBounds( this.toolStripContainer1.RightToolStripPanel, rightPanelToolStrips, locationDictionary );
            DetermineToolStripBounds( this.toolStripContainer1.BottomToolStripPanel, bottomPanelToolStrips, locationDictionary );

            // now add each ToolStrip in turn
            AddToolStripsToPanel( this.toolStripContainer1.TopToolStripPanel, topPanelToolStrips );
            AddToolStripsToPanel( this.toolStripContainer1.LeftToolStripPanel, leftPanelToolStrips );
            AddToolStripsToPanel( this.toolStripContainer1.RightToolStripPanel, rightPanelToolStrips );
            AddToolStripsToPanel( this.toolStripContainer1.BottomToolStripPanel, bottomPanelToolStrips );
        }

        private void DetermineToolStripBounds( ToolStripPanel panel, SortedList<ToolStrip, ToolStrip> strips,
            Dictionary<ToolStrip, Point> locations )
        {
            // Seems the only way to set the bounds of a strip after items have been made invisible
            // is to add it to a panel.
            foreach ( ToolStrip strip in strips.Values )
            {
                DetermineToolStripBounds( panel, strip, locations[strip] );
            }
        }

        private void DetermineToolStripBounds( ToolStripPanel panel, ToolStrip strip, Point location )
        {
            panel.Controls.Add( strip );
            panel.Controls.Remove( strip );
            strip.Location = location;
        }

        private void AddToolStripsToPanel( ToolStripPanel panel, SortedList<ToolStrip, ToolStrip> strips )
        {
            if ( strips.Count == 0 )
            {
                return;
            }

            // first, fixup any overlaps
            for ( int i = 1; i < strips.Count; ++i )
            {
                ToolStrip strip1 = strips.Values[i - 1];
                ToolStrip strip2 = strips.Values[i];

                if ( strip2.Bounds.IntersectsWith( strip1.Bounds ) )
                {
                    int xDiff = 0;
                    if ( panel.Orientation == Orientation.Horizontal )
                    {
                        xDiff = strip1.Bounds.Right - strip2.Bounds.X + c_toolStripSpacing;
                    }

                    int yDiff = 0;
                    if ( panel.Orientation == Orientation.Vertical )
                    {
                        yDiff = strip1.Bounds.Bottom - strip2.Bounds.Y + c_toolStripSpacing;
                    }

                    // now push everything out
                    for ( int j = i; j < strips.Count; ++j )
                    {
                        ToolStrip strip3 = strips.Values[j];
                        strip3.Bounds = new Rectangle( strip3.Bounds.X + xDiff, strip3.Bounds.Y + yDiff,
                            strip3.Bounds.Width, strip3.Bounds.Height );
                    }
                }
            }

            int tabIndex = 0;

            // now add to the panel
            panel.SuspendLayout();
            foreach ( ToolStrip strip in strips.Values )
            {
                strip.TabIndex = tabIndex++;
                panel.Controls.Add( strip );
            }
            panel.ResumeLayout( false );
        }

        private ToolStripLocation CreateToolStripLocation( ToolStrip strip )
        {
            ToolStripLocation loc = new ToolStripLocation( strip, DockStyle.Top, strip.Location );
            if ( strip.Parent != null )
            {
                if ( strip.Parent == this.toolStripContainer1.LeftToolStripPanel )
                {
                    loc.Dock = DockStyle.Left;
                }
                else if ( strip.Parent == this.toolStripContainer1.RightToolStripPanel )
                {
                    loc.Dock = DockStyle.Right;
                }
                else if ( strip.Parent == this.toolStripContainer1.BottomToolStripPanel )
                {
                    loc.Dock = DockStyle.Bottom;
                }
                else
                {
                    loc.Dock = DockStyle.Top;
                }
            }

            return loc;
        }

        private void CenterLocation( Control parent, DockableWindow window )
        {
            Rectangle rect = parent.Bounds;

            int x = (rect.Width / 2) + rect.Left - (window.Width / 2);
            int y = (rect.Height / 2) + rect.Top - (window.Height / 2);

            window.FloatingLocation = new Point( x, y );
        }

        private void AddRecentFile( string filename )
        {
            if ( this.fileRecentFilesToolStripMenuItem.DropDownItems.Count == 10 )
            {
                this.fileRecentFilesToolStripMenuItem.DropDownItems.RemoveAt( 9 );
            }

            ToolStripMenuItem item = new ToolStripMenuItem( filename, null, new EventHandler( fileRecentFilesItemToolStripMenuItem_Click ) );
            this.fileRecentFilesToolStripMenuItem.DropDownItems.Insert( 0, item );
        }

        private void RemoveRecentFile( string filename )
        {
            for ( int i = 0; i < this.fileRecentFilesToolStripMenuItem.DropDownItems.Count; ++i )
            {
                if ( this.fileRecentFilesToolStripMenuItem.DropDownItems[i].Text == filename )
                {
                    this.fileRecentFilesToolStripMenuItem.DropDownItems.RemoveAt( i );
                    break;
                }
            }
        }

        private void BeginReadMessage()
        {
            this.messagesListBox.SuspendLayout();

            m_messageCountBeforeRead = m_messageCount;
        }

        private void EndReadMessage()
        {
            if ( m_messageCountBeforeRead != m_messageCount )
            {
                SendMessage( this.messagesListBox.Handle, WM_VSCROLL, (IntPtr)SB_BOTTOM, IntPtr.Zero );
                this.Text = "MemTracker " + this.CurrentTrackerFilename;
                this.messageCountToolStripStatusLabel.Text = "Message Count:  " + m_messageCount;
            }

            this.messagesListBox.ResumeLayout( m_messageCountBeforeRead != m_messageCount );

            m_messageCountBeforeRead = m_messageCount;
        }
        #endregion

        #region Interop
        private const int WM_VSCROLL = 277; // Vertical scroll
        private const int SB_BOTTOM = 7; // Scroll to bottom 

        [DllImport( "user32.dll", CharSet = CharSet.Auto )]
        private static extern int SendMessage( IntPtr hWnd, int wMsg, IntPtr wParam, IntPtr lParam );
        #endregion
    }

    #region MessageLine class
    internal class MessageLine
    {
        public MessageLine( uint index, string text, FileFlag flag )
        {
            m_index = index;
            m_text = text;

            switch ( flag )
            {
                case FileFlag.Start:
                    m_fileFlag = 0x0f;
                    break;
                case FileFlag.End:
                    m_fileFlag = 0xf0;
                    break;
                default:
                    m_fileFlag = 0;
                    break;
            }
        }

        #region Enums
        public enum FileFlag
        {
            None,
            Start,
            End,
        }
        #endregion

        #region Variables
        private uint m_index;
        private string m_text;
        private byte m_fileFlag;
        #endregion

        #region Properties
        public uint Index
        {
            get
            {
                return m_index;
            }
            set
            {
                m_index = value;
            }
        }

        public string Text
        {
            get
            {
                return m_text;
            }
        }

        public bool IsStartFile
        {
            get
            {
                return (m_fileFlag & 0x0f) == 0x0f;
            }
            set
            {
                m_fileFlag = 0;

                if ( value )
                {
                    m_fileFlag = 0x0f;
                }
            }
        }

        public bool IsEndFile
        {
            get
            {
                return (m_fileFlag & 0xf0) == 0xf0;
            }
            set
            {
                m_fileFlag = 0;

                if ( value )
                {
                    m_fileFlag = 0xf0;
                }
            }
        }
        #endregion

        public override string ToString()
        {
            StringBuilder text = new StringBuilder();

            text.Append( m_index );
            text.Append( ".  " );
            text.Append( m_text );
            
            return text.ToString();
        }

        public override int GetHashCode()
        {
            return m_text.GetHashCode();
        }
    }
    #endregion

    #region ToolStrips IComparer classes
    internal class ToolStripHorizontalSortComparer : IComparer<ToolStrip>
    {
        public int Compare( ToolStrip x, ToolStrip y )
        {
            if ( x.Location.Y < y.Location.Y )
            {
                return -1;
            }
            else if ( x.Location.Y > y.Location.Y )
            {
                return 1;
            }
            else
            {
                if ( x.Location.X < y.Location.X )
                {
                    return -1;
                }
                else if ( x.Location.X > y.Location.X )
                {
                    return 1;
                }
                else
                {
                    return 0;
                }
            }
        }
    }

    internal class ToolStripVerticalSortComparer : IComparer<ToolStrip>
    {
        public int Compare( ToolStrip x, ToolStrip y )
        {
            if ( x.Location.X < y.Location.X )
            {
                return -1;
            }
            else if ( x.Location.X > y.Location.X )
            {
                return 1;
            }
            else
            {
                if ( x.Location.Y < y.Location.Y )
                {
                    return -1;
                }
                else if ( x.Location.Y > y.Location.Y )
                {
                    return 1;
                }
                else
                {
                    return 0;
                }
            }
        }
    }
    #endregion

    #region ToolStripLocation
    internal class ToolStripLocation
    {
        public ToolStripLocation()
        {

        }

        public ToolStripLocation( ToolStrip strip )
        {
            this.Strip = strip;
        }

        public ToolStripLocation( ToolStrip strip, DockStyle dock, Point location )
        {
            this.Strip = strip;
            this.Dock = dock;
            this.Location = location;
        }

        #region Properties
        public ToolStrip Strip = null;
        public DockStyle Dock = DockStyle.Top;
        public Point Location = Point.Empty;
        #endregion
    }
    #endregion
}