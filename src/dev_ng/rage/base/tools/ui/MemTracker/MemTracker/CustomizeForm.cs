using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using MemTrackerControls;

namespace MemTracker
{
    public partial class CustomizeForm : Form
    {
        protected CustomizeForm()
        {
            InitializeComponent();
            InitializeAdditionalComponents();
        }

        /// <summary>
        /// Create and initialize this form with *copies* of the settings.
        /// </summary>
        /// <param name="overviewSettings"></param>
        /// <param name="allocationsSettings"></param>
        /// <param name="memoryLayoutSettings"></param>
        public CustomizeForm( OverviewListControlSettings overviewSettings, AllocationsTreeListControlSettings allocationsSettings,
            MemoryLayoutControlSettings memoryLayoutSettings )
        {
            InitializeComponent();

            m_overviewSettings = overviewSettings.Clone() as OverviewListControlSettings;
            m_allocationsSettings = allocationsSettings.Clone() as AllocationsTreeListControlSettings;
            m_memoryLayoutSettings = memoryLayoutSettings.Clone() as MemoryLayoutControlSettings;

            InitializeAdditionalComponents();
        }

        #region Properties
        public OverviewListControlSettings OverviewSettings
        {
            get
            {
                return m_overviewSettings;
            }
        }
        public AllocationsTreeListControlSettings AllocationsSettings
        {
            get
            {
                return m_allocationsSettings;
            }
        }

        public MemoryLayoutControlSettings MemoryLayoutSettings
        {
            get
            {
                return m_memoryLayoutSettings;
            }
        }
        #endregion

        #region Variables
        OverviewListControlSettings m_overviewSettings;
        AllocationsTreeListControlSettings m_allocationsSettings;
        MemoryLayoutControlSettings m_memoryLayoutSettings;
        #endregion

        #region Private Functions
        private void InitializeAdditionalComponents()
        {
            this.overviewPropertyGrid.SelectedObject = m_overviewSettings;
            this.allocationsPropertyGrid.SelectedObject = m_allocationsSettings;
            this.memoryLayoutPropertyGrid.SelectedObject = m_memoryLayoutSettings;
        }
        #endregion
    }
}