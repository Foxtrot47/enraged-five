using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;

using MemTrackerControls;

namespace MemTracker
{
    public abstract class MemTrackerControlSettings : Component
    {
        protected MemTrackerControlSettings( string guid, int index )
        {
            m_guid = guid;
            m_index = index;
        }

        #region Properties
        [Browsable( false )]
        public string Guid
        {
            get
            {
                return m_guid;
            }
        }

        [Browsable( false )]
        public int Index
        {
            get
            {
                return m_index;
            }
        }
        #endregion

        #region Variables
        protected string m_guid;
        protected int m_index = -1;
        #endregion

        #region Public Functions
        public abstract MemTrackerControlSettings Clone();

        public virtual string Serialize()
        {
            StringBuilder text = new StringBuilder();

            text.Append( m_guid );
            text.Append( ';' );

            text.Append( m_index.ToString() );
            text.Append( ';' );

            return text.ToString();
        }

        public virtual bool Deserialize( string text )
        {
            string[] split = text.Split( new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries );

            if ( split.Length < 2 )
            {
                return false;
            }

            m_guid = split[0];
            m_index = int.Parse( split[1] );

            return true;
        }
        #endregion
    }

    public class OverviewListControlSettings : MemTrackerControlSettings
    {
        public OverviewListControlSettings()
            : base( "default", -1 )
        {

        }

        public OverviewListControlSettings( string guid, int index, OverviewListControl overview )
            : base( guid, index )
        {
            this.ShowBy = overview.ShowBy;
            this.ShowEmptyItems = overview.ShowEmptyItems;

            this.VisibleStacks = overview.VisibleStacks;
            if ( this.VisibleStacks == OverviewListControl.SelectedStacks.Selected )
            {
                this.ShowUnassignedStack = overview.ShowUnassignedStack;
            }

            this.VisibleDatatypes = overview.VisibleDatatypes;
            if ( this.VisibleDatatypes == OverviewListControl.SelectedDatatypes.Selected )
            {
                this.ShowNativeDatatypes = overview.ShowNativeDatatypes;
                this.ShowClassDatatypes = overview.ShowClassDatatypes;
                this.ShowStructureDatatypes = overview.ShowStructureDatatypes;
                this.ShowUnknownDatatypes = overview.ShowUnknownDatatypes;
            }

            this.ShowAllocations = overview.ShowAllocations;
        }

        #region Properties
        /// <summary>
        /// Gets or sets the <see cref="OverviewListControl.DisplayType"/>.
        /// </summary>
        [Browsable( true ),
        Category( "Display Type" ),
        Description( "The type of list to show." ),
        DefaultValue( "OverviewListControl.DisplayType.MemoryType" )]
        public OverviewListControl.DisplayType ShowBy
        {
            get
            {
                return m_showBy;
            }
            set
            {
                m_showBy = value;
            }
        }

        /// <summary>
        /// Gets or sets whether to display the Unassigned stack name when <see cref="OverviewListControl.ShowBy"/> is set to <see cref="OverviewListControl.DisplayType."/>StackNames.
        /// </summary>
        [Browsable( true ),
        Category( "Show By Stack Name" ),
        Description( "Whether to show the Unassigned Stack." ),
        DefaultValue( true )]
        public bool ShowUnassignedStack
        {
            get
            {
                return m_showUnassignedStack;
            }
            set
            {
                m_showUnassignedStack = value;
            }
        }

        /// <summary>
        /// Gets or sets whether to display Native datatypes when <see cref="OverviewListControl.ShowBy"/> is set to <see cref="OverviewListControl.DisplayType."/>DataTypes.
        /// </summary>
        [Browsable( true ),
        Category( "Show By Datatype" ),
        Description( "Whether to show Native datatypes." ),
        DefaultValue( true )]
        public bool ShowNativeDatatypes
        {
            get
            {
                return m_showNativeDatatypes;
            }
            set
            {
                m_showNativeDatatypes = value;
            }
        }

        /// <summary>
        /// Gets or sets whether to display Classes when <see cref="OverviewListControl.ShowBy"/> is set to <see cref="OverviewListControl.DisplayType."/>DataTypes.
        /// </summary>
        [Browsable( true ),
        Category( "Show By Datatype" ),
        Description( "Whether to show Classes." ),
        DefaultValue( true )]
        public bool ShowClassDatatypes
        {
            get
            {
                return m_showClassDatatypes;
            }
            set
            {
                m_showClassDatatypes = value;
            }
        }

        /// <summary>
        /// Gets or sets whether to display Structures when <see cref="OverviewListControl.ShowBy"/> is set to <see cref="OverviewListControl.DisplayType."/>DataTypes.
        /// </summary>
        [Browsable( true ),
        Category( "Show By Datatype" ),
        Description( "Whether to show Structures." ),
        DefaultValue( true )]
        public bool ShowStructureDatatypes
        {
            get
            {
                return m_showStructDatatypes;
            }
            set
            {
                m_showStructDatatypes = value;
            }
        }

        /// <summary>
        /// Gets or sets whether to display Unknown datatypes when <see cref="OverviewListControl.ShowBy"/> is set to <see cref="OverviewListControl.DisplayType."/>DataTypes.
        /// </summary>
        [Browsable( true ),
        Category( "Show By Datatype" ),
        Description( "Whether to show Unknown datatypes." ),
        DefaultValue( true )]
        public bool ShowUnknownDatatypes
        {
            get
            {
                return m_showUnknownDatatypes;
            }
            set
            {
                m_showUnknownDatatypes = value;
            }
        }

        /// <summary>
        /// Gets or sets whether to display allocations or deallocations.
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        Description( "Whether to show Allocations or Deallocations." ),
        DefaultValue( true )]
        public bool ShowAllocations
        {
            get
            {
                return m_showAllocations;
            }
            set
            {
                m_showAllocations = value;
            }
        }

        /// <summary>
        /// Gets or sets whether to display or hide items with a size of 0 bytes.
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        Description( "Whether to show items with a size of 0 bytes." ),
        DefaultValue( false )]
        public bool ShowEmptyItems
        {
            get
            {
                return m_showEmptyItems;
            }
            set
            {
                m_showEmptyItems = value;
            }
        }

        /// <summary>
        /// Gets or sets the visibility of Stack names using the <see cref="SelectedStacks"/> enumeration.
        /// </summary>
        [Browsable( true ),
        Category( "Show By Stack Name" ),
        Description( "Gets or sets the visibility of Stack names." ),
        DefaultValue( "OverviewListControl.SelectedStacks.All" )]
        public OverviewListControl.SelectedStacks VisibleStacks
        {
            get
            {
                return m_selectedStacks;
            }
            set
            {
                m_selectedStacks = value;
                
                if ( m_selectedStacks != OverviewListControl.SelectedStacks.Selected )
                {
                    this.ShowUnassignedStack = m_selectedStacks == OverviewListControl.SelectedStacks.All;
                }
            }
        }

        /// <summary>
        /// Gets or sets the visibility of Datatypes using the <see cref="OverviewListControl.SelectedDatatypes"/> enumeration.
        /// </summary>
        [Browsable( true ),
        Category( "Show By Datatype" ),
        Description( "Gets or sets the visibility of Datatypes." ),
        DefaultValue( "OverviewListControl.SelectedDatatypes.All" )]
        public OverviewListControl.SelectedDatatypes VisibleDatatypes
        {
            get
            {
                return m_selectedDatatypes;
            }
            set
            {
                m_selectedDatatypes = value;

                if ( m_selectedDatatypes != OverviewListControl.SelectedDatatypes.Selected )
                {
                    bool vis = m_selectedDatatypes == OverviewListControl.SelectedDatatypes.All;

                    this.ShowNativeDatatypes = vis;
                    this.ShowClassDatatypes = vis;
                    this.ShowStructureDatatypes = vis;
                    this.ShowUnknownDatatypes = vis;
                }
            }
        }
        #endregion

        #region Variables
        private OverviewListControl.DisplayType m_showBy = OverviewListControl.DisplayType.MemoryType;
        private bool m_showAllocations = true;
        private bool m_showEmptyItems = false;
        private bool m_showUnassignedStack = true;
        private bool m_showNativeDatatypes = true;
        private bool m_showClassDatatypes = true;
        private bool m_showStructDatatypes = true;
        private bool m_showUnknownDatatypes = true;

        private OverviewListControl.SelectedStacks m_selectedStacks = OverviewListControl.SelectedStacks.All;
        private OverviewListControl.SelectedDatatypes m_selectedDatatypes = OverviewListControl.SelectedDatatypes.All;
        #endregion

        #region Public Functions
        public override MemTrackerControlSettings Clone()
        {
            OverviewListControlSettings overviewSettings = new OverviewListControlSettings();

            overviewSettings.m_guid = this.Guid;
            overviewSettings.m_index = this.Index;

            overviewSettings.ShowBy = this.ShowBy;
            overviewSettings.ShowEmptyItems = this.ShowEmptyItems;

            overviewSettings.VisibleStacks = this.VisibleStacks;
            if ( overviewSettings.VisibleStacks == OverviewListControl.SelectedStacks.Selected )
            {
                overviewSettings.ShowUnassignedStack = this.ShowUnassignedStack;
            }

            overviewSettings.VisibleDatatypes = this.VisibleDatatypes;
            if ( overviewSettings.VisibleDatatypes == OverviewListControl.SelectedDatatypes.Selected )
            {
                overviewSettings.ShowNativeDatatypes = this.ShowNativeDatatypes;
                overviewSettings.ShowClassDatatypes = this.ShowClassDatatypes;
                overviewSettings.ShowStructureDatatypes = this.ShowStructureDatatypes;
                overviewSettings.ShowUnknownDatatypes = this.ShowUnknownDatatypes;
            }

            overviewSettings.ShowAllocations = this.ShowAllocations;

            return overviewSettings;
        }

        public override string Serialize()
        {
            StringBuilder text = new StringBuilder();

            text.Append( base.Serialize() );

            text.Append( m_showBy.ToString() );
            text.Append( ';' );

            text.Append( m_showAllocations.ToString() );
            text.Append( ';' );

            text.Append( m_showEmptyItems.ToString() );
            text.Append( ';' );

            text.Append( m_showUnassignedStack.ToString() );
            text.Append( ';' );

            text.Append( m_showNativeDatatypes.ToString() );
            text.Append( ';' );

            text.Append( m_showClassDatatypes.ToString() );
            text.Append( ';' );

            text.Append( m_showStructDatatypes.ToString() );
            text.Append( ';' );

            text.Append( m_showUnknownDatatypes.ToString() );
            text.Append( ';' );

            text.Append( m_selectedStacks.ToString() );
            text.Append( ';' );

            text.Append( m_selectedDatatypes.ToString() );

            return text.ToString();
        }

        public override bool Deserialize( string text )
        {
            if ( !base.Deserialize( text ) )
            {
                return false;
            }

            string[] split = text.Split( new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries );
            if ( split.Length < 12 )
            {
                return false;
            }

            m_showBy = (OverviewListControl.DisplayType)Enum.Parse( typeof(OverviewListControl.DisplayType), split[2] );
            m_showAllocations = bool.Parse( split[3] );
            m_showEmptyItems = bool.Parse( split[4] );
            m_showUnassignedStack = bool.Parse( split[5] );
            m_showNativeDatatypes = bool.Parse( split[6] );
            m_showClassDatatypes = bool.Parse( split[7] );
            m_showStructDatatypes = bool.Parse( split[8] );
            m_showUnknownDatatypes = bool.Parse( split[9] );

            m_selectedStacks = (OverviewListControl.SelectedStacks)Enum.Parse( typeof(OverviewListControl.SelectedStacks), split[10] );
            m_selectedDatatypes = (OverviewListControl.SelectedDatatypes)Enum.Parse( typeof(OverviewListControl.SelectedDatatypes), split[11] );

            return true;
        }
        #endregion
    }

    public class AllocationsTreeListControlSettings : MemTrackerControlSettings
    {
        public AllocationsTreeListControlSettings()
            : base( "default", -1 )
        {

        }

        public AllocationsTreeListControlSettings( string guid, int index, AllocationsTreeListControl allocations )
            : base( guid, index )
        {
            this.GroupBy = allocations.GroupBy;
            this.MinSize = allocations.MinSize;
            this.MaxSize = allocations.MaxSize;

            this.VisibleStacks = allocations.VisibleStacks;
            if ( this.VisibleStacks == AllocationsTreeListControl.SelectedStacks.Selected )
            {
                this.ShowUnassignedStack = allocations.ShowUnassignedStack;
            }

            this.VisibleDatatypes = allocations.VisibleDatatypes;
            if ( this.VisibleDatatypes == AllocationsTreeListControl.SelectedDatatypes.Selected )
            {
                this.ShowNativeDatatypes = allocations.ShowNativeDatatypes;
                this.ShowClassDatatypes = allocations.ShowClassDatatypes;
                this.ShowStructureDatatypes = allocations.ShowStructureDatatypes;
                this.ShowUnknownDatatypes = allocations.ShowUnknownDatatypes;
            }

            this.VisibleMemoryTypes = allocations.VisibleMemoryTypes;
            if ( this.VisibleMemoryTypes == AllocationsTreeListControl.SelectedMemoryTypes.Selected )
            {
                this.ShowGameVirtualMemory = allocations.ShowGameVirtualMemory;
                this.ShowResourceVirtualMemory = allocations.ShowResourceVirtualMemory;
                this.ShowGamePhysicalMemory = allocations.ShowGamePhysicalMemory;
                this.ShowResourcePhysicalMemory = allocations.ShowResourcePhysicalMemory;
                this.ShowConsoleSpecificMemory = allocations.ShowConsoleSpecificMemory;
                this.ShowUndeterminedMemory = allocations.ShowUndeterminedMemory;
            }

            this.ShowAllocations = allocations.ShowAllocations;

            this.MultiSelect = allocations.MultiSelect;
        }

        #region Properties
        /// <summary>
        /// Gets or sets the minimum allocation size to display.
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        Description( "The minimum size of the allocation to be displayed." ),
        DefaultValue( 0 )]
        public int MinSize
        {
            get
            {
                return m_minSize;
            }
            set
            {
                if ( value < 0 )
                {
                    value = 0;
                }
                else if ( value > m_maxSize )
                {
                    value = m_maxSize;
                }

                m_minSize = value;
            }
        }

        /// <summary>
        /// Gets or sets the maximum allocation size to display.
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        Description( "The maximum size of the allocation to be displayed." ),
        DefaultValue( 999999999 )]
        public int MaxSize
        {
            get
            {
                return m_maxSize;
            }
            set
            {
                if ( value < 0 )
                {
                    value = 0;
                }
                else if ( value < m_minSize )
                {
                    value = m_minSize;
                }
                else if ( value > 999999999 )
                {
                    value = 999999999;
                }

                m_maxSize = value;
            }
        }

        /// <summary>
        /// Gets or sets the <see cref="DisplayType"/>.
        /// </summary>
        [Browsable( true ),
        Category( "Display Type" ),
        Description( "How to arrange the allocation nodes." ),
        DefaultValue( "AllocationsTreeListControl.DisplayType.MemoryType" )]
        public AllocationsTreeListControl.DisplayType GroupBy
        {
            get
            {
                return m_groupBy;
            }
            set
            {
                m_groupBy = value;
            }
        }

        /// <summary>
        /// Gets or sets whether to display the Unassigned stack name when <see cref="AllocationsTreeListControl.ShowBy"/> is set to <see cref="AllocationsTreeListControl.DisplayType."/>StackNames.
        /// </summary>
        [Browsable( true ),
        Category( "Stack Names" ),
        Description( "Whether to show the Unassigned Stack." ),
        DefaultValue( true )]
        public bool ShowUnassignedStack
        {
            get
            {
                return m_showUnassignedStack;
            }
            set
            {
                m_showUnassignedStack = value;
            }
        }

        /// <summary>
        /// Gets or sets whether to display Native datatypes when <see cref="AllocationsTreeListControl.ShowBy"/> is set to <see cref="AllocationsTreeListControl.DisplayType."/>DataTypes.
        /// </summary>
        [Browsable( true ),
        Category( "Datatypes" ),
        Description( "Whether to show Native datatypes." ),
        DefaultValue( true )]
        public bool ShowNativeDatatypes
        {
            get
            {
                return m_showNativeDatatypes;
            }
            set
            {
                m_showNativeDatatypes = value;
            }
        }

        /// <summary>
        /// Gets or sets whether to display Classes when <see cref="AllocationsTreeListControl.ShowBy"/> is set to <see cref="AllocationsTreeListControl.DisplayType."/>DataTypes.
        /// </summary>
        [Browsable( true ),
        Category( "Datatypes" ),
        Description( "Whether to show Classes." ),
        DefaultValue( true )]
        public bool ShowClassDatatypes
        {
            get
            {
                return m_showClassDatatypes;
            }
            set
            {
                m_showClassDatatypes = value;
            }
        }

        /// <summary>
        /// Gets or sets whether to display Structures when <see cref="AllocationsTreeListControl.ShowBy"/> is set to <see cref="AllocationsTreeListControl.DisplayType."/>DataTypes.
        /// </summary>
        [Browsable( true ),
        Category( "Datatypes" ),
        Description( "Whether to show Structures." ),
        DefaultValue( true )]
        public bool ShowStructureDatatypes
        {
            get
            {
                return m_showStructDatatypes;
            }
            set
            {
                m_showStructDatatypes = value;
            }
        }

        /// <summary>
        /// Gets or sets whether to display Unknown datatypes when <see cref="AllocationsTreeListControl.ShowBy"/> is set to <see cref="AllocationsTreeListControl.DisplayType."/>DataTypes.
        /// </summary>
        [Browsable( true ),
        Category( "Datatypes" ),
        Description( "Whether to show Unknown datatypes." ),
        DefaultValue( true )]
        public bool ShowUnknownDatatypes
        {
            get
            {
                return m_showUnknownDatatypes;
            }
            set
            {
                m_showUnknownDatatypes = value;
            }
        }

        /// <summary>
        /// Gets or sets whether to display allocations or deallocations.
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        Description( "Whether to show Allocations or Deallocations." ),
        DefaultValue( true )]
        public bool ShowAllocations
        {
            get
            {
                return m_showAllocations;
            }
            set
            {
                m_showAllocations = value;
            }
        }

        /// <summary>
        /// Gets or sets the visibility of Stack names using the <see cref="AllocationsTreeListControl.SelectedStacks"/> enumeration.
        /// </summary>
        [Browsable( true ),
        Category( "Stack Names" ),
        Description( "Gets or sets the visibility of Stack names." ),
        DefaultValue( "AllocationsTreeListControl.SelectedStacks.All" )]
        public AllocationsTreeListControl.SelectedStacks VisibleStacks
        {
            get
            {
                return m_selectedStacks;
            }
            set
            {
                m_selectedStacks = value;

                if ( m_selectedStacks != AllocationsTreeListControl.SelectedStacks.Selected )
                {
                    this.ShowUnassignedStack = m_selectedStacks == AllocationsTreeListControl.SelectedStacks.All;
                }
            }
        }

        /// <summary>
        /// Gets or sets the visibility of Datatypes using the <see cref="AllocationsTreeListControl.SelectedDatatypes"/> enumeration.
        /// </summary>
        [Browsable( true ),
        Category( "Datatypes" ),
        Description( "Gets or sets the visibility of Datatypes." ),
        DefaultValue( "AllocationsTreeListControl.SelectedDatatypes.All" )]
        public AllocationsTreeListControl.SelectedDatatypes VisibleDatatypes
        {
            get
            {
                return m_selectedDatatypes;
            }
            set
            {
                m_selectedDatatypes = value;

                if ( m_selectedDatatypes != AllocationsTreeListControl.SelectedDatatypes.Selected )
                {
                    bool vis = m_selectedDatatypes == AllocationsTreeListControl.SelectedDatatypes.All;

                    this.ShowNativeDatatypes = vis;
                    this.ShowClassDatatypes = vis;
                    this.ShowStructureDatatypes = vis;
                    this.ShowUnknownDatatypes = vis;
                }
            }
        }

        /// <summary>
        /// Gets or sets whether to display allocations made with the Game Virtual memory type.
        /// </summary>
        [Browsable( true ),
        Category( "Memory Types" ),
        Description( "Whether to show Game Virtual memory." ),
        DefaultValue( true )]
        public bool ShowGameVirtualMemory
        {
            get
            {
                return m_memoryTypeVisibility[(int)Allocation.MemoryTypeIndex.GameVirtual];
            }
            set
            {
                m_memoryTypeVisibility[(int)Allocation.MemoryTypeIndex.GameVirtual] = value;
            }
        }

        /// <summary>
        /// Gets or sets whether to display allocations made with the Game Resource Physical memory type.
        /// </summary>
        [Browsable( true ),
        Category( "Memory Types" ),
        Description( "Whether to show Game Physical memory." ),
        DefaultValue( true )]
        public bool ShowGamePhysicalMemory
        {
            get
            {
                return m_memoryTypeVisibility[(int)Allocation.MemoryTypeIndex.GamePhysical];
            }
            set
            {
                m_memoryTypeVisibility[(int)Allocation.MemoryTypeIndex.GamePhysical] = value;
            }
        }

        /// <summary>
        /// Gets or sets whether to display allocations made with the Game Resource Physical memory type.
        /// </summary>
        [Browsable( true ),
        Category( "Memory Types" ),
        Description( "Whether to show Resource Physical memory." ),
        DefaultValue( true )]
        public bool ShowResourcePhysicalMemory
        {
            get
            {
                return m_memoryTypeVisibility[(int)Allocation.MemoryTypeIndex.ResourcePhysical];
            }
            set
            {
                m_memoryTypeVisibility[(int)Allocation.MemoryTypeIndex.ResourcePhysical] = value;
            }
        }

        /// <summary>
        /// Gets or sets whether to display allocations made with the Resource Virtual memory type.
        /// </summary>
        [Browsable( true ),
        Category( "Memory Types" ),
        Description( "Whether to show Resource Virtual memory." ),
        DefaultValue( true )]
        public bool ShowResourceVirtualMemory
        {
            get
            {
                return m_memoryTypeVisibility[(int)Allocation.MemoryTypeIndex.ResourceVirtual];
            }
            set
            {
                m_memoryTypeVisibility[(int)Allocation.MemoryTypeIndex.ResourceVirtual] = value;
            }
        }

        /// <summary>
        /// Gets or sets whether to display allocations of Undetermined memory type.
        /// </summary>
        [Browsable( true ),
        Category( "Memory Types" ),
        Description( "Whether to show Undetermined memory." ),
        DefaultValue( true )]
        public bool ShowUndeterminedMemory
        {
            get
            {
                return m_memoryTypeVisibility[(int)Allocation.MemoryTypeIndex.Undetermined];
            }
            set
            {
                m_memoryTypeVisibility[(int)Allocation.MemoryTypeIndex.Undetermined] = value;
            }
        }

        /// <summary>
        /// Gets or sets whether to display allocations made with the Console Specific memory type, typically Xenon External.
        /// </summary>
        [Browsable( true ),
        Category( "Memory Types" ),
        Description( "Whether to show Console Specific memory." ),
        DefaultValue( true )]
        public bool ShowConsoleSpecificMemory
        {
            get
            {
                return m_memoryTypeVisibility[(int)Allocation.MemoryTypeIndex.ConsoleSpecific];
            }
            set
            {
                m_memoryTypeVisibility[(int)Allocation.MemoryTypeIndex.ConsoleSpecific] = value;
            }
        }

        /// <summary>
        /// Gets or sets the visibility of Memory Types using the <see cref="AllocationsTreeListControl.SelectedMemoryTypes"/> enumeration.
        /// </summary>
        [Browsable( true ),
        Category( "Memory Types" ),
        Description( "Gets or sets the visibility of Memory Types." ),
        DefaultValue( "AllocationsTreeListControl.SelectedMemoryTypes.All" )]
        public AllocationsTreeListControl.SelectedMemoryTypes VisibleMemoryTypes
        {
            get
            {
                return m_selectedMemoryTypes;
            }
            set
            {
                m_selectedMemoryTypes = value;

                if ( m_selectedMemoryTypes != AllocationsTreeListControl.SelectedMemoryTypes.Selected )
                {
                    bool vis = m_selectedMemoryTypes == AllocationsTreeListControl.SelectedMemoryTypes.All;

                    this.ShowGameVirtualMemory = vis;
                    this.ShowResourceVirtualMemory = vis;
                    this.ShowGamePhysicalMemory = vis;
                    this.ShowResourcePhysicalMemory = vis;
                    this.ShowConsoleSpecificMemory = vis;
                    this.ShowUndeterminedMemory = vis;
                }
            }
        }

        /// <summary>
        /// Gets or sets whether multiple rows can be selected, typically for copying to the <see cref="Clipboard"/>.
        /// </summary>
        /// <remarks>
        /// Enabling this operation impacts performance negatively.  It is recommended that it be turned off immediately
        /// after completing whatever operation for which it was enabled.
        /// </remarks>
        [Browsable( true ),
        Category( "Misc" ),
        Description( "Allow multiple row selection." )]
        public bool MultiSelect
        {
            get
            {
                return m_multiSelect;
            }
            set
            {
                m_multiSelect = value;
            }
        }
        #endregion

        #region Variables
        private AllocationsTreeListControl.DisplayType m_groupBy = AllocationsTreeListControl.DisplayType.Order;
        private bool m_showAllocations = true;
        private bool m_multiSelect = false;
        private bool m_showUnassignedStack = true;
        private bool m_showNativeDatatypes = true;
        private bool m_showClassDatatypes = true;
        private bool m_showStructDatatypes = true;
        private bool m_showUnknownDatatypes = true;

        private int m_minSize = 0;
        private int m_maxSize = 999999999;

        private AllocationsTreeListControl.SelectedStacks m_selectedStacks = AllocationsTreeListControl.SelectedStacks.All;
        private AllocationsTreeListControl.SelectedDatatypes m_selectedDatatypes = AllocationsTreeListControl.SelectedDatatypes.All;
        private AllocationsTreeListControl.SelectedMemoryTypes m_selectedMemoryTypes = AllocationsTreeListControl.SelectedMemoryTypes.All;

        private bool[] m_memoryTypeVisibility = new bool[(int)Allocation.MemoryTypeIndex.NumIndexes];
        #endregion

        #region Public Functions
        public override MemTrackerControlSettings Clone()
        {
            AllocationsTreeListControlSettings allocationsSettings = new AllocationsTreeListControlSettings();

            allocationsSettings.m_guid = this.Guid;
            allocationsSettings.m_index = this.Index;

            allocationsSettings.GroupBy = this.GroupBy;
            allocationsSettings.MinSize = this.MinSize;
            allocationsSettings.MaxSize = this.MaxSize;

            allocationsSettings.VisibleStacks = this.VisibleStacks;
            if ( allocationsSettings.VisibleStacks == AllocationsTreeListControl.SelectedStacks.Selected )
            {
                allocationsSettings.ShowUnassignedStack = this.ShowUnassignedStack;
            }

            allocationsSettings.VisibleDatatypes = this.VisibleDatatypes;
            if ( allocationsSettings.VisibleDatatypes == AllocationsTreeListControl.SelectedDatatypes.Selected )
            {
                allocationsSettings.ShowNativeDatatypes = this.ShowNativeDatatypes;
                allocationsSettings.ShowClassDatatypes = this.ShowClassDatatypes;
                allocationsSettings.ShowStructureDatatypes = this.ShowStructureDatatypes;
                allocationsSettings.ShowUnknownDatatypes = this.ShowUnknownDatatypes;
            }

            allocationsSettings.VisibleMemoryTypes = this.VisibleMemoryTypes;
            if ( allocationsSettings.VisibleMemoryTypes == AllocationsTreeListControl.SelectedMemoryTypes.Selected )
            {
                allocationsSettings.ShowGameVirtualMemory = this.ShowGameVirtualMemory;
                allocationsSettings.ShowResourceVirtualMemory = this.ShowResourceVirtualMemory;
                allocationsSettings.ShowGamePhysicalMemory = this.ShowGamePhysicalMemory;
                allocationsSettings.ShowResourcePhysicalMemory = this.ShowResourcePhysicalMemory;
                allocationsSettings.ShowConsoleSpecificMemory = this.ShowConsoleSpecificMemory;
                allocationsSettings.ShowUndeterminedMemory = this.ShowUndeterminedMemory;
            }

            allocationsSettings.ShowAllocations = this.ShowAllocations;

            allocationsSettings.MultiSelect = this.MultiSelect;
            allocationsSettings.ShowAllocations = this.ShowAllocations;

            return allocationsSettings;
        }

        public override string Serialize()
        {
            StringBuilder text = new StringBuilder();

            text.Append( base.Serialize() );

            text.Append( m_groupBy.ToString() );
            text.Append( ';' );

            text.Append( m_minSize.ToString() );
            text.Append( ';' );

            text.Append( m_maxSize.ToString() );
            text.Append( ';' );

            text.Append( m_showAllocations.ToString() );
            text.Append( ';' );

            text.Append( m_multiSelect.ToString() );
            text.Append( ';' );

            text.Append( m_showUnassignedStack.ToString() );
            text.Append( ';' );

            text.Append( m_showNativeDatatypes.ToString() );
            text.Append( ';' );

            text.Append( m_showClassDatatypes.ToString() );
            text.Append( ';' );

            text.Append( m_showStructDatatypes.ToString() );
            text.Append( ';' );

            text.Append( m_showUnknownDatatypes.ToString() );
            text.Append( ';' );

            text.Append( m_selectedStacks.ToString() );
            text.Append( ';' );

            text.Append( m_selectedDatatypes.ToString() );
            text.Append( ';' );

            text.Append( m_selectedMemoryTypes.ToString() );
            text.Append( ';' );

            foreach ( bool vis in m_memoryTypeVisibility )
            {
                text.Append( vis.ToString() );
                text.Append( ';' );
            }

            return text.ToString();
        }

        public override bool Deserialize( string text )
        {
            if ( !base.Deserialize( text ) )
            {
                return false;
            }

            string[] split = text.Split( new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries );
            if ( split.Length < 15 + m_memoryTypeVisibility.Length )
            {
                return false;
            }

            m_groupBy = (AllocationsTreeListControl.DisplayType)Enum.Parse( typeof( AllocationsTreeListControl.DisplayType ), split[2] );
            m_minSize = int.Parse( split[3] );
            m_maxSize = int.Parse( split[4] );
            m_showAllocations = bool.Parse( split[5] );
            m_multiSelect = bool.Parse( split[6] );
            m_showUnassignedStack = bool.Parse( split[7] );
            m_showNativeDatatypes = bool.Parse( split[8] );
            m_showClassDatatypes = bool.Parse( split[9] );
            m_showStructDatatypes = bool.Parse( split[10] );
            m_showUnknownDatatypes = bool.Parse( split[11] );

            m_selectedStacks = (AllocationsTreeListControl.SelectedStacks)Enum.Parse( typeof( AllocationsTreeListControl.SelectedStacks ), split[12] );
            m_selectedDatatypes = (AllocationsTreeListControl.SelectedDatatypes)Enum.Parse( typeof( AllocationsTreeListControl.SelectedDatatypes ), split[13] );
            m_selectedMemoryTypes = (AllocationsTreeListControl.SelectedMemoryTypes)Enum.Parse( typeof( AllocationsTreeListControl.SelectedMemoryTypes ), split[14] );

            for ( int i = 0; i < m_memoryTypeVisibility.Length; ++i )
            {
                m_memoryTypeVisibility[i] = bool.Parse( split[i + 15] );
            }

            return true;
        }
        #endregion
    }

    public class MemoryLayoutControlSettings : MemTrackerControlSettings
    {
        public MemoryLayoutControlSettings()
            : base( "default", -1 )
        {

        }

        public MemoryLayoutControlSettings( string guid, int index, MemoryLayoutControl memoryLayout )
            : base( guid, index )
        {
            this.ColorizeBy = memoryLayout.ColorizeBy;
            this.BytesPerPixel = memoryLayout.BytesPerPixel;
            this.PixelWidth = memoryLayout.PixelWidth;
            this.SizeScale = memoryLayout.SizeScale;
        }

        #region Properties
        /// <summary>
        /// Gets or sets the coloring scheme of the report.
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        DefaultValue( "MemoryLayoutControl.DisplayType.StackNames" ),
        Description( "The coloring scheme of the report." )]
        public MemoryLayoutControl.DisplayType ColorizeBy
        {
            get
            {
                return m_colorizeBy;
            }
            set
            {
                m_colorizeBy = value;
            }
        }

        /// <summary>
        /// Gets or sets the number of bytes that each pixel represents.
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        DefaultValue( "MemoryLayoutControl.Bytes.OneHundredTwentyEight" ),
        Description( "The number of bytes that each pixel represents." )]
        public MemoryLayoutControl.Bytes BytesPerPixel
        {
            get
            {
                return m_bytesPerPixel;
            }
            set
            {
                m_bytesPerPixel = value;
            }
        }

        /// <summary>
        /// Gets or sets the number of pixels across to display.
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        DefaultValue( "MemoryLayoutControl.Widths.TwoHundredFiftySix" ),
        Description( "The number of pixels across to display." )]
        public MemoryLayoutControl.Widths PixelWidth
        {
            get
            {
                return m_pixelWidth;
            }
            set
            {
                m_pixelWidth = value;
            }
        }

        /// <summary>
        /// Gets or sets the level of detail for the colors when <see cref="MemoryLayoutControl.ColorizeBy"/> is <see cref="MemoryLayoutControl.DisplayType"/>.AllocationSize.
        /// </summary>
        [Browsable( true ),
        Category( "Misc" ),
        DefaultValue( "MemoryLayoutControl.Sizes.Medium" ),
        Description( "The level of detail for the colors when colorizing by allocation size." )]
        public MemoryLayoutControl.Sizes SizeScale
        {
            get
            {
                return m_sizeScale;
            }
            set
            {
                m_sizeScale = value;
            }
        }
        #endregion

        #region Variables
        private MemoryLayoutControl.DisplayType m_colorizeBy = MemoryLayoutControl.DisplayType.StackNames;
        private MemoryLayoutControl.Bytes m_bytesPerPixel = MemoryLayoutControl.Bytes.OneHundredTwentyEight;
        private MemoryLayoutControl.Widths m_pixelWidth = MemoryLayoutControl.Widths.TwoHundredFiftySix;
        private MemoryLayoutControl.Sizes m_sizeScale = MemoryLayoutControl.Sizes.Medium;
        #endregion

        #region Public Functions
        public override MemTrackerControlSettings Clone()
        {
            MemoryLayoutControlSettings memoryLayoutSettings = new MemoryLayoutControlSettings();

            memoryLayoutSettings.m_guid = this.Guid;
            memoryLayoutSettings.m_index = this.Index;

            memoryLayoutSettings.ColorizeBy = this.ColorizeBy;
            memoryLayoutSettings.BytesPerPixel = this.BytesPerPixel;
            memoryLayoutSettings.PixelWidth = this.PixelWidth;
            memoryLayoutSettings.SizeScale = this.SizeScale;

            return memoryLayoutSettings;
        }

        public override string Serialize()
        {
            StringBuilder text = new StringBuilder();

            text.Append( base.Serialize() );

            text.Append( m_colorizeBy.ToString() );
            text.Append( ';' );

            text.Append( m_bytesPerPixel.ToString() );
            text.Append( ';' );

            text.Append( m_pixelWidth.ToString() );
            text.Append( ';' );

            text.Append( m_sizeScale.ToString() );
            text.Append( ';' );

            return text.ToString();
        }

        public override bool Deserialize( string text )
        {
            if ( !base.Deserialize( text ) )
            {
                return false;
            }

            string[] split = text.Split( new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries );
            if ( split.Length < 6 )
            {
                return false;
            }

            m_colorizeBy = (MemoryLayoutControl.DisplayType)Enum.Parse( typeof( MemoryLayoutControl.DisplayType ), split[2] );
            m_bytesPerPixel = (MemoryLayoutControl.Bytes)Enum.Parse( typeof( MemoryLayoutControl.Bytes ), split[3] );
            m_pixelWidth = (MemoryLayoutControl.Widths)Enum.Parse( typeof( MemoryLayoutControl.Widths ), split[4] );
            m_sizeScale = (MemoryLayoutControl.Sizes)Enum.Parse( typeof( MemoryLayoutControl.Sizes ), split[5] );

            return true;
        }
        #endregion
    }
}
