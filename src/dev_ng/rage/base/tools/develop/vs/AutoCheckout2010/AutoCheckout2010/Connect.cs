using Microsoft.VisualStudio.CommandBars;
using System;
using System.Globalization;
using System.Resources;
using System.Reflection;
using System.Windows.Forms;
using Extensibility;
using EnvDTE;
using EnvDTE80;
namespace AutoCheckout2010
{
    ///  !!!!!!!!!!!!!!!! p4 sync Assemblyinfo.cs#1 to get this p.o.s. to compile !!!!!!!!!!!!!!!!!!!!
    /// 
	/// <summary>The object for implementing an Add-in.</summary>
	/// <seealso class='IDTExtensibility2' />
	public class Connect : IDTExtensibility2 , IDTCommandTarget
	{
        private bool Disabled = false;

		/// <summary>Implements the constructor for the Add-in object. Place your initialization code within this method.</summary>
		public Connect()
		{
		}

		/// <summary>Implements the OnConnection method of the IDTExtensibility2 interface. Receives notification that the Add-in is being loaded.</summary>
		/// <param term='application'>Root object of the host application.</param>
		/// <param term='connectMode'>Describes how the Add-in is being loaded.</param>
		/// <param term='addInInst'>Object representing this Add-in.</param>
		/// <seealso class='IDTExtensibility2' />
		public void OnConnection(object application, ext_ConnectMode connectMode, object addInInst, ref Array custom)
		{
			_applicationObject = (DTE2)application;
			_addInInstance = (AddIn)addInInst;

            textEditorEvents = _applicationObject.Events.get_TextEditorEvents(null);
            textEditorEvents.LineChanged += new _dispTextEditorEvents_LineChangedEventHandler(LineChanged);

            EnvDTE80.Events2 events = (EnvDTE80.Events2)_applicationObject.Events;
            textDocKeyEvents =(EnvDTE80.TextDocumentKeyPressEvents) events.get_TextDocumentKeyPressEvents(null);
            // Connect to the BeforeKeyPress delegate exposed from the
            // TextDocumentKeyPressEvents object retrieved above.
            textDocKeyEvents.BeforeKeyPress += new _dispTextDocumentKeyPressEvents_BeforeKeyPressEventHandler(BeforeKeyPress);

            OutputWindow outputWindow = (OutputWindow)_applicationObject.Windows.Item(Constants.vsWindowKindOutput).Object;
            outputWindowPane = outputWindow.OutputWindowPanes.Add("Perforce");

            outputWindowPane.Activate();
            outputWindowPane.OutputString("p4 autocheckout addin installed, version 1.10\n");
            outputWindowPane.OutputString("AUTOEXEC_VS environment variable can contain semicolon-delimited list of commands to run at startup.\n");
            outputWindowPane.OutputString("If DevStudio hangs when you press a key in a new file, the p4 server may be down for maintenance.\n");
            outputWindowPane.OutputString("If you TASKKILL /IM p4.exe one or twice, it should recover, after which the plugin will disable\n");
            outputWindowPane.OutputString("itself until the next time you launch DevStudio.\n");

            String autoexec = Environment.GetEnvironmentVariable("AUTOEXEC_VS");
            if (autoexec != null)
            {
                // outputWindowPane.OutputString("Executing contents of AUTOEXEC_VS env var: " + autoexec + "\n");
                String[] commands = autoexec.Split(new char[] { ';' });
                foreach (String i in commands)
                {
                    // outputWindowPane.OutputString("[[["+i+"]]]\n");
                    System.Diagnostics.Process auto = new System.Diagnostics.Process();
                    int space = i.IndexOf(' ');
                    auto.StartInfo.UseShellExecute = false;
                    auto.StartInfo.FileName = space==-1? i : i.Substring(0,space);
                    auto.StartInfo.RedirectStandardOutput = true;
                    auto.StartInfo.RedirectStandardError = true;
                    auto.StartInfo.CreateNoWindow = true;
                    // auto.StartInfo.WorkingDirectory = ;
                    auto.StartInfo.Arguments = space==-1? "" : i.Substring(space+1);
                    outputWindowPane.OutputString("Running '" + auto.StartInfo.FileName + "' '" + auto.StartInfo.Arguments + "'...\n");
                    auto.Start();
                    auto.WaitForExit();
                }
            }

            AttachMenu();
        }

        private void AttachMenu()
        {
            object []contextGUIDS = new object[] { };
            Commands2 commands = (Commands2)_applicationObject.Commands;
            string toolsMenuName;

            try
            {
                ResourceManager resourceManager = new     
                  ResourceManager("AutoCheckout.CommandBar",  
                  Assembly.GetExecutingAssembly());
                CultureInfo cultureInfo = new 
                  System.Globalization.CultureInfo
                  (_applicationObject.LocaleID);
                string resourceName = String.Concat(cultureInfo.
                  TwoLetterISOLanguageName, "Tools");
                toolsMenuName = resourceManager.GetString(resourceName);
            }
            catch
            {
                toolsMenuName = "Tools";
            }

            CommandBar menuBarCommandBar = 
              ((CommandBars)_applicationObject.CommandBars)
              ["MenuBar"];

              CommandBarControl toolsControl = 
                menuBarCommandBar.Controls[toolsMenuName];
              CommandBarPopup toolsPopup = 
                (CommandBarPopup)toolsControl;

              try
              {
                  Command command = commands.AddNamedCommand2
                    (_addInInstance, "ResetAutoCheckout", "Reset AutoCheckout", "Re-enables auto-checkout plug-in after it has auto-deactivated", false, /*59*/ null, ref 
                    contextGUIDS, (int)vsCommandStatus.
                    vsCommandStatusSupported+(int)vsCommandStatus.
                    vsCommandStatusEnabled, (int)vsCommandStyle.
                    vsCommandStylePictAndText, vsCommandControlType.
                    vsCommandControlTypeButton);

                  if((command != null) && (toolsPopup != null))
                  {
                      command.AddControl(toolsPopup.CommandBar, 1);
                  }
            }
            catch(System.ArgumentException)
            {
            }
        }

        /// <summary>
        ///  Update status for menu item to make sure it's enabled
        /// </summary>
        /// <param name="commandName"></param>
        /// <param name="neededText"></param>
        /// <param name="status"></param>
        /// <param name="commandText"></param>
        public void QueryStatus(string commandName,
  vsCommandStatusTextWanted neededText, ref vsCommandStatus status,
  ref object commandText)
        {
            if (neededText ==
              vsCommandStatusTextWanted.vsCommandStatusTextWantedNone)
            {
                if (commandName == "AutoCheckout2010.Connect.ResetAutoCheckout")
                {
                    status = (vsCommandStatus)vsCommandStatus.
                      vsCommandStatusSupported | vsCommandStatus.
                      vsCommandStatusEnabled;
                    return;
                }
            }
        }

        /// <summary>
        /// Handle the menu item.
        /// </summary>
        /// <param name="commandName"></param>
        /// <param name="executeOption"></param>
        /// <param name="varIn"></param>
        /// <param name="varOut"></param>
        /// <param name="handled"></param>
        public void Exec(string commandName, vsCommandExecOption
          executeOption, ref object varIn, ref object varOut, ref bool
          handled)
        {
            handled = false;
            if (executeOption ==
              vsCommandExecOption.vsCommandExecOptionDoDefault)
            {
                if (commandName == "AutoCheckout2010.Connect.ResetAutoCheckout")
                {
                    if (Disabled)
                    {
                        Disabled = false;
                        handled = true;
                        System.Windows.Forms.MessageBox.
                          Show("Auto-checkout has been re-enabled.");
                    }
                    else
                    {
                        System.Windows.Forms.MessageBox.
                          Show("Auto-checkout wasn't even disabled. Maybe something else is wrong.");
                    }
                    return;
                }
            }
        }



		/// <summary>Implements the OnDisconnection method of the IDTExtensibility2 interface. Receives notification that the Add-in is being unloaded.</summary>
		/// <param term='disconnectMode'>Describes how the Add-in is being unloaded.</param>
		/// <param term='custom'>Array of parameters that are host application specific.</param>
		/// <seealso class='IDTExtensibility2' />
		public void OnDisconnection(ext_DisconnectMode disconnectMode, ref Array custom)
		{
		}

		/// <summary>Implements the OnAddInsUpdate method of the IDTExtensibility2 interface. Receives notification when the collection of Add-ins has changed.</summary>
		/// <param term='custom'>Array of parameters that are host application specific.</param>
		/// <seealso class='IDTExtensibility2' />		
		public void OnAddInsUpdate(ref Array custom)
		{
		}

		/// <summary>Implements the OnStartupComplete method of the IDTExtensibility2 interface. Receives notification that the host application has completed loading.</summary>
		/// <param term='custom'>Array of parameters that are host application specific.</param>
		/// <seealso class='IDTExtensibility2' />
		public void OnStartupComplete(ref Array custom)
		{
        }

		/// <summary>Implements the OnBeginShutdown method of the IDTExtensibility2 interface. Receives notification that the host application is being unloaded.</summary>
		/// <param term='custom'>Array of parameters that are host application specific.</param>
		/// <seealso class='IDTExtensibility2' />
		public void OnBeginShutdown(ref Array custom)
		{
		}

        void p4edit()
        {
            if (Disabled)
            {
                outputWindowPane.Clear();
                outputWindowPane.OutputString("...plugin is disabled!!!!!!!!!!!\n");
                outputWindowPane.Activate();
                return;
            }

            // To work on this plugin, you have to uninstall the copied version.
            // Note, the AssemblyInfo.cs in perforce is fucked up.
            // To get a working version, sync AssemlyInfo.cs#1.  Sigh.
            string file_path = _applicationObject.ActiveDocument.Path;
            string file_name = _applicationObject.ActiveDocument.Name;

            System.Diagnostics.Process checkout = new System.Diagnostics.Process();
            checkout.StartInfo.UseShellExecute = false;
            checkout.StartInfo.FileName = "p4.exe";
            checkout.StartInfo.RedirectStandardOutput = true;
            checkout.StartInfo.RedirectStandardError = true;
            checkout.StartInfo.CreateNoWindow = true;
            checkout.StartInfo.WorkingDirectory = file_path;
            checkout.StartInfo.Arguments = "edit \"" + file_name + "\"";
            outputWindowPane.OutputString("Running '" + checkout.StartInfo.FileName + " " + checkout.StartInfo.Arguments + "'...\n");

            // Make sure they see our status updates:
            checkout.Start();
            if (!checkout.WaitForExit(30000))
            {
                outputWindowPane.Clear();
                outputWindowPane.Activate();
                outputWindowPane.OutputString("...timed out (30 seconds), server is probably down for maintenance.\n");
                Disabled = true;
            }
            
            outputWindowPane.OutputString(checkout.StandardOutput.ReadToEnd());
            outputWindowPane.OutputString(checkout.StandardError.ReadToEnd());
        }

        void LineChanged(TextPoint StartPoint, TextPoint EndPoint, int Hint)
        {
            // Ignore caret movement when that's all there is.
            if (_applicationObject.ActiveDocument.ReadOnly && Hint != 4)
            {
                outputWindowPane.OutputString("...line change detected...\n");
                p4edit();
            }
        }

        void BeforeKeyPress(string Keypress, EnvDTE.TextSelection Selection, bool InStatementCompletion, ref bool CancelKeypress)
        {
            if (_applicationObject.ActiveDocument.ReadOnly)
            {
                outputWindowPane.OutputString("...alphanumeric keypress detected...\n");
                p4edit();
            }
        }
		
		private DTE2 _applicationObject;
		private AddIn _addInInstance;
        private EnvDTE.TextEditorEvents textEditorEvents;
        private EnvDTE80.TextDocumentKeyPressEvents textDocKeyEvents;
        private OutputWindowPane outputWindowPane;
    }
}