//
// logger.cpp
//
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved.
//
// PURPOSE
//		A simple logger class for debugging. It appends to an already existing file. It can be 
//		disabled easily through the constructor.
//

// Main Include ///////////////////////////////////////////////////////////////////////////////////

#include "logger.h"

// Library Includes ///////////////////////////////////////////////////////////////////////////////

#include <stdarg.h>
#include <stdlib.h>

// Methods ////////////////////////////////////////////////////////////////////////////////////////

Logger::Logger( char const* filename, bool enabled )
	: m_filename( filename )
	, m_enabled( enabled )
{
}

Logger::~Logger()
{
}

void Logger::Write( char const* format, ... )
{
	if ( m_enabled == false )
	{
		return;
	}

	FILE* fileHandle;

	if ( fopen_s( &fileHandle, m_filename.c_str(), "at" ) != 0 )
	{
		return;
	}

	char buff[ 256 ];

	va_list arglist;

	va_start( arglist, format );
	int length = vsprintf_s( buff, sizeof( buff ) - 1, format, arglist );
	va_end( arglist );

	if ( length <= 0 )
	{
		return;
	}

	buff[ sizeof( buff ) - 1 ] = '\0';

	fwrite( buff, length, 1, fileHandle );
	fflush( fileHandle );

	fclose( fileHandle );
}
