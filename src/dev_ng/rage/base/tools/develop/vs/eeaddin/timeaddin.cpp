/** 
 *  http://msdn.microsoft.com/en-us/library/8fwk67y3%28VS.80%29.aspx
 *
 *	To make this work I had to:
 *	- Modify the !ReadDebuggeeMemoryEx() to ReadDebuggeeMemoryEx()!=S_OK
 *	- Change the Release build linker configuration, so it uses the eeaddin.def 
 *	- Don't use the @28 syntax in the autoexp.dat
 */
#include <windows.h>
#include <stdio.h>
#include <string.h>
#include <tchar.h>
#include <assert.h>

#include "custview.h"

#include "TimeAddIn.h"
#include "logger.h"

enum TYPE_ENUM
{
	TYPE_INVALID			= -1,
	TYPE_FILE,
	TYPE_DUMMY,

	TYPE_ENUM_COUNT
};

typedef INT8		s8;
typedef INT16		s16;
typedef INT32		s32;
typedef UINT8		u8;
typedef UINT16		u16;
typedef UINT32		u32;

// NOTE
//		This structure is a copy of strIndexDebugManager::Entity and must be
//		kept in sync.
//
struct strIndexEntry
{
	s8			m_refCount;
	s8			m_type;
	char		m_description[ 62 ];
};

Logger g_logger( "x:\\eeaddin.log", false ); // Disable logging.
//Logger g_logger( "x:\\eeaddin.log", true ); // Enable logging.

// Helper routine that converts a system time into ASCII

static HRESULT FormatDateTime( SYSTEMTIME *pSystemTime, char *pResult, size_t max )
{
	GetDateFormat( GetThreadLocale(), DATE_SHORTDATE, pSystemTime, NULL, pResult, (int)max );
	size_t len = _tcslen( pResult );
	if ( (max-len) < 2 )
		return E_FAIL;						// if not enough room in buffer
	pResult[ len ] = ' ';
	len++;
	GetTimeFormat( GetThreadLocale(), TIME_NOSECONDS, pSystemTime, NULL, pResult + len, (int)(max - len) );
	return S_OK;
}

ADDIN_API HRESULT WINAPI AddIn_SystemTime( DWORD dwAddress, DEBUGHELPER *pHelper, int nBase, BOOL bUniStrings, char *pResult, size_t max, DWORD reserved )
{
	SYSTEMTIME SysTime;
	DWORD nGot;

	// read system time from debuggee memory space
	if (pHelper->ReadDebuggeeMemoryEx(pHelper, pHelper->GetRealAddress(pHelper), sizeof(SysTime), &SysTime, &nGot)!=S_OK )
		return E_FAIL;
	if (nGot!=sizeof(SysTime))
		return E_FAIL;

	return FormatDateTime( &SysTime, pResult, max );
}

ADDIN_API HRESULT WINAPI AddIn_FileTime( DWORD dwAddress, DEBUGHELPER *pHelper, int nBase, BOOL bUniStrings, char *pResult, size_t max, DWORD reserved )
{
	FILETIME FileTime;
	SYSTEMTIME SysTime;
	DWORD nGot;

	// read file time from debuggee memory space
	if (pHelper->ReadDebuggeeMemoryEx(pHelper, pHelper->GetRealAddress(pHelper), sizeof(FileTime), &FileTime, &nGot)!=S_OK )
		return E_FAIL;
	if (nGot!=sizeof(FileTime))
		return E_FAIL;

	// convert to SystemTime
	if (!FileTimeToSystemTime( &FileTime, &SysTime ))
		return E_FAIL;

	return FormatDateTime( &SysTime, pResult, max );
}

#include <windows.h>

typedef struct {
	LARGE_INTEGER start;
	LARGE_INTEGER stop;
} stopWatch;

class CStopWatch {

private:
	stopWatch timer;
	LARGE_INTEGER frequency;
	double LIToSecs( LARGE_INTEGER & L);
public:
	CStopWatch();
	void startTimer( );
	void stopTimer( );
	double getElapsedTime();
	double CStopWatch::getElapsedTimeMs();
};

double CStopWatch::LIToSecs( LARGE_INTEGER & L) {
	return ((double)L.QuadPart /(double)frequency.QuadPart) ;
}

CStopWatch::CStopWatch(){
	timer.start.QuadPart=0;
	timer.stop.QuadPart=0; 
	QueryPerformanceFrequency( &frequency ) ;
}

void CStopWatch::startTimer( ) {
	QueryPerformanceCounter(&timer.start) ;
}

void CStopWatch::stopTimer( ) {
	QueryPerformanceCounter(&timer.stop) ;
}

double CStopWatch::getElapsedTime() {
	LARGE_INTEGER time;
	time.QuadPart = timer.stop.QuadPart - timer.start.QuadPart;
	return LIToSecs( time) ;
}

double CStopWatch::getElapsedTimeMs() {
	LARGE_INTEGER time;
	time.QuadPart = timer.stop.QuadPart - timer.start.QuadPart;
	return LIToSecs( time) * 1000 ;
}

struct Vector
{
	float x, y, z, w;
};



//////////////////////////////////////////////////////////////////////////
BOOL g_Initialised;
BOOL g_bigEndian;
BOOL g_is64bit;
DWORD g_debugDataBuffer[1024];
unsigned int g_debugDataBufferSize;

DWORD64* g_debugDataBuffer64 = reinterpret_cast<DWORD64*>(&g_debugDataBuffer[0]);

static const unsigned int d3bg(MAKEFOURCC('d', '3', 'b', 'g'));
static const unsigned int gb3g(MAKEFOURCC('g', 'b', '3', 'd'));
static const unsigned int db64(MAKEFOURCC('d', 'b', '6', '4'));

void Swapper(unsigned int &instance) {
	if (g_bigEndian) {
		instance = (instance >> 24) | ((instance >> 8) & 0xFF00) | ((instance << 8) & 0xFF0000) | (instance << 24);
	}
}

void Swapper(float &instance) {
	Swapper((unsigned int &) instance);
}

void Swapper(unsigned short &instance) {
	if (g_bigEndian) {
		instance = (instance >> 8) | (instance<< 8);
	}
}

void Swapper(Vector &vector) {
	Swapper(vector.x);
	Swapper(vector.y);
	Swapper(vector.z);
	Swapper(vector.w);
}

static BOOL GetDebugData(DEBUGHELPER *pHelper, char *pResult, int max)
{
	if (g_Initialised)
		return TRUE;

	// See bootmgr_common.cpp - these are arbitrary, known addresses that we allocate on each platform to hold data for this debugger plugin.
	// There are multiple addresses because each platform has its own address space, and we don't know which one we're debugging
	static DWORD64 knownAddresses[] = {
		0x000007f000000000,	// PC
		0x70070000,			// PS4
		0x0000020700000000, // XBONE
		0x00070000,			// XENON
		0,
	};

	BOOL readOK = FALSE;
	int addressIdx = 0;

	do 
	{
		DWORD nGot;
		if (pHelper->ReadDebuggeeMemoryEx(pHelper,knownAddresses[addressIdx],sizeof(g_debugDataBuffer),g_debugDataBuffer,&nGot)==S_OK) {

			// Check for a known 4cc
			switch(g_debugDataBuffer[0])
			{
			case d3bg:
				readOK = TRUE;
				g_logger.Write( "32 bit little endian\n" );
				break;
			case gb3g:
				g_bigEndian = TRUE;
				readOK = TRUE;
				g_logger.Write( "32 bit big endian\n" );
				break;
			case db64:
				g_is64bit = TRUE;
				g_logger.Write( "64 bit little endian\n" );
				readOK = TRUE;
				break;
			default:
				sprintf_s(pResult, max, "Error: Invalid debug memory - Bad FOURCC at 0x%016I64x = 0x%08x", knownAddresses[addressIdx], g_debugDataBuffer[0]);
				g_logger.Write( "%s\n", pResult );
				break;
			}
		}

		addressIdx++;
	} while (!readOK && knownAddresses[addressIdx] != 0);

	if (!readOK)
	{
		sprintf_s(pResult, max, "Error: Couldn't read debug memory");
	}

	if (g_is64bit)
	{
		g_debugDataBufferSize = (int)g_debugDataBuffer64[1];
	}
	else
	{
		g_debugDataBufferSize = g_debugDataBuffer[1];
		Swapper(g_debugDataBufferSize);
	}

	if (g_debugDataBufferSize > 1024) {
		sprintf_s(pResult, max, "Error: Too many values %d", g_debugDataBufferSize);
		return FALSE;
	}

	g_Initialised = TRUE;

	return TRUE;
}

static BOOL FindDebugData(unsigned int key, DWORD64& value)
{
	Swapper(key);

	if (g_is64bit)
	{
		for (unsigned int i = 2; i < g_debugDataBufferSize; ++i)	
		{
			if (key == g_debugDataBuffer64[i])
			{
				value = g_debugDataBuffer64[i+1];
				return TRUE;
			}
		}
	}
	else
	{
		for (unsigned int i = 2; i < g_debugDataBufferSize; ++i)	
		{
			if (key == g_debugDataBuffer[i])
			{
				unsigned int val = g_debugDataBuffer[i+1];
				Swapper(val);
				value = val;
				return TRUE;
			}
		}
	}

	return FALSE;
}


struct String
{
	char *			m_Data;			// String payload.  Ignored if length is zero, else is guaranteed to be null-terminated

	void Endian()
	{
		Swapper(reinterpret_cast<unsigned int&>(m_Data));	
	}
};

struct Entry
{
	unsigned int	key;
	String			data;
	Entry*			next;

	void Endian()
	{
		Swapper(key);	
		data.Endian();	
		Swapper(reinterpret_cast<unsigned int&>(next));	
	}
};

struct Map
{
	Entry **		m_Hash;			// Storage for toplevel hash array
	unsigned short	m_Slots;		// Number of slots in toplevel hash
	unsigned short 	m_Used;			// Number of those slots currently in use

	void Endian()
	{
		Swapper(reinterpret_cast<unsigned int&>(m_Hash));	
		Swapper(m_Slots);
		Swapper(m_Used);
	}
};



struct String64
{
	DWORD64			m_Data;			// String payload.  Ignored if length is zero, else is guaranteed to be null-terminated
};

struct Entry64
{
	unsigned int	key;
	String64		data;
	DWORD64			p_next;
};

struct Map64
{
	DWORD64			m_ppHash;		// Storage for toplevel hash array
	unsigned short	m_Slots;		// Number of slots in toplevel hash
	unsigned short 	m_Used;			// Number of those slots currently in use
};


ADDIN_API HRESULT WINAPI AddIn_Hash_Internal64( DWORD dwAddress, DEBUGHELPER *pHelper, int nBase, BOOL bUniStrings, char *pResult, size_t max, DWORD reserved, DWORD64 mapAddr )
{
	DWORD nGot;

	// get the hash
	DWORD hash;
	if (pHelper->ReadDebuggeeMemoryEx(pHelper, pHelper->GetRealAddress(pHelper), sizeof(hash), &hash, &nGot)!=S_OK )\
	{
		sprintf_s(pResult, max, "Error: Can't connect and retrieve the hash for this entry");
		return E_FAIL;
	}

	// read the nth atMap root entry
	Map64 map;
	if (pHelper->ReadDebuggeeMemoryEx(pHelper,mapAddr,sizeof(map),&map,&nGot)!=S_OK) {
		sprintf_s(pResult, max, "Error: Could not read Map");
		return S_OK;
	}

	sprintf_s(pResult, max, "m_hash=0x%08x", hash);
	if (map.m_Slots == 0)
	{
		return S_OK;
	}

	// find the entry from the modulo
	DWORD64 addressOfEntryPointer = (DWORD64)(map.m_ppHash + sizeof(DWORD64)*(hash % map.m_Slots));
	DWORD64 entryPointer;

	if (pHelper->ReadDebuggeeMemoryEx(pHelper,addressOfEntryPointer,sizeof(entryPointer),&entryPointer,&nGot)!=S_OK) {
		sprintf_s(pResult, max, "Error: Could not read entry address");
		return S_OK;
	}

	// follow the linked list of entries.
	while (entryPointer)
	{
		Entry64 entry;

		if (pHelper->ReadDebuggeeMemoryEx(pHelper,entryPointer,sizeof(entry),&entry,&nGot)!=S_OK) {
			sprintf_s(pResult, max, "Error: Could not read Entry");
			return S_OK;
		}

		if (entry.key == hash)
		{
			const String64& string = entry.data;

			const int MAX_STRING_LEN = 128;

			char hashString[MAX_STRING_LEN];
			memset(hashString,0, MAX_STRING_LEN);
			nGot = 0;

			if (pHelper->ReadDebuggeeMemoryEx(pHelper,string.m_Data,MAX_STRING_LEN,hashString,&nGot)!=S_OK && nGot == 0) {
				sprintf_s(pResult, max, "Error: Could not read string contents at %x%x %d",(unsigned int)(string.m_Data >> 32), (unsigned int)(string.m_Data & 0xFFFFFFFF), MAX_STRING_LEN);
				return S_OK;
			}

			if (strlen(hashString) >= MAX_STRING_LEN)
			{
				hashString[MAX_STRING_LEN-4] = '.';
				hashString[MAX_STRING_LEN-3] = '.';
				hashString[MAX_STRING_LEN-2] = '.';
				hashString[MAX_STRING_LEN-1] = '\0';
			}
			else if (nGot > 0)
			{
				hashString[nGot-1] = '\0';
			}

			sprintf_s(pResult, max, "m_hash=0x%08x \"%s\"", hash, hashString);

			break;
		}

		entryPointer = entry.p_next;
	}

	return S_OK;
}


#define STOPWATCH 0

ADDIN_API HRESULT WINAPI AddIn_Hash_Internal( DWORD dwAddress, DEBUGHELPER *pHelper, int nBase, BOOL bUniStrings, char *pResult, size_t max, DWORD reserved, DWORD64 mapAddr )
{
	if (g_is64bit)
	{
		return AddIn_Hash_Internal64(dwAddress, pHelper, nBase, bUniStrings, pResult, max, reserved, mapAddr);
	}

	DWORD nGot;

#if STOPWATCH
	CStopWatch stopwatch;
	stopwatch.startTimer();
#endif // STOPWATCH


	// get the hash
	unsigned int hash;
	if (pHelper->ReadDebuggeeMemoryEx(pHelper, pHelper->GetRealAddress(pHelper), sizeof(unsigned int), &hash, &nGot)!=S_OK )\
	{
		sprintf_s(pResult, max, "Error: Can't connect and retrieve the hash for this entry");
		return E_FAIL;
	}
	Swapper(hash);

	// read the nth atMap root entry
	Map map;
	if (pHelper->ReadDebuggeeMemoryEx(pHelper,mapAddr,sizeof(map),&map,&nGot)!=S_OK) {
		sprintf_s(pResult, max, "Error: Could not read Map");
		return S_OK;
	}

	sprintf_s(pResult, max, "m_hash=0x%08x", hash);
	if (map.m_Slots == 0)
	{
		return S_OK;
	}

	map.Endian();

	// find the entry from the modulo
	unsigned int addressOfEntryPointer = (unsigned int)(map.m_Hash + (hash % map.m_Slots));
	unsigned int entryPointer;

	if (pHelper->ReadDebuggeeMemoryEx(pHelper,addressOfEntryPointer,sizeof(Entry*),&entryPointer,&nGot)!=S_OK) {
		sprintf_s(pResult, max, "Error: Could not read entry address");
		return S_OK;
	}

	// follow the linked list of entries.
	Swapper(entryPointer);
	while (entryPointer)
	{
		Entry entry;

		if (pHelper->ReadDebuggeeMemoryEx(pHelper,entryPointer,sizeof(Entry),&entry,&nGot)!=S_OK) {
			sprintf_s(pResult, max, "Error: Could not read Entry");
			return S_OK;
		}

		entry.Endian();

		if (entry.key == hash)
		{
			const String& string = entry.data;

			const int MAX_STRING_LEN = 128;

			char* hashString = new char[MAX_STRING_LEN];
			memset(hashString,0, MAX_STRING_LEN);

			if (pHelper->ReadDebuggeeMemoryEx(pHelper,(unsigned int)string.m_Data,MAX_STRING_LEN,hashString,&nGot)!=S_OK) {
				sprintf_s(pResult, max, "Error: Could not read string contents %p %d",string.m_Data, MAX_STRING_LEN);
				return S_OK;
			}

			if (strlen(hashString) >= MAX_STRING_LEN)
			{
				hashString[MAX_STRING_LEN-4] = '.';
				hashString[MAX_STRING_LEN-3] = '.';
				hashString[MAX_STRING_LEN-2] = '.';
				hashString[MAX_STRING_LEN-1] = '\0';
			}

			sprintf_s(pResult, max, "m_hash=0x%08x \"%s\"", hash, hashString);

			delete [] hashString;
			break;
		}

		entryPointer = (unsigned int) entry.next;
	}

#if STOPWATCH
	char timer[255];
	stopwatch.stopTimer();
	sprintf_s(timer, " %2.3fms", stopwatch.getElapsedTimeMs());
	strcat_s(pResult, max, timer);
#endif // STOPWATCH

	return S_OK;
}



ADDIN_API HRESULT WINAPI AddIn_Hash( DWORD dwAddress, DEBUGHELPER *pHelper, int nBase, BOOL bUniStrings, char *pResult, size_t max, DWORD reserved )
{
	// read the debug data
	if (!GetDebugData(pHelper, pResult, max))
		return S_OK;

	// find the relevant entry pointer using the FOURCC
	DWORD64 hashStrings = 0;
	if (!FindDebugData(MAKEFOURCC('h', 'a', 's', 'h'), hashStrings)) {
		sprintf_s(pResult, max, "Error: Can't find debug data key with FOURCC. Buffer is %08x %08x %08x %08x", g_debugDataBuffer[0], g_debugDataBuffer[1], g_debugDataBuffer[2], g_debugDataBuffer[3]);
		return S_OK;
	}

	return AddIn_Hash_Internal(dwAddress, pHelper, nBase, bUniStrings, pResult, max, reserved, hashStrings);
}

ADDIN_API HRESULT WINAPI AddIn_FinalHash( DWORD dwAddress, DEBUGHELPER *pHelper, int nBase, BOOL bUniStrings, char *pResult, size_t max, DWORD reserved )
{
	// read the debug data
	if (!GetDebugData(pHelper, pResult, max))
		return S_OK;

	// find the relevant entry pointer using the FOURCC
	DWORD64 hashStrings = 0;
	if (!FindDebugData(MAKEFOURCC('f', 'h', 's', 'h'), hashStrings)) {
		sprintf_s(pResult, max, "Error: Can't find debug data key with FOURCC %d", g_debugDataBufferSize);
		return S_OK;
	}

	return AddIn_Hash_Internal(dwAddress, pHelper, nBase, bUniStrings, pResult, max, reserved, hashStrings);
}

ADDIN_API HRESULT WINAPI AddIn_LiteralHash( DWORD dwAddress, DEBUGHELPER *pHelper, int nBase, BOOL bUniStrings, char *pResult, size_t max, DWORD reserved )
{
	// read the debug data
	if (!GetDebugData(pHelper, pResult, max))
		return S_OK;

	// find the relevant entry pointer using the FOURCC
	DWORD64 hashStrings = 0;
	if (!FindDebugData(MAKEFOURCC('l', 'h', 's', 'h'), hashStrings)) {
		sprintf_s(pResult, max, "Error: Can't find debug data key with FOURCC");
		return S_OK;
	}

	return AddIn_Hash_Internal(dwAddress, pHelper, nBase, bUniStrings, pResult, max, reserved, hashStrings);
}


ADDIN_API HRESULT WINAPI AddIn_NamespacedHash_Internal( DWORD dwAddress, DEBUGHELPER *pHelper, int nBase, BOOL bUniStrings, char *pResult, size_t max, DWORD reserved, int nameSpace )
{
	// read the debug data
	if (!GetDebugData(pHelper, pResult, max))
		return S_OK;

	// find the relevant entry pointer using the FOURCC
	DWORD64 hashStringArr = 0;
	if (!FindDebugData(MAKEFOURCC('h', 's', 'h', 'n'), hashStringArr)) {
		// special cases for backwards compatibility
		switch(nameSpace)
		{
		case 0:	return AddIn_Hash(dwAddress, pHelper, nBase, bUniStrings, pResult, max, reserved);
		case 1: return AddIn_FinalHash(dwAddress, pHelper, nBase, bUniStrings, pResult, max, reserved);
		case 2: return AddIn_LiteralHash(dwAddress, pHelper, nBase, bUniStrings, pResult, max, reserved);
		}

		sprintf_s(pResult, max, "Error: Can't find debug data key with FOURCC. Buffer is %08x %08x %08x %08x", g_debugDataBuffer[0], g_debugDataBuffer[1], g_debugDataBuffer[2], g_debugDataBuffer[3]);
		return S_OK;
	}

	DWORD64 stride = g_is64bit ? sizeof(Map64) : sizeof(Map);

	return AddIn_Hash_Internal(dwAddress, pHelper, nBase, bUniStrings, pResult, max, reserved, hashStringArr + nameSpace * stride);
}

#define AddIn_NamespacedHash(n) ADDIN_API HRESULT WINAPI AddIn_NamespacedHash##n(DWORD dwAddress, DEBUGHELPER *pHelper, int nBase, BOOL bUniStrings, char *pResult, size_t max, DWORD reserved) \
{ \
	return AddIn_NamespacedHash_Internal(dwAddress, pHelper, nBase, bUniStrings, pResult, max, reserved, n); \
} \

AddIn_NamespacedHash(0);
AddIn_NamespacedHash(1);
AddIn_NamespacedHash(2);
AddIn_NamespacedHash(3);
AddIn_NamespacedHash(4);
AddIn_NamespacedHash(5);
AddIn_NamespacedHash(6);
AddIn_NamespacedHash(7);
AddIn_NamespacedHash(8);
AddIn_NamespacedHash(9);
AddIn_NamespacedHash(10);
AddIn_NamespacedHash(11);
AddIn_NamespacedHash(12);
AddIn_NamespacedHash(13);
AddIn_NamespacedHash(14);
AddIn_NamespacedHash(15);

struct fwTransform
{
	unsigned short m_Type;
	short m_Irrelevant;
};

struct fwMatrixTransform : public fwTransform
{
	int m_Pad[3];
	Vector m_Rows[4];
};

struct fwQuaternionTransform : public fwTransform
{
	float m_ScaleXY;
	float m_ScaleZ;
	int m_Pad;
	Vector m_Position;
	Vector m_Quaternion;
};

struct fwSimpleTransform : public fwTransform
{
	float m_SinHeading;
	float m_CosHeading;
	float m_Heading;
	Vector m_Position;
};

enum {
	TRANSFORM_SIMPLE,
	TRANSFORM_MATRIX,
	TRANSFORM_SIMPLE_SCALED,
	TRANSFORM_MATRIX_SCALED,
	TRANSFORM_IDENTITY,
	TRANSFORM_QUATERNION = 8,
	TRANSFORM_QUATERNION_SCALED = 10,
};

ADDIN_API HRESULT WINAPI AddIn_Transform( DWORD dwAddress, DEBUGHELPER *pHelper, int nBase, BOOL bUniStrings, char *pResult, size_t max, DWORD reserved )
{
	// read the debug data
	if (!GetDebugData(pHelper, pResult, max))
	{
		sprintf_s(pResult, max, "No debug data");
		return S_OK;
//		return E_FAIL;
	}

	fwTransform trans;
	DWORD size;

	pHelper->ReadDebuggeeMemoryEx(pHelper, dwAddress, sizeof(fwTransform), &trans, &size);

	if (size < sizeof(fwTransform))
	{
		sprintf_s(pResult, max, "Only %d bytes read", g_debugDataBufferSize);
		return S_OK;
//		return E_FAIL;
	}

	Swapper(trans.m_Type);

	switch (trans.m_Type)
	{
	case TRANSFORM_SIMPLE:
		{
			fwSimpleTransform st;
			pHelper->ReadDebuggeeMemoryEx(pHelper, dwAddress, sizeof(fwSimpleTransform), &st, &size);
			Swapper(st.m_Position);
			Swapper(st.m_Heading);

			sprintf_s(pResult, max, "fwSimpleTransform <%f, %f, %f>, Heading %f",
				st.m_Position.x, st.m_Position.y, st.m_Position.z, st.m_Heading);

			return S_OK;
		}

	case TRANSFORM_SIMPLE_SCALED:
		{
			fwSimpleTransform st;
			pHelper->ReadDebuggeeMemoryEx(pHelper, dwAddress, sizeof(fwSimpleTransform), &st, &size);
			Swapper(st.m_Position);
			Swapper(st.m_Heading);

			sprintf_s(pResult, max, "fwSimpleScaledTransform <%f, %f, %f>, Heading %f",
				st.m_Position.x, st.m_Position.y, st.m_Position.z, st.m_Heading);

			return S_OK;
		}

	case TRANSFORM_MATRIX:
		{
			fwMatrixTransform mt;
			pHelper->ReadDebuggeeMemoryEx(pHelper, dwAddress, sizeof(fwMatrixTransform), &mt, &size);
			Swapper(mt.m_Rows[0]);
			Swapper(mt.m_Rows[1]);
			Swapper(mt.m_Rows[2]);
			Swapper(mt.m_Rows[3]);

			sprintf_s(pResult, max, "fwMatrixTransform <<%f, %f, %f>, <%f, %f, %f>, <%f, %f, %f>, <%f, %f, %f>>",
				mt.m_Rows[0].x, mt.m_Rows[0].y, mt.m_Rows[0].z, mt.m_Rows[0].w,
				mt.m_Rows[1].x, mt.m_Rows[1].y, mt.m_Rows[1].z, mt.m_Rows[1].w,
				mt.m_Rows[2].x, mt.m_Rows[2].y, mt.m_Rows[2].z, mt.m_Rows[2].w,
				mt.m_Rows[3].x, mt.m_Rows[3].y, mt.m_Rows[3].z, mt.m_Rows[3].w);

			return S_OK;
		}

	case TRANSFORM_MATRIX_SCALED:
		{
			fwMatrixTransform mt;
			pHelper->ReadDebuggeeMemoryEx(pHelper, dwAddress, sizeof(fwMatrixTransform), &mt, &size);
			Swapper(mt.m_Rows[0]);
			Swapper(mt.m_Rows[1]);
			Swapper(mt.m_Rows[2]);
			Swapper(mt.m_Rows[3]);

			sprintf_s(pResult, max, "fwMatrixScaledTransform <<%f, %f, %f>, <%f, %f, %f>, <%f, %f, %f>, <%f, %f, %f>>",
				mt.m_Rows[0].x, mt.m_Rows[0].y, mt.m_Rows[0].z, mt.m_Rows[0].w,
				mt.m_Rows[1].x, mt.m_Rows[1].y, mt.m_Rows[1].z, mt.m_Rows[1].w,
				mt.m_Rows[2].x, mt.m_Rows[2].y, mt.m_Rows[2].z, mt.m_Rows[2].w,
				mt.m_Rows[3].x, mt.m_Rows[3].y, mt.m_Rows[3].z, mt.m_Rows[3].w);

			return S_OK;
		}

	case TRANSFORM_IDENTITY:
		sprintf_s(pResult, max, "fwIdentityTransform");
		return S_OK;

	case TRANSFORM_QUATERNION:
		{
			fwQuaternionTransform qt;
			pHelper->ReadDebuggeeMemoryEx(pHelper, dwAddress, sizeof(fwQuaternionTransform), &qt, &size);
			Swapper(qt.m_Position);
			Swapper(qt.m_Quaternion);

			sprintf_s(pResult, max, "fwQuaternionTransform <<%f, %f, %f, %f>, pos=<%f, %f, %f>",
				qt.m_Quaternion.x, qt.m_Quaternion.y, qt.m_Quaternion.z, qt.m_Quaternion.w,
				qt.m_Position.x, qt.m_Position.y, qt.m_Position.z);

			return S_OK;
		}

	case TRANSFORM_QUATERNION_SCALED:
		{
			fwQuaternionTransform qt;
			pHelper->ReadDebuggeeMemoryEx(pHelper, dwAddress, sizeof(fwQuaternionTransform), &qt, &size);
			Swapper(qt.m_Position);
			Swapper(qt.m_Quaternion);

			sprintf_s(pResult, max, "fwQuaternionScaledTransform <<%f, %f, %f, %f>, pos=<%f, %f, %f>",
				qt.m_Quaternion.x, qt.m_Quaternion.y, qt.m_Quaternion.z, qt.m_Quaternion.w,
				qt.m_Position.x, qt.m_Position.y, qt.m_Position.z);

			return S_OK;
		}
	}

	sprintf_s(pResult, max, "Bad type (%d)", trans.m_Type);
	return S_OK;

//	return E_FAIL;
}

ADDIN_API HRESULT WINAPI AddIn_strIndex( DWORD dwAddress, DEBUGHELPER *pHelper, int nBase, BOOL bUniStrings, char *pResult, size_t max, DWORD reserved )
{
	// read the debug data

	g_logger.Write( "Called AddIn_strIndex!\n" );
	
	if (!GetDebugData(pHelper, pResult, max))
	{
		g_logger.Write( "%s\n", pResult );

		return S_OK;
	}

	// find the relevant entry pointer using the FOURCC
	DWORD64 strIndexArray = 0;

	if (!FindDebugData( MAKEFOURCC('s', 't', 'r', 'i'), strIndexArray ) ) 
	{
		sprintf_s(pResult, max, "Error: Can't find debug data key with FOURCC. Buffer is %08x %08x %08x %08x", g_debugDataBuffer[0], g_debugDataBuffer[1], g_debugDataBuffer[2], g_debugDataBuffer[3]);
		g_logger.Write( "%s\n", pResult );

		return S_OK;
	}

	DWORD dstSize;

	// get the entry index (called hash in the game but it's an index rather than a hash).

	DWORD32 entryIndex;

	if ( pHelper->ReadDebuggeeMemoryEx(pHelper, pHelper->GetRealAddress( pHelper ), sizeof( entryIndex ), &entryIndex, &dstSize) != S_OK )
	{
		sprintf_s( pResult, max, "Error: Can't connect and retrieve the hash for this entry");
		g_logger.Write( "%s\n", pResult );

		return S_OK;
	}

	g_logger.Write( "found entry index %d\n", entryIndex );

	Swapper( entryIndex );

	// read the entry data.

	strIndexEntry entry;

	DWORD64 offset = strIndexArray + entryIndex * sizeof( strIndexEntry );

	if ( pHelper->ReadDebuggeeMemoryEx( pHelper, offset, sizeof( strIndexEntry ), &entry, &dstSize ) != S_OK ) 
	{
		sprintf_s(pResult, max, "Error: Could not read Map");
		g_logger.Write( "%s\n", pResult );

		return S_OK;
	}

	if ( dstSize != sizeof( strIndexEntry ) )
	{
		sprintf_s(	pResult, 
					max, 
					"Error: Could not read strIndex debug data. Requested %db but received %db.", 
					sizeof( strIndexEntry ), 
					dstSize );

		g_logger.Write( "%s\n", pResult );

		return S_OK;
	}

	entry.m_description[ sizeof( entry.m_description ) - 1 ] = '\0';

	switch ( entry.m_type )
	{

	case TYPE_INVALID:
		sprintf_s(pResult, max, "index=%d refcount=%i (unknown type)'", entryIndex, entry.m_refCount );
		break;

	case TYPE_FILE:
		sprintf_s(pResult, max, "index=%d refcount=%i file='%s'", entryIndex, entry.m_refCount, entry.m_description );
		break;

	case TYPE_DUMMY:
		sprintf_s(pResult, max, "index=%d refcount=%i dummy='%s'", entryIndex, entry.m_refCount, entry.m_description );
		break;
	}

	g_logger.Write( "%s\n", pResult );

	return S_OK;
}

