Imports EnvDTE
Imports System.Diagnostics
Imports Microsoft.VisualStudio.VCProjectEngine

Public Module IDE

    ' AUTHOR: Michael Kelley (kelley@rockstarsandiego.com)
    ' PURPOSE:  brings up an input box that allows you to modify just the 
    '           command line parameters.  I suggest you map it to ALT-F7
    Sub CommandLineArgumentDialogBox()

        'find the active solution
        Dim sb As SolutionBuild = DTE.Solution.SolutionBuild
        'MsgBox(sb.ActiveConfiguration.Name())

        Dim cfgs, tools As IVCCollection
        Dim prj As VCProject = DTE.Solution.Projects.Item(1).Object
        cfgs = prj.Configurations
        Dim idx

        ' find active configuration and get input for command line arguments:
        For idx = 1 To cfgs.Count
            Dim cfg As VCConfiguration = cfgs.Item(idx)
            Dim cfgName As String = cfg.Name
            cfgName = cfgName.Replace("|", " ")
            ' SLIGHTLLLLLY HACKY? using two different paths
            ' to determine active configuration and the 
            ' current configuration name
            If (cfgName = sb.ActiveConfiguration.Name()) Then
                Dim tool As VCDebugSettings
                tool = cfg.DebugSettings
                Dim Input As String = tool.CommandArguments
                Input = InputBox("Enter your Command line arguments (put in a space for none):", "'" + cfgName + "' Command line arguments", Input)
                If (Input <> "") Then
                    tool.CommandArguments = Input
                End If
                Exit For
            End If


            ' myfile.Name has the name of the file
        Next

        'MsgBox(tool.CommandArguments)
    End Sub

    ' AUTHOR: Russell Schaaf (rschaaf@rockstarsandiego.com)
    ' PURPOSE: brings up the project properties dialog box for the root project.
    '          Another useful thing to map to Alt-F7 (for a more VS6 style than above)
    Sub StartUpProperties()
        DTE.Windows.Item(Constants.vsWindowKindSolutionExplorer).Activate()
        DTE.ActiveWindow.Object.GetItem(DTE.Solution.Properties.Item("Name").Value + "\" + DTE.Solution.Properties.Item("StartupProject").Value).Select(vsUISelectionType.vsUISelectionTypeSelect)
        DTE.ExecuteCommand("Project.Properties")
    End Sub



    ' Author: Russ Schaaf
    ' Purpose: Assembles selected text as an array of strings
    ' code mostly taken from http://www.codeguru.com/Cpp/V-S/devstudio_macros/article.php/c3151/
    Function CollectLines(ByVal Selection)
        '-- make sure the top of the selection is really the top
        Dim StartLine = Selection.TopLine
        Dim EndLine = Selection.BottomLine
        If EndLine < StartLine Then
            Dim Temp = "StartLine"
            StartLine = "EndLine"
            EndLine = "Temp"
        End If
        Dim lines() '-- don't try to collect an empty selection 
        If StartLine > EndLine Then
            ReDim lines(0)
            CollectLines = lines
            Exit Function
        End If

        '-- collect all the lines of the selection into an array
        '-- this could be prohibitive on large selections ( > 2M ? )
        ReDim lines(EndLine - StartLine)
        Dim i
        For i = StartLine To EndLine
            Selection.GoToLine(i)
            Selection.SelectLine()
            lines(EndLine - i) = Selection.Text
        Next

        CollectLines = lines
    End Function

    ' Author: Russ Schaaf
    ' Purpose: Sorts all the lines in the current selection
    ' code mostly taken from http://www.codeguru.com/Cpp/V-S/devstudio_macros/article.php/c3151/
    Sub SortSelectedText()
        'DESCRIPTION: Sorts the selected lines
        If ActiveDocument.Type <> "Text" Then
            MsgBox("This macro can only be run when a text editor window is active.")
            Exit Sub
        End If

        '-- make sure the top of the selection is really the top
        Dim StartLine = ActiveDocument.Selection.TopLine
        Dim EndLine = ActiveDocument.Selection.BottomLine
        If EndLine < StartLine Then
            Dim Temp = StartLine
            StartLine = EndLine
            EndLine = Temp
        End If

        '-- collect the lines of the selection into an array
        Dim lines() As Object
        lines = CollectLines(ActiveDocument.Selection)

        If (UBound(lines) <= 0) Then
            '-- don't try to sort an empty selection
            Exit Sub
        End If

        '-- sort the array
        System.Array.Sort(lines)

        'Call QuickSort(lines, LBound(lines), UBound(lines), 0, 0)

        '-- select the entire original selection, then delete it
        ActiveDocument.Selection.GoToLine(StartLine)

        ActiveDocument.Selection.LineDown(True, (EndLine - StartLine) + 1)
        ActiveDocument.Selection.Delete()

        '-- write the sorted lines out to the file
        Dim foo
        foo = String.Concat(lines)
        ActiveDocument.Selection.Insert(String.Concat(lines))

    End Sub


End Module
