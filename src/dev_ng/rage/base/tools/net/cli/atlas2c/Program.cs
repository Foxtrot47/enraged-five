﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;

using Rockstar.Gamespy.Atlas;

namespace atlas2c
{
    class Program
    {
        //Version history:
        //  1.0 Base version
        //  1.1 Added support for reading custom rule operations

        const uint MajorVersion = 1;
        const uint MinorVersion = 1;

        static string ProgramName { get { return string.Format("atlas2c v{0}.{1}", MajorVersion, MinorVersion); } }
        static string Credits { get { return "Created by " + ProgramName + " " + DateTime.Now; } }

        static void WriteLine(string format, params object[] args)
        {
            Trace.WriteLine(string.Format(format, args));
        }

        static string CreateFilename(string dir, string filename)
        {
            if (!string.IsNullOrEmpty(dir))
            {
                char[] trimChars = { '/', '\\' };
                filename = dir.TrimEnd(trimChars) + "\\" + filename;
            }
            return filename;
        }

        static void Usage()
        {
            WriteLine(ProgramName);
            WriteLine("Usage: atlas2c [atlas.xml] {{-outdir [path]}} {{-inc [conditional inclusion expression]}}");
        }

        static void Main(string[] args)
        {
            Trace.Listeners.Add(new ConsoleTraceListener());

            try
            {
                string inFilename = "";
                string outdir = "";
                string inc = "";

                for (int i = 0; i < args.Length; i++)
                {
                    if (args[i] == "-outdir")
                    {
                        outdir = args[++i];
                    }
                    else if (args[i] == "-inc")
                    {
                        inc = args[++i];
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(inFilename))
                        {
                            inFilename = args[i];
                        }
                        else
                        {
                            Usage();
                            return;
                        }
                    }
                }

                if (string.IsNullOrEmpty(inFilename))
                {
                    Usage();
                    return;
                }

                //Load the ruleset.
                AtlasRuleset ruleset = AtlasRuleset.Load(inFilename);
                if (ruleset == null)
                {
                    WriteLine("Error: Failed to load or process {0}", inFilename);
                    return;
                }
                else
                {
                    WriteLine("Successfully loaded ruleset from {0}...", inFilename);
                }

                //Create rlGamespyAtlasRuleset code.
                string className = string.Format("atlas_{0}_v{1}", ruleset.GameName, ruleset.Version);
                
                string headerFilename = CreateFilename(outdir, className + ".h");
                if (!AtlasCpp.GenerateHeader(ruleset, className, headerFilename, inc, Credits))
                {
                    WriteLine("Error: Failed to convert Atlas ruleset to C++ header {0}", headerFilename);
                    return;
                }
                else
                {
                    WriteLine("Successfully generated C++ header {0} from Atlas ruleset...", headerFilename);
                }

                string cppFilename = CreateFilename(outdir, className + ".cpp");
                if (!AtlasCpp.GenerateSource(ruleset, className, cppFilename, inc, Credits))
                {
                    WriteLine("Error: Failed to convert Atlas ruleset to C++ source {0}", cppFilename);
                    return;
                }
                else
                {
                    WriteLine("Successfully generated C++ source {0} from Atlas ruleset...", cppFilename);
                }

                WriteLine("atlas2c succeeeded.");
            }
            catch (Exception ex)
            {
                WriteLine("Exception: " + ex.ToString());
            }
        }
    }
}
