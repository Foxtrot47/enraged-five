﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;

using Rockstar.Gamespy.Atlas;

namespace profile2atlas
{
    class Program
    {
        //Version history:
        //  1.0 Base version
        //  1.1 Added -noprefix option.  Built with Atlas.lib that auto-includes rs_GamerHandle and rs_GamerName in output.
        //  1.2 Removed -noprefix.  If needed later, we'll add it.

        const uint MajorVersion = 1;
        const uint MinorVersion = 2;
        static string ProgramName { get { return string.Format("profile2atlas v{0}.{1}", MajorVersion, MinorVersion); } }
        static string Credits { get { return "Created by " + ProgramName + " " + DateTime.Now; } }

        static void WriteLine(string format, params object[] args)
        {
            Trace.WriteLine(string.Format(format, args));
        }

        static void Usage()
        {
            WriteLine(ProgramName);
            WriteLine("Usage: profile2atlas [profilestats xml filename] -gsgamename [gameName] -version [version] {{-out [filename]}}");
        }

        static void Main(string[] args)
        {
            Trace.Listeners.Add(new ConsoleTraceListener());

            try
            {
                string inFilename = "";
                string gsgamename = "";
                int version = 0;
                string outFilename = "";

                for (int i = 0; i < args.Length; i++)
                {
                    if (args[i] == "-gsgamename")
                    {
                        gsgamename = args[++i];
                    }
                    else if (args[i] == "-version")
                    {
                        version = int.Parse(args[++i]);
                    }
                    else if (args[i] == "-out")
                    {
                        outFilename = args[++i];
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(inFilename))
                        {
                            inFilename = args[i];
                        }
                        else
                        {
                            Usage();
                            return;
                        }
                    }
                }

                if (string.IsNullOrEmpty(inFilename)
                    || string.IsNullOrEmpty(gsgamename)
                    || (version < 1))
                {
                    Usage();
                    return;
                }

                //Load the profile stats.
                ProfileStats ps = ProfileStats.Load(inFilename);
                if (ps == null)
                {
                    WriteLine("Error: Failed to load or process {0}", inFilename);
                    return;
                }
                else
                {
                    WriteLine("Successfully loaded profile stats from {0}...", inFilename);
                }

                //Create an Atlas ruleset from the profile stats.
                AtlasRuleset ruleset = ps.GenerateRuleset(gsgamename, version);
                if (ruleset == null)
                {
                    WriteLine("Error: Failed to generate Atlas ruleset");
                    return;
                }
                else
                {
                    WriteLine("Successfully generated Atlas ruleset from profile stats...");
                }

                //Create Atlas XML for importing to backend.
                if (string.IsNullOrEmpty(outFilename))
                {
                    outFilename = string.Format("{0}_v{1}.atlas.xml", gsgamename, version);
                }

                if (!ruleset.Save(outFilename, Credits))
                {
                    WriteLine("Error: Failed to save Atlas ruleset to {0}", outFilename);
                    return;
                }
                else
                {
                    WriteLine("Succesfully wrote Atlas ruleset to {0}...", outFilename);
                }

                WriteLine("profile2atlas succeeeded.");
            }
            catch (Exception ex)
            {
                WriteLine("Exception: " + ex.ToString());
            }
        }
    }
}
