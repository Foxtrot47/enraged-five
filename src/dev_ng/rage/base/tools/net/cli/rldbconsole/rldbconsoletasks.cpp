// 
// rlDbConsoleTasks.cpp
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "rlDbConsoleTasks.h"
#include "data/base64.h"
#include "diag/seh.h"
#include "math/amath.h"
#include "parsercore/attribute.h"
#include "parsercore/element.h"
#include "parsercore/stream.h"
#include "parser/manager.h"
#include "parser/tree.h"
#include "parser/treenode.h"
#include "rline/rldatabaseclient.h"

using namespace rage;

////////////////////////////////////////////////////////////////////////////////
// rlDbConsoleTask::RecordInfo
////////////////////////////////////////////////////////////////////////////////
rlDbConsoleTask::RecordInfo::RecordInfo()
: m_RecordId(0)
, m_NumFields(0)
, m_Fields(0)
, m_FieldNamesBuf(0)
, m_FieldNames(0)
{
}

rlDbConsoleTask::RecordInfo::~RecordInfo()
{
    Clear();
}

void
rlDbConsoleTask::RecordInfo::Clear()
{
    if(m_Fields)
    {
        rlDelete(m_Fields, m_NumFields);
        m_Fields = 0;
    }

    if(m_FieldNamesBuf)
    {
        rlFree(m_FieldNamesBuf);
        m_FieldNamesBuf = 0;
    }

    if(m_FieldNames)
    {
        rlFree(m_FieldNames);
        m_FieldNames = 0;
    }

    m_NumFields = 0;
    m_RecordId = RLDB_INVALID_RECORDID;
}

void 
rlDbConsoleTask::RecordInfo::SetRecordId(const rlDbRecordId recordId)
{
    m_RecordId = recordId;
}

bool 
rlDbConsoleTask::RecordInfo::SetNumFields(const unsigned numFields)
{
    rlDbRecordId temp = m_RecordId;
    Clear();
    m_RecordId = temp;

    m_NumFields = numFields;

    m_Fields = RL_NEW_ARRAY("rlDbConsoleTask::RecordInfo", rlDbField, m_NumFields);
    if(!m_Fields) return false;

    m_FieldNamesBuf = (char*)RL_ALLOCATE("rlDbConsoleTask::RecordInfo", m_NumFields * RLDB_MAX_FIELD_NAME_CHARS);
    if(!m_FieldNamesBuf) return false;
    sysMemSet(m_FieldNamesBuf, 0, sizeof(m_FieldNamesBuf));

    m_FieldNames = (char**)RL_ALLOCATE("rlDbConsoleTask::RecordInfo", sizeof(char*) *m_NumFields);
    if(!m_FieldNames) return false;

    for(int i = 0; i < m_NumFields; i++)
    {
        m_FieldNames[i] = &m_FieldNamesBuf[i * RLDB_MAX_FIELD_NAME_CHARS];
    }

    return true;
}

void
rlDbConsoleTask::RecordInfo::SetFieldName(const unsigned index, const char* name)
{
    Assert((int)index < m_NumFields);
    safecpy(m_FieldNames[index], name, RLDB_MAX_FIELD_NAME_CHARS);
    m_Fields[index].SetName(m_FieldNames[index]);
}


////////////////////////////////////////////////////////////////////////////////
// rlDbConsoleTask
////////////////////////////////////////////////////////////////////////////////
rlDbConsoleTask::rlDbConsoleTask()
{
}

rlDbConsoleTask::~rlDbConsoleTask()
{
}

bool 
rlDbConsoleTask::ReadSchema(rlDbDynSchema* schema, parTreeNode* root)
{
    rtry
    {
        schema->Clear();

        //First, count how many fields there are.
        unsigned numFields = 0;
        for(parTreeNode::ChildNodeIterator node = root->BeginChildren(); 
            node != root->EndChildren(); 
            ++node) 
        {
            parElement& child = (*node)->GetElement();

            if(!strcmp("Field", child.GetName()))
            {
                ++numFields;
            }
        }

        rcheck(schema->Reset(numFields, rlDbDynSchema::COPY_NAME_STRING), 
               catchall, 
               rlError("Failed to Reset() for %d fields", numFields));

        //Now read the field data
        numFields = 0;
        for(parTreeNode::ChildNodeIterator node = root->BeginChildren(); 
            node != root->EndChildren(); 
            ++node) 
        {
            parElement& child = (*node)->GetElement();

            if(!strcmp("Field", child.GetName()))
            {
                char nameBuf[RLDB_MAX_FIELD_NAME_CHARS];
                rcheck(child.FindAttributeStringValue("name", 0, nameBuf, sizeof(nameBuf), true),
                    catchall,
                    rlError("Couldn't find name attribute in <Field>"));

                char buf[RLDB_MAX_VALUE_TYPE_STR_CHARS];
                rcheck(child.FindAttributeStringValue("type", 0, buf, sizeof(buf), true),
                    catchall,
                    rlError("Couldn't find type attribute in <Field>"));

                rlDbValueType vt;
                rcheck(RLDB_VALUETYPE_INVALID != (vt = rlDbValueTypeFromString(buf)),
                    catchall,
                    rlError("Couldn't parse value type in <Field>"));

                rcheck((schema->GetNumFields() > 0) || !strcmp(nameBuf, rlDbGetFieldName(RLDB_FIELD_RECORD_ID)),
                       catchall,
                       rlError("First field in a schema must be %s", rlDbGetFieldName(RLDB_FIELD_RECORD_ID)));

                schema->AddField(nameBuf, vt);
            }
        }

        return true;
    }
    rcatchall
    {
        return false;
    }
}

bool 
rlDbConsoleTask::WriteSchema(const rlDbSchemaBase& schema, parStreamOut* s)
{
    for(unsigned i = 0; i < schema.GetNumFields(); i++)
    {
        parElement fieldEl;
        fieldEl.SetName("Field", false);
        fieldEl.AddAttribute("name", schema.GetFieldName(i), false);
        fieldEl.AddAttribute("type", rlDbValueTypeToString(schema.GetFieldType(i)), false);
        s->WriteLeafElement(fieldEl);
    }

    return true;
}

bool
rlDbConsoleTask::ReadRecords(parTreeNode* root, 
                             const rlDbSchemaBase& schema,
                             RecordList* records)
{
    RecordInfo* r = 0;

    rtry
    {
        for(parTreeNode::ChildNodeIterator recordIter = root->BeginChildren(); 
            recordIter != root->EndChildren(); 
            ++recordIter) 
        {
            parTreeNode* recordNode = *recordIter;

            if(!strcmp("Record", recordNode->GetElement().GetName()))
            {
                r = rage_new RecordInfo();
                
                rcheck(r->SetNumFields(schema.GetNumFields()), catchall, rlError("Failed to set num fields on record"));

                for(parTreeNode::ChildNodeIterator fieldNode = recordNode->BeginChildren(); 
                    fieldNode != recordNode->EndChildren(); 
                    ++fieldNode) 
                {
                    if(!strcmp("Field", (*fieldNode)->GetElement().GetName()))
                    {
                        char nameBuf[RLDB_MAX_FIELD_NAME_CHARS];
                        rcheck((*fieldNode)->GetElement().FindAttributeStringValue("name", 0, nameBuf, sizeof(nameBuf), true),
                            catchall,
                            rlError("Couldn't find name attribute in <Field>"));

                        int index = schema.GetFieldIndex(nameBuf);
                        rcheck(index >= 0, catchall, rlError("Couldn't find schema field %s", nameBuf));

                        r->SetFieldName(index, nameBuf);

                        char valueBuf[4096];
                        rcheck((*fieldNode)->GetElement().FindAttributeStringValue("value", 0, valueBuf, sizeof(valueBuf), true),
                            catchall,
                            rlError("Couldn't find value attribute in <Field> %d", index));

                        rcheck(r->GetFieldValue(index).FromString(valueBuf, schema.GetFieldType(index)),
                            catchall,
                            rlError("Failed to set value for field %d from string %s", index, valueBuf));


                        if(!strcmp(r->GetFieldName(index), rlDbGetFieldName(RLDB_FIELD_RECORD_ID)))
                        {
                            r->SetRecordId(r->GetFieldValue(index).GetInt32());
                        }
                    }
                }

                records->push_back(r);
            }
        }

        return true;
    }
    rcatchall
    {
        if(r) delete r;
        return false;
    }
}

bool 
rlDbConsoleTask::LoadTableXml(const char* filename,
                              char* tableName,
                              rlDbDynSchema* schema, 
                              RecordList* records)
{
    rtry
    {
        bool prevSetting = PARSER.Settings().SetFlag(parSettings::PROCESS_SPECIAL_ATTRS, false);

        parTree* tree = PARSER.LoadTree(filename, "xml");

        PARSER.Settings().SetFlag(parSettings::PROCESS_SPECIAL_ATTRS, prevSetting);

        rcheck(tree, catchall, rlError("Failed to load XML file %s", filename));

        parTreeNode* root = tree->GetRoot();

        if(tableName)
        {
            rcheck(!strcmp("Table", root->GetElement().GetName()),
                catchall,
                rlError("Root element is not <Table>"));

            rcheck(root->GetElement().FindAttributeStringValue("name", 0, tableName, RLDB_MAX_TABLE_NAME_CHARS, true),
                catchall,
                rlError("Couldn't find name attribute in <Table>"));
        }

        parTreeNode* node = 0;

        if(schema)
        {
            rcheck(0 != (node = root->FindChildWithName("Schema")),
                catchall,
                rlError("Failed to find <Schema>"));

            rcheck(ReadSchema(schema, node),
                catchall,
                rlError("Failed to read <Schema> node"));
        }

        if(schema && records)
        {
            rcheck(0 != (node = root->FindChildWithName("Records")),
                catchall,
                rlError("Failed to find <Records>"));

            rcheck(ReadRecords(node, *schema, records),
                catchall,
                rlError("Failed to read <Records> node"));
        }

        delete tree;

        return true;
    }
    rcatchall
    {
        return false;
    }
}

bool
rlDbConsoleTask::SaveTableXml(const char* filename,
                              const char* tableName,
                              const rlDbSchemaBase& schema,
                              const rlDbReadResults& dbResults)
{
    parStreamOut* s = 0;

    rtry
    {
        s = PARSER.OpenOutputStream(filename, "xml");
        rcheck(s, catchall, rlError("Failed to open file %s", filename));

        //Write the <Table> element
        parElement tableEl;
        tableEl.SetName("Table", false);
        tableEl.AddAttribute("name", tableName, false);
        s->WriteBeginElement(tableEl);

        //Write the <Schema> element
        parElement schemaEl;
        schemaEl.SetName("Schema", false);
        s->WriteBeginElement(schemaEl);
        rcheck(WriteSchema(schema, s), catchall, rlError("Failed to write scheam to parser stream"));
        s->WriteEndElement(schemaEl);

        //Write the <Records> element
        parElement recordsEl;
        recordsEl.SetName("Records", false);
        s->WriteBeginElement(recordsEl);

        //Write all the <Record> elements
        for(unsigned i = 0; i < dbResults.GetNumRecords(); i++)
        {
            parElement recordEl;
            recordEl.SetName("Record", false);
            s->WriteBeginElement(recordEl);

            for(unsigned j = 0; j < dbResults.GetSchema()->GetNumFields(); j++)
            {
                parElement fieldEl;
                fieldEl.SetName("Field", false);
                fieldEl.AddAttribute("name", dbResults.GetSchema()->GetFieldName(j), false);

                if(RLDB_VALUETYPE_ASCII_STRING == dbResults.GetFieldType(j))
                {
                    fieldEl.AddAttribute("value", dbResults.GetAsciiString(i, j), false);
                }
                else if(RLDB_VALUETYPE_BINARY_DATA == dbResults.GetFieldType(j))
                {
                    const rlDbBinaryData& bd = dbResults.GetBinaryData(i, j);
                    
                    unsigned bufSize = datBase64::GetMaxEncodedSize(bd.GetData(), bd.GetSize()) + 1; //+1 for null
                    char* buf = (char*)RL_ALLOCATE("rlDbConsoleTask::SaveTableXml", bufSize);
                    rcheck(buf, catchall, rlError("Failed to allocate string buffer"));

                    fieldEl.AddAttribute("value", dbResults.ToString(i, j, buf, bufSize), false);

                    rlFree(buf);
                }
                else
                {
                    char buf[64];
                    fieldEl.AddAttribute("value", dbResults.ToString(i, j, buf, sizeof(buf)), false);
                }

                s->WriteLeafElement(fieldEl);
            }

            s->WriteEndElement(recordEl);
        }

        s->WriteEndElement(recordsEl);

        s->WriteEndElement(tableEl);

        s->Close();
        delete s;

        return true;
    }
    rcatchall
    {
        if(s)
        {
            s->Close();
            delete s;
        }
        return false;
    }
}

////////////////////////////////////////////////////////////////////////////////
// rlDbConsoleUploadFileTask
////////////////////////////////////////////////////////////////////////////////
rlDbConsoleUploadFileTask::rlDbConsoleUploadFileTask()
: m_FileId(0)
, m_FileSize(0)
{
}

rlDbConsoleUploadFileTask::~rlDbConsoleUploadFileTask()
{
}

bool 
rlDbConsoleUploadFileTask::Configure(
    rlDbClient* /*ctx*/,
    const char* filename,
    const char* outFilename)
{
    safecpy(m_Filename, filename);
    
    m_OutFilename[0] = '\0';
    if(outFilename)
    {
        safecpy(m_OutFilename, outFilename);
    }

    return true;
}

void 
rlDbConsoleUploadFileTask::Start()
{
    rlDbConsoleTask::Start();

    FILE* f = fopen(m_Filename, "rb");

    rtry
    {
        rcheck(f, catchall, rlError("Failed to open file %s", m_Filename));

        rcheck(0 < (m_FileSize = fread(m_FileBuf, 1, sizeof(m_FileBuf), f)),
            catchall,
            rlError("Failed to read anything from %s", m_Filename));

        fclose(f);

        rcheck(m_Ctx->UploadFile(m_FileBuf, m_FileSize, 0, &m_FileId, &m_MyStatus), 
               catchall, 
               rlError("UploadFile failed"));
    }
    rcatchall
    {
        m_FileSize = 0;
        fclose(f);
        Finish(FINISH_FAILED);
    }
}

void 
rlDbConsoleUploadFileTask::Update(const unsigned timeStep)
{
    rlDbConsoleTask::Update(timeStep);

    if(!m_MyStatus.Pending())
    {
        Finish(m_MyStatus.Succeeded() ? FINISH_SUCCEEDED : FINISH_FAILED);
    }
}

void 
rlDbConsoleUploadFileTask::Finish(const FinishType finishType, const int resultCode)
{
    rlDbConsoleTask::Finish(finishType, resultCode);

    if(finishType == FINISH_SUCCEEDED)
    {
        Printf("Uploaded %s (%u bytes) file ID = %d.\n", m_Filename, m_FileSize, m_FileId);

        if(strlen(m_OutFilename))
        {
            FILE* f = fopen(m_OutFilename, "a+t");
            if(!f)
            {
                rlError("Upload succeeded, but failed to open outfile %s", m_OutFilename);
            }
            else
            {
                char buf[512];
                formatf(buf, sizeof(buf), "%d %s\n", m_FileId, m_Filename);

                fwrite(buf, 1, strlen(buf), f);

                fclose(f);

                Printf("Appended file ID and filename to %s\n", m_OutFilename);
            }
        }
    }
    else
    {
        rlDebug1("Failed to upload file");
    }
}

////////////////////////////////////////////////////////////////////////////////
// rlDbConsoleDownloadFileTask
////////////////////////////////////////////////////////////////////////////////
rlDbConsoleDownloadFileTask::rlDbConsoleDownloadFileTask()
: m_FileId(0)
, m_FileSize(0)
{
}

rlDbConsoleDownloadFileTask::~rlDbConsoleDownloadFileTask()
{
}

bool 
rlDbConsoleDownloadFileTask::Configure(
    rlDbClient* /*ctx*/,
    const rlDbFileId fileId,
    const char* filename)
{
    m_FileId = fileId;
    safecpy(m_Filename, filename);

    return true;
}

void 
rlDbConsoleDownloadFileTask::Start()
{
    rlDbConsoleTask::Start();

    if(!m_Ctx->DownloadFile(m_FileId, m_FileBuf, sizeof(m_FileBuf), 0, &m_FileSize, &m_MyStatus))
    {
        Finish(FINISH_FAILED);
    }
}

void 
rlDbConsoleDownloadFileTask::Update(const unsigned timeStep)
{
    rlDbConsoleTask::Update(timeStep);

    if(!m_MyStatus.Pending())
    {
        if(m_MyStatus.Succeeded())
        {
            FILE* f = fopen(m_Filename, "wb");

            rtry
            {
                rcheck(f, catchall, rlError("Failed to open file %s", m_Filename));

                rcheck(m_FileSize == fwrite(m_FileBuf, 1, m_FileSize, f),
                    catchall,
                    rlError("Failed to write all data to %s", m_Filename));

                fclose(f);

                Finish(FINISH_SUCCEEDED);
            }
            rcatchall
            {
                fclose(f);
                Finish(FINISH_FAILED);
            }
        }
        else
        {
            Finish(FINISH_FAILED);
        }
    }
}

void 
rlDbConsoleDownloadFileTask::Finish(const FinishType finishType, const int resultCode)
{
    rlDbConsoleTask::Finish(finishType, resultCode);

    if(finishType == FINISH_SUCCEEDED)
    {
        Printf("Downloaded file ID %d to %s (%u bytes).\n", m_FileId, m_Filename, m_FileSize);
    }
    else
    {
        rlDebug1("Failed to download file");
    }
}

////////////////////////////////////////////////////////////////////////////////
// rlDbConsoleDeleteRecordTask
////////////////////////////////////////////////////////////////////////////////
rlDbConsoleDeleteRecordTask::rlDbConsoleDeleteRecordTask()
{
}

rlDbConsoleDeleteRecordTask::~rlDbConsoleDeleteRecordTask()
{
}

bool 
rlDbConsoleDeleteRecordTask::Configure(rlDbClient* /*ctx*/,
                              const char* tableName,
                              const rlDbRecordId recordId)
{
    safecpy(m_TableName, tableName);
    m_RecordId = recordId;

    return true;
}

void 
rlDbConsoleDeleteRecordTask::Start()
{
    rlDbConsoleTask::Start();

    if(!m_Ctx->DeleteRecord(m_TableName, m_RecordId, &m_MyStatus))
    {
        Finish(FINISH_FAILED);
    }
}

void 
rlDbConsoleDeleteRecordTask::Update(const unsigned timeStep)
{
    rlDbConsoleTask::Update(timeStep);

    if(!m_MyStatus.Pending())
    {
        Finish(m_MyStatus.Succeeded() ? FINISH_SUCCEEDED : FINISH_FAILED);
    }
}

void 
rlDbConsoleDeleteRecordTask::Finish(const FinishType finishType, const int resultCode)
{
    rlDbConsoleTask::Finish(finishType, resultCode);

    if(finishType == FINISH_SUCCEEDED)
    {
        Printf("Deleted record %d from %s.\n", m_RecordId, m_TableName);
    }
    else
    {
        rlDebug1("Failed to delete record %d from %s", m_RecordId, m_TableName);
    }
}

////////////////////////////////////////////////////////////////////////////////
// rlDbConsoleImportTableTask
////////////////////////////////////////////////////////////////////////////////
rlDbConsoleImportTableTask::rlDbConsoleImportTableTask()
: m_State(STATE_IDLE)
, m_NumCreates(0)
, m_NumUpdates(0)
{
}

rlDbConsoleImportTableTask::~rlDbConsoleImportTableTask()
{
    while(!m_Records.empty())
    {
        RecordInfo* r = *m_Records.begin();
        m_Records.erase(m_Records.begin());
        delete r;
    }
}

bool 
rlDbConsoleImportTableTask::Configure(rlDbClient* /*ctx*/,
                             const char* filename)
{
    return LoadTableXml(filename, m_TableName, &m_Schema, &m_Records);
}

void 
rlDbConsoleImportTableTask::Start()
{
    rlDbConsoleTask::Start();

    m_State = STATE_START_REQUEST;
}

void 
rlDbConsoleImportTableTask::Update(const unsigned timeStep)
{
    rlDbConsoleTask::Update(timeStep);

    switch(m_State)
    {
    case STATE_START_REQUEST:
        if(!m_Records.empty())
        {
            RecordInfo* r = *m_Records.begin();

            if(r->GetRecordId() != RLDB_INVALID_RECORDID)
            {
                rlDebug2("Updating record %d", r->GetRecordId());

                //NOTE: The first field of each records is always its recordid.
                //      It is illegal to send that field.
                if(!m_Ctx->UpdateRecord(m_TableName, r->GetRecordId(), &r->GetFields()[1], r->GetNumFields() - 1, &m_MyStatus))
                {
                    rlError("Failed to start UpdateRecord");
                    Finish(FINISH_FAILED);
                    return;
                }
            }
            else
            {
                rlDebug2("Creating record");
                
                //NOTE: The first field of each records is always its recordid.
                //      It is illegal to send that field.
                if(!m_Ctx->CreateRecord(m_TableName, &r->GetFields()[1], r->GetNumFields() - 1, &m_RecordId, &m_MyStatus))
                {
                    rlError("Failed to start CreateRecord");
                    Finish(FINISH_FAILED);
                    return;
                }
            }

            m_State = STATE_REQUEST_PENDING;
        }
        else
        {
            Finish(FINISH_SUCCEEDED);
        }
        break;

    case STATE_REQUEST_PENDING:
        if(!m_MyStatus.Pending())
        {
            if(m_MyStatus.Succeeded())
            {
                RecordInfo* r = *m_Records.begin();
                m_Records.erase(m_Records.begin());

                r->GetRecordId() ? ++m_NumUpdates : ++m_NumCreates;

                delete r;

                m_State = STATE_START_REQUEST;
            }
            else
            {
                Finish(FINISH_FAILED);
            }
        }
    }
}

void 
rlDbConsoleImportTableTask::Finish(const FinishType finishType, const int resultCode)
{
    rlDbConsoleTask::Finish(finishType, resultCode);

    if(finishType == FINISH_SUCCEEDED)
    {
        Printf("Created %d and updated %d records in table %s.\n", m_NumCreates, m_NumUpdates, m_TableName);
    }
    else
    {
        rlDebug1("Failed to create or update all records in table %s", m_TableName);
    }
}

////////////////////////////////////////////////////////////////////////////////
// rlDbConsoleExportTableTask
////////////////////////////////////////////////////////////////////////////////
rlDbConsoleExportTableTask::rlDbConsoleExportTableTask()
: m_Values(0)
, m_NumValues(0)
, m_Schema(0)
{
}

rlDbConsoleExportTableTask::~rlDbConsoleExportTableTask()
{
    if(m_Values)
    {
        rlDelete(m_Values, m_NumValues);
        m_Values = 0;
        m_NumValues = 0;
    }
}

bool 
rlDbConsoleExportTableTask::Configure(rlDbClient* /*ctx*/,
                                      const char* filename,
                                      const char* outFilename,
                                      const unsigned startingRank,
                                      const unsigned maxRecords)
{
    if(!LoadTableXml(filename, m_TableName, &m_LoadedSchema, NULL))
    {
        return false;
    }

    m_Schema = &m_LoadedSchema;

    return Configure(m_Ctx, m_TableName, &m_LoadedSchema, outFilename, startingRank, maxRecords);
}

bool 
rlDbConsoleExportTableTask::Configure(rlDbClient* /*ctx*/,
                                      const char* tableName,
                                      const rlDbSchemaBase* schema,
                                      const char* outFilename,
                                      const unsigned startingRank,
                                      const unsigned maxRecords)
{
    safecpy(m_TableName, tableName);
    safecpy(m_ExportFilename, outFilename);
    m_StartingRank = startingRank;

    m_Schema = schema;

    m_NumValues = m_Schema->GetNumFields() * maxRecords;
    m_Values = RL_NEW_ARRAY(rlDbConsoleExportTableTask, rlDbValue, m_NumValues);
    if(!m_Values) return false;

    m_DbResults.Reset(m_Schema, maxRecords, m_Values);

    return true;
}

void 
rlDbConsoleExportTableTask::Start()
{
    rlDbConsoleTask::Start();

    if(!m_Ctx->ReadRecords(m_TableName, 
                           "", 
                           "", 
                           m_StartingRank,
                           "", 
                           0, 
                           false, 
                           m_DbResults.GetMaxRecords(), 
                           &m_DbResults, 
                           &m_MyStatus))
    {
        Finish(FINISH_FAILED);
    }
}

void 
rlDbConsoleExportTableTask::Update(const unsigned timeStep)
{
    rlDbConsoleTask::Update(timeStep);

    if(!m_MyStatus.Pending())
    {
        if(m_MyStatus.Succeeded())
        {
            Finish(SaveTableXml(m_ExportFilename, m_TableName, *m_Schema, m_DbResults) ? FINISH_SUCCEEDED : FINISH_FAILED);
        }
        else
        {
            Finish(FINISH_FAILED);
        }
    }
}

void 
rlDbConsoleExportTableTask::Finish(const FinishType finishType, const int resultCode)
{
    rlDbConsoleTask::Finish(finishType, resultCode);

    if(finishType == FINISH_SUCCEEDED)
    {
        Printf("Read %u records from %s.\n", m_DbResults.GetNumRecords(), m_TableName);
    }
    else
    {
        rlDebug1("Failed to read records from %s", m_TableName);
    }
}

////////////////////////////////////////////////////////////////////////////////
// rlDbConsoleSyncTableTask
////////////////////////////////////////////////////////////////////////////////
rlDbConsoleSyncTableTask::rlDbConsoleSyncTableTask()
: m_State(STATE_IDLE)
, m_NumRecords(0)
, m_NumDeletes(0)
, m_NumCreates(0)
, m_NumUpdates(0)
, m_Values(0)
, m_NumValues(0)
{
}

rlDbConsoleSyncTableTask::~rlDbConsoleSyncTableTask()
{
    while(!m_Records.empty())
    {
        RecordInfo* r = *m_Records.begin();
        m_Records.erase(m_Records.begin());
        delete r;
    }

    if(m_Values)
    {
        rlDelete(m_Values, m_NumValues);
        m_Values = 0;
        m_NumValues = 0;
    }
}

bool 
rlDbConsoleSyncTableTask::Configure(rlDbClient* /*ctx*/,
                                    const char* filename,
                                    const char* outFilename)
{
    if(!outFilename || !strlen(outFilename))
    {
        return false;
    }

    safecpy(m_ExportFilename, outFilename);

    return LoadTableXml(filename, m_TableName, &m_Schema, &m_Records);
}

void 
rlDbConsoleSyncTableTask::Start()
{
    rlDbConsoleTask::Start();

    m_State = STATE_START_COUNT_REQUEST;
}

void 
rlDbConsoleSyncTableTask::Update(const unsigned timeStep)
{
    rlDbConsoleTask::Update(timeStep);

    switch(m_State)
    {
    case STATE_START_COUNT_REQUEST:
        rlDebug2("Counting records in %s", m_TableName);
        if(m_Ctx->GetNumRecords(m_TableName, "", &m_NumRecords, &m_MyStatus))
        {
            m_State = STATE_COUNT_REQUEST_PENDING;
        }
        else
        {
            rlError("GetNumRecords() failed");
            Finish(FINISH_FAILED);
        }
        break;
    case STATE_COUNT_REQUEST_PENDING:
        if(!m_MyStatus.Pending())
        {
            if(m_MyStatus.Succeeded())
            {
                rlDebug2("%d records currently in %s", m_NumRecords, m_TableName);

                if(m_NumRecords > 0)
                {
                    rlDebug2("Reading existing record IDs to determine which to delete...");

                    m_CheckRecordSchema.Clear();
                    m_CheckRecordSchema.AddField(rlDbGetFieldName(RLDB_FIELD_RECORD_ID), RLDB_VALUETYPE_INT32);

                    m_NumValues = m_CheckRecordSchema.GetNumFields() * m_NumRecords;

                    m_Values = RL_NEW_ARRAY(rlDbConsoleSyncTableTask, rlDbValue, m_NumValues);
                    if(!m_Values)
                    {
                        rlError("Failed to allocate DB values array");
                        Finish(FINISH_FAILED);
                    }
                    else 
                    {
                        m_DbResults.Reset(&m_CheckRecordSchema, m_NumRecords, m_Values);

                        if(m_Ctx->ReadRecords(m_TableName,
                                                   "",
                                                   "",
                                                   1,
                                                   "",
                                                   0,
                                                   false,
                                                   m_DbResults.GetMaxRecords(),
                                                   &m_DbResults,
                                                   &m_MyStatus))
                        {
                            m_State = STATE_READ_REQUEST_PENDING;
                        }
                        else
                        {
                            rlError("ReadRecords() failed");
                            Finish(FINISH_FAILED);
                        }
                    }
                }
                else
                {
                    m_State = STATE_START_CREATEUPDATE_REQUEST;
                }
            }
            else
            {
                Finish(FINISH_FAILED);
            }
        }
        break;
    case STATE_READ_REQUEST_PENDING:
        if(!m_MyStatus.Pending())
        {
            if(m_MyStatus.Succeeded())
            {
                m_CurRecord = 0;
                while(m_CurRecord < m_DbResults.GetNumRecords())
                {
                    m_RecordId = m_DbResults.GetInt32(m_CurRecord, 0);
                    if(!GetRecordInfo(m_RecordId))
                    {
                        m_State = STATE_START_DELETE_REQUEST;
                        return;
                    }
                    ++m_CurRecord;
                }

                m_State = STATE_START_CREATEUPDATE_REQUEST;
            }
            else
            {
                rlError("ReadRecords request failed");
                Finish(FINISH_FAILED);
            }
        }
        break;
    case STATE_START_DELETE_REQUEST:
        rlDebug2("Deleting record %d", m_RecordId);

        if(m_Ctx->DeleteRecord(m_TableName, m_RecordId, &m_MyStatus))
        {
            m_State = STATE_DELETE_REQUEST_PENDING;
        }
        else
        {
            rlError("DeleteRecord() failed");
            Finish(FINISH_FAILED);
        }
        break;
    case STATE_DELETE_REQUEST_PENDING:
        if(!m_MyStatus.Pending())
        {
            if(m_MyStatus.Succeeded())
            {
                ++m_NumDeletes;
                ++m_CurRecord;
                while(m_CurRecord < m_DbResults.GetNumRecords())
                {
                    m_RecordId = m_DbResults.GetInt32(m_CurRecord, 0);
                    if(!GetRecordInfo(m_RecordId))
                    {
                        m_State = STATE_START_DELETE_REQUEST;
                        return;
                    }
                    ++m_CurRecord;
                }

                m_State = STATE_START_CREATEUPDATE_REQUEST;
            }
            else
            {
                rlError("DeleteRecord request failed");
                Finish(FINISH_FAILED);
            }
        }
        break;
    case STATE_START_CREATEUPDATE_REQUEST:
        if(!m_Records.empty())
        {
            RecordInfo* r = *m_Records.begin();

            if(r->GetRecordId() != RLDB_INVALID_RECORDID)
            {
                //See if the record already exists.  If it doesn't, create it.
                //If we didn't create it, then it's possible for the record
                //to be lost if the user is overwriting their original xml.
                bool found = false;

                for(unsigned i = 0;!found && (i < m_DbResults.GetNumRecords()); i++)
                {
                    rlDbRecordId id = m_DbResults.GetInt32(i, 0);
                    found = (id == r->GetRecordId());
                }

                if(found)
                {
                    rlDebug2("Updating record %d", r->GetRecordId());

                    //NOTE: The first field of each records is always its recordid.
                    //      It is illegal to send that field.
                    if(!m_Ctx->UpdateRecord(m_TableName, r->GetRecordId(), &r->GetFields()[1], r->GetNumFields() - 1, &m_MyStatus))
                    {
                        rlError("Failed to start UpdateRecord");
                        Finish(FINISH_FAILED);
                        return;
                    }
                }
                else
                {
                    //Isn't already in the table, so force a create.
                    rlDebug2("Record %d isn't already in table, so re-creating", r->GetRecordId());
                    r->SetRecordId(RLDB_INVALID_RECORDID);
                }
            }
            
            if(r->GetRecordId() == RLDB_INVALID_RECORDID)
            {
                rlDebug2("Creating record");

                //NOTE: The first field of each records is always its recordid.
                //      When creating, it is illegal to send that field.
                if(!m_Ctx->CreateRecord(m_TableName, &r->GetFields()[1], r->GetNumFields() - 1, &m_RecordId, &m_MyStatus))
                {
                    rlError("Failed to start CreateRecord");
                    Finish(FINISH_FAILED);
                    return;
                }
            }

            m_State = STATE_CREATEUPDATE_REQUEST_PENDING;
        }
        else
        {
            m_State = STATE_START_EXPORT;
        }
        break;
    case STATE_CREATEUPDATE_REQUEST_PENDING:
        if(!m_MyStatus.Pending())
        {
            if(m_MyStatus.Succeeded())
            {
                RecordInfo* r = *m_Records.begin();
                m_Records.erase(m_Records.begin());

                r->GetRecordId() ? ++m_NumUpdates : ++m_NumCreates;

                delete r;

                m_State = STATE_START_CREATEUPDATE_REQUEST;
            }
            else
            {
                Finish(FINISH_FAILED);
            }
        }
        break;
    case STATE_START_EXPORT:
        if(rlTaskBase::Configure(&m_ExportTask,
                                m_Ctx, 
                                m_TableName, 
                                &m_Schema, 
                                m_ExportFilename, 
                                1, 
                                Max(m_NumRecords + m_NumCreates, (unsigned)1), 
                                &m_MyStatus))
        {
            m_ExportTask.Start();
            m_State = STATE_EXPORT_PENDING;
        }
        else
        {
            rlError("Failed to configure export task");
            Finish(FINISH_FAILED);
        }
        break;
    case STATE_EXPORT_PENDING:
        m_ExportTask.Update(timeStep);

        if(!m_MyStatus.Pending())
        {
            if(m_MyStatus.Succeeded())
            {
                Finish(FINISH_SUCCEEDED);
            }
            else
            {
                rlError("Export failed");
                Finish(FINISH_FAILED);
            }
        }
        break;
    }
}

void 
rlDbConsoleSyncTableTask::Finish(const FinishType finishType, const int resultCode)
{
    rlDbConsoleTask::Finish(finishType, resultCode);

    if(finishType == FINISH_SUCCEEDED)
    {
        Printf("%d deletes, %d creates, and %d updates in table %s.\n", 
               m_NumDeletes, m_NumCreates, m_NumUpdates, m_TableName);
    }
    else
    {
        rlDebug1("Failed to sync table %s", m_TableName);
    }
}

const rlDbConsoleSyncTableTask::RecordInfo*
rlDbConsoleSyncTableTask::GetRecordInfo(const rlDbRecordId recordId) const
{
    for(RecordList::const_iterator iter = m_Records.begin();
        iter != m_Records.end();
        ++iter)
    {
        const RecordInfo* r = (*iter);

        if(r->GetRecordId() == recordId)
        {
            return r;
        }
    }

    return 0;
}

