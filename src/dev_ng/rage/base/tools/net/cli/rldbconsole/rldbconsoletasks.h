// 
// rldbconsoletasks.h
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLDBCONSOLETASKS_H
#define RLDBCONSOLETASKS_H

#include "net/status.h"
#include "rline/rltask.h"
#include "rline/rldatabase.h"
#include "atl/inlist.h"

#include "parsercore/attribute.h"
#include "parsercore/element.h"
#include "parsercore/stream.h"
#include "parser/manager.h"
#include "parser/tree.h"
#include "parser/treenode.h"

namespace rage
{

class rlDbClient;

class rlDbConsoleTask : public rlTask<class rlDbClient>
{
public:
    RL_TASK_DECL(rlDbConsoleTask);

    rlDbConsoleTask();
    virtual ~rlDbConsoleTask();

protected:
    netStatus m_MyStatus;

    struct RecordInfo
    {
    public:
        RecordInfo();
        ~RecordInfo();

        void Clear();

        void SetRecordId(const rlDbRecordId recordId);
        bool SetNumFields(const unsigned numFields);
        void SetFieldName(const unsigned index, const char* name);

        rlDbRecordId GetRecordId() const { return m_RecordId; }
        unsigned GetNumFields() const { return m_NumFields; }
        const rlDbField* GetFields() const { return m_Fields; }
        const char** GetFieldNames() const { return (const char**)m_FieldNames; }
        const char* GetFieldName(const unsigned index) const { return m_FieldNames[index]; }
        rlDbValue& GetFieldValue(const unsigned index) { return m_Fields[index].GetValue(); }

    private:
        int m_RecordId;
        int m_NumFields;
        rlDbField* m_Fields;
        char* m_FieldNamesBuf;
        char** m_FieldNames;

    public:
        inlist_node<RecordInfo> m_ListLink;
    };
    typedef inlist<RecordInfo, &RecordInfo::m_ListLink> RecordList;

    static bool LoadTableXml(const char* filename,
                             char* tableName,
                             rlDbDynSchema* schema, 
                             RecordList* records);

    static bool SaveTableXml(const char* filename,
                             const char* tableName,
                             const rlDbSchemaBase& schema, 
                             const rlDbReadResults& dbResults);

    static bool ReadRecords(parTreeNode* root, 
                            const rlDbSchemaBase& schema,
                            RecordList* records);

    static bool ReadSchema(rlDbDynSchema* schema, 
                           parTreeNode* root);
    static bool WriteSchema(const rlDbSchemaBase& schema, 
                            parStreamOut* strm);
};

//PURPOSE
//  Uploads a file to the backend.
class rlDbConsoleUploadFileTask : public rlDbConsoleTask
{
public:
    RL_TASK_DECL(rlDbConsoleUploadFileTask);

    rlDbConsoleUploadFileTask();
    virtual ~rlDbConsoleUploadFileTask();

    bool Configure(rlDbClient* ctx,
                   const char* filename,
                   const char* outFilename);

    virtual void Start();
    virtual void Update(const unsigned timeStep);

protected:
    void Finish(const FinishType finishType, const int resultCode = 0);

private:
    char m_Filename[256];    
    char m_OutFilename[256];    
    rlDbFileId m_FileId;
    
    enum { MAX_FILE_SIZE = 1024 * 1024 * 10 };
    u8 m_FileBuf[MAX_FILE_SIZE];
    unsigned m_FileSize;
};

//PURPOSE
//  Downloads a file, saving it to specified filename.
class rlDbConsoleDownloadFileTask : public rlDbConsoleTask
{
public:
    RL_TASK_DECL(rlDbConsoleDownloadFileTask);

    rlDbConsoleDownloadFileTask();
    virtual ~rlDbConsoleDownloadFileTask();

    bool Configure(rlDbClient* ctx,
        const rlDbFileId fileId,
        const char* filename);

    virtual void Start();
    virtual void Update(const unsigned timeStep);

protected:
    void Finish(const FinishType finishType, const int resultCode = 0);

private:
    char m_Filename[256];    
    rlDbFileId m_FileId;

    enum { MAX_FILE_SIZE = 1024 * 1024 * 10 };
    u8 m_FileBuf[MAX_FILE_SIZE];
    unsigned m_FileSize;
};

//PURPOSE
//  Delete a record from a table.
class rlDbConsoleDeleteRecordTask : public rlDbConsoleTask
{
public:
    RL_TASK_DECL(rlDbConsoleDeleteRecordTask);

    rlDbConsoleDeleteRecordTask();
    virtual ~rlDbConsoleDeleteRecordTask();

    bool Configure(rlDbClient* ctx,
        const char* tableName,
        const rlDbRecordId recordId);

    virtual void Start();
    virtual void Update(const unsigned timeStep);

protected:
    void Finish(const FinishType finishType, const int resultCode = 0);

private:
    char m_TableName[RLDB_MAX_TABLE_NAME_CHARS];
    rlDbRecordId m_RecordId;
};

//PURPOSE
//  Read a file containing the table schema and a list of records to either
//  create or update.  If a record has a recordid field it is an update,
//  otherwise a new record will be created.
class rlDbConsoleImportTableTask : public rlDbConsoleTask
{
public:
    RL_TASK_DECL(rlDbConsoleImportTableTask);

    rlDbConsoleImportTableTask();
    virtual ~rlDbConsoleImportTableTask();

    bool Configure(rlDbClient* ctx,
                   const char* filename);

    virtual void Start();
    virtual void Update(const unsigned timeStep);

protected:
    void Finish(const FinishType finishType, const int resultCode = 0);

private:
    char m_TableName[RLDB_MAX_TABLE_NAME_CHARS];   
    rlDbDynSchema m_Schema;
    RecordList m_Records;

    enum
    {
        STATE_IDLE = 0,
        STATE_START_REQUEST,
        STATE_REQUEST_PENDING
    };
    int m_State;

    rlDbRecordId m_RecordId;
    int m_NumCreates;
    int m_NumUpdates;
};

//PURPOSE
//  Exports records from a table, getting only fields specified in the
//  schema file.  The exported file will be in the same format that 
//  the import task takes, to make importing changes easier.
class rlDbConsoleExportTableTask : public rlDbConsoleTask
{
public:
    RL_TASK_DECL(rlDbConsoleExportTableTask);

    rlDbConsoleExportTableTask();
    virtual ~rlDbConsoleExportTableTask();

    bool Configure(rlDbClient* ctx,
        const char* filename,
        const char* outFilename,
        const unsigned startingRank,
        const unsigned maxRecords);

    bool Configure(rlDbClient* ctx,
        const char* tableName,
        const rlDbSchemaBase* schema,
        const char* outFilename,
        const unsigned startingRank,
        const unsigned maxRecords);

    virtual void Start();
    virtual void Update(const unsigned timeStep);

protected:
    void Finish(const FinishType finishType, const int resultCode = 0);

private:
    char m_TableName[RLDB_MAX_TABLE_NAME_CHARS];   

    rlDbDynSchema m_LoadedSchema;
    const rlDbSchemaBase* m_Schema;
    rlDbValue* m_Values;
    unsigned m_NumValues;
    rlDbReadResults m_DbResults;

    char m_ExportFilename[256];
    unsigned m_StartingRank;
};

//PURPOSE
//  Read a file containing the table schema and a list of records to either
//  create or update.  If a record has a recordid field it is an update,
//  otherwise a new record will be created.
class rlDbConsoleSyncTableTask : public rlDbConsoleTask
{
public:
    RL_TASK_DECL(rlDbConsoleSyncTableTask);

    rlDbConsoleSyncTableTask();
    virtual ~rlDbConsoleSyncTableTask();

    bool Configure(rlDbClient* ctx,
        const char* filename,
        const char* outFilename);

    virtual void Start();
    virtual void Update(const unsigned timeStep);

protected:
    void Finish(const FinishType finishType, const int resultCode = 0);

private:
    const RecordInfo* GetRecordInfo(const rlDbRecordId recordId) const;

    char m_ExportFilename[256];

    enum
    {
        STATE_IDLE = 0,

        STATE_START_COUNT_REQUEST,
        STATE_COUNT_REQUEST_PENDING,

        STATE_READ_REQUEST_PENDING,

        STATE_START_DELETE_REQUEST,
        STATE_DELETE_REQUEST_PENDING,

        STATE_START_CREATEUPDATE_REQUEST,
        STATE_CREATEUPDATE_REQUEST_PENDING,

        STATE_START_EXPORT,
        STATE_EXPORT_PENDING
    };
    int m_State;

    int m_NumDeletes;
    int m_NumCreates;
    int m_NumUpdates;

    char m_TableName[RLDB_MAX_TABLE_NAME_CHARS];   
    RecordList m_Records;
    unsigned m_NumRecords;

    rlDbFixedSchema<1> m_CheckRecordSchema;
    rlDbDynSchema m_Schema;
    rlDbValue* m_Values;
    unsigned m_NumValues;
    rlDbReadResults m_DbResults;

    unsigned m_CurRecord;
    rlDbRecordId m_RecordId;

    rlDbConsoleExportTableTask m_ExportTask;
};

}   //namespace rage

#endif // RLDBCONSOLETASKS_H
