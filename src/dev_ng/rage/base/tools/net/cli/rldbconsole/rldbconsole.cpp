// 
// rldbconsole/rldbconsole.cpp 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#include "system/main.h"

#include "diag/seh.h"
#include "net/nethardware.h"
#include "net/status.h"
#include "parsercore/attribute.h"
#include "parsercore/element.h"
#include "parsercore/stream.h"
#include "parser/manager.h"
#include "parser/tree.h"
#include "parser/treenode.h"
#include "rline/rl.h"
#include "rline/rldatabaseclient.h"
#include "string/string.h"
#include "system/ipc.h"
#include "system/memory.h"
#include "rline/rlgamespy.h"
#include "rline/rlgamespysake.h"
#include "rline/rltask.h"
#include "rline/rltitleid.h"
#include "system/nelem.h"
#include "system/param.h"
#include "system/timer.h"
#include <stdio.h>

#include "rlDbConsoleTasks.h"

using namespace rage;

//Version history
//  09/01/09    1.0 Initial version
//  09/11/09    1.1 Added sync command
//  09/21/09    1.2 Added -maxrecords, and -outfile for upload cmd
//  09/23/09    1.3 Changed import/export to no longer need schema files.
//                  Added startRank to export.
//                  Made outfile required on sync.
//  12/15/09    1.4 Added support for binary fields in base-64.  Misc bug fixes.
//  01/08/10    1.5 Bug fix in reading base-64 in sync task.
//  02/16/10    1.6 Bug fix in rlDbValue::FromString() for hex int64 values
const unsigned MAJOR_VERSION = 1;
const unsigned MINOR_VERSION = 6;

///////////////////////////////////////////////////////////////////////////////
//Cmdline parameters
///////////////////////////////////////////////////////////////////////////////
PARAM(gsEnvironment,        "Environment to run in (part, cert, prod)");
PARAM(gsGameName,           "Gamespy game name (ex. \"rockstardev\")");
PARAM(gsSecretKey,          "Gamespy secret key (ex. \"1a8bBi\")");
PARAM(gsProductId,          "Gamespy product ID (ex. 10947)");
PARAM(gsGameId,             "Gamespy game ID (ex. 1572)");
PARAM(gsLoginEmail,         "Email used to login to Gamespy (ex. \"test1@rage\")");
PARAM(gsLoginPassword,      "Password used to login to Gamespy (ex. \"password\"");

PARAM(cmd,                  "Operation to perform");
PARAM(table,                "Table to work on");
PARAM(file,                 "File to load or save to");
PARAM(recordid,             "Record ID (for updates, deletes, and specific reads");
PARAM(fileid,               "File to download, or point to");
PARAM(outfile,              "File to save sync results to");
PARAM(startrank,            "Rank to start exporting rows from (default is 1)");
PARAM(maxrecords,           "Maximum records to export (defaults to 1000)");

netHardware m_NetHw;
netSocket   m_NetSkt;
u8* m_Heap = 0;
sysMemSimpleAllocator* m_Allocator = 0;
rlDbClient m_DbClient;
bool m_Initialized = false;

const unsigned MAX_FILE_SIZE = 1024 * 1024 * 10;
char m_FileBuf[MAX_FILE_SIZE];
unsigned m_FileSize = 0;
unsigned m_RecordId = 0;
rlDbFileId m_FileId = 0;

void Usage()
{
    Printf("rldbconsole v%u.%u\n", MAJOR_VERSION, MINOR_VERSION);
    Printf("\n");
    Printf("Usage:\n");
    Printf("    rldbconsole [required params] [cmd params]\n");
    Printf("\n");
    Printf("Required params:\n");
    Printf("    -gsLoginEmail       Gamespy login name for admin account (ex. rdr2admin@rockstarsandiego.com)\n");
    Printf("    -gsLoginPassword    Pasword for admin account\n");
    Printf("    -gsEnvironment      part|cert|prod\n");
    Printf("    -gsGameId           Gamespy game ID (ex. 2753)\n");
    Printf("    -gsGameName         Gamespy game name (ex. rdr2x360)\n");
    Printf("    -gsProductId        Gamespy product ID (ex. 11981\n");
    Printf("    -gsSecretKey        Gamespy secret key (ex. H1Dgd3)\n");
    Printf("    -cmd                upload|download|delete|import|export|sync\n");
    Printf("\n");
    Printf("Upload cmd params:\n");
    Printf("    -file               File to upload\n");
    Printf("    {-outfile}          File to save resulting file ID to.  This file will be appended to.\n");
    Printf("Download cmd params:\n");
    Printf("    -fileid             ID of file to download\n");  
    Printf("    -file               File to save to\n");
    Printf("Delete cmd params:\n");
    Printf("    -table              Table to delete from (w/o env prefix)\n");
    Printf("    -recordid           ID of record to delete\n");
    Printf("Import cmd params:\n");
    Printf("    -file               XML file to import\n");
    Printf("Export cmd params:\n");
    Printf("    -file               XML containing table name and schema\n");
    Printf("    -outfile            File to export to\n");
    Printf("    -startrank          Rank to start exporting from (default is 1)\n");
    Printf("    -maxrecords         Max records to export (default is 1000)\n");
    Printf("Sync cmd params:\n");
    Printf("    -file               XML file to import\n");
    Printf("    -outfile            XML file to export resulting table\n");
}

void Shutdown()
{    
    if(m_Initialized)
    {
        m_DbClient.Shutdown();

        if(rlIsInitialized())
        {
            rlShutdown();
        }

        if(m_NetSkt.GetHardware())
        {
            m_NetHw.DestroySocket(&m_NetSkt);
        }

        if(m_Allocator)
        {
            delete m_Allocator;
            m_Allocator = NULL;

            delete [] m_Heap;
            m_Heap = NULL;
        }

        SHUTDOWN_PARSER;

        m_Initialized = false;
    }
}

bool Init()
{
    rtry
    {
        rcheck(!m_Initialized, catchall,);

        m_Initialized = true; //So Shutdown() will work if we have error

        INIT_PARSER;

        //Allocate a the heap that everything will use
        unsigned sizeofHeap = 1024 * 1024 * 10;
        m_Heap = rage_new u8[sizeofHeap];
        m_Allocator = rage_new sysMemSimpleAllocator(m_Heap, sizeofHeap, sysMemSimpleAllocator::HEAP_NET);

        //Get title ID info
        const char* gsEnvironment   = 0;
        const char* gsGameName      = 0;
        const char* gsSecretKey     = 0;
        const char* gsLoginEmail    = 0;
        const char* gsLoginPassword = 0;
        int gsProductId             = 0;
        int gsGameId                = 0;

        rcheck(PARAM_gsEnvironment.Get(gsEnvironment), catchall, Printf("Missing or invalid param: gsEnvironment\n"));
        rcheck(PARAM_gsGameName.Get(gsGameName), catchall, Printf("Missing or invalid param: gsGameName\n"));
        rcheck(PARAM_gsSecretKey.Get(gsSecretKey), catchall, Printf("Missing or invalid param: gsSecretKey\n"));
        rcheck(PARAM_gsProductId.Get(gsProductId), catchall, Printf("Missing or invalid param: gsProductId\n"));
        rcheck(PARAM_gsGameId.Get(gsGameId), catchall, Printf("Missing or invalid param: gsGameId\n"));
        rcheck(PARAM_gsLoginEmail.Get(gsLoginEmail), catchall, Printf("Missing or invalid param: gsLoginEmail\n"));
        rcheck(PARAM_gsLoginPassword.Get(gsLoginPassword), catchall, Printf("Missing or invalid param: gsLoginPassword\n"));

        //Determine our environment
        rlGamespyEnvironment env = RLGS_ENV_DEV;
        if(!strcmp(gsEnvironment, "part"))
        {
            env = RLGS_ENV_DEV;
        }
        else if(!strcmp(gsEnvironment, "cert"))
        {
            env = RLGS_ENV_CERT;
        }
        else if(!strcmp(gsEnvironment, "prod"))
        {
            env = RLGS_ENV_PROD;
        }

        rlGamespyTitleId gamespyTitleId(env,
            gsGameName,
            gsSecretKey,
            gsProductId,
            gsGameId,
            NULL,       //Not using Atlas
			NULL,
			NULL,
            false);     //Don't encrypt Sake

#if __WIN32PC
        rlRosTitleId rosTitleId("", RLROS_ENV_DEV);
        rlSocialClubTitleId socialClubTitleId(0, RLSC_ENV_UNKNOWN);
        rlTitleId titleId(gamespyTitleId, rosTitleId, socialClubTitleId);

        rcheck(m_NetHw.CreateSocket(&m_NetSkt, 0, NET_PROTO_UDP, NET_SOCKET_NONBLOCKING), catchall, );
        rcheck(rlInit(m_Allocator, &m_NetSkt, &titleId, 0), catchall, );
        rcheck(m_DbClient.Init(), catchall, );

        //Set the account Gamespy should login with.
        rcheck(rlGamespy::SetToolLogin(gsLoginEmail, gsLoginPassword), catchall, );
#endif
    }
    rcatchall
    {
        Shutdown();
        return false;
    }
    return true;
}

void Update()
{
    m_NetHw.Update();
    rlUpdate();
    m_DbClient.Update();
    sysIpcSleep(1);
}

rlDbConsoleTask* CreateTask(netStatus* status)
{
    rlDbConsoleTask* task = 0;

    rtry
    {
        //See what cmd it is we'll perform
        const char* cmdName = 0;
        rcheck(PARAM_cmd.Get(cmdName), catchall, rlError("No cmd specified"));

        //Get parameters for ops, just to have them on hand.
        const char* table = "";
        PARAM_table.Get(table);

        int recordId = 0;
        PARAM_recordid.Get(recordId);

        int fileId = 0;
        PARAM_fileid.Get(fileId);

        const char* filename = "";
        PARAM_file.Get(filename);

        const char* outFilename = "";
        PARAM_outfile.Get(outFilename);

        int startRank = 1;
        PARAM_fileid.Get(startRank);

        int maxRecords = 1000;
        PARAM_fileid.Get(maxRecords);

        if(!strcmp(cmdName, "upload"))
        {
            task = rage_new rlDbConsoleUploadFileTask();
            rcheck(rlTaskBase::Configure((rlDbConsoleUploadFileTask*)task, &m_DbClient, filename, outFilename, status), catchall, );
        }
        else if(!strcmp(cmdName, "download"))
        {
            task = rage_new rlDbConsoleDownloadFileTask();
            rcheck(rlTaskBase::Configure((rlDbConsoleDownloadFileTask*)task, &m_DbClient, fileId, filename, status), catchall, );
        }
        else if(!strcmp(cmdName, "delete"))
        {
            task = rage_new rlDbConsoleDeleteRecordTask();
            rcheck(rlTaskBase::Configure((rlDbConsoleDeleteRecordTask*)task, &m_DbClient, table, recordId, status), catchall, );
        }
        else if(!strcmp(cmdName, "import"))
        {
            task = rage_new rlDbConsoleImportTableTask();
            rcheck(rlTaskBase::Configure((rlDbConsoleImportTableTask*)task, &m_DbClient, filename, status), catchall, );
        }
        else if(!strcmp(cmdName, "export"))
        {
            task = rage_new rlDbConsoleExportTableTask();
            rcheck(rlTaskBase::Configure((rlDbConsoleExportTableTask*)task, &m_DbClient, filename, outFilename, startRank, (unsigned)maxRecords, status), catchall, );
        }
        else if(!strcmp(cmdName, "sync"))
        {
            task = rage_new rlDbConsoleSyncTableTask();
            rcheck(rlTaskBase::Configure((rlDbConsoleSyncTableTask*)task, &m_DbClient, filename, outFilename, status), catchall, );
        }
    }
    rcatchall
    {
        if(task)
        {
            delete task;
            task = 0;
        }
    }

    return task;
}

int Main()
{
    rlDbConsoleTask* task = 0;
    netStatus status;

    rtry
    {
        rcheck(Init(), catchall, Usage());

        rcheck(0 != (task = CreateTask(&status)), catchall, Usage());

        Printf("Logging into Gamespy...");

        unsigned startTime = sysTimer::GetSystemMsTime();

        while(!g_rlGamespy.IsOnline())
        {
            Update();

            if(!g_rlGamespy.IsOnline() && ((sysTimer::GetSystemMsTime()-startTime) > 20000))
            {
                Printf("FAILED(timed out).\n");
                return -1;
            }
        }

        Printf("...SUCCEEDED.\n");

        Printf("Performing %s...\n", task->GetTaskName());

        task->Start();

        while(status.Pending())
        {
            Update();
            task->Update(1);
        }

        if(status.Succeeded())
        {
            Printf("%s SUCCEEDED.\n", task->GetTaskName());
        }
        else
        {
            Printf("%s FAILED.\n", task->GetTaskName());
        }
    }
    rcatchall
    {
    };

    if(task)
    {
        delete task;
        task = 0;
    }

    Shutdown();

    return status.Succeeded() ? 0 : -1;
}
