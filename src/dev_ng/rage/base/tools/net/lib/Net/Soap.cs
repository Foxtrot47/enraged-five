﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Text;

namespace Rockstar.Net
{
    public enum SoapVersion
    {
        Soap1_1,
        Soap1_2
    };

    //PURPOSE
    //  Minimal SOAP envelope information.
    public class SoapEnvelope
    {
        public SoapVersion Version { get; private set; }
        public string Content { get; private set; }

        public SoapEnvelope(SoapVersion version,
                            string content)
        {
            Version = version;
            Content = content;
        }
    }

    //PURPOSE
    //  Soap envelope with additional context information.
    public class SoapEnvelopeEx : SoapEnvelope
    {
        public string MethodName { get; private set; }
        public string MethodNamespace { get; private set; }

        public SoapEnvelopeEx(SoapVersion version,
                              string methodName,
                              string methodNamespace,
                              string content)
        : base(version, content)
        {
            MethodName = methodName;
            MethodNamespace = methodNamespace;
        }
    }

    //PURPOSE
    //  Helper class for constructing SOAP envelopes.  Call AddParam() to add
    //  method parameters, and when finished call GenerateEnvelope() to get
    //  the SoapEnvelope.
    public class SoapEnvelopeBuilder
    {
        public SoapVersion Version { get; private set; }
        public string MethodName { get; private set; }
        public string MethodNamespace { get; private set; }
        public int NumParams { get; private set; }
        
        private StringBuilder m_Sb;

        public SoapEnvelopeBuilder(SoapVersion version,
                                   string methodName,
                                   string methodNamespace)
        {
            Version = version;
            MethodName = methodName;
            MethodNamespace = methodNamespace;
            NumParams = 0;

            if (!MethodNamespace.EndsWith("/"))
            {
                MethodNamespace = MethodNamespace + "/";
            }

            m_Sb = new StringBuilder();

            m_Sb.Append("<?xml version=\"1.0\" encoding=\"utf-8\"?>");


            if (Version == SoapVersion.Soap1_1)
            {
                m_Sb.Append("<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"><soap:Body>");
            }
            else
            {
                m_Sb.Append("<soap12:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap12=\"http://www.w3.org/2003/05/soap-envelope\"><soap12:Body>");
            }

            m_Sb.Append(string.Format("<{0} xmlns=\"{1}\" >", MethodName, MethodNamespace));
        }

        public void AddParam(string name, string value)
        {
            m_Sb.Append("<" + name + ">");
            if (value != null)
            {
                m_Sb.Append(value);
            }
            m_Sb.Append("</" + name + ">");
            ++NumParams;
        }

        public string GenerateContent()
        {
            string content = m_Sb.ToString() + string.Format("</{0}>", MethodName);

            if (Version == SoapVersion.Soap1_1)
            {
                content += "</soap:Body></soap:Envelope>";
            }
            else
            {
                content += "</soap12:Body></soap12:Envelope>";
            }

            return content;
        }

        public SoapEnvelope GenerateEnvelope()
        {
            return new SoapEnvelope(Version, GenerateContent());
        }

        public SoapEnvelopeEx GenerateEnvelopeEx()
        {
            return new SoapEnvelopeEx(Version, MethodName, MethodNamespace, GenerateContent());
        }
    };
}
