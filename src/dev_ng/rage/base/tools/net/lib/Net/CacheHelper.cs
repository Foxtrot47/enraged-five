using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Diagnostics;
using System.Web;
using System.Web.Caching;

using Rockstar.Diag;

namespace Rockstar.Net
{
    public class CacheHelper<T>
    {
        private static readonly object m_Lock = new object();

        //PURPOSE
        //  Delegate to create an object.  Apps should use usedata (passed
        //  into CacheHelper.Get()) to provide any info necessary for creation.
        public delegate void CreateObjectDelegate(object userdata,
                                                  out T obj,
                                                  out DateTime absoluteExpirationTime);

        //PURPOSE
        //  Retrieves the object from the given cache.  If the object does not
        //  exist, the create delegate is called to create it.
        public static T Get(string key,
                            CreateObjectDelegate createDelegate,
                            object userdata)
        {
            Context ctx = NetContext.Create("key={0}", key);

            //See if it's in the cache...
            CachedObjectInfo info = HttpRuntime.Cache.Get(key) as CachedObjectInfo;

            if (info != null)
            {
                ctx.Debug3("Found in cache");
                return info.m_Object;
            }
            else
            {
                ctx.Debug3("Not in cache, acquiring lock...");

                lock (m_Lock)
                {
                    ctx.Debug3("Checking again...");

                    //Try to get again; it may have been inserted by another thread
                    //while we were waiting for lock.
                    info = HttpRuntime.Cache.Get(key) as CachedObjectInfo;

                    if (info != null)
                    {
                        ctx.Debug3("Found in cache (inserted by other thread)");
                        return info.m_Object;
                    }
                    else
                    {
                        ctx.Debug3("Still not in cache, creating...");

                        T obj;
                        DateTime absExpiration;
                        createDelegate(userdata, out obj, out absExpiration);

                        if (obj != null)
                        {
                            ctx.Debug3("Inserting into cache...");

                            //Put the object in the cache.  Because we need to know
                            //how to handle the object in the remove callback, we 
                            //wrap it in an info object.
                            info = new CachedObjectInfo(obj,
                                                        absExpiration,
                                                        createDelegate,
                                                        userdata);
                            HttpRuntime.Cache.Add(key,
                                                  info,
                                                  null,           //Dependency
                                                  absExpiration,  //Absolute expiration
                                                  Cache.NoSlidingExpiration,
                                                  CacheItemPriority.Normal,
                                                  OnRemoveCallback);
                        }

                        return obj;
                    }
                }
            }
        }

        //PURPOSE
        //  Callback for when cached object is removed
        public static void OnRemoveCallback(string key, 
                                            object value, 
                                            CacheItemRemovedReason reason)
        {
            Context ctx = NetContext.Create("key={0}", key);

            try
            {
                if (reason == CacheItemRemovedReason.Removed
                   || reason == CacheItemRemovedReason.Underused)
                {
                    ctx.Debug3("Not refreshing cache (reason={0})", reason.ToString());
                    return;
                }

                CachedObjectInfo info = value as CachedObjectInfo;
                if (info == null)
                {
                    ctx.Error(ErrorCode.InvalidData, "Cached object is not CachedObjectInfo<T>");
                    return;
                }

                lock (m_Lock)
                {
                    //Make sure another thread didn't cache it while acquiring lock.
                    if (null != HttpRuntime.Cache.Get(key))
                    {
                        ctx.Debug3("Object was cached in other thread");
                        return;
                    }

                    //Recache the object if it hasn't be recached.  This is so other 
                    //threads can still get the previous value while we are refreshing,
                    //which may take significant time if DB is involved.
                    if (info.m_RecachedCount == 0)
                    {
                        ctx.Debug3("Recaching object");

                        ++info.m_RecachedCount;

                        HttpRuntime.Cache.Insert(key,
                                                 info,
                                                 null,
                                                 DateTime.Now.AddMinutes(5),  //Just needs to be enough time to create new object
                                                 Cache.NoSlidingExpiration,
                                                 CacheItemPriority.Normal,
                                                 null);
                    }
                }

                //Refresh the object.
                T obj;
                DateTime absoluteExpiration;
                info.m_CreateDelegate(info.m_UserData, out obj, out absoluteExpiration);

                if (obj == null)
                {
                    ctx.Error(ErrorCode.UnknownError, "Failed to create new object");
                    return;
                }

                //Put the new object into the cache
                CachedObjectInfo newInfo = new CachedObjectInfo(obj,
                                                                absoluteExpiration,
                                                                info.m_CreateDelegate,
                                                                info.m_UserData);

                HttpRuntime.Cache.Insert(key,
                                         newInfo,
                                         null,                            //Dependency
                                         newInfo.m_AbsoluteExpiration,    //Absolute expiration
                                         Cache.NoSlidingExpiration,
                                         CacheItemPriority.Normal,
                                         OnRemoveCallback);
            }
            catch
            {
                //Don't re-throw.  Some common SQL exceptions in the create
                //would make for a lot of false positives.
            }
        }

        //PURPOSE
        //  Wrapper for a cached object.  We need a wrapper because the remove
        //  callback needs to know how to re-cache the object.
        private class CachedObjectInfo
        {
            public T m_Object { get; private set; }
            public DateTime m_AbsoluteExpiration { get; private set; }
            public CreateObjectDelegate m_CreateDelegate { get; private set; }
            public object m_UserData { get; private set; }
            public int m_RecachedCount;

            public CachedObjectInfo(T obj,
                                    DateTime absoluteExpiration,
                                    CreateObjectDelegate createDelegate,
                                    object userdata)
            {
                m_Object = obj;
                m_AbsoluteExpiration = absoluteExpiration;
                m_CreateDelegate = createDelegate;
                m_UserData = userdata;
                m_RecachedCount = 0;
            }
        }
    }
}

