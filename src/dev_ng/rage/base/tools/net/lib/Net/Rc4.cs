﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;

using Rockstar.Diag;

namespace Rockstar.Net
{
    //PURPOSE
    //  Performs RC4 encryption/decryption (Encrypt() does both).
    public class Rc4Context
    {
        public bool IsValid { get; private set; }
        public byte[] Key { get; private set; }
        private byte m_X;
        private byte m_Y;
        private byte[] m_State = new byte[256];

        public static Rc4Context Generate(uint keyLength)
        {
            byte[] key = new byte[keyLength];
            
            Random rng = new Random();
            rng.NextBytes(key);

            return new Rc4Context(key);
        }

        public Rc4Context()
        {
            IsValid = false;
        }

        public Rc4Context(byte[] key)
        {
            Reset(key);
        }

        public void Reset(byte[] key)
        {
            IsValid = false;

            if (key == null) throw new ArgumentNullException("buf");

            Key = key;
            m_X = 0;
	        m_Y = 0;

	        for(uint i = 0; i < m_State.Length; i++)
            {
		        m_State[i] = (byte)i;
            }
        	
            byte j = 0;

            for (uint i = 0; i < m_State.Length; i++)
	        {
                j = (byte)(j + m_State[i] + key[i % key.Length]);

                byte temp = m_State[i];
                m_State[i] = m_State[j];
                m_State[j] = temp;
	        }

            IsValid = true;
        }

        public void Encrypt(byte[] buf)
        {
            if (buf == null) throw new ArgumentNullException("buf");
            if (!IsValid) throw new Exception("Rc4context not valid");

	        for(uint i = 0; i < buf.Length; i++)
	        {
		        m_X = (byte)(m_X + 1);
		        m_Y = (byte)(m_Y + m_State[m_X]);

                byte temp = m_State[m_X];
                m_State[m_X] = m_State[m_Y];
                m_State[m_Y] = temp;
        		
                buf[i] ^= m_State[(byte)(m_State[m_X] + m_State[m_Y])];
	        }
        }
    };

}
