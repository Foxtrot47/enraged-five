﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Text;
using System.Xml;

using Rockstar.Diag;
using Rockstar.Net;

namespace Rockstar.Net
{
    //PURPOSE
    //  Manages an individual client connection to the SG.
    public class TelnetClient
    {
        public delegate void MsgReceivedDelegate(TelnetClient c, string s);
        public delegate void ConnectionClosedDelegate(TelnetClient c, SocketHelper.Reason reason);

        public uint Id {get; private set;}

        private readonly object m_Lock = new object();
        private SocketHelper m_Skt = new SocketHelper();
        private MsgReceivedDelegate m_MsgReceivedDelegate;
        private ConnectionClosedDelegate m_ConnectionClosedDelegate;
        private object m_UserData;
        private bool m_Initialized = false;

        public TelnetClient(uint id)
        {
            Id = id;
        }

        public Error Init(Socket s,
                          uint inactivityTimeoutMs,
                          MsgReceivedDelegate msgReceivedDelegate,
                          ConnectionClosedDelegate connectionClosedDelegate,
                          object userdata)
        {
            lock (m_Lock)
            {
                Context ctx = NetContext.Create("id={0}", Id);

                m_MsgReceivedDelegate = msgReceivedDelegate;
                m_ConnectionClosedDelegate = connectionClosedDelegate;
                m_UserData = userdata;

                string[] msgTerminators = { "\r\n", "\n\r" };
                Error err;

                if (null != (err = m_Skt.Init(s,
                                new WaitCallback(MsgReceived),
                                new WaitCallback(ConnectionClosed),
                                new TextMsgReader(msgTerminators),
                                new TextMsgWriter(msgTerminators),
                                inactivityTimeoutMs)))
                {
                    return ctx.Error(err);
                }

                m_Initialized =  true;

                return ctx.Success2();
            }
        }

        public void Shutdown()
        {
            lock (m_Lock)
            {
                Context ctx = NetContext.Create("id={0}", this.Id);

                if (m_Initialized)
                {
                    m_Skt.Shutdown();
                    m_Skt = null;
                    m_MsgReceivedDelegate = null;
                    m_ConnectionClosedDelegate = null;
                    m_UserData = null;
                    m_Initialized = false;

                    ctx.Success2();
                }
            }
        }

        private void ConnectionClosed(object reason)
        {
            ConnectionClosedDelegate dlgt = m_ConnectionClosedDelegate;

            Shutdown();

            dlgt(this, (SocketHelper.Reason)reason);
        }

        private void MsgReceived(object msg)
        {
            m_MsgReceivedDelegate(this, msg as string);
        }
    }
}
