﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

using Rockstar.Diag;

namespace Rockstar.Net
{
    public struct ListenSocketHelperStats
    {
        public uint NumAccepts;
        public uint NumCallbacks;
        public uint NumDelegateFailures;
        public uint NumInvalidSockets;
        public uint NumOtherExceptions;
        public uint NumSocketExceptions;

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            int num = 0;

            sb.Append("Lsh[");
            if (NumCallbacks > 0) 
            { 
                sb.Append("cb=" + NumCallbacks); 
                ++num; 
            }
            if (NumAccepts > 0)
            {
                if (num > 0) sb.Append(" ");
                sb.Append("a=" + NumAccepts);
                ++num;
            }
            if (NumInvalidSockets > 0) 
            {
                if (num > 0) sb.Append(" ");
                sb.Append("inv=" + NumInvalidSockets);
                ++num;
            }
            if (NumOtherExceptions > 0) 
            {
                if (num > 0) sb.Append(" ");
                sb.Append("oex=" + NumOtherExceptions);
                ++num;
            }
            if (NumSocketExceptions > 0) 
            {
                if (num > 0) sb.Append(" ");
                sb.Append("sex=" + NumSocketExceptions);
                ++num;
            }
            sb.Append("]");

            return sb.ToString();
        }
    }

    public class ListenSocketHelper
    {      
        public delegate bool OnAcceptCallback(Socket skt);

        public string Name { get; private set; }
        public IPEndPoint EndPoint { get; private set; }
        public ListenSocketHelperStats Stats { get { return m_Stats; } }

        private readonly object m_Lock = new object();
        private Socket m_Skt = null;
        private AsyncCallback m_AcceptCallback;         //From BeginAccept
        private OnAcceptCallback m_OnAcceptCallback;    //Our callback to app on EndAccept
        private ListenSocketHelperStats m_Stats = new ListenSocketHelperStats();
        
        public Error Init(string name,
                          IPEndPoint ep,
                          uint listenBacklogSize,
                          OnAcceptCallback onAcceptCallback)
        {
            Context ctx = NetContext.Create(name);

            lock (m_Lock)
            {
                try
                {
                    Name = name;
                    EndPoint = ep;
                    m_OnAcceptCallback = onAcceptCallback;

                    m_Stats = new ListenSocketHelperStats();

                    m_Skt = new Socket(AddressFamily.InterNetwork,
                                       SocketType.Stream,
                                       ProtocolType.Tcp);
                    m_Skt.Bind(EndPoint);
                    m_Skt.Listen((listenBacklogSize == 0) ? int.MaxValue : (int)listenBacklogSize);

                    m_AcceptCallback = new AsyncCallback(AcceptCallback);

                    m_Skt.BeginAccept(m_AcceptCallback, null);
                    
                    return ctx.Success2("Listening on " + EndPoint.ToString());
                }
                catch (SocketException ex)
                {
                    ++m_Stats.NumSocketExceptions;
                    Trace.WriteLine(string.Format("BeginAccept SocketException: {0} ({1})", ex.SocketErrorCode, (int)ex.SocketErrorCode));
                    Shutdown();
                    return ctx.Error(ex);
                }
                catch (System.Exception ex)
                {
                    ++m_Stats.NumOtherExceptions;
                    Shutdown();
                    return ctx.Error(ex);
                }
            }
        }

        public void Shutdown()
        {
            lock (m_Lock)
            {
                Context ctx = NetContext.Create(Name);

                if (m_Skt != null)
                {
                    ctx.Debug2("Shutting down " + EndPoint.ToString());
                    
                    //FIXME: It appears this is incompatible with AcceptCallback.
                    //       If we close, then AcceptCallback will trigger a null
                    //       reference exception.
                    m_Skt.Close();
                    m_Skt = null;
                }

                m_AcceptCallback = null;
            }
        }

        //PURPOSE
        //  Called when our async Accept() finishes.
        private void AcceptCallback(IAsyncResult ar)
        {
            lock (m_Lock)
            {
                ++m_Stats.NumCallbacks;

                //Context ctx = NetContext.Create();
                bool beginAcceptSucceeded = false;

                try
                {
                    //Socket skt = ar.AsyncState as Socket;

                    //Get the accepted socket
                    try
                    {
                        Socket s = m_Skt.EndAccept(ar);

                        if (s != null && s.Connected)
                        {
                            ++m_Stats.NumAccepts;

                            //ctx.Debug3("Accepted connection from {0}", s.RemoteEndPoint);

                            //Notify our owner of the new socket.
                            bool dlgtSucceeded = false;

                            try
                            {
                                dlgtSucceeded = m_OnAcceptCallback(s);
                            }
                            catch (Exception ex)
                            {
                                //ctx.Error(ex);
                                Trace.WriteLine(ex);
                            }

                            if (!dlgtSucceeded)
                            {
                                ++m_Stats.NumDelegateFailures;
                                s.Close();
                            } 
                        }
                        else
                        {
                            ++m_Stats.NumInvalidSockets;
                            //ctx.Debug3("Null or unconnected socket; ignoring");
                            Trace.WriteLine("Null or unconnected socket; ignoring");
                        }
                    }
                    catch (SocketException ex)
                    {
                        ++m_Stats.NumSocketExceptions;
                        //ctx.Error(ex.SocketErrorCode, "EndAccept SocketException");
                        Trace.WriteLine("EndAccept SocketException: " + ex.SocketErrorCode);
                    }
                    catch (Exception ex)
                    {
                        ++m_Stats.NumOtherExceptions;
                        //ctx.Error(ex);
                        Trace.WriteLine(ex);
                    }

                    //Begin our next accept.
                    try
                    {
                        m_Skt.BeginAccept(m_AcceptCallback, m_Skt);
                        beginAcceptSucceeded = true;
                    }
                    catch (SocketException ex)
                    {
                        ++m_Stats.NumSocketExceptions;
                        //ctx.Error(ex.SocketErrorCode, "BeginAccept SocketException");
                        Trace.WriteLine("BeginAccept SocketException: " + ex.SocketErrorCode);
                    }
                    catch (Exception ex)
                    {
                        ++m_Stats.NumOtherExceptions;
                        //ctx.Error(ex);
                        Trace.WriteLine(ex);
                    }
                }
                catch (Exception ex)
                {
                    ++m_Stats.NumOtherExceptions;
                    //ctx.Error(ex);
                    Trace.WriteLine(ex);
                }

                if (!beginAcceptSucceeded)
                {
                    //TODO: If the BeginAccept failed, we're pretty fucked.  We have 
                    //      to Shutdown, and wait to be reinitialized.
                    //      Probably need to have a callback to notify someone when
                    //      this happens.
                }
            }
        }
    }

}
