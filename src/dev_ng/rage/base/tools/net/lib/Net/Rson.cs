﻿using System;
using System.Diagnostics;
using System.Text;
using System.Linq;

using Rockstar.Diag;

namespace Rockstar.Net
{

/*
    RSON is a text based data definition language inspired by
    JSON (http://www.json.org), which is based on JavaScript, though
    RSON more closely resembles Lua.

    RSON consists of a hierarchy of objects.  Objects can have names.
    Composite objects are surrounded by '{' and '}' and contain a comma
    separate collection of more objects.

    RSON is intended to be as expressive as XML but much less verbose.
    Almost anything that can be expressed in XML can be expressed in RSON.

    Example RSON:

    config=
    {
        Windows=
        {
            MainWindow={x=100,y=300,w=600,h=400,background={color=red,image="c:/foo.jpg"},
            LogWindow={x=32,y=30,w=500,h=100,background={color=white}
        },
        RecentFiles=
        {
            "c:/Documents/jackass.doc",
            "c:/Documents/YearEndSummary.xls",
            "c:/Temp/MyCV.doc"
        }
    }

    RsonWriter is used to compose an RSON object, while RsonReader is used to
    parse RSON objects.

    RSON's initial purpose was to construct an optimized request/response
    protocol for accessing web services from game consoles.  Between the
    console and the web server would sit a proxy that would transform RSON
    requests into SOAP and/or REST-ful web service requests and forward
    them to the appropriate web server.  The proxy would receive the response
    from the server, transform it to RSON format, and forward it to the
    game console.

    There are C++ versions of RsonWriter and RsonReader located in Rage.
*/

    internal class Rson
    {
        public static string RESERVED_CHARS_STRING = "{}=,";
        public static char[] RESERVED_CHARS = RESERVED_CHARS_STRING.ToCharArray();

        public static string WHITESPACE_CHARS_STRING = " \f\n\r\t\v";
        public static char[] WHITESPACE_CHARS = WHITESPACE_CHARS_STRING.ToCharArray();

        public static int[] BASE64_SHIFT =
        {
            10, 4, 6, 8,
        };

        public static char[] BASE64_ALPHABET =
            "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/".ToCharArray();

        //PURPOSE
        //  Returns true if the character is reserved in RSON.
        public static bool IsReserved(char c)
        {
            return RESERVED_CHARS_STRING.Contains(c);
        }

        //PURPOSE
        //  Returns true if the character is reserved in RSON.
        public static bool IsReserved(byte b)
        {
            return RESERVED_CHARS_STRING.Contains((char) b);
        }

        //PURPOSE
        //  Returns true if the character is whitespace.
        public static bool IsSpace(char c)
        {
            return WHITESPACE_CHARS_STRING.Contains(c);
        }

        //PURPOSE
        //  Returns true if the character is whitespace.
        public static bool IsSpace(byte b)
        {
            return WHITESPACE_CHARS_STRING.Contains((char) b);
        }
    }

    public class RsonWriter
    {
        //Compatibility flags

        //All strings are in double quotes.
        //If this flag is not set then only strings with whitespace
        //and/or special characters will be in double quotes.
        public static readonly uint FLAG_QUOTED_STRINGS     = 0x01;

        //Default flags
        public static readonly uint FLAG_DEFAULT_FLAGS = FLAG_QUOTED_STRINGS;

        //PURPOSE
        //  Construct an RsonWriter with an initial capacity.
        //PARAMS
        //  capacity    - Initial number of characters to reserve.
        public RsonWriter(int capacity)
        {
            m_Buf = new StringBuilder(capacity);
        }

        //PURPOSE
        //  Reset to the post construction state.
        //NOTES
        //  The buffer created in the constructor will continue to be used.
        //  Compatibility flags will persist.
        public void Reset()
        {
            m_Buf.Length = 0;
            //Don't clear compatibility flags.
            m_Error = false;
            m_NeedComma = false;
        }

        //PURPOSE
        //  Reset to the post construction state, but save the current
        //  "comma" state.  IOW, if another item is written  the comma
        //  logic will proceed as if there were prior items added.
        //NOTES
        //  This is used to continue writing after the buffer has been filled
        //  and flushed, which can occur if the buffer is not large enough
        //  to contain the entire collection of items.
        public void ResetToContinue()
        {
            //Preserve flags and needComma.
            uint flags = m_Flags;
            bool needComma = this.NeedComma();
            this.Reset();
            m_Flags = flags;
            m_NeedComma = needComma;
        }

        //PURPOSE
        //  Sets compatibility flags.
        public void SetFlags(uint flags)
        {
            m_Flags = flags;
        }

        //PURPOSE
        //  Returns compatibility flags.
        public uint GetFlags()
        {
            return m_Flags;
        }

        //PURPOSE
        //  Begins a new object.  Appends a '{'.
        public bool Begin()
        {
            int i;
            return this.Begin(null, out i);
        }

        //PURPOSE
        //  Begins a new named object.  Appends '<name>={'.
        //PARAMS
        //  name        - Name of object.  Can be null.
        public bool Begin(string name)
        {
            int i;
            return this.Begin(name, out i);
        }

        //PURPOSE
        //  Begins a new named object and returns the prior cursor position.
        //  Appends '<name>={'.
        //PARAMS
        //  name        - Name of object.  Can be null.
        //  cursor      - Will contain prior cursor position on return.
        //NOTES
        //  Used to begin an object and preserve the cursor position in case
        //  the caller needs to "undo".
        public bool Begin(string name, out int cursor)
        {
            cursor = m_Buf.Length;

            if(!m_Error)
            {
                this.Write("{0}", name, "{");
            }

            return !m_Error;
        }

        //PURPOSE
        //  Ends the current object.  Appends a '}'.
        public bool End()
        {
            return this.Write("}");
        }


        //PURPOSE
        //  Undo back to the position where the cursor was saved (see Begin()).
        //PARAMS
        //  cursor      - Cursor position saved from a call to Begin().
        public void Undo(int cursor)
        {
            if(cursor != m_Buf.Length)
            {
                Debug.Assert(cursor >= 0);
                Debug.Assert(cursor < m_Buf.Length);
                Debug.Assert('{' == m_Buf[cursor] || ',' == m_Buf[cursor]);

                m_Buf.Length = cursor;

                if(cursor > 0)
                {
                    m_NeedComma = ('{' != m_Buf[cursor-1] && ',' != m_Buf[cursor-1]);
                }
                else
                {
                    m_NeedComma = false;
                }
            }
        }

        //PURPOSE
        //  Write an integer value.
        //PARAMS
        //  name        - Name of the value.  Can be NULL.
        //  value       - The value.
        public bool WriteInt(string name, int value)
        {
            return this.Write("{0:d}", name, value);
        }

        //PURPOSE
        //  Write an 64-bit integer value.
        //PARAMS
        //  name        - Name of the value.  Can be NULL.
        //  value       - The value.
        public bool WriteInt64(string name, Int64 value)
        {
            return this.Write("{0:d}", name, value);
        }

        //PURPOSE
        //  Write an unsigned value.
        //PARAMS
        //  name        - Name of the value.  Can be NULL.
        //  value       - The value.
        public bool WriteUns(string name, uint value)
        {
            return this.Write("{0:d}", name, value);
        }

        //PURPOSE
        //  Write an unsigned 64-bit value.
        //PARAMS
        //  name        - Name of the value.  Can be NULL.
        //  value       - The value.
        public bool WriteUns64(string name, UInt64 value)
        {
            return this.Write("{0:d}", name, value);
        }

        //PURPOSE
        //  Write a float value.
        //PARAMS
        //  name        - Name of the value.  Can be NULL.
        //  value       - The value.
        public bool WriteFloat(string name, double value)
        {
            return this.Write("{0:F5}", name, value);
        }

        //PURPOSE
        //  Write an unsigned value in hexadecimal, preceded by 0x.
        //PARAMS
        //  name        - Name of the value.  Can be NULL.
        //  value       - The value.
        public bool WriteHex(string name, uint value)
        {
            return this.Write("0x{0:X}", name, value);
        }

        //PURPOSE
        //  Write an unsigned 64-bit value in hexadecimal, preceded by 0x.
        //PARAMS
        //  name        - Name of the value.  Can be NULL.
        //  value       - The value.
        public bool WriteHex64(string name, UInt64 value)
        {
            return this.Write("0x{0:X}", name, value);
        }

        //PURPOSE
        //  Encodes binary to base64 (RFC 4648).  This implementation does
        //  not add padding as defined by RFC 4648.
        //PARAMS
        //  name        - Name of the value.  Can be NULL.
        //  value       - Bytes to encode.
        //  len         - Number of bytes
        public bool WriteBinary(string name, byte[] value)
        {
            bool success = this.Write("{0}", name, "");

            int numBits = value.Length * 8;
            int numCodes = (numBits + 5) / 6;
            int idx0 = 0;
            int idx1 = 6;
            for(int i = 0; i < numCodes && success; ++i, idx0 += 6, idx1 += 6)
            {
                uint tmp = (uint) (value[idx0 >> 3] << 8);
                if(idx1 < numBits)
                {
                    tmp |= value[idx1 >> 3];
                }

                int shift = Rson.BASE64_SHIFT[i & 0x03];
                uint val = (uint) (tmp >> shift) & 0x3F;

                success = this.Write(Rson.BASE64_ALPHABET[val]);
            }

            return success;
        }

        //PURPOSE
        //  Write a boolean value.
        //PARAMS
        //  name        - Name of the value.  Can be NULL.
        //  value       - The value.
        public bool WriteBool(string name, bool value)
        {
            return this.Write("{0}", name, value ? "true" : "false");
        }

        //PURPOSE
        //  Write a string value.
        //PARAMS
        //  name        - Name of the value.  Can be NULL.
        //  value       - The value.
        public bool WriteString(string name, string value)
        {
            bool success = false;

            StringBuilder sb = new StringBuilder(value.Length);
            bool escaped = false;

            foreach(char c in value)
            {
                if(escaped)
                {
                    escaped = false;
                }
                else if('\\' == c)
                {
                    escaped = true;
                }
                else if('\"' == c)
                {
                    sb.Append('\\');
                }

                sb.Append(c);
            }

            value = sb.ToString();

            //Strings will be enclosed in quotes if they contain reserved
            //characters or if the QUOTED_STRINGS flag is on.

            if((0 != (m_Flags & FLAG_QUOTED_STRINGS))
                || value.IndexOfAny(Rson.RESERVED_CHARS) >= 0
                || value.IndexOfAny(Rson.WHITESPACE_CHARS) >= 0)
            {
                success = this.Write("\"{0}\"", name, value);
            }
            else
            {
                success = this.Write("{0}", name, value);
            }

            return success;
        }

        //PURPOSE
        //  Write a value.
        //PARAMS
        //  name        - Name of the value.  Can be null.
        //  value       - The value.
        bool WriteValue(string name, int value){return this.WriteInt(name, value);}
        bool WriteValue(string name, uint value){return this.WriteUns(name, value);}
        bool WriteValue(string name, UInt64 value){return this.WriteUns64(name, value);}
        bool WriteValue(string name, float value){return this.WriteFloat(name, value);}
        bool WriteValue(string name, bool value){return this.WriteBool(name, value);}
        bool WriteValue(string name, string value){return this.WriteString(name, value);}
        bool WriteValue(string name, object val)
        {
            Type type = val.GetType();

            if(typeof(int) == type)
            {
                return this.WriteValue(name, (int) val);
            }
            else if(typeof(uint) == type)
            {
                return this.WriteValue(name, (uint) val);
            }
            else if(typeof(UInt64) == type)
            {
                return this.WriteValue(name, (UInt64) val);
            }
            else if(typeof(float) == type)
            {
                return this.WriteValue(name, (float) val);
            }
            else if(typeof(bool) == type)
            {
                return this.WriteValue(name, (bool) val);
            }
            else if(typeof(string) == type)
            {
                return this.WriteValue(name, (string) val);
            }

            return false;
        }

        //PURPOSE
        //  Write an array of values surrounded by '{' '}'.
        //PARAMS
        //  name        - Name of the array.  Can be null.
        //  values      - The values.
        //  count       - Number of values to write.
        public bool WriteArray(string name, object values, int count)
        {
            bool success = false;

            Array avals = values as Array;
            if(null != avals)
            {
                success = this.Begin(name);
                for(int i = 0; i < count && success; ++i)
                {
                    success = this.WriteValue(null, avals.GetValue(i));
                }

                success = success && this.End();
            }

            return success;
        }

        //PURPOSE
        //  Returns true if there was an error while writing.
        //NOTES
        //  If there's an error no further writing will occur until one
        //  of the Reset() functions is called.
        public bool HasError()
        {
            return m_Error;
        }

        //PURPOSE
        //  Return the string that has been built.
        public override string ToString()
        {
            return m_Buf.ToString();
        }

        //PURPOSE
        //  Return the string that has been built as an array of bytes.
        public byte[] ToBytes()
        {
            return System.Text.Encoding.UTF8.GetBytes(this.ToString());
        }

        private bool NeedComma()
        {
            return m_NeedComma;
        }

        private string Comma()
        {
            return this.NeedComma() ? "," : "";
        }

        private int Available()
        {
            int avail = m_Buf.Capacity - m_Buf.Length
                        - 1;    //number of chars added by End()

            return avail > 0 ? avail : 0;
        }

        private bool Write<T>(string fmt, string name, T value)
        {
            if(!m_Error)
            {
                string tmpFmt;

                if(null != name && name.Length > 0)
                {
                    tmpFmt = String.Format("{0}{1}={2}", this.Comma(), name, fmt);
                }
                else
                {
                    tmpFmt = String.Format("{0}{1}", this.Comma(), fmt);
                }

                this.Write(String.Format(tmpFmt, value));
            }

            return !m_Error;
        }

        private bool Write(string buf)
        {
            if(!m_Error)
            {
                if(this.Available() >= buf.Length)
                {
                    m_Buf.Append(buf);
                    m_NeedComma = ('{' != buf[buf.Length-1] && ',' != buf[buf.Length - 1]);
                }
                else
                {
                    m_Error = true;
                }
            }

            return !m_Error;
        }

        private bool Write(char c)
        {
            if(!m_Error)
            {
                if(this.Available() >= 1)
                {
                    m_Buf.Append(c);
                    m_NeedComma = ('{' != c && ',' != c);
                }
                else
                {
                    m_Error = true;
                }
            }

            return !m_Error;
        }

        private StringBuilder m_Buf;

        private uint m_Flags = FLAG_DEFAULT_FLAGS;

        private bool m_Error = false;
        private bool m_NeedComma = false;

        public static void TestMe()
        {
            RsonWriter rw = new RsonWriter(1024);

            System.Text.ASCIIEncoding  encoding = new System.Text.ASCIIEncoding();
            byte[] binval = encoding.GetBytes("foobar");
            int[] ints = {1,2,3,4};
            uint[] uns = {1,2,3,4};
            float[] floats = {1.5f,1.25f,1.125f,1.0625f};

            rw.SetFlags(0);

            bool success = rw.Begin();
            Debug.Assert(success);
            success = rw.WriteInt("int", 123);
            Debug.Assert(success);
            success = rw.WriteUns("uns", 0xFFFF1230);
            Debug.Assert(success);
            success = rw.WriteFloat("float", 1.334455);
            Debug.Assert(success);
            success = rw.WriteHex("hex", 0xABCDEF);
            Debug.Assert(success);
            success = rw.WriteHex64("hex64", 0x123456789ABCDEF);
            Debug.Assert(success);
            success = rw.WriteBinary("foobar", binval);
            Debug.Assert(success);
            success = rw.WriteBool("bool", true);
            Debug.Assert(success);
            success = rw.WriteString("string", "fuckoff");
            Debug.Assert(success);
            success = rw.WriteString("string2", "fuck off");
            Debug.Assert(success);
            success = rw.WriteString("string3", "\"fuck off\"");
            Debug.Assert(success);
            success = rw.WriteArray("ints", ints, ints.Length);
            Debug.Assert(success);
            success = rw.WriteArray("uns", uns, uns.Length);
            Debug.Assert(success);
            success = rw.WriteArray("floats", floats, floats.Length);
            Debug.Assert(success);
            success = rw.End();
            Debug.Assert(success);

            Debug.Assert(rw.ToString() ==
                "{int=123,uns=4294906416,float=1.33446,hex=0xABCDEF,hex64=0x123456789ABCDEF,foobar=Zm9vYmFy,bool=true,string=fuckoff,string2=\"fuck off\",string3=\"\\\"fuck off\\\"\",ints={1,2,3,4},uns={1,2,3,4},floats={1.50000,1.25000,1.12500,1.06250}}");

            //Test calling End() when the buffer has been reset to continue
            //and nothing further has been added.  This occurs when the
            //underlying memory buffer was exhausted and the writer was reset
            //to continue, but there was no further data to write.
            rw.Reset();
            success = rw.Begin();
            Debug.Assert(success);
            success = rw.WriteInt("int", 123);
            Debug.Assert(success);
            rw.ResetToContinue();
            success = rw.End();
            Debug.Assert(success);

            Debug.Assert(rw.ToString() == "}");

            //Now test with compatibility flags.
            rw.Reset();
            rw.SetFlags(FLAG_DEFAULT_FLAGS);
            success = rw.Begin();
            Debug.Assert(success);
            success = rw.WriteString("string", "fuckoff");
            Debug.Assert(success);
            success = rw.WriteString("string2", "fuck off");
            Debug.Assert(success);
            success = rw.End();
            Debug.Assert(success);

            //All strings should be enclosed in double quotes.
            Debug.Assert(rw.ToString() ==
                        "{string=\"fuckoff\",string2=\"fuck off\"}");
        }
    }

    public class RsonReader
    {
        //PURPOSE
        //  Initializes the reader with a string.
        //PARAMS
        //  buf     - The string.
        //  offset  - Offset in string at which to start parsing.
        public bool Init(string buf, int offset)
        {
            this.Clear();

            Debug.Assert(null != buf);

            if(null != buf)
            {
                int nameOff = -1, nameLen = -1;
                int valOff = -1, valLen = -1;

                int i = SkipWs(buf, offset);

                if(i >= 0)
                {
                    if('{' == buf[i])
                    {
                        //This is a nameless object.

                        valOff = i;

                        i = EndOfObject(buf, valOff);
                        if(i > valOff)
                        {
                            valLen = i - valOff;
                        }
                        else
                        {
                            valOff = -1;
                        }
                    }
                    else if(!Rson.IsReserved(buf[i]))
                    {
                        nameOff = i;

                        i = EndOfToken(buf, nameOff);

                        if(i > nameOff)
                        {
                            nameLen = i - nameOff;

                            i = SkipWs(buf, i);

                            if(i >= 0 && '=' == buf[i])
                            {
                                ++i;
                                valOff = SkipWs(buf, i);

                                if(valOff >= i)
                                {
                                    if('{' == buf[valOff])
                                    {
                                        i = EndOfObject(buf, valOff);
                                    }
                                    else
                                    {
                                        i = EndOfString(buf, valOff);
                                    }

                                    if(i > valOff)
                                    {
                                        valLen = i - valOff;
                                    }
                                    else
                                    {
                                        valOff = -1;
                                    }
                                }
                            }
                            else
                            {
                                //There is no name for this value.

                                valOff = nameOff;
                                valLen = nameLen;
                                nameOff = -1;
                                nameLen = -1;
                            }
                        }
                    }
                }

                if(valOff >= 0 && valLen > 0)
                {
                    m_ValOffset = valOff;
                    m_ValLen = valLen;
                    m_Buf = buf;

                    if(nameOff >= 0 && nameLen > 0)
                    {
                        m_NameOffset = nameOff;
                        m_NameLen = nameLen;
                    }
                    else
                    {
                        m_NameOffset = m_NameLen = -1;
                    }
                }
                else
                {
                    this.Clear();
                }

                //Check that if we have a non-null name that the name has a length.
                Debug.Assert(m_NameOffset < 0 || m_NameLen > 0);
            }

            return (null != m_Buf);
        }

        //PURPOSE
        //  Initializes the reader with a string.
        //PARAMS
        //  buf     - The string.
        public bool Init(string s)
        {
            return this.Init(s, 0);
        }

        //PURPOSE
        //  Clears the reader to the post-construction state.
        public void Clear()
        {
            m_Buf = null;
            m_NameOffset = m_ValOffset = -1;
            m_NameLen = m_ValLen = -1;
        }

        //PURPOSE
        //  Retrieves the first member in the collection.
        //PARAMS
        //  rr      - On successful return will reference the first member.
        public bool GetFirstMember(ref RsonReader rr)
        {
            bool success = false;
            if(null != m_Buf
                && '{' == m_Buf[m_ValOffset]
                && m_ValLen > 0)
            {
                if(null == rr)
                {
                    rr = new RsonReader();
                }

                success = rr.Init(m_Buf, m_ValOffset + 1);
            }

            return success;
        }

        //PURPOSE
        //  Retrieves the current member's next sibling.
        //PARAMS
        //  rr      - On successful return will reference the sibling.
        public bool GetNextSibling(ref RsonReader rr)
        {
            bool success = false;
            if(null != m_Buf)
            {
                int i = SkipWs(m_Buf, m_ValOffset + m_ValLen);
                if(i >= 0)
                {
                    if(',' == m_Buf[i])
                    {
                        for(++i; i < m_Buf.Length && (',' == m_Buf[i] || Rson.IsSpace(m_Buf[i])); ++i)
                        {
                        }

                        if(i < m_Buf.Length)
                        {
                            if(null == rr)
                            {
                                rr = new RsonReader();
                            }

                            success = rr.Init(m_Buf, i);
                        }
                    }
                    else if('}' != m_Buf[i])
                    {
                        //rsonError("Unexpected character at offset:%d in \"%s\"", i, m_Buf);
                    }
                }
            }

            return success;
        }

        //PURPOSE
        //  Retrieves the named member from the collection.
        //PARAMS
        //  name    - Name of the member to retrieve.
        //  rr      - On successful return will reference the first member.
        public bool GetMember(string name, ref RsonReader rr)
        {
            bool success = false;
            RsonReader tmp = null;
            for(bool ok = this.GetFirstMember(ref tmp); ok; ok = tmp.GetNextSibling(ref tmp))
            {
                if(name.Length != tmp.m_NameLen)
                {
                    continue;
                }

                bool isMatch = true;

                for(int i = 0; i < name.Length; ++i)
                {
                    if(tmp.m_Buf[i + tmp.m_NameOffset] != name[i])
                    {
                        isMatch = false;
                        break;
                    }
                }

                if(isMatch)
                {
                    rr = tmp;
                    success = true;
                    break;
                }
            }

            return success;
        }

        //PURPOSE
        //  Retrieves the current object's name.
        //PARAMS
        //  name        - Buffer in which to copy the name.
        //  nameLen     - Number of chars available in the buffer.
        public bool GetName(out string name)
        {
            bool success = false;

            if(null != m_Buf && m_NameLen >= 0)
            {
                name = m_Buf.Substring(m_NameOffset, m_NameLen);
                success = true;
            }
            else
            {
                name = null;
            }

            return success;
        }

        //PURPOSE
        //  Retrieves the current object's value.
        //PARAMS
        //  val         - Buffer in which to copy the value.
        //  valLen      - Number of chars available in the buffer.
        public bool GetValue(out string val)
        {
            bool success = false;

            if(null != m_Buf && m_ValLen >= 0)
            {
                val = m_Buf.Substring(m_ValOffset, m_ValLen);
                success = true;
            }
            else
            {
                val = null;
            }

            return success;
        }

        //PURPOSE
        //  Retrieves the named value from the collection.
        //PARAMS
        //  name        - Name of the value to retrieve.
        //  val         - Buffer in which to copy the value.
        //  valLen      - Number of chars available in the buffer.
        public bool GetValue(string name, out string val)
        {
            val = "";
            RsonReader rr = null;
            return this.GetMember(name, ref rr) && rr.GetValue(out val);
        }

        //PURPOSE
        //  Returns the number of chars in the value string.
        public int GetValueLength()
        {
            return m_ValLen;
        }

        //PURPOSE
        //  Returns the number of chars in the named value's string.
        public int GetValueLength(string name)
        {
            RsonReader rr = null;
            return this.GetMember(name, ref rr) ? rr.GetValueLength() : -1;
        }

        //PURPOSE
        //  Returns the number of bytes that will result from interpreting
        //  the value as a base-64 encoded binary value.  I.e., the number
        //  of bytes required to retrieve the value as binary.
        public int GetBinaryValueLength()
        {
            int len = 0, offset = 0;

            this.GetB64OffsetAndLength(out offset, out len);

            return (len * 6) / 8;
        }

        //PURPOSE
        //  Returns the number of bytes that will result from interpreting
        //  the value as a base-64 encoded binary value.  I.e., the number
        //  of bytes required to retrieve the value as binary.
        public int GetBinaryValueLength(string name)
        {
            RsonReader rr = null;
            return this.GetMember(name, ref rr) ? rr.GetBinaryValueLength() : -1;
        }

        //PURPOSE
        //  Converts the current value to an integer.
        public bool AsInt(out int val)
        {
            val = -1;
            string s;
            return this.GetValue(out s) && int.TryParse(s, out val);
        }

        //PURPOSE
        //  Converts the current value to an int64.
        public bool AsInt64(out Int64 val)
        {
            val = -1;
            string s;
            return this.GetValue(out s) && Int64.TryParse(s, out val);
        }

        //PURPOSE
        //  Converts the current value to an unsigned.
        public bool AsUns(out uint val)
        {
            val = ~0u;
            string s;
            bool success = this.GetValue(out s);
            if(success)
            {
                if(m_ValLen > 2 && '0' == s[0] && 'x' == s[1])
                {
                    System.Globalization.NumberStyles style =
                        System.Globalization.NumberStyles.HexNumber;
                    success = uint.TryParse(s.Substring(2), style, null, out val);
                }
                else if(m_ValLen > 1 && '-' == s[0])
                {
                    int i = -1;
                    success = int.TryParse(s, out i);
                    if(success)
                    {
                        val = (uint) i;
                    }
                }
                else
                {
                    success = uint.TryParse(s, out val);
                }
            }

            return success;
        }

        //PURPOSE
        //  Converts the current value to an unsigned 64-bit number.
        public bool AsUns64(out UInt64 val)
        {
            val = ~0u;
            string s;
            bool success = this.GetValue(out s);
            if(success)
            {
                if(m_ValLen > 2 && '0' == s[0] && 'x' == s[1])
                {
                    System.Globalization.NumberStyles style =
                        System.Globalization.NumberStyles.HexNumber;
                    success = UInt64.TryParse(s.Substring(2), style, null, out val);
                }
                else if(m_ValLen > 1 && '-' == s[0])
                {
                    Int64 i = -1;
                    success = Int64.TryParse(s, out i);
                    if(success)
                    {
                        val = (UInt64) i;
                    }
                }
                else
                {
                    success = UInt64.TryParse(s, out val);
                }
            }

            return success;
        }

        //PURPOSE
        //  Converts the current value to a float.
        public bool AsFloat(out float val)
        {
            val = -1;
            string s;
            return this.GetValue(out s) && float.TryParse(s, out val);
        }

        //PURPOSE
        //  Converts the current value to a binary value.
        //PARAMS
        //  val     - Destination buffer
        //  len     - On success contains the number of bytes decoded.
        //            On failure, contains the number of bytes required for
        //            decoding.
        //NOTES
        //  Assumes the value is in base-64 format.
        public bool AsBinary(byte[] val, out int len)
        {
            bool success = false;

            len = 0;

            if(null != m_Buf)
            {
                int srcLen = 0, offset = 0;

                this.GetB64OffsetAndLength(out offset, out srcLen);

                len = (srcLen * 6) / 8;

                if(len > 0 && val.Length >= len)
                {
                    int src = offset;
                    int tmp = 0;
                    int accum = 0;
                    int d = 0;

                    for(int i = 0; i < srcLen; ++i)
                    {
                        if('=' == m_Buf[src+i])
                        {
                            //Ignore trailing pad characters.
                            break;
                        }

                        if(accum >= 8)
                        {
                            val[d++] = (byte) ((tmp >> (accum - 8)) & 0xFF);
                            accum -= 8;
                        }

                        tmp = (tmp << 6) | Base64CodeToValue(m_Buf[src+i]);

                        accum += 6;
                    }

                    if(accum > 0)
                    {
                        Debug.Assert(8 == accum || 10 == accum || 12 == accum);

                        val[d++] = (byte) ((tmp >> (accum - 8)) & 0xFF);
                    }

                    Debug.Assert(len == d);

                    success = true;
                }
            }

            return success;
        }

        //PURPOSE
        //  Converts the current value to a boolean.
        public bool AsBool(out bool val)
        {
            val = false;
            string s;
            return this.GetValue(out s) && bool.TryParse(s, out val);
        }

        //PURPOSE
        //  Converts the current value to a string.
        public bool AsString(out string val)
        {
            bool success = false;
            if(null != m_Buf)
            {
                if('\"' == m_Buf[m_ValOffset])
                {
                    val = m_Buf.Substring(m_ValOffset + 1, m_ValLen - 2);
                }
                else
                {
                    val = m_Buf.Substring(m_ValOffset, m_ValLen);
                }

                success = true;
            }
            else
            {
                val = "";
            }

            return success;
        }

        //PURPOSE
        //  Uses the current value to initialize an RSON reader.
        //  This is more efficient than extracting the value as
        //  a string to initialize an RSON reader.
        public bool AsReader(ref RsonReader rr)
        {
            bool success = false;
            if(null != m_Buf)
            {
                if(null == rr)
                {
                    rr = new RsonReader();
                }

                rr.Init(m_Buf, m_ValOffset);

                success = true;
            }

            return success;
        }

        //PURPOSE
        //  Converts the current value to the given type.
        bool AsValue(out int val){return this.AsInt(out val);}
        bool AsValue(out uint val){return this.AsUns(out val);}
        bool AsValue(out UInt64 val){return this.AsUns64(out val);}
        bool AsValue(out float val){return this.AsFloat(out val);}
        bool AsValue(out bool val){return this.AsBool(out val);}
        bool AsValue(out string val){return this.AsString(out val);}
        bool AsValue(ref object val, Type type)
        {
            if(typeof(int) == type)
            {
                int v = -1; if(this.AsValue(out v)){val = v;return true;}
            }
            else if(typeof(uint) == type)
            {
                uint v = ~0u; if(this.AsValue(out v)){val = v;return true;}
            }
            else if(typeof(UInt64) == type)
            {
                UInt64 v = ~0u; if(this.AsValue(out v)){val = v;return true;}
            }
            else if(typeof(float) == type)
            {
                float v = -1; if(this.AsValue(out v)){val = v;return true;}
            }
            else if(typeof(bool) == type)
            {
                bool v = false; if(this.AsValue(out v)){val = v;return true;}
            }
            else if(typeof(string) == type)
            {
                string v = ""; if(this.AsValue(out v)){val = v;return true;}
            }

            return false;
        }

        int AsArray(object values, int count)
        {
            bool success = false;
            int numRead = 0;

            Array avals = values as Array;
            if(null != avals)
            {
                Type eltype = values.GetType().GetElementType();
                RsonReader rr = null;
                success = this.GetFirstMember(ref rr);
                for(numRead = 0; numRead < count && success; ++numRead)
                {
                    object obj = null;
                    if(rr.AsValue(ref obj, eltype))
                    {
                        avals.SetValue(obj, numRead);
                    }
                    else
                    {
                        success = false;
                    }

                    if(success && !rr.GetNextSibling(ref rr))
                    {
                        break;
                    }
                }
            }
            return success ? numRead + 1 : 0;
        }

        //PURPOSE
        //  Reads the named value as an integer.
        public bool ReadInt(string name, out int val)
        {
            val = -1;
            RsonReader rr = null;
            return this.GetMember(name, ref rr) && rr.AsInt(out val);
        }

        //PURPOSE
        //  Reads the named value as a signed 64-bit value.
        public bool ReadInt64(string name, out Int64 val)
        {
            val = -1;
            RsonReader rr = null;
            return this.GetMember(name, ref rr) && rr.AsInt64(out val);
        }

        //PURPOSE
        //  Reads the named value as an unsigned.
        public bool ReadUns(string name, out uint val)
        {
            val = ~0u;
            RsonReader rr = null;
            return this.GetMember(name, ref rr) && rr.AsUns(out val);
        }

        //PURPOSE
        //  Reads the named value as an unsigned 64-bit value.
        public bool ReadUns64(string name, out UInt64 val)
        {
            val = ~0u;
            RsonReader rr = null;
            return this.GetMember(name, ref rr) && rr.AsUns64(out val);
        }

        //PURPOSE
        //  Reads the named value as a float.
        public bool ReadFloat(string name, out float val)
        {
            val = -1;
            RsonReader rr = null;
            return this.GetMember(name, ref rr) && rr.AsFloat(out val);
        }

        //PURPOSE
        //  Reads the named value as base-64 format.
        public bool ReadBinary(string name, byte[] val, out int len)
        {
            RsonReader rr = null;
            len = 0;
            return this.GetMember(name, ref rr) && rr.AsBinary(val, out len);
        }

        //PURPOSE
        //  Reads the named value as a boolean.
        public bool ReadBool(string name, out bool val)
        {
            val = false;
            RsonReader rr = null;
            return this.GetMember(name, ref rr) && rr.AsBool(out val);
        }

        //PURPOSE
        //  Reads the named value as a string.
        public bool ReadString(string name, out string val)
        {
            val = "";
            RsonReader rr = null;
            return this.GetMember(name, ref rr) && rr.AsString(out val);
        }

        //PURPOSE
        //  Reads the named value as an array.
        //PARAMS
        //  name        - Name of the item.
        //  values      - The values.
        //  count       - Number of values to read.
        //RETURNS
        //  Number of values read.
        public int ReadArray(string name, object values, int count)
        {
            RsonReader rr = null;
            return this.GetMember(name, ref rr) ? rr.AsArray(values, count) : 0;
        }

        //PURPOSE
        //  Return the string that has been built.
        public override string ToString()
        {
            int offset = m_NameOffset >= 0 ? m_NameOffset : m_ValOffset;
            int len = (m_ValOffset + m_ValLen) - offset;
            return m_Buf.Substring(offset, len);
        }

        private void GetB64OffsetAndLength(out int offset, out int len)
        {
            len = m_ValLen;
            offset = m_ValOffset;

            if('\"' == m_Buf[offset])
            {
                Debug.Assert(len > 2);
                len -= 2;  //2 * double quotes
                ++offset;
            }

            //Trim pad characters.
            while(len > 0 && '=' == m_Buf[offset+len-1])
            {
                --len;
            }
        }

        private static int SkipWs(string buf, int offset)
        {
            Debug.Assert(offset >= 0);

            int result = offset;
            if(result >= 0)
            {
                for(; result < buf.Length && Rson.IsSpace(buf[result]); ++result)
                {
                }

                if(result >= buf.Length)
                {
                    result = -1;
                }
            }
            else
            {
                result = -1;
            }

            return result;
        }

        private static int EndOfToken(string buf, int offset)
        {
            Debug.Assert(offset >= 0);

            int result = offset;
            if(result >= 0 && result < buf.Length)
            {
                for(++result; result < buf.Length; ++result)
                {
                    if(Rson.IsReserved(buf[result]) || Rson.IsSpace(buf[result]))
                    {
                        break;
                    }
                }
            }
            else
            {
                result = -1;
            }

            return result;
        }

        private static int EndOfString(string buf, int offset)
        {
            Debug.Assert(offset >= 0);

            int result = -1;

            if(offset >= 0 && offset < buf.Length)
            {
                int i = offset;

                if('\"' == buf[i])
                {
                    bool escaped = false;

                    for(++i; i < buf.Length; ++i)
                    {
                        if(escaped)
                        {
                            escaped = false;
                            continue;
                        }

                        if('\\' == buf[i])
                        {
                            escaped = true;
                        }
                        else if('\"' == buf[i])
                        {
                            result = ++i;
                            break;
                        }
                    }
                }
                else
                {
                    result = EndOfToken(buf, offset);
                }
            }

            return result;
        }

        private static int EndOfObject(string buf, int offset)
        {
            int result = -1;

            if(offset >= 0 && offset < buf.Length)
            {
                int i = offset;

                Debug.Assert('{' == buf[i]);

                if('{' == buf[i])
                {
                    int braceCount = 1;

                    for(++i; i >= offset && i < buf.Length; ++i)
                    {
                        if('\"' == buf[i])
                        {
                            i = EndOfString(buf, i) - 1;
                        }
                        else if('{' == buf[i])
                        {
                            ++braceCount;
                        }
                        else if('}' == buf[i])
                        {
                            --braceCount;
                            if(0 == braceCount)
                            {
                                result = ++i;
                                break;
                            }
                        }
                    }
                }
            }

            return result;
        }

        static int Base64CodeToValue(char code)
        {
            int val;

            if(code >= 'A' && code <= 'Z')
            {
                val = code - 'A';
            }
            else if(code >= 'a' && code <= 'z')
            {
                val = code - 'a' + 26;
            }
            else if(code >= '0' && code <= '9')
            {
                val = code - '0' + 52;
            }
            else if('+' == code)
            {
                val = 62;
            }
            else if('/' == code)
            {
                val = 63;
            }
            else
            {
                val = -1;
            }

            return val;
        }

        static int Base64CodeToValue(byte code)
        {
            return Base64CodeToValue((char) code);
        }

        private string m_Buf        = null;
        private int m_NameOffset    = -1;
        private int m_NameLen       = -1;
        private int m_ValOffset     = -1;
        private int m_ValLen        = -1;

        public static void TestMe()
        {
            string[] rsonTests =
            {
                "{vers=123,foo=bar,user={onname=\"Your Anus\",id=0x43dc7a128add9087},time=1234567}",
                //Extra double quotes
                "{foo = \"bubba\"\",bar=hubba}",
                //Misplaced }
                "{foo = \"bubba\"},bar=hubba}",
                //No {}
                "foo = \"bubba\",bar=hubba",
                //No {}
                "{b0=Zg,d0=f,b1=Zm8,d1=fo,b2=Zm9v,d2=foo,b3=Zm9vYg,d3=fooob,b4=Zm9vYmE,d4=foooba,b5=Zm9vYmFy,d5=fooobar}",
                //Arrays of int, unsigned, float
                "{ints={1,2,3,4},uns={-1,-2,-3,-4},floats={1.5,1.25,1.125,1.0625}}",
            };

            RsonReader rr = new RsonReader(), tmp = null;
            string str;
            UInt64 id;
            uint time, u;
            bool success = rr.Init(rsonTests[0]);
            Debug.Assert(success);
            success = rr.GetMember("user", ref tmp);
            Debug.Assert(success);
            success = tmp.ReadString("onname", out str);
            Debug.Assert(success && "Your Anus" == str);
            success = tmp.ReadUns64("id", out id);
            Debug.Assert(success && 0x43dc7a128add9087 == id);
            success = rr.ReadUns("time", out time);
            Debug.Assert(success && 1234567 == time);
            success = rr.ReadUns("foo", out u);
            Debug.Assert(!success);
            success = rr.ReadString("foo", out str);
            Debug.Assert(success && "bar" == str);
            success = rr.ReadUns("bar", out u);
            Debug.Assert(!success);

            success = rr.Init(rsonTests[1]);
            Debug.Assert(!success);

            success = rr.Init(rsonTests[2]);
            success = rr.ReadString("foo", out str);
            Debug.Assert(success && "bubba" == str);
            success = rr.ReadString("bar", out str);
            Debug.Assert(!success);

            success = rr.Init(rsonTests[3]);
            success = rr.ReadString("foo", out str);
            Debug.Assert(!success);
            success = rr.ReadString("bar", out str);
            Debug.Assert(!success);
            success = rr.AsString(out str);
            Debug.Assert(success && "bubba" == str);

            byte[] bv = new byte[128];
            int len;
            success = rr.Init(rsonTests[4]);
            success = rr.ReadBinary("b0", bv, out len);
            Debug.Assert(success && 1 == len && 'f' == bv[0]);
            success = rr.ReadBinary("b1", bv, out len);
            Debug.Assert(success && 2 == len && 'f' == bv[0] && 'o' == bv[1]);
            success = rr.ReadBinary("b2", bv, out len);
            Debug.Assert(success && 3 == len && 'f' == bv[0] && 'o' == bv[1] && 'o' == bv[2]);
            success = rr.ReadBinary("b3", bv, out len);
            Debug.Assert(success && 4 == len && 'f' == bv[0] && 'o' == bv[1] && 'o' == bv[2] && 'b' == bv[3]);
            success = rr.ReadBinary("b4", bv, out len);
            Debug.Assert(success && 5 == len && 'f' == bv[0] && 'o' == bv[1] && 'o' == bv[2] && 'b' == bv[3] && 'a' == bv[4]);
            success = rr.ReadBinary("b5", bv, out len);
            Debug.Assert(success && 6 == len && 'f' == bv[0] && 'o' == bv[1] && 'o' == bv[2] && 'b' == bv[3] && 'a' == bv[4] && 'r' == bv[5]);

            int[] ints = new int[4];
            uint[] uns = new uint[4];
            float[] floats = new float[4];

            rr.Init(rsonTests[5], 0);
            Debug.Assert(4 == rr.ReadArray("ints", ints, 4));
            Debug.Assert(1==ints[0] && 2==ints[1] && 3==ints[2] && 4==ints[3]);
            Debug.Assert(4 == rr.ReadArray("uns", uns, 4));
            unchecked
            {
                Debug.Assert((uint)-1==uns[0] && (uint)-2==uns[1] && (uint)-3==uns[2] && (uint)-4==uns[3]);
            }
            Debug.Assert(4 == rr.ReadArray("floats", floats, 4));
            Debug.Assert(1.5f==floats[0] && 1.25f==floats[1] && 1.125f==floats[2] && 1.0625f==floats[3]);
        }
    }
}
