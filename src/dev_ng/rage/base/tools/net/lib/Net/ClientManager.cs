using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Diagnostics;
using System.Net;
using System.Net.Sockets;
using System.Web;
using System.Web.Caching;

using Rockstar.Rockon.Diag;

namespace Rockstar.Rockon.Net
{
    //PURPOSE
    //  Manages a pool of objects.
    public class Pool<T, T_KEY> where T:Nullable, new()
    {
        private readonly object m_Lock = new object();

        private T[] m_Heap;
        private List<T> m_Pool = new List<T>();
        private Dictionary<T_KEY, T> m_Dict = new Dictionary<T_KEY, T>();

        public Pool()
        {
        }

        public Error Init(uint size)
        {
            Context ctx = new NetContext();

            try
            {
                m_Heap = new T[size];
                for (uint i = 0; i < size; ++i)
                {
                    m_Heap[i] = new T();
                    m_Pool.Add(m_Heap[i]);
                }

                return ctx.Success3();
            }
            catch (Exception ex)
            {
                return ctx.Error(ex);
            }
        }

        public Dictionary<T_KEY, T> GetObjects()
        {
            return m_Dict;
        }

        public void Shutdown()
        {
            lock (m_Lock)
            {
                m_Dict.Clear();
                m_Pool.Clear();
                m_Heap = null;
            }
        }

        public int Count
        {
            get { return m_Heap.Length - m_Pool.Count; }
        }

        public int Max
        {
            get { return m_Heap.Length; }
        }

        //PURPOSE
        //  Handles new connections accepted on our listen socket.
        private T Alloc(T_KEY key)
        {
            Context ctx = new NetContext();

            lock (m_Lock)
            {
                if (m_Pool.Count > 0)
                {
                    T t = m_Pool[0];
                    m_Pool.RemoveAt(0);
                    m_Dict.Add(key, t);
                }
                else
                {                        
                    return null;
                }
            }

            return null;
        }

        private void RemoveClient(T client)
        {
            lock (m_Lock)
            {
                Context ctx = new NetContext();

                //TODO: Call the app-level remove delegate

                m_Pool.Add(client);
                ctx.Debug2("Removed client {0} ({1} clients now)", client.Id, this.NumClients);
                client.Shutdown();
            }
        }
    }
}

