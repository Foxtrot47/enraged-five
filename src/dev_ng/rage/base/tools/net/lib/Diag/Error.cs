using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Rockstar.Diag
{
    //PURPOSE
    //  Describes an error by code and (optionally) a descriptive message.  
    //  The code is mandatory, as it is the primary way callers identify the error type.  
    //  The message is intended only for debugging, and should never be displayed to users.
    public class Error
    {
        [XmlElementAttribute(ElementName = "Code")]
        public string Code = null;

        [XmlElementAttribute(ElementName = "Msg")]
        public string Msg = null;

        [XmlIgnore]
        //Used only by Contexts to know whether to prefix the Error's msg
        //in their Failed() method.
        public bool m_ContextSet = false;

        public Error()
        {
            Set(ErrorCode.UnknownError, null);
        }

        public Error(object code)
        {
            Set(code, null);
        }

        public Error(object code, string msg)
        {
            Set(code, msg);
        }

        public Error(Exception ex)
        {
            Set(ErrorCode.Exception, ex.ToString());
        }

        private void Set(object code, string msg)
        {
            if (code == null)
            {
                throw (new ArgumentNullException("Parameter \"code\" cannot be null."));
            }

            Code = code.ToString();
            Msg = msg;
        }

        public override string ToString()
        {
            return string.Format("ERROR({0}) {1}", Code, (Msg != null) ? Msg : "");
        }
    }
}
