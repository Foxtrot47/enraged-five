using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Rockstar.Diag
{
    //PURPOSE
    //  Result and TResult<> are standard structures used for returning results
    //  from web methods.  They indicate the overall success/failure of the call,
    //  and optional have an Error description (on failure) or a Result element
    //  (on success) that can contain any data to return.
    //
    //  Returning this structure is preferable to returning simple types or using
    //  out parameters for several reasons:
    //  1. The SG expects results in this format, so that it can translate them
    //     to a format the client expects.
    //  2. Using out parameters makes testing more difficult, since they cannot
    //     be entered on the auto-generated web service pages.
    //  3. It's a clean way to combine all the info a caller needs in one return value.
    //
    //  It is not recommended that these be used for returning results from non-
    //  web methods, as it is generally more unwieldy than returning an Error to 
    //  indicate success/failure, and using params to return results.

    public class Result
    {
        [XmlElementAttribute(ElementName = "Success")]
        public bool Success = true;

        [XmlElementAttribute(ElementName = "Error")]
        public Error Error = null;

        public Result()
        {
            SetSucceeded();
        }

        public Result SetSucceeded()
        {
            Success = true;
            Error = null;
            return this;
        }

        public Result SetFailed()
        {
            return SetFailed(null);
        }

        public Result SetFailed(Error err)
        {
            Success = (null == err);
            Error = err;
            return this;
        }
    }

    //PURPOSE
    //  Template for result that can carry data on success.
    public class TResult<T>
    {
        [XmlElementAttribute(ElementName = "Success")]
        public bool Success = true;

        [XmlElementAttribute(ElementName = "Error")]
        public Error Error = null;

        [XmlElementAttribute(ElementName = "Result")]
        public T Result = default(T);

        public TResult()
        {
            this.SetSucceeded(default(T));
        }

        public TResult<T> SetSucceeded(T result)
        {
            Success = true;
            Result = result;
            Error = null;
            
            return this;
        }

        public TResult<T> SetFailed()
        {
            return SetFailed(null);
        }

        public TResult<T> SetFailed(Error err)
        {
            Success = false;
            Result = default(T);
            Error = err;

            return this;
        }
    }
}
