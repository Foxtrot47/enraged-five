﻿using System;

namespace Rockstar.Np
{
    //PURPOSE
    //  Describes an NP cipher we keep in the global DB.
    //  SGs query the global DB at startup for any ciphers they are
    //  configured to use.
    public class CipherInfo
    {
        public string Name;
        public byte[] CipherData;
        public DateTime CreateDateTime;
    }
}
