using System;
using Rockstar.Diag;

namespace Rockstar.Np
{
    public class NpChannel
    {
        private static Channel m_Channel = new Channel("Np");

        public static Channel Channel
        {
            get { return m_Channel; }
        }
    }

    public class NpContext
    {
        public static Context Create()
        {
            return new Context(NpChannel.Channel);
        }

        public static Context Create(string desc)
        {
            return new Context(NpChannel.Channel, desc);
        }

        public static Context Create(string format, params object[] args)
        {
            return new Context(NpChannel.Channel, format, args);
        }
    }
}
