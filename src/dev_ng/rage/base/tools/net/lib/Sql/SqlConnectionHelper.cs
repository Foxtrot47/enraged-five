using System;
using System.Configuration;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;

namespace Rockstar.Sql
{
    //PURPOSE
    //  Convenience class for building parameter list for a command.
    public class SqlCmdParams
    {
        public List<KeyValuePair<string, object>> m_ParamList { get; private set; }

        public SqlCmdParams()
        {
            m_ParamList = new List<KeyValuePair<string,object>>();
        }

        public void Clear()
        {
            m_ParamList.Clear();
        }

        public void Add(string name, object val)
        {
            if (!name.StartsWith("@"))
            {
                name = "@" + name;
            }

            m_ParamList.Add(new KeyValuePair<string,object>(name, val));
        }
    }

    //PURPOSE
    //  Convenience class for running SQL commands.
    public class SqlConnectionHelper : IDisposable
    {
        private string m_ConnectionStr;     //Connection string used for all vendors.
        private SqlConnection m_Con;        //Connection object used by SQL Server vendor.
        private bool m_Disposed = false;    

        public SqlConnectionHelper(string connectionStr)
        {
            m_ConnectionStr = connectionStr;
        }

        public void Dispose()
        {
            Dispose(true);

            // Use SupressFinalize in case a subclass of this type implements a finalizer.
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            // If you need thread safety, use a lock around these 
            // operations, as well as in your methods that use the resource.
            if (!m_Disposed)
            {
                if (disposing)
                {
                    if (m_Con != null)
                    {
                        m_Con.Dispose();
                    }
                }

                // Indicate that the instance has been disposed.
                m_Con = null;
                m_Disposed = true;
            }
        }

        //PURPOSE
        //  Opens a connection to a DB.  If a connection was already open, 
        //  it is closed first. Returns true on success.
        public bool Open()
        {
            bool success = true;

            if (IsConnected())
            {
                Close();
            }

            if (!SqlHelper.UseMySql)
            {
                m_Con = new SqlConnection(m_ConnectionStr);

                try
                {
                    m_Con.Open();
                }
                catch (Exception ex)
                {
                    success = false;
                    Close();
                    throw (ex);
                }
            }

            return success;
        }

        //PURPOSE
        //  Closes DB connection.  This is safe to call even if not connected.
        public void Close()
        {
            m_ConnectionStr = "";

            if (m_Con != null)
            {
                m_Con.Close();
                m_Con = null;
            }
        }

        //PURPOSE
        //  Returns true if instance is currently connected to a server.
        public bool IsConnected()
        {
            if (SqlHelper.UseMySql)
            {
                return (m_ConnectionStr.Length != 0);
            }
            else
            {
                return (m_Con != null);
            }
        }

        //PURPOSE
        //  Returns true if DB exists on server.
        public bool CheckDatabaseExists(string dbName)
        {
            string s = string.Format(@"SELECT COUNT(name) FROM sys.databases WHERE name = '{0}';", dbName);

            object result = AdHocExecuteScalar(s);

            return (1 == (int)result);
        }

        //PURPOSE
        //  Sets the connection to USE the specified database, if one wasn't specified 
        //  in the connection string.
        public bool UseDatabase(string dbName)
        {
            return (0 >= AdHocExecuteNonQuery(string.Format("USE [{0}];", dbName)));
        }

        //PURPOSE
        //  Returns true if the given table exists.
        //NOTES
        //  Either the connection string must have specified a database, or
        //  UseDatabase() must be called before being able to check the table.
        public bool CheckTableExists(string tableName)
        {
            string baseTableName = tableName;
            int dboIdx = baseTableName.IndexOf("dbo.");
            if (0 == dboIdx)
            {
                baseTableName = baseTableName.Substring(4);
            }

            string s = @"SELECT COUNT(table_name)
                        FROM information_schema.tables
                        WHERE table_name = '{0}';";
            s = String.Format(s, baseTableName);

            object result = AdHocExecuteScalar(s);

            return (1 == (int)result);
        }

        //PURPOSE
        //  Returns a DbDataReader instance containing the results of the SQL command.
        //NOTE
        //  Because SQL server incurs a compile hit for commands with different values,
        //  this method should only be used for static or very infrequently called 
        //  commands.  For command strings with variables that frequently change, use
        //  ExecuterReader().
        //PARAMS
        //  cmdText     - SQL command string
        public DbDataReader AdHocExecuteReader(string cmdText)
        {
            return ExecuteReader(cmdText, null);
        }

        //PURPOSE
        //  Returns a DbDataReader instance containing the results of the parameterized
        //  SQL command.  This is the preferred method if you have variables in the cmd
        //  that will change often.
        //PARAMS
        //  cmdText     - SQL command string
        //  paramList   - Parameters to SQL command
        public DbDataReader ExecuteReader(string cmdText, SqlCmdParams cmdParams)
        {
            DbDataReader dataReader = null;

            if (!IsConnected())
            {
                throw (new Exception("Not connected to a database server"));
            }
            else
            {
                if (SqlHelper.UseMySql)
                {
                    dataReader = MySqlHelper.ExecuteReader(m_ConnectionStr, cmdText);
                }
                else
                {
                    SqlCommand cmd = CreateSqlCommand(cmdText, cmdParams);
                    dataReader = cmd.ExecuteReader();
                }
            }

            return dataReader;
        }

        //PURPOSE
        //  Returns the result of the sql command.  This will be -1 on any statement
        //  other than UPDATE, INSERT, or DELETE, and the number of rows affected 
        //  by the command otherwise.
        //  Errors should be detected through exception handling.
        //NOTE
        //  Because SQL server incurs a compile hit for commands with different values,
        //  this method should only be used for static or very infrequently called 
        //  commands.  For command strings with variables that frequently change, use
        //  ExecuteNonQuery().
        //PARAMS
        //  cmdText     - SQL command string
        public int AdHocExecuteNonQuery(string cmdText)
        {
            return ExecuteNonQuery(cmdText, null);
        }

        //PURPOSE
        //  Returns the result of the sql command.  This will be -1 on any statement
        //  other than UPDATE, INSERT, or DELETE, and the number of rows affected 
        //  by the command otherwise.
        //  Errors should be detected through exception handling.
        //PARAMS
        //  cmdText         - SQL command string
        //  cmdParams       - Parameters to SQL command
        public int ExecuteNonQuery(string cmdText, SqlCmdParams cmdParams)
        {
            if (!IsConnected())
            {
                throw (new Exception("Not connected to a database server"));
            }
            else
            {
                if (SqlHelper.UseMySql)
                {
                    throw (new Exception("Not supported with MySql; ask RAGE to add support"));
                }
                else
                {
                    SqlCommand cmd = CreateSqlCommand(cmdText, cmdParams);

                    return cmd.ExecuteNonQuery();
                }
            }
        }

        //PURPOSE
        //  Executes a SQL command that results in a scalar value.
        //  The type of the result depends on the command.  It could be int,
        //  long, float, double, etc.
        //NOTE
        //  Because SQL server incurs a compile hit for commands with different values,
        //  this method should only be used for static or very infrequently called 
        //  commands.  For command strings with variables that frequently change, use
        //  ExecuteScalar().
        //PARAMS
        //  cmdText     - SQL command string
        public object AdHocExecuteScalar(string cmdText)
        {
            return ExecuteScalar(cmdText, null);
        }

        //PURPOSE
        //  Executes a SQL command that results in a scalar value.
        //  The type of the result depends on the command.  It could be int,
        //  long, float, double, etc.
        //PARAMS
        //  cmdText         - SQL command string
        //  cmdParams       - Parameters to command
        public object ExecuteScalar(string cmdText, SqlCmdParams cmdParams)
        {
            if (!IsConnected())
            {
                throw (new Exception("Not connected to a database server"));
            }
            else
            {
                if (SqlHelper.UseMySql)
                {
                    throw (new Exception("Not supported with MySql; ask RAGE to add support"));
                }
                else
                {
                    SqlCommand cmd = CreateSqlCommand(cmdText, cmdParams);

                    return cmd.ExecuteScalar();
                }
            }
        }

        //PURPOSE
        //  Returns a DataSet instance containing the results of the sql command.
        //PARAMS
        //  cmdText         - SQL command string
        public DataSet ExecuteDataset(string cmdText)
        {
            DataSet ds = null;

            if (!IsConnected())
            {
                throw (new Exception("Not connected to a database server"));
            }
            else
            {
                if (SqlHelper.UseMySql)
                {
                    ds = MySqlHelper.ExecuteDataset(m_ConnectionStr, cmdText);
                }
                else
                {
                    SqlDataAdapter da = new SqlDataAdapter(cmdText, m_Con);
                    ds = new DataSet();
                    da.Fill(ds);
                }
            }

            return ds;
        }

        //PURPOSE
        //  Helper for creating SqlCommands from a string and parameter list.
        private SqlCommand CreateSqlCommand(string s, SqlCmdParams cmdParams)
        {
            SqlCommand cmd = new SqlCommand(s, m_Con);

            if ((cmd != null) && (cmdParams != null))
            {
                foreach (KeyValuePair<string, object> kvp in cmdParams.m_ParamList)
                {
                    object val;

                    //Sql Server doesn't handle unsigned types.

                    if(kvp.Value is uint || kvp.Value is UInt32)
                    {
                        val = Convert.ToInt32(kvp.Value);
                    }
                    else if(kvp.Value is UInt64)
                    {
                        val = Convert.ToInt64(kvp.Value);
                    }
                    else
                    {
                        val = kvp.Value;
                    }

                    cmd.Parameters.Add(new SqlParameter(kvp.Key, val));
                }
            }

            return cmd;
        }
    }
}
