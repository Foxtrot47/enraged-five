using System;
using System.Data;
using System.Data.Common;
using System.Text;

namespace Rockstar.Sql
{
    //PURPOSE
    //  This is a temporary holding place for miscellaneous SQL helper code
    //  to experiment with.

    //PURPOSE
    //  Used to help construct and (more often) parse connection strings.
    public class SqlConnectionStringBuilder
    {
        private DbConnectionStringBuilder m_Sb = new DbConnectionStringBuilder();

        public SqlConnectionStringBuilder()
        {
        }

        public SqlConnectionStringBuilder(string connectionString)
        {
            m_Sb.ConnectionString = connectionString;
        }

        public string ConnectionString
        {
            get { return m_Sb.ConnectionString; }
            set { m_Sb.ConnectionString = value; }
        }

        public string Server { get { return GetKey("server"); } }
        public string Database 
        { 
            get 
            {
                string s = GetKey("database");
                if(s == null) { s = GetKey("initial catalog"); }
                return s;
            } 
        }
        
        public string UserId { get { return GetKey("user id"); } }

        public string Password { get { return GetKey("password"); } }

        public string GetKey(string name)
        {
            object value;
            if (m_Sb.TryGetValue(name.ToLower(), out value))
            {
                return value.ToString();
            }
            return null;
        }
    }

    //PURPOSE
    //  Helps build filter strings.  It can be tedious trying to piece
    //  together filters with multiple parts that may or may not be present.
    //  This helps simplify the process.
    public class SqlFilterStringBuilder
    {
        private StringBuilder m_Sb = new StringBuilder();

        public void AddCondition(string condition)
        {
            if (m_Sb.Length == 0)
            {
                m_Sb.Append(" WHERE ");
            }
            else
            {
                m_Sb.Append(" AND ");
            }

            m_Sb.Append(condition);
        }

        public override string ToString()
        {
            return m_Sb.ToString();
        }
    }
}
