using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Xml;

namespace Rockstar.Gamespy.Atlas
{
    public enum AtlasVersion
    {
        Invalid = 0
    }

    public enum AtlasId
    {
        Invalid = 0
    }

    //PURPOSE
    //  Data types for Atlas keys and stats.
    //  Do NOT change the spelling of these in any way,
    //  as Atlas expects the lowercase versions of these in the XML.
    public enum AtlasType
    {
        Invalid,
        Byte,
        Int,
        Int64,
        Float,
        Short,
        String
    }

    //PURPOSE
    //  Category types for Atlas stats.
    //  Do NOT change the case or spelling of these in any way, 
    //  as they must exactly match what Atlas expects in XML.
    public enum AtlasStatCategory
    {
        Invalid,
        Static,
        Player
    }

    //PURPOSE
    //  Operations that Atlas rules can perform.
    //  Do NOT change the case or spelling of these in any way, 
    //  as they must exactly match what Atlas expects in XML.
    public enum AtlasOperation
    {
        Invalid,
        Add,
        ArenaPlayerLadderUpdate,
        ArenaTeamLadderUpdate,
        Custom,
        DesyncIncrement,
        DisconnectIncrement,
        Divide,
        Increment,
        LossIncrement,
        LossStreak,
        Maximum,
        Minimum,
        Multiply,
        Ratio,
        Replace,
        Subtract,
        StandardELO,
        TieIncrement,
        TieStreak,
        WinIncrement,
        WinStreak
    }

    public interface IAtlasItem
    {
        int GetId();
        void SetId(int v);
        string GetName();
        bool IsValid();
        bool EquivalentTo(IAtlasItem other);
    }

    //PURPOSE
    //  Helper generic for key/stat/rule sets, which have dual keys (ID and name).
    [Serializable]
    public class AtlasItemSet<T> where T : IAtlasItem
    {
        private SortedList ById = new SortedList();
        private SortedList ByName = new SortedList();

        public int HighestId { get; private set; }
        public int Count { get { return ById.Count; } }

        public AtlasItemSet()
        {
            HighestId = (int)AtlasId.Invalid;
        }

        public T Get(int id)
        {
            if (ById.ContainsKey(id)) return (T)ById[id];
            return default(T);
        }

        public T Get(string name)
        {
            if (ByName.ContainsKey(name)) return (T)ByName[name];
            return default(T);
        }

        public T GetSortedById(int index)
        {
            return (T)ById.GetByIndex(index);
        }

        public T GetSortedByName(int index)
        {
            return (T)ByName.GetByIndex(index);
        }

        //Adds a new item to the set.  If the item already existed, then it 
        //is only updated if the new value is functionally equivalent to the
        //old.  (This primarily allows us to update descriptions and other
        //non-essential attributes.)
        public bool Add(T t)
        {
            if (t == null || !t.IsValid())
            {
                Trace.WriteLine("Null or invalid object");
                return false;
            }

            bool add = false;
            T idMatch = Get(t.GetId());
            T nameMatch = Get(t.GetName());

            if (idMatch.IsValid() && nameMatch.IsValid())
            {
                //We already have this item.  Make sure it doesn't conflict
                //with what we already have.
                add = t.EquivalentTo(idMatch);
            }
            else if (!idMatch.IsValid() && !nameMatch.IsValid())
            {
                //New item, add it.
                add = true;
            }
            else
            {
                //Only one matched.  This is invalid, both must be unique.
                Trace.WriteLine("ID or name mismatched existing entry");
                if (idMatch != null)
                {
                    Trace.WriteLine("Existing: " + idMatch);
                }
                else
                {
                    Trace.WriteLine("Existing: " + nameMatch);
                }
                Trace.WriteLine("New:      " + t);

                add = false;
            }

            if (add)
            {
                ById.Add(t.GetId(), t);
                ByName.Add(t.GetName(), t);

                if (t.GetId() > HighestId)
                {
                    HighestId = t.GetId();
                }
            }

            return add;
        }

        //Adds items in one set to this one.  Items that match the name of 
        //existing items are given the same IDs.  New items are given IDs
        //starting at the current highest ID.
        public bool Combine(AtlasItemSet<T> newSet)
        {
            foreach (T t in newSet.ById)
            {
                T cur = Get(t.GetName());

                if (cur.IsValid())
                {
                    t.SetId(cur.GetId());
                }
                else
                {
                    t.SetId(HighestId + 1);
                }

                if (!Add(t)) return false;
            }

            return true;
        }
    }

    public struct AtlasKey : IAtlasItem
    {
        public int Id;
        public string Name;
        public string Description;
        public AtlasType Type;

        public int GetId() { return Id; }
        public void SetId(int v) { Id = v; }
        public string GetName() { return Name; }

        public override string ToString()
        {
            return string.Format("KEY[id={0}  name={1}  type={2}  desc={3}]", Id, Name, Type, Description);
        }

        public bool IsValid()
        {
            return Id != (int)AtlasId.Invalid 
                && !string.IsNullOrEmpty(Name)
                && Type != AtlasType.Invalid;
        }

        public bool EquivalentTo(IAtlasItem other)
        {
            if (!IsValid()) return false;

            AtlasKey t = (AtlasKey)other;
            return (Id == t.Id) && (Name == t.Name) && (Type == t.Type);
        }

        public void Write(XmlWriter writer)
        {
            writer.WriteStartElement("Key");
            writer.WriteAttributeString("name", Name);
            writer.WriteAttributeString("id", Id.ToString());
            writer.WriteAttributeString("type", Type.ToString().ToLower());
            writer.WriteAttributeString("desc", Description);
            writer.WriteEndElement();
        }

        public static AtlasKey Read(XmlElement el)
        {
            AtlasKey t;

            t.Id = int.Parse(el.GetAttribute("id"));
            t.Name = el.GetAttribute("name");
            t.Type = (AtlasType)Enum.Parse(typeof(AtlasType), el.GetAttribute("type"), true);
            t.Description = el.GetAttribute("desc");

            return t;
        }
   }

    public struct AtlasStat : IAtlasItem
    {
        public int Id;
        public string Name;
        public string Description;
        public AtlasType Type;
        public AtlasStatCategory Category;

        public int GetId() { return Id; }
        public void SetId(int v) { Id = v; }
        public string GetName() { return Name; }

        public override string ToString()
        {
            return string.Format("STAT[id={0}  name={1}  type={2}  cat={3}  desc={4}]", Id, Name, Type, Category, Description);
        }

        public bool IsValid()
        {
            return Id != (int)AtlasId.Invalid
                && !string.IsNullOrEmpty(Name)
                && Type != AtlasType.Invalid
                && Category != AtlasStatCategory.Invalid;
        }

        public bool EquivalentTo(IAtlasItem other)
        {
            if (!IsValid()) return false;

            AtlasStat t = (AtlasStat)other;
            return (Id == t.Id) 
                && (Name == t.Name) 
                && (Type == t.Type) 
                && (Category == t.Category);
        }

        public void Write(XmlWriter writer)
        {
            writer.WriteStartElement("Stat");
            writer.WriteAttributeString("name", Name);
            writer.WriteAttributeString("id", Id.ToString());
            writer.WriteAttributeString("category", Category.ToString());
            writer.WriteAttributeString("type", Type.ToString().ToLower());
            writer.WriteAttributeString("desc", Description);
            writer.WriteEndElement();
        }

        public static AtlasStat Read(XmlElement el)
        {
            AtlasStat t;

            t.Id = int.Parse(el.GetAttribute("id"));
            t.Name = el.GetAttribute("name");
            t.Type = (AtlasType)Enum.Parse(typeof(AtlasType), el.GetAttribute("type"), true);
            t.Category = (AtlasStatCategory)Enum.Parse(typeof(AtlasStatCategory), el.GetAttribute("category"), true);
            t.Description = el.GetAttribute("desc");

            return t;
        }
    }

    public struct AtlasRule : IAtlasItem
    {
        public int Id;
        public string Name;
        public string Description;
        public AtlasOperation Operation;
        public string CustomOperationName;
        public List<string> InputKeys;
        public List<string> InputStats;
        public string OutputStat;

        public int GetId() { return Id; }
        public void SetId(int v) { Id = v; }
        public string GetName() { return Name; }

        public override string ToString()
        {
            if (Operation == AtlasOperation.Custom)
            {
                return string.Format("RULE[id={0}  name={1}  op={2}  customOpName={3}  numInKeys={4}  numInStats={5}  outStat={6}  desc={7}]",
                                     Id, Name, Operation, CustomOperationName, InputKeys.Count, InputStats.Count, OutputStat, Description);
            }
            else
            {
                return string.Format("RULE[id={0}  name={1}  op={2}  numInKeys={3}  numInStats={4}  outStat={5}  desc={6}]",
                                     Id, Name, Operation, InputKeys.Count, InputStats.Count, OutputStat, Description);
            }
        }

        public bool IsValid()
        {
            return Id != (int)AtlasId.Invalid
                && !string.IsNullOrEmpty(Name)
                && Operation != AtlasOperation.Invalid
                && (Operation != AtlasOperation.Custom || !string.IsNullOrEmpty(CustomOperationName))
                && (InputKeys != null)
                && InputKeys.Count > 0
                && (Operation == AtlasOperation.Custom || !string.IsNullOrEmpty(OutputStat));
        }

        public bool EquivalentTo(IAtlasItem other)
        {
            if (!IsValid()) return false;

            AtlasRule t = (AtlasRule)other;
             if(!((Id == t.Id) 
                 && (Name == t.Name)
                 && (Operation == t.Operation)
                 && (CustomOperationName == t.CustomOperationName)
                 && (OutputStat == t.OutputStat)
                 && (InputKeys.Count == t.InputKeys.Count)))
            {
                return false;
            }

            if(InputStats != null && t.InputStats == null)
            {
                return false;
            }
            else if(InputStats == null && t.InputStats != null)
            {
                return false;
            }
            else if(InputStats != null 
                && t.InputStats != null
                && InputStats.Count != t.InputStats.Count)
            {
                return false;
            }

            for (int i = 0; i < InputKeys.Count; i++)
            {
                if (InputKeys[i] != t.InputKeys[i]) return false;
            }

            for (int i = 0; i < InputStats.Count; i++)
            {
                if (InputStats[i] != t.InputStats[i]) return false;
            }

            return true;
        }

        public void Write(XmlWriter writer)
        {
            writer.WriteStartElement("Rule");
            writer.WriteAttributeString("name", Name);
            writer.WriteAttributeString("id", Id.ToString());

            if (Operation == AtlasOperation.Custom)
            {
                writer.WriteAttributeString("operation", CustomOperationName);
            }
            else
            {
                writer.WriteAttributeString("operation", Operation.ToString());
            }

            writer.WriteAttributeString("desc", Description);

            foreach (string s in InputKeys)
            {
                writer.WriteStartElement("InputKey");
                writer.WriteAttributeString("name", s);
                writer.WriteEndElement();
            }

            if (InputStats != null)
            {
                foreach (string s in InputStats)
                {
                    writer.WriteStartElement("InputStat");
                    writer.WriteAttributeString("name", s);
                    writer.WriteEndElement();
                }
            }

            writer.WriteStartElement("OutputStat");
            writer.WriteAttributeString("name", OutputStat);
            writer.WriteEndElement();

            writer.WriteEndElement();
        }

        public static AtlasRule Read(XmlElement el)
        {
            AtlasRule t = default(AtlasRule);

            t.Id = int.Parse(el.GetAttribute("id"));
            t.Name = el.GetAttribute("name");

            try
            {
                t.Operation = (AtlasOperation)Enum.Parse(typeof(AtlasOperation), el.GetAttribute("operation"), true);
            }
            catch
            {
                t.Operation = AtlasOperation.Custom;
                t.CustomOperationName = el.GetAttribute("operation");
            }
            
            t.Description = el.GetAttribute("desc");
            t.InputKeys = new List<string>();
            t.InputStats = new List<string>();

            for (XmlNode node = el.FirstChild; null != node; node = node.NextSibling)
            {
                XmlElement child = node as XmlElement;

                if (child.Name == "InputKey")
                {
                    t.InputKeys.Add(child.GetAttribute("name"));
                }
                else if (child.Name == "InputStat")
                {
                    t.InputStats.Add(child.GetAttribute("name"));
                }
                else if (child.Name == "OutputStat")
                {
                    t.OutputStat = child.GetAttribute("name");
                }
            }

            return t;
        }
    }

    //PURPOSE
    //  Utility functions.
    public class AtlasUtil
    {
        public static T DeepClone<T>(T obj)
        {
            using (var ms = new MemoryStream())
            {
                var formatter = new BinaryFormatter();
                formatter.Serialize(ms, obj);
                ms.Position = 0;

                return (T)formatter.Deserialize(ms);
            }
        }
    }

    //PURPOSE
    //  Completely describes an Atlas ruleset.
    //IMPORTANT
    //  You MUST use Combine() when going from one version to another.
    //  Combine ensures that the new version is a superset of the old
    //  version, which is necessary to work with stats committed with 
    //  the old version.
    [Serializable]
    public class AtlasRuleset
    {
        public string GameName;
        public int Version = (int)AtlasVersion.Invalid;

        public AtlasItemSet<AtlasKey> Keys { get; private set; }
        public AtlasItemSet<AtlasStat> Stats { get; private set; }
        public AtlasItemSet<AtlasRule> Rules { get; private set; }

        public AtlasRuleset()
        {
            Keys = new AtlasItemSet<AtlasKey>();
            Stats = new AtlasItemSet<AtlasStat>();
            Rules = new AtlasItemSet<AtlasRule>();
        }

        //PURPOSE
        //  Combines and old and new ruleset.  The new ruleset must be
        //  a superset of the old;  this is so we don't lose stats when
        //  adding new stats, like when leaderboards are added by DLC.
        public static bool Combine(AtlasRuleset oldRuleset,
                                   AtlasRuleset newRuleset,
                                   out AtlasRuleset finalRuleset)
        {
            finalRuleset = new AtlasRuleset();

            if (oldRuleset.GameName != newRuleset.GameName)
            {
                //Must be same game.
                return false;
            }

            if (oldRuleset.Version >= newRuleset.Version)
            {
                //Must be going from older to newer version.
                return false;
            }

            //Start with the old ruleset.
            finalRuleset = AtlasUtil.DeepClone(oldRuleset);

            //Now add the new ruleset.  If any items have the same IDs as in the old
            //ruleset but are functionally different, then the combine will fail.
            if (!finalRuleset.Keys.Combine(newRuleset.Keys)) return false;
            if (!finalRuleset.Stats.Combine(newRuleset.Stats)) return false;
            if (!finalRuleset.Rules.Combine(newRuleset.Rules)) return false;

            return true;
        }

        //PURPOSE
        //  Loads a ruleset from an XML file.
        public static AtlasRuleset Load(string filename)
        {
            FileInfo fileInfo = new FileInfo(filename);
            string uri = "file://" + fileInfo.FullName;

            XmlDocument doc = new XmlDocument();
            doc.Load(uri);

            XmlElement rulesetEl = doc.DocumentElement["Ruleset"];

            AtlasRuleset ruleset = new AtlasRuleset();
            ruleset.GameName = rulesetEl.GetAttribute("gameName");
            ruleset.Version = int.Parse(rulesetEl.GetAttribute("version"));

            XmlNode node = rulesetEl.FirstChild;
            for (; null != node; node = node.NextSibling)
            {
                XmlElement el = node as XmlElement;

                if (el != null)
                {
                    if (el.Name == "Key")
                    {
                        if (!ruleset.Keys.Add(AtlasKey.Read(el)))
                        {
                            return null;
                        }
                    }
                    else if (el.Name == "Stat")
                    {
                        if (!ruleset.Stats.Add(AtlasStat.Read(el)))
                        {
                            return null;
                        }
                    }
                    else if (el.Name == "Rule")
                    {
                        if (!ruleset.Rules.Add(AtlasRule.Read(el)))
                        {
                            return null;
                        }
                    }
                }
            }

            return ruleset;
        }

        //PURPOSE
        //  Saves a ruleset to an XML file.
        public bool Save(string filename, string credits)
        {
            FileInfo fi = new FileInfo(filename);
            if (fi.Exists && fi.IsReadOnly)
            {
                return false;
            }

            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;
            settings.IndentChars = "\t";
            settings.NewLineChars = "\n";
            settings.Encoding = Encoding.Unicode; //IMPORTANT: Atlas will reject any other encoding.

            XmlWriter writer = XmlTextWriter.Create(fi.FullName, settings);

            writer.WriteStartDocument();
            
            writer.WriteComment(fi.Name);
            if (!string.IsNullOrEmpty(credits))
            {
                writer.WriteComment(credits);
            }
            
            writer.WriteStartElement("Atlas");
            writer.WriteComment(string.Format("RULESET DEFINITION ({0} keys, {1} stats, {2} rules)",
                                Keys.Count, Stats.Count, Rules.Count));

            writer.WriteStartElement("Ruleset");
            writer.WriteAttributeString("gameName", GameName);
            writer.WriteAttributeString("version", Version.ToString());

            writer.WriteComment("KEYS");
            for (int i = 0; i < Keys.Count; i++)
            {
                Keys.GetSortedByName(i).Write(writer);
            }

            writer.WriteComment("STATS");
            for (int i = 0; i < Stats.Count; i++)
            {
                Stats.GetSortedByName(i).Write(writer);
            }

            writer.WriteComment("RULES");
            for (int i = 0; i < Rules.Count; i++)
            {
                Rules.GetSortedById(i).Write(writer);
            }

            writer.WriteEndElement(); //<Ruleset>
            writer.WriteEndElement(); //<Atlas>
            writer.WriteEndDocument();

            writer.Close();

            return true;
        }
    }
}