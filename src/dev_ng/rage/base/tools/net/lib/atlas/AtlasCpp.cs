using System;
using System.Collections.Generic;
using System.IO;

namespace Rockstar.Gamespy.Atlas
{
    public class AtlasCpp
    {
        public enum Version
        {
            Major = 0,
            Minor = 1
        }

        //PURPOSE
        //  Generates .h and .cpp files for use in game project.  These files
        //  are functionally equivalent to what the Atlas admin site generates,
        //  but are contained within an rlGamespyAtlas object and optimized 
        //  for size and lookup speed.
        public static bool GenerateHeader(AtlasRuleset ruleset, 
                                          string className, 
                                          string filename,
                                          string conditionalInclusionExpression,
                                          string credits)
        {
            string includeGuard = className.ToUpper() + "_H";

            if (!filename.EndsWith(".h")) filename += ".h";

            FileInfo fi = new FileInfo(filename);
            StreamWriter sw = new StreamWriter(fi.FullName, false);
            if (sw == null)
            {
                return false;
            }

            sw.WriteLine("// {0}", fi.Name);
            if (!string.IsNullOrEmpty(credits))
            {
                sw.WriteLine("// " + credits);
            }
            sw.WriteLine();
            sw.WriteLine("#ifndef " + includeGuard);
            sw.WriteLine("#define " + includeGuard);
            sw.WriteLine();

            if (!string.IsNullOrEmpty(conditionalInclusionExpression))
            {
                sw.WriteLine(conditionalInclusionExpression);
                sw.WriteLine();
            }

            sw.WriteLine("#include \"rline\\rlgamespyatlas.h\"");
            sw.WriteLine();

            sw.WriteLine("class {0} : public rage::rlGamespyAtlasRuleset", className);
            sw.WriteLine("{");
            sw.WriteLine("public:");
            sw.WriteLine("\t{0}() {{ }};", className);
            sw.WriteLine("\tvirtual ~{0}() {{ }};", className);
            sw.WriteLine();
            sw.WriteLine("\tvirtual unsigned GetVersion() const {{ return {0}; }}", ruleset.Version);
            sw.WriteLine("\tvirtual int GetKey(const char* name) const;");
            sw.WriteLine("\tvirtual int GetStatPageByName(const char* name) const;");
            sw.WriteLine("};");            
            sw.WriteLine();

            if (!string.IsNullOrEmpty(conditionalInclusionExpression))
            {
                sw.WriteLine("#endif // " + conditionalInclusionExpression);
                sw.WriteLine();
            }

            sw.WriteLine("#endif //{0}", includeGuard);

            sw.Close();

            return true;
        }

        public static bool GenerateSource(AtlasRuleset ruleset,
                                          string className,
                                          string filename,
                                          string conditionalInclusionExpression,
                                          string credits)
        {
            if (!filename.EndsWith(".cpp")) filename += ".cpp";

            FileInfo fi = new FileInfo(filename);
            StreamWriter sw = new StreamWriter(fi.FullName, false);
            if (sw == null)
            {
                return false;
            }

            sw.WriteLine("// {0}", fi.Name);
            if (!string.IsNullOrEmpty(credits))
            {
                sw.WriteLine("// " + credits);
            }
            sw.WriteLine();

            if (!string.IsNullOrEmpty(conditionalInclusionExpression))
            {
                sw.WriteLine(conditionalInclusionExpression);
                sw.WriteLine();
            }

            sw.WriteLine("#include \"{0}\"", className.ToLower() + ".h");
            sw.WriteLine();


            //Keys         
            List<uint> hashes = new List<uint>();
            sw.WriteLine("////////////////////////////////////////////////////////////////////////////////");
            sw.WriteLine("// Keys");
            sw.WriteLine("////////////////////////////////////////////////////////////////////////////////");
            sw.WriteLine("static struct { unsigned hash; int id; } keys[] = {");
            for (int i = 0; i < ruleset.Keys.Count; i++)
            {
                AtlasKey key = ruleset.Keys.GetSortedByName(i);

                if (hashes.Contains(Hash(key.Name)))
                {
                    return false; //Duplicate key!
                }

                sw.WriteLine("\t{{ 0x{0}, {1} }}, //{2}", Hash(key.Name).ToString("x8"), key.Id, key.Name);
            }
            sw.WriteLine("};");
            sw.WriteLine();

            //Stats
            hashes.Clear();
            sw.WriteLine("////////////////////////////////////////////////////////////////////////////////");
            sw.WriteLine("// Stats");
            sw.WriteLine("////////////////////////////////////////////////////////////////////////////////");
            sw.WriteLine("static struct { unsigned hash; } stats[] = {");
            for (int i = 0; i < ruleset.Stats.Count; i++)
            {
                AtlasStat stat = ruleset.Stats.GetSortedById(i);

                if (hashes.Contains(Hash(stat.Name)))
                {
                    return false; //Duplicate key!
                }

                sw.WriteLine("\t{{ 0x{0} }}, //{1} (Page {2} [{3}/{4}])",
                             Hash(stat.Name).ToString("x8"),
                             stat.Name,
                             (i / 1000) + 1,
                             (i % 1000) + 1,
                             1000);
            }
            sw.WriteLine("};");
            sw.WriteLine();

            //Hash function
            sw.WriteLine("//Helper that computes hash for a key or stat name.");
            sw.WriteLine("static unsigned hash(const char *string)");
            sw.WriteLine("{");
            sw.WriteLine("\tunsigned h = 0;");
            sw.WriteLine("\twhile (*string)");
            sw.WriteLine("\t{");
            sw.WriteLine("\t\th += *string++;");
            sw.WriteLine("\t\th += (h << 10);");
            sw.WriteLine("\t\th ^= (h >> 6);");
            sw.WriteLine("\t}");
            sw.WriteLine("\th += (h << 3);");
            sw.WriteLine("\th ^= (h >> 11);");
            sw.WriteLine("\th += (h << 15);");
            sw.WriteLine("\treturn h;");
            sw.WriteLine("}");
            sw.WriteLine();

            //GetKey()
            sw.WriteLine("//Returns ID of key by name.");
            sw.WriteLine("int {0}::GetKey(const char* name) const", className);
            sw.WriteLine("{");
            sw.WriteLine("\tif(name)");
            sw.WriteLine("\t{");
            sw.WriteLine("\t\tunsigned h = hash(name);");
            sw.WriteLine("\t\tint numKeys = sizeof(keys)/sizeof(keys[0]);");
            sw.WriteLine("\t\tfor(int i = 0; i < numKeys; i++)");
            sw.WriteLine("\t\t{");
            sw.WriteLine("\t\t\tif(keys[i].hash == h)");
            sw.WriteLine("\t\t\t{");
            sw.WriteLine("\t\t\t\treturn keys[i].id;");
            sw.WriteLine("\t\t\t}");
            sw.WriteLine("\t\t}");
            sw.WriteLine("\t}");
            sw.WriteLine("\treturn rlGamespyAtlasRuleset::INVALID_ID;");
            sw.WriteLine("}");
            sw.WriteLine();

            //GetStatPageByName()
            sw.WriteLine("//Returns page number a stat is on.");
            sw.WriteLine("int {0}::GetStatPageByName(const char* name) const", className);
            sw.WriteLine("{");
            sw.WriteLine("\tif(name)");
            sw.WriteLine("\t{");
            sw.WriteLine("\t\tunsigned h = hash(name);");
            sw.WriteLine("\t\tint numStats = sizeof(stats)/sizeof(stats[0]);");
            sw.WriteLine("\t\tfor(int i = 0; i < numStats; i++)");
            sw.WriteLine("\t\t{");
            sw.WriteLine("\t\t\tif(stats[i].hash == h)");
            sw.WriteLine("\t\t\t{");
            sw.WriteLine("\t\t\t\treturn (i / 1000) + 1; //1000 stats per page");
            sw.WriteLine("\t\t\t}");
            sw.WriteLine("\t\t}");
            sw.WriteLine("\t}");
            sw.WriteLine("\treturn rlGamespyAtlasRuleset::INVALID_PAGE;");
            sw.WriteLine("}");
            sw.WriteLine();

            if (!string.IsNullOrEmpty(conditionalInclusionExpression))
            {
                sw.WriteLine("#endif // " + conditionalInclusionExpression);
                sw.WriteLine();
            }

            sw.Close();

            return true;
        }

        private static uint Hash(string s)
        {
            uint key = 0;

            if (!string.IsNullOrEmpty(s))
            {
                for (int i = 0; i < s.Length; i++)
                {
                    key += (uint)s[i];
                    key += (key << 10);
                    key ^= (key >> 6);
                }
            }

            key += (key << 3);
            key ^= (key >> 11);
            key += (key << 15);

            return key;
        }
    }
}