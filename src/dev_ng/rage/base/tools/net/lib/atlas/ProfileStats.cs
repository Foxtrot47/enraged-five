using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Xml;

namespace Rockstar.Gamespy.Atlas
{
    public class ProfileStat
    {
        public string Name;
        public string Category = "";
        public AtlasType Type = AtlasType.Invalid;
        public string Description = "";

        public static ProfileStat Read(XmlElement el)
        {
            ProfileStat stat = new ProfileStat();
            stat.Name = el.GetAttribute("name");
            stat.Category = el.GetAttribute("category");
            stat.Type = (AtlasType)Enum.Parse(typeof(AtlasType), el.GetAttribute("type"), true);
            stat.Description = el.GetAttribute("desc");
            return stat;
        }

        public void Write(XmlWriter writer)
        {
            writer.WriteStartElement("Stat");
            writer.WriteAttributeString("name", Name);
            writer.WriteAttributeString("category", Category);
            writer.WriteAttributeString("type", Type.ToString().ToLower());
            writer.WriteAttributeString("desc", Description);
            writer.WriteEndElement();
        }

        public override string ToString()
        {
            return string.Format("{0} [cat={1}  type={2}  desc={3}]", Name, Category, Type.ToString(), Description);
        }
    }

    public class ProfileStats
    {
        public Dictionary<string, ProfileStat> Stats { get; private set; }

        public ProfileStats()
        {
            Stats = new Dictionary<string, ProfileStat>();
        }

        //PURPOSE
        //  Loads a set of profile stats from an XML.
        public static ProfileStats Load(string filename)
        {
            ProfileStats ps = new ProfileStats();

            FileInfo fileInfo = new FileInfo(filename);
            string uri = "file://" + fileInfo.FullName;

            XmlDocument doc = new XmlDocument();
            doc.Load(uri);

            XmlElement mainEl = doc.DocumentElement;
            XmlNode node = mainEl.FirstChild;
            for (; null != node; node = node.NextSibling)
            {
                XmlElement el = node as XmlElement;

                if (el != null)
                {
                    if (el.Name == "Stat")
                    {
                        if (!ps.AddStat(ProfileStat.Read(el)))
                        {
                            return null;
                        }
                    }
                }
            }

            return ps;
        }

        //PURPOSE
        //  Adds a stat to the set.  If there is already a stat by that name in
        //  set and this stat differs from it, false will be returned.
        public bool AddStat(ProfileStat stat)
        {
            if (Stats.ContainsKey(stat.Name))
            {
                //If this is a dupe, return error unless it exactly matches.
                if (!stat.Equals(Stats[stat.Name]))
                {
                    Trace.WriteLine("Stat " + stat.Name + " already is in set, with different values.");
                    Trace.WriteLine("Current: " + Stats[stat.Name]);
                    Trace.WriteLine("New:     " + stat);

                    return false;
                }
            }

            Stats[stat.Name] = stat;

            return true;
        }

        public bool Save(string filename, string headerComment, string footerComment)
        {
            FileInfo fi = new FileInfo(filename);
            if (fi.Exists && fi.IsReadOnly)
            {
                Trace.WriteLine("File " + filename + " is read-only; cannot overwrite");
                return false;
            }

            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;
            settings.IndentChars = "\t";
            settings.NewLineChars = "\n";

            XmlWriter writer = XmlTextWriter.Create(fi.FullName, settings);

            writer.WriteStartDocument();

            writer.WriteComment(fi.Name);
            if (!string.IsNullOrEmpty(headerComment))
            {
                writer.WriteComment(headerComment);
            }

            writer.WriteStartElement("ProfileStats");
            writer.WriteComment(string.Format("{0} stats", Stats.Count));

            foreach (KeyValuePair<string, ProfileStat> kvp in Stats)
            {
                kvp.Value.Write(writer);
            }

            writer.WriteEndElement(); //<ProfileStats>

            if (!string.IsNullOrEmpty(footerComment))
            {
                writer.WriteComment(footerComment);
            }

            writer.WriteEndDocument();

            writer.Close();

            return true;
        }

        //PURPOSE
        //  Creates an Atlas ruleset from the profile stats.
        public AtlasRuleset GenerateRuleset(string gameName, int version)
        {
            AtlasRuleset ruleset = new AtlasRuleset();
            ruleset.GameName = gameName;
            ruleset.Version = version;

            int nextId = (int)AtlasId.Invalid + 1;

            //Make sure we have stats used for lookup and debugging.
            ProfileStat ps = new ProfileStat();
            ps.Name = "rs_GamerHandle";
            ps.Type = AtlasType.String;
            if(!AddStatToRuleset(nextId++, ps, ruleset))
            {
                Trace.WriteLine("Failed to add stat " + ps.Name);
                return null;
            }

            ps = new ProfileStat();
            ps.Name = "rs_GamerName";
            ps.Type = AtlasType.String;
            if(!AddStatToRuleset(nextId++, ps, ruleset))
            {
                Trace.WriteLine("Failed to add stat " + ps.Name);
            }

            //Add all the profile stats.
            foreach (KeyValuePair<string, ProfileStat> kvp in Stats)
            {
                if (!AddStatToRuleset(nextId++, kvp.Value, ruleset))
                {
                    Trace.WriteLine("Failed to add stat " + kvp.Value);
                    return null;
                }
            }

            return ruleset;
        }

        private bool AddStatToRuleset(int id,
                                      ProfileStat ps,
                                      AtlasRuleset ruleset)
        {
            string name = ps.Name;
            string category = ps.Category;
            AtlasType type = ps.Type;
            string desc = ps.Description;

            AtlasKey key = default(AtlasKey);
            key.Id = id;
            key.Name = name;
            key.Type = type;
            key.Description = desc;

            AtlasStat stat = default(AtlasStat);
            stat.Id = id;
            stat.Name = name;
            stat.Type = type;
            stat.Category = AtlasStatCategory.Player;
            stat.Description = desc;

            AtlasRule rule = default(AtlasRule);
            rule.Id = id;
            rule.Name = name;
            rule.Description = desc;
            rule.Operation = AtlasOperation.Replace;
            rule.InputKeys = new List<string>();
            rule.InputKeys.Add(key.Name);
            rule.OutputStat = stat.Name;

            if (!ruleset.Keys.Add(key))
            {
                Trace.WriteLine("Failed to add key " + key);
                return false;
            }

            if (!ruleset.Stats.Add(stat))
            {
                Trace.WriteLine("Failed to add stat " + stat);
                return false;
            }

            if (!ruleset.Rules.Add(rule))
            {
                Trace.WriteLine("Failed to add rule " + rule);
                return false;
            }

            return true;
        }
    }
}