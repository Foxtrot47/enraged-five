// rlDbProxy.cpp
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 

#if __WIN32PC

#include "rldatabaseproxy.h"
#include "servicemain.h"
#include "diag/seh.h"
#include "rline/rlgamespy.h"
#include "rline/rlgamespysake.h"
#include "rline/rl.h"
#include "rline/rltitleid.h"
#include "system/nelem.h"
#include "system/param.h"
#include "system/simpleallocator.h"
#include "system/timer.h"
#include "system/xtl.h"

#define NO_PERF_QUEUE
#pragma comment(lib, "pdh")
#include "cpdh.cpp"
#include "perflib.cpp"

using namespace rage;

const unsigned DEFAULT_HEAP_MB                  = 250;
const unsigned DEFAULT_TITLE_SERVER_PORT        = 8001;
const unsigned DEFAULT_ACCEPT_SLEEP_MS          = 10;
const unsigned DEFAULT_INACTIVITY_TIMEOUT_MS    = 10 * 60 * 1000; //10 min
const unsigned DEFAULT_EMAIL_HOUR               = 1; //1am
const unsigned DEFAULT_MAX_MESSAGE_BYTES        = 1024 * 1024; //Enough for most uploads

static unsigned s_MaxMsgSize;

//Helper macro for reporting failed rchecks() in a uniform way
#if !__FINAL
#define RLCHECK_SET_CTX(name) const char* s_rlCheckContext = #name ;
#define RLCHECK_CTX(cond) rcheck(cond, catchall, rlError("%s: \"" #cond "\" failed", s_rlCheckContext));
#else
#define RLCHECK_SET_CTX(name) 
#define RLCHECK_CTX(cond) rcheck(cond, catchall, );
#endif

extern bool SendAdminEmail(const char* subject, const char *text);

///////////////////////////////////////////////////////////////////////////////
//Cmdline parameters
///////////////////////////////////////////////////////////////////////////////
PARAM(inactivitytimeoutms,  "ms without receiving anything from client before we disconnect them");
PARAM(acceptsleepms,        "ms that accept() thread sleeps");
PARAM(dailyemailhour,       "Hour at which to send daily stats email (defaults to 1am PST)");
PARAM(microreportms,        "Used for testing.  This forces a stats report to be sent every n ms (if n > 0).");
PARAM(titleserverport,      "Port that title server listens on");
PARAM(heapmb,               "Size of heap, in MB (default is 768)");
PARAM(maxmsgkb,             "Maximum size of messages (in KB) we'll accept from clients");

//Gamespy config
PARAM(gsEnvironment,        "Environment to run in (part, cert, prod)");
PARAM(gsGameName,           "Gamespy game name (ex. \"rockstardev\")");
PARAM(gsSecretKey,          "Gamespy secret key (ex. \"1a8bBi\")");
PARAM(gsProductId,          "Gamespy product ID (ex. 10947)");
PARAM(gsGameId,             "Gamespy game ID (ex. 1572)");
PARAM(gsLoginEmail,         "Email used to login to Gamespy (ex. \"test1@rage\")");
PARAM(gsLoginPassword,      "Password used to login to Gamespy (ex. \"password\"");
PARAM(gsEncryptSake,        "Set to 1 if Sake requests should use SSL (must be configured on backend as well to work)");
PARAM(gsUseManualRating,    "Set to 1 if not using Sake's built-in rating system (this is the default)");

//Norad options
PARAM(norad_ip,             "IP address of network interface on which to listen for requests.");
PARAM(norad_port,           "Port on which to listen for requests.");
PARAM(norad_appname,        "Application name.");

//Telnet
PARAM(telnet_ip,            "IP address of network interface on which to listen for requests.");
PARAM(telnet_port,          "Port on which to listen for requests.");

RAGE_DECLARE_SUBCHANNEL(rline, dbproxy);
RAGE_DEFINE_SUBCHANNEL(rline, dbproxy);

#if defined(rlDebug1)
#undef rlDebug
#undef rlDebug1
#undef rlDebug2
#undef rlDebug3
#undef rlWarning
#undef rlError
#endif

#define rlDebug1(fmt, ...)  RAGE_DEBUGF1(rline_dbproxy, fmt, ##__VA_ARGS__)
#define rlDebug2(fmt, ...)  RAGE_DEBUGF2(rline_dbproxy, fmt, ##__VA_ARGS__)
#define rlDebug3(fmt, ...)  RAGE_DEBUGF3(rline_dbproxy, fmt, ##__VA_ARGS__)
#define rlWarning(fmt, ...) RAGE_WARNINGF(rline_dbproxy, fmt, ##__VA_ARGS__)
#define rlError(fmt, ...)   RAGE_ERRORF(rline_dbproxy, fmt, ##__VA_ARGS__)

///////////////////////////////////////////////////////////////////////////////
// Windows service functions
// These must be defined somewhere in the app.
///////////////////////////////////////////////////////////////////////////////
rlDbProxy g_rlDbProxy;

bool 
ServiceInit()
{
    return g_rlDbProxy.Init();
}

void 
ServiceShutdown()
{
    g_rlDbProxy.Shutdown();
}

bool 
ServiceUpdate()
{
    return !g_rlDbProxy.Update();
}

///////////////////////////////////////////////////////////////////////////////
//rlDbProxy::Request
///////////////////////////////////////////////////////////////////////////////
rlDbProxy::Request::Request(const int type)
: m_Type(type)
, m_ClientId(INVALID_CLIENTID)
, m_Progress(0.0f)
, m_LastProgress(0.0f)
, m_LastProgressReportTime(0)
, m_StartTime(0)
{
    Assert(m_Type != REQUEST_INVALID);
}

rlDbProxy::Request::~Request()
{
    if(m_Status.Pending())
    {
        m_Status.SetFailed();
    }
}

bool 
rlDbProxy::CreateRecordRequest::Start()
{
    return m_Ctx->m_DbClient.CreateRecord(m_Msg.m_TableName, m_Msg.m_Fields, m_Msg.m_NumFields, &m_RecordId, &m_Status);
}

bool 
rlDbProxy::UpdateRecordRequest::Start()
{
    return m_Ctx->m_DbClient.UpdateRecord(m_Msg.m_TableName, m_Msg.m_RecordId, m_Msg.m_Fields, m_Msg.m_NumFields, &m_Status);
}

///////////////////////////////////////////////////////////////////////////////
//rlDbProxy::DeleteRecordRequest
///////////////////////////////////////////////////////////////////////////////
bool 
rlDbProxy::DeleteRecordRequest::Start()
{
    return m_Ctx->m_DbClient.DeleteRecord(m_Msg.m_TableName, m_Msg.m_RecordId, &m_Status);
}

///////////////////////////////////////////////////////////////////////////////
//rlDbProxy::RateRecordRequest
///////////////////////////////////////////////////////////////////////////////
bool 
rlDbProxy::RateRecordRequest::Start()
{
    return m_Ctx->m_DbClient.RateRecord(m_Msg.m_TableName, 
                                        m_Msg.m_RecordId, 
                                        m_Msg.m_CurNumRatings, 
                                        m_Msg.m_CurAvgRating, 
                                        m_Msg.m_Rating, 
                                        &m_NumRatings, 
                                        &m_AvgRating, 
                                        &m_Status);
}

///////////////////////////////////////////////////////////////////////////////
//rlDbProxy::GetNumRecordsRequest
///////////////////////////////////////////////////////////////////////////////
bool 
rlDbProxy::GetNumRecordsRequest::Start()
{
    return m_Ctx->m_DbClient.GetNumRecords(m_Msg.m_TableName, m_Msg.m_Filter, &m_NumRecords, &m_Status);
}

///////////////////////////////////////////////////////////////////////////////
//rlDbProxy::ReadRecordsRequest
///////////////////////////////////////////////////////////////////////////////
rlDbProxy::ReadRecordsRequest::ReadRecordsRequest() 
: TRequest<rlDbMsgReadRecordsRequest>(REQUEST_READ_RECORDS) 
, m_Values(0)
, m_NumValues(0)
{
}

rlDbProxy::ReadRecordsRequest::~ReadRecordsRequest() 
{
    if(m_Values)
    {
        rlDelete(m_Values, m_NumValues);
        m_Values = 0;
        m_NumValues = 0;
    }
}

bool 
rlDbProxy::ReadRecordsRequest::Configure(rlDbProxy* ctx,
                                         unsigned clientId, 
                                         const void* requestMsgData,
                                         const unsigned requestMsgSize)
{
    return TRequest<rlDbMsgReadRecordsRequest>::Configure(ctx, clientId, requestMsgData, requestMsgSize);
}

bool 
rlDbProxy::ReadRecordsRequest::Start()
{
    Assert(!m_Values);

    m_NumValues = m_Msg.m_Schema->GetNumFields() * m_Msg.m_MaxRecords;
    m_Values = RL_NEW_ARRAY(rlDbProxyReadRecordsRequest, rlDbValue, m_NumValues);
    if(!m_Values) return false;

    m_Results.Reset(m_Msg.m_Schema, m_Msg.m_MaxRecords, m_Values);

    return m_Ctx->m_DbClient.ReadRecords(m_Msg.m_TableName, 
                                         m_Msg.m_Filter, 
                                         m_Msg.m_Sort, 
                                         m_Msg.m_StartingRank, 
                                         m_Msg.m_PostSortFilter, 
                                         m_Msg.m_Radius, 
                                         m_Msg.m_CacheResults, 
                                         m_Msg.m_MaxRecords, 
                                         &m_Results, 
                                         &m_Status);
}

///////////////////////////////////////////////////////////////////////////////
//rlDbProxy::ReadRandomRecordRequest
///////////////////////////////////////////////////////////////////////////////
rlDbProxy::ReadRandomRecordRequest::ReadRandomRecordRequest() 
: TRequest<rlDbMsgReadRandomRecordRequest>(REQUEST_READ_RANDOM_RECORD) 
, m_Values(0)
, m_NumValues(0)
{
}

rlDbProxy::ReadRandomRecordRequest::~ReadRandomRecordRequest() 
{
    if(m_Values)
    {
        rlDelete(m_Values, m_NumValues);
        m_Values = 0;
        m_NumValues = 0;
    }
}

bool 
rlDbProxy::ReadRandomRecordRequest::Configure(rlDbProxy* ctx,
                                              unsigned clientId, 
                                              const void* requestMsgData,
                                              const unsigned requestMsgSize)
{
    return TRequest<rlDbMsgReadRandomRecordRequest>::Configure(ctx, clientId, requestMsgData, requestMsgSize);
}

bool 
rlDbProxy::ReadRandomRecordRequest::Start()
{
    Assert(!m_Values);

    m_NumValues = m_Msg.m_Schema->GetNumFields();
    m_Values = RL_NEW_ARRAY(rlDbProxyReadRandomRecordRequest, rlDbValue, m_NumValues);
    if(!m_Values) return false;

    m_Results.Reset(m_Msg.m_Schema, 1, m_Values);

    return m_Ctx->m_DbClient.ReadRandomRecord(m_Msg.m_TableName, m_Msg.m_Filter, &m_Results, &m_Status);
}

///////////////////////////////////////////////////////////////////////////////
//rlDbProxy::UploadFileRequest
///////////////////////////////////////////////////////////////////////////////
bool 
rlDbProxy::UploadFileRequest::Start()
{
    return m_Ctx->m_DbClient.UploadFile(m_Msg.m_FileData, m_Msg.m_FileSize, &m_Progress, &m_FileId, &m_Status);
}

///////////////////////////////////////////////////////////////////////////////
//rlDbProxy::DownloadFileRequest
///////////////////////////////////////////////////////////////////////////////
rlDbProxy::DownloadFileRequest::DownloadFileRequest()
: TRequest<rlDbMsgDownloadFileRequest>(REQUEST_DOWNLOAD_FILE)
, m_Buf(0)
, m_NumBytesDownloaded(0)
{
}

rlDbProxy::DownloadFileRequest::~DownloadFileRequest()
{
    if(m_Buf) delete [] m_Buf;
}

bool 
rlDbProxy::DownloadFileRequest::Start()
{
    RLCHECK_SET_CTX(DownloadFileRequest::Start);

    rtry
    {
        RLCHECK_CTX(0 != m_Msg.m_FileId);

        RLCHECK_CTX(m_Msg.m_BufSize > 0);       
        RLCHECK_CTX(!m_Buf);
        RLCHECK_CTX(m_Msg.m_BufSize <= s_MaxMsgSize);
        RLCHECK_CTX(0 != (m_Buf = rage_new u8[m_Msg.m_BufSize]));

        RLCHECK_CTX(m_Ctx->m_DbClient.DownloadFile(m_Msg.m_FileId, m_Buf, m_Msg.m_BufSize, &m_Progress, &m_NumBytesDownloaded, &m_Status));
    }
    rcatchall
    {
        return false;
    }
    return true;
}

///////////////////////////////////////////////////////////////////////////////
//rlDbProxy::OutboundMsg
///////////////////////////////////////////////////////////////////////////////
rlDbProxy::OutboundMsg::OutboundMsg()
: m_MsgBuf(0)
, m_MsgSize(0)
{
}

rlDbProxy::OutboundMsg::~OutboundMsg()
{
    if(m_MsgBuf) 
    {
        delete [] m_MsgBuf;
        m_MsgBuf = 0;
    }
}

bool
rlDbProxy::OutboundMsg::Init(const void* data, const unsigned size)
{
    RLCHECK_SET_CTX(OutboundMsg::Init);

    rtry
    {       
        m_MsgSize = size;
        RLCHECK_CTX(m_MsgSize > 0);
        RLCHECK_CTX(0 != (m_MsgBuf = rage_new u8[m_MsgSize]));

        RLCHECK_CTX(data);
        sysMemCpy(m_MsgBuf, data, m_MsgSize);
    }
    rcatchall
    {
        return false;
    }
    return true;
}

///////////////////////////////////////////////////////////////////////////////
//rlDbProxy::ClientRec
///////////////////////////////////////////////////////////////////////////////
rlDbProxy::ClientRec::ClientRec()
: m_ClientId(INVALID_CLIENTID)
, m_NumRequestsInProgress(0)
, m_LastActiveTime(0)
, m_Socket(INVALID_SOCKET)
, m_MsgBuf(0)
, m_MsgBytesRead(0)
, m_MsgSize(0)
, m_MsgSizeBytesRead(0)
, m_MsgBytesSent(0)
{
}

rlDbProxy::ClientRec::~ClientRec()
{
    if(INVALID_SOCKET != m_Socket)
    {
        closesocket(m_Socket);
    }

    if(m_MsgBuf) 
    {
        delete [] m_MsgBuf;
        m_MsgBuf = 0;
    }

    while(!m_OutboundMsgList.empty())
    {
        OutboundMsg* m = *m_OutboundMsgList.begin();
        m_OutboundMsgList.erase(m_OutboundMsgList.begin());
        delete m;
    }
}

///////////////////////////////////////////////////////////////////////////////
//rlDbProxy
///////////////////////////////////////////////////////////////////////////////
rlDbProxy::rlDbProxy()
: m_Initialized(false)
, m_Started(false)
, m_Heap(0)
, m_Allocator(0)
, m_OldAllocator(0)
, m_ServerSocket(INVALID_SOCKET)
, m_WaitSema(0)
, m_ThreadHandle(sysIpcThreadIdInvalid)
, m_UptimeMs(0)
, m_CurTime(sysTimer::GetSystemMsTime())
, m_CurHour(0)
, m_IncomingBytesAccum(0)
, m_OutgoingBytesAccum(0)
, m_MicroReportMs(0)
, m_DailyEmailHour(DEFAULT_EMAIL_HOUR)
, m_MicroReportTimer(0)
, m_EmailFixupMs(0)
, m_StartedNorad(false)
, m_TelnetSkt(-1)
, m_InactivityTimeoutMs(DEFAULT_INACTIVITY_TIMEOUT_MS)
{
}

rlDbProxy::~rlDbProxy()
{
    if(m_Initialized)
    {
        Shutdown();
    }
}

bool
rlDbProxy::Init()
{
    RLCHECK_SET_CTX(Init);

    rtry
    {
        RLCHECK_CTX(!m_Initialized);

        //Allocate a the heap that everything will use
        unsigned heapmb = DEFAULT_HEAP_MB;
        PARAM_heapmb.Get(heapmb);

        unsigned sizeofHeap = heapmb * 1024 * 1024;

        m_Heap = sysMemVirtualAllocate(sizeofHeap);
        RLCHECK_CTX(m_Heap);

        m_Allocator = rage_new sysMemSimpleAllocator(m_Heap, sizeofHeap, sysMemSimpleAllocator::HEAP_NET);
        m_OldAllocator = &sysMemAllocator::GetCurrent();
        sysMemAllocator::SetCurrent(*m_Allocator);

        //Cache config params
        PARAM_microreportms.Get(m_MicroReportMs);
        m_MicroReportTimer = m_MicroReportMs;

        m_DailyEmailHour = DEFAULT_EMAIL_HOUR;
        PARAM_dailyemailhour.Get(m_DailyEmailHour);

        m_InactivityTimeoutMs = DEFAULT_INACTIVITY_TIMEOUT_MS;
        PARAM_acceptsleepms.Get(m_InactivityTimeoutMs);

        s_MaxMsgSize = DEFAULT_MAX_MESSAGE_BYTES;
        if(PARAM_maxmsgkb.Get(s_MaxMsgSize))
        {
            s_MaxMsgSize *= 1024;
        }

        rlDebug2("%s: heapmb             = %d", s_rlCheckContext, heapmb);
        rlDebug2("%s: microreportms      = %d", s_rlCheckContext, m_MicroReportTimer);
        rlDebug2("%s: dailyemailhour     = %d", s_rlCheckContext, m_DailyEmailHour);
        rlDebug2("%s: acceptsleepms      = %d", s_rlCheckContext, m_InactivityTimeoutMs);

        //Init subsystems
        RLCHECK_CTX(InitNet());
        RLCHECK_CTX(InitRline());
        RLCHECK_CTX(m_DbClient.Init());
        RLCHECK_CTX(m_Upload.Init(&m_DbClient));
        RLCHECK_CTX(InitNorad());
        RLCHECK_CTX(InitTelnet());

        m_Initialized = true;

        RLCHECK_CTX(Start());
    }
    rcatchall
    {
        Reset();
        return false;
    }
    return true;
}

bool
rlDbProxy::Start()
{
    RLCHECK_SET_CTX(Start);

    rtry
    {
        RLCHECK_CTX(!m_Started);

        rlDebug2("%s: Initializing TCP...", s_rlCheckContext);
        RLCHECK_CTX(CreateServerSocket());
        RLCHECK_CTX(StartAcceptThread());

        m_Started = true;

        rlDebug2("%s: succeeded.", s_rlCheckContext);
    }
    rcatchall
    {
        Reset();
        return false;
    }
    return true;
}

void
rlDbProxy::Stop()
{
    //Shutdown TCP
    {
        SYS_CS_SYNC(m_CsServerSocket);

        if(INVALID_SOCKET != m_ServerSocket)
        {
            closesocket(m_ServerSocket);
            m_ServerSocket = INVALID_SOCKET;
        }
    }

    if(m_ThreadHandle != sysIpcThreadIdInvalid)
    {
        sysIpcWaitThreadExit(m_ThreadHandle);
        m_ThreadHandle = sysIpcThreadIdInvalid;
    }

    if(m_WaitSema)
    {
        sysIpcDeleteSema(m_WaitSema);
        m_WaitSema = 0;
    }

    m_Started = false;
}

void
rlDbProxy::Reset()
{
    rlDebug2("rlDbProxy::Reset");

    Stop();

    //Shutdown telnet
    m_CmdServer.Stop();

    if(m_TelnetSkt >= 0)
    {
        closesocket(m_TelnetSkt);
        m_TelnetSkt = -1;
    }

    //Shutdown Norad
    if(m_StartedNorad)
    {
        FreeTCPPerfLib();
        m_StartedNorad = false;
    }

    //Stop our uploads.
    m_Upload.Shutdown();
    m_DbClient.Shutdown();

    //Clear the request queue
    while(!m_RequestList.empty())
    {
        Request* r = *m_RequestList.begin();
        m_RequestList.erase(m_RequestList.begin());
        delete r;
    }

    //Clear the client map
    while(!m_ClientMap.empty())
    {
        ClientRec* c = m_ClientMap.begin()->second;
        m_ClientMap.erase(m_ClientMap.begin());
        delete c;
    }

    //Shutdown rline
    if(rlIsInitialized())
    {
        rlShutdown();
    }

    //Shutdown net
    if(m_NetSkt.GetHardware())
    {
        m_NetHw.DestroySocket(&m_NetSkt);
    }

    //Free heap and restore allocators
    if(m_OldAllocator)
    {
        sysMemAllocator::SetCurrent(*m_OldAllocator);
        delete m_Allocator;
        m_Allocator = NULL;
        m_OldAllocator = NULL;
        sysMemVirtualFree(m_Heap);
        m_Heap = NULL;
    }
}

void
rlDbProxy::Shutdown()
{    
    Assert(m_Initialized);

    rlDebug2("rlDbProxy::Shutdown() starting");

    Reset();

    m_Initialized = false;

    rlDebug2("rlDbProxy::Shutdown() finished");
}

bool
rlDbProxy::Update()
{
    m_CurTime = sysTimer::GetSystemMsTime();

    m_NetHw.Update();

    rlUpdate();

    m_DbClient.Update();

    m_Upload.Update();

    CheckCompletedRequests();

    CheckTimedOutConnections();

    CheckNewConnections();

    CheckNewRequests();

    SendToClients();

    UpdateStats();

    UpdateTelnet();

    return true;
}

bool 
rlDbProxy::InitNet()
{
    RLCHECK_SET_CTX(InitNet);

    rtry
    {
        rlDebug2("%s: Initializing...", s_rlCheckContext);

        //NOTE: Since we don't actually send or receive anything on this socket,
        //      we just grab any available port.
        RLCHECK_CTX(m_NetHw.CreateSocket(&m_NetSkt, 0, NET_PROTO_UDP));

        rlDebug2("%s: Succeeded", s_rlCheckContext);
    }
    rcatchall
    {
        return false;
    }
    return true;
}

bool 
rlDbProxy::InitRline()
{
    RLCHECK_SET_CTX(InitRline);

    //Initialize the rline subsystem.
    rtry
    {
        rlDebug2("%s: Initializing...", s_rlCheckContext);

        //Get title ID info
        const char* gsEnvironment   = 0;
        const char* gsGameName      = 0;
        const char* gsSecretKey     = 0;
        const char* gsLoginEmail    = 0;
        const char* gsLoginPassword = 0;
        int gsProductId             = 0;
        int gsGameId                = 0;
        int gsEncryptSake           = 0;
        int gsUseManualRating       = 1;
 
        //Required params
        RLCHECK_CTX(PARAM_gsEnvironment.Get(gsEnvironment));
        rlDebug2("%s: gsEnvironment      = %s", s_rlCheckContext, gsEnvironment);

        RLCHECK_CTX(PARAM_gsGameName.Get(gsGameName));
        rlDebug2("%s: gsGameName         = %s", s_rlCheckContext, gsGameName);

        RLCHECK_CTX(PARAM_gsSecretKey.Get(gsSecretKey));
        rlDebug2("%s: gsSecretKey        = %s", s_rlCheckContext, gsSecretKey);

        RLCHECK_CTX(PARAM_gsProductId.Get(gsProductId));
        rlDebug2("%s: gsProductId        = %d", s_rlCheckContext, gsProductId);

        RLCHECK_CTX(PARAM_gsGameId.Get(gsGameId));
        rlDebug2("%s: gsGameId           = %d", s_rlCheckContext, gsGameId);

        RLCHECK_CTX(PARAM_gsLoginEmail.Get(gsLoginEmail));
        rlDebug2("%s: gsLoginEmail       = %s", s_rlCheckContext, gsLoginEmail);

        RLCHECK_CTX(PARAM_gsLoginPassword.Get(gsLoginPassword));
        rlDebug2("%s: gsLoginPassword    = %s", s_rlCheckContext, gsLoginPassword);

        PARAM_gsEncryptSake.Get(gsEncryptSake);
        rlDebug2("%s: gsEncryptSake      = %s", s_rlCheckContext, (gsEncryptSake != 0) ? "true" : "false");

        PARAM_gsUseManualRating.Get(gsUseManualRating);
        rlDebug2("%s: gsUseManualRating  = %s", s_rlCheckContext, (gsUseManualRating != 0) ? "true" : "false");

        //Determine our environment
        rlGamespyTitleId::eEnvironment env = rlGamespyTitleId::ENV_DEVELOPMENT;
        if(!strcmp(gsEnvironment, "part"))
        {
            env = rlGamespyTitleId::ENV_DEVELOPMENT;
        }
        else if(!strcmp(gsEnvironment, "cert"))
        {
            env = rlGamespyTitleId::ENV_CERTIFICATION;
        }
        else if(!strcmp(gsEnvironment, "prod"))
        {
            env = rlGamespyTitleId::ENV_PRODUCTION;
        }

        rlGamespyTitleId gamespyTitleId(env,
                                        gsGameName,
                                        gsSecretKey,
                                        gsProductId,
                                        gsGameId,
                                        NULL,       //Not using Atlas
                                        gsEncryptSake != 0);

        rlTitleId titleId(gamespyTitleId);

        static const unsigned MIN_AGE_RATING = 0;

        rlInit(m_Allocator, &m_NetSkt, &titleId, MIN_AGE_RATING);

        //Set the account Gamespy should login with.
        RLCHECK_CTX(g_rlGamespy.SetLoginInfo(gsLoginEmail, gsLoginPassword));

        //IMPORTANT: Set database to use manual rating.  This is necessary since all rating is
        //           being done through the single account.
        rlGamespySake::SetUsingManualRating(gsUseManualRating != 0);

        //IMPORTANT: Set rlGamespy to stay connected to GP.  This is necessary so we can detect
        //           GS going down for any reason, and auto-log back in once available again.
        g_rlGamespy.SetRemainConnectedToGp(true);

        rlDebug2("%s: Succeeded", s_rlCheckContext);
    }
    rcatchall
    {
        return false;
    }
    return true;
}

bool
rlDbProxy::InitNorad()
{
    RLCHECK_SET_CTX(InitNorad);

    //Initialize gamespy's performance monitoring lib.
    const char* noradIpStr = "";
    int noradPort;
    const char* noradAppName;

    rtry
    {
        rlDebug2("%s: Initializing...", s_rlCheckContext);

        if(PARAM_norad_ip.Get(noradIpStr) 
           && PARAM_norad_port.Get(noradPort)
           && PARAM_norad_appname.Get(noradAppName))
        {
            rlDebug2("%s: norad_ip       = %s", s_rlCheckContext, noradIpStr);
            rlDebug2("%s: norad_port     = %d", s_rlCheckContext, noradPort);
            rlDebug2("%s: norad_appname  = %s", s_rlCheckContext, noradAppName);
            
            RLCHECK_CTX(true == (m_StartedNorad = InitTCPPerfLib(noradIpStr, noradPort, noradAppName)));

            rlDebug2("%s: Succeeded", s_rlCheckContext);
        }
        else
        {
            rlDebug2(("%s: Not all parameters supplied; not using Norad", s_rlCheckContext));
        }
    }
    rcatchall
    {
        return false;
    }
    return true;
}

bool
rlDbProxy::InitTelnet()
{
    RLCHECK_SET_CTX(InitTelnet);

    rtry
    {
        rlDebug2("%s: Initializing...", s_rlCheckContext);

        RLCHECK_CTX(m_TelnetSkt == -1);

        unsigned port = 0;
        PARAM_telnet_port.Get(port);

        if(!port)
        {
           rlDebug2("Telnet port not specified, skipping");
           return true;
        }

        unsigned ip = INADDR_NONE;
        const char* ipStr = "127.0.0.1";

        if(!PARAM_telnet_ip.Get(ipStr))
        {
            ipStr = "";

            char hostname[128];

            rcheck(SOCKET_ERROR != gethostname(hostname, sizeof(hostname)-1),
                    catchall,
                    rlError("Error calling gethostname: %d", WSAGetLastError()));

            struct hostent* he = gethostbyname(hostname);

            rcheck(he,
                   catchall,
                   rlError("Error calling gethostbyname: %d", WSAGetLastError()));

            Assert(sizeof(ip) == he->h_length);
            memcpy(&ip, he->h_addr_list[0], he->h_length);

            if(INADDR_NONE == ip)
            {
                ip = inet_addr("127.0.0.1");
            }
        }
        else
        {
            ip = inet_addr(ipStr);
        }

        ip = ntohl(ip);

        rlDebug2("%s: telnet_ip      = %s", s_rlCheckContext, ipStr);
        rlDebug2("%s: telnet_port    = %d", s_rlCheckContext, port);

        netAddress addr(ip, (u16)port);
        rlDebug2("%s: Listening on %d.%d.%d.%d:%d", s_rlCheckContext, NET_ADDR_FOR_PRINTF(addr));

        sockaddr_in sin = {0};
        sin.sin_family = AF_INET;
        sin.sin_port = htons(unsigned short(port));
        sin.sin_addr.s_addr = htonl(ip);

        m_TelnetSkt = socket(AF_INET, SOCK_STREAM, 0);

        rcheck(m_TelnetSkt >= 0,
               catchall,
               rlError("Error creating command server listen socket: %d", WSAGetLastError()));
        
        rcheck(0 == bind(m_TelnetSkt, (sockaddr*) &sin, sizeof(sin)),
               catchall,
               rlError("Error binding command server listen socket: %d", WSAGetLastError()));
        
        //u_long nonblock = 1;
        //rcheck(0 == ioctlsocket(m_TelnetSkt, FIONBIO, &nonblock),
        //       catchall,
        //       rlError("Failed to set telnet socket to non-blocking"));

        rcheck(0 == listen(m_TelnetSkt, SOMAXCONN),
               catchall,
               rlError("Error listening on command server socket: %d", WSAGetLastError()));

        m_CmdServer.Start(m_TelnetSkt);

        rlDebug2("%s: Succeeded", s_rlCheckContext);
    }
    rcatchall
    {
        if(m_TelnetSkt >= 0)
        {
            closesocket(m_TelnetSkt);
            m_TelnetSkt = -1;
        }

        return false;
    }
    return true;
}

void
rlDbProxy::UpdateTelnet()
{
    Command* cmd = m_CmdServer.NextCommand();

    if(cmd)
    {
        bool knowCmd = false;
        const char* cmdStr = cmd->GetString();

        rlDebug1("Received command: %s", cmdStr);

        if(!strcmp(cmdStr, "stop"))
        {
            knowCmd = true;

            Stop();
            m_CmdServer.Say("stopped");
        }
        else if(!strcmp(cmdStr, "start"))
        {
            knowCmd = true;

            if(!m_Started)
            {
                if(Start())
                {
                    m_CmdServer.Say("started");
                }
                else
                {
                    m_CmdServer.Say("failed to start");
                }
            }
            else
            {
                m_CmdServer.Say("already started");
            }
        }
#if !__NO_OUTPUT
        else if(strstr(cmdStr, "set debug") == cmdStr)
        {
            knowCmd = true;

            int dbgLvl;
            if(sscanf(cmdStr, "set debug %d", &dbgLvl) == 1)
            {
                rlDebug1("Setting debug level to: %d", dbgLvl);
                Channel_rline_dbproxy.TtyLevel = u8(dbgLvl + DIAG_SEVERITY_DEBUG1 - 1);
            }

            char buf[256];
            formatf(buf, sizeof(buf), "debug level is %d", Channel_rline_dbproxy.TtyLevel);
            m_CmdServer.Say(buf);
        }
#endif

        if(!knowCmd)
        {
            char buf[256];
            formatf(buf, sizeof(buf), "unknown command \"%s\"", cmdStr);
            m_CmdServer.Say(buf);
        }

        m_CmdServer.DisposeCommand(cmd);
    }
}

void
rlDbProxy::CheckCompletedRequests()
{
    //Go through list of requests and process ones that are no longer pending.
    if(!m_RequestList.empty())
    {
        const unsigned SEND_PROGRESS_MS = 500;

        RequestList::iterator it = m_RequestList.begin();
        while(it != m_RequestList.end())
        {
            Request* r = *it;
            Assert(r);

            if(!r->m_Status.Pending())
            {
                ++m_DayStats.m_RequestStats[r->m_Type].m_NumResults[r->m_Status.GetResultCode()];

                ProcessCompletedRequest(r);

                RequestList::iterator next = it;
                ++next;
                m_RequestList.erase(it);
                it = next;
                delete r;
                continue;
            }
            else if((r->m_Progress != r->m_LastProgress) && ((m_CurTime - r->m_LastProgressReportTime) > SEND_PROGRESS_MS))
            {
                SendProgressMsg(r->m_ClientId, r->m_Progress);

                r->m_LastProgress = r->m_Progress;
                r->m_LastProgressReportTime = m_CurTime;
            }
           
            ++it;
        }
    }

    m_EmailFixupMs = 0;
}

void
rlDbProxy::CheckTimedOutConnections()
{
    //Check for connections that have been inactive for too long
    if(!m_ClientMap.empty())
    {
        ClientMap::iterator it = m_ClientMap.begin();
        while(it != m_ClientMap.end())
        {
            ClientRec* c = it->second;
            Assert(c);

            if(c->m_NumRequestsInProgress)
            {
                c->m_LastActiveTime = m_CurTime;
            }
            else
            {
                if((m_CurTime - c->m_LastActiveTime) >= m_InactivityTimeoutMs)
                {
                    rlDebug2("Disconnecting inactive client connection %d.%d.%d.%d:%d",
                              NET_ADDR_FOR_PRINTF(c->m_Address));
                    
                    it = RemoveClient(c->m_ClientId);
                    continue;
                }
            }

            ++it;
        }
    }
}

rlDbProxy::ClientMap::iterator
rlDbProxy::RemoveClient(const unsigned clientId)
{
    ClientMap::iterator it = m_ClientMap.find(clientId);
    if(it != m_ClientMap.end())
    {
        ClientRec* c = it->second;
        ClientMap::iterator next = it;
        ++next;
        m_ClientMap.erase(it);
        it = next;
        delete c;
    }
    return it;
}

bool
rlDbProxy::ReceiveFromClient(ClientRec* c,
                             const void* data,
                             const unsigned size)
{
    Assert(c && data && size);

    RLCHECK_SET_CTX(ReceiveFromClient);

    bool success = false; 

    Request* r = 0;

#define HANDLE_DB_REQUEST(name, type) \
    if(!success && msgId == rlDbMsg##name##Request::MSG_ID()) \
    { \
        rlDebug2("%s: " #name " request (%u bytes) from %d.%d.%d.%d:%d", s_rlCheckContext, size, NET_ADDR_FOR_PRINTF(c->m_Address)); \
        ++m_DayStats.m_HourStats[m_CurHour].m_NumRequests; \
        m_DayStats.m_RequestStats[ REQUEST_##type ].m_Size.AddSample(size); \
        name##Request* req = 0; \
        RLCHECK_CTX(0 != (req = rage_new name##Request())); \
        r = req; \
        RLCHECK_CTX(req->Configure(this, c->m_ClientId, data, size)); \
        RLCHECK_CTX(req->Start()); \
        QueueRequest(r);\
        ++c->m_NumRequestsInProgress; \
        success = true; \
    }

    rtry
    {
        unsigned msgId;
        RLCHECK_CTX(netMessage::GetId(&msgId, data, size));

//#define HANDLE_DB_REQUEST(name, type) \
//        if(!success && msgId == rlDbMsg##name##Request::MSG_ID()) \
//        { \
//            rlDebug2("%s: " #name " request (%u bytes) from %d.%d.%d.%d:%d", s_rlCheckContext, size, NET_ADDR_FOR_PRINTF(c->m_Address)); \
//            ++m_DayStats.m_HourStats[m_CurHour].m_NumRequests; \
//            m_DayStats.m_RequestStats[ REQUEST_##type ].m_Size.AddSample(size); \
//            rlDbMsg##name##Request req; \
//            RLCHECK_CTX(req.Import(data, size)); \
//            RLCHECK_CTX(ReceiveRequest##name##(c, req)); \
//            ++c->m_NumRequestsInProgress; \
//            success = true; \
//        }

        HANDLE_DB_REQUEST(CreateRecord, CREATE_RECORD);
        HANDLE_DB_REQUEST(UpdateRecord, UPDATE_RECORD);
        HANDLE_DB_REQUEST(DeleteRecord, DELETE_RECORD);
        HANDLE_DB_REQUEST(RateRecord, RATE_RECORD);
        HANDLE_DB_REQUEST(GetNumRecords, GET_NUM_RECORDS);
        HANDLE_DB_REQUEST(ReadRecords, READ_RECORDS);
        HANDLE_DB_REQUEST(ReadRandomRecord, READ_RANDOM_RECORD);
        HANDLE_DB_REQUEST(UploadFile, UPLOAD_FILE);
        HANDLE_DB_REQUEST(DownloadFile, DOWNLOAD_FILE);

        if(!success)
        {
            rlError("%s: Unhandled msg ID (%d), ignoring", s_rlCheckContext, msgId);
        }
    }
    rcatchall
    {
        if(r) delete r;
    }

#undef HANDLE_DB_REQUEST

    return success;
}

void
rlDbProxy::QueueRequest(Request* r)
{
    r->m_StartTime = m_CurTime;
    m_RequestList.push_back(r);
}
/*
bool
rlDbProxy::ReceiveRequestCreateRecord(ClientRec* c, const rlDbMsgCreateRecordRequest& req)
{
    Assert(c);

    RLCHECK_SET_CTX(ReceiveRequestCreateRecord);

    CreateRecordRequest* r = 0;

    rtry
    {
        RLCHECK_CTX(0 != (r = rage_new CreateRecordRequest(c->m_ClientId)));
        RLCHECK_CTX(r->Init(req.m_TableName, req.m_Fields, req.m_NumFields));
        RLCHECK_CTX(m_DbClient.CreateRecord(r->m_TableName, r->m_Fields, req.m_NumFields, &r->m_RecordId, &r->m_Status));

        QueueRequest(r);
    }
    rcatchall
    {
        if(r) delete r;
        return false;
    }  
    return true;
}

bool
rlDbProxy::ReceiveRequestUpdateRecord(ClientRec* c, const rlDbMsgUpdateRecordRequest& req)
{
    Assert(c);

    RLCHECK_SET_CTX(ReceiveRequestUpdateRecord);

    UpdateRecordRequest* r = 0;

    rtry
    {
        RLCHECK_CTX(0 != (r = rage_new UpdateRecordRequest(c->m_ClientId)));
        RLCHECK_CTX(r->Init(req.m_TableName, req.m_Fields, req.m_NumFields));
        RLCHECK_CTX(m_DbClient.UpdateRecord(r->m_TableName, req.m_RecordId, r->m_Fields, req.m_NumFields, &r->m_Status));

        QueueRequest(r);
    }
    rcatchall
    {
        if(r) delete r;
        return false;
    }  
    return true;
}

bool
rlDbProxy::ReceiveRequestDeleteRecord(ClientRec* c, const rlDbMsgDeleteRecordRequest& req)
{
    Assert(c);

    RLCHECK_SET_CTX(ReceiveRequestDeleteRecord);

    DeleteRecordRequest* r = 0;

    rtry
    {
        RLCHECK_CTX(0 != (r = rage_new DeleteRecordRequest(c->m_ClientId)));
        RLCHECK_CTX(r->Init(req.m_TableName, req.m_RecordId));
        RLCHECK_CTX(m_DbClient.DeleteRecord(r->m_TableName, r->m_RecordId, &r->m_Status));

        QueueRequest(r);
    }
    rcatchall
    {
        if(r) delete r;
        return false;
    }  
    return true;
}

bool
rlDbProxy::ReceiveRequestRateRecord(ClientRec* c, const rlDbMsgRateRecordRequest& req)
{
    Assert(c);

    RLCHECK_SET_CTX(ReceiveRequestRateRecord);

    RateRecordRequest* r = 0;

    rtry
    {
        RLCHECK_CTX(0 != (r = rage_new RateRecordRequest(c->m_ClientId)));
        RLCHECK_CTX(r->Init(req.m_TableName));
        RLCHECK_CTX(m_DbClient.RateRecord(r->m_TableName, 
                                                  req.m_RecordId, 
                                                  req.m_CurNumRatings,
                                                  req.m_CurAvgRating,
                                                  req.m_Rating,
                                                  &r->m_NumRatings,
                                                  &r->m_AvgRating,
                                                  &r->m_Status));

        QueueRequest(r);
    }
    rcatchall
    {
        if(r) delete r;
        return false;
    }  
    return true;
}

bool
rlDbProxy::ReceiveRequestGetNumRecords(ClientRec* c, const rlDbMsgGetNumRecordsRequest& req)
{
    Assert(c);

    RLCHECK_SET_CTX(ReceiveRequestGetNumRecords);

    GetNumRecordsRequest* r = 0;

    rtry
    {
        RLCHECK_CTX(0 != (r = rage_new GetNumRecordsRequest(c->m_ClientId)));
        RLCHECK_CTX(r->Init(req.m_TableName, 
                            req.m_Filter));
        RLCHECK_CTX(m_DbClient.GetNumRecords(r->m_TableName, 
                                                     r->m_Filter,
                                                     &r->m_NumRecords, 
                                                     &r->m_Status));

        QueueRequest(r);
    }
    rcatchall
    {
        if(r) delete r;
        return false;
    }  
    return true;
}

bool
rlDbProxy::ReceiveRequestReadRecords(ClientRec* c, const rlDbMsgReadRecordsRequest& req)
{
    Assert(c);

    RLCHECK_SET_CTX(ReceiveRequestReadRecords);

    ReadRecordsRequest* r = 0;

    rtry
    {
        RLCHECK_CTX(0 != (r = rage_new ReadRecordsRequest(c->m_ClientId)));
        RLCHECK_CTX(r->Init(req.m_TableName, 
                            req.m_Filter,
                            req.m_Sort,
                            req.m_PostSortFilter,
                            (const char**)req.m_FieldNames,
                            req.m_NumFieldNames,
                            req.m_MaxRecords));
        RLCHECK_CTX(m_DbClient.ReadRecords(r->m_TableName, 
                                                   r->m_Filter,
                                                   r->m_Sort,
                                                   req.m_StartingRank,
                                                   r->m_PostSortFilter,
                                                   req.m_Radius,
                                                   req.m_CacheResults,
                                                   //(const char**)r->m_FieldNames,
                                                   r->m_Results.GetMaxRecords(),
                                                   &r->m_Results,
                                                   &r->m_Status));

        QueueRequest(r);
    }
    rcatchall
    {
        if(r) delete r;
        return false;
    }  
    return true;
}

bool
rlDbProxy::ReceiveRequestReadRandomRecord(ClientRec* c, const rlDbMsgReadRandomRecordRequest& req)
{
    Assert(c);

    RLCHECK_SET_CTX(ReceiveRequestReadRandomRecord);

    ReadRandomRecordRequest* r = 0;

    rtry
    {
        RLCHECK_CTX(0 != (r = rage_new ReadRandomRecordRequest(c->m_ClientId)));

        RLCHECK_CTX(r->Init(req.m_TableName, 
                            req.m_Filter,
                            (const char**)req.m_FieldNames,
                            req.m_NumFieldNames));

        RLCHECK_CTX(m_DbClient.ReadRandomRecord(r->m_TableName, 
                                                        r->m_Filter,
                                                        (const char**)r->m_FieldNames,
                                                        &r->m_Results,
                                                        &r->m_Status));

        QueueRequest(r);
    }
    rcatchall
    {
        if(r) delete r;
        return false;
    }  
    return true;
}

bool
rlDbProxy::ReceiveRequestUploadFile(ClientRec* c, const rlDbMsgUploadFileRequest& req)
{
    Assert(c);

    RLCHECK_SET_CTX(ReceiveRequestUploadFile);

    UploadFileRequest* r = 0;

    rtry
    {
        RLCHECK_CTX(0 != (r = rage_new UploadFileRequest(c->m_ClientId)));
        RLCHECK_CTX(r->Init(req.m_FileData, req.m_FileSize));
        RLCHECK_CTX(m_DbClient.UploadFile(r->m_FileData, 
                                                  r->m_FileSize, 
                                                  &r->m_Progress,
                                                  &r->m_FileId,
                                                  &r->m_Status));

        QueueRequest(r);
    }
    rcatchall
    {
        if(r) delete r;
        return false;
    }  
    return true;
}

bool
rlDbProxy::ReceiveRequestDownloadFile(ClientRec* c, const rlDbMsgDownloadFileRequest& req)
{
    Assert(c);

    RLCHECK_SET_CTX(ReceiveRequestDownloadFile);

    DownloadFileRequest* r = 0;

    rtry
    {
        RLCHECK_CTX(0 != (r = rage_new DownloadFileRequest(c->m_ClientId)));
        RLCHECK_CTX(r->Init(req.m_FileId, req.m_BufSize));
        RLCHECK_CTX(m_DbClient.DownloadFile(r->m_FileId, 
                                                    r->m_Buf, 
                                                    r->m_BufSize, 
                                                    &r->m_Progress,
                                                    &r->m_NumBytesDownloaded, 
                                                    &r->m_Status));

        QueueRequest(r);
    }
    rcatchall
    {
        if(r) delete r;
        return false;
    }  
    return true;
}
*/
void
rlDbProxy::ProcessCompletedRequest(Request* r)
{
    Assert(r);

    RLCHECK_SET_CTX(ProcessCompletedRequest);

    //Record the time it took to handle the request (minus any time it took to send email in previous frame).
    unsigned responseMs = m_CurTime - r->m_StartTime - m_EmailFixupMs;
    m_DayStats.m_HourStats[m_CurHour].m_ResponseMs.AddSample(responseMs);
    m_DayStats.m_RequestStats[r->m_Type].m_ResponseMs.AddSample(responseMs);

    //Find the client that made the request
    ClientMap::iterator it = m_ClientMap.find(r->m_ClientId);
    if(m_ClientMap.end() == it)
    {
        return; //Client no longer connected; ignore
    }

    ClientRec* c = it->second;
    Assert(c);

    //Mark client as no longer performing this request
    --c->m_NumRequestsInProgress;
    Assert(c->m_NumRequestsInProgress >= 0);

    //Handle based on the request type
#define REQUEST_CASE(name, type) \
    case( REQUEST_##type ): \
        rlDebug2("%s: " #name " completed (%s) for %d.%d.%d.%d:%d", \
                  s_rlCheckContext, \
                  r->m_Status.Succeeded() ? "success" : "failed", \
                  NET_ADDR_FOR_PRINTF(c->m_Address)); \
        if(!SendReply##name##(c, (name##Request*)r)) \
        { \
            rlError("rlDbProxy::ProcessCompletedRequest: Failed to send " #name " reply to %d.%d.%d.%d:%d; disconnecting...", \
                     NET_ADDR_FOR_PRINTF(c->m_Address)); \
            RemoveClient(c->m_ClientId); \
        } \
        break;

    switch(r->m_Type)
    {
    REQUEST_CASE(CreateRecord, CREATE_RECORD);
    REQUEST_CASE(UpdateRecord, UPDATE_RECORD);
    REQUEST_CASE(DeleteRecord, DELETE_RECORD);
    REQUEST_CASE(RateRecord, RATE_RECORD);
    REQUEST_CASE(GetNumRecords, GET_NUM_RECORDS);
    REQUEST_CASE(ReadRecords, READ_RECORDS);
    REQUEST_CASE(ReadRandomRecord, READ_RANDOM_RECORD);
    REQUEST_CASE(UploadFile, UPLOAD_FILE);
    REQUEST_CASE(DownloadFile, DOWNLOAD_FILE);
    default:
        rlError("Unhandled request case (%d)", r->m_Type);
    };

#undef REQUEST_CASE
}

bool
rlDbProxy::SendToClient(ClientRec* c, 
                            const void* data,
                            const unsigned size)
{
    Assert(c && data && size);

    OutboundMsg* m = rage_new OutboundMsg();
    
    if(!m)
    {
        rlError("rlDbProxy::SendToClient: Failed to allocate OutboundMsg");
        return false;
    }

    if(!m->Init(data, size))
    {
        rlError("rlDbProxy::SendToClient: Failed to Init() OutboundMsg (%d bytes)", size);
        return false;
    }

    c->m_OutboundMsgList.push_back(m);

    return true;
}

#define RLDATABASEPROXY_SEND(c, msg, type, id) \
    unsigned maxSize = msg.GetExportByteSize(); \
    char* buf = rage_new char[maxSize]; \
    if(!buf) \
    {  \
        rlError("RLDATABASEPROXY_SEND(" #type "): Failed to allocate %d bytes", maxSize); \
        return false; \
    } \
    rlDebug3("Allocated %u bytes for send buffer", maxSize); \
    unsigned size = 0; \
    bool success = true; \
    if(!msg.Export(buf, maxSize, &size)) \
    { \
        rlError("RLDATABASEPROXY_SEND(" #type "): Export failed"); \
        success = false; \
    } \
    if(success && !SendToClient(c, buf, size)) \
    { \
        rlError("RLDATABASEPROXY_SEND(" #type "): SendToClient failed"); \
        success = false; \
    } \
    delete [] buf; \
    if(REQUEST_INVALID != REQUEST_##id ) m_DayStats.m_ReplyStats[ REQUEST_##id ].m_Size.AddSample(size); \
    return success;

bool
rlDbProxy::SendProgressMsg(const unsigned clientId,
                                 const float progress)
{
    ClientMap::iterator it = m_ClientMap.find(clientId);
    if(m_ClientMap.end() == it)
    {
        return true; //Client no longer connected; ignore
    }

    ClientRec* c = it->second;
    Assert(c);

    rlDbMsgProgress msg;
    msg.Reset(progress);

    RLDATABASEPROXY_SEND(c, msg, rlDbMsgProgress, INVALID);
}

bool
rlDbProxy::SendReplyCreateRecord(ClientRec* c, CreateRecordRequest* r)
{
    rlDbMsgCreateRecordReply msg;
    msg.Reset(r->m_Status.Succeeded(), r->m_Status.GetResultCode(), r->m_RecordId);

    RLDATABASEPROXY_SEND(c, msg, rlDbMsgCreateRecordReply, CREATE_RECORD);
}

bool
rlDbProxy::SendReplyUpdateRecord(ClientRec* c, UpdateRecordRequest* r)
{
    rlDbMsgUpdateRecordReply msg;
    msg.Reset(r->m_Status.Succeeded(), r->m_Status.GetResultCode());

    RLDATABASEPROXY_SEND(c, msg, rlDbMsgUpdateRecordReply, UPDATE_RECORD);
}

bool
rlDbProxy::SendReplyDeleteRecord(ClientRec* c, DeleteRecordRequest* r)
{
    rlDbMsgDeleteRecordReply msg;
    msg.Reset(r->m_Status.Succeeded(), r->m_Status.GetResultCode());

    RLDATABASEPROXY_SEND(c, msg, rlDbMsgDeleteRecordReply, DELETE_RECORD);
}

bool
rlDbProxy::SendReplyRateRecord(ClientRec* c, RateRecordRequest* r)
{
    rlDbMsgRateRecordReply msg;
    msg.Reset(r->m_Status.Succeeded(), r->m_Status.GetResultCode(), r->m_NumRatings, r->m_AvgRating);

    RLDATABASEPROXY_SEND(c, msg, rlDbMsgRateRecordReply, RATE_RECORD);
}

bool
rlDbProxy::SendReplyGetNumRecords(ClientRec* c, GetNumRecordsRequest* r)
{
    rlDbMsgGetNumRecordsReply msg;    
    msg.Reset(r->m_Status.Succeeded(), r->m_Status.GetResultCode(), r->m_NumRecords);

    RLDATABASEPROXY_SEND(c, msg, rlDbMsgGetNumRecordsReply, GET_NUM_RECORDS);
}

bool
rlDbProxy::SendReplyReadRecords(ClientRec* c, ReadRecordsRequest* r)
{
    rlDbMsgReadRecordsReply msg;    
    msg.ResetForExport(r->m_Status.Succeeded(), r->m_Status.GetResultCode(), &r->m_Results);

    RLDATABASEPROXY_SEND(c, msg, rlDbMsgReadRecordsReply, READ_RECORDS);
}

bool
rlDbProxy::SendReplyReadRandomRecord(ClientRec* c, ReadRandomRecordRequest* r)
{
    rlDbMsgReadRecordsReply msg;    
    msg.ResetForExport(r->m_Status.Succeeded(), r->m_Status.GetResultCode(), &r->m_Results);

    RLDATABASEPROXY_SEND(c, msg, rlDbMsgReadRecordsReply, READ_RANDOM_RECORD);
}

bool
rlDbProxy::SendReplyUploadFile(ClientRec* c, UploadFileRequest* r)
{
    rlDbMsgUploadFileReply msg;    
    msg.Reset(r->m_Status.Succeeded(), r->m_Status.GetResultCode(), r->m_FileId);

    RLDATABASEPROXY_SEND(c, msg, rlDbMsgUploadFileReply, UPLOAD_FILE);
}

bool
rlDbProxy::SendReplyDownloadFile(ClientRec* c, DownloadFileRequest* r)
{
    rlDbMsgDownloadFileReply msg(r->m_Buf, r->m_Msg.m_BufSize);
    
    Assert(!r->m_Status.Succeeded() || (r->m_Msg.m_BufSize >= r->m_NumBytesDownloaded));

    msg.Reset(r->m_Status.Succeeded(), r->m_Status.GetResultCode(), r->m_NumBytesDownloaded);

    RLDATABASEPROXY_SEND(c, msg, rlDbMsgDownloadFileReply, DOWNLOAD_FILE);
}

bool
rlDbProxy::CreateServerSocket()
{
    RLCHECK_SET_CTX(CreateServerSocket);

    rtry
    {
        SYS_CS_SYNC(m_CsServerSocket);

        RLCHECK_CTX(-1 != (m_ServerSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)));

        unsigned titleserverport = DEFAULT_TITLE_SERVER_PORT;
        if(!PARAM_titleserverport.Get(titleserverport))
        {
            rlDebug2("%s: titleserverport parameter not specified; using default (%d)", titleserverport);
        }

        sockaddr_in addr;
        sysMemSet(&addr, 0, sizeof(addr));
        addr.sin_family         = AF_INET;
        addr.sin_addr.s_addr    = INADDR_ANY;
        addr.sin_port           = htons((unsigned short)titleserverport);

        rcheck(SOCKET_ERROR != bind(m_ServerSocket, (struct sockaddr*)&addr, sizeof(addr)),
               catchall,
               rlError("%s: Failed to bind server socket to port %d (error=%d)", 
                        s_rlCheckContext, titleserverport, WSAGetLastError()));

        u_long nonblock = 1;
        rcheck(0 == ioctlsocket(m_ServerSocket, FIONBIO, &nonblock),
               catchall,
               rlError("Failed to set socket to non-blocking"));

        const int LISTEN_BACKLOG = 500; //This will automatically get capped by underlying code.
        rcheck(SOCKET_ERROR != listen(m_ServerSocket, LISTEN_BACKLOG),
               catchall,
               rlError("%s: Failed to listen() (error=%d)", s_rlCheckContext, WSAGetLastError()));

        rlDebug2("%s: Server listening for connections on port %d", s_rlCheckContext, titleserverport);
    }
    rcatchall
    {
        return false;
    }
    return true;
}

bool
rlDbProxy::StartAcceptThread()
{
    RLCHECK_SET_CTX(StartAcceptThread);

    Assert(!m_WaitSema);
    Assert(sysIpcThreadIdInvalid == m_ThreadHandle);

    rtry
    {
        m_WaitSema = sysIpcCreateSema(0);

        RLCHECK_CTX(m_WaitSema);

        m_ThreadHandle = sysIpcCreateThread(&rlDbProxy::AcceptThreadFunc,
                                            this,
                                            sysIpcMinThreadStackSize,
                                            PRIO_NORMAL,
                                            "rlDbProxy::AcceptThreadFunc");

        RLCHECK_CTX(sysIpcThreadIdInvalid != m_ThreadHandle);

        //Wait for the thread to start.
        sysIpcWaitSema(m_WaitSema);
    }
    rcatchall
    {
        return false;
    }
    return true;
}

void
rlDbProxy::AcceptThreadFunc(void *userdata)
{
    rlDebug2("rlDbProxy::AcceptThreadFunc() starting...");

    rlDbProxy* p = (rlDbProxy*)userdata;
    Assert(p);

    sysIpcSignalSema(p->m_WaitSema);

    unsigned sleepms = DEFAULT_ACCEPT_SLEEP_MS;
    PARAM_acceptsleepms.Get(sleepms);

    while(1)
    {
        SYS_CS_SYNC(p->m_CsServerSocket);

        if(p->m_ServerSocket != INVALID_SOCKET)
        {
            sockaddr_in addr;
            sysMemSet(&addr, 0, sizeof(addr));

            int size = sizeof(addr);

            SOCKET s = accept(p->m_ServerSocket, (struct sockaddr*)&addr, &size);

            if(INVALID_SOCKET != s)
            {
                SYS_CS_SYNC(p->m_CsAcceptedList);
                
                AcceptedConnection* a = rage_new AcceptedConnection();
                a->m_Socket  = s;
                a->m_Address = netAddress(ntohl(addr.sin_addr.s_addr), ntohs(addr.sin_port));

                //rlDebug2("rlDbProxy::AcceptThreadFunc(): Accepted connection from %d.%d.%d.%d:%d",
                //          NET_ADDR_FOR_PRINTF(a->m_Address));

                static unsigned s_id = 0;
                while(++s_id == INVALID_CLIENTID);
                a->m_ClientId = s_id;
                
                p->m_AcceptedList.push_back(a);
            }
            else if(WSAEWOULDBLOCK == WSAGetLastError())
            {
                sysIpcSleep(sleepms);
            }
            else
            {
                rlDebug2("Error accepting connection (%d)", WSAGetLastError());
            }
        }
        else
        {
            break; //Server socket closed, shuttting down.
        }
    }
}

void
rlDbProxy::CheckNewConnections()
{
    //Create client records for each accepted connection.
    SYS_CS_SYNC(m_CsAcceptedList);

    if(!m_AcceptedList.empty())
    {
        //rlDebug2("Accepting %d connections...", m_AcceptedList.size());

        HourStats& hs = m_DayStats.m_HourStats[m_CurHour];
        hs.m_NumNewConnections.AddSample(m_AcceptedList.size());

        while(!m_AcceptedList.empty())
        {
            AcceptedConnection* a = *m_AcceptedList.begin();
            m_AcceptedList.pop_front();

            ClientMap::iterator it = m_ClientMap.find(a->m_ClientId);

            if(m_ClientMap.end() != it)
            {
                rlError("Got connection from client already in our client map. Removing previous client.");            
                ClientRec *c = it->second;
                m_ClientMap.erase(it);
                delete c;
            }

            ClientRec* c = rage_new ClientRec();

            if(c)
            {
                c->m_ClientId       = a->m_ClientId;
                c->m_Socket         = a->m_Socket;
                c->m_Address        = a->m_Address;
                c->m_LastActiveTime = m_CurTime;
                
                m_ClientMap.insert(c->m_ClientId, c);

                rlDebug2("Created clientrec (ID=%d) for new connection %d.%d.%d.%d:%d",
                          c->m_ClientId, NET_ADDR_FOR_PRINTF(c->m_Address));
            }
            else
            {
                rlError(("Failed to allocate ClientRec; closing connection"));
                closesocket(a->m_Socket);
            }

            delete a;
        }
    }

    Assert(m_AcceptedList.empty());
}

void
rlDbProxy::CheckNewRequests()
{
    if(m_ClientMap.empty())
    {
        return; //No sockets to check
    }

    rtry
    {
        //Select sockets that are readable (or exceptioned)
        fd_set readfds;
        FD_ZERO(&readfds);

        fd_set exceptionfds;
        FD_ZERO(&exceptionfds);

        SOCKET highestSocket = 0;
        
        ClientMap::iterator it = m_ClientMap.begin();
        while(it != m_ClientMap.end())
        {
            ClientRec* c = it->second;
            Assert(c);

            if(c->m_Socket != INVALID_SOCKET)
            {
                FD_SET(c->m_Socket, &readfds);
                FD_SET(c->m_Socket, &exceptionfds);

                if(c->m_Socket > highestSocket)
                {
                    highestSocket = c->m_Socket;
                }
            }
            else
            {
                rlError("Client %d.%d.%d.%d:%d has invalid socket; removing client...",
                         NET_ADDR_FOR_PRINTF(c->m_Address));
                it = RemoveClient(c->m_ClientId);
                rcheck(!m_ClientMap.empty(), catchall, ); //Our last client was removed
                continue;
            }

            ++it;
        }

        timeval t;
        t.tv_sec  = 0;
        t.tv_usec = 100000;

        int result = select(highestSocket, &readfds, NULL, &exceptionfds, &t);

        rcheck(-1 != result, catchall, rlError("select() error (%d)", WSAGetLastError()));
        rcheck(result, catchall, ); //Timeout, no biggie

        //Handle the reads and exceptions
        it = m_ClientMap.begin();
        while(it != m_ClientMap.end())
        {
            ClientRec* c = it->second;
            Assert(c);

            if(FD_ISSET(c->m_Socket, &exceptionfds))
            {
                rlError("Client %d.%d.%d.%d:%d had socket exception (%d); removing client...",
                         NET_ADDR_FOR_PRINTF(c->m_Address),
                         WSAGetLastError());
                it = RemoveClient(c->m_ClientId);
            }

            if(FD_ISSET(c->m_Socket, &readfds))
            {
                //rlDebug2("Socket is readable for client %d [%d.%d.%d.%d:%d]",
                //          c->m_ClientId, NET_ADDR_FOR_PRINTF(c->m_Address));

                if(!ReceiveFromClient(c))
                {
                    //Client disconnected, or there was read error
                    it = RemoveClient(c->m_ClientId);
                    continue;
                }
            }

            ++it;
        }
    }
    rcatchall
    {
    }
}

bool
rlDbProxy::ReceiveFromClient(ClientRec* c)
{
    Assert(c);

    //Have we read the msg size?  If not, continue reading it.
    if(c->m_MsgSizeBytesRead < sizeof(c->m_MsgSize))
    {
        //Continue reading the message size
        int bytesRead = recv(c->m_Socket, 
                             &(((char*)&c->m_MsgSize)[c->m_MsgSizeBytesRead]),
                             sizeof(c->m_MsgSize) - c->m_MsgSizeBytesRead,
                             0);

        if(0 < bytesRead)
        {
            c->m_MsgSizeBytesRead += bytesRead;
            m_IncomingBytesAccum += bytesRead;
        }
        else if(0 == bytesRead)
        {
            rlDebug2("Client %d.%d.%d.%d:%d disconnected", NET_ADDR_FOR_PRINTF(c->m_Address));
            return false;
        }
        else
        {
            rlDebug2("Client %d.%d.%d.%d:%d had error reading msg size (err=%d, %d of %d bytes)", 
                      NET_ADDR_FOR_PRINTF(c->m_Address),
                      WSAGetLastError(), 
                      c->m_MsgSizeBytesRead, 
                      sizeof(c->m_MsgSize));
            return false;
        }

        //If still not done, return.
        if(c->m_MsgSizeBytesRead < sizeof(c->m_MsgSize))
        {
            return true;
        }

        c->m_MsgSize = ntohl(c->m_MsgSize);

        if(!c->m_MsgSize)
        {
            rlDebug2("Client %d.%d.%d.%d:%d msg has zero size; discarding", 
                NET_ADDR_FOR_PRINTF(c->m_Address),
                c->m_MsgSize);
            return false;
        }

        if(c->m_MsgSize > s_MaxMsgSize)
        {
            rlDebug2("Client %d.%d.%d.%d:%d msg too big (%u bytes, %u is max)", 
                      NET_ADDR_FOR_PRINTF(c->m_Address),
                      c->m_MsgSize,
                      s_MaxMsgSize);
            return false;
        }

        Assert(!c->m_MsgBuf);
        Assert(!c->m_MsgBytesRead);

        //rlDebug2("Got msg size: %u", c->m_MsgSize);

        return true; //One recv() per call
    }

    Assert(c->m_MsgSizeBytesRead == sizeof(c->m_MsgSize));

    //Allocate our message buffer, if necessary
    if(!c->m_MsgBuf)
    {
        Assert(c->m_MsgSize <= s_MaxMsgSize);

        c->m_MsgBuf = rage_new u8[c->m_MsgSize];

        if(!c->m_MsgBuf)
        {
            rlError("Failed to allocated message buffer (%d bytes)", c->m_MsgSize);
            return false;
        }
    }

    Assert(c->m_MsgBuf);
    Assert(c->m_MsgBytesRead < c->m_MsgSize);

    //Continue reading the message
    int bytesRead = recv(c->m_Socket, 
                         (char*)&c->m_MsgBuf[c->m_MsgBytesRead],
                         c->m_MsgSize - c->m_MsgBytesRead,
                         0);

    if(0 < bytesRead)
    {
        c->m_MsgBytesRead += bytesRead;
        m_IncomingBytesAccum += bytesRead;
    }
    else if(0 == bytesRead)
    {
        rlDebug2("Client %d.%d.%d.%d:%d disconnected while reading msg (%d of %d bytes)", 
                  NET_ADDR_FOR_PRINTF(c->m_Address),
                  c->m_MsgBytesRead, 
                  c->m_MsgSize);
        return false;
    }
    else
    {
        rlDebug2("Client %d.%d.%d.%d:%d had error while reading msg (err=%d, %d of %d bytes)", 
                  NET_ADDR_FOR_PRINTF(c->m_Address),
                  WSAGetLastError(), 
                  c->m_MsgBytesRead, 
                  c->m_MsgSize);
        return false;
    }

    Assert(c->m_MsgBytesRead <= c->m_MsgSize);

    //If done...
    if(c->m_MsgBytesRead == c->m_MsgSize)
    {
        //rlDebug2("Received msg from %d.%d.%d.%d:%d (%u bytes)", 
        //          NET_ADDR_FOR_PRINTF(c->m_Address),
        //          c->m_MsgBytesRead);

        //Handle the message
        ReceiveFromClient(c, c->m_MsgBuf, c->m_MsgSize);

        //Reset message reading
        delete [] c->m_MsgBuf;
        c->m_MsgBuf = 0;
        c->m_MsgBytesRead = 0;
        c->m_MsgSize = 0;
        c->m_MsgSizeBytesRead = 0;
    }

    return true;
}

void 
rlDbProxy::SendToClients()
{
    if(m_ClientMap.empty())
    {
        return;
    }

    ClientMap::iterator it = m_ClientMap.begin();
    while(it != m_ClientMap.end())
    {
        ClientRec* c = it->second;
        Assert(c);

        //If client somehow has an invalid socket, then remove them.
        if(c->m_Socket == INVALID_SOCKET)
        {
            it = RemoveClient(c->m_ClientId);
            continue;
        }

        //If client has something to send and their socket is writable, then continue sending.
        if(!c->m_OutboundMsgList.empty() && WaitForSocket(c->m_Socket, false, 0))
        {
            if(!SendToClient(c))
            {
                it = RemoveClient(c->m_ClientId);
                continue;
            }
        }

        ++it;
    }
}

bool
rlDbProxy::SendToClient(ClientRec* c)
{
    Assert(!c->m_OutboundMsgList.empty());

    OutboundMsg* m = *c->m_OutboundMsgList.begin();
    Assert(m);

    //Have we written the msg size?  If not, continue writing it.
    if(c->m_MsgBytesSent < sizeof(m->m_MsgSize))
    {
        u32 msgSize = htonl(m->m_MsgSize);
        Assert(sizeof(m->m_MsgSize) == sizeof(msgSize));

        int bytesSent = send(c->m_Socket, 
                             &(((char*)&msgSize)[c->m_MsgBytesSent]),
                             sizeof(msgSize) - c->m_MsgBytesSent,
                             0);

        if(0 < bytesSent)
        {
            c->m_MsgBytesSent += bytesSent;
            m_OutgoingBytesAccum += bytesSent;
        }
        else
        {
            rlDebug2("Client %d.%d.%d.%d:%d had error sending msg size (err=%d, %d of %d bytes)", 
                      NET_ADDR_FOR_PRINTF(c->m_Address),
                      WSAGetLastError(), c->m_MsgBytesSent, m->m_MsgSize);
            return false;
        }

        //If still not done, return.
        if(c->m_MsgBytesSent < sizeof(msgSize))
        {
            return true;
        }

        //rlDebug2("Wrote msg size: %u", m->m_MsgSize);

        return true; //One send() per call
    }

    Assert(m->m_MsgBuf);
    Assert(c->m_MsgBytesSent < (m->m_MsgSize + sizeof(m->m_MsgSize)));

    //Continue reading the message
    int offset = c->m_MsgBytesSent - sizeof(m->m_MsgSize);
    int bytesSent = send(c->m_Socket, 
                         (char*)&m->m_MsgBuf[offset],
                         m->m_MsgSize - offset,
                         0);

    if(0 < bytesSent)
    {
        c->m_MsgBytesSent += bytesSent;
        m_OutgoingBytesAccum += bytesSent;
    }
    else
    {
        rlDebug2("Client %d.%d.%d.%d:%d had error while sending msg (err=%d, %d of %d bytes)", 
                  NET_ADDR_FOR_PRINTF(c->m_Address),
                  WSAGetLastError(), offset, m->m_MsgSize);
        return false;
    }

    Assert(c->m_MsgBytesSent <= (m->m_MsgSize + sizeof(m->m_MsgSize)));

    //If done...
    if(c->m_MsgBytesSent == (m->m_MsgSize + sizeof(m->m_MsgSize)))
    {
        rlDebug3("Sent msg to client %d.%d.%d.%d:%d (%u bytes)",
                  NET_ADDR_FOR_PRINTF(c->m_Address),
                  c->m_MsgBytesSent - sizeof(m->m_MsgSize));

        c->m_OutboundMsgList.erase(c->m_OutboundMsgList.begin());
        delete m;
        c->m_MsgBytesSent = 0;
    }

    return true;
}

bool
rlDbProxy::WaitForSocket(SOCKET s, 
                               const bool forRead, //if false, it's for writing
                               const unsigned timeoutSecs)
{
    rtry
    {
        fd_set fds;
        fd_set exceptionfds;

        FD_ZERO(&fds);
        FD_SET(s, &fds);

        FD_ZERO(&exceptionfds);
        FD_SET(s, &exceptionfds);

        timeval t;
        t.tv_sec  = timeoutSecs;
        t.tv_usec = 0;

        int result = select(s+1, forRead ? &fds : NULL, forRead ? NULL : &fds, &exceptionfds, &t);

        rcheck(-1 != result, catchall, rlError("select() error (%d)", WSAGetLastError()));
//        rcheck(result, catchall, rlDebug2("select() timed out after %d seconds", t.tv_sec));
        rcheck(!FD_ISSET(s, &exceptionfds), catchall, rlError("select() found exception on our socket"));
        rcheck(FD_ISSET(s, &fds), catchall, rlError(("select() returned sockets, but our socket is not one of them; most likely an error")));
    }
    rcatchall 
    {
        return false;
    }
    return true;
}

void
rlDbProxy::UpdateStats()
{
    //Get frame time
    unsigned frameMs = 0;

    static unsigned s_LastTime = 0;
    if(s_LastTime)
    {
        frameMs = m_CurTime - s_LastTime;
    }
    s_LastTime = m_CurTime;

    //Add to uptime
    m_UptimeMs += frameMs;

    //Update hour stats
    HourStats& hs = m_DayStats.m_HourStats[m_CurHour];
    hs.m_HeapUsed.AddSample(m_Allocator->GetMemoryUsed(-1));
    hs.m_FrameMs.AddSample(frameMs);
    hs.m_NumConcurrent.AddSample(m_ClientMap.size());

    const unsigned BW_PERIOD_MS = 1000;
    static unsigned s_LastBwTime = 0;
    unsigned bwDeltaMs = m_CurTime - s_LastBwTime;
    if(bwDeltaMs > BW_PERIOD_MS)
    {
        if(s_LastBwTime)
        {
            hs.m_IncomingKBps.AddSample((bwDeltaMs * m_IncomingBytesAccum / 1000.0f)/1000);
            m_IncomingBytesAccum = 0;

            hs.m_OutgoingKBps.AddSample((bwDeltaMs * m_OutgoingBytesAccum / 1000.0f)/1000);
            m_OutgoingBytesAccum = 0;
        }

        s_LastBwTime = m_CurTime;
    }

    //Advance hour, and send daily report if necessary.
    SYSTEMTIME t;
    GetLocalTime(&t);

    if(m_CurHour != t.wHour)
    {
        m_CurHour = t.wHour;

        //If it is the daily email hour, then email a stats report and flush all stats.
        if(m_CurHour == m_DailyEmailHour)
        {
            ReportStats();
            m_DayStats = DayStats();
        }
    }

    //If we are configured to send a periodic report (like for stress testing),
    //see if it is time to send.
    if(m_MicroReportMs)
    {
        if(frameMs > m_MicroReportTimer)
        {
            ReportStats();
            m_DayStats = DayStats();
            m_MicroReportTimer = (unsigned)m_MicroReportMs;
        }
        else
        {
            m_MicroReportTimer -= frameMs;
        }
    }
}

void
rlDbProxy::PrintRequestStats(char* buf, 
                                 const unsigned sizeofBuf,
                                 const char* label,
                                 RequestStats* r)
{
    Assert(buf && sizeofBuf && label && r);

    formatf(buf, sizeofBuf, "%s   num=%u", label, r->m_Size.GetNumSamples());

    char catBuf[256];

    if(r->m_Size.GetNumSamples())
    {
        for(int i = 0; i < RLDB_NUM_RESULT_CODES; i++)
        {
            if(r->m_NumResults[i])
            {
                formatf(catBuf, sizeof(catBuf), 
                        " r%d=%0.0f%%(%u)",
                        i, ((float)r->m_NumResults[i]/r->m_Size.GetNumSamples()) * 100.0f, r->m_NumResults[i]);

                safecat(buf, catBuf, sizeofBuf);
            }
        }
    }

    formatf(catBuf, sizeof(catBuf), " avgSize=%u  maxSize=%u  minMs=%u  maxMs=%u  avgMs=%u",
            unsigned(r->m_Size.GetAvg()),
            r->m_Size.GetMax(),
            r->m_ResponseMs.GetMin(),
            r->m_ResponseMs.GetMax(),
            (unsigned)r->m_ResponseMs.GetAvg());

    safecat(buf, catBuf, sizeofBuf);
}

void
rlDbProxy::ReportStats()
{
    char buf[1024];
    memset(buf, 0, sizeof(buf));
    
    char reportBuf[10000];
    memset(reportBuf, 0, sizeof(reportBuf));

#define REPORTCAT(text) \
    rlDebug2("%s", text); \
    safecat(reportBuf, text, sizeof(reportBuf)); \
    safecat(reportBuf, "\n", sizeof(reportBuf));

    float secs = m_UptimeMs/1000.0f;
    formatf(buf, sizeof(buf), "UPTIME: %02uD %02uH %02uM %02.0fS", 
            unsigned(secs/(24*60*60)),
            unsigned(secs/(60*60))%24,
            unsigned(secs/60)%60,
            fmodf(secs, 60.0f));
    REPORTCAT(buf);

    //--------------------------------------------------------------------------
    //Per-request stats
    //--------------------------------------------------------------------------
    formatf(buf, sizeof(buf), "-------- STATS BY REQUEST TYPE ---------");
    REPORTCAT(buf);

    RequestStats* reqStats = 0;
#define REPORT_REQUEST_STATS(label, type)\
    reqStats = &m_DayStats.m_RequestStats[ REQUEST_##type ]; \
    PrintRequestStats(buf, sizeof(buf), label, reqStats); \
    REPORTCAT(buf);

    //REPORT_REQUEST_STATS("GetTableSize      ", GET_TABLE_SIZE);
    //REPORT_REQUEST_STATS("GetNumOwned       ", GET_NUM_OWNED);
    //REPORT_REQUEST_STATS("ReadRecordsByRank ", READ_RECORDS_BY_RANK);
    //REPORT_REQUEST_STATS("ReadRecordsByGamer", READ_RECORDS_BY_GAMER);
    //REPORT_REQUEST_STATS("ReadRandomRecord  ", READ_RANDOM_RECORD);
    //REPORT_REQUEST_STATS("ReadRecord        ", READ_RECORD);
    //REPORT_REQUEST_STATS("RateRecord        ", RATE_RECORD);
    REPORT_REQUEST_STATS("DeleteRecord      ", DELETE_RECORD);
    //REPORT_REQUEST_STATS("UploadFile        ", UPLOAD_FILE);
    //REPORT_REQUEST_STATS("OverwriteFile     ", OVERWRITE_FILE);
    //REPORT_REQUEST_STATS("DownloadFile      ", DOWNLOAD_FILE);
    //REPORT_REQUEST_STATS("OverwriteUserData ", OVERWRITE_USER_DATA);
    REPORT_REQUEST_STATS("GetNumRecords     ", GET_NUM_RECORDS);

#undef REPORT_REQUEST_STATS

    //--------------------------------------------------------------------------
    //Per-reply stats
    //--------------------------------------------------------------------------
    formatf(buf, sizeof(buf), "-------- STATS BY REPLY TYPE ---------");
    REPORTCAT(buf);

    ReplyStats* replyStats = 0;
#define REPORT_REPLY_STATS(label, type)\
    replyStats = &m_DayStats.m_ReplyStats[ REQUEST_##type ]; \
    formatf(buf, sizeof(buf), "%s   num=%u  avgSize=%u  maxSize=%u", \
            label, \
            replyStats->m_Size.GetNumSamples(), \
            unsigned(replyStats->m_Size.GetAvg()), \
            replyStats->m_Size.GetMax()); \
    REPORTCAT(buf);

    REPORT_REPLY_STATS("CreateRecord      ", CREATE_RECORD);
    REPORT_REPLY_STATS("UpdateRecord      ", UPDATE_RECORD);
    REPORT_REPLY_STATS("DeleteRecord      ", DELETE_RECORD);
    REPORT_REPLY_STATS("RateRecord        ", RATE_RECORD);
    REPORT_REPLY_STATS("GetNumRecords     ", GET_NUM_RECORDS);
    REPORT_REPLY_STATS("ReadRecords       ", READ_RECORDS);
    REPORT_REPLY_STATS("ReadRandomRecord  ", READ_RANDOM_RECORD);
    REPORT_REPLY_STATS("UploadFile        ", UPLOAD_FILE);
    REPORT_REPLY_STATS("DownloadFile      ", DOWNLOAD_FILE);

#undef REPORT_REPLY_STATS

    //--------------------------------------------------------------------------
    //Per-hour stats
    //--------------------------------------------------------------------------
    formatf(buf, sizeof(buf), "-------- HOURLY STATS ---------");
    REPORTCAT(buf);

    for(unsigned i = 0; i < 24; i++)
    {
        HourStats& hs = m_DayStats.m_HourStats[i];

        if(hs.m_NumRequests || hs.m_HeapUsed.GetMax())
        {
            formatf(buf, sizeof(buf), "%02u00 - %02u00:", i, i+1);
            REPORTCAT(buf);

#define PRINT_STAT(label, statName) \
            formatf(buf, sizeof(buf), "    %s  min=%u  max=%u  avg=%0.1f", \
                    label, (unsigned)hs.m_##statName .GetMin(), (unsigned)hs.m_##statName .GetMax(), hs.m_##statName .GetAvg()); \
            REPORTCAT(buf);

            PRINT_STAT("HeapUsed         ", HeapUsed);
            PRINT_STAT("FrameMs          ", FrameMs);
            PRINT_STAT("NumNewConnections", NumNewConnections);
            PRINT_STAT("NumConcurrent    ", NumConcurrent);
            PRINT_STAT("IncomingKBps     ", IncomingKBps);
            PRINT_STAT("OutgoingKBps     ", OutgoingKBps);
            PRINT_STAT("ResponseMs       ", ResponseMs);

            formatf(buf, sizeof(buf), "    NumRequests        %u", hs.m_NumRequests);
            REPORTCAT(buf);
        }
    }

    unsigned emailStartTime = sysTimer::GetSystemMsTime();

    SendAdminEmail("Daily stats report", reportBuf);

    m_EmailFixupMs = sysTimer::GetSystemMsTime() - emailStartTime;
}

#endif

