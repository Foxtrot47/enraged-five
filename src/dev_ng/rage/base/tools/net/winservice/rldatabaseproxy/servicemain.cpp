// servicemain.cpp 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 

#include "servicemain.h"
#include "serverdiag.h"

#if !__WIN32PC || !__WIN32SERVICE
#include "system/main.h"
#endif

#include "servicemsg.h"
#include "diag/email.h"
#include "diag/channel.h"
#include "string/string.h"
#include "system/ipc.h"
#include "system/memory.h"
#include "system/param.h"
#include "system/stack.h"
#include "system/timer.h"
#include "system/xtl.h"
#include <stdio.h>

using namespace rage;

//Macros for structured error handling, copied from netdiag.h
#ifndef rtry
    #define rtry
    #define rthrow( dest, action ) do{ action; }while( 0 ); goto rexcept_##dest;
    #define rcheck( cond, dest, action )\
        do{ if( !( cond ) ){ rthrow( dest, action ); } }while( 0 );
    #define rcatch( label )\
        while( 0 )\
        rexcept_##label:
    #define rcatchall rcatch( catchall )
#endif //#ifndef rtry

const unsigned DEFAULT_SLEEP_MS = 10;
const unsigned DEFAULT_MAX_SIZEOF_LOG           = 1024;
const unsigned DEFAULT_LOG_HISTORY_SIZE         = 0;
const unsigned DEFAULT_DEBUG_LEVEL              = 2;
const unsigned DEFAULT_CONSOLE_ENABLED          = 0;

namespace rage
{  
//Command line parameters
PARAM(adminemail,       "Email address of server admin");
PARAM(serviceemail,     "Email address of the service (i.e. who emails appear to come from)");
PARAM(runms,            "Number of ms to run before exiting, useful for detecting memory leaks");
PARAM(sleepms,          "ms to sleep between updates");
PARAM(logpath,          "Path to create log files in.");
PARAM(logmaxkb,         "Maximum size of a server log file in kilobytes (default is 1024).");
PARAM(logmaxpinched,    "Max number of pinched log files to keep around");
PARAM(logdebuglevel,    "Debug level (default is 2).");
PARAM(logtoconsole,     "If non-zero, output log text to the console (default is 1).");
};

#if __WIN32SERVICE

//Name of service, filled in when the service starts.
char g_ServiceName[256] = "";

//Name of registry value where services can specify a cmdline.
#define CMDLINE_REGISTRY_VALUE_NAME "cmdline"

//When running as a service, we don't use the normal program entry points
//(main or WinMain), so we must manually do what main() does; hence the
//need to extern some things.
namespace rage
{
    extern bool g_EnableRfs;
    extern int CommonMain(int argc,char **argv);
};

SERVICE_STATUS g_ServiceStatus =
{
    SERVICE_WIN32_OWN_PROCESS,                      //dwServiceType
    SERVICE_STOPPED,                                //dwCurrentState
    SERVICE_ACCEPT_STOP | SERVICE_ACCEPT_SHUTDOWN,  //dwControlsAccepted
    0,                                              //dwWin32ExitCode
    0,                                              //dwServiceSpecificExitCode
    0,                                              //dwCheckPoint
    0                                               //dwWaitHint
};

SERVICE_STATUS_HANDLE g_hServiceStatus = 0;
HANDLE g_hServiceShutdown = 0;
HANDLE g_hServiceStopped = 0;

//-----------------------------------------------------------------------------
//Configure event logging for service
//-----------------------------------------------------------------------------
static BOOL ServiceInitEventLogging()
{
    //Get the executable file path
    CHAR strFilePath[MAX_PATH];
    ::GetModuleFileName(NULL, strFilePath, sizeof(strFilePath));

    //Make registry entries to support logging messages
    //Add the source name as a subkey under the Application
    //key in the EventLog service portion of the registry.
    CHAR szKey[256];
    HKEY hKey = NULL;
    strcpy(szKey, "SYSTEM\\CurrentControlSet\\Services\\EventLog\\Application\\");
    strcat(szKey, g_ServiceName);
    if(::RegCreateKey(HKEY_LOCAL_MACHINE, szKey, &hKey) != ERROR_SUCCESS)
    {
        return FALSE;
    }

    //Add the Event ID message-file name to the 'EventMessageFile' subkey.
    ::RegSetValueEx(hKey,                               // Handle to open a key
                    "EventMessageFile",                 // String containing the
                                                        // name of the value to set
                    0,                                  // Reserved; must be zero.
                    REG_EXPAND_SZ,                      // Type of data
                    (CONST BYTE*) strFilePath,          // buffer containing the data to be stored
                    (DWORD) strlen( strFilePath ) + 1); // Size data buffer

    //Set the supported types flags.
    DWORD dwData = EVENTLOG_ERROR_TYPE | EVENTLOG_WARNING_TYPE | EVENTLOG_INFORMATION_TYPE;

    ::RegSetValueEx(hKey,
                    "TypesSupported",
                    0,
                    REG_DWORD,
                    (CONST BYTE*)&dwData,
                    sizeof(DWORD));

    ::RegCloseKey(hKey);

    return TRUE;
}

//-----------------------------------------------------------------------------
//Logs a message to the windows event log.  Call ConfigureService() first.
//-----------------------------------------------------------------------------
VOID __cdecl 
LogEvent(WORD wEventType, const char* strFormat, ...)
{
    //Format the message
    const INT MAX_OUTPUT_STR = 2048;
    CHAR strBuffer[ MAX_OUTPUT_STR ];
    va_list pArglist;
    
    va_start(pArglist, strFormat);
    wvsprintf(strBuffer, strFormat, pArglist);
    va_end(pArglist);

    //Write it to the windows event log
    const CHAR* rgStrings[1];
    rgStrings[0] = strBuffer;

    HANDLE hEventSource = ::RegisterEventSource(NULL, g_ServiceName);

    if(hEventSource != NULL)
    {
        ::ReportEvent(hEventSource, 
                      wEventType, 
                      0, 
                      MSG_GENERIC, 
                      NULL, 
                      1, 
                      0, 
                      (LPCTSTR*)&rgStrings[0], 
                      NULL);

        ::DeregisterEventSource(hEventSource);
    }
}

//-----------------------------------------------------------------------------
//Service control handler
//-----------------------------------------------------------------------------
VOID ServiceHandler(DWORD dwRequest) 
{ 
    if((SERVICE_CONTROL_STOP == dwRequest) || 
       (SERVICE_CONTROL_SHUTDOWN == dwRequest))
    {
        g_ServiceStatus.dwCurrentState = SERVICE_STOP_PENDING; 
        SetServiceStatus(g_hServiceStatus, &g_ServiceStatus);

        ::SetEvent(g_hServiceShutdown);

        ::WaitForSingleObject(g_hServiceStopped, INFINITE);

        g_ServiceStatus.dwCurrentState = SERVICE_STOPPED; 
        SetServiceStatus(g_hServiceStatus, &g_ServiceStatus);
    }
}

//-----------------------------------------------------------------------------
//Gets the service command line from the registry
//-----------------------------------------------------------------------------
bool GetCmdlineFromRegistry(char* cmdline, const unsigned sizeofCmdline)
{
    Assert(cmdline && sizeofCmdline);

    char keyName[1024];
    formatf(keyName, sizeof(keyName), "SYSTEM\\CurrentControlSet\\Services\\%s", g_ServiceName);
    HKEY hkey;
    if(RegOpenKeyEx(HKEY_LOCAL_MACHINE,
                    keyName,
                    0,
                    KEY_QUERY_VALUE,
                    &hkey) == ERROR_SUCCESS) 
    {
        DWORD dwType = 0;

        char buf[256];
        sysMemSet(buf, 0, sizeof(buf));

        DWORD dwSize = 0;
        dwSize = sizeof(buf);
        
        if((ERROR_SUCCESS == RegQueryValueEx(hkey, CMDLINE_REGISTRY_VALUE_NAME, 0, &dwType, (BYTE*)buf, &dwSize)) && 
           (REG_SZ == dwType))
        {
            if(dwSize > 1)
            {
                formatf(cmdline, sizeofCmdline, "%s", buf);                
                LogEvent(EVENTLOG_INFORMATION_TYPE, "Found cmdline in registry: %s", cmdline);
                return true;
            }
            else
            {
                LogEvent(EVENTLOG_ERROR_TYPE, "Registry value \"" CMDLINE_REGISTRY_VALUE_NAME "\" is null");
            }
        }
        else
        {
            LogEvent(EVENTLOG_ERROR_TYPE, "Could not query registry value \"" CMDLINE_REGISTRY_VALUE_NAME "\"");
        }
    }
    else
    {
        LogEvent(EVENTLOG_ERROR_TYPE, 
                 "Could not query registry value \"" CMDLINE_REGISTRY_VALUE_NAME "\" (key does not exist: %s)", 
                 keyName);
    }

    return false;
}

//-----------------------------------------------------------------------------
//Service entry point
//-----------------------------------------------------------------------------
VOID ServiceMain(int argc, char** argv) 
{
    rtry
    {
        //Init our service status.
        g_ServiceStatus.dwServiceType               = SERVICE_WIN32_OWN_PROCESS;
        g_ServiceStatus.dwCurrentState              = SERVICE_STOPPED;
        g_ServiceStatus.dwControlsAccepted          = SERVICE_ACCEPT_STOP | SERVICE_ACCEPT_SHUTDOWN;
        g_ServiceStatus.dwWin32ExitCode             = 0;
        g_ServiceStatus.dwServiceSpecificExitCode   = 0;
        g_ServiceStatus.dwCheckPoint                = 0;
        g_ServiceStatus.dwWaitHint                  = 0;

        //The first parameter is always the service name.
        safecpy(g_ServiceName, argv[0], sizeof(g_ServiceName));

        ServiceInitEventLogging();

        LogEvent(EVENTLOG_INFORMATION_TYPE, "Service %s starting", g_ServiceName);

        //Register our service control handler.
        rcheck(0 != (g_hServiceStatus = ::RegisterServiceCtrlHandler(g_ServiceName, 
                                                                     (LPHANDLER_FUNCTION)ServiceHandler)),
               catchall,
               LogEvent(EVENTLOG_ERROR_TYPE, "Failed to RegisterServiceCtrlHandler"));

        g_ServiceStatus.dwCurrentState = SERVICE_START_PENDING; 
        SetServiceStatus(g_hServiceStatus, &g_ServiceStatus);

        //Create events to catch stop and shutdown events.
        rcheck(0 != (g_hServiceStopped = ::CreateEvent(NULL, TRUE, FALSE, NULL)),
               catchall,
               LogEvent(EVENTLOG_ERROR_TYPE, "Failed to create g_hServiceStopped event"));

        rcheck(0 != (g_hServiceShutdown = ::CreateEvent(NULL, FALSE, FALSE, NULL)),
               catchall,
               LogEvent(EVENTLOG_ERROR_TYPE, "Failed to create g_hServiceShutdown event"));

        //Fetch the cmdline from the registry and append to args array.
        char* finalArgv[256];
        sysMemSet(finalArgv, 0, sizeof(finalArgv));

        for(int i = 0; i < argc; i++)
        {
            finalArgv[i] = argv[i];
        }

        char cmdline[256];
        rcheck(GetCmdlineFromRegistry(cmdline, sizeof(cmdline)),
               catchall,
               LogEvent(EVENTLOG_ERROR_TYPE, "Failed to read cmdline from registry"));
        finalArgv[argc++] = cmdline;

        LogEvent(EVENTLOG_INFORMATION_TYPE, "Service %s started successfully", g_ServiceName);

        //Run the main function once.
        g_ServiceStatus.dwCurrentState = SERVICE_RUNNING; 
        SetServiceStatus(g_hServiceStatus, &g_ServiceStatus);

#ifdef DISABLE_RFS
	    g_EnableRfs = false;
#endif
        LogEvent(EVENTLOG_INFORMATION_TYPE, "Service %s calling CommonMain", g_ServiceName);
     
        int result = CommonMain(argc, finalArgv);

        if(SERVICE_RUNNING != g_ServiceStatus.dwCurrentState)
        {
            //CommonMain was told to exit by the SCM, who is waiting on notification.
            LogEvent(EVENTLOG_INFORMATION_TYPE, "Service %s CommonMain exited per external event; notifying SCM", g_ServiceName);
            ::SetEvent(g_hServiceStopped);
        }
        else if(0 ==result)
        {
            //CommonMain exited on its own, so we must tell the SCM we stopped.
            LogEvent(EVENTLOG_INFORMATION_TYPE, "Service %s CommonMain exited cleanly", g_ServiceName);
            g_ServiceStatus.dwCurrentState = SERVICE_STOPPED; 
            SetServiceStatus(g_hServiceStatus, &g_ServiceStatus);
        }
        else
        {
            //We exited due to an exception; abort() so the SCM interprets this as a
            //service failure, so we'll auto-restart if recovery options have been set.
            LogEvent(EVENTLOG_INFORMATION_TYPE, "Service %s CommonMain exited with error code %d", g_ServiceName, result);
            _set_abort_behavior(0, _WRITE_ABORT_MSG); //Suppress the abort dialog
            abort();
        }
    }
    rcatchall
    {
        LogEvent(EVENTLOG_INFORMATION_TYPE, "Service %s failed to start", g_ServiceName);

        if(g_hServiceStatus)
        {
            g_ServiceStatus.dwCurrentState = SERVICE_STOPPED;
            g_ServiceStatus.dwWin32ExitCode = (DWORD)-1;
            SetServiceStatus(g_hServiceStatus, &g_ServiceStatus);
        }
    }
}

//-----------------------------------------------------------------------------
//Entry point for service applications
//-----------------------------------------------------------------------------
VOID __cdecl main()
{
    SERVICE_TABLE_ENTRY ServiceTable[2] = { 0 };
    ServiceTable[0].lpServiceName = (LPSTR)"";
    ServiceTable[0].lpServiceProc = (LPSERVICE_MAIN_FUNCTION)ServiceMain;

    ::StartServiceCtrlDispatcher(ServiceTable);
}

#endif // __WIN32SERVICE

//Sends a system email to the server admin.
bool SendAdminEmail(const char* subject, const char *text)
{
    const char* recipient = 0;
    PARAM_adminemail.Get(recipient);

    if(!recipient || !strlen(recipient))
    {
        return false;
    }

    const char* sender = 0;
    PARAM_serviceemail.Get(sender);

    if(!sender || !strlen(sender))
    {
        return false;
    }

    return SendSystemEmail(sender, recipient, subject, text);
}

#if __NO_OUTPUT

void OpenLog(const char* /*logBasePath*/,
             const char* /*logBaseName*/)
{
}

void CloseLog(const bool)
{
}

#else 

//Override rage's default print function so we can print to the correct log file.
static bool (*sPrevDiagCallback)(const diagChannel&,diagSeverity&,const char*,int,const char*) = NULL;

static bool MyDiagCallback(const diagChannel&,
                           diagSeverity& severity,
                           const char* filename,
                           int line,
                           const char* msg)
{
    switch(severity)
    {
    case DIAG_SEVERITY_DEBUG3:
	case DIAG_SEVERITY_DEBUG2:
	case DIAG_SEVERITY_DEBUG1:
	case DIAG_SEVERITY_DISPLAY:
        SrvLog(SrvLog::LOG_INFO, "rage", NULL, 0, NULL).LogLine("%s", msg);
        break;
    case DIAG_SEVERITY_WARNING:
        SrvLog(SrvLog::LOG_WARNING, "rage", NULL, 0, NULL).LogLine("%s(%d): %s", filename, line, msg);
        break;
    case DIAG_SEVERITY_ERROR:
    case DIAG_SEVERITY_ASSERT:
    case DIAG_SEVERITY_FATAL:
        SrvLog(SrvLog::LOG_ERROR, "rage", NULL, 0, NULL).LogLine("%s(%d): %s", filename, line, msg);
        break;

    case DIAG_SEVERITY_COUNT:
        break;
    }

    return severity != DIAG_SEVERITY_FATAL;
}

void OpenLog(const char* logBasePath,
             const char* logBaseName)
{
    Assert(logBasePath);
    Assert(logBaseName);

    unsigned maxSizeofSrvrLog = DEFAULT_MAX_SIZEOF_LOG;
    PARAM_logmaxkb.Get(maxSizeofSrvrLog);

    unsigned logMaxPinched = DEFAULT_LOG_HISTORY_SIZE;
    PARAM_logmaxpinched.Get(logMaxPinched);

    SrvLog::Config config;
    config.m_MaxPinched     = logMaxPinched;
    config.m_MaxSizeofLog   = maxSizeofSrvrLog*1024;
    config.m_LogBasePath    = logBasePath;
    config.m_LogBaseName    = logBaseName;

    if(!SrvLog::InitClass(config))
    {
#if __WIN32SERVICE
        LogEvent(EVENTLOG_ERROR_TYPE, "Service %s SrvLog::InitClass failed; most likely cause is missing or bad cmdline parameter", g_ServiceName);
#endif
        return;
    }

    sPrevDiagCallback = diagExternalCallback;
    diagExternalCallback = MyDiagCallback;

    unsigned debugLevel = DEFAULT_DEBUG_LEVEL;
    PARAM_logdebuglevel.Get(debugLevel);
    SrvLog::SetDebugLevel(debugLevel);
    
    const char* logPath = ".";
    PARAM_logpath.Get(logPath);

    slogDebug1("service", ("logpath         = %s", logPath));
    slogDebug1("service", ("logmaxkb        = %u", maxSizeofSrvrLog));
    slogDebug1("service", ("logmaxpinched   = %u", logMaxPinched));
    slogDebug1("service", ("logdebuglevel   = %u", debugLevel));

#if __WIN32SERVICE
    SrvLog::SetConsoleEnabled(false);
#else
    unsigned consoleEnabled = DEFAULT_CONSOLE_ENABLED;
    PARAM_logtoconsole.Get(consoleEnabled);
    SrvLog::SetConsoleEnabled(consoleEnabled != 0);

    slogDebug1("service", ("logtoconsole    = %u", consoleEnabled));
#endif

#if __WIN32SERVICE        
    if(SrvLog::IsLogFileOpen())
    {
        LogEvent(EVENTLOG_INFORMATION_TYPE, "Service %s log started at %s", g_ServiceName, SrvLog::GetLatestLogFilename());
    }
    else
    {
        LogEvent(EVENTLOG_ERROR_TYPE, "Service %s failed to open log file", g_ServiceName);
    }
#endif
}

void CloseLog(const bool crashed)
{
    if(sPrevDiagCallback)
    {
        diagExternalCallback = sPrevDiagCallback;
        sPrevDiagCallback= NULL;
    }

    SrvLog::CloseLog(crashed ? SrvLog::CLOSE_CRASH : SrvLog::CLOSE_NORMAL);

    if(crashed)
    {
        char subject[128];

#if __WIN32SERVICE
        formatf(subject, sizeof(subject), "Crash occurred in %s", g_ServiceName);
#else
        formatf(subject, sizeof(subject), "Crash occurred");
#endif

        char text[1024];
        formatf(text, sizeof(text), "Crash occurred!  Last log file located at: %s", SrvLog::GetLatestLogFilename());

        SendAdminEmail(subject, text);
    }
}

#endif //__NO_OUTPUT


//Main title server loop.
void Run()
{
    if(!ServiceInit())
    {
#if __WIN32SERVICE
        LogEvent(EVENTLOG_ERROR_TYPE, "Service %s failed to init; most likely cause is missing or bad cmdline parameter", g_ServiceName);
#endif
        return;       
    }

    //int *i = 0;
    //Assert(i);  //Test assert reporting
    //*i = 10;    //Test crash reporting

    bool done = false;
    unsigned runms = 0;
    PARAM_runms.Get(runms);

    unsigned sleepms = DEFAULT_SLEEP_MS;
    PARAM_sleepms.Get(sleepms);

    slogDebug1("service", ("runms           = %u", runms));
    slogDebug1("service", ("sleepm          = %u", sleepms));

    while(!done)
    {
        done = ServiceUpdate();
        sysIpcSleep(sleepms);

#if __WIN32SERVICE
        if(!done)
        {
            done = (WAIT_TIMEOUT != ::WaitForSingleObject(g_hServiceShutdown, 0));
        }
#endif

        if(!done && runms)
        {
            static unsigned lastTime = 0;
            static unsigned runAccum = 0;
            unsigned curTime = sysTimer::GetSystemMsTime();

            if(lastTime)
            {
                runAccum += (curTime - lastTime);

                if(runAccum >= runms)
                {
                    slogDebug1("service", ("Service has run for %ums, shutting down", runAccum));

#if __WIN32SERVICE
                    ::SetEvent(g_hServiceShutdown);
#else
                    done = true;
#endif
                }
            }

            lastTime = curTime;
        }
    }

    ServiceShutdown();
}

//-----------------------------------------------------------------------------
//Regardless of whether we run as a console app or a service, this will get
//called from CommonMain().
//-----------------------------------------------------------------------------
int Main()
{
    //Get the log base directory, and create a directory for the run.
    const char* logBasePath = ".";
    if(!PARAM_logpath.Get(logBasePath))
    {
#if __WIN32SERVICE
        LogEvent(EVENTLOG_ERROR_TYPE, "Service %s logpath not specified.  This must be set to the absolute path of the desired logs directory.", g_ServiceName);
        return -1;
#endif
    }


    //Construct a timestamped directory for the logs and crash dump of this run.
    char c = logBasePath[strlen(logBasePath)-1];
    bool slashAtEnd = (c == '\\') || (c == '/');
    
    char logPath[512];
    SYSTEMTIME t;
    GetLocalTime(&t);

    formatf(logPath, sizeof(logPath),
            "%s%slog %02u_%02u_%02u %02u-%02u-%02u",
            logBasePath,
            slashAtEnd ? "" : "\\",
            t.wMonth, t.wDay, t.wYear % 100,
            t.wHour, t.wMinute, t.wSecond);

    char cmdBuf[256];
    formatf(cmdBuf, sizeof(cmdBuf), "mkdir \"%s\"", logPath);
    ::system(cmdBuf);


    //Make a logfile name for this run that has the same timestamp as the directory.
    char logName[512];
    formatf(logName, sizeof(logName),
#if __WIN32SERVICE
        "%s %02u_%02u_%02u %02u-%02u-%02u",
        g_ServiceName,
#else
        "%02u_%02u_%02u %02u-%02u-%02u",
#endif
        t.wMonth, t.wDay, t.wYear % 100,
        t.wHour, t.wMinute, t.wSecond);


#if !__FINAL
    //Enable minidumps, and set .dmp file to go to the run's log directory.
    sysStack::SetEnableMinidumps(true);
    sysStack::SetMinidumpDirectory(logPath);
#endif

    //Disable the quit on fail behavior for memory allocation; it is
    //undesirable behavior for a server.
    sysMemAllocator::EnableQuitOnFail(false);

    EXCEPTION_BEGIN
    {
	    OpenLog(logPath, logName);

        Run();

        CloseLog(false);

#if __WIN32SERVICE
        LogEvent(EVENTLOG_INFORMATION_TYPE, "Service %s shutdown cleanly", g_ServiceName);
#endif
    }
    EXCEPTION_END 
    {
        CloseLog(true);

#if __WIN32SERVICE
        LogEvent(EVENTLOG_ERROR_TYPE, "Service %s shutdown due to exception", g_ServiceName);
#endif

        return -1;
	}

    return 0;
}
