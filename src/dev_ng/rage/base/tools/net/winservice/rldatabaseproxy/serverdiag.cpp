// 
// serverdiag.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#include "serverdiag.h"

#include "diag/output.h"
#include "string/string.h"
#include "system/nelem.h"
#include "system/xtl.h"

#include <time.h>

using namespace rage;

bool                        SrvLog::sm_ClassInitialized = false;
sysCriticalSectionToken     SrvLog::sm_Cs;
FILE*                       SrvLog::sm_Fp = 0;
int                         SrvLog::sm_DebugLevel = 2;
unsigned                    SrvLog::sm_SizeofLog;
unsigned                    SrvLog::sm_MaxSizeofLog = SrvLog::DEFAULT_MAX_SIZEOF_LOG;
unsigned                    SrvLog::sm_MaxPinched = 0;
unsigned                    SrvLog::sm_NumFilesOpened = 0;
bool                        SrvLog::sm_ConsoleEnabled = false;
char                        SrvLog::sm_LogBasePath[MAX_BASE_PATH_SIZE]      = {"."};
char                        SrvLog::sm_LogBaseName[MAX_BASE_NAME_SIZE]      = {"server"};
char                        SrvLog::sm_LogFullName[MAX_FULL_NAME_SIZE]      = {""};

bool
SrvLog::InitClass(const Config& config)
{
    if(AssertVerify(!sm_ClassInitialized))
    {
        if(AssertVerify(config.m_MaxSizeofLog))
        {
            if(config.m_LogBasePath)
            {
                safecpy(sm_LogBasePath, config.m_LogBasePath, COUNTOF(sm_LogBasePath));
            }
            else 
            {
                safecpy(sm_LogBasePath, ".", COUNTOF(sm_LogBasePath));
            }

            if(config.m_LogBaseName)
            {
                safecpy(sm_LogBaseName, config.m_LogBaseName, COUNTOF(sm_LogBaseName));
            }
            else
            {
                sm_LogBaseName[0] = 0;
            }

            sm_MaxSizeofLog = config.m_MaxSizeofLog;
            sm_MaxPinched = config.m_MaxPinched;
            sm_NumFilesOpened = 0;
            memset(sm_LogFullName, 0, sizeof(sm_LogFullName));

            sm_ClassInitialized = true;

            return true;
        }
    }

    return false;
}

SrvLog::SrvLog(const LogLevel level,
               const char* facility,
               const char* fileName,
               const int fileLine,
               const char* funcName)
: m_Level(level)
, m_Facility(facility)
, m_FileName(fileName)
, m_FileLine(fileLine)
, m_FuncName(funcName)
, m_PrintPreamble(true)
{
    sm_Cs.Lock();
}

SrvLog::~SrvLog()
{
    sm_Cs.Unlock();
}

#if __NO_OUTPUT
SrvLog&
SrvLog::Log(const char* /*fmt*/, ...)
{
    return *this;
}
#else   //__NO_OUTPUT
SrvLog&
SrvLog::Log(const char* fmt, ...)
{
    va_list args;
    va_start(args, fmt);
    this->VLog(fmt, args, false);
    return *this;
}
#endif  //__NO_OUTPUT

#if __NO_OUTPUT
SrvLog&
SrvLog::VLog(const char* /*fmt*/, va_list /*args*/)
{
    return *this;
}
#else
SrvLog&
SrvLog::VLog(const char* fmt, va_list args)
{
    this->VLog(fmt, args, false);
    return *this;
}
#endif  //__NO_OUTPUT

#if __NO_OUTPUT
SrvLog&
SrvLog::LogLine(const char* /*fmt*/, ...)
{
    return *this;
}
#else   //__NO_OUTPUT
SrvLog&
SrvLog::LogLine(const char* fmt, ...)
{
    va_list args;
    va_start(args, fmt);
    this->VLog(fmt, args, true);
    return *this;
}
#endif  //__NO_OUTPUT

#if __NO_OUTPUT
SrvLog&
SrvLog::VLogLine(const char* /*fmt*/, va_list /*args*/)
{
    return *this;
}
#else
SrvLog&
SrvLog::VLogLine(const char* fmt, va_list args)
{
    this->VLog(fmt, args, true);
    return *this;
}
#endif  //__NO_OUTPUT

void
SrvLog::MakeTimeStr(char (&str)[32])
{
    SYSTEMTIME t;
    GetLocalTime(&t);

    formatf(str, sizeof(str), "[%02u/%02u/%02u %02u:%02u:%02u.%03u]",
            t.wMonth, t.wDay, t.wYear % 100,
            t.wHour, t.wMinute, t.wSecond, t.wMilliseconds);
}

//private:

#if __NO_OUTPUT
void 
SrvLog::OpenLogfile()
{
}

void
SrvLog::VLog(const char* /*fmt*/, va_list /*args*/, const bool /*nl*/)
{
}
#else   //__NO_OUTPUT

void
SrvLog::OpenLogfile()
{
    //The full name includes the serial number and the "active" extension.
    formatf(sm_LogFullName, sizeof(sm_LogFullName), 
            "%s\\%s %u active.log",
            sm_LogBasePath,
            sm_LogBaseName,
            sm_NumFilesOpened);

    sm_Fp = fopen(sm_LogFullName, "wb");
    sm_SizeofLog = 0;

    if(sm_Fp)
    {
        ++sm_NumFilesOpened;
    }
}

void
SrvLog::CloseLog(const eCloseReason reason)
{
    SYS_CS_SYNC(sm_Cs);

    if(!sm_ClassInitialized)
    {
        return;
    }

    if(sm_Fp)
    {
        fclose(sm_Fp);
        sm_Fp = NULL;
    }

    if(sm_NumFilesOpened)
    {
        //Rename the last file to reflect the way it was closed.
        //NOTE: If our last file was pinched and no new file was opened, this will
        //      rename the last pinched file as either closed or crashed, so that 
        //      the final state of the series is indicated.
        char temp[MAX_FULL_NAME_SIZE];
        Assert(sm_NumFilesOpened);

        switch(reason)
        {
        case CLOSE_PINCH:
            formatf(temp,
                    sizeof(temp),
                    "%s\\%s %u pinched.log",
                    sm_LogBasePath,
                    sm_LogBaseName,
                    sm_NumFilesOpened - 1);
            break;

        case CLOSE_NORMAL:
            formatf(temp,
                    sizeof(temp),
                    "%s\\%s %u closed.log",
                    sm_LogBasePath,
                    sm_LogBaseName,
                    sm_NumFilesOpened - 1);
            break;

        case CLOSE_CRASH:
            formatf(temp,
                    sizeof(temp),
                    "%s\\%s %u crashed.log",
                    sm_LogBasePath,
                    sm_LogBaseName,
                    sm_NumFilesOpened - 1);
            break;
        };

        rename(sm_LogFullName, temp);
        memcpy(sm_LogFullName, temp, sizeof(temp));

        //Delete pinched files past our threshold. 
        //NOTE: We always keep the first pinch file, since it may contain useful debug info.
        if(sm_MaxPinched 
           && (sm_NumFilesOpened > 2)
           && ((sm_NumFilesOpened - 2) > sm_MaxPinched))
        {
            formatf(temp,
                    sizeof(temp),
                    "%s\\%s %u pinched.log",
                    sm_LogBasePath,
                    sm_LogBaseName,
                    sm_NumFilesOpened - 2 - sm_MaxPinched);

            char cmdBuf[256];
            formatf(cmdBuf, sizeof(cmdBuf), "del \"%s\"", temp);
            ::system(cmdBuf);
        }

        //Restart the series if we closed in any way other than PINCH
        if(reason != CLOSE_PINCH)
        {
            sm_NumFilesOpened = 0;
        }
    }
}

void
SrvLog::VLog(const char* fmt, va_list args, const bool nl)
{
    if(sm_Fp && sm_SizeofLog >= sm_MaxSizeofLog)
    {
        this->CloseLog(CLOSE_PINCH);
    }

    if(!sm_Fp)
    {
        OpenLogfile();
    }

    if(sm_Fp)
    {
        int numChars;

        if(m_PrintPreamble)
        {
            const char* logLevel =
                LOG_INFO == m_Level ? "INF" :
                LOG_DEBUG == m_Level ? "DBG" :
                LOG_WARNING == m_Level ? "WRN" : "ERR";

            char timeStr[32];
            SrvLog::MakeTimeStr(timeStr);

            char buf[1024];

            if(LOG_INFO == m_Level
                || LOG_DEBUG == m_Level
                || !m_FileName)
            {
                //Don't print file, line, func
                numChars = _snprintf(buf, COUNTOF(buf) - 1,
                                    "%s [%s:%s] ",
                                    timeStr,
                                    logLevel,
                                    m_Facility);
                buf[numChars] = '\0';
            }
            else
            {
                numChars = _snprintf(buf, COUNTOF(buf) - 1,
                                    "%s [%s:%s] %s:%d (%s) ",
                                    timeStr,
                                    logLevel,
                                    m_Facility,
                                    "FIXME(UNKNOWN FILENAME)", //diagSpew::BaseFN(m_FileName),
                                    m_FileLine,
                                    m_FuncName);
                buf[numChars] = '\0';
            }

            if(numChars > 0)
            {
                fprintf(sm_Fp, buf);
                sm_SizeofLog += numChars;

                if(sm_ConsoleEnabled)
                {
                    fprintf(stdout, buf);
                }
            }

            m_PrintPreamble = false;
        }

        numChars = vfprintf(sm_Fp, fmt, args);
        if(numChars > 0){sm_SizeofLog += numChars;}

        if(nl)
        {
            numChars = fprintf(sm_Fp, "\r\n");
            if(numChars > 0){sm_SizeofLog += numChars;}
        }

        fflush(sm_Fp);

        if(sm_ConsoleEnabled)
        {
            vfprintf(stdout, fmt, args);

            if(nl)
            {
                fprintf(stdout, "\r\n");
            }
        }
    }
}
#endif  //__NO_OUTPUT
