#ifndef _PERFLIB_H_
#define _PERFLIB_H_
void InitPerfLib(char *queueMachine, char *queueName, char *applicationName);
void LogIntPerfData(char *shortname, char *counter, char *instance, int value);

bool InitTCPPerfLib(const char *localAddr, int sentryPort, const char *applicationName);
void SetPerfErrorState(const char *errorStr);
void ClearPerfErrorState();
void FreeTCPPerfLib();
char *BuildSendBuffer(char *buf);

#endif
