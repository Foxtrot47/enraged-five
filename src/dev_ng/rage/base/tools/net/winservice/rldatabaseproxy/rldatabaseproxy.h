// 
// rlDbProxy.h
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef RLDATABASEPROXY_H
#define RLDATABASEPROXY_H

#if __WIN32PC

#include "commandserver.h"
#include "rline/rldatabaseclient.h"
#include "rline/rldatabasemsgs.h"
#include "rline/rlupload.h"
#include "net/nethardware.h"
#include "atl/inlist.h"
#include "atl/inmap.h"
#include "system/criticalsection.h"
#include "system/simpleallocator.h"

#include "rline/rldatabasemsgs.h"

#define WIN32_LEAN_AND_MEAN
#include "system/xtl.h" //NOTE(RDT): Use instead of windows.h
#include <winsock.h>

namespace rage
{

class rlDbProxy
{
public:
    rlDbProxy();
    ~rlDbProxy();

    bool Init();
    void Shutdown();
    bool Update();

private:
    void Reset();
    
    bool InitNet();
    bool InitRline();
    bool InitNorad();
    bool InitTelnet();

    bool Start();
    void Stop();

    void CheckCompletedRequests();
    void CheckTimedOutConnections();  
    void SendToClients();

private:
    static const unsigned INVALID_CLIENTID = 0xFFFFFFFF;

    //Message queued to be sent to a client.
    struct OutboundMsg
    {
        OutboundMsg();
        ~OutboundMsg();

        bool Init(const void* data, const unsigned size);

        u8* m_MsgBuf;
        unsigned m_MsgSize;

        inlist_node<OutboundMsg> m_ListLink;
    };
    typedef inlist<OutboundMsg, &OutboundMsg::m_ListLink> OutboundMsgList;

    //Current state of a client connection
    struct ClientRec
    {
        ClientRec();
        ~ClientRec();

        unsigned    m_ClientId;
        netAddress  m_Address;
        unsigned    m_NumRequestsInProgress;
        unsigned    m_LastActiveTime;

        SOCKET      m_Socket;
        
        //Receiving
        u8*         m_MsgBuf;
        unsigned    m_MsgBytesRead;
        unsigned    m_MsgSize;
        unsigned    m_MsgSizeBytesRead;

        //Sending
        OutboundMsgList m_OutboundMsgList;
        unsigned        m_MsgBytesSent;

        inmap_node<unsigned, ClientRec> m_ByIdLink;
    };
    typedef inmap<unsigned, ClientRec, &ClientRec::m_ByIdLink> ClientMap;
    ClientMap m_ClientMap;

    //Request that client has sent to us
    enum eRequestType
    {
        REQUEST_INVALID = -1,
        REQUEST_CREATE_RECORD,
        REQUEST_UPDATE_RECORD,
        REQUEST_DELETE_RECORD,
        REQUEST_RATE_RECORD,
        REQUEST_GET_NUM_RECORDS,
        REQUEST_READ_RECORDS,
        REQUEST_READ_RANDOM_RECORD,
        REQUEST_UPLOAD_FILE,
        REQUEST_DOWNLOAD_FILE,
        NUM_REQUESTS
    };

    struct Request
    {
        Request(const int type);
        virtual ~Request();

        bool Configure(rlDbProxy* ctx, unsigned clientId) 
        { 
            m_Ctx = ctx;
            m_ClientId = clientId; 
            return true; 
        }

        virtual bool Start() = 0;

        const int   m_Type;
        rlDbProxy*  m_Ctx;
        unsigned    m_ClientId;

        float       m_Progress;
        float       m_LastProgress;
        unsigned    m_LastProgressReportTime;
        netStatus   m_Status;

        unsigned    m_StartTime;

        inlist_node<Request> m_ListLink;
    };
    typedef inlist<Request, &Request::m_ListLink> RequestList;
    RequestList m_RequestList;

    template<class T_MSG>
    struct TRequest : public Request
    {
        TRequest(const int type) : Request(type) {}
        virtual ~TRequest() {}

        virtual bool Configure(rlDbProxy* ctx,
                               unsigned clientId, 
                               const void* requestMsgData,
                               const unsigned requestMsgSize)
        {
            if(!Request::Configure(ctx, clientId)) return false;

            if(!m_Msg.Import(requestMsgData, requestMsgSize)) return false;

            return true;
        }

        T_MSG m_Msg;
    };

    struct CreateRecordRequest : public TRequest<rlDbMsgCreateRecordRequest>
    {
        CreateRecordRequest() : TRequest<rlDbMsgCreateRecordRequest>(REQUEST_CREATE_RECORD) {}
        virtual ~CreateRecordRequest() {}
        virtual bool Start();

        rlDbRecordId m_RecordId;
    };

    struct UpdateRecordRequest : public TRequest<rlDbMsgUpdateRecordRequest>
    {
        UpdateRecordRequest() : TRequest<rlDbMsgUpdateRecordRequest>(REQUEST_UPDATE_RECORD) {}
        virtual ~UpdateRecordRequest() {}
        virtual bool Start();
    };

    struct DeleteRecordRequest : public TRequest<rlDbMsgDeleteRecordRequest>
    {
        DeleteRecordRequest() : TRequest<rlDbMsgDeleteRecordRequest>(REQUEST_DELETE_RECORD) {}
        virtual ~DeleteRecordRequest() {}
        virtual bool Start();
    };

    struct RateRecordRequest : public TRequest<rlDbMsgRateRecordRequest>
    {
        RateRecordRequest() : TRequest<rlDbMsgRateRecordRequest>(REQUEST_RATE_RECORD) {}
        virtual ~RateRecordRequest() {}
        virtual bool Start();
        
        unsigned m_NumRatings;
        float m_AvgRating;
    };

    struct GetNumRecordsRequest : public TRequest<rlDbMsgGetNumRecordsRequest>
    {
        GetNumRecordsRequest() : TRequest<rlDbMsgGetNumRecordsRequest>(REQUEST_GET_NUM_RECORDS) {}
        virtual ~GetNumRecordsRequest() {}
        virtual bool Start();

        unsigned m_NumRecords;
    };

    struct ReadRecordsRequest : public TRequest<rlDbMsgReadRecordsRequest>
    {
        ReadRecordsRequest();
        virtual ~ReadRecordsRequest();

        virtual bool Configure(rlDbProxy* ctx,
                               unsigned clientId, 
                               const void* requestMsgData,
                               const unsigned requestMsgSize);

        virtual bool Start();

        rlDbValue* m_Values;
        unsigned m_NumValues;
        rlDbReadResults m_Results;
    };

    struct ReadRandomRecordRequest : public TRequest<rlDbMsgReadRandomRecordRequest>
    {
        ReadRandomRecordRequest();
        virtual ~ReadRandomRecordRequest();

        virtual bool Configure(rlDbProxy* ctx,
            unsigned clientId, 
            const void* requestMsgData,
            const unsigned requestMsgSize);

        virtual bool Start();

        rlDbValue* m_Values;
        unsigned m_NumValues;
        rlDbReadResults m_Results;
    };

    struct UploadFileRequest : public TRequest<rlDbMsgUploadFileRequest>
    {
        UploadFileRequest() : TRequest<rlDbMsgUploadFileRequest>(REQUEST_UPLOAD_FILE) {}
        virtual ~UploadFileRequest() {}
        virtual bool Start();

        rlDbFileId m_FileId;
    };

    struct DownloadFileRequest : public TRequest<rlDbMsgDownloadFileRequest>
    {
        DownloadFileRequest();
        virtual ~DownloadFileRequest();
        virtual bool Start();
        
        u8* m_Buf;
        unsigned m_NumBytesDownloaded;
    };

    template<class T>
    class Stat
    {
    public:
        Stat()
        : m_Max(0)
        , m_Min(0) 
        , m_Avg(0.0f)
        , m_NumSamples(0)
        {
        }

        void AddSample(const T& sample)
        {
            if(!m_NumSamples || (sample > m_Max))
            {
                m_Max = sample;
            }

            if(!m_NumSamples || (sample < m_Min))
            {
                m_Min = sample;
            }

            m_Avg *= m_NumSamples;
            m_Avg += float(sample);
            ++m_NumSamples;
            m_Avg /= m_NumSamples;
        }

        void AddStats(const Stat<T>& s)
        {
            if(s.m_Max > m_Max)
            {
                m_Max = s.m_Max;
            }

            if(s.m_Min < m_Min)
            {
                m_Min = s.m_Min;
            }

            m_Avg = (m_Avg*m_NumSamples) + (s.m_Avg*s.m_NumSamples);
            m_NumSamples += s.m_NumSamples;
            m_Avg /= m_NumSamples;
        }

        const T& GetMax() const { return m_Max; }
        const T& GetMin() const { return m_Min; }
        unsigned GetNumSamples() const { return m_NumSamples; }
        float    GetAvg() const { return m_Avg; }

    private:
        T m_Max;
        T m_Min;
        unsigned m_NumSamples;
        float m_Avg;
    };

    struct HourStats
    {
        Stat<unsigned>  m_HeapUsed;
        Stat<unsigned>  m_FrameMs;
        Stat<unsigned>  m_NumNewConnections;
        Stat<unsigned>  m_NumConcurrent;
        Stat<float>     m_IncomingKBps;
        Stat<float>     m_OutgoingKBps;
        Stat<unsigned>  m_ResponseMs;
        unsigned        m_NumRequests;

        HourStats()
        : m_NumRequests(0)
        {
        }
    };

    struct RequestStats
    {
        Stat<unsigned> m_Size;
        Stat<unsigned> m_ResponseMs;
        unsigned m_NumResults[RLDB_NUM_RESULT_CODES];

        RequestStats()
        {
            memset(m_NumResults, 0, sizeof(m_NumResults));
        }
    };

    struct ReplyStats
    {
        Stat<unsigned> m_Size;
    };

    struct DayStats
    {
        HourStats       m_HourStats[24];
        RequestStats    m_RequestStats[NUM_REQUESTS];
        ReplyStats      m_ReplyStats[NUM_REQUESTS];
    };

    ClientMap::iterator RemoveClient(const unsigned clientId);

    bool ReceiveFromClient(ClientRec* c,
                           const void* data,
                           const unsigned size);
    void QueueRequest(Request* r);
    bool ReceiveRequestCreateRecord(ClientRec* c, const rlDbMsgCreateRecordRequest& req);
    bool ReceiveRequestUpdateRecord(ClientRec* c, const rlDbMsgUpdateRecordRequest& req);
    bool ReceiveRequestDeleteRecord(ClientRec* c, const rlDbMsgDeleteRecordRequest& req);
    bool ReceiveRequestRateRecord(ClientRec* c, const rlDbMsgRateRecordRequest& req);
    bool ReceiveRequestGetNumRecords(ClientRec* c, const rlDbMsgGetNumRecordsRequest& req);
    bool ReceiveRequestReadRecords(ClientRec* c, const rlDbMsgReadRecordsRequest& req);
    bool ReceiveRequestReadRandomRecord(ClientRec* c, const rlDbMsgReadRandomRecordRequest& req);
    bool ReceiveRequestUploadFile(ClientRec* c, const rlDbMsgUploadFileRequest& req);
    bool ReceiveRequestDownloadFile(ClientRec* c, const rlDbMsgDownloadFileRequest& req);

    void ProcessCompletedRequest(Request* r);

    bool SendToClient(ClientRec* c);
    bool SendToClient(ClientRec* c, 
                      const void* data,
                      const unsigned size);
    bool SendProgressMsg(const unsigned clientId, const float progress);
    bool SendReplyCreateRecord(ClientRec* c, CreateRecordRequest* r);
    bool SendReplyUpdateRecord(ClientRec* c, UpdateRecordRequest* r);
    bool SendReplyDeleteRecord(ClientRec* c, DeleteRecordRequest* r);
    bool SendReplyRateRecord(ClientRec* c, RateRecordRequest* r);
    bool SendReplyGetNumRecords(ClientRec* c, GetNumRecordsRequest* r);
    bool SendReplyReadRecords(ClientRec* c, ReadRecordsRequest* r);
    bool SendReplyReadRandomRecord(ClientRec* c, ReadRandomRecordRequest* r);
    bool SendReplyUploadFile(ClientRec* c, UploadFileRequest* r);
    bool SendReplyDownloadFile(ClientRec* c, DownloadFileRequest* r);

    bool CreateServerSocket();
    bool StartAcceptThread();
    void CheckNewConnections();
    void CheckNewRequests();
    bool ReceiveFromClient(ClientRec* c);
    bool WaitForSocket(SOCKET s, 
                       const bool forRead, //if false, it's for writing
                       const unsigned timeoutSecs);
    
    void UpdateStats();
    void ReportStats();

    void UpdateTelnet();

    void PrintRequestStats(char* buf, 
                           const unsigned sizeofBuf,
                           const char* label,
                           RequestStats* r);

private:
    netHardware m_NetHw;
    netSocket   m_NetSkt;

    void* m_Heap;
    sysMemSimpleAllocator* m_Allocator;
    sysMemAllocator* m_OldAllocator;

    rlDbClient m_DbClient;
    rlUpload m_Upload;

    unsigned    m_CurTime;  //Current system time, updated at start of frame.  This is cached to prevent hundreds of calls to get it.
    bool        m_Initialized   : 1;
    bool        m_Started       : 1;

    //Stats-related data.
    u64         m_UptimeMs;
    DayStats    m_DayStats;
    unsigned    m_CurHour;
    u64         m_IncomingBytesAccum;
    u64         m_OutgoingBytesAccum;
    int         m_MicroReportMs;
    unsigned    m_MicroReportTimer;
    unsigned    m_DailyEmailHour;
    unsigned    m_EmailFixupMs;

    //TCP socket for communicating w/ clients
    SOCKET m_ServerSocket;

    //Worker thread that does blocking accept(), and places accepted sockets
    //in a queue for the main thread.
    static void AcceptThreadFunc(void* userdata);
    sysIpcSema      m_WaitSema;
    sysIpcThreadId  m_ThreadHandle;

    struct AcceptedConnection
    {
        AcceptedConnection() 
        {
            m_Socket = INVALID_SOCKET;
            m_ClientId = unsigned(INVALID_CLIENTID);
        }

        SOCKET      m_Socket;
        unsigned    m_ClientId;
        netAddress  m_Address;
        inlist_node<AcceptedConnection> m_ListLink;
    };

    typedef inlist<AcceptedConnection, &AcceptedConnection::m_ListLink> AcceptedList;
    AcceptedList m_AcceptedList;
    unsigned m_InactivityTimeoutMs;
    
    mutable sysCriticalSectionToken m_CsServerSocket;
    mutable sysCriticalSectionToken m_CsAcceptedList;    

    bool m_StartedNorad;

    CommandServer m_CmdServer;
    int m_TelnetSkt;    
};

}   //namespace rage

#endif //__WIN32PC

#endif // RLDATABASEPROXY_H
