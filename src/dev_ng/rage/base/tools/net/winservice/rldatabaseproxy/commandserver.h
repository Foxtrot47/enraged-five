// 
// server/commandserver.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SERVER_COMMANDSERVER_H
#define SERVER_COMMANDSERVER_H

#include "system/ipc.h"
#include "system/criticalsection.h"

class CommandServer;

class Command
{
    friend class CommandServer;

public:

    Command()
        : m_Str(NULL)
        , m_Next(NULL)
    {
    }

    const char* GetString() const
    {
        return m_Str;
    }

private:

    char* m_Str;

    Command* m_Next;
};

class CommandServer
{
public:

    CommandServer();

    ~CommandServer();

    bool Start(const int listenSkt);

    void Stop();

    void Say(const char* say) const;

    Command* NextCommand();

    void DisposeCommand(Command* cmd);

private:

    static void Worker(void* ctx);

    int m_ListenSkt;
    int m_CurSkt;

    Command* m_Head;
    Command* m_Tail;

    rage::sysIpcThreadId m_ThreadId;

    rage::sysIpcSema m_WaitSema;
    rage::sysCriticalSectionToken m_CmdCs;
    mutable rage::sysCriticalSectionToken m_SktCs;

    bool m_Started      : 1;
    bool m_StopWorker   : 1;
};

#endif  //SERVER_COMMANDSERVER_H
