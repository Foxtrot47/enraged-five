/*
Module : CPDH.H
Purpose: Defines the interface for a wrapper class for the PDH API

Copyright (c) 1999 - 2000 by PJ Naughter.  
All rights reserved.

*/

#ifndef __CPDH_H__
#define __CPDH_H__


///////////////////////////////// Includes //////////////////////////////
#include <pdh.h>



///////////////////////////////// Classes ///////////////////////////////


                      
//Class which encapsulates a PDH counter
class CPdhCounter
{
public:
//Constructors / Destructors
  CPdhCounter(LPCTSTR pszFullCounterPath, DWORD dwUserData = 0);
  CPdhCounter(PDH_COUNTER_PATH_ELEMENTS& CounterPathElements, DWORD dwUserData = 0);
  ~CPdhCounter();  

//Methods
  PDH_STATUS GetRawValue(PDH_RAW_COUNTER& value, DWORD &dwType);
  /*void  SplitPath(CStr& sMachineName, CStr& sObjectName, CStr& sInstanceName,
                  CStr& sParentInstance, DWORD& dwInstanceIndex, CStr& sCounterName);*/
  PDH_STATUS  GetInfo(PDH_COUNTER_INFO *info, DWORD dwBufferSize);
  PDH_STATUS  CalculateFromRawValue(DWORD dwFormat, PDH_RAW_COUNTER& rawValue1, 
                              PDH_RAW_COUNTER& rawValue2, PDH_FMT_COUNTERVALUE& fmtValue);
  PDH_STATUS GetFormattedValue(DWORD dwFormat, PDH_FMT_COUNTERVALUE& value);
  PDH_STATUS  ComputeStatistics(DWORD dwFormat, DWORD dwFirstEntry, DWORD dwNumEntries, 
                          PPDH_RAW_COUNTER lpRawValueArray, PPDH_STATISTICS data);
  PDH_STATUS  SetScaleFactor(LONG lFactor);
 
protected:
//Data
  HCOUNTER m_hCounter;
  LPTSTR  m_sCounterPath;
  DWORD    m_dwUserData;

  friend class CPdhQuery;
};


//Class which encapsulates a PDH query
class CPdhQuery
{
public:
//Constructors / Destructors
  CPdhQuery();
  ~CPdhQuery();

//Methods
  PDH_STATUS Open(DWORD dwUserData = 0);
  void Close();
  PDH_STATUS Add(CPdhCounter& counter);
  PDH_STATUS Remove(CPdhCounter& counter);
  PDH_STATUS Collect();

protected:
//Data
  HQUERY m_hQuery;
};


//Other PDH functions which don't belong anywhere else
class CPdh
{
public:
//Static Methods
  static PDH_STATUS ValidatePath(LPCTSTR pszFullCounterPath);
  static PDH_STATUS ExpandPath(LPCTSTR pszWildCardPath, LPTSTR expandedPaths, DWORD dwBufferSize);
  static PDH_STATUS ConnectMachine(LPCTSTR pszMachineName);
   static PDH_STATUS EnumMachines(LPTSTR machineNames, DWORD dwBufferSize);
  static PDH_STATUS EnumObjects(LPCTSTR pszMachineName, LPTSTR objectNames, DWORD dwBufferSize, 
                          DWORD dwDetailLevel, BOOL bRefresh);
  static PDH_STATUS EnumObjectItems(LPCTSTR pszMachineName, LPCTSTR pszObjectName, LPTSTR counters, DWORD dwCounterBufferSize,
								 LPTSTR instances, DWORD dwInstanceBufferSize,
								 DWORD dwDetailLevel);
  static PDH_STATUS GetDefaultCounter(LPCTSTR pszMachineName, LPCTSTR pszObjectName, LPTSTR sDefaultCounterName, DWORD dwBufferSize);
  static PDH_STATUS GetDefaultObject(LPCTSTR pszMachineName, LPTSTR sDefaultObjectName, DWORD dwBufferSize);
  static PDH_STATUS BrowseCounters(PDH_BROWSE_DLG_CONFIG& BrowseDlgData);
  static PDH_STATUS ParseInstanceName(LPCTSTR pszInstanceString, LPTSTR sInstanceName, DWORD dwInstanceNameBufferSize, LPTSTR sParentName, DWORD dwParentNameBufferSize, DWORD& Index);
};



#endif //__CPDH_H__
