@echo off

rem Use this file to make a snapshot of a build, including the PDB, so
rem crashdumps can be debugged.

if "%1"=="" goto needname

set SNAPSHOTDIR=snapshot_%1

echo Snapshotting to %SNAPSHOTDIR%...

if exist %SNAPSHOTDIR% goto alreadyexists

mkdir %SNAPSHOTDIR%

if not exist %SNAPSHOTDIR% goto errormakingdir

copy *args.txt  %SNAPSHOTDIR%


if exist win32_debug (
copy /B win32_debug\*.exe  %SNAPSHOTDIR%\server_debug_%1.exe
)

if exist win32_beta (
copy /B win32_beta\*.exe  %SNAPSHOTDIR%\server_beta_%1.exe
)

if exist win32_release (
copy /B win32_release\*.exe  %SNAPSHOTDIR%\server_release_%1.exe
)

"C:\Program Files\WinRAR\rar.exe" a %SNAPSHOTDIR%_exe %SNAPSHOTDIR%

if exist win32_debug (
copy /B win32_debug\*debug.pdb  %SNAPSHOTDIR%\server_debug_%1.pdb
)

if exist win32_beta (
copy /B win32_beta\*beta.pdb  %SNAPSHOTDIR%\server_beta_%1.pdb
)

if exist win32_release (
copy /B win32_release\*release.pdb  %SNAPSHOTDIR%\server_release_%1.pdb
)

copy *.cpp  %SNAPSHOTDIR%
copy *.h  %SNAPSHOTDIR%
copy *.txt  %SNAPSHOTDIR%
copy *.pl  %SNAPSHOTDIR%
copy *.bat  %SNAPSHOTDIR%
copy *.reg  %SNAPSHOTDIR%
copy *.sln  %SNAPSHOTDIR%
copy *.vcproj  %SNAPSHOTDIR%
copy *.mk  %SNAPSHOTDIR%
copy *.guid  %SNAPSHOTDIR%

"C:\Program Files\WinRAR\rar.exe" a %SNAPSHOTDIR% %SNAPSHOTDIR%
rmdir /S /Q %SNAPSHOTDIR%

goto done

:needname
echo Error: Missing snapshot name
goto done

:alreadyexists
echo Error: Directory %SNAPSHOTDIR% already exists
goto done

:errormakingdir
echo Error: Creating directory %SNAPSHOTDIR%
goto done

echo Snapshot succeeded.

:done
