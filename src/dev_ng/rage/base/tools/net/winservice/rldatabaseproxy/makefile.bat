set ARCHIVE=rldatabaseproxy

set FILES=serverdiag servicemain commandserver 

set HEADONLY=rldatabaseproxy servicemsg

set CUSTOM=servicemsg.rc servicemsg.mc servicemsg_MSG00001.bin

set LIBS=%RAGE_CORE_LIBS% %RAGE_NET_LIBS%

set LIBS_rldatabaseproxy=%LIBS%

set TESTERS=rldatabaseproxy

set XPROJ=%RAGE_DIR%\base\src %RAGE_DIR%\base\samples %RAGE_DIR%\suite\src
