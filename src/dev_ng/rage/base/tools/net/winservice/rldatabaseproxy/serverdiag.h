// 
// server/diag.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SERVER_DIAG_H
#define SERVER_DIAG_H

#include "system/criticalsection.h"

#include <stdio.h>
#include <stdarg.h>
#include <varargs.h>

class SrvLog
{
public:
    enum
    {
        DEFAULT_MAX_SIZEOF_LOG  = 1024*1024
    };

    enum LogLevel
    {
        LOG_INFO,
        LOG_DEBUG,
        LOG_WARNING,
        LOG_ERROR
    };

    struct Config
    {
        Config()
            : m_LogBasePath(0)
            , m_LogBaseName("server")
            , m_MaxSizeofLog(DEFAULT_MAX_SIZEOF_LOG)
            , m_MaxPinched(0)
        {
        }

        const char* m_LogBasePath;
        const char* m_LogBaseName;
        unsigned m_MaxSizeofLog;
        unsigned m_MaxPinched;
    };

    static bool InitClass(const Config& config);

    SrvLog(const LogLevel level,
            const char* facility,
            const char* fileName,
            const int fileLine,
            const char* funcName);

    ~SrvLog();

    //Logs a formatted string.
    SrvLog& Log(const char* fmt, ...);

    //Logs a formatted string.
    SrvLog& VLog(const char* fmt, va_list args);

    //Logs a formatted string and appends a newline.
    SrvLog& LogLine(const char* fmt, ...);

    //Logs a formatted string and appends a newline.
    SrvLog& VLogLine(const char* fmt, va_list args);

    //Returns true if the log system is initialized.
    static bool IsInitialized()
    {
        return sm_ClassInitialized;
    }

    //Returns true if a log file is currently open.
    static bool IsLogFileOpen()
    {
        return 0 != sm_Fp;
    }

    //Returns name of current log file (if open), last file (if one has been created),
    //or null if no log files have been created yet.
    static const char* GetLatestLogFilename()
    {
        if(sm_LogFullName[0])
        {
            return sm_LogFullName;
        }
        return 0;
    }

    //Possible reasons for closing the current log (if open).
    enum eCloseReason
    {
        CLOSE_PINCH = 0,    //Closing current log file to continue in a new file
        CLOSE_NORMAL,       //Close log because run finished without error
        CLOSE_CRASH         //Close log because run finished due to error
    };

    //Closes the current logfile, if open.
    static void CloseLog(const eCloseReason reason);

    //Returns true if the current debug level is greater or equal
    //the passed-in value.
    static bool ChkDbgLvl(const int threshold)
    {
        return sm_DebugLevel >= threshold;
    }

    //Returns the maximum size in bytes to which a log file can grow
    //before it is pinched.
    static unsigned GetMaxSizeofLog()
    {
        return sm_MaxSizeofLog;
    }

    //Sets the current debug level.
    static void SetDebugLevel(const int level)
    {
        sm_DebugLevel = level;
    }

    //Returns the current debug level.
    static int GetDebugLevel()
    {
        return sm_DebugLevel;
    }

    //Enables/disables debug output to the console.
    static void SetConsoleEnabled(const bool b)
    {
        sm_ConsoleEnabled = b;
    }

    //Returns true if debug output to the console is enabled.
    static bool IsConsoleEnabled()
    {
        return sm_ConsoleEnabled;
    }

    //Generates a time string using standard time functions.
    static void MakeTimeStr(char (&str)[32]);

private:
    SrvLog();
    SrvLog(const SrvLog&);
    SrvLog& operator=(const SrvLog&);

    void OpenLogfile();
    void VLog(const char* fmt, va_list args, const bool nl);

    LogLevel    m_Level;
    const char* m_Facility;
    const char* m_FileName;
    int         m_FileLine;
    const char* m_FuncName;
    bool        m_PrintPreamble : 1;

    enum
    {
        MAX_BASE_PATH_SIZE  = 128,
        MAX_BASE_NAME_SIZE  = 64,
        MAX_FULL_NAME_SIZE  = MAX_BASE_PATH_SIZE + MAX_BASE_NAME_SIZE + 64
    };

    static bool sm_ClassInitialized;
    static rage::sysCriticalSectionToken sm_Cs;
    static FILE* sm_Fp;
    static unsigned sm_NumFilesOpened;
    static int sm_DebugLevel;
    static bool sm_ConsoleEnabled;
    static unsigned sm_SizeofLog;
    static unsigned sm_MaxSizeofLog;
    static unsigned sm_MaxPinched;
    static char sm_LogBasePath[MAX_BASE_PATH_SIZE];
    static char sm_LogBaseName[MAX_BASE_NAME_SIZE];
    static char sm_LogFullName[MAX_FULL_NAME_SIZE];

};

#if !__NO_OUTPUT

#define slogDebug1(fac, a)  while(!__NO_OUTPUT && SrvLog::ChkDbgLvl(1)){SrvLog(SrvLog::LOG_DEBUG, fac, __FILE__, __LINE__, __FUNCTION__).LogLine a;break;}
#define slogDebug2(fac, a)  while(!__NO_OUTPUT && SrvLog::ChkDbgLvl(2)){SrvLog(SrvLog::LOG_DEBUG, fac, __FILE__, __LINE__, __FUNCTION__).LogLine a;break;}
#define slogDebug3(fac, a)  while(!__NO_OUTPUT && SrvLog::ChkDbgLvl(3)){SrvLog(SrvLog::LOG_DEBUG, fac, __FILE__, __LINE__, __FUNCTION__).LogLine a;break;}
#define slogWarning(fac, a) while(!__NO_OUTPUT){SrvLog(SrvLog::LOG_WARNING, fac, __FILE__, __LINE__, __FUNCTION__).LogLine a;break}
#define slogError(fac, a)   while(!__NO_OUTPUT){SrvLog(SrvLog::LOG_ERROR, fac, __FILE__, __LINE__, __FUNCTION__).LogLine a;break;}
#define slogInfo(fac, a)    while(!__NO_OUTPUT){SrvLog(SrvLog::LOG_INFO, fac, __FILE__, __LINE__, __FUNCTION__).LogLine a;break;}

#else

#define slogDebug1(fac, a)
#define slogDebug2(fac, a)
#define slogDebug3(fac, a)
#define slogWarning(fac, a)
#define slogError(fac, a)
#define slogInfo(fac, a)

#endif  //__NO_OUTPUT

#endif  //SERVER_DIAG_H
