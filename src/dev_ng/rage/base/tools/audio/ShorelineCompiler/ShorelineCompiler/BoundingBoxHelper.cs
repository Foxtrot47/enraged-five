﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace rage.ShorelineCompiler
{

    class BoundingBoxHelper : Vector3
    {
        public BoundingBoxHelper()
        {
            m_min = new Vector3(0, 0, 0);
            m_max = new Vector3(0, 0, 0);
        }

        public void Set(Vector3 min, Vector3 max)
        {
            m_min = min;
            m_max = max; 
        }
        public void GrowPoint(Vector3 point) 
        {
            m_min = Min(point, m_min);
            m_max = Max(point, m_max);
        }
        public Vector3 GetMin() { return m_min; }
        public Vector3 GetMax() { return m_max; }
        public Vector3 GetCenter() 
        {
            float x = (m_max.GetX() + m_min.GetX()) * 0.5f;
            float y = (m_max.GetY() + m_min.GetY()) * 0.5f;
            float z = (m_max.GetZ() + m_min.GetZ()) * 0.5f;
            Vector3 centre = new Vector3(x, y, z);
            return centre; 
        }
        public void Invalidate() 
        {
            m_min.SetX((float)3.4028234663852886e+38);
            m_min.SetY((float)3.4028234663852886e+38);
            m_min.SetZ((float)3.4028234663852886e+38);

            m_max.SetX((float)-3.4028234663852886e+38);
            m_max.SetY((float)-3.4028234663852886e+38);
            m_max.SetZ((float)-3.4028234663852886e+38);
            
        }

        public bool ContainsPointFlat(Vector3 point) 
        {
            return (IsGreaterThanOrEqualAllFLat(point, m_min) & IsLessThanOrEqualAllFLat(point, m_max)); 
        }

        private bool IsGreaterThanOrEqualAllFLat(Vector3 ponint, Vector3 point2)
        {
            return ((ponint.GetX() >= point2.GetX()) &&
                (ponint.GetY() >= point2.GetY()) ? true : false);
        }

        private bool IsLessThanOrEqualAllFLat(Vector3 ponint, Vector3 point2)
        {
            return ((point2.GetX() >= ponint.GetX()) &&
                (point2.GetY() >= ponint.GetY()) ? true : false);
        } 

        Vector3 m_min, m_max;
    }
}
