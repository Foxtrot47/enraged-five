﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.IO;

using rage;
using rage.ToolLib.Logging;
using rage.Transform;

namespace rage.ShorelineCompiler
{

    [CanTransform("ShoreLineLakeAudioSettings")]
    public class LakeTransformer : ITransformer
    {

        public bool Init(ILog log, params string[] args)
        {
            log.Information("ShoreLineLakeAudioSettings init");
            return true;
        }


        //set up the close lake type based on the depth,width and speed.
        public TransformResult Transform(ILog log, XElement element)
        {
            TransformResult result = new TransformResult { Result = null };

            XElement lakeShore = element.Element("ShoreType");
            if (lakeShore == null)
            {
                XElement lakeShoreElem = new XElement("ShoreType", "AUD_LAKE_SHORE_ROCKS");
                element.Add(lakeShoreElem);
                lakeShore = element.Element("ShoreType");
            }
            XElement lakeBehaviour = element.Element("LakeBehaviour");
            if (lakeBehaviour == null)
            {
                XElement behaviourElem = new XElement("LakeBehaviour", "AUD_LAKE_CALM");
                element.Add(behaviourElem);
                lakeBehaviour = element.Element("LakeBehaviour");
            }
            Console.WriteLine(lakeShore.Value);
            Console.WriteLine(lakeBehaviour.Value);

            try
            {
                // First set the distant type
//                 if (element.Element("DistantType") == null)
//                 {
//                     XElement distantType = new XElement("DistantType", "AUD_DISTANT_RIVER_DEFAULT");
//                     element.Add(distantType);
//                     if (element.Element("DistantType") != null)
//                     {
//                         element.Element("DistantType").SetValue("AUD_DISTANT_RIVER_DEFAULT");
//                     }
//                 }
//                 if (element.Element("DistantType") != null && element.Element("DistantType").Value == "AUD_DISTANT_RIVER_DEFAULT")
//                 {
//                     element.Element("DistantType").Value = GetRiverDistantType(riverDepth.Value, riverWidth.Value, riverSpeed.Value);
//                     Console.WriteLine(element.Element("DistantType").Value);
//                 }

                // Close type
                if (element.Element("CloseType") == null)
                {
                    XElement closeType = new XElement("CloseType", "AUD_CLOSE_ROCKS_CALM");
                    element.Add(closeType);
                    if (element.Element("CloseType") != null)
                    {
                        element.Element("CloseType").SetValue("AUD_CLOSE_ROCKS_CALM");
                    }
                }
                if (element.Element("CloseType") != null)
                {
                    element.Element("CloseType").Value = GetLakeCloseType(lakeShore.Value, lakeBehaviour.Value);
                    Console.WriteLine(element.Element("CloseType").Value);
                }
                result.Result = element;
            }
            catch (System.Exception ex)
            {
                log.Exception(ex);
            }
            return result;
        }

        public void Shutdown()
        {

        }

//         public string GetRiverDistantType(string depth, string width, string speed)
//         {
//             string result = "AUD_DISTANT_RIVER_NONE";
// 
//             switch (depth)
//             {
//                 case "AUD_RIVER_DEPTH_VERY_SHALLOW":
//                     {
//                         if (speed == "AUD_RIVER_SPEED_MEDIUM" || speed == "AUD_RIVER_SPEED_FAST")
//                         {
//                             result = "AUD_DISTANT_RIVER_LIGHT";
//                         }
//                     }
//                     break;
//                 case "AUD_RIVER_DEPTH_SHALLOW":
//                     if (width == "AUD_RIVER_WIDTH_VERY_NARROW" || (width == "AUD_RIVER_WIDTH_NARROW" && speed == "AUD_RIVER_SPEED_MEDIUM"))
//                     {
//                         if (speed == "AUD_RIVER_SPEED_MEDIUM" || speed == "AUD_RIVER_SPEED_FAST")
//                         {
//                             result = "AUD_DISTANT_RIVER_LIGHT";
//                         }
//                     }
//                     else if (width == "AUD_RIVER_WIDTH_NARROW")
//                     {
//                         if (speed == "AUD_RIVER_SPEED_FAST")
//                         {
//                             result = "AUD_DISTANT_RIVER_MEDIUM";
//                         }
//                     }
//                     else if (width == "AUD_RIVER_WIDTH_WIDE")
//                     {
//                         if (speed == "AUD_RIVER_SPEED_MEDIUM")
//                         {
//                             result = "AUD_DISTANT_RIVER_MEDIUM";
//                         }
//                         else if (speed == "AUD_RIVER_SPEED_FAST")
//                         {
//                             result = "AUD_DISTANT_RIVER_HEAVY";
//                         }
//                     }
//                     break;
//                 case "AUD_RIVER_DEPTH_MEDIUM":
//                     if (width == "AUD_RIVER_WIDTH_NARROW")
//                     {
//                         if (speed == "AUD_RIVER_SPEED_MEDIUM")
//                         {
//                             result = "AUD_DISTANT_RIVER_LIGHT";
//                         }
//                     }
//                     else if (width == "AUD_RIVER_WIDTH_WIDE")
//                     {
//                         if (speed == "AUD_RIVER_SPEED_MEDIUM")
//                         {
//                             result = "AUD_DISTANT_RIVER_MEDIUM";
//                         }
//                         else if (speed == "AUD_RIVER_SPEED_FAST")
//                         {
//                             result = "AUD_DISTANT_RIVER_HEAVY";
//                         }
//                     }
//                     break;
//                 case "AUD_RIVER_DEPTH_DEEP":
//                     if (width == "AUD_RIVER_WIDTH_NARROW" || width == "AUD_RIVER_WIDTH_WIDE")
//                     {
//                         if (speed == "AUD_RIVER_SPEED_MEDIUM")
//                         {
//                             result = "AUD_DISTANT_RIVER_MEDIUM";
//                         }
//                         else if (speed == "AUD_RIVER_SPEED_FAST")
//                         {
//                             result = "AUD_DISTANT_RIVER_HEAVY";
//                         }
//                     }
//                     break;
//                 default:
//                     break;
// 
//             }
//             return result;
//         }
        public string GetLakeCloseType(string shoreType, string lakeBehaviour)
        {
            string result = "AUD_CLOSE_LAKE_NONE";

            switch (shoreType)
            {
                case "AUD_LAKE_SHORE_VEGETATION":
                    {
                        if (lakeBehaviour == "AUD_LAKE_CALM")
                        {
                             result = "AUD_CLOSE_VEG_CALM";
                        }
                        else if (lakeBehaviour == "AUD_LAKE_MED")
                        {
                            result = "AUD_CLOSE_VEG_MED";
                        }
                        else if (lakeBehaviour == "AUD_LAKE_BUSY")
                        {
                            result = "AUD_CLOSE_VEG_BUSY";
                        }
                    }
                    break;
                case "AUD_LAKE_SHORE_ROCKS":
                    {
                        if (lakeBehaviour == "AUD_LAKE_CALM")
                        {
                             result = "AUD_CLOSE_ROCKS_CALM";
                        }
                        else if (lakeBehaviour == "AUD_LAKE_MED")
                        {
                            result = "AUD_CLOSE_ROCKS_MED";
                        }
                        else if (lakeBehaviour == "AUD_LAKE_BUSY")
                        {
                            result = "AUD_CLOSE_ROCKS_BUSY";
                        }
                    }
                    break;
                case "AUD_LAKE_SHORE_SAND":
                    {
                        if (lakeBehaviour == "AUD_LAKE_CALM")
                        {
                             result = "AUD_CLOSE_SAND_CALM";
                        }
                        else if (lakeBehaviour == "AUD_LAKE_MED")
                        {
                            result = "AUD_CLOSE_SAND_MED";
                        }
                        else if (lakeBehaviour == "AUD_LAKE_BUSY")
                        {
                            result = "AUD_CLOSE_SAND_BUSY";
                        }
                    }
                    break;
                default:
                    break;

            }
            return result;
        }
    }
}
