﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace rage.ShorelineCompiler
{
    class Vector3
    {
        public Vector3()
        {
            x = 0;
            y = 0;
            z = 0;
        }
        public Vector3(float setX, float setY, float setZ)
        {
            x = setX;
            y = setY;
            z = setZ;
        }

        // PURPOSE: Returns the x component of this vector.
        public float GetX() { return x; }

        // PURPOSE: Returns the y component of this vector.
        public float GetY() { return y; }

        // PURPOSE: Returns the z component of this vector.
        public float GetZ() { return z; }

        // PURPOSE: Set the x component of this vector.
        // PARAMS
        //    f - The new value for the x component.
        public void SetX(float f) { x = f; }

        // PURPOSE: Set the y component of this vector.
        // PARAMS
        //    f - The new value for the y component.
        public void SetY(float f) { y = f; }

        // PURPOSE: Set the z component of this vector.
        // PARAMS
        //    f - The new value for the z component.
        public void SetZ(float f) { z = f; }

        static public Vector3 Create(XElement v)
        {
            Vector3 vOut = new Vector3();
            vOut.SetX(float.Parse(v.Element("X").Value));
            vOut.SetY(float.Parse(v.Element("Y").Value));
            return vOut;
        }

        public Vector3 Min(Vector3 vec1, Vector3 vec2)
        {
            Vector3 outVect = new Vector3(FPMin(vec1.x, vec2.x), FPMin(vec1.y, vec2.y), FPMin(vec1.z, vec2.z));
            return outVect;
        }

        public Vector3 Max(Vector3 vec1, Vector3 vec2)
        {
            Vector3 outVect = new Vector3(FPMax(vec1.x, vec2.x), FPMax(vec1.y, vec2.y), FPMax(vec1.z, vec2.z));
            return outVect;
        }

        private float FPMin(float f, float f1)
        {
            if (f <= f1)
                return f;
            else return f1;
        }

        private float FPMax(float f, float f1)
        {
            if (f > f1)
                return f;
            else return f1;
        }

        float x, y, z;
    }
}
