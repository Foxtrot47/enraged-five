﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.IO;
 
using rage;
using rage.ToolLib.Logging;
using rage.Transform;

namespace rage.ShorelineCompiler
{

    [CanTransform("ShoreLinePoolAudioSettings")]
    public class ShorelineTransformer : ITransformer
    {

        public bool Init(ILog log, params string[] args)
        {
            log.Information("ShorelinePoolTransformer init");
            return true;
        }


        //Divide the shorline in 4: Analyze the shoreline and calculates the index of the last shoreline node per quadrant.
        public TransformResult Transform(ILog log, XElement element)
        {
            TransformResult result = new TransformResult { Result = null };

            BoundingBoxHelper m_AABB = new BoundingBoxHelper();
            m_AABB.Invalidate();
            IEnumerable<XElement> shorelinePoints = element.Element("ShoreLinePoints").Elements("Instance");
            if (shorelinePoints.Count() == 0)
            {
                // old school format
                shorelinePoints = element.Elements("ShoreLinePoints");
            }
            List<Vector3> points = new List<Vector3>();
            points.AddRange(from sp in shorelinePoints select Vector3.Create(sp));

            for (int i = 0; i < points.Count(); i++)
            {
                m_AABB.GrowPoint(points[i]);
            } 

            // First quadrant:
            XElement firstQuadElement = element.Element("FirstQuadIndex");
            XElement secondQuadElement = element.Element("SecondQuadIndex");
            XElement thirdQuadElement = element.Element("ThirdQuadIndex");
            XElement fourthQuadElement = element.Element("FourthQuadIndex");
            if (firstQuadElement == null || firstQuadElement.Value != "-1")
            {
                return result;
            }
            else
            {
                try
                {
                    // First quad
                    BoundingBoxHelper firstQuad = new BoundingBoxHelper();
                    firstQuad.Set(m_AABB.GetMin(), m_AABB.GetCenter());
                    // Second quad
                    BoundingBoxHelper secondQuad = new BoundingBoxHelper();
                    Vector3 min = new Vector3(m_AABB.GetCenter().GetX(), m_AABB.GetMin().GetY(), m_AABB.GetMin().GetZ());
                    Vector3 max = new Vector3(m_AABB.GetMax().GetX(), m_AABB.GetCenter().GetY(), m_AABB.GetMin().GetZ());
                    secondQuad.Set(min, max);
                    // Third quad
                    BoundingBoxHelper thirdQuad = new BoundingBoxHelper();
                    thirdQuad.Set(m_AABB.GetCenter(), m_AABB.GetMax());
                    // Fourth quad
                    BoundingBoxHelper fourthQuad = new BoundingBoxHelper();
                    min = new Vector3(m_AABB.GetMin().GetX(), m_AABB.GetCenter().GetY(), m_AABB.GetMin().GetZ());
                    max = new Vector3(m_AABB.GetCenter().GetX(), m_AABB.GetMax().GetY(), m_AABB.GetMin().GetZ());
                    fourthQuad.Set(min, max);

                    for (int i = 0; i < points.Count(); i++)
                    {
                        Vector3 shorelinePoint = new Vector3(points[i].GetX(), points[i].GetY(), 0);
                        if (firstQuad.ContainsPointFlat(shorelinePoint) && secondQuadElement.Value == "-1")
                        {
                            element.Element("FirstQuadIndex").SetValue(i);
                        }
                        else if (secondQuad.ContainsPointFlat(shorelinePoint))
                        {
                            element.Element("SecondQuadIndex").SetValue(i); 
                        }
                        else if (thirdQuad.ContainsPointFlat(shorelinePoint))
                        {
                            element.Element("ThirdQuadIndex").SetValue(i);
                        }
                        else if (fourthQuad.ContainsPointFlat(shorelinePoint))
                        {
                            element.Element("FourthQuadIndex").SetValue(i);
                        }
                    }
                    result.Result = element;
                }
                catch (System.Exception ex)
                {
                    log.Exception(ex);
                }
            }
            return result;
        }

        public void Shutdown()
        {

        }

    }
}
