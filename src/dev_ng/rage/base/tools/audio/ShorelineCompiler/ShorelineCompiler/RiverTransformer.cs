﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.IO;

using rage;
using rage.ToolLib.Logging;
using rage.Transform;

namespace rage.ShorelineCompiler
{

    [CanTransform("ShoreLineRiverAudioSettings")]
    public class RiverTransformer : ITransformer
    {

        public bool Init(ILog log, params string[] args)
        {
            log.Information("ShoreLineRiverAudioSettings init");
            return true;
        }


        //set up the close river type based on the depth,width and speed.
        public TransformResult Transform(ILog log, XElement element)
        {
            TransformResult result = new TransformResult { Result = null };

            XElement riverDepth = element.Element("Depth");
            if (riverDepth == null)
            {
                XElement riverDepthElem = new XElement("Depth", "AUD_RIVER_DEPTH_MEDIUM");
                element.Add(riverDepthElem);
                riverDepth = element.Element("Depth");
            }
            XElement riverWidth = element.Element("Width");
            if (riverWidth == null)
            {
                XElement riverWidthElem = new XElement("Width", "AUD_RIVER_WIDTH_WIDE");
                element.Add(riverWidthElem);
                riverWidth = element.Element("Width");
            }
            XElement riverSpeed = element.Element("Speed");
            if (riverSpeed == null)
            {
                XElement riverSpeedElem = new XElement("Speed", "AUD_RIVER_SPEED_MEDIUM");
                element.Add(riverSpeedElem);
                riverSpeed = element.Element("Speed");
            }
            Console.WriteLine(riverDepth.Value);
            Console.WriteLine(riverWidth.Value);
            Console.WriteLine(riverSpeed.Value);

            try
            {
                // First set the distant type
                if (element.Element("DistantType") == null)
                {
                    XElement distantType = new XElement("DistantType", "AUD_DISTANT_RIVER_DEFAULT");
                    element.Add(distantType);
                    if (element.Element("DistantType") != null)
                    {
                        element.Element("DistantType").SetValue("AUD_DISTANT_RIVER_DEFAULT");
                    }
                }
                if (element.Element("DistantType") != null && element.Element("DistantType").Value == "AUD_DISTANT_RIVER_DEFAULT")
                {
                    element.Element("DistantType").Value = GetRiverDistantType(riverDepth.Value, riverWidth.Value, riverSpeed.Value);
                    Console.WriteLine(element.Element("DistantType").Value);
                }

                // Close type
                if (element.Element("CloseType") == null)
                {
                    XElement distantType = new XElement("CloseType", "AUD_CLOSE_MED_WIDE_MED");
                    element.Add(distantType);
                    if (element.Element("CloseType") != null)
                    {
                        element.Element("CloseType").SetValue("AUD_CLOSE_MED_WIDE_MED");
                    }
                }
                if (element.Element("CloseType") != null)
                {
                    element.Element("CloseType").Value = GetRiverCloseType(riverDepth.Value, riverWidth.Value, riverSpeed.Value);
                    Console.WriteLine(element.Element("CloseType").Value);
                }
                result.Result = element;
            }
            catch (System.Exception ex)
            {
                log.Exception(ex);
            }
            return result;
        }

        public void Shutdown()
        {

        }

        public string GetRiverDistantType(string depth, string width, string speed)
        {
            string result = "AUD_DISTANT_RIVER_NONE";

            switch (depth)
            {
                case "AUD_RIVER_DEPTH_VERY_SHALLOW":
                    {
                        if (speed == "AUD_RIVER_SPEED_MEDIUM" || speed == "AUD_RIVER_SPEED_FAST")
                        {
                            result = "AUD_DISTANT_RIVER_LIGHT";
                        }
                    }
                    break;
                case "AUD_RIVER_DEPTH_SHALLOW":
                    if (width == "AUD_RIVER_WIDTH_VERY_NARROW" || (width == "AUD_RIVER_WIDTH_NARROW" && speed == "AUD_RIVER_SPEED_MEDIUM"))
                    {
                        if (speed == "AUD_RIVER_SPEED_MEDIUM" || speed == "AUD_RIVER_SPEED_FAST")
                        {
                            result = "AUD_DISTANT_RIVER_LIGHT";
                        }
                    }
                    else if (width == "AUD_RIVER_WIDTH_NARROW")
                    {
                        if (speed == "AUD_RIVER_SPEED_FAST")
                        {
                            result = "AUD_DISTANT_RIVER_MEDIUM";
                        }
                    }
                    else if (width == "AUD_RIVER_WIDTH_WIDE")
                    {
                        if (speed == "AUD_RIVER_SPEED_MEDIUM")
                        {
                            result = "AUD_DISTANT_RIVER_MEDIUM";
                        }
                        else if (speed == "AUD_RIVER_SPEED_FAST")
                        {
                            result = "AUD_DISTANT_RIVER_HEAVY";
                        }
                    }
                    break;
                case "AUD_RIVER_DEPTH_MEDIUM":
                    if (width == "AUD_RIVER_WIDTH_NARROW")
                    {
                        if (speed == "AUD_RIVER_SPEED_MEDIUM")
                        {
                            result = "AUD_DISTANT_RIVER_LIGHT";
                        }
                    }
                    else if (width == "AUD_RIVER_WIDTH_WIDE")
                    {
                        if (speed == "AUD_RIVER_SPEED_MEDIUM")
                        {
                            result = "AUD_DISTANT_RIVER_MEDIUM";
                        }
                        else if (speed == "AUD_RIVER_SPEED_FAST")
                        {
                            result = "AUD_DISTANT_RIVER_HEAVY";
                        }
                    }
                    break;
                case "AUD_RIVER_DEPTH_DEEP":
                    if (width == "AUD_RIVER_WIDTH_NARROW" || width == "AUD_RIVER_WIDTH_WIDE")
                    {
                        if (speed == "AUD_RIVER_SPEED_MEDIUM")
                        {
                            result = "AUD_DISTANT_RIVER_MEDIUM";
                        }
                        else if (speed == "AUD_RIVER_SPEED_FAST")
                        {
                            result = "AUD_DISTANT_RIVER_HEAVY";
                        }
                    }
                    break;
                default:
                    break;

            }
            return result;
        }
        public string GetRiverCloseType(string depth, string width, string speed)
        {
            string result = "AUD_CLOSE_RIVER_NONE";

            switch (depth)
            {
                case "AUD_RIVER_DEPTH_VERY_SHALLOW":
                    {
                        if (width == "AUD_RIVER_WIDTH_VERY_NARROW")
                        {
                            if (speed == "AUD_RIVER_SPEED_MEDIUM")
                            {
                                result = "AUD_CLOSE_VSHALLOW_VNARROW_MED";
                            }
                            else if (speed == "AUD_RIVER_SPEED_FAST")
                            {
                                result = "AUD_CLOSE_VSHALLOW_VNARROW_FAST";
                            }
                        }
                        else if (width == "AUD_RIVER_WIDTH_NARROW")
                        {
                            if (speed == "AUD_RIVER_SPEED_MEDIUM")
                            {
                                result = "AUD_CLOSE_VSHALLOW_NARROW_MED";
                            }
                            else if (speed == "AUD_RIVER_SPEED_FAST")
                            {
                                result = "AUD_CLOSE_VSHALLOW_NARROW_FAST";
                            }
                        }
                        else if (width == "AUD_RIVER_WIDTH_WIDE")
                        {
                            if (speed == "AUD_RIVER_SPEED_MEDIUM")
                            {
                                result = "AUD_CLOSE_VSHALLOW_WIDE_MED";
                            }
                            else if (speed == "AUD_RIVER_SPEED_FAST")
                            {
                                result = "AUD_CLOSE_VSHALLOW_WIDE_FAST";
                            }
                        }
                    }
                    break;
                case "AUD_RIVER_DEPTH_SHALLOW":
                    {
                        if (width == "AUD_RIVER_WIDTH_VERY_NARROW")
                        {
                            if (speed == "AUD_RIVER_SPEED_MEDIUM")
                            {
                                result = "AUD_CLOSE_SHALLOW_VNARROW_MED";
                            }
                            else if (speed == "AUD_RIVER_SPEED_FAST")
                            {
                                result = "AUD_CLOSE_SHALLOW_VNARROW_FAST";
                            }
                        }
                        else if (width == "AUD_RIVER_WIDTH_NARROW")
                        {
                            if (speed == "AUD_RIVER_SPEED_MEDIUM")
                            {
                                result = "AUD_CLOSE_SHALLOW_NARROW_MED";
                            }
                            else if (speed == "AUD_RIVER_SPEED_FAST")
                            {
                                result = "AUD_CLOSE_SHALLOW_NARROW_FAST";
                            }
                        }
                        else if (width == "AUD_RIVER_WIDTH_WIDE")
                        {
                            if (speed == "AUD_RIVER_SPEED_MEDIUM")
                            {
                                result = "AUD_CLOSE_SHALLOW_WIDE_MED";
                            }
                            else if (speed == "AUD_RIVER_SPEED_FAST")
                            {
                                result = "AUD_CLOSE_SHALLOW_WIDE_FAST";
                            }
                        }
                    }
                    break;
                case "AUD_RIVER_DEPTH_MEDIUM":
                    {
                        if (width == "AUD_RIVER_WIDTH_NARROW")
                        {
                            if (speed == "AUD_RIVER_SPEED_SLOW")
                            {
                                result = "AUD_CLOSE_MED_NARROW_SLOW";
                            }
                            else if (speed == "AUD_RIVER_SPEED_MEDIUM")
                            {
                                result = "AUD_CLOSE_MED_NARROW_MED";
                            }
                            else if (speed == "AUD_RIVER_SPEED_FAST")
                            {
                                result = "AUD_CLOSE_MED_NARROW_FAST";
                            }
                        }
                        else if (width == "AUD_RIVER_WIDTH_WIDE")
                        {
                            if (speed == "AUD_RIVER_SPEED_SLOW")
                            {
                                result = "AUD_CLOSE_MED_WIDE_SLOW";
                            }
                            else if (speed == "AUD_RIVER_SPEED_MEDIUM")
                            {
                                result = "AUD_CLOSE_MED_WIDE_MED";
                            }
                            else if (speed == "AUD_RIVER_SPEED_FAST")
                            {
                                result = "AUD_CLOSE_MED_WIDE_FAST";
                            }
                        }
                    }
                    break;
                case "AUD_RIVER_DEPTH_DEEP":
                    {
                        if (width == "AUD_RIVER_WIDTH_NARROW")
                        {
                            if (speed == "AUD_RIVER_SPEED_MEDIUM")
                            {
                                result = "AUD_CLOSE_DEEP_NARROW_MED";
                            }
                            else if (speed == "AUD_RIVER_SPEED_FAST")
                            {
                                result = "AUD_CLOSE_DEEP_NARROW_FAST";
                            }
                        }
                        else if (width == "AUD_RIVER_WIDTH_WIDE")
                        {
                            if (speed == "AUD_RIVER_SPEED_SLOW")
                            {
                                result = "AUD_CLOSE_RIVER_DEEP_WIDE_SLOW";
                            }
                            else if (speed == "AUD_RIVER_SPEED_MEDIUM")
                            {
                                result = "AUD_CLOSE_DEEP_WIDE_MED";
                            }
                            else if (speed == "AUD_RIVER_SPEED_FAST")
                            {
                                result = "AUD_CLOSE_DEEP_WIDE_FAST";

                            }
                        }
                    }
                    break;
                default:
                    break;

            }
            return result;
        }
    }
}
