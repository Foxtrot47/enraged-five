﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using rage.ShorelineCompiler;
using rage.ToolLib.Logging;

namespace ShorelineCompilerTest {
    public class Program {

        static void Main(string[] args) {

            ILog log = new TextLog(new ConsoleLogWriter(), new ContextStack());
            ShorelineTransformer transformer = new ShorelineTransformer();

            

            transformer.Init(log, new string[0]);
            foreach(XElement elemnt in getHardCodedTestData())
            {
                transformer.Transform(log, elemnt);
            }
            
        }

        //e.g. X:\gta5\audio\dev\ASSETS\OBJECTS\Core\Audio\GAMEOBJECTS\WATER\RIVERS.XML
        private static IEnumerable<XElement> getDataFromFile(string filePath)
        {
            return XDocument.Load(filePath, LoadOptions.None).Root.Elements();
        }

        private static IEnumerable<XElement> getHardCodedTestData()
        {
            return XDocument.Parse(@"
                <Objects>
                  <ShoreLineRiverAudioSettings name=""SHORELINE_RIVER_DAKOTA_0_0"">
                    <ActivationBox>
                      <Center>
                        <X>-611.088440</X>
                        <Y>-237.884674</Y>
                      </Center>
                      <Size>
                        <Width>1000</Width>
                        <Height>1000.00000000</Height>
                      </Size>
                      <RotationAngle>27.000000</RotationAngle>
                    </ActivationBox>
                    <RiverType>AUD_RIVER_MEDIUM</RiverType>
                    <ShoreLinePoints>
                      <X>-719.438110</X>
                      <Y>-304.616302</Y>
                      <RiverWidth>44.000000</RiverWidth>
                    </ShoreLinePoints>
                    <ShoreLinePoints>
                      <X>-702.778442</X>
                      <Y>-295.256866</Y>
                      <RiverWidth>45.000000</RiverWidth>
                    </ShoreLinePoints>
                    <ShoreLinePoints>
                      <X>-683.087952</X>
                      <Y>-287.415955</Y>
                      <RiverWidth>46.500000</RiverWidth>
                    </ShoreLinePoints>
                    <ShoreLinePoints>
                      <X>-666.940979</X>
                      <Y>-277.485199</Y>
                      <RiverWidth>51.500000</RiverWidth>
                    </ShoreLinePoints>
                    <ShoreLinePoints>
                      <X>-641.761658</X>
                      <Y>-270.701080</Y>
                      <RiverWidth>42.000000</RiverWidth>
                    </ShoreLinePoints>
                    <ShoreLinePoints>
                      <X>-630.518738</X>
                      <Y>-263.738464</Y>
                      <RiverWidth>42.000000</RiverWidth>
                    </ShoreLinePoints>
                    <ShoreLinePoints>
                      <X>-611.116943</X>
                      <Y>-250.675537</Y>
                      <RiverWidth>42.000000</RiverWidth>
                    </ShoreLinePoints>
                    <ShoreLinePoints>
                      <X>-585.933594</X>
                      <Y>-231.359619</Y>
                      <RiverWidth>41.000000</RiverWidth>
                    </ShoreLinePoints>
                    <ShoreLinePoints>
                      <X>-560.509399</X>
                      <Y>-205.885010</Y>
                      <RiverWidth>35.000000</RiverWidth>
                    </ShoreLinePoints>
                    <ShoreLinePoints>
                      <X>-544.098022</X>
                      <Y>-187.427032</Y>
                      <RiverWidth>39.000000</RiverWidth>
                    </ShoreLinePoints>
                    <ShoreLinePoints>
                      <X>-524.719543</X>
                      <Y>-165.783951</Y>
                      <RiverWidth>40.000000</RiverWidth>
                    </ShoreLinePoints>
                    <ShoreLinePoints>
                      <X>-510.230927</X>
                      <Y>-150.650330</Y>
                      <RiverWidth>39.500000</RiverWidth>
                    </ShoreLinePoints>
                    <NextShoreline>SHORELINE_RIVER_DAKOTA_0_1</NextShoreline>
                  </ShoreLineRiverAudioSettings>    
                </Objects>
            ").Root.Elements();
        }
    }
}
