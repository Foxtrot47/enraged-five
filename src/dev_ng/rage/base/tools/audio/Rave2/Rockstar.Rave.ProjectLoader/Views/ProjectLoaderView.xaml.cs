﻿namespace ProjectLoader3.Views
{
    using System.Windows;

    using audAssetManagement2;

    using ProjectLoader3.Enums;

    using rage;

    /// <summary>
    /// Interaction logic for ProjectLoaderWindow.xaml
    /// </summary>
    public partial class ProjectLoaderView : Window
    {
        /// <summary>
        /// The path.
        /// </summary>
        private readonly string path;

        /// <summary>
        /// The project list.
        /// </summary>
        private audProjectList projectList;

        /// <summary>
        /// The current project.
        /// </summary>
        private audProjectEntry currentProject;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProjectLoaderView"/> class.
        /// </summary>
        /// <param name="path">
        /// The path.
        /// </param>
        public ProjectLoaderView(string path)
        {
            this.path = path;
            this.InitializeComponent();
            this.InitProjectList();
        }

        /// <summary>
        /// Gets the action.
        /// </summary>
        public OnExitActions Action { get; private set; }

        /// <summary>
        /// Gets the project.
        /// </summary>
        public audProjectEntry Project { get; private set; }

        /// <summary>
        /// Gets the asset manager.
        /// </summary>
        public assetManager AssetManager { get; private set; }

        /// <summary>
        /// Enable input on the project settings controls.
        /// </summary>
        /// <param name="enable">
        /// Enabled status of inputs.
        /// </param>
        public void EnableInput(bool enable)
        {
            project.IsEnabled =
                userName.IsEnabled =
                password.IsEnabled =
                assetManager.IsEnabled =
                server.IsEnabled =
                project.IsEnabled =
                settings.IsEnabled =
                depotRoot.IsEnabled =
                softRoot.IsEnabled =
                getLatestPS3.IsEnabled =
                getLatestXbox360.IsEnabled =
                getLatestPC.IsEnabled =
                waveEditor.IsEnabled =
                okayBtn.IsEnabled = enable;
        }

        /// <summary>
        /// Initialize the project list.
        /// </summary>
        private void InitProjectList()
        {
            this.projectList = new audProjectList(this.path);

            if (this.projectList.Projects.Count == 0)
            {
                this.EnableInput(false);
            }
            else
            {
                foreach (var proj in this.projectList.Projects.ToArray())
                {
                    this.project.Items.Add(proj);
                }

                this.project.SelectedItem = project.Items[0];
                this.LoadProject((audProjectEntry)project.Items[0]);
            }
        }

        /// <summary>
        /// Load the project.
        /// </summary>
        /// <param name="projectEntry">
        /// The project entry.
        /// </param>
        private void LoadProject(audProjectEntry projectEntry)
        {
            this.currentProject = projectEntry;
            userName.Text = projectEntry.UserName;
            password.Text = projectEntry.Password;
            server.Text = projectEntry.AssetServer;
            project.Text = projectEntry.AssetProject;
            depotRoot.Text = projectEntry.DepotRoot;
            softRoot.Text = projectEntry.SoftRoot;
            getLatestPS3.IsChecked = projectEntry.GrabLatestPS3;
            getLatestXbox360.IsChecked = projectEntry.GrabLatestX360;
            getLatestPC.IsChecked = projectEntry.GrabLatestPC;
            waveEditor.Text = projectEntry.WaveEditor;
            this.InitAssetManagerList(projectEntry);
        }

        /// <summary>
        /// Initialize the asset manager list.
        /// </summary>
        /// <param name="projectEntry">
        /// The project entry.
        /// </param>
        private void InitAssetManagerList(audProjectEntry projectEntry)
        {
            assetManager.Items.Add("Perforce");
            assetManager.Items.Add("Local");
            
            // Set selected item
            switch (projectEntry.AssetManager)
            {
                case "3":
                    assetManager.SelectedItem = "Local";
                    break;
                default:
                    assetManager.SelectedItem = "Perforce";
                    break;
            }
        }

        /// <summary>
        /// Set action and close window when 'okay' button clicked.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The event args.
        /// </param>
        private void OkayBtnClick(object sender, RoutedEventArgs e)
        {
            this.Action = OnExitActions.LoadProject;
            this.Close();
        }

        /// <summary>
        /// Set action and close window when 'close' button clicked.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The event args.
        /// </param>
        private void CloseBtnClick(object sender, RoutedEventArgs e)
        {
            this.Action = OnExitActions.Cancel;
            this.Close();
        }

        /// <summary>
        /// Update the selected project when the user makes a selection.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The event args.
        /// </param>
        private void ProjectSelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            // TODO
        }
    }
}
