﻿#pragma checksum "..\..\..\Views\ProjectLoaderView.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "9D11B8D6FD845CF21C25B70E0E96DB74E63E829EFD4F527C60C269E0E2E374DA"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace ProjectLoader3.Views {
    
    
    /// <summary>
    /// ProjectLoaderView
    /// </summary>
    public partial class ProjectLoaderView : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 23 "..\..\..\Views\ProjectLoaderView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox project;
        
        #line default
        #line hidden
        
        
        #line 24 "..\..\..\Views\ProjectLoaderView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button newProjectBtn;
        
        #line default
        #line hidden
        
        
        #line 49 "..\..\..\Views\ProjectLoaderView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox userName;
        
        #line default
        #line hidden
        
        
        #line 52 "..\..\..\Views\ProjectLoaderView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox password;
        
        #line default
        #line hidden
        
        
        #line 55 "..\..\..\Views\ProjectLoaderView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox assetManager;
        
        #line default
        #line hidden
        
        
        #line 58 "..\..\..\Views\ProjectLoaderView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox server;
        
        #line default
        #line hidden
        
        
        #line 61 "..\..\..\Views\ProjectLoaderView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox workspace;
        
        #line default
        #line hidden
        
        
        #line 64 "..\..\..\Views\ProjectLoaderView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox settings;
        
        #line default
        #line hidden
        
        
        #line 67 "..\..\..\Views\ProjectLoaderView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox depotRoot;
        
        #line default
        #line hidden
        
        
        #line 70 "..\..\..\Views\ProjectLoaderView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox softRoot;
        
        #line default
        #line hidden
        
        
        #line 89 "..\..\..\Views\ProjectLoaderView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox getLatestPS3;
        
        #line default
        #line hidden
        
        
        #line 92 "..\..\..\Views\ProjectLoaderView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox getLatestXbox360;
        
        #line default
        #line hidden
        
        
        #line 95 "..\..\..\Views\ProjectLoaderView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox getLatestPC;
        
        #line default
        #line hidden
        
        
        #line 108 "..\..\..\Views\ProjectLoaderView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox waveEditor;
        
        #line default
        #line hidden
        
        
        #line 114 "..\..\..\Views\ProjectLoaderView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button closeBtn;
        
        #line default
        #line hidden
        
        
        #line 115 "..\..\..\Views\ProjectLoaderView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button okayBtn;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/ProjectLoader3;component/views/projectloaderview.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\Views\ProjectLoaderView.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.project = ((System.Windows.Controls.ComboBox)(target));
            
            #line 23 "..\..\..\Views\ProjectLoaderView.xaml"
            this.project.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.ProjectSelectionChanged);
            
            #line default
            #line hidden
            return;
            case 2:
            this.newProjectBtn = ((System.Windows.Controls.Button)(target));
            return;
            case 3:
            this.userName = ((System.Windows.Controls.TextBox)(target));
            return;
            case 4:
            this.password = ((System.Windows.Controls.TextBox)(target));
            return;
            case 5:
            this.assetManager = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 6:
            this.server = ((System.Windows.Controls.TextBox)(target));
            return;
            case 7:
            this.workspace = ((System.Windows.Controls.TextBox)(target));
            return;
            case 8:
            this.settings = ((System.Windows.Controls.TextBox)(target));
            return;
            case 9:
            this.depotRoot = ((System.Windows.Controls.TextBox)(target));
            return;
            case 10:
            this.softRoot = ((System.Windows.Controls.TextBox)(target));
            return;
            case 11:
            this.getLatestPS3 = ((System.Windows.Controls.CheckBox)(target));
            return;
            case 12:
            this.getLatestXbox360 = ((System.Windows.Controls.CheckBox)(target));
            return;
            case 13:
            this.getLatestPC = ((System.Windows.Controls.CheckBox)(target));
            return;
            case 14:
            this.waveEditor = ((System.Windows.Controls.TextBox)(target));
            return;
            case 15:
            this.closeBtn = ((System.Windows.Controls.Button)(target));
            
            #line 114 "..\..\..\Views\ProjectLoaderView.xaml"
            this.closeBtn.Click += new System.Windows.RoutedEventHandler(this.CloseBtnClick);
            
            #line default
            #line hidden
            return;
            case 16:
            this.okayBtn = ((System.Windows.Controls.Button)(target));
            
            #line 115 "..\..\..\Views\ProjectLoaderView.xaml"
            this.okayBtn.Click += new System.Windows.RoutedEventHandler(this.OkayBtnClick);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

