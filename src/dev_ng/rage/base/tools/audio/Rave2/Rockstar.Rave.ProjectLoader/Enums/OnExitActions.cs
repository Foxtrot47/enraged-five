﻿namespace ProjectLoader3.Enums
{
    public enum OnExitActions
    {
        LoadProject,
        Cancel
    }
}
