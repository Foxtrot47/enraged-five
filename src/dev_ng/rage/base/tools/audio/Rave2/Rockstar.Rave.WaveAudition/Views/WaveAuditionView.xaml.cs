﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WaveAuditionView.xaml.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   Interaction logic for WaveAuditionView.xaml
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Rockstar.Rave.WaveAudition.Views
{
    using System.Windows;

    using Rockstar.Rave.WaveAudition.ViewModels;

    /// <summary>
    /// Interaction logic for WaveAuditionView.xaml
    /// </summary>
    public partial class WaveAuditionView : Window
    {
        /// <summary>
        /// The view model.
        /// </summary>
        private readonly WaveAuditionViewModel viewModel;

        /// <summary>
        /// Initializes a new instance of the <see cref="WaveAuditionView"/> class.
        /// </summary>
        /// <param name="viewModel">
        /// The view model.
        /// </param>
        public WaveAuditionView(WaveAuditionViewModel viewModel)
        {
            this.viewModel = viewModel;
            this.InitializeComponent();
        }
    }
}
