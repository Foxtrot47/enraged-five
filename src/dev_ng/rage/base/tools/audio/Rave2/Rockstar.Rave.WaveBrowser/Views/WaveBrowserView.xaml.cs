﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WaveBrowserView.xaml.cs" company="">
//   
// </copyright>
// <summary>
//   Interaction logic for WaveBrowserView.xaml
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Rockstar.Rave.WaveBrowser.Views
{
    using System.Windows.Controls;

    using Rockstar.Rave.WaveBrowser.ViewModels;

    /// <summary>
    /// Interaction logic for WaveBrowserView.xaml
    /// </summary>
    public partial class WaveBrowserView : UserControl
    {
        /// <summary>
        /// The view model.
        /// </summary>
        private readonly WaveBrowserViewModel viewModel;

        /// <summary>
        /// Initializes a new instance of the <see cref="WaveBrowserView"/> class.
        /// </summary>
        /// <param name="viewModel">
        /// The view Model.
        /// </param>
        public WaveBrowserView(WaveBrowserViewModel viewModel)
        {
            this.viewModel = viewModel;
            this.DataContext = viewModel;
            this.InitializeComponent();
        }
    }
}
