﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TagEditorView.xaml.cs" company="Rockstar North">
//   
// </copyright>
// <summary>
//   Interaction logic for TagEditorView.xaml
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Rockstar.Rave.WaveBrowser.Views
{
    using System.Windows;

    using Rockstar.Rave.WaveBrowser.ViewModels;

    /// <summary>
    /// Interaction logic for TagEditorView.xaml
    /// </summary>
    public partial class TagEditorView : Window
    {
        /// <summary>
        /// The view model.
        /// </summary>
        private readonly TagEditorViewModel viewModel;

        /// <summary>
        /// Initializes a new instance of the <see cref="TagEditorView"/> class.
        /// </summary>
        /// <param name="viewModel">
        /// The view model.
        /// </param>
        public TagEditorView(TagEditorViewModel viewModel)
        {
            this.viewModel = viewModel;
            this.InitializeComponent();
        }
    }
}
