﻿// -----------------------------------------------------------------------
// <copyright file="UndoRedo.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace Rockstar.Common.UndoRedo
{
    using System.Collections.Generic;

    using Rockstar.Common.UndoRedo.Interfaces;

    /// <summary>
    /// Provides a container for basic undo/redo functionality.
    /// </summary>
    public class UndoRedo
    {
        /// <summary>
        /// The undo stack.
        /// </summary>
        private readonly Stack<IUndoRedoItem> undoStack;

        /// <summary>
        /// The redo stack.
        /// </summary>
        private readonly Stack<IUndoRedoItem> redoStack;

        /// <summary>
        /// Initializes a new instance of the <see cref="UndoRedo"/> class.
        /// </summary>
        public UndoRedo()
        {
            this.undoStack = new Stack<IUndoRedoItem>();
            this.redoStack = new Stack<IUndoRedoItem>();
        }
    }
}
