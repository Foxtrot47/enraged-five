﻿// -----------------------------------------------------------------------
// <copyright file="IUndoRedoItem.cs" company="Rockstar North">
// 
// </copyright>
// -----------------------------------------------------------------------

namespace Rockstar.Common.UndoRedo.Interfaces
{
    /// <summary>
    /// An item capable of undo/redo functionality should implement this interface.
    /// </summary>
    public interface IUndoRedoItem
    {
        // TODO
    }
}
