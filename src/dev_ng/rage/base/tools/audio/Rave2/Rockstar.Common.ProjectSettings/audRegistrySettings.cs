﻿using Microsoft.Win32;

namespace rage
{
    public class audRegistrySettings
    {
        public int AssetManagerType;
        public bool RegistryRead;

        public audRegistrySettings()
        {
            var common = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\\Rockstar Games\\RAGE Audio\\Build Manager");
            if (common != null)
            {
                AssetManagerType = (int) common.GetValue("AssetManagerType");
                RegistryRead = true;
            }
            else
            {
                RegistryRead = false;
            }
        }
    }
}