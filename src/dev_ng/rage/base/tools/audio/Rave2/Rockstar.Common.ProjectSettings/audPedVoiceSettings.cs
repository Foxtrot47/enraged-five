﻿using System.Xml;

namespace rage
{
    public class audPedVoiceSettings
    {
        private readonly string m_Episode;
        private readonly string m_GameObjectPath;
        private readonly string m_IDEPath;
        private readonly string m_SpeechPack;

        public audPedVoiceSettings(XmlNode node)
        {
            foreach (XmlNode child in node.ChildNodes)
            {
                switch (child.Name)
                {
                    case "Episode":
                        m_Episode = child.InnerText;
                        break;
                    case "SpeechPack":
                        m_SpeechPack = child.InnerText;
                        break;
                    case "IdePath":
                        m_IDEPath = child.InnerText;
                        break;
                    case "GameObjectPath":
                        m_GameObjectPath = child.InnerText;
                        break;
                }
            }
        }

        public string Episode
        {
            get { return m_Episode; }
        }

        public string SpeechPack
        {
            get { return m_SpeechPack; }
        }

        public string IdePath
        {
            get { return m_IDEPath; }
        }

        public string SpeechObjectPath
        {
            get { return m_GameObjectPath; }
        }
    }
}