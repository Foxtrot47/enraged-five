﻿using System.Collections.Generic;
using System.Xml;

namespace rage
{
    public class audBuildSet
    {
        public audBuildSet()
        {
            BuildElements = new List<audBuildElement>();
        }

        public audBuildSet(XmlNode n) : this()
        {
            Name = n.Attributes["name"].InnerText;
            foreach (XmlNode c in n.ChildNodes)
            {
                if (c.Name == "BuildElement")
                {
                    BuildElements.Add(new audBuildElement(c));
                }
            }
        }

        public List<audBuildElement> BuildElements { get; private set; }
        public string Name { get; set; }
    }
}