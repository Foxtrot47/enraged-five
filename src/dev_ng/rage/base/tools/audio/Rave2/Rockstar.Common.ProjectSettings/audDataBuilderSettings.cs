using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Xml;

namespace rage
{
    public class audDataBuilderSettings
    {
        public audDataBuilderSettings(XmlNode settingsNode)
        {
            if (settingsNode == null)
            {
                throw new ArgumentNullException("settingsNode");
            }

            foreach (XmlNode node in settingsNode.ChildNodes)
            {
                switch (node.Name)
                {
                    case "Mail":
                        {
                            MailSettings = new AudioDataBuilderMailSettings(node);
                            break;
                        }
                }
            }
            if (MailSettings == null)
            {
                throw new ApplicationException("Missing required \"Mail\" node from AudioDataBuilderSettings");
            }
        }

        public AudioDataBuilderMailSettings MailSettings { get; private set; }
    }

    public class AudioDataBuilderMailSettings
    {
        public AudioDataBuilderMailSettings(XmlNode mailNode)
        {
            if (mailNode == null)
            {
                throw new ArgumentNullException("mailNode");
            }

            var addresses = new List<string>();
            foreach (XmlNode node in mailNode.ChildNodes)
            {
                switch (node.Name)
                {
                    case "Host":
                        {
                            Host = node.InnerText;
                            break;
                        }
                    case "FromAddress":
                        {
                            FromAddress = node.InnerText;
                            break;
                        }
                    case "Addresses":
                        {
                            foreach (XmlNode addressNode in node.ChildNodes)
                            {
                                if (addressNode.Name == "Address")
                                {
                                    var mailAddress = addressNode.InnerText;
                                    if (!Regex.IsMatch(mailAddress, ".+\\@.+\\..+"))
                                    {
                                        throw new ArgumentException(string.Format("Invalid e-mail address in AudioDataBuilderSettings: {0}", mailAddress));
                                    }
                                    addresses.Add(mailAddress);
                                }
                            }
                            
                            break;
                        }
                }
            }

            if (string.IsNullOrEmpty(Host))
            {
                throw new ApplicationException("Invalid mail host specified in AudioDataBuilderSettings");
            }

            if (addresses.Count == 0)
            {
                throw new ApplicationException("No e-mail addresses specified in AudioDataBuilderSettings");
            }

            Addresses = addresses.AsReadOnly();
        }

        public string Host { get; private set; }
        public string FromAddress { get; private set; }
        public IList<string> Addresses { get; private set; }
    }
}