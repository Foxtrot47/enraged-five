﻿using System.Xml;

namespace rage
{
    public class audObjectDefinition
    {
        public readonly string ClassIncludePath;
        public readonly string ClassPrefix;
        public readonly string DefinitionsFile;

        public audObjectDefinition(XmlNode node)
        {
            foreach (XmlNode child in node.ChildNodes)
            {
                switch (child.Name)
                {
                    case "DefinitionsFile":
                        DefinitionsFile = child.InnerText;
                        break;
                    case "ClassPrefix":
                        ClassPrefix = child.InnerText;
                        break;
                    case "ClassIncludePath":
                        ClassIncludePath = child.InnerText;
                        break;
                }
            }
        }
    }
}