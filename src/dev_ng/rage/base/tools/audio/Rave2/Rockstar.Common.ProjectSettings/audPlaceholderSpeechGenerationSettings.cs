using System;

namespace rage
{
    public class audPlaceholderSpeechGenerationSettings
    {
        public audPlaceholderSpeechGenerationSettings(string dialoguePath,
                                                      string characterVoiceConfig)
        {
            if (string.IsNullOrEmpty(dialoguePath))
            {
                throw new ArgumentNullException("dialoguePath");
            }
            if (string.IsNullOrEmpty(characterVoiceConfig))
            {
                throw new ArgumentNullException("characterVoiceConfig");
            }
            DialoguePath = dialoguePath;
            CharacterVoiceConfig = characterVoiceConfig;
        }

        public string DialoguePath { get; private set; }
        public string CharacterVoiceConfig { get; private set; }
    }
}