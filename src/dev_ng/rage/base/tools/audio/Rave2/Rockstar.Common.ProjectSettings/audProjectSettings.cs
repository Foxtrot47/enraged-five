﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Xml;

namespace rage
{
    /// <summary>
    ///   Parses RAGE Audio projectSettings.xml files
    /// </summary>
    public class audProjectSettings
    {
        public List<audBuildSet> BatchBuilds { get; private set; }
        private bool m_shouldEncrypt;
        private string m_assetProject;
        private string m_assetServer;

        private string m_buildAssetClient;
        private string m_buildAssetPassword;
        private string m_buildAssetUser;

        // build pipeline

        //build Modules
        private string m_buildPath;
        private string m_buildToolsPath;
        private string m_metadataCompilerPath;
        private string m_backupWorkingPath;
        private ArrayList m_builderList;
        private PlatformSetting m_currentPlatform;

        private audMetadataFile[] m_metadataSettings;
        private audMetadataType[] m_metatdataTypes;

        private List<audPedVoiceSettings> m_pedVoiceSettings;
        private ArrayList m_platformSettings;
        private ArrayList m_postBuilderList;
        private ArrayList m_preBuilderList;
        private string m_presetPath;
        private string m_projectName;
        private XmlNode[] m_ravePlugins;
        private string m_serverHost;
        private int m_serverPort;
        private string m_soundXml;
        private Dictionary<string, string> m_staticWaveSlotSettings;
        private bool m_use3ByteNameTableOffset = true;
        private string[] m_waveAttributes;
        private string m_waveInput;
        private List<string> m_waveRefsToSkip;
        private string m_waveSlotSettings;

        public audProjectSettings(string projectSettingsPath)
        {
            BatchBuilds = new List<audBuildSet>();
            if (!LoadSettings(projectSettingsPath))
            {
                throw new Exception("Couldn't load project settings from " + projectSettingsPath);
            }
        }

        public string DefaultModelPath { get; set; }

        public audPlaceholderSpeechGenerationSettings PlaceholderSpeechGenerationSettings { get; private set; }

        public string GlobalVariablesList { get; private set; }

        public string[] MetadataCompilerJitPaths { get; private set; }

        public audDataBuilderSettings AudioDataBuilderSettings { get; private set; }

        public XmlNode[] BuildModules { get; private set; }

        public string Path { get; private set; }

        public string FailBuildAtStage { get; private set; }

        public string ResolvePath(string path)
        {
            var newpath = path;

            if (newpath.ToUpper().Contains("{PLATFORM}"))
            {
                newpath = newpath.ToUpper().Replace("{PLATFORM}", m_currentPlatform.PlatformTag);
            }

            return newpath;
        }

        public bool Use3ByteNameTableOffset()
        {
            return m_use3ByteNameTableOffset;
        }

        public bool ShouldEncrypt()
        {
            return m_shouldEncrypt;
        }

        public audMetadataType[] GetMetadataTypes()
        {
            return m_metatdataTypes;
        }

        public audMetadataFile[] GetMetadataSettings()
        {
            return m_metadataSettings;
        }

        public ArrayList GetPlatformSettings()
        {
            return m_platformSettings;
        }

        public PlatformSetting GetCurrentPlatform()
        {
            return m_currentPlatform;
        }

        public string GetBuildToolsPath()
        {
            return m_buildToolsPath;
        }

        public string GetMetadataCompilerPath()
        {
            return m_metadataCompilerPath;
        }

        public string GetBackupWorkingPath()
        {
            return m_backupWorkingPath;
        }

        public void SetCurrentPlatform(PlatformSetting s)
        {
            m_currentPlatform = s;
        }

        public void SetCurrentPlatformByTag(string platformTag)
        {
            foreach (PlatformSetting s in m_platformSettings)
            {
                if (s.PlatformTag == platformTag)
                {
                    SetCurrentPlatform(s);
                    break;
                }
            }
        }

        public string GetProjectName()
        {
            return m_projectName;
        }

        public bool IsBigEndian()
        {
            return m_currentPlatform.IsBigEndian;
        }

        public string GetPlatformTag()
        {
            return m_currentPlatform.PlatformTag;
        }

        public string GetAssetServer()
        {
            return m_assetServer;
        }

        public string GetAssetProject()
        {
            return m_assetProject;
        }

        public string GetSoundXmlPath()
        {
            return m_soundXml;
        }

        public string GetWaveInputPath()
        {
            return m_waveInput;
        }

        public string GetBuildInfoPath()
        {
            return m_currentPlatform.BuildInfo;
        }

        public string GetBuildOutputPath()
        {
            return m_currentPlatform.BuildOutput;
        }

        public string GetBuildPath()
        {
            return m_buildPath;
        }

        public string GetServerHost()
        {
            return m_serverHost;
        }

        public int GetServerPort()
        {
            return m_serverPort;
        }

        public string GetBuildAssetUser()
        {
            return m_buildAssetUser;
        }

        public string GetBuildAssetPassword()
        {
            return m_buildAssetPassword;
        }

        public string GetBuildAssetClient()
        {
            return m_buildAssetClient;
        }

        public string GetWaveSlotSettings()
        {
            return m_waveSlotSettings;
        }

        public ArrayList GetPreBuilders()
        {
            return m_preBuilderList;
        }

        public ArrayList GetBuilders()
        {
            return m_builderList;
        }

        public ArrayList GetPostBuilders()
        {
            return m_postBuilderList;
        }

        public string[] GetWaveAttributes()
        {
            return m_waveAttributes;
        }

        public string GetPresetPath()
        {
            return m_presetPath;
        }

        public XmlNode[] GetRavePlugins()
        {
            return m_ravePlugins;
        }

        public bool LoadSettings(string projectSettingsPath)
        {
            Path = projectSettingsPath;
            var doc = new XmlDocument();
            doc.Load(projectSettingsPath);
            if (doc.DocumentElement.Name != "ProjectSettings" ||
                doc.DocumentElement.Attributes["name"] == null)
            {
                return false;
            }

            m_projectName = doc.DocumentElement.Attributes["name"].Value;

            foreach (XmlNode node in doc.DocumentElement.ChildNodes)
            {
                switch (node.Name)
                {
                    case "PlatformSettings":
                        ParsePlatformSettings(node);
                        break;
                    case "AssetPaths":
                        ParseAssetPaths(node);
                        break;
                    case "StaticWaveSlotSettings":
                        ParseWaveSlotSettings(node);
                        break;
                    case "BuildServer":
                        ParseBuildServer(node);
                        break;
                    case "BuildPipeline":
                        ParseBuildPipeline(node);
                        break;
                    case "WaveAttributes":
                        ParseWaveAttributes(node);
                        break;
                    case "MetadataSettings":
                        ParseMetadataSettings(node);
                        break;
                    case "RAVEPlugins":
                        ParseRavePlugins(node);
                        break;
                    case "BuildModules":
                        ParseBuildModules(node);
                        break;
                    case "WaveRefs":
                        ParseWaveRefs(node);
                        break;
                    case "Use3ByteNameTableOffset":
                        m_use3ByteNameTableOffset = ParseBoolValue(node);
                        break;
                    case "DefaultModelPath":
                        DefaultModelPath = node.InnerText;
                        break;
                    case "PedVoiceSettings":
                        ParsePedVoices(node);
                        break;
                    case "BatchBuilds":
                        ParseBatchBuilds(node);
                        break;
                    case "PlaceholderSpeechGenerationSettings":
                        ParsePlaceholderSpeechGeneration(node);
                        break;
                    case "GlobalVariablesList":
                        GlobalVariablesList = node.InnerText;
                        break;
                    case "AudioDataBuilderSettings":
                        AudioDataBuilderSettings = new audDataBuilderSettings(node);
                        break;
                    case "FailBuildAtStage":
                        FailBuildAtStage = node.InnerText;
                        break;
                }
            }

            DebugPrintSettings();

            return true;
        }

        private void ParseMetadataCompilerJitPaths(XmlNode node)
        {
            var jitPaths = (from XmlNode childNode in node.ChildNodes
                            where childNode.Name == "MetadataCompilerJitPath"
                            select childNode.InnerText).ToArray();

            if (jitPaths.Length > 0)
            {
                MetadataCompilerJitPaths = jitPaths;
            }
        }

        private void ParsePlaceholderSpeechGeneration(XmlNode node)
        {
            string dialoguePath = null, characterVoiceConfig = null;
            foreach (XmlNode childNode in node.ChildNodes)
            {
                switch (childNode.Name)
                {
                    case "DialoguePath":
                        {
                            dialoguePath = childNode.InnerText;
                            break;
                        }
                    case "CharacterVoiceConfig":
                        {
                            characterVoiceConfig = childNode.InnerText;
                            break;
                        }
                }
            }
            PlaceholderSpeechGenerationSettings = new audPlaceholderSpeechGenerationSettings(dialoguePath,
                                                                                             characterVoiceConfig);
        }

        private void ParseBatchBuilds(XmlNode node)
        {
            BatchBuilds = new List<audBuildSet>();
            foreach (XmlNode c in node.ChildNodes)
            {
                if (c.Name == "BuildSet")
                {
                    BatchBuilds.Add(new audBuildSet(c));
                }
            }
        }

        public List<audPedVoiceSettings> GetPedVoiceSettings()
        {
            return m_pedVoiceSettings ?? (m_pedVoiceSettings = new List<audPedVoiceSettings>());
        }

        private void ParsePedVoices(XmlNode node)
        {
            if (m_pedVoiceSettings == null)
            {
                m_pedVoiceSettings = new List<audPedVoiceSettings>();
            }

            foreach (XmlNode pedVoiceSetting in node.ChildNodes)
            {
                m_pedVoiceSettings.Add(new audPedVoiceSettings(pedVoiceSetting));
            }
        }

        public Dictionary<string, string> GetStaticWaveSlotPaths()
        {
            return m_staticWaveSlotSettings;
        }

        private void ParseWaveSlotSettings(XmlNode node)
        {
            m_staticWaveSlotSettings = new Dictionary<string, string>();

            foreach (XmlNode setting in node.ChildNodes)
            {
                m_staticWaveSlotSettings.Add(setting.Attributes["platform"].Value, setting.InnerText);
            }
        }

        private void ParseWaveRefs(XmlNode node)
        {
            m_waveRefsToSkip = new List<string>();
            foreach (XmlNode child in node.ChildNodes)
            {
                if (child.Name == "Skip")
                {
                    m_waveRefsToSkip.Add(child.InnerText);
                }
            }
        }

        private static PlatformSetting ParsePlatformSetting(XmlNode node)
        {
            var setting = new PlatformSetting {Name = node.Attributes["name"].Value};
            foreach (XmlNode child in node.ChildNodes)
            {
                switch (child.Name)
                {
                    case "Tag":
                        setting.PlatformTag = child.InnerText;
                        break;
                    case "BigEndian":
                        setting.IsBigEndian = ParseBoolValue(child);
                        break;
                    case "BuildOutput":
                        setting.BuildOutput = child.InnerText;
                        break;
                    case "LiveOutput":
                        setting.LiveOutput = child.InnerText;
                        break;
                    case "BuildInfo":
                        setting.BuildInfo = child.InnerText;
                        break;
                    case "WaveMemory":
                        setting.MaxWaveMemory = int.Parse(child.InnerText);
                        break;
                    case "BuildEnabled":
                        setting.BuildEnabled = ParseBoolValue(child);
                        break;
                    case "GetLatestEnabled":
                        setting.GetLatestEnabled = ParseBoolValue(child);
                        break;
                    case "AlignmentSamples":
                        setting.AlignmentSamples = Int32.Parse(child.InnerText);
                        break;
                    case "Encoder":
                        setting.Encoder = child.InnerText;
                        break;
                    case "StreamingPacketBytes":
                        setting.StreamingPacketBytes = Int32.Parse(child.InnerText);
                        break;
                }
            }
            return setting;
        }

        private void ParsePlatformSettings(XmlNode node)
        {
            m_platformSettings = new ArrayList();
            foreach (XmlNode n in node)
            {
                if (n.Name == "Platform")
                {
                    m_platformSettings.Add(ParsePlatformSetting(n));
                }
            }
            m_currentPlatform = (PlatformSetting) m_platformSettings[0];
        }

        private void ParseAssetPaths(XmlNode node)
        {
            foreach (XmlNode child in node.ChildNodes)
            {
                switch (child.Name)
                {
                    case "AssetServer":
                        m_assetServer = child.InnerText;
                        break;
                    case "AssetProject":
                        m_assetProject = child.InnerText;
                        break;
                    case "SoundXml":
                        m_soundXml = child.InnerText;
                        break;
                    case "WaveInput":
                        m_waveInput = child.InnerText;
                        break;
                    case "WaveSlotSettings":
                        m_waveSlotSettings = child.InnerText;
                        break;
                }
            }
        }

        private void ParseBuildServer(XmlNode node)
        {
            foreach (XmlNode child in node.ChildNodes)
            {
                switch (child.Name)
                {
                    case "Path":
                        m_buildPath = child.InnerText;
                        break;
                    case "BuildToolsPath":
                        m_buildToolsPath = child.InnerText;
                        break;
                    case "ServerHost":
                        m_serverHost = child.InnerText;
                        break;
                    case "ServerPort":
                        m_serverPort = Int32.Parse(child.InnerText);
                        break;
                    case "AssetUser":
                        m_buildAssetUser = child.InnerText;
                        break;
                    case "AssetPassword":
                        m_buildAssetPassword = child.InnerText;
                        break;
                    case "AssetClient":
                        m_buildAssetClient = child.InnerText;
                        break;
                    case "EnableEncryption":
                        m_shouldEncrypt = ParseBoolValue(child);
                        break;
                }
            }
        }

        private void ParseBuildPipeline(XmlNode node)
        {
            m_preBuilderList = new ArrayList();
            m_builderList = new ArrayList();
            m_postBuilderList = new ArrayList();

            foreach (XmlNode child in node.ChildNodes)
            {
                switch (child.Name)
                {
                    case "PreBuilder":
                        m_preBuilderList.Add(new audBuildComponent(child));
                        break;
                    case "Builder":
                        m_builderList.Add(new audBuildComponent(child));
                        break;
                    case "PostBuilder":
                        m_postBuilderList.Add(new audBuildComponent(child));
                        break;
                }
            }
        }

        private void ParseWaveAttributes(XmlNode node)
        {
            var waveAttrs = new List<string>();
            foreach (XmlNode child in node.ChildNodes)
            {
                switch (child.Name)
                {
                    case "WaveAttribute":
                        waveAttrs.Add(child.InnerText);
                        break;
                    case "PresetPath":
                        try
                        {
                            m_presetPath = child.InnerText;
                        }
                        catch (Exception e)
                        {
                            Debug.WriteLine(e.Message);
                        }
                        break;
                }
            }
            m_waveAttributes = waveAttrs.ToArray();
        }

        private void ParseMetadataSettings(XmlNode node)
        {
            var metadataFiles = new List<audMetadataFile>();
            var metadataTypes = new List<audMetadataType>();

            foreach (XmlNode child in node.ChildNodes)
            {
                switch (child.Name)
                {
                    case "MetadataFile":
                        metadataFiles.Add(new audMetadataFile(child));
                        break;
                    case "MetadataType":
                        metadataTypes.Add(new audMetadataType(child));
                        break;
                    case "MetadataCompilerPath":
                        m_metadataCompilerPath = child.InnerText;
                        break;
                    case "BackupWorkingPath":
                        m_backupWorkingPath = child.InnerText;
                        break;
                    case "MetadataCompilerJitPaths":
                        ParseMetadataCompilerJitPaths(child);
                        break;
                }
            }
            m_metadataSettings = metadataFiles.ToArray();
            m_metatdataTypes = metadataTypes.ToArray();
        }

        private void ParseBuildModules(XmlNode node)
        {
            BuildModules = node.ChildNodes.Cast<XmlNode>().Where(child => child.Name == "BuildModule").ToArray();
        }

        private void ParseRavePlugins(XmlNode node)
        {
            m_ravePlugins = node.ChildNodes.Cast<XmlNode>().Where(child => child.Name == "Plugin").ToArray();
        }

        public List<string> WaveRefsToSkip()
        {
            return m_waveRefsToSkip;
        }

        public static bool ParseBoolValue(XmlNode node)
        {
            if (string.Compare(node.InnerText, "yes", true) == 0)
            {
                return true;
            }
            if (string.Compare(node.InnerText, "no", true) == 0)
            {
                return false;
            }
            throw new FormatException(string.Format("Invalid value for boolean field {0} ({1}): expected yes or no",
                                                    node.Name,
                                                    node.InnerText));
        }

        #region Debug Methods

        [Conditional("DEBUG")]
        public void DebugPrintSettings()
        {
            DebugPrintPlatformSettings();
            DebugPrintAssetPaths();
            DebugPrintBuildServer();
        }

        [Conditional("DEBUG")]
        public void DebugPrintPlatformSettings()
        {
            foreach (PlatformSetting p in m_platformSettings)
            {
                Debug.WriteLine("Platform Settings:");
                Debug.WriteLine("\tTag: " + p.PlatformTag);
                Debug.WriteLine("\tBigEndian: " + p.IsBigEndian);
                Debug.WriteLine("\tBuildOutput: " + p.BuildOutput);
                Debug.WriteLine("\tBuildInfo: " + p.BuildInfo);
            }
        }

        [Conditional("DEBUG")]
        public void DebugPrintAssetPaths()
        {
            Debug.WriteLine("Asset Paths:");
            Debug.WriteLine("Asset Server: " + m_assetServer);
            Debug.WriteLine("Asset Project:" + m_assetProject);
            Debug.WriteLine("Sound Xml: " + m_soundXml);
            Debug.WriteLine("Wave Input: " + m_waveInput);
        }

        [Conditional("DEBUG")]
        public void DebugPrintBuildServer()
        {
            Debug.WriteLine("Build Server:");
            Debug.WriteLine("Host: " + m_serverHost);
            Debug.WriteLine("Port: " + m_serverPort);
            Debug.WriteLine("UserName: " + m_buildAssetUser);
            Debug.WriteLine("Password: " + m_buildAssetPassword);
        }

        #endregion
    }
}