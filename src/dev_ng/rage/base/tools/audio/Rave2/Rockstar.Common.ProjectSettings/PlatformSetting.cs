﻿namespace rage
{
    // platform settings
    public class PlatformSetting
    {
        private bool buildEnabled = true;
        private bool getLatestEnabled = true;

        public bool GetLatestEnabled
        {
            get { return getLatestEnabled; }
            set { getLatestEnabled = value; }
        }

        public bool BuildEnabled
        {
            get { return buildEnabled; }
            set { buildEnabled = value; }
        }

        public int MaxWaveMemory { get; set; }

        public string Name { get; set; }

        public bool IsBigEndian { get; set; }

        public string PlatformTag { get; set; }

        public string BuildOutput { get; set; }

        public string LiveOutput { get; set; }

        public string BuildInfo { get; set; }

        public int AlignmentSamples { set; get; }

        public string Encoder { set; get; }

        public int StreamingPacketBytes { set; get; }

        public override string ToString()
        {
            return Name;
        }
    }
}