﻿using System.Xml;

namespace rage
{
    public class audProjectEntry
    {
        private readonly XmlNode m_enableProgrammerSupport;
        private readonly XmlNode m_projectNode;
        private XmlNode m_assetManager;
        private XmlNode m_assetProject;
        private XmlNode m_assetServer;
        private XmlNode m_depotRoot;
        private XmlNode m_grabLatestPS3;
        private XmlNode m_grabLatestX360;
        private XmlNode m_grabLatestPC;
        private XmlNode m_projectSettings;
        private XmlNode m_softRoot;
        private XmlNode m_userName;
        private XmlNode m_waveEditor;

        public audProjectEntry(XmlNode node)
        {
            m_projectNode = node;

            foreach (XmlNode n in node.ChildNodes)
            {
                switch (n.Name)
                {
                    case "UserName":
                        m_userName = n;
                        break;
                    case "Password":
                        Password = n.InnerText;
                        break;
                    case "AssetManager":
                        m_assetManager = n;
                        break;
                    case "AssetServer":
                        m_assetServer = n;
                        break;
                    case "AssetProject":
                        m_assetProject = n;
                        break;
                    case "ProjectSettings":
                        m_projectSettings = n;
                        break;
                    case "DepotRoot":
                        m_depotRoot = n;
                        break;
                    case "SoftRoot":
                        m_softRoot = n;
                        break;
                    case "WaveEditor":
                        m_waveEditor = n;
                        break;
                    case "GrabLatestPS3":
                        m_grabLatestPS3 = n;
                        break;
                    case "GrabLatestX360":
                        m_grabLatestX360 = n;
                        break;
                    case "GrabLatestPC":
                        m_grabLatestPC = n;
                        break;
                    case "EnableProgrammerSupport":
                        m_enableProgrammerSupport = n;
                        break;
                }
            }
        }

        public string Password { get; set; }

        public string UserName
        {
            get
            {
                if (m_userName == null)
                {
                    return string.Empty;
                }
                return m_userName.InnerText;
            }
            set
            {
                if (m_userName == null)
                {
                    m_userName = m_projectNode.OwnerDocument.CreateElement("UserName");
                    m_projectNode.AppendChild(m_userName);
                }

                m_userName.InnerText = value;
            }
        }

        public string AssetManager
        {
            get
            {
                if (m_assetManager == null)
                {
                    m_assetManager = m_projectNode.OwnerDocument.CreateElement("AssetManager");
                    m_projectNode.AppendChild(m_assetManager);
                    //default perforce
                    m_assetManager.InnerText = "2";
                }

                return m_assetManager.InnerText;
            }
            set
            {
                if (m_assetManager == null)
                {
                    m_assetManager = m_projectNode.OwnerDocument.CreateElement("AssetManager");
                    m_projectNode.AppendChild(m_assetManager);
                }

                m_assetManager.InnerText = value;
            }
        }

        public string AssetServer
        {
            get
            {
                if (m_assetServer == null)
                {
                    return string.Empty;
                }
                return m_assetServer.InnerText;
            }
            set
            {
                if (m_assetServer == null)
                {
                    m_assetServer = m_projectNode.OwnerDocument.CreateElement("AssetServer");
                    m_projectNode.AppendChild(m_assetServer);
                }

                m_assetServer.InnerText = value;
            }
        }

        public string AssetProject
        {
            get
            {
                if (m_assetProject == null)
                {
                    return string.Empty;
                }
                return m_assetProject.InnerText;
            }
            set
            {
                if (m_assetProject == null)
                {
                    m_assetProject = m_projectNode.OwnerDocument.CreateElement("AssetProject");
                    m_projectNode.AppendChild(m_assetProject);
                }

                m_assetProject.InnerText = value;
            }
        }

        public string ProjectSettings
        {
            get
            {
                if (m_projectSettings == null)
                {
                    m_projectSettings = m_projectNode.OwnerDocument.CreateElement("ProjectSettings");
                    m_projectNode.AppendChild(m_projectSettings);
                    m_projectSettings.InnerText = "ProjectSettings.xml";
                }
                return m_projectSettings.InnerText;
            }
            set
            {
                if (m_projectSettings == null)
                {
                    m_projectSettings = m_projectNode.OwnerDocument.CreateElement("ProjectSettings");
                    m_projectNode.AppendChild(m_projectSettings);
                }

                m_projectSettings.InnerText = value;
            }
        }

        public string DepotRoot
        {
            get
            {
                if (m_depotRoot == null)
                {
                    return string.Empty;
                }
                return m_depotRoot.InnerText;
            }
            set
            {
                if (m_depotRoot == null)
                {
                    m_depotRoot = m_projectNode.OwnerDocument.CreateElement("DepotRoot");
                    m_projectNode.AppendChild(m_depotRoot);
                }

                m_depotRoot.InnerText = value;
            }
        }

        public string SoftRoot
        {
            get
            {
                if (m_softRoot == null)
                {
                    return string.Empty;
                }
                return m_softRoot.InnerText;
            }
            set
            {
                if (m_softRoot == null)
                {
                    m_softRoot = m_projectNode.OwnerDocument.CreateElement("SoftRoot");
                    m_projectNode.AppendChild(m_softRoot);
                }

                m_softRoot.InnerText = value;
            }
        }

        public string WaveEditor
        {
            get
            {
                if (m_waveEditor == null)
                {
                    m_waveEditor = m_projectNode.OwnerDocument.CreateElement("WaveEditor");
                    m_projectNode.AppendChild(m_waveEditor);
                    m_waveEditor.InnerText = @"C:\Program Files\Sony\Sound Forge 8.0\forge80.exe";
                }
                return m_waveEditor.InnerText;
            }
            set
            {
                if (m_waveEditor == null)
                {
                    m_waveEditor = m_projectNode.OwnerDocument.CreateElement("WaveEditor");
                    m_projectNode.AppendChild(m_waveEditor);
                }

                m_waveEditor.InnerText = value;
            }
        }

        public string Name
        {
            get
            {
                if (m_projectNode.Attributes["name"] != null)
                {
                    return m_projectNode.Attributes["name"].Value;
                }
                return string.Empty;
            }

            set { ((XmlElement) m_projectNode).SetAttribute("name", value); }
        }

        public bool GrabLatestPS3
        {
            get
            {
                if (m_grabLatestPS3 == null)
                {
                    m_grabLatestPS3 = m_projectNode.OwnerDocument.CreateElement("GrabLatestPS3");
                    m_projectNode.AppendChild(m_grabLatestPS3);
                    m_grabLatestPS3.InnerText = "false";
                }

                return bool.Parse(m_grabLatestPS3.InnerText);
            }
            set
            {
                if (m_grabLatestPS3 == null)
                {
                    m_grabLatestPS3 = m_projectNode.OwnerDocument.CreateElement("GrabLatestPS3");
                    m_projectNode.AppendChild(m_grabLatestPS3);
                }

                m_grabLatestPS3.InnerText = value.ToString();
            }
        }

        public bool GrabLatestX360
        {
            get
            {
                if (m_grabLatestX360 == null)
                {
                    m_grabLatestX360 = m_projectNode.OwnerDocument.CreateElement("GrabLatestX360");
                    m_projectNode.AppendChild(m_grabLatestX360);
                    m_grabLatestX360.InnerText = "false";
                }

                return bool.Parse(m_grabLatestX360.InnerText);
            }
            set
            {
                if (m_grabLatestX360 == null)
                {
                    m_grabLatestX360 = m_projectNode.OwnerDocument.CreateElement("GrabLatestX360");
                    m_projectNode.AppendChild(m_grabLatestX360);
                }

                m_grabLatestX360.InnerText = value.ToString();
            }
        }

        public bool GrabLatestPC
        {
            get
            {
                if (m_grabLatestPC == null)
                {
                    m_grabLatestPC = m_projectNode.OwnerDocument.CreateElement("GrabLatestPC");
                    m_projectNode.AppendChild(m_grabLatestPC);
                    m_grabLatestPC.InnerText = "false";
                }

                return bool.Parse(m_grabLatestPC.InnerText);
            }
            set
            {
                if (m_grabLatestPC == null)
                {
                    m_grabLatestPC = m_projectNode.OwnerDocument.CreateElement("GrabLatestPC");
                    m_projectNode.AppendChild(m_grabLatestPC);
                }

                m_grabLatestPC.InnerText = value.ToString();
            }
        }

        public bool IsProgrammerSupportEnabled
        {
            get
            {
                //  We don't bother to create this in the xml, since this is a "hidden" feature
                return m_enableProgrammerSupport != null && bool.Parse(m_enableProgrammerSupport.InnerText);
            }
        }

        public override string ToString()
        {
            return Name;
        }
    }
}