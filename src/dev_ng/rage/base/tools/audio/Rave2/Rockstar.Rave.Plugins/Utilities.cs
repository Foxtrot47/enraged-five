using System;
using System.Collections;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Xml;

namespace Rave.Plugins
{
    public class Utilities
    {
        #region ActionType enum

        public enum ActionType
        {
            AddBank,
            AddBankFolder,
            AddPack,
            AddPackFolder,
            AddTag,
            AddWave,
            AddWaveFolder,
            ChangeWave,
            DeleteBank,
            DeleteBankFolder,
            DeletePack,
            DeleteTag,
            DeleteWaveFolder,
            EditWave,
            ModifyTag,
            BulkAction,
            DeleteWave,
            Dummy
        }

        #endregion

        public static Form ActiveForm { get; set; }

        public static void LoadTypeIcons(string baseIconPath,
                                         Hashtable iconTable,
                                         ImageList iconList,
                                         TypeDefinitions typeDefs,
                                         bool useDefault)
        {
            // load default icon
            iconList.Images.Add(new Icon(baseIconPath + "default.ico"));

            // try to load custom sound type icons

            var curIndex = iconList.Images.Count;
            var baseIndex = curIndex - 1;

            if (typeDefs.ObjectTypes != null)
            {
                foreach (var td in typeDefs.ObjectTypes)
                {
                    int indexToUse;
                    try
                    {
                        if (File.Exists(baseIconPath + td.Name + ".ico"))
                        {
                            iconList.Images.Add(new Icon(baseIconPath + td.Name + ".ico"));
                            indexToUse = curIndex++;
                        }
                        else
                        {
                            indexToUse = baseIndex;
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine(ex.Message);
                        indexToUse = baseIndex;
                    }

                    if (useDefault || indexToUse != baseIndex)
                    {
                        iconTable.Add(td.Name, indexToUse);
                    }
                }
            }
        }

        public static void CreateRequiredDirectories(string path)
        {
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
        }

        public static string FormatGameString(string str)
        {
            return str.Replace(" ", "_");
        }

        public static bool StringIsValidObjectName(string str)
        {
            return str.All(c => (char.IsLetterOrDigit(c) || c == '_' || c == '-'));
        }

        public static bool StringIsValidWaveOrMidiName(string str)
        {
            var tmp = str.ToUpper();
            if (!tmp.EndsWith(".WAV") &&
                !tmp.EndsWith(".MID"))
            {
                return false;
            }
            return tmp.All(c => (char.IsLetterOrDigit(c) || c == '_' || c == '-' || c == '.'));
        }

        public static bool IsWaveReference(XmlNode node)
        {
            if (node.Name == "WaveRef")
            {
                return true;
            }

            if (node.ChildNodes.Count == 2)
            {
                var names = node.ChildNodes.Cast<XmlNode>().Select(x => x.Name).ToList();
                return names.Contains("WaveName") && names.Contains("BankName");
            }
            return false;
        }

        public static bool IsPopulatedWaveReference(XmlNode node)
        {
            return IsWaveReference(node) && node.ChildNodes.Count == 2;
        }
    }
}