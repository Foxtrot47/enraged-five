using System;
using System.Collections.Generic;
using System.Text;

namespace Rave.Plugins
{
    public interface IRAVEReportPlugin:IRAVEPlugin
    {
       void DisplayResults(IWaveBrowser waveBrowser, Dictionary<string,ISoundBrowser> browsers);
    }
}
