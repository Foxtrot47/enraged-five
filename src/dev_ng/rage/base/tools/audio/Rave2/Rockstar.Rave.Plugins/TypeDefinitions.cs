using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Schema;

namespace Rave.Plugins
{
    public abstract class FieldDefinition
    {
        protected FieldDefinition(XmlNode definition, TypeDefinition typeDefinition)
        {
            Name = GetAttributeValue(definition, "name");
            TypeName = GetAttributeValue(definition, "type");
            Units = GetAttributeValue(definition, "units");
            DefaultValue = GetAttributeValue(definition, "default");
            Description = GetAttributeValue(definition, "description");
            DisplayGroup = GetAttributeValue(definition, "displayGroup");
            Visible = GetBooleanAttributeValue(definition, "visible", true);
            Ignore = GetBooleanAttributeValue(definition, "ignore", false);
            AllowOverrideControl = GetBooleanAttributeValue(definition, "allowOverrideControl", false);
            AllowedType = GetAttributeValue(definition, "allowedType");
            Node = definition;
            TypeDefinition = typeDefinition;
        }

        public string Name { get; private set; }
        public string TypeName { get; private set; }
        public string Units { get; private set; }
        public string DefaultValue { get; private set; }
        public string Description { get; private set; }
        public string DisplayGroup { get; private set; }
        public string AllowedType { get; private set; }

        public bool Visible { get; private set; }
        public bool Ignore { get; private set; }
        public bool AllowOverrideControl { get; private set; }

        public XmlNode Node { get; private set; }
        public TypeDefinition TypeDefinition { get; set; }

        public override string ToString()
        {
            return Name;
        }

        protected string GetAttributeValue(XmlNode node, string valName)
        {
            if (node.Attributes[valName] != null)
            {
                return node.Attributes[valName].Value.Trim();
            }
            return null;
        }

        protected bool GetBooleanAttributeValue(XmlNode node, string valName, bool defaultVal)
        {
            if (node.Attributes[valName] != null)
            {
                return node.Attributes[valName].Value == "yes";
            }
            return defaultVal;
        }

        protected float GetFloatAttributeValue(XmlNode node, string valName)
        {
            if (node.Attributes[valName] != null)
            {
                return float.Parse(node.Attributes[valName].Value);
            }
            return 0;
        }

        protected int GetIntegerAttributeValue(XmlNode node, string valName)
        {
            if (node.Attributes[valName] != null)
            {
                return int.Parse(node.Attributes[valName].Value);
            }
            return 0;
        }

        public static FieldDefinition[] ParseFieldDefinitions(XmlNode parent, TypeDefinition typeDefinition)
        {
            var fieldList = new ArrayList();
            foreach (XmlNode fieldNode in parent.ChildNodes)
            {
                if (fieldNode.GetType() !=
                    typeof (XmlComment))
                {
                    switch (fieldNode.Name)
                    {
                        case "CompositeField":
                            fieldList.Add(new CompositeFieldDefinition(fieldNode, typeDefinition));
                            break;
                        case "AllocatedSpaceField":
                        case "Field":
                            fieldList.Add(new SimpleFieldDefinition(fieldNode, typeDefinition));
                            break;
                        default:
                            throw new Exception("Unknown field type in SoundDefinitions.xml");
                    }
                }
            }

            return (FieldDefinition[]) fieldList.ToArray(typeof (FieldDefinition));
        }

        public virtual bool DoesFieldContainSoundRef()
        {
            return (Units == "ObjectRef");
        }

        public abstract XmlNode GenerateDefaultXmlNode(XmlNode parentNode);
    }

    public class EnumValueEntry
    {
        public EnumValueEntry(XmlNode node)
        {
            ValueName = node.InnerText.Trim();
            DisplayName = node.Attributes["display"] != null ? node.Attributes["display"].Value.Trim() : ValueName;
            if (node.Attributes["description"] != null)
            {
                Description = node.Attributes["description"].Value.Trim();
            }
        }

        public string DisplayName { get; private set; }
        public string ValueName { get; private set; }
        public string Description { get; private set; }

        public override string ToString()
        {
            return DisplayName;
        }
    }

    public class EnumDefinition
    {
        public EnumDefinition(XmlNode node)
        {
            Name = node.Attributes["name"].Value.Trim();
            var al = new ArrayList();
            foreach (XmlNode child in node.ChildNodes)
            {
                if (child.Name == "Value")
                {
                    al.Add(new EnumValueEntry(child));
                }
            }
            Values = (EnumValueEntry[]) al.ToArray(typeof (EnumValueEntry));
        }

        public string Name { get; private set; }
        public EnumValueEntry[] Values { get; private set; }

        public EnumValueEntry FindEnumFromValue(string valueName)
        {
            foreach (var e in Values)
            {
                if (e.ValueName == valueName)
                {
                    return e;
                }
            }
            return null;
        }
    }

    public class CompositeFieldDefinition : FieldDefinition
    {
        public CompositeFieldDefinition(XmlNode definition, TypeDefinition typeDefinition)
            : base(definition, typeDefinition)
        {
            MaxOccurs = GetIntegerAttributeValue(definition, "maxOccurs");
            if (MaxOccurs == 0)
            {
                // default to 1...(0 isnt valid anyway)
                MaxOccurs = 1;
            }

            Fields = ParseFieldDefinitions(definition, TypeDefinition);
        }

        public FieldDefinition[] Fields { get; private set; }
        public int MaxOccurs { get; private set; }

        public override XmlNode GenerateDefaultXmlNode(XmlNode parentNode)
        {
            return GenerateDefaultXmlNode(parentNode, false);
        }

        public XmlNode GenerateDefaultXmlNode(XmlNode parentNode, bool force)
        {
            // only generate default xml if maxoccurs is one, ie its an old-school composite
            // field and not a new fangled repeating composite field.  makes it 
            // more intuitive for the user
            if (MaxOccurs == 1 || force)
            {
                XmlNode node = parentNode.OwnerDocument.CreateElement(Name);
                parentNode.AppendChild(node);

                foreach (var f in Fields)
                {
                    f.GenerateDefaultXmlNode(node);
                }

                return node;
            }
            return null;
        }

        public override bool DoesFieldContainSoundRef()
        {
            return Fields.Any(f => f.DoesFieldContainSoundRef());
        }

        public FieldDefinition FindFieldDefinition(string fieldName)
        {
            return Fields.FirstOrDefault(f => f.Name == fieldName);
        }
    }

    public class SimpleFieldDefinition : FieldDefinition
    {
        public SimpleFieldDefinition(XmlNode definition, TypeDefinition typeDefinition)
            : base(definition, typeDefinition)
        {
            Min = GetFloatAttributeValue(definition, "min");
            Max = GetFloatAttributeValue(definition, "max");
            EnumName = GetAttributeValue(definition, "enum");
            Length = GetIntegerAttributeValue(definition, "length");
        }

        public float Min { get; private set; }
        public float Max { get; private set; }
        public string EnumName { get; private set; }
        public int Length { get; private set; }

        public override XmlNode GenerateDefaultXmlNode(XmlNode parentNode)
        {
            // if there is no default value then don't create the node
            // (makes sense for flags to leave them unspecified by default)
            if (DefaultValue != null)
            {
                XmlNode node = parentNode.OwnerDocument.CreateElement(Name);
                node.InnerText = DefaultValue;
                parentNode.AppendChild(node);
                return node;
            }
            return null;
        }
    }

    public class TypeDefinition
    {
        private List<TypeDefinition> m_descendantTypes;

        public TypeDefinition(XmlNode definition, TypeDefinitions typeDefinitions)
        {
            TypeDefinitions = typeDefinitions;
            Name = definition.Attributes["name"].Value;
            InheritsFrom = (definition.Attributes["inheritsFrom"] != null
                                ? definition.Attributes["inheritsFrom"].Value
                                : null);
            Group = (definition.Attributes["group"] != null ? definition.Attributes["group"].Value : null);
            Description = (definition.Attributes["description"] != null
                               ? definition.Attributes["description"].Value
                               : null);
            IsAbstract = (definition.Attributes["isAbstract"] != null &&
                          definition.Attributes["isAbstract"].Value == "yes");
            Fields = FieldDefinition.ParseFieldDefinitions(definition, this);

            foreach (var f in Fields)
            {
                if (f.DoesFieldContainSoundRef())
                {
                    ContainsSoundRef = true;
                }
                else if (f.Units == "WaveRef")
                {
                    ContainsWaveRef = true;
                }
                else if (f.Units == "BankRef")
                {
                    ContainsBankRef = true;
                }
            }
        }

        public TypeDefinitions TypeDefinitions { get; private set; }
        public FieldDefinition[] Fields { get; set; }
        public string Name { get; private set; }
        public string InheritsFrom { get; private set; }
        public string Group { get; private set; }
        public string Description { get; private set; }
        public bool IsAbstract { get; private set; }
        public bool ContainsWaveRef { get; private set; }
        public bool ContainsSoundRef { get; private set; }
        public bool ContainsBankRef { get; private set; }

        public override string ToString()
        {
            return Name;
        }

        public FieldDefinition FindFieldDefinition(string fieldName)
        {
            return Fields.FirstOrDefault(f => f.Name == fieldName);
        }

        public XmlNode GenerateDefaultXmlNode(XmlDocument parentDoc, string soundName)
        {
            XmlNode soundNode = parentDoc.CreateElement(Name);
            var nameAttr = parentDoc.CreateAttribute("name");
            nameAttr.Value = soundName;
            soundNode.Attributes.Append(nameAttr);

            foreach (var f in Fields)
            {
                f.GenerateDefaultXmlNode(soundNode);
            }

            return soundNode;
        }

        public FieldDefinition[] FindSoundRefFields()
        {
            var al = new ArrayList();

            foreach (var f in Fields)
            {
                if (f.DoesFieldContainSoundRef())
                {
                    al.Add(f);
                }
            }

            return (FieldDefinition[]) al.ToArray(typeof (FieldDefinition));
        }

        public IEnumerable<TypeDefinition> GetDescendantTypes()
        {
            if (m_descendantTypes == null)
            {
                m_descendantTypes = new List<TypeDefinition> {this};
                foreach (var type in TypeDefinitions.ObjectTypes)
                {
                    var currentType = type;
                    while (!string.IsNullOrEmpty(currentType.InheritsFrom))
                    {
                        if (currentType.InheritsFrom == Name)
                        {
                            m_descendantTypes.Add(currentType);
                            break;
                        }
                        currentType = TypeDefinitions.FindTypeDefinition(currentType.InheritsFrom);
                    }
                }
            }
            return m_descendantTypes;
        }

        public List<string> GetDisplayGroups()
        {
            var set = new HashSet<string>();
            foreach (var field in Fields)
            {
                if (!string.IsNullOrEmpty(field.DisplayGroup))
                {
                    set.Add(field.DisplayGroup);
                }
            }
            var groups = set.ToList();
            groups.Sort();
            return groups;
        }
    }

    /// <summary>
    ///   Summary description for SoundTypes.
    /// </summary>
    public class TypeDefinitions
    {
        private static readonly StringBuilder ms_errorMessage = new StringBuilder();
        private readonly string m_schemaPath;

        static TypeDefinitions()
        {
            AllTypeDefinitions = new Dictionary<string, TypeDefinitions>(StringComparer.OrdinalIgnoreCase);
        }

        public TypeDefinitions(string[] filePaths, string schemaPath)
        {
            m_schemaPath = (string.IsNullOrEmpty(schemaPath)
                                ? null
                                : AssetManager.GetAssetManager().GetWorkingPath(schemaPath));
            Load(filePaths);
        }

        public static Dictionary<string, TypeDefinitions> AllTypeDefinitions { get; private set; }

        public TypeDefinition[] ObjectTypes { get; private set; }
        public EnumDefinition[] Enums { get; private set; }

        public string SchemaPath
        {
            get { return m_schemaPath; }
        }

        public string[] FilePaths { get; private set; }

        public void Load(string[] filePaths)
        {
            ms_errorMessage.Remove(0, ms_errorMessage.Length);

            FilePaths = filePaths;

            var objectList = new ArrayList();
            var enumList = new ArrayList();

            var schemaSet = new XmlSchemaSet();
            XmlTextReader schemaReader = null;
            var settings = new XmlReaderSettings();

            if (!string.IsNullOrEmpty(SchemaPath))
            {
                GetLatest(SchemaPath);
                
                schemaReader = new XmlTextReader(SchemaPath);
                schemaSet.Add(null, schemaReader);
                settings.ValidationType = ValidationType.Schema;
                settings.Schemas = schemaSet;
            }

            foreach (var filePath in filePaths)
            {
                GetLatest(filePath);

                var document = new XmlDocument();
                try
                {
                    settings.ValidationEventHandler += settings_ValidationEventHandler;
                    var tr = new XmlTextReader(filePath);
                    var reader = XmlReader.Create(tr, settings);
                    document.Load(reader);
                    reader.Close();                   
                    tr.Close();
                    settings.ValidationEventHandler -= settings_ValidationEventHandler;
                }
                catch (Exception e)
                {
                    throw new ApplicationException(string.Format("Exception in: {0}", filePath), e);
                }

                foreach (XmlNode node in document.DocumentElement.ChildNodes)
                {
                    if (node.GetType() !=
                        typeof (XmlComment))
                    {
                        switch (node.Name)
                        {
                            case "Enum":
                                enumList.Add(new EnumDefinition(node));
                                break;
                            case "TypeDefinition":
                                objectList.Add(new TypeDefinition(node, this));
                                break;
                        }
                    }
                }
            }

            if (ms_errorMessage.Length > 0)
            {
                ErrorManager.HandleError(ms_errorMessage.ToString());
            }

            ObjectTypes = (TypeDefinition[]) objectList.ToArray(typeof (TypeDefinition));
            Enums = (EnumDefinition[]) enumList.ToArray(typeof (EnumDefinition));

            // add base sound fields to all object types
            foreach (var sd in ObjectTypes)
            {
                if (sd.InheritsFrom != null)
                {
                    var baseSound = FindTypeDefinition(sd.InheritsFrom);
                    if (baseSound == null)
                    {
                        throw new Exception("Object inherits from unknown type");
                    }
                    var fieldList = new ArrayList();
                    fieldList.AddRange(baseSound.Fields);
                    fieldList.AddRange(sd.Fields);
                    sd.Fields = (FieldDefinition[]) fieldList.ToArray(typeof (FieldDefinition));
                }
            }

            if (schemaReader != null)
            {
                schemaReader.Close();
            }
        }

        private static void GetLatest(string path)
        {
            try
            {
                AssetManager.GetAssetManager().GetLatest(path, false);
            }
            catch (Exception)
            {
                try
                {
                    AssetManager.GetAssetManager().GetLatest(path, true);
                }
                catch (Exception e)
                {
                    ErrorManager.HandleError(e);
                }
            }
        }

        private static void settings_ValidationEventHandler(object sender, ValidationEventArgs e)
        {
            var xmlreader = sender as XmlReader;

            ms_errorMessage.AppendLine("Error in TypeDefinition ");
            ms_errorMessage.AppendLine(xmlreader.BaseURI);
            ms_errorMessage.AppendLine("NodeType  = " + xmlreader.NodeType);
            ms_errorMessage.AppendLine("NodeName  = " + xmlreader.Name);
            ms_errorMessage.AppendLine("NodeValue  = " + xmlreader.Value);

            if (xmlreader.NodeType.ToString() != "Attribute" &&
                xmlreader.HasAttributes)
            {
                ms_errorMessage.AppendLine("Attributes:");
                while (xmlreader.MoveToNextAttribute())
                {
                    ms_errorMessage.AppendLine("   AttributeName = " + xmlreader.Name);
                    ms_errorMessage.AppendLine("   AttributeValue = " + xmlreader.Value);
                }
            }

            ms_errorMessage.AppendLine("-----------------------");
        }

        public TypeDefinition FindTypeDefinition(string typeName)
        {
            return ObjectTypes.FirstOrDefault(t => t.Name == typeName);
        }

        public EnumDefinition FindEnumDefinition(string enumName)
        {
            return Enums.FirstOrDefault(ed => ed.Name == enumName);
        }

        public List<TypeDefinition> GetAllowedTypes(FieldDefinition fieldDef)
        {
            if (!string.IsNullOrEmpty(fieldDef.AllowedType))
            {
                var typeDefinitions = fieldDef.TypeDefinition.TypeDefinitions;
                var allowedType = typeDefinitions.FindTypeDefinition(fieldDef.AllowedType);
                if (allowedType != null)
                {
                    var concreteDescendantTypes =
                        (from descendantType in allowedType.GetDescendantTypes()
                         where descendantType.IsAbstract == false
                         select descendantType).ToList();

                    if (concreteDescendantTypes.Count > 0)
                    {
                        concreteDescendantTypes.Sort((x, y) => string.Compare(x.Name, y.Name));
                    }
                    return concreteDescendantTypes;
                }
            }
            return new List<TypeDefinition>();
        }
    }

    public class FieldDefinitionInstance
    {
        private readonly string m_defaultValue;

        public FieldDefinitionInstance(FieldDefinition fieldDefinition, string defaultValue)
        {
            FieldDef = fieldDefinition;
            m_defaultValue = defaultValue;
        }

        public FieldDefinition FieldDef { get; private set; }

        public string Name
        {
            get { return FieldDef.Name; }
        }

        public string TypeName
        {
            get { return FieldDef.TypeName; }
        }

        public string Units
        {
            get { return FieldDef.Units; }
        }

        public string DefaultValue
        {
            get { return string.IsNullOrEmpty(m_defaultValue) ? FieldDef.DefaultValue : m_defaultValue; }
        }

        public string Description
        {
            get { return FieldDef.Description; }
        }

        public string DisplayGroup
        {
            get { return FieldDef.DisplayGroup; }
        }

        public string AllowedType
        {
            get { return FieldDef.AllowedType; }
        }

        public bool Visible
        {
            get { return FieldDef.Visible; }
        }

        public bool Ignore
        {
            get { return FieldDef.Ignore; }
        }

        public bool AllowOverrideControl
        {
            get { return FieldDef.AllowOverrideControl; }
        }

        public XmlNode Node
        {
            get { return FieldDef.Node; }
        }

        public TypeDefinition TypeDefinition
        {
            get { return FieldDef.TypeDefinition; }
            set { FieldDef.TypeDefinition = value; }
        }

        public override string ToString()
        {
            return Name;
        }

        public SimpleFieldDefinitionInstance AsSimpleField()
        {
            var simpleFieldDef = FieldDef as SimpleFieldDefinition;
            if (simpleFieldDef != null)
            {
                return new SimpleFieldDefinitionInstance(simpleFieldDef,
                                                         string.IsNullOrEmpty(m_defaultValue) ? null : m_defaultValue);
            }
            return null;
        }

        public CompositeFieldDefinitionInstance AsCompositeField()
        {
            var compositeFieldDef = FieldDef as CompositeFieldDefinition;
            if (compositeFieldDef != null)
            {
                return new CompositeFieldDefinitionInstance(compositeFieldDef,
                                                            string.IsNullOrEmpty(m_defaultValue) ? null : m_defaultValue);
            }
            return null;
        }
    }

    public class SimpleFieldDefinitionInstance : FieldDefinitionInstance
    {
        public SimpleFieldDefinitionInstance(SimpleFieldDefinition fieldDefinition, string defaultValue)
            : base(fieldDefinition, defaultValue)
        {
            SimpleFieldDef = fieldDefinition;
        }

        public SimpleFieldDefinition SimpleFieldDef { get; private set; }

        public float Min
        {
            get { return SimpleFieldDef.Min; }
        }

        public float Max
        {
            get { return SimpleFieldDef.Max; }
        }

        public string EnumName
        {
            get { return SimpleFieldDef.EnumName; }
        }

        public int Length
        {
            get { return SimpleFieldDef.Length; }
        }
    }

    public class CompositeFieldDefinitionInstance : FieldDefinitionInstance
    {
        public CompositeFieldDefinitionInstance(CompositeFieldDefinition fieldDefinition, string defaultValue)
            : base(fieldDefinition, defaultValue)
        {
            CompositeFieldDef = fieldDefinition;
        }

        public CompositeFieldDefinition CompositeFieldDef { get; private set; }

        public FieldDefinition[] Fields
        {
            get { return CompositeFieldDef.Fields; }
        }

        public int MaxOccurs
        {
            get { return CompositeFieldDef.MaxOccurs; }
        }
    }
}