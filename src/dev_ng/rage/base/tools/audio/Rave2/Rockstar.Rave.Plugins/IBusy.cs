using System;
using System.Collections.Generic;
using System.Text;

namespace Rave.Plugins
{
    public interface IBusy
    {
        void FinishedWorking();
        void SetProgress(int percentage);
        void SetUnknownDuration(bool unknownDuration);
        void SetStatusText(string text);
        void Show();
        void Close();
        void Hide();
    }
}
