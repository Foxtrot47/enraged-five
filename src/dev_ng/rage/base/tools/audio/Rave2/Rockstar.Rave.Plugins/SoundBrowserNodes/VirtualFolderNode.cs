namespace Rave.Plugins.SoundBrowser.Nodes
{
    /// <summary>
    ///   Summary description for VirtualFolderNode.
    /// </summary>
    public class VirtualFolderNode : BaseNode
    {
        private const int BASE_IMAGE_INDEX = 3;

        public VirtualFolderNode(string displayName, string episode, string type) : base(displayName, episode, type)
        {
            SelectedImageIndex = ImageIndex = BASE_IMAGE_INDEX;
        }

        public override int GetBaseIconIndex()
        {
            return BASE_IMAGE_INDEX;
        }

        public override string GetTypeDescription()
        {
            return "Virtual Folder";
        }
    }
}