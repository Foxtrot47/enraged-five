using System;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace Rave.Plugins
{
    public static class ErrorManager
    {
        private static bool ms_isCommandLineMode;
        private static StreamWriter ms_outputLog;

        public static int NumErrors { get; private set; }

        public static void Init(bool isCommandLineMode, string filePath)
        {
            ms_isCommandLineMode = isCommandLineMode;
            if (!string.IsNullOrEmpty(filePath))
            {
                var file = new FileStream(filePath, FileMode.Create, FileAccess.Write);
                ms_outputLog = new StreamWriter(file);
            }
        }

        public static void ShutDown()
        {
            if (NumErrors == 0)
            {
                ms_outputLog.WriteLine("No Errors");
            }
            if (ms_outputLog != null)
            {
                ms_outputLog.Dispose();
                ms_outputLog = null;
            }
        }

        public static void HandleError(string message)
        {
            ShowError(message, string.Empty);
        }

        public static void HandleError(string context, string message)
        {
            ShowError(string.Format("Context: {0}{1}Message: {2}", context, Environment.NewLine, message), string.Empty);
        }

        public static void HandleError(Exception e)
        {
            ShowError(e.Message, e.StackTrace);
        }

        public static void HandleException(string context, Exception ex)
        {
            var messageBuilder = new StringBuilder();
            messageBuilder.Append(string.Format("Context: {0}", context));
            messageBuilder.Append(Environment.NewLine);
            messageBuilder.Append(Environment.NewLine);
            messageBuilder.Append(string.Format("Message: {0}", ex.Message));

            var stackTraceBuilder = new StringBuilder(ex.StackTrace);

            var tempEx = ex.InnerException;
            while (tempEx != null)
            {
                if (!string.IsNullOrEmpty(tempEx.Message))
                {
                    messageBuilder.Append(Environment.NewLine);
                    messageBuilder.Append("------------------------------------------------------");
                    messageBuilder.Append(Environment.NewLine);
                    messageBuilder.Append(string.Format("Message: {0}", tempEx.Message));
                }

                if (!string.IsNullOrEmpty(tempEx.StackTrace))
                {
                    stackTraceBuilder.Append(Environment.NewLine);
                    stackTraceBuilder.Append("------------------------------------------------------");
                    stackTraceBuilder.Append(Environment.NewLine);
                    stackTraceBuilder.Append(tempEx.StackTrace);
                }
                tempEx = tempEx.InnerException;
            }

            ShowError(messageBuilder.ToString(), stackTraceBuilder.ToString());
        }

        private static void ShowError(string message, string stackTrace)
        {
            NumErrors++;
            if (ms_isCommandLineMode && ms_outputLog != null)
            {
                var sb = new StringBuilder();
                sb.Append("Error: ");
                sb.Append(message);
                sb.Append(" ");
                sb.Append(stackTrace);
                ms_outputLog.WriteLine(sb.ToString());
            }
            else
            {
                using (var errorForm = new frmError(message, stackTrace))
                {
                    errorForm.ShowDialog();
                }
            }
        }

        public static DialogResult HandleQuestion(string message, MessageBoxButtons buttons, DialogResult defaultResult)
        {
            if (ms_isCommandLineMode && ms_outputLog != null)
            {
                var sb = new StringBuilder();
                sb.Append("Question: ");
                sb.Append(message);
                sb.Append(" Response: ");
                sb.Append(defaultResult.ToString());
                ms_outputLog.WriteLine(sb.ToString());
                return defaultResult;
            }
            return MessageBox.Show(Utilities.ActiveForm, message, "Information", buttons, MessageBoxIcon.Question);
        }

        public static void HandleInfo(string message)
        {
            Debug.WriteLine(ms_isCommandLineMode);
            if (ms_isCommandLineMode && ms_outputLog != null)
            {
                var sb = new StringBuilder();
                sb.Append("Information: ");
                sb.Append(message);
                ms_outputLog.WriteLine(sb.ToString());
            }
            else
            {
                MessageBox.Show(Utilities.ActiveForm,
                                message,
                                "Information",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Information);
            }
        }
    }
}