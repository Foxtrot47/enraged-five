using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using Rave.Plugins.SoundHierarchyNodes;

namespace Rave.Plugins
{
    public interface IPropertiesEditor
    {
        void SetTab(int i);
        int GetNumberofTabs();
    }
}
