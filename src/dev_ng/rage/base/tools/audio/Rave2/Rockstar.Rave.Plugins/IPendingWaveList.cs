using System.Collections.Generic;
using System.Windows.Forms;

namespace Rave.Plugins
{
    public interface IPendingWaveList
    {
        void RecordAddPack(string p);

        void RecordAddBankFolder(string p);

        void RecordAddBank(string p);

        void RecordAddWaveFolder(string p);

        void RecordAddWave(string p);

        void RecordAddPreset(string p, string value);

        void RecordAddTag(string p, string value);

        void RecordDeletePack(string p);

        void RecordDeleteBankFolder(string p);

        void RecordDeleteBank(string p);

        void RecordDeleteWaveFolder(string p);

        void RecordDeleteWave(string p);

        void RecordDeletePreset(string p);

        void RecordDeleteTag(string p);

        void RecordModifyWave(string p);

        void RecordModifyTag(string p, string value);

        bool GetLatestBuiltWaveListFromAssetManager();

        bool SerialisePendingWaveList();

        void LoadPendingWaveList();

        bool UndoLockPendingWaveList();

        void ClearLocalStore();

        void ShowPendingChanges(TreeView treeView, IActionLog log, string platformTag);

        void ShowBuiltWaves(TreeView treeView, IActionLog log, string platformTag);

        bool IsPendingWaveListCheckedOut();

        bool GetLatestPendingWaveListFromAssetManager();

        void LoadBuiltWaveList();

        bool LockPendingWaveList();

        bool ContainsVoiceTags();

        bool ContainsVoiceTags(string packName);

        IEnumerable<string> GetPackNames();
    }
}