using System;

namespace Rave.Plugins.WaveBrowser.Nodes
{
    /// <summary>
    ///   Summary description for BankNode.
    /// </summary>
    public class BankNode : WaveContainerNode
    {
        private const int BASE_IMAGE_INDEX = 8;

        public BankNode(IActionLog actionLog, string strDisplayName, IWaveBrowser waveBrowser)
            : base(actionLog, strDisplayName, waveBrowser)
        {
        }

        public override string GetObjectPath()
        {
            return ((PackNode) Parent).GetObjectPath() + "\\" + GetObjectName();
        }

        public override int GetBaseIconIndex(IWaveBrowser waveBrowser)
        {
            return BASE_IMAGE_INDEX;
        }

        public override bool TryToLock()
        {
            UpdateLockedState();
            if (!IsLocked() &&
                !AllowAutoLock)
            {
                if (LockForEditing())
                {
                    return true;
                }
                return false;
            }
            return true;
        }

        public override bool LockForEditing()
        {
            if (!m_lockedLocally &&
                AssetManager.LockWavePath(GetObjectPath()))
            {
                m_lockedLocally = true;
                UpdateDisplay();
            }

            return m_lockedLocally;
        }

        public override void Unlock()
        {
            if (m_lockedLocally)
            {
                try
                {
                    AssetManager.UnlockWavePath(GetObjectPath());
                    m_lockedLocally = false;
                    UpdateDisplay();
                }
                catch (Exception e)
                {
                    ErrorManager.HandleError(e);
                }
            }
        }

        public override string GetBuiltPath()
        {
            if (this is WaveFolderNode)
            {
                return (Parent as EditorTreeNode).GetBuiltPath();
            }
            return (Parent as EditorTreeNode).GetBuiltPath() + "\\" + GetObjectName();
        }

        public override string GetTypeDescription()
        {
            return "Bank";
        }

        public string GetBankPath()
        {
            var pn = FindParentPackNode(this);
            return Utilities.FormatGameString(pn.GetObjectName() + "\\" + GetObjectName());
        }
    }
}