using System;
using System.Windows.Forms;
using System.Drawing;
using System.Collections;
using Rave.Plugins.WaveBrowser.Nodes;

namespace Rave.Plugins
{
    public class NodeSorter : IComparer
    {
        public int Compare(object x, object y)
        {
            TreeNode NodeX = x as TreeNode;
            TreeNode NodeY = y as TreeNode;

            bool isTagX = x is TagNode || x is PresetNode;
            bool isTagY = y is TagNode || y is PresetNode;
            if (isTagX && isTagY)
            {
                return string.Compare(NodeX.Text, NodeY.Text);
            }
            else if (isTagX)
            {
                return -1;
            }
            else if (isTagY)
            {
               return 1;
            }

            return string.Compare(NodeX.Text, NodeY.Text);
        }
    }

	/// TreeView that knows how to handle the scrolling beyond its boundaries.
	public class ScrollableTreeView : TreeView
	{
        public ScrollableTreeView()
        {
           TreeViewNodeSorter = new NodeSorter();
        }

        public ScrollableTreeView(bool Sorted)
        {
            if (Sorted)
            {
                TreeViewNodeSorter = new NodeSorter();
            }
            else
            {
                TreeViewNodeSorter = null;
            }
        }

		public ArrayList SelectedItems
		{
			get
			{
				return m_SelectedNodes;
			}
		}

		public TreeNode LastSelectedNode
		{
			get
			{
				if(m_SelectedNodes.Count>0)
				{
					return (TreeNode)m_SelectedNodes[m_SelectedNodes.Count-1];
				}
				else
				{
					return null;
				}
			}
			set
			{
				UnselectAllNodes();
				if(value != null)
				{
					SelectNode(value);
					value.EnsureVisible();
				}
			}
		}

		/* all this is to handle the drag up and down beyond boundaries */
		long m_Ticks;
		protected override void OnDragOver(DragEventArgs drgevent)
		{
			base.OnDragOver (drgevent);
			Point pt = PointToClient(new Point(drgevent.X, drgevent.Y));
			TreeNode node = GetNodeAt(pt);
			TimeSpan ts = new TimeSpan(DateTime.Now.Ticks - m_Ticks);
			//scroll up
			if (pt.Y < ItemHeight) 
			{
                if (node != null)
                {
                    // if within one node of top, scroll quickly
                    if (node.PrevVisibleNode != null)
                    {
                        node = node.PrevVisibleNode;
                    }
                    node.EnsureVisible();
                    m_Ticks = DateTime.Now.Ticks;
                }
			} 
			else if (pt.Y < (ItemHeight * 2)) 
			{
                // if within two nodes of the top, scroll slowly
                if (ts.TotalMilliseconds > 350)
                {
                    if (node != null)
                    {
                        node = node.PrevVisibleNode;
                        if (node.PrevVisibleNode != null)
                        {
                            node = node.PrevVisibleNode;
                        }
                        node.EnsureVisible();
                    }
                    m_Ticks = DateTime.Now.Ticks;
                }
            }


            //scroll down
			if (pt.Y > ItemHeight) 
			{
				// if within one node of top, scroll quickly
                if (node != null)
                {
                    if (node != null && node.NextVisibleNode != null)
                    {
                        node = node.NextVisibleNode;
                    }
                    if (node != null)
                        node.EnsureVisible();
                }
				m_Ticks = DateTime.Now.Ticks;
			} 
			else if (pt.Y > (ItemHeight * 2)) 
			{
				// if within two nodes of the top, scroll slowly
				if (ts.TotalMilliseconds > 350)
				{
                    if (node != null)
                    {
                        node = node.NextVisibleNode;
                        if (node.NextVisibleNode != null)
                        {
                            node = node.NextVisibleNode;
                        }
                        node.EnsureVisible();
                    }
					m_Ticks = DateTime.Now.Ticks;
				}
			}
		}

        protected override void OnDragLeave(EventArgs e)
        {
            // reset selection to the node that was selected when the user started dragging
            UnselectAllNodes();
            foreach (TreeNode n in m_SelectedNodesAtDragStart)
            {
                SelectNode(n);
            }
            base.OnDragLeave(e);
        }

		private void InitializeComponent()
		{
			// 
			// ScrollableTreeView
			// 

			PathSeparator = ".";
		}

		private void UnselectNode(TreeNode n)
		{
			m_SelectedNodes.Remove(n);
			n.BackColor = Color.White;
			n.ForeColor = Color.Black;
		}

		private void SelectNode(TreeNode n)
		{
			if(!m_SelectedNodes.Contains(n))
			{
				m_SelectedNodes.Add(n);
				n.BackColor = SystemColors.Highlight;
				n.ForeColor = SystemColors.HighlightText;
			}
		}

		private void UnselectAllNodes()
		{
			foreach(TreeNode sn in m_SelectedNodes)
			{
				sn.BackColor = Color.White;
				sn.ForeColor = Color.Black;
			}
			m_SelectedNodes.Clear();
		}

		protected override void OnBeforeSelect(TreeViewCancelEventArgs e)
		{
			base.OnBeforeSelect (e);
			e.Cancel = true;
		}

		protected override void OnMouseUp(System.Windows.Forms.MouseEventArgs e)
		{
			base.OnMouseUp(e);
			// dont unselect on right click since it screws with popup menus
			if(e.Button == MouseButtons.Left)
			{
				TreeNode n = GetNodeAt(e.X, e.Y);
				if(n != null)
				{
					// is this node already selected?
					if(m_SelectedNodes.Contains(n))
					{
						if(!(Control.ModifierKeys == Keys.Control) && !(Control.ModifierKeys == Keys.Shift))
						{
							UnselectAllNodes();
							SelectNode(n);
						}
					}
				}
			}
		}

        protected override void OnMouseDown(System.Windows.Forms.MouseEventArgs e)
		{
			base.OnMouseDown(e);

			TreeNode n = GetNodeAt(e.X, e.Y);
			if(n != null)
			{
                m_SelectedNodesAtDragStart.Clear();
				// is this node already selected?
				if(m_SelectedNodes.Contains(n))
				{
					if(Control.ModifierKeys == Keys.Control)
					{
						// control is pressed - remove this from selection
						UnselectNode(n);					
					}
//					else
//					{
//						UnselectAllNodes();
//						SelectNode(n);
//					}
				}
				else
				{
					TreeNode lastNode = null;
					
					if(m_SelectedNodes.Count > 0)
					{
						lastNode = (TreeNode)m_SelectedNodes[m_SelectedNodes.Count-1];
					}

					if(!(Control.ModifierKeys == Keys.Control))
					{	
						UnselectAllNodes();
					}	
					if((Control.ModifierKeys == Keys.Shift))
					{
						if(lastNode != null && lastNode.Parent == n.Parent)
						{
							// select all nodes between lastNode and this node
							for(int i = lastNode.Index; i != n.Index; i += Math.Sign(n.Index-lastNode.Index))
							{
								if(lastNode.Parent != null)
								{
									SelectNode(lastNode.Parent.Nodes[i]);
								}
								else
								{
									SelectNode(Nodes[i]);
								}
							}
						}
					}
					SelectNode(n);
				}
                m_SelectedNodesAtDragStart.AddRange(m_SelectedNodes);
			}
			else
			{
				UnselectAllNodes();
			}
           
		}

		protected override void OnKeyDown(KeyEventArgs e)
		{
            if (LastSelectedNode != null)
            {
                if (e.KeyCode == Keys.Left)
                    LastSelectedNode.Collapse();
                else if (e.KeyCode == Keys.Right)
                    LastSelectedNode.Expand();
                else if (e.KeyCode == Keys.Up)
                {
                    TreeNode prevNode = LastSelectedNode.PrevVisibleNode;
                    if (prevNode != null)
                    {
                        UnselectAllNodes();
                        SelectNode(prevNode);
                    }
                }
                else if (e.KeyCode == Keys.Down)
                {
                    TreeNode nextNode = LastSelectedNode.NextVisibleNode;
                    if (nextNode != null)
                    {
                        UnselectAllNodes();
                        SelectNode(nextNode);
                    }
                }
            }

			base.OnKeyDown(e);
		}
        
		private ArrayList m_SelectedNodes = new ArrayList();
        private ArrayList m_SelectedNodesAtDragStart = new ArrayList();

        public void AddNode(TreeNode tn)
        {
            this.Nodes.Add(tn);
        }

	}
}
