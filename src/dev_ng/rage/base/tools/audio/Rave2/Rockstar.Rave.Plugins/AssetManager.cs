using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using audAssetManagement2;
using rage;

namespace Rave.Plugins
{
    /// <summary>
    ///   Wrapper around the autAssetManagement library
    /// </summary>
    public class AssetManager
    {
        private static readonly List<asset> ms_lockedAssets = new List<asset>();
        private static assetManager ms_assetManager;

        public static changelist WaveChangeList { get; set; }

        public static void SetAssetManager(assetManager assetManager)
        {
            ms_assetManager = assetManager;
        }

        public static assetManager GetAssetManager()
        {
            return ms_assetManager;
        }

        public static bool IsWavePathLocked(string waveAssetPath)
        {
            var assetLockPath = Configuration.PlatformWavePath + waveAssetPath;
            var assetLockFile = assetLockPath + ".lock";
            // if the lock file exists locally but not in asset management then we say its locked...
            return ((!GetAssetManager().ExistsAsAsset(assetLockFile) && File.Exists(assetLockFile)) ||
                    GetAssetManager().IsCheckedOut(assetLockFile));
        }

        public static bool LockWavePath(string assetPath)
        {
            if (WaveChangeList == null)
            {
                WaveChangeList = GetAssetManager().CreateNewChangelist("[Rave] Wave Changelist");
            }

            var assetLockPath = Configuration.PlatformWavePath + assetPath;
            var assetLockFile = assetLockPath + ".lock";

            // make sure there is a local copy of the directory structure
            Utilities.CreateRequiredDirectories(assetLockPath);

            if (!ms_assetManager.ExistsAsAsset(assetLockFile))
            {
                var sw = new StreamWriter(assetLockFile, true, Encoding.ASCII);
                sw.WriteLine("{0} - {1}", Environment.UserName, DateTime.Now);
                sw.Close();
                
                //sumbit this change straight away to "Lock" wave Path
                try
                {
                    var lockList = ms_assetManager.CreateNewChangelist("[Rave] Lock for Wave Path added");
                    lockList.MarkAssetForAdd(assetLockFile, "text");
                    lockList.Submit();
                }
                catch (Exception e)
                {
                    ErrorManager.HandleError(e);
                    return false;
                }

                try
                {
                    var a = WaveChangeList.CheckoutAsset(assetLockFile, true);
                    ms_lockedAssets.Add(a);
                }
                catch (Exception e)
                {
                    ErrorManager.HandleError(e);
                    return false;
                }
            }
            else
            {
                var status = ms_assetManager.GetFileLockInfo(assetLockFile);
                if (status.IsLocked)
                {
                    if (!status.IsLockedByMe)
                    {
                        ErrorManager.HandleInfo(assetLockFile + " is already locked by " + status.Owner);
                        return false;
                    }
                }
                else
                {
                    //Assume already in asset list
                    if (!status.IsLockedByMe)
                    {
                        try
                        {
                            var a = WaveChangeList.CheckoutAsset(assetLockFile, true);
                            ms_lockedAssets.Add(a);
                        }
                        catch (Exception e)
                        {
                            // couldn't check out
                            ErrorManager.HandleError(e);
                            return false;
                        }
                    }    
                }
            }
            return true;
        }

        public static void UnlockWavePath(string path)
        {
            var assetPath = Configuration.PlatformWavePath + "\\" + path + "\\.lock";
            if (WaveChangeList != null)
            {
                var a = WaveChangeList.GetAsset(assetPath);
                if (a != null)
                {
                    a.Revert();
                }
            }
        }

        public static void UnlockAllAssets()
        {
            var lockedAssetsToRemove = new List<asset>();
            foreach (var lockedAsset in ms_lockedAssets)
            {
                try
                {
                    lockedAsset.Revert();
                    lockedAssetsToRemove.Add(lockedAsset);
                }
                catch (Exception e)
                {
                    ErrorManager.HandleError(e);
                }
            }

            foreach (var a in lockedAssetsToRemove)
            {
                ms_lockedAssets.Remove(a);
            }
        }

        public static changelist GoLive(ArrayList platformSettings)
        {
            var liveList = ms_assetManager.CreateNewChangelist("Going Live (All Platforms)");
            var bIntegrateSuccess = false;

            foreach (PlatformSetting ps in platformSettings)
            {
                // get latest built wave assets
                try
                {
                    ms_assetManager.GetLatest(ps.BuildOutput, false);
                    ms_assetManager.GetLatest(ps.LiveOutput, false);

                    ms_assetManager.Integrate(ps.BuildOutput, ps.LiveOutput, liveList);
                    ms_assetManager.Resolve(ps.LiveOutput, enResolveType.kAcceptThiers);

                    bIntegrateSuccess = true;
                }
                catch (Exception e)
                {
                    if (e.ToString().ToLower().Contains("already integrated"))
                    {
                        continue;
                    }
                    throw;
                }
            }
            if (!bIntegrateSuccess)
            {
                liveList.Delete();
                liveList = null;
            }
            return liveList;
        }
    }
}