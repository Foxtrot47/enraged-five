using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using rage.ToolLib;

namespace Rave.Plugins.SoundBrowser.Nodes
{
    /// <summary>
    ///   Summary description for SoundBankNode.
    /// </summary>
    public class SoundBankNode : BaseNode
    {
        private const int BASE_IMAGE_INDEX = 1;

        private readonly Hashtable m_iconMap;

        public SoundBankNode(IObjectBank bank, Hashtable iconMap) : base(bank.Name, bank.Episode, bank.Type)
        {
            m_iconMap = iconMap;
            ObjectBank = bank;
            ObjectBank.StatusChanged += OnBankStatusChanged;
            RecreateSoundNodes();
        }

        public IObjectBank ObjectBank { get; private set; }
        public event Action BankStatusChanged;

        private int GetIconIndexForTypeName(string typeName)
        {
            if (m_iconMap.ContainsKey(typeName))
            {
                return (int) m_iconMap[typeName];
            }
            return 0;
        }

        public override int GetBaseIconIndex()
        {
            return ObjectBank != null && ObjectBank.IsBankCheckedOut() ? BASE_IMAGE_INDEX + 1 : BASE_IMAGE_INDEX;
        }

        public override string GetTypeDescription()
        {
            return "Sound Bank";
        }

        public void RecreateSoundNodes()
        {
            var expandedVirtualFolders = new HashSet<string>();
            foreach (var node in Nodes)
            {
                var virtualFolderNode = node as VirtualFolderNode;
                if (virtualFolderNode != null && virtualFolderNode.IsExpanded && !expandedVirtualFolders.Contains(virtualFolderNode.ObjectName))
                {
                    expandedVirtualFolders.Add(virtualFolderNode.ObjectName);
                }
            }
            Nodes.Clear();
            CreateSoundNodes(expandedVirtualFolders);
            UpdateIcon();
        }

        private void CreateSoundNodes(HashSet<string> expandedVirtualFolders)
        {
            foreach (var s in ObjectBank.Objects)
            {
                if (s.VirtualFolderName == null)
                {
                    Nodes.Add(new SoundNode(s, GetIconIndexForTypeName(s.TypeName)));
                }
                else
                {
                    var parentNode = Nodes.Cast<BaseNode>().FirstOrDefault(bn => bn.GetType() == typeof (VirtualFolderNode) && bn.ObjectName == s.VirtualFolderName);
                    if (parentNode == null)
                    {
                        parentNode = new VirtualFolderNode(s.VirtualFolderName, ObjectBank.Episode, ObjectBank.Type);
                        Nodes.Add(parentNode);
                    }
                    parentNode.Nodes.Add(new SoundNode(s, GetIconIndexForTypeName(s.TypeName)));
                }
            }

            foreach (var node in Nodes)
            {
                var virtualFolderNode = node as VirtualFolderNode;
                if (virtualFolderNode != null && expandedVirtualFolders.Contains(virtualFolderNode.ObjectName))
                {
                    virtualFolderNode.Expand();
                }
            }
        }

        private void UpdateIcon()
        {
            if (ObjectBank.IsBankCheckedOut())
            {
                SelectedImageIndex = ImageIndex = BASE_IMAGE_INDEX + 1;
            }
            else
            {
                SelectedImageIndex = ImageIndex = BASE_IMAGE_INDEX;
            }
        }

        private void OnBankStatusChanged()
        {
            BankStatusChanged.Raise();
            RecreateSoundNodes();
        }
    }
}