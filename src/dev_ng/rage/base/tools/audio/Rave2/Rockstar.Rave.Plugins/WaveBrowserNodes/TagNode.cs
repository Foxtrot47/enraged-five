using System.Collections.Generic;

namespace Rave.Plugins.WaveBrowser.Nodes
{
    /// <summary>
    ///   Summary description for TagNode.
    /// </summary>
    public class TagNode : EditorTreeNode
    {
        private readonly Dictionary<string, string> m_platformValues;
        private string m_tagName;

        public TagNode(IActionLog actionLog, string strDisplayName, IWaveBrowser waveBrowser)
            : base(actionLog, strDisplayName, waveBrowser)
        {
            m_tagName = strDisplayName;
            m_platformValues = new Dictionary<string, string>();
        }

        public Dictionary<string, string> Values
        {
            get { return m_platformValues; }
        }

        public string GetTagName()
        {
            return m_tagName;
        }

        public void SetTagName(string tagName)
        {
            m_tagName = tagName;
        }

        public void RemoveValue(string platform)
        {
            if (m_platformValues.ContainsKey(platform))
            {
                m_platformValues.Remove(platform);
            }
            if (m_platforms.Contains(platform))
            {
                m_platforms.Remove(platform);
            }
        }

        public void SetValue(string platform, string val)
        {
            if (platform == null)
            {
                foreach (var s in m_platforms)
                {
                    m_platformValues[s] = val;
                }
            }
            else
            {
                if (!m_platforms.Contains(platform))
                {
                    m_platforms.Add(platform);
                }

                m_platformValues[platform] = val;
            }

            UpdateDisplayName();
        }

        public string GetCurrentValue(string platform)
        {
            if (!m_platformValues.ContainsKey(platform))
            {
                return null;
            }
            return m_platformValues[platform];
        }

        public override string GetObjectPath()
        {
            return ((EditorTreeNode) Parent).GetObjectPath() + "\\" + m_tagName;
        }

        public override int GetBaseIconIndex(IWaveBrowser waveBrowser)
        {
            return waveBrowser.GetEditorTreeView().GetIconIndex(m_objName);
        }

        public override bool LockForEditing()
        {
            return false;
        }

        public override bool TryToLock()
        {
            return false;
        }

        public override void Unlock()
        {
            //Nothing to do here
            return;
        }

        public override bool IsNodeTypeLockable()
        {
            return false;
        }

        public override string GetTypeDescription()
        {
            return "Tag";
        }
    }
}