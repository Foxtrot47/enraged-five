using System.Windows.Forms;
using Rave.Plugins.SoundBrowser.Nodes;

namespace Rave.Plugins
{
    public interface ISoundBrowser
    {
        TreeView GetTreeView();

        void FindObject(IObjectInstance obj);

        void FindObjectBank(IObjectBank objectBank);

        void ShowHierarchy(SoundNode node);
    }
}