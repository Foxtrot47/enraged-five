using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using System.Xml;
using rage;

namespace Rave.Plugins
{
    /// <summary>
    ///   Summary description for Configuration.
    /// </summary>
    public static class Configuration
    {
        private const string WAVE_WRAPPER_SOUND_TYPE = "SimpleSound";
        private const string SOUND_WRAPPER_SOUND_TYPE = "WrapperSound";
        private static audProjectEntry ms_project;

        public static string[] MetadataCompilerJitPaths { get; private set; }

        public static IRAVEWaveImportPlugin SelectedPlugin { get; set; }

        public static string ProjectSettingsAssetPath
        {
            get { return ms_project.ProjectSettings; }
        }

        public static int AssetManagementType { get; private set; }

        public static string AssetServer { get; private set; }

        public static string AssetProject { get; private set; }

        public static string AssetUser { get; private set; }

        public static string AssetPassword { get; private set; }

        public static string DepotRoot { get; private set; }

        public static string SoftRoot { get; private set; }

        public static bool IsProgrammerSupportEnabled
        {
            get
            {
                //  We don't bother to create this in the xml, since this is a "hidden" feature
                return ms_project != null && ms_project.IsProgrammerSupportEnabled;
            }
        }

        public static string PlatformConfigPath { get; private set; }

        public static string PlatformWavePath { get; private set; }

        public static string ObjectXmlPath { get; private set; }

        public static string WorkingPath { get; private set; }

        public static string DefaultModelPath { get; set; }

        public static string ProjectName
        {
            get { return ProjectSettings.GetProjectName(); }
        }

        public static string WaveWrapperSoundType
        {
            get { return WAVE_WRAPPER_SOUND_TYPE; }
        }

        public static string SoundWrapperSoundType
        {
            get { return SOUND_WRAPPER_SOUND_TYPE; }
        }

        public static string[] WaveAttributes
        {
            get { return ProjectSettings.GetWaveAttributes(); }
        }

        public static string WaveEditorPath { get; set; }

        public static string WaveSlotFile { get; private set; }

        public static ArrayList PlatformSettings
        {
            get { return ProjectSettings.GetPlatformSettings(); }
        }

        public static string MetadataCompilerPath { get; private set; }

        public static IEnumerable<audMetadataType> MetadataTypes
        {
            get { return ProjectSettings.GetMetadataTypes(); }
        }

        public static IEnumerable<audMetadataFile> MetadataFiles
        {
            get { return ProjectSettings.GetMetadataSettings(); }
        }

        public static string WaveBuildPath { get; private set; }

        public static string WaveBuildToolsPath { get; private set; }

        public static string PresetPath { get; private set; }

        public static string GlobalVariablesListPath { get; private set; }

        public static XmlNode[] RavePlugins
        {
            get { return ProjectSettings.GetRavePlugins(); }
        }

        public static IEnumerable<audBuildSet> BuildSets
        {
            get { return ProjectSettings.BatchBuilds; }
        }

        public static bool Use3ByteNameTableOffset
        {
            get { return ProjectSettings.Use3ByteNameTableOffset(); }
        }

        public static audProjectSettings ProjectSettings { get; private set; }

        public static void SetCurrentPlatform(PlatformSetting setting)
        {
            ProjectSettings.SetCurrentPlatform(setting);
        }

        public static void SetCurrentPlatformByTag(string platformTag)
        {
            ProjectSettings.SetCurrentPlatformByTag(platformTag);
        }

        public static void InitProject(audProjectEntry project)
        {
            ms_project = project;
            AssetManagementType = Int32.Parse(project.AssetManager);
            AssetPassword = project.Password;
            AssetProject = project.AssetProject;
            AssetServer = project.AssetServer;
            AssetUser = project.UserName;
            DepotRoot = project.DepotRoot;
            SoftRoot = project.SoftRoot;
            WaveEditorPath = project.WaveEditor;
        }

        public static bool LoadProjectSettings()
        {
            try
            {
                AssetManager.GetAssetManager().GetLatest(ms_project.ProjectSettings, false);
            }
            catch (Exception)
            {
                AssetManager.GetAssetManager().GetLatest(ms_project.ProjectSettings, true);
            }

            ProjectSettings =
                new audProjectSettings(AssetManager.GetAssetManager().GetWorkingPath(ms_project.ProjectSettings));

            return SetupPaths();
        }

        private static bool SetupPaths()
        {
            var assetMgr = AssetManager.GetAssetManager();

            if (ProjectSettings.MetadataCompilerJitPaths != null)
            {
                var count = ProjectSettings.MetadataCompilerJitPaths.Length;
                MetadataCompilerJitPaths = new string[count];
                for (var i = 0; i < count; ++i)
                {
                    MetadataCompilerJitPaths[i] = assetMgr.GetWorkingPath(ProjectSettings.MetadataCompilerJitPaths[i]);
                    if (!ValidatePath(MetadataCompilerJitPaths[i]))
                    {
                        return false;
                    }
                }
            }

            PlatformConfigPath =
                assetMgr.GetWorkingPath(ProjectSettings.GetBuildInfoPath()).Replace(Path.AltDirectorySeparatorChar,
                                                                                       Path.DirectorySeparatorChar);
            if (!ValidatePath(PlatformConfigPath))
            {
                return false;
            }

            ObjectXmlPath =
                assetMgr.GetWorkingPath(ProjectSettings.GetSoundXmlPath()).Replace(Path.AltDirectorySeparatorChar,
                                                                                      Path.DirectorySeparatorChar);
            if (!ValidatePath(ObjectXmlPath))
            {
                return false;
            }

            PlatformWavePath =
                assetMgr.GetWorkingPath(ProjectSettings.GetWaveInputPath()).Replace(Path.AltDirectorySeparatorChar,
                                                                                       Path.DirectorySeparatorChar);
            if (!ValidatePath(PlatformWavePath))
            {
                return false;
            }

            WorkingPath = assetMgr.GetWorkingPath(string.Empty);
            if (string.IsNullOrWhiteSpace(WorkingPath))
            {
                WorkingPath = ProjectSettings.GetBackupWorkingPath();
            }
            if (!ValidatePath(WorkingPath))
            {
                return false;
            }

            MetadataCompilerPath =
                assetMgr.GetWorkingPath(ProjectSettings.GetMetadataCompilerPath()).Replace(
                    Path.AltDirectorySeparatorChar, Path.DirectorySeparatorChar);
            if (!ValidatePath(MetadataCompilerPath))
            {
                return false;
            }

            WaveSlotFile =
                assetMgr.GetWorkingPath(ProjectSettings.GetWaveSlotSettings()).Replace(
                    Path.AltDirectorySeparatorChar, Path.DirectorySeparatorChar);
            if (!ValidatePath(WaveSlotFile))
            {
                return false;
            }

            WaveBuildPath =
                assetMgr.GetWorkingPath(ProjectSettings.GetBuildPath()).Replace(Path.AltDirectorySeparatorChar,
                                                                                   Path.DirectorySeparatorChar);
            if (!ValidatePath(WaveBuildPath))
            {
                return false;
            }

            WaveBuildToolsPath =
                assetMgr.GetWorkingPath(ProjectSettings.GetBuildToolsPath()).Replace(Path.AltDirectorySeparatorChar,
                                                                                        Path.DirectorySeparatorChar);
            if (!ValidatePath(WaveBuildToolsPath))
            {
                return false;
            }

            PresetPath =
                assetMgr.GetWorkingPath(ProjectSettings.GetPresetPath()).Replace(Path.AltDirectorySeparatorChar,
                                                                                    Path.DirectorySeparatorChar);
            if (!ValidatePath(PresetPath))
            {
                return false;
            }

            if (!string.IsNullOrEmpty(ProjectSettings.GlobalVariablesList))
            {
                GlobalVariablesListPath =
                        assetMgr.GetWorkingPath(ProjectSettings.GlobalVariablesList).Replace(Path.AltDirectorySeparatorChar,
                                                                                                Path.DirectorySeparatorChar);
                if (!ValidatePath(GlobalVariablesListPath))
                {
                    return false;
                } 
            }
            return true;
        }

        public static List<string> WaveRefsToSkip()
        {
            return ProjectSettings.WaveRefsToSkip();
        }

        public static string ResolvePath(string path)
        {
            return ProjectSettings.ResolvePath(path);
        }

        private static bool ValidatePath(string path)
        {
            var modifiedPath = path.Trim().ToLower();
            if (string.IsNullOrEmpty(modifiedPath) ||
                modifiedPath.Contains(Path.GetDirectoryName(Application.ExecutablePath).ToLower()))
            {
                ErrorManager.HandleInfo(string.Format("Invalid path: \"{0}\" detected! Rave will not be started.", path));
                return false;
            }
            return true;
        }
    }
}