using System;

namespace Rave.Plugins.WaveBrowser.Nodes
{
    /// <summary>
    ///   Summary description for PackNode.
    /// </summary>
    public class PackFolderNode : WaveContainerNode
    {
        private const int BASE_IMAGE_INDEX = 23;

        public PackFolderNode(IActionLog actionLog, string strDisplayName, IWaveBrowser waveBrowser)
            : base(actionLog, strDisplayName, waveBrowser)
        {
        }

        public override string GetObjectPath()
        {
            return GetObjectName();
        }

        public override int GetBaseIconIndex(IWaveBrowser waveBrowser)
        {
            return BASE_IMAGE_INDEX;
        }

        public override bool TryToLock()
        {
            UpdateLockedState();
            if (!IsLocked() &&
                !AllowAutoLock)
            {
                if (LockForEditing())
                {
                    return true;
                }
                return false;
            }
            return true;
        }

        public override bool LockForEditing()
        {
            if (!m_lockedLocally &&
                AssetManager.LockWavePath(GetObjectPath()))
            {
                m_lockedLocally = true;
                UpdateDisplay();
            }

            return m_lockedLocally;
        }

        public override void Unlock()
        {
            if (m_lockedLocally)
            {
                try
                {
                    AssetManager.UnlockWavePath(GetObjectPath());
                    m_lockedLocally = false;
                    UpdateDisplay();
                }
                catch (Exception e)
                {
                    ErrorManager.HandleError(e);
                }
            }
        }

        public override string GetTypeDescription()
        {
            return "Pack Folder";
        }
    }
}