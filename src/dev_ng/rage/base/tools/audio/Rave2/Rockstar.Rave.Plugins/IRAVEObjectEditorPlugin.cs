using System;

namespace Rave.Plugins
{
    public enum Mode
    {
        DEFAULT,
        TEMPLATE,
        MULTIEDIT,
        DATAPARENT,
        DATACHILD
    } ;

    public interface IRAVEObjectEditorPlugin : IRAVEPlugin
    {
        string ObjectType { get; }

        event Action<IObjectInstance> OnObjectEditClick;
        event Action<IObjectInstance> OnObjectRefClick;
        event Action<string, string> OnWaveRefClick;
        event Action<string> OnWaveBankRefClick;

        void EditObject(IObjectInstance objectInstance, Mode mode);
    }
}