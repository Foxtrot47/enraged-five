using System;
using System.Xml;

namespace Rave.Plugins
{
	/// <summary>
	/// Summary description for PropertyEditorUtil.
	/// </summary>
    /// 
    public struct tCategorySettings
    {
        public float Volume;
        public float DistanceRollOffScale;
        public float OcclusionDamping;
        public float EnvironementalFilterDamping;
        public float EnvironmentalReverbDamping;
        public float EnvironmentalLoudness;
        public short Pitch;
        public ushort FilterCutoff;

        public tCategorySettings(float vol, float dis, float occ, float envFD, float envRD, float envL, short pitch, ushort filter)
        {
            Volume = vol;
            DistanceRollOffScale = dis;
            OcclusionDamping = occ;
            EnvironementalFilterDamping = envFD;
            EnvironmentalReverbDamping = envRD;
            EnvironmentalLoudness = envL;
            Pitch = pitch;
            FilterCutoff = filter;
        }
    }

	public class PropertyEditorUtil
	{
		public static int ConvertFromDisplayValue(string displayVal, string units)
		{
             float actualVal = float.Parse(displayVal);

                if (units != null)
                {
                    switch (units.ToLower())
                    {
                        case "cents":
                        case "0.01units":
                        case "mb":
                            actualVal *= 100.0f;
                            break;
                    }
                }


                return Convert.ToInt32(actualVal);
                     
		}

		public static void ConvertToDisplayValue(decimal val, string units, out string displayVal, out string displayUnits)
		{
			decimal actualVal = val;
			displayUnits = "";

			if(units != null)
			{
				switch(units.ToLower())
				{
					case "mb":
						actualVal /= 100;
						displayUnits = "dB";
						break;
					case "0.01units":
						actualVal /= 100;
						break;
					case "cents":
						actualVal /= 100;
						displayUnits = "st";
						break;
					case "ms":
						displayUnits = "ms";
						break;
                    case "degrees":
						if(val == -1)
						{
							displayUnits = "";
						}
						else
						{
							displayUnits = "\x00B0";
						}
						break;
					default:
						displayUnits = units;
						break;
				}
			}

			displayVal =  actualVal.ToString();
		}

        public static XmlNode FindXmlNode(string name, XmlNode parent)
        {
            if (parent == null)
            {
                return null;
            }
            foreach (XmlNode n in parent.ChildNodes)
            {
                if (n.Name == name)
                {
                    return n;
                }
            }
            return null;
        }

    }
}
