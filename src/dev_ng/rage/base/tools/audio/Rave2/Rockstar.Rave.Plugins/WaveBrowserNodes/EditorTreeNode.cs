using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using rage;

namespace Rave.Plugins.WaveBrowser.Nodes
{
    /// <summary>
    ///   Summary description for EditorTreeNode.
    /// </summary>
    public abstract class EditorTreeNode : TreeNode
    {
        #region NodeDisplayState enum

        public enum NodeDisplayState
        {
            NotPresent,
            AlreadyBuilt,
            AddedInPendingWaveList,
            ChangedInPendingWaveList,
            AddedLocally,
            ChangedLocally,
            NotPresentMetadata, //not used can't tell if non present has metadata
            AlreadyBuiltMetadata,
            AddedInPendingWaveListMetadata,
            ChangedInPendingWaveListMetadata,
            AddedLocallyMetadata,
            ChangedLocallyMetadata
        }

        #endregion

        protected IActionLog m_actionLog;
        private NodeDisplayState m_activeState;
        protected bool m_lockedLocally;
        protected string m_objName;
        protected Dictionary<string, NodeDisplayState> m_platformStates;
        protected List<string> m_platforms;
        protected IWaveBrowser m_waveBrowser;

        protected EditorTreeNode(IActionLog actionLog, string strDisplayName, IWaveBrowser waveBrowser)
        {
            m_waveBrowser = waveBrowser;
            m_actionLog = actionLog;
            m_objName = strDisplayName;
            Text = m_objName;

            ImageIndex = GetBaseIconIndex(m_waveBrowser) + (int) IconColours.GreenIcon;
            SelectedImageIndex = ImageIndex;

            m_lockedLocally = false;

            m_platforms = new List<string>();
            m_platformStates = new Dictionary<string, NodeDisplayState>();
        }

        public static bool AllowAutoLock { get; set; }

        public abstract int GetBaseIconIndex(IWaveBrowser waveBrowser);

        public List<string> GetPlatforms()
        {
            return m_platforms;
        }

        public virtual void AddPlatform(string platform, NodeDisplayState state)
        {
            if (platform != "All")
            {
                foreach (var p in m_platforms)
                {
                    if (p == platform)
                    {
                        m_platformStates[platform] = state;
                        return;
                    }
                }
                m_platforms.Add(platform);
                m_platformStates.Add(platform, state);
            }
            else
            {
                // add all platforms
                foreach (PlatformSetting platsetting in Configuration.PlatformSettings)
                {
                    if (!m_platforms.Contains(platsetting.PlatformTag))
                    {
                        m_platforms.Add(platsetting.PlatformTag);
                        m_platformStates.Add(platsetting.PlatformTag, state);
                    }
                }
            }
        }

        public virtual void RemovePlatform(string platform)
        {
            m_platforms.Remove(platform);
            m_platformStates.Remove(platform);
        }

        public void UpdateDisplayName()
        {
            if (m_platforms.Count !=
                Configuration.PlatformSettings.Count)
            {
                Text = m_objName + " (" + GetPlatformsString() + ")";
            }
            else
            {
                Text = m_objName;
            }
        }

        public void SetPlatformDisplayState(string platform, NodeDisplayState state)
        {
            if (platform == "All")
            {
                foreach (var p in m_platforms)
                {
                    m_platformStates[p] = state;
                }
            }
            else
            {
                m_platformStates[platform] = state;
            }
        }

        public string GetPlatformsString()
        {
            var ret = string.Empty;
            if (m_platforms.Count !=
                Configuration.PlatformSettings.Count)
            {
                var needComma = false;
                foreach (var platform in m_platforms)
                {
                    if (needComma)
                    {
                        ret += ",";
                    }
                    ret += platform;
                    needComma = true;
                }
            }
            else
            {
                ret = "All";
            }
            return ret;
        }

        public string GetObjectName()
        {
            return m_objName;
        }

        public NodeDisplayState GetNodeDisplayState()
        {
            return m_activeState;
        }

        public NodeDisplayState GetNodeDisplayState(string platform)
        {
            if (!m_platforms.Contains(platform))
            {
                return NodeDisplayState.NotPresent;
            }

            return m_platformStates[platform];
        }

        public string GetStateDescription(string platformtag)
        {
            if (!m_platformStates.ContainsKey(platformtag))
            {
                return "Not Present";
            }
            return GetStateDescription(m_platformStates[platformtag]);
        }

        public string GetStateDescription()
        {
            return GetStateDescription(m_activeState);
        }

        public string GetStateDescription(NodeDisplayState state)
        {
            string desc;

            switch (state)
            {
                case NodeDisplayState.AlreadyBuilt:
                    desc = "Built";
                    break;
                case NodeDisplayState.AlreadyBuiltMetadata:
                    desc = "Built: Custom Metadata";
                    break;
                case NodeDisplayState.AddedInPendingWaveList:
                case NodeDisplayState.ChangedInPendingWaveList:
                    desc = "Pending";
                    break;
                case NodeDisplayState.AddedInPendingWaveListMetadata:
                case NodeDisplayState.ChangedInPendingWaveListMetadata:
                    desc = "Pending: Custom Metadata";
                    break;
                case NodeDisplayState.AddedLocally:
                    desc = "Added locally";
                    break;
                case NodeDisplayState.AddedLocallyMetadata:
                    desc = "Added locally: Cutsom Metadata";
                    break;
                case NodeDisplayState.ChangedLocally:
                    desc = "Changed locally";
                    break;
                case NodeDisplayState.ChangedLocallyMetadata:
                    desc = "Changed locally: Custom MetaData";
                    break;
                default:
                    desc = "Unknown";
                    break;
            }

            if (m_lockedLocally)
            {
                desc += " (locked locally)";
            }

            return desc;
        }

        public void UpdateLockedState()
        {
            m_lockedLocally = AssetManager.IsWavePathLocked(GetObjectPath());
        }

        public static PackNode FindParentPackNode(TreeNode child)
        {
            if (child == null)
            {
                return null;
            }

            if (child.GetType() ==
                typeof(PackNode))
            {
                return child as PackNode;
            }
            return FindParentPackNode(child.Parent);
        }

        public static BankNode FindParentBankNode(TreeNode child)
        {
            if (child == null)
            {
                return null;
            }

            if (child.GetType() ==
                typeof (BankNode))
            {
                return child as BankNode;
            }
            return FindParentBankNode(child.Parent);
        }

        public void UpdateDisplayState(NodeDisplayState state, string platform, bool bForce, bool bUpdateParent)
        {
            SetPlatformDisplayState(platform, state);

            if (bForce || m_activeState != state)
            {
                UpdateDisplay();
            }

            if (bUpdateParent)
            {
                if (m_activeState != state && state != NodeDisplayState.NotPresent &&
                    (int) m_activeState > 5)
                {
                    state = state - 6;
                }
                var parent = Parent as EditorTreeNode;
                if (parent != null)
                {
                    parent.UpdateDisplayState(state, platform, bForce, true);
                }
            }
        }

        private NodeDisplayState GetActiveDisplayState()
        {
            NodeDisplayState state;

            if (m_waveBrowser.GetActivePlatform() == "All")
            {
                // find the 'dominant' state
                var statenum = 0;
                foreach (var platform in m_platforms)
                {
                    // use mod 6 to distguish between states with metadata
                    if (m_platformStates.ContainsKey(platform) &&
                        (int) m_platformStates[platform] > statenum)
                    {
                        statenum = (int) m_platformStates[platform];
                    }
                }
                state = (NodeDisplayState) statenum;
            }
            else
            {
                state = m_platformStates.ContainsKey(m_waveBrowser.GetActivePlatform())
                            ? m_platformStates[m_waveBrowser.GetActivePlatform()]
                            : NodeDisplayState.NotPresent;
            }
            return state;
        }

        public void UpdateDisplay()
        {
            var iconIndex = 0;
            m_activeState = GetActiveDisplayState();

            switch (m_activeState)
            {
                case NodeDisplayState.AlreadyBuilt:
                    iconIndex = GetBaseIconIndex(m_waveBrowser) + (int) IconColours.GreenIcon;
                    break;
                case NodeDisplayState.AlreadyBuiltMetadata:
                    iconIndex = GetBaseIconIndex(m_waveBrowser) + (int) IconColours.GreenMetadataIcon;
                    break;
                case NodeDisplayState.AddedInPendingWaveList:
                    iconIndex = GetBaseIconIndex(m_waveBrowser) + (int) IconColours.AmberIcon;
                    break;
                case NodeDisplayState.AddedInPendingWaveListMetadata:
                    iconIndex = GetBaseIconIndex(m_waveBrowser) + (int) IconColours.AmberMetadataIcon;
                    break;
                case NodeDisplayState.ChangedInPendingWaveList:
                    iconIndex = GetBaseIconIndex(m_waveBrowser) + (int) IconColours.AmberIcon;
                    break;
                case NodeDisplayState.ChangedInPendingWaveListMetadata:
                    iconIndex = GetBaseIconIndex(m_waveBrowser) + (int) IconColours.AmberMetadataIcon;
                    break;
                case NodeDisplayState.AddedLocally:
                    iconIndex = GetBaseIconIndex(m_waveBrowser) + (int) IconColours.RedIcon;
                    break;
                case NodeDisplayState.AddedLocallyMetadata:
                    iconIndex = GetBaseIconIndex(m_waveBrowser) + (int) IconColours.RedMetadataIcon;
                    break;
                case NodeDisplayState.ChangedLocally:
                    iconIndex = GetBaseIconIndex(m_waveBrowser) + (int) IconColours.RedIcon;
                    break;
                case NodeDisplayState.ChangedLocallyMetadata:
                    iconIndex = GetBaseIconIndex(m_waveBrowser) + (int) IconColours.RedMetadataIcon;
                    break;
                case NodeDisplayState.NotPresent:
                    iconIndex = GetBaseIconIndex(m_waveBrowser) + (int) IconColours.GrayIcon;
                    break;
            }

            //wave icon doesn't change for locally locked
            if (m_lockedLocally && GetType() != typeof (WaveNode))
            {
                iconIndex += 4;
            }

            SelectedImageIndex = ImageIndex = iconIndex;

            if (m_activeState == NodeDisplayState.NotPresent)
            {
                ForeColor = Color.Gray;
            }
            else
            {
                ForeColor = Color.Black;
            }

            UpdateDisplayName();
        }

        public abstract string GetObjectPath();

        public abstract string GetTypeDescription();

        public abstract bool LockForEditing();

        public abstract void Unlock();

        public virtual string GetBuiltPath()
        {
            return string.Empty;
        }

        public bool IsLocked()
        {
            return m_lockedLocally;
        }

        public abstract bool TryToLock();

        public virtual bool IsNodeTypeLockable()
        {
            return true;
        }

        #region Nested type: IconColours

        private enum IconColours
        {
            GreenIcon = 0,
            AmberIcon,
            RedIcon,
            GrayIcon,
            GreenMetadataIcon,
            AmberMetadataIcon,
            RedMetadataIcon,
        }

        #endregion
    }
}