using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace Rave.Plugins
{
    public interface IRAVEEditorPlugin: IRAVEPlugin
    {
       
        void EditNodes(TreeView m_TreeView, IRemoteControl m_RemoteControl);

    }
}
