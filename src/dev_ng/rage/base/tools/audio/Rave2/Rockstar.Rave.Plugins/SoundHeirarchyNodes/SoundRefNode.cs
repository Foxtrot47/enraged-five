namespace Rave.Plugins.SoundHierarchyNodes
{
    /// <summary>
    ///   Summary description for SoundRefNode.
    /// </summary>
    public class SoundRefNode : SoundHeirarchyNode
    {
        public SoundRefNode(ObjectRef soundRef) : base(soundRef.ReferencedObject)
        {
            SoundReference = soundRef;
        }

        public ObjectRef SoundReference { get; private set; }
    }
}