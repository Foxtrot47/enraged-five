using System;
using System.Collections.Generic;
using System.Text;
using Rave.Plugins;
using Rave.Plugins.WaveBrowser.Nodes;
using System.Windows.Forms;
using System.Collections;

namespace Rave.Plugins
{
    public class ActionParams
    {
        private EditorTreeNode m_Node;
        private EditorTreeNode m_ParentNode;
        private ScrollableTreeView m_TreeView;
        private IActionLog m_ActionLog;
        private IWaveBrowser m_WaveBrowser;
        private string m_Name;
        private Dictionary<string,string> m_Value;
        private List<string> m_Platforms;
        private string m_WavePath;
        private ArrayList m_Actions;

        public ActionParams(IWaveBrowser waveBrowser, EditorTreeNode parentNode, EditorTreeNode node, string wavePath, ScrollableTreeView treeview, IActionLog actionLog,
                            string name, List<string> platforms, Dictionary<string,string> value, ArrayList actions)
        {
            m_WaveBrowser = waveBrowser;
            m_Node = node;
            m_ParentNode = parentNode;
            m_TreeView = treeview;
            m_ActionLog = actionLog;
            m_Name = name;
            m_Value = value;
            m_Platforms = platforms;
            m_WavePath = wavePath;
            m_Actions = actions;
        }


        public IWaveBrowser WaveBrowser
        {
            get { return m_WaveBrowser; }
        }
        public EditorTreeNode Node
        {
            get { return m_Node; }
        }

        public EditorTreeNode ParentNode
        {
            get { return m_ParentNode; }
        }

        public ScrollableTreeView TreeView
        {
            get { return m_TreeView; }
        }

        public IActionLog ActionLog
        {
            get { return m_ActionLog; }
        }

        public string Name
        {
            get { return m_Name; }
        }
        public Dictionary<string,string> Value
        {
            get { return m_Value; }
        }
        public List<string> Platforms
        {
            get { return m_Platforms; }
        }
        public string WavePath
        {
            get { return m_WavePath; }
        }
        public ArrayList Actions
        {
            get { return m_Actions; }
        }
    }
}
