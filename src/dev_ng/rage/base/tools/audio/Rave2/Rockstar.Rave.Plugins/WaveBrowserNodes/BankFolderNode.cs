using System;

namespace Rave.Plugins.WaveBrowser.Nodes
{
    /// <summary>
    ///   Summary description for WaveFolderNode.
    /// </summary>
    public class BankFolderNode : PackNode
    {
        private const int BASE_IMAGE_INDEX = 23;

        public BankFolderNode(IActionLog actionLog, string strDisplayName, IWaveBrowser waveBrowser)
            : base(actionLog, strDisplayName, waveBrowser)
        {
        }

        public override string GetObjectPath()
        {
            return ((PackNode) Parent).GetObjectPath() + "\\" + GetObjectName();
        }

        public override int GetBaseIconIndex(IWaveBrowser waveBrowser)
        {
            return BASE_IMAGE_INDEX;
        }

        public override bool TryToLock()
        {
            var pn = FindParentPackNode(this);
            return pn.TryToLock();
        }

        public override bool LockForEditing()
        {
            var pn = FindParentPackNode(this);
            if (pn != null)
            {
                return pn.LockForEditing();
            }
            return false;
        }

        public override void Unlock()
        {
            if (m_lockedLocally)
            {
                try
                {
                    AssetManager.UnlockWavePath(GetObjectPath());
                    m_lockedLocally = false;
                    UpdateDisplay();
                }
                catch (Exception e)
                {
                    ErrorManager.HandleError(e);
                }
            }
        }

        public override string GetTypeDescription()
        {
            return "Bank Folder";
        }
    }
}