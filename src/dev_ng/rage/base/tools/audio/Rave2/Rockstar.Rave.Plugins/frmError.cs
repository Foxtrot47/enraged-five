using System;
using System.Text;
using System.Windows.Forms;

namespace Rave.Plugins
{
    public partial class frmError : Form
    {
        private const string SHOW = "Show Details";
        private const string HIDE = "Hide Details";

        private readonly string m_details;
        private readonly string m_message;

        public frmError(string message, string stackTrace)
        {
            m_message = message;
            m_details = stackTrace;

            InitializeComponent();
            txtMsg.Text = m_message;
            lblShowDetails.Text = SHOW;
            if (string.IsNullOrEmpty(m_details))
            {
                lblShowDetails.Visible = true;
            }
            Height -= txtDetails.Height;
        }

        private void lblShowDetails_Click(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (lblShowDetails.Text == SHOW)
            {
                txtDetails.Visible = true;
                txtDetails.Text = m_details;
                lblShowDetails.Text = HIDE;
                Height += 100;
                txtDetails.Height = 100;
            }
            else
            {
                txtDetails.Visible = false;
                lblShowDetails.Text = SHOW;
                Height -= txtDetails.Height;
            }
        }

        private void btCopy_Click(object sender, EventArgs e)
        {
            var sb = new StringBuilder(m_message);
            if (m_details != "")
            {
                sb.Append(" ");
                sb.Append(m_details);
            }
            Clipboard.SetText(sb.ToString());
        }
    }
}