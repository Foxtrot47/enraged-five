namespace Rave.Plugins
{
    public interface IActionLog
    {
        void AddAction(IUserAction action);

        void Reset();

        IUserAction GetUndoAction();

        IUserAction GetRedoAction();

        void UndoLastAction();

        void RedoNextAction();

        bool Commit();

        string GetCommitSummary();

        int GetCommitLogCount();

        IUserAction SearchForAction(string wavePath);

        IUserAction SearchForWaveDeletion(string wavePath);

        IUserAction SearchForWaveAdd(string wavePath);

        IUserAction SearchForBankDeletion(string bankPath);

        IUserAction SearchForBankAdd(string bankPath);

        IUserAction SearchForPackDeletion(string packPath);

        IUserAction SearchForPackAdd(string packPath);
    }
}