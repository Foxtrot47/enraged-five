using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;

namespace Rave.Plugins.WaveBrowser.Nodes
{
    /// <summary>
    ///   Summary description for WaveNode.
    /// </summary>
    public class WaveNode : EditorTreeNode
    {
        private const int BASE_IMAGE_INDEX = 16;
        private readonly Dictionary<string, XmlNode> m_xmlNodes;

        public WaveNode(IActionLog actionLog, string strDisplayName, IWaveBrowser waveBrowser)
            : base(actionLog, strDisplayName, waveBrowser)
        {
            m_xmlNodes = new Dictionary<string, XmlNode>();
            IsMidi = Path.GetExtension(GetObjectName().ToUpper()) == ".MID";
        }

        public bool IsMidi { get; private set; }

        public override string GetObjectPath()
        {
            return ((BankNode) Parent).GetObjectPath() + "\\" + GetObjectName();
        }

        public override string ToString()
        {
            return GetObjectPath();
        }

        public string GetBankPath()
        {
            return FindParentBankNode(this).GetBankPath();
        }

        public string GetWaveName()
        {
            return Utilities.FormatGameString(Path.GetFileNameWithoutExtension(GetObjectName()));
        }

        public override int GetBaseIconIndex(IWaveBrowser waveBrowser)
        {
            return BASE_IMAGE_INDEX;
        }

        public override bool TryToLock()
        {
            var bn = FindParentBankNode(this);
            return bn.TryToLock();
        }

        public override bool LockForEditing()
        {
            // search up for a bank and lock that
            var n = FindParentBankNode(this);
            if (n != null)
            {
                return n.LockForEditing();
            }
            return false;
        }

        public override void Unlock()
        {
            //nothing to do here
            return;
        }

        public override string GetBuiltPath()
        {
            var parentPath = (Parent as EditorTreeNode).GetBuiltPath();
            if (!string.IsNullOrEmpty(parentPath))
            {
                return parentPath + "\\" + Path.GetFileNameWithoutExtension(GetObjectName());
            }
            return Path.GetFileNameWithoutExtension(GetObjectName());
        }

        public override bool IsNodeTypeLockable()
        {
            return false;
        }

        public override string GetTypeDescription()
        {
            return "Wave";
        }

        public void SetPlatformXmlNode(string platform, XmlNode node)
        {
            if (m_xmlNodes.ContainsKey(platform))
            {
                m_xmlNodes[platform] = node;
            }
            else
            {
                m_xmlNodes.Add(platform, node);
            }
        }

        public XmlNode GetXmlNode(string platform)
        {
            if (m_xmlNodes.ContainsKey(platform))
            {
                return m_xmlNodes[platform];
            }
            return null;
        }
    }
}