using System.Collections;
using System.Windows.Forms;

namespace Rave.Plugins.SoundBrowser.Nodes
{
    /// <summary>
    ///   Summary description for FolderNode.
    /// </summary>
    public class FolderNode : BaseNode
    {
        private const int BASE_IMAGE_INDEX = 0;
        private readonly string m_fullPath;

        public FolderNode(string displayName, string fullPath, string episode, string type)
            : base(displayName, episode, type)
        {
            m_fullPath = fullPath;
            SelectedImageIndex = ImageIndex = BASE_IMAGE_INDEX;
        }

        public string GetFullPath()
        {
            return m_fullPath;
        }

        public override int GetBaseIconIndex()
        {
            return BASE_IMAGE_INDEX;
        }

        public override string GetTypeDescription()
        {
            return "Folder";
        }

        public static void GetBanks(FolderNode folder, ArrayList banks)
        {
            foreach (TreeNode tn in folder.Nodes)
            {
                var sbn = tn as SoundBankNode;
                var fn = tn as FolderNode;
                if (sbn != null)
                {
                    banks.Add(sbn);
                }
                if (fn != null)
                {
                    GetBanks(fn, banks);
                }
            }
        }
    }
}