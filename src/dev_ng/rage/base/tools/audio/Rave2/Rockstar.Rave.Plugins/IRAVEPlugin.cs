using System.Xml;

namespace Rave.Plugins
{
    public interface IRAVEPlugin
    {
        string GetName();

        bool Init(XmlNode settings);
    }
}