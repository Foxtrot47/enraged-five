namespace Rave.Plugins
{
    public interface IWaveBrowser
    {
        IUserAction CreateAction(Utilities.ActionType actionType, ActionParams paramaters);

        IWaveEditorTreeView GetEditorTreeView();

        IPendingWaveLists GetPendingWaveLists();

        IActionLog GetActionLog();

        string GetActivePlatform();

        bool CommitChanges(bool showDialogue);
    }
}