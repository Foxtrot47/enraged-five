using Rave.Plugins.WaveBrowser.Nodes;

namespace Rave.Plugins
{
    public interface IRAVEWaveImportPlugin : IRAVEPlugin
    {
        void HandleDroppedFiles(IWaveBrowser waveBrowser,
                                EditorTreeNode selectedNode,
                                IActionLog actionLog,
                                string[] files);
    }
}