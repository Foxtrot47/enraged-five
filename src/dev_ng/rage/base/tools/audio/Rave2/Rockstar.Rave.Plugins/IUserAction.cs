using System;
using System.IO;

namespace Rave.Plugins
{
	/// <summary>
	/// Summary description for UserAction.
	/// </summary>
	public interface IUserAction
	{
		// fully commit the action
		// PARAMS
		//	If localOnly is true then assets won't be checked into the asset mgr etc
		bool Commit();
		
		// reset the UI state to undo this action 
		bool Undo();
		// update the UI state to show the action has been performed
		bool Action();

		// return a user-friendly summary string
		string GetSummary();

        // returns the asset path that this action affects
        string GetAssetPath();
	}
}
