using System;
using System.Collections.Generic;
using System.Text;

using Rave.Plugins.WaveBrowser.Nodes;
using System.Collections;

namespace Rave.Plugins
{
    
    public interface IRAVEWaveBrowserPlugin:IRAVEPlugin
    {
        void Process(IWaveBrowser waveBrowser, IActionLog actionLog, ArrayList nodes,IBusy busyFrm);

    }
}
