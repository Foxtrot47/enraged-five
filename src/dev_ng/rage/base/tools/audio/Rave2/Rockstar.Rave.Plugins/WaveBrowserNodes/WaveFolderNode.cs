using System;

namespace Rave.Plugins.WaveBrowser.Nodes
{
    /// <summary>
    ///   Summary description for WaveFolderNode.
    /// </summary>
    public class WaveFolderNode : BankNode
    {
        private const int BASE_IMAGE_INDEX = 23;

        public WaveFolderNode(IActionLog actionLog, string strDisplayName, IWaveBrowser waveBrowser)
            : base(actionLog, strDisplayName, waveBrowser)
        {
        }

        public override string GetObjectPath()
        {
            return ((BankNode) Parent).GetObjectPath() + "\\" + GetObjectName();
        }

        public override int GetBaseIconIndex(IWaveBrowser waveBrowser)
        {
            return BASE_IMAGE_INDEX;
        }

        public override bool TryToLock()
        {
            var bn = FindParentBankNode(this);
            return bn.TryToLock();
        }

        public override bool LockForEditing()
        {
            var bn = FindParentBankNode(this);
            if (bn != null)
            {
                return bn.LockForEditing();
            }
            return false;
        }

        public override void Unlock()
        {
            if (m_lockedLocally)
            {
                try
                {
                    AssetManager.UnlockWavePath(GetObjectPath());
                    m_lockedLocally = false;
                    UpdateDisplay();
                }
                catch (Exception e)
                {
                    ErrorManager.HandleError(e);
                }
            }
        }

        public override string GetTypeDescription()
        {
            return "Wave Folder";
        }
    }
}