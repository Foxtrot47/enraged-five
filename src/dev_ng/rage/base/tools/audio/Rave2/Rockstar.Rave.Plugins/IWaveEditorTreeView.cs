using System.Collections;
using System.Windows.Forms;
using Rave.Plugins.WaveBrowser.Nodes;

namespace Rave.Plugins
{
    public interface IWaveEditorTreeView
    {
        int GetIconIndex(string objName);

        void SetText(string text);

        void WavesContainString(string str, ArrayList results);

        void WavesInFolder(string folderName, ArrayList results);

        void SpeechToBeProcessed(string str, ArrayList results);

        void SpeechToBeProcessed_OutOfDate(string str, ArrayList results);

        void GetSelectedWaveNodes(TreeNode n, bool skipMetada, ArrayList nodeList, bool speechOnly = false);

        void FindNode(string path, string wave);

        void AuditionWave(string path, string wave);

        ScrollableTreeView GetTreeView();

        TreeNodeCollection GetNodes();

        void DeleteNode(EditorTreeNode node);

        bool LoadWaveLists();
    }
}