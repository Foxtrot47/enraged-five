using System;
using System.Collections.Generic;
using System.Text;

using System.Windows.Forms;
using System.Collections;

namespace Rave.Plugins
{

    public interface IRAVESpeechSearchPlugin : IRAVEPlugin
    {
        void Process(IWaveBrowser waveBrowser, IActionLog actionLog, ArrayList nodes, IBusy busyFrm);

    }
}
