using System.Collections.Generic;

namespace Rave.Plugins.WaveBrowser.Nodes
{
    /// <summary>
    ///   Summary description for TagNode.
    /// </summary>
    public class PresetNode : EditorTreeNode
    {
        private readonly Dictionary<string, string> m_platformValues;
        private readonly string m_presetName;

        public PresetNode(IActionLog actionLog, string strDisplayName, IWaveBrowser waveBrowser)
            : base(actionLog, strDisplayName, waveBrowser)
        {
            m_presetName = strDisplayName;
            m_platformValues = new Dictionary<string, string>();
        }

        public Dictionary<string, string> Values
        {
            get { return m_platformValues; }
        }

        public string GetPresetName()
        {
            return m_presetName;
        }

        public string GetCurrentValue(string platform)
        {
            if (!m_platformValues.ContainsKey(platform))
            {
                return null;
            }
            return m_platformValues[platform];
        }

        public override string GetObjectPath()
        {
            return ((EditorTreeNode) Parent).GetObjectPath() + "\\" + m_presetName;
        }

        public override int GetBaseIconIndex(IWaveBrowser waveBrowser)
        {
            return 31;
        }

        public override bool LockForEditing()
        {
            return false;
        }

        public override bool TryToLock()
        {
            return false;
        }

        public override void Unlock()
        {
            //Nothing to do here
            return;
        }

        public override bool IsNodeTypeLockable()
        {
            return false;
        }

        public override string GetTypeDescription()
        {
            return "Preset";
        }
    }
}