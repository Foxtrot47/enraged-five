using System;
using System.Collections;
using System.IO;
using System.Diagnostics;
using System.Xml;
using System.Windows.Forms;
using Rave.Plugins;
using Rave.Plugins.WaveBrowser.Nodes;
using System.Collections.Generic;

namespace Rave.Plugins
{

	public class PendingWaveLists
	{
		private List<PendingWaveList> sm_WaveLists;
        private rage.audProjectSettings m_ProjectSettings;
        
        public List<PendingWaveList> WaveLists
        {
            get { return sm_WaveLists; }
        }

        public PendingWaveLists(rage.audProjectSettings projectSettings)
		{
            sm_WaveLists = new List<PendingWaveList>();
            m_ProjectSettings = projectSettings;

			foreach(rage.PlatformSetting platform in projectSettings.GetPlatformSettings())
			{
				sm_WaveLists.Add(new PendingWaveList(platform.BuildInfo,platform.PlatformTag));
			}
		}

		public bool LoadLatestWaveLists()
		{
			bool ret = true;
			foreach(PendingWaveList w in sm_WaveLists)
			{
				if(!w.GetLatestBuiltWaveListFromAssetManager() || !w.GetLatestPendingWaveListFromAssetManager())
				{
					ret = false;
				}
						
				w.LoadPendingWaveList();
				w.LoadBuiltWaveList();
			}

			return ret;
		}

		public bool LockPendingWaveLists()
		{
			bool ret = true;
			foreach(PendingWaveList w in sm_WaveLists)
			{
				if(!w.LockPendingWaveList())
				{
					ret = false;
				}
				// reload it to ensure we're working with the latest version
				w.LoadPendingWaveList();
			}
			return ret;
		}

		public bool UnlockPendingWaveLists()
		{
			bool ret = true;
			foreach(PendingWaveList w in sm_WaveLists)
			{
				if(!w.UnlockPendingWaveList())
				{
					ret = false;
				}
			}
			return ret;
		}

		public bool UndoLockPendingWaveLists()
		{
			return AssetManager.GetAssetManager().DeleteCurrentChangelist();
        }

		public bool SerialiseWaveLists()
		{
			bool success = true;
			foreach(PendingWaveList w in sm_WaveLists)
			{
				if(!w.SerialiseWaveList())
				{
					success = false;
					break;
				}
			}
		
			if(success)
			{
				// check in all wave lists
				foreach(PendingWaveList w in sm_WaveLists)
				{
					w.ClearLocalStore();
					w.UnlockPendingWaveList();
				}
			}
			else
			{
				// undo all wave list check outs
				foreach(PendingWaveList w in sm_WaveLists)
				{
					w.UndoLockPendingWaveList();
				}				
			}

			return success;
		}

		public void RecordAddWave(string wavePath)
		{
			foreach(PendingWaveList w in sm_WaveLists)
			{
				w.RecordAddWave(wavePath);
			}
		}

		public void RecordAddPack(string packPath)
		{
			foreach(PendingWaveList w in sm_WaveLists)
			{
				w.RecordAddPack(packPath);
			}
		}

		public void RecordAddBank(string bankPath)
		{
			foreach(PendingWaveList w in sm_WaveLists)
			{
				w.RecordAddBank(bankPath);
			}
		}

		public void RecordAddBankFolder(string bankFolderPath)
		{
			foreach(PendingWaveList w in sm_WaveLists)
			{
				w.RecordAddBankFolder(bankFolderPath);
			}
		}

		public void RecordAddWaveFolder(string waveFolderPath)
		{
			foreach(PendingWaveList w in sm_WaveLists)
			{
				w.RecordAddWaveFolder(waveFolderPath);
			}
		}

		public void RecordDeleteWave(string wavePath)
		{
			foreach(PendingWaveList w in sm_WaveLists)
			{
				w.RecordDeleteWave(wavePath);
			}
		}

		public void RecordDeleteWaveFolder(string waveFolderPath)
		{
			foreach(PendingWaveList w in sm_WaveLists)
			{
				w.RecordDeleteWaveFolder(waveFolderPath);
			}
		}

		public void RecordDeleteBank(string bankPath)
		{
			foreach(PendingWaveList w in sm_WaveLists)
			{
				w.RecordDeleteBank(bankPath);
			}
		}	

		public void RecordDeleteBankFolder(string bankFolderPath)
		{
			foreach(PendingWaveList w in sm_WaveLists)
			{
				w.RecordDeleteBankFolder(bankFolderPath);
			}
		}

		public void RecordDeletePack(string packPath)
		{
			foreach(PendingWaveList w in sm_WaveLists)
			{
				w.RecordDeletePack(packPath);
			}
		}

		public void RecordModifyWave(string wavePath)
		{
			foreach(PendingWaveList w in sm_WaveLists)
			{
				w.RecordModifyWave(wavePath);
			}
		}

		public void RecordDeleteTag(string tagPath, string tagPlatform)
		{
			// find the wave list for the platform
			for(int i = 0 ; i < sm_WaveLists.Count; i++)
			{
				if(tagPlatform == null || sm_WaveLists[i].Platform == tagPlatform)
				{
					sm_WaveLists[i].RecordDeleteTag(tagPath);
				}
			}
		}


		public void RecordModifyTag(string tagPath, string tagPlatform, string val)
		{
			// find the wave list for the platform
			for(int i = 0 ; i < sm_WaveLists.Count; i++)
			{
				if(tagPlatform == null || sm_WaveLists[i].Platform == tagPlatform)
				{
					sm_WaveLists[i].RecordModifyTag(tagPath, val);
				}
			}
		}

		public void RecordAddTag(string tagPath, string tagPlatform, string val)
		{
			// find the wave list for the platform
			for(int i = 0 ; i < sm_WaveLists.Count; i++)
			{
				if(tagPlatform == null || sm_WaveLists[i].Platform == tagPlatform)
				{
					sm_WaveLists[i].RecordAddTag(tagPath, val);
				}
			}
		}

		public string GetTagValue(string tagPath, string tagPlatform)
		{
			// find the wave list for the platform
			for(int i = 0 ; i < sm_WaveLists.Count; i++)
			{
				if(tagPlatform == null || sm_WaveLists[i].Platform == tagPlatform)
				{
					return sm_WaveLists[i].GetTagValue(tagPath);
				}
			}
			return null;
		}
		


        #region IPendingWaveLists Members


        public void RecordAddPackFolder(string packPath)
        {
            foreach (PendingWaveList w in sm_WaveLists)
            {
                w.RecordAddPackFolder(packPath);
            }
        }

        public void RecordDeletePackFolder(string packPath)
        {
            foreach (PendingWaveList w in sm_WaveLists)
            {
                w.RecordDeletePackFolder(packPath);
            }
        }

        #endregion
    }
	/// <summary>
	/// Summary description for PendingWaveList.
	/// </summary>
	public class PendingWaveList
	{
		private MetadataStore m_LocalStore, m_PendingWaves, m_BuiltWaves;
		private string m_PendingWaveListAssetPath, m_PendingWaveListWorkingPath, m_BuiltWaveListAssetPath, m_BuiltWaveListWorkingPath;
        private string m_BuildInfoAssetPath;
        private string m_Platform;

        public MetadataStore PendingStore
        {
            get { return m_PendingWaves; }
        }

        public string Platform
        {
            get { return m_Platform; }
        }

        public MetadataStore BuiltStore
        {
            get { return m_BuiltWaves; }
        }
	
		public PendingWaveList(string buildInfoAssetPath,string platform)
		{
            m_Platform = platform;
            m_BuildInfoAssetPath = buildInfoAssetPath;
			m_LocalStore = new MetadataStore();
            m_PendingWaves = new MetadataStore();
            m_BuiltWaves = new MetadataStore();

			m_PendingWaveListAssetPath = buildInfoAssetPath + "\\PendingWaves.xml";
			m_BuiltWaveListAssetPath = buildInfoAssetPath + "\\BuiltWaves.xml";

			m_PendingWaveListWorkingPath = AssetManager.GetAssetManager().GetWorkingPath(m_PendingWaveListAssetPath);
			m_BuiltWaveListWorkingPath = AssetManager.GetAssetManager().GetWorkingPath(m_BuiltWaveListAssetPath);
		}

		public void LoadPendingWaveList()
		{
            try
            {
                m_PendingWaves.LoadXml(m_PendingWaveListWorkingPath);
            }
            catch (DirectoryNotFoundException)
            {
                if (DialogResult.Yes == MessageBox.Show(Rave.Plugins.Utilities.ActiveForm, m_PendingWaveListWorkingPath + " does not exist.  Create it?", "Directory not found", MessageBoxButtons.YesNo, MessageBoxIcon.Information))
                {
                    System.IO.Directory.CreateDirectory(AssetManager.GetAssetManager().GetWorkingPath("") + m_BuildInfoAssetPath);
                    m_PendingWaves.CreateRoot("PendingWaves");
                    m_PendingWaves.SaveXml(m_PendingWaveListWorkingPath);
                    AssetManager.GetAssetManager().Import("", m_PendingWaveListWorkingPath, "initial import of pending wavelist");
                }
            }
            catch (FileNotFoundException)
            {
                if (DialogResult.Yes == MessageBox.Show(Rave.Plugins.Utilities.ActiveForm, m_PendingWaveListWorkingPath + " does not exist.  Create it?", "Directory not found", MessageBoxButtons.YesNo, MessageBoxIcon.Information))
                {
                    m_PendingWaves.CreateRoot("PendingWaves");
                    m_PendingWaves.SaveXml(m_PendingWaveListWorkingPath);
                    AssetManager.GetAssetManager().Import("", m_PendingWaveListWorkingPath, "initial import of pending wavelist");
                }
            }
		}

		public void LoadBuiltWaveList()
		{
            try
            {
                m_BuiltWaves.LoadXml(m_BuiltWaveListWorkingPath);
            }
            catch (DirectoryNotFoundException)
            {
                if (DialogResult.Yes == MessageBox.Show(Rave.Plugins.Utilities.ActiveForm, m_BuiltWaveListWorkingPath + " does not exist.  Create it?", "Directory not found", MessageBoxButtons.YesNo, MessageBoxIcon.Information))
                {
                    System.IO.Directory.CreateDirectory(AssetManager.GetAssetManager().GetWorkingPath("") + m_BuildInfoAssetPath);
                    m_BuiltWaves.CreateRoot("BuiltWaves");
                    m_BuiltWaves.SaveXml(m_BuiltWaveListWorkingPath);
                    AssetManager.GetAssetManager().Import("", m_BuiltWaveListWorkingPath, "initial import of pending wavelist");
                }
            }
            catch (FileNotFoundException)
            {
                if (DialogResult.Yes == MessageBox.Show(Rave.Plugins.Utilities.ActiveForm, m_BuiltWaveListWorkingPath + " does not exist.  Create it?", "Directory not found", MessageBoxButtons.YesNo, MessageBoxIcon.Information))
                {
                    m_BuiltWaves.CreateRoot("BuiltWaves");
                    m_BuiltWaves.SaveXml(m_BuiltWaveListWorkingPath);
                    AssetManager.GetAssetManager().Import("", m_BuiltWaveListWorkingPath, "initial import of pending wavelist");
                }
            }
		}


		public bool LockPendingWaveList()
		{
			
			bool ret = AssetManager.GetAssetManager().SimpleCheckOut(m_PendingWaveListAssetPath, "Wave Editor committing");
			LoadPendingWaveList();
			return ret;
		}

		public bool UnlockPendingWaveList()
		{
			
		    return AssetManager.GetAssetManager().CheckInOrImport(m_PendingWaveListWorkingPath, m_PendingWaveListAssetPath, "WaveEditor commit");
			
		}

		public bool UndoLockPendingWaveList()
		{
			return AssetManager.GetAssetManager().UndoCheckOut(m_PendingWaveListAssetPath);
		}

		public bool SerialiseWaveList()
		{
			m_PendingWaves.Combine(m_LocalStore);
			return m_PendingWaves.SaveXml(m_PendingWaveListWorkingPath);
		}

		public void ClearLocalStore()
		{
            m_LocalStore = new MetadataStore();
		}

		// PURPOSE: updates the local pending wave list from the server
		public bool GetLatestPendingWaveListFromAssetManager()
		{
			return AssetManager.GetAssetManager().GetLatest(m_PendingWaveListAssetPath);
		}

		// PURPOSE: updates the local built wave list from the server
		public bool GetLatestBuiltWaveListFromAssetManager()
		{
			return AssetManager.GetAssetManager().GetLatest(m_BuiltWaveListAssetPath);
		}

		public bool HasPendingWaveListChanged()
		{
			// check to see if the wave list has been modified on the server
			if(!AssetManager.GetAssetManager().IsUpToDate(m_PendingWaveListAssetPath))
			{
				return true;
			}
			return false;
		}

		public bool HasBuiltWaveListChanged()
		{
			if(!AssetManager.GetAssetManager().IsUpToDate(m_BuiltWaveListAssetPath))
			{
				return true;
			}
			return false;
		}

		public void RecordAddWave(string wavePath)
		{
			m_LocalStore.RecordAddedWave(wavePath, new MetadataStore[] {m_PendingWaves, m_BuiltWaves});
		}

        public void RecordAddPackFolder(string packFolderPath)
        {
            m_LocalStore.RecordAddPackFolder(packFolderPath, new MetadataStore[] { m_PendingWaves, m_BuiltWaves });
        }

		public void RecordAddPack(string packPath)
		{
			m_LocalStore.RecordAddedPack(packPath, new MetadataStore[] {m_PendingWaves, m_BuiltWaves});
		}

		public void RecordAddBank(string bankPath)
		{
			m_LocalStore.RecordAddedBank(bankPath, new MetadataStore[] {m_PendingWaves, m_BuiltWaves});
		}

		public void RecordAddBankFolder(string bankFolderPath)
		{
			m_LocalStore.RecordAddedBankFolder(bankFolderPath, new MetadataStore[] {m_PendingWaves, m_BuiltWaves});
		}

		public void RecordAddWaveFolder(string waveFolderPath)
		{
			m_LocalStore.RecordAddedWaveFolder(waveFolderPath, new MetadataStore[] {m_PendingWaves, m_BuiltWaves});
		}

		public void RecordDeleteWave(string wavePath)
		{
			m_LocalStore.RecordRemovedWave(wavePath, new MetadataStore[] {m_PendingWaves, m_BuiltWaves});
		}

		public void RecordDeleteWaveFolder(string waveFolderPath)
		{
			m_LocalStore.RecordRemovedWaveFolder(waveFolderPath, new MetadataStore[] {m_PendingWaves, m_BuiltWaves});
		}

		public void RecordDeleteBank(string bankPath)
		{
			m_LocalStore.RecordRemovedBank(bankPath, new MetadataStore[] {m_PendingWaves, m_BuiltWaves});
		}	

		public void RecordDeleteBankFolder(string bankFolderPath)
		{
			m_LocalStore.RecordRemovedBankFolder(bankFolderPath, new MetadataStore[] {m_PendingWaves, m_BuiltWaves});
		}

        public void RecordDeletePackFolder(string packFolderPath)
        {
            m_LocalStore.RecordRemovedPackFolder(packFolderPath, new MetadataStore[] { m_PendingWaves, m_BuiltWaves });
        }

		public void RecordDeletePack(string packPath)
		{
			m_LocalStore.RecordRemovedPack(packPath, new MetadataStore[] {m_PendingWaves, m_BuiltWaves});
		}

		public void RecordModifyWave(string wavePath)
		{
			m_LocalStore.RecordModifiedWave(wavePath, new MetadataStore[] {m_PendingWaves, m_BuiltWaves});
		}

		public void RecordDeleteTag(string tagPath)
		{
			m_LocalStore.RecordRemovedTag(tagPath, new MetadataStore[] {m_PendingWaves, m_BuiltWaves});
		}

		public void RecordModifyTag(string tagPath, string tagValue)
		{
			m_LocalStore.RecordModifiedTag(tagPath, tagValue,  new MetadataStore[] {m_PendingWaves, m_BuiltWaves});
		}

		public void RecordAddTag(string tagPath, string tagValue)
		{
			m_LocalStore.RecordAddedTag(tagPath, tagValue, new MetadataStore[] {m_PendingWaves, m_BuiltWaves});
		}

		public string GetTagValue(string tagPath)
		{
			string val;

			val = m_LocalStore.FindTagValue(tagPath);

			if(val == null)
			{
				// try pending wave list
				val = m_PendingWaves.FindTagValue(tagPath);
			}
			if(val == null)
			{
				// try built waves
				val = m_BuiltWaves.FindTagValue(tagPath);
			}

			return val;
		}
		
	}
}
