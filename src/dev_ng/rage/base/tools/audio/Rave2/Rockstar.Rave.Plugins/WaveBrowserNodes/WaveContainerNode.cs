using System.Collections;

namespace Rave.Plugins.WaveBrowser.Nodes
{
    /// <summary>
    ///   Summary description for PackNode.
    /// </summary>
    public abstract class WaveContainerNode : EditorTreeNode
    {
        protected WaveContainerNode(IActionLog actionLog, string strDisplayName, IWaveBrowser waveBrowser)
            : base(actionLog, strDisplayName, waveBrowser)
        {
        }

        public ArrayList GetChildWaveNodes()
        {
            var al = new ArrayList();
            foreach (EditorTreeNode n in Nodes)
            {
                var container = n as WaveContainerNode;
                if (container != null)
                {
                    var childWaves = container.GetChildWaveNodes();
                    foreach (WaveNode w in childWaves)
                    {
                        if (!al.Contains(w))
                        {
                            al.Add(w);
                        }
                    }
                }
                else
                {
                    if (n.GetType() ==
                        typeof (WaveNode))
                    {
                        al.Add(n);
                    }
                }
            }
            return al;
        }
    }
}