using System;
using System.Collections.Generic;
using System.Xml;
using Rave.Plugins.WaveBrowser.Nodes;

namespace Rave.Plugins
{
    public struct ObjectLookupID
    {
        public const string TEST = "TEST";
        public const string BASE = "BASE";

        public string Episode;
        public string Type;

        public ObjectLookupID(string type, string episode)
        {
            Type = type.ToUpper();
            Episode = episode.ToUpper();
        }

        public override int GetHashCode()
        {
            return (Type + Episode).GetHashCode();
        }
    }

    public class ObjectRef
    {
        public XmlNode ObjectRefXml { get; set; }
        public IObjectInstance ReferencedObject { get; set; }
    }

    public interface IObjectInstance
    {
        bool IsReadOnly { get; }
        IObjectBank ObjectBank { get; set; }
        List<ObjectRef> Children { get; }
        Dictionary<string, IObjectInstance> ObjectLookup { get; }
        string Name { get; }
        string VirtualFolderName { get; set; }
        string TypeName { get; }
        List<IObjectInstance> ReferencingSounds { get; }
        XmlNode Node { get; set; }
        string Type { get; set; }
        TypeDefinition TypeDefinition { get; }
        string Episode { get; set; }
        IList<string> GlobalVariables { get; }
        TypeDefinitions TypeDefinitions { get; }
        event Action OnObjectChildrenChanged;

        void Delete();

        void RemoveReferencingSound(IObjectInstance sound);

        void MarkAsDirty();

        void FindChildSounds();

        bool CanAddChild(IObjectInstance soundRef);

        int FindChild(IObjectInstance sound, XmlNode node);

        void RemoveChild(IObjectInstance sound, XmlNode node);

        void AddChildAt(IObjectInstance sound, XmlNode node, int index);

        void AddChild(IObjectInstance sound, XmlNode node);

        void AddReferencingSound(IObjectInstance parent);

        string[] FindDefinedVariables();

        List<string> GetSoundVariables();

        bool HandleDroppedObject(IObjectInstance s, TypeDefinitions globalraveTypeDefinitions);

        List<IObjectInstance> GetFlatListOfChildren();

        void HandleDroppedWave(WaveNode w);

        void Rename(string newName);

        void RemoveBankDelegate();
    }
}