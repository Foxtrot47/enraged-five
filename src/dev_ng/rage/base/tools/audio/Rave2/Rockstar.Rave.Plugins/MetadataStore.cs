using System;
using System.Xml;
using System.Windows.Forms;
using System.Collections;
using Rave.Plugins.WaveBrowser.Nodes;

namespace Rave.Plugins
{
	/// <summary>
	/// Summary description for MetadataStore.
	/// </summary>
	public class MetadataStore
	{
		private XmlDocument m_Document;
        private IWaveBrowser m_WaveBroswer;

        public XmlDocument Document
        {
            get { return m_Document; }
        }

		public MetadataStore()
		{
            
			m_Document = new XmlDocument();
			// ensure we have a doc elem
			m_Document.AppendChild(m_Document.CreateElement("LocalWaveChanges"));
		}

		public bool LoadXml(string path)
		{
			bool ret = false;
            try
            {
                m_Document.Load(path);
                ret = true;
            }
            catch
            {
    	        throw;
            }
			return ret;
		}

		public bool SaveXml(string path)
		{
			bool ret = false;
			try
			{
				m_Document.Save(path);
				ret = true;
			}
			catch(Exception ex)
			{
				System.Diagnostics.Debug.WriteLine(ex.ToString());
			}

			return ret;
		}

        public void CreateRoot(string elementName)
        {
            if (m_Document.DocumentElement == null)
            {
                // Create the root element
                XmlElement rootNode = m_Document.CreateElement(elementName);
                m_Document.AppendChild(rootNode);
            }
        }

		// PURPOSE
		//	Updates metadata store, ensures that it contains the specified object of the specified type with
		//	the specified operation attribute value, pulling in nodes from the supplied metadatastores as
		//	required
		private XmlNode SetObjectOperation(string path, string type, string op, MetadataStore[] externalStores)
		{

			// we need to build up structure if it is missing from pending waves + built waves

			string[] elems = path.Split('\\');
			bool foundNode = false;
			XmlNode currentNode = m_Document.DocumentElement, parentNode = m_Document.DocumentElement;

			for(int i = 0 ; i < elems.Length - 1; i++)
			{
				foundNode = false;
				parentNode = currentNode;

				foreach(XmlNode n in currentNode.ChildNodes)
				{
					string name = n.Attributes["name"].Value;
					if(name == elems[i])
					{
						currentNode = n;
						foundNode = true;
						break;
					}
				}


				if(foundNode)
				{
					// node was found locally, we're good to continue to the next
					// element in the path
					continue;
				}
				else
				{
					// we don't have this node and we need it - see if its in the pending
					// wave list
					string tempPath = "";

					for(int j = 0 ; j <= i; j++)
					{
						tempPath += elems[j];
						if(j < i)
						{
							tempPath += "\\";
						}
					}

					foreach(MetadataStore ms in externalStores)
					{
						currentNode = ms.FindNodeFromPath(tempPath);
						if(currentNode != null)
						{
							break;
						}
					}

					if(currentNode == null)
					{
						// node doesnt exist anywhere - this is valid if it's being
						// added
						if(op != "add")
						{
							throw new Exception("Node doesn't exist but isn't being added");
						}
						break;
					}

					currentNode = m_Document.ImportNode(currentNode,true);
					
					// remove all children but retain all attributes other than operation					
					ArrayList nodesToRemove = new ArrayList();
					foreach(XmlNode childNode in currentNode.ChildNodes)
					{	
						nodesToRemove.Add(childNode);
					}

					// strip off operation attrib since we don't want to
					// merge with that until commit time
					if(currentNode.Attributes["operation"]!=null)
					{
						currentNode.Attributes.Remove(currentNode.Attributes["operation"]);
					}
						
					

					foreach(XmlNode childNode in nodesToRemove)
					{
						currentNode.RemoveChild(childNode);
					}

					// add to our local document
					parentNode.AppendChild(currentNode);
				}
			}

			if(currentNode == null)
			{
				throw new Exception("Couldn't find parent");
			}

			// search currentNode to see if the node we're after exists - 
			// if it does then we need to merge this operation, however note that
			// we're not searching pending/built wave lists for this node - that merge
			// will be done at commit time.
		
			foreach(XmlNode n in currentNode.ChildNodes)
			{
				XmlNode ret = n;
				if(n.Attributes["name"].Value == elems[elems.Length-1])
				{
					string curOp = (n.Attributes["operation"]!=null?n.Attributes["operation"].Value:null);

					if(curOp == null)
					{
						// this node was previously just pulled from built waves
						n.Attributes.Append(m_Document.CreateAttribute("operation"));
						n.Attributes["operation"].Value = op;
					}
					// merge operations
					else if(curOp == "add" && op == "remove")
					{
						// it was added then removed so we can remove the node
						currentNode.RemoveChild(n);
						// no point returning the obsolete node
						ret = null;
					}
					else if(op == "add" && curOp == "remove")
					{
						// it was removed then added so we need to change the add to modify
						n.Attributes["operation"].Value = "modify";
					}
					else if(curOp == "add" && op == "modify")
					{
						// it was added then modified - should remain added
						//n.Attributes["operation"].Value = "add";
					}
					else if(curOp == "modify" && op == "add")
					{
						throw new Exception("Tried to add a node that already existed (modified");
					}
					else if(curOp == "remove" && op == "modify")
					{
						throw new Exception("Tried to modified a node that was removed");
					}

					return ret;
				}
			}
		
			// node wasn't found - create a new one
			XmlAttribute attrib = m_Document.CreateAttribute("operation");
			attrib.Value = op;

			XmlElement elem = m_Document.CreateElement(type);
			elem.Attributes.Append(attrib);

			attrib = m_Document.CreateAttribute("name");
			attrib.Value = elems[elems.Length-1];
			elem.Attributes.Append(attrib);

			currentNode.AppendChild(elem);	
			return elem;
		}

		public string FindTagValue(string tagPath)
		{
			XmlNode node = FindNodeFromPath(tagPath);
			if(node == null || node.Name != "Tag")
			{
				return null;
			}
			else
			{
				return (node.Attributes["value"] != null ? node.Attributes["value"].Value : null);
			}
		}

		public void RecordAddedWave(string path, MetadataStore[] externalStores)
		{
			SetObjectOperation(path, "Wave", "add", externalStores);
		}

       
		public void RecordModifiedWave(string path, MetadataStore[] externalStores)
		{
			SetObjectOperation(path, "Wave", "modify", externalStores);
		}

        public void RecordAddPackFolder(string path, MetadataStore[] externalStores)
        {
            SetObjectOperation(path, "PackFolder", "add", externalStores);
        }

		public void RecordAddedPack(string path, MetadataStore[] externalStores)
		{
			SetObjectOperation(path, "Pack", "add", externalStores);
		}

		public void RecordAddedBank(string path, MetadataStore[] externalStores)
		{
			SetObjectOperation(path, "Bank", "add", externalStores);
		}

        
		public void RecordAddedBankFolder(string path, MetadataStore[] externalStores)
		{
			SetObjectOperation(path, "BankFolder", "add", externalStores);
		}

		public void RecordAddedWaveFolder(string path, MetadataStore[] externalStores)
		{
			SetObjectOperation(path, "WaveFolder", "add", externalStores);
		}

		public void RecordRemovedWave(string path, MetadataStore[] externalStores)
		{
			SetObjectOperation(path, "Wave", "remove", externalStores);
		}

		public void RecordRemovedWaveFolder(string path, MetadataStore[] externalStores)
		{
			SetObjectOperation(path, "WaveFolder", "remove", externalStores);
		}

		public void RecordRemovedBank(string path, MetadataStore[] externalStores)
		{
			SetObjectOperation(path, "Bank", "remove", externalStores);
		}

		public void RecordRemovedBankFolder(string path, MetadataStore[] externalStores)
		{
			SetObjectOperation(path, "BankFolder", "remove", externalStores);
		}

        public void RecordRemovedPackFolder(string path, MetadataStore[] externalStores)
        {
            SetObjectOperation(path, "PackFolder", "remove", externalStores);
        }

		public void RecordRemovedPack(string path, MetadataStore[] externalStores)
		{
			SetObjectOperation(path, "Pack", "remove", externalStores);
		}

		public void RecordAddedTag(string tagPath, string tagValue, MetadataStore[] externalStores)
		{
			XmlNode n = SetObjectOperation(tagPath, "Tag", "add", externalStores);

			if(tagValue != null)
			{
				// update value
				if(n.Attributes["value"] == null)
				{
					XmlAttribute attrib = m_Document.CreateAttribute("value");
					attrib.Value = tagValue;
					n.Attributes.Append(attrib);
				}
				else
				{
					n.Attributes["value"].Value = tagValue;
				}		
			}
		}

		public void RecordModifiedTag(string tagPath, string tagValue, MetadataStore[] externalStores)
		{
			// this ensures that it exists locally
			XmlNode n = SetObjectOperation(tagPath, "Tag", "modify", externalStores);
			
			// update value
			if(n.Attributes["value"] == null)
			{
				XmlAttribute attrib = m_Document.CreateAttribute("value");
				attrib.Value = tagValue;
				n.Attributes.Append(attrib);
			}
			else
			{
				n.Attributes["value"].Value = tagValue;
			}		
		}
		public void RecordRemovedTag(string tagPath, MetadataStore[] externalStores)
		{
			SetObjectOperation(tagPath, "Tag", "remove", externalStores);
		}

		

		// PURPOSE
		//	Combines this metadata store with the supplied store
		public void Combine(MetadataStore store)
		{
			//ResolveDuplicateNodes(store.m_Document.DocumentElement);
			Combine(m_Document.DocumentElement, store.m_Document.DocumentElement);
			StripEmptyElements(m_Document.DocumentElement);
		}

		private void ResolveDuplicateNodes(XmlNode parentNode)
		{
			ArrayList nodesToRemove = new ArrayList();
			bool foundDupe;
			do
			{
				foundDupe = false;

				// this loops from  the end of the list so that in the case where multiple nodes
				// exist we preserve the most recent - this is required if there are multiple
				// modifies for example.
				for(int i = parentNode.ChildNodes.Count-1; i >= 0; i--)
				{
					XmlNode node1 = parentNode.ChildNodes[i];
					if(!nodesToRemove.Contains(node1))
					{
						for(int j = i-1; j >= 0; j--)
						{
							XmlNode node2 = parentNode.ChildNodes[j];
							if(!nodesToRemove.Contains(node2))
							{
								if(node1.Attributes["name"].Value == node2.Attributes["name"].Value)
								{
									// duplicate node - resolve
									nodesToRemove.Add(node2);

									string op1, op2;

									op1 = (node1.Attributes["operation"]!=null?node1.Attributes["operation"].Value:null);
									op2 = (node2.Attributes["operation"]!=null?node2.Attributes["operation"].Value:null);

									if(op2 == "add" && op1 == "remove")
									{
										// it was added then removed so we can remove both nodes
										nodesToRemove.Add(node1);
									}
									else if(op1 == "add" && op2 == "remove")
									{
										// it was removed then added so we need to change the add to modify
										node1.Attributes["operation"].Value = "modify";
									}
									else if((op1 == "add" && op2 == "modify") || (op1 == "modify" && op2 == "add"))
									{
										node1.Attributes["operation"].Value = "add";
									}

									foundDupe = true;
								}
							}
						}
					}
				}

				foreach(XmlNode n in nodesToRemove)
				{
					parentNode.RemoveChild(n);
				}
				nodesToRemove.Clear();

			}while(foundDupe);

			foreach(XmlNode n in parentNode.ChildNodes)
			{
				ResolveDuplicateNodes(n);
			}
		}

		private void StripEmptyElements(XmlNode parent)
		{
			ArrayList nodesToDelete = new ArrayList();

			foreach(XmlNode child in parent.ChildNodes)
			{
				StripEmptyElements(child);

				if(child.ChildNodes.Count == 0)
				{
					if(child.Attributes["operation"] == null)
					{
						nodesToDelete.Add(child);
					}
				}
			}

			for(int i = 0; i < nodesToDelete.Count; i++)
			{
				parent.RemoveChild((XmlNode)nodesToDelete[i]);
			}
		}

		private void Combine(XmlNode pending, XmlNode local)
		{
			// ensure that everything in store2 with an attribute is copied over to store1,
			// resolving conflicts

			foreach(XmlNode ln in local.ChildNodes)
			{
				// iterate through local wave list
				XmlNode matchNode = null;
				string lname = ln.Attributes["name"].Value;
				
				foreach(XmlNode pn in pending.ChildNodes)
				{
					string pname = pn.Attributes["name"].Value;
				
					if(pname == lname)
					{
						matchNode = pn;
						break;
					}
				}

				if(matchNode == null)
				{
					// this node doesnt exist in the pending wave list, copy it from local
					matchNode = m_Document.ImportNode(ln, true);

					if(matchNode.Attributes["operation"] != null)
					{						
						pending.AppendChild(matchNode);
					}
					else
					{
						// this node has no attributes, just a hierarchy node so remove all children before importing
						ArrayList nodesToRemove = new ArrayList();
						foreach(XmlNode childNode in matchNode.ChildNodes)
						{
							if(childNode.GetType() != typeof(XmlAttribute))
							{
								nodesToRemove.Add(childNode);
							}
						}

						foreach(XmlNode childNode in nodesToRemove)
						{
							matchNode.RemoveChild(childNode);
						}

						pending.AppendChild(matchNode);	
					}
				}
				else
				{
					// this node exists in the pending wave list already,
					// need to combine operations
					string poper, loper;

					if(ln.Attributes["operation"] != null && matchNode.Attributes["operation"] != null)
					{
						// have operation in local and pending, need to decide which to use
						poper = matchNode.Attributes["operation"].Value;
						loper = ln.Attributes["operation"].Value;

						if(poper != loper)
						{
							switch(poper)
							{
								case "add":
								switch(loper)
								{
									case "remove":
										// these operations cancel each other out
										// this node's operation should be removed
										// nodes with no children and no operation are removed by StripEmptyElements()
										matchNode.Attributes.Remove(matchNode.Attributes["operation"]);
										break;
									case "modify":
										// need to copy all attributes across
										foreach(XmlAttribute a in ln.Attributes)
										{
											if(a.Name != "operation")
											{
												if(matchNode.Attributes[a.Name] == null)
												{
													XmlAttribute attrib = matchNode.OwnerDocument.CreateAttribute(a.Name);
													matchNode.Attributes.Append(attrib);
												}
												matchNode.Attributes[a.Name].Value = a.Value;
											}
										}
										break;
								}
								break;

								case "remove":
								switch(loper)
								{
									case "add":
										// removed in pending list, added locally
										// this node's operation should updated to modify
										matchNode.Attributes["operation"].Value = "modify";
										if(matchNode.Name == "Tag")
										{
											// its a tag so make sure we preserve the latest value
											if(ln.Attributes["value"] == null)
											{
												if(matchNode.Attributes["value"] != null)
												{
													matchNode.Attributes.Remove(matchNode.Attributes["value"]);
												}
											}
											else
											{
												if(matchNode.Attributes["value"] == null)
												{
													matchNode.Attributes.Append(matchNode.OwnerDocument.CreateAttribute("value"));
												}
												matchNode.Attributes["value"].Value = ln.Attributes["value"].Value;
											}
										}
										break;
									case "modify":
										// this shouldnt ever happen
										break;
								}
									break;
								case "modify":
								switch(loper)
								{
									case "add":
										// modified in pending, added in local
										// shouldnt ever happen
										break;
									case "remove":
										// modified in pending, removed locally
										// should remove
										matchNode.Attributes["operation"].Value = loper;
										break;
								}
									break;
							}
						}
						else if(loper == "modify" && matchNode.Name == "Tag")
						{
							// make sure we use the most recently modified tag value
							if(ln.Attributes["value"]!=null)
							{
								if(matchNode.Attributes["value"]==null)
								{
									matchNode.Attributes.Append(matchNode.OwnerDocument.CreateAttribute("value"));
								}
								matchNode.Attributes["value"].Value = ln.Attributes["value"].Value;
							}
							else
							{
								if(matchNode.Attributes["value"]!=null)
								{
									matchNode.Attributes.Remove(matchNode.Attributes["value"]);
								}
							}
							
						}
					}
					else if(ln.Attributes["operation"] != null && matchNode.Attributes["operation"] == null)
					{	
						// copy the local attribute into the pending wave list
						XmlAttribute attrib = m_Document.CreateAttribute("operation");
						attrib.Value = ln.Attributes["operation"].Value;
						matchNode.Attributes.Append(attrib);
					}
					else
					{
						// no attribute in local, do nothing (and continue using pending wave list attrib)
					}


				}
				// continue combine
				if(matchNode != null)
				{
					Combine(matchNode, ln);
				}
			}
		}

		

		private XmlNode FindNodeFromPath(string path)
		{
			return FindNodeFromPath(path, m_Document.DocumentElement);
		}

		private XmlNode FindParentNodeFromPath(string path)
		{
			int index = path.LastIndexOf("\\");
			string objPath = path.Remove(index, path.Length - index);

			return FindNodeFromPath(objPath);
		}

		private XmlNode FindNodeFromPath(string path, XmlNode parent)
		{
			string[] elems = path.Split('\\');
			XmlNode currentNode = parent;	

			foreach(string s in elems)
			{
				if(s.Length <= 0)
				{
					continue;
				}
				bool foundNode = false;

				foreach(XmlNode n in currentNode.ChildNodes)
				{
					string name = n.Attributes["name"].Value;

					if(name == s)
					{
						currentNode = n;
						foundNode = true;
						break;
					}
				}

				if(!foundNode)
				{
					return null;
				}
			}

			return currentNode;
		}
	}
}
