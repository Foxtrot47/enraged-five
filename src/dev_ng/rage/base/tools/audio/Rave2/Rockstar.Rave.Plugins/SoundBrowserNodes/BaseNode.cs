using System.Windows.Forms;

namespace Rave.Plugins.SoundBrowser.Nodes
{
    /// <summary>
    ///   Summary description for BaseNode.
    /// </summary>
    public abstract class BaseNode : TreeNode
    {
        protected BaseNode(string objectName, string episode, string type)
        {
            ObjectName = objectName;
            Text = objectName;
            Type = type;
            Episode = episode;
        }

        public string Episode { get; set; }

        public string Type { get; set; }

        public string ObjectName { get; set; }

        public abstract int GetBaseIconIndex();

        public void ResetText()
        {
            Text = ObjectName;
        }

        public string GetObjectPath()
        {
            if (Parent == null)
            {
                return ObjectName;
            }
            return ((BaseNode) Parent).GetObjectPath() + "\\" + ObjectName;
        }

        public abstract string GetTypeDescription();
    }
}