using System;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Xml;
using System.Reflection;
using System.Xml.Xsl;

namespace Rave.Plugins
{
    public partial class frmReport : Form
    {
        private string m_FilePath;

        public frmReport(string title)
        {
            InitializeComponent();
            this.Text = title;
            this.BringToFront();
        }

        public void SetURL(string url)
        {
            m_FilePath = url;
            m_WebBrowser.Url = new Uri(url);
        }

        public void SetText(string text)
        {
            m_WebBrowser.DocumentText = text;
        }

        public void SetXml(XmlDocument doc)
        {
            Stream s = Assembly.GetExecutingAssembly().GetManifestResourceStream("Rave.Plugins.defaultss.xsl");

            XmlReader xr = XmlReader.Create(s);
            XslCompiledTransform xct = new XslCompiledTransform();
            xct.Load(xr);

            StringBuilder sb = new StringBuilder();
            XmlWriter xw = XmlWriter.Create(sb);
            xct.Transform(doc, xw);
            xw.Close();
            m_WebBrowser.DocumentText = sb.ToString();
            m_FilePath = "xml";
        }

        private void OnClose(object sender, FormClosingEventArgs e)
        {
            if(m_FilePath !="" && File.Exists(m_FilePath))
            {
                File.Delete(m_FilePath);
            }
        }

        private void Save_Click(object sender, EventArgs e)
        {
          
            if (m_FilePath==null || m_FilePath.EndsWith("html"))
            {
                saveFileDialog1.Filter = "html files (*.html)|*.html";
            }
            else if (m_FilePath.EndsWith("xml"))
            {
                saveFileDialog1.Filter = "xml files (*.xml)|*.xml";
            }
            else
            {
                saveFileDialog1.Filter = "txt files (*.txt)|*.txt|csv files (*.csv)|*.csv";
            }

            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                Stream mystream;
                if ((mystream = saveFileDialog1.OpenFile()) != null)
                {
                    StreamReader sr = new StreamReader(this.m_FilePath);
                    StreamWriter sw = new StreamWriter(mystream);
                    sw.Write(sr.ReadToEnd());
                    sr.Close();
                    sw.Close();
                    mystream.Close();
                }
            }
            saveFileDialog1.Dispose();
        }

        private void Exit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}