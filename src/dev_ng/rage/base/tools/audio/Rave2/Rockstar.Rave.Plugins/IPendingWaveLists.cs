using System.Windows.Forms;

namespace Rave.Plugins
{
    public interface IPendingWaveLists
    {
        bool LoadLatestWaveLists();

        bool LockPendingWaveList();

        bool SerialisePendingWaveList();

        void ShowPendingChanges(TreeView treeView, IActionLog log);

        void ShowBuiltWaves(TreeView treeView, IActionLog log);

        IPendingWaveList GetPendingWaveList(string platform);

        bool ArePendingWavelistsCheckedOut();

        bool ContainsVoiceTags(string platformTag);

        bool ContainsVoiceTags(string platformTag, string packName);
    }
}