using System;
using System.Collections.Generic;
using System.Text;
using Rave.Plugins.SoundHierarchyNodes;
using System.Collections;

namespace Rave.Plugins
{

    public interface IRAVEObjectBrowserPlugin : IRAVEPlugin
    {
        void Process(ArrayList nodes);
    }

}
