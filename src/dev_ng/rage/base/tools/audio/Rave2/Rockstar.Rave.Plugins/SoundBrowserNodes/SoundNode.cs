namespace Rave.Plugins.SoundBrowser.Nodes
{
    /// <summary>
    ///   Summary description for SoundNode.
    /// </summary>
    public class SoundNode : BaseNode
    {
        private readonly int m_iconIndex;

        public SoundNode(IObjectInstance sound, int iconIndex) : base(sound.Name, sound.Episode, sound.Type)
        {
            ObjectInstance = sound;
            m_iconIndex = iconIndex;
            ImageIndex = iconIndex;
            SelectedImageIndex = iconIndex;
        }

        public IObjectInstance ObjectInstance { get; set; }

        public override int GetBaseIconIndex()
        {
            return m_iconIndex;
        }

        public override string GetTypeDescription()
        {
            return "Sound";
        }
    }
}