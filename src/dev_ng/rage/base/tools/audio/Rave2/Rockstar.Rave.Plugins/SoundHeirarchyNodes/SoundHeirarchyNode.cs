using System.Drawing;
using System.Windows.Forms;

namespace Rave.Plugins.SoundHierarchyNodes
{
    /// <summary>
    ///   Summary description for SoundHierarchyNode.
    /// </summary>
    public class SoundHeirarchyNode : TreeNode
    {
        private const string INVALID_REFERENCE = "(invalid reference)";
        private string m_objectName;

        public SoundHeirarchyNode(IObjectInstance sound)
        {
            Sound = sound;
            ImageIndex = 0;

            if (sound == null)
            {
                ObjectName = INVALID_REFERENCE;
                ForeColor = Color.Red;
            }
            else
            {
                ObjectName = sound.Name;
            }
        }

        public IObjectInstance Sound { get; private set; }

        public string ObjectName
        {
            get { return m_objectName; }
            set
            {
                m_objectName = value;
                Text = m_objectName;
            }
        }
    }
}