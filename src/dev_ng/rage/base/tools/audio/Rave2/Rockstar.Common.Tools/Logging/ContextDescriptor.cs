namespace rage.ToolLib.Logging
{
    public class ContextDescriptor
    {
        private const string FORMAT_TO_STRING = "{0}:{1}";

        public ContextDescriptor(ContextType type, string name)
        {
            Type = type;
            Name = name;
        }

        public ContextType Type { get; private set; }

        public string Name { get; private set; }

        public override string ToString()
        {
            return string.Format(FORMAT_TO_STRING, Type, Name);
        }
    }
}