using System;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Linq;

namespace rage.ToolLib.WavesFiles
{
    public class WavesFileCombiner : IWavesFileCombiner
    {
        private const string PACK = "Pack";
        private const string PACK_FILE = "PackFile";
        private const string XML_EXTENSION = ".xml";

        #region IWavesFileCombiner Members

        public string Run(string packListFile, string destinationDirectory)
        {
            if (!File.Exists(packListFile))
            {
                throw new ApplicationException(string.Format("Pack list file: {0} does not exist", packListFile));
            }

            return Process(packListFile, destinationDirectory);
        }

        #endregion

        private static string Process(string packListFile, string destinationDirectory)
        {
            if (!File.Exists(packListFile))
            {
                throw new ApplicationException(string.Format("Pack List file: {0} does not exist", packListFile));
            }

            if (!Directory.Exists(destinationDirectory))
            {
                Directory.CreateDirectory(destinationDirectory);
            }

            return Combine(packListFile, destinationDirectory);
        }

        private static string Combine(string packListFile, string destinationDirectory)
        {
            var packListDirectory = Path.GetDirectoryName(packListFile);
            var packListDoc = XDocument.Load(packListFile);

            var packFiles =
                (from pack in packListDoc.Root.Descendants(PACK_FILE)
                 select Path.Combine(packListDirectory, pack.Value)).ToList();

            var rootElementName = packListDoc.Root.Name;
            var root = new XElement(rootElementName);

            foreach (var packFile in packFiles)
            {
                var doc = LoadWavesFile(packFile);
                if (doc.Root != null &&
                    doc.Root.Name == PACK)
                {
                    root.Add(doc.Root);
                }
            }

            var destinationDoc = new XDocument(root);
            var outputFile = Path.Combine(destinationDirectory, string.Concat(rootElementName, XML_EXTENSION));
            destinationDoc.Save(outputFile);

            return outputFile;
        }

        private static XDocument LoadWavesFile(string fileName)
        {
            var readerSettings = new XmlReaderSettings
                                     {
                                         IgnoreComments = true,
                                         IgnoreWhitespace = true,
                                         IgnoreProcessingInstructions = true,
                                         CloseInput = true
                                     };
            using (var reader = XmlReader.Create(File.OpenRead(fileName), readerSettings))
            {
                return XDocument.Load(reader);
            }
        }
    }
}