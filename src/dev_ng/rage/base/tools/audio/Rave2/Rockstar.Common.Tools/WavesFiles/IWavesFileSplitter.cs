using System.Collections.Generic;

namespace rage.ToolLib.WavesFiles
{
    public interface IWavesFileSplitter
    {
        IEnumerable<string> Run(string sourceFile, string outputDirectory);
    }
}