namespace rage.ToolLib.Logging
{
    public interface IContextStack
    {
        string CurrentContext { get; }

        void Push(ContextType type, string name);

        ContextDescriptor Peek();

        ContextDescriptor Pop();

        void Clear();
    }
}