using System;
using System.Diagnostics;

namespace rage.ToolLib.CmdLine
{
    public class NoOutputAsyncCommandExecutor : IAsyncCommandExecutor
    {
        private bool m_isExecuting;
        public event Action<Exception> Error;
        public event Action<bool> Finished;

        public void Execute(string command)
        {
            if (m_isExecuting)
            {
                throw new InvalidOperationException("Attemping to execute multiple commands with a NoOutputAsyncCommandExecutor at the same time.");
            }

            Execute(new ProcessStartInfo(command));
        }

        public void Execute(string command, string args)
        {
            if (m_isExecuting)
            {
                throw new InvalidOperationException("Attemping to execute multiple commands with a NoOutputAsyncCommandExecutor at the same time.");
            }

            Execute(new ProcessStartInfo(command, args));
        }

        private void Execute(ProcessStartInfo psi)
        {
            psi.CreateNoWindow = true;
            psi.UseShellExecute = false;

            try
            {
                var process = new Process {StartInfo = psi, EnableRaisingEvents = true};
                process.Exited += OnProcessExited;

                m_isExecuting = true;
                process.Start();
            }
            catch (Exception e)
            {
                Error.Raise(e);

                m_isExecuting = false;
                Finished.Raise(false);
            }
        }

        private void OnProcessExited(object sender, EventArgs e)
        {
            var process = sender as Process;
            if (process != null)
            {
                m_isExecuting = false;
                Finished.Raise(process.ExitCode == 0);
            }
        }
    }
}