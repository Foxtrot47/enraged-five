using System.Collections.Generic;

namespace rage.ToolLib.Repository
{
    public interface IRepository<TKey, TValue> where TValue : IRepositoryItem<TKey>
    {
        IEnumerable<TValue> Items { get; }

        bool TryGetItem(TKey id, out TValue item);

        void Add(TValue item);

        void Remove(TValue item);
    }
}