using System;
using System.Net.Mail;
using rage.ToolLib.Logging;

namespace rage.ToolLib.Mail
{
    public class SmtpMailSender : IMailSender
    {
        private readonly ILog m_log;
        private readonly string m_host;

        public SmtpMailSender(ILog log, string host)
        {
            if (log == null)
            {
                throw new ArgumentNullException("log");
            }
            if (string.IsNullOrEmpty(host))
            {
                throw new ArgumentException("host");
            }
            m_log = log;
            m_host = host;
        }

        public bool Send(MailMessage message)
        {
            try
            {
                var client = new SmtpClient(m_host);
                client.Send(message);
            }
            catch(Exception e)
            {
                m_log.Exception(e);
                return false;
            }
            return true;
        }
    }
}