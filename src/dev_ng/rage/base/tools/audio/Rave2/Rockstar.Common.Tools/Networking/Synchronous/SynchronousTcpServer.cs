using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;

namespace rage.ToolLib.Networking.Synchronous
{
    public class SynchronousTcpServer : IServer
    {
        private readonly IClientFactory m_clientFactory;
        private readonly TcpListener m_listener;

        public SynchronousTcpServer(IClientFactory clientFactory, int port)
        {
            if (clientFactory == null)
            {
                throw new ArgumentNullException("clientFactory");
            }
            m_clientFactory = clientFactory;
            m_listener = new TcpListener(IPAddress.Any, port);
        }

        #region IServer Members

        public IEnumerable<IClient> Clients
        {
            get { return m_clientFactory.Clients; }
        }

        public event Action<IClient> ClientConnected;
        public event Action<IClient> ClientDisconnected;

        public void Start()
        {
            m_listener.Start();
        }

        public void Update()
        {
            while (m_listener.Pending())
            {
                ClientConnected.Raise(m_clientFactory.Create(m_listener.AcceptTcpClient()));
            }

            var disconnectedClients =
                (from client in m_clientFactory.Clients where !client.IsConnected select client).ToList();
            foreach (var disconnectedClient in disconnectedClients)
            {
                ClientDisconnected.Raise(disconnectedClient);
                m_clientFactory.Destroy(disconnectedClient);
            }
        }

        public void Stop()
        {
            m_listener.Stop();
            m_clientFactory.DestroyAll();
            foreach (var client in m_clientFactory.Clients)
            {
                ClientDisconnected.Raise(client);
            }
        }

        #endregion
    }
}