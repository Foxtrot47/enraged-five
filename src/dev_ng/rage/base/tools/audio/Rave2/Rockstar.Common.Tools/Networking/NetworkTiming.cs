using System;

namespace rage.ToolLib.Networking
{
    public class NetworkTiming : INetworkTiming
    {
        private readonly DateTime m_startTime;

        public NetworkTiming()
        {
            m_startTime = DateTime.Now;
        }

        #region INetworkTiming Members

        public uint GetTimestamp()
        {
            return (uint) ((DateTime.Now - m_startTime).TotalMilliseconds);
        }

        #endregion
    }
}