using System;

namespace rage.ToolLib
{
    public static class EventExtensions
    {
        public static void Raise(this Action action)
        {
            if (action != null)
            {
                action();
            }
        }

        public static void Raise<T>(this Action<T> action, T payload1)
        {
            if (action != null)
            {
                action(payload1);
            }
        }

        public static void Raise<T1, T2>(this Action<T1, T2> action, T1 payload1, T2 payload2)
        {
            if (action != null)
            {
                action(payload1, payload2);
            }
        }

        public static void Raise<T1, T2, T3>(this Action<T1, T2, T3> action, T1 payload1, T2 payload2, T3 payload3)
        {
            if (action != null)
            {
                action(payload1, payload2, payload3);
            }
        }
    }
}