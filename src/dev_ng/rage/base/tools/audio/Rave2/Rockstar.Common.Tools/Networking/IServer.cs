using System;
using System.Collections.Generic;

namespace rage.ToolLib.Networking
{
    public interface IServer
    {
        IEnumerable<IClient> Clients { get; }
        event Action<IClient> ClientConnected;
        event Action<IClient> ClientDisconnected;

        void Start();

        void Update();

        void Stop();
    }
}