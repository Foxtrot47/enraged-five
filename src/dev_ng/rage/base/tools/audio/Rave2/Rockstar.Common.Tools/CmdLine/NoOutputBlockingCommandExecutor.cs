using System;
using System.Diagnostics;

namespace rage.ToolLib.CmdLine
{
    public class NoOutputBlockingCommandExecutor : ICommandExecutor
    {
        #region ICommandExecutor Members

        public Exception Error { get; private set; }

        public string Output
        {
            get { return string.Empty; }
        }

        public bool Execute(string command)
        {
            return Execute(new ProcessStartInfo(command));
        }

        public bool Execute(string command, string args)
        {
            return Execute(new ProcessStartInfo(command, args));
        }

        #endregion

        private bool Execute(ProcessStartInfo psi)
        {
            psi.CreateNoWindow = true;
            psi.UseShellExecute = false;

            try
            {
                var process = new Process {StartInfo = psi};
                process.Start();
                process.WaitForExit();
                return process.ExitCode == 0;
            }
            catch (Exception e)
            {
                Error = e;
                return false;
            }
        }
    }
}