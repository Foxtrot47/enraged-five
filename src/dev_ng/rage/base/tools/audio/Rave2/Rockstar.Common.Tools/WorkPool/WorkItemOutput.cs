namespace rage.ToolLib.WorkPool
{
    public class WorkItemOutput
    {
        public WorkItemOutputTypes Type { get; set; }
        public string Message { get; set; }
    }
}