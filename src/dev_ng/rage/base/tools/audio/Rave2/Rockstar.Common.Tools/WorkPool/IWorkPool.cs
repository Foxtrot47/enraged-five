using System.Collections.Generic;

namespace rage.ToolLib.WorkPool
{
    public interface IWorkPool
    {
        IWorkItem ErrorItem { get; }

        bool Process(List<IWorkItem> workItems);
    }
}