﻿namespace rage.ToolLib.Logging
{
    public enum ContextType
    {
        Application,
        MetadataType,
        Enum,
        Field,
        File,
        Object,
        Template,
        Type,
        Unknown,
    }
}