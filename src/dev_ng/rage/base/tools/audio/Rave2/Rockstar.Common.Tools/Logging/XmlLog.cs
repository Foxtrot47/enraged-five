using System;
using System.Xml.Linq;

namespace rage.ToolLib.Logging
{
    public class XmlLog : ILog
    {
        private const string FORMAT_3_ARGS = "{0}{1}{2}";
        private const string CONTEXT = "context";

        public const string EXCEPTION = "Exception";

        public const string ERROR = "Error";

        public const string WARNING = "Warning";

        private readonly IContextStack m_contextStack;

        private readonly XElement m_errorElement;

        private readonly XElement m_exceptionElement;

        private readonly XElement m_infoElement;
        private readonly string m_rootElementName;

        private readonly XElement m_textElement;

        private readonly XElement m_warningElement;

        private readonly ILogWriter m_writer;

        private bool m_disposed;

        public XmlLog(ILogWriter writer, IContextStack contextStack, string rootElementName)
        {
            if (writer == null)
            {
                throw new ArgumentNullException("writer");
            }
            if (contextStack == null)
            {
                throw new ArgumentNullException("contextStack");
            }
            if (string.IsNullOrEmpty(rootElementName))
            {
                throw new ArgumentNullException("rootElementName");
            }
            m_writer = writer;
            m_contextStack = contextStack;
            m_rootElementName = rootElementName;
            m_exceptionElement = new XElement(EXCEPTION);
            m_errorElement = new XElement(ERROR);
            m_warningElement = new XElement(WARNING);
            m_infoElement = new XElement("Info");
            m_textElement = new XElement("Text");
            m_writer.WriteLine(FORMAT_3_ARGS, "<", rootElementName, ">");
        }

        #region ILog Members

        public void Dispose()
        {
            if (!m_disposed)
            {
                m_disposed = true;
                m_writer.WriteLine(FORMAT_3_ARGS, "</", m_rootElementName, ">");
            }
        }

        public LogVerbosity Verbosity { get; set; }

        public uint NumExceptionsLogged { get; private set; }

        public uint NumErrorsLogged { get; private set; }

        public uint NumWarningsLogged { get; private set; }

        public event Action<string, Exception> ExceptionLogged;

        public event Action<string, string> ErrorLogged;

        public event Action<string, string> WarningLogged;

        public event Action<string, string> InformationLogged;

        public void Exception(Exception exception)
        {
            NumExceptionsLogged++;
            m_exceptionElement.ReplaceAll(new XAttribute(CONTEXT, m_contextStack.CurrentContext), exception);
            m_writer.WriteLine(m_exceptionElement);
            ExceptionLogged.Raise(m_contextStack.CurrentContext, exception);
        }

        public void Error(string error, params object[] args)
        {
            NumErrorsLogged++;
            var formattedError = string.Format(error, args);
            m_errorElement.ReplaceAll(new XAttribute(CONTEXT, m_contextStack.CurrentContext), formattedError);
            m_writer.WriteLine(m_errorElement);
            ErrorLogged.Raise(m_contextStack.CurrentContext, formattedError);
        }

        public void Warning(string warning, params object[] args)
        {
            NumWarningsLogged++;
            var formattedWarning = string.Format(warning, args);
            m_warningElement.ReplaceAll(new XAttribute(CONTEXT, m_contextStack.CurrentContext), formattedWarning);
            m_writer.WriteLine(m_warningElement);
            WarningLogged.Raise(m_contextStack.CurrentContext, formattedWarning);
        }

        public void Information(string info, params object[] args)
        {
            if (Verbosity == LogVerbosity.Everything)
            {
                var formattedInfo = string.Format(info, args);
                m_infoElement.ReplaceAll(new XAttribute(CONTEXT, m_contextStack.CurrentContext), formattedInfo);
                m_writer.WriteLine(m_infoElement);
                InformationLogged.Raise(m_contextStack.CurrentContext, formattedInfo);
            }
        }

        public void WriteFormatted(string data, params object[] args)
        {
            var text = string.Format(data, args);
            if (!string.IsNullOrEmpty(text))
            {
                m_textElement.ReplaceAll(text);
                m_writer.WriteLine(m_textElement);
            }
        }

        public void Write(object data)
        {
            m_writer.WriteLine(data);
        }

        public void ResetLogCounts()
        {
            NumExceptionsLogged = NumErrorsLogged = NumWarningsLogged = 0;
        }

        public void PushContext(ContextType type, string name)
        {
            m_contextStack.Push(type, name);
        }

        public ContextDescriptor PeekContext()
        {
            return m_contextStack.Peek();
        }

        public ContextDescriptor PopContext()
        {
            return m_contextStack.Pop();
        }

        #endregion
    }
}