using System;

namespace rage.ToolLib.Logging
{
    public class TextLog : ILog
    {
        private const string FORMAT_INFO = "Info: {0}";

        private const string FORMAT_WARNING = "Warning: {0}";

        private const string FORMAT_ERROR = "Error: {0}";

        private const string FORMAT_EXCEPTION = "Exception: {0}";

        private readonly IContextStack m_contextStack;

        private readonly ILogWriter m_writer;

        public TextLog(ILogWriter writer, IContextStack contextStack)
        {
            if (writer == null)
            {
                throw new ArgumentNullException("writer");
            }
            if (contextStack == null)
            {
                throw new ArgumentNullException("contextStack");
            }
            m_writer = writer;
            m_contextStack = contextStack;
        }

        #region ILog Members

        public void Dispose()
        {
            // do nothing
        }

        public LogVerbosity Verbosity { get; set; }
        public uint NumExceptionsLogged { get; private set; }
        public uint NumErrorsLogged { get; private set; }
        public uint NumWarningsLogged { get; private set; }
        public event Action<string, Exception> ExceptionLogged;
        public event Action<string, string> ErrorLogged;
        public event Action<string, string> WarningLogged;
        public event Action<string, string> InformationLogged;

        public void Exception(Exception exception)
        {
            NumExceptionsLogged++;
            m_writer.WriteLine(FORMAT_EXCEPTION, m_contextStack.CurrentContext);
            m_writer.WriteLine(exception);
            ExceptionLogged.Raise(m_contextStack.CurrentContext, exception);
        }

        public void Error(string error, params object[] args)
        {
            NumErrorsLogged++;
            m_writer.WriteLine(FORMAT_ERROR, m_contextStack.CurrentContext);
            m_writer.WriteLine(error, args);
            ErrorLogged.Raise(m_contextStack.CurrentContext, string.Format(error, args));
        }

        public void Warning(string warning, params object[] args)
        {
            NumWarningsLogged++;
            m_writer.WriteLine(FORMAT_WARNING, m_contextStack.CurrentContext);
            m_writer.WriteLine(warning, args);
            WarningLogged.Raise(m_contextStack.CurrentContext, string.Format(warning, args));
        }

        public void Information(string info, params object[] args)
        {
            if (Verbosity == LogVerbosity.Everything)
            {
                m_writer.WriteLine(FORMAT_INFO, m_contextStack.CurrentContext);
                m_writer.WriteLine(info, args);
                InformationLogged.Raise(m_contextStack.CurrentContext, string.Format(info, args));
            }
        }

        public void WriteFormatted(string data, params object[] args)
        {
            m_writer.WriteLine(data, args);
        }

        public void Write(object data)
        {
            m_writer.WriteLine(data);
        }

        public void ResetLogCounts()
        {
            NumExceptionsLogged = NumErrorsLogged = NumWarningsLogged = 0;
        }

        public void PushContext(ContextType type, string name)
        {
            m_contextStack.Push(type, name);
        }

        public ContextDescriptor PeekContext()
        {
            return m_contextStack.Peek();
        }

        public ContextDescriptor PopContext()
        {
            return m_contextStack.Pop();
        }

        #endregion
    }
}