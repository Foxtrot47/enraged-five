using System.Collections.Generic;

namespace rage.ToolLib.DataStructures
{
    public class TreeNode<T>
    {
        public TreeNode(TreeNode<T> parent)
        {
            Parent = parent;
            Children = new List<TreeNode<T>>();
        }

        public T Data { get; set; }
        public TreeNode<T> Parent { get; set; }
        public IList<TreeNode<T>> Children { get; private set; }
    }
}