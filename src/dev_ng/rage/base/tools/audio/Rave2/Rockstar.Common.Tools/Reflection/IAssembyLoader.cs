using System.Collections.Generic;
using System.Reflection;

namespace rage.ToolLib.Reflection
{
    public interface IAssembyLoader
    {
        ICollection<Assembly> Load(string[] paths);
    }
}