namespace rage.ToolLib.WorkPool
{
    public enum WorkItemOutputTypes
    {
        Error,
        Information,
        Warning
    }
}