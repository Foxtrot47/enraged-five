using System;
using System.IO;

namespace rage.ToolLib.Writer
{
    public interface IWriter : IDisposable
    {
        System.IO.BinaryWriter BinWriter { get; }

        bool IsBigEndian { get; }

        void Close();

        void Flush();

        uint Seek(uint offset, SeekOrigin origin);

        uint Tell();

        void Write(sbyte val);

        void Write(byte val);

        void Write(byte[] val, bool needsEndian);

        void Write(short val);

        void Write(ushort val);

        void Write(int val);

        void Write(uint val);

        void Write(long val);

        void Write(ulong val);

        void Write(float val);

        void Write(string val);
    }
}