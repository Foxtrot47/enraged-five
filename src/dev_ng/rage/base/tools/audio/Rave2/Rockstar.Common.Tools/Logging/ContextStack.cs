using System.Collections.Generic;
using System.Text;

namespace rage.ToolLib.Logging
{
    public class ContextStack : IContextStack
    {
        private readonly List<ContextDescriptor> m_stack = new List<ContextDescriptor>();

        #region IContextStack Members

        public string CurrentContext
        {
            get
            {
                var builder = new StringBuilder();
                foreach (var descriptor in m_stack)
                {
                    builder.Append(string.Format("\\[{0}]", descriptor));
                }
                return builder.ToString();
            }
        }

        public void Push(ContextType type, string name)
        {
            m_stack.Add(new ContextDescriptor(type, name));
        }

        public ContextDescriptor Peek()
        {
            var count = m_stack.Count;
            if (count > 0)
            {
                return m_stack[count - 1];
            }
            return null;
        }

        public ContextDescriptor Pop()
        {
            var count = m_stack.Count;
            if (count > 0)
            {
                var descriptor = m_stack[count - 1];
                m_stack.RemoveAt(count - 1);
                return descriptor;
            }
            return null;
        }

        public void Clear()
        {
            m_stack.Clear();
        }

        #endregion
    }
}