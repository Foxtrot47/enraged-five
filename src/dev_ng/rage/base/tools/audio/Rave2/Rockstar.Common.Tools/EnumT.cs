﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace rage.ToolLib
{
    public static class Enum<T>
    {
        public static T Parse(string value)
        {
            return (T)Enum.Parse(typeof(T), value, true);
        }

        public static T Parse(string value, bool ignoreCase)
        {
            return (T)Enum.Parse(typeof(T), value, ignoreCase);
        }

        public static T TryParse(string value, bool ignoreCase, T defaultVal)
        {
           string[] names = Enum.GetNames(typeof(T));
           if (names.Contains(value, ignoreCase ? StringComparer.OrdinalIgnoreCase : StringComparer.Ordinal))
           {
               return Parse(value, ignoreCase);
           }          
           return defaultVal;
        }

        public static IList<T> GetValues()
        {
            var list = Enum.GetValues(typeof (T)).Cast<T>().ToList();
            list.Sort((x, y) => string.Compare(x.ToString(), y.ToString(), true));
            return list;
        }
    }  
}
