using System.Text;

namespace rage.ToolLib
{
    // adapted from rage atl/map.cpp
    public class Hash
    {
        private string m_value;

        public uint Key { get; private set; }
        public uint PartialKey { get; private set; }

        public string Value
        {
            get { return m_value; }
            set
            {
                Key = 0;
                if (!string.IsNullOrEmpty(value))
                {
                    m_value = value;
                    GenerateHash();
                }
                else
                {
                    m_value = string.Empty;
                }
            }
        }

        private void GenerateHash()
        {
            // This is the one-at-a-time hash from this page:
            // http://burtleburtle.net/bob/hash/doobs.html
            // (borrowed from /soft/swat/src/swcore/string2key.cpp)
            var str = Encoding.UTF8.GetBytes(Value.Replace("\"", string.Empty).Replace("\\", "/").ToLower());
            var length = str.Length;
            for (var index = 0; index < length; ++index)
            {
                Key += str[index];
                Key += (Key << 10); //lint !e701
                Key ^= (Key >> 6); //lint !e702
            }
            // Set partial key at this point
            PartialKey = Key;
            // Continue to generate full key
            Key += (Key << 3); //lint !e701
            Key ^= (Key >> 11); //lint !e702
            Key += (Key << 15); //lint !e701
            // The original swat code did several tests at this point to make
            // sure that the resulting value was representable as a valid
            // floating-point number (not a negative zero, or a NaN or Inf)
            // and also reserved a nonzero value to indicate a bad key.
            // We don't do this here for generality but the caller could
            // obviously add such checks again in higher-level code.
        }
    }
}